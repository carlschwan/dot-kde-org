---
title: "KDE-CVS-Digest for July 2, 2004"
date:    2004-07-03
authors:
  - "dkite"
slug:    kde-cvs-digest-july-2-2004
comments:
  - subject: "Life's good. "
    date: 2004-07-03
    body: "Nothing like Saturday morning ... fresh baguettes, fresh coffee, fresh CVS Digest."
    author: "Eike Hein"
  - subject: "Kmplot source comparison"
    date: 2004-07-03
    body: "In kmplot (and I guess everywhere else), the \"view source\" code comparison output is very impressive and looks like something we would like to use - how was that generated? "
    author: "john"
  - subject: "Re: Kmplot source comparison"
    date: 2004-07-03
    body: "It's in the kde cvs repository at www/areas/cvs-digest/enzyme\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/www/areas/cvs-digest/enzyme/\n\nenzyme_diff.php, enzyme_diffparts.php are the pertinent files.\n\nIf you have any suggestions please email.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Kmplot source comparison"
    date: 2004-07-03
    body: "Thks for the info - I think this would be really powerful if the php programs could be integrated into KDevelop + subversion to be used in regular application programming (though I guess you could say it is being used in the production of KDE).  Very nice effort - will make code review far easier."
    author: "john"
  - subject: "Re: Kmplot source comparison"
    date: 2004-07-05
    body: "kompare works well on the desktop.\n\nWhat this and webcvs have are ways to view the repository differences, ie. extract patches. A necessary tool for collaborative development. Somehow having apache and php running on one's machine for development seems overkill.\n\nDerek"
    author: "Derek Kite"
  - subject: "KDE 4"
    date: 2004-07-03
    body: "Hmm, \n\n1. A Personal desktop assistent :-)\n2. Speech sythesis and recognition\n3. VRML+ Modules\n4. video processing\n5. A Planner tool for Kontact\n6. a nice chess client\n7 Yast integration"
    author: "hein"
  - subject: "Re: KDE 4"
    date: 2004-07-04
    body: "I'd chose more and better Scientific Stuff!\n\nMostly frontends to already existing stuff like R, Maxima, Yacas, Octave. Shouldn't be that complicated and I bet most of the projects would be willing to cooperate. This stuff would be much more mainstream if there were nice interfaces with integrated tutorials etc. (take a look at MuPAD for instance).\n\nAh and have board games providing standard interfaces for external programs to connect and play!! That would be excelent as they could be used to test other peoples Artificial Inteligence modules really easily!! That would attract lots of great coders!!\n\nOh and KDE/Qt bindings to functional languages, preferably Haskell.\nGnome will end up owning all Haskellers because of the GTK+ and WxWindows bindings. There are lots of them and it would be interesting to reach a wider variety of great coders."
    author: "Plake"
  - subject: "Functional Languages"
    date: 2004-07-04
    body: "Ashley Winters (PerlQt/Smoke scripting language independent lib designer) is working on a O'CamlQt binding - I don't know if he has a kde version planned. How does O'Caml compare with Haskell?"
    author: "Richard Dale"
  - subject: "Re: Functional Languages"
    date: 2004-07-04
    body: "I've never used it, thought the main differences are probably that Haskell is lazy and purely functional, while O'Caml is strict and impure. It can still probably be of some use though if someone's giving Haskell bindings a try.\n\nI wouldn't expect it to be that complicated to do. Like I said there are GTK and WxWindows bindings. These are C libraries, but KDE C bindings are there for the purpose of inteface with other languages right?\n\nAlso it is possible to interface Haskell directly with OO Languages. \nHere is a paper that discribes Haskell Objective-C bindings on MacOSX.\nhttp://www.cse.unsw.edu.au/~chak/papers/PC03.html\n\nAnyway, I might be wrong as I have no experience in this sort of task (otherwise I'd probably be giving it a try myself)."
    author: "Plake"
  - subject: "Re: KDE 4"
    date: 2004-07-04
    body: "- Releasing the core independenly from the applications\n\nCore: kdesktop, kicker and konqueror, kdm, arts, and all the system daemons\n-> kdelibs and kdebase\n\nThe \"normal\" applications should be released whenever there is a need\nor a new version available.\n\n\n\nThank you for KDE, it rocks."
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2004-07-05
    body: "> Oh and KDE/Qt bindings to functional languages, preferably Haskell.\n> Gnome will end up owning all Haskellers because of the GTK+ and WxWindows bindings. There are lots of them and it would be interesting to reach a wider variety of great coders.\n\nTake a look at Kommander, which is coming along nicely. Kommander visually builds the GUI using standard *.ui format files and the editor is currently based on Designer, though that will almost certainly change in 4.0. It has a concise and useful set of internal functions based on very quick internal DCOP and can be externally DCOP scripted from any application or the command line. It can also integrate any scripting language. I'm not familiar with Haskell, except having heard of it, but any compiled language with DCOP bindings should be easy to integrate with Kommander applications too. In fact you can have multiple scripting languages in the same dialog or even the same bound widget. Additionally we are in work on MainWindow support and free slots running scripts.\n\nKommander is one of KDE's best kept secrets which will work well for:\n* scripters wanting GUI interfaces - any language the shell supports!\n* prototyping\n* users wanting to extend and integrate with KDE\n* application developers wanting to deliver no compile updates and specific interfaces that are user editable\n\nLinks to check things out...\nhttp://dot.kde.org/1087424515/ - recent article on the Dot\nhttp://kde-apps.org/content/show.php?content=12865 - latest release"
    author: "Eric Laffoon"
  - subject: "Re: KDE 4"
    date: 2004-07-04
    body: "Dreaming or how much do you pay?"
    author: "Anonymous"
  - subject: "Re: KDE 4"
    date: 2004-07-04
    body: "I'd add \nmeta-data search capabilities (maybe using a kio-slave, and with ReiserFS4 compatibillity?)\nand cairo/3d support.\nmaybe a sidebar/dasboard like tool (http://www.nat.org/dashboard/)\nbetter integration of all communication tools (kmail/koptete etc)\nsupport in konqi/kparts for also edditing of files (kword embedded in konqi, and being able to edit the file too, I'd love that. no more 5 kword windows open, just 1 konqi, with a filemanager, webpages, kword, all nicely in 1 window - a whole \"projeKt\" together. and of course I'd love being able to save the projekt! Gnome adds more windows for 1 task, KDE can integrate windows in 1... I think that's much better but its just me ;-) )"
    author: "superstoned"
  - subject: "Re: KDE 4"
    date: 2004-07-04
    body: ">2. Speech synthesis and recognition\n\nKdeaccessibility has synthesis. There is a good free speech synthesis engine available which makes it possible.\n\nRecognition is difficult. I would have thought that after 10 or 15 years of development there would be a good workable solution available, but there really isn't. A couple of years ago I talked to a fellow who worked for RCMP, where they have to transcribe data. He tried all the solutions, watched developments over a few years, but it didn't hit the sweet spot where it worked reliably for everyone. And the two big players seemed to have given up.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KDE 4"
    date: 2004-07-04
    body: "My KDE 4 wishlist:\n\n1) Low latency, high-performance replacement for aRts.\n2) A real option for a \"minimalist\" UI, that doesn't involve manually removing dozens of toolbar and context menu items.\n3) Some esthetic touches, like a KIO download status box not completely covered with text, removal of some extraneous bevels here and there, and a less-ugly replacement for the Qt \"disabled icon\" effect.\n4) Faster application startup.\n5) Full Crystalization for icons that still don't have it (eg: Cervisia).\n\nAll in all, pretty minor stuff. That's how good KDE is already :)"
    author: "Rayiner Hashem"
  - subject: "Re: KDE 4"
    date: 2004-07-05
    body: "I second this plus :\n1) A improvement/replacement for aRts so that I could enjoy amarok without having 90% cpu load and sound skips.\n2) An interface builder integrated to Kdevelop / so that everybody could finally easily (ie with very few knowledge) write applications for KDE.\n3) Compatibility with XUL so that one could develop rich web applications that we could use/deploy both on the Mozilla and the KDE platform (we need that for this technology to gain momentum).\n4) Modularity in the UI : possibility to have like different \" UI schemas\" for beginners (few options available), medium (reasonnable set of options) and expert (all options available). Maybe we could link that with Waldo's kiosk tool (to generate \"UI schemas\").\n5) An global underlying layer that would manage user's data and would enable web services so that information (contacts, bookmarks, tasks...) can easily be shared / accessed between applications / machines / platforms.\n6) Tight integration of Nomachine's NX technology (client & server) to have native thin client computing possibilities (Access to distant applications or sharing local applications...).\n7) Better performance : memory footprint, cpu usage, launch time...\n8) And as always : better usability.\n\nKDE as a platform should try to follow freedesktop guidelines/implementations in order to follow the path toward a coherent Linux platform (so far there's no such Linux platform, there's Linux+Gnome platform, Linux+KDE, etc...).\n\nMy 2 cents... \nCongratulations for the kde accomplished so far, it's just amazing !\n\nthibs"
    author: "thibs"
  - subject: "Re: KDE 4"
    date: 2004-07-05
    body: "> 2) An interface builder integrated to Kdevelop \n\nCorrect me, but isn't his not already happening in KDevelop 3.1?"
    author: "Anonymous"
  - subject: "Re: KDE 4"
    date: 2004-07-07
    body: "This _is_ happening in 3.1 ;)"
    author: "adymo"
  - subject: "New KDE sounds!"
    date: 2004-07-04
    body: "My compliments on the new sounds for KDE3.3!!\n\ncheck them out: http://www.artemio.net/projects/kdevibes/vibes/kde3.3/"
    author: "ac"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-04
    body: "Also read on new KDE 4 \"searching stuff\": http://www.csis.gvsu.edu/~abreschm/designs/ubiquitous_searching/index.html\n\nand KDE 4 file selector stuff:\nhttp://www.csis.gvsu.edu/~abreschm/designs/file_selector/index.html"
    author: "ac"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-04
    body: "Nice proposals indeed..."
    author: "Waldo Bastian"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-04
    body: "I like the file selector proposal too, where can I vote for it?"
    author: "Alex"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-04
    body: "Only place where I disagree is having the bookmarks in the left area instead of a global bookmark menu."
    author: "Alex"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-05
    body: "Very cool! I really like these ideas..."
    author: "Anonymous"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-11
    body: "Seriously, I prefer bigger icons.\n\nBut the concept is good.\n\nHome, Desktop, Documents, \"Mounted drive\", Recents, Bookmarks\n\nIt should be configurable, but a good way would be to have \na big bookmark icons that \"expands\" on the right side like this:\n\nRecents and Bookmarks should be \"directories\" that can expand horizontally\n\n+________+\n|_________|>+___________________+\n|_________|_|_My_Projects_________>\n|_________|_|_favorite_link_123456___|\n|_________|_|_favorite_link_abcdef____|\n Bookmarks \n+________+\n|_________| \n|_________|\n|_________|\n\n\nThere should \"really\" be a Network / IO Slave entry,\nso that novice users and even expert don't have to type cryptic URL\nfor CVS, FTP, SFTP, FISH, etc.\n\nUser could therefore \"discover\" IO Slave \"naturally\"\nand see \"wow I can do all those things\" instead of\nsaying KDE cannot do this or that, because\nthey don't know that a IO slave exist.\n\n+________+\n|_________|>+________________________+\n|_________|_|_FTP/SFTP______________>_+__________________+\n|_________|_|_Fish_(SSH)______________|_|_[*]_Add_an_FTP_site__|\n|_________|_|_CVS_/_CVS_over_SSH2___|_|_[D]_My_FTP_Project__>\n_I/O_Slave__|_Search_for_new_I/O_slave__|_|_[~]_My_FTP_y_y_y_y__|\n__________+________________________|_|_____________________|\n\nand then by clicking on such link,\nthey could prompt a dialog and specify\nhost name, protocol, port number, encrypted or not, login/passwd,\npassword prompt, directory location, file name, etc.\n\ninstead of wondering how to do it or writting things like:\n\nftp://login:pw@x_x_x_x:21/dir/file_txt\n\nThere's the current CLI way and there should also be \nthe user friendly way_\n\nCLI is faster if you know it,\nGUI is faster if you don't.\n\nAlso, users could see once they \"created their FTP shortcut\",\nhow the CLI way is and learn it gradually.\n\ni.e. If a user goes through the wizard/dialog after 10 times,\nhe will see that by chosing this and that options, he ends up with\nftp://login:pw@x.x.x.x:21/dir/file.txt\n\nand then automatically learn how to do it without reading any manuals,\nHow to or asking for help by simply discovering how it works\n\nThis can be useful even for gurus who wish to learn\nnew CLI commands for new IO slaves without bothering \nwith reading manuals_ Just give them a dialog and the \"end results\"\nLike I always say: \"Give me a damn example and I'll handle the rest =)\"\n\nJust my 2 cents thinking\n\nFred"
    author: "fprog"
  - subject: "Re: New KDE sounds!"
    date: 2004-07-04
    body: "FULL ACK!\n\nApart of the \"KDE_Drum_Break.ogg\" which IMHO sounds poor, the sounds are\ngenerally very good, and they will enhance the KDE-experience and make it\nfeel very modern.\n\nGood Work!"
    author: "ac"
---
In <a href="http://cvs-digest.org/index.php?issue=jul22004">this week's KDE CVS-Digest</a>:
<a href="http://www.koffice.org/kword/">KWord</a> now can mailmerge from <a href="http://www.koffice.org/kspread/">KSpread</a> as data source.
Less flicker in <a href="http://www.konqueror.org/">Konqueror</a> and Kicker.
And many bugfixes in <a href=" http://www.ipso-facto.demon.co.uk/ksnapshot/">KSnapshot </a>, <a href="http://www.konqueror.org/">Konqueror</a>, <a href=" http://www.konqueror.org/features/browser.php">khtml </a> and <a href="http://kmail.kde.org/">KMail</a>.



<!--break-->
