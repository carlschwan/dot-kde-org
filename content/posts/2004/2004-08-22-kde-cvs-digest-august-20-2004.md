---
title: "KDE-CVS-Digest for August 20, 2004"
date:    2004-08-22
authors:
  - "dkite"
slug:    kde-cvs-digest-august-20-2004
comments:
  - subject: "Woohoo!"
    date: 2004-08-21
    body: "First post! ;)\n\nI would just like to say that this is some amazing bit of work, absolutely wonderful. No week goes by without me spending a bit of time reading through this, even if I do reside in #kde-commits.\n\nOne of the things that hit me this time was that, after a long period of decline in the percentage of the Danish translation. I am very happy to find that Erik has managed to get it back up again: Skide godt, Egon! Eller noget ;)\n\nAlso, the news of the official release of the first amaroK 1.1 beta is very, very good news, since this is what I personally hope will end up being chosen as the music player of choice by everybody! It's really good stuff, people :)\n\nAnd, the best news in this digest is quite possibly what you highlighted above: KDM's new sesstion switching capabilities. This has been sorely missing for quite some time, and I welcome it. Now all we need is for kdmthemes to be completed and merged with HEAD ;)\n\nAgain, thanks for this, it's much appreciated :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Kolourpaint"
    date: 2004-08-21
    body: "One of the highlights for me in this release is Kolourpaint. It is a wonderful paint app with huge potential. I am using it for some game graphics now, and the hotkey support is great. A tool to work with.\n\nKrita might once become a Gimp beater - but Kolourpaint is really a great fill in the gap for people who want to draw bitmap sprites or just work with pixels. Hopefully the new cel animation functionality will be implemented before the next version of kde. Then it will finally bring something new to Linux users - animation outside of the keyframe paradigm that Macromadia forced upon us :D And, no more fiddeling with layers in the gimp to produce animation! :) Yay for that. And all in KDE!"
    author: "Hogne Titlestad"
  - subject: "Re: Kolourpaint"
    date: 2004-08-22
    body: "Indeed, Kolourpaint is most impressive. Krita is making steady progress, but I've just got the Gimp up and running with the Qt-GTK engine. What can I say, but wow."
    author: "David"
  - subject: "Re: Kolourpaint"
    date: 2004-08-22
    body: "I'm discovering Kolourpaint right now and this might be perfect for me. I used the gimp for most of the graphic editing that I did, but I almost never used most functions. It would seem that all I need it in Kolourpaint, or almost everything and that with a little developement it will be perfect for me.\n\nThanks to the devs!"
    author: "Mikhail Capone"
  - subject: "Groupwise"
    date: 2004-08-22
    body: "Well, I suppose support had to happen sooner or later. Now all we need to find is someone who actually uses Groupwise."
    author: "David"
  - subject: "Re: Groupwise"
    date: 2004-08-22
    body: "Well, I'm one of them, and I'm not the only one :)\n\nOur consortium uses GroupWise e-mail and calendar. So far I've used email with IMAP (KMail), and kept my calendar personal only (Nokia Communicator).\n\nI just installed Linux version of GroupWise, but it has some shortcomings as far as I'm concerned:\n\n- it can't sync with any pda device (I could use iPaq or Nokia Communicator 9210i)\n- it can't sync with eGroupWare, which I also use\n- it's writtin in Java, so it's slow and looks ugly\n\nSome of these features are already in kdepim/kontact, some are coming in the future.\n\nSo far I've not managed to sync KOrganizer calendar with a pda device, but hopefully this can be done in future. If anyone has a tip on this one, please give a hint (I can sync addressbook with multisync and synce, but that's not enough)!\n\nThe work for PIM / Groupware applications is very important, and it's progressing amazingly fast at the moment. I'd like to thank all developers, who work on these tasks. Corporate desktop needs these applications and it's great, that in many areas KDE:s PIM applications are at the same level as commercial counterparts, on some parts they can even beat commercial solutions!\n\nI wouldn't put high hopes on commecial software vendors including support for competitive groupware servers. This openness is one of the best advantages of FLOSS, and I higly appreciate it.\n\nGo For It, KDE!\n\nEleknader"
    author: "Tapio Kautto"
  - subject: "Re: Groupwise"
    date: 2004-08-23
    body: "> - it's writtin in Java, so it's slow and looks ugly\n\nI doubt that this conclusion can be held up."
    author: "CE"
  - subject: "Re: Groupwise"
    date: 2004-08-24
    body: "> > - it's writtin in Java, so it's slow and looks ugly\n> \n> I doubt that this conclusion can be held up.\n\nI've yet to see a full Java app that looks good and works fast.\nSo far I've tried StarOffice/OpenOffice.org, ArgoUML and Groupwise.\n\nI've nothing against Java, it's an easy language to use, cross-platform\ncompatible and everything. It just has it's limitations, as all languages\nand development environments have."
    author: "Tapio Kautto"
  - subject: "Re: Groupwise"
    date: 2004-08-24
    body: "And I use it too ... well, actually the company where I am contracting currently does ... not too bad a software, I must say. I have used Outlook and Notes as well other than Netscape Mail :-)"
    author: "Kanwar"
  - subject: "slow kdm startup..."
    date: 2004-08-22
    body: "On my box (AMD XP2200) it takes about 10 seconds from moment the X-server\nstarts until I get the login window of kdm.\nI use debian unstable, and kdm is the last service to be started.\n\nDoes anyone else see that slowness also?\nIf yes I'm gonna make a bugreport"
    author: "Yves"
  - subject: "Re: slow kdm startup..."
    date: 2004-08-23
    body: "Try fc-cache as super-user, if it does not change anything, look at the logs in your /var/log/kdm.log"
    author: "Pierre Souchay"
  - subject: "KDM bug?"
    date: 2004-08-23
    body: "One annoying bug (feature?) that's crept into KDE 3.3 is that KDM's default vt is 13, as cited in the KDM config. Annoyingly you can't switch to this vt using the F-keys, so you either have to use alt {left,right} or hack the file to get the original behaviour back.\n\nKDM should probably attempt to claim the vt starting from tty0, and if it fails, move onto the next one. Having a hardcoded constant is even more stupid than the originally acceptable default of tty7.\n\nComments?"
    author: "KDE Fan"
---
This <a href="http://cvs-digest.org?issue=aug202004">week's KDE CVS-Digest</a>:
KDM implements session switching and improves shutdown.
KDEPIM adds configuration wizard for Novell Groupwise client.
And many bugfixes in KST and <a href="http://kdepim.kde.org/components/korganizer.php">KOrganizer</a>.

<!--break-->
