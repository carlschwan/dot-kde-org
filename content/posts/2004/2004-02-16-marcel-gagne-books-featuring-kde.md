---
title: "Marcel Gagne: Books Featuring KDE"
date:    2004-02-16
authors:
  - "fmous"
slug:    marcel-gagne-books-featuring-kde
comments:
  - subject: "Great :-D"
    date: 2004-02-16
    body: "Thanks a lot Marcel, that was nice of you. ;-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Awesome Marcelo"
    date: 2004-02-16
    body: "This is exactly what is needed. KDE is totally usable for office environments and IT administrators etc. alike, with some work still to do over the next eighteen months I'd say. Importantly it also has an awesome over-arching development infrastructure that is here today. If there is any Linux-based desktop that has a chance, it is KDE."
    author: "David"
  - subject: "Re: Awesome Marcelo"
    date: 2004-02-18
    body: "Marcelo? Where did that o come from?"
    author: "David"
  - subject: "Very Cool"
    date: 2004-02-16
    body: "I'm heading over to Amazon right now to order a copy."
    author: "Kraig"
  - subject: "Re: Very Cool"
    date: 2004-02-16
    body: "Please don't. There still is a boycott against Amazon and its dirty ePatent policy. There are better bookshops on the net.\n\nI as a German prefer www.libri.de, the German book system is very good. I can order today via libri.de and take it away in my local bookstore the next day."
    author: "Holler"
  - subject: "Re: Very Cool"
    date: 2004-02-16
    body: "http://www.gnu.org/philosophy/amazon.html\n"
    author: "hiasl"
  - subject: "Re: Very Cool"
    date: 2004-02-16
    body: "well, if you want it politically correct, you could try http://www.bookzilla.de\n\nthey don't have as many books available (this one, for example): \nbut they support the fsfeurope for every book you buy (5%).\n\nbest regards\nThilo"
    author: "bangert"
  - subject: "Re: Very Cool"
    date: 2004-02-16
    body: "To late already ordered. It's GNU Richard Stallmans organization? Not sure i like silly patents but then again i'm not sure i like some of the sillyness of there either."
    author: "Kraig"
  - subject: "Re: Very Cool"
    date: 2004-02-16
    body: "\n<rant on>\n\nwhich part do you especially dislike?\n\nIs it the gcc that made possible compiling what is called now the linux kernel and KDE?\n\nIs it GNU ls, because it shows your files? Or GNU tar which is the package format KDE source is distributed in?\n\nIs it, GNU Emacs in which (likely) most of KDE was written (this from a converted vi user)?\n\nIs it GNU bash, without which your system likely wouldn't even boot?\n\nWhat's wrong with Stallman starting GNU when virtually only people like him which were determined to produce a Free Operating System, were doing all these unsexy things and asked people to do it, although it was no fun.\n\nThere is nothing wrong with Stallman. There is a lot wrong with people who don't appreciate.\n\nIn the beginning, Free Software was dead without people that made it for the Freedom. Sure, NOW there is KDE which is fun to make. Everybody wants to be part of it, takes pride of it. But even there, the good ones know on which foundations they stand.\n\nBut do you really think there would be a Free system without Stallman? I doubt it very. And today, who is fighting patents? Stallman is. Are you? Do you even care that it may no longer be possible to even run Linux on bought hardware? \n\nYou do, ... maybe. But maybe it's only your selfishness.... maybe you don't want to loose your KDE toy. When Free Software will be there, when there is enough selfishness in society to keep on using it, who do you think got it to that point?\n\nI hope that KDE and similar software convince enough people that they just can't go back without loosing too much.\n\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: Very Cool"
    date: 2004-02-16
    body: "Ok i'll bite. much of what you said is true but that does not change the fact  that Stallman over the years has  turned a lot of people away from Linux because of his sillyness. I'll just call it gnu/sillyness. If you get my point."
    author: "Kraig"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "Hello,\n\nwhat specifically do you find silly? And how would you as person be less \"silly\" if you were to try the same thing?\n\nBy not insisting on getting your point across, you can get a whole lot more acceptance and will be regarded a reasonable person. When you make enough compromise so that you suggest nobody to change really anything at all, all fine.\n\nBut what would you change? I dare say: Nothing!\n\nThere are a lot individual decisions which I would criticize. One being e.g. that Stallman still insisted on Gnome after Qt was GPL. He doesn't trust the people that created KDE on a non-free base. I think he should embrace KDE.\n\nE.g. the KDE-Debian vs. UserLinux debacle has clearly shown that in the desparate attempt to push Gnome, he obviously has allies that don't care for Freedom at all. UserLinux is _intended_ for non-free software! And it goes with Gnome, because it will better allow that.\n\nI think for similar reasons, he distrusts Linus and Linux the kernel. For a long time, Linux was very friendly to closed software and modules. A lot of firmware was in it, binary only.\n\nBut recently, Linus has gotten a lot more confrontial with this. Some of the changes even appear to be intended to break binary modules that invade the kernel internals (and make it crash).\n\nThere are some more examples, all along these lines. So what? He doesn't trust people. And why should he? Everybody and the world has NOT been wanting to change it, only after it became to have practical advantanges, people started to care about it not being taken away from them.\n\nWhy should he trust them to not give it away piece by piece. I mean, this is still a MS desktop world, and people can be heard talking how fine it is, not?\n\nYours, Kay\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "gnu/oh gnu/i'm gnu/sorry gnu/can't gnu/think gnu/of gnu/anything gnu/i gnu/would gnu/change."
    author: "Kraig"
  - subject: "Re: Very Cool"
    date: 2004-02-18
    body: "I saw RMS speak in Conneticut last night and he explained why he is such a PITA re: GNU/Linux is that he doesn't want the discussion of ethics and freedoms to disappear.  If Linus talked about the importance of software freedoms, then RMS said he would drop that point in a heartbeat.\n\nAs usual, he has a valid point; for example, yesterday I read a new Open Source software guide for non profits, and not once did they mention the ethical issues associated with free software."
    author: "Mark Bucciarelli"
  - subject: "Re: Very Cool"
    date: 2004-02-18
    body: "Linux does talk about freedoms - the freedom to put software under any license you want and not have people bitch about it."
    author: "David"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "Personally as a KDE contributor the GNU project and Richard Stallman are ok with me. In my opinion they both have contributed a lot to making KDE the success it is today, I'm thankful for that.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "I thought that Stallman was very positive to the GPLing of Qt - after all, he wants to see all software using the GPL, not just \"top of the software stack\" stuff like end-user applications.\n\nAnd as for the remark that Stallman turns people away from Linux [1], I think they've been listening to \"De Icaza FM\" a bit too much. Many people either don't know who Stallman is (and thus don't appreciate the work he has done) or just learn their lines from Slashdot teen trolls.\n\n[1] Or is it GNU/Linux? ;-)"
    author: "The Badger"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "Okay, I appreciate gcc -- but really, that is the only gnu tool I use anymore...\n\nI use freebsd's ls, and freebsd's tar and never use emacs and don't have bash installed on this box.\n\nAlright, I'll give GNU credit for holding the copyright and namesake for a half decent compiler.\n\nOh, and there are other tools with GNU stamped on them that I can appreciate, like gimp.  However, silly gnu/flamewars are not helpful in promoting GNU, especially when the arguments are based on only half-way valid premises.\n\n*grins*\n\n--Troy Unrau\nBored to tears? http://tblog.ath.cx/troy can make it worse..."
    author: "Troy Unrau"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "do you use glibc?"
    author: "anon"
  - subject: "Re: Very Cool"
    date: 2004-02-17
    body: "On FreeBSD? nope."
    author: "Lauri Watts"
  - subject: "check bsd"
    date: 2004-02-17
    body: "GNU is Not UNIX - FreeBSD is!"
    author: "Anton Velev"
  - subject: "Re: check bsd"
    date: 2004-02-18
    body: "so?"
    author: "Sergio"
  - subject: "Sounds great"
    date: 2004-02-16
    body: "I can't wait for it. Haven't seen too many books about KDE on the corporate desktop."
    author: "Alex"
  - subject: "Re: Sounds great"
    date: 2004-02-18
    body: "Talking about books. There are quite a few books about Redhat but very few about SuSE. Novell/SuSE should invest in getting some books published (in english) - also so Redhat's has something to lean up against on the shelf in the bookstore."
    author: "Claus"
  - subject: "Re: Sounds great"
    date: 2004-02-18
    body: "SUSE has its own publishing house (http://www.susepress.de) but they seem do to only German books. But don't forget the comprehensive documentation of English products like SUSE Professional."
    author: "Anonymous"
---
Marcel Gagne, very famous for his "<A href="http://www.marcelgagne.com/ljcooking.html">Cooking with Linux</A>" 
columns in <A href="http://www.linuxjournal.com/">Linux Journal</A>, has informed us about his 
second book "<A href="http://www.marcelgagne.com/KBSODG/">Moving to Linux: Kiss the Blue Screen of Death Goodbye!</A>" It concentrates on the user desktop, specifically KDE 3.1. This book covers basic desktop functions, KMail, Konqueror, KDE printing, CD burning, multimedia, games, etc. You can watch a nice slide presentation about the book in Shockwave Flash or OpenOffice Impress on the site. Marcel has also stated that beside bookstore sales, there are a few desktop Linux training companies that are starting to use the book as their Linux desktop guide. The book comes with a remastered <A href="http://www.knopper.net/knoppix/index-en.html">Knoppix</a> disk featuring, you guessed it, KDE. But we have even more hot news...


<!--break-->
<p>We have a scoop about a new book Marcel is writing. He is finishing his third book which will feature <A href="http://www.kde.org/announcements/announce-3.2.php">KDE 3.2</A> as 
the corporate desktop of choice.  In fact, the book is a blueprint for moving offices from Windows to Linux desktops. <i>"I chose KDE as my primary desktop for many reasons.  If I had to pick one over the others, it 
would have to be the sheer beauty of usability.  If ever there was a desktop environment that could make the corporate world sit up and notice Linux, it is most certainly KDE,"</i> notes Marcel.</p>

