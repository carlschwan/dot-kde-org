---
title: "Report: KDE at LWE NY, 2004"
date:    2004-01-29
authors:
  - "igeiser"
slug:    report-kde-lwe-ny-2004
comments:
  - subject: "very nice report!"
    date: 2004-01-29
    body: "And its very good to hear all the great news about KDE getting a lot attention...\n\nthanks for all the work, I think this is very good for the KDE project - I hope this will improve commercial support for KDE..."
    author: "superstoned"
  - subject: "Best 5 Minute Hack Ever!"
    date: 2004-01-29
    body: "Make sure you all try the google klip hack...  We came up with this sitting at a diner near Times Square and talking about DCOP the friday after the show.  Some of the google folks dropped by the KDE booth and handed out t-shirts.  Give it a try folks.\n\nOh, and if 'gg' doesn't work for you then modify the entry to be: \n\nkonqueror \"http://www.google.com/search?q=`dcop klipper klipper getClipboardContents`\"\n\n... and make sure you have klipper running."
    author: "manyoso"
  - subject: "Re: Best 5 Minute Hack Ever!"
    date: 2004-01-29
    body: "It's amazing to see this work. \n\nIf you look at the source, it's basically 2 or three lines of code only with the rest looking like default entries.\n\nBut: How do I install this thing?\n\n"
    author: "Anonymous"
  - subject: "Re: Best 5 Minute Hack Ever!"
    date: 2004-01-29
    body: "Just download the linked file and then go into KControl --> Regional & Accessibility --> KHotkeys --> General Settings && click the 'import new actions' button and select the linked file you downloaded in the file selection dialogue.  You might have to change the line starting: 'konqueror gg:' with my line above.\n\nThis can also be installed on a system wide basis (surely this should be a default in cvs), but I'm not sure where it goes.  Have fun!"
    author: "manyoso"
  - subject: "Re: Best 5 Minute Hack Ever!"
    date: 2004-01-30
    body: "Ummm... \n\nThis is with 3.2 you're saying, I assume?  I'm still on 3.1.5 (I'm on a modem...) and don't see anything like you're saying.  There's:\n\nKControl --> Regional & Accessibility --> Keyboard Shortcuts\n\nBut there's nothing about importing anything (and I don't see anyway of typing it in). :-\\\n\n\n\nAside, I personally think that a better tweak than a hotkey would be able to right-click on a word and choose \"Define Word\" or something like that.  You wouldn't happen to know how to go about adding something like that, would you?\n\n"
    author: "Xanadu"
  - subject: "Re: Best 5 Minute Hack Ever!"
    date: 2004-01-30
    body: "Well, just create a kmenu-entry with the command \nkonqueror \"gg:`dcop klipper klipper getClipboardContents`\"\nand associate a shortcut to that menu-entry.\n\nRinse\n"
    author: "rinse"
  - subject: "I almost forgot!"
    date: 2004-01-29
    body: "Mad props to the etherboot guys again.  As usual they helped KDE by having strange hardware that we always seem to forget to bring ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "What's the use?"
    date: 2004-01-29
    body: "OK folks, this really is NO trolling, but I've been wondering for some\ntime now why there is a need to search for sth. on the clipboard\nor i.e. the current selection like Mozilla.\nAdmitted, there might be a slight chance I'm searching for sth.\nlike that BUT I'm using the web quite frequently and never ever\nmissed such functionality. Everything I'm looking for usually\ncomes from my mind and not from some existing text.\nCould someone please enlighten me on the use of such a functionality \nso that it's useful at least once a day?\nIf used less frequently the saved amount of time and work would be \nmicroscopic.\nAnyway, as there seems to be a demand for it, it's nice we have it in KDE.\n\n\n"
    author: "Mike"
  - subject: "Re: What's the use?"
    date: 2004-01-29
    body: "You are reading something. It says \"He felt flabbergasted\". You want to know what that means? Do the same exact thing as the google clip, but using a dict: URL. Bingo, automatic dictionary lookups of the selected text.\n\nYou are reading something. It says \"The hypenguin distro is amazing\". It provides no link.\n\nYou want to know more. You google-clip it."
    author: "Roberto Alsina"
  - subject: "Re: What's the use?"
    date: 2004-01-29
    body: "Great idea Roberto!\n\nI just added dictionary and thesaurus search to the google klip.\n\nYou can download the khotkeys file here:\n\nhttp://www.mit.edu/~manyoso/google_dictionary_thesaurus.khotkeys\n\nNow:\n\nAlt + G = a google search of clipboard contents\nAlt + D = a dictionary.com search of clipboard contents\nAlt + T = a thesaurus.com search of clipboard contents\n\nyou need use cases?\n\nYou open a document in KWord or OO.o Writer and find a word you don't know?... highlight it and press 'Alt + D'... searching for a synonym of a word you come across on a webpage?... highlight and press 'Alt + T'... searching for an unknown error message you get on the console or in KDevelop?... highlight and press 'Alt + G'\n\nTransparent searches across all apps?  How?  KDE *that's* how!"
    author: "manyoso"
  - subject: "Re: What's the use?"
    date: 2004-01-29
    body: "Actually the proper command is \"kfmclient openURL 'gg: ... and so on'\", not konqueror directly, because kfmclient can take advantage of konqueror's preloading or reusing.\n\nOh well ... if people start creating all kinds of useful or not so useful khotkeys actions, I guess we need something like kde-look section for them or something.\n"
    author: "Lubos Lunak"
  - subject: "Re: What's the use?"
    date: 2004-01-29
    body: "actually we tried this a few times with no joy... unless we are missing something.  that is why we ended up using konqueror directly... otherwise this would have been a 30 second hack ;)\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: What's the use?"
    date: 2004-01-30
    body: "Maybe with exec instead of openURL?\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: What's the use?"
    date: 2004-01-30
    body: "*sigh*\nNo, could have tested that before posting.\n\nSorry,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: What's the use?"
    date: 2004-01-29
    body: "Wow! You guys are good!\nI am thoroughly impressed.\n\nKeep up the great work!"
    author: "dapawn"
  - subject: "Re: What's the use?"
    date: 2004-01-29
    body: "Ah! Thanks a lot for the great ideas.\nEspecially searching for an error message. \nThat's a really cool idea.\nNow this has convinced me to try it out!\n"
    author: "Mike"
  - subject: "Re: What's the use?"
    date: 2004-01-31
    body: "Please tell how to use this under KDE 3.1?\n\nThanks\nSarang"
    author: "Sarang"
  - subject: "Re: What's the use?"
    date: 2004-01-30
    body: "Or if a programm exits with an error message, and you want to find out what that error is using google..\n"
    author: "rinse"
  - subject: "Spectators are impressed"
    date: 2004-01-29
    body: "<quote>\nThe KDE people really impressed me. At one point one of them wanted to show me \nhow you can write simple javascripts to create full KDE apps or dock applets. \nHe didn't have it installed though, so he decided to download it from the \nnet; there was a compatibility problem with the binary, so he pulled the code \nfrom CVS; he didn't want to wait for a long compile, so he decided to use the \nother processors on the LAN, but to do that he needed icecream; he pulled \nthat from CVS... All this was done at a fast and furious pace, he had 10 or \n12 shells running at the same time, was bouncing between them; other \ndevelopers stuck their heads in: \"which shell is patching...?\" Development in \naction. It was cool\n</quote>\n\nhttp://www.ninenines.org/dot/dot.html"
    author: "Richard"
  - subject: "Re: Spectators are impressed"
    date: 2004-01-29
    body: "You mean I can get icecream from CVS? And here I was calling for it on the phone. I am so 80s!."
    author: "Roberto Alsina"
  - subject: "Re: Spectators are impressed"
    date: 2004-01-30
    body: "Hey sounds cool, but how does icecream  differ from  distcc?"
    author: "Henning"
  - subject: "Re: Spectators are impressed"
    date: 2004-01-30
    body: "icecream comes with a scheduler (and with a very cool monitor). The scheduler distributes the compile jobs dynamically while with distcc you only have a static list of computers.\n\nicecream in action:\nhttp://www.matha.rwth-aachen.de/~ingo/kde/icemon_in_action1.png\nhttp://www.matha.rwth-aachen.de/~ingo/kde/icemon_in_action2.png\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Telephony and other Stuff"
    date: 2004-01-29
    body: "Yer, I think telephony could be very big, and KDE has the tools to be able to really make this happen. There was an app on kdeapps.com a while back before it became defunct that implemented some telephony and video conferencing stuff. Can't remember what it was though. Kontact seems like a great basis to integrate this with.\n\nMicrosoft will be very curious about KDevelop (the damn thing is free - how the hell did they develop something that good for free? They must have better development tools than us), and they have always been curious about Qt because Microsoft like to attract developers better than anyone else. KDE and Qt have a real chance to attract developers by using an innovative development basis that Microsoft have no control or influence over, even indirectly. Notice that Microsoft is not really worried about Gnome or what Novell/Ximian/IBM are doing (although they might copy some of their ideas). They are looking at the small companies like Lindows (god they hate them), Xandros etc. which are companies of the size and stature that Microsoft once were. These distros all run KDE. What a shock.\n\n\"There was also the specter of Opera now providing a KPart so that Konqueror can use either KHTML or the Opera rendering engine (or the Mozilla engine as an XPart). This is exciting folks, as we will have two choices of browsers in KDE that are completely integrated!\"\n\nGoodness me, that sounds interesting. This is the first stage of developer attraction I'm talking about. Developers are going to love things like KParts. The influence of Safari on KHTML may have had something to do with this. I'm really impressed with KHTML so far considering that most of the Safari stuff doesn't seem to have been merged yet. Since Opera is Qt-based this sounds quite logical to enhance on an environment where Opera will be at home.\n\nIt is also nice to see that some of the Gnome people are good interested hackers as well. I think we miss that sometimes in the fog of the 'corporate' people seemingly pushing Gnome.\n\nA big well done to everyone. As an outsider looking in, KDE has really improved, focused and sold itself better than ever before. There is no need to be shy about what KDE has. It really is unbelievable good, particularly from, you guessed it, a developer perspective."
    author: "David"
  - subject: "Re: Telephony and other Stuff"
    date: 2004-01-29
    body: "> There was an app on kdeapps.com a while back before it became defunct that\n> implemented some telephony and video conferencing stuff.\n\nDo you mean this one?\nhttp://konference.sourceforge.net/\n"
    author: "Richard"
  - subject: "Re: Telephony and other Stuff"
    date: 2004-01-29
    body: "http://www.wirlab.net/kphone/"
    author: "OI"
  - subject: "Re: Telephony and other Stuff"
    date: 2004-01-30
    body: "I would love to see kphone intigrated with kopete and KDE's addressbook."
    author: "Kraig"
  - subject: "More photos"
    date: 2004-01-29
    body: "Some more photos I have put up here that I took while there:\n\nhttp://www.csh.rit.edu/~benjamin/about/phpquickgallery/dir.php?gallery=2004/\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: More photos"
    date: 2004-01-30
    body: "Cool, my first chance to see Zack post hair cut :)\n"
    author: "Don"
  - subject: "Very nice article"
    date: 2004-01-29
    body: "I thought it was a very interesting overview, great job! KDE definitely eneds more publicity.\n\nAlso, how do I use the opera part?"
    author: "Alex"
  - subject: "Very well done"
    date: 2004-01-30
    body: "It especially amazes me, how much new you can learn very day by reading about KDE.  The Google-Klip and the much worked on version in the discussion (eg. dictionary search accross all applications) is mind-blowing -- so simple, yet so powerful.  Very elegant.\n\nThe papers about KDevelop and Quanta could use some polishment though.  I'd really like to spread a couple of copies at my university and see who picks it up.\n\nThanks for all the good work so far and keep it up!"
    author: "he-sk"
  - subject: "Re: Very well done"
    date: 2004-01-30
    body: "> The papers about KDevelop and Quanta could use some polishment though. I'd really like to spread a couple of copies at my university and see who picks it up.\n\nI see by your email address that English doesn't appear to be your first language so I'm going to let \"polishment\" go in the context of a critique. ;-)\n\nIt's not so easy to answer a bunch of Questions from Ian to format all down to this one sheet. He did a pretty good job condensing it and I had time to submit one edit. Like I say, it's really hard to describe an application as diverse as Quanta in that space. It's not perfect, but I thought it was okay. If you have specific suggestions you could send them, but for now I don't think there are any plans to revise it. \n\nI thought it looked good and gave a good thumbnail idea of what Quanta is. I hope it is of use to people. Really you need a web site to tell the story well and we are in the early stages of a complete ground up redesign there."
    author: "Eric Laffoon"
  - subject: "Re: Very well done"
    date: 2004-01-30
    body: "I think the content is fine, I just noted some formatting issues with the paragraphs.  Eg in the KDevelop flyer, the line between the second and the third paragraph is superfluous.  Also, the formatting of the lists (eg the Toolkit support) could simply use some bullets to make it all clearer.  Other than that I found the flyers to be ready to be printed.  It's just that those kind of things tend to strongly stand out for me.\n\nBTW, kghostview (from Debian unstable) didn't even render the two files.  What's up with that?  I had to fallback to gpdf.\n\nCiao,\nViktor\n\nPS: Your right, English is my second language.  But I'd rather blame those mistakes on the advanced night and the increased alcohol intake.  We had a couple of thinks to celebrate, yesterday.  :)"
    author: "he-sk"
  - subject: "Re: Very well done"
    date: 2004-01-31
    body: "> BTW, kghostview (from Debian unstable) didn't even render the two files. What's up with that?\n\nThe file was developed in Scribus which writes (IIRC) PDF 4. I used the Adobe software and it rendered beautiflly. I don't know why KDE doesn't yet render it.\n\n> PS: Your right, English is my second language. But I'd rather blame those mistakes on the advanced night and the increased alcohol intake. \n\nWell even intoxicated your English makes my German look like I'm reaching toxic alcohol levels even when I'm sober and well rested. ;-)"
    author: "Eric Laffoon"
  - subject: "KDE: King of the Software Libre! "
    date: 2004-01-30
    body: "KDE is, in my opinion, the true leader of the Free Software movement (ok ok, right after Linux the Kernel).\n\nI'm not going to talk about the technical achievements, because we all know them; I'm thinking more about the community behind this project.\n\nA *true* open community. If you follow the various major KDE mailing lists (I do, religiously) you can see how every decision is planned, is agreed upon. Of course, sometimes it doesn't work the first time; some people disagree, but in the end, it seems everyone can always find his way...\n\nThe 2nd thing you notice (it shall be the 1st, really), is the motivation, the drive behind all the people. It's just amazing. Everyone is willing to just put his best, always, to create the ultimate desktop, for Free, awesome.\n\nReading this story's text, you could feel it. Developpers are proud of their creation, and it shows. Not only that, but it shows that they also really enjoy what they do (either professionally or as a hobby).\n\nI think the KDE developpers deserve a big \"kudos\" (whatever the hell that means), to me, they were a source of inspiration. I went to the library, bought some books on some stuff I felt I needed before starting to write code and then I started hacking. Hopefully, I will complete the project and publish it as part of KDE...\n\nWith the focus-shift on integration and a stronger application library (hey, everyone can help there!), I just can't wait for the next KDE!\n\n</fanboy> :P"
    author: "NamShub"
  - subject: "Re: KDE: King of the Software Libre! "
    date: 2004-01-30
    body: "How about a developer getting excited about reading a post by someone excited about what developers are doing. I wouldn't want to downplay how nice it is to know you're part of something special when it's praised. Certainly KDE is a great group of people and excellent software. As much as I love KDE I'd really like to think that these qualities are not so very unique in open source. However people aren't always reasonable and rational. KDE seems to attract people who carefully evaluate things. That's nice.\n\nSo reading your post reminds me of something I realized. I've long admired the people in KDE and the project for how well it works. Somewhere along the line I seem to have gotten swept into development because of my own passions. I even managed to get involved by having my project be part of the main release package. I got more involved with people in KDE and then something odd happened. KDE developers started saying nice things about me. I still saw myself as someone looking in. It dawned on me that I was actually one of the people making things happen in KDE.\n\nYour post is fun because you have been infected with enthusiasm for the project. You are becoming part of what you're excited about. Someday I hope we can get together at a developers conference and enjoy the fact that all these talented people let us hang out with them. ;-)\n\nBTW your rabid enthusiasm is always welcome on Quanta."
    author: "Eric Laffoon"
  - subject: "Re: KDE: King of the Software Libre! "
    date: 2004-01-30
    body: "FLOSS is like true love. You love your children, because they are yours. What's really special about Free/libre open source software is the personal relationship. You know the names of the developers and you respect them, you admire them and you are thankful for what they have contributed. So it's a kind of economy of stars. Eric, sure you are one of our heroes. I believe not only the code, but also the community effect plays an important role. Humans don't like to be lonely. technology is nice but it must be fun for us, it must be made personal. A stone may get a personal value for you if it was picked up at the beach, together with your girlfriend. As the human is in the center of the floss model it is quite natural to contribute as you help friends. Sending a detailled bug report: A contribution to Open Source. In the other world it would be regarded as someone who makes trouble as he is not pleased with the product. Perhpas the company will start professional training, you will talk with professional trained aid personal and it is very likely that you will not reach the responsible development department with your \"contribution\", a kind of information sink. \"organisierte Verantwortungslosigkeit\" we call it in Germany, organized lack of responsibility. You find this in all hierarchical structured organisation, especially in military. The information is there but not at the right place, and there are organisational incentives not to distribute infos. That's the second reason, the open discourse. We like OSS despite of its bug because we know that the developers do everything they can to fix them and we could do it. And we like them as they are the ones who improve our computer environment. "
    author: "elektroschock"
  - subject: "Re: KDE: King of the Software Libre! "
    date: 2004-01-31
    body: "Nice post. Very true and well put.\n\n> Eric, sure you are one of our heroes.\n\nI have two reactions to this...\n1) Cool!\n(30 seconds later)\n2) Whoa... this is still weird. ;-)\n\nAccidentally getting involved with a project that you become compulsive about doing because it's fun isn't supposed to be heroic... which just proves that it is really a community of very nice people awarding recognition that makes FLOSS extra special. I am actually inspired greatly by the stories and contributions of a lot of users. That is my fuel.\n\nWhat I really wish is that I could somehow give more people a chance to experience how great it is to be a part of this. I would not quit for any amount of money. Every time someone says \"I wish I could do something\" I wish I could get them to see that they can do what they want. What is worthwhile is never really easy, but what is easy is never really rewarding.\n\nAt the risk of sounding corney I'll paraphrase a statement about success. \"It is not about what you acquire on your journey, but about what you become in the process.\" That means change, which we inherently fear. Fear paralyzes. Embrace the process, race to the journey and revel in becoming... even when it hurts. ;-) Somebody else will not *always* \"do it\". To lead you don't have to do what others can't, just what others won't.\n\nOkay, enough soapbox. Everybody find some small thing you can do to feel good about. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: KDE: King of the Software Libre! "
    date: 2004-01-31
    body: "Hi Eric\n\nis there some announcement list in kde where i can keep track of new releases of software. do you have a roadmap on quanta/\n?\n\nregards\nRahul"
    author: "Rahul"
  - subject: "Universal Sidebar"
    date: 2004-01-30
    body: "Someone else mentioned that reading the dot is a great way of finding out about new stuff. Universal Sidebar? Anyone?"
    author: "Brad Hards"
  - subject: "Re: Universal Sidebar"
    date: 2004-01-30
    body: "imagine the konqi sidebar only on the kde desktop like a kicker...\n"
    author: "Ian Reinhart Geiser"
  - subject: "congrats"
    date: 2004-01-30
    body: "Congrats KDE on a succesful Expo and thanks for the work/effort down the years - hope more capital comes your way this year to continue the development of what is clearly the most powerful/fully featured Desktop Environment out there."
    author: "sap"
---
KDE has wrapped up another successful visit to <a href="http://www.linuxworldexpo.com/">Linux World Expo</a>, this
year at the Javits Convention Center in New York.  For three days, 10
KDE developers and a few others helped deal with a huge amount of
interest from the crowd, showing off the latest and greatest in KDE
3.2.  This year's show was thought to be at least twice as good as last
year's, and a great time was had by all.  The fun didn't stop after the
expo either, with several dinners providing plenty of time for spirited
arguments and the occasional hacking session.  A new application for KDE
was conceived (rss syndication), as well as a 5-minute hack ("<a href="http://www.sourcextreme.com/projects/googlesearch.khotkeys">Google
Klip</a>") which allows a Google search on the current Klipper selection to
be invoked with a single keyboard shortcut.
<!--break-->
<p>
First, we'd like to thank the many groups and individuals who made our
attendance at the show possible.  <a href="http://www.suse.com/">SUSE</a> generously provided 2 computers
with nice LCD screens, plus came to our rescue on the first day,
providing an extra table (at significant cost) when we realized that we
did not have enough table space for all of our awards and demos. SUSE
also provided a boxed copy of SUSE LINUX 9.0 for us to give away.
<p>
Thanks to <a href="http://www.oracle.com/">Oracle</a> and Linux World Conference and Expo for the free booth. 
Nathan Krause from <a href="http://www.nalekra.com">Nalekra</a> ran a giveaway of a nice <a href="http://www.transmeta.com/">Transmeta</a> Crusoe 1.0 GHz 
laptop.  <a href="http://www.codeweavers.com/">CodeWeavers</a> donated a copy of Crossover Office for us to give away.  
The developers did not go unrewarded, with a copy of <a href="http://www.xandros.com/">Xandros</a> Desktop 2.0 
Deluxe and a <a href="http://www.google.com/">Google</a> t-shirt each.  Klaus Knopper dropped by to give us 
copies of the latest <a href="http://www.knopper.net/knoppix/">KNOPPIX</a> 3.3.
<p>
Additional hardware for the booth was provided by <a href="http://www.sourcextreme.com">SourceXtreme</a> 
(mini cube + 21" LCD screen).  Additionally, due to the persistent efforts of
Ian Geiser and great information from Eric Lafoon (<a href="http://quanta.sf.net/">Quanta</a>) and Alexander
Dymo (<a href="http://www.kdevelop.org/">KDevelop</a>), we were able to have handouts for the two highly
popular KDE projects. These are posted online: <a href="http://www.sourcextreme.com/presentations/kdevelop.pdf">kdevelop.pdf</a> 
and <a href="http://www.sourcextreme.com/presentations/quanta.pdf">quanta.pdf</a>. They will be eventually
moved to kdepromo once there is a reliable way established to keep them
up to date. More handouts for other projects could only benefit us at future shows.  
Ian Geiser will be sending out requests for information from other projects as time permits.
<p>
Finally, a big thanks to those who ran the booth... KDE Developers:
George Staikos, Ian Reinhart Geiser, Mark Bucciarelli, Mathieu
Chouinard, Adam Treat, Hamish Rodda, Nadeem Hasan, Zack Rusin, Benjamin
Meyers, Matthias Ettrich; and our friends Jason Nocks of sourceXtreme;
Matthew Staikos of <a href="http://www.staikos.net">staikos.net</a>; and Nathan Krause of Nalekra.com. Pictures 
of the booth can be found at <a href="http://www.mit.edu/~manyoso/LWAlbum/index.html">Adam's webpage</a>.
<p>
KDE did not leave the show unrewarded, far from it!  We were up for both
the Best Open Source Project and the Best Development Tools awards. 
While the Best Open Source Project went to Real for their Helix player,
KDevelop took the prize in the Best Development Tools section up
against quite a few closed source projects.  This victory was made all 
the more sweet by the judges' comments, who said KDevelop was the most 
polished presentation out of 35 other products and that they were proud to give an OpenSource project an award on merit alone. Kudos to the team for being able to accomplish so 
much without the backing of a corporation like all of the other entrants!  
We also picked up the <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a> award for the Best Desktop 
Manager (3rd year running).
<p>
We weren't content to just passively show off KDE.  George Staikos gave
a presentation to the conference attendees on  <a href="http://www.staikos.net/information.php">"Designing Scriptable and
Extensible Desktop Applications"</a>, 
<a href="http://www.linuxworld.com/story/39249.htm">Mark Bucciarelli and George gave Syscon interviews</a>, and 
Mark had an interview with Brian Proffitt of Linux Today.
<p>
KDE 3.2 was generating a large amount of interest, with "what's new?"
being the most common question.  Hot favorites with the crowd were
<a href="http://www.kontact.org/">Kontact</a>, including <a href="http://kolab.kde.org/">Kolab</a> and MS Exchange support, KDevelop, Quanta, and
more.  Many people were enchanted by the <a href="http://edu.kde.org/kstars/index.php">KStars</a> demonstrations run on
the last day, hopefully boosting the <a href="http://edu.kde.org/">KDE Education Project</a>'s visibility.  Other things that people were interested
to hear about were KDE's <a href="http://www.kde.org/areas/sysadmin/">Kiosk mode</a>, <a href="http://printing.kde.org/">solid printing support</a>, wallet
functionality and the new Universal Sidebar.
<p>
A few interesting feature requests were made, including the ability to
drag several people from the address book (or even Kopete :) and
schedule a meeting with them (adding them as attendees
automatically); Jim Gettys suggested being able to set the location in
KStars to any location in the universe, not just anywhere on Earth; and
several people were enquiring about KDE development books.  Others were
looking for business cases for KDE (help out <a href="http://enterprise.kde.org/">enterprise.kde.org</a> if you
can!). Timothy Ney, President of the GNOME Foundation was impressed by 
the new gtkqt theme engine and the ability to edit files over an SSH 
connection, especially  when he saw that OpenOffice could do so using 
the new fuse_kio prototype.  He stopped by the booth to invite any 
interested KDE hackers to GNOME's upcoming Guadec Meeting in Norway.
<p>
Several interesting leads came out of the event.  Microsoft is curious
about KDevelop.  Digium is interested in improving telephony support 
in KDE's groupware.  WatchIT.com want to do an educational video on migrating 
to KDE. Merryl Lynch is currently doing a large scale test deployment of KDE
desktops.  No Starch Press want to do a book on development in KDE. 
Open Source Migrations want business cases for migrating to KDE
desktops, including groupware solutions. Harvard University's Arnold
Arboreum is interested in KDE on desktop.  There was also the specter of
Opera now providing a KPart so that Konqueror can use either KHTML or  the
Opera rendering engine (or the Mozilla engine as an XPart).  This is exciting folks, as we will have two choices of browsers
in KDE that are completely integrated!
<p>
That's about it for now.  KDE is certainly doing very well in North
America!
