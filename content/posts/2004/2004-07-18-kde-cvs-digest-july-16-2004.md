---
title: "KDE-CVS-Digest for July 16, 2004"
date:    2004-07-18
authors:
  - "dkite"
slug:    kde-cvs-digest-july-16-2004
comments:
  - subject: "Thanks to 2 people!"
    date: 2004-07-17
    body: "Many thanks to Derek for his steady work, and many many thanks to Enrico Ros\nfor his wonderful and very important work in improving the visual\nappearance of konqueror and kde [1].\n\nKeep up the good work!\n\n\n[1] http://robotics.dei.unipd.it/~koral/KDE/kflicker.html"
    author: "yo"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-18
    body: "Yep, go Enrico! (italians do it better ;-)\n\nPS. Where did you take this wallpaper from?\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/mydesk2.png"
    author: "fake guy"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-19
    body: "I second this..(italians do always it better....)\n\nGo Enrico !!!\n\nAnyway, who's the girl ?\n\nThanks and best regards\n\n\n\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/mydesk2.png"
    author: "Brinkley"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-19
    body: "> I second this..(italians do always it better....)\n\nI third^H^H^H^H^Hdoubt this.. (especially when writing english: idaib -> idiab, not to mention \"italians do it on topic\")"
    author: "Flinkbaum"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-19
    body: "Wow!  I've been away from KDE really for a couple of years now since I got my first Mac.  But I like to pop back every now and then and have a look at how you guys are getting on.  Must say that that is to date the most beautiful KDE screen shot I've ever seen.  So I took a look at some of the other graphics examples in your directory too.  I'm really impressed.  Very crisp, very clean, and looking very mature.  If this is what's coming in KDE 3.3, then I might just have to dust off my PC and have another play.\n\nWell done guys."
    author: "John"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-19
    body: "The widget style (Plastik) will ship with KDE 3.3, but not as default. All are other stuff (icon theme, window deco, wallpaper) seem to be user customizations."
    author: "Anonymous"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-18
    body: "Yes, that's the greatest advantage of open source, it's not just about shipping a product that somehow works, but also about improving and optimizing it afterwards.\n\nThat's why KDE gets better every release while many people aren't sure about other products... (DRM, product activation, etc.) "
    author: "Roland"
  - subject: "Re: Thanks to 2 people!"
    date: 2004-07-19
    body: "Hey, My many thanks to those kool developers with a such precision observation :) Thank you!!!"
    author: "Fast_Rizwaan"
  - subject: "konstruct"
    date: 2004-07-17
    body: "\"Michael Pyne has created a script to assist users to install KDE from cvs. The script and documentation can be found at http://grammarian.homelinux.net/kdecvs-build/.\"\n\nIsn't this the same thign as konstruct?"
    author: "me"
  - subject: "Re: konstruct"
    date: 2004-07-18
    body: "Nope, Konstruct doesn't do a CVS checkout AFAIK. It's for releases only (including those marked as \"unstable\", such as alphas, betas and RCs)."
    author: "Eike Hein"
  - subject: "Re: konstruct"
    date: 2004-07-18
    body: "Nope. This is for those who like being on the bleeding edge. Run code written yesterday.\n\nYou are on your own, and at your own risk. It may not build, and you may end up with a build that is broken. That being said, kde cvs is remarkably stable most of the time.\n\nKonstruct is for releases.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: konstruct"
    date: 2004-07-18
    body: "do we get a build system for KDE ? one that like mozilla and so ? tinderbox is it.\nwe need a compile farm so see whats broken in cvs."
    author: "compilefarm"
  - subject: "The problem posed by Derek is real"
    date: 2004-07-18
    body: "There are often times where people make changes to applications which don't really have clear (or active maintainers), or to parts that affect different parts of KDE, like Frans did. Yeah, consensus seems to have worked in the past, but I don't think it any longer works as well in issues that can be quite devisive.. usually with one or two people *really* advocating one side or another. So, what happens is that long discussions happen with no consensus occuring, and the same discussion happens every month or two with the issue never getting resolved. \n\nThe other thing that can occur is that someone posts a RFC about an issue. Only a few people seem to care. A few months later, people suddenly get riled up when things get committed. "
    author: "smt"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "what was the problem that kulow and frans had ? as i understand kulow did not know of any discussion and just reverted his patch. Does he think he could be aware of *all* discussions ? i think no one can.\nand just reverting someones patches can kill all motivation and make bad climate in the project.\n\nsounds like some *linus* type person for kde."
    author: "compilefarm"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "Kulow's issue was due to the feature freeze. We are well into a release cycle, and the release dude has to keep a lid on things otherwise nothing will stabilize.\n\nThis is good.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "normaly usability fixes dont break anything - so you could relax here a litte bit.\n\na he should heave pointed out that he revertet because of the feature freeze - but from his mail he reverted because he did not know of any discussion. And thats the point.\n\ni think we dont need someone blindly reverting patches like *he* like kde to be.\n\n\ni think he thought:\noh i dont like this fix - revert :-( this suxx"
    author: "ch"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "Sorry, but Stephan Kulow did not revert just because he did not like it.\n\nHe is the release manager and therefore he must careful of the state of KDE. That some KDE applications suddenly would not appear in GNOME is not good, not at all. (Especially that the .desktop files were unified to allow that they show in all desktops following the specification.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "Rubbish. Stephan is the release co-ordinator, it is  his duty to ensure that any unplanned changes however large or small get reverted during the crucial stages of the release cycle.\n\nUntil you become a developer you have no right to complain."
    author: "Anonymous Coward"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "You are being extremely unfair to Stephan Kulow. Frans commited the patch without discussing it, and, just after the patch was commited, lots and lots of people asked for it to be reverted, as no discussion happened, and many people disagreed with the changes. Coolo just pointed out that the patch violated the release plan. Then, Frans himself reverted the patch, not coolo. Read the mailinglist archives if you don't believe me."
    author: "Henrique Pinto"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "Riled up is maybe the wrong word. There were valid concerns about the changes, and some different implementation ideas.\n\nUsability improvements happen on two levels. First is to have consistency in the UI, capitalization, button placements, common dialogs all the same, etc. That is happening already. Stephan Binner, who also maintains konstruct, does style guide fixes. There are still improvements to be made on this level.\n\nThe second level is changes in the way things work in an attempt to make it easier to use, or possibly easier to learn. Already there are two sometimes conflicting goals, ie. file management by console commands. An extreme example, but easy to use, hard to learn. Which do we want in KDE? In each situation, how can both be accomplished? Do we cater to our existing user base, which is predominantly experienced users, or do we alienate them in an effort to pursue new users? Or do we try to find the difficult sweet spot in the middle? The project needs developers, who more likely will come from the experienced users. Or maybe from some packager who sells to new users. Hmm.\n\nTo confuse the issue even more, we should ask whether there is a problem that needs fixing. I don't think there is even consensus on this basic question.\n\nOne advantage KDE has is the experience of others. We can avoid the mistakes that others have made.\n\nWhere do I stand in all this? I still haven't forgiven the industry for moving to the CUA. My fingers find it unnatural and frustrating. What really bothers me is to learn how to do something, then find I have to relearn because someone made it 'easier'. I like the efforts that have gone into optimizations. I like how more and more things just work in KDE. I don't like change for the sake of change. And being an unusually cranky sort, I keep my opinions to myself. And live with whatever others decide. So far so good.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "I think Kulow is not as good in making Usability Decisions as Frans is. Frans is dedicated to bring usability enhancements to kde. Kulow ist dedicated for code (library development). so i dont see the point that he is judging the usability fixes.\n\ni hear like \"i like my system, please dont change it\" attitude. \n\nI like that the Gnome people have the courage to make massive desicions to change their Enviroment, this makes innovation !! (the thing on the spatial mode will calm down).\n\nMy Proposal is to Name one Head of Usability who will design the enhancements and assign the coding to some core developers with discussion of the list.\n\nI think the developers should subordinate themselves to the usability gurus not the other way around , what so you think ?????"
    author: "ch"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "Stephan Kulow is the release manager of KDE. So it is him who has the final decision in case of conflicts (and this was a conflict).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: "> I think Kulow is not as good in making Usability Decisions as Frans is\n\nWhy? Stephan is a highly respected and one of the original developers. How you can you claim that?\n\n> Frans is dedicated to bring usability enhancements to kde.\n\nHow do you know that?\n\n> Kulow ist dedicated for code (library development). so i dont see the point that\n> he is judging the usability fixes.\n\nRubbish.\n\n> My Proposal is to Name one Head of Usability who will design the \n> enhancements and assign the coding to some core developers with\n> discussion of the list.\n\nNonsense. That is the complete anti-thesis to how KDE is developed. People do coding because they want to, not because they get assigned it by some dictator. Since they are unpaid and do it because the like doing it, I hardly think it's reasonable to expect them to do stuff they're ordered to.\n\n> I think the developers should subordinate themselves to the usability \n> gurus not the other way around , what so you think ?????\n\nI think you should get more of an idea about how KDE  is developed."
    author: "Anonymous Coward"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-19
    body: "i think you should read the mailing lists more - Just answering rubbish to every statement is not very good.\n\nthank you mr.rubbish."
    author: "ch"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-19
    body: "> My Proposal is to Name one Head of Usability who will design the enhancements\n> and assign the coding to some core developers with discussion of the list.\n\njust like we name one Head of Software Design who designs the feature enhancements and assigns the coding to the various KDE developers? ;-)\n\nof course not, that's not how Open Source methodologies work.\n\nso the question is: is Usability a centrist's wet dream which only works when driven top-down? or is it possible to rework the process such that it can effectively co-exist with the Open Source coding model? i think the answer is \"yes\". before you nay-say, remember that many used to (and some continue to) say that Open Source coding practices can't, won't and don't work largely because they thought that all software development must look like the models they had theretofore known (closed models, primarily).\n\n> I think the developers should subordinate themselves to the \n> usability gurus not the other way around\n\nwhy should anybody be subjugated to anyone else in this scenario? why not achieve an organic, flexible and cooperative relationship between the various disciplines?\n\n(art, documentation, translation, usability, accessability, coding, Q/A, promotion, etc..)"
    author: "Aaron J. Seigo"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: ">  Riled up is maybe the wrong word. There were valid concerns about the changes, and some different implementation ideas.\n\nOf course.. I'm not exactly talking about this situation, which I'm blisfully ignorant of, but other situations in general that I've noticed. A lot of objections only seem to happen *after* things are commited rather than *before* when people ask for comments. That causes a lot of strife. People seem to read kde-cvs more than kde-core-devel :-)"
    author: "smt"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-18
    body: ">People seem to read kde-cvs more than kde-core-devel :-)\n\nYou are right about that. Most of the developers don't subscribe to kde-core-devel.\n\nAnd I agree that sometimes the discussions get rather emotional.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-19
    body: "i just looks like everyone has to ask kde-core-devel for permission if you want to change some usability stuff."
    author: "ch"
  - subject: "Re: The problem posed by Derek is real"
    date: 2004-07-19
    body: "No, but you should ask kde-core-devel when you do a major change to several apps across module boundaries in the middle of the feature freeze.\n\nImproving usability of a single app isn't a problem at all when the maintainer knows about it.\n\nIt seems to me as if you're talking about things you don't know anything about."
    author: "Christian Loose"
  - subject: "Just \"thanks\""
    date: 2004-07-18
    body: "I just want to say \"thanks\" to all the developers who work on KDE. I can't wait for 3.3, I'm sure it's going to be a great one."
    author: "Mikhail Capone"
  - subject: "Expand Internationalization Status"
    date: 2004-07-18
    body: "Hi Derek,\n\nFirst of all thanks a lot for the time and effort you put in the Digest every week. I appreciate it as much as KDE itself.\n\nI would like to make a request to expand the Internationalization Status. The status is now the top 10 of http://i18n.kde.org/stats/gui/HEAD/toplist.php This is the GUI translation. Would you be so kind to add a top 10 of http://i18n.kde.org/stats/doc/HEAD/toplist.php This is the documentation translation. Which IMHO is (almost) important as the GUI translation.\n\nThanks a lot !!!"
    author: "AC"
  - subject: "Statistics: votes summed by developer"
    date: 2004-07-18
    body: "At the start of the weekly CVS Digest there is a nice section with statistics.  Currently there is the top ten list for most number of commits, and the number of changed lines is also shown\n\nI think that another nice piece of statistics would be the total number of votes for the bugs that each developer has fixed.  This would be a way to see who has done the community the biggest service this week, as it were.  :-)\n\nWould this be too complicated to implement?"
    author: "ingwa"
  - subject: "Re: Statistics: votes summed by developer"
    date: 2004-07-19
    body: ">Would this be too complicated to implement?\n\nProbably. I'm not sure if such statistics are available readily. The majority of the stats come from the repository itself. What you are asking for would come from the bugzilla site.\n\nIf you want, send me the necessary query(s) from bugs.kde.org and I could use it.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Statistics: votes summed by developer"
    date: 2004-07-19
    body: "Don't know how it works 'inside' bugs.kde.org.\n\nhttp://bugs.kde.org/votes.cgi?action=show_bug&bug_id=48302\ntext on page \"Total votes: 397\"\n\nregexp like /Total votes: (\\d+)/i may help with it?.."
    author: "Anonymous"
  - subject: "Re: Statistics: votes summed by developer"
    date: 2004-07-19
    body: "The following should work 'inside' bugs.kde.org:\n\nSELECT\n   profiles.login_name,\n   SUM(votes.count) AS vote_sum\nFROM\n   bugs_activity ba\n   INNER JOIN profiles ON ba.who = profiles.userid\n   INNER JOIN bugs ON ba.bug_id = bugs.bug_id\n   INNER JOIN votes ON ba.bug_id = votes.bug_id\nWHERE\n   bugs.bug_severity != 'wishlist' AND\n   ba.added IN ('RESOLVED', 'CLOSED') AND\n   DATE_SUB(NOW(), INTERVAL 7 DAYS) < ba.bug_when\nGROUP BY profiles.login_name\nORDER BY vote_sum DESC\nLIMIT 10\n;"
    author: "anonymous"
  - subject: "separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-18
    body: "An email I sent to kde@mail.kde.org:\n\nAlthough I'm aware of how the view profiles\nwork; what I've been hoping for wasn't the already existing ability to \nconfigure separate views, but rather the actual functionality of separate \n'homes' for the web browser and file management views.\n\nFor instance:   Settings -> Configure Konqueror... -> Behavior -> 'Home URL'\n\nI currently have set to 'http://slashdot.org', and I've saved the 'Web \nBrowsing' view profile, so that when I fire up Konqueror web browser, it \ngoes straight to my \"home page\".\n\nThe problem that annoys me to a large degree is that when I'm using Konqueror\nFile Management, the 'home' button is a web page ( slashdot.org )... when I'm\nfile browsing, it's logical that I would want my 'home' to \nbe /home/<username> ...   not http://www.slashdot.org!\n\nWhat I think should be the case would be separate 'home urls' - one under:\n\nSettings -> Configure Konqueror ... -> Behavior -> 'Home URL'  \n\n...and another under: \n\nSettings -> Configure Konqueror ... -> Web Behavior -> 'Home URL'\n\n\nIn fact, if you read the descriptive heading under Configure Konqueror -> \nBehavior, it clearly states:  'You can configure how Konqueror acts as a file \nmanager here.'   ... and then there's the 'Home URL' setting, complete with a \ndefault of '~' and even a file dialog button to browse your filesystem with \nto set a new \"home\" with. Then there's the 'Web Behavior', which states\n'Configure the browser behavior' ... and it lacks the ability to set a home \nurl.\n\nNot only is this extremely counter intuitive, but from a practical \nfunctionality standpoint - it makes no sense, and is quite frustrating.\n\nI was wondering whether others were similarly frustrated with the current \nbehavior, and whether anyone knew whether this was going to be \"fixed\" or \nnot.\n\nThanks!"
    author: "corey"
  - subject: "Re: separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-18
    body: "The home url should be left as ~. If you want to (web) browser to start on slashdot u should open konq in webbrowsing profile, goto slashdot, then save view profile webbrowsing and tick the box that says save URLs in profile.\n\nNow u click web konq it goes to slashdot, Click file konq it goes to ~."
    author: "Matt"
  - subject: "Re: separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-18
    body: "Good idea, create a wish bug and it will have my voting points."
    author: "AC"
  - subject: "Re: separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-18
    body: "Please search before creating a new bug report.\n\nSuch a bug probably exists already, as the idea is far from being new.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-19
    body: "This has been partialally fixed in 3.3.0.\n\nYou can configure it so that: \"Load Profile\" is next to or replaces the Home button.  This will give you a drop down menu of profiles to load and the \"webrowsing\" profile can contain your web home URL while the \"filemanagement\" provile can contain your $HOME (or other) directory.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-18
    body: "The solution is to make /home/user your home URL and use the bookmark toolbar for your web bookmarks.  It's just a different way of thinking about Konqueror.  The idea of a home url on the web doesn't really make that much sense; I'm sure you have many URLs you visit regularly and arbitrarily choosing one to be \"home\" above the others doesn't make that much sense.  OTOH, there is a clearly defined home directory.  \n\nPersonally, I sometimes find it useful to turn a web browser into a file browser by clicking the home button, so it can be useful.  Like I said, it's just a different way of thinking about Konqueror.  Discussions about this behavior of Konqueror must be as old as Konqueror itself.  It hasn't been changed yet and it isn't likely to be changed."
    author: "Spy Hunter"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-19
    body: "I'm cool with thinking about software that works differently from what I'm used to in new/different ways - but certain things I believe tend to be better off left alone; especial \"standard\" or \"de facto\" behavior... such as the long standing (like, since forever) concept of a \"home page\"/\"home url\" when using a web browser.\n\nIt's not just me ( a long time unix/linux user and programmer ) who fails to see the logic in this behavior - but everyone I've ever introduced to KDE. The view profiles functionality is one thing, but the \"either/or\" behavior of Konqueror's \"home url\" is a real pain to introduce to people, not because they're unable to work in new and different ways from what they're used to - but because it truly is a counter-intuitive and somewhat awkward behavior. Especialy in KDE, which does a truly phenomenal job in allowing the USER to define his/her preferred environment and behavior through configuration options. I would expect the \"modify your habits\" attitude to be more becoming of Gnome or Windows.\n\nHaving the ability to configure the \"home url\" separately for the File Management and Web Browsing views in konqueror would allow people to work the way they want to, instead of the way Konqueror forces them to work.\n\nAt anyrate, I'll search the bugs db and create a feature wish if there isn't already one existing concerning this."
    author: "corey"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-19
    body: "This, and the fact that there are 2 million different toolbars for konqueror are the things that annoy me most. Am I the only one in thinking that there should be one (yes ONE) toolbar for the browser and one (yes ONE again) for the file manager?"
    author: "Gogs"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-19
    body: "No, I agree with you :)"
    author: "Rayiner Hashem"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-19
    body: "What about configuring the animated logo to take you to your home page once clicked? Right now, it opens another konqueror instance. Why would one need that in the first place?"
    author: "charles"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-19
    body: "Yes, you *are* the only one. :-)\n\nActually, for a configuration that some people wanted, we need an additional toolbar for Konqueror (it would be empty and not used by default).  To configure like IE without changing the Main & Extra toolbars.\n\nWhat I *do* think is that the default configuration has too many icons on it.  I remove some of them and configure Konqueror to be more like Mozilla.\n\nI would also like to see the Animated Logo on the Menubar -- like other browsers.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "Alright, but why another *extra* toolbar. What's wrong with just having one or two toolbars and using as many icons as is needed. To give you an example, I really don't like cluttered toolbars either. So, I take away all the icons I don't need and leave about six. Then, if I decided to change view from multicolumn (which still doesn't work right btw) to detailed list...lo and behold. Two more icons appear! (increase/decrease font sizes) So, I remove them, change to say tree view, and THEY'RE BACK!!! And so on, and so on. Please give me ONE good reason why this is needed. Seriously, I just can't figure it out......"
    author: "Gogs"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "As I said, an additional toolbar is necessary to have an IE like setup without modifying the existing Main and Extra toolbars:\n\nhttp://home.earthlink.net/~tyrerj/kde/kde00.png\n\nIf it is to be used only for this use, it should probably be called the Navigation ToolBar.\n\nIn regard to your other problem.  I have also experienced problems removing icons form toolbars.  Were they still there after you restarted Konqueror (you might even have to kill all running instances of it).  If you still have this problem in 3.3.0 Beta 2, please file a bug report if there isn't one already.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "That looks like ages old versions of IE, nothing like IE 6.0, whose toolbar can't really be replicated because Qt doesn't support selective text labels. The small toolbar to the left of your screen is ridiculous. The icons are small and hard to click. There are too many of them. "
    author: "AC"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "I am puzzled.  My IE-6.0 looks just like it:\n\nhttp://home.earthlink.net/~tyrerj/files/ie6-jrt.png\n\nAnd exactly what can't be replicated -- were is the selective text label?  The \"Go\" icon in IE says \"Go\" but that is an *icon* with text in it.\n\nI find the small toolbar quite useful.  I don't use these often and it is easier than using the menu.  But this is KDE, if you want it larger just right click and select a larger Icon Size.  Too many of them -- yes, I agree that the default configuration has too many icons in the tool bars.  Unfortunately, this appears to be a political issue with the developers.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "But why don't you want to modify the existing main and (one too many :o) ) extra toolbars. What's the difference between changing and saving one toolbar, and using a completely different one. Except that using a completely different one is silly of course! :o)"
    author: "Gogs"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "You don't want to modify the Main ToolBar because it has things hard coded into it.  You need to have the separate Extra ToolBar if you use my configuration with the Main & Extra ToolBars oriented Vertical.  If you have too many icons in them, they will stack (side by side) rather than scrolling off the bottom of the screen (which is what happens if you had all of the icons in one toolbar).  This is more important if the user chooses a larger icon size for them.  As I said, to actually replicate my doctored screen shot, you need an additional toolbar.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-22
    body: "Then surely that's a pretty serious design flaw? I understand what you mean, but to me a toolbar is a toolbar is a toolbar. At most I can see the need for ONE extra, if the main toolbar is so seriously hardwired...."
    author: "Gogs"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2005-01-22
    body: "I agree with you. Making them one single program makes it more complex than it should be, especially for the newbie. You can only do so much with the view profile. Currently it is problematic to put the location bar at the right of the main toolbar in Konqueror-web because it would double the location bar in the file manager."
    author: "Ninaw"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "> The idea of a home url on the web doesn't really make that much sense; I'm sure you have many URLs you visit regularly and arbitrarily choosing one to be \"home\" above the others doesn't make that much sense.\n\nI don't think this is true. For example, my \"web browsing home page\" is a local html page generated from my bookmarks, using a tool like bkmrkconv. To me, this make perfect sense as my \"web home\", while I obviously want ~ to be my home for file management purposes. I totally agree with Corey here."
    author: "AC"
  - subject: "Re: separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-19
    body: "This seems to relate closely (or the 'evolution' of it's feedback) with this 'bug' reported in the year ...2000: http://bugs.kde.org/show_bug.cgi?id=8333\n\nIt has been reported in the year 2000, only 4 years ago in august."
    author: "ac"
  - subject: "Re: separate homes for konqueror-web & konqueror-file?"
    date: 2004-07-20
    body: "It frustrates me that konq is different from every other browser in the world that I can't set a 'real' home-page (not so frustrating is that I consider it the best browser around).\n\nGoing further with this flaw I also find frustrating is that I can't specify different behavior for file-type handling in web and file-browsing modes. I'd prefer to have text files open in-browser in web mode and in an external editor in file browsing mode. "
    author: "LogicG8"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-20
    body: "I have no idea why people always compare apples and oranges and then complain about things not working a certain way. Konqueror unlike any thing else you have used was designed to be a universal information browser. It is neither a file manager, nor a web browser. What that simply means is that Konqueror does not care  about the location of a particular information so long as the underlying network handler, KIO, supports it. It uniformly deals with all resources whether they are located locally on your own hard drive or on some remote machine.\n\nIf what you want is a separate web-browser and file manager then konqueror is not the right tool for you, period. You can use mozilla/firefox and krusader for filemanagement for example. Do not attempt to make a tool what it is not..."
    author: "DA"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-07-21
    body: "While, as you say: \"[Konqueror] is neither a file manager, nor a web browser\", and \"Konqueror does not care about the location of a particular information...\" - may be true, but for Konqueror to not care and not provide a functional differentiation between local and remote browsing, and file and web browsing, is to make itself too generic. \n\nThus, Konqueror's concept of \"Views\" - i.e., the default Views of \"File Management\", and \"Web Browsing\".  If, as your argument indicates, Konqueror doesn't care, why then would it differentiate between its concept of \"View Profiles\", and why would it have separate configuration dialogs for \"Web Behavior\" and \"Behavior\" (i.e. file management behavior)?\n\nSo, going further, if Konqueror itself is obviously aware of the different use cases inherent in Web Browsing versus File Browsing/Management via its Views concept/behavior/functionality, why does it then procede to squash 'Home URL' and MIME/File-Type behavior together - regardless of the real and practical differences inherent between the two default View types? \n\nErgo Konqueror makes itself somewhat of a victim of its own hypocracy, and therefore - to answer your question as to \"why people always compare apples and oranges and then complain about things not working a certain way\", specificaly when concerning Konqueror's treatment of Home URL and MIME - NO WONDER why people are confused. Additionaly, while you claim that Konqueror \"is neither a file manager, nor a web browser\", I claim that it is BOTH a file manager AND a web browser! So it should be able to act like both, as it provides the sole means of both these activities under the KDE environment.\n\nPerhaps the most logical solution is to make Home URL and MIME behavior configurable aspects of the _View_, rather than of Konqueror itself. Meaning, in the same manner that within the Profile Management configuration dialog, we currently have 'Save URLs in profile', and 'Save window size in profile' - the Profile Management would also be made to include 'Home URL' and File-Type handling configuration options.\n\nThis would keep the idea of 'Konqueror as universal information browser' intact and sound, while still allowing USERS to operate their File Manager and their Web Browser the way they prefer/expect, via the Views paradigm. Not to mention of course the bonus affect of facilitating whatever other \"View\" ( method/purpose of interacting with Konqueror ) the user might dream up aside from the two main defaults."
    author: "corey"
  - subject: "Re: separate homes for konqueror-web & konqueror-f"
    date: 2004-08-23
    body: "Yes to everything. Someone please fix the toolbars. Configuration is a nightmare and the \"view profiles\" do not respect different toolbar configurations. "
    author: "anonymous"
  - subject: "Krita"
    date: 2004-07-20
    body: "Does anyone know if Krita will have support for writing your own filters, and if so, if there is any documentation about it?"
    author: "Noname"
  - subject: "Re: Krita"
    date: 2004-07-20
    body: "Do you mean filters like \"putting an effect on an image\" or do you mean filters like \"load/saving images\".\n\nIf you mean loading/saving, I think it already can do it. As for image effects, I think that is why the plugins are for (for example the histogram plugin.)\n\nAs for documentation, I do not know if the current developers have much time for it. But I think that they would not mind have volunteers, see the Krita's mailing list (named kimageshop):\nhttp://www.kde.org/mailinglists/index.php#krita\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Krita"
    date: 2004-07-22
    body: "I meant filters like \"putting an effect on an image\". I have dabbled with such things in Delphi previously and thought it might be nice to try and do much the same thing in a more mature program than my own attempt. :-)\n\nNot sure about joining the mailinglist, though. Feels like I should be reasonably sure I would actually be able to contribute something. As it is, I am not even sure how to actually install Krita (my cvs knowledge does not extend beyond Konstruct). :-)"
    author: "Noname"
  - subject: "Re: Krita"
    date: 2004-07-24
    body: "Well, you really need to be able to compile Krita from cvs to play with it -- even though you don't need to know much about its internals to write filters. In fact, we could use someone who has knowledge about image manipulation to add a few useful filters.\n\nThere's one problem, though. We are currently redesigning the standard way of accessing image data -- there are now two different ways of grabbing pixels and channels, and we want a third, better way. It's probably not much use doing a lot of work with the current code.\n\nHowever, I really, really want to deliver a first alpha with at least that interface stabilized this autumn, and from that moment you can write plugin filters to your hearts content."
    author: "Boudewijn Rempt"
---
This week's <a href="http://cvs-digest.org/index.php?issue=jul162004">KDE CVS-Digest</a>:
<a href="http://kolourpaint.sourceforge.net/">Kolourpaint</a> adds Emboss and dithering effects, more levels of undo.
<a href="http://digikam.sourceforge.net/">Digikam</a> adds RGB balance plugin.
<a href="http://www.koffice.org/kpresenter/">KPresenter</a> adds a custom slide show option.
<a href="http://www.koffice.org/krita/">Krita</a> improves input tablet support.
<a href="http://www.koffice.org/kexi/">Kexi</a> continues improvement to query editing.
<a href="http://www.koffice.org/kspread/">KSpread</a> gets a new formula engine.
<a href="http://kopete.kde.org/">Kopete</a> sees beginnings of MSN file transfer support.
<a href="http://extragear.kde.org/apps/kconfigeditor.php">KConfigEditor</a> can edit both Gnome and KDE configurations, and export configurations in <a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> JavaScript.

<!--break-->
