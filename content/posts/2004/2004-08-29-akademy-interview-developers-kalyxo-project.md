---
title: "aKademy Interview: Developers From Kalyxo Project"
date:    2004-08-29
authors:
  - "ateam"
slug:    akademy-interview-developers-kalyxo-project
comments:
  - subject: "Nice project"
    date: 2004-08-30
    body: "You two guys rock! (konqi also)\nI wish you success! I also do hope SkoleLinux will be used in more and more schools, giving strong focus on the importance of Edutainment (http://edu.kde.org) in KDE!"
    author: "a (maybe-not-so-annmanymous) fan :)"
  - subject: "Good work!"
    date: 2004-08-30
    body: "Anybody who does work with Konq and ioslaves is okay in my book. Keep up the good work, kevin!"
    author: "Mikhail Capone"
  - subject: "Sounds very nice but..."
    date: 2004-08-30
    body: "This all sounds very nice, but why has this to be done in a distribution-specific way?\nI mean, it's a good thing to improve integration in Debian.\nFor the moment.\nBut this creates other problems again.\nMaybe there will come up some tool for system administration.\nNice thing.\nBut will I be able to use it on Gentoo?\nOn Mandrake?\nSuSE? Red Hat? (you name it)\nMy opinion is the new kio-slaves and all this integration with other toolkits/applications is fantastic.\nThe integration in a specific distribution is not.\nIt's a work that need to be done x times (insert any number greater than 1000 for the x).\nMaybe we should take a look at the Linux Registry project.\nhttp://registry.sf.net\nI think this is a better long-term solution.\nIf this is adopted widely we wouldn't need to care about distribution-specific stuff anymore.\n\nIf I got something utterly wrong and my posting is nonsense - please feel free to tell me so :-)"
    author: "SegFault II."
  - subject: "Re: Sounds very nice but..."
    date: 2004-08-30
    body: "Hello,\n\nDebian is the Universal OS, or at least is supposed to be. It\nhas no company behind it, but many companies base products on\nDebian, and even more should.\n\nThe Linux Registry is not ready. Some people want to have what\nworks for Debian now. In the future, this may become portable,\nbut so far, it's only vaporware. I believe it can never really\nbecome portable before the LSB doesn't cover it and lots of\ncode is changed.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Sounds very nice but..."
    date: 2004-08-30
    body: "One small issue.\n\nIIUC, the LSB specifies Sys V type startup scripts and a subset of RPM for binary packages.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Sounds very nice but..."
    date: 2004-08-30
    body: "I'd say integrate all the features other distributions have in debian. actually there isnt much debian has not... debian isnt that strong with source code, but thats it, imho.\nsome work on auto-package and zero-install to integrate it in debian whould be appreciated i think.\n\ndebian tries to be a 'meta' distribution - and it succeeds, look at mephis, lindows, xandros and others. rpm sucks quite a bit, while you almost never have dependency problems with .deb's... I think its really a pitty gentoo was started instead of improving debian, although I also think it was a good thing, to shake up things a bit.\n\nbut anyway, I agree with you on the linux-registry point."
    author: "superstoned"
  - subject: "Re: Sounds very nice but..."
    date: 2004-08-30
    body: "Well...\nI think Gentoo is an even better \"meta\" distribution.\nBut thats my poor opinion, not based on any technical reasons.\nJust how I feel - so please feel free to ignore it ;-)\n\nYou say deb hasn't problems with dependencies.\nI don't think that has anything to do with the package-format itself.\nTry apt4rpm or urpmi and you won't have dependency-problems with rpm neither.\n\nBack to linux-registry:\nMaybe I'll add an interface to this thingy in my apps.\nIt's a nice thing and it needs support to get widely spread."
    author: "SegFault II."
  - subject: "Great work"
    date: 2004-08-30
    body: "you will likely have more impact that userlinux which if it is not dea will likely die soon."
    author: "AntiGuru"
  - subject: "Re: Great work"
    date: 2004-08-30
    body: "What do you mean? Do you think Debian will die? Or the code they are writing will be erased? there is no way this project is a waste of efforts."
    author: "Amadeu"
  - subject: "Re: Great work"
    date: 2004-08-30
    body: "I _think_ AntiGuro ment that Userlinux will die soon, and that Kalyxo will have more impact than Userlinux had."
    author: "Andre Somers"
  - subject: "thanks and good luck!"
    date: 2004-09-01
    body: "Maybe someone of the project still reads this, as I'd like to say thanks for all your work and the things to come. That's one of the projects that could really bring Debian / KDE to a whole new level.\nNow I only wish there was some more cooperation between all the KDE or Desktop oriented Debian projects, or those based upong... MEPIS Linux, MunjoyLinux and Debian Desktop Distribution come to mind. "
    author: "Christoph Wiesen"
  - subject: "Re: thanks and good luck!"
    date: 2004-09-14
    body: "There's some cooperation between Kalyxo and DDD regarding the use of The Swirl snapshots as a foundation for Kalyxo releases. And about MEPIS and Munjoy, AFAIK they haven't shown any interest at all in cooperating."
    author: "Qerub"
---
At <a href="http://conference2004.kde.org/">aKademy</a> <A href="http://conference2004.kde.org/cfp-userconf/kevin.ottens.peter.rockai-kalyxo.linkingworlds.php">a talk</A> about <a href="http://www.kalyxo.org/">Kalyxo</a> was given by K&#233;vin Ottens and Peter Rockai. Curious what this effort is all about we
  got in touch with the K&#233;vin and Peter and they explained it all.



<!--break-->
<p><IMG src="http://static.kdenews.org/fab/events/aKademy/pics/kalyxo/kalyxo-2-outlines-4.png"  align="right" border="0"></p>
  
<p><strong>Please introduce yourself and what's your role in KDE project </strong></p>

<p><strong>K&#233;vin Ottens:</strong> My name is K&#233;vin Ottens (known as ervin on IRC).
    I'm working mostly around the ioslaves in KDE (lately the new trash:/ and filesystems:/ ioslaves).
    I also made some bugfixing and improvements to Konqueror and worked 
    on fuse-kio which is now <A href="http://freedesktop.org/Software/CTD">hosted on freedesktop.org</A>.</p>

<p><strong>Peter Rockai:</strong> My name is Peter Rockai (known as mornfall on IRC). I work on Kapture which is an APT fronted for KDE.
    I have lightly touched kwin in the past (and plan to revisit it soon).
    Other KDE related activities are fixing bugs from time to time or implement small features 
    here and there as I see fit.</p>
    

<p><strong>At aKademy you guys did a talk about a project called Kalyxo. What is this project all about? </strong></p>

<p><strong>K&#233;vin Ottens: </strong> The primary focus is on creating and bringing innovative technology to the desktop user. 
     [To make the desktop experience smooth for the user, we are working on integration 
     projects as an important part of our mission.]
     We focus on several areas, from desktop integration (among various toolkits and environments 
     like GTK and Qt) to system integration, where KDE integrates seamlessly with hardware and 
     other lower level features of the system. </p>

     
<p><strong>How do you envision this system and desktop integration?</strong></p>

<p><strong>Peter Rockai:</strong> In the ideal case, the user would not need to worry about having GTK and Qt applications on his desktop.
    The big part of this is the KIO-fuse bridge, which enables usage KDE network-transparency 
    in non-KDE applications. If implemented properly within the system (we are working on it), it should be
    possible to edit images with Gimp over ftp without any hurdles (just like you can edit text with Kate
     now). Same for OpenOffice.org and other 3rd party applications.</p>
     
<p><strong>K&#233;vin Ottens:</strong> We're working on the common look and feel too, and that's why we <A href="http://www.kalyxo.org/twiki/bin/view/Main/GtkQtEngine">package the qt-gtk-engine</A> 
    for the Debian users. But the real challenge is in the system integration, so that things 
    work seamlessly from the user point of view. </p>
    
    <p>The future filesystems:/ ioslave is a part of this plan it will replace the old 
    devices:/ ioslave and go even further. We will introduce a way to do more guessing for 
    devices on supported platforms. This way KDE will still be portable which is a big plus of the project.
    But we can provide extra functionality on specific platforms like Linux.</p>
    
    <p>Since we will package some specific hotplug scripts for Kalyxo, we'll ensure that this 
    integration will work even better for Debian. </p>

<p><strong>Peter Rockai:</strong> On another front of system integration, we are trying to close the gap between the underlying 
    system and its UNIX-inherited configuration system and the modern user interface KDE provides.
    This part of effort is really specific to Debian based systems, as we need intimate knowledge of the
    underlying system to be able to fullfill all the criteria users expect in such software. </p>

    
<p><strong>So Kalyxo is sort of subproject and not another distribution?</strong></p>

<p><strong>Peter Rockai: </strong>Well, there are several aspects to Kalyxo. Currently, we are mostly focusing on the 
    augmentation of the existing system, yes. But for the future, we plan to spin off a new 
    platform project, which should be tightly based on current, preexisting Debian technology 
    and infrastructure. </p>
    
<p><strong>K&#233;vin Ottens:</strong> I would say it comes from two facts. First we're KDE developers so we contribute to it. 
    Second the structure of the Debian project itself. As soon as you work with Debian, you're 
    like a subpart of the project. But that does not mean we won't provides CD of course.</p>  
    
<p><IMG src="http://static.kdenews.org/fab/events/aKademy/pics/kalyxo/IMG_0021.JPG"  align="right" border="0"></p>
    
<p><strong>Is there any software or packages for people to try?</strong></p>
            
<p><strong>Peter Rockai:</strong> Yes, currently Kalyxo team is maintaining several KDE applications, some of them already in 
    Debian Unstable, rest in Kalyxo archive.  You can find details on the archive location 
    on our homepage <A href="http://www.kalyxo.org/">http://www.kalyxo.org</A>. As for examples, there is 
    <a href="http://akregator.sourceforge.net/">aKregator</a>, the RSS 
    feed agregator, maintained by Pierre Habouzit in Debian Unstable. </p>
    
    <p>Any Debian user can already start to use <A href="http://www.kalyxo.org/twiki/bin/view/Main/KDebConf">KDebconf</A> (the KDE Debconf frontend) 
    which has been developed by Kalyxo and is now provided by official Debian packages.
    Mario Bensi is currently working on improving its user interface and visual appeal 
    so we expect to have a new release of it ready for integrating into Debian soon. </p>
    
    <p>Then, there is amaroK, the new media player 
    you may have heard about, which is currently in unstable branch of the Kalyxo archive.</p> 
    
    <p>Another technology you hear about much lately is NX and Kalyxo did not miss this either, 
    we now provide the <A href="http://www.kalyxo.org/twiki/bin/view/Main/NoMachineNX">FreeNX</A> server in our repository. </p>
    
    <p>Recently Pedro Jurado Maqueda joined us and maintaining Kiosktool and KGeography.
    He is very interested by educational packages recently, which is a good thing for 
    sharing with a project like <A href="http://www.skolelinux.org/portal/index_html">Skolelinux</A>. 
    We're looking forward to cooperate with the 
    Skolelinux crew in the future.</p>
     
    <p>Finally we also have some more experimental packages for inhouse projects like <A href="http://www.kalyxo.org/twiki/bin/view/Main/Kapture">Kapture</A>.</p>


<p><strong>Are you paid to work on Kalyxo?</strong></p>    

<p><strong>K&#233;vin Ottens:</strong> As far as I know, nobody is currently paid to work on this project.    </p>


<p><strong>It is said that you guys are a reaction to <a href="http://www.userlinux.com">UserLinux</a>, the effort which 
Bruce Perens started. Is this true and if so can you tell me why?</strong></p>

<p><strong>K&#233;vin Ottens:</strong> Yes, UserLinux is part of our history. We're the children of the early
    KDE-Debian effort (hence the name of our mailing list). With UserLinux
    we expected to be able to work on a well integrated platform. Unfortunately
    Bruce Perens chose to support only Gnome. Since then some people left this
    effort very disappointed.</p>
    
    <p>It was the dark age of the Kalyxo project and now we have this new cute name
    and new plans!</p>
    
<p><strong>Peter Rockai:</strong> I have to agree with Kevin on this. The exclusion of KDE from UserLinux was
    pretty unfortunate event, at first. But then, we think it led to rise of Kalyxo,
    which we deem a good end to the whole UserLinux and KDE story.</p>
    
<p><strong>If the Kalyxo project tries to be Debian compatible then why not join <A href="http://www.debian.org/">the Debian 
project</A>?</strong></p>

<p><strong>K&#233;vin Ottens:</strong> In some way we're already joining the Debian project. As I said when you work
    with the Debian project you're a subpart of it. But not fully joining it
    allows us to be a bridge between the KDE and the Debian projects hence our
    slogan "linking the worlds".</p>

<p><strong>Peter Rockai:</strong> Another good point in refraining from being fully absorbed by Debian is the very
    high entry barrier for new contributors. It often takes months before your first
    contribution gets accepted into Debian and then usually even longer to become
    an official Debian Developer.</p>
    
    <p>We are trying to raise a more friendly and more open community in Kalyxo, to help
    newbies contribute more efficiently and with more satisfactory results. By effectively
    "holding their hands" while they create first packages, we let them learn by example,
    instead of forcing them to read loads of technical documentation upfront. Our packaging
    team will then check and upload the packages into Debian. We do not require that all
    our contributors are Debian Developers, but we still encourage everyone to become one.</p>

<p><strong>It is rumored there is a some kind of LiveCD. If so where is this available?</strong></p>

<p><strong>Peter Rockai:</strong> At one point, yes, there was a Kalyxo LiveCD, based on <a href="http://www.credativ.de/">credativ's</a> distribution, made
    by Andreas Mueller. The iso images are currently not available though, because of
    intermittent server problems and server migration. We will make them available
    again at some point, but they are currently outdated and need more work.</p>

<p><strong>What have you guys been working on at aKademy? Anything cooking?</strong></p>

<p><strong>K&#233;vin Ottens:</strong> KDebconf got really more attention during aKademy thanks to Mario Bensi, he really
    made a great job on it.</p>
    
    <p>For me, aKademy has been a great opportunity to discuss the system integration
    area with people here. Since some of them work on other distributions than Debian
    it's interesting to improve the current situation and keeping the portability as
    well. In most case, that mean that our design will be flexible enough to evolve
    and support new technologies. Who knows which facilities the Linux kernel will
    offer us in this area in a year?</p>
    
<p><strong>Peter Rockai:</strong> Yes, we met lots of people from different projects, made new acquaintances and
    of course had lots of fun hacking and talking with other developers. We also gave
    a talk about Kalyxo at the user conference. We got some positive feedback on this
    and a very interesting offer from a Spanish Debian-based distribution called <A href="http://www.tuxum.com/">Tuxum</A>,
    with which we hope to establish close cooperation soon. During the hacking week, we
    spoke with few Skolelinux developers, and they support our goals and are open for
    cooperation. We hope to utilize much of these new contacts to good of all the
    involved projects.</p>
    
<p><strong>What does the Kalyxo project need most now?</strong></p>

<p><strong>K&#233;vin Ottens:</strong> Just like most free software projects - contributors! We want you! =)
    Since we work on several areas we have three kind of tasks to offer :</p>
    
    <ol>
    
     <li>Packaging new KDE related software for Debian unstable, since we
    want that all the packages made by the Kalyxo project will enter Sid.
    It's a strong point of our policy, and we'll try to give advice and
    train you if necessary.</li>

    <li>Developing the missing KDE tools for Debian. We already mentioned
    KDebconf and Kapture, which would need more contributors of course.
    We're planning to work on system configuration tools too, we've some
    plans and early design discussed. But this is a task that will
    require more developers. System configuration means parsing, which
    means lot of work. </li>

    <li>And soon, we expect to work on our Kalyxo platform and CD. We'll
    snapshot a subset of Debian to provide it on a time based release
    schedule. But those snapshots will need to be maintained for bugfixes
    and security, we'll surely need help for this.</li>
    
    </ol>



