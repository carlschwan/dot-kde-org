---
title: "KDE Community World Summit: Registration Now Opened!"
date:    2004-05-03
authors:
  - "kpfeifle"
slug:    kde-community-world-summit-registration-now-opened
comments:
  - subject: "Sounds like there  will be a cool program"
    date: 2004-05-03
    body: "I am sure KDE organizes a great program there. Hopefully I can get at least a few days off work to visit the Summit. Ludwigsburg is not too far from Switzerland..\n\nBTW, will there be shown initial NX/NoMachine integration into KDE ? We could use it in our organization right now. The only atternative is to go for Solaris and SunRays.... Our systems will be migrating from next winter/spring. Currently GNOME on Solaris/SunRays or KDE on Linux/zSeries are under consideration. I personally would prefer KDE -- not least because SunRay's fat *Fire servers cost lots of $$$....\n\nChr\u00c3\u00bcezi,\nAdrian [writing this from within an NX session connecting me to a German public NX application server]"
    author: "Adrian"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "Actually, there is an alternative to NoMachine.  You could take a look at ThinLinc, the product that my company Cendio AB develops.\n\nThinLinc is a thin client server product based on Linux, and we have great integration with KDE.  We even have a feature where you can define per user or unix group which applications are available in the K menu, at the desktop and on the panel for that user.  Well, at least we will have, when version 1.3 is released in about a week from now.\n\nWe also have partners in Germany. We have shown ThinLinc at Cebit together with UniCon, which is well known for creating the Linux based OS that sits in the thin clients from Fujitsu Siemens.\n\nLook at http://www.thinlinc.com/. Under the \"Documentation\" tab you can find a white paper and the complete manual (about 100 pages) in PDF format."
    author: "Inge Wallin"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "### ThinLinc is a thin client server product based on ###\n### Linux, and we have great integration with KDE. ###\n\nNice. A few questions:\n\n* Does it use compression?\n* If it uses compression: is compression ratio configurable?\n* What \"known\" protocol does it use or is it based on: VNC? X11? RDP?\n* If not a known protocol: is it a new one, developed by your company?\n* If a new protocol: is it closed/proprietary or open/Free Software? Are specifications available? \n* What is a typical price of the product?\n* How much bandwidth does a typical office productivity session consume on avarage, if a user works with and has opened at the same time\n --> web browser Mozilla, \n --> mail client KMail, \n --> file manager Konqueror \n --> and word processor OpenOffice?\n* Can it also be used in a peer-to-peer setup across the internet between two equally-equipped KDE boxen?\n\n* Does your company want to sponsor our event mentioned in the news above, \"KDE Community World Summit\"? (Then please get in touch with me...)    ;-)\n\nThanks,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "Actually, we had \"looked\" at ThinLinc (reading their documentation etc. and sending someone to CeBIT). So we are a bit familiar with what it does.\n\n\n \"Does it use compression?\"\n----\nIt uses TightVNC to access Windows Terminal Servers, Linux and Unix X-Application servers. Cendio uses a lot of OpenSource components to create this product: VNC, CUPS, LDAP, rdesktop, SSH, PuTTY...\n\n\n \"If it uses compression: is compression ratio configurable?\"\n----\nUnknown to me -- but this is unlikely\n\n\n \"What \"known\" protocol does it use or is it based on: VNC? X11? RDP?\"\n----\nIt uses all these protocols. One or more distinguished \"ThinLinc\" servers are serving as gateways: the clients access these using TightVNC, and these gateways redirect to Windows Terminal Servers or Linux/Unix X11 Servers, translatinng the VNC protocol to RDP and X11.\n\n\n \"If not a known protocol: is it a new one, developed by your company?\"\n----\nNo -- but still they ask  us in their \"User License Agreement\" to obey a few regulations which we found unacceptable: \n* They want Server as well as Client Access License fees\n* They want to forbid us benchmark testing(!): \"You may not disclose the results of any benchmark test of either the ThinLinc Server Software or ThinLinc Client Software to any third party without Cendio's prior written approval.\"\n* The want to forbid us \"reverse engineering and dissassembly\": \"You may not re-verse engineer, decompile, or disassemble the Product\".\n\nGiven that they use this much Open Source and GPL software we were not sure if they would not at one point be sued by some opensource development group which may feel cheated by them. \n\nWe prefer fully closed source software and licenses or fully open source licenses. In case there are mixed ownerships of intellectual property, we want to have a very clear picture. Cendio didn't provide us this.\n\n\n \"If a new protocol: is it closed/proprietary or open/Free Software? Are specifications available?\"\n----\nThey seem to make a big secret about the addittional technolgies they developed on top of open source components. We didn't trust the legal soundness of that approach and didn't  investigate ThinLinc further for this reason.\n\n\n \"What is a typical price of the product?\"\n----\nI am still curious for that! But their web  page didn't provide a single hint to answer this question. However, they say this \"ThinLinc is largely based on Open Source software, which makes an aggressive pricing possible since the cost of development and maintenance has been kept down.\"\n\n\n \"How much bandwidth does a typical office productivity session consume on avarage, if a user works with and has opened at the same time\n --> web browser Mozilla, \n --> mail client KMail, \n --> file manager Konqueror \n --> and word processor OpenOffice?\"\n----\nSee their attempt to prevent anyone from publishing benchmarks. I assume you get the typical TightVNC responsiveness and performance: in a 100 MBit switched LAN you _can_ work with it -- but it is not a pure pleasure! It will not work well through an ISDN or WAN line.\n\n\n \"Can it also be used in a peer-to-peer setup across the internet between two equally-equipped KDE boxen?\"\n----\nNo. It looks like you would have to install dedicated ThinLinc servers on the remote side to proxy all access to any remote application. And TightVNC will suck over the internet, especially if you have to use dial-up access.\n\n\nChruezi,\nAdrian"
    author: "Adrian"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "\"See their attempt to prevent anyone from publishing benchmarks. I assume you get the typical TightVNC responsiveness and performance: in a 100 MBit switched LAN you _can_ work with it -- but it is not a pure pleasure! It will not work well through an ISDN or WAN line.\"\n\nWhen I was writing my thesis on LTSP, I experimented with thin-clients. I set up a small LTSP-system in my apartment (one client, one server, running through my 100MB switch). Using basic productivity apps in KDE (2.2.2 at the time) consumed minisucle amounts of bandwidth. Watching near DVD-quality movie on the client consumed more bandwidth, but it displayed it without any problems. I really don't see how you could saturate the network by using \"normal\" apps. If you watch a movie, listen to music and use some apps, then maybe you can saturate it in such way that it starts to get unusable. But how many times do you do that in typical LTSP-setup?\n\nNow, remote access (ISDN, WAN etc.) is whole different ballgame."
    author: "janne"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "Actually TightVNC's problem in high-bandwidth networks is not the bandwidth, but the slow codec. JPEG compression costs too much CPU time, rather use the vnc older codecs. (krdc does this when you select 'high')\n"
    author: "AC"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "\"I experimented with thin-clients. I set up a small LTSP-system in my apartment (one client, one server, running through my 100MB switch)\"\n----\nOur needs are different. We are talking about one server (or a cluster of servers), plus 435 clients (figure rising over the next years), with half of them connected via ISDN links.. _That_ is a different ballgame also, even if you ignore the ISDN part! Your type of setup may work for up to 2 dozen clients, but then you have reached the saturation level for the 100MBit/sec link (not on the client side, but on the server).\n\nAlso, LTSP doesn't typically use VNC or TightVNC -- it uses X11."
    author: "Adrian"
  - subject: "Re: Sounds like there  will be a cool program"
    date: 2004-05-04
    body: "\"Your type of setup may work for up to 2 dozen clients\"\n\nI have heard of setups with ALOT more than two dozen clients. City of Largo support something like 220 concurrent users with just two servers. So it's over 100 clients for one server.\n\n\"but then you have reached the saturation level for the 100MBit/sec link (not on the client side, but on the server).\"\n\nWell, the server would obviously use Gigabit Ethernet. and it's not like the clients are pulling huge amounts of data all the time from the server. Most of the time the traffic would be relatively minor. Of course, you could set up another server to serve the clients, or you could see which app takes most resources on the server and move it to a server of it's own.\n\n\"Also, LTSP doesn't typically use VNC or TightVNC -- it uses X11.\"\n\nTrue, I wasn't making a 1:1 comparison there. But I don't think TightVNC eats huge amounts of bandwidth either. Although I haven't studied it, so feel free to correct me :)."
    author: "janne"
  - subject: "aKademy not without me!"
    date: 2004-05-03
    body: "Want to let the world know that you`re going to aKademy? Curious which people you will meet at aKademy? Enter yourself in the <a href=\"http://wiki.kdenews.org/tiki-index.php?page=aKademy+not+without+me+%21\">aKademy not without me!</a> wiki page!\n\n"
    author: "Anonymous"
  - subject: "Re: aKademy not without me!"
    date: 2004-05-03
    body: "Why didn't *you* give your own name then? Are you not coming?  ;-)"
    author: "Kurt Pfeifle"
  - subject: "Re: aKademy not without me!"
    date: 2004-05-03
    body: "My decision depends on who comes. :-)"
    author: "Anonymous"
  - subject: "Good luck all!"
    date: 2004-05-04
    body: "Sounds like an event nobody should miss and I wish I could be there. Unfortunately, it's a bit far from here and I can't leave my company alone for a whole week.\n\nAnyway, make it a good one!\n\nUwe\n"
    author: "Uwe Thiem"
---
KDE announces the immediate opening of <a href="http://conference2004.kde.org/registration.php">registration</a> 
for the <a href="http://conference2004.kde.org/"><i>KDE Community World Summit</i></a> event. Taking place for 9 days (from 21st to 29th of August in Ludwigsburg, Germany) the organizing team is scheduling 5 different event modules.

<!--break-->
<ul>
<li> <a href="http://conference2004.kde.org/devconf.php">KDE Contributors' and Developers' Conference</a> (21st/22nd of August) </li>
<li> <a href="http://conference2004.kde.org/hackfest.php">KDE Coding Marathon for Contributors and Developers</a> (23rd to 27th of August) </li>
<li> <a href="http://conference2004.kde.org/tutorials.php">KDE Tutorials for Users and Administrators</a> (23rd to 27th of August) </li>
<li> <a href="http://conference2004.kde.org/devconf.php">KDE Users' and Administrators' Conference</a> (28th/29th of August)  </li>
<li> <a href="http://conference2004.kde.org/freedomfest.php">Social Event at "International Software FreedomDay"</a> (28th of August) </li>
</ul>
</p> 

<p>
More details about the tutorial program will be announced shortly. KDE contributors:
<ul>
<li> Make sure to organize your travel arrangements in time. 
<li> Make sure to register as soon as possible. Cheap International Youth Hostel beds may be booked out early. 
</ul>
</p> 


