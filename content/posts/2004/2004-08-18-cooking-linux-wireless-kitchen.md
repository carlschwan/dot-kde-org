---
title: "Cooking with Linux: The Wireless Kitchen"
date:    2004-08-18
authors:
  - "mgagn\u00e9"
slug:    cooking-linux-wireless-kitchen
comments:
  - subject: "Wifi Scanning"
    date: 2004-08-18
    body: "KWifiManager is nice to see the signal strength of your current connection but you can't use it, or any other GUI program, to scan for networks.  A graphical kismet is needed.  \n\nThen you need the graphical kismet to be able to stop scanning and try and dhcp passing networks, that would be great."
    author: "Jonathan Riddell"
  - subject: "Re: Wifi Scanning"
    date: 2004-08-18
    body: "I have tried KWifiManager but it crashes the second the gui pops up. My wireless is already working and Knemo already detects my wireless stats correctly. Dunno what is up with it. I will have to sift through bugs.kde.org to get an idea if somebody has any similar problem. I currently use an ARtem card (wavelan/orinoco driver) using PCMCIA on a gentoo box. Works like a charm, even in Kismet."
    author: "rafelbev"
  - subject: "Re: Wifi Scanning"
    date: 2004-08-19
    body: "What the heck is going on with so many of these apps!? There is 'kifi' in kdenonbeta which is poised to make the transition to KDE proper (that's what kdenonbeta is for). But then there's also KWirelessMonitor(http://www.KDE-Apps.org/content/show.php?content=11576), KNemo(http://www.KDE-Apps.org/content/show.php?content=12956), and KWlanInfo(http://www.KDE-Apps.org/content/show.php?content=9827).  So there's a KWifiManager too?!\n\nCan we just settle on one?? Each has there own strengths and weaknesses but the duplication of effort is hurting. The effort should be consolidated."
    author: "Jesse"
  - subject: "Re: Wifi Scanning"
    date: 2004-08-19
    body: "is the \"we\" implicating , that you are working on one of the projects ?"
    author: "chris"
  - subject: "Re: Wifi Scanning"
    date: 2004-08-19
    body: "hey!  while we're busting bawls...what projects are you actively working on?  the poster was merely suggesting a standardization of kde wifi.  if you're not one of the maintainers of those projects, leave him alone.  he's merely trying to promote discussion.  hey!  maybe we'll get lucky & one of dot.kde's readers or project maintaners will take some initiative towards that standardization and we can all benefit.  better than throwing stones i suspect."
    author: "mark"
  - subject: "Re: Wifi Scanning"
    date: 2004-08-19
    body: "I agree mark\n\nThe dot is a place to discuss. Several ideas have originated and been discussed here. I think it is a good place to interact, core developers, users, etc. People who don't have time or training to contribute may have good ideas. \n\nOf course, people should not be rude. In particular, people who are not contributing shouldn't attack the ones doing it (as it sometimes happens, unfortuntately). But this was not the case of the original poster.\n\nAnd going back to the topic. Multimedia consolidation and architecture will be one of the goals of KDE 4. Maybe Internet configuration, including WiFi\n(and other devices) autodetection and easy handling, things like being able to activate and de-activate interfaces without using ifup/ifdown on the command line ... may be this should be another goal for KDE 4. To implement some sort of Internet Connection applet or whatever, to consolidate all this functionality."
    author: "MandrakeUser"
  - subject: "WPA"
    date: 2004-08-19
    body: "Is there a plan to support WPA? Since some of the wireless projects have started supporting WPA and configuring WPA requires a bit of reading around, it would help a lot of people if a nice GUI tool can be used for WPA setup.\nTIA"
    author: "Anon"
  - subject: "Yeesh, this is annoying"
    date: 2004-08-19
    body: "As is usually the case with Marcel's columns, there's some good content but the French restaurant gimmick is just excruciating..."
    author: "Otter"
---
In his latest "<a href="http://www.marcelgagne.com/ljcooking.html">Cooking with Linux</a>", Marcel Gagné covers a number of applications for monitoring and connecting to wireless access points, including KWiFiManager, the wireless LAN manager for KDE. Read all about it in "<a href="http://www.linuxjournal.com/article.php?sid=7623">The Wireless Kitchen"</a>.



<!--break-->
