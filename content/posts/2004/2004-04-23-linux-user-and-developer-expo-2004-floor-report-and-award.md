---
title: "Linux User and Developer Expo 2004, Floor Report and Award"
date:    2004-04-23
authors:
  - "gwright"
slug:    linux-user-and-developer-expo-2004-floor-report-and-award
comments:
  - subject: "The .ORG Village"
    date: 2004-04-23
    body: "Was anyone else a little disappointed with the .ORG village in general? I was roaming around for the FFII, UKCDR and AFFS and I just got the impression that it looked very amateurish and unimportant. In contrast to the shiny corporate exhibition going on on the ground floor, down some stairs that looked like they led to the toilets, you found a very small square space with barely room for two or three laptops, and little custom.\n\nIt was nice to put names to faces, and a good opportunity for some of the key activists on the scene to have some long chats, but it didn't really feel very worthwhile. It certainly didn't feel like the Free Software community, who, let's face it, do 90% of the work for the \"Linux User & Developer\" community (be it hackers or pointy haired bosses), were being given the chance to show off their work, their culture, the whole *point* of Free Software to visitors.\n\nBut then I suppose that's just the kind of thing one should expect from a trade show. Maybe next time *all* the .ORG village exhibitors should go to the pub together! ;-)"
    author: "Tom Chance"
  - subject: "Re: The .ORG Village"
    date: 2004-04-23
    body: "thats par for the course.\nwe refer to the LWE .org pavilion as the .org ghetto.\n\nthe truth is these conferences are for big companies to do more conferences. we are just there to make it not look like the other 50 trade shows they do every year.\n\nnot to be totally ungrateful, they are willing to at least acknowledge our presence, but just remember we are just there for the decoration."
    author: "Ian Reinhart Geiser"
  - subject: "Re: The .ORG Village"
    date: 2004-04-23
    body: "that's one of the things i really liked about the (admittedly small) LinuxFest Northwest this past weekend: companies weren't the focus, it was about the people and the projects. i think it makes a ton of sense to have such community-centric events to compliment the business-centric ones."
    author: "Aaron J. Seigo"
  - subject: "Re: The .ORG Village"
    date: 2004-04-23
    body: "yeah i miss that here.  we had a linux fest in chicago back in the late 90s that was a ton of fun.  it was a bunch of linux hackers from uiuc as i remember.  not to bash the guys who pay the bills, but its nice to not loose focus of why we all came here."
    author: "Ian Reinhart Geiser"
  - subject: "Re: The .ORG Village"
    date: 2004-04-25
    body: "Sure, _would_ be great. Reason it's not happening (nearly often enough): money."
    author: "Inorog"
  - subject: "Re: The .ORG Village"
    date: 2004-04-23
    body: "It was quite disappointing generally. As per usual, the Debian people had by far the nicest booth. We had a bookshelf we had to stand in front of, which was extremely inconvenient. Firstly there was no where to hide behind when you wanted a break, and secondly it was pretty annoying because it was hard to know _who_ exactly was interested in KDE and wanted your attention, rather than just wandering past. It's far easier if they can come up to an actualy booth and catch your eye rather than stare at your back while you finish doing whatever you were doing on your laptop.\n\nAlso the wireless internet was pretty crap, at least it existed though :)\n\nSharing a booth with GNOME is something that is being contemplated, we should then have a large booth with a decent number of visitors and the Debian people can go on a nasty small booth for once :-)"
    author: "Chris Howells"
  - subject: "Re: The .ORG Village"
    date: 2004-04-23
    body: "How did the Debian people get that great big booth?\n\nI agree that theirs was about the only inviting booth in the area. Standing on the AFFS bookshelf, having three \"staff\" blocked the public from our wares entirely, so that we had to keep moving around to let people pick up leaflets."
    author: "Tom Chance"
  - subject: "Re: The .ORG Village"
    date: 2004-04-24
    body: "No idea, bribing the .organiser maybe? :)\n\nI guess it was just envisioned that they would have the largest number of people visiting and therefore they should get the nicest place to go. Which I think is unfair -- the other stands would have had more visitors had they actually been nicer :)\n\nWhile I certainly enjoyed the expo for its social aspects (was greating meeting other kde-gb people and having a drink or two or ten with them afterwards), I don't feel that things went generally as well as the last expo.\n\nOh well I'm doing a bit of thinking and having a look at ways that it could be improved."
    author: "Chris Howells"
  - subject: "Re: The .ORG Village"
    date: 2004-04-24
    body: "Yes, the size of the Debian booth was unfair; perhaps that's why JD spent so much time there? :)\n\nThe expo wasn't too bad - I agree that the booth was pretty rubbish though, and the less said about the wireless internet the better... Still, we had our Irn Bru and IRC - what more could a geek want?\n\nGeorge"
    author: "George Wright"
  - subject: "Re: The .ORG Village"
    date: 2004-04-23
    body: "Just something of interest I found... this shows what the village looked like:\n\nhttp://www.linuxexpo.org.uk/images/stories/village.jpg"
    author: "Tom Chance"
  - subject: "Re: The .ORG Village"
    date: 2004-04-25
    body: "eeek!"
    author: "Inorog"
  - subject: "Nice report"
    date: 2004-04-23
    body: "Thanks for the nice lively report, George! Well done to all KDE representatives! Sure the London expo is a small show but nonetheless important. KDE had a good team here and I hope that Free Software will gain momentum in the UK in the near future :-)\n(ah, I miss London)\n"
    author: "annma"
  - subject: "Nice One"
    date: 2004-04-25
    body: "The Desktop award was very funny. We had what was presumably vanilla KDE going up against commercially 'polished' desktops from Sun and Ximian, and it still one! Where was plain, old vanilla Gnome here?"
    author: "David"
  - subject: "Re: Nice One"
    date: 2004-04-26
    body: "They might just have thought that three GNOMEs against one KDE was a bit silly, and the two commercial GNOMEs make better desktops than vanilla GNOME."
    author: "Tom Chance"
---
On the 20th and 21st April, those of us in the UK were proud to represent KDE at the <a href="http://www.linuxuserexpo.com/">Linux User and Developer Expo</a> 2004 at London's Olympia exhibition centre. <a href="http://www.geeksoc.org/~jr/2004-04-linux-user-expo/100_0856.JPG">Representatives</a> were Jonathan Riddell, Charles Samuels, Chris Howells, Jeff Snyder, David Pashley, Malcolm Hunter, Richard Moore and George Wright, although Malcolm and Richard only came for the second day.










<!--break-->
<p>The show started at 10:00am on the 20th, and the first hour or so was spent getting to know each other, and demonstrating our shiny hardware. Afterwards, we got stuck in to some admin. There was wireless on the stall provided by Adelix network services, demonstrating their content filtering system. We were not too impressed - upon trying to access kde.org we received an "Access Denied!" message! IRC had also been blocked, as well as many other services, so we had to use ssh tunnels to be able to contact the outside world, and to be able to show off our nifty site!</p>

<p>SUSE had been kind enough to provide a demonstration machine for us, running the press copy release of SUSE 9.1 Personal, and also donated a lot of SUSE software for us to hand out - including a highly sought after 2 CD set of SUSE 9.1 Personal! (which we kept for ourselves...)</p>

<p>There was lots of KDE merchandise on the stall, with <a href="http://www.kde.org/stuff/">KDE badges, T-Shirts, polo shirts and countless other wearables</a>. We also had 300 flyers to give to passers by and Jonathan had his Irn Bru; we drank a lot of Irn Bru those two days...</p>

<p>After making the wireless network more usable we started to talk to passers by, making sure that if they didn't use KDE now they would be in the future! Lots of flyers were given out and we hopefully converted a lot of people to KDE!</p>

<p>At about 1:00pm we <a href="http://www.geeksoc.org/~jr/2004-04-linux-user-expo/100_0857.JPG">went to the pub</a> to have lunch - leaving David looking after the stand as a punishment for being treacherous and devoting all his time to the Debian stand, and were greeted with hostile words when we finally returned after an hour or so. Jonathan took an LPI certification exam and we are still awaiting the results.</p>

<p>The main highlight of the day, however, was the <a href="http://www.linuxuser.co.uk/expo/index.php?option=content&task=blogsection&id=8&Itemid=87">prize giving ceremony</a> that evening. Only Richard Moore was able to attend, but he picked up the <a href="http://www.kde.org/awards/">prize for category "Best Desktop Environment"</a> - beating Sun Java Desktop and Ximian Gnome!</p>

<p>The next day, therefore, we were heralded by a <a href="http://ktown.kde.org/~howells/award.JPG">shiny glass plaque</a> stating our victory over <a href="http://www.geeksoc.org/~jr/2004-04-linux-user-expo/100_0853.JPG">the Gnomes</a> and we got back down to business. On the whole, the second day was probably a lot more productive than the first with us being a lot more interactive with passers by!</p>

<p>At 5:00pm it was the end of the expo, with most of us heading off to the pub except myself who had to rush back to school!</p>

<p>Hopefully a few of us will be at <a href="http://conference2004.kde.org/">aKademy</a> in August - I know I will be!</p>

-- George Wright









