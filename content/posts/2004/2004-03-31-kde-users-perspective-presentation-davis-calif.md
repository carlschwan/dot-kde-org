---
title: "\"KDE: A User's Perspective\" Presentation in Davis, Calif."
date:    2004-03-31
authors:
  - "wkendrick"
slug:    kde-users-perspective-presentation-davis-calif
comments:
  - subject: "Kiosk"
    date: 2004-03-31
    body: "Cool! That kiosk is a great idea. Every LUG should do that :)"
    author: "Henrik"
  - subject: "How are the GUI kiosk app planned for KDE 3.3"
    date: 2004-03-31
    body: "going?"
    author: "AC"
  - subject: "Re: How are the GUI kiosk app planned for KDE 3.3"
    date: 2004-03-31
    body: "Fine? http://extragear.kde.org/apps/kiosktool.php"
    author: "Anonymous"
  - subject: "Re: How are the GUI kiosk app planned for KDE 3.3"
    date: 2004-04-02
    body: "\"Status\nIn development...\"\n\nSo where do we download it to try it out?\n"
    author: "Xanadu"
  - subject: "Re: How are the GUI kiosk app planned for KDE 3.3"
    date: 2004-04-02
    body: "kdeextragear-3 CVS module"
    author: "Anonymous"
  - subject: "California"
    date: 2004-03-31
    body: "KDE - An earthquake-proof system. \n\nCalifornia got a poor reputation because of their new governor. But there still must be smart people around.\n\nI think the KDE guys can turn this around and spread the idea of KDE and Free Software. We deserve so much to califonia and its industry."
    author: "Bert"
  - subject: "Re: California"
    date: 2004-03-31
    body: "No no no! California got its poor reputation because of several earlier governors. The current governator is merely reinforces it :-)"
    author: "David Johnson"
  - subject: "Re: California"
    date: 2004-03-31
    body: "You may not know this, but the KDE community is supposed to be nonpolitical. Political statements in this community, no matter how subtle, are inappropriate. Everybody please refrain from such abuse in the future for the good of the project."
    author: "LuckySandal"
  - subject: "Re: California"
    date: 2004-03-31
    body: "agreed... political statements are offtopic"
    author: "AC"
  - subject: "Re: California"
    date: 2004-03-31
    body: "I hate politics and politicians anyway."
    author: "David"
  - subject: "Re: California"
    date: 2004-04-12
    body: "You *do* realise that that is a political statement also?\n<BR><BR>David"
    author: "David Pye"
  - subject: "kiosk webcafe ?"
    date: 2004-03-31
    body: "is it possible to know a step by step procedure to setup one ?\n\nwhat does kiosk allow you to prevent people to do.\n\nwe need more advertising for kiosk.\n\nif the cafe want to charge for the internet, can we setup periode of time, with login and things like that ?\n\n"
    author: "somekool"
  - subject: "Re: kiosk webcafe ?"
    date: 2004-03-31
    body: "The KDE Kiosk Framework README (hidden somewhere in CVS :^) ) has lots of info.\nI've started putting together a step-by-step on how I made the latest kiosk, which is Debian Woody, IceWM and Konqueror from KDE 3.2.0.  (I've had difficulty getting my specific kludgy config files to work under 3.2.1.)\n\nI've only done very, very basic kiosk setup, using Konq as a locked-down browser.  No word processing, no save/load to/from disk, no time limits, etc. etc.\n\n(I think pay-for-internet is only really viable at huge places like Kinkos and Starbucks, where business-people on-the-go know they'll get wireless or Internet access.  Folks around town will usually find the good free wireless places or kiosks ;^) )\n"
    author: "Bill Kendrick"
  - subject: "Re: kiosk webcafe ?"
    date: 2004-03-31
    body: "That's very cool Bill. I'm just wondering what the possibilities with Kiosk would be with things like server, network and directory services. There looks like a lot of really good ways of using this in many environments that hasn't even been scratched yet.\n"
    author: "David"
  - subject: "Woot Woot"
    date: 2004-04-01
    body: "Switches his father from a machine in-capable of running OS X to Debian Linux.  Being a Debian Linux user that's not something to brag as a business victory.\n\nAll the same it is great to read an elderly person found the KDE experience enjoyable as we all tend to do."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Woot Woot"
    date: 2004-04-01
    body: "Hey, there are Debian users here. You don't see us trolling around here about how horrible RPM is, and that Gentoo is a complete waste of time, do you? At least, we don't without provocation. You're a jackass."
    author: "AC"
  - subject: "Re: Woot Woot"
    date: 2004-04-01
    body: "\"Being a Debian Linux user that's not something \"\n\n\"Hey, there are Debian users here\"\n\n2 Debian users and 1 jackass.\n"
    author: "reihal"
  - subject: "Re: Woot Woot"
    date: 2004-04-01
    body: "In the last poll I saw Debian was the most popular distro for KDE Developers, with 25%. http://www.kdedevelopers.org/node/view/294"
    author: "Leonscape"
  - subject: "Any dot regulars going to be there?"
    date: 2004-04-05
    body: "I'll be there... who else is going?  I'd like to match some faces to the names I see here."
    author: "Evan \"JabberWokky\" E."
---
Avid KDE user and Linux- and Open-Source-advocate Bill Kendrick will be discussing the ways KDE creates a rich, useful and fun user experience for Linux newbies and hackers alike.



<!--break-->
<p>Bill Kendrick has become addicted to his KDE 3.2 desktop, recently set up a Konqueror-based <a href="http://www.lugod.org/photos/chamonix/">kiosk for a local cafe</a>, and even switched his father from an aging Macintosh to Debian Linux running KDE, with success and ease.</p>

<p>On Tuesday, April 6th, 2004, Bill will present "KDE: A User's Perspective" at the <a href="http://www.lugod.org/">Linux Users' Group of Davis</a>, California (near Sacramento; close to the San Francisco Bay Area).</p>

<p>LUGOD meets at 6:30pm at the Davis Public Library, 314 East 14th Street.  This presentation will be geared towards a non-technical crowd.  The event is free, and open to the public.  For details, visit: <a href="http://www.lugod.org/meeting/">http://www.lugod.org/meeting</a>.</p>
