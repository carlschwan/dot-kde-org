---
title: "Konqi Graphics Updated, Promo Video Makes Debut"
date:    2004-10-15
authors:
  - "jriddell"
slug:    konqi-graphics-updated-promo-video-makes-debut
comments:
  - subject: "here's an ogg theora version"
    date: 2004-10-15
    body: "check it out"
    author: "Pat"
  - subject: "Re: here's an ogg theora version"
    date: 2004-10-15
    body: "Wow!  I finally found a file format that mplayer can't play (only sound works).  But the high-res AVI plays very well and looks superb so I'm just gonna say that mplayer rocks hard and stick to that story. :)"
    author: "ac"
  - subject: "Re: here's an ogg theora version"
    date: 2004-10-15
    body: "mplayer does play ogg theora, otherwise we would never be able to watch the videos from akademy.  You do need a very new version of mplayer though and linked against some just as new theora libraries.\n"
    author: "Jonathan Riddell"
  - subject: "Re: here's an ogg theora version"
    date: 2004-10-15
    body: "I didn't check the file, but the file format is most probably Theora. Which works with mplayer also if you have libtheora installed (most likely you have also to recompile mplayer...) ;-)"
    author: "lippel"
  - subject: "Re: here's an ogg theora version"
    date: 2004-10-15
    body: "u just need to compile oggtheora and then recompile mplayer or xine. For now only Helix comes with theora out of the box, but I know the xine and mplayer  dudes are working on it :)"
    author: "Pat"
  - subject: "Please don't use .ogg for videos"
    date: 2004-10-15
    body: "Please, please use .ogm, so that normal people know it's a video and not an audio.\nPlease, please use .ogm, so that my *VIDEO* application opens it and not xmms.\nYes it sounds strange, but I use different apps for video and audio files.\n\nPlease, please use .ogm, so that it has a chance of succeeding - using .ogg will result only it confusion.\n\nUSING .ogg for video and audio HURTS BOTH OGG-VORBIS AND OGG-THEORA. DON'T DO IT. NEVER.\n\nYes, I know it's \"cool\" to do technocrat things only few people understand (like naming files according to their container (which is completeley irrelevant) instead of their real file type (which is what counts)) but it's very stupid if you think about it.\n\nThank you very much\n\n"
    author: "Roland"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-15
    body: "That isn't what people at xiph.org (developers of Ogg, Vorbis, Theora, Speex, ...) say. They recommend using .ogg. And I don't think Ogg's developers want to hurt Ogg.\n\nYou can discuss the thing with them (like thousands of people did before you)."
    author: "Anonymous"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-15
    body: "> That isn't what people at xiph.org (developers of Ogg, Vorbis, Theora, Speex, ...) say.\n\nUnfortunately that's true.\n\n>  I don't think Ogg's developers want to hurt Ogg.\n\nThey sure don't want to, but with that decision they do.\n\nLet's just look at the upsides and downsides of using .ogg for video and audio:\n\ndownsides:\n- People are confused\n- Applications are confused\n- Everything on the commandline gets harder: find . -name \".ogg\" will no longer get you what you want\n- Because of this confusion, people will stay away from the format altogether. PEOPLE HATE EVERYTHING THEY DON'T UNDERSTAND. Effectively it's the \"U\"-Part of FUD: Uncertainity. Ask ANY marketing guy. No, ask ANYBODY not deeply into audio/video formats wether he wants to use the same extensions for video and audio files. Bad marketing leads to bad popularity, bad popularity leads to bad support from hardware and software makers.\n\nupsides:\n+ Can't think of any, maybe you can? If there was a good reason for it, maybe I could accept all the downsides - but so far I haven't seen any.\n\n> You can discuss the thing with them (like thousands of people did before you).\n\nUnfortunately, those people are excellent engineers but are so lousy at marketing that they are actually FUD-ing their own project.\n\nThe optimistic outlook is that most .ogm users simply ignore it and name their video files .ogm, at least so far I haven't seen any video files on P2P-networks with .ogg extension but a few .ogms.\nOnly on xiph.org and rarely on other open-source related websites I've seen that.\n\nMaybe the xiph people will wake up sometime and give up this idiotic recommendation.\n\n"
    author: "Roland"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-15
    body: "Sorry to reply to myself, but do you know what real good marketing would be?\n\nUsing \".video\" as extension. Yes, the DOS-times are over and we are allowed to use more than 3 letters.\n\n.video would be perfect - It's still quite short, yet everybody knows what's inside (even those who have absolutely no idea what's an avi, ogm or wmv) which would take away lot of Fear when confronted with a new format.\n\nBut actually, anything other than ogg will be fine - and the community has standardized on .ogm, so let's use that.\n"
    author: "Roland"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-15
    body: "I entirly agree with you.\n\nogg for sound and ogm for movie (that's still not so user friendly but very better than ogg for both).\n\nAnd it's very common to use different software for music and movie : JuK (or amaroK or...) does well with music and xine (or mplayer...) with video.\n\nWith aKademy videos I was very surprized : the sound and video streams was both in .ogg, and when launching the video, xine doesn't support the video and then showed me a visualisation of the sound : I was confused : is it really a video ? a video with sound-visualisation (very stupid, but I never played audio with xine) or a sound only file (as .ogg was for (at least in my mind))?\n\nImagine what it would be for \"casual users\"..."
    author: "Sebien"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-15
    body: "Wow, you have some strong beliefs on this matter. \n\nAs someone who used to work tech support, I think you are giving folks too much credit. Windows has extensions hidden by default remember? Folks don't care about extensions. \n\nI would agree that its kind of confusing, since there are certainly applications that support .ogg the audio file but not .ogg the video file. But its confusing to folks like us, not to the general public which doesn't care."
    author: "Ian Monroe"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-16
    body: "Those won't work well with windows. Windows uses the extension, so having ogg will open the audio player, which may play music, but it may not too."
    author: "Maynard"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-16
    body: "> As someone who used to work tech support, I think you are giving folks too \n> much credit. Windows has extensions hidden by default remember? Folks don't \n> care about extensions.\n\nSo you really want to claim that \"ordinary folks\" never use the Web? (there is no extension hiding in URLs)\n\nI hear \"ordinary people\" talking about mp3s all the time, but I never hear them talk about \"MPEG Audio files\" (or whatever is shown as file type when extensions are hidden), there is even a whole industry named after a file extension: MP3-players.\n\nSo yes, extensions mean a great deal - for everybody.\n\n"
    author: "Roland"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2006-08-23
    body: "Why not just have a customized extension before the .ogg extension?\n\nFor example,\nMusic file: my-song.a.ogg\nVideo file: my-clip.v.ogg\n\nwhere a is audio and v is video."
    author: "Sumit Dutta"
  - subject: "Please don't click twice on the Add button"
    date: 2004-10-15
    body: "Please, please don't click twice on the Add button, so that your comment isn't posted multiple times.\n\nSCNR\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Please don't click twice on the Add button"
    date: 2004-10-15
    body: "Sorry, blame zope for it ;-)"
    author: "Roland"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2004-10-15
    body: "better yet, make the server send the right mimetypes."
    author: "smt"
  - subject: "Re: Please don't use .ogg for videos"
    date: 2005-02-01
    body: "I agree. People may not be bothered by certain extensions because they have been already stablished, MP3 extensions being the most obvious and already mentioned. Besides, if enough people use it, Windows will add a OGM extension reader anyways."
    author: "SomeGuy"
  - subject: "hehehe"
    date: 2004-10-15
    body: "\"no dragons were hurt in the making of this animation\"\n\ni love that :)"
    author: "ac"
  - subject: "SVG, please...."
    date: 2004-10-15
    body: "The most annoying thing about Konqi artwork at the moment is that there is no SVG version of Konqi which I can use it my custom splash screens, etc. I don't want to fire up Blender and render myself all the time I change resolutions or create different resolution versions. \nKDE has put so much work in SVG integration, why isn't there a SVG version of their maskot? \nSomeone has to make up a vector graphics version, please."
    author: "Marcus"
  - subject: "Re: SVG, please...."
    date: 2004-10-15
    body: "hi,\n\nyes.. I could create a SVG too, it's not that hard to trace it from any of the 3d rendered poses. the problem with SVG is, it's static. and one reason for me to create the 3d model was, to have a poseable character. currently, when we see Konqi anywhere, it's always the same pose. and many times it doesnt really fit :) \n\nespecially splash screens are more alive if instead of using always the same pic, you would have a different angle and pose for the character.. \n\nanyways, in what kind of a pose would you want him to be in then, if there was a SVG version? \n\n\n\n.b"
    author: "basse"
  - subject: "Re: SVG, please...."
    date: 2004-10-15
    body: "basse, I just wanted to say I really had great fun watching your video. It gave me a big kick.  Thanks for your great work."
    author: "KDE User"
  - subject: "Re: SVG, please...."
    date: 2004-10-15
    body: "And thanks also to artemio!"
    author: "KDE User"
  - subject: "Re: SVG, please...."
    date: 2004-10-15
    body: "Is there an SVG Export/Renderer for Blender? \n(I know that Maya has an Flash export.)"
    author: "MaX"
  - subject: "Re: SVG, please...."
    date: 2004-10-15
    body: "I think any maskot should have a default pose...\nthink of Tux and the BSD maskots, all of them come in a default pose. Even if Koqi is a 3D model, it should have a pose in which it is used often (in image context), to make it easier to recognize the mascot."
    author: "Marcus"
  - subject: "avi file on Windows"
    date: 2004-10-15
    body: "Hi,\n\nTo show my colleagues in the company this very nice video (thanks Basse!), who mainly work on Windows XP :-(( I sent them the link, but heard that they could not watch the video on WinAmp3 or on Windows media player (version 8).\n\nHas someone a version which can be viewed on those systems ?"
    author: "Martin Koller"
  - subject: "Re: avi file on Windows"
    date: 2004-10-15
    body: "With the VideoLan player you view the movie. This player can be downloaded at:\nhttp://videolan.org/"
    author: "Auke"
  - subject: "Re: avi file on Windows"
    date: 2004-10-15
    body: "Have not tried these video files myself, but it could be you need to download the DivX codec, for general info about codecs http://www.microsoft.com/windows/windowsmedia/format/codecs.aspx. I googled and found links on http://www.divx.com or http://www.divxmovies.com/codec/. Try to get the codec only and make sure its the free version. You do *not* want the adware version. Another cause of problems could be that they have Data Execution Prevention (DEP) enabled in windows xp sp2 (the least likely alternative), if they already have the DivX codec. Or download an alternative videoplayer if that has a divx codec included or installs one, like Auke suggested."
    author: "dnm"
  - subject: "Re: avi file on Windows"
    date: 2004-10-15
    body: "\nyes.. it's avi divx.. so extra codec needed. still, I've noticed that sometimes I can't play these at my work place either (where we run windows 2000) on windows media player, even though I have installed the codec package.. so I nowadays only   use the divx player that comes in the divx codec bundle. that works just fine.\n\nanyways, for people without divx codec, and posibility to install it, I packed it as mpeg1. hope this helps.\nhere it is:\nhttp://www.kimppu.org/basse/kde/konqi_ad1.mpg\n\n.b"
    author: "basse"
  - subject: "Re: avi file on Windows"
    date: 2004-10-15
    body: "Great! Thanks."
    author: "Martin Koller"
  - subject: "Excellent work"
    date: 2004-10-15
    body: "This is absolutely excellent stuff. I just loved the video. I can't remember how many times I watched it to start with but it must have been at least five :) Beautifully and very cutely animated, well done!"
    author: "Chris Howells"
  - subject: "ffdshow"
    date: 2004-10-15
    body: "just a small note on windows codecs\ndivx from www.divx.com is easy to find, but it's lousy and the non-pay version watermarks everything.\nI highly recommend the ffdshow codecs from the ffdshow project on sourceforge. opensource, high quality, plays divx and xvid formats, etc.\n\n"
    author: "Bassam"
  - subject: "new location for files"
    date: 2004-10-15
    body: "yes, I knew this could happen, but it happened sooner than I was hoping for.. this months download limits are now used on my kimppu.org site, I have to do something about that next week, but for now, I moved the files to another location.\n\nhttp://koti.welho.com/bsalmela/kde/\n\nthere. let's hope that stays online :)\n\n.b"
    author: "basse"
  - subject: "Re: new location for files"
    date: 2004-10-15
    body: "http://www.kde.org/stuff/clipart/konqi-magical-rope-video-720x576-divx.avi\n\n\nfab"
    author: "Fabrice Mous"
  - subject: "Why not make some more videos of actual KDE?"
    date: 2004-10-15
    body: "Very nice. I personally would like to see more videos like the one I made when 3.3.1 was released (it's at \nhttp://linuxreviews.org/news/2004/10/13_0_kde_3.3.1/  and plays embedded..)\nthat actually shows off how KDE works, how it can be used and so on.."
    author: "xiando"
---
A range of new Konqi the Dragon graphics and the first Konqi video has been put together by newcomer to the <a href="http://wiki.kdenews.org/tiki-index.php?page=KDE+Artists">KDE Artists</a> mailing list <a href="http://www.kimppu.org/basse/me.html">Bastian Salmela (Basse)</a>.  Unlike previous versions, this new Konqi wireframe model is made in the Free Software application <a href="http://www.blender3d.com/">Blender</a>. You can find Konqi and the Magical Rope of Curiosity video (our <A href="http://dot.kde.org/1097741967/">killer feature</a> at LinuxWorld London) as well as still graphics and their sources on the <a href="http://www.kde.org/stuff/clipart.php">KDE Clipart page</a> and <a href="http://www.kimppu.org/basse/kde/">Basse's KDE page</a>.  Basse is working on more videos and improving the Konqi model so expect more soon.

<!--break-->
