---
title: "KPDF Icon Contest Announces Winner"
date:    2004-11-16
authors:
  - "fmous"
slug:    kpdf-icon-contest-announces-winner
comments:
  - subject: "KPDF"
    date: 2004-11-16
    body: "IMHO KPDF will be the next killer app for the KDE desktop, just like k3b is right now. Look here:\n\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_composting_incremental_overlayalpha.png\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_zoom_rect_transparent.png\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_singlepage_contents.png\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_continous_search.png\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_2pages_fitPage.png\nhttp://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_2p_continous_popup.png\n\nEnrico, Albert, what can I say.. you people rock!!\n\n\nAnd, BTW, congrats Marco! The K-shaped Acrobat logo is delicious!"
    author: "Anonymous"
  - subject: "what about text selection and links....?"
    date: 2004-11-16
    body: "Yes, what about text selection, text search and internet links from within a PDF document? Are these now possible in KPDF? I must admit I am a bit rusty here. But I thank the programmers for the wonderful KDE.\n\nCb.."
    author: "charles"
  - subject: "Re: what about text selection and links....?"
    date: 2004-11-16
    body: "from the screenshots, text search seems to work, as does selection. dunno about internet links. But I guess they work too, with all these improvements, its very unlikely they forgot that :D"
    author: "superstoned"
  - subject: "Re: what about text selection and links....?"
    date: 2004-11-16
    body: "Text selection did work last time I tried, and links inside the PDF works for those PDF with table of contents etc. Text search is supposed to work, but I have not tried. And I would guess internet links got fixed at the same time as the internal links."
    author: "Morty"
  - subject: "Re: KPDF"
    date: 2004-11-16
    body: ">The K-shaped Acrobat logo is delicious!\n\nIt reminds me to a pair of scissors ..err.. skissors ;)"
    author: "Carlo"
  - subject: "Re: KPDF"
    date: 2004-11-16
    body: "I totally agree with you! KPDF is simply amazing, can't wait until kde 3.4 comes out."
    author: "LB"
  - subject: "Why dis you select this ugly one ?"
    date: 2004-11-16
    body: "The other ones are much better"
    author: "Albert"
  - subject: "Re: Why dis you select this ugly one ?"
    date: 2004-11-16
    body: "My post is better then your post.\n\nAt least provide some constructive criticism."
    author: "ac"
  - subject: "Re: Why dis you select this ugly one ?"
    date: 2004-11-16
    body: "Its rather creative but also easy to identify."
    author: "Ian Monroe"
  - subject: "kgs problem."
    date: 2004-11-16
    body: "Sorry for being off-topic.\n\nI recently upgraded kde on my Mandrake 10.1 community \nto version 3.3.1.  And kghostview cannot render eps \nimages anymore.  It was working alright before and only\nkghostview has this problem; gv and ggv show up eps \nimages right.\n\nAny help will be appreciated.\n\nAsk"
    author: "ask"
  - subject: "Re: kgs problem."
    date: 2004-11-16
    body: "> Any help will be appreciated.\n\nhttp://www.kde-forum.org/\nhttps://mail.kde.org/mailman/listinfo\nirc://irc.kde.org/#kde\ncomp.windows.x.kde\n"
    author: "Anonymous"
  - subject: "A huge mistake"
    date: 2004-11-16
    body: "This ikon is a rip-off of the Adobe logo, imho very dangerous from a legal perspective and most Ikons were bad from a usability perspective.\n\nNot a 100% rip-off but at least you are unable to understand it unless you know the adobe logo.\n\nI prefered this one\nhttp://www.kde-look.org/content/show.php?content=16715\n\nI think it is self-explaining.\n\nAll the Icons mentioned above look bad imho."
    author: "gerd"
  - subject: "Re: A huge mistake"
    date: 2004-11-16
    body: "There are no legal concerns because look and feel (such as icons) can not be patented. Also, keeping it close enough but still distinct has the advantage that the users will understand what it is. \n\nThe icon that you prefer has the disadvantage that it has text inside the picture. Icons are supposed to be language-independent so they do not have to be translated when KDE is translated into many languages. For this reason, the icon you mention is not even acceptable quality.\n\nOsho"
    author: "Osho"
  - subject: "Re: A huge mistake"
    date: 2004-11-16
    body: "I have a feeling that that Adobe thing would fall under the realm of a trademark, so yes it could be a legal thing. I think the stacked books would have been a better choice, but for different reasons."
    author: "Sean Brown"
  - subject: "Re: A huge mistake"
    date: 2004-11-17
    body: "well the winner has text in it."
    author: "AC"
  - subject: "WRONG!!! Study up, mate or shut up."
    date: 2004-11-18
    body: "This is not about patenting looknfeel. This is about icon trademarks. An icon that clearly copies a trademarked icon can NOT be used in the EU without a severe legal risk."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: A huge mistake"
    date: 2004-11-17
    body: "\"This ikon is a rip-off of the Adobe logo..\"\n\nYou mean Acrobat Reader's logo? Sorry, but have you ever seen Adobe how Acrobat's application icon and logo look like? Acrobat's aplication icon is a rectangle which has Adobe's logo on top of it (white letter A on red background) and Arobat's logo has either a running man or a juggler as main element).\n\nI guess you probably mean that the red ribbon in the selected Kpdf icon is a ripp off of the red ribbon from the Adobe Acrobat's mime icon. Well, you are wrong again. The red ribbon in Kpdf icon represents a letter K, and the ribon in Acrobat's mime icon is a triangle. \n\n\"...most Ikons were bad from a usability perspective.\"\n\nWell, wrong again. The icon which was chosen as the winner of the contest is good from usability point of view. It is simple (it has only two elements: a book and a ribbon), and it is distinctive (there is no other icon in default Crystal icon set that is similar to Kpdf one). "
    author: "wygiwyg"
  - subject: "Re: A huge mistake"
    date: 2004-11-17
    body: "I think it *is* a mistake. Adobe's PDF logo is trademarked. I feel the resemblance is too large and think a judge might feel the same. The red curved thingy should have been avoided.\n\nEven if a judge would agree with you, you don't want to be dragged into court for something like this, because it will cost you.\n\nTo be clear, I don't think the chosen logo itself is bad. Personally I think the E.G.O. logo would have been a better choice because of the above.\n\nAdobe's PDF icon:  http://www.adobe.co.jp/products/acrobat/images_gen/about_main.jpg"
    author: "Patrick"
  - subject: "Re: A huge mistake"
    date: 2004-11-20
    body: "And trademark rights are almost always sharply enforced, much more than patents, because not doing so would be a huge risk for a company.\n\nIt is their most valuable asset, their very identity, and the last thing they want is to let it slip in the public domain by lack of enforcement.\n"
    author: "."
  - subject: "Re: A huge mistake"
    date: 2004-11-20
    body: "I would also agree with Gerd.  Whilst it's not identical to the Adobe ribbon, it is very very close.  I'm pretty sure a judge would rule in favour of Adobe on this.  Legally, to include this is incredibly risky and stupid of KDE.  \n\nJust remember Lindows vs Windows - a common word (that should never been trademarked in the first place).  A large number of European courts rule in favour of Microsoft.  Now personally, I didn't think it infringed, but it's the courts that matter, not individual personal viewpoints.  \n\nNow before anyone has a go at me, I do realise the difference between a word logo and a image logo.  BUT - they are still identifiable \"images\"\n\nI didn't check all the icons out, but I also like the icon that Gerd pointed out @ http://www.kde-look.org/content/show.php?content=16715 .  It looks good from an artistic point of view.  \n\nI'm not trying to be mean, or play favourites, or bad mouth any of the artists - they are all brilliant, if it wasn't for the legal perspective i'd agree with the judges.  I'm just worried about the legal side of things, have we learnt nothing from the fiaSCO?  Remember, Adobe is a high profile developer, with a large userbase.  If this went to jury, most of the jury would have used Adobe Acrobat at some time in the past, and I would say would easily recognise the ribbon, and judge that it's similar enough to be considered a rippoff.  \n\nUse at your own risk.  Can we please include other icons for kpdf so that users can choose other icons and avoid any risk?\n\nDave W Pastern"
    author: "David Pastern"
  - subject: "Get it while it's hot"
    date: 2004-11-16
    body: "Heya,\n\nI packaged kpdf for your testing pleasure. It's from the kpdf_experiments branch as of 14:50 today. It should compile with KDE 3.3 (at least I did last time I tested it). Here we go:\n\nhttp://developer.kde.org/~danimo/kpdf/\n\nDISCLAIMER: I am not be be blamed if kpdf takes your firstborn or even if it acts up in a less odd manner. Enjoy! :)"
    author: "Daniel Molkentin"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "Hi, thanks very much for the package! Sadly I get this here:\n\n_NO_COMPAT -DQT_NO_TRANSLATION  -MT document.lo -MD -MP -MF \".deps/document.Tpo\" -c -o document.lo document.cpp; \\\nthen mv -f \".deps/document.Tpo\" \".deps/document.Plo\"; else rm -f \".deps/document.Tpo\"; exit 1; fi\ndocument.cpp:771:24: document.moc: No such file or directory\nmake[4]: *** [document.lo] Error 1\n\nAnything that could be done about it? (KDE 3.3.1, Qt 3.3.3)\n\nBye, Joost"
    author: "JC"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "Same here, I simply touch'ed this file and the compile went fine, as did make install. When I try to start it I get some errors though:\n\nkdecore (KLibLoader): WARNING: KLibrary: /usr/kde/3.3/lib/kde3/libkpdfpart.so: undefined symbol: init_libkpdfpart\nkdecore (KLibLoader): WARNING: The library libkpdfpart does not offer an init_libkpdfpart function.\n\nSeems touch'ing the file wasn't enough or there is something else wrong..\n\nrainer."
    author: "rainer"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-17
    body: "\"kdecore (KLibLoader): WARNING: KLibrary: /usr/kde/3.3/lib/kde3/libkpdfpart.so: undefined symbol: init_libkpdfpart\n kdecore (KLibLoader): WARNING: The library libkpdfpart does not offer an init_libkpdfpart function.\"\n\n\nI'm getting this same problem. The make/compile runs without error just fine, however when I try running the built program, I get the above error, and nothing but an empty window appears.\n\n\n \n"
    author: "corey"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-20
    body: "I got this problem when building with gcc 3.4.3.\n\nIt worked with gcc 3.3.4.\n\n"
    author: "JW"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "Seems like cvs2dist has a bug, works with the orginal sources :(\nWill investigate once I find time."
    author: "Daniel Molkentin"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-18
    body: "I just tried compiling it from cvs directly and after fixing a minor issue it worked, but after make install I got the exact same error:\nkdecore (KLibLoader): WARNING: KLibrary: /usr/kde/3.3/lib/kde3/libkpdfpart.so: undefined symbol: init_libkpdfpart\nkdecore (KLibLoader): WARNING: The library libkpdfpart does not offer an init_libkpdfpart function.\n\nMaybe the error is somewhere else?\n"
    author: "rainer"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "I noticed this too in my last CVS update. I think it was accidently left out of the configuration files. Try in kpdf source directory:\n\n$QTDIR/bin/moc kpdf/document.h -o kpdf/document.o\n\nThen make."
    author: "Jose Hernandez"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "*******CORRECTION***********\n\n$QTDIR/bin/moc kpdf/kpdf/document.h -o kpdf/kpdf/document.moc"
    author: "Jose Hernandez"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "Thanks very much for your help!\n\nSomehow it does not want to work:\n\nmoc kpdf/kpdf/document.h -o kpdf/kpdf/document.moc\nkpdf/kpdf/document.h:0: Warning: No relevant classes found. No output generated.\n\nBye, Joost"
    author: "JC"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "The warning indicated an empty .moc file was created, the product of which is essentially the same as using 'touch' in this case. I hadn't noticed it before, though I remembing receiving that same warning. It appears to be a small problem with the source, introduced a few CVS updates ago judging from my experience with successive builds (it could be easily overlooked by the developers if document.moc was already present in the build directory from past builds).\n\nIn any event, the empty document.moc file is now created, so 'make' should now build it.\n\n"
    author: "Jose Hernandez"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-16
    body: "Fix that worked for me:\ntouch kpdf/document.moc"
    author: "someguy"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-19
    body: "Worked just fine for me, thank you, I'm running KDE 3.3.0, and Kpdf that was shipped with it was almost completely unusable, bad text rendering and such... but, this one.. just great ;)"
    author: "petar"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-17
    body: "Thanks, works very well!"
    author: "Asdex"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-20
    body: "I downloaded this and got the following error from ./configure:\nchecking if UIC has KDE plugins available... no\nconfigure: error: you need to install kdelibs first.\n\nWhat does this mean? I do indeed have kdelibs installed (3.3.1). \n\nAlso, in order to build the cvs version, do I have to get the entire KDE source directory? cvs co -r kpdf_experiments kdegraphics/kpdf gave me a source tree that clearly needed other stuff in order to be configured. "
    author: "Jordan Benjamin"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-20
    body: "Qt seems not to know where kdelibs is.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Get it while it's hot"
    date: 2004-11-20
    body: "Ah! And for kpdf_experiments branch, I suppose that you need the root and admin directories of kdegraphics:\ncvs co -A -l kdegraphics\ncvs co -A kdegraphics/admin\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Unifying eletronic document reading app"
    date: 2004-11-16
    body: "The icon says \"Control over your e-books\". So, does this mean that KPdf is being worked out to be independent from the format?\nAnd if yes, when can one expect it to support PS and others?"
    author: "blacksheep"
  - subject: "Re: Unifying eletronic document reading app"
    date: 2004-11-16
    body: "I would guess the statments reflects the fact that PDF are the most used e-book format. None of the other formats comes even close. \n\nPS support is handled by KGhostView. You cold probably add support in KPdf with some kparts magic, byt why. As for suporting other e-books formats, those usually contain some sort of DRM and thus creating problems for users/developers not living in the free world(DCMCA etc). The easiest one to support I belive would be Adobes ebook format, which I belive simply are some DRM atop of a PDF variant."
    author: "Morty"
  - subject: "Re: Unifying eletronic document reading app"
    date: 2004-11-16
    body: "> I would guess the statments reflects the\n> fact that PDF are the most used e-book format.\n> None of the other formats comes even close.\n\nIn the future there might appear more types of e-books, so it'd be wise to prepare the app for that.\n\n> You cold probably add support in KPdf with some kparts magic, byt why.\n\nI think the advantages are obvious:\n- common interface for users;\n- others formats could benefit from the KPdf excelency;\n- user can load another e-book format from within KPdf."
    author: "blacksheep"
  - subject: "Re: Unifying eletronic document reading app"
    date: 2004-11-16
    body: "> when can one expect it to support PS and others?\n\nplease, define 'others'. PS can be supported but what else?\nI have 130+ ebooks in pdf, a couple in TXT and that's all..\nPS can be supported.. I'm pretty sure kpdf can handle those before 1 year, but what about the mysterious 'others' ?\n\nPoll: which ebook format would you see supported by kpdf ?\n"
    author: "arachno"
  - subject: "Re: Unifying eletronic document reading app"
    date: 2004-11-16
    body: "Well, at least DVI I guess..."
    author: "LaTeX user"
  - subject: "Other ebooks"
    date: 2004-11-16
    body: "Well you have Adobes ebook format as I mentioned earlier. You have some ebook format used on PDA's like palm and winCE (.prc). Then there is the format used by Rocket EBook(.rb). And of you have Microsoft Reader ebooks (.lit), which looks like some sort of packaged html-subset much like the html help documents (.chm), but combined with DRM.\n\nYou can get some examples and good reads at Baen Free Library: http://www.baen.com/library/defaultTitles.htm"
    author: "Morty"
  - subject: "Font rendering and sub-pixel anti-aliasing"
    date: 2004-11-16
    body: "Hi,\n\nIt seems that KPDF is the only KDE application that doesn't respect KDE's Font settings about anti-aliasing etc. I have set it up so that I have RGB sub-pixel anti-aliased text in KDE and it works greta in all KDE applications except in KPDF's PDF rendering (the menus etc. are fine - just not the PDF rendering). Any idea when this is going to be fixed? Currently, I use acrobat's CoolType to get this. But, there is no reason why we can not use regular Qt's font rendering to achieve this.\n\nAlso, is there support to rotate the PDF ? This helps a lot in reading books on laptops - I typically just rotate the PDF view by 90 degrees in acrobat reader and that way the book page is of the same size as the laptop screen. Very helpful!!\n\nThese two are the main features that I find missing for my e-book reading needs.\n\nthanks,\nOsho"
    author: "Osho"
  - subject: "Re: Font rendering and sub-pixel anti-aliasing"
    date: 2004-11-16
    body: "isn't the rendering done by xpdf/ghostscripts gs? (which are not a kde apps)"
    author: "Mark Hannessen"
  - subject: "Great job!"
    date: 2004-11-16
    body: "I just compiled KPDF and I really love it. Acrobat Reader is the only non open-source application I am using regularly on my computer and it looks like there is finally a very good open source alternative showing up! Keep up the good work!"
    author: "Michael Thaler"
  - subject: "Re: Great job!"
    date: 2004-11-17
    body: "In one word: WOW!\n\nNote: if links work than back and forward buttons should be implemented."
    author: "miro"
  - subject: "Re: Great job!"
    date: 2004-11-17
    body: "on TODO"
    author: "arachno"
---
Some time ago an <A href="http://www.kde-look.org/news/index.php?id=127">icon contest</A> was hosted on KDE-Look.org to find a new icon for KPDF. Since then, we have <a href="http://dot.kde.org/1095261317/">interviewed</a> the jury, and mentioned KPDF Coolness here on the dot. The jury had a tough time deciding on a winning icon, but they have finally chosen the icon to be used in the next version of KPDF. However, before we reveal to you the winner we would like to mention some of the other runners-up.

<!--break-->
<h3>Some words about the submissions ...</h3>

<p>The jury, which consisted of Albert Astals Cid, Enrico Ros, Frank Karlitschek and Kenneth Wimer have announced several extra awards:</p>

<p>meNGele [Nenad Grujicic] was the first to submit his entry, <A href="http://www.kde-look.org/content/show.php?content=16141">KPDF Pack</A>, merely a few hours after the contest was announced.</p>

<p>We would like to give DrFaustus [Mario GarcÃ­a H.] the concept prize for his submission: <A href="http://www.kde-look.org/content/show.php?content=16531">E.G.O. kpdf</A>. When looking carefully, you can see that the "K" is actually a person reading. She is wearing glasses, petting her cat and smiling. Look closer and you'll see it, too!</p>

<p><A href="http://www.kde-look.org/content/show.php?content=16287">KPDF_icon_2 -Library Card</A> by dadeisvenm [Donald Ade] - 
We would like to give Donald the mis-concept prize :-) <br> A KPDF Library Card... get it and print millions!</p>


<p><A href="http://www.kde-look.org/content/show.php?content=17035">This KPDF wallpaper</A> is a must have for 2 fanatic programmers working on KPDF. Thanks for his wallpaper theobroma [J.D.]!</p>

<p>The Giotto prize: the perfect circle. <br>The <A href="http://www.kde-look.org/content/show.php?content=16307">submission</A> by <a href="http://www.kde-look.org/usermanager/search.php?username=Quickly">Quickly</a> received the Giotto prize. It's the only icon without a single corner!</p>

<p>We had some quite creative and productive artists working - we would like give the productivity award to <A href="http://www.kde-look.org/usermanager/search.php?username=dadeisvenm">dadeisvenm</A> [Donald Ade]: With 6 icons and 1 splashscreen, he was the most productive artist during this contest.</p>


<h3>And the winner is ...</h3>



<p>"But who won?" you might ask. Well the jury have given their <a href="http://static.kdenews.org/fab/interviews/iconcontest/kpdf.html">scores</a>, and it has become clear that <a href="http://www.kde-look.org/usermanager/search.php?username=mart">Marco Martin</a> has won the icon contest for <a href="http://www.kde-look.org/content/show.php?content=16146">another kpdf icon</a>. Congratulations Marco!</p>

<p>When asked for his comment on winning this contest Marco replied: <em>"What can I say, I'm very happy that you liked it and that I could do something for the project that I love... oh my God how I'm banal :-)"</em></p>

<p>KPDF-hacker Albert Astals Cid expresses the sentiment of the jury best: <em>With 33 entries I am very impressed with and proud of the artist community at KDE-Look.org.</em></p>

<p align="center"><a href="http://static.kdenews.org/fab/interviews/iconcontest/16146-1.png"><img src="http://static.kdenews.org/fab/interviews/iconcontest/16146-1_small.png" alt="16146-1_small.png" width="200" height="200" border="0" /></a></p>
