---
title: "KDE CVS-Digest for October 29, 2004"
date:    2004-10-30
authors:
  - "dkite"
slug:    kde-cvs-digest-october-29-2004
comments:
  - subject: "i18n"
    date: 2004-10-30
    body: "Would it be possible to split up the CVS statistics in i18n commits and code commits? It would be fairer to the coders I think, because translations as quickly a lot of lines."
    author: "neli"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "while you're at it, could you also please split up the bug statistics into \"useful\" and \"i'm just there for the fame\" bug \"fixers\".  Some bug \"fixers\" just go around closing wishlist items like \"oh, i don't think that should be implemented\" 50 times or so."
    author: "bug statistics"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "Do you want to troll?\n\n> Some bug \"fixers\" just go around closing wishlist items like \"oh, i don't think that should be implemented\" 50 times or so.\n\nThat's the good right of maintainers and other developers if founded. Open wishes also add to bug.kde.org's clutter."
    author: "Anonymous"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "This is totally wrong. It seems that you never looked in the bug database as we leave even old reports. I check some bugs myself and invite you to to the same. If a user says for example this URL crashes Konqueror, I test it with my cvs HEAD and if it does not crash I add this as a comment. If it crashes then I try to make a test case. If you take time to read the bug reports you'll see that very often the first problem is to understand the bug and to try reproduce it. Doing so takes time. The top 10 bugs closer deserve to have a list.\n\nThere is no such a thing as \"i'm just there for the fame\" bug \"fixers\".\n\nAs for tranlations, they should have a separate list because often there is only a few (1 or 2) people with cvs access and they commit the work of other developers."
    author: "annma"
  - subject: "Re: i18n"
    date: 2004-10-31
    body: "> It seems that you never looked in the bug database as we leave even old\n> reports. I check some bugs myself and invite you to to the same.\n\ni suggest you watch kde-bugs-dist more regularly before you comment then:\n\n<quote author=\"aseigo\">\nwhile i haven't been doing it for the \"fame\" (what little there is to come from it), i have been closing various wishlist items for kicker since i started caretaking\n</quote>\n\nclosing wishlist items defeats the purposes of having them in the first place.  why not just leave them open or mark them with a lower priority.\n\nalso, as for the \"fame\", i have been there and done that :-)"
    author: "moo"
  - subject: "Re: i18n"
    date: 2004-10-31
    body: "in case you get the wrong impression: today i actually implemented one wish, and closed 2 others that had already been implemented (but not closed). i've also been going through and marking wishes that i think should at least be seriously considered and probably implemented at some point as \"New\" (as opposed to Uncomfirmed). more wishes are being marked New than anything else.\n\nbut why would i close a wish if i didn't fulfill it? here's why:\n\nlet's use an absurd example: if someone suggests kicker should crash whenever you click three times on a button, should i implement that? of course not. that would be stupid.\n\nlet's use a less absurd example: if someone wishes that the kmenu should be the middle by default and another wishes that it should be on the left by default, can both wishes be fulfilled? no, because it's not possible. \n\nlet's use an even les absurd example: if someone wishes that kicker could be used to create CAD drawings, should it be kept even though it is completely outside of the scope of the program? of course not.\n\nso the act of submitting a wish does not automatically grant it validity. the wish may not be sensible, it may be outside the scope of the program, it may even conflict with another wish... i used the above three examples because i have dismissed wishes that fall into each of those categories. i hope you can appreciate the rational behind it.\n\nthe bug database isn't there to serve as a Hall Of Wishes People Have Dreamed Up, it's there to propel the development progress. by going through the wishes for kicker, which i don't think has been done exhaustively in a long time, i'm getting a very good idea of what the users want (and don't want). and *that* is why we have a bug database.\n\ndoes that make sense to you? if not, what parts don't make sense to you, and what would be your reasoning?"
    author: "Aaron J. Seigo"
  - subject: "Re: i18n"
    date: 2004-11-01
    body: "ok, your points are valid but could you at least ask the reporter _before_ closing his pet wish (which he/I still thinks is reasonable)?  even if i reopen the bug, i get the impression that you would just close it immediately."
    author: "aff"
  - subject: "Re: i18n"
    date: 2004-11-02
    body: "> but could you at least ask the reporter _before_ closing his pet wish\n\nwhy? the wishlist entry is (or at least should be) a good description of the request, including why it's wanted. a wishlist is a request to the developer(s).\n\n> even if i reopen the bug, i get the impression that you would\n> just close it immediately.\n\nlikely. you could always provide a good rational for the new feature. you could also always ask why it was closed first, as is the norm.\n\nso, which wish of yours did i close?"
    author: "Aaron J. Seigo"
  - subject: "Re: i18n"
    date: 2004-11-04
    body: "e.g. from a few years ago (i'll do this one since it pisses me off the most): \nhttp://bugs.kde.org/show_bug.cgi?id=56922\n\nand the associated commit message shows unreasonableness, to say the least. why can't i have keramik with double-click close? i head for it everytime but it doesn't work because it was removed due to \"_usability_\" reasons. somehow it's more usable for me now. reminds me of gnome people disabling the escape button for cancel (or was it close?) dialogs.\n\nbut i won't go on and on about because people in that report already did and were ignored.\n\nso what if i was to reopen the bug? would you be reasonable about it? you weren't the first time."
    author: "asdf"
  - subject: "Re: i18n"
    date: 2004-11-06
    body: "regarding your blog entry, being \"reasonable\" is defined as:\n\n1. accepting that 90% of users come from windows background and would at least want an _option_ to emulate windows behaviour\n\n2. realising that removing a behaviour and not making it configurable alienates users\n\njust look at some of the people who commented in that bug report. they are not habitual trolls or flamers - some of them are even developers.\n\nmaybe you should step back and realise that i and they have a point."
    author: "asfd"
  - subject: "elephants"
    date: 2004-11-06
    body: "re 1)  the theme provides you with exactly that option.  You chose to use the nicer KDE theme but now you don't want 1 part of it.\n\nMakes you sound like you want back to the freedom you had as a child, but still want to be take seriously like an adult..\n\nThing is; in a world where your actions influence others you will have to take responsibility for your actions.  When Aaron told you that the feature, being configurable or not,  hurts other people you failed to take any responsibility and just continue saying its a good thing to have.\n\nSorry you feel that way."
    author: "Thomas Zander"
  - subject: "Re: elephants"
    date: 2004-11-07
    body: "> re 1) the theme provides you with exactly that option. You chose to use the\n> nicer KDE theme but now you don't want 1 part of it.\n\nwhy is it that look _must_ be tied to feel? don't answer that just to me - answer it to all the people in the bug report.\n\ndo you agree that kde decision making should be by concensus? did you read the associated commit message and how he forcibly closed the bug report without any consultation?\n\n> Thing is; in a world where your actions influence others you will have to\n> take responsibility for your actions. \n\nso why does he walk away from the responsibility of so many people who agree with me?\n\n> When Aaron told you that the feature, being configurable or not, hurts other\n> people you failed to take any responsibility and just continue saying its a\n> good thing to have.\n \nwhere is the justification? it was made up. <sarcasm>and i randomly click on items too so i might accidently hit the X button in the top-right. \"*poof* the window would disappear\". so should the X button be removed too?</sarcasm> why is closing a window so destructive if it prompts you when you have unsaved changes anyway?\n\nseigo has made it less usable for me and the 90% of users who will come from windows background. where is the responsibility you so ironically refer to?\n\nthis behavior is obviously so common that there is even another report about it: http://bugs.kde.org/show_bug.cgi?id=61729 (and i didn't even report it).\n\nseigo has changed behavior and made it unconfigurable. he is _forcing_ _his_ opinions/changes to the way users interact with the computer. then why don't i just use gnome?\n\nseigo is redefining \"usability\" as \"whatever seigo says\". if you are going to change some behavior that is an ingrained habit, then at least make it configurable. this is common sense.\n\nI'm sorry you don't feel that way.\n"
    author: "1234"
  - subject: "Re: elephants"
    date: 2004-11-07
    body: "Why don't you just use the Redmond theme which allows you to have just what you want?"
    author: "ac"
  - subject: "Re: elephants"
    date: 2004-11-23
    body: "It's been a while since I used windows, but i don't recall it acting that way at all.  I'm pretty sure that it would maximize windows when you double-clicked the title bar, and restore them to original size if you double-clicked while maximized...not close them."
    author: "kundor"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "while i haven't been doing it for the \"fame\" (what little there is to come from it), i have been closing various wishlist items for kicker since i started caretaking it ~2 weeks ago. there are currently 249 open wishes for kicker, which is 42 less than a week ago! \n\nsome of those wishes are duplicates and some are just not going to happen due to being unworkable for one reason or another. just because adding a feature is technically possible doesn't mean it's a good idea. but a lot of those wishes are valid, however, and some are truly well thought out.\n\nbut the only way to know which are valid and which aren't is to go through all 249 remaining wishes and triage them one by one. this means some will get closed with WONTFIX, some will get close as DUPLICATE, some will get closed as FIXED due to being implemented in a past version while the bug database was never triaged.\n\nif you want to see even a fraction of those 249 wishes become reality, the process to make it happen will result in a good portion of those wishes being closed along the way. this is how bug database maintenance works.\n\ni understand it can be disappointing if a wish you put there gets marked as WONTFIX, but the goal is to create great software not to implement every singe user's pet idea of the moment. the latter objective is simply not possible to do and leads to disastrous results when attempted."
    author: "Aaron J. Seigo"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "First off, statistics are meaningless. Especially when it comes to programming. Let's repeat that. Statistics are meaningless when it comes to representing programming effort.\n\nSecond, are you suggesting that the i18n effort is not noteworthy? When someone produces a thousand line commit, isn't that remarkable? Doesn't it represent hours of work by an individual or a team?\n\nThird, if someone really wants to be first, they can cheat. Are you suggesting that the developers aren't smart enough to figure that out :)\n\nAnd more seriously, if a coder has done a substantial amount of work, usually his (or her) commits will be highlighted elsewhere. That isn't the case with the i18n people.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "> First off, statistics are meaningless.\n\nNot true. Statistics are meaningless when they totally fail to represent any useful tendancy. That is the case for those \"number of lines commited\" figures.\n\nIf you want to somehow picture the programming effort, maybe you could wheight bugs closed from CVS commits, and wheight again from the number of votes attached to said bugs?\n\n> When someone produces a thousand line commit\n\nThat is only true for i18n, so those should be separate IMHO.\nOtherwise thousand lines commits only mean \"updating autogenerated content\". Wow.\n\nIt is not unusual that a one byte commit represents hours of works by an individual or a team, and fixes 10 bugs in a row.\n\n"
    author: "."
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "Development is also beyond the bug reports. So the commit statistic should not depend on bug reports' features.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: i18n"
    date: 2004-10-30
    body: "> So the commit statistic should not depend on bug reports' features.\n\nI was talking about wheighting the \"top ten bug fixers\", not the number of lines commited :)\n\n\n\n"
    author: "."
  - subject: "Re: i18n"
    date: 2004-10-31
    body: "You would like me to make a judgement on what constitutes worthy work?\n\nNo, it isn't going to happen.\n\nAs I said earlier, statistics are meaningless. At most they tell us that someone has been active. If you are interested, you can then go see what they have done.\n\nThe bug database is simply that. A database of information that contains anything from useless drivel to very useful feedback and patches from users. What developers do after perusing or mucking out the database is what is of interest.\n\nYou seem to have some issue with what asiego has done. Why not watch what code he commits? Read his commit logs. He is an experienced developer with good taste in design. Watch what he does, the code he writes.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: i18n"
    date: 2004-11-01
    body: "> You would like me to make a judgement on what constitutes worthy work?\n\nQuite the contrary: I would appreciate to see some sense and some facts instead of raw figures that just bring confusion.\n\neg. the \"top ten bug fixers\" is certainly misleading as it really is the \"top ten bug closer\".\nDon't get me wrong, having volunteers willing to check bugs and close resolved issues is great, but they don't fix bugs. Bugs closed from CVS commits are bug fixes.\n\n> You seem to have some issue with what asiego \n\nno sorry, wrong Anonymous Coward :)\n"
    author: "."
  - subject: "Ruby support"
    date: 2004-10-30
    body: "Thanks Alexander Dymo for the ruby support to\nkdevelop. This gives me a good excuse to run\nthe cvs version of kdevelop. And this shows\nagain that kdevelop is the number one IDE under\nkde.\nGreat work!\n"
    author: "pieter"
  - subject: "Status of Qt-Mozilla?"
    date: 2004-10-30
    body: "Is there any information available about the status of Qt-Mozilla.\nThings started so promising some weeks ago, hopefully it still goes on :-)\n\nMany thanks to the excellent hackers to dive into Mozilla/Gecko code and\nadd what IMHO was missing so much!"
    author: "ac"
  - subject: "Re: Status of Qt-Mozilla?"
    date: 2004-10-30
    body: "The second hacker is still fighting for CVS access (Mozilla #265484)."
    author: "Anonymous"
  - subject: "Re: Status of Qt-Mozilla?"
    date: 2004-10-30
    body: "If there's anything a hacker can't fight, that would be bureaucracy ;("
    author: "."
  - subject: "KHTML"
    date: 2004-10-30
    body: "I'd just like to thank all of the KHTML developers for their great work. It would be awesome if some of them were actually paid to work on KHTML full-time! :-)\n\nFurthermore, I hope Dirk Mueller will be able (or finds the motivation to) work on KHTML again in the near future.\n\nGreat work, folks! Believe me when I say that it is most appreciated.\n\nBTW, as always, thank you for the digest Derek!"
    author: "Greatful KHTML User"
  - subject: "Financing KHTML Development"
    date: 2004-10-30
    body: "I'd like to see (as many others) Konqueror in a far superior level, and right now it seems there aren't paid devels working on it full time. I'd donate an amount of money to KDE if KDE can grant me that my money would be invested on developping Konqueror, so the question is.... Can I donate to certain parts of KDE that I find underdeveloped or simply so great for the efforts that currently has? Maybe some donations toa 'Konqi team' can bring 1 or 2 full time developers for a couple of months....\n\nJust an idea"
    author: "KHTML Enthusiast"
  - subject: "Re: Financing KHTML Development"
    date: 2004-10-30
    body: "Such an offer should be done to kfm-devel@kde.org I think."
    author: "cartman"
  - subject: "Re: Financing KHTML Development"
    date: 2004-11-01
    body: "Why can't we put a bounty (paypal style) tag to the different bugs and feature requests? I would like to pitch in to a bounty for web-cam support on msn in kopete. But ONLY for this. And if we where 20 or so people chipping in on this bounty, maybe the bounty would be large enough for some one to bother to put this feature in. Just think of a bugzilla feature that gives a (potential) developer the opportunity to look on the bug list and see:\n\nBug nr XXXXX \"Please include this feature!\" VOTES:928 BOUNTY: 142,63 Euros\n\nCheers Jo"
    author: "Jo \u00d8iongen"
  - subject: "KDE 3.4 Release Plan and Feature Plan"
    date: 2004-10-30
    body: "Is there a way for me to update the KDE 3.4 Feature Plan or do I have to mail somebody and ask for it?\n\nI can't find it in the WWW CVS module, and I want to add a few features to kdegames."
    author: "Inge Wallin"
  - subject: "Re: KDE 3.4 Release Plan and Feature Plan"
    date: 2004-10-30
    body: "developer.kde.org is in the developer.kde.org module. :-)"
    author: "Anonymous"
  - subject: "Re: KDE 3.4 Release Plan and Feature Plan"
    date: 2004-10-30
    body: "Ah!  Thanks everybody who answered."
    author: "Inge Wallin"
  - subject: "Re: KDE 3.4 Release Plan and Feature Plan"
    date: 2004-10-30
    body: "it's in the \"developer.kde.org\" module under \"development-versions/kde-features.xml\"; the release plan is there, too"
    author: "Melchior FRANZ"
  - subject: "more iconsets"
    date: 2004-10-31
    body: "in 3.4 feature plan there is :\n    *  NEW IN KDE: Import Noia Icontheme Frank Karlitschek <frank@kde-look.org>\n\nwhat about adding nuvola as well?  it is complete enough now\n\nand put them in kde-more-artwork or somthing similar as they are big (in size)"
    author: "M.Assar"
  - subject: "Re: more iconsets"
    date: 2004-10-31
    body: "> in 3.4 feature plan there is : * NEW IN KDE: Import Noia Icontheme\n\nFYI, that was already added for 3.2 or 3.3 so I wouldn't bet it ever happens."
    author: "Anonymous"
  - subject: "Thank You Apple (NeXT) Engineering for WebCore"
    date: 2004-11-01
    body: "These folks rock. (biased because I used to work with them)\n\nThank you for seeing KHTML's potentials and following through with contributing back what you create ala WebCore."
    author: "Marc Driftmeyer"
  - subject: "Purely For Humor Sake concerning KLatin"
    date: 2004-11-01
    body: "KLatin for KDE 3.4\n\n \nAdd Latin dictionary George Wright <gwright@kde.org>\n \nMake grammar questions less repetitive George Wright <gwright@kde.org>\n\n---------\n\nKinda makes its hard to learn Latin if you don't have a dictionary.\n\n---------\n\nOn a serious note: Thank you George.  Being someone who reads lots of obscure works that are littered with bits of latin I am glad to hopefully know what the hell they are saying."
    author: "Marc Driftmeyer"
  - subject: "GStreamer 0.8.7 [Slightly OT]"
    date: 2004-11-02
    body: "GStreamer was mentioned so I bring this issue up.\n\nGStreamer-0.8.7 has now been released in the newest GNOME release [2.8.1].\n\nKGSt won't compile against this because several functions have been removed from gstutils & there seems to be some other stuff missing that wrapper needs.\n\nI hacked at GStreamer a bit, but found that it wasn't just a few missing functions.  Version 0.8.x appears to be a parital rewrite of version 0.6.x.  So, KGSt is going to have go be rewritten.\n\nAnd GStreamer 0.6.4 has an error in the ASM code.  GCC-3.4.2 finds the error and won't build it. :-(  JuK won't work with Arts.  Things are not going well.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: GStreamer 0.8.7 [Slightly OT]"
    date: 2004-11-02
    body: "Wheeler has promised to update the GStreamer bindings when he has time. Apparently he hasn't got much time. \n\nJuK should work fine with aRts (not to mention KDEMM and aKode in the branch). If regressions happens please file bug-reports or just complain loudly on kde-multimedia like everybody else ;)"
    author: "Allan Sandfeld"
---
In <a href="http://cvs-digest.org/index.php?issue=oct292004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org?newissue=oct292004">experimental layout</a>):
Auto logout support in <a href="http://extragear.kde.org/apps/kiosktool.php">Kiosk Tool</a>.
<a href="http://www.freedesktop.org/Software/hal">Hardware Abstraction Layer</a> support in media kio-slave.
Journal plugin for <a href="http://kontact.org/">Kontact</a>.
<a href="http://www.kdevelop.org/">KDevelop</a> improves <A href="http://developer.kde.org/language-bindings/ruby/">Ruby support</a>.
Bidi support from Webcore merged into <a href="http://www.konqueror.org/features/browser.php">khtml</a>.
<!--break-->
