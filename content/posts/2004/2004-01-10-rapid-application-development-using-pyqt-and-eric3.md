---
title: "Rapid Application Development using PyQt and Eric3"
date:    2004-01-10
authors:
  - "ralsina"
slug:    rapid-application-development-using-pyqt-and-eric3
comments:
  - subject: "Very cool"
    date: 2004-01-09
    body: "That's was pretty damn impressive! Very well-written too. There is a very big potential for Python on KDE. In particular, there is a large domain of applications out there that would be written in Python without problems. You probably wouldn't want to do KHTML in it, because of speed concerns, but what about configuration tools, like RedHat does? \n\nThat said, KDE bindings really need to be in the kdelibs distribution. I'm sure lots of people would like to write PyKDE apps, but don't want to burden users with having to install PyKDE itself.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Very cool"
    date: 2004-01-09
    body: "it will never happen, pyKDE is not part of KDE and is reliant on pyQt outside of KDE.  maby if the maintainers worked within KDE it would be better, but that seems that they wouldnt like to do so.\n\nluckly with things like KJSEmbed and awesome ruby support kdebindings will be all users need.\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser "
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "As I have said before in this forum, there is no problem with including a copy of PyQt in KDE in exactly the same way as it includes qt-copy. It just needs somebody to actually do it.\n\nPhil"
    author: "Phil Thompson"
  - subject: "Re: Very cool"
    date: 2004-01-09
    body: "If more developers would use PyQt or PyKDE, we would see more mature, stable and feature rich applications on linux. And the speed of these programs won't be a big concern. The drawing is done by Qt and if you have performance intensive functions you can still code them in c/c++ and use them from python. But most applications on a desktop don't need that much computing power.\n\nI've heard that the KDE-Debian project will have some administration tools that are based on PyKDE and I think this is a very good decision. :))"
    author: "Wido Depping"
  - subject: "Re: Very cool"
    date: 2004-01-09
    body: "Really, installing PyQt qnd/or PyKDE in Debian or Red Hat is very easy.\n\nBoth are just an apt-get away :-) (although right now if you jump to latest PyQt you lose PyKDE, but that has a simple solution, don't update yet ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "If (when) Python gets a Parrot port, we'll be able to use Python for pretty much every GUI app, without any performance hit!\n"
    author: "Rayiner Hashem"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "/we'll be able to use Python for pretty much every GUI app, without any performance hit!/\n\nPerformance hit is minimal in GUI apps anyway. The only hit Py* GUI apps get, really, is the startup time on slow machines."
    author: "An on"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "Yes, it takes about 5 seconds to start a pyqt app in my box, while a regular qt app takes 1 or 2.\n\nIt\u00b4s specially noticeable for the first such app."
    author: "Roberto"
  - subject: "Re: Very cool"
    date: 2004-01-11
    body: "In my experiance, the impact on startup times is small. Perhaps a second or two extra while all of the libs (Qt+KDE) take 5-10x longer.\n\nIt's not worth worrying about. Worry about the KDE libs startup and linking instead. :-)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "I like the tabbed views onto the text and final output! The design works a lot better than the \"browser above, editor below\" design I went for:\n\nhttp://www.boddie.org.uk/david/Projects/Python/KDE/Images/embedwiki-3.png\n\nI don't know exactly why you say that you \"...probably wouldn't want to do KHTML in it, because of speed concerns...\" as all the rendering work is done by the library. I interpreted the above as advice against rewriting KHTML in Python, although I wouldn't be surprised if a pure-Python renderer could compete quite well on small documents.\n\nUnfortunately, it's true that installing PyKDE is more effort than most people are prepared to put themselves through. Package maintainers do try hard to provide packages for certain distributions, but some appear to have fallen behind:\n\nhttp://sourceforge.net/project/showfiles.php?group_id=61057"
    author: "David Boddie"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "What I *really* wanted was to have the tabs on the right side :-)\n\nMaybe I will switch to a widget stack with a toggle on the toolbar, or something like that, though."
    author: "Roberto"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "Uhm, tabs on the right side like \"kdeappname --reverse\"? Ok, that's probably too much on the right side. ;P"
    author: "Datschge"
  - subject: "Re: Very cool"
    date: 2004-01-10
    body: "No, more like on the right edge instead of on the top :-)"
    author: "Roberto"
  - subject: "Re: Very cool"
    date: 2006-01-13
    body: "For the record, a quick glance at my Gentoo Portage repo seems to indicate that in the time since all this was posted (almost exactly 2 years), PyKDE has become part of KDE proper.\n\ndev-python/pykde stops at version 3.12_pre20051013 and kde-base/pykde exists in versions 3.4.3 and 3.5.0"
    author: "Stephan Sokolow"
  - subject: "Before anyone else says it..."
    date: 2004-01-09
    body: "I'm going to say it.  The one thing I ache for is a RAD environment (meaning mindless drawing of pushbuttons, double clicking aid button and writing code for what it does).  It would be so cool if we had a VB replacement so that I can write some stupid gui app in minutes... especially in python.  (/me drools)\n\n...maybe someday...\n\n(please don't suggest qt designer -- I use it and I know it's fantastic, but it's not so easy as point-and-click development)"
    author: "standsolid"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-09
    body: "Except for clicking on widgets to edit their methods (which Designer can't do AFAIK), everything here was drag&drop&click.\n\nCan you give me a little example of what you find easier? Like \"on VB.Net I can click here and this happens, while using Eric3+Designer I need to do all these other longer awful things\".\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-09
    body: "Well, I stayed away from the whole .NET thing (escaped Windows before then, and refuse to upgrade my VB6 install on vmware)\n\n\nIn VB, I can do an extremely simple project in a few steps\n\n1. In VB -> Start a new project (windows dialog)\n2. Select the button widget to draw a button in the middle\n3. Double click said widget to get  code for the form\n4. A sub is created automagically and i type in 'MessageBox(\"w00t\")'\n5. File->Compile \nand done.\n\nIn python/designer (i don't have eric3 installed, but it seems eric would be overkill in this instance)  I have limited experience using python, so bear with me here.  correct me if  I am mistaken, please\n\n1. In QT designer -> start a new dialog\n2. Select the button widget to draw a button in the middle\n3. Save form1.ui\n4. Open up a shell and run pyuic form1.ui -o form1_ui.py\n5. Make a new Makefile / python code file (like form1_code.py)\n6. write skeleton python app with messagebox popup when form1's pushbutton1 is pressed (or even open eric3 to do the last two steps?)\n7. run make\nand done\n\nIt's not just the increased number of steps, it's the fact that it's really not that intuitive coming from a VB standpoint.  For most apps, I'll use C++ and designer and I love every minute of it.  If i want to make a quick and dirty little app (like adding a gui frontend to a script), It would be so cool to have point-and-click development.\n\nIt's not an awful thing to do it the Designer/Eric3 way, I would just really like a single interface to it all.  \n\nonce again, /me drools\n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-10
    body: "Well, you're using an IDE for VB. Ever tried writing a VB app using a text editor and the CLI? ;-)\n\nSo, let's use eric3.\n\n1. Create new project\n2. Create the main script (this is always the same thing, really, just C&P)\n3. Create a new dialog (menu in eric3)\n4. Open the dialog. (I don;t remember if eric3 opens it when you create it)\n5. Drop a button in the middle\n6. Connect that button to some slot (however you want to call it)\n7. Save the form\n8. Create the Window subclass, and implement the slot there\n9. Compile all forms (it's in the menu in eric3)\n\nNow, you may say, \"hey, that's *more* steps!\". And you are right.\n\nHowever, apps are not built using one form with one button on it that uses\nonly the click callback, are they? ;-)\n\nSteps 1 and 2 are once per-project: don't even think about them. If they bother you too much, save the project at that stage, and work from there\nevery time.\n\nStep 4 is only an extra step once per window. A reasonable app may have about 10 windows. So, it's ten extra clicks over the lifetime of the development. Not really much ;-)\n\nSo, it's 6 steps using eric3, vs 5 steps using VB. Not too bad.\n\nAlso, the reason why step 4 is so easy on VB is because you are just editing something that happens when you click the button.\n\nIf you wanted that same thing to happen, for example, every 10 minutes, or when the user clicks on a menu, *in adddition to when you click on the button*, then you need to create a method somewhere, and put a call there. On Qt, you just connect more stuff to the slot. In the long run, Qt is simpler there.\n\nAlso, once the slots are created, they are available on a dropdown box on eric3, just like in VB.\n\nFinally, I don't know if VB has some equivalent to Qt's actions, qhich let you put the same thing in a menu or a toolbar or whatever with extremely little code.\n\nReally, I think that particular list of steps is heavily biased towards writing apps with a button that say \"w00t\" ;-)\n"
    author: "Roberto Alsina"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-10
    body: "For me, it's not so much the steps you have to take to get there, But ease of use getting to the last step.  If it took me 20 steps in VB and it was point-and-click, IT would be easier to me. by the way -- having a call every ten  minutes would be super easy in VB (new timer -- you could even drag one to your dialog), but I know that's not the point :).\n\nWhat i guess I REALLY want is Designer to be tightly integrated into a source code editor like eric3 or kdevelop.  that would be really awesome\n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-09
    body: "and by the way, i really liked your tutorial too, thanks!\n\nI never looked into eric3 before this.\n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-10
    body: "Yeah, eric3 is not nearly as popular as it should be :-("
    author: "Roberto Alsina"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-10
    body: "Yes, I use it all the time. It's so much easier to program Qt with python than C++. C++ is faster in runtime but python is so much more flexible. I hope for SIP 4 with new-class support.\n\nI don't say C++ is a piece of shit. I enjoy to read book like Modern C++ Design(very good book if you read The C++ Programing Language). But I don't like static typed language for dynamik stuff. You do all the time trick with to circle the type system. "
    author: "Marco Bubke"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-10
    body: "I'm very fond of PyQt myself -- I hope to get the second royalty check for the book I wrote on PyQt any day now -- and now I am working in C++ we (my fingers and me) ache every day for the comfort it gave. If you're writing an ordinary GUI application, then PyQt (and PyKDE) will give you all the speed you need, and your application will be done faster than you'd have thought possible.\n\nStill, when I tried to do a real paint application in Python, one that went beyond reusing QPainter/QPaintDevice, I couldn't even do a prototype that was nearly fast enough, not even by pulling in things like PIL or Numeric. Conceivably, using bindings to ImageMagick might have done it, but instead I decided it was time for my next programming language, and started on C++.\n\nImage my surprise when I realized that a class isn't an object in C++ :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: Before anyone else says it..."
    date: 2004-01-09
    body: "Gambas\nhttp://gambas.sourceforge.net/\n\nHBasic\nhttp://hbasic.sourceforge.net/\n\nLazarus\nhttp://www.lazarus.freepascal.org/\n\n\nCheers' \n\nFab\n"
    author: "Fabrice Mous"
  - subject: "OT:  I did it in wxPython too"
    date: 2004-01-10
    body: "Interesting.  I wrote almost the same app but used wxPython (running windows at work).  It took about 3 hours to do the interface (by hand).  I love python;)  I had some python classes I had previously written that act as a simple search engine. \nI find it very useful to take notes in, especially for things I do once in a while, but have to look up or google every time."
    author: "ac cosa"
  - subject: "mode information"
    date: 2004-01-10
    body: "Hmmm it seems that the PyQt toolkit can be really useful and relatively easy to work with. However does anyone know any book  or any extensive online tutorial about Eric/Designer/Python that can be also used as a python introduction? (hmmm, am I asking too much?)"
    author: "Reader"
  - subject: "Re: mode information"
    date: 2004-01-10
    body: "If you want an intro to Python, the best place to start is the official tutorial:\n\nhttp://www.python.org/doc/current/tut/\n\nIts very clear, very well written, and shouldn't take more than a day or so to complete. Good luck!"
    author: "Rayiner Hashem"
  - subject: "Re: mode information"
    date: 2004-01-10
    body: "Look at http://www.awaretek.com/plf.html, http://www.opendocs.org/pyqt/ and http://diveintopython.org/. Maybe you have questions. There is comp.lang.python."
    author: "Marco Bubke"
  - subject: "Re: mode information"
    date: 2004-01-10
    body: "I wrote a book on PyQt, and the publisher really, really wanted it to also have an introduction on Python, which I really, really didn't want to do, so don't rely on those chapters. However, for anyone who has ever programmed in any programming language, it should be easy enough to pick up Python on the go as you start from the first hello world button program in Chapter 6. I know a couple of people who did...\n\nOh, and if you like the book on http://www.opendocs.org/pyqt/ -- please consider also buying it...  If enough people actually buy the book (I know there's a university where it's a set text, but they tell the students to use the web version, which doesn't have all the illustrations, don't know why), I might get a chance to do an update, one that also includes information on Eric.\n\nNot that Eric needs much in the way of manuals or things: the thing just works and is completely transparent to anyone who has ever used an IDE."
    author: "Boudewijn Rempt"
  - subject: "SIP, PyQt, and Eric3 require root to install"
    date: 2004-01-10
    body: "It is unfortunate that in order to try out the tutorial, users need to have root access to install most of the software.  Eric3, PyQt, and SIP (which PyQt requires) all require root privileges to install.  This seems like a ludicrous requirement and I am unable to give the tutorial a look."
    author: "devotchka"
  - subject: "Re: SIP, PyQt, and Eric3 require root to install"
    date: 2004-01-10
    body: "now that is BS with a capital B ;)\n\nthey require no more privilages than any other KDE program.  I have developed full applications in PyQt including plugins and embedding with no such requirement.\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: SIP, PyQt, and Eric3 require root to install"
    date: 2004-01-10
    body: "It's hardly \"BS.\"  The documentation for all the products say they will only install under root.  Jim Bublitz even admits this is the case:\n\nhttp://mats.imk.fraunhofer.de/pipermail/pykde/2002-August/003449.html\n\nRoberto says below that it is possible to install all three as non-privileged users, but it requires that I compile Python under $HOME--a non-trivial task.  I hope I don't have to devote a weekend getting this to work."
    author: "devotchka"
  - subject: "Re: SIP, PyQt, and Eric3 require root to install"
    date: 2004-01-10
    body: "Compiling Python is just as trivial as compiling PyQt -- configuring and compiling stuff isn't hard anymore, these days."
    author: "Boudewijn Rempt"
  - subject: "Re: SIP, PyQt, and Eric3 require root to install"
    date: 2004-01-10
    body: "Well, installing Qt and KDevelop also require root access!\n\nAnyway, you *can* get all those installed into a user account. It\u00b4s not the suggested method, but you could build your own python interpreter in your homedir, then use that instead of the system\u00b4s.\n\nThat should work at least for eric3 and PyQt, I am not 100% sure about sip."
    author: "Roberto"
  - subject: "Re: SIP, PyQt, and Eric3 require root to install"
    date: 2004-01-13
    body: "Installing apps of any kind generally requires root on *NIXes."
    author: "anon"
  - subject: "I second that"
    date: 2004-01-10
    body: "I confirm that programming an application with Python and PyQT is remarkably simple. I'm a very inexperienced programer (mainly fortran in molecular physics) but I've been able to write a very limited animation utility with its GUI in less than a month, Python and PyQT learning time included.\nThis is really impressive work and it makes programming a GUI application nearly available to anybody.\nSince you seem to know these tools well, I would like to ask a programing question : I'm looking for a way to display some custom widgets vertically (always the same type, a frame with some text fields that represent a single movie sequence, though of course there will be several different instances) with scrollbar appearing if the display is too small and with a possibility to add/delete/move the widgets. It's basically to create a movie timeline, like in a video application, except there will be only one with no sound right now.\nI don't like QListView because it's text only, a sequence cannot be a custom widget. I tried QTable but cannot manage to set the Widget in a Cell, though it should be supported, as far as I could read. I tried a simple QFrame and a QVBoxLayout, but I don't have scrollbars appearing if too many sequences are added to the main layout. \nThere is probably a very simple way of doing what I want but it escaped me right now.... :-(\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: I second that"
    date: 2004-01-10
    body: "QListViewItem:s can be made to contain QPixmaps. I use that feature heavily in my PyQt based apps for managing digicamera pictures. See the setPixmap() method of QListViewItem. QListView has other annoyances, but this is not one of them. :)\n"
    author: "Chakie"
  - subject: "Re: I second that"
    date: 2004-01-10
    body: "It's not just an image I try to set, but a whole frame with different QLineEdit objects. I don't think it can be created as a QPixmap, does it?\nBut thanks anyway. Actually, I've finally found what I need with QScrollView. It's so simple I'm actually ashamed of having posted a question here. :-D\nI'd like to point out, though, to come back on the topic, that my problems are more related to my very limited knowledge fo Qt than to PyQT.\nPyQT works really impressively well and is actually fast enough for most graphical applications."
    author: "Richard Van Den Boom"
  - subject: "Re: I second that"
    date: 2004-01-10
    body: "Ah, I read your original message a bit too fast. :) Indeed, it's quite fun to sometimes just browse the wealth of Qt classes and see what's available. Often you find something that you haven't needed before but which suddenly fits very well into some project you are working on.\n"
    author: "Chakie"
  - subject: "Rapid KDE development?"
    date: 2004-01-10
    body: "I've been trying to write some stuff using this method (before this article was written), basically its a program to make an index of my lecture notes, but for recall and so on I'd like to embed a KPart of the gs viewer so I can view my notes (all scanned in in PDF format)... Can't seem to make it work, even if I change all my classes to be the equivelent K classes.\n\nAnyone tried anything like this before and could give some pointers?"
    author: "Mark Zealey"
  - subject: "Re: Rapid KDE development?"
    date: 2004-01-11
    body: "I've experienced some issues with embedding KParts before, so I experimented a little with PyKDE-3.8.0 and found that I could embed the KGhostView part, but it wouldn't show the document.\n\nIt's quite strange that some KParts work and others fail. For example, the text ones tend to work, but only one of the image viewers will get embedded and show a picture. It's something worth looking into, certainly."
    author: "David Boddie"
  - subject: "Fixed wrong download link"
    date: 2004-01-10
    body: "The link to download the notty souces was broken, but it\u00b4s now fixed."
    author: "Roberto Alsina"
  - subject: "Glad to see TT crippling GPL software..."
    date: 2004-01-12
    body: "\"In order to use eric3 under Win... operating systems you need a commercial or educational license of Qt 3.x from Trolltech and the corresponding license of PyQt (which includes QScintilla) from Riverbank.\"\n"
    author: "tomten"
  - subject: "Re: Glad to see TT crippling GPL software..."
    date: 2004-01-12
    body: "Just be happy that they license it to you for free, moron."
    author: "Roberto Alsina"
---
Tired of long, carefully crafted tutorials trying to show you the quickest way to write a program? Here I take another approach. I just went and did a program <i>and</i> the tutorial <i>while</i> checking the clock. So here you have all three in one: 
<a href="http://www.pycs.net/lateral/stories/16.html">
the app, the tutorial, and the timeline</a>. :-)





<!--break-->
