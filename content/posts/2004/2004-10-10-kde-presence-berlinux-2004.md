---
title: "KDE Presence at Berlinux 2004"
date:    2004-10-10
authors:
  - "binner"
slug:    kde-presence-berlinux-2004
comments:
  - subject: "SUSE"
    date: 2004-10-10
    body: "Is KDE the default in SUSE 9.2?  It was the default and only in SUSE 9.1 Personal, so it will be interesting to see what influence Novell has had."
    author: "ac"
  - subject: "Re: SUSE"
    date: 2004-10-10
    body: "\"It was the default and only in SUSE 9.1 Personal\"\n\nI am not sure what you want to say with this but it sounds wrong to me.\n\nKDE is default for both SUSE LINUX Pro and Personal since a long time.\nHowever, the SUSE LINUX Personal line carried less software, e.g. no development tools and KDE was the only desktop shipped with it. However, for a reason unknown to me, the Personal edition was dropped on 9.2.\n\nNow there is only the Professional edition left, which comes with KDE as default, but also ships with a customized version of GNOME 2.6.\n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "One weakness I see... "
    date: 2004-10-10
    body: "...after having installed SuSE Linux 9.1, I was not amused by the fonts. Heck, much as a product could be technically superior [and SuSE surely is], it must be attractive too. All the fonts look blurry! Even after running the `webfonts.sh' script, I could not easily locate the fonts that I had downloaded! Even when I finally did, they looked blurry too. Not using anti-aliasing made them worse! On the other hand, fonts on Windows look OK and crisp. But I commend SuSE for having automagically idenitified ALL my hardware correctly. All versions of Windows including the very latest, had to be assisted in all cases. My suggestion to the developers is to put more effort on \nthe first impression as well.\n "
    author: "charles"
  - subject: "Re: One weakness I see... "
    date: 2004-10-10
    body: "yes ... of course. I also nether understood this problem. I'm using mandrake, and I have the same problen. Why do the fonts I use _allways_ looks so bad under linux then they are _not_ antialiased ?  In windows they looks much better ! I don't like antialiasing - It's in fact only an artifact to make bad looking fonts looking better. And they really slow down your whole system. Compile mozilla with anti-aliasing and then without. And start it. You will see what I mean ! \n\nHelloWorld"
    author: "HelloWorld82"
  - subject: "Re: One weakness I see... "
    date: 2004-10-10
    body: "Thats because the hinting bytecode interpreter in the Freetype2 library is disabled by default, due to a possible patent issue.  Of course, a mean spirited user could rebuild Freetype2 from source and enable that thing...\n"
    author: "HelloWorld83"
  - subject: "Re: One weakness I see... "
    date: 2004-10-10
    body: "Or use plf freetype packages for mandrake users."
    author: "gnumdk"
  - subject: "tell us how plz"
    date: 2004-10-11
    body: "or does da DCMA forbid that?"
    author: "thatguiser"
---
KDE will be present at <a href="http://www.berlinux.de/">Berlinux 2004</a> which takes place on 22nd and 23rd October in Berlin's technical university. Among <a href="http://www.berlinux.de/vortrag2.htm">the talks</a> will be also one about KDE as enterprise desktop (in German). At the booth we will demonstrate <a href="http://www.kde.org/screenshots/kde330shots.php">KDE 3.3.1</a> and thanks to SUSE the <a href="http://www.suse.com/us/private/products/suse_linux/preview/">upcoming SUSE 9.2</a> KDE desktop which includes among other things <a href="http://static.kdenews.org/binner/dot-stories/OOo-kde.png">OpenOffice.org 1.1.3 with KDE file dialog</a> integration. Of course we will also demonstrate <a href="http://extragear.kde.org/apps/kiosktool.php">Kiosktool</a>, <a href="http://dot.kde.org/1094109094/">kNX</a>, <a href="http://www.scribus.org.uk/">Scribus</a> and hopefully <a href="http://dot.kde.org/1094924433/">Mozilla/Qt and the Gecko kpart</a>.

<!--break-->
