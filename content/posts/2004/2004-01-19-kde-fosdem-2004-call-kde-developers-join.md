---
title: "KDE at FOSDEM 2004: A Call for KDE Developers to Join"
date:    2004-01-19
authors:
  - "fmous"
slug:    kde-fosdem-2004-call-kde-developers-join
comments:
  - subject: "Schmi Dt?"
    date: 2004-01-18
    body: "Typo?"
    author: "CE"
  - subject: "Re: Schmi Dt?"
    date: 2004-01-18
    body: "nope .. \n\nhttp://accessibility.kde.org/about/gunnar.php\n\nCiao \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Schmi Dt?"
    date: 2004-01-18
    body: "I should have used Google first ... (www.schmi-dt.de)"
    author: "CE"
  - subject: "I'd love to"
    date: 2004-01-19
    body: "I'd love to attend some meeting/event/whatever related to KDE, unfortunatly, where I live (Argentina), nothing intrested to that really happens, only plain Linux/LUG events.\nHave fun!"
    author: "Pupeno"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "Every once in a while I speak on one of those events (I think the next one I will is June). Drop by and have a beer ;-)\n\nO sea: En Junio creo que estoy dando una charla, pas\u00e1 y tomamos una birra ;-)"
    author: "Roberto"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "Aja ! Y por que no http://www.proposicion.org.ar/ ;-)\n(shameless plug to a big Argentinian FLOSS org)\n\nPupeno: can't you apply for KDE travel support ? Are you a KDE dev ?\nIf so I think you might qualify !\n"
    author: "MandrakeUser"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "Intresting site, but I think there's plainly not enough people here (in south America) to hold a KDE event, yet.\nI used to be a KDE dev, but for a long time I didn't do anything directly into KDE, just 'independent' applications."
    author: "Pupeno"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "Nice to see you arround Roberto.\nYou're giving a talk in June ? Where ? about what ?\nI'm getting reading to give a talk about RAD (Rapid Application Development) on March (the first CaFeLug CTT, Charla Tecnica Trimestral).\nUnfortunatly, for one reason or another I wasn't able to attend a LUG meeting yet,  I'll try to go to the February meeting, do you go to the meetings ?"
    author: "Pupeno"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "June is for some thing XTech is setting up (no ideea on the details).\n\nI am not a member of Cafelug, though. But hey, I can go anyway. I know some of the guys."
    author: "Roberto Alsina"
  - subject: "Other conference in Malaga (Spain)"
    date: 2004-01-19
    body: "There is another conference almost in the same time. The http://www.opensourceworldconference.com/ in Malaga, Spain in February 18th, 19th and 20th 2004.\n\nI'll do a presentation http://www.opensourceworldconference.com/index.php?module=pagemaster&PAGE_user_op=view_page&PAGE_id=30&MMN_position=51:26:27 (not update yet) about KDE like an enterprise desktop, Antonio Larrosa about KDE development and Kurt Pfeifle about KDEPrint.\n\nI hope many people here, can discover and learn about the KDE project.\n\nP.D. My conference is in Spanish because in English nobody can understand me ;-)\n\n"
    author: "Pedro Jurado Maqueda"
  - subject: "Re: Other conference in Malaga (Spain)"
    date: 2004-01-19
    body: "Pedro, why don't you post a story hete at the dot announcing the conference and the KDE releated talks ? Pretty cool stuff :-)"
    author: "MandrakeUser"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "/*I'd love to attend some meeting/event/whatever related to KDE, unfortunatly, where I live (Argentina), nothing intrested to that really happens, only plain Linux/LUG events.*/\n\nI would like to improve the exchange of ideas with South America in the field of Software patents. I got to know how fruitful the contributions from the Brazilian Government were in the WSIS process. In order to raise the question of software patents efficiency on the international level it will be very important to get to know about voices from South America. \n\nhttp://swpat.ffii.org\n"
    author: "Andre"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "maybe a good place to talk would be on the kde-promo mailinglist \nhttps://mail.kde.org/mailman/listinfo/kde-promo\n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: I'd love to"
    date: 2004-01-19
    body: "Assebility: http://www.ffii.org is a good example, seems to be doen by a colorlind CSS webdesigner. Are there tools with which you can check wether your website s compatible for colorblind people and the other way round to assist colofblind people to create proper CSS?"
    author: "Gerd Br\u00fcckner"
  - subject: "So, I'm not a household name..."
    date: 2004-01-19
    body: "Granted, I'm no RMS, but I was at FOSDEM last year, and will be this year. (I travel from England). I saw David Faure speak, and popped into the developer room -- I had a great time.\n\nNow, like I said, I'm no household name, but I am a hacker to the bone, and I love to write KDE/Qt code (although alot of my more memorable work has not been in C++). If some one has heard of \"Kuake\" or \"KickerPager\" -- that would be me with KDE (nothing extraordinary, but I have also contributed to slicker, kplayer, and I intend Rekall). With KDE ideas, I have a habit of running out of time to conclude the projects. I frequently get ideas, and hack together working-prototypes without finishing them. If anybody is interested, I've documented a few of them here (I'll upload the source-code later):\n\nhttp://www.nemohackers.org/ideas.htm\">http://www.nemohackers.org/ideas.htm\n\nIf there is a place for me this year at FOSDEM, other than a spectator, let me know."
    author: "Martin Galpin"
  - subject: "Re: So, I'm not a household name..."
    date: 2004-01-23
    body: "kuake is great :)"
    author: "Sergio Garcia"
  - subject: "Re: So, I'm not a household name..."
    date: 2004-01-23
    body: "I haven't worked on it for nearly a year.\n\nRight, my pledge. I will have a new version of Kuake before FOSDEM this year."
    author: "Martin Galpin"
  - subject: "Re: So, I'm not a household name..."
    date: 2004-01-23
    body: "KUAKE has transformed my life completly.\n\nThankyou man!\n\nIf you ever come to Newcastle, I will buy you a pint.\n\nRock on Free Software Hacker."
    author: "Allan Clark"
  - subject: "Link zum Thema Assesibility"
    date: 2004-01-20
    body: "http://www.daisy.org\nhttp://forum-on-disability.sbszh.ch\n"
    author: "Hans J\u00e4ger"
  - subject: "What about a kde-debian presentation ?"
    date: 2004-01-20
    body: "I'm looking forward to hearing how the kde-debian project is going ! It would be great if there were such a presentation in the KDE Developpers room..."
    author: "Thibauld Favre"
  - subject: "Re: What about a kde-debian presentation ?"
    date: 2004-01-20
    body: "Skolelinux?"
    author: "Andre"
  - subject: "fosdem pictures"
    date: 2004-02-24
    body: "http://moosh.et.son.brol.be/album/ficheEvent.php?ids=61"
    author: "Moosh"
---
<a href="http://www.fosdem.org/">FOSDEM</a> will take place February 21 and 22, 2004 in Brussels, Belgium. This two-day summit will bring together leading developers in the Open Source community. <a href="http://accessibility.kde.org/about/gunnar.php">Gunnar Schmi Dt</a>
will be the <a href="http://www.fosdem.org/2004/index/speakers/speakers_schmi">main KDE speaker at FOSDEM 2004</a>
and will talk about the <a href="http://accessibility.kde.org/">KDE Accessibility Project</a>. We also have a <a href="http://www.fosdem.org/2004/index/dev_room_kde">dedicated KDE Developers room</a> available
for development talks as well as presentations. We are looking for developers who want to join and perhaps do a talk and/or presentation at the "KDE Developers Room". This is your chance to let us know what kind of talks you would like to see in the KDE Developers Room. Several developers already confirmed for this event: David Faure, Ralf Nolden, Gunnar Schmi Dt, Scott Wheeler, Christophe Devriese, Andy Goossens, Olivier Goffart and Dominique Devriese.



<!--break-->
