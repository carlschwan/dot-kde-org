---
title: "KDE-CVS-Digest for May 21, 2004 or Thereabout"
date:    2004-05-25
authors:
  - "dkite"
slug:    kde-cvs-digest-may-21-2004-or-thereabout
comments:
  - subject: "View Images (9 Files)"
    date: 2004-05-25
    body: "I really like the image comparison feature (although the first splash screen pair seems to be wrong)."
    author: "Anonymous"
  - subject: "Re: View Images (9 Files)"
    date: 2004-05-25
    body: "In what way are they wrong? The images are scaled if they are too large to fit. Click on the scaled image to see full size.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: View Images (9 Files)"
    date: 2004-05-25
    body: "They show the wrong splash screen. It seems that default/locolor/splash_top.png and blue-bend/splash_top.png have both the same filename and revision numbers so they write to the same image cache file and the latter overwrote the first."
    author: "Anonymous"
  - subject: "Gnome HIG, eh?"
    date: 2004-05-25
    body: "\"If this reminds anyone of the progress dialog in Evolution, that's because\nwe now follow the Gnome HIG, as everyone knows. ;)\"\n\nDon't you dare touch the button order on dialog boxes! ;)\n\n*runs* :p"
    author: "Eike Hein"
  - subject: "Re: Gnome HIG, eh?"
    date: 2004-05-25
    body: "> Don't you dare touch the button order on dialog boxes! ;)\n\nToo late ... this is now implemented as a configuration option, too. Just add this line to the \"[KDE]\" section in ~/.kde/share/config/kdeglobals:\n\n[KDE]\nButtonLayout=1\n\nSee kdelibs/kdeui/kdialogbase.cpp for the different layoutRules.\n:-P"
    author: "Melchior FRANZ"
  - subject: "Re: Gnome HIG, eh?"
    date: 2004-05-25
    body: "That's not nearly as helpful as a similar option in Gnome would be ;-)"
    author: "ac"
  - subject: "Re: Gnome HIG, eh?"
    date: 2004-05-25
    body: "Yes, please! Who is the first with a patch against Gtk+/GNOME? :-)"
    author: "Anonymous"
  - subject: "A KDE rss reader?"
    date: 2004-05-25
    body: "Great!.. I've been missing a good KDE RSS reader for a while now.. the only ones available for Linux are gtk (straw or liferea) or java-based. I don't mind that, but they aren't good as their windows equivalents anyways.. looks like akregator is a good start! Will it be included with KDE?\n"
    author: "Jason Stothson"
  - subject: "Re: A KDE rss reader?"
    date: 2004-05-25
    body: "only beta is now available...\nkdepim+=akregator ?"
    author: "Nick Shafff"
  - subject: "Re: A KDE rss reader?"
    date: 2004-05-25
    body: "Please let it mature before even thinking about it."
    author: "Anonymous"
  - subject: "Re: A KDE rss reader?"
    date: 2004-05-25
    body: "why let it mature? Just make it a kpart and embed it in kontact if you want. If you don't, don't!"
    author: "me"
  - subject: "Re: A KDE rss reader?"
    date: 2004-05-27
    body: "It already is a kpart."
    author: "Anonymous"
  - subject: "Re: A KDE rss reader?"
    date: 2004-05-25
    body: "http://akregator.upnet.ru/"
    author: "Joseph Reagle"
  - subject: "Re: A KDE rss reader?"
    date: 2004-05-26
    body: "I prefer eventwatcher (http://kde-apps.org/content/show.php?content=10361) for that.\nIt is also promising.\n\nAs always, if we take a look at all in development programs or to be done features it's a dream and we can't wait to have them mature.\nAnd again and again (with other programs/features)... ;-)"
    author: "Sebien"
  - subject: "Browsing a object oriented project like this"
    date: 2004-05-25
    body: "I was wondering if you use any sort of graphical tool to view the relationships beetwen classes in kde. I'm asking this because I'm a newbie programmer (C++) and I wanted to try my hand at coding something, but I always get a bit daunted on big projects like this.\n\nDan"
    author: "Daniel"
  - subject: "Re: Browsing a object oriented project like this"
    date: 2004-05-25
    body: "Doxygen? See http://api.kde.org/cvs-api/kdeui/html/classKAction.html"
    author: "Anonymous"
  - subject: "replace accents as you type...."
    date: 2004-05-26
    body: "That is gonna save me so much time.\n\nTyping 5 characters when only one is needed and only one will appear is just plain painfull...."
    author: "ybouan"
  - subject: "Re: replace accents as you type...."
    date: 2004-05-26
    body: "Why typing 5 characters? The feature is there so that Quanta does it on the fly. Of course, only if you want it...\n\n(By the way, character entity references can be longer than 5 characters, for example &ccedil; )\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
In a rather late <a href="http://cvs-digest.org/index.php?issue=may212004">KDE CVS-Digest</a>:
<a href="http://dot.kde.org/1084792117/">Security fixes</a> in URI handlers. 
<a href="http://kdepim.kde.org/components/kaddressbook.php">KAddressbook</a> now handles IM addresses. 
<a href="http://devel-home.kde.org/~kppp/">Kppp</a> now can handle multiple modem configurations. 
KUser now can use LDAP, Samba and MD5 shadow passwords.



<!--break-->
