---
title: "George Staikos: Preview of KDE 3.4"
date:    2004-12-03
authors:
  - "binner"
slug:    george-staikos-preview-kde-34
comments:
  - subject: "can't wait"
    date: 2004-12-02
    body: "looks like another rock solid release to make my pc experience that much greater.\n\nthanks."
    author: "dk"
  - subject: "Re: can't wait"
    date: 2004-12-02
    body: "well, right, kde 3.4 will have some goodies (altough, will it have support for hall/d-bus/udev? I am afraid that will be seriously lacking... no automagically appearing usbstick-icons on the desktop (unless the distribution does some work) and no automatically starting of k3b/cdplayer/etc when inserting media... :( thats a pitty. and what about xorg?)\n\nbut I am more waiting for KDE 4 :D\nmore speed, better multi-media, easier to use (I think I'm gonna love the search features) and better looks. hmmmm :D"
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-02
    body: "> but I am more waiting for KDE 4 :D\n\n<rant>\nI'm sure KDE 4.0 will rock, with all the waiting you do\n</rant>\n\nmore seriously though, why not contribute... one step at a time, a JJ after another... :)"
    author: "squared"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "ej, I'm trying to do my little best... write some doc's, answer questions, be there at KDE stands in the netherlands, report bugs, etc... but I'm no coder, I know..."
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-02
    body: "\"and no automatically starting of k3b/cdplayer/etc when inserting media\"\n\nI *H-A-T-E* this feature in Windows XP. It's ******* annoying.\nWell, i think if KDE would have such a feature, it would be possible to turn it off. Not so in Windows."
    author: "panzi"
  - subject: "Re: can't wait"
    date: 2004-12-02
    body: "Actually it's possible to turn it off (with Powertoys, XP-Antispy or alike). Or you can press Shift while inserting a CD - but for this you may be impeached for evading a copy protection. :-)"
    author: "Anonymous"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "The word is \"arrested\". Impeachment could only happen if Bush did it.\t"
    author: "Joe"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "SUSE has this \"feature\" as well in 9.1 Pro.  It took me a little while to figure out where to turn it off (it runs as a applet in the system tray)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "Well, I could wait through at least two more 3.3.x bug fix releases.  It would be nice if we could decide how to install the icons before 3.4.0 is released for one thing.\n\nBut about automatic playing of CD music disks.\n\nMy KDE 3.3 BRANCH install has this feature.  You have to have KsCD running in the background which is optional in KDE (I presume that it isn't in Windows) and it is easy to turn on and off.  There is a check box in the configure dialog for KsCD.\n\nI still have problems playing CDs.  They sound like a record that skips and nobody seems to know how to fix that.  That BUG is much more important than automatic play.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "Hi.\nYour problem with the CDROM and playing music is a hardware problem. Cause, the KSCD frontend just triggers the CDROM (hardware) to start playing. Older CDROM's even had a button under the tray to start playing. And the music itself is routed directly from the CDROM via the audiocable to the soundcard and routed to the speaker out without anything to do for the OS. Though I think another CDROM will solf your problem.\n\nBest regards\nJ.Z."
    author: "Joerg Zopes"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "I used to dislike this feature too, so I disabled Suse's Plugger asap, but recently I tried it out again und now I love it. \n\nI disabled those instances I don't need or like and replaced kscd with \"kfmclient openProfile filemanagement audiocd:/\"."
    author: "Flitcraft"
  - subject: "Re: can't wait"
    date: 2004-12-04
    body: "I agree with u, it'a a very boring feature.\nOnly, it's possible to turn it off in Windows,\nmodifying a line in the system registry.\nI'm not interested in this feature,\nand in any case I hope that the next release\nof KDE let us the possibility of leave\nor turn off thi function\n\nMS"
    author: "Samiel"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "\"well, right, kde 3.4 will have some goodies (altough, will it have support for hall/d-bus/udev? I am afraid that will be seriously lacking... no automagically appearing usbstick-icons on the desktop (unless the distribution does some work) and no automatically starting of k3b/cdplayer/etc when inserting media... :( thats a pitty. \n\nHmm, kde 3.3 does all this on SuSE 9.1 :)\n\nDunno if they released it under a open source license though..\n\nRinse\n"
    author: "Rinse"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "knoppix does it too"
    author: "Pat"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "yeah, but the way they did it isn't considdered 'properly' by the kernel dev's (submount, if I'm right, or supermount (which I use) but both aren't good enough to ever make it into the kernel - udev/hall/d-bus dont have to BE in the kernel.)\n\nits open-source code, btw (at least supermount is) but its like, well, a hack... not \"the right way(TM)\". So we have to wait for d-bus/hall/udev support to have it on lets say debian and slackware, who aren't willing to add such things as a hack."
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "SuSEPlugger watches changes to /proc (or whatever the new one in 2.6 is called) for new devices. hotplug and coldplug monitor for new hardware, automagically insert kernel modules for it, and BAM its in proc or sysfs  or whatever you use. That's how that works. SuSEPlugger then just automounts it and says \"Hey, dude... you got hardware.\" or \"hey, dude... you got media\" depending on whether or not its a USB device or a CD.\n\nThe way it can tell if there's a CD is the CD drive tells the kernel its got media, and the kernel modifies one of those crazy files in proc (or sysfs, yet again). SuSEPlugger then probes the disc to see if its blank, audio, or what have you, and lets u know (and, if applicable, automounts it)\n\nThere no wrong way, to eat a reeses... i mean... probe for hardware and media. \n\nForgive the stilted speech. I haven't slept in days...\n\n"
    author: "Aaron Krill"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "I can't stand that annoying automaticly starts media player when inserting disc, please, please don't put this in KDE !!!"
    author: "Gilles Leblanc"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "hey, I didnt like it in windoze too, and I dont really need it, but if its configurable enough, I can put it to some use :D\n\nfor example, k3b might be started if I put a cdrw into the drive. and a musiccd might be added to the playlist in amarok. a cd with a moviefile might just start playing in kaffeine, I whoulnt mind that."
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "The new media:/ kioslave has a HAL Backend. If HAL is found, it will be used to list backend, hence automatically listing devices upon insersion. However, this backend seriously lakes testing, so please feel free to compile it (You only need a fairly recent HAL >= 4.0, and DBus-Qt bindings from cvs)"
    author: "J\u00e9r\u00f4me Lodewyck"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "yes, but will a icon automatically appear on my desktop if I put in the usbstick? and if I connect my digital camera, will it start digikam?\n\nIf so, well, I dont think we need more. if not, well, there might be some work to do... I didnt use these kind'a functions under windows, but if I can fully configure it, well, I think it whould be usefull for me. and to newbies, it whould be usefull anyway..."
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "If you plug your usb stick, it will get listed automatically by media:/. So if the desktop uses media:/ to list disk partitions (It used to relying on devices:/, but I am not sure if this has been changed to media:/), your usb stick should appear on the desktop.\n\nIf you plug you camera, it will also get listed and the media:/ entry will be mapped to the corresponding camera:/ url.\n\nThere is no \"application autoplaying\" by now, but this might be a good idea for KDE 4 !?\n\nJ\u00e9r\u00f4me"
    author: "J\u00e9r\u00f4me Lodewyck"
  - subject: "Re: can't wait"
    date: 2004-12-03
    body: "I'm kind of also waiting for the big 4 instead of any smaller update. KDE really is a great desktop environment already but it has some small missing features. One is this difficulty to use usbstick and digital cameras, not to mention still cdrom and floppy. If you compare it to Windows, My Computer should be also in KDE. system:/ is a small replacement but still far far behind...\n\nAnd other thing that I hope will be fixed soon is the icons on the desktop. At the moment it's quite difficult to organise them in straight lines and the desktop is a mess really fast if you copy anything to it."
    author: "Big-A"
  - subject: "Re: can't wait"
    date: 2004-12-05
    body: "well, imho \"My Computer\" is a totally braindeath concept. you can easily create a KIO slave for it, considered what is there: hard disks, cdrom drives and other media, and the config screen (??? why the heck? what has that to do with media?) and network. media:/ and locations:/ are much more logical.\n\nby the way, the way windows works with drives (c: d: etc) is stupid. in unix this is done much more elegant: you can mount your drives anywhere.\n\nSo I certainly whouldnt want to see the \"My Computer\" anywhere."
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-06
    body: "OK, I agree that My Computer is stupid name and in Linux it's not very useful to show the hard disks since everyone can view the contents in the directory tree.\n\nBut using some removeable devises can be a bit difficult. Not just CDROM and Floppy which are quite well supported but the biggest problems come with digital cameras and usbsticks. IMHO there should be easier way to access these different devices, without the need to mount the devices manually...\n\nThere might be a easier way to do this, perhaps I just haven't found it yet...\n"
    author: "Big-A"
  - subject: "Re: can't wait"
    date: 2004-12-06
    body: "well, the Locations:/ KIO slave in the upcoming kde 3.4 will fix this... \n\nIt will show (afaik) remote and local filesystems, like ftp:/, samba:/ audiocd:/ pop3:/ telnet:/ etc. \n\nSystem:/ will show other KIOslaves, like (maybe) man:/ and locate:/, fish:/, settings:/, apt:/, programs:/ etc etc. (at least, this is what I imagine... and I'm sure it will be configurable!)."
    author: "superstoned"
  - subject: "Re: can't wait"
    date: 2004-12-06
    body: "My Computer is the stupidest concept since the dawn of time, and the MS implementation is even stupider. Keep it out of KDE."
    author: "KOffice fan"
  - subject: "imap folders in filter rules/sieve"
    date: 2004-12-02
    body: "Yep, I know you can use imap folders with disconnected imap, yes you can easily setup sieve filter rules with kio_sieve but what about  \nhttp://bugs.kde.org/show_bug.cgi?id=50997\n\nAny news from Don Sanders and Marc Mutz?\n\nHmm, thinking about kio_sieve, a simple gui would be a nice kommander project if Marcs Sieve GUI dont get into 3.4  :-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: imap folders in filter rules/sieve"
    date: 2004-12-03
    body: "I'm not doing any sieve related work currently. I am working on bug 50997 (client side imap filtering).\n\nI have a prototype of client side imap filtering working. I still have at least one bug to track down (in the imap code) before it will be working reliably.\n\nAfter that I have to test the prototype in the wild on different imap servers, measure the performance (to compare with other mail clients) and add some fault tolerance to handle unexpectedly losing the connection with the server (currently no mail should be lost but some mails may not be filtered).\n\nBasically it's just quite time consuming. If people want to encourage me to spend more time on it, or work faster, then please see the commercial improvement system and consider making a pledge for Client side IMAP filtering.\n\nhttp://kontact.org/shopping/\nhttp://kontact.org/shopping/sanders.php\n\nThanks,\nDon.\n"
    author: "Don"
  - subject: "Looks a bit oversold"
    date: 2004-12-03
    body: "There are a few things in there that aren't really done (eg QCA isn't complete, even though the article says it is)."
    author: "Brad Hards"
  - subject: "Re: Looks a bit oversold"
    date: 2004-12-03
    body: "It says it's a planned feature for inclusion.  It doesn't say that it's done.  If there's no intention for it to be in KDE 3.4, then it should be moved out of kdesupport anyway.  That's what kdenonbeta means."
    author: "George Staikos"
  - subject: "Re: Looks a bit oversold"
    date: 2004-12-03
    body: "QCA integration is one of the things on the table for KDE 4. kdesupport is it's permanent home now. QCA will be a seperate package, like taglib, that KDE will make use of. \n\nI think you seriously misinterpret the current use of kdesupport, but we can discuss that privately if you'd like."
    author: "Matt Rogers"
  - subject: "Re: Looks a bit oversold"
    date: 2004-12-03
    body: "QCA 1.0 is totally usable for KDE.  And not just usable, but used: Kopete. :)\n\nNot that I'm in charge of anything, but I'd say ship QCA 1 with KDE 3.4, and we can put QCA 2 with KDE 4."
    author: "Justin Karneges"
  - subject: "Compatible w/Firefox?"
    date: 2004-12-03
    body: "I hope Kmail will become completely compatible with Firefox. Konq is a great file browser but it absolutely sucks as a resource robbing web browser. Setting file association to open Firefox instead of Konq is useless and I ****ing HATE the way it writes to /var/tmp/kdecache-'user' before it opens (sometimes!) Firefox. I hope 3.4 or 4.0 fixes this grave weakness. \n\nKDE? Are you listening? Some of us HATE Konq as a browser but love it as a file browser. Please take note!"
    author: "Cougar Smith"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-03
    body: "Did you file bugreports or wished in bugs.kde.org? Plus, nobody forces you to use Konq as a webbrowser. Why do you complain about a choice somebody offers you for free?"
    author: "Carsten"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-03
    body: "No, I admit Konqueror is _still_ not at resource robbing as Firefox. But as a small hack you are free to add long while loops to the code, to get it more on par ;)"
    author: "Allan Sandfeld"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-03
    body: "Ok, just *HOW* can I rectify the Kde/Firefox boondoggle? I'm no programmer and although I love Kde, I will always hate Konq as a web browser."
    author: "Cougar Smith"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-06
    body: "\" I'm no programmer and although I love Kde, I will always hate Konq as a web browser.\"\n\nYou will ALWAYS hate Konqueror as a web-browser? Even if Konqueror in KDE4 would blow Firefox away? Don't make claims you cannot keep. I cannot say that \"I will ALWAYS love Konqueror as a web-browser\", since it's possible that Konq starts to suck in the future releases. Anything is possible.\n\nBut, to answer your question (\"how to use Firefox as the default-browser instad of Konqueror?\"): Control Center ==> KDE Components ==> Component Chooser ==> Web Browser ==> \"Open HTTP and HTTPS URLs in the following browser\" ==> Firefox\n\nI tried that just now: I clicked a link in Kmail, and it opened in Firefox. Zero problems. Really, things like these are NOT rocket-science!"
    author: "Janne"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-03
    body: "You must not only associate mozilla-firefox with HTML, you must also tell KDE, that mozilla-firefox can handle urls. Replace the command line in the desktop file  with \"mozilla-firefox %u\". Then mozilla will handle the url, not kio.\n\nThis is not a bug of KDE, but of the mozilla-firefox.desktop file."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Compatible w/Firefox?"
    date: 2005-02-08
    body: "this is not exactly true,\ni have associated Firefox as default browser for kde 3.2 and added %u in the end\nbut still rss feed links in kontact newfeeds open only in Konqueror,\nbut in KMail links contained in e-mails open in firefox fine"
    author: "Raghu GS"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-03
    body: "KControl -> KDE Components -> Component Chooser -> Web Browser-> Pick firefox.\n\nHave a nice day, and next time, do some research in KControl before ranting about nonsense."
    author: "Jason Keirstead"
  - subject: "Re: Compatible w/Firefox?"
    date: 2004-12-29
    body: "This post really isn't nonsense since so many people have had problems with it.  Do a search on the internet for making Firefox the default browser.  MozillaZine doesn't even tell you about the URL handling part.  The make default browser option also does not work correctly."
    author: "Anonymous"
  - subject: "Re: Compatible w/Firefox?"
    date: 2005-08-23
    body: "Heh, yes that is a partial solution of the problem but if only that was the end of the matter! The real problem extend beyond using another browser as the default.\n\nIf you associate html with another browser, Konq no longer functions as a browser. This isn't the desired functionality! For example a web developer may associate html with an editor/browser, but also test those pages in konq. Konq isn't good at playing both the role of internet browser and file browser -> sometimes those roles conflict. It won't happen just because I say so, but konq should be split into two seperate applications. "
    author: "John Dean"
  - subject: "Re: Compatible w/Firefox?"
    date: 2005-01-12
    body: "Sorry to be an idiot but in my component chooser i only have thw following three\n\nemail client\nembedded text editor\nterminal emulator\n\nso how do I add web browser\n\nusing suse 9.1 and KDE 3.2.1\nthanks in anticipation\n\nJohn"
    author: "John"
  - subject: "Re: Compatible w/Firefox?"
    date: 2005-12-02
    body: "indeed choosing firefox in the kde control center works, >except that the browser page remains behind the kmail screen until i click on the desktop task bar item for firefox. anyone had to deal with this  one?\n\ntom arnall\nnorth spit, ca\n"
    author: "tom arnall"
  - subject: "KDE-Science Mailing list"
    date: 2004-12-03
    body: "KDE 3.4 is [ck]ool!\nThank George for your review.\n\nIn meanwhile, people interested in Scientific and Professional Applications enjoy in a new mailing list called \"KDE-Science\".\n\nTopic:\nDiscuss ways to share code, knowledge, ideas.\n\nArguments:\n- \"components\" to link R, Octave, ATLAS lib and QT/KDE apps;\n- \"components\" to plot many scientific graphs (eg. boxplot, ...);\n\nhttps://mail.kde.org/mailman/listinfo/kde-science\n\nEnjoy."
    author: "Daniele"
  - subject: "KDE 3.4 Alpha 1"
    date: 2004-12-03
    body: "I know it's usually not wise to put Alphas onto \"live\" machines, but does anyone know how stable 3.4A1 is - would it be a bad idea to put it onto my main machine for day-to-day use?\n\nPhil."
    author: "Phil Rigby"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-03
    body: "Depends. Do you run windows at all? If you do, you're already running alpha-quality software so you may as well. :-p"
    author: "Aaron Krill"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-03
    body: "Ooooooooooh, put those claws away!!! :-)\n\nI love KDE, have done since I first started using it (KDE 2 some version).  I find 331 to be rock solid, I'm dying to look at 3.4 but don't want all kinds of stability problems... if the general feeling is good for it, albeit an alpha, I'll try it but I don't want to regret putting it on if you know what I mean.\n\nPhil."
    author: "Phil Rigby"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-03
    body: "In the past i've run alphas and never had any major issues. Been doing that since 3.0 alpha 1. Heh. You could always install KDE 3.4 to a separate directory, and as a different user than you usually use. That way you can try it out without the chance of messing up your stable KDE 3.3 data."
    author: "Aaron Krill"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-03
    body: "Yes that's a good point, I wasn't sure if there were any ramifications of having 2 seperate KDE installs on (even if they are in seperate directories).\n\nPhil."
    author: "Phil Rigby"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-03
    body: "I installed the 3.3 alpha when it came out using Konstruct.\n\nInstalled great in its own directory so I could run it, drool over the new features, and safely return to my KDE 3.2 install.\n\n"
    author: "Leo S"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-04
    body: "Only one small thing, if you intent to alternate between the two versions using the same user you need to set up separate .kde directory's for the different versions. To guard against differences in the configuration files, which may give strange problems or instability."
    author: "Morty"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-04
    body: "Thanks Morty - how do I go about setting up another .kde dir?  I thought KDE just defaulted to .kde.\n\nPhil."
    author: "Phil Rigby"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-04
    body: "The directory can be set by an environment variable KDEHOME, I believe it's called. Just set it to point to wherever you want the config dir to be, in the appropriate loginscript for your system."
    author: "Morty"
  - subject: "Re: KDE 3.4 Alpha 1"
    date: 2004-12-04
    body: "Thanks Morty... can't wait for the Alpha to be released now!\n\nPhil."
    author: "Phil Rigby"
---
In his column "<a href="http://osdir.com/News+index-catid-206.phtml">KDE: From the Source</a>" at O'Reilly's <a href="http://osdir.com/">OSDir.com</a> George Staikos details in <a href="http://osdir.com/Article2722.phtml">the current issue</a> what is to be expected from the <a href="http://developer.kde.org/development-versions/kde-3.4-release-plan.html">upcoming 3.4 version of KDE</a> and includes a few screenshots. KDE 3.4 Alpha 1 is planned to be published within the next days.


<!--break-->
