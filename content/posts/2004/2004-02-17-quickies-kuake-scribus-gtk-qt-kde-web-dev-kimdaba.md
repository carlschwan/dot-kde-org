---
title: "Quickies: Kuake, Scribus, GTK-Qt, KDE Web Dev, KimDaba"
date:    2004-02-17
authors:
  - "fmous"
slug:    quickies-kuake-scribus-gtk-qt-kde-web-dev-kimdaba
comments:
  - subject: "QT-GTK and "
    date: 2004-02-17
    body: "The QT-GTK approach is for me a bright hack, but wouldn't be a better solution to have a proper Qt engine, a proper GTK engine, and some common THEMES definitions (in XML, s-expressions (simpler, better I think) or whatever floats your boat) ?\n\nThus, Gnome has a pure GTK desktop, KDE has a pure Qt/KDElibs desktop, whatever other widget set can use it and themers can use whatever they like and produce just themes that will render identical on each platform, only differing in the underlying technology ?\n\nfreedesktop could be using it as a specification, not an implementation.\n\nIs there something (besides \"just do it\") that i'm missing ? Sure, you wouldn't have the complete set of features, but couldn't a common denominator be found ?"
    author: "AC"
  - subject: "Re: QT-GTK and "
    date: 2004-02-17
    body: "Good luck on getting everybody to agree on something.  I think Qt-GTK theme engine is a good idea (and works well from my experience).  It doesn't resolve the integration issues but it solves a lot of the look 'n feel ones (besides things such as icons, etc.).  If it's shown that GTK+ apps can integrate better in the KDE environment than KDE apps in a GNOME environment (since both have their winning applications, although KDE is replacing them with each release) then perhaps vendors/distributors/users will all get on the KDE bandwagon."
    author: "Jay"
  - subject: "Re: QT-GTK and "
    date: 2004-02-17
    body: "I think Qt-GTK is a much better approach as it doesn't require yet another redesign of the theming engine, or two identical themes (like Bluecurve).\n \nQt-GTK is one piece of the puzzle leading to desktop unification. It allows GTK applications to look native under KDE. If we could have them use the same icons, we'd be almost there.\n \nAdd to this the proposed migration of GNOME to the DCOP-based DBUS in the future and the talks about migrating from aRts (which I've never really liked) to GStreamer, the unified Qt/GTK event loop allowing GTK apps to use KDE's dialogues, and the fact that KPrinter is available to all apps, and in the future there will be one desktop (KDE), and a choice between Qt and GTK as toolkits.\n \nI see this as a positive thing. People who want to develop proprietary apps using Qt (with all the advantages this brings) will pay for a license, and those who are more stingy can use GTK. The user doesn't see a problem."
    author: "dimby"
  - subject: "Re: QT-GTK and "
    date: 2004-02-17
    body: "It all somewhat makes sense until you say in the future there will be one desktop. Are you the one who is going to decide?\n\nMany of the advantages of Qt can and will be made irrelevant by Mono anyway. I thnk a common theme spec more solves the problem than this hack anyway. A theme spec can be used by everyone, including toolkits like Fox, wxWindows, FLTK and so on.\n\nOne little change to GTK could render the hack useless. See what happens to Qt on the Mac. They have to update the look for each new version of OSX. If they implemented the proper stuff, there would not be this problem.\n\nBesides, part of the look and feel is in the other stuff like spacing of words in Menus and so on. How do you proposed to get that fixed. "
    author: "Maynard"
  - subject: "Re: QT-GTK and "
    date: 2004-02-18
    body: "\"Many of the advantages of Qt can and will be made irrelevant by Mono anyway.\"\n\nFunny! Mono is years away from being a complete environment that can be used. Qt is here now. There are also many issues regarding the use of an Intermediate Runtime that Microsoft are just starting to face - Qt is natively compiled. Mono, as a whole, is fairly pointless.\n\n\"A theme spec can be used by everyone, including toolkits like Fox, wxWindows, FLTK and so on.\"\n\nIt takes more than a theme spec. There have to be equivalent decisions made for each toolkit - radio buttons, file dialogues etc.\n\n\"One little change to GTK could render the hack useless.\"\n\nNot really. GTK isn't going to change completely overnight.\n\n\"See what happens to Qt on the Mac. They have to update the look for each new version of OSX. If they implemented the proper stuff, there would not be this problem.\"\n\nWell the Mac environment is closed, and Qt is a cross-platform toolkit, so look-and-feel issues are bound to happen. KDE is different from Qt.\n\n\"Besides, part of the look and feel is in the other stuff like spacing of words in Menus and so on. How do you proposed to get that fixed.\"\n\nWell, I'd like to know that as well."
    author: "David"
  - subject: "Re: QT-GTK and "
    date: 2004-02-18
    body: "> Besides, part of the look and feel is in the other stuff like spacing of\n> words in Menus and so on. How do you proposed to get that fixed. \n\nbad example. TT has introduced style hints for menu spacing in Qt 3.3. here's a better example: part of the look 'n feel is the ordering of buttons on dialogs. UNIX used to be relatively consistent in this regard until the GNOME usability people decided in all their wisdom to switch the order of the buttons. the usability win is absolutely non-existent in practice and now we have a NEW inconsistency that is much harder to reconcile. it was a completely unreasonable change on their part, but now we have to deal with it. and themes aren't able to fix it.\n\na common set of UI guidelines would, but the chances of that happening are as good as me becoming the next King of Neptune. and that's not for a lack of trying (the UI guidelines, not setting up a new monarchy on the outer planets), but for a lack of interest. there was interest on the KDE side; Tink (i think.. or was it Lauri? eek!) even did a bunch of work prepping KDE materials. =("
    author: "Aaron J. Seigo"
  - subject: "Re: QT-GTK and "
    date: 2004-02-22
    body: "\"TT has introduced style hints for menu spacing in Qt 3.3.\"\n\nMmm. That's quite interesting Aaron. Any other development on this front regarding look and feel with Qt 3.3? I actually have it compiled and up and running with KDE 3.2 now :). The only problem I seem to have had was some bizarre KMenu spacing issues.\n\n\"UNIX used to be relatively consistent in this regard until the GNOME usability people decided in all their wisdom to switch the order of the buttons. the usability win is absolutely non-existent in practice and now we have a NEW inconsistency that is much harder to reconcile. it was a completely unreasonable change on their part, but now we have to deal with it. and themes aren't able to fix it.\"\n\nWell, I'm afraid that is Gnome's problem. I think QtGTK is a good idea but no one can go completely re-engineering Gnome apps and other Gnome specific stuff to get this working. No one has to deal with it, although I get the impression that people at Suse have to find some way of integrating with Ximian and their tools. I think this will hurt them to be honest. Some people around Gnome wanted a way to try and attack KDE, and look and feel and usability seemed good ways to do it. I agree that themes aren't able to fix all this, but then that may have been the point :).\n\nSwitching the order of buttons and inheriting a lot of Mac specific stuff has doomed Gnome to failure. You put that down in front of an average business computer user and they will run a mile."
    author: "David"
  - subject: "Re: QT-GTK and "
    date: 2004-02-17
    body: "It hasn't been done because no one's ever done it. As simple as that. I do not see any insurmountable obstacles to writing a common theme format. However there are still many obstacles to overcome.\n\n1) A widget style is 99% detail. Qt provides control over a different set of details than GTK+. While there is a lot of overlap, there are a lot of points that are going to be hard to reconcile. You can get two themes that are significantly similar, but they won't be pixel-to-pixel identical.\n\n2) A theme \"definition\" pretty much limits the theme to non-programmatic drawing. In other words, pixmap-based styles only. While easy to create, and allowing non-programmers to create styles, they have significant drawbacks over style \"engines\".\n\n3) A common style engine necessitates a common API. You would need two separate theme engines for both GTK+ and Qt that exported this API, so the same plugin style would work for both. The disadvantage is that you now have a new API to learn. The non-programmer would not be able to create a new style."
    author: "David Johnson"
  - subject: "Re: QT-GTK and "
    date: 2004-02-17
    body: "This would be *really* hard to do. There are two ways you could get a common theme format without involving code:\n\n- Vector graphics UI: The problem with this is two fold. First, until we get FD.O and its OpenGL-accelerated Cairo goodness, a vector-based UI will be slow. Second, resolutions are not yet high enough that we can go completely vector-based. Therefore, we'd need some sort of delta-hinting mechanism like TrueType has, to do pixel-accurate adjustments. The whole spec would be very complex, and themes would get a lot more complex to write.\n\n- Pixmap-based UI: The problem with this is also two-fold. First, pixmaps themes are slow and wasteful of memory. Second, they are very limiting --- you can alter the look easily, but the behavior is pretty tightly constrainted to what's in the spec.\n"
    author: "Rayiner Hashem"
  - subject: "ehemm"
    date: 2004-02-17
    body: "Kimdaba:I remeber Dos-Formatprogramms and floppy copy programs. They disappeared. Today it is simply intergrated in your operating system. Same applies for Kimdaba, K3B ecc. Do we really need specialized applications? \nModular programming: given the fact that programs die what parts can be provided by kimdaba that may be useful without the application such as image filters?\n\nScribus: Like other qt apps such as Hbasic they always look a bit outdated as the qt engine is slightly different from KDE. What can be done to improve qt-KDE integration? given the fact that the gap is closed between GTK and qt. "
    author: "Holler"
  - subject: "kuake on all desktops"
    date: 2004-02-17
    body: "kuake is really nice, \nI added the line\n<pre> \n...// Correct calls to the window manager \n...KWin::setActiveWindow (winId());    \n...KWin::setState (winId(), NET::StaysOnTop | NET::Sticky | NET::SkipTaskbar);  \n+  KWin::setOnAllDesktops (winId(), true); \n\nto have it on all Desktops and set a global shortcut, it really rocks.\n\nI miss some tabs now! Or I'm blind:)\n\nCan I get a similar behaviour with konsole or another tool? "
    author: "gogo"
  - subject: "kuake on all desktops (more)"
    date: 2004-02-17
    body: "A KToggleAction in the \"right click\" menu would also be nice for \"To all Desktops\". "
    author: "gogo"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "I agree, kuake is pretty cool, but I too miss the tabs. If it had those it would be wonderful. :-)"
    author: "Gagnefs Tr\u00e4skodansarf\u00f6rening"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "Where to put this (which file?)\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "In kuake/src/kuake.cpp..."
    author: "gogo"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "thanks, works now :)\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "You don't need a source change for that.\nYou can just open Kuake and ALT+F3 will open the window menu, which has an option for \"on all desktops\"\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "lol, thx, forget about that:P"
    author: "gogo"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "When I first installed Kuake I also changed the code to achieve this ;)\nI found the easier way \"by accident\""
    author: "Kevin Krammer"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "Since I've never played Quake (not even sure I've ever seen it being played), I have no idea what Kuake does.  Something to do with the window size or something along those lines?   Can somebody explain to the non-gamers out here?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "quake shows a konsole scrolling down from the top of your screen by pressing the button unter ESC, and if you want to hide it, press it again.\n"
    author: "chris"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-17
    body: "but i use ESC to switch modes in vi!"
    author: "alfons"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-18
    body: "He's talking about left tic, \" ` \".  Which is used to escape sub shell expressions in bash.  Although I often use the more traditional $( expr ) instead, I still use backtick fairly often.  \n\nI wonder if it will pull it down when you're entering an accented vowel?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: kuake on all desktops"
    date: 2004-02-18
    body: "This is probably the first time I have found my non-english keyboard to have an advantage (apart from being able to type non-english characters, of course ;-)). On my keyboard, the key under esc is \"\u00a7\", which of course is almost useless. :-)"
    author: "Gagnefs Tr\u00e4skodansarf\u00f6rening"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-18
    body: "I think I got it working with the (otherwise redundant) windows key once."
    author: "Greatred"
  - subject: "Re: kuake on all desktops"
    date: 2004-02-19
    body: "I've added a package to Mandrake cooker contrib for Kuake, that includes this patch.\nSomeone (with an account) may wish to a entry for Kuake to kde-apps.org.\nDoes anyone know if there is cvs repo for kuake?"
    author: "Anon"
  - subject: "combination of imgSeek and KimDaBa"
    date: 2004-02-17
    body: "It would be really cool to intergrate imgSeek (http://imgseek.sourceforge.net) in KimDaBa. That would make it a really great tool."
    author: "Patrick"
  - subject: "Quanta"
    date: 2004-02-17
    body: "So, in the cvs source tree, quanta is now moved into the ./kdewebdev/quanta and ./quanta is depcrecated? "
    author: "Jose Hernandez"
  - subject: "Re: Quanta"
    date: 2004-02-17
    body: "Generally yes, but with some clarification:\n- kdewebdev/quanta contains less than the old \"quanta\", as some programs were moved to the first level of kdewebdev. So get the whole kdewebdev if you want what the quanta module was before\n- the old quanta module is still alive, but only holds the 3.2.x versions (thus it's the \"stable\" Quanta).\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Quanta"
    date: 2004-02-17
    body: "Hmmm? not sure that's completely clear...\n\nkdewebdev is the development branch which can currently be run on KDE 3.1x and up. Stable branches, as Andras said, remain in the quanta module.\n\nWhat you see as Quanta includes kommander as well as Kommander dialogs (quick Start, TemplateMagic, XML tools, etc...) and also KFileReplace, KXSL Debug and more. Of that kdewebdev/quanta only includes the Kommander dialogs, not the executor or editor.\n\nkdewebdev has been a restructuring so that separate applications like KImageMapEditor are separate programs in the module, and they also function as kpart plug ins with Quanta. It seemed better organized and more consistent with KDE. Now if you want to get just one of the apps like Kommander or Quanta it's easier to do.\n\nI hope this helps. BTW there are some very cool additions going on in Kommander lately too!\n\nEnjoy!"
    author: "Eric Laffoon"
  - subject: "Re: Quanta"
    date: 2004-02-18
    body: "will other modules be added so it becomes a webdeveloper package?"
    author: "Holler"
  - subject: "Re: Quanta"
    date: 2004-02-18
    body: "> will other modules be added so it becomes a webdeveloper package?\n\nThere's two answers to that... (assuming you meant programs as the whole package is a CVS module)\n\n1) Yes other programs will be added as we encounter or build programs that make sense here. For instance we recently added KImageMapEditor. If there is a really good KDE program we don't know about please tell me.\n\n2) Hey! Whaddaya mean \"becomes a webdeveloper package\"? Cheese -n- crackers, whaddaya think Quanta is? ;-)\n\nSeriously, Quanta does a lot of things most people aren't aware of and most of what is not in there is not in there because we don't have enough sleep deprived fingers and eyeballs... but if you think it's really missing something say so and maybe someone will be inspired to help us build it."
    author: "Eric Laffoon"
  - subject: "Scribus 1.1.5 has some issues"
    date: 2004-02-17
    body: "Compiling from source left me with a Documentation tree from Scribus 0.9.8--I filed a bug. (Debian Sid)\nMake sure you have python2.3 dev and cups dev to guarantee support.\n\nExport to PDF has some issues I'm trying to verify before filing a new bug.\n\nI'd expect 1.2 to really be refined.  The new additions do make 1.1.5 more responsive and the font management keeps getting smoother.  There are other nice amenities included in the Release Notes."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Scribus 1.1.5 has some issues"
    date: 2004-02-21
    body: "This is probably a local system or packaging issue. Note, debian Scribus packages are in the process of being overhauled. More info is available on IRC."
    author: "scribusdocs"
  - subject: "perl hack for kuake"
    date: 2004-02-17
    body: "Here is a perl script that does something similar to kuake, except that it uses konsole. It shows/hides a konsole window at the top of the screen.\nGo to kmenuedit, add an entry called something like ShowHideKonsole, which executes this perl script as the command, and bind it to whatever key you like 9all within  kmenuedit). \nNow you have a popup konsole, i.e. tabs etc.\n\n------\n#!/usr/bin/perl\n$geometry = \"1280 500\";\n\n$started = 0 ;\n$kon = \"\" ;\nwhile( $kon eq \"\" ) {\n  open(DCOP,\"dcop |\") ;\n  while(<DCOP>) {\n    chop;\n    if( /konsole-(\\d+)/ ) {\n      $kon = $1 ;\n      last ;\n    }\n  }\n  \n  \n  close(DCOP) ;\n  if( $kon eq \"\" ) {\n    if( $started == 0 ) {\n      system(\"konsole &\") ;\n      sleep 1 ;\n      $started = 1 ;\n    }\n  }\n}\n\n$com = \"dcop konsole-$kon konsole-mainwindow#1 \" ;\n\nif( $started > 0 ) {\n  system(\"$com setGeometry 0 0 $geometry\") ;\n  system(\"$com raise\") ;\n} else {\n  open( DCOP, \"$com visible|\" ) ;\n  $visible = <DCOP> ;\n  chop($visible) ;\n  if( $visible eq \"true\" ) {\n    system(\"$com hide\") ;\n  } else {\n    system(\"$com setGeometry 0 0 $geometry\") ;\n    system(\"$com show\") ;\n  }\n}\n"
    author: "Michael"
  - subject: "Re: perl hack for kuake"
    date: 2004-02-17
    body: "I have sth. similar for ruby. \nThe problem is konsole doesn't come up at the top, \nso I don't have a focus on it can't type direclty), \nand I can't find a method to achieve this:(\n\nWhen you open kuake with a shortcut, you can type directly in the console.\n\n"
    author: "gogo"
  - subject: "Re: perl hack for kuake"
    date: 2004-02-18
    body: "Nice hack.  I like it!  However, on my desktop when I hit the short cut for that Perl script, I get an ugly taskbar entry with the Name of the menu item.  Unfortunately it doesn't disappear when the program has finished, but only after it times out (30 seconds or so).  Any idea on fixing that?\n\nCiao,\nViktor"
    author: "he-sk"
  - subject: "Doubt on Qt-Gtk implementation"
    date: 2004-02-18
    body: "I heard it draws to a pixmap using Qt then blits it onto a gtk widget...\n\nWouldn\u00b4t it be faster to write a QPaintDevice that draws into a gdk graphic context? It would be somewhat harder (and it is one of the few areas of Qt that is not well documented) but it is not impossible.\n\nThe current way (if that is how it\u00b4s done :-) uses more memory and should be more awkward."
    author: "Roberto Alsina"
---
<A href="http://www.unixreview.com/">Unixreview.com</A> has a <a href="http://www.unixreview.com/documents/s=8989/ur0402f/">nice article</a> about <A href="http://www.nemohackers.org/kuake.php">Kuake</A>, a Konsole application with the look and feel of that in the Quake engine.

The <a href="http://ahnews.music.salford.ac.uk/scribus/">Scribus</a> team 
<A href="http://ahnews.music.salford.ac.uk/scribus/modules.php?op=modload&name=News&file=article&sid=40&mode=thread&order=0&thold=0">announced Scribus 1.1.5</A>, a layout program made with the <A href="http://www.trolltech.com/products/qt/index.html">Qt toolkit</A>. This will be the last development release before the next stable release Scribus 1.2.

A new version of the <A href="http://www.kde-look.org/content/show.php?content=9714">GTK-Qt Theme Engine</A> has been released and made its way to <A href="http://www.freedesktop.org/Software/gtk-qt">freedesktop.org</A>.

It seems that not only a <A href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdewebdev/">new CVS module</A> 
is created to foster all those nice web development tools in KDE, but also a 
<A href="http://kdewebdev.org/">new website</A> has been registered for this purpose. 

Not too long ago we covered the <A href="http://dot.kde.org/1070419373/">announcement of version 1.0</A> of 
<A href="http://ktown.kde.org/kimdaba/">KimDaBa</A> and now KimDaba 1.1 
has been released. This new version includes lots of new features plus a large amount of optimizations, which makes it useful for image sets with even tens of thousands of images. Try the <A href="http://ktown.kde.org/kimdaba/tour.html">KimDaba Feature Overview Tour</A> for a quick overview of features.


<!--break-->
