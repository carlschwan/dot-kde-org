---
title: "Matthias Ettrich talks about KDE and aKademy"
date:    2004-08-08
authors:
  - "ateam"
slug:    matthias-ettrich-talks-about-kde-and-akademy
comments:
  - subject: "major distros?"
    date: 2004-08-08
    body: "There is a rumor that one major distro (SuSE) will be shifting to GNOME, and another that Novell will be having two items, the SuSE Linux Desktop and the Novell Linux Desktop. A question should have been asked to ME about what he thinks about the potential shift. I know that he'd probably not want t speculate, but as a programmer, I am sure he understands what I mean when users want to see solutions to [potential] scenarios.\n\nCb.."
    author: "charles"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "Look out for an interview I'll be publishing soon with Waldo Bastian, a SuSE employee :-)"
    author: "Tom"
  - subject: "Re: major distros?"
    date: 2004-08-09
    body: "Is this going to be on the dot, or elsewhere?\n\nthanks,\nJason"
    author: "LMCBoy"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "SUSE does not shift to Gnome. They are using KDE for SUSE Linux 9.2 by default and will do so in the future as well\n\nThe Novell Linux Desktop (which is targeted at business users while SUSE Linux Personal and Professional will be targeted at the common user) might be released next year and will let you choose between KDE and Gnome."
    author: "max"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "Good idea.\n\nI like choices. It is either between distros or it is between desktops. "
    author: "a.c."
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "While choice may be a great thing for the common nerd, not too many corporations appreciate it. It just makes matters more complex, and thus, more expensive."
    author: "huu"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "What Novell want to do here is give corporations a look at what is on offer, and make a decision based on a variety of needs."
    author: "David"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "of course. however not every corporation chooses the same technologies for various reasons (some good, some not so good). because an OS is not a final product for one specific person, group or company, the whole \"companies will only use one thing internally anyways\" argument is bunk as a reason to limit choice as to what is available. \n\nas long as interoperability and Freedom are maintained at the highest levels, then allowing entities to choose what works best for them (and then stick to it for as long as they wish) is a great selling point. it offers \"best fit\" scenarios, built-in (friendly) competition and a failsafe (if one desktop stops meeting our needs......)\n\na good analogy may be gleaned from asking yourself why there are so many different types of vehicles on the road (sedan, coup, SUV, pickup truck, semi, etc)."
    author: "Aaron J. Seigo"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "I really don't think that they going to do that. Though SuSE allows to run both GNOME and KDE it is mainly a KDE distribution. I would assume that at least 90% of the SuSE Linux users are running KDE on their desktop and I'm pretty sure that most of them don't want to miss it. So if Novell would force SuSE to switch from KDE to Ximian GNOME as the default desktop choice they would risk to loose a significant portion of their customers. On the other hand Novell not only bought the KDE company SuSE they also bought Ximian. But if you look a little closer then you'll notice that they've bought Ximian for about 10 or 20 million dollars and SuSE for about 220 million dollars. Therefore, risking the larger investment would be pretty stupid."
    author: "Anon"
  - subject: "Re: major distros?"
    date: 2004-08-08
    body: "I'm afraid that there are politically opposed forces within Novell who are trying to tell everyone the way things are going to be (even in direct conflict with existing product lines, and even their own Vice Chairrman). I have a feeling they're going to be rather embarrassed."
    author: "David"
  - subject: "Java, jvm, jni"
    date: 2004-08-08
    body: "\"Still it would be nice to take advantage of JIT-compiled bytecode where it makes sense, and have the two worlds interoperate. Currently there are two technical options: integrating Mono and the CLR, or going for a Java Virtual Machine. Mono at present has several advantages: First, there is no free JIT-compiling JVM that is equally actively developed and it doesn't look like there will be one. Second, cooperating with Miguel and the Ximian group at Novell is probably a lot easier than cooperating with Sun. And third, it is easier to integrate native C++ code with the CLR than going through the JNI.\"\n\nMiguel is so bloody good with his marketing, that he completely bamboozles evenpeople like Matthias. There is development as active as that on Mono on a free jvm, a free Java class library. You can, right now, use Java to code Qt applications, compile them to native code and _still_ have the advantage of a garbage collector that manages your memory for you. JNI is a crutch, but CNI is a great thing. GCJ, GIJ, Classpath and CNI are truly free software, and are at least as stable and useful as Mono. Dash it -- every time Miguel prances about showing off Eclipse, what he shows is ikvm and classpath. \n\nFree java on the desktop is ready _now_ -- or at least, more ready than Mono. The only reason people don't know about it is that a) you don't need it if you have Qt and C++ (Java is nicer, but the difference isn't killing. C++ and Qt together is easy and productive enough) and that b) The gcj/classpath people are really bad at marketing."
    author: "Boudewijn Rempt"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-08
    body: "Though I definitely like to work with Qt and C++ I will now have to use Java and Eclipse for my next programming job. I started using Eclipse a just a few weeks ago and I have to say that it indeed has a few very nice features. The only thing that is annoying me is the it looks ugly and feels slow and kind of alien in an KDE environment. I think that this is mainly the result of using of using GTK as underlying toolkit for Eclipse. So my quesion is, do you know if there is effort under way to implement the SWT Interface with the Qt or KDE libraries?   "
    author: "Anonymous"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-08
    body: "Anything IBM looks ugly and feels slow. Have you ever tried any of the Visual Age or Visual Studio stuff by IBM? It's not pretty, and is slower than molasses flowing uphill in the winter."
    author: "Me"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-08
    body: "Not really -- Eclipse with Qt, I mean. On the other hand, Redhat has a natively compiled Eclipse 2 that is rumoured to be as snappy as any other GTK application. (Which doesn't impress me all that much, but then, my main beef with Eclipse is that I have to do a lot of work to free the editor from the surrounding blurb.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-08
    body: "Hmm, too bad, because a Qt version of SWT would surely also be a nice way to write (simple) applications that work and look nice under KDE and Windows. \n\nP.S.: Thank you for your work on Krita. I'm really looking forward to have a Paint application with a sane UI under Linux. :-) "
    author: "Anonymous"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-09
    body: "\"Hmm, too bad, because a Qt version of SWT would surely also be a nice way to write (simple) applications that work and look nice under KDE and Windows.\"\n\nIBM's CPL license isn't compatible with the GPL, so that isn't possible. \n\nI think the Qt api rendered via the QtJava bindings, is more complete and elegant than SWT - so why do we need SWT? The QtJava development and marketing budget is obviously a bit smaller than IBM's."
    author: "Richard Dale"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-09
    body: "Hi Richard,\nI'd like to write small Java application and it would be really nice to have a native looking user interface under KDE. However, it is also important that this application works under windows. Does that mean that I have to write the user interface twice, once in Swing and once in Qt Java?\nAnother question is, is Qt Java still maintained and will it work with the newest KDE and Qt Versions? The readme file under http://developer.kde.org/language-bindings/java/qtjava-readme.html says something like this: \"Here are Java JNI based api bindings for Qt 2.2.4.\"\n\nThank you"
    author: "Luke"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-09
    body: "\"I'd like to write small Java application and it would be really nice to have a native looking user interface under KDE. However, it is also important that this application works under windows. Does that mean that I have to write the user interface twice, once in Swing and once in Qt Java?\"\n\nI don't have a windows development environment, but as far as I know QtJava works perfectly fine with very little change on windows (and Mac OS X too). I don't really understand Qt/Windows licensing issues too well - if you distribute your small app, you might have to include the QtJava sources to comply with the GPL.\n\n\"Another question is, is Qt Java still maintained and will it work with the newest KDE and Qt Versions? The readme file under http://developer.kde.org/language-bindings/java/qtjava-readme.html says something like this: \n\nHere are Java JNI based api bindings for Qt 2.2.4.\"\n\nWell developer.kde.org/language-bindings/java doesn't sound as though it is being maintained too well, but the bindings themselves are in good shape. If anyone fancies sorting out the docs on developer.kde.org/language-bindings please go ahead.."
    author: "Richard Dale"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-09
    body: "\"I don't have a windows development environment, but as far as I know QtJava works perfectly fine with very little change on windows (and Mac OS X too).\"\n\nWow, that sounds cool, but I guess that would have to buy a Qt Version for Windows. Still, that would be an option. \n\n\"I don't really understand Qt/Windows licensing issues too well - if you distribute your small app, you might have to include the QtJava sources to comply with the GPL.\"\n\nWith a bought version of Qt which I guess is not under GPL, do you know whether I would have to include the source of my program? I mean is the code of qtjava completely GPL or is there a way to use qtjava under the same conditions as the commercial Qt Version? If not, then you could possibly try to sell qtjava to Trolltech. This way it could stay GPL under Linux but could be boundled with Qt under a commercial license for Windows? But that's just a thought.\n\nRegarding SWT do you really think that there is no way to combine the CPL and the GPL in any way? Because I would think that QtJava would be a pretty solid foundation for a Qt implementation of the SWT. May be it could be done the same way as NVIDIA integrated their driver into the Linux kernel. I still think having a native looking version of Eclipse would be a real gain for the KDE environment. \n\nFinally, I managed to test some of the demo apps that are in the java-bindings source file ( I couldn't find them on my computer even though I had java-bindings package installed.). A problem that I had with these demos was that I had to set the LD_LIBRARY_PATH. I thought that this would not be necessary since the libqtjava.so is installed in the /opt/kde3/lib dir. But it seems it is necessary.\n\nAnyways, it works now very nicely and I guess I'll use it.\nGreat work!"
    author: "Luke"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-10
    body: "\" I mean is the code of qtjava completely GPL or is there a way to use qtjava under the same conditions as the commercial Qt Version? If not, then you could possibly try to sell qtjava to Trolltech. This way it could stay GPL under Linux but could be boundled with Qt under a commercial license for Windows? But that's just a thought.\"\n\nIf there was sufficient demand for a commercial version of QtJava I would happy to dual license it (with Trolltech's permission as normally you can't change from the GPL version of some Qt software to issue a commercial version). But there hasn't been any real demand so far.."
    author: "Richard Dale"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-09
    body: "\"IBM's CPL license isn't compatible with the GPL, so that isn't possible. \"\n\nI always wondered why there was such a hype about SWT when its licence makes it impossible to use it in GPL applications.\n\nAnyway, does anyone know if CPL is also incompatible with QPL?\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-08
    body: "Well, the point of showing Eclipse running on IKVM is to show that our JIT engine is mature enough to run something of the complexity of IKVM and with the complexity of Eclipse running on top of it.\n\nGreat kudos should go to the hackers that develop GNU Classpath, without which\nEclipse on IKVM would not be possible.\n\nMiguel. "
    author: "Miguel de Icaza"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-08
    body: "Um, maybe it would be a good idea then to give these kudo's in public whenever possible. 'Ikvm + classpath == eclipse' running means you're using Java, C#. (Irrelevant aside: I always feel like I'm in Amsterdam when reading about that language -- hiss, see? hash!) Anyway, what I like classpath and gcj for is portable code that compiles to native code and still used garbage collection."
    author: "Boudewijn Rempt"
  - subject: "Re: Java, jvm, jni"
    date: 2004-08-09
    body: "C# isn't about hash, it's about Chash."
    author: "moneymaker"
  - subject: "Transcript of talk?"
    date: 2004-08-08
    body: "> At aKademy he will be talking about how to design intelligent, Qt-style APIs.\n\nThis sounds very, *very* interesting. Will there be a transcript of the talk for those of us who won't be at the aKademy? Thanks! :)"
    author: "Anonymous Coward"
  - subject: "Software Freedom Day"
    date: 2004-08-08
    body: "Will you guys (KDE community) be doing any outreach on Software Freedom Day as well? Globally, we'll be handing out about 10 000 packs containing TheOpenCD and Knoppix (both in special editions) at 18 locations. I still have some materials (printed CD covers + sleeves) available if anyone in Ludwigsburg wants to hand stuff out. Our customised Knoppix CD (3.4) has KDE 3.2 as default, though you would probably want you make a fresh one with KDE 3.3 (Knoppix 3.6?). \n\nSee info about the materials here:\nhttp://www.softwarefreedomday.org/article.php?story=20040726161207293\nand the aKademy wiki page on our site here: \nhttp://softwarefreedomday.org/wiki/index.php/AKademy%2C_Ludwigsburg\n\n- Henrik "
    author: "Henrik"
  - subject: "Native code FUD"
    date: 2004-08-08
    body: "\"Native code is and will be the solid basis of every successful computing platform, simply for its flexibility, its performance, and its low memory consumption.\"\n\nThat's FUD. First of all, in theory there is no difference in performance between native code and managed code. Both are just different forms of expressing code. Every output of a native compiler can be generated by a runtime that uses managed code. And every optimization that a JIT can do is also possible with native code. The only difference is the point at which the native code is generated, but as a JIT can cache it, there is no real problem.\n\nThings are a little bit different if you consider how much work different types of compilers need. Writing a simple JIT compiler is usually more work than writing a simple static compiler. If you want to write a static compiler with a moderate number of optimizations (like gcc), this is less work than a JIT compiler.\n\nHowever, if you write a truly dynamic compiler, doing it with managed code is far easier than with native code, because managed code is easier to work with than native code. Classic native compilers like gcc are static, they just compile the code, one compilation units after the other. After compilation the code won't be changed anymore. The linker does not modify the code either, so it can not inline a library function, leaving out a lot of potential for optimization. Dynamic compilers do not have such boundaries for optimization. All code is equal for them, because all code is stored in the prefered way for optimization.\n\nThere are some optimizations that a static compiler can't do and which are only possible for dynamic compilers. For example the JavaVM checks whether a virtual method is actually overridden by a class. If not, it is treated like a non-virtual method and can enjoy optimizations for non-virtual methods like inlining. Because Java allows loading new classes while the app is running (like gcc's C++ does with dlopen()), it may be necessary to recompile the method as real virtual method while the program is running. A static compiler can't do optimizations like this, it always has to optimize for the worst case.\n\nThat's why, in the end, managed code will win. Or at least if you want the same kind of performance with native code, it will be much more work. If today managed code does not match the speed of native code yet, you can be sure that MS has sufficient resources to make it faster in the long run."
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-08
    body: "\"That's FUD. First of all, in theory there is no difference in performance between native code and managed code.\"\n\nPlease tell me that's some sort of joke. First of all, a managed environment has not been proven simply because major things have not been re-written in one. Once a whole desktop is re-written with a managed environment and it performs acceptably then we'll know. As it is, that hasn't happened and not even Microsoft is going to re-write Windows for the CLR - although of course, there will be 'seamless' interfaces so no one notices :).\n\nAs we have seen with various benchmarks, managed code can be fast. However, once you start running everything through it and have six or seven applications open at the same time, all targetted for managed code, that is somewhat different. That's where Java has been found wanting over the years. Because of garbage collection, and the overhead of the managed environment itself, it is always going to consume more resources than native code.\n\nFor providing an environment for desktop applications that are easier to develop for, easier to debug (potentially) and work with, managed code is certainly a plus. However, in terms of a system as a whole you're just never going to get managed code everywhere (and Microsoft will certainly never achieve it). Managed code will not win, but it will be useful for some tasks."
    author: "David"
  - subject: "Re: Native code FUD"
    date: 2004-08-08
    body: "\"First of all, a managed environment has not been proven simply because major things have not been re-written in one. Once a whole desktop is re-written with a managed environment and it performs acceptably then we'll know.\"\n\nYou don't have to. There's simply not a single optimization that native code can do and managed code can't. (I didn't doubt that implementing a managed code runtime is more work)\n\n\n\"Because of garbage collection, and the overhead of the managed environment itself, it is always going to consume more resources than native code.\"\n\nGargage collection has nothing to do with managed/native code, you can have managed code without (There are C compilers for IL). GC a language issue.\n\n\"For providing an environment for desktop applications that are easier to develop for, easier to debug (potentially) and work with, managed code is certainly a plus.\"\n\nNo, these are no advantages of managed code. I would even doubt that the programmer has to notice the difference. The reason why developing with Java or .Net is nicer than with gcc is just the age and history of make, gcc and the whole unix build system. Advantages of managed code are that it is usually much easier to\n- check whether code is secure / run it in a sandbox\n- run the same binary on multiple CPU architectures\n- manipulate the code (optimize, analyze etc)\n\nNote that all this is possible with native code, it's just more difficult for the developer of the system."
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "> - check whether code is secure / run it in a sandbox\n\nThese are features that must be built in the OS, not in the \nprogramming language or the VM.\n\n> - run the same binary on multiple CPU architectures\n\nThis is made possible by building multiple personalities\nin the CPU. Furthermore, if you can run a JIT compiler you \ncan translate the code from a reference architecture to \nanother on the fly.\n\n> - manipulate the code (optimize, analyze etc)\n\nThis can be done with native code even better.\n\nThere is no need for virtual machines and 'managed code'\nwhen you have the OS and the computing architecture handling \nall this stuff.\n\nPeople should spend more time improving the OS instead of \ntrying to reinvent an OS on top of the OS.\n\n/Gian Filippo."
    author: "Gian Filippo Pinzari"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: ">> - check whether code is secure / run it in a sandbox\n>These are features that must be built in the OS, not in the \n>programming language or the VM.\n\nJava implements it in the VM without any help from the OS... \n\n>> - run the same binary on multiple CPU architectures\n> This is made possible by building multiple personalities\n> in the CPU. Furthermore, if you can run a JIT compiler you \n> can translate the code from a reference architecture to \n> another on the fly.\n\nAs I said, it is possible (WinNT did this to run x86 software on Alphas), it is just less work to translate from an intermediate language like IL or Java's.\n\n> - manipulate the code (optimize, analyze etc)\n> This can be done with native code even better.\n\nI doubt that. It's extremely hard to have any automatic optimizations on assembly code like x86. First of all, assembly has more instructions do deal with, and in x86 they are not exactly nice. With native code you have much more knowledge about the program's intentions. You know which method is virtual (and thus could be converted into a non-virtual), you can easily see where is mutex is acquired (all modifications without this knowledge can be pretty dangerous) and so on. In many cases you probably need to convert the assembly code back to an intermediate language anyway, because it's hard to modify code with a limited number of registers.\n\nAnd then, even if you manage all this, you also have the problem that your code will be very platform specific and is useless on other architectures.\n\n\n>>People should spend more time improving the OS instead of trying to reinvent an OS on top of the OS.<<\n\nBasically I agree with that, but Linux in today's form is pretty useless in this respect. It just lacks infrastructure for too many things that would be needed."
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "Hmmm,\n\n\nEvery line of code, that is run on a system is native.\nOtherwise, it would not run.\nOr am I wrong here?\n\n(I assume that if the cpu contains emulators that the emulated code is native too, as that's in the cpu, so I understand with native, something the cpu understands).\n\nYour JIT compiler compiles the program to native code.\nIt might do it in a different way than a \"static\" compiler.\n\n\nQuote:\n\"And then, even if you manage all this, you also have the problem that your code will be very platform specific and is useless on other architectures.\"\n\nThe same with JIT. When it's compiled, it's platform specific.\n\n\n\nJIT compiling is good for small programs, not for large ones (in my opinion).\nI don't think my PII 233MHz would like \"office\"-size JIT compiled programs.\n\n\nAnd I've heard stories, although I can't confirm them, or know if they are true, that the garbage collector etc... sometimes delete the wrong objects, or think they can delete them.\n\nI've always found that garbage collection is something for people who do not know how to program (personal opinion, although I understand it's faster to code with). I prefer using profiling and debugging tools to analyse my programs."
    author: "tbscope"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"Every line of code, that is run on a system is native. Otherwise, it would not run.\"\n\nThe main difference is in which form the code is distributed, and which kind(s) of code are stored at disk.\nWith native code you distribute and store code that's optimized to run on the CPU with as few modifications as possible (usually you still need to link before actually running).\n'Managed code', byte code, IL and whatever they are called are optimized for other purposes. For example for being easy to translate to a variety of CPU architectures, for being secure (programs can only access their own memory), for being easy to manipulate, for being small, and so on. Not necessary all of them at the same time, depending on the designer's goals.\n\n\n\"The same with JIT. When it's compiled, it's platform specific.\"\n\nYes, but you can rely on having the byte code available. This byte code is the same on every platform, so you can manipulate the program in a platform-independent way, even if the runtime later changes turns it into native code.\n\n\n\"JIT compiling is good for small programs, not for large ones (in my opinion).\nI don't think my PII 233MHz would like \"office\"-size JIT compiled programs.\"\n\nCompilation results can be cached though. "
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "<i>Compilation results can be cached though.</i>\n\nYou've said this a few times, so if it can be cached, why isn't it?  Apparently it's not as easy as you believe.\n\nI think you can argue all you want about how managed code is/can be just as fast or faster than native code, but the fact is: it isn't.  Perhaps it's possible to code a program in Java/C# that's just as fast as the same one in C++, but in practice, this doesn't happen.  Off the top of my head, the non-trivial java apps that I use or have used:\n\nEclipse.   Useable, but still very slow.\nAzureus (java bittorrent client): Painfully slow\nFreenet.  (java) Huge memory hog, takes forever to start up.\nFrost (java freenet client)  Same as above,  almost unuseable on a 256MB RAM machine.\n\nI've had some better experiences with C# apps but still nothing close to native speed.\n\nTheoretically, managed code may be just as fast, but realistically, with today's major platforms, it isn't.  And you can't get around that reality with words."
    author: "Leo Spalteholz"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"I think you can argue all you want about how managed code is/can be just as fast or faster than native code, but the fact is: it isn't.\"\n\nGive it some time. C# is still very new. Java already made a lot of progress from it's early beginnings, but they started with an interpreter and Java is not exactly a language that has been designed with performance in mind. \n\nI never claimed that managed code is faster today. I just complained about the statement that native code will be faster and whatnot for eternity."
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "True.  I misread your original post a bit.\n\nIn any case, I think eventually it will become a moot point.\nIf I can implement features much faster in a mangaged language than in a static one then I can certainly live with the end result being a bit slower.  Current average hardware isn't quite at the point where we can do this everywhere but it will be.  Just like the transition from assembly to C to C++.  The trend is towards languages that allow faster programming at the expense of some speed.\n\nMaybe in 50 years I'll be coding in C##++ on a PIC :) "
    author: "Leo Spalteholz"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "> Java implements it in the VM without any help from the OS... \n\nThat's the problem. It's the wrong place.\n\n> it is just less work to translate from an intermediate language\n\nEvery language is an intermediate language, to some \nextent. It is the VM idea that is useless, not the idea that \nthe code produced by the compiler can be optimized on the \nfly for the target architecture or the target CPU.\n\n> Linux in today's form is pretty useless in this respect\n\nI completely agree. That's sounds like a good reason for \nimproving it.\n\nThe main motivation for building VMs it to run code on a \nwide-spread architecture and OS without writing that code \nfor the specific OS and, most importantly, its API. It is a \ngood motivation, I think, but is not going to last once you \nhave the same code running on that architecture and that\nOS natively.\n\nSoon we'll see Microsoft Windows running native Linux \nprograms and Linux running native Windows programs. The\nplatform that will win will be the platform that offers\nthe better facilities for running those programs securely,\nwith the best scalability and the best manageability. \n\nThere is an alternative, though. Somebody comes with a new \ncomputing architecture that requires programs to be written \nfor a new API or a new programming language. Who controls \nthe API, again, controls the platform. It is not a matter \nof having the best OS anymore. It becomes a matter of having \nthe vast user-base required to impose the API. The idea is \nvery good, it it works.\n\n/Gian Filippo."
    author: "Gian Filippo Pinzari"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "<i>Dynamic compilers do not have such boundaries for optimization. All code is equal for them, because all code is stored in the prefered way for optimization.</i>\nGood static compilers have no such boundries either --- LLVM will do inter-library optimization at link time. Also, java or CLR bytecodes are a terrible representation for optimization. Before doing any optimzation on CLR code, Mono first converts it from the stack-oriented CLR model to a virtual-register oriented SSA form. \n \n<i>For example the JavaVM checks whether a virtual method is actually overridden by a class.</i>\nStatic compilers do this too. For high-performance native compilers for Lisp or Smalltalk, such optimzations are standard operating procedure.\n\n<i>A static compiler can't do optimizations like this, it always has to optimize for the worst case.</i>\nNo it doesn't. The capability to do optimization in the precense of dynamic code has nothing to do with native code and everything to do with having a compiler available at runtime. Static compilers for Lisp can do these sorts of optimizations, even though they generate native code.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"Good static compilers have no such boundries either --- LLVM will do inter-library optimization at link time.\"\n\nHmm? I don't know LLVM very well, but I'd call LLVM a dynamic compiler with managed code (their virtual instruction set). \n\n\"<i>A static compiler can't do optimizations like this, it always has to optimize for the worst case.</i>\n No it doesn't. The capability to do optimization in the precense of dynamic code has nothing to do with native code and everything to do with having a compiler available at runtime.\"\n\nIf they can compile at runtime, I would call them dynamic compilers with managed code. The only difference may be that they don't use an intermediate language, but work directly on the source code."
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"Managed code\" and \"native code\" have rather precise definitions. Managed code is stored as an abstract bytecode, and compiled *on-the-fly* to machine code. Native code is stored as machine code, and executed directly. JIT's and compilers available at runtime blur the lines, but the fundemental distinction is still that JIT's compile on the fly by default, and cache native code as an optimization, while native compilers use native code by default, but make it possible to regenerate that code. Certainly, using the accepted definitions of \"native code compiler\" and \"managed code compiler,\" Java/C# are managed code platforms, while LLVM/CMUCL/etc are native code platforms."
    author: "Rayiner Hashem"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "Performance and memory consumption is not only a matter of the instruction set and how the runtime system executes it, but depends largely on data structures, the object model itself and on what kind of APIs and programming style the programming languages in use imply or encourage. This is not about static versus dynamic compiler optimizations techniques or the benefits of having another layer on top of the CPU's instruction set. This is about comparing how developers today write native applications on both Microsoft Windows and Linux, and how they write Java or .NET applications. So I could have said \"C/C++\" instead of \"native code\".\n\nC++ does not necessarily mean speed, even if the language makes it harder to write slow programs. Just imagine a C++ programming style where all functions and inheritances are virtual, all classes inherit from a common base class that does central object registration, all objects are always allocated on the heap and every single pointer or array access is checked beforehand. If you then create an API that encourages the creation of massive amounts of small objects that call into each other through listener interfaces, and requires dynamically checked type casts to narrow objects, then you would not end up with the speed we see with Qt and KDE today, and for that matter any other well written C/C++ software.\n\nWhat makes C++ code so fast is not only that the CPU understands the assembly directly, at least not exclusively. Most of its speed stems from the powerful language. It's good when a JIT compiler can optimize function calls, but it's better if you don't even need a function call. It's good when an allocator is really fast when allocating small objects, but it's better if you don't have to allocate anything at all. And so on.\n\nThis is why C++ is here to stay, on both Microsoft Windows and on Linux."
    author: "Matthias Ettrich"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"Just imagine a C++ programming style where all functions and inheritances are virtual, all classes inherit from a common base class that does central object registration, all objects are always allocated on the heap and every single pointer or array access is checked beforehand\"\n\nWell I've used Objective-C/OpenStep (ie Apple Cocoa) a lot. It was easily fast enough 10 years ago, and it certainly is now. In Objective-C every method is effectively virtual, including the equivalent of 'static' methods. All classes inherit from NSObject, and the NSArray class doesn't allow you to drop off the end and crash. All Objective-C instances are allocated on the heap.\n\n\"If you then create an API that encourages the creation of massive amounts of small objects that call into each other through listener interfaces, and requires dynamically checked type casts to narrow objects, then you would not end up with the speed we see with Qt and KDE today\"\n\nThis is where Objective-C diverges from the java approach. In Cocoa there are many fewer instances required to do something, and the inheritance heirachies are flatter. You can use delegation or categories (ie dynamically adding/changing methods to running instances) where you would need to subclass in java. Listener interfaces are a design disaster as they ignore the dynamic reflection possibilities in java. Qt takes a static language and makes it more dynamic via the moc and signals/slots, while Swing java takes a dynamic language and implements a clunky api based entirely on static typing.\n\nI'm fussy and I don't like a lot of software, but I really think Qt/KDE is the best application framework since NeXTStep (especially with the KDE 3.3 ruby bindings)."
    author: "Richard Dale"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"All Objective-C instances are allocated on the heap.\"\n\nBTW this is not strictly necessary in Objective-C, and neither in Java or C#. They could use stack-allocation for many short-lived objects. It would require more/better analysis of the code though, to determine for which objects it is possible.\n\n(Similarly all the other performance problems listed by Matthias can be solved by more intelligent compilers - we're just not there yet)"
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"BTW this is not strictly necessary in Objective-C,\"\n\nIt isn't possible in Objective-C - you create an instance by messaging a class object, and then sending an initialization message to the new instance. Only string literals of the form @\"mystring\" can be statically allocated\n\nInstances are allocated in 'autorelease pools'. You can create you're own autorelease pools, and you would do that if you have a lot of short lived objects.\n\nC# already allows to allocate short lived objects on the stack."
    author: "Richard Dale"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "Lot's of compilers are already at that point. Stalin, CMUCL, d2c, Bigoo, etc, all do these sorts of optimizations. It's just something that hasn't come to C# and Java compilers yet (and will never come to C/C++ compilers, because of their semantics)."
    author: "Rayiner Hashem"
  - subject: "Re: Native code FUD"
    date: 2004-08-12
    body: "umm i very much doubt this...\n\nc is here to stay. but c++ i doubt.\ndynamic optimisation is not possible\nwithout a jitting mechanism and a \nlanguage that doesn't overspecify.\ngood jit compiler do inlines of\nthe methods based on execution \nprofiling. the best c++ compiler\ncan only guess. and slowly at\nthat. sure with profile feedback\nit can do much better. but still\nnowhere near perfect.\n\nalso its because of the \noverspecification of interfaces\nin c++ that we don't see automatic\nremoval of virtual keywords based\non profiling / path analysis\n\nsame goes for the need to \nexplicitly use zone allocators\nrather than have the runtime\nfigure it out at er.. yeah\nruntime :)\n\nAlex"
    author: "lypanov"
  - subject: "Re: Native code FUD"
    date: 2004-08-13
    body: "\"c is here to stay. but c++ i doubt.\n dynamic optimisation is not possible\n without a jitting mechanism and a \n language that doesn't overspecify.\n good jit compiler do inlines of\n the methods based on execution \n profiling. the best c++ compiler\n can only guess. and slowly at\n that. sure with profile feedback\n it can do much better. but still\n nowhere near perfect.\"\n\nI think you've lost the plot here. Which is most important - a gui framework based on a more expressive dynamic language such as ruby, or a statically compiled language which is a bit faster at runtime because of jit compilation? And should a toolkit be written in the same language that end users will program it in?\n\nWhich gui toolkits are so awesome that they just need a bit of jit'ing to make them perfect? MFC, Swing, WinForms, Delphi, Taligent, WxWidgets, GTK+, GTK#. To me they're all just a bunch of dogs compared with Cocoa or Qt/KDE. \n\nC# is a complex systems programming language (as a systems programmer I find it fun), but it is most certainly not a RAD language that everyday programmers will feel comfortable with. Ruby doesn't have jit'ing, but does anyone care, it's just much easier to get stuff done with ruby (or python) than C# or java or C++. Why can't we talk about the programming language usability vs. efficiency tradeoff? The Qt toolkit will never be as popular as it should be while it is C++ specific - a jit'ed C# version wouldn't solve that problem at all."
    author: "Richard Dale"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "The other problem with JITing runtimes tends to be memory pressure, which impacts desktop performance far more than CPU overhead these days.\n\nI think most of the advantages of JIT compiled code are bogus. Security is not to me a convincing argument as the only security you gain is via type-checking, which is not fine grained enough for real world security. For instance you can prove the code does not use the File object, but you cannot restrict which files it can access and when (at least, not without seriously hacking the class library sources itself). A worse-is-better approach seems to be an SELinux style one, where security is applied at the process level rather than the type level, but policy is easier to specify and more flexible.\n\nCPU independence - please. The CPU is already abstracted by the compiler, no need to do it twice given the dominance of x86 (on the desktop, it may be more useful on the server).\n\nEasier to examine the code: well, there is no rule that says you cannot have reflective native code, and indeed gcj/java does exactly this.\n\nI don't have a strong opinion on Java vs Mono, but right now it seems they are evenly matched. Mono has the community momentum and nicer APIs (at least for GTK developers) but GCJ has a more traditional toolchain and produces native code that is easily integrated with existing systems. It also seems to have easier integration with native code via the CNI and the upcoming GDirect thing (similar to P/Invoke). I do not know which will \"win\", but I suspect they will both be strong. Right now Mono seems to be in the lead if only because there aren't any \"real\" desktop Java apps outside Eclipse that are in wide use on Linux."
    author: "Mike Hearn"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "I don't have an opinion on this Java vs Mono but somehow, seing the debate reminds me a bit of the java momentum, when java was supposed to replace everything and of the orbit vs dcop/kpart debate too.\n\nWe sure like to have heated discussion with technical arguments..."
    author: "Philippe Fremy"
  - subject: "Re: Native code FUD"
    date: 2004-08-09
    body: "\"The other problem with JITing runtimes tends to be memory pressure, which \nimpacts desktop performance far more than CPU overhead these days.\"\n\nThese problems can easily eliminated by caching the results. The advantage is\nthat you can do best-case optimizations that a static compiler can't afford, - if the worst-case happens a dynamic compiler can still recompile at runtime. But that doesnt mean that it needs to happen frequently.\n\n\n\"I think most of the advantages of JIT compiled code are bogus. Security is not to me a convincing argument as the only security you gain is via type-checking, which is not fine grained enough for real world security. \"\n\nThere are two things that you need for Java-like sandboxing: \n1. you need to integrate security checks in all libraries that communicate with the world outside of the process/VM and should be used by the sandbox'd code\n2. you need to make sure that the only way to access any part of the system is using the public APIs\n\nWith native code, number 2 becomes very hard.\n\n\n\"For instance you can prove the code does not use the File object, but you cannot restrict which files it can access and when (at least, not without seriously hacking the class library sources itself).\"\n\nJava allows this:\nhttp://java.sun.com/j2se/1.4.2/docs/api/java/io/FilePermission.html\n\n\n\"A worse-is-better approach seems to be an SELinux style one, where security is applied at the process level rather than the type level, but policy is easier to specify and more flexible.\"\n\nThis allows only very low-level security. For example Java allows that applets in a sandbox can create windows, but only if the window has a large warning sign. Try enforcing that with SELinux.\n\n\n\"CPU independence - please. The CPU is already abstracted by the compiler, no \nneed to do it twice given the dominance of x86 (on the desktop, it may be \nmore useful on the server).\"\n\nEven if there would be only x86, there are already more than enough variants through extensions. 32-bit and 64-bit, MMX, 3DNow, SSE, SSE2...\n\nManaged code also has the advantage that it will give x86 CPU vendors more room for improvement. Right now they have the problem that even if they come up with a good (low-level) extension, there will hardly any software that makes use of it. \n\n\n\"Easier to examine the code: well, there is no rule that says you cannot have reflective native code, and indeed gcj/java does exactly this.\"\n\nThat's examining at runtime and not what I meant. I was talking about analysing and modifying programs that are stored on the disk in their distribution format."
    author: "Tim Jansen"
  - subject: "Re: Native code FUD"
    date: 2004-08-11
    body: "Yes, you can enforce such windowing policies with SELinux (or more accurately the in-development SE-X). SELinux policy can be propogated up into userspace object managers.\n\nAs to the other points, well maybe but I've not seen much software that would benefit from such on the fly optimization. Algorithmic optimization usually matters far more."
    author: "Mike Hearn"
  - subject: "Transcripts"
    date: 2004-08-09
    body: "I, for one, hope that transcripts from the speeches that are given at the aKademy will be made available (at the Dot? please!) so that the people who can't attend have access to them."
    author: "Mikhail Capone"
  - subject: "Re: Transcripts"
    date: 2004-08-09
    body: "That's not really practical at all as doing transcriptions for 32 hours of talks (the users and devel conference) would easily be several hundred hours of work in transcription and would presume that they're recorded.  In the case that they actually are recorded, it would be a lot easier to just post the audio."
    author: "Scott Wheeler"
  - subject: "Re: Transcripts"
    date: 2004-08-09
    body: "Why not something simple like:\n\n(1) post the slides on wiki.kde.org (one wiki page / talk)\n\n(2) have people in attendence annotate the bottom of the wiki page with their notes and recollections."
    author: "mbucc"
  - subject: "Re: Transcripts"
    date: 2004-08-10
    body: "Because people that have travelled X miles and spent Y dollars to be at aKademy probably don't want to bother doing lots of work for free just so others can save themselves the trip.\n\n(and no I won't be there)"
    author: "Leo S"
  - subject: "Re: Transcripts"
    date: 2004-08-10
    body: "Depending on the effort of volunteers at the conference, we should have lots of reviews and articles coming from the conference, so you should at least get hourly coverage during the event.\n\nAfter the conference, I'm sure we can get lots of slides put up on the web."
    author: "Tom"
  - subject: "Akademy for the public"
    date: 2004-08-09
    body: "hey ! \nplease publish Quicktime movie of conference and all the stuff\n\njust like the Mac World and Steve Jobs keynotes, we users wants to know more and are ready to watch what we miss because we are so far away from the aKademy location"
    author: "somekool"
  - subject: "Re: Akademy for the public"
    date: 2004-08-10
    body: "You will sponsor KDE to be able to do that?"
    author: "Anonymous"
  - subject: "Re: Akademy for the public"
    date: 2004-08-10
    body: "http://www.archive.org/contribute.php sponsors free bandwidth for the movies!"
    author: "Anonymous"
  - subject: "Re: Akademy for the public"
    date: 2004-08-10
    body: "Uhm, bandwidth isn't the issue.  We have that.  We on the other hand don't have digital video recorders and people that know how to use them.  They're not exactly cheap."
    author: "Scott Wheeler"
  - subject: "Re: Akademy for the public"
    date: 2004-08-10
    body: "Ok, does anybody volunteer on \n\n1.) installing a webcam and attach it to the microphone  \n\nand/or\n\n2.) provide a digital video recorder and make use of it :) ?"
    author: "Torsten Rahn"
  - subject: "Re: Akademy for the public"
    date: 2004-08-10
    body: "You will need everything twice because there are two parallel tracks."
    author: "Anonymous"
---
Continuing the series of articles previewing KDE's World Summit, 
<a href="http://conference2004.kde.org/">aKademy</a> (running from August 21st to 29th), Tom 
Chance interviewed Matthias Ettrich, the founder of the KDE project, the creator of the LyX 
document-processor, and an employee of Trolltech. At aKademy he will be talking about 
<a href="http://conference2004.kde.org/cfp-devconf/matthias.ettrich-designingqtstyleapis.php">how 
to design intelligent, Qt-style APIs</a>. I asked him for his thoughts about the status of the 
KDE project, its achievements, and what he is looking forward to in aKademy. You can read the 
previous interview with Nils Magnus of LinuxTag <a href="http://dot.kde.org/1091772577/">here</a>.


<!--break-->
<p><b>Q: We all know you as the founder of KDE, but what is your current role in the project?</b></p>
  
<p>Matthias Ettrich: Today I am very much focused on KDE's underlying technology, the Qt
toolkit. This pretty much is a full-time job, so I'm no longer feeling
bad about not actively contributing code to other parts of KDE
anymore. When you take a step back and recognize how much the KDE team
achieves in relation to its financial backup and the number of
developers, you'll clearly see how important a solid foundation is. We
are an insanely productive development community, and we achieve that
by layering our software stack and investing into the foundation,
instead of constantly reinventing the wheel.</p>
 
<p>It's all about developers
and what developers need to be efficient. Every hour spent on Qt and
the KDE libraries is an hour spent wisely, because an every growing
number of applications benefits from it. So that's what I do.</p>

<p>In addition my Trolltech position allows me to contribute indirectly to
KDE's success: Some of our engineers can do part-time work on KDE, we
sponsor David Faure, and of course we are an aKademy gold sponsor. On
a more personal level I do my share of giving talks and interviews, I
make an effort to bring people together, and I try to actively help
with community events like last year's conference in Nove Hrady and
this year's aKademy.</p>

<p><b>Q: What is your favorite development in the project since you started it?</b></p>

<p>ME: The greatest thing for me is that we managed to grow the project while
keeping its initial culture and soul intact. We started out with a
relatively small group of equals that cooperated purely based on
mutual respect and technical merits. This is pretty standard for small
engineering groups. What makes KDE special, though, is that we managed
to scale this to the overwhelming size the project has today. With KDE
e.V. and its statutes we have found and established a mechanism that
makes sure KDE stays this way: a project owned and controlled by its
active community of individual contributors. Establishing KDE e.V. and
seeing it gaining acceptance within the KDE community was probably the
most important non-technical development that happened, and this
process is far from being over.</p>

<p><b>Q: Almost four years ago [1] you said that in 2005 you'll be a manager due
 to the success of KDE (which "will be a leading desktop platform by then").
 Given that you only have one year left, what are your thoughts on this
 prediction?</b></p>

<p>ME: Well, I have been working as a Director of Software Development for
some time now, so for me it became true already. Luckily my concerns
about being a manager turned out to be exaggerated, managing people is
not as bad as I anticipated it to be. Lesson to be learned: one should
not rely on Dilbert as the only source of information. The obvious
downside is less time for coding, but it comes with a strong upside:
by working through a team you can achieve far more than what you could
do on your own. Just imagine somebody offered you 50 extra hands. And
not only that: each pair of hands came with a brain of its own, each
with extra skills and talents that complete your own. Now, how good
does that sound?</p>

<p>With regards to KDE becoming a leading desktop platform: we are
already, in many areas. We are a leader in terms of active community,
in terms of network integration, in terms of providing freedom and
choice to desktop users, and in terms of providing a sophisticated
development framework for application developers.</p>

<p><b>Q: What do you think the "next big thing" in KDE will be?</b></p>

<p>ME: There is one thing that will become increasingly important in the
future, not just for KDE, but for all of Linux: a convincing answer to
Microsoft's .Net. I'm not concerned about the server, I'm concerned
about the client, and about the belief that some people in the
community share, that you can successfully clone Microsoft's APIs and
then keep up with them. Free software should not be about cloning, but
about creating. If we want to be successful, we need to have our own
APIs. And guess what, we are really good at that. There is no reason
to throw everything away and start all over again from scratch.
Instead we must built upon what we already have, and that is native
code.</p>

<p>Native code is and will be the solid basis of every successful
computing platform, simply for its flexibility, its performance, and
its low memory consumption. With KDE and Qt, it's easy to develop
native code.  Once you get the hang on it, it is easier than
e.g. developing complex applications with Java/Swing.</p>

<p>Still it would be nice to take advantage of JIT-compiled bytecode where it makes
sense, and have the two worlds interoperate. Currently there are two
technical options: integrating Mono and the CLR, or going for a Java
Virtual Machine. Mono at present has several advantages: First, there
is no free JIT-compiling JVM that is equally actively developed and it
doesn't look like there will be one. Second, cooperating with Miguel
and the Ximian group at Novell is probably a lot easier than
cooperating with Sun. And third, it is easier to integrate native
C++ code with the CLR than going through the JNI.</p>

<p><b>Q: What are you looking forward to in aKademy?</b></p>

<p>ME: Meeting people, having fun, and watching KDE improve! Every KDE
conference so far has been a big happy gathering of friends that
kick-started an insane commit rate to the CVS. And there's no reason
why aKademy 2004 should be any different.</p>

<p><b>Q: What do you think people should make a special effort to attend at
 aKademy?</b></p>

<p>ME: There's so many interesting things going on at aKademy, it's hard to
pick just one. But if you are a developer and haven't thought much
about accessibility yet, I suggest you listen to Aaron Leventhal's
opening speech of the Unix Accessibility Forum on Sunday. Assistive
technologies are not only an interesting technical challenge, but an
area where we as a free software project can make a real difference in
many peoples' lifes. For the User and Administrators conference I
suggest you give the groupware and collaboration track some special
attention. Kolab and Kontakt are exciting projects that have not yet
gotten the attention they deserve. And nobody should miss the social
event on Saturday when we celebrate the Freedom Software Day.</p>

<p><b>Q: Thank you for your answers and your time.</p>

<p>ME: My pleasure :)</p>
