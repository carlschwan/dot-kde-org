---
title: "dot.kde.org: We are back!"
date:    2004-06-16
authors:
  - "wbastian"
slug:    dotkdeorg-we-are-back
comments:
  - subject: "Thanks!"
    date: 2004-06-16
    body: "Yes! And btw thank to all who are involved in keeping us informed! In particular big thanks to Navindra Umanee who did so much for dot.kde.org, not sure though if he is still involved?! "
    author: "MK"
  - subject: "Re: Thanks!"
    date: 2004-06-16
    body: "Heh, thanks for the thought.  Still around but my internet access is rather spotty at the moment.  I'm still in the background but letting others do most of the work.  :-)"
    author: "Navindra Umanee"
  - subject: "Congrats..."
    date: 2004-06-16
    body: "... for being back!\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Great to see the dot back!!"
    date: 2004-06-16
    body: "I was really missing this great, indispensable news and community site."
    author: "wilbert"
  - subject: "KDE-Forum.org is back too!!!"
    date: 2004-06-16
    body: "The Dot and KDE-Forum are on the same server so kde-forum.org is back too!!!\n\nBye,\nZeno\nwww.kde-forum.org"
    author: "zenok"
  - subject: "sigh..."
    date: 2004-06-16
    body: "ah. I can breath again."
    author: "illogic-al"
  - subject: "hmmm"
    date: 2004-06-16
    body: "missed my favorite kde-info site ;-)"
    author: "superstoned"
  - subject: "RDF"
    date: 2004-06-16
    body: "Not to be \"that guy\", but any word on the dot.kde RDF feed?"
    author: "Ian Monroe"
  - subject: "Re: RDF"
    date: 2004-06-16
    body: "Seems to be fine currently...  any specifics?"
    author: "Navindra Umanee"
  - subject: "Re: RDF"
    date: 2004-06-16
    body: "Don't know what the parent meant, and can't see how it is right now, but it used to be pretty empty, only containing titles. Not even teasers :-("
    author: "Roberto Alsina"
  - subject: "Re: RDF"
    date: 2004-06-16
    body: "Yea, its working fine now, its coming up completely blank when I tried earlier today. Though it was from IE, so that might have being messing something up.\n\nIt looks like the issue I've actually been having is that http://dot.kde.org/rdf has news from a few weeks ago. I guess the feed was renamed, I fixed it now."
    author: "Ian Monroe"
  - subject: "specifics"
    date: 2004-06-17
    body: "Is the RDF permanently moved to kde.org?  If so, somebody needs to advise CowboyNeal (for Slashdot's dot.kde.org slashbox)."
    author: "Spy Hunter"
  - subject: "Re: specifics"
    date: 2004-06-17
    body: "The old URL is supposed to still work for the sake of backwards compatibility.  I'll take a look."
    author: "Navindra Umanee"
  - subject: "Whew!"
    date: 2004-06-16
    body: "I thought you were lost.  Whew!"
    author: "Steve Mallett"
  - subject: "wiki.kde.org is back, too"
    date: 2004-06-16
    body: "...thanks to some MySQL kung-fu from Christian Mueller."
    author: "Navindra Umanee"
  - subject: "No worries!"
    date: 2004-06-16
    body: "Perfect timing with my harddrive crash, I just got back online yesterday after a couple of weeks offline;)"
    author: "DiCkE"
  - subject: "Re: No worries!"
    date: 2004-06-18
    body: "u lucky basterd! i've been looking at this site every day, but it was down :("
    author: "superstoned"
  - subject: "Welcome Back!"
    date: 2004-06-16
    body: "Goodness me. You can't not have KDE news!"
    author: "David"
  - subject: "Yippie!!!"
    date: 2004-06-16
    body: "dot.kde.org is back.\nWell - you never know what you've got 'til it's gone they say.\nBig thanks to everyone involved.\n"
    author: "Martin"
  - subject: "Where is dot.kde.org?"
    date: 2004-06-18
    body: "Where is dot.kde.org?\nDo you have a link to the site?"
    author: "Clueless Slashdot Loser"
---
You may have noticed that your beloved KDE news site experienced availability problems during the last two weeks. The reason for the outage was a scheduled server movement followed by some unscheduled problems. We hope to have everything under control again and would like to thank <a href="http://pem.levillage.org/">
Pierre-Emmanuel Muller</a> of
<a href="http://www.levillage.org">leVillage</a> for hosting dot.kde.org and for helping us out with the recent problems.


<!--break-->
