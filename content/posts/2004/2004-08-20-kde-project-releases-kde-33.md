---
title: "KDE Project Releases KDE 3.3"
date:    2004-08-20
authors:
  - "skulow"
slug:    kde-project-releases-kde-33
comments:
  - subject: "3.3"
    date: 2004-08-19
    body: "werd..."
    author: "Johnny"
  - subject: "Re: 3.3"
    date: 2004-08-19
    body: "SUSE quietly released their RPMs a couple of days ago.  Everything seems better/nicer/more polished/elegant/stop reading when you get bored reading eulogy.\n\nSlightly off topic - but if anyone is looking for evidence in \"the when are Novell going to dump KDE\" debate, they clearly won't find it on the SUSE website.  SUSE KDE RPM publishing/updating feels more active since Novell."
    author: "gerry"
  - subject: "Re: 3.3"
    date: 2004-08-19
    body: "Just updated my 9.1 through YaST and it's a great job from KDE/SuSE people!!"
    author: "DiCkE"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Can't seem to upgrade to KDE 3.3 via YAST.  Obviously I'm mising something here - thanks for any help.\n\ntf"
    author: "Tom Flaherty"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "To your YaST sources you need to add ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.1/yast-source .  Alternatively, you can use apt4rpm to update."
    author: "Jeremy Erickson"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "How does that work exactly?  Will SuSE eventually update their main YOU servers, or do I need to manually change the sources if I want to use this?  If so, how do I do this?  Sorry, I just switched to SuSE and don't know how they work."
    author: "ac"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "By default SuSE only provides security and bug-fix updates via YOU. They are constantly backporting fixes and what not, so you'll have a completely stable and secure KDE 3.2.\n\nThey don't provide updates for new features or application versions though, this is to prevent maintenance headaches in large organisations when everyone updates to new versions with new issues and new bugs.\n\nFeature updates are available on the SuSE site however, and if you follow the parent's instructions you can access these new updates via YaST2."
    author: "Bryan Feeney"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "In YaST, you can specify multiple software sources.\nJust add the source mentioned above in addition to your primary installation source.\nYaSt will display the available upgrades in the software installation module.\nSelect the ones you want to upgrade by clicking with your RMB on the packagename and select 'upgrade'. You can also upgrade a complete group by selecting [selections] in the combobox on the right, and RMB click on the selection [kde packages]. Choose the option called something like [upgrade packages if available]. "
    author: "anonymous"
  - subject: "Getting 3.3"
    date: 2004-08-21
    body: "http://www.mirror.ac.uk/mirror/ftp.kde.org/pub/kde/stable/3.3/SuSE/ix86/9.1/\n\ndownload the tarball... "
    author: "gerry"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Did you update to provided qt-3.3.3 as well? I'm having tons of problems with it."
    author: "jmk"
  - subject: "Re: 3.3"
    date: 2004-08-26
    body: "HI all, \nI upgraded my SuSE 9.1 box to KDE 3.3 via yast as described above. I have one problem now...openoffice won't start. I get the following:\n\ndaveor@jimbo:~> OOo-writer\nStarting /opt/OpenOffice.org/program/swriter...\nsoffice.bin: kdecore/kcmdlineargs.cpp:159: static void KCmdLineArgs::init(int, char**, const KAboutData*, bool): Assertion `argsList == 0' failed.\nAborted\ndaveor@jimbo:~>    \n\n\nanyone seen this or know what it's about?\n\nthanks for the help!\ndaveor"
    author: "daveor"
  - subject: "Re: 3.3 -- OOo Problem"
    date: 2004-08-27
    body: "I have the same problem. Since upgrading (sic) to KDE 3.3 I haven't been able to start Open Office within KDE. It pops the same accursed message you just posted. OO runs in any other desktop environment perfectly, though.\n\nI am trying the OpenOffice installer today, instead of relying on the Apt updates. I'll post the results here in a while (but I doubt there won't be any problem)\n\nAny hints meanwhile?\n\nelmusafir"
    author: "elmusafir"
  - subject: "Re: 3.3"
    date: 2004-08-27
    body: "As expected.\n\nDeleted OpenOffice_org (rpm -e OpenOffice_org-en  OpenOffice_org-lang  OpenOffice_org-en-help OpenOffice_org) and then went to the openoffice site and downloaded the installer tarball ( http://download.openoffice.org/1.1.2/index.html )\nuntarred/unzipped it (tar zxvf OOo_1.1.2_LinuxIntel_install.tar.gz) and then installed it (~/OOo_1.1.2_LinuxIntel_install/./setup -net)\n\nNever to trust those darned rpms again.\n\n(sorry about the instructions but if someone needs them, they are here!)\n\n;-)\n\nelmusafir"
    author: "elmusafir"
  - subject: "Re: 3.3"
    date: 2004-08-19
    body: "Actually, the packages a few days ago were of RC2.  They were labeled as 3.3 in case that release were deemed good enough to be called final.  They have freshly updated 3.3 final packages just posted today.  Multimedia doesn't seem to have updated packages yet, but that might just be because nothing changed from RC2.  (Just speculation.  Does anybody know for sure?)  Bindings and sdk are also still at 3.2.x\n\nStill, your point about quick updates still stands.  Plus, unlike the upgrade to 3.2 on SuSE 9, there is no conflict between kdebase and kdebase-SuSE."
    author: "Hooded One"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Does anyone else have problems with Plastik not being available after the update on SUSE 9.0? I tried both the kdeartwork3-3.3.0-1.i586.rpm I got via apt-rpm and the .0-3 build from the SUSE ftp server. Both files applied without conflicts, but plastik is not available in KControl anymore. The files are in /opt/kde3/lib... \n\n/opt/kde3/lib/kde3/kwin_plastik_config.la\n/opt/kde3/lib/kde3/kwin_plastik_config.so\n/opt/kde3/lib/kde3/kwin3_plastik.la\n/opt/kde3/lib/kde3/kwin3_plastik.so\n/opt/kde3/lib/kde3/kstyle_plastik_config.la\n/opt/kde3/lib/kde3/kstyle_plastik_config.so\n/opt/kde3/lib/kde3/plugins/styles/plastik.la\n/opt/kde3/lib/kde3/plugins/styles/plastik.so\n/opt/kde3/share/apps/kwin/plastik.desktop\n/opt/kde3/share/apps/kstyle/themes/plastik.themerc\n\nPlastik worked fine in beta 2 (SUSE RPMs again).\n\nAny ideas?"
    author: "Anonymous"
  - subject: "Re: 3.3"
    date: 2004-08-21
    body: "Updated to the latest packages - works fine now."
    author: "Anonymous"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Just a heads up. The SuSE 8.2 RPM that contains kopete (kdenetwork3-InstantMessenger) does noit seem to include the Jabber plugin. The 9.0/9.1 RPMs do.\n\nI'm off to SuSE's site to see if I can figure out how to let them know about this."
    author: "Rob"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Why should they dump KDE?\nQT is superior to GTK+\nKDE ist by far superior to GNOME (hat to work with it for 2 days, it's crap)\n\nI think, is more resonable for Novell to port Evolution to KDE.\nThat would sell, eDirectory and Win/Lin Evolution Client..."
    author: "tracer"
  - subject: "Re: 3.3"
    date: 2005-06-01
    body: "I know my employers would immediately stop buying SuSE if they \"dumped KDE\", and I feel that the idea of there being a serious \"when are Novell going to dump KDE\" debate is nothing more than flamebait.\n\nMost companies I know of that have large GUI-based workflows are not going to \"dump\" any of the principal contenders for end-user productivity, and with the best will in the world, there are Desktop Environments out there that suffer because they don't seem to get used in any such context.\n\nI mean, can you see designers doing any serious GUI-based work in a GUI which doesn't even let them select multiple files in the stock file selection dialog?\n\nThat's if you're charitable and ignore design decisions like \"let's remove Cancel buttons and utterly stop end-users discarding file-screwing changes\" - or \"let's remove OK buttons so that end-users aren't sure whether anything got saved\" - both of which seem determined to force/trick end users into committing bad changes on their machines.\n\n...and that's if you forget about the 'loads of design and no useful functionality' superfluous technologies that keep getting incorporated into gnome.\n\nPeople have to download and install >20mb of superfluous tech to get indexed search in some desktop environments - indexed search which, hilariously, relies on a separately obtained kernel modification anyway. All sounds a bit Redmond to a lot of companies, that sort of thing.\n\nI like Gnome, and KDE, but I'd be very surprised if anybody sane is having a \"when are Novell going to dump KDE\" debate.\n\nAt least this side of Slashdot, anyway :)"
    author: "Marcus"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Yay! Time to find Slackware binaries.."
    author: "Mikhail Capone"
  - subject: "KOPETE: big bug or I am crazy?"
    date: 2004-08-20
    body: "Just finished installing 3.3.0 and figured I'd try Kopete to see if it's mature enough to replace gaim and maybe xChat.\n\nWell, I can't find an option to add a jabber account anywhere in the config menus. Not where the other accounts are, not in the plugins..\n\nThe weird thing is that on Kopete's website they have screenshots that showa jabber account and they talk about jabber and link to the website.\n\nSo what happened? Anybody else having problems with that?"
    author: "Mikhail Capone"
  - subject: "Re: KOPETE: big bug or I am crazy?"
    date: 2004-08-20
    body: "You have to install libidn before compiling Kopete for Jabber support, see list of compilation requirements."
    author: "Anonymous"
  - subject: "Re: KOPETE: big bug or I am crazy?"
    date: 2004-08-20
    body: "Ah, well, I just downloaded the slackware binaries that were linked from kde.org .. Maybe I'll wait for the official slackware binaries to come out and see if they are compiled differently.\n\nI don't really feel like compiling KDE on my K6-2 450mhz"
    author: "Mikhail Capone"
  - subject: "Re: KOPETE: big bug or I am crazy?"
    date: 2004-08-20
    body: "Well you won\u00b4t have other Slackware packages. These are non official but I\u00b4m providing them for the last past 3 years. So they are kind of official :)\n\nAnyways about Kopete, I\u00b4m not using it for a long time because it\u00b4s full of bugs. Totally unusable. At least for the MSN messenger plugin. Don\u00b4t know for others."
    author: "JC"
  - subject: "Re: KOPETE: big bug or I am crazy?"
    date: 2004-09-06
    body: "There are official packages, Pat already added them to testing/packages/kde-3.3/ for slackware-current.\n"
    author: "Rob Kaper"
  - subject: "Re: KOPETE: big bug or I am crazy?"
    date: 2004-09-06
    body: "Oh, but those currently do not have libidn support either, because libidn is apparently still alpha and not for production use. Hopefully not many parts of KDE require it.\n"
    author: "Rob Kaper"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "WOOOOOOOOOOOW! What did you do to get KDE that much faster? kontact is there in 1 or 2 seconds - around 15 before...."
    author: "Kai F. Lahmann"
  - subject: "Re: 3.3"
    date: 2004-08-20
    body: "Yes, I noticed that too. And the new splash screen is nice.\n\nIt does seem that the first time I move from one area of kontact to the other it does some more loading, so that could be how they speeded up the initial loading. Still nice, though."
    author: "Mikhail Capone"
  - subject: "Re: 3.3"
    date: 2004-08-21
    body: "Look here: http://www.kdedevelopers.org/node/view/534"
    author: "anonymous"
  - subject: " Python bindings"
    date: 2004-08-19
    body: "The Python Bingind in CVS are the killer here IMHO. This, together with kdevelop or qtdesigner, should make KDE _THE_ best RAD tool in the free software world \n\nKudos !"
    author: "MandrakeUser"
  - subject: "Re:  Python bindings"
    date: 2004-08-19
    body: "But don't forget the QtRuby/Korundum bindings either! They too are about the most powerful RAD tool that's ever existed, and have added excellent DCOP support for the KDE 3.3 release. \n\nAnd BTW, kjsembed isn't half bad as a RAD or scripting tool.."
    author: "Richard Dale"
  - subject: "Re:  Python bindings"
    date: 2004-08-19
    body: "I have to agree with this. Python, PyQt and Designer is what I use for all my private development. It's so easy and so fast to get advanced things done. And the stability of the bindings is amazing, I can't remember a single crash which wasn't due to an error I did. :)"
    author: "Chakie"
  - subject: "Re:  Python bindings"
    date: 2004-08-19
    body: "That's absolutely true... I remember I was completely astonished at finally having found a problem that didn't fit PyQt -- a fast, optimized drawing application with natural media support. For most things, from data mangling to mail clients, PyQt is the smart choice."
    author: "Boudewijn Rempt"
  - subject: "Re:  Python bindings"
    date: 2004-08-20
    body: "Add Eric3 to it. It is perfect.\nhttp://www.die-offenbachs.de/detlev/eric3.html\n\nThen get nkids crystal icons for eric3. Just for Looks.\nhttp://www.die-offenbachs.de/detlev/eric3-contrib.html"
    author: "NS"
  - subject: "whats new guide"
    date: 2004-08-19
    body: "http://linuxreviews.org/kde/what_is_new_in_kde_3.3/#toc2"
    author: "anon"
  - subject: "Mandrake 10.1 ?"
    date: 2004-08-19
    body: "Does anyone know whether KDE 3.3 will make it to Mandrake 10.1 ? I would think so, since Cooker is still undergoing major changes. Mandrake 10.1 beta 2 is still not released, etc. But who knows ! \n\nOtherwise, anyone know of unofficial Mandrake 10.0 packages ?\n\nThanks so much in advance ..."
    author: "MandrakeUser"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-19
    body: "It seems that you won't get KDE 3.3 with mandrake 10.1 because they won't have enough time to test it.\nI remember Mandrake 10.0 with its KDE 3.2 CVS !"
    author: "Pinaraf"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "No, Mandrakelinux 10.1 will still have KDE 3.2.x (stable CVS).  The idea of updating the 10.1 public stable tree (now called \"Community\" in 10.0) to KDE 3.3 later is being tossed around."
    author: "David Walser"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "Argg. Bad choice IMHO. This is the time for Mandrake to get KDE 3.3 in Community. That's exactly what community is for: community testing. KDE 3.3 will take several package rebuilding and bug-fix backporting from CVS cycles before it gets stable. No way it will stabilize for the official release if they dont release now and get broad testing. Anyways, I don't wanna get off-topic too much. Thanks guys for the feedback!"
    author: "MandrakeUser"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "No, a rock solid 3.2 is far better than 3.3. Many people, including me, need a distribution that is suitable for people who are not computer experts. A distribution they can use without having to worry about bugs.\n\nThe problem with Mandrake is that they do not offer the latest KDE for the experienced users, not even at the commercial mandrakeclub."
    author: "testerus"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "You should read more then. Community is for testing and NON-experts. "
    author: "Joe Schmoe"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "\"Community is for testing and NON-experts.\"\n\nThat was exactly my point. Mandrake community is not even close to release, and official will follow a month later. This gives ... what ? 2 months ? about 2 months from now. To me, that's plenty of time to stabilize KDE 3.3 (with CVS fixes, plus cooker fixes, plus community reported bugs being fixed). Gee, KDE may release a quick 3.3.1 with the first bugfix collection even before official. Oh well ...\n\nAnd back to the drake, IMHO they are missing one component: a yearly, corporate edition, low paced but ultra stable. Big corporations will rather miss a few features than hit a bug. KDE 3.2.* would be ideal for this corporate edition right now. \n\nAnd one more point. Mandrake won a lot of mind sharing by staying in the bleeding edge. Lots of people, mostly enthusiasts and devs, like that. Give them that, keep them in, and get their contributions!\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "Addendum: there is a thread on cooker about this:\nhttp://archives.mandrakelinux.com/cooker/2004-08/msg02957.php\n\nBottomline: the problem seems to be to port all the mandrake customizzations to KDE 3.3 \n\nQuestion: shouldn't they have switched to the 3.3 CVS branch a couple weeks ago when this branch went into deep freeze for the RC's ? "
    author: "MandrakeUser"
  - subject: "Re: Mandrake 10.1 ?"
    date: 2004-08-20
    body: "> You should read more then. Community is for testing and NON-experts\n\nIt seems you are wrong here. Cooker is for testing. Community is meant to be a stable version of Mandrake Linux. Official is Community with additional bugfixes . (See http://www.mandrakelinux.com/en/pr-releaseprocess.php3)\n\nMandrake 10 contained KDE 3.2.0 with some additional fixes, but there were still enough bugs in KDE 3.2 to release KDE 3.2.1, 3.2.2 and 3.2.3."
    author: "testerus"
  - subject: "Way to early"
    date: 2004-08-19
    body: "Way to early. There are still some killer-bugs there, like this one: http://bugs.kde.org/show_bug.cgi?id=87401 \n\nI also hope they fixed Kdcop, because it hangs when I open it in RC2.\n\nI will see,\n\nJeroen"
    author: "Jeroen"
  - subject: "Re: Way to early"
    date: 2004-08-19
    body: "Nobody forces you to upgrade to kde 3.3\nIf kde 3.3 contains bugs that are grave to you, then don't upgrade."
    author: "anonymous"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "Final releases aren't expected to work any more? If major bugs still exist, it is wrong to call it \"final\".\nJMO"
    author: "anon"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "they are final thats all, they dont have to work in *every aspect*"
    author: "chris"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "1. The release works just fine. \n2. This is definately not a showstopper / major bug.\n3. The maintainer cannot reproduce this bug and is waiting for more information."
    author: "affenschlaffe"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "How is that even REMOTELY a showstopper?\n\nGive me a break."
    author: "Joe"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "Why should this be a showstopper???\n\nBTW: why don't YOU provide backtraces so that developer can fix the problem.\nIf you looked in the bug report you would have noticed that this would be what the developer are asking for and not just complaining in a forum...\nYOU can do something about it... The more helpful YOU are the more chances you have your bugs get fixed early..."
    author: "Norbert"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "I tried, but as you can read in the bugreport, artsd can only be killed with -9 and that does produce backtraces. I am also not the only one experiencing this bug.\n\nAnd why should it not be a showstopper or a nearby-showstopper, because people are not able to play radiostreams (.pls) and that is a real problem to my opinion.\n\nCheers,\n\nJeroen"
    author: "Jeroenvrp"
  - subject: "Re: Way to early"
    date: 2004-08-21
    body: "try for KDE apps:\n kill -SEGV <pid>\nprobably for artsd, you have to start it from gdb (as there will be no kdecrash dialog)"
    author: "koos"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "It's beyond me why anyone would still install artsd, much less try to use it. It's been crashing forever, as has anything based on it.\n\nJust grab KPlayer, kplayer.sf.net, and enjoy your music, radios, movies, whatever. Forget about arts, you will never need it."
    author: "----"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "I had a lot of problems with artsd in the past, but they are gone for me since 3.2.1: no more crashes or lags (kernel 2.6 helps a lot for that), and now I think artsd is priceless for:\n- mixing several wave sources (system notifications, juk, ...)\n- let the many games that use OSS to run seamlessly in KDE using \"arstdsp\" (I play neverwinter nights this way, still listening mp3 music)\n- running out of the box\n\nI personaly use KMPlayer with the xine/artsd backend"
    author: "lp"
  - subject: "Re: Way to early"
    date: 2004-08-20
    body: "Yes, they shipped RC3 and called it final.\n\nHowever, this bug is marked as FIXED.  Are you saying that it isn't fixed?\n\nYes it was too early.  The seriously broken Focus Stealing Prevention in KWin was in the release but the most serious issue has already been fixed the the 3.3 BRANCH on CVS. :-(\n\nThen there is the killer bug with the desktop icons.  When aligning to grid, the X distance has a default that is OK and it is configurable, but the Y distance is TWICE (actually more than TWICE) what I am using with 3.2.3 and it is NOT configurable.  The cute comment in the code says it all:\n\n    int gridX = gridXValue();\n    int gridY = 120; // 120 pixels should be enough for everyone (tm)\n\nIs there some reason that \"gridX\" is configurable and \"gridY\" is a constant? \n\nyes, 120 is enough, but it is TOO MUCH for most users.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Way to early"
    date: 2004-08-21
    body: "Yes, there are (fairly obvious) bugs in 3.3.0. That's inevitable - it's a huge piece of software, the developers don't have unlimited time and testing is boring.\nIt's a chicken-and-egg problem - not many people will install something if it's called \"beta\" software, so it never gets tested completely. At some point the developers have to release a 'final' version, just to increase the installed base and get some real testing done. Unless you're volunteering to form a beta-test group to do complete functionality and regression testing? :)"
    author: "Ben"
  - subject: "Re: Way to early"
    date: 2004-08-21
    body: "You seem to have missed the point completely.\n\nI see no chickens or eggs.  This could have been released as RC3 and enough people would use it to have it tested.\n  \nIf 3.3.0 \"final\" was released with bugs because they hadn't been found yet, this would be understandable and your comment would make sense.\n\nHowever this is really irrelevant since bug reports have been filed against 3.3Beta? and 3.3RC? and some (obvious and significant ones) have not been fixed before the \"final\" release of 3.3.0.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Way to early"
    date: 2004-08-21
    body: "There's a life after 3.3.0."
    author: "Datschge"
  - subject: "Re: Way to early"
    date: 2004-08-22
    body: "At some point they have to release. This isn't a final release, it's just a .0 release. The bug you mention is a non-bug to me, so I'd rather they released than delayed for that bug. This is Open Source development, not a commercial piece of software.\n\nPersonally, I've found the Slackware packaged version of KDE3.3.0 to be a great improvement over the 3.2.2 I had before. I did report some bugs, but they seem to be SuSE specific, so I switched distro and now things seem much more stable.\n\nIf it's too buggy for you, don't upgrade. You can see the bugs outstanding - if they're going to be a problem, don't upgrade. Personally, I'm glad they did it now. The official release tag means distro packagers get working, more testing and a sooner release for 3.3.1 with even fewer bugs. If they held on until there were no bugs, you lose the advantage of having parallel development and testing, as most of the code sits in the repository while the few developers whose code still has bugs filed work."
    author: "Ben"
  - subject: "Re: Way to early"
    date: 2004-08-22
    body: "Hmm, looking at the \"most severe\" bug list maybe some of those shouldn't really be in a release version. Dammit, no edit button. Maybe they should slow down the feature additions to go back to bugfixing."
    author: "Ben"
  - subject: "Re: Way to early"
    date: 2004-08-21
    body: "OK, I compiled the final and the bug is indeed solved. Sorry to make people panic :-)\n\nJ"
    author: "Jeroenvrp"
  - subject: "Re: Way to early"
    date: 2004-08-31
    body: "kdcop works fine, just find the broken application and close it which blocks dcop (e.g. OpenOffice.org/KDE)."
    author: "Anonymous"
  - subject: "hope to contribute sometime"
    date: 2004-08-20
    body: "i have no time right now, but i hope i can contribute sometime in the feature.\nkeep up the good work !"
    author: "chris"
  - subject: "Re: hope to contribute sometime"
    date: 2004-08-20
    body: "If you don\u00b4t have time to help, you can send money or hardware to developers.\nIt takes only a few minutes and it\u00b4s very helpful."
    author: "JC"
  - subject: "Disconnected IMAP?"
    date: 2004-08-20
    body: "I have a very \"user\" question; I'd like feedback from actual users.  When disconnected IMAP first appeared, I used it. This was when it had all sorts of warnings.  It nuked my mailbox.  Has it become stable enough for business use?  It would be one heck of a useful feature, as plowing through my mailboxes on my laptop during lunch or otherwise away from the office would be a really nice feature."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Disconnected IMAP?"
    date: 2004-08-20
    body: "I use it myself since quite a while"
    author: "coolo"
  - subject: "Re: Disconnected IMAP?"
    date: 2004-08-20
    body: "I haven't experienced data loss with 3_3_BRANCH but I did loose all my emails once with 3.2x and dIMAP. To quote David Faure (from a bug report):\n\n\"Since you can compile from sources, I strongly recommend using CVS HEAD of kdepim. The disconnected-imap support is much much better there.\"\n\nHTH"
    author: "affenschlaffe"
  - subject: "Re: Disconnected IMAP?"
    date: 2004-08-20
    body: "I've started using it yesterday. I tried do to so for as long as kmail supported it, but never got it working (somehow my folders didn't show up, even though I can see them in the (un)subscribe windows). With the upgrade to 3.3 it suddenly started working. Syncing takes rather long (~10 minutes) even when most of the folders don't have any changes to sync, but as my mailbox contains over 10.000 messages, and synchronizing can be done asynchronous I don't consider that a problem.\nLoading a message is noticably faster with dimap on my P4 2.4 / 1GB / 100 mbit system. (The imap server is a P3 450 with 192MB).\n\nAll in all I'm very happy with disconnected imap, but I've not used it enough to assess it's stability."
    author: "CAPSLOCK2000"
  - subject: "Re: Disconnected IMAP?"
    date: 2004-08-20
    body: "I've had many troubles with it, maybe has something to do with my mail server or local setup. These two bugs make it unusable for me:\nhttp://bugs.kde.org/show_bug.cgi?id=66469 and\nhttp://bugs.kde.org/show_bug.cgi?id=85896."
    author: "suki"
  - subject: "Regression?"
    date: 2004-08-20
    body: "Disabling \"Allow moving and resizing of maximized windows\" doesn't seem to work properly anymore.\n\nKDCOP is still busted for me as well.  The Process Table in ksysguard works again, but it always shows up on my second Xinerama screen.  :/"
    author: "Hooded One"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "It does 'work', it's just that the developer changed what it does, except that the string freeze meant he couldn't change the name of the option. Apparently, it now means \"don't draw a border around maximized windows\", not \"prevent moving maximized windows\".\nWhy anyone would want a border around a maximized window I don't know - another option that could be replaced with a sensible default. There are several votes and complaints on bugs.kde.org asking for it to be changed back to the old behaviour."
    author: "Ben"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "I must say, that does sound pretty stupid.  Not only to essentially remove a pref that many people use, but to do so in a way that the new pref's description is completely irrelevant.\n\nA quick search for \"allow moving resizing maximized\" finds me one UNCONFIRMED bug and one WORKSFORME.  Is there another, more popular bug that I'm missing?"
    author: "Hooded One"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "This one:\n\nhttp://bugs.kde.org/show_bug.cgi?id=86847\n\nit has 40 votes already."
    author: "Ben"
  - subject: "Re: Regression?"
    date: 2004-08-21
    body: "40 votes.. which basically means that two people have voted for it :(  (I was one)\n\nI really don't like the new behaviour.  The old one was actually useful..  I went ahead and rolled back some changes in the source and made my own kwin .deb that uses the old behaviour.  One of the good things about open source :)"
    author: "MamiyaOtaru"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "Ditto for me. Seems that there are lots of Xinerama related problems.\n\nAlso noticed that System Tray, Pager and Taskbar stop updating their contents after a while. Plus all of my screensavers are now toast - I can't test or configure them. KWeather loses its network connection intermittantly.\n\nOne of the least stable KDE releases I've had in a while... not sure if it's SuSE or KDE. I'm guessing SuSE."
    author: "DeckerEgo"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "Ooh, I'd forgotten about the screensaver problem (I just use blank screen, which works fine) but yeah, I get it too.\n\nI've had the taskbar stop updating only once since I installed RC2, but that may be the same thing you're seeing.\n\nI worked around the Process Table showing up in the wrong place by going into the window-specific KWin settings (right-click titlebar -> Advanced -> Special Window settings, and I chose to match the exact window) and in Workarounds setting Ignore Requested Position to Force Yes.  (Why does this need two checkboxes plus a selectbox to be enabled?  It should be a simple boolean pref.)  It's already  This made it always appear on my first screen, but in the corner, so in Geometry I enabled Force Centered for Placement."
    author: "Hooded One"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "Ick. It shouldn't need to be that hackified. Methinks this release is completely messed."
    author: "DeckerEgo"
  - subject: "First impressions - SUSE 9.1 Pro rpms"
    date: 2004-08-20
    body: "It seems faster on my laptop.  The Quick Launcher now supports more than two columns of icons, which is an incredibly useful thing for anybody who uses it.  Thanks appear to go to Dan Bullok for the work (according to the About dialog).\n\nThe first obvious thing is that my fonts are *huge* in Konqueror and I've lost all my toolbar settings for Konqueror.  I had tweaked it out so I had one toolbar (location) with back, up, forward and refresh, plus a drop down for Profiles.  This was all removed by the install.\n\nKontact looks much nicer.  The appearance has had a delightful cleanup.  My mail did not appear at first (everything else including News and Notes did), but merely opening the KMail config dialog and hitting 'OK' made it reappear.  Scary, if I didn't know my mail was on the server.  Oooo... a search bar on the mail.  \n\nThere appears to be quite a few functional and useful updates for 3.3.  Normally, in past releases, I've followed development.  I've not had the time for most of 2004, so all these nifty things are surprises - quite pleasant ones.  I need to figure out why my fonts in Konqueror are huge (likely a simple setting - yep, set the medium font size in Konq's settings), and retweak a few settings in the toolbars, and I'm happy.\n\nWell worth an upgrade.  Thanks, guys!"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: First impressions - SUSE 9.1 Pro rpms"
    date: 2004-08-20
    body: "First of all, thanks to the KDE developers: You did a fantastic work!\n\nI installed the rpm's on a SuSE 9.0 without flaws, except that i needed the --force option when freshening kdebase and kdelibs. Maybe there is a problem with kdeartwork3-xscreensaver. After installing, All screensaver except clock won't work, the screen just turns black. The problem can be solved by installing a older version of this rpm. Just removing kdeartwork3-xscreensaver will also work, but the list of available screensavers is much smaller."
    author: "Benjamin Stocker"
  - subject: "Re: First impressions - SUSE 9.1 Pro rpms"
    date: 2004-08-20
    body: "Did you see kdemultimedia 9.1 rpm's because I couldn't find them? Found the tar ball so it must be there. Further more starting yast2 modules form KDE Control Center doesn't work anymore. Also had some probelms with Konqueror but is working now after a reboot. And I needed to install jack-0.94.0-71.i586.rpm to be able to install arts-1.3.0-7.i586.rpm which wasn't needed before."
    author: "homernt"
  - subject: "Re: First impressions - SUSE 9.1 Pro rpms"
    date: 2004-08-20
    body: "SuSE offers 4 packages for kdemultimedia 3.3\nI checked yast in kcontrol, works fine over here.\nAll suse-specific kdestuff, like krpm, yast in kcontrol, power control, hardware control, etc still work in kde 3.3.\nOnly SuSE-hack that is gone is the easy way to change users in kde-sessions. KDE 3.3 has the 'old' way of changing users, you can start a new session in k-menu.\n\nIf you experience problems with suse rpm's, check if your ftp mirror is up to date. suSE is offering kde 3.3 since 11-08, and until 18-08, those packages were not free of packaging bugs."
    author: "anonymous"
  - subject: "Re: First impressions - SUSE 9.1 Pro rpms"
    date: 2004-08-21
    body: "Found the kdemultimedia 3.3 rpm's on the suse site they are from the 11-08 but I installed them anyway. Did some further testing on starting yast2 modules from KDE Control Center and it seems that it only occures when I do this under root. With other users it asks for root the password and then it starts up. With logged in as root it never passes the starting up fase."
    author: "homernt"
  - subject: "Problems with SuSE and Kicker"
    date: 2004-08-20
    body: "With SuSE 9.1, when I log in the systray and taskbar applets don't work.  The systray shows only Gaim, nothing else, while the taskbar has one blank entry.  If I remove these from the panel and then add them again, they work fine.\n\nAnyone else have this problem?"
    author: "jaykayess"
  - subject: "Icons cut off"
    date: 2004-08-20
    body: "With KDE 3.3.0 for SuSe 9.1, my icons are being cut off at the top\nand bottoms of the screen.\n\nDoes anyone know a way to fix the icon problem?\n\nHere is picture of the problem on my desktop.\nhttp://audioseek.net/~hiryu/icons.png\n\nAlso, it seems the option has either been\nremoved or moved again for MacOS type bar at the top.\nI have it because KDE3.3.0 is retaining my settings from\n3.2\n\nAside from these problems, seems like a nice and solid release\nand I'm enjoying it. The icon thing is just really annyoing.\n\nThanks."
    author: "Hiryu"
  - subject: "Re: Icons cut off"
    date: 2004-08-20
    body: "MacOS style menubar carried over for me (SUSE 9.1 Pro).  \n\nAnd about half an hour after I installed it, I found a really nifty page about how to add a clock (and other applets) to the menubar (basically turn it into a child panel):\n\nhttp://wiki.kdenews.org/tiki-index.php?page=MacOS-like+Menu+Bar\n\nI note that the MacOS menubar and child menus are now much more strict about not allowing windows over them.  I used to put XMMS into a mini-bar and float it over the menu. Can't do that now, but I suppose that's a \"fix\", since you could lose windows under the menu and bars.\n\nI also noticed a icon quirk on my desktop - the align to grid now uses a huge grid even though my icons plus text are small.  The grid size used to depend on how large the largest icon+description was.  Now it seems fixed at a fairly large size.  It's just different."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Icons cut off"
    date: 2004-08-21
    body: "> http://wiki.kdenews.org/tiki-index.php?page=MacOS-like+Menu+Bar\nNice feature, allthough you probable want to add something first to the child panel before adding the 'menu' applet (now way to do it afterwards) .. bit buggy in my setup, if the 'Help' menu of konqueror doesn't fit then there is no scroll button (like there is if more menu items get hidden) .. oh well for 3.3.1 or so .. and a good reason to clean up my panel :-)"
    author: "koos"
  - subject: "Re: Icons cut off"
    date: 2004-08-20
    body: "I had the same problem with 3.2.x but happens sometimes. Never be able to reproduce this problem.\n\nBtw, what\u00b4s the fonts are you using on your desktop ? :) I like it."
    author: "JC"
  - subject: "Kommander"
    date: 2004-08-20
    body: "Kommander in kdewebdev? Please, move this fantastic tool to kdebase!\nThis program is so versatile that every human should have a copy when it's brought into life!\n\nJostein"
    author: "Jostein Chr. Andersen"
  - subject: "Re: Kommander"
    date: 2004-08-20
    body: "My idea exactly. Development is *so* much faster with Kommander. The only downside of Kommander is that not everyone has it installed.\n\nBut I think Kommander will really go off like a rocket once it has some good documentation with examples and tutorials."
    author: "Dik Takken"
  - subject: "Re: Kommander"
    date: 2004-08-20
    body: "Just like everything else in Linux."
    author: "Some Dude"
  - subject: "Re: Kommander"
    date: 2004-08-20
    body: "Yah except there is no documentation for it. Liked the idea, tried it, hated it, went back to good old C++. KDevelop rocks!"
    author: "the future is K++"
  - subject: "Re: Kommander"
    date: 2004-08-21
    body: "There is already some documentation, just try Help->Kommander Handbook since Kommander Editor 1.0Alpha6 (including the one in KDE 3.3.0). It's not complete, but it is constantly updated. You can get the up to date versions from CVS and you might want to look at the examples shipped with Kommander as well. There is also a function browser in the latest version.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Regression?"
    date: 2004-08-20
    body: "eigher I have found a funny regression or a funny debian-Bug: try to select a file icon (icon-view, not the first in your folder) with the mouse (drag around it), press F2 and wonder, which one is still selected!"
    author: "Kai F. Lahmann"
  - subject: "Re: Regression?"
    date: 2004-08-20
    body: "Yepp, confirmed under colinux/debian. Go ahead: bugs.kde.org"
    author: "Jochen P."
  - subject: "KMail has a bug - Else everything just rocks..."
    date: 2004-08-20
    body: "Hi, I just installed and everthing works fine (and looks really great). The only problem I found is that I can no longer use Distribution Lists in the BC field in KMail. No matter what I do it ignores all Distributions Lists that I put in the BCC field.\n\nIs anyone else experiencing this?\n\nHugo"
    author: "Hugo Costelha"
  - subject: "KGet still crashes at launch?"
    date: 2004-08-20
    body: "Just installed KDE 3.3, KGet still not runs :\n\nnicolas@pcfixe nicolas $ kget\n*** KGet got signal 11\nERROR: Communication problem with kget, it probably crashed."
    author: "Nicolas Blanco"
  - subject: "Re: KGet still crashes at launch?"
    date: 2004-08-21
    body: "Same problem here. The strange thing is that this is marked as fixed on bugs.kde.org. \n\nHowever, this is probably not the right place to discuss this. Let's try to get some usefull output and head over to bugs.kde.org."
    author: "ralph"
  - subject: "Re: KGet still crashes at launch?"
    date: 2004-08-21
    body: "FYI, here's the bug report, and a patch:\nhttp://bugs.kde.org/show_bug.cgi?id=84450"
    author: "Rex Dieter"
  - subject: "Cleaner!"
    date: 2004-08-20
    body: "The interface is definitively cleaner, even if some changes are not always clear to me (why do we have bigger font in Konqueror?).\n\nGreat work overall. You start taking into account the remarks from the Gnome camp .-)\n\nI'm sure that what Trolltech has started with QT4, is the result of a long experience, and that it will have a good impact on KDE."
    author: "veton"
  - subject: "incredible"
    date: 2004-08-20
    body: "I've been running 3.3(-rc) for a couple of days, and I haven't been moved to report a single bug yet. I think this is the first KDE release since I started using the first beta of 2.0 where the quality has been this good."
    author: "cbcbcb"
  - subject: "Re: incredible"
    date: 2004-08-21
    body: "You can't be looking very hard ;) I've found five annoying bugs already! Still a great improvement on 3.2 though."
    author: "Ben"
  - subject: "Sweet"
    date: 2004-08-20
    body: "Been using it from CVS for a while now. It is much cleaner. Sounds like it's time for a cvs up."
    author: "the future is K++"
  - subject: "Laptop sleep"
    date: 2004-08-21
    body: "Does anyone know if KDE 3.3 supports laptop sleep when I flip down my display. It does not work with 3.2"
    author: "Alex"
  - subject: "Re: Laptop sleep"
    date: 2004-08-21
    body: "It's not KDE's responsiblity to handle power events, that's what apmd is for. Check your distro's power management handling configuration."
    author: "Ben"
  - subject: "Kontakt/KMail rocks! "
    date: 2004-08-21
    body: "The new search field in KMail just rocks! Very cool feature! Thank you!"
    author: "Emmeran \"Emmy\" Seehuber"
  - subject: "Updating SuSE 8.2 / KDE 3.1.1 - anyone?"
    date: 2004-08-21
    body: "Hi!\n\nI'm planning to update my SuSE 8.2 box running KDE 3.1.1 to KDE 3.3. When trying to update with previous versions, i frequently ran into rpm incompatibility issues (don't remember any details, though). I also once freaked up my system by trying to install the RPMs from within a running KDE, quite a blunder as i was forced to learn shortly later...\n\nSome folks wrote that a simple rpm -Uvh -nodeps -force *.rpm while being in single user mode would do the job, but \"-nodeps -force\" has also been commented as \"not for idiots\", which (sort of) applies to me.\n\nDid anyone try upgrading a system similar to mine to KDE 3.3? What is the best way to do it?\n\nThanks!"
    author: "-nodeps -force -> Idiot"
  - subject: "Re: Updating SuSE 8.2 / KDE 3.1.1 - anyone?"
    date: 2004-08-26
    body: "Hi, I actually did that move today.\nI used apt4rpm. See the instructions on \nhttp://linux01.gwdg.de/apt4rpm/.\nThere are instructions for KDE 3.1 --> KDE 3.2, but these\ninstructions are still good."
    author: "nildevice"
  - subject: "Konqueror regression?"
    date: 2004-08-21
    body: "For some reason Konqueror seems a bit slower than it was at connecting to website. It's almost as if there's a delay built in before attempting to connect to a new web-server, but once it has connected clicking on links within that server is pretty much instantaneous (something immediately appears in the status bar).\n\nBut a few times it doesn't do this, which makes it hard to know if it's not just in my head or because of my internet connection. I doubt it, though, because I noticed the different right after upgrading to 3.3.0 from 3.2.3 and it has been pretty constant since.\n\nI guess that I don't have the ressources to test konq performances, but some konq developer should probably look into it and make sure that (especially on slower systems) there isn't something that causes a delay before konq tries to reach a server for the first time.\n\nAside from that, I quite like all the polish that Konq got for 3.3!"
    author: "Mikhail Capone"
  - subject: "Re: Konqueror regression?"
    date: 2004-08-22
    body: "I have noticed that if I click twice in a row the delay before trying to reach the server disappears."
    author: "Mikhail Capone"
  - subject: "Re: Konqueror regression?"
    date: 2004-08-23
    body: "I have experienced similar slowdown with konqueror.  Also simpy the loading of some pages takes longer.  Some pages are faster than kde3.2.3 but others like http://www.luchtvaartnieuws.nl or http://www.standaard.be are up to 10 times slower...  It takes a lot more time until the central text shows up.  Firefox is faster so it's not because of my connection.\n\nI'm using the debian unstable packages, maybe the reason is to be found there...?\n\nFilip"
    author: "Filip"
  - subject: "Re: Konqueror regression?"
    date: 2004-08-23
    body: ">I'm using the debian unstable packages, maybe the reason is to be found there...?\n\nI'm using Slackware so I doubt it.\n\nAnother change I've noticed is that if you open many tabs in a short amount of time, it won't open them two by two in order like it did before. It will try to load all of them at once, so if there are many of them, you'll have to wait longer for one of them to be completed."
    author: "Mikhail Capone"
  - subject: "made a bug report (bug 87895)"
    date: 2004-08-24
    body: "I didn't find similar bug reports..  So I reported it.  \nIt 's bug #87895\n\nFilip"
    author: "Filip"
  - subject: "Minicli bug?"
    date: 2004-08-21
    body: "I'm not actually sure whether this is a regression or anything -- I have been messing about with the SuSE kde rpm's from the ftp.gwdg.de apt repository -- but since my last update this morning, the minicli 'remembers' the desktop it was first called up on, and refused to appear in other desktops. This is quite a nuisance..."
    author: "Boudewijn Rempt"
  - subject: "Re: Minicli bug?"
    date: 2004-08-22
    body: "Same is happening with me. I used the same rpms. Its kde bug or problem with rpms?"
    author: "nileshbansal"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "Same overhere,\nThis can be tackled through configuring the special setting under menuentry advanced in the popupmenu when right clicking on the window caption."
    author: "anonymous"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "Please be a little more verbose. What exactly to choose in special setting dialog box?"
    author: "nileshbansal"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "well, my english is bad, and I'm using the Dutch version of kde, zo can't be too precise :)\n\nrmb click on the caption of the window (any window. In the popup menu, you will see something like [Advanced->special window configurations]. This menu entry pops up one or two dialogs wich you can use to setup advanced stuff for your window.\nFor minicli, go to tab [geometry] and select the option [desktops -> on all desktops], or change the option in something else, that prefents minicli from remembering the previous desktop setting.\n\ngood luck"
    author: "anonymous"
  - subject: "Re: Minicli bug?"
    date: 2004-08-24
    body: "That's curious... I don't have any tabs in that dialog. I get 'information about selected window', and can't do much except select applicability to window class. Perhaps a kwin setting hidden in my .kde..."
    author: "Boudewijn Rempt"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "I had a lot of trouble with the SuSE RPMs. Several bugs I found in them aren't present in the Slackware packages I'm using now."
    author: "Ben"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "I've solved the problem by going back to my compiled version of KDE, but with SuSE's arts package, since I didn't manage to compile a version of arts that actually produced sounds."
    author: "Boudewijn Rempt"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "Which unfortunately means that I've lost my image previews in konqueror. There must be some difference between the SuSE KDE rpm's and kde-from-cvs (KDE_3_3_BRANCH) that makes that difference because of a unknown configuration setting I've got set for user and not for another."
    author: "Boudewijn Rempt"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "Posted too early... Apparently, it's not enough to select image previews in View/Preview, but you must also select the file: protocol in Settings/Configure Konqueror/Previews & Meta Data/Select Protocols. The local protocols appear to be disabled by default. You need to scroll down. because only remote protocols are shown in the treeview."
    author: "Boudewijn Rempt"
  - subject: "Re: Minicli bug?"
    date: 2004-08-23
    body: "Weird, previews work fine over here without any additional setting.\nThe only thing that doesn't work on my suse 9.1, are the screensavers.\nAlso heard about Debian users with the same problem, so that is not SuSE specific"
    author: "anonymous"
  - subject: "Konqueror crashes with SuSE 9.0 updates."
    date: 2004-08-25
    body: "This is not the responsibility of the KDE team, of course, but I've got a problem with the SuSE 9.0 rpms for KDE 3.3\n\nKonqueror consistently crashes on startup with \"Signal 6, SIGABRT\".\n\nNot sure if anyone else is experiencing this?"
    author: "Rune Kristian Viken"
  - subject: "Re: Konqueror crashes with SuSE 9.0 updates."
    date: 2004-08-27
    body: "I also expierienced this.\n\n[New Thread 16384 (LWP 12501)]\n[KCrash handler]\n#4  0x4169fb71 in kill () from /lib/i686/libc.so.6\n#5  0x41522cf1 in pthread_kill () from /lib/i686/libpthread.so.0\n#6  0x4152300b in raise () from /lib/i686/libpthread.so.0\n#7  0x4169f904 in raise () from /lib/i686/libc.so.6\n#8  0x416a0e8c in abort () from /lib/i686/libc.so.6\n#9  0x41698e84 in __assert_fail () from /lib/i686/libc.so.6\n#10 0x401d3140 in KParts::Factory::createObject(QObject*, char const*, char const*, QStringList const&) (this=0x0, parent=0x82f9c48, \n    name=0x83368b0 \"libkrpmview\", classname=0x0, args=@0x0) at factory.cpp:81\n#11 0x409abee1 in KLibFactory::create(QObject*, char const*, char const*, QStringList const&) (this=0x83375f0, parent=0x0, name=0x0, classname=0x0, args=@0x0)\n    at klibloader.cpp:86\n#12 0x401d824d in createInstanceFromFactory<KParts::Plugin> (factory=0x0, \n    parent=0x0, name=0x0, args=@0x4152868c) at componentfactory.h:62\n#13 0x401d819f in createInstanceFromLibrary<KParts::Plugin> (\n    libraryName=0x4152868c \"\\214\u00e5\", parent=0x0, name=0x0, args=@0x0, error=0x0)\n    at componentfactory.h:144\n#14 0x401c8b7a in KParts::Plugin::loadPlugin(QObject*, char const*) (\n    parent=0x0, libname=0x83368b0 \"libkrpmview\") at plugin.cpp:177\n#15 0x401c9446 in KParts::Plugin::loadPlugins(QObject*, KXMLGUIClient*, KInstance*, bool) (parent=0x82f9c48, parentGUIClient=0x82f9ccc, instance=0x82fa260, \n    enableNewPluginsByDefault=true) at plugin.cpp:286\n#16 0x401c4188 in KParts::PartBase::loadPlugins(QObject*, KXMLGUIClient*, KInstance*) (this=0x0, parent=0x0, parentGUIClient=0x0, instance=0x0) at part.cpp:125\n#17 0x401c4137 in KParts::PartBase::setInstance(KInstance*, bool) (\n    this=0x82f9c70, inst=0x82fa260, bLoadPlugins=true) at part.cpp:119\n#18 0x41c83bd9 in KHTMLPart (this=0x82f9c48, __vtt_parm=0x1, parentWidget=0x0, \n    widgetname=0x0, parent=0x0, name=0x0, prof=BrowserViewGUI)\n    at khtml_part.cpp:167\n#19 0x421d27b1 in KonqAboutPage::KonqAboutPage(QWidget*, char const*, QObject*, char const*) () from /opt/kde3/lib/kde3/konq_aboutpage.so\n#20 0x421cc919 in KonqAboutPageFactory::createPartObject(QWidget*, char const*, QObject*, char const*, char const*, QStringList const&) ()\n   from /opt/kde3/lib/kde3/konq_aboutpage.so\n#21 0x401d2fcf in KParts::Factory::createPart(QWidget*, char const*, QObject*, char const*, char const*, QStringList const&) (this=0x82f9708, \n    parentWidget=0x0, widgetName=0x0, parent=0x0, name=0x0, classname=0x0, \n    args=@0x0) at factory.cpp:44\n#22 0x400b7ef9 in KonqViewFactory::create(QWidget*, char const*, QObject*, char const*) () from /opt/kde3/lib/libkdeinit_konqueror.so\n#23 0x400a8e4d in KonqFrame::attach(KonqViewFactory const&) ()\n   from /opt/kde3/lib/libkdeinit_konqueror.so\n#24 0x40097496 in KonqView::switchView(KonqViewFactory&) ()\n   from /opt/kde3/lib/libkdeinit_konqueror.so\n#25 0x40097ca1 in KonqView::changeViewMode(QString const&, QString const&, bool) () from /opt/kde3/lib/libkdeinit_konqueror.so\n#26 0x400c22f5 in KonqMainWindow::openView(QString, KURL const&, KonqView*, KonqOpenURLRequest&) () from /opt/kde3/lib/libkdeinit_konqueror.so\n#27 0x400a3785 in KonqViewManager::loadItem(KConfig&, KonqFrameContainerBase*, QString const&, KURL const&, bool) () from /opt/kde3/lib/libkdeinit_konqueror.so\n#28 0x400a186d in KonqViewManager::loadViewProfile(KConfig&, QString const&, KURL const&, KonqOpenURLRequest const&, bool) ()\n   from /opt/kde3/lib/libkdeinit_konqueror.so\n#29 0x400a5acf in KonqMisc::createBrowserWindowFromProfile(QString const&, QString const&, KURL const&, KParts::URLArgs const&, bool, QStringList) ()\n   from /opt/kde3/lib/libkdeinit_konqueror.so\n#30 0x4008d2f0 in kdemain () from /opt/kde3/lib/libkdeinit_konqueror.so\n#31 0x0804865b in main ()"
    author: "Andr\u00e4 Steiner"
---
The KDE Project is pleased to announce the immediate availability of <a href="http://www.kde.org/announcements/announce-3.3.php">KDE 3.3</a>, the fourth major release of the award-winning KDE3 desktop platform. Over the past six months, hundreds of applications and desktop components <a href="http://www.kde.org/announcements/changelogs/changelog3_2_3to3_3.php">have been enhanced</a> by a community of developers, with a particular focus on integration of components.






<!--break-->
<p>
Stephan Kulow, KDE Release Coordinator, said: "The desktop reached a quality
hard to top in previous releases. Nevertheless, KDE 3.3 is a great
improvement and will further strengthen KDE's position as the leading Free
desktop environment."
</p>

<p>
Building upon previous releases, noticeable improvements in usability,
stability and integration have been achieved, enhancing the desktop
experience. A range of new applications and features have also been
implemented, including <a href="http://kolourpaint.sourceforge.net/">KolourPaint</a> (<a href="http://kolourpaint.sourceforge.net/screenshots.html">screenshots</a>), an easy-to-use paint program. Over 7,000
bugs have been fixed, and 2,000 wishes fulfilled, with over 60,000 lines of
code and documentation being written in the past six months. KDE is <a href="http://www.kde.org/info/3.3.php">now
available</a> in 89 different languages, with recent additions including Farsi
(Iran) and Indic character support.
</p>





