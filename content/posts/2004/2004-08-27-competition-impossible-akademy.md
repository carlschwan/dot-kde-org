---
title: "Competition (Im)possible at aKademy"
date:    2004-08-27
authors:
  - "ateam"
slug:    competition-impossible-akademy
comments:
  - subject: "kstart and kdcop"
    date: 2004-08-26
    body: "I don't think either of these have a particular section in the User Guide (yet!), but they'd also be bits that a developer could write a little bit about.\nFor kstart, a full description (with screenshots?) of the --type options would be particularly useful."
    author: "Philip Rodrigues"
  - subject: "Re: kstart and kdcop"
    date: 2004-08-27
    body: "Wow I've never even heard of Kstart... and most of KDcop's features don't seem to work (or im not using them right...) too bad I can't RTFM! ... or maybe RTFUG... yeah thats more accurate"
    author: "Corbin"
  - subject: "Re: kstart and kdcop"
    date: 2004-08-27
    body: "The type option set the window type to one of those listed in the NET WM specification:\n\nhttp://freedesktop.org/Standards/wm-spec/1.0/ar01s05.html#id2503078\n\nCheers\n\nRich."
    author: "Richard Moore"
  - subject: "WTFM!"
    date: 2004-08-27
    body: "Actually, its Write TFM!"
    author: "Jonathan C. Dietrich"
  - subject: " Competition Impossible "
    date: 2004-08-27
    body: "\"Competition Impossible\" sounds like a book about the dangers of software patents."
    author: "Frankie"
---
Attention <a href="http://conference2004.kde.org/">aKademy</a> participants! The mission, if you choose to accept it, is to update at least a single unwritten entry in the new KDE User Guide. You can see the current content <a href="http://users.ox.ac.uk/~chri1802/kde/userguide-tng">here</a>. The best submission received by Saturday 12 noon will win a book from the great collection of <a href="http://www.oreilly.com/">O'Reilly</a> books (including a signed Samba book, C++ Pocket References, and the entire Linux Webserver CD Bookshelf and much more to choose from). Read on for full details.



<!--break-->
<p>
This competition is only open to people at aKademy - we will be running similar open competitions in the future. Submit your work in plain text to kde-doc-english@kde.org before midday on Saturday (28th). Take a look at the content in Part I to get an idea of what we're looking
for.
</p>
<p>Rumors suggest that entries for Part II, Chapter 12 "Tinkering Under the Hood", covering the undocumented wonders of KDE's config files, daemons and underlying technologies, are in with a great chance.
</p>
<p>
The full book list, kindly donated by O'Reilly, is as follows:
</p>
<ul>
<li>Programming Qt (signed by Kalle, the author)
<li>Secure Coding Principles & Practices
<li>Linux in a Nutshell
<li>We the Media
<li>Webmaster in a Nutshell
<li>The Linux Webserver CD Bookshelf
<li>Linux Security Cookbook
<li>Managing IMAP
<li>Linux Server Hacks
<li>XML Hacks
<li>Linux Pocket Guide
<li>Linux Network Administrator's Guide
<li>Using Samba (signed by John H. Terpstra)
<li>Hackers & Painters, Big Ideas from the Computer Age
<li>Free as in Freedom
<li>C++ Pocket Reference
</ul>
<p>
For more information, contact Lauri Watts, Philip Rodrigues or Tom Chance.</p>
<p>
This announcement will not self-destruct in 5 seconds.</p>

