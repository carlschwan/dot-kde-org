---
title: "Linux Bangalore 2004: Call for Volunteers"
date:    2004-11-20
authors:
  - "swheeler"
slug:    linux-bangalore-2004-call-volunteers
comments:
  - subject: "KDE Hindi will be there too"
    date: 2004-11-20
    body: "We will also be running localized KDEs ( Hindi, Tamil etc) at our IndLinux (http://www.indlinux.org) booth.\n\nKarunakar"
    author: "G Karunakar"
  - subject: "Re: KDE Hindi will be there too"
    date: 2004-11-20
    body: "Of course we want to see pictures :) "
    author: "fab"
---
Sirtaj Singh Kang and I will be presenting talks at this year's <a href="http://www.linux-bangalore.org/">Linux Bangalore</a> conference at the Indian Institute for Science.  The fair runs December 1 - 3 and will feature tracks on various topics including Linux on the desktop.  I'll be doing a talk on searching and contextual linkage and Taj will be presenting a roadmap for KDE's future.


<!--break-->
<p>We will also have a KDE booth.  Ideally this booth will contain more than Taj, myself and my laptop.  This is where you come in.</p>
<p>Things we could use:</p>

<ul>
  <li>People to staff the booth</li>
  <li>People to organize printing of KDE promotional materials (poster, fliers, etc.)</li>
  <li>Machines with KDE 3.3.x installed (I'll have CVS on my laptop for showing that.)</li>
</ul>

<p>If you're interested in helping out, please contact the <a href="mailto:kde-promo@kde.org">KDE Promo mailing list</a> with how you'd be willing to help and what days you're available and I'll follow up and send further instructions.  If we end up having an overwhelming response I'll work out a booth schedule for those that are interested.</p>


