---
title: "K3b: Easy CD Burning Program for Linux"
date:    2004-11-22
authors:
  - "Eugenia"
slug:    k3b-easy-cd-burning-program-linux
comments:
  - subject: "Why only Linux?"
    date: 2004-11-21
    body: "Does K3B really depend on Linux?\nAnd since when does a KDE program run on the kernel?\n'Point & Click Linux!'? I only see Linux during boot-up. I've never been able to click the kernel."
    author: "Sorath"
  - subject: "Re: Why only Linux?"
    date: 2004-11-21
    body: "It seems according to IDC that Unix is dying and being replaced by Linux and Windows.  Take that as you will but it seems Linux has or is gaining the marketshare and so naturally most books will be targeted at it."
    author: "ac"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "\"Fear not, newbie! In this Point and click Gnu/XFree/KDE/Linux book we are going to describe the K3B CD/DVD burning, ISO creator and what else application that not only works in our Gnu/XFree/KDE/Linux system but on many other unix-like systems (*bsd, solaris, etc).\"\n"
    author: "User"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "Spare us your smart-a$$ism.\nLinux has become synonymous to the Linux PLATFORM. And so, you like it or not, Linux does represent the Platform Experience as a whole, and not just the kernel."
    author: "Disgusted"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "Well said."
    author: "Ian Monroe"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "so...if everyone say my chicken is a dog it is true?"
    author: "mjb"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "If \"everyone\" includes the Oxford dictionary, then yes."
    author: "ac"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "Do you use language to differentiate yourself from others, or to communicate with them?"
    author: "Iain"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "well, that could very well be, as my chick can do doggy style"
    author: "irony"
  - subject: "Re: Why only Linux?"
    date: 2005-10-03
    body: "Does your dog run from trouble?  Then your dog a chicken mon."
    author: "GinEric"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "AFAIK it also runs on FreeBSD."
    author: "vito gonzaga"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "well said; it also runs on FreeBSD :)"
    author: "anonymous"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "> Does K3B really depend on Linux?\n\nNo, but Point & Click AIX just didn't have the same ring to it.\n\n> And since when does a KDE program run on the kernel?\n> 'Point & Click Linux!'? I only see Linux during boot-up. I've never been able to click the kernel.\n\nShouldn't you be using Herd anyway?  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: "You mean Hurd?"
    author: "ac"
  - subject: "GNU Herd... ?"
    date: 2004-11-22
    body: "Was that supposed to be a joke? :) lol"
    author: "Darkelve"
  - subject: "Re: GNU Herd... ?"
    date: 2006-12-23
    body: "No, it is not a joke  :-|\n\n  http://www.gnuherds.org/GNU_Herds_Hackers_Guide.php#Current_work_team\n\nThat project is working to be part of The GNU project."
    author: "GNU Herds work team"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: ">Does K3B really depend on Linux?\n >And since when does a KDE program run on the kernel?\n\nactually k3b does use the but not directly kernel a lot. k3b uses cdrtools and cdrdao which are apps closely connected to the kernel (scsi and stuff), although it's a frontend for them. So yes K3b is definetly a Linux app :)\n\n>Point & Click Linux!'? I only see Linux during boot-up. I've never been able to click the kernel\n\nduring boot up u see init not linux :p"
    author: "Pat"
  - subject: "Re: Why only Linux?"
    date: 2004-11-22
    body: ">Does K3B really depend on Linux?\n\nYes, although there now is a BSD-port available\n\n> And since when does a KDE program run on the kernel?\n\nHmm, I thought that it actually did :) (not directly, but all software runs on the kernel in one way or the other. No kernel, no KDE, that's for sure :o) I'ts true that kde can run on multiple kernels, not only on the Linux-kernel, but apparently the autors of the book wanted to stick to a Linux-powered operating system in this book\nWich is a good idea, becaur every operating system, kernel, whatever has its own features etc..\n\n> 'Point & Click Linux!'? I only see Linux during boot-up.\n nope, that is init.\n\n\n> I've never been able to click the kernel.\nWell you can, go to /boot and click on vmlinuz or similar file.\n\nAC"
    author: "ac"
  - subject: "Concern."
    date: 2004-11-22
    body: "Dear god, who gave Eugenia post rights to the dot?\n\nUsually if we don't want to listen to her bikeshedding, we'd just not visit osnews. Now we'll have to listen to her half baked ideas and contempt for OSS programmers here too."
    author: "Robert"
  - subject: "Re: Concern."
    date: 2004-11-22
    body: "Go away, troll. In case you hadn't noticed, anyone can contribute to KDE Dot News.  Furthermore, your rant has nothing whatsoever to do with Eugenia's contribution.  Thread closed, and please don't start another one or it will be deleted and you'll be banned.\n"
    author: "Navindra Umanee"
  - subject: "k3b can't copy vcd :("
    date: 2004-11-22
    body: "It is very unfortunate that k3b can't copy vcd or mode2xa cdroms! are there any plans to support mode2xa in k3b. the latest cdrtools also won't help k3b.\n\ni have many cds with mode2xa mode. please support mode2xa cd copying. thanks. \n\napart from mode2xa k3b is just superb."
    author: "Fast Rizwaan"
  - subject: "Re: k3b can't copy vcd :("
    date: 2004-11-22
    body: "K3b can copy VCDs using cdrdao.  I think you have to create a BIN/CUE image (instead of ISO image) from the Tools menu.  If you want to do it from the command line it's something along the lines of:\n\ncdrdao copy --driver generic-mmc --device 0,3,0\n\nNo idea what mode2xa is."
    author: "Navindra Umanee"
  - subject: "mode2xa"
    date: 2004-11-22
    body: "mode2xa is a used for e.g. PlayStation CD-ROM:s. See http://www.brankin.com/main/technotes/Notes_ISO9660.htm"
    author: "Per"
  - subject: "Yes it can"
    date: 2004-11-22
    body: "See \"clone copy\" option, in the \"advanced\" tab, in the copy cd dialog. easy.\n"
    author: "Humberto Massa"
  - subject: "Re: k3b can't copy vcd :("
    date: 2004-11-23
    body: "There are plans for VCD.\nAnd mode2xa should work. At least it does here. Did you ever really try it? What was the result? What was the error if there was any?"
    author: "Sebastian Trueg"
  - subject: "K3b problemo with sessions"
    date: 2004-11-22
    body: "I posted this in K3b.org and no feedback, perhaps the .dot will save me :-(\n\nHi.\n\n\nRunning k3b 0.11.17 on Gentoo (2.6.8-gentoo-r3) . Drive is Plextor 708A (DVD Writer).Logged in as root user.\n\n\nI tried to add new files to a DVD+RW and rename some of the already present in the media. So I drag 'n drop and click \"Burn\" only to find some error with accesing the device. No burning.\n\n\nOk, I unmount the dvd and now K3b says it cannot find some of the compilation files (the ones already present in the DVD) but it burns.\n\n\nProblem: I only see the newer files, the files from the first session seems to have vanished. I guess serious screwup has happened. Not happy \n\n\nQ1: How can I recover the files already present on the DVD?. I suppose K3b didn't delete them so they must be \"somewhere\", right?\n\n\nQ2: What is the \"right\" way to add a new session to an already written CD/DVD+RW without messing it up?\n\n\nThanks."
    author: "Wheresmydata"
  - subject: "Re: K3b problemo with sessions"
    date: 2004-11-22
    body: "insert you cd/dvd, start a new data project, then menu->project->import session.\nThen add files/dir.\nAnd burn !"
    author: "Crisalide"
  - subject: "Re: K3b problemo with sessions"
    date: 2005-11-26
    body: "To recover the data in the \"deleted\" first session, you will have to mount the DVD using a special option, available in Linux kernels since 2.3.4. Use the \"session=1\" option. You will probably have to be root, and you may have to unmount the DVD first using \"umount\".\n\nI cannot be sure exactly what type of filesystem you have written on the DVD (udf, iso9660) so I cannot help you more.\n\nSee the mount man page (\"man mount\") for more details.\n\n"
    author: "RonK"
  - subject: "Guis suck"
    date: 2004-11-25
    body: "   It's actually easier to do it direct. A 2.6.9 + kernel even does away with the need for ide-scsi.\n\n You can open your app ,K3B in this case, but it takes time to open and then you have to click away.\n\n A script, well it's so small it hardly qualifies;).\n\n  Dupe (for data, in this case from cdreader to cdburner, copies the copy protection if it's there ;)\n\n   dd if=/dev/cdrom | cdrecord speed=? dev=/dev/cdburner -data -\n\n Save this line as a script called dupe and then any time you want to dupe a data cd stuff it in the reader and a burnable in da burner and type \"dupe\" ... you'r done.\n\n I got a million, well hundreds that do everything including beating what have you into complient .vobs for dvdauthor.\n\n You will never learn this extremly powerful aspect of *nix if you stay in yer gui. \n\n  PenGun\n Do What Now ??? ... Standards and Practices !\n\n"
    author: "PenGun"
  - subject: "Re: Guis suck"
    date: 2005-10-03
    body: "I'm curious as to what conceivable advantage is possible by \"A 2.6.9 + kernel even does away with the need for ide-scsi?\"\n\nI sure hope you're not talking about replacing it with the basic Commodore interface, called USB?\n\n1 wire = serial = 1x speed\n64 wires = IDE parallel = 64x speed\n80 wires = SCSI parallel = 80x speed\n\nUSB is a 50 year old loser!\n\nSCSI was way ahead of its time, and IDE and PCI are easily implemented in 64-bit architecture.  SATA is nothing but a doublewide.\n\nWhat standards and practices?  A Plug and Play one wire buss for all devices?  Someone out there needs to get real.\n"
    author: "GinEric"
  - subject: "Re: Guis suck"
    date: 2005-10-03
    body: "The driver ide-scsi was hack to access an IDE CD writer as SCSI device. This is not needed anymore with newer Linux versions, as it can be accessed as IDE device directly.\n\nAs for the \"wire\" calculations, you are missing that in IDE and SCSI most lines do not transport data. Therefore the speed increase is not so big.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Guis suck"
    date: 2005-10-03
    body: " Umm ... the point was we don't need to emulate SCSI anymore. You can just do cdrecord dev=/dev/dvd etc. You don't need ide-scsi in your lilo.conf no more. \n\n The rest of your post is wrong BTW.\n\n\n You'll need to peruse the cartoon network at http://adultswim.com and perhaps saunter on over to http://myspleen.net and aquire Aqua Teen Hunger Force cartoons to have any hope of understanding my sig.\n\n  PenGun\n Do What Now ??? ... Standards and Practices !\n\n"
    author: "PenGun"
  - subject: "Re: Guis suck"
    date: 2005-10-03
    body: "The emulator was always a bad idea, a substitute for actually writing a proper driver for each buss.\n\nMy post is absolutely right.  I design computers, busses, anything electrical or electronic.\n\nMost of what you're using is the design developed by my tutors, my employers, my colleagues, our team, and myself.  If I say a USB Buss is ancient history, it's ancient history; if I say it's slow, it's slow; if I say it's the cause of the \"small timing error\" that Microsoft refers to in its Blue Screen of Death crashes, it's various bugchecks, and Intel's various PCI, DMA, and other buss indirect addressing and alleged vector indirect addressing resulting in the wrong index being indexed, then it is all of the above.\n\nIf I say that full 64-bit architecture requires a full 64-bit Interrupt Request Line Buss, then it does.  And if I say Plug and Play and USB are just cheap ways that a company can cheat on quality to customers, they that is also true, as well as it being true that you can't run everything from single pair of twisted wires called a USB, and even remotely compare these to parallel processing.\n\nIt is a simple physical fact, governed by the laws of physics, that 64 wires can carry 64 times the throughput of one wire.\n\nIf you disagree with that, that's okay, but don't tell me that I and the laws of physics are wrong.\n\nbtw, I'm not the one out to crushthe.us nor send evilwebmail.com\n\nIt's easy to say someone is wrong; now try proving it with Maxwell's Equations.\n\nWhile you're pondering that, perhaps you want to look at the cdrecord source code and see if it is still emulating SCSI.\n"
    author: "GinEric"
  - subject: "Re: Guis suck"
    date: 2005-10-04
    body: " You are a moron sir, it's OK, have fun with it. Oh grab a SATA connector and count the wires, just for fun.\n\n You can be sure that if I went and purposly aquired and paid for an evilemail account, in this case @crushthe.us, I did it to annoy assholes like you.\n\n Have a nice day ;)\n\n  PenGun\n Do What Now ??? ... Standards and Practices !"
    author: "PenGun"
  - subject: "Re: Guis suck"
    date: 2005-10-04
    body: "Opps!  I seem to have fallen into talking to a group of:\n\nA.)  America haters\nB.)  Copyright Infringers\nC.)  teenagers with no formal education\nD.)  ill bred, ungroomed, and obnoxious ignorants whose main goal in life is to support their tiny self-confidence and superiority complexed egos by calling others names, thus raising their stature to \"legend in their own minds.\"\n\nkeep on buying what you buy, unless of course, you don't buy it but \"burn\" it.\n\nDo not threaten the U.S. or it's people, this is very unwise as you may wind up being the one crushed.\n\nIf you can't play nice, leave the playground.\n\nAnd take a really long look in the mirror when you get up in the morning and ask yourself \"Who is the moron?\"\n\nStand there for at least 15 minutes, staring blankly, to see if you can see who it is.\n\nI think you need an opthamologist and a neurologist; a psychiatrist wouldn't hurt you either.\n\nAre the authorities aware of your personality disorder?\n"
    author: "GinEric"
  - subject: "Re: Guis suck"
    date: 2005-10-03
    body: "Please do a little reading up on why the industry is moving away from parallel transfers:  http://www.hardwaresecrets.com/article/47\n\n(But as the others have said, this doesn't have anything to do with the removal of the ide-scsi hack as they just removed the emulation of a SCSI interface for IDE CD writers)\n\n"
    author: "cm"
  - subject: "Re: Guis suck"
    date: 2005-10-04
    body: "Everything your source Torres says is a newspeak lie.\n\nIf you really knew computers and the industry, you'd know that such propoganda, like that of Torres, is paid for advertising for those companies that make very cheap quality goods that do not last long and do not work.\n\nObviously, I have fallen into a group of children and amateurs who know nothing at all about computers or physics.\n\nIf you really believe that serial is faster than parallel, you should apply for a lifetime career at the nearest point and click McDonalds.\n\nYou've cited a source akin to Herodius, \"The father of all lies.\"\n\nWhat was it you said about reading up?  Ah yes, you said \"Please do a little reading up on why the industry is moving away from parallel transfers: . . .\"  Actually, I don't have to because I am writing up the books on the real reasons that the cheap manufacturers are moving away from parallel, because they are not educated in engineering, they are mostly business school graduates and children of rich computer industry innovators who haven't got a clue nor care about a quality product, only money, and because there are people out there stupid enough to believe that they are not being taken with things like USB and other pure fabrications of hype advertising.\n\nI'm an engineer; what are you?\n\nGet an education.\n\nI'm outta here; ignorant children annoy me.\n"
    author: "GinEric"
  - subject: "Re: Guis suck"
    date: 2005-10-04
    body: "> I'm outta here;\n\nThat's very much appreciated.  Thank you. \n\n"
    author: "cm"
  - subject: "Re: Guis suck"
    date: 2005-10-04
    body: "Don't worry cm, you've made the list of undesireables and someone will keep an eye on you, just in case we can't find a suspect in some 711 heist or a a mad bomber attempt.\n\nWe keep track of that child that \"A Christmas Carrol\" warns of being wary of most of all, \"ignorance.\"\n\nMaybe you'll change when some good woman pulls you off that high horse you've been riding all this time.\n\nYou may not have seen the last of some people; do you feel like you have to look over your shoulder constantly?\n\nIt's probably the F.B.I. behind you; not to worry, your small fish and potatoe chips.\n"
    author: "GinEric"
  - subject: "Re: Guis suck"
    date: 2005-10-04
    body: "Yawn.  \n\nI've seen much better trolls than you.  Not as insulting but much more entertaining. \n\nCome on, make an effort, you can do better than that! \n\n"
    author: "cm"
  - subject: "Re: Guis suck"
    date: 2006-10-30
    body: "Eric you're pathetic, probably some 85 year old man who has nothing better to do than taunt people who know less than you.. you're one of those people who get a couple years of engineering and then think they know more than everyone put together, you make me so sick I want to vomit until I pass out.. how about passing some helpful knowledge instead of trolling and insulting people?"
    author: "Mike Baker"
---
<A href="http://www.k3b.org/">K3b</a> is a CD and DVD burning program for Linux. <A HREF="http://www.informit.com/articles/article.asp?p=349045">This online chapter</A> from the book '<a href="http://www.informit.com/title/0131488724">Point &amp; Click Linux!</a>' by Robin Miller will help you get started with this useful program right away.








<!--break-->
