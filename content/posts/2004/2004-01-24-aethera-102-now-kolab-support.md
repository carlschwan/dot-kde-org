---
title: "Aethera 1.0.2 Now with Kolab Support"
date:    2004-01-24
authors:
  - "sgordon"
slug:    aethera-102-now-kolab-support
comments:
  - subject: "Nothing to remember..."
    date: 2004-01-23
    body: "I think it was V1.0 when I tried it. What I know is, that Aethera have to much space wasted for graphics and it feels a lot buggy.\n\nIs it better now?\n\nIt has no features for a private person to sync contacts, calendar entries and tasks with a phone, Palm or PocketPC.\n\nSo it looks like a buggy mail-only program with bloated graphics, a propietary calendar that is only compatible to himself and only can be read on a pc.\n\nOk, it has a Kolab plugin now. But I don't know how much private pc's sync with your brother's and sister's calendar entries on a Kolab-Server ;-)\n\nInstead I'm very happy to have Kontact :-) Hopefully, synce-kde can soon sync with KAB and Korganizer again.\n\nFor my Sony T610, I can only use (for now) Evolution with multisync(.sourceforge.net)\n\nMaybe a kSync (like iSync on OS-X) can soon sync everything in KDE?? This would be not only a very nice feature on a private PC ;-)\n"
    author: "anonymous"
  - subject: "Re: Nothing to remember..."
    date: 2004-01-23
    body: "The Kolab server as we all know came out of the Kroupware project. Aethera uses korganizer as calendar app, just like your Kontact. There is a KMail version with Kolab support, but the integration into the main branch almost stopped a fews month ago I think. Perhaps there could be at least two Kolab clients in the near future.\n"
    author: "mdo"
  - subject: "Re: Nothing to remember..."
    date: 2004-01-24
    body: "Hi,\nYes, I have fixed most of the reported bugs.\nWhy do you say it cannot synchronize calendar and tasks ?\nKOrganizer plugin has mail support from MailCenter and also it could export a calendar in ical format even in 1.0. I cannot see a difference here between KOrganizer app and DateBook (KOrganizer plugin).\nI don't see as a minus the fact that Aethera can manage many calendars.\nContacts can be imported/exported as VCARD and this makes possible the Kolab synchronization too.\nEven more, for Kolab support, the notes are synchronized too.\nMails are in mailbox format so that it can be exported easily.\n\nSince Aethera can use these standard formats I am sure the next release will have synchronization with PDA devices.\nRegards,\nEug\n"
    author: "Eug"
  - subject: "Re: Nothing to remember..."
    date: 2004-01-25
    body: "> Why do you say it cannot synchronize calendar and tasks ?\n\nSorry if I'm wrong, I wrote only this one:\n\"It has no features for a private person to sync contacts, calendar entries and tasks with a phone, Palm or PocketPC.\"\n\nDo you mean, I can sync Aethera with a Phone, Palm and/or PocketPC? How can I do that? I only see a Kolab-Plugin now, but no Sync support for mobile devices!\nI don't run a Kolab-Server at home and I think many other peoples don't do that (and because of this, one phrase of my last post was meant ironically...)\n\nWhat about graphics? Can I change the symbols/icons from big to small and vice versa?\n"
    author: "anonymous"
  - subject: "Re: Nothing to remember..."
    date: 2004-01-26
    body: "Aethera doesn't synch its data directly to the phone etc.\nThis I have to do for the next release.\nBut I am pretty sure you can use the exported data (ical format for Korganizer) to sync it with your phone. And I don't mean using Kolab server.\n\nYou are able to hide(or close) the top banner or even your folder tree view (if you want to work only with one folder) and this can be a lot more space.\nBut I don't have icons with different sizes so this isn't possible.\nWell, all the icons are outside of the binary... if someone can offer a set of smaller images, I can do the option for the next release.\n"
    author: "Eug"
  - subject: "Good stuff.."
    date: 2004-01-23
    body: "Good to see Aethera finally in a usable state. Kolab support before Kontact has full support is nice too :)"
    author: "anon"
  - subject: "Good job!"
    date: 2004-01-24
    body: "More than a year ago or so I looked at Aethera, and it was not good enough yet. Now it looks great, especially for the ones used to Outlook etc. I will definetely consider it for the next one I try to convert from windose to Linux/kde.\n\nI think it is great to have more than one PIM solution for KDE. Even more when I see that they can work together so easily.\n\nGood job!!!"
    author: "Matt"
  - subject: "Alternate download location"
    date: 2004-01-28
    body: "Something appears to be wrong with Sourceforge mirrors at the moment, so we've also put the files at www.smga3000.com/thekompany/Aethera/  make sure to read the 'demo-readme' file."
    author: "Shawn Gordon"
  - subject: "Kolab not working on any redhat machine"
    date: 2004-01-29
    body: "Kolab sounds great but it really needs to sort out its openpkg as I have tried and no workee"
    author: "johnjones"
---
After months of hard work, the multi-platform email/PIM client <a href="http://www.thekompany.com/projects/aethera/">Aethera</a> now has integrated support for the <a href="http://www.kolab.org/">Kolab</a> groupware server.    Aethera 1.0.2 comes with support for email, calendar and todo using <a href="http://korganizer.kde.org/">KOrganizer</a> as a plug-in, sticky notes, address book and now Kolab integration.  There are optional commercial plug-in modules for an advanced and integrated Jabber client, Whiteboard client and Voice over IP application, all integrated in to the environment.  Everything but the VoIP is available on both Linux and Windows. For full details on what else is new, <a href="http://www.thekompany.com/projects/aethera/chlog.php3">see the change log</a>. Aethera is fully GPL and <a href="http://www.sf.net/projects/aethera/">available on Sourceforge</a> for download.  We'd be happy for contributors and comments.

<!--break-->
