---
title: "Quickies: Thesis about Kolab, Task Juggler IDE, Helix-Qt, Blast from the Past"
date:    2004-10-10
authors:
  - "fmous"
slug:    quickies-thesis-about-kolab-task-juggler-ide-helix-qt-blast-past
comments:
  - subject: "Palm"
    date: 2004-10-10
    body: "Does interaction withb Palm work?"
    author: "hein"
  - subject: "Re: Palm"
    date: 2004-10-10
    body: "perhaps it uses kitchensync ? so it can sync not just with this palm things that no one has."
    author: "chris"
  - subject: "Re: Palm"
    date: 2004-10-10
    body: "no one has? It is perfect, it is cheap. There is a lot free sodftware for the palmOS. "
    author: "hein"
  - subject: "Re: Palm"
    date: 2004-10-10
    body: "sure - if you mean everyone - then you perhaps can say everyone has a handy to sync, but not a palm .->"
    author: "chris"
  - subject: "TaskJuggler demo @ Berlinux 2004"
    date: 2004-10-10
    body: "<p>Check out the KDE crew at <a href=\"http://dot.kde.org/1097408288/\">Berlinux 2004</a> where they are showcasing TaskJuggler IDE.</p>\n\n\n<p>Ciao</p>\n\n\n<p>Fab</p>"
    author: "Fabrice Mous"
  - subject: "hmm"
    date: 2004-10-10
    body: "Everything is qtfied and looks good. But some projects stick to GTK such as Lazarus.freepascal.org -- really sad."
    author: "hein"
  - subject: "Re: hmm"
    date: 2004-10-10
    body: "try to convince them. if they all use gnome as their primary desktop they wont rewrite it to use QT. What do they want with Kde dialogs in gnome ?!?!?"
    author: "chris"
  - subject: "Re: hmm"
    date: 2004-10-10
    body: "\"At this point in the development we are using gtk+ as our initial API widget set. Some work is also being done with Qt and the Win32 API..\"\n\nSee:\nhttp://www.lazarus.freepascal.org/modules.php?op=modload&name=StaticPage&file=index&sURL=about"
    author: "Richard Dale"
  - subject: "KPlato"
    date: 2004-10-10
    body: "What will happen to KPlato? Will it be dropped?"
    author: "Anonymous"
  - subject: "Re: KPlato"
    date: 2004-10-11
    body: "Why should it? It's a different effort."
    author: "Anonymous"
  - subject: "name for TaskJuggler IDE"
    date: 2004-10-11
    body: "projeKt\n\nTIDE (Taskjuggler IDE)\n\n\nJust some ideas\n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: name for TaskJuggler IDE"
    date: 2004-10-11
    body: "What about 'taakje'.\n\nTasks And All Kde Jobs Editor\n"
    author: "pieter"
  - subject: "Re: name for TaskJuggler IDE"
    date: 2004-10-13
    body: "Unfortunately TIDE is already taken ...\n\nWhat about \"TaskForce\" (suggested by Torsten Rahn on taskjuggler-devel)?"
    author: "Joachim Werner"
---
The <a href="http://www.kolab.org/">Kolab project</a> 
<a href="ftp://ftp.kolab.org/kolab/contrib/diplom_thetis_stoermer">has published</a> a 
thesis by Magnus Stoermer about Kolab the Free Software groupware solution.
Although the language is German it has a <a href="http://creativecommons.org/licenses/by/2.0/de/">very liberal licence</a>
which shouldn't stop anyone from translating this publication. *** 

<a href="http://www.taskjuggler.org/">TaskJuggler</a> has started a subproject called 
<a href="http://www.taskjuggler.org/ide.php">TaskJuggler IDE</a> which aims to replace ktjview. 
<a href="http://www.taskjuggler.org/screenshots_ide.php">Check it out</a> and go fetch those 
<a href="http://www.taskjuggler.org/download/taskjuggler-cvs.tar.bz2">daily CVS snapshots</a> and start participating 
in this project as a developer, tester or translator. Everyone can help here as this improved KDE frontend to TaskJuggler is also in need of a better name. ***
 

A <a href="https://player.helixcommunity.org/2004/developer/qt/">new page has popped up</a> at the 
<a href="https://helixcommunity.org/">Helix Community website</a>. Are we going to see a Qt frontend to the Helix Player which is currently using a 
default <a href="https://player.helixcommunity.org/2004/common/creative/screenshots/gtk_hxplay_m2_shot.png">GTK gui</a>?   ***

In our section <em>Blast from the Past</em> someone showed me <a href="http://www.geocities.com/SiliconValley/6702/kmail.html">this page</a> on IRC. Well isn't that a spiffy KDE application? 


<!--break-->
