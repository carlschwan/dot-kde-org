---
title: "Quickies: KolourPaint, KWin Deco Howto, Desktop Theming, Boson, Opie"
date:    2004-05-13
authors:
  - "fmous"
slug:    quickies-kolourpaint-kwin-deco-howto-desktop-theming-boson-opie
comments:
  - subject: "Mozillux"
    date: 2004-05-13
    body: "Do they plan to switch the button order in Firefox back to the logical order?  I would pay real money for that change--forget themes!"
    author: "ac"
  - subject: "Re: Mozillux"
    date: 2004-05-13
    body: "I got this from the MoIllazine Firefox forum. It's not perfect, but it helps.\n\nPut this in your userChrome.css file:\n\n~/<path to profile>/chrome/userChrome.css\n\n\n/*\n*  swap OK and cancel buttons in dialogs - from logan on Firefox discussion forum\n*/\n.dialog-button-box { -moz-box-direction: reverse; -moz-box-pack: right; }\n.dialog-button-box spacer { display: none !important; }\n\nIf you need help locating the config files, see\n\nhttp://texturizer.net/firefox/edit.html\n"
    author: "frank"
  - subject: "I might include this in the next release"
    date: 2004-05-14
    body: "Interesting. I didn't knew about this. I'll test it and if it works properly I'll include it in the next release of Plastikfox."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "\nThere is no ONE logical order. \n\ndont claim nonsense"
    author: "getit"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "You mean at your place it's possible to order stuff from negative to positive while speaking without sounding funny? (ie. eg. \"Do you want to go to cinema, no or yes?\", \"Not to be or to be?\", \"Do you have no car or a car?\")"
    author: "Datschge"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "Yes, that is possible. Never heard of other languages than English?"
    author: "Andre Somers"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "Do you think Datschge is a native English speaker? ;-)"
    author: "Andras Mantia"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "No idea, but judging from his post, it is a possibility. Point remains that there is no one \"right\" order for buttons, it all comes down to what is being expressed by the dialog, in what language, in what cultural context, etc. The way KDE has adopted not using \"Yes\" and \"No\" but things like \"Save\" and \"Disgard\" instead helps a lot here, I think."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "As long as you don't have the Microsoft style of error messages which go something like this:\n\n\"\"\"\nWhat do you want, then?\n(Press \"OK\" to make something or other happen, or press \"Cancel\" to do something else.)\n\n[OK] [Cancel]\n\"\"\"\n\nAt which point, you want an extra button: [What was the question?]"
    author: "The Badger"
  - subject: "Re: Mozillux"
    date: 2004-05-15
    body: "I'm German and I don't think there are many cases in German where negative to positive ordering sound good unless you are making a (then often cynical) rhetorical question. You are right that this depend on the language and the respective cultural context. I believe that the best possible user interface uses natural (not technical) language, and there the KDE policy not to use \"Yes/No\" but written out descriptions is, as you wrote, really dead on. Ultimately I think dialogs windows (i.e. short text followed by multiple choice of what to do next) should be constructed in an order which mirrors the flow of the actual language in use, and up to now I only know of languages which basically describe negatives as extension of positives and not vice versa. I don't mind at all if there is a language proving me wrong though. ;)"
    author: "Datschge"
  - subject: "Re: Mozillux"
    date: 2004-10-11
    body: "> Point remains that there is no one \"right\" order for buttons\n\nNot in terms of the overall environment, perhaps, but certainly for individual applications.  If all the other applications have the affirmative action on the left, and a single application has the affirmative action on the right, that's a big usability issue.  I lost a lot of bookmarks in Firefox when they changed the order, as I was clicking the wrong button almost as soon as it came up.\n\nSo, in this context, there is certainly a \"right\" order for the Firefox buttons, and it doesn't use it by default.\n"
    author: "Jim"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "So KDE has Shakespearean dialogs?\n\nSorry, couldn't resist;)\n\nMarc\n"
    author: "Marc"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "In fact in programming, as well as math, for classical truth values we always have False < True. Just try it in C, Pascal, Haskell, whatever, or read some books on logic ;)"
    author: "John C.S. Freak "
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "I'd also prefer the No..Yes order, because (1) I've had it that way under Atari TOS. :-) And (2): I find it more logical to have the \"positive\" button (Yes, OK, etc.) at the bottom right corner in LR languages (likewise for RL). Rationale: as we are reading from left to right, from top down, the bottom right corner is typically the point where we finish a page. It's the logical end of a page. And (3) it's a lot easier to spot than the second, or third, or ... bottom from the right. But in the end it's a matter of taste ... and I'll eventually fix that in my kdeui, anyway ..."
    author: "Melchior FRANZ"
  - subject: "Re: Mozillux"
    date: 2004-05-18
    body: "Woohoo ... thanks to Waldo's commit from yesterday, it's now possible to switch the button layout with one simple entry in ~/.kde/share/config/kdeglobals, section \"[KDE]\": ButtonLayout=1\n\nNo need to patch the sources, and \"OK\" on the right place (pun intended :-)."
    author: "Melchior FRANZ"
  - subject: "Re: Mozillux"
    date: 2004-05-18
    body: "This would have been more important: http://tinyurl.com/24oo7"
    author: "anonymous"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "Firefox is a GTK app inteded for GNOME, not KDE. The button order is recommended by the GNOME human interface guidelines which Firefox complies with. On the Win32 platform, it follows the Microsoft guide lines.\n\nThis button order has been debated 10,000 times before, if you complain the GNOME developers will just flame you, so don't make complaints about the button order because the reasons have been posted in the GNOME guidelines.\n\nThe Mozilla Organization and GNOME are very integrated, and it shows in their apps."
    author: "norman"
  - subject: "Re: Mozillux"
    date: 2004-05-14
    body: "\"Firefox is a GTK app inteded for GNOME, not KDE.\"\n\nActually, Firefox is a GTK app intended for no single desktop. Just as not everything written in Qt is a KDE app, not everything written with GTK is a Gnome app."
    author: "David Johnson"
  - subject: "Re: Mozillux"
    date: 2004-05-25
    body: "That's what we'd like, but unfortunately that's not the way it is. Firefox *is* intended for GNOME. It's UI shows that. The parent poster is right in saying the folks from Mozilla and GNOME are very close. The KDE people don't seem to reach for such integration for the simple reason they (we, including myself as a user) have their (our) own browser."
    author: "Hisham Muhammad"
  - subject: "Good news"
    date: 2004-05-15
    body: "I've found the way to reverse the button order with no problems. It works perfectly and everywhere so you'll feel happy with Firefox. I'll release a new version of Plastikfox very soon with a checkbox you'll have to activate during the installation to get this feature."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Updated!"
    date: 2004-05-15
    body: "I've posted a new version of Plastikfox. You can now install a patch to get the button order reversed. Just set the checkbox and the patch will be installed together with the theme. In fact, it's checked by default under Linux. :) Under other OSes it will remain unchecked by default but you can still check it if you want."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Mozillux"
    date: 2005-05-13
    body: "The fact of the matter.... All flames and arguments aside!!! is that any diviance from what other programs are doing on a platform breaks usability.... Assumably most users using ix86-linux will have ether learned on windows, still used windows, use other programs that uses the OK/Cancel convention, or have some background with the OK/Cancel convention.  Thus it has little to do with the argument that gnome makes towards it being more logical for the brain.... My priest used to always say \"People are like cows, they have there stalls.\"  Meaing we are creatures of habit....  When i go to clik on the OK button with out thinking just because I konw where it is like the G on the keyboard and insted i get cancel where is the logic in that.... it like for KDE lets use the qwerty layout gnome oh its bettter devorak layout....  If your going to deviate give the option at runtime to customize it.....  So thumbs down for gnome2 from me for that one.... completly counter productive, and it supose to make me more productive.\n"
    author: "bradg"
  - subject: "KolourPaint"
    date: 2004-05-13
    body: "I think that KolourPaint is great. It can do everything I was able to do with Paint in Windows a couple years back and this is all I need in 90% of the cases. Until now I always tried to use KPaint but it was unusable, so that I often had to use Gimp, whose usability still baffles me every time I open it."
    author: "Stephan Richter"
  - subject: "Re: KolourPaint"
    date: 2004-05-14
    body: "I agree completely. It's even better than paint for some things.\nI'm amazed to see that it's rock solid already. At first I had expected something\nlike kpaint, but it's rock solid.\nThanks Kolourpaint guys!"
    author: "dwt"
  - subject: "opie for cellphones"
    date: 2004-05-14
    body: "on trolltech website there's a phone edition and a pda edition, is there a phone edition for opie too? that would be cool to get a full opensource phone :) cause I know the pim part of qtopia is not free IIRC"
    author: "Pat"
  - subject: "Re: opie for cellphones"
    date: 2004-05-14
    body: "Nope. No opie for cellphones. Probably won't be one anytime soon either."
    author: "Greg Gilbert"
  - subject: "boson"
    date: 2004-05-15
    body: "Boson? Is it usable??\n\nI only need a chess client but I cannot compile knights. What's wrong?"
    author: "gerd"
  - subject: "Re: boson"
    date: 2004-06-05
    body: "About boson: IMO it is useable"
    author: "Felix"
---
<A href="http://kolourpaint.sourceforge.net/">KolourPaint</A> 
is progressing nicely as the latest version
brings <A href="http://kolourpaint.sourceforge.net/screenshot0_big.png">new icons</A> by Kristof Borrey, 
a new user manual and numerous bug fixes and improvements. 


While browsing at <A href="http://www.kde-look.org/">KDE-Look</A> 
I noticed <A href="http://www.kde-look.org/content/show.php?content=6332">this howto</A> for 
making native KWin decorations. Now there are no more excuses for not writing that decoration you 
always wished someone else would do! 

Always wanted a cool KDE (Crystal) look 'n feel for your GTK-apps? 
The <A href="http://www.crystalgnome.org/">crystalgnome.org</A> site released the 
<A href="http://www.crystalgnome.org/kidsicons.htm">Kids icon set</A> for GNOME 
(<A href="http://www.kde-look.org/content/show.php?content=9144">KDE version</A>) and is 
also offering <A href="http://www.crystalgnome.org/index2.htm">Crystal themes</A>.

A project named <A href="http://www.polinux.upv.es/mozilla/">Mozillux</A> has 
a similar goal, to integrate <A href="http://www.mozilla.org/">Mozilla</A> and 
<A href="http://www.mozilla.org/products/firefox/">Firefox</A> better in the KDE desktop. 



Looking for a cool real-time strategy game for KDE? Wait no more, 
<A href="http://boson.eu.org/screenshots.php?show=44">just</A> 
<A href="http://boson.eu.org/screenshots.php?show=43">check</A> 
<A href="http://boson.eu.org/screenshots.php?show=35">out </A>
<A href="http://boson.eu.org/">Boson</A>.  
The Boson hackers <A href="http://boson.eu.org/announces/boson-0.10.php">recently released</A> 
the latest version of this game.

The Opie Team <A href="http://opie.handhelds.org/cgi-bin/moin.cgi/News">announced</A> 
the first public release of the Opie 1.1.x branch - version 1.1.3.  
The <A href="http://opie.handhelds.org/">Open Palmtop Integrated Environment</A> is a 'fork' of <A href="http://www.trolltech.com/products/qtopia/index.html">Qtopia environment</A> 
developed by <A href="http://www.trolltech.com/">Trolltech</A>. 



<!--break-->
