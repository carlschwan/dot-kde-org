---
title: "Knoppix 3.6 'aKademy Edition' Announced at KDE World Summit"
date:    2004-08-24
authors:
  - "ateam"
slug:    knoppix-36-akademy-edition-announced-kde-world-summit
comments:
  - subject: "Well..."
    date: 2004-08-23
    body: "I'm not really sure what to say... all I can think of is NX is a cool name...\n\nIs FreeNX an open source version of NoMachine's NX thingy?\n\nAlso what does the *NX servers have to do with KDE's speed (I get that it increases the speed of a remote desktop connection, but thats increasing X's speed, not really KDE)?"
    author: "Corbin"
  - subject: "Kalyxo server"
    date: 2004-08-23
    body: "Hello, we have intermittent server hassles (migrating to a different box: changing dns, moving the data, etc). I hope we'll be back up fully operational tonight. In the meanwhile, there is the news item from the site:\n\n23.08.2004: FreeNX packages in experimental\n\nThe long awaited FreeNX server is finally here! Apt-get it from:\n\ndeb http://www.freedesktop.org/~mornfall/debian/ experimental main\n\nAfter installing it (by =apt-get install nxserver=), run, as root =nxsetup=, then add your users (again as root), like \"nxserver --adduser <username>\" and \"nxserver --passwd <username>\". The user must exist in the system database first. When you have done this, you can run nxclient (get it from http://www.nomachine.com)."
    author: "mornfall"
  - subject: "Re: Kalyxo server"
    date: 2004-08-24
    body: "Great!\nThanks for the GPL'd deb packages!\n\nBut:\nroot@server:~# nxserver --password user\nNX> 100 NXSERVER - Version 1.4.0-01 OS (GPL)\nNX> 500 Error: Function --password not implemented yet.\nNX> 999 Bye\n\nAnyway to set the user-passwd manually? \n\nGreetings\nKaeptnB"
    author: "kaeptnb"
  - subject: "Re: Kalyxo server"
    date: 2004-08-24
    body: "pengu:[masuel]% sudo nxserver --passwd masuel\nNX> 100 NXSERVER - Version 1.4.0-01 OS (GPL)\nNew password:\nPassword changed.\nNX> 999 Bye\n\nits passwd not password"
    author: "masuel"
  - subject: "Re: Kalyxo server"
    date: 2004-08-24
    body: "oops...\n\n;)"
    author: "kaeptnb"
  - subject: "Re: Kalyxo server"
    date: 2004-08-24
    body: "I needed to type:\n\n% sudo mkdir /etc/nxserver\n% sudo touch /etc/nxserver/passwords\n\nThen it worked"
    author: "Corrin"
  - subject: "Re: Kalyxo server"
    date: 2004-08-24
    body: "Looks really neat, \n\nAdding a user seemed to go fine, but so far I have been unble to connect to it.  I tried checking the history and got:\n\nscrappy:/home/bloodpup# nxserver --history\nNX> 100 NXSERVER - Version 1.4.0-01 OS (GPL)\n/usr/bin/nxserver: line 793: nxnode_start: command not found\nNX> 999 Bye\n\nAre there any docs up yet?  I have not been able to find any."
    author: "theorz"
  - subject: "Re: Kalyxo server"
    date: 2004-08-24
    body: "You can use the Client-Deb from http://debian.tu-bs.de/knoppix/nx/nxclient-1.4.0.tar.gz.\n\nThat one works.\n\nThe one on the Knoppix also works.\n\n1.3.2 should also work.\n\ncu\n\nFabian"
    author: "Fabianx"
  - subject: "Re: Kalyxo server"
    date: 2004-08-25
    body: "i have the same problem , how do i solve it ?"
    author: "chris"
  - subject: "Re: Kalyxo server"
    date: 2004-08-25
    body: "ok. nxserver works fine now.\n\nBut neither VNC nor KDE/CDE/GNOME/Terminal/Custom things start up (when selected in the Client)\nOnly thing i see is blank nxclient window.\n\nAlthough i can start thinks manualy on the server, e.g.\n\"export DISPLAY=:1000 ;wmaker\"\nthis way i finaly got a viewably nx-setup ;)\nbut i doubt there's no easier way to get a working session.\n\nAnother thing: On LinuxTag i saw \"suspending\" and \"resuming\" of X/VNC/RDP-Sessions. To ask the Windows-way: Where's the button for this ? ;)\n\nGreetings\nKaeptnB"
    author: "kaeptnb"
  - subject: "Source debs"
    date: 2004-08-25
    body: "Where are the source debs?"
    author: "Ed Warnicke"
  - subject: "Tutorial"
    date: 2004-08-23
    body: "Can anyone ask mr knopper to publish the tutorial?\nOr if someone has the presentation can he upload it somewhere"
    author: "anonymous"
  - subject: "KDE version"
    date: 2004-08-23
    body: "Is there any specific reason why KDE 3.2.2 was chosen while 3.3.0 has been released at the beginning of the same event? I think it would have been much cooler if it featured the latest and greatest KDE ever: 3.3.0!"
    author: "Andre Somers"
  - subject: "Re: KDE version"
    date: 2004-08-23
    body: "As Knoppix is basically Debian, I run Debian unstable and not all KDE 3.3 packages have made it to unstable yet."
    author: "ac"
  - subject: "switch to DVD"
    date: 2004-08-24
    body: "Looks very good. What Knoppix needs now is to switch to DVD."
    author: "ac"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "What you need is to read Knoppix homepage:\nKNOPPIX 3.5 LinuxTag 2004 DVD Edition\n;)"
    author: "Isaac Clerencia"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "Yeah,\nbut the DVD-Editions of Knoppix are not available on the net, but can only be ordered via mail.\nThat is the thing that should change - most people by now own a DVD-recorder (which until now was the argument for knoppix not to switch), recorders are at 50-80 euro, so affordable for almost everybody.\n\nbtw: Linux Tag Edition DVD is great, have installed it on several systems without any major problems"
    author: "Tobias"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "I dont have a dvd burner, can't afford one, and if I could, the DVD is much larger - so it whould take much longer to download, increase the load on the servers etc.\n\nI think this is the best way: download the free cd version, buy the (cheap) dvd and support knoppix. if you can spend 50-80 euro on a DVD burner (I dont even have a DVD reader) you can spend E 5-10 on a DVD sent to your home."
    author: "superstoned"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "Where do you live? Most people I know do not have DVD burners. In fact I know very few people who do. That said it would be nice if both were made available for download."
    author: "John Savings Freak"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "Why don't you build your own DVD and make it public via Bittorrent?\nIf EVERYBODY wants a DVD instead of CD, than it will be a piece of cake to make it worldwide available."
    author: "anno"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "Reality check: I don't own a dvd recorder, and I know _nobody_ who owns one. DVD recorders will be standard in a couple years. And this is assuming the industry stops fscking around with incompatible formats:\n\nhttp://www.dvd-recorders-guide.com/dvd-recorder-formats.html"
    author: "MandrakeUser"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "    Here almost everybody who likes computers has a DVD burner. It just doesn't make sense to buy a computer with a CD-ROM burner now, and to buy a brand new burner, as stated in the thread, is just 50-80. Come on, even 4 extra-pizzas and a six pack can be more expensive ;)"
    author: "Leny"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "You make it sound inexpensive like that, but it's all a matter of perception.\nYou can also say \"Come on, even a dinner at Bobby Flay's Mesa Grill in New York can be more expensive!\" (and Mesa Grill is rather expensive).\n\nHow much of the money I spend on a DVD burner goes to the MPAA?\n\nRoey"
    author: "Roey Katz"
  - subject: "Re: switch to DVD"
    date: 2004-08-26
    body: "What about those of us who do not want to buy (or cannot afford to buy) a new computer? My 2 year old laptop doesn't even have a CD burner but it is still very usable for Linux/KDE. There is no need for me to buy a new PC, and YES I do like computers but no I don't want to waste money buying things I don't need. Or cannot use as the laptop isn't upgradeable."
    author: "George Moody"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "look at the prices of dvd recorders , and you see imidiatly that *everyone* has one. \nthe only people that have none are you and your friends."
    author: "chris"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "This is not true. I bought my computer last year (medion). It has nearly everything you could expect from a computer at that time (cd RW, DVD reader, smart card readers,..) but it does'nt have a DVD writer...\nNot everyone does upgrade or buy a new computer each year."
    author: "veton"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "At least here in California almost everyoen I know has a DVD burner.\n\nMy family has oen for each computer, even the laptop. Though we only have one dual layer DVD burner."
    author: "Alex"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "you are the minority - face it - and dont complain."
    author: "chris"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "Maybe I am missing something ?\nMaybe Free Software is about helping each other instead of being anal ?\n\nIn the end, this is nosense. The best way to do this is to open up a poll in the project page for knoppix. And ask Knoppix users if they want the switch."
    author: "MandrakeUser"
  - subject: "Re: switch to DVD"
    date: 2004-08-24
    body: "yes but there always people saying of i have no Feature A (dvdplayer) so why should the world need this. This really sux - those people have a problem , they have nothing to say but want to express for the whole world."
    author: "chris"
  - subject: "Re: switch to DVD"
    date: 2004-08-25
    body: "Does the fact that Knoppix is distributed in a CD format prevent you from using it then? I thought DVD burners can burn CD's too? If that's the case, the situation is exactly the opposite from what you claim: \"I have feature A, what do I care if the rest of the world does not and would not be able to use the software?\"\n\nAndr\u00e9\nnot owning a DVD burner either."
    author: "Andre Somers"
  - subject: "Re: switch to DVD"
    date: 2004-08-25
    body: "> Does the fact that Knoppix is distributed in a CD format prevent you\n> from using it then?\n\nIn a way it does. A CD is too small for a modern distro, so\nit must be crippled to fit on that medium."
    author: "ac"
  - subject: "Re: switch to DVD"
    date: 2004-08-26
    body: "Chris,\n\nYou obviously live in a world where your parents have spoilt you by buying all the new toys and by them not pointing out that you are a priviledged little pillock (an English word for idiot who knows no better). Not everybody has a DVD recorder for TV or a DVD burner in their PC, in fact most of the world's population do not have a PC.\n\nGo get a life and an understanding of the real world, not that one you inhabit in your bedroom."
    author: "Annoyed Parent"
  - subject: "Re: switch to DVD"
    date: 2004-08-25
    body: "heres the torrent :)\n\nhttp://62.212.84.226/torrents/Knoppix-3.5-DVD-remaster.iso.torrent\nhttp://66.90.75.92/suprnova//torrents/2410/Knoppix-3.5-DVD-remaster.iso.torrent\n\n\nhave fun"
    author: "marcel"
  - subject: "Re: switch to DVD"
    date: 2004-08-28
    body: "Thx for the torrents :)\n\nbtw: I didn't want to start a discussion about wether or not a dvd recorder is common hardware and \"affordable\". It was just my experience from the people I know that by now 8 out of 10 of my friends own a recorder, the medium is by now more cost efficient than cds, ...\nOf course there should still be cd-version, knoppix should be runnable on almost all PCs, that's one of its strength.\nBut if you look at it from this, the majority of today's in use PCs have at least a drive capable of reading DVDs."
    author: "Tobias"
  - subject: "Re: switch to DVD"
    date: 2005-02-06
    body: "well, I do have to admit, DVD burners are getting dirt cheap.\nI picked one up for my wife at $69 the other day at walmart, and that was a\ndual-layer 16x model..    The older  4x DVD+_r models that don't\nsupport dual-layer are even cheaper then that.. $69, thats like \n3 weeks of $20 payments on layaway..  I spend more then that on cloths\nor groceries at walmart...  Sorry but thats dirt cheap, and I only make\n$6 an hour at my job..  but at $69, for a dual-layer, thats a steal..\nAt that price, I got 2 of em, so now my wife and I both have one for our\nold broken down computers. LOL   I think you'd better check around again\nguys..\n\nAs for a DVD version,  I would like to see one, but its no big deal.\nI'm not too much of a knoppix fan anyway. Its bad enough they put KDE,\nthe crappiest desktop manager known to the LInux community, on there,\nwould be nice to have Gnome, or even xfce4 instead,    if you guys are\nSOO concerned about DVD's being expensive, why don't you DITCH the KDE\nbloat and get something smaller that will fit better on a CD-R, like\nxfce or even Gnome instead.  nobody will even know the difference.\nIts better then taking out documentation to make 3.7 fit on a 700 mb CD-R, thats just rediculous..  and take out twm, and some of those other window managers that don't even work under knoppix, thats just a waste of space.\nIf yer gonna insist on KDE, at least put KDE on there as well as another\nchoice that actually works with the distribution.    THats just, a waste of space that could be used for more programs or documentation for the project.\n\nthats my opinion anyway,   but personally I could care less if they come out with a DVD version or not, because I am not a big fan of knoppix anymoore anyway,  but I had to comment on the DVD burner issue, they ARE getting dirt cheap..  I make $6 an hour raising 3 kids dude, and I can afford one..  ever heard of layaway?  $69 heck I pay 3x that amount in groceries man..  thats dirt cheap if you ask me."
    author: "bubazoo"
  - subject: "Re: switch to DVD"
    date: 2005-10-27
    body: "I have 2 DVD/RW burners :)"
    author: "hfj"
---
To celebrate the start of the <a href="http://conference2004.kde.org/sched-marathon.php">CodeMarathon</a> event at the <a href="http://conference2004.kde.org/">KDE World Summit 2004</a>, the KDE Project and <a href="http://www.knoppix.org">Knoppix</a> proudly announce <a href="http://www.knopper.net/knoppix-mirrors/index-en.html">Knoppix 3.6</a>, dubbed 'aKademy Edition'. Last-minute bugs were fixed, and the finished version demonstrated exclusively in an aKademy <a href="http://conference2004.kde.org/tut-knoppix.php">tutorial on Knoppix</a>, indicative of the strong relationship between the Knoppix and KDE developer and user communities.

<!--break-->
<p>
Klaus Knopper, said: "Since the first Knoppix version users have been using KDE and are very comfortable with its 
efficiency and userfriendliness."</p>

 


<p>
Building on their successful LiveCD platform, Knoppix 3.6 comes with KDE 3.2.3 and the FreeNX Server, which is the speed boosting <a href="http://www.nomachine.com/resources.php">NX Terminal Server technology</a> in KDE. In presentations at <a href="http://www.linuxtag.de">LinuxTag</a> and aKademy, co-authors Fabian Franz and Kurt Pfeifle impressed audiences showing how FreeNX with KDE can provide multiple reliable remote X connections, a huge advantage for KDE developers and Knoppix users alike.</p> 


<p>Check the <a href="http://www.knopper.net/knoppix-mirrors/index-en.html">Knoppix mirrors</a> for downloading the ISO. Also you can
get FreeNX packages at <a href="http://www.kalyxo.org">Kalyxo</a>.</p>

