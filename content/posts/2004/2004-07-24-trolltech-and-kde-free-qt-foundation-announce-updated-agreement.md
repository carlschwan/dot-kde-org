---
title: "Trolltech and KDE Free Qt Foundation Announce Updated Agreement"
date:    2004-07-24
authors:
  - "cschumacher"
slug:    trolltech-and-kde-free-qt-foundation-announce-updated-agreement
comments:
  - subject: "You people Rock"
    date: 2004-07-24
    body: "\"In particular, should Trolltech ever discontinue making regular releases of the Qt Free Edition for any reason - including a buyout or merger of Trolltech or the liquidation of Trolltech - the Qt Free Edition will be released under the BSD license and optionally under one or more other Open Source Licenses designated by the Board of the Foundation.\"\n\nI would hope that it would go under the BSD license or else linux would fall the way of the dodo bird.  It is essential to allow proprietary development on the Linux platform.\n\nBTW Rock on KDE and QT.  All of you guys/gals are doing an excellent job....the future of linux depends as much upon you as it does on the kernel and driver developers. \n\nThank you all.\n-John Furr"
    author: "gnuLNX"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "I agree... :)"
    author: "FJ"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "\u00abIt is essential to allow proprietary development on the Linux platform.\u00bb\n\nWhat?? If propietary software with a free unix system, is so needed, why then Linux is more popular that BSD?\n\nGTK+ is LGPL, and how much propietary software is using GTK?"
    author: "suy"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "VMware is"
    author: "Flameeyes"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "Get your facts straight: Vmware uses Motif. It's obvious at first glance..."
    author: "iks"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "No, vmware use Gtk+.\n\nhttp://www.vmware.com/products/desktop/img/ws4_large4.gif"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "Linux is a program, for a program gpl is better than bsd\n\nQT, GTK+ are libraries, for a library lgpl is better than gpl\n\nGTK+ multiplatform support sucks, QT multiplatform support rocks and propietary (and free?) software only for Linux is a suicide\n\nAnd Borland leave Kylix (QT based) development and adopt wxwindows that is LGPL and use GTK+/X11/etc/not QT in Unix\n\nNobody choose QT for been the better toolkit, they choose QT for been the better multiplatform toolkit, until wxwindows arrives"
    author: "I"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "\"And Borland leave Kylix (QT based) development and adopt wxwindows that is LGPL and use GTK+/X11/etc/not QT in Unix\"\n\nWhere do you get that from?\n\nThe only reason that i can see for Borland to make such a resource consuming switch it that KDevelop serves everyone needs more than well AND fits perfectly in the KDE desktop experience. So the only ppl that concider a 'switch' to Kylix are the GTK zealots that wont run KDevelop. That honestly the only reason that pops to mind when thinking of this weird/unconfirmed toolkit move my Borland. \n\n\"Nobody choose QT for been the better toolkit, they choose QT for been the better multiplatform toolkit, until wxwindows arrives\"\n\nwxWindows is arround for quite some time, right, so when can we call it 'arriving'? What commercial development is using wxWindows right now? If I check their website i only find some toy-apps (excusee le mot) using wxWindows.\n\nI dont have the experience, I code on Qt only, but I hear from all directions that GUI software development with Qt is MUCH easier and straight forward than with GTK, GTK+, of M$'s API.\n\nMy question to you, did you use Qt/KDevelop to develop anything? "
    author: "cies"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "http://wxwindows.org/borland01.htm\n\nI like QT (and disklike qtdesigner) and KDevelop but speaking about proprietary software Borland likes WxWidgets and his C++BuilderX, that's all\n\nThe developers of these apps http://www.wxwindows.org/apps2.htm likes WxWidgets\n\nDevelopers of multiplatform free apps likes WxWidgets\n\nI have used QT/QTDesigner and GTK/Glade (c++ and python bindings) and prefer GTK/Glade mainly for Glade (and for the license IN ALL PLATFORMS), QTDesigner layout management sucks. Python/Glade/Kdevelop is an awesome development platform \n\nA lot of people also prefer QT for the network and database libraries, qt is a framework, gtk is a graphic toolkit. QT as a graphics toolkit doesn't offer a big advantage over other toolkits. And wxwindows also have network and database libraries. And languages like python with gtk bindings have these libraries. Why depend on TrollTech?\n\n\"wxWindows is arround for quite some time, right, so when can we call it 'arriving'?\" When Linux to the Desktop, this year? KDE have made a lot of  advertising of QT, as a KDE user my first toolkit was QT and you? the first and the unique? GTK in C is laborious, in C++ o Python is like QT o pyQT and Glade rocks, you can copy/cut/paste widgets into hboxs/vboxs in qtdesigner not"
    author: "I"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "> QTDesigner layout management sucks\n\ni'm sorry, but you obviously have no idea how to use Designer then, as it has superb layout management, something Glade (inc glade2) could take a lesson or 10 from.\n\ncome to aKademy and i'll help bring you up to speed =)"
    author: "Aaron J. Seigo"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "Jesus - Glade?! I don't know why you're mentioning that for. Hopefully, in terms of GTK or Gnome development (but thanks to the excellent C# bindings work by many people totally feasible for KDE) the forms designer from SharpDevelop will get added to something like MonoDevelop.\n\nHowever, nothing comes close to Qt Designer's layout management. Gone are the days when, as a developer, you worried what on Earth was going to happen when someone resized the damn form...."
    author: "David"
  - subject: "Re: You people Rock"
    date: 2004-07-26
    body: "i mentioned glade because the post i was replying to mentioned glade =)\n\nas for SharpDevelop's form creator, it looks a lot like Designer =) how does it manage layouts?"
    author: "Aaron J. Seigo"
  - subject: "Re: You people Rock"
    date: 2004-08-05
    body: "It doesn't, and after painful experience, neither does Visual Studio or .Net in general. With Whidbey we'll get about two new pre-defined layouts. Joy...."
    author: "David"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "I have no idea how to use Designer and can't go to aKademy. Please tell me how to add a new widget to a layout. In qt layouts are like other widgets, in qtdesigner not. In qt you create a layout and add widgets, in qtdesigner you create widgets and apply a layout, i don't like this."
    author: "I"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "Thought you'd said you'd used Designer?"
    author: "David"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "I love Qt, KDE and KDevelop but the layout tool in QtDesigner makes me pull my hair out (and I don't have enough left to afford that..).\n\ne.g.\n\n1 - I have a layout in a layout in a layout (in QTDesigner), how do I add a widget to the deepest layout without having to delete the outer layouts first and then recreate them all over again (and again, and again..)?\n\n2 - Often the layout seems fine in preview in the designer, only for widgets to drop off the bottom in run-time.\n\nabdulhaq"
    author: "abdulhaq"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "May I assume that you have already reported the problem(s) to qt-bugs@trolltech.com ?\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "Mmm... :-)\n\nare they bugs or features? Point 2 you could say is a bug but it's not something that can easily be reproduced. If it is, they don't need me to tell them.\n\nabdulhaq"
    author: "abdulhaq"
  - subject: "Re: You people Rock"
    date: 2004-07-25
    body: "Well, you seems not liking to have to remove a few layouts to modify the inner one. However if nobody reports it, it might be never changed.\n\nSure, may be the developers see the behaviour. But if they find it right or not too much disturbing, then somebody else must point that there is a real problem. \n\nFor example I can understand your problem, but it was never one for me, because I first designed a dialog and then only put the layout.\n\n(Also reporting bugs also help to make priorities, especially on something like usability.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: You people Rock"
    date: 2004-07-25
    body: "I think I will report them, you're right. But I DO think they're a real problem. It's fine to say that you design it first, well that is good design, but designs change, especially over months and years.\n\nI've been developing for over 20 years and haven't found layout quite so annoying before. Nevertheless, Qt is superb.\n\nabdulhaq"
    author: "abdulhaq"
  - subject: "Re: You people Rock"
    date: 2004-07-26
    body: "1. yes, you have to break the layouts. this is not great, but i don't find it a horrendous problem in practice as it's quick to re-add the layouts. if you do have layouts-in-layouts-in-layout-etc... you usually are misusing layouts. i find that people will often use multiple layers of layouts when spacers and gridlayouts will do just as well\n\n2. this is avoidable."
    author: "Aaron J. Seigo"
  - subject: "Re: You people Rock"
    date: 2004-07-26
    body: "> 1. ..you usually are misusing layouts...\n\nprobably.\n\n> i find that people will often use multiple layers of layouts\n> when spacers and gridlayouts will do just as well\n\nCan spacers be used without using layouts, then? Grid layouts aren't available from the designer, are they?\n\n>2. this is avoidable.\n\nIf you have time to explain how then this would be greatly appreciated!\n\nabdulhaq"
    author: "abdulhaq"
  - subject: "Re: You people Rock"
    date: 2004-07-26
    body: ">Grid layouts aren't available from the designer, are they?\n\nOf course they are - sorry, temporary brain failure."
    author: "abdulhaq"
  - subject: "understood"
    date: 2004-07-24
    body: "Thanks for clarifying ur self... I understand your view better now :)\n\nI can understand that GTK/Glade on Python is a good platform for quick, multiplatform GUI app development -- especially cauz Python makes the non-GUI classes multiplatform. And with your dislikes towards QTDesigner you certainly have a point (that beeing worked on iirc).\n\n>> Why depend on TrollTech?\n\nFor me personally it feels good to have a healthy company taking care of all the dirty low-level stuff. They provide a very good product, maintain it and bring frequent upgrades that keeps us working on a up-to-date development platform. Using Qt (KDE) apps depend on less libaries than using GTK/Glade/etc/etc/etc which keeps desktop lightweight.\n\nI dont mind depending on Trolltech for these reasons, and if i get paid to make commercial software I wouldnt mind to pay for Trolltechs commercial Qt licence. Qt for windows is very stable (i dunno for wxWindows)\n\nCheers\nCies."
    author: "cies"
  - subject: "Re: understood"
    date: 2004-07-24
    body: "\"Qt for windows is very stable (i dunno for wxWindows)\". Qt for windows is not free, I like freedom of free software. And wxwindows also have support http://www.wxwindows.org/support.htm. Linux also had his unstable time\n\n\"I can understand that GTK/Glade on Python is a good platform for quick, multiplatform GUI app development -- especially cauz Python makes the non-GUI classes multiplatform. And with your dislikes towards QTDesigner you certainly have a point (that beeing worked on iirc).\"\n\nThat beeing worked on? tell me more. If somebody fix qtdesigner I would use KDE (qt file dialog sucks) on Unix and Win32 in Windows, and Python for non-gui/desktop libraries"
    author: "I"
  - subject: "Re: understood"
    date: 2004-07-25
    body: "> If somebody fix qtdesigner I would use KDE (qt file dialog sucks) on Unix and Win32 in Windows, and Python for non-gui/desktop libraries\n\nDid you filed bugs/wishes? [which would give you reason to complain]"
    author: "cies"
  - subject: "Re: understood"
    date: 2004-07-26
    body: "That is not a bug, is a fundamental design flaw, according to me qtdesigner SUCKS\n\nKDE community like qtdesigner\n\n\"> QTDesigner layout management sucks\n\ni'm sorry, but you obviously have no idea how to use Designer then, as it has superb layout management, something Glade (inc glade2) could take a lesson or 10 from.\"\n\nI must be wrong or qtdesigner is not for me. What is your opinion? If you tell me qtdesigner layout management sucks I fill a wish"
    author: "I"
  - subject: "Re: understood"
    date: 2004-07-26
    body: "I thought you said just some messages above you never actually used QtDesigner...? Opinionmaking about tools one never actually used? o.O"
    author: "Datschge"
  - subject: "Re: understood"
    date: 2004-07-26
    body: "Where?  \"I have no idea how to use Designer and can't go ...\" You know \"sarcasm\" word?. I have said in another comment \"I have used QT/QTDesigner and GTK/Glade (c++ and python bindings) and prefer GTK/Glade mainly for Glade (and for the license IN ALL PLATFORMS), QTDesigner layout management sucks. Python/Glade/Kdevelop is an awesome development platform\""
    author: "I"
  - subject: "Re: understood"
    date: 2004-07-26
    body: "What exactly don't you like about Designer's layout management? Yeah, the last time I used glade was back in the gtk 1.2 days, but both glade and gtk seems have more flakey layout management than Qt and Designer do. "
    author: "smt"
  - subject: "Re: understood"
    date: 2004-07-27
    body: "read abdulhaq comments"
    author: "I"
  - subject: "Re: understood"
    date: 2004-07-26
    body: "...I'm probably just confused about why you rant at this website, of all places, about Trolltech's QtDesigner..."
    author: "Datschge"
  - subject: "Re: understood"
    date: 2004-07-25
    body: "\"Qt for windows is not free, I like freedom of free software\"\n\nThen why do you care about win32 Qt? Windows is not free. If you really care about free software, why care about unfree operating-system? If you love free software, you will run your free software on free operating-system, and not on some proprietary monstrosity."
    author: "Janne"
  - subject: "Re: understood"
    date: 2004-07-26
    body: "http://dot.kde.org/1090614115/1090633760/1090639756/\n\nin win32/gtk+/wxwindows you can choose the license\n\nwith gtk+ you can program a free Linux app and at no cost run it on windows"
    author: "I"
  - subject: "Re: You people Rock"
    date: 2004-07-25
    body: "GTK+ multiplatform rocks, GAIM works, mplayer-gui, xchat, gimp works in windows without any problems, and its improving with every release."
    author: "salsa king"
  - subject: "Re: You people Rock"
    date: 2004-07-25
    body: "Woopdy doo."
    author: "David"
  - subject: "Re: You people Rock"
    date: 2004-07-26
    body: "You forgot inkscape in the list ;)\nThis software is really impressive, especially in speed of development. It is similar to scribus on this point.\n\nIf only Qt were available on windows for free software! There is no windows Qt free software available on this platform.\nIt is a nonsense (and a shame) that people have to go for Xwindows ports to windows in order to use Qt software they love on Linux when Qt is said to be multiplatform: LyX and Scribus are two examples. This gives the impression that Qt is Linux-only while Gtk is multiplatform. Trolltech guys should think of this as a  marketing advantage to have good apps (LyX and Scribus) available for Windows users, so they see how good Qt is and start develop with it."
    author: "oliv"
  - subject: "Re: You people Rock"
    date: 2004-07-26
    body: "GTK+ is not multiplatform, it is a platform, a great platform, and it works, GTK platform rocks, OK?"
    author: "I"
  - subject: "Re: You people Rock"
    date: 2004-07-27
    body: "Gtk is a mess."
    author: "Me"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: ">I would hope that it would go under the BSD license or else linux would fall the way of the dodo bird. It is essential to allow proprietary development on the Linux platform.\n\nYou have it exactly backwards.  *non*-proprietary development on Linux is essential for its continued growth.  This is the whole point of Free Software like Linux.  "
    author: "LMCBoy"
  - subject: "Re: You people Rock"
    date: 2004-07-24
    body: "Everybody in the world today does proprietary development by paying for a license. BSD, LGPL or otherwise, it isn't going to make a damn bit of difference.\n\nI don't understand people who talk about free proprietary development. It doesn't happen in any meaningful way."
    author: "David"
  - subject: "Re: You people Rock"
    date: 2004-07-27
    body: "> I would hope that it would go under the BSD license...\n\nIt would.  Note the phrasing \"...will be released under the BSD license **and** optionally under one or more other Open Source Licenses...\""
    author: "Jim"
  - subject: "Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "I apologize if it sounds like I'm spouting FUD here, but is there actually a practical way to make free software written with Qt available on Windows? Weaning the Windows masses by gradually replacing their proprietary apps with tasty free alternatives is, I think, a laudable thing (Mozilla, Gimp, OpenOffice, etc), but as far as I can see the GPL'd QT Free Edition is only available for Unix/X11, Mac OS X, and embedded Linux."
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: ">> I apologize if it sounds like I'm spouting FUD here\n\nyou dont, at least to me you dont :)\n\n>> but is there actually a practical way to make free software written with Qt available on Windows?\n\nYes there are:\n1. Use the commercial licence of Qt [http://www.trolltech.com/products/licensing.html] (Wow, new website look!)\n2. Use one of these projects:\n       http://kde-cygwin.sourceforge.net/\n       http://kde-cygwin.sourceforge.net/qt3-win32/ (hee, news)\n3. There's some Non-commericial Qt version for Windows around, check here:\n       http://dot.kde.org/1076068778/\n       http://developers.slashdot.org/comments.pl?sid=95499&cid=8181832\n\nI hope I informed you well enough.\n\nCies."
    author: "cies"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "The commercial license and non-commercial versions are clearly not useful; if you can't compile it and you can't sell it, it's not free software. The X11 version on Cygwin/XFree86 should work but is a bit roundabout -- huge installation dependencies. The win32 port of the X11 version sounds like it's actually got some potential, though!\n\nThanks for the pointers."
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-26
    body: "Why is the commercial version not useful? \n\nYou just need to own one, and Trolltech has already given at least one open source team I know of, free commercial licenses."
    author: "Allan S."
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-26
    body: "\"3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for \nmaking modifications to it. For an executable work, complete source \ncode means all the source code for all modules it contains, plus any \nassociated interface definition files, plus the scripts used to \ncontrol compilation and installation of the executable. However, as a \nspecial exception, the source code distributed need not include \nanything that is normally distributed (in either source or binary \nform) with the major components (compiler, kernel, and so on) of the \noperating system on which the executable runs, unless that component \nitself accompanies the executable.\""
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-26
    body: "GPL software may link against non-free libs (although it's discouraged by the FSF):\nhttp://www.gnu.org/licenses/gpl-faq.html#FSWithNFLibs\nhttp://www.gnu.org/licenses/gpl-faq.html#GPLIncompatibleLibs\n\nA special exception may be needed to link against non-free libs that are not part of the base system the software is to run on, but you're free to do that as you're the author of the software (unless you're also using stuff written by other people, of course...).  If you have a commercial license of Qt you can certainly distribute the library along with your software. \n\nSo, where's the problem?"
    author: "cm"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-27
    body: "As you said, you can't do it on a GPL'd package without changing the license; this isn't always possible if ownership is spread out and not all authors can be contacted. Further, it destroys the package's free software status on that platform by making it impossible for users to recompile the app and distribute it under the same terms without getting an expensive per-seat license from Trolltech.\n\nIn comparison, a GPL'd Win32-capable toolkit would allow compiling and running a Win32 version without requirement for *any* non-free software. (Not even Windows is strictly necessary; you can cross-compile from Linux with gcc and run it in Wine or on ReactOS.)"
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-27
    body: "Well, some things *are* possible with the commercial version and so I would say it's something else than \"clearly not useful\". \n\nBut I see what you mean.  \n\nI would prefer to have Qt under LGPL on all platforms but I'm afraid that won't happen.  "
    author: "cm"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-27
    body: "What's really the problem?\n\nSo.. If it was Debian it would end up in contrib, but there really aren't any Debians for Windows, and there can't be. And since compilers especially outside the Cygwin-distribution are expensive and rare on windows, I can't see the point of distributing the source-code for windows-users."
    author: "Allan S."
  - subject: "Availability of compilers for Windows"
    date: 2004-07-27
    body: "That's simply not true. MinGW and Cygwin both provide GCC, and Microsoft's compiler (not the IDE) is available for download gratis."
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-08-12
    body: "> As you said, you can't do it on a GPL'd package \n> without changing the license; this isn't always \n> possible if ownership is spread out and not all \n> authors can be contacted. \n\nBut it is possible, and has already been done with one large KDE app (KOrganizer).\n\n> Further, it destroys the package's free software \n> status on that platform by making it impossible \n> for users to recompile the app and distribute it \n> under the same terms without getting an expensive \n> per-seat license from Trolltech.\n\nThe QT binaries can be redistributed without problem.\n\nI'm not sure about the headers, but if you see a karm.exe appear in the next few months, the answer is yes. ;)"
    author: "mbucc"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-08-21
    body: "Please, I don't wish this to sound like a troll, but I have to point this out. \n\nWithout a GPL software licence for QT on all platforms, QT is eventually going to loose out to GTK. Just look at the most popular open apps (OO.org, Mozilla, Evolution, Gimp); none of these are based on QT, and this is for a reason; IBM/Novell/Sun all want to develop cross platform solutions for business. QT doesn't allow them to develop 'free' cross platform solutions. I love KDE/QT and have been using it for over five years now, but GTK is getting a lot more money poured into it now. \n\nI just hope the powers that be can find a way to GPL QT on all platforms. \n\nBTW, how much is the estimated market value of Trolltec? Could Novell just buy them out and completely GPL QT?"
    author: "john_ireland"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "> weaning the Windows masses by gradually replacing their \n> proprietary apps with tasty free alternatives is\n\nthis is rather OT, but..... i don't agree. most computer users don't notice or care about the relative freedom afforded them. all they know is how well (or not well) their current system works. if we work hard to provide quality Free(dom) software on non-Free platforms, all it does it make the pain of those non-Free(dom) platforms lessen while removing much of the strategic advantage of running Free(dom) OSes. this will result in people sticking with the devil they know (e.g. Windows) because it's now \"cheap enough\" and does what they need. this in turn allows those who maintain those non-Free systems (e.g. Microsoft) to once again work their way up from the OS to the application space by changing APIs, giving their products efficiency advantages through internal knowledge of non-published APIs, etc. while sucking resources from the Open Source development community.\n\ni'm starting to hear people who say they wish certain apps (e.g. K3B, Apollon, Scribus, etc...) ran on Windows. these applications are a REASON to switch to a Free OS. if they existed on Windows, they would stay with that non-Free system.\n\nyou know: competitive advantage. unique sales proposition. supporting the development ecosystem. and other such marketing phrases that have an actual basis in reality."
    author: "Aaron J. Seigo"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "These free apps have other advantages, though; they keep a singly commercial entity from controlling computers on the small and large scale (locally and on the internet). BY supporting open standards and cheap or free software, we are giving power to the people and basically democratizing the whole process."
    author: "Mikhail Capone"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "To explain myself better:\n\nI meant that not everybody running mozilla or open offices knows and care about the freedom behind the apps, but they are nonetheless encouraging open standards, thus making things better for everybody."
    author: "Mikhail Capone"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "I doubt that somebody who doesn't care about Free Software will care about open standards at all.\nWe have to educate the people and explain them what Free Software really is and why using Free Software\non a propritary system will always sucks..."
    author: "Tobias K\u00f6nig"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-25
    body: "That's really true - creating free software (or any other free support) for non-free (M$-) OS is nothing else than just feeding the Bosses of the evil empire for free.\nBtw. that's also the reason why I've stopped fixing my friend's wondoze boxes."
    author: "hoernerfranz"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-25
    body: "Well, there are cases where you *have* to use windows, and then it is very nice to be able to run at least some of the software you're used to from linux. (e.g. at work where can't simply replace your windows with linux, because there are some proprietary apps essential for the job where no replacement exists)\n\nAt least I am happy that I can use cygwin, mozilla, doxygen and OOo on my windows box at work.\n\nAlex"
    author: "aleXXX"
  - subject: "I agree"
    date: 2004-07-24
    body: "That's exactly my point of view. Most users are pragmatic. If they can install k3b, scribus and the gimp on Microsoft, they'll do it (if they don't manage to suck a spyware-contaminated WaReZ Photoshop, Corel, whatever..) and won't ever start to think about _what_ they're using."
    author: "Thomas"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-24
    body: "I'm curious; what's your opinion on proprietary Unix? Should we all be removing Solaris and AIX support from our software? Why or why not?"
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-25
    body: "How many proprietary Unix vendors fit the following descriptions?\n* Investigating pushing a DRM scheme that requires you to a run certified binary kernel to run multimedia and other content effectively giving them root control of your system\n* Expressing an interest in eliminating spam by providing their user base with a proprietary signature generated by their software which they can protect from reverse engineering with DMCA\n* Notorious about leaving security holes in their software to bog down the internet with spam (so they leave room for a solution)\n* Convicted of abusing monopoly status in numerous anti competitive practices (only one company in the last century fits that)\n* Uses a proliferation of APIs and undocumented APIs as tactical warfare to dominate key markets\n* Is financially structured around a growth model that makes competition and co-existence unacceptable to them so their approach is to attempt to eliminate any perceived threats by whatever measures necessary\n* Traditionally manages data in proprietary formats with licensing that effectively means users don't really fully own their own information without paying their dues\n\nYou might be able to alledge some degree of the above on some proprietary Unix vendor, but this and more is largely the exclusive domain of one company and product line. If a single proprietary Unix vendor were doing all these things and putting our freedoms at risk there would be reason for concern, but as it happens proprietary Unix is getting it's butt kicked up around it's ears by Linux. Just ask Sun.\n\nOf course if they are doing commercial development with Qt they will be buying license that are very reasonably priced (as evidenced by the health of Trolltech). The reality though is Sun is talking about open sourcing Solaris to a collective yawn and IBM is pushing Linux on the mainframe because it delivers better tools then AIX and saves them billions in development. Any support for these fading OS's is on GNU tools by people adapting the software and is a non issue."
    author: "Eric Laffoon"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-25
    body: "I would argue that the best way to defeat those evil vendor lock-in schemes is probably _not_ to guarantee the lock-in wedge applications a large market share by not providing any competition for them in the biggest OS market.\n\nMaybe I'm wrong, though. Maybe turning the OS into a commodity to resist lock-in schemes is counterproductive. Maybe we should *encourage* people to use Internet Explorer and Microsoft Office and all those DRM technologies so it'll be as hard as possible for them to to make the switch to a free OS... I'm not sure I understand how that would help, though."
    author: "brion"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-25
    body: "You encourage people to do that by making people look at applications like K3B and others, and giving people a great alternative environment to run them in - that isn't Windows. If you like applications like K3B and others, you can move - it's that simple. That is the attitude Microsoft has taken with Windows over the years, and we can't be any different. You don't give people an excuse to keep using Windows by expending massive amounts of needless effort to port applications to it. I think Microsoft has enough resources to do that for themselves.\n\nThe commodity desktop does not mean accepting, and porting to, Windows."
    author: "David"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-26
    body: "what props up OSes are the apps and 3rd party development. which is why i'd much rather see proprietary apps (inc MS apps) join the mix of Free Software on Free OSes than Free Software applications join the mix of proprietary apps on proprietary OSes.\n\nmoving from IE to something not IE is going to be as expensive whether you do it on Windows or not on Windows. the only IE issue, really, is ActiveX components (and other such Windows specific technologies). this isn't helped whether they stage their move or do it all at once.\n\nmoving from MS Office to something not MS Office is going to be as expensive whether you do it on Windows or not on Windows since it's all about the file formats and not much else. moving things like Access databases is more troublesome because those file formats are weakly supported by Free Software apps.\n\njust as important as the browser or Office switches, however, are the supporting apps. these are applications to which there is little to no data issues: music, CD / DVD authoring, desktop publishing (there are no switching alternatives here, usually; it's often \"recreate everything\"), graphics, file management, educational software, etc...\n\nif we have nothing but a bunch of apps can also get where you already are (e.g. Windows or MacOS X) and they run just as well there, why would people switch? meanwhile, Microsoft will take advantage of this lead time and once again mess with APIs to give their application software a competitive edge landing us right back at square one."
    author: "Aaron J. Seigo"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-26
    body: "i don't think removing people's hard work is a useful approach, and i wouldn't want to dictate what other people can or can not work on when it comes to Free Software. however, i do think it's important to keep the focus and direction moving towards Free software rather than away from it. the proprietary UNIX world is already well on its way in that direction, and the proprietary UNIX world has been much more accepting and supportive of the Free / Open Source community. usually it's people running those proprietary UNIXes that do the porting (if at all necessary) so they can run Open Source software (this includes the MacOS X ppl); whereas it has been my observation that on Windows it's often people on Free systems hoping to dangle a carrot out to Windows users who then spend their time doing just that.\n\nour priority (and by \"our\" i mean the whole Open Source community, not \"KDE\") should be on Open Source systems first and foremost. non-Free OSes which are no t providing support for themselves should be a distant second concern."
    author: "Aaron J. Seigo"
  - subject: "Re: Qt-based Free Software on Windows?"
    date: 2004-07-25
    body: "The hypocrite of the repeated request for removal of the already reasonable per-developer license in favor of a market dominating proprietary system which is only available with per-machine licenses plus lots of additional EULA restrictions is not even funny anymore."
    author: "Datschge"
  - subject: "A few questions"
    date: 2004-07-24
    body: "Trolltech can put the price they want to the qpl license? They can put a special price for a particular company?. If KDE take over the desktop this can be a problem?"
    author: "I"
  - subject: "Re: A few questions"
    date: 2004-07-24
    body: "Well, in this case just use the GPL'ed version, that's the correct licence anyway..."
    author: "Tobias K\u00f6nig"
  - subject: "Re: A few questions"
    date: 2004-07-24
    body: "and if your company depend on a propietary program? you want to say that qpl license is useless? all the world must embrace GPL? is Windows more free? you can use the license you want"
    author: "I"
  - subject: "Re: A few questions"
    date: 2004-07-24
    body: "Right. Trolltech will start charging $1billion for Qt commercial license to screw KDE...\n\nDo understand the concept of making money at all? Let me explain this to you:\n- Trolltech is a company,\n- they need to be earning money,\n- they want Qt used everywhere,\n- they can't be charging silly prices or they won't achieve any of the two above."
    author: "Reality Check"
  - subject: "Re: A few questions"
    date: 2004-07-26
    body: "If KDE take over the desktop Trolltech is the new monopoly, at least for propietary programs\n\n- Microsoft is a company,\n- they need to be earning money,\n- they want Windows used everywhere,\n- they can't be charging silly prices or they won't achieve any of the two above.\n\nbut we have a problem\n\nMicrosoft were the goods when IBM were the bads\n\nI am very paranoic but you are very trusting :)"
    author: "I"
  - subject: "Re: A few questions"
    date: 2004-07-27
    body: "I must have missed Microsoft releasing Windows under GPL.\nI must have missed KDE being owned by Qt.\nI must have missed this announcment which assures that if something bad should happen Qt will be released under the BSD license."
    author: "Reality Check"
  - subject: "Re: A few questions"
    date: 2004-07-27
    body: "KDEs developer-base is many times larger than Qts. If it really became an issue and they somehow circumvented the FreeQt agreement (can't see how it can be done though), Qt would be reimplemented in half a year or so. \n\nIt's not like there are secret undocumented API calls we could be strugling to implement for a decade like win32."
    author: "Allan S."
  - subject: "Re: A few questions"
    date: 2004-07-26
    body: "How is Windows more free?  You can distribute software by any license (same as Linux), but you can't bundle dependencies without legal action.  I really hate this business about more choices being a bad thing; a license is a license is a license, and if you can't abide by it, you're breaking the law.  The difference is that licenses like the GPL give you the freedom, but I guess it's like the old saying about giving an inch:  give a user a little freedom and they whine about little restrictions.\n\nRant finsished."
    author: "Keith"
  - subject: "Re: A few questions"
    date: 2004-07-26
    body: "Windows is there, users pay the libraries. Windows is more free as in free beer for developers, OK?"
    author: "I"
  - subject: "What is it worth?"
    date: 2004-07-26
    body: "First, it is a nice move from Trolltech to renew the agreement and Trolltech has shown that it really cares about Free Software when it GPLed Qt.\n\nBut what is the agreement really worth when Trolltech gets bough up?\n\n\"... should Trolltech ever discontinue making regular releases of the Qt Free Edition for any reason - ... - the Qt Free Edition will be released under the BSD license ...\"\n\nWhat is regular? Every 10 years?\n\nWhat is a release? Change a few bits and delete a method and give it a new number?\n\n\"... the Qt Free Edition will be released under the BSD license ...\"\n\nWho will release if Trolltech does not exist anymore and the company which has bought it doesn't want to?\n\nI do not want to be negative as this encouragement to use Qt is a positive act. But could it have real legal consiquences when I want to develop non-free software, and make sure that another (not too small) company cannot severely hurt my business by buying Trolltech and let Qt practically die?"
    author: "guenther.palfinger"
  - subject: "Re: What is it worth?"
    date: 2004-07-26
    body: "\"What is regular? Every 10 years?\"\n\nThe agreement spells these things out, see the links at the bottom of http://www.kde.org/whatiskde/kdefreeqtfoundation.php#updated_agreement\n\n\"Who will release if Trolltech does not exist anymore and the company which has bought it doesn't want to?\"\n\nIf they stop releasing either Qt altogether or just Qt Free Edition (which continuing to release non-free Qt), then the provisions of the agreement kick in in 12 months and the KDE Free Qt Foundation has the right to make the BSD-licensed release. \"Doesn't want to\" would be followed by a lawsuit if necessary, and a court will order them to fulfill the obligations they took on by their purchase."
    author: "brion"
  - subject: "Re: What is it worth?"
    date: 2004-07-27
    body: "Thank's for the link. I don't know why I didn't find it myself, although looking for it.\n\nIMHO, it is really very well phrased - one couldn't ask for more.\n\n-- Guenther"
    author: "guenther.palfinger"
---
On July 23rd 2004, <a href="http://www.trolltech.com/">Trolltech</a> and the <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a> announced the signing of an <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php#updated_agreement">updated agreement</a> between Trolltech and the KDE Free Qt Foundation about securing the availability of Qt for development of free software. The new agreement replaces the <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php#agreement">original agreement</a> from June 1998 by adapting it to the current situation and providing a new text which addresses the problem in a more exact and more complete way. The intention and basic content are still the same.
Read the <a href="http://www.kde.org/whatiskde/kdefreeqt_announcement_20040723.php">full press release</a> for more details.


<!--break-->
  <h2>Trolltech and KDE Free Qt
  Foundation Announce Amended and Restated Software License Agreement </h2>
  
  <p>(July 23, 2004) <a href="http://www.trolltech.com/">Trolltech</a>,
  developer of Qt, the leading cross-platform development framework upon which <a
  href="http://www.kde.org/">KDE</a> is
  based, and the <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">KDE
  Free Qt Foundation</a> (the <em>Foundation</em>) today announced the signing
  of an <a
  href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php#updated_agreement">Amended
  and Restated Software License Agreement</a>
  (the <em>Agreement</em>). </p>
  <p>The Foundation was formed in 1998 by Trolltech and <a
  href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a>, which represents KDE in
  certain legal and financial matters, to provide assurances to free software
  developers developing with Qt should Qt development cease. </p>
  <p>The revised Agreement continues to honour the original purposes of the
  Foundation. In particular, should Trolltech ever discontinue making regular
  releases of the Qt Free Edition
  for any reason - including a buyout or merger of Trolltech or the liquidation
  of Trolltech - the Qt Free Edition will be released under the BSD license and
  optionally under one or more other Open Source Licenses designated by the
  Board of the Foundation. </p>
  <p>The amendments are aimed at modernizing the Agreement in light of
  developments occurring after the original agreement was executed - including
  the release of the Qt Free Edition under the <a
  href="http://www.gnu.org/copyleft/gpl.html">GPL</a> and <a
  href="http://www.trolltech.com/licenses/qpl.html">QPL</a> in October 2000 -
  and at improving the accuracy of the parties' understanding, thereby ensuring
  that the Agreement continues to operate in the best interest of both parties.
  The major amendments include improved precision in the definitions of the Qt
  product releases, the clarification of licensing terms, and the addition of
  possible termination of the Agreement under certain limited circumstances. </p>
  <p>&quot;Third-party software development is a crucial element of KDE's
  continued future success, and the restated Agreement encourages both Open
  Source and proprietary software developers and enterprises to develop with
  KDE and/or Qt on the X Window System by providing a practical solution should
  Trolltech be unable to continue developing Qt&quot;, explained Martin Konold,
  a KDE
  board member on the Foundation and a key figure in the formation of the
  Foundation and execution of the <a
  href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php#updated_agreement">original
  agreement</a> in 1998. &quot;As with the release of Qt under the GPL
  and QPL, we are very delighted that Trolltech has again demonstrated its
  ongoing support of, and commitment to, Open Source software development, and
  to the KDE project in particular.&quot; </p>
  <p>&quot;Maintaining a close connection with the Open Source community is
  essential to the success of Trolltech,&quot; said Matthias Ettrich, director
  of software tools development at Trolltech. &quot;The amended Agreement
  provides an excellent framework for a continued and mutually rewarding
  relationship between Trolltech and the KDE Free Qt Foundation.&quot; </p>

<!--
  <h3>About Qt</h3>
  <p>Qt is a complete C++ application development framework, which includes a
  class library and tools for cross-platform development and internationalization.
  The Qt API and tools are consistent across all supported platforms, enabling
  platform independent application development and deployment. </p>

  <h3>About Trolltech</h3>
  <p>Trolltech® is a world leader in delivering tools, components, and
  libraries for C++ developers across all major operating systems. Trolltech
  products constitute a leading open source application development framework
  and are an integral part of the Linux desktop. Trolltech also develops
  innovative UI platforms that enable key players to adopt Linux for mobile
  devices. <br>
  <br>
  Trolltech creates two product lines: Qt® and Qtopia®. Qt is a complete C++
  application development framework, which includes a class library and tools
  for cross-platform development and internationalization. Qtopia is the first
  comprehensive application platform built for embedded Linux, and is used on
  numerous Linux-based PDAs and mobile phones. <br>
  <br>
  Trolltech is a second generation open source company with a dual licensing
  business model and provides development software to some of the largest
  companies in the world including Intel, IBM, Motorola, and Sharp, among
  thousands more. Trolltech is headquartered in Oslo, Norway, with offices in
  Brisbane, Australia, and Palo Alto, California. More about Trolltech can be
  found at <a href="http://www.trolltech.com">http://www.trolltech.com</a>.
  </p>
-->

  <h3>About the KDE Free Qt Foundation </h3>
  <p>The KDE project, via KDE e.V., and Trolltech, the creators of Qt,
  announced the formation of the <em>KDE Free Qt Foundation</em> in June 1998.
  The purpose of the Foundation is to assure the availability of Qt for free
  software development now and into the future. The Foundation is controlled by
  a Board, which consists of two KDE e.V. representatives and two Trolltech
  representatives; in many cases, the KDE e.V. representatives break a voting
  tie. The governing document of the Foundation is available online (<a
  href="http://www.kde.org/whatiskde/images/kdefreeqt5.png">Page 1</a>, <a
  href="http://www.kde.org/whatiskde/images/kdefreeqt6.png">Page 2</a>). </p>

  <h3>About KDE </h3>
  <p>KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet to
  create and freely distribute a sophisticated, customizable and stable desktop
  and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform. KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large set of
  networking and administration tools and utilities, and an efficient,
  intuitive development environment featuring the excellent IDE <a
  href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof that the
  Open Source &quot;Bazaar-style&quot; software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software. </p>

<pre>
For further information, please contact:

Trolltech AS
Tonje Sund
+47 21 60 48 78
press@trolltech.com

KDE
Martin Konold
+49 7071 940353
kde-pr@kde.org
</pre>