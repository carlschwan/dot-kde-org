---
title: "KDevelop TechNotes Issue #3"
date:    2004-12-05
authors:
  - "adymo"
slug:    kdevelop-technotes-issue-3
comments:
  - subject: "Killer feature"
    date: 2004-12-05
    body: "Hi.\n\nIt surprises me a bit that this feature is announced as the main feature in 3.2. AFAIK, Trolltech doesn't supports any bindings to Qt, and C++ is the only \"official\" language. Indeed, the only applications in the official KDE modules, are written in C++. Or I missed something?\n\nI don't say this work isn't important, of course. I always wanted to try one of the languages supported in kdebindings, but being the API documentation addressed only to C++, it's a bit discouraging.\n\nThanks."
    author: "suy"
  - subject: "Re: Killer feature"
    date: 2004-12-05
    body: ">I always wanted to try one of the languages supported in kdebindings, but\n> being the API documentation addressed only to C++, it's a bit discouraging.\nTry and you will see the difference;) I'm not kidding here. Under some circumstances Ruby/Qt or Ruby/KDE can become more powerful tool than C++/Qt. And those cases are not rare.\nPS: from personal experience, Qt API documentation is as helpful for ruby programming as it is for c++.\nPPS: about \"official\", how often do you write \"official\" applications for \"official\" KDE modules using \"official\" languages? I don't (with a notable exception of KDevelop, but even to develop KDevelop plugin I sometimes want to  use Ruby, KJS or Python ;)).\n"
    author: "adymo"
  - subject: "Re: Killer feature"
    date: 2004-12-05
    body: "With Ruby at least, the C++ API documentation is the Ruby API documentation since it uses all the same function names. And KDE on Ruby doesn't require a pre-processor, which is nice."
    author: "Ian Monroe"
  - subject: "Re: Killer feature"
    date: 2004-12-05
    body: "And the same for the Python bindings. "
    author: "Morty"
  - subject: "Re: Killer feature"
    date: 2004-12-06
    body: "Yes, it's really good that PyQt/PyKDE are in the kdebindings module now. \n\nBut it would be nice to have some improved support for them in KDevelop. As an example, at the moment there are only KDevelop templates for PyQt projects, not PyKDE. There are some project templates in kdebindings/python/pykde/templates/annotated, and it should straightforward to convert them to KDevelop project templates. You don't need to even write any C++ code to add a new project template. Just take the source and replace the class names with macros such as %{APPNAMESC}, then these macros get substituted for the real class names when you create a new project. Then you add a screen shot as an icon for the project Wizard to show, and a KDevelop xml project file, and that's about it.\n\nOr integrating the 'puic' tool in the makefiles, and adding Qt Designer Integration isn't too much work either, but that would really make KDevelop a load more useful for PyKDE.\n\nA bit harder would be to add a python debugger. I wonder if the code from the Eric IDE could be adapted to be used in a KDevelop python debugger."
    author: "Richard Dale"
  - subject: "Big thanks to the kdevelop team"
    date: 2004-12-05
    body: "They created the best ide for qt/kde development,\nbut did not stop there.\nAnd I'm glad they added ruby support, more than\njust colored syntax, but also integration of qt\ndesigner (qt designer is not so nice in my opinion,\nbut glad it is there, better than nothing) and a\ndebugger for ruby.\nAnd ruby is very nice for kde development, hope\nto seen ruby/kde programs in the near future work\nout of the box on future installations. Really\nlooking forward.\n\nThanks again!"
    author: "pieter"
  - subject: "Re: Big thanks to the kdevelop team"
    date: 2004-12-05
    body: "you REALLY have Richard Dale to thank for this.  He has really gone out of his way to ensure that the bindings look and feel as native in the binding language as they can.  I really wish someone would hire him full time to work on the Ruby stuff both bindings and the KDevelop parts."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Big thanks to the kdevelop team"
    date: 2004-12-05
    body: "Yer, I've been somewhat bemused with the reaction to some of Richard's efforts - which are first-rate. Since Trolltech are looking at (and will quite simply have to) look at languages beyond C++ at an 'application development' and 'desktop development' levels (they talked strongly about it at akademy) then I think they should put some serious effort into this for Qt 4.0 and beyond. These languages would be at a higher level than C++, which would still be there of course, and would be things like C#, Java, Python, Ruby or some sort of Visual Basic type level. Hiring Richard wouldn't be a bad start for them, as I've always been impressed with his balanced ideas on how to approach these things.\n\nWhether these languages would be bindings, or something more tightly integrated with Qt, I don't know. However, the bindings aren't going to be good enough unless Trolltech looks seriously beyond C++ and starts to support non-C++ development with Qt. At the moment, what Trolltech wants to do for the future and what people like Richard are doing with the bindings are pretty much one and the same."
    author: "David"
  - subject: "Re: Big thanks to the kdevelop team"
    date: 2004-12-06
    body: "Thanks Ian. I'd certainly be happy to get some paid work, and can quote for jobs such as adding debuggers for other languages to KDevelop, language bindings development and so on. \n\nWhat I'd really like is to find a company that wants do do a pilot project with KDevelop/Korundum ruby RAD, and employ me as a consultant on that. Then we can fix problems/add enhancements as we go, in the light of actually using the environment.\n\nI've found with enhancing KDevelop, you can get a lot done for relatively little work, because it's a 'framework' - you don't have to start from scratch for each language. It has all the support services, such as setting break points for the debugger, class browser and so on, and you just plug your language into those. And there's enough existing languages that you can probably borrow code from them too - the ruby part was based on the python part as a starting point.\n"
    author: "Richard Dale"
  - subject: "great work! but what about python? "
    date: 2004-12-05
    body: "I think kdevelop is becoming a great IDE and perhaps already the reference IDE on Linux.\nI like very much this ruby integration, great stuff.\nBut what about python?\nThere is a goog kde python binding and python is still much more popular than ruby. A better python integration [debugger, RAD etc] could be a real killer feature!!\nIs anyone working on this?\n"
    author: "ste"
  - subject: "Re: great work! but what about python? "
    date: 2004-12-05
    body: "Python support is also not that bad now and I've already thought about implementing the designer integration for python. Looks like it is possible. Will do if time permits. But to implement the debugger for python we need a person who actually works with python. Volunteers are welcome ;)"
    author: "adymo"
  - subject: "Re: great work! but what about python? "
    date: 2004-12-05
    body: "i will have to take a look at this too, this is the last big part i want for KJSEmbed support, but it makes perfect sense for the Ruby,Python and Perl stuff too... Next i suppose people will be bothering me to expand my RAD presentation to Ruby and Python ;)"
    author: "Ian Reinhart Geiser"
  - subject: "Re: great work! but what about python? "
    date: 2004-12-05
    body: "The python users already have a pretty good ide in Erik3, but getting an alternative in KDevelop are great. Having two great ide's to choose from are just fantastic:-) Perhaps you could rip out and reuse the debugger from eric, and save some work. Anyway you should probably drop a line to the PyKDE mailinglist, there are probably several people there who would want to help."
    author: "Morty"
  - subject: "Re: great work! but what about python? "
    date: 2004-12-06
    body: "Pleeeeeeease, implement Python debugger and integrate it with Qt designer !\n\nThis would be the absolute killer feature of KDevelop! Python has become a high-level modern scripting language standard for Linux and Qt/KDE a superior application framework. Having such rappid application development environmemt would speed up application development for Linux greatly. Eric3 is not bad but it is still rather a toy compared to what Kdevelop/Python could be thanks to the infrastructure it already provides.\n"
    author: "krystof"
  - subject: "Re: great work! but what about python? "
    date: 2004-12-06
    body: "As some people have pointed out, I think a lot of python programmers are using Eric3. It's got all of the functionality you could want as a python programmer. I don't know what KDevelop has that Eric doesn't when it comes to Python. Although I admit that the GUI could use a bit of 'polish'. (It's got a bit _too_ much stuff in there). But still, it is a great example of what can be done by a one man team (Detlev Offenbach) using Python + PyQt. \n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "JSP in KDevelop"
    date: 2004-12-06
    body: "Does KDevelop support jsp-development?"
    author: "jsp dev"
  - subject: "/."
    date: 2004-12-06
    body: "More fame for Ruby/KDE! \nhttp://developers.slashdot.org/article.pl?sid=04/12/06/1158251"
    author: "KDE User"
  - subject: "Qt and OS X"
    date: 2004-12-07
    body: "This debate about language support by Qt seems rather interesting.  Here we have discussions on how Apple should adopt KOffice, improve it and make it an MS Office killer, but no one bothers to address the lack of Objective-C support in Qt.\n\nHaving worked at NeXT and Apple I can assure you if Qt is adamant about C++ all the way you can bet your ass Cocoa/ObjC is Apple's focus.  But ironically, at least I can develop using C++ in Xcode as well as Java, Ruby, Python and other languages like Fortran.\n\nFor being so Open how is it that Qt doesn't open itself up to ObjC?"
    author: "Marc Driftmeyer"
  - subject: "Objective-C and Qt"
    date: 2004-12-07
    body: "There was Objective-C bindings for Qt and KDE, it was added to KDE cvs in April 2001 before KDE2.2 release. I think most, if not all of the work was done by Richard Dale. The bindings died sometimes round KDE 3.2 and was retired from cvs end of March 2004. The main reason it died was probably because nobody actually used it, except maybe Richard. Don't think it was any protesting to it's removal. To me it looks like the problem are not lack of Objective-C support with Qt, but the lack of developers wanting to use Objective-C with Qt."
    author: "Morty"
  - subject: "Re: Objective-C and Qt"
    date: 2004-12-07
    body: "\"The main reason it died was probably because nobody actually used it, except maybe Richard.\"\n\nYes, that's about right - they never really caught on. The main technical problem is that there is no Objective-C++ on Linux, and you have to wrap the C++ api with C bindings, which means the libraries were huge by the time you have Qt and KDE wrapped for both C and Objective-C."
    author: "Richard Dale"
  - subject: "Re: Objective-C and Qt"
    date: 2006-08-19
    body: "It'll be really nice to have it back now. C++ doesn't work to well next to Objective-C."
    author: "Amit Ron"
  - subject: "Re: Objective-C and Qt"
    date: 2008-03-21
    body: "Damn this is old but if anyone's wondering, the best reason this died might be that anyone can use a QT class in OBjective C using Objective C++ and using the \"has a\" relationship. at least that how I understand it. \nhttp://developer.apple.com/documentation/Cocoa/Conceptual/ObjectiveC/Articles/chapter_12_section_3.html"
    author: "Nehemiah"
  - subject: "VM support?"
    date: 2004-12-07
    body: "Will there be support for any VM in the future? There are some promissing approaches e.g. Portable.NET/Mono or in the future parrot.\nIt seems to me like work on the C# bindings has stopped."
    author: "Sven Langkamp"
---
The <a href="http://www.kdevelop.org/doc/technotes/rubyrad.html">third issue</a> of "<a href="http://www.kdevelop.org/doc/technotes/index.html">KDevelop TechNotes</a>" is out. Improved Ruby programming language support with Qt Designer integration and Ruby debugger is considered as the "killer" feature of upcoming KDevelop 3.2. Therefore this issue describes new "hot" features, discusses RAD using Ruby language and Qt/KDE libraries and lays stress on integrated GUI design with KDevelop Designer.

<!--break-->
