---
title: "Customize.org Invites the KDE Community"
date:    2004-05-01
authors:
  - "bcrescimanno"
slug:    customizeorg-invites-kde-community
comments:
  - subject: "Didn't knew that .. "
    date: 2004-05-01
    body: "Woow I looked here http://www.customize.org/details/33109 and noticed an reference to http://www.crystalgnome.org \n\nWell this is really cool and it will defintely contribute to a more unified look 'n feel of my desktop. \n\nWhat I'm missing though on www.crystalgnome.org  is any reference to the GTK-Qt Theme Engine http://www.freedesktop.org/Software/gtk-qt but maybe that's not its goal\n\nAnyway .. kudos to www.crystalgnome.org and www.customize.org :)\n\n\nFab"
    author: "fab"
  - subject: "Re: Didn't knew that .. "
    date: 2004-05-01
    body: "http://kdelook.org/content/show.php?content=11264\n\nMaybee you're looking at this :)\n\nMissing gnome-vfs crystal but i think it's crystalgnome jobs :)"
    author: "Bellegarde Cedric"
  - subject: "Re: Didn't knew that .. "
    date: 2004-05-01
    body: "Thank you very much for the kudos.  The website will be updated very soon hopefully, so I will look into that theme engine, I did not knew it existed before.\n\nI have also ported the Kids icon set by Everaldo to Gnome, which will be uploaded shortly to Customize.org  (A true SVG set of Crystal SVG for Gnome will be coming soon too, since I have aquired most of the SVGs).  And all of the Metacity themes and such that you see on crystalgnome will be uploaded too within a day or so to Customize.org."
    author: "Exdaix"
  - subject: "Uhm..."
    date: 2004-05-01
    body: "Wouldn't it be more efficient to send this invitation to kde-look?"
    author: "Datschge"
  - subject: "Re: Uhm..."
    date: 2004-05-01
    body: "You expect kde-look to advertize a \"competitor\"?"
    author: "Anonymous"
  - subject: "Re: Uhm..."
    date: 2004-05-01
    body: "I doubt anyone will move there anyway, I'm actually surprised that it got posted here after all. But still, I'd prefer \"customize\" to talk to kde-look directly instead doing a somewhat sneaky news post here."
    author: "Datschge"
  - subject: "Re: Uhm..."
    date: 2004-05-01
    body: "I don't see any harm in anything like this, and I don't see the harm in the post. I think it is good for communities to get together and produce more shared work and I don't see any reason why anyone will 'move here' or 'move there'. People are just doing what they enjoy, afterall. It may very well bring KDE to a wider audience."
    author: "David"
  - subject: "Re: Uhm..."
    date: 2004-05-01
    body: "That's definitely where we were going with this invitation David.  We're supporing the KDE & GNOME communities because we believe strongly in what both communities are trying to achieve; both separately and together.  We feel that by proving a place where artists from all platforms can collaborate, we can bring some of the best works out there to all different mediums."
    author: "Brian Crescimanno"
  - subject: "Re: Uhm..."
    date: 2004-05-01
    body: "The reason it was posted here was because dot.kde.org is the KDE news website, it makes sense to post it here.\n\nCustomize.org sent a \"Welcomes the Gnome community\" to Footnotes not long ago, which is the Gnome news website.\n\nIt's not \"sneaky\" at all."
    author: "Exdaix"
  - subject: "customize.org"
    date: 2004-05-01
    body: "customize org is a very good community. We know how much KDe benefits from KDE-look. Now they can really bridge the gap to the windows world."
    author: "Gerd Br\u00fcckner"
  - subject: "Re: customize.org"
    date: 2004-05-01
    body: "Hello,\n\nwe need no sticking bridge to Luna aestetics.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: customize.org"
    date: 2004-05-01
    body: "1. some of us like Luna\n2. don't presume to speak for the whole community\n3. there is more to Windows themeing than just Luna"
    author: "fault"
  - subject: "Not a very nice website..."
    date: 2004-05-02
    body: "I checked out customize.org from work, and was greeted by 3 popups and an invitation to install GAIM. I know, serves me right to be using IE, but still?"
    author: "John Hughes"
  - subject: "Re: Not a very nice website..."
    date: 2004-05-02
    body: "If you read the latest news post they are working on reducing the pop-ups and such.  The problem is the cost to run the website, since all themes are uploaded and hosted on-site.  Of course, I run Firefox with the Adblock plugin, so I get no banners or pop-ups."
    author: "Exdaix"
---
Recently <a href="http://www.customize.org/">Customize.org</a>, one of the original desktop customization sites on the Web, has added some sections for KDE skins, themes and art such as cursors for general X11.  We'd like to invite the KDE art community to submit their work to our site.  By attempting to bring GNOME, KDE, and Windows artists together under one roof, we hope that we can increase porting and cooperation among all the communities.  So come join us and show us what you've got! 


<!--break-->
