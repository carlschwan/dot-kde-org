---
title: "The People Behind KDE: Till Adam"
date:    2004-03-29
authors:
  - "Tink"
slug:    people-behind-kde-till-adam
comments:
  - subject: "LOL"
    date: 2004-03-29
    body: "\"The day I fit into a T.V. show world please someone shoot me.\"\n\nWEll put :P"
    author: "Alex"
  - subject: "TPBK!"
    date: 2004-03-29
    body: "I just love nested acronyms.  I think my favorite is GNOME:\n\nGNOME stands for GTK Network Object Model Environment\nGTK stands for GIMP Toolkit\nGIMP stands for GNU Image Manipulation Program\nGNU stands for GNU's not Unix, so we finish the whole thing off with a nice recursive acronym!\n\nOf course, I just read the GNOME FAQ and they now claim that the G in GNOME is for GNU, not GTK :(\n\nOh, and nice interview! :)"
    author: "LMCBoy"
  - subject: "Re: TPBK!"
    date: 2004-03-29
    body: "It was always GNU."
    author: "Roberto Alsina"
  - subject: "Re: TPBK!"
    date: 2004-03-30
    body: " \nGNOME = GNU's not Unix Image Manipulation Program Toolkit Network Object Model Environment = GNUIMPTNOME"
    author: "GNOMEr "
  - subject: "Re: TPBK!"
    date: 2004-03-30
    body: "I like this:\n\n\n\"What the Hurd means\n\nAccording to Thomas Bushnell, BSG, the primary architect of the Hurd: \n`Hurd' stands for `Hird of Unix-Replacing Daemons'. And, then, `Hird' stands for `Hurd of Interfaces Representing Depth'. We have here, to my knowledge, the first software to be named by a pair of mutually recursive acronyms.\"\n\nhttp://www.gnu.org/software/hurd/hurd.html#name\n"
    author: "Marcos Tolentino"
  - subject: "Tink, please re-check"
    date: 2004-03-29
    body: "the fish market link under sites to see in his part of the world."
    author: "a.c."
  - subject: "Nice One"
    date: 2004-03-29
    body: "KDE Widows! This series of TPBK articles just keep getting better. I wonder if this is some sort of hint?\n\nSince KDE itself is also an abbreviation, shouldn't the abbreviation be TPBKDE? But that looks like a bit of DNA code. Anyway, I digress...\n\n\"I think KDE is full of little gems that are the result of many busy sunday afternoons being put in by many devoted people.\"\n\nDamn right.\n\n\"An LDAP client and web services infrastructure.\"\n\nI agree here, although is there not some sort of LDAP client lying on KDE apps or something? I can't remember. I agree about web services. KDE provides a hell of a lot programming-wise, but moving into web services and a complete plug-in environment you can use is something that may be a must. Mono, Qt and Qt# may provide this, or some sort of really good open source Java implementation. Not quite sure about Eclipse at the client-end - it looks like a bit of an orphan, particularly licensing-wise. Despite my misgivings over Mono (none of it patent related) I'm willing to believe that, if handled correctly, it could be a positive thing. I'm not going to get religious over it though.\n\n\"And KTeatime, of course, the single most useful application ever conceived.\"\n\nOh yer, KTeatime! If people want to get Linux (non-Windows) desktops and KDE successfully into businesses and organisations, just hype this little applet. I'm actually not joking here, which is probably a sad reflection of the sorts of people there are within many organisations :).\n\n\"About twice a week we watch Pride and Prejudice together. That works for us. :)\"\n\nNot another one!\n\n"
    author: "David"
  - subject: "Re: Nice One"
    date: 2004-03-30
    body: "What role do web services play in a Desktop Environment? And what exactly are web services? I assume its another way of saying a web application."
    author: "Ian Monroe"
  - subject: "Re: Nice One"
    date: 2004-03-30
    body: "\"What role do web services play in a Desktop Environment?\"\n\nYep, web services is definitely a marketing term and I don't like it, but it is really a way of saying that the barriers of a client and a server are broken down and you are really developing across what used to be boundaries. It's all a bit more than a web application, but since there is no real defined terms for either it is difficult to explain. Anyway, being able to do all that in a unifying framework (not necessarily one to rule everything - .NET, Mono etc. :)) is a good idea, although rather 'blue sky' at the moment, certainly for KDE I think.\n\nCertainly don't buy the hype.\n"
    author: "David"
  - subject: "Re: Nice One"
    date: 2004-03-30
    body: "A lot of it is the ability to make remote procedure calls over HTTP.  Remote procedure calls are when an app calls a method (procedure call, legacy language) on an object in an app on another machine (remotely).  If you know about DCOP, same idea, but over the network.  They've been around for a long time but have mainly been used in tightly integrated systems, where you wouldn't know it was going on.  'Web' just means you use HTTP to transmit these calls, which saves you having to arrange with all your firewall admins to 'open port 9999 on machine XYZ please' because 80 is probably already open for HTTP, and because straight Web servers can be extended to do Web Services.  Of course, this can become a new security nightmare.\n\n\"What role do web services play in a Desktop Environment\"?\n\nI don't know about offering services, but as a user of services there are plenty of applications.  I have a little python script here that can send SMS messages using a Web Service - plug it into Kopete and you have an app.  Web Services are a lot more robust way to do this than a script that goes away and fills in the fields on a web pages, and breaks every time someone changes the page...\n\nOr KDict... Or KWeather... "
    author: "Will Stephenson"
  - subject: "Re: Nice One"
    date: 2004-03-31
    body: ">>'Web' just means you use HTTP to transmit these calls, which saves you having to arrange with all your firewall admins to 'open port 9999 on machine XYZ please' because 80 is probably already open for HTTP, and because straight Web servers can be extended to do Web Services. Of course, this can become a new security nightmare.<<\n\nNot really. The existence of web services does not make allow any firewall tunneling that wasnt available before. An an administrator who believes that allowing HTTP only lets people access the web is pretty naive (and even more if HTTPS is allowed, which is even better for tunneling).\n\nIf you allow any kind of access (HTTP, SMTP, even DNS!) to random computers your users can transmit everything that they really want. It is less convenient, but for security that's not really relevant.\n\n \n"
    author: "AC"
  - subject: "This is it for Suse's KDE Support!"
    date: 2004-03-29
    body: "http://www.eweek.com/article2/0,1759,1553087,00.asp\n\nWhy Am I surprised?\n\n/D"
    author: "David"
  - subject: "Re: This is it for Suse's KDE Support!"
    date: 2004-03-29
    body: "And the point of posting this here is?"
    author: "David"
  - subject: "Re: This is it for Suse's KDE Support!"
    date: 2004-03-29
    body: "Frustration?\n\nBTW: are you me? :)"
    author: "David"
  - subject: "Re: This is it for Suse's KDE Support!"
    date: 2004-03-30
    body: "\"Frustration?\"\n\nCertainly looks like it, but if you are, it isn't my problem.\n\n\"BTW: are you me? :)\"\n\nNo I can guarantee you I'm not, so stop trying to pretend I'm posting stuff that you are please."
    author: "David"
  - subject: "Re: This is it for Suse's KDE Support!"
    date: 2004-03-30
    body: "> Why Am I surprised?\n\nIf you are then you missed the discussion about it on this site on week ago. Also seeing your topic you misinterpreted something: SUSE doesn't cease KDE support."
    author: "Anonymous"
  - subject: "Till Adam"
    date: 2006-07-20
    body: "I'm trying to reach Till Adam. Till-billy - my email is marthatori@yahoo.com. You know who this is - it's Martha - TAG!"
    author: "Martha"
---
<a href="http://www.kde.nl/people">This week's</a> <em>TPBK</em> [The People Behind KDE] travels to <a href="http://www.laboe.de">Laboe</a> in Germany for a chat with one of KDE's more recent additions to the community.
He has been an active developer since a year and  has already managed to make his mark. He's known among friends for talking too much about too many things, is not very fond of green shorts and keeps his wife on a stable release. It's <a href="http://www.kde.nl/people/till.html">Till Adam</a>!





<!--break-->
<p>
On the <a href="http://www.kde.nl/people/news.html">TPBK News page</a> we feature this week an item about the Orkut Community, <em>KDE Widows</em>, created by Birte Lilienthal, who coincidentally also happens to be Mrs. Adam! ;-]
Hop over to the <a href="http://www.kde.nl/people/news.html">News page</a> to get the scoop about the KDE Widows Community.
</p>


