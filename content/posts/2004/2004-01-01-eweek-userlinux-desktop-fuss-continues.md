---
title: "eWEEK: UserLinux Desktop Fuss Continues"
date:    2004-01-01
authors:
  - "numanee"
slug:    eweek-userlinux-desktop-fuss-continues
comments:
  - subject: "I must say"
    date: 2004-01-01
    body: "That's one *hell* of a teaser! 2004 is going to be one kick-ass year for KDE!\n"
    author: "Rayiner Hashem"
  - subject: "What's his plan anyway?"
    date: 2004-01-01
    body: "Hi,\n\nso what's the deal about UserLinux at all. What can it be that is not already out there. Being a Debian fan, may I say that if not Debian GNU/Linux then SUSE will offer exactly that?\n\nSure, it's all buzzword, lots of promises. More or less what they actually can release has to be \"Debian testing\" with non-free addons like closed source drivers, but which exactly who will validate? \n\nWho will validate? How will UserLinux exactly be financed? What exactly will it allow a corporate \"user\" that Debian not already does. Debian has a open structure, will even give all choice and ... everything but quick series of releases. \n\nIf it's just for the desktops to use and a development platform to target, SUSE will do for now. One day, Debian will also be good enough for most companies. So?\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: What's his plan anyway?"
    date: 2004-01-01
    body: "From lurking on the list, this is what I gather ...\n\nThe central non-profit (UL) will brand the distribution and certify support providers.  It will not have any technical staff and will be financed through fees that the support providers pay to become certified.  I don't think it will be a large scale operation.\n\nThe technical decisions will be made based on a \"meritocracy\", with Bruce presiding as a benevolent dictator (to step in when consensus cannot be reached, or any other reason he deems important enough--like the GPL vs LGPL decision).  The existing Debian infrastructure will be used to maintain the distribution source code, and UL will contribute back to Debian.\n\nFor example, Bruce is primarily working on the installer right now, which (AFAIK) is one of the main things holding up the next release.  (Well that, and 500+ release critical bugs ...)  From what I understand, he wrote the first installer so this seems like a good fit.\n\nThe central non-profit will provide a central 800 # where support calls go--these leads get farmed out to the support companies that have been certified.\n\nI guess this is the main difference w.r.t. a corporate user--a central point of contact for support that remains constant and controls what is in the distribution.  It's different from existing models in that the support is provided in a distributed model--for example, he has made it clear a couple of times that the certification process should not rule out sole-proprieterships.\n\nThat's my understanding ... "
    author: "Mark Bucciarelli"
  - subject: "Re: What's his plan anyway?"
    date: 2004-01-01
    body: "\"For example, Bruce is primarily working on the installer right now\"\n\nNot another bloody installer."
    author: "David"
  - subject: "Re: What's his plan anyway?"
    date: 2004-01-01
    body: "The new Debian installer.\n\nhttp://www.debian.org/devel/debian-installer/"
    author: "Mark Bucciarelli"
  - subject: "Tired of UL alread!"
    date: 2004-01-01
    body: "I have really no faith that UL will survive. I mean, really. When it comes down to it, it's just another distro based on debian. Does it mean anyone will use it? \n\nTake this as a factor. Novell, now owning Suse/Ximian, will be pushing linux hard. The places UL is trying to cater to, small IT shops, alot of those IT shops are running Novell services. So now that Suse is backed by Novell alot of those IT shops will just go with the same company that they have been trusting for years. Why would they use a fresh distro that has not \"proven\" itself yet and is in all technicality, crippled.\n\nUL is just a big mess of political nonsense that no one wants to deal with. If they want to deprive their users of quality free software, then it's their own necks they are hanging themselves by."
    author: "Namaseit"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "quality-free software?  LOL!"
    author: "cm"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Read again. And don't forge."
    author: "Anonymous"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Don't worry, I was just kidding :-) \n"
    author: "cm"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "See the URL below for some news about upcoming Novell services:<br>\n<a href=\"http://www.linuxplanet.com/linuxplanet/reports/5175/1/\">\nhttp://www.linuxplanet.com/linuxplanet/reports/5175/1/\n</a>\n"
    author: "Waldo Bastian"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Wow!  The article says the GUI will support either KDE or GNOME!  Good to see Novell keeping their KDE support.  Maybe the Ximian division will start doing KDE development?"
    author: "KDE User"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Does the article say it'll support *both*?\nI didn't understand it that way.\n\n"
    author: "cm"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Yes, it does say that in my opinion."
    author: "KDE User"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "No, it doesn't.\n\nIt just says they will choose one of the two.\n\nTo avoid a big public tohoowabohoo now, they will announce later, *after* the fact, that they only support a GUI of Gtk/Gnome.....\n\n ;-(\n .\n ."
    author: "Pessimist"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "I see 'a GUI that will support \"either KDE or GNOME,\" ' in the article. \n\nIIRC \"either ... or\" means a mutually exclusive or relation."
    author: "'nother KDE User"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "No.  It's just a way of talking that might not be familiar to non-native English speakers.  I'm pretty sure he means BOTH."
    author: "KDE User"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Could you ask *him*? Maybe write an e-Mail? And then report back here with a dot story?"
    author: "Pessimist"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Actually as a \"native english speaker,\" the statement is ambiguous. It could mean both, or only one or the other. When I read it it seemed to me to be one of them would be supported but not both, and they'd tell us after the fact. I think you are being overly optimistic."
    author: "Tormak"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-02
    body: "The following quote from the depth of my archives came to my mind: \n\n\"The warning message we sent the Russians was \na calculated ambiguity that would be clearly understood.\"\n\t\t-- Alexander Haig\n\n(Though I don't know the original context. Cold war, AFAIK)\n\n"
    author: "cm"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-02
    body: "Waldo? Your take on that?   ;-)"
    author: "Kurt Pfeifle"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "Not necessarily. You can say \"the M-16 can fire either a 40mm grenade or a 5.56mm round.\" That means it supports both. So the statement is really ambiguous, it can mean it will support both GNOME and KDE, or GNOME or KDE."
    author: "Rayiner Hashem"
  - subject: "Re: Tired of UL alread!"
    date: 2004-01-01
    body: "\"You can get it for people as a stocking stuffer,\"\n\nI think that tells you how seriously you can take this."
    author: "David"
  - subject: "Re: Tired of UL alread!"
    date: 2006-10-11
    body: "i like it when i do a poo and let it out slowly-it feels guuuuuuuuuud ;D"
    author: "al23"
  - subject: "Re: Tired of UL alread!"
    date: 2006-10-11
    body: "riggghhhhhhhhhht.................wierdo lol"
    author: "lily"
  - subject: "Also"
    date: 2004-01-01
    body: "I read a linked article from the posted eweek article. And Bruce says something interesting.\n\nBut, in the end, Perens said he \"made a decision for this particular project. I don't pretend that it was a democratic decision, and we're moving on to other levels. I can't be everything to everyone.\"\n\nSo basicly he was lying when he even asked for debate. He had his mind made up. Nothing was/is going to change it. It's not UserLinux, its BruceLinux. I thought this was supposed to be a *COMMUNITY* project. I though *USER* inplied user involvement.\n\nAlso something interesting from the article. UL has no known developers and the KDE/Debian initiative has 35 developers. Interesting, to say the least.\n\nSo despite Bruce's efforts, he isn't going to kill KDE. "
    author: "Namaseit"
  - subject: "Re: Also"
    date: 2004-01-01
    body: "It's the developers, stupid.\n\nTo paraphrase an election slogan. KDE and Gnome both have very active development communities. Neither will go away. In fact, despite what some think, it is in no-ones interest that either go away. Monocultures either proprietary or free, don't work well.\n\nThere are some that wish KDE would go away, but they only speak for their companies, not any community. This fuss is not about free software, it is commercial competition pure and simple.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Also"
    date: 2004-01-01
    body: "> There are some that wish KDE would go away, but they only speak for their companies, not any community. This fuss is not about free software, it is commercial competition pure and simple.\n\nExactly. These people only wants to use what is free without giving back to the free community, or even outrun their free competition in the future.\nIs this really just and fair?"
    author: "OI"
  - subject: "Re: Also"
    date: 2004-01-01
    body: "> I thought this was supposed to be a *COMMUNITY* project.\n\nA true community project doesn't need one million dollar funding to kick off."
    author: "Anonymous"
  - subject: "Re: Also"
    date: 2004-01-01
    body: "wonder why they chose userlinux.com and not userlinux.org...\n"
    author: "OI"
  - subject: "Re: Also"
    date: 2004-01-01
    body: "Nor does it need backers who according to Bruce do not want the publicity at this time. So the obvious question is, who are these backers? SCO?"
    author: "stumbles"
  - subject: "GraphicalUnicity"
    date: 2004-01-01
    body: "See\n<a href=\"http://cgi.userlinux.com/cgi-bin/wiki.pl?Proposals/GraphicalUnicity\">\nhttp://cgi.userlinux.com/cgi-bin/wiki.pl?Proposals/GraphicalUnicity</a> - it will be interesting to see how they want to make a Qt branding theme without including Qt (forcing ISVs to ship statically Qt-linked applications which don't support style plugins).\n"
    author: "Anonymous"
  - subject: "Awesome news"
    date: 2004-01-01
    body: "I think this initiative is exactly the right thing for KDE to\nbe focusing on right now.  In the past, distros just didn't\nstep up to the plate and bridge the gap between what\nKDE offered and a complete desktop OS system.  SuSE\nprobably came closest, but even they fell short.  (In that\nsense maybe the Novell merger will turn out to be a good\nthing for KDE, sinced it probably helped spur on this work.)\n\nMy only question about all this is can the KDE/Debian\ninitiatives be given their own project name?  (KDE OS? \nKebian? hehe)  Seriously I think the association with \nUserLinux will end up as an historical footnote.  Why not\nmove now to brand the new project so it has a more \nrecognizable identity, independent of UserLinux?"
    author: "steve"
  - subject: "UserLinux? Right."
    date: 2004-01-01
    body: "UsereLinux is bollocks. Enterprises are never going to use it or even get to trusting something like that. Since proprietary software will be developed for it anyway, if by some miracle it is successful, the logic behind it is totally flawed. I think many in the free software/open source communities, with no experience outside, are very, very deluded."
    author: "David"
  - subject: "It should just be a branding project"
    date: 2004-01-01
    body: "a sub porject of Debian ... nothing more.\n\nUnfortunately it seems like UL is trying to be YET ANOTHER distro ..."
    author: "Ogga"
  - subject: "Good reply to Perens"
    date: 2004-01-01
    body: "Eugenia from OSnews said it best\n<a href=\"http://www.osnews.com/comment.php?news_id=5475&offset=15&rows=30#179969\">\nhere</a> and \n<a href=\"http://www.osnews.com/story.php?news_id=5475\">here</a>.\n"
    author: "Matt"
  - subject: "\"UserLinux and KDE\"... Debacle?"
    date: 2004-01-02
    body: "So, a distribution chooses to not ship KDE, and it's a \"debacle\"? Wow. There sure is a heck of a lot of noise coming out of KDE on issues like this at the moment, and it's not looking professional or justified <i>at all</i>. It's KDE people who are always pointing out that more distros ship KDE as the default desktop environment... Do you hear the GNOME people whinging about it? Nuh-uh, they just get on with the job."
    author: "Surprised"
  - subject: "Re: \"UserLinux and KDE\"... Debacle?"
    date: 2004-01-02
    body: "I hope you are aware that the wording is mostly used by the \"press\" and other rather unrelated and univolved audiences. If you care to dig into the KDE developer community you'll find out that the vast majority doesn't really care and does business as usual."
    author: "Datschge"
  - subject: "Re: \"UserLinux and KDE\"... Debacle?"
    date: 2004-01-02
    body: "Ah, but it was core KDE developers who were involved in, and largely instigating, the whole mailing list mess. :-)"
    author: "Surprised"
  - subject: "Re: \"UserLinux and KDE\"... Debacle?"
    date: 2004-01-02
    body: "I counted two developers who happen to be involved with Debian as well, so they were affected both ways (no business as usual possible in that case). You don't expect them to abandon one side just due to Bruce's decision, do you?"
    author: "Datschge"
  - subject: "Re: \"UserLinux and KDE\"... Debacle?"
    date: 2004-01-02
    body: "Certainly didn't expect the behaviour."
    author: "Surprised"
  - subject: "Re: \"UserLinux and KDE\"... Debacle?"
    date: 2004-01-02
    body: "What \"the\" behavior?"
    author: "Datschge"
  - subject: "one ironic thing"
    date: 2004-01-02
    body: "...about UserLinux standardizing on GNOME/Gtk because it's \"easier for enterprise developers\" is that there are no VB-like tools based on Gtk, and at least two nearly mature ones for Qt.  I have yet to find a business with more than 100 or so employees with no VB apps, and those VB coders aren't suddenly going to pick up C overnight.  And then of course there's Access [shudder], which isn't really duplicated at all under Linux but at least KDE has Rekall if people can stomach a little python.\n\nAnd no, Glade does *not* count.  (Neither does wxBasic, at least till they come up with an IDE like VB.)\n\nI wonder how the UL and Ximian guys will react when HBasic or Gambas hits 1.0 (which will, in all likelihood, occur sometime this month.)\n"
    author: "raindog"
  - subject: "Re: one ironic thing"
    date: 2004-01-02
    body: "http://www.go-mono.com/\nhttp://www.gtk-sharp.sf.net/\nhttp://www.eclipse.org/\n"
    author: "Biswa"
  - subject: "Re: one ironic thing"
    date: 2004-01-02
    body: "While I agree with you on the VB like apps, I don't agree on the point \"easier for enterprise developers\".\n\nWhen you start a project with eg. Gambas, then the code must be GPL if you distribute it, as it is linked against the GPL-QT version.\n\nYou now might state, that you can keep it in your company, so it's not a \"distribution\". But how many companies have subisdiaries in the same contry or even overseas. I would need a lawyer here, but from my point of view, this is then distribution if the same tool is used in these subsidiaries. Therefore it then must be GPL.\n"
    author: "Philipp"
  - subject: "VB like"
    date: 2004-01-02
    body: "Hbasic - qt/.Net based\nhttp://hbasic.sf.net\n\nGambas - gtk based\nhttp://gambas.sf.net"
    author: "elektroschock"
  - subject: "Grammar Cop Warning"
    date: 2004-01-02
    body: "fruitation ==> fruition.  Please see dictionary.com for more information."
    author: "AC"
  - subject: "What is the new Debian tools ?"
    date: 2004-01-02
    body: "I never heard of them ?\nWhat can we do with them ?\nAny link ?"
    author: "Debian lower"
  - subject: "A view from somewhat outside"
    date: 2004-01-03
    body: "First, let me note that I'm historically a (classic) Machead (and developer), my views are not well aligned with any UNIX party line. I'd take Copland/Gershwin over Jaguar/Panther anytime. Recently, I was \"thrown into cold water\" and had to port the 2.4 kernel to some embedded hardware, so I gained a few insights into Linux development. Here's my opinion on the QT/UserLinux/Desktop stuff (and a bit more):\n\n1.) Desktop choice\n\nFunnily, I run non-default desktops: KDE 3.1 on Redhat 9 at work, and Gnome 2.4 on Mandrake Cooker at home. Reason for KDE was that Gnome 2.2 was sluggish and so feature-poor that it was no fun. Gnome 2.4 has caught up well enough to be usable, and now I prefer the \"mousefeeling\" of Gnome (to bend the McD-Testers \"mouthfeeling\" impression a bit). I have no idea how quantify this, and I may be biased because Nautilus has been done by Ex-Apple-Finder people. But it certainly has something to do with a cleaned up and simple appearance. And, not trolling, I feel KDE is closer to the spirit of Windows, while Gnome leans ever closer towards the Mac (spatial Nautilus!). One example is the OK-Button order where Windows diverged from the Mac way for no good reason (well, possibly because of ancient SAA-Key-Controls, or just to look different). Thought food, maybe.\n\n2.) The QT-not-LGPL issue\n\nForget your QT-centric arguments. Period. No matter how much QT is worth and businesses can fork up. It's not how it works out there. I code for some large companies with enough petty cash to buy the whole Troll(.no) shop, and the QT licensing is a TOTAL showstopper in any engineering decision process. Problem is that these large companies have a Dell|HP|whatever Windows-only default policy as a decree from the \"decisionmakers\". Anything else will have to go through a purchase process with these people. That will put engineers on the radar of the PHBs, stupid questions ensue, and you end up with a \"do it with our Windows tools, and use our new [whatever-XML-tool], btw.\" order. No logic, no economics, politics.\n\nUnless QT salespeople win a whole shop over from the top, Linux development slips in from the grassroots, and engineers will chose a freely available and DEPLOYABLE solution because of said politics. By the time, the slip-in is a success to stay, the GTK/Gnome chain is entrenched. And if it fails, there is no evidence or commitment in the books. While with QT, the proponents would be scapegoats, a la \"You made us buy these tools and now you don't deliver.\" (Actually that can happen even if they do deliver...). For these very reasons, some hardware production support software I work on will be done on PyGTK (plus, it can be rather easily put on Windoze laptops for field support).\n\nAll similar to the VHS story, where the superior formats (2000, Beta) were license encumbered and lost out. Qt was the first _decent_ toolkit, but by now GTK is good enough (and so is Glade, though it sucks goats).\n\n3.) What the KDE team should do (just from _my_ point of view)\n\nScratch the remaining itches. Five different demo disks, Knoppix, MandrakeMove, some Slackware-based stuff, whatever, don't make a difference, if the stuff doesn't work great beyond the CD its on. That includes system administration and software deployment. KDE went over the well-established Motif standard and came up with something new. It may be time to think bold again. Here's my totally uneducated masterplan:\n\n- split up KControl into KControl and KAdmin. KControl does everything the non-privileged user wants to do in his session.\n\n- KAdmin controls the system (and requires root): Clock, Firewall, services, mounts, global shares, and whatever drakconf or yast does. In a pleasing manner, not mirroring *.conf cryptics.\n\n- Unlike XST/GST, don't use backends for KAdmin. Stick to the LSB (or so), accomodate slight differences where easy, but don't cater for an exotic variety. So there's one way, and that can be brought to work perfectly in this terrible mess of system configuration.\n\n- Provide a KDE reference platform for KAdmin. This will also be used to create the \"Official KDE Reference CD\". At this point, welcome to the distro business, btw.\n\n- Now rethink packaging. While yum or apt-get might be your friends here, if I want to install an application, I want to install \"Application 1.23\" in German, and not appl-1.23-9kdx, appl-i18n-de-1.23-9kdx, libappl-1.23-9kdx, and libwslinfro-0.91-1kdx (that was \"lib-whatever support lib is needed for real operation\", btw). Mark \"main\" packages, give them clear names. For initial distribution, metapackages will be needed if the fine granularity below must stay. Pack everything not in the base system into the meta-packages provided for download.\n\n- I've given away above, that I'd call the whole lot \"KDX\". A nice, industrially compatible, buzzword-ready TLA. Canonical software might be named like \"Kate 2.2 of KDX\", and third-party software could be like \"Scribus 1.2 for KDX\". The end result is that I download ONE file with that name (or maybe application-1.23.kdx), the installer takes care of not overwriting a newer version of libwslinfro, sees that I'm German, and stuffs in appl-i18n-de-1.23-9kdx. I have a tea while the progress bar wiggles along, and when I come back, the app is ready to run.\n\n- All this DOES need the commitment to throw a certain amount of (script file etc.) compatibility overboard, but it CAN be done in a way, where glue to legacy systems can be maintained. It might actually provide convenience to \"common\" distros, where \"kdx-to-dpkg|rpm|tgz\" tools would totally automate packaging beyond the pain of distro-specific specfile maintenance. I don't think this break with legacy is much bigger than what KDE originally did with the accepted Motif.\n\nNote: With C++ and the fragile base class problem (and mentioned autoconversions), this might mean \"source packages for deployment\". With the amount of iBooks and upcoming AMD64 systems, this might actually help a lot.\n\n- When all bits are in place, make bootable ISOs for the popular platforms that work nicely as CD-boot systems with an USB dongle, which can \"infect\" clueless PCs through a friendly graphical installer (we're talking Desktop, NOT headless MIPS-based 1\" servers). Et voila. World Domination.\n\nWell, almost. Friggin lobby the trolls to LGPL at least their Linux QT. Even with all the shit above, the other side is \"good enough\". If people were willing to pay a bit extra for much better computers, we'd be all using Macs now.\n\nThanks for reading and hope you enjoyed the rant!\n\nRich\n"
    author: "Rich"
  - subject: "Re: A view from somewhat outside"
    date: 2004-01-03
    body: "haha,\n\nqt actually has a non-Linux market. And there are no gtk applications out there.\n\nqt creates an incentive to license your programs as gpl whereas in the gnome world we see an incompatible license mix.\n\nQt is not expensive and used by many companys that even don't serve the Linux or KDE market. It's nothing special for a company to buy a toolkit such as they buy VisualC, Delphi and so on."
    author: "andre"
  - subject: "Young friend,"
    date: 2004-01-03
    body: "try directing your plentiful energy towards raising awareness and education. You will need these skills once you are sent down to fight in the corporate trenches. (Reading Dilbert helps understanding, too).\n\n> qt actually has a non-Linux market\n\nSo has Borland. I like Delphi. It's just not mainstream. Heck, people still keep the Amiga alive. But it is entirely irrelevant, as this discussion is about a Linux distribution with an appeal as broad as possible.\n\n> And there are no gtk applications out there\n\nI assume that in your eagerness to reply, you forgot \"on Windows\". Well, except for our little in-house stuff. And maybe the Gimp. Or Pan. I like Pan.\n\n> qt creates an incentive to license your programs as gpl\n\nYoung friend, whether I have an incentive or not is totally irrelevant. I am not in the position to license out anything at all, for I just carry my skin to market in exchange for a little money. The people who give me money 0wN everything I do for them in return. This decision is made by corporate lawyers in a country far, far away, behind a large ocean. Lawyers may look like people in suits, but really they're totally different from us people inside. I just know I'd be royally screwed if I did anything these lawyers don't agree with.\n\n> It's nothing special for a company to buy a toolkit\n\nOh yes. Just yesterday I saw a big industrial company walk into the software shop and pick up a copy of Mandrake 9.2 Powerpack. I thought, *wow*, what a cool company not to get Windows XP. Well. Fun aside. You must not only be young, but in a very remote location, too, to not have heard about the evil forces summoned by trying to purchase ANYTHING in a big industrial company. Two vertical layers of approval plus providing a vendor certification is for entry level skills. This gets you about a nice ballpen and a sketchpad. With software, other obstacles get involved, from license management to the IT department.\n\nNow get this in your tiny head: In big companies, Programmers don't decide about toolkits. \"Decisionmakers\" do. They do so by looking at a glossy 3-page-foldout advertising the toolkit and then deciding. It helps if the foldout has a lot of blue that matches their suit and says \"Microsoft\".\n\nYou CAN NOT run a QT purchase past these \"Decisionmakers\" as long as it's not corporate policy to use it. The only way to get Linux in at all is for the people doing the work to take what they can get for free and just make it work. In most cases, the reality is not really acknowledged, but accepted once it works.\n"
    author: "Rich"
  - subject: "Re: Young friend,"
    date: 2004-01-04
    body: "\"You CAN NOT run a QT purchase past these \"Decisionmakers\" as long as it's not corporate policy to use it.\"\n\nWell, that rules out GTK then."
    author: "David"
  - subject: "Big Picture - Why LGPL is Necessary"
    date: 2004-01-04
    body: "For any company, in house development is a very big issue.  The safest route is with the biggest standard - which is MS Visual C++ or Visual Basic.  But with this approach comes the whole licensing issue, cost per desktop, one platform, one vendor lock-in etc.  Which of course is why many of us have chosen to develop and operate on Linux.  So what are the Linux/Win alternatives?  \n\nI chose Qt and it is an excellent C++ toolkit.  It is well written and stable and thus, allows somewhat rapid development, though not like VB.  The development tools (Designer, KDevelop, etc) are now mediocre (KDevelop is still fairly buggy, no GUI integration).  The code is somewhat non-standard with moc, .uih, QValueVectors etc.  It is very expensive ($3000 per dual-license plus $1000/year (per programmer - not even per seat) and has very strict commercial licensing.  As my company expands, so does my overhead. So in retrospect, it costs me more money over MS tools, keeps me in the whole licensing mess, and is somewhat Vendor specific, requiring a programmer to be educated in the Qt way.  It is very cross-platform, but is that enough to offset the other disadvantages? \n\nFor non-gui intensive applications, there is java (which is free), an entire development package (Ant, Eclipse, Junit, NetBeans, etc) which are also free, and most CS professionals have some java experience.  Maybe SWT will overcome the gui issues with java since the gui coding, performance, and visual issues are very real (been there, done that).\n\nFor fast, pure C/C++, gui applications there is Gtk/Gtkmm which is freely distributable, free in cost, and is the standard that Sun, RedHat, and Novell have chosen.  Is it as pretty as Qt or MSVC/VB? No, but good enough.  Is it less productive than Qt?  Probably, but the how much is debatable. Does it have the developer tools as MS or Java? No.  Are the developer tools much worse than Qt?  Not really.  If I choose Gtk (or better yet, Gtkmm), I might lose some productivity of my employees (certainly a lot from VB), but gain the advantage of cross-platform, free cost, and no licensing hassles.  Is it worth it over MS VC/VB? Maybe.  Is it worth it over Qt?  Comparing the facts, probably.  At least productivity can be addressed with templates, etc.  \n\nMost companies are slashing overhead where ever they can.  And all software development productivity, no matter what package is used, is viewed as lousy.  Thus, when you compare approaches, Qt costs $3000, MSVC/VB costs $1000 (the safe approach), and Gtk costs $0, Qt is going to be a very tough sell.  A niche product.  And do I want to base my company software on a unique product that is a tough sell?\n\nSo I hope this explains why LGPL and low-cost(if not free) development tools are necessary to provide the incentive for companies to develop on Linux instead of Visual C/VB."
    author: "john"
  - subject: "Re: Big Picture - Why LGPL is Necessary"
    date: 2004-01-06
    body: "Where are the tools and support for GTK? Qt is also a heck of a lot more than graphical toolkit."
    author: "David"
  - subject: "Re: Big Picture - Why LGPL is Necessary"
    date: 2004-01-07
    body: "For any company, in house development is a very big issue. \n[...]\n\nYou have no clue what you are talking about. You can do as much \"in-house\" development with KDE/Qt as you whish. GPL is all about _distributing_!! As long as is stays in-house, you don't need LGPL. Since you don't know this, I assume atht you didn't do the sligtest research. since you didn't do the slightest research, I assume you didn't actually spend $3k for commercial Qt or you have so much $$ that you don't care. Since the former is probable, I conclude that you are a liar and have no clue about any big picture.\n\nCheers"
    author: "silversun"
  - subject: "Multiple Toolkits??"
    date: 2004-01-05
    body: "Hello-\n\nI recently read both sides of this UserLinux (KDE vs. GNOME) debate on the www.userlinux.com site, and I must say, I'm a little perplexed by something and I was hoping that someone could explain it to me.  Let me start though by saying that I am a much bigger Mac OS X user then a day-to-day Linux user and that my development group actually uses professional Qt for all of our products (which incidently run on Mac OS X, Linux, IRIX, and Windows)  - so you know where I am comming from.  \n\nAnyway, Bruce makes these statements:\n\n\"But all of the efforts to unify these two desktops do not change the fact that there are two entirely different GUI SDKs. The two competing GUIs are each of a complexity equal to or greater than that of the Linux kernel. For developers and support staff, maintaining expertise in both of two GUIs is an expensive proposition. Many IT shops, when faced with such choices, have decided to consolidate to fewer options in order to reduce expense.\"\n\n\"UserLinux is intended to be a system for business people. Central to its design is a network of competing for-profit service providers, who perform engineering and support services for the system. Because these service providers are basing their business upon a commodity product, there are already economic limits upon how profitable they can be. The difference between one and two GUIs may spell profitability or bankruptcy for some of our service providers. In a similar vein, internal support and engineering staff at businesses that employ UserLinux would like to have only one GUI SDK to develop for and maintain. This is not to say that choice is bad. Rather, it's bad when people aren't allowed to choose.\"\n\nI certainly can't disagree with much of this statement.  It is certainly true the toolkits are complex and there may be greater cost associated with doing it this way.  However, I do take issue with one very important point: Apple also maintains separate, competing toolkits - very successfully I might add.  You can completely develop an application from the ground up on any one of these toolkits without touching any of the other frameworks.  These include:\n\n\tCarbon - a C/C++ based framework that is more closely related to the \"old\" Mac OS 9 (and lower) \"Toolbox\".\n\n\tCocoa - an ObjectiveC-based framework that is more closely related to the Next/OpenStep.\n\n\tJava - well we all know what Java is.\n\nIt seems that the real question is whether you want to provide the greatest breadth of applications with a unified look-and-feel for users, along with the greatest number of choices for developers so that there can continue to be the greatest breadth of applications.  I think we can all agree that the current level of unified look-and-feel for these applications is certainly not there and that much of this is probably more or less due to the greatest number of choices for developers (perhaps more should be spent on this KDE-GNOME unification front).  However, it seems that as UserLinux summarily, and may I say illogically, throws out a toolkit just because they don't want to worry about support, that the bigger problem will be the lack of applications.  He makes a very good point that you can freely develop proprietary software using gtk and that is something that you do not get with Qt.  But, one could also argue that a very important reason why Qt is better then gtk is so many ways is that it does have a budget for QA/QC.  What is really the additional cost associated with providing Qt?  Again, analogously, Apple too provides several toolkits, yet they encourage developers to develop using Cocoa.  Can't UserLinux do the same thing?\n\nOh well.  Admittedly I really don't know Bruce, but it seems that his logic is flawed. I can't see how this version of Linux will be any different from any other version of Linux - except perhaps that it is less useful since it lacks most of the applications that many people use (without going through an install process).\n\n-Lance\n____________________\nLance M. Westerhoff\nChief Software Engineer\nQuantumBio Inc. \n        - \"A quantum improvement in drug discovery.\"\n"
    author: "Lance Westerhoff"
  - subject: "spellcheck isn't all it krax up to be"
    date: 2004-01-06
    body: "things come to fruition, it's true, and this year seems like a bumper crop!"
    author: "michaelbrower"
  - subject: "Been there, seen that."
    date: 2004-01-08
    body: "It' kind of deja v\u00f9.\n\nAfter Gnome 1 -> Qt goes to an OSI aproved licence (QPL was that ?)\n\nAfter Gnome 2, Sun & Gnome Foundation -> QT goes GPL and KDE Free QT Foundation.\n\nNow with only one of these happening...\n- IBM Desktop initiative goes GNOME.\n- Novell & SuSE goes GNOME (hey Sun says GNOME supports java apps ;-)\n- UserLinux & Debian taking some ground on corporate use\n... then Qt will go LGPL, and THIS IS GOOD, we will have two free, enterprise ready desktop alternatives. "
    author: "Anonymous George"
  - subject: "K3b?"
    date: 2004-01-08
    body: "All I can say is how sorry I'm going to feel for those UserLinux users who won't be able to use the most beautiful CD/DVD Burning software in the entire world... K3B !\n\nI wouldn't want to use UserLinux purely for that one reason."
    author: "anonymous"
---
<a href="http://www.eweek.com/">eWEEK</a> is currently <a href="http://www.eweek.com/article2/0,4149,1424337,00.asp">featuring an article</a> on the <a href="http://desktop.kdenews.org/strategy.html">UserLinux and KDE</a> debacle.    Indeed, exciting times are ahead as we forge on with our plans for both <a href="http://enterprise.kde.org/">KDE::Enterprise</a> in general and the <a href="https://mail.kde.org/mailman/listinfo/kde-debian/">KDE/Debian project</a>.  You've heard of cool hacks to make KIO slaves accessible on a system-wide basis, you've heard of GTK/KDE integration (don't be surprised if KDE <a href="http://www.gimp.org/">Gimp</a>, a.k.a. Kimp, makes an effortless return), you've heard of the new Debian tools in development, you've heard of a KDE/Debian live CD.  All of this and more is coming to fruitation.  See you next year!

<!--break-->
