---
title: "KDE at LinuxWorld London Report"
date:    2004-10-14
authors:
  - "jriddell"
slug:    kde-linuxworld-london-report
comments:
  - subject: "Do we need elections ?"
    date: 2004-10-14
    body: "Elections take a lot of time and energy in the Debian project and in the end, it is not more democratic or open than KDE.\n\nBut I agree with Matthias that people outside KDE may have difficulties understanding who are the important people in KDE. We have already official representatives to give outside people an entry point in the KDE world. Maybe, we should have a way to give a nice title (like KDE lead engineer) to the most productive and active developers so people would feel that the talk is not made by a nobody but by an important chap.\n\nCheers,\nCharles "
    author: "Charles de Miramon"
  - subject: "Re: Do we need elections ?"
    date: 2004-10-14
    body: "Why would you need elections for such positions? Wouldn't it be much easier to assign official 'heads' for subprojects within KDE, like \"Konqueror Project Leader\", \"Quanta Project Leader\" and such. If such titles are listed at a somewhat official manner of the site, journalists and the like can contact those people for interviews. While there may be some cases where it's not directly certain, I think these titles could be given without going through the hassle of elections.\n\nArend jr."
    author: "Arend jr."
  - subject: "Re: Do we need elections ?"
    date: 2004-10-14
    body: "Excellent idea, really superb..... but: who confers these titles? Who takes them away? \n\nOK we could have a board of alpha men inside KDE who are trusted by eveyone with the power to give and take these titles. But who appoints these alpha men? And who impeaches them?"
    author: "Sceptic Bureaucrat"
  - subject: "Re: Do we need elections ?"
    date: 2004-10-14
    body: "Who currently appoints the 'release dude'?"
    author: "AC"
  - subject: "Re: Do we need elections ?"
    date: 2004-10-14
    body: "Er... Don't most applications and libs already have a maintainer?  A position that not only carries with it the certainty of fame, respect, and remuneration beyond ones wildest dreams, but also signifies the trust which has been invested into that superior class of indivduals by the powers that be. Their moral stature is such that only through gross dereliction of duty or voluntary abdication they can be evicted from their lofty pedestal. If one were to number the rank and file of KDE developers in the Greek alphabet, then it is surely they who will get the distinction of being furthest removed from those carrying the opprobrious stigma of being counted among those worthy of not more than an omega."
    author: "Boudewijn"
  - subject: "Re: Do we need elections ?"
    date: 2004-10-15
    body: ":)\nI think it would be a very good idea, as long as the maintainers themselves also agree. Otherwise, they should try and ask someone they consider fit to be the project leader."
    author: "Claire"
  - subject: "Re: Do we need elections ?"
    date: 2008-11-02
    body: "YA, BUT DO U THINK IT IS NESSECARY? TO ME MAYBE."
    author: "dvaid"
  - subject: "Exchange"
    date: 2004-10-14
    body: "Where can I find details on the Exchange support? Is it just Korganizer or does Kmail join in too?"
    author: "Nick"
  - subject: "Re: Exchange"
    date: 2004-10-15
    body: "KMail supports Exchange via IMAP since ages."
    author: "Ingo Kl\u00f6cker"
  - subject: "Konqi And The Magical Rope Of Curiosity"
    date: 2004-10-14
    body: "Very cute! I hope to see more of those soon.\n\n\n\n\n"
    author: "Fabio"
  - subject: "Re: Konqi And The Magical Rope Of Curiosity"
    date: 2004-10-14
    body: "Yeap it's excellent!"
    author: "John Konqui Freak"
  - subject: "Re: Konqi And The Magical Rope Of Curiosity"
    date: 2004-10-14
    body: "That is one of the coolest (if not the coolest) things I've ever seen in my life. The Konqui fire and smoke at the end - brilliant."
    author: "David"
  - subject: "Clutter in Konqueror"
    date: 2004-10-14
    body: "\"He is also concerned about the clutter in KDE, especially in Konqueror.\"\n\nI agree. Where did those applications and settings menus come from in there?"
    author: "David"
---
Last week saw the first <a href="http://www.linuxworldexpo.co.uk/">LinuxWorld Conference & Expo</a> franchise in London. Representing the KDE project were Jonathan Riddell (Umbrello), Jeff Snyder (Kompare), Richard Smith (Kopete), George Wright (KLatin), Martijn Dekkers, Ben Lamb and David Pashley (on the Debian stand).





<!--break-->
<p>We ran a joint stall with the GNOMEs which gave us one of the biggest spaces in the .org village and was used to jointly promote <a href="http://www.freedesktop.org/">freedesktop.org</a> and <a href="http://www.x.org/">X.org</a>. Only one GNOME turned up so we spread ourselves, and the excellent collection of machines that had been lent to us, along the stall.</p>

<p>The most popular demonstration of KDE 3.3 was <a href="http://www.kontact.org/">Kontact</a>. A lot of people were interested in how well it could talk to Exchange. X.org 6.8 impressed everyone with its composite extension featuring transparent windows and nice shadows. XComposite runs slowly but the FreeNX demonstration showing a remote X desktop was as fast as it could be, even connecting to a server in Germany over a busy wifi link. The FreeNX server is likely to become a freedesktop.org project soon; the knx client is in kdenonbeta but badly needs developers to work on it and integrating it with KDE's existing remote desktop application. We also demonstrated KOffice and were pleased to meet a number of satisfied users and many more interested in the database frontend Kexi.</p>

<p>We were proud to demonstrate <a href="http://www.scribus.org.uk/">Scribus DTP</a>. The Scribus developers say they have considered making Scribus a part of KDE but some of their users did not want the depencies so it remains pure Qt.</p>

<p>There were a number of people interested in KDE's lockdown framework <a href="http://www.kde.org/areas/sysadmin/">Kiosk</a>. We discussed a large KDE deployment taking place this week which makes heavy use of it, demonstrating the power and flexibility of KDE & Kiosk in the enterprise environment.</p>
 
<p>Several people asked how to start developing for KDE, we pointed them towards <a href="http://quality.kde.org/">the KDE Quality teams</a> and the <a href="http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=JJ%3A&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">Junior Jobs in Bugzilla</a>.</p>

<p>One of the highlights of the expo was the presence of KDE founder Matthias Ettrich who attended as a speaker to give a talk on "Qt: the engine for professional desktop applications on Linux and beyond". Matthias thinks that KDE should have positions elected to give developers official titles (this should mean he has fewer requests to do talks). He is also concerned about the clutter in KDE, especially in Konqueror.</p>

<p>The killer feature in attracting punters to our stand was the new animation from Basse <a href="http://www.kimppu.org/basse/kde/">Konqi And The Magical Rope Of Curiosity</a> which looks excellent on a 20" dual screen monitor.</p>

<p>This was an excellent show not least because of the number of happy KDE users. Our thanks go to drochaid for printing the flyers, Ben and Martijn for lending us computers and Brian for organising the .org village.</p>

<p>Photos from <a href="http://orion.x3n.me.uk/photo/linux/expo2004/">Michael</a>, <a href="http://www.thos.me.uk/Photos/Linux%20Expo%20-%20October%202004/">thos</a>, <a href="http://www.gwright.org.uk/images/?dir=pictures/KDE/LWCE-2004">George</a>, <a href="http://www.davidpashley.com/linuxworld2004/">David</a> and <a href="http://muse.19inch.net/~jr/tmp/linuxworld-photos/?pindex=1">Jonathan</a>.</p>







