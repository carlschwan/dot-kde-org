---
title: "OnebaseGo \"KDE 3.3 Beta2\" LiveCD Released"
date:    2004-08-05
authors:
  - "pbaskaran"
slug:    onebasego-kde-33-beta2-livecd-released
comments:
  - subject: "thanx"
    date: 2004-08-05
    body: "very much thanx to the onebase linxu team for this release, its nice to have an up-to-date livecd!"
    author: "superstoned"
  - subject: "well wicked"
    date: 2004-08-05
    body: "I'll check this toroughly as soon as those folks from the admin team in our dormitory enables my internet access again. I wish for a ride, and I think I'll get hell of one. KDE is the best desktop ever. Go KDE, go.\n(ehhrm, btw, what about 3D anyone? You wouldn't mind trying that on akademy, would you (*hint, hint*)?)\ncya that guiser"
    author: "that guiser"
  - subject: "Re: well wicked"
    date: 2004-08-05
    body: "what about 3d?"
    author: "Ian Monroe"
  - subject: "Password ? "
    date: 2004-08-05
    body: "Does anybody know the root password for this live distro ?? I couldn't find the password to enter kde :-(("
    author: "Andreas Scherf"
  - subject: "Re: Password ? "
    date: 2004-08-05
    body: "http://www.ibiblio.org/onebase/onebaselinux.org/Support/go-guide.html"
    author: "Anonymous"
  - subject: "Re: Password ? "
    date: 2004-08-05
    body: "Thanks but couldn't find any password or anything else on this page ... ??? "
    author: "Andreas Scherf"
  - subject: "Re: Password ? "
    date: 2004-08-05
    body: "login:root\npassword:one"
    author: "Lukumo"
  - subject: "Re: Password ? "
    date: 2004-08-24
    body: "Has anyone found the password for root yet.. Still looking\n\nThanks"
    author: "Rockem"
  - subject: "Excellent way to get more beta-testers"
    date: 2004-08-06
    body: "A lot of people can download, burn and test a live cd. A lot less scary than installing some rpm's, let alone compiling from source.\n\nIf only it wasn't released on the freeze date for kde-3.3-RC1 :(\n\nToo bad, this could be such a valuable tool."
    author: "Johan Veenstra"
  - subject: "amarok?"
    date: 2004-08-06
    body: "Is amarok included in this release?"
    author: "tux"
  - subject: "Re: amarok?"
    date: 2004-08-06
    body: "Why not find out?\n\nSimply questions require equally simple answers :P"
    author: "uniplex"
  - subject: "Re: amarok?"
    date: 2004-08-06
    body: "Sounds quite arrogant for an answer where a simple yes or no would do."
    author: "anonymous"
  - subject: "Responsible behaviour"
    date: 2004-08-06
    body: "I think the big warning in the background image stating the purpose of this CD (\"test version, software may be unstable, if you prefer a stable version then get it from the website\") is a very responsible measure. "
    author: "cm"
  - subject: "Enrico Ros for President!"
    date: 2004-08-09
    body: "Enrico, your optimizations make konqueror so much more fun to use, well\nI don't have the words to describe... :-)\n\n(For those who don't know what I'm speaking about:\nhttp://robotics.dei.unipd.it/~koral/KDE/kflicker.html )\n\nEnrico, you rock and I hope you will make your way through kde cvs and look\nfor more things to optimize!\n\nSuch \"small\" optimizations are so important!!\nThey make KDE look very professionnal.\n\nApart of that, support for css::border-collapse:collapse; is a nice and\nimportant addition aswell!\n\nBig thanks to all the KDE developers from a happy user!"
    author: "ac"
  - subject: "Re: Enrico Ros for President!"
    date: 2004-08-10
    body: "Thanks for pointing this out, it is indeed AWESOME stuff. Keep going, Mr. Ros!\n\nCan't wait for 3.3 to see all this stuff in action!"
    author: "Mikhail Capone"
---
The <a href="http://www.onebaselinux.org/">Onebase Linux Team</a> is pleased to announce a new flavor of OnebaseGo 2.1 LiveCD providing exclusive preview of the complete KDE 3.3 Beta2 "<a href="http://dot.kde.org/1090503271/">Kollege</a>" desktop suite. This edition has been primarily released to test and report bugs about this unstable KDE version (gdb is included). All the KDE packages were compiled and optimized with GCC 3.4.1 to provide top-notch performance and speed. Also included are <a href="http://www.koffice.org/releases/1.3.2-release.php">KOffice 1.3.2</a>, Flash 7.0 and many more software. Read the <a href="http://www.ibiblio.org/onebase/onebaselinux.org/Community/phpBB2/viewtopic.php?t=1201">full announcement</a>.




<!--break-->
