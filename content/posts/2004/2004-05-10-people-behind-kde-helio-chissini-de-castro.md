---
title: "The People Behind KDE: Helio Chissini de Castro"
date:    2004-05-10
authors:
  - "Tink"
slug:    people-behind-kde-helio-chissini-de-castro
comments:
  - subject: "The name is Curiti<b>B</b>a"
    date: 2004-05-09
    body: "Yeah, whatever..."
    author: "Renato Sousa"
  - subject: "congratulations!"
    date: 2004-05-10
    body: "Hoje podemos usar o nosso portugu\u00eas :)\nParab\u00e9ns Helio e obrigado pela contribui\u00e7ao ao kde.\n\nToday we can use our Portuguese :)\nCongratulations and thanks for the contribution to kde"
    author: "alexandre"
  - subject: "curitida?"
    date: 2004-05-10
    body: "batida?"
    author: "hehe"
  - subject: "PeaceMatters.org"
    date: 2004-05-11
    body: "KDE has taken like wildfire in Brazil. Brazil will be the epicenter of truly disruptive technological change, because if GNU/Linux/KDE work in Brazil, many other Latin American countries will follow suit."
    author: "PeaceMatters.org"
---
After a week of absence <a href="http://www.kde.nl/people/">The People Behind KDE</a> travels all the way to beautiful Curitiba in Brazil to have a chat with one of KDE's most active developers. This guy is active on many fronts within KDE, his latest addition is the adoption of KDE by the Brazilian government and he doesn't stop there! Read all about Connectiva's <a href="http://www.kde.nl/people/helio.html">Helio Chissini de Castro</a>!




<!--break-->
