---
title: "KDE CVS-Digest for September 10, 2004"
date:    2004-09-11
authors:
  - "dkite"
slug:    kde-cvs-digest-september-10-2004
comments:
  - subject: "Nice to see .... "
    date: 2004-09-11
    body: "Nice to see bugfixers mentioned and Michael being the top bugfixer this week. So many thanks to all those who are reporting bugs!!  \n\nCiao \n\nFab"
    author: "fab"
  - subject: "Re: Nice to see .... "
    date: 2004-09-11
    body: "Yes :-) But it is more important that I get some work done on the bug hunting guide. Real Life was just more important the last couple of days :-)"
    author: "Michael Jahn"
  - subject: "Re: Nice to see .... "
    date: 2004-09-11
    body: ">>Real Life was just more important the last couple of days\n\nI wonder what realm it is you consider yourself to be dwelling in when \"working on the bug hunting guide\"."
    author: "ac"
  - subject: "Re: Nice to see .... "
    date: 2004-09-11
    body: ">>I wonder what realm it is you consider yourself to be dwelling in when \"working on the bug hunting guide\".\n\nDon't misunderstand me. I don't consider myself to be an expert, not at all. I don't have enough coding knowledge and and I don't have enough experience. But these guys do: http://developer.gnome.org/projects/bugsquad and they have some excellent documentation. I wanted to work on integrating some of that into our guides. \n\nSee Mr. Anonymous Coward, I don't know what you have done for KDE but I just want to help the devs by cleaning up bko a little. And it is not nice being accused of arrogance when doing that! (That's what you are doing after all, isn't it? Or do I completely misunderstand your post? )"
    author: "Michael Jahn"
  - subject: "Re: Nice to see .... "
    date: 2004-09-12
    body: "Rock on, Michael.\n\nYour efforts are greatly appreciated. I mean it."
    author: "Carlos Leonhard Woelz"
  - subject: "Re: Nice to see .... "
    date: 2004-09-12
    body: "Indeed.. don't let trolls bother you :)"
    author: "anon"
  - subject: "Re: Nice to see .... "
    date: 2004-09-12
    body: "erm.. \"Mr. Anonymous Coward\" here, I think you read way to much into this Michael Jahn. Where did I make any accusation of arrogance? Where did I tell about you being an expert?? This is weird.\n\nObviously you do good work, no need to be so defensive. It's just funny how some people call KDE work \"something else\" and other stuff \"real life\". That's all, relax man.."
    author: "ac"
  - subject: "Re: Nice to see .... "
    date: 2004-09-12
    body: ">It's just funny how some people call KDE work \"something else\" and other stuff \"real life\". That's all, relax man..\n\nOh, then I completely misunderstood your post. I am sorry. (The way I understood it sounded very offensive to me.)"
    author: "Michael Jahn"
  - subject: "Re: Nice to see .... "
    date: 2004-09-12
    body: "heh. no problem, I think I understand your initial view now, something like a developer asking you how it feels to be some \"low\" documenting guy thinking he is all great among the elite coders, or something like that? Yes, that sounds really really arrogant!"
    author: "ac"
  - subject: "Backporting"
    date: 2004-09-11
    body: "I reacted to a comment that was made regarding a fix to kpdf: \n\n> Reduce margins and spacing. Not backporting to the 3.3.x branch as is not critical.\n\nWhat I would like to know is if there is a set policy for when to backport and when not to.  I thought that bugs should always be backported and wishes and new features should not (as a rule). But this seems not to be the case.  So what is it?"
    author: "Inge Wallin"
  - subject: "Re: Backporting"
    date: 2004-09-11
    body: "Obviously having a margin of 10 instead of a margin of 3 is not a bug, or is it?"
    author: "Albert Astals Cid"
  - subject: "Re: Backporting"
    date: 2004-09-12
    body: "Perhaps not, but that was not the point.\n\nI wondered if there is a set policy for when things should be backported.  And if there is, where can I read it?"
    author: "Inge Wallin"
  - subject: "Re: Backporting"
    date: 2004-09-12
    body: "The thing as you said is NOT backporting when that includes new features and/or new strigns to translate.\n\nBackporting is encouraged (spelling?) when it fixes a bug, but as always nothing can be obligatory most of us are working in a non-paid way so obligatory things are impossible\n\nAnd yes i know that does not answer that question, the answer to you question is \"i don't know about any writen guidelines\""
    author: "Albert Astals Cid"
  - subject: "Re: Backporting"
    date: 2004-09-12
    body: "Really?  Not backporting new features, I understand, but new strings??"
    author: "Inge Wallin"
  - subject: "Re: Backporting"
    date: 2004-09-12
    body: "We don't want to break things for the translators. Some translation teams aren't that big you know (or doesn't have that much time on their hands)."
    author: "Peter Simonsson"
  - subject: "Re: Backporting"
    date: 2004-09-11
    body: "I also think it should be backported, especially since the changes affect only *one* (yes 1) line of code...\n"
    author: "ac"
  - subject: "Re: Backporting"
    date: 2004-09-11
    body: "If you want to commit the backport you have my ok"
    author: "Albert Astals Cid"
  - subject: "Re: Backporting"
    date: 2004-09-13
    body: "Bugs should never be backported, but bug fixes should. SCNR"
    author: "Ingo Kl\u00f6cker"
  - subject: "Dasher is not a GNOME app"
    date: 2004-09-11
    body: "Please note that Dasher is not a GNOME app per se; it originally ran on handheld computers (running Windows CE IIRC) and should be pretty much tool kit independent, it's just that the person who ported it to Linux decided to use GTK.\n\nI've heard stories of people working on a KDE version...."
    author: "Chris Howells"
  - subject: "Re: Dasher is not a GNOME app"
    date: 2004-09-11
    body: "It dates back further than that.  I remember running a prototype a few years ago (probably on my KDE desktop ;-D) and remember thinking it was a bit odd, but was amazed to learn that, yes, it's possible to get a fairly high wpm count.\n\nGoing back to handhelds: anyone know of a PalmOS port?  I hate Grafitti (one of the things that keeps me from buying a Palm. ;-D)"
    author: "regeya"
  - subject: "typewriter invented for blind people?"
    date: 2004-09-11
    body: "> He said that the typewriter was invented for a blind person so he could write love letters.\n\nOnly if you consider the US-American Christopher Latham Sholes the inventor of the typewriter.\nBut actually, the real inventor was the Austrian Peter Mitterhofer, who invented the typerwriter a few years before him in 1864 -- and not for blind people. :-P\n\nhttp://www.google.com/search?q=typewriter+peter+mitterhofer"
    author: "Melchior FRANZ"
  - subject: "Re: typewriter invented for blind people?"
    date: 2004-09-13
    body: "Did you really have to spoil this nice myth?\n\nAnyway, I found the following (http://encarta.msn.com/text_761571837___2/Typewriter.html):\n\"The first recorded attempt to produce a writing machine was made by the British inventor Henry Mill, who obtained a British patent on such a machine in 1714.\"\n\"Several other inventors attempted to produce machines designed to make embossed impressions that could be read by the blind. One such machine, developed by the American inventor Alfred Ely Beach in 1856 [...]\"\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: typewriter invented for blind people?"
    date: 2005-06-16
    body: "No,Alexander Gramham Bell invented the typewriting."
    author: "some1"
  - subject: "Re: typewriter invented for blind people?"
    date: 2007-02-09
    body: "hey this typewriter is for deaf people idiotfucker..."
    author: "anty"
  - subject: "general setting for the delete behaviour?"
    date: 2004-09-11
    body: "\"Added convenience method KIO::trash( url or url list ), to encapsulate the\ntrash implementation (it now moves to \"trash:/\"). Should also make it easy\nfor apps to use it instead of del().\"\n\nWouldn't it be nice to have just one action (for the user): Delete, and have a checkbox in the config \"Move files to Trash on delete\". If that one is selected, deleted files are moved to the trash, otherwise just deleted."
    author: "wilbert"
  - subject: "Re: general setting for the delete behaviour?"
    date: 2004-09-11
    body: "KDE 3.3 has an option to only show the \"Trash\" action."
    author: "Anonymous"
  - subject: "Re: general setting for the delete behaviour?"
    date: 2004-09-11
    body: "Ah, great, I wasn't aware of the fact that it also truly disabled the 'delete' action. It works fine!"
    author: "wilbert"
  - subject: "KDElibs/win32?"
    date: 2004-09-11
    body: "How far are we from seeing, for example, konqueror running natively on windows? I know some webdevelopers who want to test their sites with Safari and Konqueror, but can't because a khtml browser doesn't run on Windows (without the hassle of something like cygwin)"
    author: "anon"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-11
    body: "Network stuff is simply not ported (volunteers are welcome). Much of the rest exists and works \"highly acceptable\" since about mid 2003.\n\nSee http://iidea.pl/~js/qkw/ for more info and http://download.kde.org/download.php?url=unstable/apps/KDE3.x/office/kexi-2005beta4-win32.exe for example."
    author: "Jaroslaw Staniek"
  - subject: "Re: KDElibs/win32?"
    date: 2004-10-23
    body: "This is the only useful inoramtion in whole topic\n\nSee http://iidea.pl/~js/qkw/ for more info and http://download.kde.org/download.php?url=unstable/apps/KDE3.x/office/kexi-2005beta4-win32.exe "
    author: "Ali"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-11
    body: "> I know some webdevelopers who want to test their sites with Safari and Konqueror, but can't because a khtml browser doesn't run on Windows\n\nHave you told them about KNOPPIX? That's the easiest way to bring KDE to Windows users. And they can try Quanta while they're at it.\n"
    author: "kf"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-12
    body: "I've asked _the same_ question when I was asked for e.g. Quanta/win32, KBabel/win32 or other tools. People told me it's often impossible to install/boot/reboot computer at their workplaces because of internal policy and locked hardware. Sometimes they are forced to have opened specialized win32-only application, although have some time to contribute (with their employer's permission) for KDE project e.g. by doing translations. I found this quite reasonable.\n\nSo, instead of forcing everyone to do bloody revolution on their desktops right now, we can better making KDE apps MUST-HAVE apps for many users. Thus, massive decision about migration to Linux OSes can be more natural.\n\n--\nJaroslaw Staniek\nKexi Team\nKDElibs/win32\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-13
    body: "Why not use VMWare + e.g. Knoppix?  I worked just great when I was testing web pages with different OS/browser combinations some years ago.\n\nIt is a commercial product, but we are talking commercial web development now, right?"
    author: "Martin"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-14
    body: "Not only commercial. Although it was an example, if you mean commercial applications, hardly anybody will want to buy $200 \"tool\" just to run $50-$100 application (average price of current offerings built on top of FOOS). And using KDE with vmware is similar in experience as using KDE with Cygwin/X11. It difficult for software vendors to distribute his products this way. \n\nLet's give an example: try to distribute application you created originally on win32, and now shipping for Linux -- in form of .exe file, attaching Wine and emulator of explorer.exe shell as required to use, just because your application depended on it when you designed it. Not quite convenient for users, yeah?"
    author: "Jaroslaw Staniek"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-14
    body: "> I know some webdevelopers who want to test their sites with Safari and \n> Konqueror\n\nThis is not the correct mind set -- not the correct way to think about it.\n\nThey can simply test on Mozzilla IF they also validate their code with the W3C's validator.  Validation of the code is the best way to test code -- rather than testing it on various browsers.\n\n--\nJRT \n\n"
    author: "James Richard Tyrer"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-14
    body: "Some people actually care about having their readers be able to read the webpages, though.\n\nIf you test *only* with standards, and then 90% of the viewers can't see the page, you are only damaging yourself."
    author: "Roberto Alsina"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-15
    body: "I should have figured that someone could miss my point.\n\nI didn't say test \"only\" with standards.  I said to test with standards FIRST.\n\nMy point was that a common mind set of testing HTML code on several browsers as the ONLY method of testing it is simply the wrong way to do it.\n\nWeb developers DO damage themselves by not using the W3C validator as the first test of their code.\n\nIf there is webpage code that passes the W3C validator AND displays correctly on the current release of Mozilla but doesn't work correctly on Konqueror-3.3.0 KHTML, please post it on BugZilla, the developers would probably be very interested to see it.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-15
    body: "Happy to fulfill your expectations. Next time, I will strive to succeed on understanding you, Oh master!"
    author: "Roberto Alsina"
  - subject: "Re: KDElibs/win32?"
    date: 2004-09-15
    body: "Grmbl, pressed post too fast:\n\nI mean, my point is, you said test with the validator and mozilla.\n\nWell, here's a better idea: test with the validator and IE and Mozilla and Konqueror.\n\nI mean, if it works only on Mozilla and the validator, the page is broken in a very practical sense."
    author: "Roberto Alsina"
---
In <a href="http://cvs-digest.org/index.php?issue=sep102004">this week's KDE CVS-Digest</a>:
Speedups in <a href=" http://www.konqueror.org/features/browser.php">khtml </a> JavaScript, <a href="http://kate.kde.org/">Kate</a>, <a href="http://kmail.kde.org/">KMail</a> and Kcminit. 
Macros and headers added to compile Kdelibs on win32. 
UI Recovery ToolKit (uirtk) improved. 
Support for building <a href=" http://developer.kde.org/language-bindings/smoke/index.html">Smoke</a> library on Mac OS X.



<!--break-->
