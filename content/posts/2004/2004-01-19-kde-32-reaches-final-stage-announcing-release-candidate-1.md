---
title: "KDE 3.2 Reaches Final Stage: Announcing Release Candidate 1"
date:    2004-01-19
authors:
  - "coolo"
slug:    kde-32-reaches-final-stage-announcing-release-candidate-1
comments:
  - subject: "first post!"
    date: 2004-01-19
    body: "great stuff! hope it is the last release candidate, it definately works like a release version!"
    author: "daaKu"
  - subject: "Re: first post!"
    date: 2004-01-19
    body: "... and the first binary packages are also available:\n\nftp://ftp.kde.org/pub/kde/unstable/3.1.95/contrib/Slackware/9.1\n\n"
    author: "Ojay"
  - subject: "Re: first post!"
    date: 2004-01-19
    body: "hehe you're fast :)\nI didn't finished to upload all the packages on the ftp :)\n\nThe users for Slack-9.0 (and VL4) will have to wait for 1 more day... sorry. I need a faster computer :o))"
    author: "JC"
  - subject: "Re: first post!"
    date: 2004-01-19
    body: "Well, fast enough! It will take my AXP 1700+ the whole night to yield my own Slackware-current packages.\nPfiou, gotta get an Opteron as soon as possible...\nAs anybody compiled KDE 3.2 in 64bits on an Opteron yet? Does it work? How does it feel?\n\nRichard"
    author: "Richard Van Den Boom"
  - subject: "Re: first post!"
    date: 2004-01-19
    body: "Try Suse64 :)\n\nI don't know if kde is faster on opteron but probably faster to build packages :)"
    author: "JC"
  - subject: "Bugs"
    date: 2004-01-19
    body: "What about the large number of outstanding bugs? (5000+ reported in latest cvs digest)\nWill it be released with most of them not fixed?\n\nNot trolling tho - I realise it's near impossible to fix all the bugs, but are most of the major ones fixed?\n\nJohnFlux"
    author: "JohnFlux"
  - subject: "Re: Bugs"
    date: 2004-01-19
    body: "if you look <a href=\"http://bugs.kde.org/weekly-bug-summary.cgi?tops=15&days=365\">at this</a> - you'll figure that 5000 is about nothing.<p>\n\nAnd yes, this release will also contain bugs we'll fix at a later point - just\nas any other software there is. Supporting software is a steady process, especially with such a huge collection as KDE is.\n\n\n\n\n"
    author: "coolo"
  - subject: "Re: Bugs"
    date: 2004-01-19
    body: "1.  5000 bugs doesn't mean 5000 problems, there are lots of duplicates and bugs that have already been fixed.\n\n2.  Just imagine how many bugs KDE was released with before there was bugs.kde.org to tell us about them!  Seems like it wasn't that big a problem before..."
    author: "Spy Hunter"
  - subject: "Re: Bugs"
    date: 2004-01-19
    body: "Plus, some of those bugs are \"wishlist\" bugs, on the order of \"KDE doesn't behave like IceWM.\""
    author: "David Johnson"
  - subject: "Re: Bugs"
    date: 2004-01-20
    body: "I wonder how many bugs can appear when Necrosoft opens their source code :)"
    author: "Assa"
  - subject: "Re: Bugs"
    date: 2004-01-20
    body: "Open their source code? There's no need to be opensource to have bugs shown! Users don't care about bugs in the source code, but bugs they find while running ;) \nBtw, they do have a site to report bugs, although I doubt they have a bug-counter showing-off their (lack of) quality :)\n"
    author: "uga"
  - subject: "Re: Bugs"
    date: 2004-01-19
    body: "There aren't 5000 outstanding bugs.  bugs.kde.org has 5000+ \"bugs\" in it's database, but only some of these are outstanding bugs.  A lot of them are bugs that have been fixed.  A lot of them are \"wishlist\" \"bugs\", feature requests, in other words.  And a lot of them are marked as duplicates of outstanding, fixed, or wishlist bugs.  bugs.kde.org is just an easy way to keep track of all that, not just a list of outstandint bugs."
    author: "Josiah"
  - subject: "Great!!"
    date: 2004-01-19
    body: "Great !!!!\n\nI was waiting for it !!! :)\nI'm going to making .debs\nLet's hope it will work\n\nThanks to all the kde team for their great work\n"
    author: "RRiChIe"
  - subject: "Re: Great!!"
    date: 2004-01-19
    body: "Will you be posting them on the web when they are built?"
    author: "Tom"
  - subject: "Re: Great!!"
    date: 2004-01-19
    body: "I hope someone builds some debs for Lindows 4.5 :) I'll probably have to wait until 5.o this spring."
    author: "Kraig"
  - subject: "Re: Great!!"
    date: 2004-01-19
    body: "It would be hard to make it for Lindows 4.5, it is too old right now, and so it probably would mess some things up for you, like CNR. Not sure though, haven't tried."
    author: "Alex"
  - subject: "Re: Great!!"
    date: 2004-01-19
    body: "debs ? hmm\ncan you put them on-line somewhere on-line ;-) ?\n"
    author: "alexander"
  - subject: "Beta versions proved very stable for me..."
    date: 2004-01-19
    body: "I just wantet to tell anyone, who's worried about stability, to definitely give it a try. I've been using 3.2 since Mandrake Cooker released binary packages (and I've updated the ocassionaly) and it all works like a charm. The only issues are:\n\n- kmail if I'w writing an email and change the dictionary (for spell check) then kmail crashes (but my email never got lost, eat your shorts MS Word auto save).\n- Konqueror crashes on trying to open discussions on gnomedesktop.org (I'm sure that's just a coincidence :-))\n\nBoth of the bugs might have been fixed by now, I haven't tried RC1 yet.\n"
    author: "David Siska"
  - subject: "Re: Beta versions proved very stable for me..."
    date: 2004-01-19
    body: "Please report bugs to http://bugs.kde.org . The people there are very responsive, and I am sure that if you report them (both are crashes <=> high priority, except for that gnome _treason_, of course :)) they will be looked at (maybe not corrected, but at least looked at) asap."
    author: "Renato Sousa"
  - subject: "Re: Beta versions proved very stable for me..."
    date: 2004-01-22
    body: "Been trolling on the Gnome lists, David?  Shame on you!    8->"
    author: "Tukla Ratte"
  - subject: "Excellent News"
    date: 2004-01-19
    body: "I'm compiling right now under Gentoo to help the RC1 bug squashing effort, so this can be the best release ever. This is the first time I'm going to use 3.2, but from the previews I've seen it looks stunning! I hope that, as per usual, the devs don't feel pressured to get this out before its ready for prime time, but I certainly hope this will be the last Release Candidate.\n\nThanks again to everyone on the KDE team."
    author: "Elmy"
  - subject: "Re: Excellent News"
    date: 2004-01-19
    body: "Same on Gentoo!  I see ya in the thread!  :)"
    author: "am"
  - subject: "Re: Excellent News"
    date: 2004-01-19
    body: "Let us know when it finishes compiling sometime next month.  :)\n\n(I'm a gentoo user, too, btw... :)  )"
    author: "cheeser"
  - subject: "Re: Excellent News"
    date: 2004-01-20
    body: "Next month, you wanted to say , within the next 4 hours, just need some CPU power :-)"
    author: "Arti"
  - subject: "Re: Excellent News"
    date: 2004-01-20
    body: "KDE 3.1.5 built in less than 6 hours (including downloads over a 256kbit/s ADSL line) on a 60Eur Athlon XP 2200+... Simply start the job before going to bed and wake up with a shiny new desktop...\nSelf-compiled KDE on my machine is twice as fast than the one shipping with SuSE 9, and their konqueror preloader doesn't change things so much..."
    author: "gigi"
  - subject: "Re: Excellent News"
    date: 2004-01-21
    body: "I got KDE installed faster than that because I have a few gentoo machines sitting around on the network, so I merged distcc and it distributed the compile... if you do that, the compile can be done in less than an hour if your machines are fast :)\n\n# emerge distcc\n# rc-update add distccd default\n# /etc/init.d/distccd start\n# distcc-config --install\n# distcc-config --set-hosts \"localhost 10.10.1.1 ...\"\n# nano -w /etc/make.conf\n -> set FEATURES=\"distcc\"\n -> set MAKEOPTS=\"-j8\" -- number of machines on network x 2\n\nit takes about 30 seconds to install on each of the machines and is well worth it :)\nhave fun bugtesting, and if my commands are wrong, sorry about that. I did them off the top of my head."
    author: "kenny"
  - subject: "What bugs?"
    date: 2004-01-21
    body: "Hehe, the title is just kidding, of course.  I just wanted to say that as a fellow Gentoo user who's been testing KDE 3.2 since beta 2, it's been smooth sailing for the whole time.  RC1, hopefully, won't give me any trouble.  And if it does, well, I'll be sure to send a bug report.  :)"
    author: "Miles Robinson"
  - subject: "slackware pkgs"
    date: 2004-01-19
    body: "Someone wanna make Slackware 9.1 packages?\n"
    author: "Jackson Barnes"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "Check here in a few days :\n\nhttp://www.linuxpackages.net/\n\nI'm sure there will be packages very soon. I'm making my own packages personnally.\nYou run for each KDE package (arts first, install, then kde-libs, install, then all the others) in the extracted directory:\n\n./configure\nmake\nmake install DESTDIR=/usr/src/tmp\n\nafter creating a tmp directory in /usr/src.\nThen you go into this /usr/src/tmp directory and you run :\n\nmakepkg -l y -c n ../<kde-package-name>-i486-1.tgz\n\nyou erase everything in the /usr/src/tmp directory with a \"rm -Rf *\" and you find your new package in /usr/src.\nRepeat for all packages.\nThese are not entirely proper packages : there's no description, no README, etc. But good enough for personnal use.\n\nRichard"
    author: "Richard Van Den Boom"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "Richard,\n\nThanks for the mini howto. I've always wondered how I do that.\n\nJackson"
    author: "Jackson Barnes"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "you need a little more than that if you want standard slackware packages, but for personnal use it's ok.\nBtw I never trust the packages from linuxpackages because I tried several ones and it never worked. The newbies should not be authorized to upload packages ... :)"
    author: "JC"
  - subject: "Re: slackware pkgs"
    date: 2004-01-20
    body: "Another source of Slackware packages may be found at\nftp.ibiblio.org/pub/packages/desktops/kde/unstable/3.1.95/contrib/Slackware\n\n\nthey have worked well for us here at Ioni.\n\nCheers,\n\n\nNick\n\n\n\n\n\n-- --\nNicholas Donovan\nPresident/CTO - Ioni Corporation\nOptimized Net Services Platforms\n\"Ioni...  Enterprise Optimized\""
    author: "Nicholas Donovan"
  - subject: "Re: slackware pkgs"
    date: 2004-01-20
    body: "There's an even easier way to make slackware packages, using the Checkinstall program.\n\nFrom source, you compile like you normally would, but instead of typing 'make install' you type 'checkinstall'.\n\nThe script will automatically generate a slackware package after asking you a few questions.  The name of the package will follow the slackware standard, and it will even collect as much of the documentation as it can to put under /usr/doc\n\nAfter the script is done, you have a package that you can install, or move to a different computer, or backup so that you don't have to compile that source package ever again for slackware.\n\nI think it's in the contrib/ folder of Slackware 9.1, but i recommend using the latest version (1.6beta3) which is available at http://checkinstall.izto.org/ "
    author: "Jeremy McNaughton"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "Go to\n\nftp://ftp.de.kde.org/pub/kde/unstable/3.1.95/contrib/Slackware/9.1\n\nThey seem to have slackware packages before the main KDE ftp server shows up with them. Interesting.\n\nGood luck.\nOliver"
    author: "Ojay"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "The sync stuff between mirrors is always interesting to follow :)\n\nBut keep in mind that the sources tarballs have been released at the same time to public and packagers. That why there are no (or a few) binaries packages.\n"
    author: "JC"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "> sources tarballs have been released at the same time to public and packagers\n\nThis is wrong. There was a day difference."
    author: "Anonymous"
  - subject: "Re: slackware pkgs"
    date: 2004-01-19
    body: "Yeah .... only 1 night ..."
    author: "JC"
  - subject: "Copying data to kate"
    date: 2004-01-19
    body: "Is anybody else also having problems with copying text e.g. from XEmacs/khtml to kate/kdevelop? Or is it just my (strange) setup?"
    author: "why not you?"
  - subject: "Re: Copying data to kate"
    date: 2004-01-19
    body: "From XEmacs to Kate: mmb works, esc-w in Xemacs, ctrl-v in Kate works,\nctrl-ins in Xemacs, ctrl-v in Kate works, I'd say it works for me..."
    author: "Boudewijn Rempt"
  - subject: "Re: Copying data to kate"
    date: 2004-01-20
    body: "shit, what can be wrong then... I cannot even copy from kdevelop to kate or vice versa. mmb works well"
    author: "hmm"
  - subject: "Re: Copying data to kate"
    date: 2004-01-20
    body: "I am seeing this problem as well. Kate doesn't seem to be able to paste when the text is cut from a different application. I've duped with khtml and kwrite. The middle mouse button does seem to work though."
    author: "Dan Christensen"
  - subject: "Re: Copying data to kate"
    date: 2004-01-21
    body: "Hi,\n  Just a wild guess..  perhaps what you are doing is copying, then killing the app, then pasting in to the new app?\nA copy and paste requires both ends open.\n"
    author: "John Flux"
  - subject: "Re: Copying data to kate"
    date: 2004-01-21
    body: "I'm not using 3.2 (3.1.2 on RH7.3 is all we have at work), but copying and pasting is always a problem with me. Sometimes it just won't work. It's my number one complaint about KDE (I think it's a KDE problem because it only happens with KDE apps, in particular Kdevelop). I'm disappointed to hear that it's still happening (though to be fair, it seems that only a few of us suffer from it)"
    author: "Ed Moyse"
  - subject: "Re: Copying data to kate"
    date: 2004-01-21
    body: "Nope, both apps are still open when the copy and paste fails. This isn't happening in 3.1.94, so it appears to be a regression. "
    author: "Dan Christensen"
  - subject: "Re: Copying data to kate"
    date: 2004-01-22
    body: "I get the exact same problem.\n\nMiddle mouse works, right click and paste is shadowed out.\n\nAlso drag and drop from kedit works!\n\ni deleted all /opt/kde3 and use the binary packages from suse for 8.2\n\nCheers\nTony"
    author: "Tony Espley"
  - subject: "Nice =)"
    date: 2004-01-19
    body: "This time KDE is strictly following its release schedule. Good news, I jsut hope that they aren't following it for the sake of it and actually following it because everything is looking like they want to. Knowing KDE and how it has delayed its products when serious problems arose I'm confident that they are following the schedule because the product is ready.\n\nIf all goes well, I should be able to have it by February 5. (I hope its announced on th because 3+2 equals 5 and so it's a very lucky date to release it on =) \n\nAlso one thing that has improved but still left me dissapointed was the lack of marketing that KDE had for their releases. The new features guide only scratched the surface of how KDE 3.1 has improved, and didn't provide enough real world examples. Remeber even annoying bugfixes should eb listed, and not just by pointing to te bug number, but by explainining what has changed in detail. Vague statements like usability has improved are worthless. There should eb clear mentions of actions menu, wallpaper properties redesign, clock configuration redesigned and with details on why and how its better. I haven't actually tried 3.2 other than the broken one wth Mandrake's cooker, but if they release beta1 with 3.1.95 I will write about some of the new features too.\n\nMARKETING IS AS IMPORTANT AS CODE! Even minor but noticeble improvements should be mentioned to add to the size of the document and impress users. ALso usability improvements should all be prominently mentioend to change the perception that some have of KDE."
    author: "Alex"
  - subject: "Re: Nice =)"
    date: 2004-01-19
    body: "I fully agree with this. the marketing of KDE really laggs behind. why dont you guys tell the world how remarkebly wonderfull this release is???"
    author: "superstoned"
  - subject: "Re: Nice =)"
    date: 2004-01-19
    body: "3.1.95 is RC1, not Beta1.\n\n"
    author: "Juergen"
  - subject: "Re: Nice =)"
    date: 2004-01-19
    body: "\"[...]3+2 equals 5 and so it's a very lucky date to release it on =)\"\n\n\nFebruary 5th is also a very nice date because it's my birthday :D"
    author: "Drantin"
  - subject: "There we have it ladies and gentlemen!"
    date: 2004-01-20
    body: "Not only would it be be good luck to release on the 5th of February, a nice coincidence 3+2=5, but it is also Drantin's birthday =)"
    author: "Alex"
  - subject: "Re: There we have it ladies and gentlemen!"
    date: 2004-01-20
    body: "Who the hell is Drantin?"
    author: "planetzappa"
  - subject: "Re: There we have it ladies and gentlemen!"
    date: 2004-01-20
    body: "Ah, i see, the poster above... Sounded like some celeb or whatnot. Never mind!"
    author: "planetzappa"
  - subject: "Re: Nice =)"
    date: 2004-01-21
    body: "it (KDE) is in your hand, your eye, your computer; \nmarketing is not in CS's mind; they only see \"bugs\" flying around their heads and codes jamming computers.\nif you want details, look inside the source code; if you want detail feeling, catch yourself a mouse."
    author: "details"
  - subject: "Re: Nice =)"
    date: 2004-01-21
    body: "Well, 3^2+2^2=5^2\n\nSo there."
    author: "Dmitry"
  - subject: "Re: Nice =)"
    date: 2004-01-21
    body: "9 + 4 = 25? :/"
    author: "Cwiiis"
  - subject: "Re: Nice =)"
    date: 2004-01-27
    body: "I think maybe you were thinking of the 3/4/5 rule...\nIt's actually 3^2 + 4^2 = 5^2"
    author: "Erik"
  - subject: "THIS IS NOT ON TIME"
    date: 2004-01-20
    body: "I remember reading the release schedule back in mid-2003 and they were shooting for a mid-December release. They changed it, and that's why it's on time."
    author: "Turd Ferguson"
  - subject: "Yup"
    date: 2004-01-20
    body: "Now it's aimed for February release, maybe February 5th ;)"
    author: "Mario"
  - subject: "Great but :-)"
    date: 2004-01-19
    body: "Great release! ;-)\nJust a little thing: i can't set anymore the kind of shadow from ~/.config/kdesktoprc \n...it doesn't work... and the default is \"ugly\" :p .\nAnyway this is great!!! :-)"
    author: "Giovanni"
  - subject: "Yeah, default shadow SUCKS"
    date: 2004-01-19
    body: "IT better not look this crappy in 3.2 final. We should have a nice MAcOSX, Nautilus, Windows XP style shadow."
    author: "Mario"
  - subject: "Re: Yeah, default shadow SUCKS"
    date: 2004-01-19
    body: "Yes, anyone know the story behind the shadow?  Why is it so bad?  How to fix?"
    author: "ac"
  - subject: "Re: Yeah, default shadow SUCKS"
    date: 2004-01-20
    body: "This shadow thing, what's it all about?  Is it good, or is it whack?"
    author: "Bob"
  - subject: "Re: Yeah, default shadow SUCKS"
    date: 2004-01-20
    body: "whack. It doesn't even look like a shadow."
    author: "Alex"
  - subject: "Re: Yeah, default shadow SUCKS"
    date: 2004-01-22
    body: "The kdesktop icon text shadow was incorporated from a patch originally posted on kde-look.org.  Here's the link: http://www.kde-look.org/content/show.php?content=4750\n\nThere are parameters that you can tweak to adjust the characteristics of the shadow, and up to KDE 3.2.0_beta2 they were located in ~/.kde3.2/share/config/kdesktoprc.\n\nSince I installed KDE 3.2.0-rc1, changing the parameters don't seem to work anymore.  Can anyone post the contents of their kdesktoprc file?"
    author: "Wilbur"
  - subject: "excellent release"
    date: 2004-01-19
    body: "I've been running HEAD since Saturday evening and so far it's fantastic.  All the glitches I had from the previous version are gone.  I see a lot of polish and I definitely see speed improvements.  I doubt another RC is needed.\n\nThat default gray/gears background isn't very good IMHO.  Not only is it drab, it's harsh.  Also the default desktop font shadow isn't anywhere good as the Nautilus desktop shadow.  I'm not sure if this can be tweaked or not.  I also think we should probably default to the Bitstream Vera fonts everywhere.  Oh yeah, and Konsole should default to Linux Colors.  :-)\n\nThe Dot frontpage looks like it lost some whitespace in Konqueror.  There's very little space at all between the \"comment number\" line and the title of the next item.  There should be a <p>.  Otherwise, Konqueror seems fantastic and all my other bugs are gone.\n\nMore serious gripe:  Cut'n'paste glitches sometimes when selecting in Konsole and pasting in Konqueror it doesn't quite work... behavior seems random -- rare but it happens. Also KDE stole my Control-A (beginning-of-line) for no good reason at all since it doesn't work.  Ctl-A selects the whole text and yet does not put it in the paste selection even after pressing Ctl-C (does not paste with Middle Button nor Ctl-V).  That's it for bugs, but I think this one is the most serious and annoying.\n\nOtherwise, I'm a real happy camper and I want to see this release adopted widely ASAP.  :-)\n\n"
    author: "Navindra Umanee"
  - subject: "Re: excellent release"
    date: 2004-01-19
    body: "Have you tried to start after prelinking kde?\nI also made a clean install in a new /usr/kde/3.2 dir and after prelinking I could not start kde. There were kdeinit startup problems and lost dcop communication. Startup crashed at 28%.\n\nMy test was done with HEAD and qt-copy not with 3.2_BRANCH.\n\nBye\n\n  Thorsten\n\n "
    author: "Thorsten Schnebeck"
  - subject: "Re: excellent release"
    date: 2004-01-19
    body: "I didn't do anything special like that.  What do I need to do to prelink my KDE? (I'm on Mandrake 9.2)"
    author: "Navindra Umanee"
  - subject: "Re: excellent release"
    date: 2004-01-19
    body: "Here is Gentoos Prelinking Howto, should be valid for Mandrake, too. \nIf you use NVidias binary driver you have to compile Qt with the (new)  \n-dlopen-opengl configure option (I *think* this option is qt-copy only, so far)\n\nhttp://www.gentoo.org/doc/en/prelink-howto.xml\n\n+++\n3. Prelinking - Prelink Usage \n\nI use the following command to prelink all the binaries in the directories given by /etc/prelink.conf. \n\n# prelink -afmR\n\nWarning: It has been observed that if you are low on disk space and you prelink your entire system then there is a possibility that your binaries may be truncated. The result being a b0rked system. Use the file or readelf command to check the state of a binary file. Alternatively, check the amount of free space on your harddrive ahead of time with df -h. \n\nThe options explained:\n-a \"All\": prelinks all the binaries\n\n-f Force prelink to re-prelink already prelinked binaries. This is needed as prelink aborts if old prelinked files are present and their library dependencies have changed. \n\n-m Conserve the virtual memory space. This is needed if you have a lot of libraries that need to be prelinked. \n\n-R Random -- randomize the address ordering, this enhances security against buffer overflows. \n\nNote: For more options and details see man prelink. \n+++\n\nBye\n\n Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: excellent release"
    date: 2004-01-20
    body: "Doesn't mandrake already prelink their binaries? I thought they did (like Redhat) which caused the great load times... "
    author: "rsk"
  - subject: "Re: excellent release"
    date: 2004-01-20
    body: "\"I also think we should probably default to the Bitstream Vera fonts everywhere.\"\n\nYup, they got added in XFree 4.4 so that would be a nice default."
    author: "bugix"
  - subject: "I agree"
    date: 2004-01-19
    body: "I agree with everything you said except the wallpaper and konsole statement. Can't wait for KDE 3.2 on February 5 (3+2=5) Please release it on the 5th."
    author: "Alex"
  - subject: "Re: I agree"
    date: 2004-01-20
    body: "How about Feb 3ed? (03/02/04) for kde 3.2? :)"
    author: "kormoc"
  - subject: "Thanks to all actors of KDE"
    date: 2004-01-19
    body: "all is in the title"
    author: "William"
  - subject: "KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "Right now it seems that KDE 3.2 is just too buggy, I think it should have a RC2 and have a mid February release date, otherwise I will consider it a rushed product.\n\nCheck the statistics for yourself, sure bug counts aren't all tat important, but when this late in the release cycle there is such a huge spike in new and unconfirmed bugs, you know there is something wrong. Besides, the goal for 3.2 was to have a release with under 3,000 (2,500 was planned) bugs, now it seems the new goal is under 4,000.\n\nBug chart here http://tinylink.com/?OdDme9ObFk \n\nNotice how last year for KDE 3.1 the number of unconfirme dbugs and new bugs went straight down as if freefalling, this is how it should look for 3.2, but instead it shows a large increase in bugs, not a decrease as is normal.\n\nI want KDE 3.2 to rock and right now on my system it just isn't where is should be a this stage in the cycle. To make KDE rock it needs at least one more RC. From what I've done on software development I know that the RC should be released when you think everything is the way you want it to, because when you think everything is like you want it to be, you will notice in the RC, that it's not quite there yet mos tof the time. But sometimes, like for Linux kernels, the RC becomes final because it really is where it should be.\n\nThank you, please consider a RC2 for a good 2-3 weeks more."
    author: "Mario"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "Did you tried RC1 ?\nIt's stable, no crash of konqueror or others apps.\n\nAll my bugs from beta2 are gone. But they still open in the bug database. Probably duplicates or somebody didn't closed them. There are many reasons why there are many opend bugs.\n\nBut the current rc1 is ready for primetime"
    author: "JC"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: ">But the current rc1 is ready for primetime\nSure? Is e.g. this showstopper ( http://bugs.kde.org/show_bug.cgi?id=70070 ) fixed in rc1?"
    author: "Carlo"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "Your bug is neither confirmed nor marked as critical. So I guess the developers have a hard time to reproduce it.\n\nAlso I'm pretty sure that they need more information from you like your language and locale settings."
    author: "anonymous"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "1) It's not a locale problem. \n2) The involved devs speak german, but even if not, everyone should be able to translate JJJJ to YYYY in this context.\n3) It doesn't matter, if the bug is marked critical or not, if the user experience sucks."
    author: "Carlo"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "> 1) It's not a locale problem.\n\nBut it seems to be at least a local problem since I never experienced anything like this. So the developer surely does need more information from you about your setup.\n\n> 2) The involved devs speak german, but even if not, everyone should be able to translate JJJJ to YYYY in this context.\n\nOf course but that's not what I meant. Talked about your language and locale _settings_.\n\n> 3) It doesn't matter, if the bug is marked critical or not, if the user experience sucks.\n\nI'm sure it sucks. I'm also sure that the developer works on this as fast as possible, but you were talking about a _showstopper_ that should delay the release. IMHO this is not the case.\n"
    author: "cloose"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: ">But it seems to be at least a local problem since I never experienced anything like this.\nPlay around in KOrganizer, setting the month and years a few decades forth and back, open the print dialog and you should be able to share the problem.\n\n> Talked about your language and locale _settings_.\nStill wondering what it could have to do with it.\n\n>[...]but you were talking about a _showstopper_ that should delay the release. IMHO this is not the case.\n\n_showstopper_ not from the technical standpoint (it's a trivial bug, rm -rf ~\\.kde and i'll get the date shown again - I hope so at least), but from the user standpoint it simply sucks. Think about opening kicker->clock->calendar and watching the date 01.DD.YYYY, searching for an email by date and all of them are formatted like this...\n\nI could mention other things. Freezing kde, when noatun opens empty or broken audio files, lost settings of open kde applications, on next restart after kde had to be killed, ..."
    author: "Carlo"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "~/.kde of course ;)"
    author: "Carlo"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "please list the bug numbers.."
    author: "anon"
  - subject: "Here as some bugnumbers. Includes a crash in 3.2RC"
    date: 2004-01-21
    body: "I think that the following bugs should be solved before 3.2\n\nCRASH: Bug 72775: CRASH: printing message in draftfolder \n\nBug 70418: [test case] table height=100% gives strange gaps (regression)\n\n(this one prevents me from using the web interface of my company's \nNovell email system68921 \n\nBug 68921: Marking text with keyboard disables keyboard input )\n\nThis one makes it very hard to edit something in a box on a web page.\n\nI could go on...\n\nFor example there are quite a few webpages where the javascript does\nnot work or even whole javascript menus do not show up.\nMail me (see the bugs for my email address) if you need examples."
    author: "John van Spaandonk"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-23
    body: "> no crash of konqueror or others apps\n\nKonqueror crashes for me.\n\nI have filed several other bug report for the branch.\n\nYes, it still needs a little work.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "> I think it should have a RC2 and have a mid February release date\n\nAnother RC would be not about greatly reducing the amount of bugs. In fact only showstopper bugs are allowed to be fixed now in KDE_3_2_BRANCH until release.\n\n> otherwise I will consider it a rushed product.\n\nSilly, it's already delayed by almost two months.\n\n> but when this late in the release cycle there is such a huge spike in new and unconfirmed bugs, you know there is something wrong\n\nSure, users delaying their reporting of bugs they know since Beta1/Beta2 until one week before release.\n\n> Notice how last year for KDE 3.1 the number of unconfirme dbugs and new bugs went straight down as if freefalling\n\nThat was the time when the bug system was switched and many duplicates and old bugs were closed. Did you also notice how until the release of KDE 3.1 in January 2003 it almost climbed up back again? \n\n> To make KDE rock it needs at least one more RC. \n\nNo, it needs KDE 3.2.1, 3.2.2, 3.2.3, ...\n\n> the RC should be released when you think everything is the way you want it to\n\nThen we would see never see a release."
    author: "Anonymous"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "> > To make KDE rock it needs at least one more RC. \n> No, it needs KDE 3.2.1, 3.2.2, 3.2.3, ...\n\nExactly,\n- To buggy to be released as stable. Too many minor bugs and annoying issues.\n- Will only be stable enough after the release . It needs time and actual use to mature. \n\nIMO the obvious solution is quite simple actually. \nKeep the process, change the names. The 1st release (with binaries and all), after the short sequence of RCs should still be called a RC and not a .0 version. It would have a lifetime of 1-2 months and then the equivalent to .1 would be the .0 release."
    author: "Source"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "This won't work. Fact is most people don't even test RCs (ask Linus)."
    author: "anonymous"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "Yes it will. Fact is most people do (ask the Mplayer team).\n"
    author: "Source"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "People are way more likely to use the RC's for something like Mplayer then for something like KDE or a kernel.  The reason?  What is the worst thing that happens if Mplayer doesn't work right?  You can't watch videos, big deal.  \n\nIf your kernel has major issues, or the system of which you most often use to interface with your computer has major issues, it is a lot bigger deal.  There is a reason why the kernel is never ready in a .0 release and that is because the user base of a kernel goes up by a factor of 10 or more as soon as they make a .0 release.  Why do you think every kernel has a rush of bug fixes at the begining and it settles out as time goes on and there are a lot fewer releases."
    author: "jts"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "\"People are way more likely to use the RC's for something like Mplayer then for something like KDE or a kernel. The reason? What is the worst thing that happens if Mplayer doesn't work right? You can't watch videos, big deal.\"\n\nI know that. I was beeing sarcastic.\nThey're more likely to use Mplayer than KDE, just like they're more likely to use KDE than the kernel. I was just using his distorted reasoning."
    author: "Source"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "MPlayer has never had a stable release in their whole project lifecycle. So, yes, if people want to run MPlayer at all they are forced to run one of the eternal \"release candidates\".\n"
    author: "Jan Ekholm"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "This is completely wrong!\n\nPlease do not confuse stable and numbered 1.0. The 0.90 was stable. Only bugfixes/minor enhancement were done to the serie, while there was a development branch aside (called \"main\").\n\nJust having add a look at Mplayer's news archive page would have given you facts instead of attacking Mplayer with blatant mistakes (or was is a lie?)\n\nFor example the annoucement for 0.90:\n\n\" 2003.04.06, Sunday :: Finally! MPlayer 0.90 released!\nposted by Gabucino\n459 days have passed since we released our last \"stable\" release: MPlayer 0.60 \"The RTFMCounter\".\""
    author: "oliv"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "You're aware that your \"bug charts\" contain feature requests, aren't you?\n"
    author: "Stephan Kulow"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "I use kde 3.2 10 hours a day 5 days a week for heavy development and it is more stable/efficient than kde3.1, gnome-2.4 or WindowsXP ever since beta1. \n\n-> No crashes \n-> lisa, fish, smb are seamless now\n-> konqueror is very fast \n-> KDE 3.2 now loads faster than winXP and gnome.\n\nI know several people that I work with have had great experiences with it so far. If you are having problems check your cflags, glibc and gcc versions or report a bug. Unfortunately many people who mean to help be reporting bugs inadverntantly submit user errors as opposed to actual bugs. It is usually a bad idea to base the stability of an app based on the number of reported bugs. \n\nOne good explanation for the increase in bugs in the increase in user base, particuarly newbies ;)   We can only hope... \n"
    author: "Moe"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-19
    body: "New apps mean new bugs (s. http://kde.ground.cz/tiki-index.php?page=KDE+3.2+Applications+Difference)."
    author: "anonymous"
  - subject: "Oops"
    date: 2004-01-19
    body: "=p"
    author: "Mario"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "\"but when this late in the release cycle there is such a huge spike in new and unconfirmed bugs, you know there is something wrong.\"\n\nWhat makes you say that? Let me guess: Since there was a spike in number of bug-reports, you automatically assume that it means that there are more bugs, right? Well, it hasn't occured to you that the spike may be due to:\n\na) There were more users reporting bugs\nb) Those bugs had been there for a long time already, but they were just now being discovered\nc) due to part a), there were lots of dublicates\nd) Due to having more users, there were more wishes (wishes are reported in bug-reports as well)\n\nNone of those are bad. Just because number of bug-reports go up does NOT mean that the code is magically becoming buggier. It just means that more people are reporting more bugs.\n\nIf I release some app, and to my knowledge it has zero bugs. After two weeks, I have received  20 bug-reports. During those two weeks I haven't touched the code at all. According to your logic, my code would have somehow magically transformed to contain those 20 bugs, when in reality they were there all the time, I just hadn't noticed them.\n\nIf you run KDE today, and next week there are (for example) 100 new bugs being reported, it does NOT mean that the KDE on your hard-drive has somehow magically become more buggier.\n\nTo my knowledge, Mozilla has over 10.000 reported bugs, and that's just for one app!"
    author: "Janne"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-20
    body: "> To my knowledge, Mozilla has over 10.000 reported bugs, and that's just for one app!\n\nit had more than 100,000 last time i checked, which was over a year ago"
    author: "anonanon"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-21
    body: "Perhaps the version numbers should be determined objectively =P\n\ne.g. version = target version - ceil ( (bugs opened - bugs closed) / n ) + c"
    author: "Anonymous"
  - subject: "Re: KDE 3.2 NEEDS A RC2"
    date: 2004-01-22
    body: "You would expect an increase in bug reports when a project moves from beta to RC status simply because more users will try it at that stage. Similarly, I'd expect another spike in bug reports when it goes from RC to final. I'm not saying you're wrong about RC1 not being ready though - just arguing with your point about more bug reports implying something has gone wrong.\nBut even 3.2.0 will probably not be a totally satisfying end-user product, just because until it's had a lot more hammering there will still be bugs to work through. It'll really start to get that hammering when 3.2.0 is out and more people try it (hopefully debian/unstable will have it pretty quickly)."
    author: "Adrian"
  - subject: "thumbs up !"
    date: 2004-01-19
    body: "Thanks to all the KDE team for this wonderful accomplishment ! Rudi (and then Dobra Voda) : my best desktop experience to date ! Since I compile from source on a \"relatively slow\" computer, I wait for the final release. For now, I keep enjoying beta2 ! Thanks again guys !"
    author: "jeanux"
  - subject: "Thanks to those responsible for..."
    date: 2004-01-19
    body: "thanks to those responsible for...\n1) the new mapping of the Win key to the kde taskbar program menu in 3.2\n2) the much-improved tab support in konqueror 3.2\n3) the beginnings of svg support in kde 3.2\n4) konsole, the crown jewel of kde (along with konqueror) for my usage\n5) the continued enhancement of khtml in 3.2\n\nthanks will go to whoever...\n1) makes file association mappings view-profile specific or local vs. network specific\n2) creates a read-only version of the ktexteditor service that is better suited for browsing of text files\n3) continues to improve khtml and ksvg\n\n"
    author: "long time kde user"
  - subject: "Re: Thanks to those responsible for..."
    date: 2004-01-20
    body: "I always thought I would give thanks to whoever...\n4) makes it easy to choose your default browser."
    author: "Trejkaz"
  - subject: "Choosing browser not work?"
    date: 2004-01-21
    body: "Does using the Control Center to change the preferred application for inode/directory and text/html not work anymore?"
    author: "Anonymous"
  - subject: "Suse RPMS"
    date: 2004-01-19
    body: "I just found suse rpms here\n\nftp://ftp.suse.com/pub/people/adrian/KDE-CVS-BUILD/current/"
    author: "Helmut"
  - subject: "Re: Suse RPMS"
    date: 2004-01-19
    body: "Hey, can you please test the rpms before you post an url ?\n\nthese have requires to qt 3.3 and the rpms do not exist there.\n\nbut the german mirror has them:\n\nftp://ftp.de.kde.org/pub/kde/unstable/3.1.95/"
    author: "Micha"
  - subject: "Re: Suse RPMS"
    date: 2004-01-19
    body: "Nice! Anyone tried this rpms yet?\nHow did it go? Any packaging problems? "
    author: "Source"
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "I'm using the RPMs for SuSE 9.0 right this second.\n\nEasiest set of KDE rpms I've ever installed, no dependency issues at all.\n\nEverything seems to be spanky :-)"
    author: "Max Howell"
  - subject: "Re: Suse RPMS"
    date: 2004-01-21
    body: "I apt-get the Suse 9.0 RPM's last night and will be backing them out tonight.\nKmail and Knode are gone, I have 3 clocks on the tool bar now and none of my previous application buttons (menu, shell, kmail, knode) Sound is not working now either and it is so slow I thought I had acidentally started my P166."
    author: "Mike "
  - subject: "8.1 packages rotten"
    date: 2004-01-20
    body: "At least the 8.1 packages are built with the wrong toolchain and require glibc 3.2.2 while SuSE 8.1 still uses 2.2.5\n\nSame as with KDE 3.1.5 lately.\nKDE 3.1.94 was ok *sigh*\n\nOliver"
    author: "Oliver Fels"
  - subject: "Re: 8.1 packages rotten"
    date: 2004-01-20
    body: "a glibc 3.2 does not exist at all. Do you mean 2.3 ? but that is wrong, \n\nrpm -qp --requires *rpm | grep GLIBC_2.3 \n\ntells you ..."
    author: "Micha"
  - subject: "Re: 8.1 packages rotten"
    date: 2004-01-20
    body: "While it says glibc 2.3, what it actually means is libstdc++ from gcc 3.2.2.  As Oliver says, SuSE 8.1 uses gcc 3.2, so it's really helpful that they got the dependancies wrong.  I wouldn't mind if they made gcc 3.2.2 available for SuSE 8.1, but they didn't even do that.\n\nActually, you can force the RPM install (--nodeps), and it seems to work OK.  If I really wanted, I could compile gcc myself from source, but that doesn't fix the dependancy problem.  Yast complains whenever I go in there!\n\nI'm still on SuSE 8.1, but if 9.1 has KDE 3.2 I'll buy it!\n\n-- Steve"
    author: "Steve"
  - subject: "Re: 8.1 packages rotten"
    date: 2004-01-20
    body: "I take that back: it doesn't all work OK (the SuSE 8.1 RPMs, that is).  Example:\n\nkcalc: /usr/lib/./libstdc++.so.5: version `GLIBCPP_3.2.2' not found (required by /opt/kde3/lib/kcalc.so)\n\nWhile I really appreciate someone else compiling stuff up for me, getting it wrong like that completely defeats the point of precompiling.\n\nTo whoever made the SuSE 8.1 packages: please fix them!  (Or provide the new GCC packages.)\n\n-- Steve"
    author: "Steve"
  - subject: "Re: 8.1 packages rotten"
    date: 2004-01-21
    body: "Bug report filed at SuSE.\nLet's wait and see what happens.\nI don't  want to build the whole beast, though I have done it in the past.\n\nOliver"
    author: "Oliver Fels"
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "Watch out for some of the QT3 rpms especially qt3-postgresql, qt3-devel-tools and qt3-extensions. These are for qt3-3.2.3, at least they were from the mirror at ibiblio.org. The main qt3 and qt3-devel where okay."
    author: "GeorgeMoody"
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "\"rpm -Uvh\" on the rpms there (ftp://ftp.de.kde.org/pub/kde/unstable/3.1.95/) for SuSE 9.0 gave me \n\"libkdeinit_noatun.so is needed by kdemultimedia3-video-3.1.95-0\".\n\nAfter some googling without success I found that I could use kdemultimedia3-3.1.95-1.i586.rpm from Adrian to get it. \n\nIt looks as if kdemultimedia3-3.1.95-x.i586.rpm is missing (at least) on the german mirror???\n\nHaven't installed it yet though, will try in a few hours. \n"
    author: "Martin"
  - subject: "Re: Suse RPMS"
    date: 2004-01-21
    body: "The one from Adrian seems to work.\nkdemultimedia3-3.1.95 is missing on the main kde-Server as well...\n\nGreat, I can use konqueror again! Was crashing in beta2.\n\nUnfortunately where are some font issues when opening a konsole. Anyone else had this?\n\n\n\n"
    author: "anony-mouse"
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "has anyone else noticed that the icons look rather strange when they are selected? I don't know if this is a SuSE specific thing, or if it is a more general issue with this RC. Any thoughts?"
    author: "Steve W."
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "It's a SUSE gcc compiler bug miscompiling MMX/SSE2 optimizations, works with pure GNU gcc."
    author: "Anonymous"
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "oh yeah... forgot, I'm also having problem with Kopete saving my passwords for automatic logon"
    author: "Steve W."
  - subject: "Re: Suse RPMS"
    date: 2004-01-21
    body: "Just tried the SuSE RPMs for 9.0 from the kde-Server. \n>init 5\n**login**\ngives me a message \"A critical error occured\"\nPlease look at the kdm's logfile(s) ...\n\nThe only error found in /var/log/kdm.log is QImage::convertDepth: Image is a null image.\n\nAre there other kdm.logs somewhere?\n\n"
    author: "anonymous"
  - subject: "Re: Suse RPMS syslog"
    date: 2007-04-02
    body: "Look for message in /var/log/messages"
    author: "jim"
  - subject: "Re: Suse RPMS"
    date: 2004-01-20
    body: "Do I have to install these manually or is there a way to use yast2?\n\nI can do it manually but I'd rather do it automagically with yast.\n\nThanks."
    author: "Hiryu"
  - subject: "Re: Suse RPMS"
    date: 2004-01-21
    body: "1) download all rpm's in one folder \n\n2) Strg+Alt+F1 \n\n3) Login as root\n\n4) #init 3\n\n5) Change to the folder where you have copied the rpm's\n\n6) #yast -i *.rpm (could take a while)\n\n7) #init 5\n\nthat's it (hopefully)\n\n\n\n\n"
    author: "thorsten k."
  - subject: "Re: Suse RPMS"
    date: 2004-01-21
    body: "i'm also trying to upgrade to 3.2, but i'm having some problems\nnow i got installed : arts, lib*, qt3 and kdelibs \nand after installing kdelibs, and trying install another rpm i get this error : There appears to be a configuration error. You have associated Konqueror with application/x-rpm, but it can't handle this file type.\n( system becomes unstable, so i have to downgrade back to 3.14 )\n\ni'm upgrading with konqueror\nis this the right way to upgrade ? ...\nwhere did i mess it up ?\n\nhelp and\ntnx for answers"
    author: "ycw_"
  - subject: "Re: Suse RPMS"
    date: 2004-01-22
    body: "Has anyone tried the SuSE RPMs from ftp.suse.com  (or a mirror) in in the supplimentary section where KDE official releases are normally placed?  I just downloaded them, but I've only seen bad reports on alt.os.linux.suse so I was hoping to hear at least one positive report before I plunge in.\n\nThanks!\n\nDave"
    author: "Dave M"
  - subject: "Re: Suse RPMS"
    date: 2004-03-23
    body: "I managed to use #rpm -Uhv --nodeps --force *.rpm and it works all fine.\nAfter that run yast system update and it tells you what to deinstall.\nMy 3.2 works great.\n\n"
    author: "Charles S"
  - subject: "Kudos"
    date: 2004-01-19
    body: "I, too, want to take the opportunity to thank everyone who helped in making this the best KDE ever!\n\nI've used Beta 2 on Gentoo recently - it's wonderful. I love the Plastik style. Kontakt is just great. KMail is notably faster on large mail folders. Control Center is much improved. So much more, and even more yet to discover...\n\nCan't wait to compile RC 1 tonight. So, thanks again - you all rock!\n\nRen\u00e9"
    author: "Ren\u00e9 Ga\u00df"
  - subject: "Re: Kudos"
    date: 2004-01-19
    body: "Rene, how did you go about compiling for Gentoo?  I tried emerging the beta and I'm getting errors trying to open many programs, most notably konqueror. I unmasked everything I thought needed to be unmasked.  If you could give me a minit how-to I would be most grateful.\n\nEric....."
    author: "Cirehawk"
  - subject: "Re: Kudos"
    date: 2004-01-20
    body: "I had first installed autoconf 2.58, then KDE 3.1.4 and later KDE 3.2 Beta 2. Nothing special on my side, except for copying all Beta 2 related lines from /usr/portage/profiles/packages.mask to /usr/portage/profiles/packages.unmask and giving ACCEPT_KEYWORDS=\"~x86\" in /etc/make.conf . I had KDE running first with XFree 4.3 and now with XFree 4.3.99.902. Beware, this is from memory only, as I don't have access to my PC right now.\n\nRen\u00e9"
    author: "Ren\u00e9 Ga\u00df"
  - subject: "Re: Kudos"
    date: 2004-01-28
    body: "In fact, it was /etc/portage/package.unmask instead of /usr/portage/profiles/packages.unmask , which I had to create as it wasn't on the system before.\n\nBTW: I've got RC1 running on Linux Kernel 2.6.1 now. This thing is just insanely faaaaast! Honestly, the speed of this combination just blows your mind right away.\n\nRen\u00e9"
    author: "Ren\u00e9 Ga\u00df"
  - subject: "Differences between current Safari & khtml of 3.2"
    date: 2004-01-19
    body: "Could someone please enlighten us and explain what's the state of of khtml\nvs current Safari webcore 1.06?\n\n\nLooking forward for 3.2 being in debian unstable :-)"
    author: "ac"
  - subject: "kde 3.2 in Debian unstable"
    date: 2004-01-19
    body: "\nIt will be, first, in experimental\nhttp://wiki.debian.net/index.cgi?DebianKDE\n\nM"
    author: "Maillequeule"
  - subject: "Re: Differences between current Safari & khtml of "
    date: 2004-01-20
    body: "\nFrom a /. post:\n\n>> How is the relationship between Apple and the kde project?\n> Good.. the khtml developers and apple have a private mailing list where they \n> roll changes back between KDE and Webcore.\n\nIs this true? Or just /. rumours... ???"
    author: "ac"
  - subject: "Re: Differences between current Safari & khtml of "
    date: 2004-01-20
    body: "That's true."
    author: "Waldo Bastian"
  - subject: "Re: Differences between current Safari & khtml of "
    date: 2004-01-21
    body: "Why is it private?"
    author: "a"
  - subject: "Re: Differences between current Safari & khtml of "
    date: 2004-01-21
    body: "Because they dont want their work interupted by ppl who mistake a work-list with a community-list ;-)\n\nBut yes.. id like it to be a read-only too.\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Differences between current Safari & khtml of "
    date: 2004-01-21
    body: "Good to here. Having a private list is a good idea, but sometimes people outside wonder what is happening sometimes."
    author: "David"
  - subject: "Re: Differences between current Safari & khtml of "
    date: 2004-01-21
    body: "I just can't spell these days!"
    author: "David"
  - subject: "made it to /."
    date: 2004-01-19
    body: "http://developers.slashdot.org/article.pl?sid=04/01/19/2056257\n\nSeems to be well-received. :)"
    author: "ac"
  - subject: "Re: made it to /."
    date: 2004-01-19
    body: "Yes, it seems just about every comment is positive. A big turn from previous KDE sightings at Slashdot. Maybe the trolls have found something more productive to do."
    author: "Mario"
  - subject: "Re: made it to /."
    date: 2004-01-19
    body: "This is a very good release.  It works very well and stable!"
    author: "ac"
  - subject: "Re: made it to /."
    date: 2004-01-20
    body: "The last several KDE articles on Slashdot have been rather positive. I think the people dispelling some of the prevailing FUD about KDE are starting to have an effect :)\n"
    author: "Rayiner Hashem"
  - subject: "KHTML Bugs"
    date: 2004-01-19
    body: "I haven't tried RC1 yet, but I am using beta-2.  The khtml bugs that were preventing me from viewing some popular, very non-geek, sites were fixed with beta-2 and I'm assuming with RC1.  I had posted a couple months ago, maybe more, about some khtml bugs.  I'm so glad the bugs were fixed.  Now I can ditch Firebird almost completely. THANKS! :)  Konqueror can now browse almost everything -- only just some minor layout-spacing bugs but everything I try is now visible.\n\n"
    author: "SuperPET Troll"
  - subject: "Re: KHTML Bugs"
    date: 2004-01-19
    body: "Good to see you happy with all those bugs.\n\n"
    author: "noname"
  - subject: "P2P distribution"
    date: 2004-01-19
    body: "Maybe some people will set up a bit-torrent or edonkey directory...\n\ned2k://|file|kde-i18n-3.1.95.tar.tar|161190315|FB6F5BE58E8E3DDEDA051717D1EA3DFD|/\ned2k://|file|kdeaccessibility-3.1.95.tar.tar|1299824|7CD4E97460899BBB40832AC0905FC28B|/\ned2k://|file|kdeaddons-3.1.95.tar.tar|1362993|C8E749B68FF5AB73E04104286B169ADB|/\ned2k://|file|kdeadmin-3.1.95.tar.tar|1604464|3DF83500A2B2D411511249DD2196FF0A|/\n\nand so on...\n\n"
    author: "Mathias Schindler"
  - subject: "Re: P2P distribution"
    date: 2004-01-20
    body: "Some of the developers have edonkey-clients running with the current tar.bz2 source tree, but sadly a lot of servers are configures to drop tar.bz2 files (dunno why). So it's kinda hard to find them."
    author: "Andy"
  - subject: "Congrats."
    date: 2004-01-20
    body: "This should be great. Thanks for all of the hard work. I have a couple of feature requests for the future... just can't find a good spot for them.. but i would really like to see a couple of features that are currently found in ROX but seem to be well suited for KDE. \n\nAppDirs (each application gets its own directory where all of it's files belong, libraries excluded)\n\nDrag and Drop save... the opposite of drag and drop load... drag from an app to the file manager and voilia.\n\nonce again. thanks a ton."
    author: "jcd"
  - subject: "Re: Congrats."
    date: 2004-01-20
    body: "> Drag and Drop save... the opposite of drag and drop load... drag from an app to the file manager and voilia.\n\nI've seen that as an experimental patch for Gimp 1.3.xx and for KOffice, but is was not accepted, since it gives many problems when running on Gnu. If translators have an ftp or sshfs shadowed into your current documents dir, it is impossible to determine, where the file(s) should go. Normal file save dialogs are adjusted so they can handle this, but pure drap and drop won't work (because you can even use ro and rw fs together in one dir)."
    author: "Andy"
  - subject: "fedora anyone?"
    date: 2004-01-20
    body: "need\nfedora rpms\nnow\n\n\ncheers,\nlars\n"
    author: "lars"
  - subject: "Re: fedora anyone?"
    date: 2004-01-21
    body: "Almost ready, keep an eye on http://kde-redhat.sf.net/ (in the unstable repoositories).\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: fedora anyone?"
    date: 2004-01-21
    body: "Are you still working on this?  I noticed that it seemed that most of the packages were there.  After correcting most of the dependencies, I'm now getting this:\n\nerror: Failed dependencies:\n        libsensors.so.1 is needed by kdebase-3.1.95-0.fdr.0.rh90\n\nBut - I have libsensors.so.2 (from the lm_sensors-2.8.0-1 package and my present kde setup requires this package) which is newer than what the error is requesting.\n\nThanks,\n\nLaura"
    author: "Laura"
  - subject: "Re: fedora anyone?"
    date: 2004-01-21
    body: "Yep, about 1/2 done, still building... (to be finished probably in another 4-6 hours).\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: fedora anyone?"
    date: 2004-01-24
    body: "I am just switching over using fedora ISO disks.. from ..that other os.  I was a mandrake 7.2 user before and loved the K desktop.  Would love to see a really noobie install for fedora of KDE 3.2 rc1..\n\nI can tar -xf stuff and run inits through sh clients.. but Im rather new on the Linux side. would like to get it going again..\n\nps www.americasarmy.com is a free game that is bleeding edge good, and FREE FOR LINUX.\n\nST-6_Machine\nJeremy"
    author: "Jeremy"
  - subject: "Re: fedora anyone?"
    date: 2004-01-21
    body: "update:\n\nFYI\nhttp://www.redhat.com/archives/fedora-list/2004-January/msg04520.html\n...\n\"yes, KDE 3.1.95 rpms will be available on ftp.kde.org for FC1 this weekend ;-)\n\nThan\n\"\n\nbig thanks to the fine folks at kde!"
    author: "lars"
  - subject: "Re: fedora anyone?"
    date: 2004-01-22
    body: "building for fc1 seems to be complete now.\ngrab them at ftp://apt.kde-redhat.org/apt/kde-redhat/1/RPMS.unstable\n\nmany thanks to rex!!\n\n"
    author: "lars"
  - subject: "Re: fedora anyone?"
    date: 2004-01-22
    body: "I've tried installing the rpms, but there seems to be a requirement for Themer >= 0:1.4 with kdebase. My installed version, and the latest I can find, is 1.3. Any ideas where I can get one that fits in with the rpm requirement?\n\n\nPip"
    author: "Pip"
  - subject: "Re: fedora anyone?"
    date: 2004-01-22
    body: "Ah, found it at:\n\nftp://apt.kde-redhat.org/apt/kde-redhat/all/RPMS.stable/themer-1.41-0.fdr.1.noarch.rpm\n\n\nPip"
    author: "Pip"
  - subject: "Not ready yet - take the time to get it right"
    date: 2004-01-20
    body: "1) KDevelop in RC1 compiling fails with bad function call (Mainwindow:251) (compiling under 3.2 b2)  This kind of error is indicative of rushing to release.\n2) PIM not completed but already has some major important changes in branch which could be merged in, maybe if given another month or two?  PIM is supposed to be one of the killer apps in 3.2 - why ship a half completed one when you could have a great one in another month or two.\n3) Pdf viewing and mime was badly mangled in b2 - not sure if fixed in RC1\n4) no time for binaries?  Allow the time so thorough beta testing is done.\n\nWhy not ship a few RC's for the next month or two  until it's at a higher QA level and PIM changes merged in - no one likes delays but it's not like you are under stockholder pressure to ship.    "
    author: "John"
  - subject: "Re: Not ready yet - take the time to get it right"
    date: 2004-01-20
    body: "kdepim will see an additional extra release in about 3 months. Have a look at  http://developer.kde.org/development-versions/kdepim-3.3-release-plan.html"
    author: "Birdy"
  - subject: "Re: Not ready yet - take the time to get it right"
    date: 2004-01-20
    body: ">>>1) KDevelop in RC1 compiling fails with bad function call (Mainwindow:251) (compiling under 3.2 b2) This kind of error is indicative of rushing to release.<<<\n\nNo, it's indicative of you assuming you can do something that you cannot (which is build KDevelop 3.0 RC1 on KDE-3.2b2).\n\nKDevelop 3.0 RC1 needs KDE-3.2 RC1 (or KDE-3.0.x/3.1.x) to build, it's as simple as that.\n"
    author: "teatime"
  - subject: "Re: Not ready yet - take the time to get it right"
    date: 2004-01-20
    body: "Well, okay.  With no binaries, we don't have the time to tie up a computer for hours (wasted 1 1/2 hours on just the failed KDevelop) - which means one less beta tester here - which gets back to my point of testing time.  I don't know: maybe not enough people will ever test an RC or maybe they don't test because binaries are not available.  Should be easy enough to run an experiment and find out - if not enough will test no matter what, well then go ahead and release quick, else release slower.  Attn students: this would make a good paper."
    author: "John"
  - subject: "Don't have these problems here"
    date: 2004-01-20
    body: "KDevelop compiled perfectly on my Slackware-current. Actually I think it's the first time I managed to compile every package without a single issue.\nMaybe a system cleanup would be appropriate? Or maybe it's just we do not have the same version of gcc/glibc/QT..."
    author: "Richard Van Den Boom"
  - subject: "Konstruct on FreeBSD"
    date: 2004-01-20
    body: "I'd like to build KDE 3.2-rc1 on my FreeBSD box. However, when using Konstruct it goes to grab glib 2.2.3. The problem is that I already have glib 2.2.3 installed via ports. Does konstruct detect preinstalled dependencies and just doesn't see my glib for some reason? It seems to do the same thing with pkgconfig. Does konstruct just not check to see if I already have the software in question installed? I have the correct versions.\n\nThanks.\n\n"
    author: "Hiryu"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2004-01-20
    body: "Does ports automatically install development files (includes, etc) .\nUsually detection scripts triy to compile a test source file and if fails sum up it s missing.\n"
    author: "Alban"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2004-01-20
    body: "99% of the time, it does. And I do know it does for glib.\n\nThanks for the response."
    author: "Hiryu"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2004-01-20
    body: "> Does konstruct just not check to see if I already have the software in question installed?\n\nIt installs everything for which it has definitions without checking. If you know that you have something installed, remove it from LIBDEPS line in the Makefiles and update with CVS to not loose your changes."
    author: "Anonymous"
  - subject: "Support for GLMatrix screensaver?"
    date: 2004-01-20
    body: "I was crushed to install KDE 3.1.4 and find out that it doesn't include the cool GLMatrix screensaver (even though many other xscreensaver savers work under KDE).\n\nDigging through the Gentoo forums, I found a post stating that the kdeartwork package has a list of \"useable\" xscreensaver modules.  Since GLMatrix isn't on this list, it isn't usable under KDE.\n\nAnyone know if GLMatrix (and other missing modules) will be there when I emerge kde next month?\n\nPS - I also wish the random screensaver mode could be configured to choose from a list of screensavers.  Yes, I know screensavers aren't a big deal to most people but they are fun :)"
    author: "Rimmer"
  - subject: "Re: Support for GLMatrix screensaver?"
    date: 2004-01-20
    body: "it's already there in kde before any 3.2 beta releases, i use it without any problem.\nhttp://lists.kde.org/?l=kde-cvs&m=106657044628526&w=2"
    author: "rzr"
  - subject: "Re: Support for GLMatrix screensaver?"
    date: 2004-01-22
    body: "I assume you mean that this in KDE 3.1.5?  It's not there in 3.1.4 for me (I'm using Gentoo)."
    author: "Rimmer"
  - subject: "Re: Support for GLMatrix screensaver?"
    date: 2004-02-01
    body: "Nope, it's not in 3.1.5 either, and btw. I'm running Gentoo also.  Why KDE doesn't just show every XScreensaver screensaver in the list is beyond me."
    author: "TX"
  - subject: "Re: Support for GLMatrix screensaver?"
    date: 2005-04-13
    body: "quite old posting, but I couldn't find anything else about this bug (?) so far ...\n\nThe problem is that although the files glmatrix.desktop and xmatrix.desktop both exists, but in some translations (e. g. german) they have the same name. As a workaround you could edit glmatrix.destop (or both files) with your favorite editor and change the translation of your language.\n\nFor example: change\nName[de]=Matrix\nto\nName[de]=MatrixGL\nif you're german.\n\nRegards,\n  Christian Stadler"
    author: "Christian Stadler"
  - subject: "Re: Support for GLMatrix screensaver?"
    date: 2004-01-20
    body: "you may need to upgrade to the latest version of xscreensaver. That's what I had to do. (I'm using latest KDE CVS)."
    author: "anonymous"
  - subject: "Mandrake RPMS"
    date: 2004-01-20
    body: "Is there any volunteer to make Mandrake RPMS?\n\nMarcin"
    author: "Manux"
  - subject: "Re: Mandrake RPMS"
    date: 2004-01-20
    body: "They've been built in cooker."
    author: "Richard Jones"
  - subject: "Re: Mandrake RPMS"
    date: 2004-01-20
    body: "does anyone know if there are plans to do a contrib .rpm for mdk9.2 for the real kde 3.2 release? i would love to use kde 3.2, but right now i'm v. v. happy with mdk9.2 and i don't think i'll want to use cooker or mdk10.0 in production...  "
    author: "spiff"
  - subject: "Re: Mandrake RPMS"
    date: 2004-01-21
    body: "The Cooker RPM'S works fein on my mdk9.2\n"
    author: "Galosche"
  - subject: "SuSE"
    date: 2004-01-20
    body: "VEry glad to see that the first company to provide binary rpms is SuSE.\nShould put some minds at ease about suses commitment to KDE ..."
    author: "Gerhard den Hollander"
  - subject: "SuSE 8.1 RPMs wrong glibc version"
    date: 2004-01-20
    body: "I have no idea what changed in the build system between KDE 3.1.94 and 3.1.95 but since the KDE3.1.5 release a few days ago, all SuSE 8.1 RPMs require a glibc3.2.2 system while SuSE 8.1 is still based on 2.2.5\n\nI wonder whose idea this has been- not so brilliant as the consequences are noticed *after* download while trying to install the first packages.\n\nOliver"
    author: "Oliver Fels"
  - subject: "Re: SuSE 8.1 RPMs wrong glibc version"
    date: 2004-01-20
    body: "not for me and \n\nrpm -qp --requires arts*rpm | grep libc.so\n\ndo only list glibc 2.0-2.2 symbols.\n\nAre you sure that you did not download the wrong packages ?"
    author: "Micha"
  - subject: "KMail - Outlook Express"
    date: 2004-01-20
    body: "I really hope the Kmail team included the best feature in express, ever.\nThe one where you can't save received pdf:s, because of virus riscs.\n\nGreat feature, probably BG hacked that one in himself. :-)"
    author: "OI"
  - subject: "Re: KMail - Outlook Express"
    date: 2004-01-20
    body: "Talk about freedom to do what you want with your software!\n\nOf course, AFAIK Outlook still opens malicious HTML messages without you even opening them!"
    author: "Anonymous"
  - subject: "Re: KMail - Outlook Express"
    date: 2005-05-16
    body: "I must admit to being more impressed with\n http://www.oemailrecovery.com outlook express errors - Outlook Express errors repair tool\nhttp://mail-repair.com Outlook Express fix -  Tool for Outlook Express fix"
    author: "outlook "
  - subject: "Re: KMail - Outlook Express"
    date: 2004-01-20
    body: "But it protects you from those terrible, executable gifs!\n\nImagine an executable Goatse... Aren't you glad that OE blocks everything, now?"
    author: "Dawnrider"
  - subject: "Can't compile kdegraphics ..?"
    date: 2004-01-20
    body: "I can't compile RC1 using Konstruct-20040119, it hangs somewhere in kdegraphics :\n\nchecking for Qt... configure: error: Qt (>= Qt 3.2) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\nMake sure that you have compiled Qt with thread support!\nmake[1]: *** [configure-work/kdegraphics-3.1.95/configure] Erreur 1\nmake[1]: Leaving directory `/home/pemessier/Programmes/konstruct-20040119/kde/kdegraphics'\nmake: *** [dep-../../kde/kdegraphics] Erreur 2\n\nAll I did is \"cd meta/everything\" and \"make install\". All other stuff, including QT, was downloaded and compiled automatically. Any ideas why it hangs just there in kdegraphics ? Beta1 compiled just fine though..."
    author: "SiLiZiUMM"
  - subject: "Re: Can't compile kdegraphics ..?"
    date: 2004-01-20
    body: "Check the following:\n\n- you're running as the same user\n- you've set $QTDIR in your .bash files, eg. $QTDIR=/opt/qt32\nThen log out and in again."
    author: "anonymous"
  - subject: "Re: Can't compile kdegraphics ..?"
    date: 2004-01-20
    body: "The $PATH and $QTDIR are (and were) correct and point to the new RC dir (~/kde3.2-rc), so it still won't work..."
    author: "SiLiZiUMM"
  - subject: "Re: Can't compile kdegraphics ..?"
    date: 2004-01-21
    body: "I ran into the same problem; setting the environment variables manually doesn't seem to help either."
    author: "crabstic"
  - subject: "Re: Can't compile kdegraphics ..?"
    date: 2004-01-21
    body: "I had a similar problem, both using a Konsole and under init 2"
    author: "Till"
  - subject: "Re: Can't compile kdegraphics ..?"
    date: 2004-02-12
    body: "I researched this problem on my machine and it turned out to be a dependancy with the sane libraries (sane being scanned libs)  You might look into that...\n\n\n_jeremy"
    author: "Jeremy"
  - subject: "DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-20
    body: "If you use KDE and like what the developers are doing for it, be sure to let them know by helping out or donating a sum of money.\n\nAgain, KDE IS NOT ASKING FOR DONATIONS as much as they should. Hence, they aren't getting as many as they deserve or even close to as many as GNOME.\n\nGNOME for example prominently features on their website with a big banner \"New Year's Resolution: Support the GNOME Foundation\" and also taking a large chunk of the page is this message:\n\n\"The GNOME Foundation\nWith hundreds of thousands of new users taking advantage of GNOME at Brazilian Telecentros and at schools in Extremadura, now is a good time to make a donation to the GNOME Foundation. To help, become a Friend of GNOME or increase your current level of sponsorship. With your contribution, the GNOME foundation can bring GNOME to the developing world and defray expenses for independent developers and students coming to conferences like GUADEC and the GNOME Summit in 2004.\n\nAll contributions are tax-deductible in the U.S.\n\nSpecial thanks to our generous friends, and remember that every donation is important.\"\n\nWhen will KDE get it! Donations don't just drop down from the sky into your lap! You have to actively encourage users to donate and provide incentive. The GNOME Friends program is a great way to do this, they provide different levels with different benefits, mostly symbolic, but still important. KDE doesn't show the need  for donations half as much as GNOME yet GNOME gets more donations. The KDE team would not even place something like GNOME has on the front page about donations because it is somehow in bad taste. WHY?\n\nIf you too think KDE isn't doing enough to encourage donations, vote here:  http://bugs.kde.org/show_bug.cgi?id=63868"
    author: "Alex"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-20
    body: "> they aren't getting as many as they deserve or even close to as many as GNOME\n\nI have never seen figures how much GNOME gets (from users, not from companies). Please enlighten me."
    author: "Anonymous"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-20
    body: "Begging?"
    author: "anonymous"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-20
    body: "Alex,\n\n\"The KDE team would not even place something like GNOME has on the front page about donations because it is somehow in bad taste. WHY?\"\n\nThat is because you are supposed to develop KDE because it makes sense to you, not because you are paid for it.\n\nI personally want no sponsored desktop development. I rather prefer to take what clever people come up to solve their problems with in a easy way.\n\nThis may be shocking to you. But KDE is about to win over Gnome, mostly because the bad feel created in Gnome about the commericialism.\n\nYours, \nKay"
    author: "Debian User"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-20
    body: "This may be shocking to you. But KDE is about to win over Gnome, mostly because the bad feel created in Gnome about the commericialism.\n--------\nThat's delusional. Its the commercialism that will help out GNOME the most in the end. If Linux on the desktop becomes a reality, and there is indeed a shakeout of desktops, it will be the commercial interests that decide \"who wins.\" The community may drive development, but technical quality means little in the computer market, and commercial interests drive adoption.\n"
    author: "Rayiner Hashem"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE "
    date: 2004-01-20
    body: "> technical quality means little in the computer market\n\nOr much if you have something to gain from it."
    author: "KA"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE "
    date: 2004-01-21
    body: "The history of the computing market should be enough to prove to anyone that superior technology means nothing without other factors at play. The computer industry is riddled with great technical achievements that have been monumental failures. The products that have become successful are almost universally mediocre. \n\nYou can't gain anything from good technology alone. Its a great complement to other factors, but means little by itself.\n"
    author: "Rayiner Hashem"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE "
    date: 2004-01-21
    body: "Well, the reason why companies with good technology lost is because they didn\u00b4t make enough money to survive.\n\nIf we don\u00b4t expect to win any money, that should not be a factor ;-)"
    author: "Roberto Alsina"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE "
    date: 2004-01-22
    body: "> Well, the reason why companies with good technology lost is because they didn\u00b4t make enough money to survive.\n \nI'm afraid I have to agree with this. I'm really getting tired of hearing about all the commercial support behind GNOME and how it's gloom and doom for KDE. Horse crap. I want software for me and if you want it great and if not I'm not going to cry about it. As someone who understands marketing and does promote a successful application within KDE I personally think there is massive confusion on this issue, and that confusion doesn't serve KDE.\n\nThe big folly of this argument is as follows:\n1) Linux on the sever - it was a grass roots entry\n2) The rise of MS - they came up with the home user and fought companies like Novell and IBM\n3) In spite of all this big talk KDE continues to win users choice awards and technical accolades with software a year behind what we're producing.\n\nA lot of arguments can be made over this, and none of them produce any damn code... which is what people will base their decisions on. Unless you have millions of dollars you're ready to dump into promotion or tens of thousands you want to put into development it's pretty pointless.\n\nHere's clue one in open source... if there is something you really think should be being done, and it's not, maybe you should research it further? Those doing something have probably already come close to exhausting their time, talent and funds. What a concept. ;-)\n\nAs the saying goes, \"don't believe anything you hear and only half of what you see\" or to quote Samuel Clemmons, \"Rumors of my death have been greatly exaggerated\". KDE is on the smart path, like the Linux kernel, with limited entanglements and helpful support. Let's play the game with our hearts and stop listening to the desperate heckling that is trying to repeat lies until people believe them and make them true. \n\nM$ is on top because of a singular commitment to be there no matter what it takes. KDE is on top on the Linux desktop because of a singular commitment to produce the best software we can. I would rather be with KDE for the right reasons than with anybody else for any other reason! No external factor is going to make me do more or less, and I believe in the reasonable people who will choose software I produce because they believe it is best for them. Our team will make it's presence felt though innovation!\n\nThe lessons in the open source world are obvious and consistent... Linux is promoted by companies because it is important to their efforts and not tied to any one company. Open source has prevailed because it's been developed without the financial overhead of traditional development paradigms. Why is it that we are willing to believe the keys to our success will bring about our doom because of the shouting from those cashing in and trading off control while changing their vision?\n\nIf finances are the key why didn't MS money unseat Quicken? For us victory is not monopoly but diversity, but for our software victory is in proving the merit of our ideals. Here's an advanced marketing quiz... Ford's Edsel was both technologically advanced and heavily advertised as the new flagship in the 1950s. Why did both technical superiority and marketing fail? Let's not over simplify our observations and miss the point.\n\nWant to see good things happen? Take action. The only way to fail is to not take action."
    author: "Eric Laffoon"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE "
    date: 2004-01-21
    body: "> You can't gain anything from good technology alone\n\nWell, not money of course. You can not just sit there in a closet thinking \"Hey! I've got the best tech and now I'll become rich! rich!...\", obviously you have to do something more. But clearly, good tech helps."
    author: "KA"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-21
    body: "There is a point you are missing. Ultimately, in Open Source projects, the best project always wins, because there is no monetary requirement.\n\nIf Sun and others choose to fund Gnome, that is all well and good, but the KDE developers and community will continue to develop it, because they want it. You can't kill off KDE by withholding funding, like you can with commercial products; they just keep going. They keep going, in fact, until sooner or later someone says \"Well, why don't we try KDE instead of Gnome?\" and then it snowballs.\n\nThe only time KDE would never have a shot, is if Gnome incoporated KDE's benefits and made a platform that to all people is better. At that point, everyone would be happy.\n\nWhat will happen? Who knows? But Gnome can't win because of a worse product with more funding. That's the Microsoft way, and it is obvious at this point that they are losing massive amounts of market share right now. Linux has been picked up by people, and now use is snowballing."
    author: "Dawnrider"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-21
    body: "> Ultimately, in Open Source projects, the best project always wins, because there is no monetary requirement.\n<p>\nActually there is a monetary requirement. We need a memory upgrade for cvs.kde.org and the \n<a href=\"http://www.ec.kingston.com/ecom/configurator/partsinfo.asp?ktcpartno=KTM3123/1024\">\nmemory sticks</a> for that\n<a href=\"http://www-1.ibm.com/support/docview.wss?rs=0&q1=8656-44G&q2=memory&uid=psg1MIGR-45479&loc=en_US&cs=utf-8&cc=us&lang=en\">\nbeast</a> are pretty expensive.\n\n"
    author: "Waldo Bastian"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE get "
    date: 2004-01-21
    body: "&gt; If you too think KDE isn't doing enough to encourage donations, vote here:\n<p>\nBetter vote <a href=\"http://www.kde.org/support/support.php\">here</a> :-)\n"
    author: "Waldo Bastian"
  - subject: "Re: DONATIONS, DONATIONS, DONATONS, WHen will KDE "
    date: 2004-01-22
    body: "Hehe, yeah, there too =)"
    author: "Alex"
  - subject: "2 issues"
    date: 2004-01-20
    body: "1) Xinerama seems broken. Windows are placed random, not where they were left when I last saved my session\n2) kweather seems to be unable to update. How am I supposed to know what the wetaher outside is, if I cannot use kweather .. ;)"
    author: "Gerhard den Hollander"
  - subject: "Re: 2 issues"
    date: 2004-01-21
    body: "Hi,\n\nDoes it update at all? I have not seen this problem at all and I only run CVS HEAD. Please provide more info or file a report at b.k.o."
    author: "Nadeem Hasan"
  - subject: "Re: 2 issues"
    date: 2004-01-21
    body: "I haven't had any trouble with KWeather. Perhaps their settings got messed up in one of the upgrades."
    author: "Ian Monroe"
  - subject: "Compiling KDE w/Prelink"
    date: 2004-01-20
    body: "How do we compile KDE from scratch using the prelink option?\nis there an info or howto on this using Konstruct."
    author: "Jello Biafra"
  - subject: "Re: Compiling KDE w/Prelink"
    date: 2004-01-20
    body: "There is no \"prelink option during compilation\". Just install KDE and run (or let it run at night if cron'ed) prelink on your system."
    author: "Anonymous"
  - subject: "Re: Compiling KDE w/Prelink"
    date: 2004-01-20
    body: "if you use gcc 3.3.x then you can use gcc/ld builtin prelinking support. Just set export LDFLAGS='-z combreloc' before running ./configure\nAlso it's useful to run ./configure with option --enable-fast-malloc=full"
    author: "ah"
  - subject: "Re: Compiling KDE w/Prelink"
    date: 2004-01-20
    body: "combreloc is not prelink (Jelinek) is not objprelink (Bottou)"
    author: "Anonymous"
  - subject: "Re: Compiling KDE w/Prelink"
    date: 2004-01-21
    body: "I find this whole issue of prelink confusing"
    author: "Jello Biafra"
  - subject: "greeaaaaaat"
    date: 2004-01-20
    body: "awsome ... absolutely awsome 8)"
    author: "poison"
  - subject: "Glory"
    date: 2004-01-20
    body: "There seem to be some glorious feeling about this release...\n\nI guess glory shines on the KDE developers."
    author: "KA"
  - subject: "KDE 3.1.95 RPMs for SuSE 9.0 unusable?!?"
    date: 2004-01-20
    body: "1. Kicker crashes when starting it with 3.1.4's config. Yes, I'd expect this to work, or how else would you do it when you administer a multi-user machine, and want to avoid breaking everybody's personal config?\n\n2. K menu comes up almost empty (even when starting from scratch, with a new user)\n\n3. Quicklauncher just shows five generic \"folder\" icons, which say \"Malformed URL\" when you click on them. There's now possibility to customize them into something useful (RightClick->AddApplication leads to a greyed-out No Entries label).\n\n4. Kcontrol (the KDE Control panel, started manually) comes up empty as well.\nOn stdout, it says \"No K menu group with X-KDE-BaseGroup=settings found ! Defaulting to Settings/\"\n\n\n5. No .xsession-errors log file created, to analyze what's going on.\n\n-------\nN.B. KDE 3.1.94 for Redhat (Fedora Core) works all right."
    author: "Alain Knaff"
  - subject: "Re: KDE 3.1.95 RPMs for SuSE 9.0 unusable?!?"
    date: 2004-01-21
    body: "i think you messed up your installation.\n\ndid you install something by force ?"
    author: "chris"
  - subject: "Re: KDE 3.1.95 RPMs for SuSE 9.0 unusable?!?"
    date: 2004-01-21
    body: "Found it.\n\nI was missing the kdebase3-SuSE-9.0-68 and desktop-data-SuSE-8.2.99-46 packages. After installing them, everything works fine.\n\nCuriously enough, abscence of these packages was no problem with KDE 3.1.4 and KDE 3.1.5 ...\n"
    author: "Alain Knaff"
  - subject: "libkdeinit_noatun.so missing"
    date: 2004-01-21
    body: "I was mistaken, actually only desktop-data-SuSE -8.2.99 is needed, not kdebase3-SuSE.\n\nNow, while continuing my install, I ran into a another problem: kdemultimedia needs libkdeinit_noatun.so, but this is nowhere to be found:\n\n# rpm -i kdeaddons3-sound-3.1.95-0.i586.rpm  kdemultimedia3-sound-3.1.95-0.i586.rpm kdemultimedia3-video-3.1.95-0.i586.rpm\nwarning: kdeaddons3-sound-3.1.95-0.i586.rpm: V3 DSA signature: NOKEY, key ID 9c800aca\nerror: Failed dependencies:\n        libkdeinit_noatun.so is needed by kdemultimedia3-video-3.1.95-0\n\nAll other KDE 3.1.95 packages, except these 3 (multimedia-sound, multimedia-video, addons-sound) are installed. Or is libkdeinit_noatun supplied by a \"standard\" SuSE RPM? If so, which one?\n\nThanks\n"
    author: "Alain Knaff"
  - subject: "Re: libkdeinit_noatun.so missing"
    date: 2004-01-21
    body: "Alain,\n\nlibkdeinit_noatun.so is supplied by kdemultimedia as shown below:\n\ngeorge~> rpm -qf /opt/kde3/lib/libkdeinit_noatun.so\nkdemultimedia3-3.1.95-1\n\n\nRegards\n\nGeorge"
    author: "GeorgeMoody"
  - subject: "Re: libkdeinit_noatun.so missing"
    date: 2004-01-21
    body: "Unfortunately, there is no kdemultimedia3-3.1.95-1 in SuSE-9.0's directory.\nSo I just used the 3.1.94 version, and it worked anyways.\nBtw, is it expected that the 3.1.95 RPMs are much smaller than the ones for 3.1.94 ? More space-efficient code, or less goodies?"
    author: "Alain Knaff"
  - subject: "Re: libkdeinit_noatun.so missing"
    date: 2004-01-26
    body: "There seems to be a kdemultimedia3-3.1.95-1 in the other SuSE directories. I think the packager forgot to upload the 9.0 version. Maybe I will try to build just KDEMultimedia from the sources.\n\n\nBTW: What I have is excellent. Well done KDE Developers!  "
    author: "Gareth Sargeant"
  - subject: "Re: libkdeinit_noatun.so missing"
    date: 2004-01-21
    body: "It is possible to install kde-3.2 with apt on SuSE.\nSee; http://linux01.gwdg.de/apt4rpm/kde31to32.html\nThis helps a lot to get dependencies solved."
    author: "Richard"
  - subject: "Re: libkdeinit_noatun.so missing"
    date: 2004-02-01
    body: "hi,\nI've got installed all kdemultimedia 3.1.95 packages from a kde mirror for SuSE 9. But when i start the packet manager of YaST, There is an error: \"You must install libkdeinit_noatun.so\" - I'm a linux newbie - can anybody help me?\n\nbye"
    author: "mj"
  - subject: "Re: KDE 3.1.95 RPMs for SuSE 9.0 unusable?!?"
    date: 2004-02-02
    body: "Do these required files exist for SuSE 8.2 also? I have the same problem with KDE 3.2rc on the SuSE 8.2 box: calling up the start menu causes the menu to be truncated at the right edge and/or to crash the kicker.\n\nWhat can be done?\n"
    author: "Dennis Nigbur"
  - subject: "Drop shadows on desktop icon text???"
    date: 2004-01-21
    body: "Something new since 3.1.4 seems to be drop shadows on the text beneath icons on the desktop.  Unfortunately, I can't find a way to turn it off.  The only thing in the control center that looks close to this is Shadowed Text in the Liquid panel, but that doesn't seem to actually turn it off.  Am I missing something?\n\nIt does seem to start up very quickly compared to 3.1.4."
    author: "Robert Krawitz"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-01-21
    body: "right click desktop->background->advanced options->shadowed text\n\nbtw, the default shadowed text looks quite ugly. :("
    author: "anon"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-01-21
    body: "Thanks.  Wonder how I missed it.\n\nPersonally, I think they've gone a bit heavy on the eye candy (bouncy task bar frobnitzim, which fortunately I did figure out how to turn off)."
    author: "Robert Krawitz"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-01-21
    body: "I think you only get those eye candy effects if you set your processor speed to high in kpersonalizer."
    author: "anon"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-01-21
    body: "The bouncy icon is *much* better than the weird flashy thing from before. Surprisingly, I quite like it myself and so do many people. The desktop shadow text could have been *so* much better though...\n\nCouldn't find the desktop shadow config either.  \"Background\" --> \"Appearance & Background\" would be better in the config app.  "
    author: "Navindra Umanee"
  - subject: "WHAT DO YOU MEAN!"
    date: 2004-01-22
    body: "I love the bouncy thingy! And IT'S NOT TOO LATE TO CHANGE THE CRAPPY SHADOW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    author: "Alex"
  - subject: "Re: WHAT DO YOU MEAN!"
    date: 2004-01-22
    body: "me thinks he likes it :)\n\nTony"
    author: "Tony Espley"
  - subject: "Re: WHAT DO YOU MEAN!"
    date: 2007-01-21
    body: "and how do you hoon?"
    author: "hoonweird"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-01-23
    body: "> Wonder how I missed it.\n\nNot hard to figure out -- it is in a very illogical place.\n\nSomebody please call the usability people! :-)\n\nFirst question, why is: \"Advanced Options\" a button rather than a tab?\n\nSecond question, why is: \"Background Icon Text\" called: \"Background\" when it is actually: \"DeskTop\"?\n\nThird question, why is that option in that obscure location?\n\nWhere to put this?  Since it is an option for: \"Color\", I would add a tab to the \"Color\" KCM -- have two tabs: \"Window\" & \"Miscellaneous\".\n\nPerhaps it could also be made clear how to set the color for the shadow.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-07-23
    body: "> Second question, why is: \"Background Icon Text\" called: \"Background\" when it is actually: \"DeskTop\"?\n\nHa! Did you ever give any thought to why people stick wallpaper to their desktops? So quit nagging on inconsistent terminology!"
    author: "Diederick de Vries"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-07-23
    body: "I was referring to what it is called in the Control Center.\n\nWall paper *is* background.  But text is not.\n\nMy comments are not based on my opinions but on the comments of users and questions on the support lists: 'kde' & 'kde-linux'.\n\nThis magic button to set this is a dumb idea, and it is in the wrong KCM.  Actually, all of these magic buttons are a very dumb idea.  The reason that they are a very dumb idea is that they are inconsistent.  The user interface should be as consistent as possible.  The reasons for this can be found in any basic (1st year college) physiological psychology text.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-05-18
    body: "I wrote a dumb tutorial on changing that default (fugly!) desktop dropshadow. Get it here:\n\nhttp://datiku.com/documents/kdesktopshadows.txt"
    author: "Abby Ricart"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2004-09-26
    body: "cool, thanks!\n:)"
    author: "mononoke"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2005-04-19
    body: "isn't the file empty?"
    author: "t|zz"
  - subject: "Re: Drop shadows on desktop icon text???"
    date: 2007-11-16
    body: "Windows XP \n\nOpen System Properties in Control Panel, choose the Advanced tab, and click Settings in the Performance section. Turn on the Use drop shadows for icon labels on the desktop option, and click Ok. \nYou can also try MicroAngelo (v5.5 or later), which allows you to change the text color, configure custom icons, and change other aspects of Windows. \nAnother utility of this type is WindowFX."
    author: "surya"
  - subject: "File manager"
    date: 2004-01-21
    body: "I dont know if this is the right place to put this question, but will ever be a profession file manager in kde like Total ( old Windows ) commander? Smth like that would be greate for freaks. There are a few commanders like Krusader, but nothing is so advanced ad TCmd.\n\nMidnight commander is good, but ... eh ... and konqueror is not very usefull when you have to manage a huge hdd.\n\nThanks.\nSorry for bothering you.\n"
    author: "anonymous"
  - subject: "Re: File manager"
    date: 2004-01-21
    body: "Hi,\n\nplease tell us a bit more exactly, where you see deficiencies in konqueror.\nWhat's missing, what can be done better ?\nYou can also email me directly, also in german: neundorf@kde.org\n\nWould be nice if I would hear from you.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: File manager"
    date: 2004-01-21
    body: "You can see, for example, why Xandros rolled their own kparts-based file manager rather than using Konqueror:\n\nhttp://consultingtimes.com/articles/xandros/filemanager/filemanager.html"
    author: "anon"
  - subject: "Re: File manager"
    date: 2004-02-03
    body: "Yes nice, but there are 2 types of file managers ... the one like krusader, XFM, windows commander, and the 2 tabed ones like krusader, mc, totalcommander.\n\nYou cant compare them, i like the 2 tabes ones :)\n\nregards."
    author: "anonymous"
  - subject: "Re: File manager"
    date: 2004-01-24
    body: "for me the performance has to be improved, improved extremely ... \n\nbut from the number of features I am totaly satiesfied."
    author: "luca"
  - subject: "Re: File manager"
    date: 2004-01-26
    body: "Well, I run KDE 3.2_rc1 on my Gentoo G4 (1ghz) ppc system and I have to say that Konqueror simply rocks. It's much faster than I ever thought it could be, and has all the features I could wish for and then some. I love gnome, but KDE 3.2 enables me to do things faster and more efficiently (Kate vs Gedit for example). I'm not trying to flame gnome, but kde is a few steps ahead as of right now. Kudos to the kde development team."
    author: "Christan Kuzmanic"
  - subject: "Re: File manager"
    date: 2004-02-03
    body: "Sorry, for long absence,\n\nWell in krusader you dont have that supr speed from total commander.\nIt takes a lot when you enter in a folder with many files, take your chance in /dev/\n\nYou can see it flipping whyle you move the cursor.\n\nJust try TC. i can case you ever touch a M$ windows :D\n\nregards."
    author: "anonymous"
  - subject: "Re: File manager"
    date: 2004-10-27
    body: "Konqueror WISH-LIST (you asked for it!):\n\npermissions-changing feedback would be nice if there is a reason\nit fails (& there must be!).  Cascaded changes through subtrees is\ngood when it works, but doesn't always (and sometimes does but the\nview is not updated to reflect that).\n\nDesktop-icon click should always start a new instance (even if\nthe global setting is single-window) ... to facilitate occasional\nseparate-window drag/drop (as for nautilus).\n\nI need to change desktops to use nautilus for some file deletes and\noverwrites that Konqueror just refuses to do (seems to be crippled\nto only working within a user's home-tree, despite group permissions\nthat allow otherwise).\n\nHows that for starters!\n\nCheers"
    author: "Sean"
  - subject: "Re: File manager"
    date: 2004-01-31
    body: "btw\n\nIf you are looking for a Total Commander-like project on KDE, have a look at Krusader (http://krusader.sourceforge.net). It is a dual pane file-manager with most of the features present in TC and a very nifty viewer. \n"
    author: "JW"
  - subject: "TKCAethera"
    date: 2004-01-21
    body: "I was browsing around the theKompany's website and I came across a free little multiplatform gem called Aethera.\n\nIt is functional, clean and attractive. (http://www.thekompany.com/projects/aethera/screenshots.php3)\n\nIt also looks a lot like Kontact, and has just about all the functions I need. Is it based on Kontact code, is it using KDE parts and is it open source? Finally, how is Kontact BETTER, or WORSE than Aethera?\n\nThank you!"
    author: "Alex"
  - subject: "Re: TKCAethera"
    date: 2004-01-21
    body: "Aethera predates the whole Kontact thing, it's been around for a good few years now (SuSE distributes it, don't know about anyone else). It doesn't use parts as far as I know, but I think it might use the KDE address book. It's not quite as good as Evolution, but it's a good Outlook replacement if you've got Gnome issues."
    author: "Bryan Feeney"
  - subject: "SVG support"
    date: 2004-01-21
    body: "The new release is VERY sharp.  I've been using it for a day on a SuSE 9 desktop, and it has performed very well so far.  However I'm trying to get SVG support to happen, and can't figure out if there's a knob I need to twist or something.\n\nI've gone so far as setting the plugin search path to include /opt/kde3/lib, etc, but that just causes the plugin search to fail.  I'm trying to show off here :-)  Any help?\n\nThanks for another great release!"
    author: "xrayspx"
  - subject: "Re: SVG support"
    date: 2004-01-21
    body: "Can't comment on ksvg as it seems to be missing from the Slackware binaries I'm using, but if you want a plugin, the latest Linux build of Adobe's SVG Viewer now works well under Konqueror. [You wait for ages, then 2 come along at once.]"
    author: "Peter Robins"
  - subject: "Re: SVG support"
    date: 2004-01-23
    body: "KSVG is a KPart, not a Netscape type Plugin.\n\nSo, just right click on the icon for any SVG file and select the KSVGPlugin in the: \"Embedding\" tab.  If you can find it -- since it is now well hidden in order to make KDE easier to use.\n\nAn OT note: the Adobe SVG Mozilla plugin also works with Konqueor-3.2.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: SVG support"
    date: 2004-01-26
    body: "That seems like the answer, seems to be embedding correctly now.  I'm unsure which plugin it's using (ksvg or Adobe), but it seems to work somewhat better.\n\nThanks for both of your help."
    author: "xrayspx"
  - subject: "kopete"
    date: 2004-01-21
    body: "the new default kopete icons are awesome, thanks!\nhttp://dndfreaks.xs4all.nl/shared/sorted/pictures/linux/kopete_icon_test.png"
    author: "Mark Hannessen"
  - subject: "konstruct & FreeBSD 5.1"
    date: 2004-01-22
    body: "I can't install KDE-3.2 RC1 via konscruct\nI got message like this:\n\"../../gar.conf.mk\", line 66: Need an operator\n\"../../gar.conf.mk\", line 74: Need an operator\n\"../../gar.conf.mk\", line 82: Need an operator\n\"../../gar.conf.mk\", line 107: Need an operator\n\"../../gar.conf.mk\", line 108: Need an operator\n\"../../gar.conf.mk\", line 109: Need an operator\n\"../../gar.conf.mk\", line 110: Need an operator\n\"../../gar.conf.mk\", line 111: Need an operator\n\"../../gar.conf.mk\", line 112: Need an operator\n\"../../gar.lib.mk\", line 272: Need an operator\n\"../../gar.lib.mk\", line 274: Need an operator\n\"../../gar.lib.mk\", line 277: Need an operator\n\"../../gar.mk\", line 64: Need an operator\n\"../../gar.mk\", line 66: Need an operator\n\"../../gar.mk\", line 68: Need an operator\nError expanding embedded variable.\n\nWhat i do wrong?\nPlease help me :)"
    author: "Beton"
  - subject: "Re: konstruct & FreeBSD 5.1"
    date: 2004-01-25
    body: "Do you use make instead of gmake?"
    author: "Yevgen Muntyan"
  - subject: "Konqui rendering problem"
    date: 2004-01-22
    body: "Hi, i've found some rendering problem in konqueror.\nTry to open this site http://www.culm.org\nand try to open the same url with mozilla..."
    author: "Psyk[o]"
  - subject: "Re: Konqui rendering problem"
    date: 2004-01-22
    body: "Read http://konqueror.org/investigatebug/, create a test-case and report it to http://bugs.kde.org"
    author: "Anonymous"
  - subject: "MacOS bar"
    date: 2004-01-22
    body: "I know a lot of people don't like this setting, however, in 3.2-rc1, I can't find the setting for placing the menu bar at the top of the screen like in MacOS. In prior releases of KDE, you can find this setting under style and then selecting the misc tab. Where has this setting moved to? Aside from this annoyance, KDE-3.2-rc1 is _great_. I'm using it in FreeBSD 5.2-RELEASE and in SuSe 9.0.\n\nI also have another bug, when I right click an icon, the colors become distorted. I have the same problem when I turn on menu transparency (regardless of the transparency effect I choose). I am using ATI's linux drivers for Xfree 4.3.0 for a radeon 9800. I don't have any of this trouble in FBSD and I'm currently using a generic vesa driver and I built rc1 for FreeBSD manually. Thanks.\n\nFortunately I don't like to use menu transparency, I decided to see if there any changes with it."
    author: "Hiryu"
  - subject: "Re: MacOS bar"
    date: 2004-01-22
    body: "I guess I'm not among those \"a lot of people\" who don't prefer a global menu bar instead one in every single application. =)\n\nThe global menu bar is now a Kicker applet so for creating a menu bar at the top of the screen you do following:\nRight click on Kicker > Add > Panel > Child Panel and move it to the top\nRight click on this new child panel > Add > Applet > Menu\nAdd anything else to the panel, resize it etc.\n\nFor making the menu applet actually show a menu you'll need to tweak another option:\nRight click on the desktop > Configure Desktop... > Behavior > Menu Bar at Top of Screen (should be renamed to \"Global Menu Bar\" now imo) and choose \"Desktop menu bar\" or \"Current application's menu bar\" (the currently available option \"None\" is irritating in this context, I know)\n\nReady. Cheers. =)"
    author: "Datschge"
  - subject: "Re: MacOS bar"
    date: 2004-01-23
    body: "How did you figure all that out? That's a lot more complex than it should be.\n\nThanks!"
    author: "Hiryu"
  - subject: "interesting KDE 3.2 RC1 compile error"
    date: 2004-01-23
    body: "Hallo, I have successfully compiled 3.1.5 & 3.2beta2. However, on the same machine in trying to build 3.2RC1 via konstruct I got a interesting error(which results in a build halt including the rest of the packages not already compiled) during the compile of kdeartwork. \"libtool: link: '/usr/lib/libGL.la' is not a valid libtool archive.\" Viewing that file it notes, \"Please DO NOT delete this file! It is necessary for linking the library. This file was generated by the nvidia-installer: 1.0.5.\" Trying to build the package by itself results in the same error. If anyone could point me in the right direction I would greatly appreciate it.\n\n-klug"
    author: "klug"
  - subject: "Re: interesting KDE 3.2 RC1 compile error"
    date: 2004-01-23
    body: "I ran into this too.  I found the answer here:\n\nhttp://distro.ibiblio.org/pub/Linux/distributions/suse/suse/i386/supplementary/X/XFree86/nvidia-installer-HOWTO\n\nBasically, libtool searches for a particular pattern in .la files and gives up if it doesn't find it.  Replace the line:\n \n# Generated by nvidia-installer: 1.0.5\n \nwith\n\n# Generated by nvidia-installer: 1.0.5 (for use by libtool)\n\n"
    author: "Shane Hathaway"
  - subject: "Re: interesting KDE 3.2 RC1 compile error"
    date: 2004-01-23
    body: "Ah, thank you very much! That edit worked perfectly.\n\n-klug"
    author: "klug"
  - subject: "where is kmail"
    date: 2004-01-23
    body: "I installed the suse rpms, but kmail is missing. Then I compiled and installed the knetwork-3.1.95 source package myself, but still no kmail. In which package is this supposed to be? \nKmail from 3.1.4 won't start cause it can't find libkdenetwork.so.2(.0.0).\n\nThanks for any help in advance."
    author: "W. Dobbe"
  - subject: "Re: where is kmail"
    date: 2004-01-23
    body: "You must install the SuSE rpm kdepim3, see below\n\ngeorge@georgelaptop1:~> rpm -qf /opt/kde3/bin/kmail\nkdepim3-3.1.95-1\ngeorge@georgelaptop1:~>\n\n"
    author: "GeorgeMoody"
  - subject: "Yet again, JuK is missing"
    date: 2004-01-23
    body: "The only way I seem to be able to get JuK any more is to download the kdemultimedia CVS and compile from source - the .tar.gz and various rpm packages don't have it any more.  Actaully, the only refs to JuK in the kde-redhat packages are:\nwarning: kdemultimedia-3.1.95-0.fdr.0.1.i386.rpm: V3 DSA signature: NOKEY, key ID ff6382fa\n/usr/share/doc/HTML/en/juk\n/usr/share/doc/HTML/en/juk/common\n/usr/share/doc/HTML/en/juk/index.cache.bz2\n/usr/share/doc/HTML/en/juk/index.docbook\n/usr/share/doc/HTML/en/juk/toolbar.png\n\nNow, I don't mind compiling JuK, but I thought it was part of KDE now..."
    author: "Duncan"
  - subject: "Re: Yet again, JuK is missing"
    date: 2004-01-23
    body: "JuK is in the kdemultimedia tarball so complain to your distributor/packager."
    author: "Anonymous"
  - subject: "Re: Yet again, JuK is missing"
    date: 2004-01-28
    body: "Last night, I was rebuilding the Fedora SRPM of kdemultimedia (to add support for ALSA and mp3s), and I noticed that configuration disables JuK if \"taglib\" is not installed. Since Fedora doesn't package taglib (actually, I couldn't find an RPM anywhere, had to install the tarball), JuK won't be built as part of kdemultimedia, even though its source is there.\n\nProbably an issue to raise with Redhat?"
    author: "MEF"
  - subject: "Re: Yet again, JuK is missing"
    date: 2004-05-25
    body: "Hi,\nyou can find fedora's taglib RPM at KDE's download servers.\nit became a habit for me, for each new KDE release I download the SRPM and the src, modify the spec file to build mpeglib (and for 3.2.2 I set juk on too)\n\ngood luck"
    author: "Zeeyed"
  - subject: "COOL!"
    date: 2004-01-24
    body: "VERY COOL!!!!\n\u00cb\u00e8\u00ed\u00f3\u00ea\u00f1\u00ee\u00e8\u00e4\u00fb \u00e2\u00f1\u00e5\u00f5 \u00f1\u00f2\u00f0\u00e0\u00ed, \u00ee\u00e1\u00fa\u00e5\u00e4\u00e8\u00ed\u00ff\u00e9\u00f2\u00e5\u00f1\u00fc"
    author: "k@LIy"
  - subject: "Umount problem"
    date: 2004-01-24
    body: "I am still have the problem I reported already here: http://dot.kde.org/1070894366/1071609698/. Nothing changes from previous beta release. It exists on 3 different computers with RedHat9, ASPLinux9 and Fedora Core 1. And I wonder why nobody else reports about this problem? "
    author: "Alexander Malashenko"
  - subject: "KPM - Processmanager"
    date: 2004-01-24
    body: "What is with KPM? Why it is not actual KDE releases? It was vary nice tool. Is here a replacement for this tool?"
    author: "sd"
  - subject: "Re: KPM - Processmanager"
    date: 2004-01-24
    body: "it used to be in kde releases.. it was depreciated in favor of the more capable ksystemguard, which is included with kde."
    author: "anon"
  - subject: "Re: KPM - Processmanager"
    date: 2004-01-31
    body: "thanks"
    author: "sd"
  - subject: "Having problem with ligGL.so"
    date: 2004-01-25
    body: "I'm using Nvidia driver 1.0-5328-pkg1.\nI have fix libGL.a(add (for use by libtool) ), but I tried to compile kdearts,kdebase and I get:\n/usr/lib/tls/libGLcore.so.1: undefined reference to `_nv000026gl'\n/usr/lib/libGL.so: undefined reference to `_nv000030gl@LIBGLCORE'\n/usr/lib/libGL.so: undefined reference to `_nv000028gl@LIBGLCORE'\n/usr/lib/libGL.so: undefined reference to `_nv000040gl@LIBGLCORE'\n/usr/lib/libGL.so: undefined reference to `_nv000031gl@LIBGLCORE'\n/usr/lib/libGL.so: undefined reference to `_nv000027gl@LIBGLCORE'\ncollect2: ld returned 1 exit status\nmake[3]: *** [kswarm.kss] Error 1\n\nCan you help me?"
    author: "Iskren Stoynov"
  - subject: "Re: Having problem with ligGL.so"
    date: 2004-01-26
    body: "You need to link arts against the xfree glx, not the one provided by nvidia.\n\nIn gentoo you would type\n# opengl-update xfree\n\nOutside of gentoo your mileage will vary. Consider reinstalling xfree and running ldconfig.\n\nThen reload your nvidia driver after your compile."
    author: "Stephen Fairchild"
  - subject: "Re: Having problem with ligGL.so"
    date: 2004-01-27
    body: "I'm using Fedora core 1\nI reinstalled  xfree and now I ger this:\n/home/iskren/qt-x11-free-3.2.3/lib/libqt-mt.so: undefined reference to `glXGetConfig@LIBGL'\n/home/iskren/qt-x11-free-3.2.3/lib/libqt-mt.so: undefined reference to `glOrtho@LIBGL'\n/home/iskren/qt-x11-free-3.2.3/lib/libqt-mt.so: undefined reference to `glXUseXFont@LIBGL'\ncollect2: ld returned 1 exit status\nmake[3]: *** [kappfinder_install] Error 1Untitled 1\nmake[3]: Leaving directory `/home/iskren/tmp_kde/kdebase-3.1.95/kappfinder'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/home/iskren/tmp_kde/kdebase-3.1.95/kappfinder'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/home/iskren/tmp_kde/kdebase-3.1.95'\nmake: *** [all] Error 2\n"
    author: "Iskren Stoyanov"
  - subject: "What about Debian packges?"
    date: 2004-01-25
    body: "I\u00b4ve missed Debian packages yet!\n\nDo you know if there are any yet?\nIf not, could someone do anything about that?!!!\nthanx"
    author: "gAlDiR"
  - subject: "Re: What about Debian packges?"
    date: 2004-01-26
    body: "you can look at wiki.debian.net on the DebianKDE section. You'll find some info about debian packages."
    author: "Swissnux"
  - subject: "Can\u0092t share you excitement "
    date: 2004-01-27
    body: "Konsrtuct chokes on uic compiling qmake_image_collection.cpp with core dump on RHEL 3. Too bad.\n\nHere is what I see:\n...\nQThreadStorage: thread bf1cc8a0 exited after QThreadStorage destroyed\nmake[8]: *** [qmake_image_collection.cpp] Segmentation fault (core dumped)\nmake[8]: *** Deleting file `qmake_image_collection.cpp'\n...\n\nSergey Kosenko"
    author: "Sergey Kosenko"
  - subject: "Re: Can\u0092t share you excitement "
    date: 2004-08-17
    body: "Really you should be a bit more upbeat about things."
    author: "Bill Eidson"
  - subject: "filemanager and choose filter pop up window"
    date: 2004-01-27
    body: "can somebody explain what is this pop up window good for? Whenever I open konqueror I get several of them. I want to disable it. Anybody has this problem too?"
    author: "ph"
  - subject: "As an almost off-topic"
    date: 2004-01-28
    body: "I had some friends over the other night with their two children, a lively daughter about 4 and a somber son about 5 or 6 years old.  We were looking something up on my computer with the children watching and the daughter piped up with \"the computer is pretty\" (referring to the screen).  The son then said \"All the windows are much better looking than the ones at home\".  They use XP at home.\n\nFWIW, KDE seems to appeal to the young demographic.  :)\n\nAlso, I am helping one of them start a balloon arrangement business, and many of the documents are being produced in KWord.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "breathtaking ..."
    date: 2004-01-28
    body: "dunno what to say....grabbed 3.1.95 as soon as it was out...\nit is pure beauty...genius...can't find words sufficient enough to honor it =)\nI love you guys...all of you who contributed to this outstanding project...\nthank you, each and every one of you for providing me such a great desktop envoironment =D\nKDE now is my second love straight after bash ^^"
    author: "poison"
  - subject: "Error in the README file"
    date: 2004-01-29
    body: "Hi everybody,\n\nI have Suse 9.0 and I used konstructor to download and install kde3.2-rc in my laptop. Once I compiled all the sources I was not able to start the kde because I followed the instructions from the README file and I export KDEDIRS=~/kde... when the correct variable is KDEDIR. Once I put the correct variable, all runs ok, and quickly!!!\n\nBest regards,\n\nDaniel"
    author: "Daniel Alomar i Claramonte"
  - subject: "Re: Error in the README file"
    date: 2004-01-29
    body: "It may be that a script added by your distribution uses KDEDIR, KDE itself should run fine with only KDEDIRS set."
    author: "Anonymous"
  - subject: "this version is really great!!!"
    date: 2004-01-29
    body: "I just have upgraded form 3.1.94 to 3.1.95 and I love this version!!! It's fast, stable and beautifull (oh, that tiny bumping cursor as application's start feedback)!!!\nThis is really a great job!!! Congratulations and a big thank you!!!\nPawel"
    author: "pawel"
  - subject: "Qt designer is operative?"
    date: 2004-01-30
    body: "Hi,\n\nWith my Suse 9.0 and kde3.2-rc I can't use the Qt designer tool. When I try to launch it, it fails and shows me \"segment violation\". \n\nkde3.2-rc is installed on /home/daniel/kde3.2-rc and although I had Qt 3.2 I had to download and compile the Qt 3.2 from konstruct. \n\nThe only thing that I found that doesn't work is the Qt \"designer\".\n\nAny idea?"
    author: "Daniel Alomar i Claramonte"
  - subject: "Re: Qt designer is operative?"
    date: 2004-01-31
    body: "It depends on the style: http://bugs.kde.org/show_bug.cgi?id=73843"
    author: "Anonymous"
  - subject: "Re: Qt designer is operative?"
    date: 2004-02-02
    body: "Sorry, but I change the style and I continue having the segment violation....\n\nMaybe something else than the style??"
    author: "Daniel Alomar i Claramonte"
  - subject: "Re: Qt designer is operative?"
    date: 2004-03-25
    body: "I had the same Problem with KDE3.2.1\nUsing gdb I found out this happens while loading the plugins.\n\nAfter deleting all plugins in my \n/opt/kde3.2.1./plugins/designer\nthe designer started.\n( This is no real solution )\n\nBut after many gdb traces I found out that the \nDesigner crashes in KLocales::isLanguageInstalled..\n\nThe Solution for me was to install all Languages with konstruct.\n( Initially I only did this for \"de\" )\n\nnow The Designer works with all plugins\n\nHope this Helps ;-)\n\n\n"
    author: "Christian Storm"
  - subject: "Cool  --  Great job"
    date: 2004-01-30
    body: "I have to say that.\nYou made really <b>great job</b>.\nNow its possible to use Linux on Desktop.\n"
    author: "Marcin Pisarski"
  - subject: "kmail still no filtering back to imap"
    date: 2004-01-30
    body: "It appears that kmail 1.6 is still not capable of filtering mail back to imap mailboxes. This is seriously dissappointing. I understand that a lot of you feel that this should be done on the server, and I don't disagree, however the main email server on campus, and many others across the net do not support any form of server side filters.(sigh) back to the lizard..."
    author: "Garett Shulman"
  - subject: "Re: kmail still no filtering back to imap"
    date: 2004-01-31
    body: "You mean http://bugs.kde.org/show_bug.cgi?id=50997? Seems that it may be contained in the kdepim 3.3 release."
    author: "Anonymous"
  - subject: "themer"
    date: 2004-01-31
    body: "I kdebase rpm requires themer>=1.40 where I can find themer 1,40???"
    author: "Iskren Stoynov"
  - subject: "Re: themer"
    date: 2004-01-31
    body: "What distribution? Ask your distributor."
    author: "Anonymous"
  - subject: "Re: themer"
    date: 2004-01-31
    body: "Fedora Core 1"
    author: "Iskren Stoynov"
  - subject: "Re: themer"
    date: 2004-01-31
    body: "I have installed arts-1.1.94,kdelibs-3.1.94,kdebase-3.1.94,kdebindings-3.1.94\nbut now I get:\n\nstartkde: Starting up...\nksplash: relocation error: /usr/lib/libkdeui.so.4: undefined symbol: _ZN9QGroupBox10setEnabledEb\nkdeinit: relocation error: /usr/lib/libDCOP.so.4: undefined symbol: _ZN6QGList5eraseEP6QLNode\nstartkde: Could not start kdeinit. Check your installation.\nWarning: connect() failed: : No such file or directory\nksmserver: relocation error: /usr/lib/libkdeui.so.4: undefined symbol: _ZN9QGroupBox10setEnabledEb\nstartkde: Shutting down...\nWarning: connect() failed: : No such file or directory\nError: Can't contact kdeinit!\nstartkde: Running shutdown scripts...\nstartkde: Done.\n"
    author: "Iskren Stoynov"
  - subject: "startkde: Could not start kdeinit."
    date: 2004-01-31
    body: "startkde: Starting up...\nksplash: relocation error: /usr/lib/libkdeui.so.4: undefined symbol: _ZN9QGroupBox10setEnabledEb\nkdeinit: relocation error: /usr/lib/libDCOP.so.4: undefined symbol: _ZN6QGList5eraseEP6QLNode\nstartkde: Could not start kdeinit. Check your installation.\nWarning: connect() failed: : No such file or directory\nksmserver: relocation error: /usr/lib/libkdeui.so.4: undefined symbol: _ZN9QGroupBox10setEnabledEb\nstartkde: Shutting down...\nWarning: connect() failed: : No such file or directory\nError: Can't contact kdeinit!\nstartkde: Running shutdown scripts...\nstartkde: Done.\n"
    author: "Iskren Stoynov"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-02-01
    body: "could you give more info about your environment?\n\nI had a very similar problem with my gentoo box where i had a complete cvs install (from iamlarryboy's builds) and a partial 3.2_rc1 install (emerged), and I got the errors you described when some apps tried to use /usr/kde/cvs/lib/* instead of /usr/kde/3.2/lib/*, despite me having all the environment variables set correctly :/\n\nhowever, doing a \"chmod 000 /usr/kde/cvs\" forced the use of the 3.2 libs, and everything's been fine since."
    author: "another ac"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-02-01
    body: "I'm using Fedora Core 1.I installed KDE from rpm's built for Fedora."
    author: "Iskren Stoyanov"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-10
    body: "I have the same problem!!\nCouldn't start KDE.\n\nThe message is:\nksplash: relocation error: /usr/lib/libkdeui.so.4: undefined symbol: _ZN9QGroupBox10setEnabledEb\nkdeinit: relocation error: /usr/lib/libDCOP.so.4: undefined symbol: _ZN6QGList5eraseEP6QLNode\nstartkde: Could not start kdeinit. Check your installation.\n\nI'm using RH 9.0 and update KDE from rpm's\n\nSomeone knows howto fix it?"
    author: "vaLar"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-22
    body: "Same Problem:\n\nInstalled RPM's for KDE 3.2.2 Fedora Core 1.\n\nNow KDE won't run.\n\nAny ideas?"
    author: "LB"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-22
    body: "I have the same problem.  Downloaded rpms from kde.org for Fedora (KDE 3.2.2).  Installed them using \n\nrpm -Fvh *.rpm\n\nEverything went fine, but I get the kdeinit error \n\nksplash relocation error /usr/lib/libkdeui.so.4"
    author: "Ken Y."
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-27
    body: "yeah erm iam getting the same problem with slackware 9.1 \nkdeinit: relocation error: /opt/kde/lib/libkdeui.so.4: undefined symbol: _ZN9QComboBox4hideEv\n"
    author: "Shadow"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-27
    body: "do the following commands(change paths):\n QTDIR=/usr/lib/qt-3.3\n LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH\n export QTDIR LD_LIBRARY_PATH\n"
    author: "Iskren Stoyanov"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-23
    body: "Well,\n\nI went into the 3.2.1 directory and noticed that qt 3.3.1 was included in that build, but it wasn't included in 3.2.2.  My guess is that if you didn't install 3.2.1 first, you would have these problems.  I am going to download the qt and install them with the kde packages on a fresh install of fedora.  I will let you all know what I find out.  If it works, then i know what my problem was.  In my case I was upgrading 3.1 to 3.2.2 and my qt was probably out of date as 3.2 requires at least qt 3.2.3.\n\nKen"
    author: "Ken Y."
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-04-23
    body: "All,\n\nThat worked.  You need to get the three qt rpms from 3.2.1 and install them first before upgrading to kde 3.2.2.  You may be able to fix your problem by just installing those three packages using the following command.\n\nsudo rpm -Uvh qt*.rpm --force --nodeps\n\nThen install the kde rpms\n\nKen"
    author: "Ken Y."
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-05-13
    body: "Ken,\nI did exactly what you said and it straightened right out.  Thanks a million!\nKarl  "
    author: "Karl"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2004-05-13
    body: "Karl,\n\nGlad it worked for you.  I notified KDE, but they didn't seem to care much.  Take care.\n\nKen"
    author: "Ken Y."
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2005-03-03
    body: "This is very old by now but I need to give my contribution... \nThere are new dependencies in the KDE directory on linuxpackages... there are 2 new packs... they are libidn-0.5.12-i486-1pcx.tgz and libmusicbrainz-2.1.1-i486-1pcx ... \nThey solve everything :D \nGood luck to all you guys. "
    author: "Est\u00eav\u00e3o Soares"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2005-11-02
    body: " hey!\nI've installed Slackware 10.2 on my notebook and I've chosen KDE 3.4.1 to install. Everything was great until the moment when I typed \"startx\" into the console.\nKDE schowed the window with information\ncode:\n\nxsetroot: unable to open display ''\nkde-config: error while loading shared libraries: libidn.so.11: cannot open shared object file: No such file or directory\n\n(...)\nkreadconfig:  error while loading shared libraries: libidn.so.11: cannot open shared object file: No such file or directory\nkreadconfig:  error while loading shared libraries: libidn.so.11: cannot open shared object file: No such file or directory\nksplash: error while loading shared libraries: libidn.so.11: cannot open shared object file: No such file or directory\nkdeinit:  error while loading shared libraries: libidn.so.11: cannot open shared object file: No such file or directory\n\n\n(...)\n\nWarring: connect() failed: No such file or directory\nError: Can't find kdeinit!\n/opt/kde/bin/startkde: line 251: artsshell: command not found\nstartkde: Running shutdown scripts...\nstartkde: Done. \n\n\nAnd then KDE shuted down. And I don't know what to do?;/ Is there anybody who can help me?... "
    author: "coyot"
  - subject: "Re: startkde: Could not start kdeinit."
    date: 2006-06-08
    body: "\nyep i had the same problem pretty annoying! i've installed the recommended full install of slackware 10.2 and KDE is missing stuff plus it complains that i have glibc-2.3.2 and not 2.3.4\nfirst i've downloaded libidn-0.6.3 compiled and installed it, that didnt help so much though..\nso i've compiled and installed kdelibs-3.5.3 package it took like five hours but it solved it..\nuff!\n\nwhat can i say KDE is worth the effort :) \ni love the fuzzy klock: quarter to two\n\nhave fun! "
    author: "toudi"
  - subject: "Where is 3.2?"
    date: 2004-02-01
    body: "In the download mirrors I'm only finding 3.1.x versions...where is 3.2?"
    author: "Sean O'Dell"
  - subject: "Re: Where is 3.2?"
    date: 2004-02-01
    body: "Read the story, it talks about a release candidate and has the links you asks for."
    author: "Anonymous"
  - subject: "Re: Where is 3.2?"
    date: 2004-02-01
    body: "The story above?  I did read it and click those links.  I found mirrors that hosted files named kde*.3.1.95 and so on.  I must be confused on something...3.2 is out in RC1 form, right?  Do not all the mirrors have the 3.2 release perhaps?  Are they up in a parent directory somewhere?  I couldn't find any kde*3.2-type files anywhere, and I spent a goodly amount of time browsing around."
    author: "Sean O'Dell"
  - subject: "Re: Where is 3.2?"
    date: 2004-02-01
    body: "KDE 3.1.95 is the release candidate. That's the reason why all \"KDE 3.2 RC\" links point to the 3.1.95 directory."
    author: "Anonymous"
  - subject: "Re: Where is 3.2?"
    date: 2004-02-01
    body: "Okay...\n\nWouldn't 3.2-rc1 have been a better name for the 3.2 RC1 release?\n\n\"All the KDE 3.2 RC links?\"  What other links?"
    author: "Sean O'Dell"
  - subject: "german language"
    date: 2004-02-01
    body: "hello,\nis for the kde 3.2rc1 a German language package avaible?\n\nbye "
    author: "mj"
  - subject: "Re: german language"
    date: 2004-02-01
    body: "Yes. But why not just wait two days until 3.2?"
    author: "Anonymous"
  - subject: "Re: german language"
    date: 2004-02-01
    body: "hmm, yes, ok ill wait then for the release - thanks :-)\n\ncu"
    author: "mj"
  - subject: "Kate ..."
    date: 2004-02-03
    body: "The best editor for linux, maybe only quanta is better but both have some problems\n\nquanta needs a lot of time to jump form a file to another ( tab ), and sometimes when you have more file ( like 20-30 ) its happening the scroll buttons <-- --> to get misteriously deactivated.\n\nkate ... any chance to be there an autocomple code or smth like this ? very usefull for lazy ppl :)\n\nregards."
    author: "anonymous"
---
After over a year of development we're ready to announce the release of the first (and hopefully last) release candidate for KDE 3.2.0. Get it from <a href="http://download.kde.org/unstable/3.1.95/src/">download.kde.org</a> or use <a href="http://developer.kde.org/build/konstruct/unstable/">Konstruct</a> if you don't feel like calling configure by yourself. Due to the time constraints, don't expect distribution binaries, but they may pop up at <a href="http://download.kde.org/unstable/3.1.95/">download.kde.org</a> URL too.




<!--break-->
