---
title: "KDE CVS-Digest for December 3, 2004"
date:    2004-12-04
authors:
  - "dkite"
slug:    kde-cvs-digest-december-3-2004
comments:
  - subject: "Am I the first one?"
    date: 2004-12-04
    body: "Big Thanks!"
    author: "Albert"
  - subject: "Re: Am I the first one?"
    date: 2004-12-04
    body: "First one what?  Am I missing something, or did you forget to include context?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Am I the first one?"
    date: 2004-12-04
    body: "First one who posts a comment?"
    author: "Niek"
  - subject: "Re: Am I the first one?"
    date: 2004-12-04
    body: "Ah. That's only impressive if it's a large site full of idiots.  I'd suggest Slashdot.  If it makes you feel warm and fuzzy regardless, set up a guestbook on your local system and keep making first posts, deleting them and redoing it.\n\nPosts can go days here without comments.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Am I the first one?"
    date: 2004-12-05
    body: ">Posts can go days here without comments. \n\nnow that's a lie :p"
    author: "Pat"
  - subject: "Re: Am I the first one?"
    date: 2004-12-05
    body: "If you think about it, there must be always at least a last post unanswered, think of memory constraints or maybe the site's app constraints =P"
    author: "c0p0n"
  - subject: "Re: Am I the first one?"
    date: 2004-12-05
    body: "who the hell cares?"
    author: "Ass"
  - subject: "Oh no! The goat ate my hands!"
    date: 2004-12-04
    body: "Things just keep getting better and better! Random selection of things missing from this story:\n\namaroK gets long awaited crossfading support in the xine engine\nDigikam gets remote gallery support\nkdnssd gets more stuff! (pervasive networking is your friend)\nSVG background images\nMost hated bug fixed this week: 79932 - KDE 3.3 Desktop Icon spacing is too wide\n"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Oh no! The goat ate my hands!"
    date: 2004-12-04
    body: "79932 was replaced by 94126, so it's not fixed yet."
    author: "Sven Langkamp"
  - subject: "Re: Oh no! The goat ate my hands!"
    date: 2004-12-04
    body: "Actually, in a strict sense, Bug 79932 was fixed some time ago, but was left open because there was no way to set the number of lines of text under the desktop icons.\n\nThe issue remains that the only way to set the DeskTop icon spacing is by hand editing the: \"~/.kde/share/config/kdeglobals\".  A GUI is needed for this.\n\nPlease add your comments on your ideas for such a GUI:\n\nhttp://bugs.kde.org/show_bug.cgi?id=94126  \n\nI hope that at least the most basic idea can be implemented soon.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Oh no! The goat ate my hands!"
    date: 2004-12-04
    body: ">kdnssd gets more stuff! (pervasive networking is your friend)\n\nIt just has been moved out of kdenonbeta."
    author: "Jakub Stachowski"
  - subject: "kpdf improvements"
    date: 2004-12-05
    body: "Will kpdf handle all the presentation \neffects in a pdf file \ncreated with latex prosper class, like\nacrobat reader does in windows ?\n\n-ask"
    author: "Ask"
  - subject: "Re: kpdf improvements"
    date: 2004-12-08
    body: "Can acrobat for linux do that?"
    author: "ac"
  - subject: "KHTML"
    date: 2004-12-07
    body: "Nice to see KHTML getting better (again).\n\nKeep up the good work, guys!"
    author: "Mikhail Capone"
  - subject: "natural voice"
    date: 2004-12-07
    body: "Just wondering...\n\nI have been searching for a decent speech program for linux for some time now.\nIs there *any* new up to date well supported \"natural voice\" system for linux?\neither commercial or open source. (last one prefered)"
    author: "Mark Hannessen"
  - subject: "Re: natural voice =/"
    date: 2006-09-02
    body: "I'm in the same situation...\nplease if anybody know some solution for this\ni'll be happy\n\nthanx  =/\n\npd: text aloud on vmware maybe the solution.........."
    author: "Tuan Tantisco"
---
In <a href="http://cvs-digest.org/index.php?issue=dec32004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=dec32004">experimental layout</a>):

<a href="http://accessibility.kde.org/developer/kttsd/">KTTSD</a> adds support for <a href="http://www.w3.org/TR/speech-synthesis/">SSML</a>/Sable.
<a href="http://digikam.sourceforge.net/">Digikam</a> implements remote gallery export of images.
Kpdf adds watch file option.
<a href="http://www.koffice.org/">KOffice</a> adds import support for PocketWord's PWD files.
Speedups in KTTSD, kwin and <a href="http://www.konqueror.org/features/browser.php">khtml</a>.


<!--break-->
