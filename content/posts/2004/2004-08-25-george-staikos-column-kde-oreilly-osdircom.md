---
title: "George Staikos Column on KDE at O'Reilly OSDir.com"
date:    2004-08-25
authors:
  - "smallett"
slug:    george-staikos-column-kde-oreilly-osdircom
comments:
  - subject: "Good column"
    date: 2004-08-26
    body: "It gives a nice overview of the release. I learned a few things."
    author: "Mikhail Capone"
  - subject: "Developers in denial??"
    date: 2004-08-27
    body: "> This is the quickest release cycle in recent history, coming roughly six \n> months after the release of KDE 3.2. To put this into perspective, KDE 3.2 \n> took over a year to complete. Does this mean fewer features, less polish, or \n> more bugs? Certainly not!\n\nI just can't figure it out.  Do the developers that say things like this actually use the current KDE releases?\n\nYes, it has more features and yes some of them are improvements.  But, there are a lot of little things that clearly indicate that it was released too soon -- it has less polish, it is less stable, and there are regressions.\n\nThe first step in solving this problem is to recognize that it exists.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Developers in denial??"
    date: 2004-08-27
    body: "If you go back to earlier releases, from 2.1 and upwards they have all had some of the small problems and a few regressions like the ones you are talking about. And they had all longer, or much longer release cycles so clearly the time does not make much difference on that count. On the other hand you have 7000 bugs closed, in addition to the bugs closed between 3.2.0 and now. And for most users the new features are more worth than some few annoyances, if not why bither to upgrade in the first place."
    author: "Morty"
  - subject: "Re: Developers in denial??"
    date: 2004-08-27
    body: "Your premise is indicative of the problem -- you appear to think that new features are more important than bug fixes.  It is possible that many users feel that way, but I doubt that commercial users do.  \n\nMany users upgrade to get bug fixes.  They would probably be happier with 3.2.4 but it isn't going to be released.\n\nYou compare 3.3 to 2.1.  This is not a valid comparison because KDE 3.x.y is a much more mature product than 2.x.y.  It probably doesn't need a lot of new features.  What is needed is a lot of small improvements.\n\nThere is also the idea that quality must improve as a software product matures.  Unfortunately, since 3.1.0 this has not been the case.  There have been serious problems with most of the releases since.  \n\nWith 3.3.0,the various small problems (some of which have already been fixed) could have been relsolved by having 3.3.0RC3 available for at least a month before the final.  Or perhaps an RC4 after that -- it should depend on an analysis of the bug reports filed against the current RC to decide when to release the final.  \n\nThere are also serious problems with the DeskTop icon placement code.  Apparently, a large rewrite will be needed to fix it.  The changes to it should not have been in a \"stable\" release branch until they worked correctly.  To avoid problems like this, changes in the development model are needed -- we would need a development branch which would not lead directly to a release branch.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
George Staikos is away at the moment but today I <a href="http://osdir.com/Article1502.phtml">published the first</a> of a bi-weekly column at O'Reilly's <a href="http://osdir.com/">OSDir.com</a> of what I hope are many insightful and informative articles by George on issues and happenings from the KDE project. Naturally, the first concerns the release of KDE 3.3.




<!--break-->
<p>I'm also on the lookout for interesting screenshots if anyone is interested in posting some and <a href="mailto:steve@osdir.com">pinging me</a>.</p>




