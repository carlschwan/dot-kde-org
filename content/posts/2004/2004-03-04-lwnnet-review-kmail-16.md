---
title: "LWN.net: Review of KMail 1.6"
date:    2004-03-04
authors:
  - "binner"
slug:    lwnnet-review-kmail-16
comments:
  - subject: "HTML Composing"
    date: 2004-03-04
    body: "I think probably the most important feature for KDEPIM 3.3 is HTML composing. Like it or not, it's quite important for many people. "
    author: "fault"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "http://bugs.kde.org/show_bug.cgi?id=4202 has a patch. Works with some glitches but misses IIRC a common HTML image background option."
    author: "Anonymous"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "> I think probably the most important feature for KDEPIM 3.3 is HTML composing. Like it or not\n\nNOT!! :))"
    author: "Vajsravana"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "Or a support for wiki structural text for mail composing."
    author: "gerd"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "learning yet another markup language, please no :)\n"
    author: "ik"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "If the previous post meant restructured text, it\u00b4s a cool geek alternative:\n\nYou do almost-wysiwyg markup in ascii, and it can be rendered to html.\n\nFor example, here\u00b4s a bulleted list:\n\n* item\n* item\n  * subitem\n\nA composer using reST would be nice. Geeky, but nice :-)"
    author: "Roberto Alsina"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Well, a standard for simplified markup languages is urgently needed.\nwikipedia, phpwiki, plone -- they use different sorts of structural text markup"
    author: "gerd"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "I filter (auto remove) all html and rtf messages\ncause only lamers and spammers use html for letters"
    author: "Nick Shafff"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "Please keep HTML composition off by default. Those people who want it can turn it on. People who don't know what it is don't need it. HTML formatted emails are decried in every netiquette guide I have ever seen. KDE should not be encouraging bad manners."
    author: "David Johnson"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Please keep it *on* by default. I know geekdom hates HTML composition, but a lot of (regular) people find it useful. \n\nNow, if no rich text is entered, I think the mail should should be sent as text/plain.. but that doesn't mean that formatting controls shouldn't appear on the screen. "
    author: "fault"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "\"I know geekdom hates HTML composition, but a lot of (regular) people find it useful.\"\n\nThe only reason they find it useful is because they don't understand how stupid it makes them look. Bright pink blinking 50 point text guranteed to trigger epileptic fits. Specified fonts that don't exist on your system. Grey on green color schemes. Etc. I can always tell when my mother is not wearing her glasses, because she switched to using 18 point font.\n\nAs web designers (those with more than two week experience) have painfully learned, people have different monitors, resolutions, DPI, color temps, etc. What looks good for the composer frequently looks like crap for the reader. Then you have different web browsers, despite the best efforts of the world's web designers to enforce a monopoly.\n\nGod forbid I ever get to the day when email I start receiving emails that say, \"It appears that you are not using Outlook Express. This message does not support your email client. To upgrade to Outlook Express, please visit www.microsoft.com...\""
    author: "David Johnson"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "I agree it should be off by default, but I for one intend to turn it on and leave it on.\n\nThere are two reasons people say HTML mails are bad netiquette and here is why both reasons are wrong:\n\nReason 1: Not all mail readers can handle HTML.\n- Response: That's what multipart/alternative is for.  KMail will send both a plaintext AND an HTML version so it's compatible with everything you throw at it.  Pine users rejoice!\n\nReason 2: HTML mails are larger than plaintext mails and hurt low-bandwidth users.\n- Response: No, low-bandwidth users are being hurt by using POP3 instead of IMAP.  This is a technological problem (slow connection) with a technological solution (limit your downloads to the parts of the e-mail you actually want).  Saying that the only solution is to ban HTML mail forever will only discourage learning about/adopting IMAP.\n\nIt isn't bad manners to use HTML mail.  It's bad manners to send mail that people can't read or that causes bandwidth problems.  Luckily, HTML mail is neither of these things.\n\nThis has been a public service announcement.  Thank you ;)"
    author: "ac"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "a) Sure. Except that pine will show you the HTML version (using lynx), so HTML shouldn't mean embedded images in that case.\n\nb) Usually dialup ISPs don't provide IMAP. Even if they did, you either stay connected (usually dialup charges per-minute outside the US) or you download them fully anyway. In both cases, yes, HTML mail will cost more money to read and/or will use more bandwidth."
    author: "Roberto Alsina"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "> b) Usually dialup ISPs don't provide IMAP. Even if they did, you either stay connected (usually dialup charges per-minute outside the US) or you download them fully anyway. In both cases, yes, HTML mail will cost more money to read and/or will use more bandwidth.\n\nI don't think it matters much these days anyways. I think you would find that more and more people use webmail anyways. "
    author: "anon"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Dialup users won't. But anyway, I was just replying at the previous post for what it said, not trying to find a full rationale for why html mail is bad (I am not even decided it is bad :-)"
    author: "Roberto Alsina"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "<sarcasm>I think you would find that more and more people use Outlook Express anyways. So I don't think KMail matters much these days anyway.</sarcasm>"
    author: "David Johnson"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: " For all those saying that the size of the messages does not matter, I'd ask to forget about permanent internet connection and switch to dial-up, where every\nsecond costs you. Needing to download 25 KB instead of 5KB means 5x more, and if you get 10 messages of 25KB it really makes a difference. Unless everybody\nswitched to DSL, cable or use the internet only at work the size of a message do matter. And IMAP is not available for everyone. And when you may use the internet for a limited time in a month, it's even more painful. It even matters in case of traffic based payment.\n Sincerely I hate HTML messages. If you want to send some nice document to somebody, send it as an attachment, but don't send HTMLs to everybody. \n I read Ingo's reply, and although their vision about HTML mail composing is user friendly, I hope that they will not forget an option to turn off HTML sending completely.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: HTML Composing"
    date: 2004-03-06
    body: "What's the point in turning HTML sending off completely? If the user doesn't make any text red, big or bold KMail won't create a HTML message. And if the user did make some text red, big or bold then he obviously wants to send the text red, big or bold and this means the message needs to be send as HTML message (or at least as rich text message).\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HTML Composing"
    date: 2004-03-06
    body: " Maybe because I don't want anyone to send HTML messages from my computer (eg. my wife) just because she/he pressed the Bold button. What would be better is to replace bold words with\n_word_ if HTML mail sending is turned off. \n Another concern is that this way (sending HTML mails if the users press a formatting button) \nthe users will never learn that what they are doing is not exactly good in many cases. They would just say: I don't send HTML mails, I just pressed that bold button on the toolbar.\n Regarding my relation with KMail: if HTML sending can be turned off or the formatting buttons are on a separate toolbar (so it's easy to remove), I'm fine with it. But regarding as an person who reads mail, I'd be a little pissed off if those who sent until now plain text messages from KMail will start to send HTML messages.\n The problem with your statement (\"And if the user did make some text red, big or bold then he obviously wants to send the text red, big or bold and this means the message needs to be send as HTML message (or at least as rich text message\") is that the user does not know what are the implications of sending such messages. And yes, I think software should be written that way that it's educating the user and is not easy to missuse. A dialog with a don't show again box warning the user about the fact that he will send much more data than just the text and that other users may not like or even be able to read such messages as well as that it's forbidden\n to send HTML mails to some mailing lists is also a good step in the right direction.\n Anyway, first let's see the real (official) implementation. I will comment on the mailing list or bugs.kde.org when there is something to talk about."
    author: "Andras Mantia"
  - subject: "Re: HTML Composing"
    date: 2004-03-07
    body: "> What's the point in turning HTML sending off completely?\n\nIf showing HTML is switched off by default, then it doesn't make much sense to have HTML editing on by default.\n\nImagine a company using KMail with default options. People would click onto \"bold\", \"red\", etc., and then find out that the text attributes are never shown. That looks like a bug in KMail at first glance.\n\nOlaf"
    author: "Olaf Jan Schmidt"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "a) Bad example with Pine ;)  But we seem agreed here.  HTML-only e-mail would be a problem, but KMail won't do that.\n\nb) Usually dialup users don't request IMAP.  It may be a chicken-egg problem, but HTML mail is neither the chicken nor the egg, so you can't blame it.  If sending HTML mail is bad netiquette, then by that logic sending photos should be grounds for execution.  Yet many plaintext-only advocates do just that all the time."
    author: "ac"
  - subject: "Re: HTML Composing"
    date: 2004-03-07
    body: "b) In many cases IMAP is a more expensive upgrade option to POP, not an issue related to \"chicken-egg\" at all."
    author: "Datschge"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Who cares what the defaults are.  This sounds like the discussion about konsole being in the panel by default.  KDE is a source distribution.  Whatever it might decide are the defaults is of little importance.  The actual Linux Distribution is the one that makes the final choice here.  If you really can't live with making some configuration modifications, then go and download the distribution that defaults it the way you like it."
    author: "Teo"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "This discussion about HTML composition being on or off by default is superfluous because KMail will automatically decide whether HTML (or rich text) is needed. If a message doesn't contain any *bold* or _underlined_ or otherwise marked-up text then a plain text message will be created. Otherwise a message with a plain text message part and a HTML message part will be created.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "What? You mean that if I type my mail just like I type my usenet posts and\nthe books  I write -- with underscores and stars around the words, KMail will decide that I want to send HTML?  That's going to make me really, really, really hate KMail and its developers. If _I_ type like this, I want it to be sent like _that_, and without any smart html parts attached. Just like I don't want html parts attached to usenet posts that I markup like that.\n\nI _never, ever, ever, ever_ want to send HTML by mail. I want to send mail using the plain and simple and understandable markup that I use without thinking about it because it's become second nature.\n\nAnd when I do, as I might when helping someone out, include a snippet of html in my mail (like -- place your text between bold tags, like this <b>bla</b>, then I still don't want a html part. Never, ever.\n\nCreating an html part would be -- well, the right adjective is moronic, but that's not polite, so I'll settle for definitely unwanted.\n\nIf people pre se want a misfeature like this, make it an option, off by default."
    author: "Boudewijn Rempt"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Sorry, for causing some confusion. Only if the user added some markup with some GUI buttons like [bold], [underline], [color] KMail will create a HTML (or maybe only a rich text message). Of course, neither usenet-style markup nor HTML tags will be intepreted as indication for HTML.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Phew! Thanks -- that helped bring blood pressure back to normal :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "How the heck could you believe it would be impossible to turn the behavior off in the first place?"
    author: "ac"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "I hope I misunderstood that. I second Boudewijn that sending a message as HTML only because it contains text marked with two asterisks/underscores/slashes would be unacceptable and outright silly. It would be a slap into the face of everyone who tries to follow the netiquette. I was quite surprised when I browsed through comp.lang.perl.misc and encountered bold code parts in Perl code snippets. It turned out, that knode did by default remove the slashes in cases like \"if (/foo/) ...\" and showed \"foo\" in bold. Disgusting!\n\nBut you probably meant this, anyway: as soon as a users makes text parts actually *look* bold/italic/slanted/underlined/centered/colored in the composer window, then and only then auto-switch to HTML. So it would require the user to select a word and to click, for example, the \"bold\" button to send an HTML message."
    author: "Melchior FRANZ"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "NO, NO, NO, NO! That sounds like Mozilla Mail. And I *friggin* hate Mozilla Mail! Please don't do anything like that. Applications should never try to guess what the user wants. HTML-mail should be *OFF* by deault, damit! And I don't want any damned auto-formatting-crap!"
    author: "Kukhuvud"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Yeah, as I said before, much of geekdom hates html mail. That doesn't mean we shouldn't cater to other users. "
    author: "fault"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "And _that_ doesn't mean we shouldn't cater to much of geekdom."
    author: "Boudewijn Rempt"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "Of course we should cater to geekdom, I'm down with a option to never send html messages. Hell, I'd use it. That doesn't mean my teenage sister shouldn't be able send red text in 28 point letters to her friends. Like every other teenage girl, she does this in both email and instant messages :)"
    author: "anon"
  - subject: "Re: HTML Composing"
    date: 2004-03-05
    body: "That doesn't sound like a good reason to use HTML :("
    author: "Coma"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-05
    body: "I also hope that it is not planed to convert text only markups to html.\nI also don't want to start kmail one day and have to see that all ;) things got converted to ugly icons and _bla_ is not underlined for me. It is _bla_\n\nI think you know this: http://www.gimp.org/~xach/wilber-helper.png\nThat is not usability.\n\nAlso I found out that many people who use html only use it because they\na: cannot change it because the program/web frontend doesn't allows it\nb: don't know what they do\n\nSure there are people who like to send html mails.\nSo I hope it will be added to kmail, but not as default/autodetect.\nThese people know what html is, and they can find/use a configuration dialog or use a distribution like SuSE which will enable it as default and add some great smiley icons.\n\nFirst thing I disabled in 3.2 was this Attachment Warning... (Very useful, but only if you configure it to take your own keywords)\n\n\nOverall I think it is great that kde tries to fokus on usability. But if that means that kde will do all things automatic and hides things I must search for something that is better for me.\n\nI say this with a look at the new kcontrol, as I like the tree view and all that options. That is one (important) reason why I use kde. I can configure so many things that it can take more time as a fresh kernel configuration ;) (without using text config files)\n\nAdd a beginners-view, ok, but don't remove the advanced view, that is usability, usability for everyone.\n\nFor me usability means using masks for beginners, normals and experts not one version useable for everyone (of course it means more).\n\nBecause that won't work.\n\n\nNote to myself: Maybe I should just read kde-usability instead of spamming the dot..."
    author: "Felix"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-05
    body: "> For me usability means using masks for beginners, normals and experts not one\n> version useable for everyone (of course it means more).\n\nwhat you describe is \"user levels\", and they don't work. this has been discussed a bajillion times, i'm not interested in doing it again when google can do it for us ;-)\n\n> Note to myself: Maybe I should just read kde-usability instead of spamming\n> the dot...\n\nplease, only if you plan on learning about usability (rather than simply offering your own personal opinion) and actually helping out. if that's the case, great! we would all welcome your participation..."
    author: "Aaron J. Seigo"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-06
    body: "\"user levels\" do work, but I'm not interested in discussing it with you as you obviously are wrong.  check the web for where you are wrong.\n\naaron you need to climb down off of that awfully high horse you have been riding in on.  you are building a mountain of ego and are really starting to sound like eugenia of all people.  i have respect for you, but it is diminishing every time you trot out arguments from authority.  knock it off.\n\nseriously, usability is all about personal opinion.  you are not anything different.  from experience watching you and listening to your recent diatribes you are all about spreading your personal opinions.  i know you are also gathering statistics, yadda, yadda, but that sure hasn't stopped you from slinging opinions with the best of them."
    author: "anon"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-06
    body: "\"User levels\" don't work. There is a lot of research pointing this out. Lots of people have spent lots of time discussing this on lots of mailing lists -- it is simply NOT feasable to constantly debate every issue every time people who have to bothered read everything you have read come by and demand an explaination ad nausem. \n\nYou think expecting people to know what they are arguing about is elitism? Its not, its called education, and you can get it from books or google, which is what hes asking you to do.\n\nIf your doctor spent 6 years in school learning about biology and medicine, and he tells you that antibiotics don't cure colds, are you tell him he needs to come down off his high horse because you disagree with him? Please, usability isn't about personal opinion any more than bridge building is about personal opinion: there are know good ways to do things and known bad ways. There is plenty of room in the middle where opinions matter, but \"user levels\" aren't one of them.\n\nI tell you what, why don't you reply with what field you have spent a serious amount of time studying, and I will proceed to get all my friends to get you to explain to me every principle over and over again, ad nauseum, without ever consulting any of the free knowledge available online, such as google or mailing list specifically set up to handle my questions. Then we'll see who the elitists are.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-07
    body: "My field is postmodern literary criticism.  I'd be happy to explain ad nauseum the heavily nuanced science of postmodern literary criticism.  I can assure you that P.M. lit crit is a fact based science, but if you have any doubts go take a look at all the well documented conversations available via google.  Of course, I'll have to set up an appointment before I come down out of my ivory tower to talk with a bunch of so-called \"usability experts\", but I'll try and find some time."
    author: "anon"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-07
    body: "Thats the difference between you and I. I have taken courses on literary criticism, and am fairly well read (on liturature and computer science). I don't consider it an unfair burden to read before I open my mouth, or issue threats like: \"I used to respect you, man!\"\n\nI'll be interested in your comments on usability when you have actually read a book on it one day.\n\nps. I assume you know where the kde usability mailing list is, since this is a kde forum. Reciprocate and let me know what list you and your friends gather in to discuss literature (I'm serious).\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-07
    body: "I have read books on usability and as this makes me an authority I hereby render judgement that you are full of crap.  The fact that you are full of crap has been discussed countless times so I will leave it as an exercise to the reader to have a look on google for the conclusive proof.  Do a little search and perhaps read a book or two on why you are full of crap and then I'll be interested in your comments about how you are not full of crap."
    author: "anon"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-07
    body: "Ok, now this is clearly just a joke or a troll, since you have yet to make any point what so ever. Congratulations, your complaint about being ignored has lead to you being ignored. I hope you consider it worth your while, because I consider talking to you a complete waste of my time.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-08
    body: "riiiiiiggggggggggghhhhhhhhttttt\n\nsuch a complete waste of time you felt the urgent need to actively reply whilst trumpting that you were ignoring me. what a chump. what a maroon."
    author: "anon"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-08
    body: "What a troll."
    author: "David"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-09
    body: "Damn right. I didn't right that comment - that's another David."
    author: "David"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-08
    body: "Yes I like to learn about it. But there is more than only books about this.\nSince usability is different for many people.\nI think it was wrong to say that \"user levels\" are the best way.\n\nI am more interested in asking what target kde has.\nHow far will kde go to get the usability for real beginners and how much features will go away for this (see gnome).\nBecause if there aren't user levels, take kcontrol3, there won't be a tree view.\nThan I ask myself, how long will it take that kde removed features like network timeout values or the great kicker or kwin menu.\n\nNote that I don't have real knowlege about usability, so maybe I am wrong here, I only saw such things already in other projects.\n\nThis is something I am really interested in as (I call me an advanced _user_ ;))\n"
    author: "Felix"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-06
    body: "I don't think anyone involved with KDE is advocating having some stupid \"It looks like you are writing a letter, would you like help with that?\" rubbish. I think that is pointless because the application can't actually help you with anything. If it could it would write the letter for you! Besides, that's not really usability.\n\nYes I think HTML mail should be turned off by default - people want to write e-mails first and foremost, but that doesn't mean we shouldn't have it. HTML mail can muck things up and it makes you look impolite in certain circumstances, so I think good education in the application is important. I think that will also reassure many users.\n\nEven as a fairly advanced e-mail users I forget to attach documents to e-mails, which has led to some half-botched excuses! The attachment warning has been very, very, very useful, but if you don't want it, turn it off.\n\nThere are a lot of good applications in KDE with god-awful configuration dialogues. Konqueror and KWrite are cases in point, but does that mean that all their features should be chopped? Hell no! Has Konsole been dropped? No! They do need to be organised better though, and that means hiding certain features that people are not going to use or readily understand most of the time. This isn't to stop stupid people doing things, but to make sure people can actually logically read what they are looking at! At times there are so many features in view that you can't see the damn thing you want. Setting a default web page was the thing for me.\n\nAaron is right about user levels - they don't work. Why? Because it has to be left up to the end-user to decide what level he/she is at. Things should just be done right in the first place.\n\nLinux desktops, and KDE, are being, and will be, used by a lot of different people and it is a big sign of KDE's popularity. Let's give a little thought to that."
    author: "David"
  - subject: "Re: HTML Composing / Usability"
    date: 2004-03-07
    body: "\"They do need to be organised better though, and that means hiding certain features that people are not going to use or readily understand most of the time.\"\n\n'Hiding' is probably a bad word here. What I mean is putting an option under a logical hierarchy of some kind so that it doesn't clutter the features people are going to use more readily. This way, people can easily find what they want when they want it.\n"
    author: "David"
  - subject: "Re: HTML Composing"
    date: 2004-03-04
    body: "(sarcasm on)\nNext to html composing is compiling a kernel from your PIM. Like it or not, it's quite important for many peoples!\n(sarcasm off)\n\nI think, masses who wants anything, could be wrong too and could not expect what happens, if they have one (useless) feature or not.\n"
    author: "anonymous"
  - subject: "KitchenSync , does that include SyncML support?"
    date: 2004-03-04
    body: "I have a SE P800 smartphone and it's using Symbian software which again uses SyncML for sync'ing with your PC. Will support be included for that? SyncML is an open standard: http://www.openmobilealliance.org/syncml/"
    author: "Joergen Ramskov"
  - subject: "Re: KitchenSync , does that include SyncML support?"
    date: 2004-03-04
    body: "There is a project to make syncml work with kichensync. \nJust be patient... ;+)\n"
    author: "MacBerry"
  - subject: "Re: KitchenSync , does that include SyncML support"
    date: 2004-03-05
    body: "Thanks for the info! Good to hear someone is working on it :)"
    author: "Joergen Ramskov"
  - subject: "Re: KitchenSync , does that include SyncML support?"
    date: 2004-03-04
    body: "I have one, too (the lock of the battery-lid just broke off today :). Someone (Maciek, thank you!) has just started work on a syncML-plugin, so I guess we need to be a little patient. The syncML specs are quite a book."
    author: "anonymous"
  - subject: "Re: KitchenSync , does that include SyncML support?"
    date: 2004-08-05
    body: "Any ideas on how patient we need to be? :-)\nLooking forward to getting a new Sony Ericsson P910i when its released."
    author: "Parwinder Sekhon"
  - subject: "On /."
    date: 2004-03-04
    body: "http://slashdot.org/articles/04/03/04/157219.shtml"
    author: "Anonymous"
  - subject: "modern, but still much room for improvement"
    date: 2004-03-04
    body: "http://www.research.ibm.com/remail/ pointed out some problems with today's email clients and made a proposal for a new email programm."
    author: "testerus"
  - subject: "Nice"
    date: 2004-03-05
    body: "Some very interesting stuff tere. Nice."
    author: "Source"
  - subject: "Important for Mail in an office"
    date: 2004-03-05
    body: "I need server side filtering rules (IMAP) in Kmail, and also a Korganiser that allows me to have my default calendar on a remote site....:)\n\nThat's all :)\n\nLuke"
    author: "Luke"
  - subject: "Re: Important for Mail in an office"
    date: 2004-03-05
    body: "> I need server side filtering rules (IMAP) in Kmail.\n\nThat's a contradiction since server side filtering happens on the server not in the client. Or do you want support for writing and uploading server side filtering rules (i.e. Sieve scripts) in KMail?\n\n> I need [...] a Korganiser that allows me to have my default calendar on a remote site.\n\nThat's already possible with KDE 3.2. Simply create a resource on the remote site.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Important for Mail in an office"
    date: 2004-03-05
    body: "1) Yes - I mean I need the client to be able to write the scripts for filtering - at the moment we're using a plug in for squirrel mail to do it, but ugly when I have to tell people...they want pop - but we don't want them to store anything on their client machines :)\n\n2) Well I thought so, but I've been trying today - and no luck - I guess I can remove it manually :)\n\nBut I suppose the problem with korganiser (and is already noted on the kdepim page) is the lack of documentation - don't get me wrong - I love KDE, Kontact - the whole project is the best thing since the linux kernel IMHO - but when I say I need....I mean my boss is planning on rolling out contact in as few a weeks as possible :)\n\nKeep up the quality guys - that's why we use OSS :) (but I really need those features for server side filtering in KMail :)"
    author: "Luke"
  - subject: "Re: Important for Mail in an office"
    date: 2004-03-06
    body: ">the problem with korganiser (and is already noted on the kdepim page) is the lack of documentation\n\nStay tuned..."
    author: "cwoelz"
  - subject: "Emoticons "
    date: 2004-03-05
    body: "Providing Emoticons shall be possible :-)"
    author: "gerd"
  - subject: "I won't use html composing without emoticons!!!!"
    date: 2004-03-05
    body: "yeah I totally agree! I won't use html composing without emoticons :)  (such as in mozilla thunderbird) maybe we could use kopete's everaldo one's."
    author: "Pat"
---
<a href="http://lwn.net/">LWN.net</a> has <a href="http://lwn.net/Articles/72945/">reviewed KMail 1.6</a> as part of a <a href="http://lwn.net/Articles/72937/">comparison of different mail clients</a>.  The reviewer also glances at <a href="http://kontact.org/">Kontact</a> with some nice words but then concentrates on its mail component and the address book. He finds it to be a modern and full-featured mail client with few deficits in IMAP and search support which <a href="http://developer.kde.org/development-versions/kdepim-3.3-features.html">will likely be addressed</a> in the <a href="http://developer.kde.org/development-versions/kdepim-3.3-release-plan.html">pending KDE PIM 3.3 release</a>.

<!--break-->
