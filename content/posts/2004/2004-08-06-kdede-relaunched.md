---
title: "KDE.de Relaunched"
date:    2004-08-06
authors:
  - "kstaerk"
slug:    kdede-relaunched
comments:
  - subject: "\"Overworked\" version of KDE.de?"
    date: 2004-08-06
    body: "\"Overworked\" has the meaning of \"having overdone with work\", \"being completely exhausted\".\n\nYou certainly meant to talk about a \"reworked\" website. \"Reworked\" has the meaning of \"overhauled\", \"refurbished\", \"renovated\".\n\nIsn't there a native English speaker in the dot editors' team? Very often your stories make it sound as they were badly translated from German. In the US, this re-inforces their \"not invented here\" syndrome companies do suffer from when they look at KDE (and compare it with GNOME)."
    author: "kdefriend2"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-06
    body: "> \"Overworked\" has the meaning of \"having overdone with work\", \"being completely exhausted\".\n\nNot only. According to both http://dict.leo.org and Langenscheidt dictionary \"overworked\" with the meaning it was used is correct while perhaps not the best solution."
    author: "Anonymous"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-06
    body: "We might say 'worked over', but not 'overworked' for this sense - reworked is perhaps the best word to use here. What is the German word, so us English speakers can learn something - 'uberwerken'?"
    author: "Richard Dale"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-06
    body: "German \"\u00fcberarbeiten\" corresponds to English \"to rework\", but \"\u00fcberarbeitet\" (the past participle) has also the meaning \"overworked\". \n\nSo: \n\n\"Er ist \u00fcberarbeitet.\" --> \"He's overworked.\", but \"Die Seite ist \u00fcberarbeitet.\" --> \"The page is reworked.\" Funny thing, this."
    author: "g.kunter"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-06
    body: "Ah, interesting - without this discussion I don't think I'd be able to remember that, but now I've got something to hang it on. I should have at least remembered \"arbeit\" -> work - useless of me! \n\nSo 'werk' isn't a proper German word, and 'MetroWerks' as in the compiler company, must be mock German.."
    author: "Richard Dale"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-06
    body: "\"Werk\" is a proper German word. See http://dict.leo.org/?search=werk"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-07
    body: "I wish 'werk' was a proper English word. Why do we spell 'fork' is a similar way to 'work', but speak it rhyming with 'awk'.\n\nIt looks to me as though 'werk', with the better German spelling, means 'a finished thing that you have worked on'. As opposed to something your still working on - 'arbeit'. So that might explain is similar difference with '\u00fcberarbeitet' that means 'the thing is finished, redone and better than the previous version'. As opposed to describing someone who is working on something who may or may not finish successfully. Perhaps it reflects higher German engineering standards, which are embedded in the language.. :)"
    author: "Richard Dale"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-09
    body: "Yup. \"Arbeit\" is the process that, if finished succesfully, leads to a \"Werk\".\n\nSide note: A \"Werk\" can also mean plant (as in factory or powerplant, not as in tomato)."
    author: "Annno v. Heimburg"
  - subject: "Re: \"Overworked\" version of KDE.de?"
    date: 2004-08-16
    body: "In fact, werk is dutch for work.\nAre you confused yet?\n\nFrank"
    author: "Frank"
  - subject: "Well done, nice site!"
    date: 2004-08-06
    body: "Well done, nice site!"
    author: "wilbert"
  - subject: "Great new site, but why new RDF-Feed?"
    date: 2004-08-06
    body: "Looked at the new site and all looks nice. I have one complaint though:\nWhy I don't get the RDF under .../news/news.rdf like it was before, but have to manually correct it to .../nachrichten/nachrichten.rdf in knewsticker?"
    author: "Thomas Heuving"
  - subject: "Re: Great new site, but why new RDF-Feed?"
    date: 2004-08-06
    body: "Hello Thomas,\n\nfirst of all: thanks for visiting KDE.de and using our news feed :-)\n\nWell, the decision about the new path for the RDF file was made for several reasons during the process of revision of KDE.de - I'm sorry! I hope it didn't bother you too much to redefine the path in your newsticker app.\n\nHave fun with the new page of KDE Germany.\n\nYours,\n\nKlaus :-)"
    author: "Klaus"
  - subject: "Good job"
    date: 2004-08-06
    body: "That looks really nice.  IMHO, the font should be bumped up a little, but that's just me.\n\nI hope KDE.org redoes their site to look the same."
    author: "archon"
  - subject: "Re: Good job"
    date: 2004-08-06
    body: "> I hope KDE.org redoes their site to look the same.\n\nVery unlikely."
    author: "Anonymous"
  - subject: "Re: Good job"
    date: 2004-08-07
    body: "Re-do in which way?\n\nIf it is the colours, that would rather be easy to have them as alternate style.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Low saxon prioject"
    date: 2004-08-09
    body: "It is sad that the German website does not mention the translation to low saxon(nds).\n\nhttp://nds.i18n.kde.org/"
    author: "bernd"
  - subject: "Re: Low saxon prioject"
    date: 2004-08-10
    body: "Send (a patch) to webmaster@kde.de"
    author: "Anonymous"
  - subject: "Rendering problem"
    date: 2004-08-10
    body: "Not sure why, but the site does not render quite nicely with Konqueror 3.2.3. It's a bit too wide to fit on the screen. Note that I'm using a 1152x764 resolution, so it's not my browserwindow that is just too small. I think that a KDE website should at least look good on KDE."
    author: "Andre Somers"
  - subject: "Re: Rendering problem"
    date: 2004-08-11
    body: "Hello Andre,\n\nIn know this \"problem\". Actually, this is a known _bug_ in Konqueror that was fixed in older versions - and seems to have re-appeared in version 3.2.3.\n\nMaybe you consult bugs.kde.org about this issue.\n\nOther browsers like Mozilla and Opera have no problems with rendering this site.\n\nYours,\n\nKlaus"
    author: "Klaus Staerk"
---
The web team of <a href="http://www.kde.de/">KDE Germany</a> is proud to present the German KDE website with a new layout. During the last weeks, the team of KDE.de - especially Dirk Trompetter and Klaus Staerk - did a great job to revise both layout and content of this website. Have a lot of fun with the <a href="http://www.kde.de/nachrichten/nachrichten.php#739">new, reworked version of KDE.de</a>.


<!--break-->
