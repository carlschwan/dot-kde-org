---
title: "KDE 3.4 Release Cycle Starts with KDE 3.4 Alpha 1 (\"Leverkusen\")"
date:    2004-12-09
authors:
  - "skulow"
slug:    kde-34-release-cycle-starts-kde-34-alpha-1-leverkusen
comments:
  - subject: "release name :)"
    date: 2004-12-09
    body: "leverkusen ? hehe, what is the naming scheme for this release ? champions league winners ?\n\n"
    author: "chris"
  - subject: "Re: release name :)"
    date: 2004-12-09
    body: "If so, the final release should be named \"Real Madrid\" :)"
    author: "ET"
  - subject: "Re: release name :)"
    date: 2004-12-10
    body: "Sorry, but I think it was FC Porto from Portugal ;)"
    author: "jcp"
  - subject: "Re: release name :)"
    date: 2004-12-10
    body: "Pardon, Sir, but it ought to be \"Extremadura CF\"."
    author: "c0p0n"
  - subject: "Re: release name :)"
    date: 2004-12-11
    body: "as an FC Porto fan and Portuguese ... \n\ndo you know what you are talking about , or just trying to make a bad joke ?"
    author: "Yagami"
  - subject: "Re: release name :)"
    date: 2004-12-14
    body: "Why not Boca Juniors? We still're the champions of the world :D"
    author: "Angel"
  - subject: "Re: release name :)"
    date: 2004-12-09
    body: "What about Bayern Muenchen then? :-))"
    author: "Michael Thaler"
  - subject: "Coolo has no clue (about soccer)"
    date: 2004-12-09
    body: "It is obvious, that the next releaee will be named by Coolo mapping to one of the remaining quarter finalists. The one after will be a semi-finalist. Then one of the finalists. Last the champion.\n\nBut Coolo -- why do you predict Leverkusen will be be out by the time of the semi-final? It is obvious that you can't have the same name twice. I could agree that Leverkusen won't make well as a \"champion\" name -- so no reason to save that name for the \"Gold\" release.\n\nBut dismissing them already now? Pfffft...... (Coolo may be an excellent Release Dude -- but he has no clue about European soccer.)"
    author: "open mind"
  - subject: "Re: Coolo has no clue (about soccer)"
    date: 2004-12-09
    body: "Bah! Who says I stick to champions league? Germany got plenty of soccer teams I can pick from. And Aachen sounds like a good second choice :)"
    author: "Stephan Kulow"
  - subject: "Re: Coolo has no clue (about soccer)"
    date: 2004-12-09
    body: "lol, Oche Al..pha II "
    author: "Carlo"
  - subject: "Re: Coolo has no clue (about soccer)"
    date: 2004-12-10
    body: "Who says you have to stick to Germany? Or to silly useless non-descriptive confusing names in the first place? :)"
    author: "Rob Kaper"
  - subject: "Re: release name :)"
    date: 2004-12-09
    body: "as long as no release is named \"bielefeld\"...\n"
    author: "Mathias Homann"
  - subject: "Re: release name :)"
    date: 2004-12-09
    body: "Im proud to be a Leverkusener"
    author: "Oliver Schliebs"
  - subject: "Re: release name :)"
    date: 2004-12-09
    body: "I am a Leverkusener too.\nBut naming an alpha-release after this town is IMHO a bit... strange.\nAre any Developers coming from this town?\n\nBenja."
    author: "Benjamin"
  - subject: "Re: release name :)"
    date: 2004-12-10
    body: "What's wrong with Bielefeldt? I lived there for a couple of years!"
    author: "Moose from Sweden!"
  - subject: "Re: release name :)"
    date: 2004-12-10
    body: "http://en.wikipedia.org/wiki/Bielefeld-Verschw%F6rung"
    author: "MaX"
  - subject: "what does leverkusen mean?"
    date: 2004-12-09
    body: "Obvious question:\n\nFor those of us who don't speak german, what does leverkusen mean?"
    author: "Paul"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "I think in Germany it is just a city name. In danish on the other hand it means liver-cunt (seriously).\n\n"
    author: "Carewolf"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "...and yesterday, their professional soccer team \"Bayer\" made it into the semi-semi-semi-final of the European Champions League!   ;-)\n(along with Werder Bremen and Bayern Muenchen)."
    author: "open mind"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "No it doesn't. \"Lever\" means liver that is correct but \"kusen\" means nothing in Danish. If it had been \"kussen\" then it would have ment liver-cunt, but for those of us who actually speaks Danish (and can spell it too) this is not the case."
    author: "Kim"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "Funny... Dutch 'kussen' means 'kisses' or 'pillow'. Must make for some marital misunderstandings in mixed Danish-Dutch marriages."
    author: "Boudewijn Rempt"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "I kiss liver all the time."
    author: "ac"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "It's a city in Germany near Cologne."
    author: "Christian Loose"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-10
    body: "And not too far from Perfume in Luxembourd..."
    author: "Aaron Krill"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "For those of you who can speak German, here is an explanation, what Leverkuse actually means:\n\nhttp://www.toenti.de/bitmaps/schalke/leverkusen.jpg"
    author: "Michael Thaler"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-09
    body: "Leverkus was man and his profession apothecy. \n\nHe discovered something big, I forgot what, and Leverkusen became named after him and a big center for chemical and pharma industry.\n\nBayer is the megacorp there.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: what does leverkusen mean?"
    date: 2004-12-10
    body: "The \"something big\" Dr. Carl Leverkus \"discovered\" - more exactly: founded - was a color factory which was bought by Bayer which was founded by Friedrich Bayer."
    author: "cyberpatrol"
  - subject: "Re: what does leverkusen mean?"
    date: 2006-08-27
    body: "http://www.leverkusen.com/uk_us/historie"
    author: "Andreas Born"
  - subject: "what does leverkusen mean?"
    date: 2004-12-09
    body: "Leverkusen is a Town near Cologne. "
    author: "jambo"
  - subject: "Leverkusen..."
    date: 2004-12-09
    body: "I can only say: Yes, it's a city. Near cologne. And I gotta know it: I live in Germany ;)\n\nBTW: Is there a changelog available?"
    author: "terra1"
  - subject: "Re: Leverkusen..."
    date: 2004-12-09
    body: "Best thing you can get is http://developer.kde.org/development-versions/kde-3.4-features.html but beware it can be VERY outdated"
    author: "Albert Astals Cid"
  - subject: "Re: Leverkusen..."
    date: 2004-12-09
    body: "Thank you..."
    author: "terra1"
  - subject: "Re: Leverkusen..."
    date: 2004-12-09
    body: "You can forget about Leverkusen the city. But Leverkusen is also the team that beat Real Madrid 3:0 ;)"
    author: "Stephan Kulow"
  - subject: "Re: Leverkusen..."
    date: 2004-12-09
    body: "Hehe, but the team that won against Madrid came from the city Leverkusen ;).."
    author: "terra1"
  - subject: "Re: Leverkusen..."
    date: 2004-12-09
    body: "Leverkusen is also the team that did LOST the final against Real Madrid....."
    author: "Javier"
  - subject: "Screenshots?"
    date: 2004-12-09
    body: "Anyone got some screenshots of the new features?  I'm interested in the Kicker work that's been done."
    author: "standsolid"
  - subject: "Re: Screenshots?"
    date: 2004-12-09
    body: "Congratulations for the first post not talking about the codename."
    author: "Anonymous"
  - subject: "Re: Screenshots?"
    date: 2004-12-09
    body: "Who, me?"
    author: "Leverkusen"
  - subject: "Re: Screenshots?"
    date: 2004-12-09
    body: "Posting from Kde 3.4, just have kdebase build, will post screenshot here tomorow.\n\nOne question, why kpdf is not the new full featured kpdf?"
    author: "gnumdk"
  - subject: "Re: Screenshots?"
    date: 2004-12-09
    body: "Because the really cool (in opposite to just cool :) new features are still in the kpdf_experiments branch only and not in HEAD."
    author: "Anonymous"
  - subject: "Re: Screenshots?"
    date: 2004-12-10
    body: "Hey, the release is named Leverkusen.\n\nThe main reason for using such names is distractiong readers from the feature list. So everybody talks about the names and nobody complains about the releases."
    author: "gerd"
  - subject: "Re: Screenshots?"
    date: 2004-12-10
    body: "Because we (ok, more Enrico than me) still have to polish the experimental kpdf a bit before merging to HEAD. I hope will have it for Beta1."
    author: "Albert Astals Cid"
  - subject: "transition effects ?"
    date: 2004-12-10
    body: "Will the new kdf support the various transition \neffects in a pdf document created by latex prosper \nclass, like Acrobat reader in windows does ?\n\nAsk"
    author: "Ask"
  - subject: "Re: transition effects ?"
    date: 2004-12-10
    body: "Adrian Leverkuhn is the main person\nof one of Thomas Mann's masterpieces,\nthe novel \"Doctor Faustus\"...\n\nMS"
    author: "samiel"
  - subject: "Re: transition effects ?"
    date: 2004-12-11
    body: "Probably no, but we haven't tested any document like that. Could you please provide a link to one of them?"
    author: "Albert Astals Cid"
  - subject: "Re: transition effects ?"
    date: 2004-12-11
    body: "Although I think that these transistion effects are not really important features: A latex prosper generated pdf containing transition effects is availible at \n\nhttp://www.nefkom.net/georg.drenkhahn/prosper/doc/prosper-tour.pdf"
    author: "furanku"
  - subject: "Re: transition effects ?"
    date: 2004-12-14
    body: "well... IMHO those animations *are* important features. People wants big buttons. Sleek graphics. Cool effects.\nLet's give them all, to conquer them. \nUh... konquer! :)\n"
    author: "superfebs"
  - subject: "KDE 3.4"
    date: 2005-01-04
    body: "Hello\nI have compile KDE 3.4 from ftp://ftp.kde.org/pub/kde/unstable/snapshots/\nKMAIL is not installed.\nWhere is KMAIL ?\njfd"
    author: "JF Digonnet"
  - subject: "Re: KDE 3.4"
    date: 2005-01-04
    body: "KMail is suppose to be in KDEPIM.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
For those who can't live without a bleeding edge KDE, but don't dare to run CVS, we have packaged KDE 3.4 Alpha 1. As you can read on the <a  href="http://developer.kde.org/development-versions/kde-3.4-release-plan.html"> KDE 3.4 release schedule</a>, this is only the start of the fun, so please hammer on it over the end of year holidays and add your contributions. We welcome code patches, translations, documentation, great icons, detailed bug reports - any kind of <A href="https://mail.kde.org/mailman/listinfo/kde-quality">help</a>.

<!--break-->
Get it from <a href="http://download.kde.org/download.php?url=unstable/3.3.90/src">download.kde.org</a> or try <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>.

As always we ask you not to use binary packages for Alpha 1, please compile it yourself using debugging options, so your bug reports will be as helpful as possible.












