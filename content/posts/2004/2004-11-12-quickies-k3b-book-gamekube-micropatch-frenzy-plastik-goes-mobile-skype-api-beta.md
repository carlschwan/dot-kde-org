---
title: "Quickies: K3b Book, GameKube, Micropatch Frenzy, Plastik goes Mobile, Skype API Beta"
date:    2004-11-12
authors:
  - "fmous"
slug:    quickies-k3b-book-gamekube-micropatch-frenzy-plastik-goes-mobile-skype-api-beta
comments:
  - subject: "what about asterisk"
    date: 2004-11-12
    body: "I love skype, dont get me wrong. But it would be cool to support asterisk too. Does asterisk offer such an API? i don't know yet I know it supports open standards, lots of hardware and since recently offers p2p functionality ala skype www.digium.com . So if asterisk offers such an API (or maybe libs as it's opensource) it would better to implement it into kdepim/kopete instead of skype first :)"
    author: "Pat"
  - subject: "Re: what about asterisk"
    date: 2004-11-12
    body: "Sorry, looking at this digium webpage, I have *no idea* what to do.\n\nSkype is a download and Just Works.  Asterix is incomprehensible."
    author: "ac"
  - subject: "Re: what about asterisk"
    date: 2004-11-12
    body: "Asterisk is actually pretty darn easy to use, but it is a completely different product than Skype.  Skype's software just provides a client for making calls to other users and phones. Asterisk on the other hand is a PBX, its purpose is to accept calls from one point and direct them to their destination. I don't know what type of system Skype runs on the backend, but they may very well use asterisk. It took me less than an afternoon to get an asterisk system up and running and receiving calls, going from scratch with no previous asterisk experience.  It does look fairly complex from the outside because of the flexibility it offers, but the handbook they have under the support section is a great start to understanding it.\n\nBoth are great programs, but they really aren't comparable. They accomplish completely different tasks. The only is that they both involve phones, there doesn't even need to be any Voice over IP involved in the system. \n"
    author: "Greg "
  - subject: "Skype in Kopete and Kontact?"
    date: 2004-11-12
    body: "Quote from http://www.skype.com/community/devzone/doc.html\n\n\"Skype API is initially only available for and supported on Microsoft Windows.\"\n\nSeems like we need to wait (a little bit) longer."
    author: "LB"
  - subject: "Re: Skype in Kopete and Kontact?"
    date: 2004-11-12
    body: "Skype is going to be providing a public API in their Linux client too, based on DBUS."
    author: "Jason Keirstead"
  - subject: "Skype, your casual undocumented secret protocol"
    date: 2004-11-12
    body: "Yes, perfect excuse to help a service (Skype) which's firmly against releasing it's source or even protocol -- to dominate the market!\nWay to go, if we wanna dig our own grave, with all the bad things non-free software can bring:\n- bad interoperatibility,\n- dependency on a single vendor,\n- inability to fix bugs or add features,\n- possible future license changes, putting us in Skype's mercy (e.g. free for first N minutes, free for ...)\nthen go ahead and jump on the bandwagon.\n\nYou know, \"free software\" isn't some freaky politic issue. It's reality! Having chat apps with undocumented protocols is something we, as Linux desktop users, got hurt from far too often in the past."
    author: "Toastie"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2004-11-12
    body: "Indeed. What's wrong with SIP/Speex?"
    author: "Robert"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2004-11-12
    body: "Well, when I tried to implement SIP, it failed to work through firewalls.\nThen I tried installing uPNP gateways for it, and it still didn't work.\nThat took about two days.\n\n\nThen I downloaded skype, and it worked in 10 minutes.\n\nSo, I guess what's wrong with SIP is that it's pretty damn hard to make it work in the real world."
    author: "anon"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2004-11-12
    body: "> Well, when I tried to implement SIP, it failed to work through firewalls.\n\nIt works now. Solution is not nice, but works. AFAIK skype uses SIP as well; it's even able to tunnel it over http."
    author: "jmk"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2004-11-12
    body: "they don't use SIP, they use their own proprietary stuff"
    author: "Pat"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2004-11-16
    body: "We don't need Skype.. \nWe have an excellent VoIP open protocol, IAX  ( http://www.voip-info.org/tiki-index.php?page=IAX ) and here ( http://iaxclient.sourceforge.net/ ) are the libs.\n"
    author: "Federico N\u00fa\u00f1ez"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2004-11-16
    body: "people don't want another protocol, they want a \"application\". something that works for them. sure... I don't like the skype lockin as much as the next guy, but skype \"just works\". you install it and you get what was promised, a easy to use program. you can search for friends, have a contact list, good quality sound, it can make calls to local phones, it's dead easy, and it's cross-platform. protocols are worth nothing by themselves, you need a decent easy to use program."
    author: "Mark Hannessen"
  - subject: "Re: Skype, your casual undocumented secret protoco"
    date: 2005-03-26
    body: "Skype DO use a derivative of SIP, but it's of course incompatibly with standard SIP."
    author: "lubba"
  - subject: "Jabber!"
    date: 2004-11-12
    body: "What about jabber and helix for voip and video?\n\nhttps://jabber.helixcommunity.org/"
    author: "am"
  - subject: "Re: Jabber!"
    date: 2004-11-12
    body: "that looks cool"
    author: "Pat"
  - subject: "Re: Jabber!"
    date: 2004-11-16
    body: "cool sounds like an understatement to me, since jabber also provides the ability for msn, icq, yahoo plugins, it might be a total killer feature. webcam and voice support has become something that people expect of a messenger today, many people feel the need to see there boy/girlfriend online while chatting every now and then and keep a windows install or pc around just for that... or even switch back to windows. webcam and voice support is a dire need."
    author: "Mark Hannessen"
  - subject: "Whats with the fuss?"
    date: 2004-11-12
    body: "No one is saying \"Kontact / Kopete is only going to supprot Skype, period\". Kontact and kopete already support many many proprietary protocols - MSN, AOL, Yahoo, Exchange. One more is not going to kill off the free software movement.\n\nAnd you rant about interoperability - what about interoperability with the 30 million existing Skype clients out there?\n\nIMO Skype should be given kudos for opening its API and *encouraging* developers to provide interoperability - it is far more than any of the other commercial IMs have done.Just adding support for Skype is not endoring Skype over SIP/Jabber (although Skype's voice quality is, in fact, far superiour to any SIP codec I have used at the same bitrate, and it works seemlessly behind my firewall, something SIP has yet to achieve).\n\nAdding support for Skype is not going to hurt anyone, and will help many who like it.\n"
    author: "Jason Keirstead"
  - subject: "Re: Whats with the fuss?"
    date: 2004-11-13
    body: "Just to clear something up, there is no such thing as a SIP codec. SIP provides a means for a caller to advertise the codecs that the softphone/hardphone supports. A phone could support one or a dozen. The callee's software picks whatever codec that they have in common based on it's own priority settings.\nSIP is a signalling protocol not a media protocol.\n\nI was doing research on a USB SIP phone that is offered in H323, SIP, and Skype versions and it turns out that the Skype phone uses the ILBC codec. This also happens to be one of the codecs available on Kphone.\n\nAs far as NAT traversal goes, I guess that Skype uses a proxy ( SIP server ) similar to what Free World Dialup uses. It is simple to set up your phone.\n\nSIP was designed to be extendable. SIP phones should ignore extensions they do not understand ( not crash ). Perhaps some feature would not work. To some extent a SIP phone could be propriatary."
    author: "Rob Dyck"
  - subject: "nice job fab!"
    date: 2004-11-12
    body: "i enjoyed those kde app quickies a lot."
    author: "acdc"
  - subject: "Re: nice job fab!"
    date: 2004-11-12
    body: "and to add to that the new Konqueror sidebar looks incredible"
    author: "acdc"
  - subject: "Re: nice job fab!"
    date: 2004-11-24
    body: "Hmm, I don't like it, for screen space reasons.\n\nIf you have more than three \"tabs\" in your sidebar, all the other tabs having their big title thing always sticking out will start to take up significant space; having 12 tabs on a small screen would probably leave very little space for the content.  Also, I haven't seen any screenshots of what it looks like with all tabs closed, but I assume that it continues to stick out and take up a lot of horizontal space, as opposed to the current behaviour which whisks into a tiny button bar.\n\nThat said, great work, you can't choose the best method if other methods aren't proposed."
    author: "kundor"
  - subject: "Patch frenzy"
    date: 2004-11-12
    body: "Why is this patch frenzy going on ? could it be possible that the core developers dont want popular changes going into the main code (see system try discussion on kde-devel) \n\nIf you look at the populatrity of some of the \"Kde improvements\" which are mostly over 90% i think everyone wants these features. \n\nwhat are the problems with these patches ? bad code ? bad design ? not useful ? no discussion ?"
    author: "Chris"
  - subject: "Re: Patch frenzy"
    date: 2004-11-12
    body: "\"could it be possible that the core developers dont want popular changes going into the main code\"\n\nIsn't that kinda harsh (and loaded) considering the number of patches that _are_ put in?\n\nFrom listening to a recent kde-devel discussion, it seems some people want to get it finished and polished before it goes into the main tree.\n"
    author: "JohnFlux"
  - subject: "Re: Patch frenzy"
    date: 2004-11-12
    body: "at least one of the hackers that issued a patchset just wasnt involved in KDE, and was not aware it was so easy to get involved (he was asked to send a mail to kde-devel, where one of the first questions was: do you want cvs-access so you can commit it yourself?).\n\nso its definately not that its hard to get involved in KDE, not at all. just someone didnt know...\n\nand for other patches, well, there is some discussion going on if these patches are really better and not just different... and another hacker, afaik, is just not involved in KDE (yet) and started to write this. now he's being asked to get CVS, too."
    author: "superstoned"
  - subject: "Re: Patch frenzy"
    date: 2004-11-12
    body: "Well, I think kde-apps is great for \"testing\" new ideas, not force them into kde. A patch needs to be tested and reviewed before its gonna be a part of kde. And its not true that no patch at all makes it in kde. Some parts of Andrunko \"Improving KDE\" patches are already in KDE, like the \"Sidebar saved with the profile\".\n\nOther bigger patches like the new sidebar look are harder to get in, because its a radical change to the look and feel of KDE, and im not sure if its a better implementation even if i came up with the idea; im not sure i could even prove its a better scheme for the sidebar.\n\nIn fact, this \"micro-patches frenzy\" is a wonderful thing, its full of original ideas, that anybody can try (and im sure some kde devs try them too).\n\nDont fear, if the idea is great, its will make it into KDE for sure.\n"
    author: "PaT"
  - subject: "Re: Patch frenzy"
    date: 2004-11-12
    body: "> see system try discussion on kde-devel\n\nerm, what about the system tray discussion on kde-devel, exactly?"
    author: "Aaron J. Seigo"
  - subject: "Re: Patch frenzy"
    date: 2004-11-13
    body: "i just wanted to point out that this is the discussion for one of the patches to be included. \n\nbtw. i like your work on cleaning up the kicker classes - that is really important work, a clean code and proper classes are the most important things that features get done right.\n\nbut i like systray icon hiding :>>>\n"
    author: "chris"
  - subject: "Re: Patch frenzy"
    date: 2004-11-13
    body: "yes, a number of people like the systray icon hiding, and the manual hide feature is one that is in demand and probably very useful to a number of people. it will probably end up in 3.4 but the code must be of release quality so that when it lands on your desktop it works properly and doesn't make it harder to add other features or fix other bugs.\n\nthose who read the threads on kde-devel will likely already know that this is exactly where we're at. there is no adversity to popular features, just bad features or poorly written features. icon hiding is not a bad feature (which is they the wish had been confirmed some time ago, actually =) we're just working on a proper implementation.\n\nit's a tough ballance to maintain between featureful software and something that's not going to disappoint due to a lack of stability or polish, but that's the goal with kicker these days."
    author: "Aaron J. Seigo"
  - subject: "kde-apps"
    date: 2004-11-12
    body: "Maybe a similar plattform is needed for document templates.\n\n"
    author: "gerd"
  - subject: "Re: kde-apps"
    date: 2004-11-12
    body: "i would like to see that the websites kde-look and kde-apps provide webservices and integrate with kde applications.\n\nhow about a extra button where your computer connects to kde-look to get more themes an buttons and wallpapers.\n\nor if its integrated with kde-apps you could perhaps download and install rpms/ebuilds or rpms.\n\nwhat do you think of that ?\n\nlike integration of windowsupdate.com with windows.\n\n"
    author: "chris"
  - subject: "Re: kde-apps"
    date: 2004-11-12
    body: "This feature is still waiting to happen. KDE features KHotNewStuff, which is a client-side infrastructure to look for new application-specific content on a website. It just needs work. Are you volunteering? :)\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Skype and Kontact/Kopete"
    date: 2004-11-15
    body: "I am very excited about the integration of Skype and Kopete. Skype is the only solution that I have found that allows me to do true multi-platform IM and voice calls without any work.\n\nBeing able to call a contact and storing his skype information in Kontact would be absolutely awesome.\n\nI would also love for Skype to be free software, but Skype is one of the first companies that treats Linux users with respect by providing timely updates to their Linux version. The fact that they have taken the care to develop a product that works well across a range of distributions is to be applauded as well. To all the naysayers who are screaming for a free software app, go and code it. When it works as well as Skype does out of the box, or at least when you have a decent 0.5 version, I'll be happy to send you a few bucks to continue working on it.\n\nFree software is our destination, but we must walk across bridges to get there. Skype has allowed me to bring new people to Linux, thereby increasing the critical mass that Linux already enjoys.\n\nPeace to everyone."
    author: "Anonymous"
  - subject: "K3B can't copy Mode2 XA Cdroms :("
    date: 2004-11-17
    body: "A wonderful application like k3b can't copy Mode2XA cdrom, and there are alot of them. \n\nExample of a Mode2XA cdrom is a normal VCD (Video CD). So K3b is very unhelpful for VCD duplications...\n\nplease support VCD copying... I am still stuck with nero :( \n\nThanks for a wonderful application."
    author: "Asif Ali Rizwaan"
---
Germany/France-based publishing house <A href="http://www.bomots.de/index.htm">bomots.de</A> announced this month the German book "<A href="http://www.bomots.de/k3b/index.htm">K3b kompakt - das offizielle Anwenderhandbuch</A>". This book talks about the famous CD and DVD burning application <A href="http://www.k3b.org/">K3b</A> which is <A href="http://extragear.kde.org/apps/k3b.php">part of</A> the <A href="http://extragear.kde.org/">Extra Gear collection</A>.  *** 

The <A href="http://www.gc-linux.org/">GameCube Linux project</A> reported that they have gotten KDE <A href="http://www.gc-linux.org/pic/kde.jpg">to run</A> on a GameCube (but with awful colors). ***

We clearly have a micropatch frenzy going on at <A href="http://www.kde-apps.org/">KDE-Apps.org</A> :-) <A href="http://www.kde-apps.org/content/show.php?content=13841">Some</A> of <A href="http://www.kde-apps.org/content/show.php?content=17669">these</A> <A href="http://www.kde-apps.org/content/show.php?content=16261">submissions</A> are <A href="http://www.kde-apps.org/content/show.php?content=17621">really</A> <A href="http://www.kde-apps.org/content/show.php?content=17732">interesting</A> and even <A href="http://www.kde-apps.org/content/show.php?content=16962">extremely popular</A>. ***

How about getting that <a href="http://www.kde-look.org/content/show.php?content=17750">cool Plastik look</a> for your SonyEricsson k700i? ***

Skype <a href="http://www.skype.com/company/news/2004/apibeta.html">announced</a> the first beta of their free and non-commercial API. Skype integration with Kopete and Kontact is the perfect excuse to <a href="http://pim.kde.org/development/common_index.php">get involved</a> in KDE-PIM development!


<!--break-->
