---
title: "Kluster @ aKademy"
date:    2004-08-29
authors:
  - "ateam"
slug:    kluster-akademy
comments:
  - subject: "Ok now..."
    date: 2004-08-29
    body: "That article was just mean spirited and evil... just bragging like that :'( !"
    author: "Corbin"
  - subject: "donations"
    date: 2004-08-31
    body: "Hmm, this sounds like little contribution - high effects. \n\nIt sometimes sound silly how small the contributions of the big players actually are. "
    author: "gerd"
---
At <a href="http://conference2004.kde.org/">aKademy</a> we got <a href="http://static.kdenews.org/fab/events/aKademy/pics/debiancluster/IMG_0003.JPG">really cool hardware</a> from <a href="http://www.transtec.de/">transtec</a> to accelerate the development by shortening compiling times with <a href="http://wiki.kde.org/tiki-index.php?page=icecream">icecream</a>. 

<!--break-->
<p>We did some quality tests for Debian and rebuild all Sarge packages with 2 blades (Intel Xeon based systems) from transtec and <a href="http://static.kdenews.org/fab/events/aKademy/pics/debiancluster/IMG_0002.JPG"> sponsored HP notebooks</a> which were on daily use in the <a href="http://conference2004.kde.org/tutorials.php">tutorials</a> and worked 2 days compiling Sarge.</p>

<p>KDE developer Diego Iastrubni comments: "It kicks ass, I have running 20 or maybe even 30 processes of 'make', someone even compiled OpenOffice.org because he was bored."</p>






