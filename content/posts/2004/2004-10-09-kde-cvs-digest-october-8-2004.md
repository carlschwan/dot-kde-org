---
title: "KDE-CVS-Digest for October 8, 2004"
date:    2004-10-09
authors:
  - "dkite"
slug:    kde-cvs-digest-october-8-2004
comments:
  - subject: "Its good to be the first one"
    date: 2004-10-09
    body: "Thanks Derek. It is always good to read news from Krita,\nand is especially good to know that gmail is going to work with konqueror!\n\n"
    author: "Oliveira"
  - subject: "Re: Its good to be the first one"
    date: 2004-10-09
    body: "It looks like the GMail fix was backported to 3.3. This means its going to be in the 3.3.1 release correct?"
    author: "Ian Monroe"
  - subject: "Re: Its good to be the first one"
    date: 2004-10-09
    body: "> This means its going to be in the 3.3.1 release correct?\n\nEither it will be not or it will delay the 3.3.1 release significantly."
    author: "Anonymous"
  - subject: "Embed safari rendered in konqueror, possible?"
    date: 2004-10-09
    body: "Just a thought would it be possible to embed safari's version of\nkhtml in konqueror?  That way konqueror would have access to 3\nrender engines, being native khtml, gecko and safari.\nIf it would be possible, users could benefit of (all) improvements made to\nsafari, while not all improvements are merged with khtml.\n\nLooking from far far away; embedding gecko into konqueror seems more\ndifficult than embedding safari's renderor."
    author: "Richard"
  - subject: "Re: Embed safari rendered in konqueror, possible?"
    date: 2004-10-09
    body: "I'm not a programmer, but I think that Safari depends too much on macOS to get it easily integrated in Konqueror.\n"
    author: "Rinse"
  - subject: "DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "I think many people who are really defending DCOP are missing the essential truth, which is that KDE can no longer stand alone within a computer system. I saw some of the in the Freedesktop talk at akademy, and virtually no developer grasped it there. KDE has to communicate with lower level components like HAL and has to communicate better with CUPS to provide the better integration people need. These components are outside the domain of KDE.\n\nBecause DCOP is essentially tied to Qt practically speaking, this is just never going to happen. You would be dictating to other projects that they effectively must use Qt to communicate, and this just isn't going to happen. Even if they aren't religious, putting a Qt dependency on a project is totally ludicrous. None of these projects are going to do it with glib or other components, they most certainly won't do it with Qt."
    author: "David"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "I do agree. DCOP may be good as an inter application message protocol for applications running in the same session. DBUS designed to dispatch messages on the system level so it's good way to communicate with the systems components outside KDE.\n\nCan't we just use DCOP and DBUS? All what I read about says that one is lacking the features of the other one, and vice versa... So why not use both?"
    author: "Nolridor"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "Hi\n\nMaking use of both might be a feasible option in kde for the short term and this is being discussed in the mailing lists. Having to maintain both with possible overlaps is an overkill. \n\nDBUS is a better system level IPC and it can probably be extended to do what dcop does. after all dbus was designed after dcop and has explicit designs to stay compatible to the extend possible"
    author: "Rahul Sundaram"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "And what if no <kde4 app will run? What if it opens the linux desktop to worms and insecurities? What if it locks when it gets busy?\n\nThose are some possibilities with the current dbus. It isn't being used yet.  There hasn't been any performance testing yet.It is a good idea, but as usual with this stuff, the devil is in the details. Maybe dbus isn't the best idea for an ipc system. Maybe something else would work better.\n\nDid I mention security? From what I understood of the discussion, each application that connects to the bus is to allow or disallow as desired. Is this good, or asking for trouble? If by design it communicates hardware details, someone has elevated privileges. Is it a good idea that every hacker can depend on dbus running, the same daemon, same codebase everywhere?\n\nAll this can be and probably will be fixed. Hopefully. One thing most Kde developers grasp is that a bad implementation of a good idea is a lot of bother.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "I agree. If Gnome actually picks up DBUS, thats a strong reason to work with it. I found the following to be a pretty useful introduction to DBUS:\nhttp://freedesktop.org/Software/dbus/doc/dbus-tutorial.html \nFrom my user point of view I just want whatever replaces DCOP to be as easy to use. DCOP is really handy for making keybindings and such.\n\nGoing with the flow should dictate what multimedia server that is eventually chosen. Conflicting ways of doing sound on Linux/Unix has been a real thorn in the side of desktop use.\n\nDoesn't KDE pretty much use CUPS already? One of the reasons I chose CUPS was its strong support in KDE 3. Printing is one of KDE's strongest areas. For non-KDE apps I just have them print to the command kprinter and I don't have to worry about it.\n\nHehe,I don't think anyone has suggested that non-KDE apps should start opening DCOP slots. Though maybe I just haven't been paying enough attention."
    author: "Ian Monroe"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "Well, CUPS was just an example really. If you plugged in a printer you should automatically know about it, and if possible, have it autoconfigured through CUPS."
    author: "David"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-10
    body: "If it were a USB printer, yea, ok, I might agree, there could be some way for \"KDE\" to pop up a message box saying that it noticed a printer just plugged in and ask what to do with it.  I have a USB flash drive and KDE doesn't seem to notice when I plug it in.  The kernel dumps it's usual messages and I click my little icon on the desktop to access it, but, KDE doesn't do anything special when I plug it in or remove it from my system.\n\nIf the printer is a parallel print then obviously a \"hot plug\" daemon in KDE to monitor devices just plugged in would be usless since the BIOS of the machine needs to see that there's something there.\n\nOr maybe I'm just misunderstanding what you're suggesting...\n\nM.\n"
    author: "Xanadu"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-10
    body: "You have to remember that KDE is not just for Linux and such hardware configuration is a job that has traditionally been left up to kernel devs and Linux distributions themselves to work out. Certainly it would be ideal that a user just plug in their printer and have it work. Given that cups already runs a daemon, seems like cups would be the correct group to implement it.\n\nThere is a hot plug daemon and its called hotplug. Granted hotplug just inserts the correct modules and whatnot. I suppose KDE could give annoying/comforting messages whenever it noticed a new module inserted (or a subset of those modules)  into the kernel. Actually its the kind of system tray utility that a KDE dev could probably whip up over a weekend (though I personally don't see the need for it, especially since I don't use autofs)."
    author: "Ian Monroe"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-11
    body: "\"I have a USB flash drive and KDE doesn't seem to notice when I plug it in. \"\n\nThis is what project utopia is all about (which uses HAL btw).\n\n"
    author: "JohnFlux"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-09
    body: "Yes, Control Center integration of Yast-Toolset. Integration of emulators such as dosbox.."
    author: "gerd"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-10
    body: "> I think many people who are really defending DCOP are missing the essential truth, which is that KDE can no longer stand alone within a computer system.\n\nAm I missing something? Defending DCOP? I'm just seeing developers ask intelligent questions, and there are a lot to be asked before proceeding. Also KDE has more inclusive hacks than anyone. You can run X applications as a KPart and even load ActiveX components. This is an unfair statement.\n\n> I saw some of the in the Freedesktop talk at akademy, and virtually no developer grasped it there.\n\nAgain, how can you make this absolute statement? Aaron for instance has done a lot with freedesktop.org and I'm curious what only you grasp?\n\n> Because DCOP is essentially tied to Qt practically speaking, this is just never going to happen.\n\nYou could say DCOP is tied to Qt, but it's based on X11 ICE. Anyway I don't think there is any question that there are some interesting things being done with DBUS that we want to take advantage of. For me one of the most interesting thing is the Qt4 wrappers making it less of a pain to deal with.\n\nThe point is that before we drag DCOP out to the curb with the coffee grounds and swear allegiance to DBUS we need to know that we have done the right thing by users... because you will complain if things slow down and we will have fingers pointed at us if we have security issues. DCOP is being used extensively in KDE. We are using it now for scripting in Kommander. I'm particular interested in real world performance because I can run several thousand DCOP calls a second right now. Performance is a real issue for Kommander.\n\nI think DBUS will probably be a good thing, but the last thing I want to do is stumble in blindly because it's trendy, get to talk to hardware and lose a responsive desktop. You have to follow sound procedure. Peer review is what open source is about, and you shouldn't get upset that people are reviewing instead of accepting it as gospel."
    author: "Eric Laffoon"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-10
    body: "\"I'm just seeing developers ask intelligent questions, and there are a lot to be asked before proceeding. Also KDE has more inclusive hacks than anyone.\"\n\nThey are asking intelligent questions - but in the wrong context. DCOP is a system that exists purely within the domain of KDE. Yes, it uses ICE, but to use usefully it as it stands it really is Qt specific.\n\n\"Again, how can you make this absolute statement? Aaron for instance has done a lot with freedesktop.org and I'm curious what only you grasp?\"\n\nAaron was about the only person who did understand it.\n\n\"The point is that before we drag DCOP out to the curb with the coffee grounds and swear allegiance to DBUS we need to know that we have done the right thing by users...\"\n\nQuite right."
    author: "David"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-10
    body: "\"Aaron was about the only person who did understand it.\"\n\nI was at the aKademy talk, and I have to say that you're talking rubbish! KDE developers know perfectly well how DCOP and D-BUS work, and their relative merits. The only issue we have is the best way forward, as discussed recently on the kde-core-devel list and reported by Derek. \n\nIn fact I had a late night discussion there with Aaron and others, where he was acting as the Devil's Advocate, and was arguing against adopting D-BUS. \n\n\"but to use usefully it as it stands it really is Qt specific\"\n\nThe Qt specific thing about DCOP is the QDataStream <-> QByteArray marshalling, and nothing else. That's published and could have been used by D-BUS. The fact that it wasn't doesn't particularly matter anyway.\n\n "
    author: "Richard Dale"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-11
    body: "\"Aaron was about the only person who did understand it.\"\n\nMaybe the whole \"reading\" thingy is not your strong side, but if you'll take a look at the D-BUS AUTHORS file you'll see two kde.org addresses there."
    author: "Correction"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-12
    body: "What I understood from the freedesktop.org presentation is that DBUS is just a start. DBUS is useless unless people start using it.\n\nFrom the Kde standpoint we have a working IPC mechanism. The desktop depends on it. If you read the thread, Ian said that the most common usage for IPC right now is asking if something is running. I plug things into my box maybe a few time a month, but I depend on what DCOP does every time I sit down.\n\nSo how do we, you included (I hope to see some patches) make Kde work as well as it does now with DBUS?\n\nIt isn't simple, and it potentially is a pile of work.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-10
    body: "Sorry, but throwing away a working, proven, stable and cross-platform IPC mechanism (as DCOP is) and breaking backwards compatibility completely just because there is some new kid on the block and it's trendy, THAT is ludicrous. If there are KDE applications which have a need to talk to DBUS they're free to do this, I haven't heard that you can't have DCOP and DBUS together in the same application. Nobody demands that other projects implement DCOP. If DBUS proves to have all the functionality, the reliability, performance and cross-platform abilities (yes, we run KDE on Solaris here and there aren't any Linux kernel events on that system) of DCOP then it might be a replacement but so far I haven't seen any applications really using it. Until then stick with the proven DCOP and implement DBUS where necessary IMO. BTW, I have the impression the KDE developers are grasping it very well in general.\n\nkk.\n"
    author: "ac"
  - subject: "Re: DBUS and DCOP Hoopla"
    date: 2004-10-11
    body: "\"..breaking backwards compatibility completely just because there is some new kid on the block and it's trendy, THAT is ludicrous.\"\n\nIt's not just because there is a new kid on the block. It's very important for KDE to interoperate with other toolkits. Because DCOP depends on X11 ICE it means you can't easily have a native Mac OS X version for instance, whereas with a sockets based ipc mechanism that's no problem.\n\n\"If DBUS proves to have all the functionality, the reliability, performance and cross-platform abilities\"\n\nI see no reason why it won't. It seems to me that it has been carefully engineered not to be over-engineered, and to be compatible with DCOP.\n\n\"yes, we run KDE on Solaris here and there aren't any Linux kernel events on that system\"\n\nWell the whole idea of the system D-BUS is to raise the level of abstraction to not depend on things like kernel events. If someone inserts a CD in the disk drive, the D-BUS signal should be the same on Solaris as Linux even though the origination of the event might differ.\n\n\"BTW, I have the impression the KDE developers are grasping it very well in general.\"\n\nYes, I agree KDE developers understand this well, along with the Gnome developers. If only we could coordinate on File Open/Close dialogs and other UI elements just as well.."
    author: "Richard Dale"
  - subject: "DBUS vs DCOP"
    date: 2004-10-10
    body: "Pardon my ignorance, I haven't read the DBUS spec yet. Does it use the X11-style IPC like DCOP?\n\nIt is important that DBUS traffic flows along with the underlying X11, so thin clients remain a flexible and powerful solution."
    author: "Jonas"
  - subject: "Re: DBUS vs DCOP"
    date: 2004-10-10
    body: "\"Does it use the X11-style IPC like DCOP?\"\n\nNo, it doesn't use ICE, it uses sockets. Which means it doesn't depend on X11, and that's a good thing."
    author: "Richard Dale"
  - subject: "Re: DBUS vs DCOP"
    date: 2004-10-10
    body: "So how do I use it between programs on different hosts? Is it supposed to be forwarded by SSH along with the X11 session?"
    author: "Jonas"
  - subject: "Re: DBUS vs DCOP"
    date: 2004-10-10
    body: "Hmm, finally someone that might know how that works with DCOP. See my question at http://lists.kde.org/?l=kde-devel&m=109351129624025&w=2\nPlease, can you give (a pointer to) an example how that works?"
    author: "koos"
---
In <a href="http://cvs-digest.org/index.php?issue=oct82004">this week's KDE-CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=oct82004">experimental layout</a>):

<a href="http://www.koffice.org/kspread/">KSpread</a> improves Gnumeric export filter.
<a href=" http://www.koffice.org/krita/">Krita</a> adds a crop tool.
<a href="http://www.koffice.org/kexi/">Kexi</a> adds database command line options.
gmail.google.com now works in <a href="http://www.konqueror.org/">Konqueror</a>.
Kicker clock supports NTP.
Whither DBUS and KDE?

<!--break-->
