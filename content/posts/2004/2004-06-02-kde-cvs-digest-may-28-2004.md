---
title: "KDE-CVS-Digest for May 28, 2004"
date:    2004-06-02
authors:
  - "dkite"
slug:    kde-cvs-digest-may-28-2004
comments:
  - subject: "What are 'spring loading folders'?"
    date: 2004-06-02
    body: "Can anyone tell me what 'spring loading folders' are?"
    author: "Florian Roth"
  - subject: "Re: What are 'spring loading folders'?"
    date: 2004-06-02
    body: "If you drag and drop something and hold in-between above a folder it opens."
    author: "Anonymous"
  - subject: "Now we should have \"Folder Actions\""
    date: 2004-06-02
    body: "Speaking of interesting Apple features, something along the lines of \"Folder Exec=\" would be great. It should be possible to attach a script to a folder that gets executed instead of opening the folder in Konqueror. \n\nIn KDE, this should be realized by making the Exec= line work in folder type .desktop files. If you, too, would like to see this feature, please vote on http://bugs.kde.org/show_bug.cgi?id=81772"
    author: "josh"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-02
    body: "I wonder about the security implications. If I try to browse a directory, I don't think I'd want programs opening behind my back instead. Perhaps if the folder actions were accessible from a right-click menu only..."
    author: "AC"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-02
    body: "You should always know what you install in the fist place! \nThere could even be a special icon for folders that have an Exec= attached to them. And of course it should be possible to disable altogether."
    author: "josh"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-02
    body: "A seperate icon sounds good. But then if it's not going to look like a folder or act like a folder, what's the benefit of it actually _being_ a folder? You could just make it a non-directory .desktop file that could look like anything you want and run anything you want when clicked... including starting up some app to view a certain \"real\" folder."
    author: "AC"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-03
    body: "The benefit of an AppFolder is that it contains everything that is needed to run a certain application, e. g. libraries, help pages, icons, etc. - all in one folder that you can easily \"install\", move around, and delete. Having a separate .desktop file outside the folder can come close, but then not everything is *inside* the folder, which is one key strength of AppFolders on the Mac. So really, folder-type .desktop files should have Exec= implemented. "
    author: "josh"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-03
    body: "I don't really think it's that interesting for KDE. On MacOSX the 'folder is the program' system is working fine, however, on Linux we can't even move our programs freely around, yet.\n\nWhy implement this, when we can't really use it in a meaningful way?\n\nbtw. check Autopackage.org"
    author: "SF"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-03
    body: "Yes, AppFolders are both possible and useful. I am working with self-made Linux AppFolders on a daily basis. "
    author: "josh"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-04
    body: "I am starting to like your idea more and more. I downloaded Mozilla the other day as a .tar.gz, and it seemed to be totally self-contained inside the folder and didn't need an install process.\n\nAnd I suppose my security concerns aren't such a big deal. You can already create a .desktop file that has its icon set to look like a folder but executes some nasty program, I bet.\n\nAn emblem might be nice though, like the symlink/shortcut emblem..."
    author: "AC"
  - subject: "Re: Now we should have \"Folder Actions\""
    date: 2004-06-04
    body: "If it is of any interest the KDE darwin project has put together a script to build bundles out of the kde .desktop files.  Just need to make an ioslave that can recognize .app folders. \n\n-Benjamin Meyer"
    author: "Ben Meyer"
  - subject: "spring-loaded folders"
    date: 2004-06-02
    body: "If you are dragging something, and hold the pointer while dragging over a folder for some time (or press the spacebar) the folder will open. This allows you to drill down quickly in a directory hierarchy while dragging -- otherwise you'd have to park your stuff somewhere, click through the hierarchy, and then drag it to its destination.\n\nIt's actually quite a handy feature -- I use it a lot when forced to work on OS X, like now when my regular laptop is broken."
    author: "Boudewijn"
  - subject: "Re: spring-loaded folders"
    date: 2004-06-02
    body: "Too bad this works only in konqueror and not on the desktop. All my dokuments are organized in folders on the desktop. (document centric work) Now I can\u00b4t use this great feature.\n\nIt\u00b4s the same problem with the \"folder icon reflects content\" feature. This is a great innovative feature of KDE. But only half implemented. It works only in konqueror and not on the desktop or in the file open dialog. :-(\n\nAnother problem of the the 3.3 features is the \"type agaid find\" in konqueror. This is again a great example of a powerful, great new feature in KDE. But NO! user is able to find it!! You have to type '/' before the search string. Do you expect that the users find this feature by accident?\nI don\u00b4t understand why it is not possible to make it compatible with mozilla and firefox. (You don\u00b4t need to type the '/')\n\nHans"
    author: "Hans"
  - subject: "Re: spring-loaded folders"
    date: 2004-06-02
    body: "Well if an entry for \"Find as you Type\" (and \"Find Links as you Type\") is added to the edit menu, just next to Find, listing the '/' shortcut, then users will find out about it the same way. The problem with no shortcut at all is that it would make it impossible to use when KHTML is embedded in other applications who require single key shortcuts."
    author: "John Searching Freak "
  - subject: "Re: spring-loaded folders"
    date: 2004-06-02
    body: "The '/' search function is great. The '/' is needed because some alphabetic keys already have a meaning (for example you can scroll the page with the hjkl keys). It's not so hard to find. I discovered it by chance, and it works as in most console based paging software (like less, or more). \nMaybe the fact that I use vim as my everyday editor explains why I found the feature so easily, however!"
    author: "Luciano"
  - subject: "Re: spring-loaded folders"
    date: 2004-06-02
    body: "Yeah, me too :-)"
    author: "garaged"
  - subject: "Re: spring-loaded folders"
    date: 2004-06-02
    body: "When forced to use OS X?\n\nI'm bleeding for you, truly I am.\n\nSorry but Column View ala NeXTSTEP is the only view worth a shit to browse a folder's contents, and if you are well-versed in NeXTSTEP you can quickly adapt special dwrites to OS X that add even more grace to that stellar operating system.\n\nSpring loaded folders was re-implemented for the whining faithful of pre-OS X days.\n\nNothing like having to go 5 levels deep with spring loaded folders when one can just go ~shortcut to exactly where you want to go in Column View with the NeXT paradigm.  That elusive ease of use seems to say one step is better than five steps.\n\nIf you don't know the dwrites of OS X I suggest researching them."
    author: "Marc J. Driftmeyer"
  - subject: "Re: spring-loaded folders"
    date: 2004-06-02
    body: "No need to bleed... I primarily keep that old Powerbook (a 2000 Pismo) around to check whether I've introduced new endian-ness bugs in Krita. I prefer to do real work on my new Dell laptop which is six times as fast when compiling Krita and has a delete key to boot. To say nothing of the vastly superior way Freetype renders fonts. Only now my Dell is in Birmingham for a keyboard replacement, so I have to do my work on that old, slow Powerbook.\n\nAnyway, I do like the column view, but I find hitting the little icon a bit hard; it seems dropping on the text doesn't do the trick in the column view. And I do know the origin of OS X and all that -- and I've even got a tip for you: GNUStep's Workspace will give you all the columns you crave."
    author: "Boudewijn Rempt"
  - subject: "Re: spring-loaded folders"
    date: 2004-06-04
    body: "This sounds like a nice feature, but in the past some dragging operations seemed a bit buggy to me. For example, sometimes just dragging a file over a particular window or the desktop causes either one or both to crash, even though neither windows are the intended target.\n\nBUT, this may have actually been resolved in one of the more recent releases, because most of the time drag and drop works fine! =)"
    author: "Archwyrm"
  - subject: "Interesting"
    date: 2004-06-02
    body: "\"Kontact adds support for SUSE Linux OpenExchange\"\n\nNice to see the groupworking options growing steadily in Kontact as a whole.\n\nKontact really is a very nice application, and everybody I have introduced it to who've used Outlook for many years have absolutely loved it. It seems as though many people know enough about Outlook to use other personal information managers, and using that knowledge they find all of the good additional stuff in Kontact.\n\nI don't know why other things get hyped as enterprize grade, and an application like Kontact gets overlooked."
    author: "David"
  - subject: "Re: Interesting"
    date: 2004-06-03
    body: "If only they could add support for outlook exchange, using ximian-connector. It would be the best email client around."
    author: "Trucker"
  - subject: "Re: Interesting"
    date: 2004-06-04
    body: "Does OpenExchange speak the MS Exchange protocol? Also what license is OpenExchange under at the moment, I heared it's or going to be GPL?"
    author: "Anonymous"
  - subject: "Re: Interesting"
    date: 2004-06-05
    body: "OpenExchange is a proprietary product as it relies on a proprietary database. SUSE does have the intention to switch to a free internal database to be able to open the entire product, but AFAIK no details are given as to how or when that should happen."
    author: "Arend jr."
  - subject: "Re: Interesting"
    date: 2004-06-05
    body: "> but AFAIK no details are given as to how or when that should happen.\n\nMy bet is that we have to wait at most until BrainShare Europe 2004. :-)"
    author: "Anonymous"
  - subject: "Apple's patent?"
    date: 2004-06-03
    body: "The spring-loaded feature is very nice, but what about Apple's patent on it?"
    author: "coulamac"
  - subject: "Re: Apple's patent?"
    date: 2004-06-03
    body: "At least in EU they are not valid yet, lets do our best to keep it that way."
    author: "John No-EU-Soft-Pantents Freak"
  - subject: "Re: Apple's patent?"
    date: 2004-06-03
    body: "Hmm... Apple's patent? MS won't be very happy about it :D\nWin9x has this feature for a long time."
    author: "KDE User"
  - subject: "Re: Apple's patent?"
    date: 2004-06-04
    body: "Really? I have never noticed this. Now I am going to have to try hard to <b>find</b> a windows machine to test it out.. lol"
    author: "Archwyrm"
---
Enjoy this issue of <a href="http://cvs-digest.org/index.php?issue=may282004">KDE CVS-Digest</a>:
Code folding and syntax highlighting improvements in <a href="http://kate.kde.org/">Kate.</a>
<a href="http://www.kdevelop.org/">KDevelop</a> has a new file template system. 
<a href="http://kgeography.berlios.de/">KGeography</a> adds more maps and flags.
<a href="http://digikam.sourceforge.net/">Digikam</a> improves EXIF tag editing and display.
<a href="http://kmail.kde.org/">KMail</a> adds detailed new mail notification and anti-virus tool support.
<a href="http://www.kontact.org/">Kontact</a> adds support for SUSE Linux OpenExchange Server.
<a href="http://www.konqueror.org/">Konqueror</a> adds spring loading folders.
<a href="http://extragear.kde.org/apps/kmyfirewall.php">KMyFirewall</a> adds rule plugins.
<!--break-->
