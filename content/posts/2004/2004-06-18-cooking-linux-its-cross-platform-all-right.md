---
title: "Cooking with Linux: It's a Cross Platform, All Right!"
date:    2004-06-18
authors:
  - "mgagn\u00e9"
slug:    cooking-linux-its-cross-platform-all-right
---
Marcel Gagné's latest "<a href="http://www.marcelgagne.com/ljcooking.html">Cooking with Linux</a>" over at the <a href="http://www.linuxjournal.com/">Linux Journal</a> covers some nice graphical ways of dealing with or joining the MS Windows <i>network neighborhood</i>, lately referred to as <i>network places</i>. <a href="http://www.linuxjournal.com/article.php?sid=7526">The article</a> shows you how to use Konqueror or Nautilus to access Samba or Windows shares, and it introduces the very cool <a href="http://smb4k.berlios.de/">SMB4K</a>, a powerful SMB share browser for KDE.




<!--break-->
