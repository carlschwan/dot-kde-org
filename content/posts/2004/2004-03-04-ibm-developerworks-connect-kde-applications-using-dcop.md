---
title: "IBM developerWorks: Connect KDE applications using DCOP"
date:    2004-03-04
authors:
  - "binner"
slug:    ibm-developerworks-connect-kde-applications-using-dcop
comments:
  - subject: "Great article"
    date: 2004-03-04
    body: "this is a great article for those who want to discover DCOP !\nThanks Martyn.\n\nI would like to know if it is possible with DCOP to get all the windows names that are opened in the kicker and force an application to \"blink\" in the kicker.\nI would like to create a script for a non-KDE application that notify the user for an event in the application.\nthanks if you have any idea.\n"
    author: "Nicolas Blanco"
  - subject: "Re: Great article"
    date: 2004-03-04
    body: "Should be possible with one of the \"dcop knotify default notify\" functions if you know the right parameters. Who knows these? :-)"
    author: "Anonymous"
  - subject: "Re: Great article"
    date: 2004-03-08
    body: "From kdelibs/arts/knotify/notify.h:\n\n        void notify(const QString &event, const QString &fromApp,\n                         const QString &text, QString sound, QString file,\n                         int present, int level, int winId, int eventId);\n\nNo idea what they mean though :)"
    author: "David Faure"
  - subject: "dcop params"
    date: 2006-12-13
    body: "Here you go:\n\ndcop knotify Notify notify notify Me \"Segmentation fault!\" nosound nofile 2 0\n\nFrom http://women.kde.org/tips/kdetips.php ."
    author: "Krishna Sethuraman"
  - subject: "dcopstart"
    date: 2004-03-04
    body: "I wonder why the article didn't use 'dcopstart'. It seems to me that this:\nkonqueror &\ndcop konqueror-$! etc\n\nwould have a tendency to break, depending on how fast konqueror started up."
    author: "AC"
  - subject: "bring top and dcop"
    date: 2004-03-05
    body: "Hello,\nHow is possible to bring a window in front of others?\neg. bring Konsole window to top.\n\nThanks.\n"
    author: "milan"
  - subject: "Re: bring top and dcop"
    date: 2004-03-05
    body: "dcop_id = `dcop | grep konsole | head -n 1`\ndcop $dcop_id konsole-mainwindow#1 raise\ndcop $dcop_id konsole-mainwindow#1 focus\n\nOr something along the lines should do it."
    author: "he-sk"
  - subject: "Virtual Desktop"
    date: 2004-07-03
    body: "First, this is an inspiring article. But the first thing I wanted to script is mystifyingly elusive.\n\nIs there a way to determine a particular window's current virtual desktop? The idea being to, upon some event, check to see if that window is on a different desktop and, if so, move it to the current desktop. \n\nSearching the DCOP methods I cannot find any way to do this. I have found no information anywhere pertaining to the manipulation of virtual desktop related info via DCOP, sadly.\n\nThanks."
    author: "Idan Waisman"
---
Martyn Honeyford from IBM's UK Labs <a href="http://www-106.ibm.com/developerworks/linux/library/l-dcop/index.html?ca=dgr-lnxw12ConnectKDE">introduces KDE's desktop communication protocol</a> (DCOP) on the <a href="http://www.ibm.com/developerworks/">IBM developerWorks site</a>. He describes how to utilize it with the graphical kdcop and the command line dcop utilities with several examples such as instantly messaging a contact in Kopete when he comes online and also how to create a simple DCOP-aware application.


<!--break-->
