---
title: "eWEEK: GNOME, KDE Aim at Windows"
date:    2004-09-25
authors:
  - "binner"
slug:    eweek-gnome-kde-aim-windows
comments:
  - subject: "Summary"
    date: 2004-09-25
    body: "The article:\n\nPans Kontact in favor of Evolution.\n\n1) \"... when we were using the down-arrow key, Kontact insisted on scrolling downward through the current message ..., rather than through messages in its message-list pane.\"\n2) \"We also had trouble with Kontact's spam filtering...\"\n3) \"Another thing that annoyed us about KDE's mail handling was the way it dealt with HTML messages.\"\n\nThe first too seem to be genuine usability bugs, but the last is a security *feature*.  I don't think we ought to compete with MS and GNOME by enabling security vulnerabilities by default.\n\nPraises Kopete for (in part) having the option to look like Gaim.\n\n1) \"We particularly appreciated the option of having Kopete render our chats to mimic other IM clients, such as the Gaim client.\"\n2) \"We also liked the tie-in between KDE's Konqueror file manager and Kopete...\"\nHmm, didn't seem to mention the \"Send Message KAction That Must Not Be Named\" ;)\n\nNotes that GNOME 2.8 apparently has a kioskalike although it prefers Waldo's kiosktool GUI to gconf-editor.\n\n...\n\n\"A nice improvement we found in GNOME 2.8 was the capability for auto-mounting removable devices such as USB (Universal Serial Bus) thumbdrives.\"\n\nWorks just fine for me with the default desktop for SuSE 9.1 ;)"
    author: "manyoso"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "> 3) \"Another thing that annoyed us about KDE's mail handling was the way it dealt with HTML messages.\"\n\nThat part of the story is bogus. Quote: \"we'd prefer that Kontact provide the option of rendering the message in a \"sanitized\" form-one that doesn't fetch remote images or objects.\" Guess what the deselected \"Allow messages to load external references from the Internet\" option does?"
    author: "Anonymous"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "It's not bogus. Evolution shows the neutered html in rendered form, and KMail shows the html in source form. Personally I like it rendered."
    author: "-"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "Well have you tried changing the settings? \nSettings->Configure  Kmail->Security->Reading tab(General on older vers.) under HTML settings.\nOr you can set it (in neutered HTML if you don't change it in Security) per folder with Folder->Prefer HTML to plain text.\n\nThese options have existed in KMail since, not sure but at least since the KDE2.0 or 2.1 days. And when you press on the link in the note box displayed when you see the source of a HTML message and then get the HTML in rendered form. It does not take much brainpower to understand since it can display HTML, it's possible to set this to default behavior. Probably take 1-2 minutest to find in the config-panel, less if you read the note in the nonrendered mail (the part claiming security reasons to not render HTML).\nI don't think bogus are far of the mark."
    author: "Morty"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "KMail is lacking the most sensible option however: \"Prefer plain text, but display HTML when only HTML is available\"\n\nI can set KMail to prefer HTML but then mails which are sent as both HTML and plain text will be rendered as HTML. Or I can set KMail to not render HTML, but then I miss out on HTML-only mails.\n"
    author: "Anonymous"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "You don't miss out on HTML only mail, its just one click away. Haven't you noticed the nifty red box containing this message?\n\nNote: This is an HTML message. For security reasons, only the raw HTML code is shown. If you trust the sender of this message then you can activate formatted HTML display for this message by clicking here.\n\nI don't think the option of displaying HTML when no plaintext are available are particularly sensible, there are reasons to not open HTML mail you know. And besides do you want HTML mails or not. If you don't the sane thing are to be able to manually override when necessary, not the other way. "
    author: "Morty"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "This annoys me also. I'd much rather read the text mail when it's available and not the HTML version, but not have the extra click to open HTML only email. It may not seem like much, but when you receive a lot of HMTML only email each day it gets old very fast.\n\n"
    author: "Greg"
  - subject: "Security"
    date: 2004-09-27
    body: "I see your point -- but of course this would completely defeat the security aspect of not rendering HTML. From a security standpoint you might as well always render HTML mail."
    author: "Martin"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "I agree. People only need to read the information Kmail presents. Doing so and they are but a click from reading their html mail. I personally think html mail should be banned but that's me. The vast majority, well over 90% of html mail I get are from spammers anyway. The few I get from trusted sources is not enough IMO to warrant a change in Kmail's behavior. Frankly I am annoyed by those who insist sending html mail.\n\nI remember the first time I saw Kmail's warning about html mail and thought, now that's a nice touch and felt a bit more safer reading my mail.\n\nMy two cents say leave the way Kmail handles it the way it is and make those who want html stuff work to get it. I know having to do an extra mouse click or two is really time consuming:)"
    author: "stumbles"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "> 1) \"... when we were using the down-arrow key, Kontact insisted on scrolling > downward through the current message ..., rather than through messages in its > message-list pane.\"\n\nLeft and right scroll up and down the message list, up and down scroll through the current message.  This means that you don't have to switch focus between message pane and list pane but also makes it slightly less intuitive (took me a while to realise)."
    author: "Mark"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "Yep, the feature is useful, but unintuitive, and different from the way every other list box in KDE behaves. \n\nBecause of that the feature should not be enabled by default. \n\nAbout usability testing in general: All feedback is good feedback, no matter whether it's from journalists, lab experts or from users participating in a usability study. They are not after us with a hidden agenda to make us look bad, but they highlight cases where our software didn't fulfill their initial expectations. It's up to us what we do with it. In this case I would say that the expectation that a list box should scroll when it has focus and I press the Up and Down arrow keys is a perfectly normal and understandable expectation. I fell into the same trap. When I first noticed it I started debugging and wanted to send Don a patch, unfortunately I wasn't very convincing.\n\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "I'm all for consistency between apps and intuitive defaults, still I cannot see a great workaround in this case... \nIn your patched kmail how would you, say, go 2 msgs up, scroll message itself,  2 msgs up, scroll message itself again? Would you need to change focuseach time you want to move in message list/scrool msg itself? Hell I don't even know how to change focus with my keyboard so I'd just end up using the mouse."
    author: "John Kmail Freak"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "You can change the focus with the Tab key. Tab goes forward, Shift+Tab goes back. This is the way just about everything else works, and this is how Kantact should do it, too."
    author: "Mitchell Mebane"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "Thanks, I didn't know about Tab/Shift Tab. Again, I like consistency, but you do agree that working that way with kmail would be a pain no? Just imagine scrolling one message, then changing to another folder, select the message you want and scrolling it. Compare both ways of doing it.\n"
    author: "John Kmail Freak"
  - subject: "Re: Summary"
    date: 2004-09-29
    body: "From a functionality point of view, the current way is definately better. However, I never noticed the left-right thing for scrolling through messages, and always gave up and used the mouse."
    author: "Mitchell Mebane"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "\"Because of that the feature should not be enabled by default.\"\n\nOnly because some users (not developers!) don't understand it?\n\n* Better show a message box the first time somebody uses the arrow keys inside Kmail/Kontact.\n* Maybe the key-binding tool should get a more prominent place so that reviewers actually use it.\n* Maybe the toolbar configuration tool should me mentioned more often so that reviewers actually use it instead of complaining about to cluttered user interfaces.\n"
    author: "Asdex"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "\"\"Because of that the feature should not be enabled by default.\"\n\nOnly because some users (not developers!) don't understand it?\"\n\nYes, that's exactly why... Pardon me if I misunderstood your tone, and you actually mean that literally rather than as a point-making question. Not everyone who uses KDE is a developer. I would venture even to say that most people who use KDE aren't. And if that isn't the case, I think that's what the desktop is aiming for. Seriously, what is this elitist attitude that the more intuitive way of functioning shouldn't be enabled by default because developers can understand the counterintuitive way of doing things? Those developers are also more capable than regular users of going to find the option to toggle their way on and off."
    author: "Antonio Salazar"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "In Outlook 2003, up / down scroll the list or the message itself, depending on what has focus.  But if you're focused on the message window, you can also use Ctl + up / down to change message.  This is a nice way of doing things, although I'm happy with KMail's approach now I've figured it out.\n\nThis could almost be a configurable setting but you can arguably have too many of those ;-)"
    author: "Mark"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "And how many users (besides you) know that one can change the message with Ctrl+up/down? This behavior is at least as unintuitive as KMail's behavior."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "If this feature is not enabled by default then no user will ever notice how efficient using KMail with the keyboard can be. Please implement keyboard shortcut profiles in KDE and then we'll include a \"Focus Mode\" and an \"Efficient Mode\"."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "I would like to be able to scroll through folders with the keyboard. Am I missing something? Alt up/down arrow scrolls through but doesn't load the message list. It used to work if you pressed spacebar or something, but that was fixed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "In KMail it is CTRL+up/down for immediate switching and CTRL+left/right for selecting a folder and another time CTRL for making it the current\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "I just don't understand why not use:\nup/down - scroll msgs up/down\nalt + up/down - select message above/below\ncntrl + up/down - slecet folder above/below\n\nSeems to be more consistent, and we'd get to keep left/right for scrolling messages to the sides."
    author: "John Kmail Freak"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "The interesting part starts when the users clicks into the message list, and the message list indicates (through highlighting and the focus rectangle) that it has focus. What should up/down do then? \n\nIMO there is only one answer: previous message / next message. \n\nIf this is unacceptable to the KMail developers, then the message listbox must not take focus. But this would be a hack. I predict that users will consider it a bug, too.\n\nThere is really no choice. Trying to make interfaces that are smarter than the users for whom the interfaces are designed is not a good idea. You can only make things behave differently when they look differently. As long as something looks like a list box, it must behave like one. Everything else is making fun of your users, best intentions aside.\n\nBut I'm not a KMail developer, I haven't contributed code for it, I haven't even submitted those observations to the the bugs system, so I shut up now. I put my faith in the KMail developers and their user base that in the long run things work out better than what I could imagine in my wildest dreams. So far it always worked :-)\n\n Matthias\n"
    author: "Matthias Ettrich"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "This issues tends to be discussed about once every six months. Here's a quote from Ingo that I think covers the issue nicely:\n\n\"KMail is designed as focus-less application. That means that all keys \nshould always work the same regardless of which pane currently has the \nfocus. What's the advantage of this? The advantage which KMail has over \nmany other applications which have several panes is the efficiency of \nKMail's keyboard support. Since email management is a task many people \nspend a lot of time with it's important to make it as efficient as \npossible.\n\nSo, why is KMail's keyboard support highly efficient? Because the focus \ndoesn't get in the way. Okay, let me explain. Let's imagine you want to \nread two consecutive messages with a \"KDE compliant\" KMail:\nFirst you have to select the first message you want to read. But wait. \nBefore this you have to make sure that the message list has the focus \n(with Tab). So now you have selected the first message (with Up/Down). \nYou start reading. Hmm, the message doesn't fit completely into the \npreview pane. You have to scroll (with Down). To do this you have to \nfocus the preview pane and scroll down. Now you want to read the next \nmessage, so you hit Tab until the message list has focus, you press \nDown to select the next message, you start reading, you hit Tab until \nthe preview pane has focus, you press Down to scroll the message down. \nDo you get it? Isn't the fact that you'd constantly have to switch the \nfocus between the panes highly annoying? And it's even more annoying to \nfind out which pane currently has the focus (unless there was a fat \nugly frame that indicated the focus).\n\nAs comparison let's now see what you would do to read two consecutive \nmessages with the \"standard breaking\" KMail:\nYou select the the first message with Left/Right, you start reading, you \nscroll down with Down, you select the next message with Right, you read \nand, as necessary, scroll down with Down.\n\nNow that's what I call efficient. You don't have to care for which pane \nhas the focus when you press Up/Down. You don't have to switch the \nfocus constantly. Instead KMail just does what you tell it to do.\n\nYou want to select another folder: Use Ctrl-Left/Right to select and \nCtrl-Space to open.\nYou want to select another message: Use Left/Right\nYou want to scroll: Use Up/Down\n\nYou don't have to agree with this philosophy, but I hope that you now at \nleast understand why KMail's works as it works, and I'm sure that \nyou'll agree that the way it works is very efficient. There's always a \ntrade-off between efficiency and \"strict standard compliance\". And in \nmy opinion in the case of KMail the efficient handling outweighs any \narguments for strict standard compliance (especially with respect to \nthe focus paradigm).\n\nRegards,\nIngo\"\n\nSo it's the way it is because no one has come up with an alternative that is convincingly better.\n"
    author: "Don"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "Then don't let the list boxes take focus: QWidget::setFocusPolicy(Qt::NoFocus). But again: people would see that as a bug, and quite rightly so.\n\nI still maintain my position that being standard compliant is an alternative that is convincingly better. \n\nThe right way IMO would be to be standard compliant, and then possibly add a \"KKmail Advanced Focus-less Keyboard Mode\" to some configuration, disabled by default."
    author: "Matthias Ettrich"
  - subject: "One more thing [was: Re: Summary]"
    date: 2004-09-27
    body: "> This issues tends to be discussed about once every six months. \n\nNow what does that tell us if not a real need that something needs to be changed? :-)"
    author: "Matthias Ettrich"
  - subject: "Re: One more thing [was: Re: Summary]"
    date: 2004-09-27
    body: "More likely that KMail doesn't have the perfect solution to a hard and very common problem. \n\nActually, I think KMail is very usable with the keyboard, much more so than for instance Outlook. Big thanks to the developers! :)"
    author: "teatime"
  - subject: "Re: One more thing [was: Re: Summary]"
    date: 2004-09-27
    body: "\"not the perfect solution\" is an understatement for a solution that is seen as a bug by everybody who first tries it. It's counter-intuitive, non-standard and not even self-explanatory (the menus for example display different shortcut keys for the same actions). I needed a KMail developer to explain it to me before I started using it. I'm lucky that I had one in the office next door, not everybody has this luxury. And not everybody is KDE addict enough to read through the startup tips or check the mailing list.  Again: trying to be smarter than your users is seldom a good idea.\n\nSeriously, had I not been a KMail user, but a happy-enough evolution user who decided to give KMail a try, that bug alone would have been enough for me to abort the evaluation. My thinking had been along the lines of \"if basic focus handling is broken, what else is broken?\".\n\nVi is a lot more usable with the keyboard than notepad, big thanks to the developers. Does that mean we would install vi as default graphical text editor in KDE? No. Why not? Different target group. As long as KMail is called KMail and meant to be the default KDE mail client, users expect standard UI behaviour as opposed to obscure safe-a-few-keystrokes tricks for power users. \n\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: One more thing [was: Re: Summary]"
    date: 2004-09-27
    body: "The only people who complain are\n1.) Reviewers who have never actually used KMail.\n2.) Self-proclaimed \"usability experts\" who have never actually used KMail.\n3.) First-time users who have never used KMail before and are therefore confused.\n\nI don't really care about groups no. 1 and 2. But I agree that the complaints of the 3rd group are a problem, even though the majority of this group has retracted their complaints after they have learned about Left/Right. I'm not aware of any user who has complained about this after he has used KMail for a couple of days.\n\nSo yes, I agree that something needs to be changed. But instead of changing the behavior I favor including a short introduction to KMail's keyboard handling.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: One more thing [was: Re: Summary]"
    date: 2004-09-27
    body: "Users generally don't read dialog boxes. You're not going to change this aspect of human nature by being indignant about it, you can only accept it and move on with your life. \n\nI'm a power user, and find KDE's attempts to \"educate me\" totally useless. Take, for example, the tips of the day that are enabled by default, or the verbose dialogs for KWallet. Who actually reads those things? If you need to spend that much time explaining how something works, the feature is broken."
    author: "Rayiner Hashem"
  - subject: "Re: Summary"
    date: 2004-09-28
    body: "Ok, here is why I think you are wrong: You break standard compliance in order to support a rather special working model of mixed mouse and keyboard usage.\n\nAlso in a standard-compliant KMail users would be able to switch messsages with keyboard shortcuts, and the focus would not leave the message area. The focus would change only if they use the mouse to change to a different message or folder, either buttons or the wheel. If they use the mouse for these operations, it can also be expected that they use the mouse to scroll the message in the content area. Observe users. When they use the mouse, they do use the mouse.\n\nOn the other hand, if user use P or N (or maybe better Ctrl-Up and Ctrl-Down) to switch to the previous or next message, the keyboard focus never leaves the content area, and Up and Down still scroll the message (and Right and Left also scroll the message if necessary). That's the usage model of the keyboard power user.\n\nSo where again was the problem? To me it almost looks like you are trying to solve a problem that does not exist. And you do it in the worst possible way, by breaking standard compliance, something that makes initial users angry and results in bad reviews. That can't be worth the price.\n\nMaybe all we really need is a little checkbox in a configuration dialog that says \"Keep keyboard focus on message all the time\"? Let the KMail power users activate that one, and let everybody else enjoy a standard compliant app.\n\n\nThat must be the first time in years that I am actually arguing _for_ adding another configuration option."
    author: "Matthias Ettrich"
  - subject: "Re: Summary"
    date: 2004-09-28
    body: "<i>On the other hand, if user use P or N (or maybe better Ctrl-Up and Ctrl-Down) to switch to the previous or next message, the keyboard focus never leaves the content area, and Up and Down still scroll the message (and Right and Left also scroll the message if necessary). That's the usage model of the keyboard power user.</i>\n\nUsing P/N or Ctrl-Up/Ctrl-Down would be much less ergonomic than Left/Right for changing the current message, especially because Up/Down scroll the message and the Left/Right keys are close to them. I think I would rather use the mouse than these keyboard shortcuts.\n\nSo changing to these suggestions would increase consistency at a significant cost in efficiency/productivity. That would be an easy tradeoff to make, and an easy way to achieve consistency, but, I think, not a good one. Instead of an easy solution I would like a good one.\n\nRegarding hiding the focus. The focus still plays several useful roles.\nFolder list - Ctrl-Left, Ctrl-Right navigation only changes the current message but not the selected one. This is again for efficiency reasons, selecting a folder and showing it can take a significant amount of time.\nFolder list - Drag and Drop focus is used to show the drop site.\nMessage list - Focus is useful when making a ctrl-shift selection.\n\nPersonally I like Ingo's suggestion to add 'keyboard shortcut profiles in KDE and then we'll include a \"Focus Mode\" and an \"Efficient Mode\"', and \"I favor including a short introduction to KMail's keyboard handling.\" (maybe in the what's new page).\n\nAlso perhaps showing Left/Right as shortcuts in the Go menu instead of P/N would be in order.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Summary"
    date: 2004-09-28
    body: "\"So changing to these suggestions would increase consistency at a significant cost in efficiency/productivity.\"\n\nNo, not at all, I disagree. The number of keyboard hits requried stays exactly the same, even under a mixed mouse/keyboard usage pattern. It it possible to have efficient keyboard bindings in standard compliant applications.\n\nAs an example, you could stick to the binding of Left/Right to mean PreviousMessage/NextMessage if you think that's a big deal (I don't, because it breaks the standard way of scrolling a message horizontally with the keyboard, e.g. a table in a HTML message). All I'm asking for is that Up/Right means PreviousMessage/NextMessage when the message list has focus - like it would do in any other application.\n\nThis is so obvious in an environment like KDE that I almost can't believe it gets discussed every few months - and then turned down %-/\n\nThe ONLY disadvantage of being standard compliant would be that after selecting a message with the mouse, you cannot use Up/Down to scroll the message content. That's it. That's the big deal. Just that users won't even notice because when the select a message with the mouse the would scroll the message content with the mouse as well.\n\n\"Folder list - Ctrl-Left, Ctrl-Right navigation only changes the current folder but not the selected one. This is again for efficiency reasons, selecting a folder and showing it can take a significant amount of time.\"\n\nYes, I have noticed that. In fact this is one example where things get worse, because suddenly KMail no longer is a focus-less application but one that uses focus in a non-standard way. Although the folder-list clearly indicates to me as a user that it has focus, it doesn't behave like any other list box that has focus. This is even worse than not taking focus.\n\nThere are other solutions to the problem that do not break standard complicance. One could have Alt-Up/Down for switching folders and solve the performance issue by either using a timer to actually load the new current folder or by deferring the loading until the alt-key gets released. This would be a lot better than requiring the user to hit a second key (Space, or Enter or whatever) in order to actually switch folders. The best user interface is the one that is invisible and does not require user interaction to solve technical challenges.\n\nI encourage you guys to discuss this on the KMail mailing list and to come up with a solution that makes these kind of discussions go away and not return in 6 months from now. And please keep in mind that KMail is not the VI of the mail client world, but a standard application for everybody and a central part of the KDE desktop experience.\n\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "People do have some difficulty discovering (i.e. guessing) that KMail uses left/right to change the current message. Unfortunately I think it would be even more difficult for users to discover (guess) that alt + up/down changes the current message.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "Why? All those shortcuts would be displayed prominently in the \"&Go\" menu."
    author: "Matthias Ettrich"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "\"Kontact insisted on scrolling downward through the current message\"\n-----\nActually, I like that key-binding. (And it can be changed, if I want....) \nBecause if I want to scroll \"through messages in its message-list pane\", I use KMail's right/left arrow keys. Enabled by default.\n\nOh my god.... if these bloody journalists doing reviews would just decide to keep the \"Tip of the Day\" enabled and read it for a day or two, they would learn such simple things quite easily. Because it clearly says \"Did you know...? ...that you can to go the next and previous message by using the right and left arrow keys respectively?\" Stupid journalists, stupid eWeek \"lab\" \"experts\".....\n\n\n"
    author: "kmail lover"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "'These bloody journalists doing reviews' do what most people do, I think, which is either disable Tip of the Day or just ignore it. That behavior is completely counterintuitive, whether you prefer it or not. I'm not saying that it's bad, but it should be optional and default to intuitive behavior. I know that annoyed me too, and I certainly don't sit there all day reading the Tip of the Day to see if maybe it tells me how to scroll through my message list when click-and-down-arrow doesn't work. Just because you figured it out quickly, doesn't mean one should have had to figure it out. The logical behavior would be scrolling the focused listbox. Different behavior should be an option that is disabled by default."
    author: "kmail appreciator"
  - subject: "Re: Summary"
    date: 2004-09-25
    body: "\"...do what most people do, I think, which is either disable Tip of the Day or just ignore it\"\n----\nSee how big a mistake that is?? If *I* start any KDE program for the first time ever, or the first time on an updated version, I just *take* the time to read 10 or 20 Tips of the Day (if there are that many), before I deny it to auto-start. Also, I sometimes start the Tips again from the Help menu. I can say that most of the neat tricks I learned about KDE, I've drawn from these tips...\n\nI agree: it is not intuitive for people who learned other programs with 3 or 4 panes before. It wasnt intuitive for me either. But I learned it from a Tip of the Day. I think it is very useful. Now I even catch myself trying that key on Outlook or Thunderbird (I need to operate various OSes and Mail programs). And every time I am tempted to curse against these KMail contenders for *them* being \"unintuitive\".\n\nIt is all a question of perspective, habits and what has been graved into your mind what you regard as \"intuitive\".... And Matthias, who posted above about intuitiveness isnt different from me here. He is prone to external influence just as I am. It is just that his influence was different from mine, and now he thinks of the left/right key as being \"unintuitive\"."
    author: "KMail lover"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "So what you're saying is\n\n  RTFM, n00b\n\nThat's not usability. Nobody said usability was easy."
    author: "-"
  - subject: "Re: Summary"
    date: 2004-09-26
    body: "I think your definition of \"usability\" is a bit on the narrow side. The fact that it does not work like other email clients doesn't mean that there is a usability problem. Also, something that takes a bit of learning before you can handle it completely isn't by definition \"un-usable\". It might in fact be *very* usable. Considder vi, for example. Most people getting into it for the very first time will be utterly lost on how to even exit the program, let alone use it to edit any piece of text. Does that make it bad usability wise? No! It is actually very good in what it does, it just has a bit of a (steep) learning curve. However, once you get the hang of it, you'll be much more productive than with your traditional editors. "
    author: "Andre Somers"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "Vi and KMail are two different beasts targetted at two different kinds of people. "
    author: "Rayiner Hashem"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "Tell me the truth here: do you ever read \"Tip of the Day\"? I can't be the only one who takes a fresh KDE install, then turns off all the intrusive \"helpful\" dialogs in sight, can I?"
    author: "Rayiner Hashem"
  - subject: "Re: Summary"
    date: 2004-09-27
    body: "> 3) \"Another thing that annoyed us about KDE's mail handling was the way it dealt with HTML messages.\"\n\nKMail doesn't render inlined images when they are included in mail. At work I send some newsletters and have to test them on other people MUA because of that."
    author: "Hrw"
  - subject: "Re: Summary"
    date: 2004-09-28
    body: "\"\"A nice improvement we found in GNOME 2.8 was the capability for auto-mounting removable devices such as USB (Universal Serial Bus) thumbdrives.\"\n\nWorks just fine for me with the default desktop for SuSE 9.1 ;)\"\n\nThe difference is that SUSE handles it (as does Mandrake. Although I haven't used either one in a long time), and not KDE. In GNOME 2.8 it's the desktop that handles it and not the distro. And that means that it will behave the same way no matter what the underlying distro is. And that is IMO a Good Thing (tm). And KDE should do it as well.\n\nThere are distros out there that do not offer automounting and such, they just provide more or less vanilla versions of the software. I mean stuff like Slackware, Gentoo, LFS and Debian(?).\n\nThe job of the Desktop Environment is to make it easier for the user to work on the machine. Volume Management as found on GNOME 2.8 is a step in the rigth direction. And since I would be using KDE-tools to work on that USB-thumbdrive, IMO it should be KDE that provides me with an convenient access to it, and not the distro."
    author: "Janne"
---
The <a href="http://www.eweek.com/">eWeek Labs</a> have <a href="http://www.eweek.com/article2/0,1759,1651227,00.asp">reviewed the latest releases</a> of both major Free Software desktops. The report focuses on their groupware applications and how the desktops can be locked down. Additionally <a the href="http://kopete.kde.org/">Kopete</a> gets praised for its improvements and desktop integration. From <a href="http://www.eweek.com/article2/0,1759,1650299,00.asp">KDE's executive summary</a>: "<em>KDE 3.3 is another strong release from the prominent KDE project and stands out most for us in the improvements to its <a href="http://www.kontact.org/">Kontact</a> groupware application.</em>"


<!--break-->
