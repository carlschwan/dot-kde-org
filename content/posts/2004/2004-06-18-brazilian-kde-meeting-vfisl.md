---
title: "Brazilian KDE Meeting at V.FISL"
date:    2004-06-18
authors:
  - "jspillner"
slug:    brazilian-kde-meeting-vfisl
comments:
  - subject: "Oh yeah, I was there!"
    date: 2004-06-19
    body: "I was on two of the KDE events, I even got a picture with Martin Konold (see atachment).\nThe 5th FISL was really nice and KDE was well represented...\nMan how I envy Helio de Castro who got that nice konquy puppet!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Oh yeah, I was there!"
    date: 2004-06-19
    body: "Don't be shocked about the foot on the background... is was just from another people ther as you can see on the article's photographs. :)"
    author: "Iuri Fiedoruk"
---
The <a href="http://www.softwarelivre.org/forum2004/">V Fórum Internacional de Software Livre</a>, the major Free Software event in Latin America, brought together a lot of KDE developers and users from June 2nd to 5th in Porto Alegre, Rio Grande do Sul, Brazil. <a href="http://kstuff.org/5fisl/">Read about the event</a>, the KDE related materials and several plans and announcements.


<!--break-->
