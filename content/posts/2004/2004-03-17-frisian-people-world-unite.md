---
title: "Frisian People of the World, Unite!"
date:    2004-03-17
authors:
  - "fmous"
slug:    frisian-people-world-unite
comments:
  - subject: "Schae..."
    date: 2004-03-17
    body: "aber i will ebbes auf schwaebisch sehen!  Was machet die Tuebinger?"
    author: "Schdief"
  - subject: "none"
    date: 2004-03-17
    body: "Oder wos auf fr\u00e4ngisch!\nF\u00fcr de Bananenbiecher von Nernberch ...\n\n"
    author: "thefrog"
  - subject: "Frysk"
    date: 2004-03-17
    body: "Schwaebisch und fr\u00e4ngisch sind keine Sprachen sondern Dialekte im gegensatz zum Friesisch.\n\nMehr unter:\nhttp://de.wikipedia.org/wiki/Friesisch\n\nOr in English:\nhttp://en.wikipedia.org/wiki/Frisian_language\n\nAnd in Frysk:\nhttp://fy.wikipedia.org/wiki/Frysk"
    author: "MaX"
  - subject: "Re: Frysk"
    date: 2004-03-17
    body: "jetzt fragt nat\u00fcrlich der ketzer, ab wann ein dialekt zur sprache wird - der unterschied ist vermutlich ebenso schwierig festzumachen, wie der zwischen sch\u00fctterem haarwuchs und einer glatze."
    author: "burki"
  - subject: "Re: Frysk"
    date: 2004-03-17
    body: "Well Frysk is officially recognized as a language by the dutch government and taught in schools. I guess that makes it an official language."
    author: "Johan Veenstra"
  - subject: "Re: Frysk"
    date: 2004-03-17
    body: "Then you just can see that the difference is just in politics. Just a questions who wins the next elections. "
    author: "wanthalf"
  - subject: "Re: Frysk"
    date: 2004-03-17
    body: "Not really, Frisian has been an official language in The Netherlands for more then a century, also Frisian as a language is older then Dutch.\n\nAncient Frisian and Ancient English are a lot alike:\n\nBoetter, Brea in griene Tsies\nIs the same in ancient English as well in Frisian.\n\nHere's an old poem in Frisian and English:\n\n\nLyk az Gods sinne weiet uus wr\u00e2ld oerschijnt;\nLike as God's sun sweetly our world o'ershines;\n\nHer warmtme in ljeacht in groed in libben schinkt;\nHer warmth and light and growth and life sends;\n\nLijk az de mijlde rein elke eker fijnt:\nLike as the mild rain each acre finds:\n\nSo dogt eak dat, wat ijn uus, minksen, tinkt.\nSo does eke that, what in us, men, thinks.\n\nDij sprankel fen Gods fjoer, ijn uus lein, jouwt\nThat sparkle of God's fire, in us laid, gives\n\nOeral eak ljeacht in FREUGDE oon Adams team.\nO'erall eke light and JOY on Adam's team.\n\nWer dij wenn't, hulken, of paleisen, bouwt\nWhere they dwelt, hulk, or palaces build,\n\nIn fen wat folk hij iz, ho hij him neam\nAnd of what folk he is, how he him names.\n\nthe writing is a bit different, but the speak is allmost the same..\n\nRinse"
    author: "rinse"
  - subject: "Re: Frysk"
    date: 2004-03-18
    body: "Da gibt es ein sch\u00f6nes Zitat:\n\"Eine Sprache ist ein Dialekt mit einer Regierung und einer Armee\""
    author: "Andreas Korinek"
  - subject: "Re: Frysk"
    date: 2004-03-18
    body: "klasse - besser kann man den unterschied glaub nicht auf den punkt bringen."
    author: "burki"
  - subject: "Re: Frysk"
    date: 2004-03-18
    body: "Only that Frysk does not have it's own government or it's own army..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Frysk"
    date: 2004-03-18
    body: "and the swiss have a government and an army on no language on their own."
    author: "burki"
  - subject: "lol"
    date: 2004-03-17
    body: "I live just a few miles away from the frisans and they treuly believe in their language. wonder how long it takes it takes before kde starts to include the dutch dialects as well :p\n\nI'd say it's fun. It proves the power of kde and it's development team.\nbut since frisians can speak native dutch as well, it is probably more fun then usefull.\n\nAnyone knows about the status of the klingon translation? :p"
    author: "Mark Hannessen"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "The status of the Klingon translation can be found in http://www.unixcode.org/kde-i18n-klingon/ :\n\n\"Update 8/6/2003: approximately 125 messages translated now.\"\n\nThe current limits for inclusion into KDE are roundabout 3500 strings out of 4500 strings from kdebase and kdelibs. So Klingon has managed to get about 4% of the minimum requirements translated. \n\nLet's see:\nthe last update was 9 months ago. Let's assume they manage 125 strings in 9 months, then we still have to wait about 21 years to see Klingon included. :)\n\nA nice remark about Klingon that I found recently was:\n<quote>\n But how are they handling error messages? Are they doing a literal translation, or will it be more \n along the lines of \"This application has shamed itself with a segmentation fault!\"?\n---\n They'll probably kill the application's parent and siblings too.\n</quote>\n\nRegards,\n\nHeiko Evermann\n(KDE Low Saxon translation team)"
    author: "Heiko Evermann"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "\"The status of the Klingon translation can be found in http://www.unixcode.org/kde-i18n-klingon/ :\n \n \"Update 8/6/2003: approximately 125 messages translated now.\"\"\n\nUnbelievable.\n\n\"the last update was 9 months ago. Let's assume they manage 125 strings in 9 months, then we still have to wait about 21 years to see Klingon included. :)\"\n\nThe fact that Klingon support in KDE is there is what is so amazing, no matter how long it takes."
    author: "David"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "<quoteTheQuote>\nBut how are they handling error messages? Are they doing a literal translation, or will it be more along the lines of \"This application has shamed itself with a segmentation fault!\"?\n</quoteTheQuote>\n\nYes, that is great! *lol*\n\nHow would some kind of Vulcan translation sound?\n\n\"The application ignored the rules of logic and reason and therefor produced a segmentation fault\""
    author: "Thilo"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "\"How would some kind of Vulcan translation sound?\"\n\nDon't get people started.... :)"
    author: "David"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "Elvish! (Or Elven? *g*)"
    author: "panzi"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "That would be sooo.. gay (not that there\u00b4s anything wrong with that!)"
    author: "Roberto Alsina"
  - subject: "Re: lol"
    date: 2004-03-17
    body: "\"wonder how long it takes it takes before kde starts to include the dutch dialects as well :p\"\n\nWell, there are plans to do a Limburgisch translation :)\n\n(gnome already has a Limburgisch version, so KDE is a bit behind :o)\n\nRinse"
    author: "rinse"
  - subject: "Re: lol"
    date: 2004-03-18
    body: "Cheesy."
    author: "reihal"
  - subject: "Re: lol"
    date: 2004-03-19
    body: "\"wonder how long it takes it takes before kde starts to include the dutch dialects as well :p\"\n\nFrysk is an official language of course, Limburgisch isn't. An no, neither is Twents ;-)\n\n--\nTink, Fries by birth."
    author: "Tink"
  - subject: "mooo'd a thunk it?"
    date: 2004-03-17
    body: "now even us cows can use KDE!\n\n(Frisian cows are world-famous)"
    author: "Daisy"
  - subject: "For Tolkien fans"
    date: 2004-03-17
    body: "Maybe someone starts elvish as well?\nBut reading the fonts could be hard :-)\n(Ok not that different to klingon)\n\nAny other ideas?\nWhich other fictional languages do we \"need\"?"
    author: "SegFault II."
  - subject: "Re: For Tolkien fans"
    date: 2004-03-17
    body: "How about 1984's novilingua? Just to get a whiff of dystopia...\n\nEvery time you would read a error message it would have been \"cleansed\"...\n\nAnd of course... The screensaver and background would say:\n\"Big Brother is watching you\""
    author: "Andr\u00e9 Esteves"
  - subject: "Re: For Tolkien fans"
    date: 2004-03-18
    body: "It would sound then too much as *that other* user environment..."
    author: "Cloaked Penguin"
  - subject: "Re: For Tolkien fans"
    date: 2004-03-17
    body: "Fifteen years ago I 'translated' WordPerfect 4.1 into one of my own conlangs using a hex editor... It's not something I would spend time on nowadays, but it was quite fun while it lasted."
    author: "Boudewijn Rempt"
  - subject: "Re: For Tolkien fans"
    date: 2004-03-18
    body: "What other languages?\n\nHow about D'ni?\n\nThe D'ni language has rather a small lexicon so it'd have to be english into D'ni characters.\n\nEnglish written using elvish (tengwar) characters isn't too difficult to read and looks very pretty.\n\nRunes would be good, or we could all go back to our roots and write in Oggam.\n\nSunil\n\n-->FIN<--"
    author: "Sunil Patel"
  - subject: "Re: For Tolkien fans"
    date: 2004-03-18
    body: "Have nordic runes been assigned unicode-numbers, and do they include the common extensions made to enable conversion from modern latin alphabet?\n\n"
    author: "Allan S."
  - subject: "Re: For Tolkien fans"
    date: 2004-03-18
    body: "Actually, Sunil, in my case, going back to my roots *would* be using the Nordic Futhark runes, not Ogham.\n\nAnd I find Allan's question very interesting.  Does anyone know this?  (About Futhark, at least?)"
    author: "Panther"
  - subject: "Re: For Tolkien fans"
    date: 2005-11-23
    body: "If anyone is intrested I started a project to translate the works of Tolkien in Frysian, the official second language of Holland.\nFor more information contact me on hear_fan_ringen@hotmail.com or take a look at the dutch Tolkien Society Unquendor, at www.unquendor.nl/forum. (Dorpsplein van Breeg, Fries vertaling werken van Tolkien Topic*)\n\nAt this time there is a translation of \"The Hobbit\" on its way, it will take some years to complete and to aquire the publishing rites, but it's on it's way.\n\n* Dutch only, DOESNOT require to login."
    author: "Huinelassir Areste"
  - subject: "Re: For Tolkien fans"
    date: 2007-01-28
    body: "Resently there has been a change of internet adres, new version is:\n\nhttp://www.unquendor.nl/ainulindale (This specific topic has not been re-opend yet, but will soon make it's new appearance.\n\nThe E-mail contact has changed as to: tolkien_friestalig@hotmail.com\n\nInformation in Dutch, but English on request.\n\nThe project is steadely going forwarth, but it still takes a lot of pactiance.\n\nThank you for your intrest."
    author: "Huinelassir Areste"
  - subject: "I want.."
    date: 2004-03-17
    body: "a binary translation ;-)"
    author: "superstoned"
  - subject: "Googlish"
    date: 2004-03-17
    body: "Wie w\u00e4rs mit googlish? English \u00fcbersetzt ins Deutsche bzw andersrum mit google!"
    author: "ichbinplatt"
  - subject: "SuSE9.1.."
    date: 2004-03-18
    body: ".. with GNOME as the default now?\nhttp://www.suse.com/us/company/press/press_releases/archive04/91.html"
    author: "foo"
  - subject: "Re: SuSE9.1.."
    date: 2004-03-18
    body: "Because the English announcement (in opposite to the German) lists it first? No, sorry to disappoint you that an ancient GNOME will not be the default."
    author: "Anonymous"
  - subject: "Re: SuSE9.1.."
    date: 2004-03-18
    body: ": Because the English announcement (in opposite to the German) lists it first?\n\nThat, and it seems to get more focus in the announcement.\n\nOh, and you wouldn't disappoint me. I really hope KDE gets the appreciation it deserves."
    author: "foo"
  - subject: "Friesisch"
    date: 2004-03-23
    body: "Frisian is an important language. But plattd\u00fc\u00fctsch as well. Instead of suggestion whoich ubsurd languges shall be supported I would rather suggest you to help Heiko Evermann and the dutch frysk guyys in their effort. A fulfillment of the EU language chgarter could assist the adoption of KDE in public offices.\n\nEala Freya Fresena!"
    author: "Johannes"
---
<A href="http://people.kde.org/rinse.html">Rinse de Vries</A>, the coordinator of 
the dutch KDE translation group, reported about a 
<A href="http://i18n.kde.org/teams/index.php?a=i&t=fy">new translation effort</A> they started. Inspired by 
the project <A href="http://dot.kde.org/1075485586/">KDE op Platt</A> 
they revitalised the idea of translating KDE to Frisian. Frisian 
is the second official language spoken in The Netherlands by almost 350.000 people 
(<A href="http://en.wikipedia.org/wiki/Friesland">more info</A>). 
The Frisian Team welcome every bit 
of help on this translation effort and people who are interested can subscribe
to the mailinglist <A href="https://mail.kde.org/mailman/listinfo/kde-i18n-fry">kde-i18n-fry@kde.org</A>. 
Also <A href="http://www.kde.nl/">www.kde.nl</A> hosts a 
<A href="http://www.kde.nl/frysk/">webpage</A> dedicated to the Frisian 
translation effort. Interested? Please contact <a href="mailto:mailto:rinse@kde.nl">Rinse de Vries</a>.

<!--break-->
