---
title: "Interview with Everaldo, the Crystal Iconset Creator"
date:    2004-02-20
authors:
  - "cwoelz"
slug:    interview-everaldo-crystal-iconset-creator
comments:
  - subject: "Kids Iconset"
    date: 2004-02-19
    body: "It's just cool, my kids love it and use it at work myself.\nHow did you come up with all these ideas?"
    author: "JJ"
  - subject: "How?"
    date: 2004-02-19
    body: "How do you make them? Do you just start sodipodi and create a new icon or do you draw a sketch oder something like this before, probably even on paper?"
    author: "ramadan"
  - subject: "Re: How?"
    date: 2004-02-20
    body: "he said that in  the interview."
    author: "adsdasdasdasd"
  - subject: "cool interview"
    date: 2004-02-20
    body: "\"In the future KDE will show it is possible to have a pretty desktop that does\n not need to insult the user's intelligence to work ..\"\n\nwhat a great quote... up till now i've really only known Everaldo through his artwork, but after reading that article i have a whole new level of respect for the guy. rock on, Everaldo! =)\n\n--\nAaron, a fellow dreamer for life ..."
    author: "Aaron J. Seigo"
  - subject: "Re: cool interview"
    date: 2004-02-20
    body: "\"In the future KDE will show it is possible to have a pretty desktop that does\n not need to insult the user's intelligence to work ..\"\n\nI too applaud that! Maybe someday he can help us make Quanta pretty. We've already made sure not to insult our user's intelligence... just their visual perceptions with some of the icons we've drawn. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: cool interview"
    date: 2004-02-20
    body: "> We've already made sure not to insult our user's intelligence...\n\nDon't be so modest, I'm sure you really mean \"users'\"! Yes, I'm an apostrophe pedant.\n\nAnyway, I had a quick look at Quanta recently, and it looks really impressive. Any day now, I'll take the plunge...\n"
    author: "Adam Foster"
  - subject: "Re: cool interview"
    date: 2004-02-21
    body: "As it's mentioned above, you do have more than one user, and a very very impressive tool that I myself have been using since KDE 3.2 Beta 2 for a wide range of different sites. The VPL mode is coming along nicely, too. Thank you very much for it, and on the subject of icons, I should like to get in touch with you about that at some point soon :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: cool interview"
    date: 2004-03-02
    body: "hi eric,\nyou still didn't replied to my last e-mail with wizard images attached. from this post it seems you're not satisfied with the icons and images i've made. why don't you try to discuss it with me?\n\ngreetings, luci"
    author: "luci"
  - subject: "Hand Drawn Icons"
    date: 2004-02-20
    body: "What this tells me is that either he can't afford a tablet or finds, like most artists, tablets aren't a match for the human pressure touch.\n\nWhen someone actually develops completely in a digital environment utilizing SVG full compliancy, and with only open-source tools then the community has something to really toot its collective penguin about.\n\nWhat would be interesting is if someone can get an interview with Tim Wasko at Apple and ask him how he makes Aqua icon sets.  I know how he used to because I watched him, but that has been several years."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Hand Drawn Icons"
    date: 2004-02-20
    body: "Well, this is done using a Wacom tablet and Gimp on a Slackware laptop :\n\nhttp://svdboom.free.fr/\n\nIt's no icons, but I guess it proves that you can do pretty good things with a tablets.\nMany artists just don't like changing their habits.\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Great Interview"
    date: 2004-02-20
    body: "Awesome interview you can tell that this is a very cool dude. I hope he makes it to the Desktop Summit In San Diego this April hosted by his employer Lindows. I'll be volunteering in the KDE booth so maybe i'll get a chance to meet him."
    author: "Kraig"
  - subject: "Interviews"
    date: 2004-02-20
    body: "Hey, what ever happened to the interviews that used to be done by kde.org?\nI have forgotten who it was doing it, but it was one of the developer's wife."
    author: "a.c."
  - subject: "Re: Interviews"
    date: 2004-02-20
    body: "Tink, our favourite pixie. ;-) Her interviews are returning. Stay tuned."
    author: "Aaron J. Seigo"
  - subject: "Re: Interviews"
    date: 2004-02-20
    body: "and it has been returning since summer 2002? ;D\n\nbut seriously, I was thinking about restarting it last year.. didn't ever actually do it though :)"
    author: "anon"
  - subject: "cool"
    date: 2004-02-20
    body: "It's nice that dot.kde linked the interview .. I have known everaldo for many years now, I realised a while back that most people did not had a clue of how much he actually loves kde and linux .. maybe becuase his english is not that good, he doesnt express himself that often to the community .. hope this article makes it all the more clear about his devotion :)\n\nIf ADOBE ever starts developing for linux, it's people like everaldo who will be the first ones to ditch their mac's or windows machines ;)"
    author: "NullMind"
  - subject: "Graphics applications"
    date: 2004-02-20
    body: "I think the interview shows how important applications like Karbon and Krita to get more artists. The"
    author: "Sven Langkamp"
  - subject: "Re: Graphics applications"
    date: 2004-02-20
    body: "Well, I've finally managed to get my Gentoo up and running and I've been using Krita. I like it *a lot*."
    author: "David"
  - subject: "Re: Graphics applications"
    date: 2004-02-20
    body: "It's still far form a release, but we are making progress. How do you like the dockers? I hope we have a first version for the public in KOffice 1.4, so that more people can try it and we perhaps get more developers."
    author: "Sven Langkamp"
  - subject: "programs and SVG icons"
    date: 2004-02-20
    body: "Perhaps I should reintroduce myself: I am an engineer not an artist but I have started participating a little in KDE-Artists and the 'kde-artists' mailing list.  I did the MIME type icons for Office.  The Crystal ones have been committed, but I'll have to check on the KDEClassic ones (didn't get them done in time for KDE-3.2).  They are currently available at:\n\nhttp://home.earthlink.net/~tyrerj/kde/pics/kdeclassic/\n\nor, a tarball for the source tree:\n\nftp://ftp-www.earthlink.net/webdocs/kde/jrt-koffice-icons0.tar.bz2\n\nTo summarize some of what has been discussed on the artists list:\n\nA major problem with the CrystalSVG icons is that we don't seem to be getting the SVG source for them from Everaldo.  He states as an excuse that \"SVG generated by Illustrator is not 100% compatible with KDE\".  This is not exactly the case.  \n\nFrom my attempts to make an SVG icon: \"kexi_kexi.svgz\" (various attempts have been posted on the 'kde-artists' list) and attempts to hand edit the SVG source files to do this, I have found what I believe to be the problem.  Everaldo's SVG source is simply to complex -- specifically, the gradients are too complex.  We do not need such complexity in icons even if KDE-SVG were able to handle it.\n\nI used as an example: \"mime_empty.svgz\"; this is covered in:\n\nhttp://lists.kde.org/?l=kde-artists&m=107461655615340&w=2\n\nI believe that if Everaldo could keep his icon design simpler that the problems with the SVG code would be solved.  In any case, the artists need the SVG code.\n\nThere is also the problem that we do not have a really good program in Linux to produce SVG icons.  He states \"Unfortunately there is no good vector graphics program in Linux\".  Well actually, there is an excellent vector graphics program for UNIX (including Linux); it is Xfig.  Unfortunately it does not produce SVG output, it does not do B\u00e9zier curves (only spline curves), and does not do gradient fill.  However, it (despite the old style Motif interface and having only a fixed 1200 DPI grid) is much more usable than the current SVG based UNIX/Linux vector drawing applications.  The specific reason that I say this is that it is designed to be a computer based drawing application -- it allows fine *digital* control of the drawing.  I hope that Karbon/Krita (they should be combined) will some day have the features which Xfig has.\n\nAnd, I am one of those that doesn't like CrystalSVG icons.  My original criticism is that they are hard to recognize in small sizes.  I find after some basic analysis that there is a valid reason for this.  According to classical information theory, they simply do not contain as much information as KDEClassic (a.k.a HiColor) icons do.  \n\nNow with the second generation of CrystalSVG icons, there is an additional problem.  See for example the new icon for Krita:\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/koffice/krita/pics/cr48-app-krita.png?rev=1.1&content-type=text/x-cvsweb-markup\n\nThis is simply too detailed for an icon.  Reduce it to 16x16 and what do you have?  A fuzzy colored blob.  Compare this with:\n\nhttp://home.earthlink.net/~tyrerj/kde/pics/crystalsvg/cr48-app-krita.png\n\nwhich isn't very pretty, but is still very recognizable at 16x16:\n\nhttp://home.earthlink.net/~tyrerj/kde/pics/crystalsvg/cr16-app-krita.png\n\nWith icons, we *do* need to KISS (Keep It Simple Stupid).\n\nNote: I do not intend to detract from the fact that Everaldo is a great artist.  He is a great artist!  He does great work.  But, it is the engineers that are left to consider the practical matters. :-)\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: programs and SVG icons"
    date: 2004-02-21
    body: "I'm using inkscape ( www.inkscape.org ) and it's quite getting there as a decent SVG editor for icon creation."
    author: "Coma"
  - subject: "Re: programs and SVG icons"
    date: 2004-02-21
    body: "Sour Grapes very uncool"
    author: "Kraig"
  - subject: "Re: programs and SVG icons"
    date: 2004-02-21
    body: "Sources are needed: icons are made from icons, to render missing sizes, to render new sizes. Everaldo does not release sources, and is keeping kde in lock with this behaviour. Unfortunately, the Crystal icon set can not be regarded as open source. If Everaldo would really believe in open source, he would release his svgs. Or, if there are no svgs, his illustrator files. At a certain moment artists were recreating vector icons from pixel icons, which is very hard work. Only because Everaldo sits on his sources. An artist that gets paid for his work lets artists that do not get paid for their work clean up the mess he creates. No way to treat fellow artists. "
    author: "vitanova"
  - subject: "Re: programs and SVG icons"
    date: 2004-02-22
    body: "He will i'm sure."
    author: "Kraig"
---
<a href="http://linuxcult.com/">LinuxCult</a> has published an <a href="http://linuxcult.com/?m=show&id=157">interview with Everaldo</a>, discussing career, graphic tools, creation process, Linux and KDE. <a href="http://www.everaldo.com/">Everaldo</a> is not one of the most communicative guys in the open source community. But not many words are necessary, his work speaks well for him. He started creating the <a href="http://kde-look.org/content/show.php?content=8341">Crystal Iconset</a>, and improved it constantly over the years. Today we have not only a polished Crystal iconset but also the <a href="http://kde-look.org/content/show.php?content=9144">Kids Iconset</a> and the <a href="http://kde-look.org/content/show.php?content=9143">Crystal based Outline Iconset</a>. So thanks to him and to the many artists in our community, we now have not only one, but <a href="http://kde-look.org/index.php?xcontentmode=22">many quality iconsets</a>.



<!--break-->
