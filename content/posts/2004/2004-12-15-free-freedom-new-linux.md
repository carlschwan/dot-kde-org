---
title: "Free as in Freedom: New Linux"
date:    2004-12-15
authors:
  - "Datschge"
slug:    free-freedom-new-linux
comments:
  - subject: "Sloccount"
    date: 2004-12-15
    body: "Is there an easy way to find out the 'SLOCCount' value for arbitrary developers?\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Sloccount"
    date: 2004-12-15
    body: "I guess you would need to use cvs annotate with it.  The problem is that code in ever-evolving, so you would have to take into account changes over time as well.  I don't think this is an easy problem."
    author: "George Staikos"
  - subject: "For god's sake..."
    date: 2004-12-15
    body: "...who might be able to read such a badly elaborated article 'til the very end?"
    author: "katakombi"
  - subject: "i've never been a big fan of ESR either..."
    date: 2004-12-15
    body: "\"I have a feeling that Raymond was just messing around. I quoted this interchange verbatim because, in 18 years of journalism, this was the nastiest, most mean-spirited, arrogant, and ultimately vapid (\"communist flake cases\"? What 1950s-era FBI comic book did he get that one from? Communist? Because I said I write for progressive \"zines\" Or maybe because I mentioned the FSF. Are they considered \"communists\" now, 15 years after the Cold War?). Perhaps he figured that just in case this article is picked up by a \"big\" publication, it was a chance to score points with his corporate/bourgeois masters. Does Netscape still exist? Maybe there?s someone there he still wants to impress.\"\n\n...and this confirmed my thoughts :)"
    author: "Pat"
  - subject: "Interesting thoughts, not the best presentation"
    date: 2004-12-15
    body: "It's a shame the author didn't spend a little more time turning a series of email conversations into something more readable. It's also a shame that he didn't expand his essays to explain the link he tried to make between anti-corporatism and Free Software, instead of relying on the odd illuminating response to an interview shed light on it.\n\nOtherwise it's a pretty reasonable set of essays, not least because he teases ESR into some of his trademark, and because he is attempting to explain why Free Software should be relevant and even important to the social movement he situates himself in."
    author: "Tom"
  - subject: "Free as in..."
    date: 2004-12-15
    body: "Free as in \"Free Writing Courses Offered At Your Local Community College\".\n\nSeriously -- how did this end up linked on the Dot?  It's neither new nor insightful and is poorly written to boot.\n\nIt uses huge blocks of quotes without highlighting points or attempting contextualization -- hammering away in the general direction of a point, hoping that if he levels the whole area that he might strike a point on accident, but really at the end all that's left is a mess.\n\n"
    author: "Anonymous"
  - subject: "Re: Free as in..."
    date: 2004-12-15
    body: "I fully concur. The opening quotation (\"...chemtrails that have left half the country stricken with a debilitating bronchitis that never goes away...\") is entirely deranged, but at least has the merit of being concise. The essay itself -- well, you put it very nicely."
    author: "Otter"
  - subject: "Re: Free as in..."
    date: 2004-12-16
    body: "By the way, for those unfamiliar with the term -- \"chemtrails\" refers to the notion that airplane contrails are actually mind-control chemicals sprayed by (insert pet conspiracy). The site he links to is a muddle of every crackpot conspiracy theory from UFOs to fluoridation to Holocaust denial.\n\nWay to go, guys -- promoting this Adam Engel is really going to advance the reputation of KDE, Linux and free software."
    author: "Otter"
  - subject: "remember"
    date: 2004-12-16
    body: "Remember it was just Navindra Umanee? Them be the days."
    author: "anon"
  - subject: "Re: Free as in..."
    date: 2004-12-17
    body: "There is a sizeable quote from George Staikos in the piece.\n\nOtherwise I agree. A rather annoying attempt to fit free software communities into a very narrow political outlook.\n\nI suppose it would be similar to someone being interviewed by a reporter who doesn't get it.\n\nDerek"
    author: "Derek Kite"
  - subject: "!?"
    date: 2004-12-19
    body: "How did this get on dot.kde.org?  "
    author: "Rich"
---
In 
<a href="http://www.pressaction.com/news/weblog/full_article/engel12122004/">
the latest addition</a>
to an interesting series of long articles called "Free as in Freedom" <a href="http://www.pressaction.com/news/weblog/listing/C94/">Adam Engel</a> 
discusses the "New Linux", a Linux that is no longer just for 
'<a href="http://www.catb.org/jargon/html/H/hacker.html">hackers</a>'.
Included is an extensive email interview with <a href="http://www.staikos.net/~staikos/">George Staikos</a> about KDE and its contribution to this development.
<!--break-->
