---
title: "KDE CVS-Digest for September 3, 2004"
date:    2004-09-04
authors:
  - "dkite"
slug:    kde-cvs-digest-september-3-2004
comments:
  - subject: "KDE 3.4???"
    date: 2004-09-04
    body: "I'm a bit suprised to find the feature plan for KDE 3.4. http://developer.kde.org/development-versions/kde-3.4-features.html\n\nWasn't KDE 4 the next major scheduled release? Why are the KDE team sticking with 3.4 atm? Are D-BUS, Qt 4, and other techologies too immature for now? \n\nWhen should 3.4 be released, will it be the same blazing release schedule as 3.3 or a bit faster?"
    author: "Alex"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "See here for further information:\nhttp://wiki.kde.org/tiki-index.php?page=KDE+3.4+or+4.0+Discussion\n\nRegards, Juergen"
    author: "Juergen Nagel"
  - subject: "Interesting discussion, my views.."
    date: 2004-09-06
    body: "I love KDE.  I hate the release schedule.\n\nNow, that's a starting point for my opinion.  I wish the KDE project was split into two major parts.  The 'core', and the applications.  The core should only consist of the libraries lots of programs use (probably what is called 'kdelibs' now), the main user interface (kwin?), and not a huge lot more (maybe kdebase). \n\nThen one should have the applications.  The applications should be decoupled from the main releases, so that users can get new versions of their applications without having to upgrade the _entire_ system, or track CVS-HEAD.  \n\nThere should be no reason, in my opinion, for the applications all to be released at the same time.  They could be released as their maintainers saw fit.  Maybe with a policy of trying to support the main release (3.0, in this case) - but with added features for 3.1, 3.2, 3.3 and HEAD - as they use more recent additions to the libraries.\n\nI would love it if I could fetch a new release of kMail sooner than the next major release of kMail is released.  The same goes for speed improvements of konqueror, and so forth.\n\nA complete decoupling of the app-releases from the core-releases would be _great_ - in my opinion."
    author: "arcade"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-06
    body: "> There should be no reason, in my opinion, for the applications all to be released at the same time\n\nWhat about translations and documentation? Do you really believe that the translations would be as good as they are now, when the translator have to track 50 different message freeze dates? Same goes for the documentation and there translation."
    author: "Christian Loose"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-06
    body: "<i>What about translations and documentation? Do you really believe that the translations would be as good as they are now, when the translator have to track 50 different message freeze dates? Same goes for the documentation and there translation.</i>\n\nWithout _knowing_, I would think that those currently engaging in translations would continue to translate their favorite programs as they're currently doing.\n\nHeck, I know that I would be _more_ interested in translating into norwegian if I could track the programs I used without having to upgrade my entire system to CVS-HEAD to be able to translate them and see the effects.  \n\nIf I could track, say, the latest versions of kMail and kOrganizer, without having to care about the rest of KDE, I would be far more interesting in contributing translations for those programs.  Having to wait until the next release of all of KDE is not exactly encouraging in that department.\n\n\n\n"
    author: "arcade"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-06
    body: "In this particular case, KDEPIM keeps compatibilty to kdelibs 3.2, so you could help translating.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-07
    body: "Nope. KDE PIM HEAD must be compatible to kdelibs 3.3. See http://lists.kde.org/?l=kde-pim&m=109351225402353."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-07
    body: "Ah, sorry!\n\nWell, nevertheless, it is the last stable version, so it should be easier to translate it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-06
    body: "Hello,\n\nwhat's so dramatic if translations are only updated in 3.3.1 or 3.3.2? It is not so much later and the .0 releases are not so stable anyway (as the later ones).\n\nHaving to do everything at the same time can slow things down. The string freeze sometimes required people to do silly things (re-using not 100% matching strings or leaving obviously wrong ones).\n\nSo instead of requiring all apps to slow down development by staying compatible for one release more, I suggest to just wait till it's stable, even packaged as binaries and then make sure that translation is down with the bug fix releases together.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-06
    body: "Well, the string freeze has to take place one time or another.\n\nAnd whatever the timepoint it will not please somebody. But I do not think that losing KDE's strong point, to have translations from the .0 version on, has to be dropped.\n\nAlso between .0 and .1, there are mostly only a month or so. For most teams, that is much too few time.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Interesting discussion, my views.."
    date: 2004-09-08
    body: "Well, translations are beside the point.  I was arguing for app-releases being split out from the 'big' releases.   I don't think it will slow translations down as another person argued.\n\n"
    author: "Rune Kristian Viken"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "http://www.kdedevelopers.org/node/view/600\n\nfor coolo's blog with some details. Feature freeze at the end of november, final release march.\n\nQT4 is a ways off, hence KDE4 would probably be around a year away (at least). There probably will be a KDE4 library port branch started as soon as QT4 code becomes available.\n\nIf KDE wants to keep all the apps and libraries together for release, a 6 month cycle is necessary. Otherwise the major applications will do their own releases.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "i think we can integrate dbus and the all other fancy stuff (xorg composite) , HAL ... with 3.4 as long as it does not break binary compatibility."
    author: "chris"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "I second that!"
    author: "ac"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "And then have a lot of work to port it to KDE 4, introducing more bugs...\n\nI don't think that is a good idea."
    author: "JohnCabron"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "Sorry, that is just clueless. \n\nKDE3 is a massive pile of code. Moving it to Qt4 will be a reasonably large task, but due to the Trolls good compatibility layers, probably not ultra painful. \n\nAdding a miniscule amount of code to allow the use of DBUS/HAL/Composite is not going to change that very much at all. \n\nRemoving/adapting DCOP in favour of DBUS is another matter. It won't be binary compatible, so it won't happen before KDE4."
    author: "hm "
  - subject: "Re: KDE 3.4???"
    date: 2004-09-06
    body: "> Sorry, that is just clueless.\n\nSorry, dito.\n\n> KDE3 is a massive pile of code. Moving it to Qt4 will be a reasonably large\n> task, but due to the Trolls good compatibility layers, probably not ultra\n> painful.\n\nIn porting KDElibs3 to Qt4, we can _of course_ _not_ make use of the Qt3 compatibility layer. Apps can, but KDElibs needs to be pure Qt4 _and_ provide compat libs/layers of it's own for kdelibs3 (source) compat.\n\nAll in all, I think you grossly underestimate the work that is needed for the Qt4 port."
    author: "Marc Mutz"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-20
    body: "would it be possible to make a converter ?\nthere is probably few known differences between Qt 3.3 and 4.\njust parse the code for each of these and apply the necessary fixes.\n\n"
    author: "somekool"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-20
    body: "i meant \"would not it be possible to ....\""
    author: "somekool"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-04
    body: "\"I'm a bit suprised to find the feature plan for KDE 3.4. http://developer.kde.org/development-versions/kde-3.4-features.html\"\n\nI am not ;)\nhttp://dot.kde.org/1091864563/1091892781/1091954569/1091961571/1091965418/"
    author: "John Told Ya So Freak"
  - subject: "Re: KDE 3.4???"
    date: 2004-09-20
    body: "a faster than 3.3 ?\nwhat is that... \n3.3 has been the fastest release ever,... the actually push it that fast in order to have a fresh version to play with for the aKademy.\n\nno way any version should be faster...\n\n\n"
    author: "somekool"
  - subject: "Any other speedups in the pipeline?"
    date: 2004-09-04
    body: "\"Enrico is god!\", and he made using konqueror a real pleasure, and I wonder\nif there are and other speed improvements in the pipeline...\n\nLet us users know!"
    author: "ac"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-04
    body: "Would now not be a good moment to get rid of kdeinit?\n\nI use my system with KDE_IS_PRELINKED=1 and KDE_FORK_SLAVES=1 and see no\nnoticeable slowdown\n\nSecurity-wise it could have advantages as a recent thread on kde-devel exposed."
    author: "ac"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-04
    body: "Is your system prelinked? I still don't think the large amount of systems are."
    author: "anon"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-05
    body: "No, it's not!\nSpeed does not seem to suffer (much) by setting these options, even on an unprelinked system.\n\nJust give it a try!"
    author: "ac"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-04
    body: "I heard about these options, but don't know where to set them. Compile-time variables? Or do you just have to export them? \nTIA\nMichael Jahn"
    author: "affenschlaffe"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-05
    body: "exporting seems to be ok...\n\nmy /etc/environment holds:\nexport KDE_IS_PRELINKED=1\nexport KDE_FORK_SLAVES=1"
    author: "ac"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-05
    body: "Dude, thanks so much for that tip! I've never liked kdeinit and being able to switch it off is a godsend!\n \nOn my (prelinked gentoo) system, there is no slowdown as a result. Subjectively, it even feels a bit snappier without kdeinit."
    author: "koffice fan"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-05
    body: "Strange, on my system application launch time and memory consumptions is noticeable worse with these options set.\nI stick with kdeinit for now ..\n\nBtw. what thread on security, have you got a link?"
    author: "koos"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-06
    body: "http://lists.kde.org/?l=kde-devel&m=109396400731118&w=2"
    author: "ac"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2004-09-06
    body: "Looks to me easier to fix selinux and fireflier instead of getting rid of kdeinit. If I look at the output from 'ps x', I see what programs/slaves are running. Just strip of 'kdeinit: '. So they could either support kdeinit directly (as KDE is not just an application), or add some finer grid configuration tools per application (as wine and probably others have these issues too)."
    author: "koos"
  - subject: "Re: Any other speedups in the pipeline?"
    date: 2005-06-16
    body: "> Looks to me easier to fix selinux and f\n\nWrong - absolutely dead wrong.\n\nthe use of fork() and the loading of libraries *is* a security problem, you know that: it's pretty obvious.\n\none process being compromised results in it being able to poison all threads, and also in it being able to utilise any file handles and other stuff [that i am not as familiar with as, say stephen smalley and the other selinux developers] to then compromise any child process etc.\n\nthe only way to obtain _complete_ security separation is to exec().\n\nthat is the reason why selinux \"domain\" tracking is done on an exec() and why fork() is most certainly not.\n\nthe bottom line is that kdeinit _used_ to provide a significant speedup, on older machines.  now it not only gets in the way but there is a better solution (prelink) and its use makes it impossible to do selinux \"domains\" for the separate kio handlers and the programs that utilise them.\n\nwhat if i want to write an selinux policy for konqueror that only allows it to do HTTP and IMAP, but nothing else?\n\ni can't even _begin_ that - because of kdeinit: i can't even track the different programs, because they are all named \"kdeinit\".\n\nif i want to make konqueror only be able to access HTTP, i can't because it's handled by _one_ program that loads all of the kioslave .so libraries.\n\nselinux, like i said, works by tracking \"exec\"()d programs, so it is _necessary_ to have a kioslave_http program, a kioslave_imap program.\n\nl.\n\n"
    author: "Luke Kenneth Casson Leighton"
  - subject: "Re: KDE_FORK_SLAVES broken?"
    date: 2004-09-07
    body: "Does KDE_FORK_SLAVES actually work for you?  For me on kde-3.3.0, it doesn't:\nhttp://bugs.kde.org/show_bug.cgi?id=88557"
    author: "Rex DIeter"
  - subject: "kde 4"
    date: 2004-09-05
    body: "I hope they build it from stratch with simplicity in mind. But obviously not going as far as Gnome 2.* went. And a different default theme which is not plastik and has no round corners unless it uses SVG."
    author: "blahster"
  - subject: "Re: kde 4"
    date: 2004-09-05
    body: "Rewrite from scratch? Why? I think you don't have clue what you're talking about if you suggest to rewrite KDE from scratch. It's a huge, complex piece of code, that generally works very well at the moment. Gradual changes is the way to go. This [1] article is informative on this matter.\n\n[1] http://www.joelonsoftware.com/articles/fog0000000069.html"
    author: "Andre Somers"
  - subject: "Re: kde 4"
    date: 2004-09-05
    body: "They did, for KDE 2. Because 1.0 kind of grew up in a very ad hoc kind of way (I still loved it) the rewrote KDE 2 from the ground up, they thought through the 'problems' of KDE 1, and they learnt. KDE 3 was an easier port (As Qt had changed its interfaces less.). KDE is now a huge chunk of good code that has aged well. Major changes will occur in the 4.0 timeframe, but starting over from scratch is just insane, there's nothing like fixing an obscure bug you crushed in the KDE 2 era, for KDE 4.0.1 ."
    author: "James"
  - subject: "Re: kde 4"
    date: 2004-09-05
    body: "It'd be pointless to rebuild KDE from the ground up, because most of the guts are quite solid. There are bits here and there that need restructuring (I'd love to see the KIO API rebuilt on top of a cross-desktop core), and the UI needs work (yay for Aaron Siego), but stuff like that can be built on top of the existing core.\n"
    author: "Rayiner Hashem"
  - subject: "Re: kde 4"
    date: 2004-09-06
    body: "Why don't you file a wishlist bug report on this, blahster?\n\nBug 89761: Rewrite KDE from scratch.\n\n/ Martin\n\n(Not an actual bug number.)"
    author: "Martin"
  - subject: "Still no fullname for me in the digest"
    date: 2004-09-05
    body: "Hmm.  My name is still shown as my login, ingwa, instead of my full name Inge Wallin.  See the Games section in the digest for an example.\n\nIs there anything that I can do to fix this or do I have to wait until somebody else fixes it?  If so, what can I do to help?"
    author: "Inge Wallin"
  - subject: "Re: Still no fullname for me in the digest"
    date: 2004-09-05
    body: "It's fixed now.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Still no fullname for me in the digest"
    date: 2004-09-06
    body: "Thank you!  What was the problem?"
    author: "Inge Wallin"
  - subject: "Re: Still no fullname for me in the digest"
    date: 2004-09-06
    body: "didn't update the kde-common/accounts file.\n\nDerek"
    author: "Derek Kite"
---
The <a href="http://cvs-digest.org/index.php?issue=sep32004">KDE CVS-Digest</a> for this week: 
<a href="http://www.kdevelop.org/">KDevelop</a> has a new project builder.
<a href="http://kmail.kde.org/">KMail</a> now supports kwallet for mail account passwords.
<a href=" http://www.koffice.org/krita/">Krita</a> adds <a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> scripting support.
<a href="http://www.koffice.org/">KOffice</a> now supports Indic.

<!--break-->
