---
title: "Illogic-al on amaroK"
date:    2004-11-18
authors:
  - "numanee"
slug:    illogic-al-amarok
comments:
  - subject: "server unreachable?"
    date: 2004-11-18
    body: "Timeout on server port 80.\n\nIs there a mirror?"
    author: "someguy"
  - subject: "Re: server unreachable?"
    date: 2004-11-18
    body: "Google cache <a href=\"http://64.233.183.104/search?q=cache:hMJP6478mFEJ:24.193.108.12/prop-amarok.html+grab+kdemultimedia+to+enjoy+the+goodness&hl=fi&ie=UTF-8\">here</a>\n<p>\nEleknader\n\n"
    author: "Eleknader"
  - subject: "Re: server unreachable?"
    date: 2004-11-18
    body: "Terribly sorry.  I didn't mirror it either. :(\n\nLooks like the amaroK site is down too."
    author: "Navindra Umanee"
  - subject: "Re: server unreachable?"
    date: 2004-11-18
    body: "> Looks like the amaroK site is down too.\n\nIt's for days already (server movement)."
    author: "Anonymous"
  - subject: "Re: server unreachable?"
    date: 2004-11-18
    body: "It's up again!  I've added a coral link, just in case."
    author: "Navindra Umanee"
  - subject: "Re: server unreachable?"
    date: 2004-11-18
    body: "Sorry 'bout that folks. The PC was \"Down for Maintenance\". I took it apart and stuff then put it back together again. Humpty Dumpty should be able to handle all the traffic you throw at him now :-)"
    author: "illogic-al"
  - subject: "What about useability?"
    date: 2004-11-18
    body: "The last time I used amaroK, I found that the analyzer always lagged behind the misic as heard by my ears, and that in my opinion, the pause and play button should be amalgamated into one. Anyone agree? Is the lag issue solved now? \n\nCb.."
    author: "charles"
  - subject: "Re: What about useability?"
    date: 2004-11-18
    body: "The lag issue is the fault of arts, the solution is to not use arts. I use xine. The next version of amaroK will have an equalizer for gstreamer so I'll probably start using that.\n\nThe reason for play/pause being seperate is that its generally bad form to change what a button does. However, they do now have a Play/Pause button available to add to your toolbar if you want it."
    author: "Ian Monroe"
  - subject: "Re: What about useability?"
    date: 2004-11-18
    body: "I had the same problem. I fixed it by dumping Arts and using Xine instead."
    author: "Janne"
  - subject: "typo"
    date: 2004-11-18
    body: "s/segway/segue/"
    author: "AC"
  - subject: "XMMS"
    date: 2004-11-18
    body: "\".... Gone are the days when KDE users would lament for an XMMS replacement.....\"\n\nWHY? amaroK & Juk have horrible user interfaces & contain way too much functionality for an audio player. XMMS is great (If only they could get it to minimiz/maximize the consol/playlist/equalizer simutanously).\n\nAll I want is small/compact user interface. A simple playlist selector/editor,  a simple equalizer with a few presets, and the odd visual effect.\n\nWinAmp 2 perfectected this interface/functionality combination & it can also be had using Quintessential with the Minima theme. XMMS may copy the WinAmp interface, but I like it for this very reason. The only other audio player interface that I liked more than WinAmp is Sonique, but sadly it is not available on Linux (to my knowledge) nor is stable on Win2k/WinXP"
    author: "thesimplefix"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "amaroK can be configured mostly to the xmmsphiles liking. The playlist can have the menu bar hidden and there's the XMMS-like 'player' interface. Equalizer is coming. You can use XMMS's visuals. Personally I think having to open a seperate window to select files is horrid UI (the problem I have when using Winamp 5 and when I used xmms), I'm glad amaroK doesn't really have an equivalent ('open media' is meant mainly for URLs).\n\nOr you can be better off and just use amaroK's playlist window as the main window."
    author: "Ian Monroe"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "come on, get real - winamp has a horrible interface. just because you are used to it doesnt mean its good. it sucks - small buttons, hard-to-use playlist etc etc.\n\nJuk's interface is so much easier. let a noob try xmms or juk - guess in what interface they start using playlists, have their songs playing etc first :D\n\ntry it yourself. make a playlist in juk. try it in xmms. switch playlist in juk. do it in xmms. compare.\n\nand suppose you have a mp3player. and you want some songs from your disk on the mp3player. well, not difficult: drag'n'drop them from juk. forget that with xmms... adding files? drag'n'drop. playlist creation? drag'n'drop. search a file? just type the name, and it searches real-time. hit enter, and it will only play the matching files, random and/or with repeat, according to your settings.\n\ndamn, and about all the settings in juk, I'm sure xmms has at least 10 times the amount of settings (could be even 50 times more...). and in juk you dont have to add songs to your collection (it does that automatically).\n\namarok is a bit more crowded, and not as easy to use (yet) but still has an edge over xmms."
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "Oh, you're sooo wrong for the interface. XMMS supports d'n'd, and does it properly. Unlike JuK. I had terrible experience with dragging files from konqueror to the juk playlist in the past. \nPlaylists in winamp are absolutely easy to create and use - with d'n'd, again, and without any problem.\n\nThen, resource usage. Two main memory hogs on my system were Juk and KDevelop, and, while latter memory usage is justified, why on Earth audio player is allowed to grab 46Mb total ?\nAnd, please, don't do something for me automatically - I want to categorize and  arrange my songs myself. \n\nAnd, most important - audio output quality. XMMS simply sounds better.\n "
    author: "Yarick"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "well, the last time I tried xmms, d'n'd didnt work, but maybe it does now. I cant start it, it crashes immediately (debian unstable. just removed it, I dont use it anyway...). but I dont understand why you like the interface. its hard to find out what the buttons do because they are so very small, the menu is *huge* and filled with options I dont know (and dont want to), you have to install lots of plugins to let it play like you want, (altough its good it DOES support plugins, juk should, too...), and it has no full-screen interface. I dont like the several seperate windows - I want just a full-screen app where I can see all my songs, easilly search through them, make playlists and I dont want to be looking for new songs and adding them to my playlist. \n\nJuk automatically looks in the appointed directory's for new songs and adds them to your collection playlist (simply the list where all songs are). I use clever playlists that automatically adjust when new songs are added (eg 1 searches for oldies, and adds them) and I have some 'static' ones I made myself. choice rules. all this is not possible in XMMS. I never used playlists in winamp, so I had to choose random in my whole collection. stupid. now I have playlists from several genres, just 1 doubleclick and they start playing.\n\nand Juk will be able to handle gstreamer soon, so the quallity issue will be solved I guess (arts sucks, although I dont care).\n\nand the resources are so huge because you count the shared libraries, I guess."
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "> and Juk will be able to handle gstreamer soon\n\nTo be clear, JuK has supported GStreamer for almost 2 years.  The recent update was just from 0.6 to 0.8."
    author: "Scott Wheeler"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "sorry ;-)"
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "To be honest, with a name like \"superstoned\", it's not surprising that you find it confusing...\n\n"
    author: "Xanadu"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "bit unfair to say such a thing. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "tnx fab. you're right."
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-22
    body: "I meant no disrespect, man.  None at all.  I just found it funny that someone giving themselves the name of superstoned was finding something confusing.  \n\nI'm not trying to \"down\" you in any way.  It was just the combination of the two things that was damn funny to my semi-warped mind...\n\n:-)"
    author: "Xanadu"
  - subject: "Re: XMMS"
    date: 2004-11-22
    body: "allright, thanx :D\n\ngreat we can keep our spirit up :)\n\nbe happy, dont worry and PEACE!"
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-22
    body: "Yes, I know, I posted my \"disclaimer\".  I'm sorry, but, the pun was far to obvious to pass...\n\nM.\n"
    author: "Xanadu"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "> well, the last time I tried xmms, d'n'd didnt work, but maybe it does now. \n\nJust launched xmms a last time to check that :\nYou\u00b4re right, dnd doesn\u00b4t work.\nIt works only one way : from Konqueror to xmms playlist\nBut it is impossible to select some item from the playlist (as I do often in amarok) and to drop them either in \n- k3b : ==> yeah, an audio cd made in 3 second\n- krename : ==> fast renaming of the tracks\n- konsole, go to the beginning of the line, type \"mplayer \", go to the end of the line and type enter. Et voila ! (I use that because mplayer 10s by 10s seeking in the file just rocks when you try to do a transcript of a long audio recording)\n\nSo, so far as I\u00b4m concerned, drag&drop does\u00b4nt work in xmms."
    author: "jmfayard"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "I used to use juk to fill my mp3player... whoulnt work with xmms, either, I guess :("
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-23
    body: "You don't have to be at the end of the line to hit enter in bash, so you can save yourself a keystroke there. ;-)"
    author: "kundor"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "The winamp/xmms interface is the pinnacle of simplicity. Granted, I don't like the crazy skinned widgets either, but you cannot fault it for being 'unfriendly'.\n\nFundamentally, you have a bunch of tracks. They are presented in a list. And. That's it. You choose the track you want.\n\nThere's none of this 'Oh, this track is from this album, but I accessed it through the search feature, so when I press _back_, will it play the previous track on the album, the track that came before it in the search results, or the last song i was playing in juk?' which you must admit all takes some getting used to."
    author: "Robert"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "I DO fault it for being unfriendly. when you open the menu, its horrible. and it cant play much filetypes, unless you add plugins. plugins rule, but most users dont add them. and how can I search, and let it play only the searched songs? and I want playlists. lots of them, and a easy way to start playing them. and to create them. and please, some auto-search playlist, which simply searches for 'genre: oldies' and adds them. and back just goes to the previous song, I guess... like it should... the last played one, isnt it? just like xmms does, on random. if it doesnt, please file a bug ;-)\n\nbtw I use amarok mostly, I like the onscreen display and the nice info about the current song (with times played, other albums from artist, other songs on album, coverpicture and automatic ratings for songs)."
    author: "superstoned"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "I just recently switched back to XMMS again after using Amarok for a while. My main problem with Amarok was lack of a good output-engine. Arts and gstreamer are both terrible with regard to cpu usage and/or skipping quickly through MP3 files. I've used it with Xine for a while, until I noticed it wouldn't play some of my OGG's (although XMMS would) and it didn't allow me to seek through MP3's that I played through a custom-made input plugin for Xine that streams from smb:// locations. So, basically - I'm back to XMMS again (after trying for the fourth time to change my habits :)."
    author: "Jelmer Feenstra"
  - subject: "Re: XMMS"
    date: 2004-11-19
    body: "aRts, gstreamer, mas, nmm and xine. All suck? I can't belive that. I am using Amarok 1.1.1 with gstreamer (plugin-version) 0.85 and its works great!"
    author: "Carsten"
  - subject: "Re: XMMS"
    date: 2004-11-18
    body: "XMMS is truly unusable and horrible for me. Maybe I'm just getting old. I tried it several times. I have a 19 inch monitor on my desktop running at high resolution. Aside from the AMD64 in my notebook I also like the bright screen as that monitor seems impossible to adjust as bright without washing out. Freaking XMMS opens in a dark theme at a fixed pixel size geared to apparently a 640x480 screen. There is no way to tell where the interface buttons are or what they are without glueing my face to the screen. I did figure out how to make it twice as big but that was horribly pixelated. In the end XMMS was too non standard, too difficult to figure out how to make it do what I wanted and not worth my time when there were KDE players that had standard interfaces and scaling layout management. (Thanks Qt!) Don't talk to me about skins. I don't want a player that looks like a sea shell until sea shells come with clearly marked buttons that scale to be readable at 1200x1024.\n\nFor me XMMS is an example that users will quickly lose patience with a horrible interface. Of course patience can be affected by whether you got a computer to work or play. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: XMMS"
    date: 2004-11-20
    body: "Hm back in my windows days, I used winamp5. They (nullsoft) finally agreed that a simple list is just not enough for a \"multimedia\" player. So they added the \"Library\". I mean draging from juk to k3b seems like a nice hack but being able to do it right from juk would be even better. Virtual directories which can themselfs have subdirectories and files, simple ripping, directory watching, radio selection, etc. Together with free-form skin support it was my #1 app."
    author: "miro"
  - subject: "Re: XMMS"
    date: 2005-01-23
    body: "Just use the \"double size option\"?"
    author: "Mek"
  - subject: "Why is everybody and his dog steamed about arts?"
    date: 2004-11-18
    body: "From a user perspective, I don't get it. Arts just works in the background, doesn't put undue load on my system, and it never crashes. I run every app with sound either directly through arts or via artsplay, with the exception of lbreakout2, whose binary is not recognized as such by arts.\n\nSo, could someone explain to me the ubiquity of those \"I hate arts\" statements?\n\nAnd yes, amarok ist The Best!"
    author: "Flitcraft"
  - subject: "Re: Why is everybody and his dog steamed about arts?"
    date: 2004-11-18
    body: "Well, for quite some time arts sound always paused for a second\nafter playing a song for some time. The CPU load was extremely high.\nThis only got better with KDE 3.3 or so. Somehow the problem\nvanished. Before it was not usable because I couldn't find out\nif a song file was broken or if it was just arts. I never\nfound out why this was happening.\nStill _very_ annoying: If you have multiple family members logged\non and they use your PC and switch with Ctrl+F7,F8,F9,... then\nonly the first user that has logged on can play sound.\nCompletely stupid. That's why I disabled all arts completely\nand play sound with MPlayer.\n\n"
    author: "Martin"
  - subject: "Re: Why is everybody and his dog steamed about arts?"
    date: 2004-11-18
    body: "as of KDE 3.3, thanks to aKode, that is no longer the case."
    author: "charles samuels"
  - subject: "Re: Why is everybody and his dog steamed about arts?"
    date: 2004-11-19
    body: "Can you please explain how can I use akode to allow multiple users play sound at the same time ?"
    author: "J. Carlos"
  - subject: "Re: Why is everybody and his dog steamed about arts?"
    date: 2005-03-09
    body: "tell alan cox to start caring about \"the desktop\""
    author: "charles samuels"
  - subject: "Re: Why is everybody and his dog steamed about arts?"
    date: 2004-11-18
    body: "And not only that, but if you compile code a lot or do other things that both use the disk a lot and use a lot of CPU, then arts has a tendency to stutter and skip. ogg123 doesn't do that, curiously enough."
    author: "Boudewijn Rempt"
  - subject: "Re: Why is everybody and his dog steamed about arts?"
    date: 2004-11-18
    body: "Well, apart from the CPU burden (which has mostly gone away these days) it also adds a pig painful layer of latency to every sound operation that a user performs. Press play.... wait..... there go the speakers. And don't even think about using arts for sound when playing a video *guffaws*."
    author: "c"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-18
    body: ">From a user perspective, I don't get it. Arts just works in the background, doesn't put undue load on my system, and it never crashes.\n\nFor Arts >=1.3.0 this is not true. Trying to \"preview\" audio files in Konqueror locked my whole system from time to time, so I disabled the feature. Running the box on high load it's possible to see it stall and getting a wonderful \"arts cpu overload\" (or something likes this) message. And I'm not the only one fighting these problems.\n\nAnother problem with Arts is, that it unnecessarily needs cpu cycles (~5% on a 1.2GHz box), even though the audio application is paused.\n\n\n"
    author: "Carlo"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-19
    body: ">arts has a tendency to stutter and skip\n>it unnecessarily needs cpu cycles\n\nDoes not aKode lib in 3.3 fix both those issues?"
    author: "Morty"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-19
    body: ">Does not aKode lib in 3.3 fix both those issues?\n\nThat's what I notice, running either Amarok 1.1.1 or Juk (KDE 3.3.1), so I think that's not the case."
    author: "Carlo"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-19
    body: "You sure aKode are installed, without aRts fall back to the same libs as used with 3.2 and you would see no difference."
    author: "Morty"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-20
    body: "Yes. But that reminds me, that Juk (or whatever, did not investitgate) is crashing, playing mpc's. :-/\n\n>slocate akode\n/usr/kde/3.3/lib/mcop/akodearts.mcoptype\n/usr/kde/3.3/lib/mcop/akodeMPCPlayObject.mcopclass\n/usr/kde/3.3/lib/mcop/akodearts.mcopclass\n/usr/kde/3.3/lib/mcop/akodeVorbisStreamPlayObject.mcopclass\n/usr/kde/3.3/lib/mcop/akodePlayObject.mcopclass\n/usr/kde/3.3/lib/mcop/akodeMPEGPlayObject.mcopclass\n/usr/kde/3.3/lib/mcop/akodeSpeexStreamPlayObject.mcopclass\n/usr/kde/3.3/lib/mcop/akodeXiphPlayObject.mcopclass\n/usr/kde/3.3/lib/libarts_akode.la\n/usr/kde/3.3/lib/libarts_akode.so\n/usr/kde/3.3/lib/libakode.so.1\n/usr/kde/3.3/lib/libakode_mpeg_decoder.la\n/usr/kde/3.3/lib/libakode_mpeg_decoder.so\n/usr/kde/3.3/lib/libakode.la\n/usr/kde/3.3/lib/libakode.so\n/usr/kde/3.3/lib/libakode_src_resampler.la\n/usr/kde/3.3/lib/libakode_src_resampler.so\n/usr/kde/3.3/lib/libakode_xiph_decoder.la\n/usr/kde/3.3/lib/libakode_xiph_decoder.so\n/usr/kde/3.3/lib/libakode.so.1.0.0\n/usr/kde/3.3/lib/libakode_mpc_decoder.la\n/usr/kde/3.3/lib/libakode_mpc_decoder.so\n/usr/kde/3.3/include/akode\n/usr/kde/3.3/include/akode/fast_resampler.h\n/usr/kde/3.3/include/akode/streamtoframe_decoder.h\n/usr/kde/3.3/include/akode/framedecoder.h\n/usr/kde/3.3/include/akode/encoder.h\n/usr/kde/3.3/include/akode/decoder.h\n/usr/kde/3.3/include/akode/mmapfile.h\n/usr/kde/3.3/include/akode/localfile.h\n/usr/kde/3.3/include/akode/file.h\n/usr/kde/3.3/include/akode/frametostream_decoder.h\n/usr/kde/3.3/include/akode/audioconfiguration.h\n/usr/kde/3.3/include/akode/sink.h\n/usr/kde/3.3/include/akode/streamdecoder.h\n/usr/kde/3.3/include/akode/audiobuffer.h\n/usr/kde/3.3/include/akode/resampler.h\n/usr/kde/3.3/include/akode/crossfader.h\n/usr/kde/3.3/include/akode/akodelib.h\n/usr/kde/3.3/include/akode/audioframe.h\n/usr/kde/3.3/include/akode/pluginhandler.h\n/usr/kde/3.3/include/akode/wav_decoder.h\n"
    author: "Carlo"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-20
    body: "Please open bug-reports for the problems you see. Both JuK, akodelib and taglib are very well maintained."
    author: "Carewolf"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-20
    body: "The preview crash-problem was fixed in KDE 3.3.1.\n\nIf arts spends unnecessary CPU time it is because you sound-driver is broken a sending (am done playing, send more data) events all the time. Especially ALSA-drivers are often bad."
    author: "Carewolf"
  - subject: "Re: Why is everybody and his dog steamed about art"
    date: 2004-11-21
    body: ">The preview crash-problem was fixed in KDE 3.3.1.\n\nApparently it isn't. Just like #68528 no one seems to have an idea about it...\n\n>Especially ALSA-drivers are often bad.\n\nIf Arts had not too much issues in the past and applications, which use Alsa directly wouldn't work without any problem, I'd take that into account.\n"
    author: "Carlo"
  - subject: "foobar"
    date: 2004-11-18
    body: "Sorry guys, I don't care about graphical overkill.\nI love eye-candy, I use KDE, but for my music I still launch VMware to run foobar2000. [Yes, it works with wine... partially; but only partially.]\nFor simplicity and being powerful while still usable, foobar2k is the best player for me.\n\nAnd I have a keen eye on the developments of beep-media-player and lamip :>.\n\n - d"
    author: "deucalion"
  - subject: "Re: foobar"
    date: 2004-11-18
    body: "This is the most ROTFL-ing thing I've ever read when talking about multimedia and Linux."
    author: "Davide Ferrari"
  - subject: "Re: foobar"
    date: 2004-11-24
    body: "you're insane, man..."
    author: "c0p0n"
  - subject: "KDE Multimedia 4.0"
    date: 2004-11-18
    body: "<i>It'll be interesting to see what KDE Multimedia 4.0 looks like.</i>\n\nAllow me to sound negative, but in my view, realistic.\n\nIn the year 2005, KDE multimedia will still not be too impressive I suspect. Yes Juk and Amarok are nice, stand alone music players that is.\n\nMultimedia on KDE, to be fair, currently is a stand alone music player and a stand alone movie player (non-kde). If you can get things to work, Mplayer and Xine will play everthing you want. From a UI point of view, both these are very different and totaly not related to kde in any way.\n\nThen we also have a Noatun \"thing\", which basically is one big ass programming framework who no one ever sees use in writing great stuff for (we go down the separate Juk and Amarok route and make it all more spread out and now we are most things twice where Noatun could prevent that). Noatun is like a piece of leftover never really used and apprciated. In the earlier KDE days it used to drag your system to the ground, you was lucky if it did not skip like hell and even if it did work, you was stuck with horrible UI and bad default playlists. Sure some people might still use it these days, but it is lightyears away from being a practical multimedia solution. Unless great practical plugins arrive for it which make Noatun absolutely interesting to use, Noatun will drag and drag and drag with KDE in the future.\n\nSo we will be stuck with at least one music player (but it plays movies also) and at least one other music player and the end-user can figure out why the hell that is and why it is confusing things.\n\nTo add to the confusion, we have Kaboodle. OK, so, if Kaboodle is a media player, what does that make Noatun? If you think you where confused, now you reached a level of confusion where you just happy you can start xmms, mplayer, xine, amarok, juk or something else and leave those kaboodle and Noatun laying around uselessly.\n\nYes that sounds very negative, but I really love using KDE. KDE is almighty impressive. There is no other desktop solution I would want to use at this moment. I could go on and on about how great I believe it is. But if there is one thing which I think is really really horrible in KDE, it's multimedia. Sure it will improve over time, but I have a gut feeling this is going to take some years still. KDE 4.0 is going to be even more great, but I seriously believe that even in KDE 4.0 multimedia will _feel_ like a hacked together bunch with useless leftovers from the past which you might get to work. KDE multimedia is big ass challenge for KDE and I doubt KDE4.0 is going to really impress me with multimedia. Well, this is just my oppionion, and I would really like to be prooven wrong by the time I can start using 4.0."
    author: "ac"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: "Uh, the discussion about Multimedia in KDE 4.0 has been about Arts and it's replacement, and not about individual multimedia-apps as such.\n\nWhy two musicplayers you ask? Juk And Amarok are quite different beasts. IMO one is not a replacement for the other, since their design is totally different. And besides, Juk ships with KDE, Amarok does not. What do you suggest we do? Ask Amarok-developers to stop developing their app? Yeah, right.\n\nAFAIK, Amarok was invited to be shipped with KDE, but the developers declined. So we now have a built-in KDE music-player (Juk) and a music-player that ships outside of KDE (Amarok). IF Amarok shipped with KDE, then we should think about doing something with Juk. But, as things are now, that is not required, since Amarok does not ship with KDE. Juk already kicks ass, and I would assume that it kicks even more ass in the future. So as far as multimedia-apps are concerned, I see no reason to worry. We already have kick-ass apps, and they will keep on kicking more and more ass.\n\nAnd what is this \"non-KDE\" movieplayers you talk about? Kaffeine? Kmplayer? Both are KDE-apps.\n\nThe one area that desperately needs work in KDE's multimedia is the sound-server (Arts). Luckily Arts is on it's way out."
    author: "Janne"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: ">>Uh, the discussion about Multimedia in KDE 4.0 has been about Arts and it's replacement, and not about individual multimedia-apps as such.\n\nSo what? Multimedia apps are part of the whole thing, outside apps tend to have much outside dependencies. It's not simply just Arts.\n\n>>Why two musicplayers you ask?\nNo I don't ask that. I _tell_ you about confusion and clutter.\n>>Juk And Amarok are quite different beasts.\nSee, you don't get my point. Two musicplayers are not Amarok and Juk, but Amarok and Noatun. Actualy it's three, Amarok, Noatun and Kaboodle. (just using Amarok as an example). And if you add the others you mention, Kaffeine and Kmplayer, make that 5 players (audio/media) of some KDE kind. It's at least 4 projects being a multimedia player or solution.\n\n>>So as far as multimedia-apps are concerned, I see no reason to worry.\nWell, where did I say I worry about multimedia apps? I am talking about Noatun and Kaboodle here as so called pure KDE apps and clutter and confusion. Juk is a good audio player and just that. That already makes 3 audio players (add noatun and kaboodle), where 2 are much considered clutter, but they are the KDE multimedia framework.\n\n>>And what is this \"non-KDE\" movieplayers you talk about? Kaffeine? Kmplayer? Both are KDE-apps.\nWell great. How depended on outside libraries are those? Do they fit in the KDE multimedia framework well? Will including them mean many new dependencies? How likely are they to end up in a default KDE build in 4.0? I hope to see them added if they mean great things, sure. Are they all just sort of GUI's for engines, how many GUI's are we talking about here then? What about Noatun and Kaboodle, arn't those supposed to be the multimedia framework already?\n\n>>The one area that desperately needs work in KDE's multimedia is the sound-server (Arts). Luckily Arts is on it's way out.\n\nWell, getting it out is not allot of work, more a thing of convinsing relevant people. Are you telling me all we have to do is get it out and replace it by something else and all of a sudden Noatun, Kaboodle, Kaffeine and Kmplayer are all doing the same thing, but added all into kde-multimedia?\n\nLook just because I offer you an oppionion does not mean I can't change that oppionion, but it will take a solid kde multimedia framework, without clutter from the past, confusion and lots of dependencies on outside applications, that's the part I think needs work also."
    author: "ac"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: "\"See, you don't get my point. Two musicplayers are not Amarok and Juk, but Amarok and Noatun. Actualy it's three, Amarok, Noatun and Kaboodle. (just using Amarok as an example). And if you add the others you mention, Kaffeine and Kmplayer, make that 5 players (audio/media) of some KDE kind. It's at least 4 projects being a multimedia player or solution.\"\n\nAFAIK, Noatun and Kaboodle are on their way out. And yep, there are different apps aout there, like Kaffeine and KMplayer. And, again: what do you suggest we should do? Force the developers not to work on their apps? AFAIK, neither Kaffeine or Kmplayer ship with KDE.\n\nHell, what if I decided that none of the current apps are any good and decided to write my own app. What would you do about it? Beat me up?\n\n\"Well great. How depended on outside libraries are those?\"\n\nXine and/or Mplayer. And I see no problem with that. Or are you suggesting that instead of using a library that already exists and works, KDE-devels should re-invent the wheel and write yet another library from scratch? Uh, OK....\n\n\"Do they fit in the KDE multimedia framework well?\"\n\nConsidering that future framework for KDE is still in the air, it's too early to say, But I would say yes.\n\n\"Well, getting it out is not allot of work, more a thing of convinsing relevant people. Are you telling me all we have to do is get it out and replace it by something else and all of a sudden Noatun, Kaboodle, Kaffeine and Kmplayer are all doing the same thing, but added all into kde-multimedia?\"\n\nNo. Could you please show me where I made a claim like that? In the future, Kaboodle, Noatun and the like will most likely be dropped. The apps that will be there are going to be Juk and a movie-player (Kaffeine?). And I see no problems with having a separate music-player and a separate movie-player.\n\n\"but it will take a solid kde multimedia framework, without clutter from the past, confusion and lots of dependencies on outside applications, that's the part I think needs work also.\"\n\nIf there are already \"outside applications\" that already work, it would be stupid to not take advantage of them. Did you know that the multimedia-framework on KDE4.0 will come from \"outside\" (the horror!). And the movie-players rely on outside libs/apps (Xine/Mplayer). And why shouldn't they? Instead of wasting time reinventing the wheel, the time should be spent elsewhere."
    author: "Janne"
  - subject: "The greatness of Kaboodle"
    date: 2004-11-18
    body: "Whatever happens to KDE multimedia I hope Kaboodle newer goes, it is the single most usable multimedia app in KDE. If you use it the way it was meant to be used. Namely playing of single files, you have none of the crap the other players have like playlist, visualizing tools, skinning etc. You use it to check files you find before you decide to add it to your playlist or just to quickly test. Kaboodle is clean and fast, if you click on a file in Konqueror it only plays it and goes away and most important it leaves your playlist alone. Anyway comparing Kaboodle to Noatun/Juk/amoraK/Kaffeine is like comparing KWrite to KDevelop. They are different tools meant for different tasks."
    author: "Morty"
  - subject: "Re: The greatness of Kaboodle"
    date: 2004-11-19
    body: "I agree, Kaboodle is great for that though I say your analogy is wrong.  It would be better to compare KEdit (KWrite's little cousin) to KDevelop.\n\nI use Kaboodle to play all those weird WAV files my friends send me (I don't want quote from Invader Zim being added to my playlists, even if they are incredibly funny)\n\nI say there should be 3 multimedia apps in KDE (shipped): Kaboodle for single media file playing, amaroK for audio files and audio library, and either Kaffeine (Xine Front End) or KPlayer (MPlayer Front End) for movie playback (I believe Xine is already a dependancy of KDE-Multimedia, so thats no extra stuff to add for Kaffeine)"
    author: "Corbin"
  - subject: "Re: The greatness of Kaboodle"
    date: 2004-11-19
    body: "> Kaboodle for single media file playing, amaroK for audio files and audio library\n\nI think it's a bad idea, you don't want to have to apps for playing sound, default audio apps in kde have to be default audio apps in konqueror."
    author: "gnumdk"
  - subject: "Re: The greatness of Kaboodle"
    date: 2004-11-19
    body: "Nope, you have to consider the usage patterns and the difference of playing single files from Konqueror and managing your music collection in a userfriendly manner. An apt analogy are the case of Kuickshow and Digikam. "
    author: "Morty"
  - subject: "Re: The greatness of Kaboodle"
    date: 2004-11-19
    body: "Seconded (or thirded?)---please keep kaboodle!"
    author: "TDH"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: ">>Hell, what if I decided that none of the current apps are any good and decided to write my own app. What would you do about it? Beat me up?\n\nYeah kick your ass. Are you serious? I am only pointing out the confusion and you helped clear some of that.\n\n>>\"Well great. How depended on outside libraries are those?\"\n>>Xine and/or Mplayer. And I see no problem with that.\n\nI see no problem usability wise, but simply does this mean that KDE will choose either of these two and add the engine code of either of the two into the official KDE source tree? All I am asking if it means just that, KDE adopts that engine code in it's own source tree, I doubt that currently.\n\n>>\"Do they fit in the KDE multimedia framework well?\"\n>>Considering that future framework for KDE is still in the air, it's too early to say, But I would say yes.\n\nExactly, it's all in the air. All I did was offer an oppionion about how that air was likely to flow, but your comment on Kaboodle offers at least some clearance.\n\n>>Kaboodle, Noatun and the like will most likely be dropped.\n\nIF \"most likely\" means -for sure- in KDE 4.0, my oppionion on it's multimedia was false granted and things are going to effectivly change. If else, I made my point well after all.\n\n>>And I see no problems with having a separate music-player and a separate movie-player.\n\nNeither do I.\n\n>>If there are already \"outside applications\" that already work, it would be stupid to not take advantage of them.\n\nObviously. I only hope it's technically feasible to do and does not run into troubles of dependencies and cross-platform-ability, that's my main concern. It's not just a matter of simply being able to take advantage an application. I do not think the question is directly if KDE would ever want to do that.\n\n>>Did you know that the multimedia-framework on KDE4.0 will come from \"outside\" (the horror!).\n\nErm no and you somehow believe I consider that a horror - where can I read about this framework going to be from \"outside\", in solid form, not in an arguement? If this really means a solid solution, no more confusion and clutter from the past, amen.\n\n>>And the movie-players rely on outside libs/apps (Xine/Mplayer). And why shouldn't they? Instead of wasting time reinventing the wheel, the time should be spent elsewhere.\n\nYes off-coarse! Just as long as it all works slick in practice, the relevant developers agree with you, it stays cross-platform, yes in theory it then works. Untill then, I will wait and see what KDE 4.0 will offer, not in theory but in practice.\n"
    author: "ac"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: ">I see no problem usability wise, but simply does this mean that KDE will choose >either of these two and add the engine code of either of the two into the >official KDE source tree? All I am asking if it means just that, KDE adopts that >engine code in it's own source tree, I doubt that currently.\n\nno, KDE wouldn't incorporate xine or mplayer. KDE would depend on it. Its the polite open source way of doing things."
    author: "Ian Monroe"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: "KDE 4.0 will feature (most likely) Gstreamer as the multi-media backend. or it will offer a choice between several.\n\nanyway, KDE will ship with Juk (noatun and kaboodle will be dropped, I guess) and probably amarok as audioplayers (an easy to use: juk and an advanced: amarok). and with kmplayer and kaffeine as videoplayers (again: easy and advanced). nothing wrong with that, as all apps (again, I think) will be able to simply use the KDE media backend (be it gstreamer or something else).\n\nSo I guess that's the future. several mm-frameworks to choose from, and 2 players for music, and 2 for video, in both cases an easy-to-use and a more advanced.\n\nMultimedia is really getting better, imho...\n\npersonally I love this configuration, I hate a all-in-one (like the horible windows media player) and I love choice."
    author: "superstoned"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: "That looks promising then.\n\nIt's good seeing KDE go with multimedia back-ends so people can simply focus on just apps and intergrating functionality into other apps.\n\nIt's also good to see video and music apps being split and both having an easy and more advanced solution for them. I think this should be good news for people now using Kaboodle for \"quick checking\". I haven't much good to say about Noatun other then being relieved to see it go.\n\nI guess it all means we can also begin to see much better Konqueror-web-multimedia intergration.\n\nKDE just keeps getting more and more amazing, and this is only the multimedia part."
    author: "ac"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "and if you had at least a bit of a clue you wouldn't write such lies."
    author: "mETz"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "and if you had at least a bit of a clue you wouldn't write such lies as apps getting dropped or gstreamer being chosen as a backend."
    author: "mETz"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "I dont say gstreamer IS being chosen, I said it MIGHT be chosen. but (as I said too) its more likely there will be choice. as always, in KDE.\n\nand about apps getting dropped, someone is going to have to port kaboodle and noatun, and both dont see much development, dont they? noatun is the master of crashes, and kaboodle - well, its indeed nice for a preview. but I guess that can be done better. I dont even considder kaboodle a mp3player, its just a preview-app. like the difference between kuickshow and pixieplus. but then, kuickshow is quite nice, kaboodle isnt.\n\nanyway, kaboodle might stay, but noatun is useless with all these much more advanced and *working* musicplayers for KDE."
    author: "superstoned"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "Good you find Kaboodle nice for preview, it's exactly what it was made to do. No other app mentioned in this discussion does this job as well as Kaboodle so none of them can replace it. I can't see why Kaboodle actually need much development as it does what it is supposed to do, and please explain how this can be done better? As a side note the sources for Kaboodle are slightly less than 55k.\n\nAs for Noatuns crashes I can't remember last time I had it crashing, and I am using it as my primary music player. Sometimes arts goes down when using internet radio, but it restarts automatically so that's no real problem. I only stop Noatun when I watch video's or use Kaboodle on singel files. There may be several reasons why Noatun don't crash for me, but one reason may be I never add ogg/mp3's to the playlist without being sure of the quality of the files (say Kaboodle). And my collection are not very big, approximately 4G.  \n "
    author: "Morty"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "well, I haven't used noatun for some time, so it might be a lot more stable now. maybe I should give it a try... but it doesnt have the ultra-clean interface juk has, and doesnt give all the features amarok has - altough it has a plugin-structure, so it should be easy to add these features. hmm, pitty the different developers prefer working on different apps instead of one great musicplayer :D (I considder adding musicplaying to a videoplayer, and video to a musicplayer, just plain stupid. konqi is difficult enough, multi-functional as it is... I dont like windows-like behaviour like the cdburner being able to rip cd's, the musicplayer being able to burn cd's, the videoplayer being able to play mp3's etc etc. madness, imho. and (in the windows case, whoulnt be in KDE, indeed) needless duplication of work... so much for the duplication of efforts in OSS)"
    author: "superstoned"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "I have been using Noatun since 2.0 and when I considered swiching to juK it was not avalible on the distro I ran at the time, and I haven't gotten round to try it out now:-) As for clean interface Noatun has several interfacepuggins, the Milk-Chocolate are as simple as they get. Personaly I always run it minimized using the system tray pluggin, and this is close to juK's. The only weaknes I have found are the playlists, none are as good as juK's. But since I'm used to it, it doesn't anoy me so much anymore:-). As for features, I use only loop and random play so I can't say I realy care.\n\nBut we agree at least on one thing, the combination of music/video players:-) Serliously why would anyone want a playlist in a videoplayer?\n"
    author: "Morty"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "\" where can I read about this framework going to be from \"outside\", in solid form, not in an arguement?\"\n\nSimple: None of the proposed backends (Gstreamer, MAS etc. etc.) are related to the KDE-project. They are all separate projects. Hell, Gstreamer has ties to the Gnome-project!\n\nYou could say it's the same thing as with Xorg. KDE requires Xorg (or Xfree or some commerial Xserver. But I'll focus on Xorg for simpicitys sake), yet Xorg is not a KDE-project. It's totally a separate project. Yet nobody considers that to be a bad thing. Why should it be different with sound-servers? It should matter even less. KDE would not work without X-server, but it would work without sound-server. Yet nobody complains that KDE is totally dependant on Xorg (an outside project), but now some are concerned that KDE would have SOME dependancies on a another separate project."
    author: "Janne"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "exactly. and KDE will use d-bus soon. a freedesktop-project, the horror!"
    author: "superstoned"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "I know all that, but thanks anyway. Now are we or are we not going to use xine AND mplayer as back-ends both at the same time? Or was is GStreamer? Or are we going to support several other multimedia back-ends as well? Or is all this just way up, way up in the air? I don't think anyone in here really has all the answers at all, much as they would like to make you believe everything is sorted out. One tells you this, the other tells you that and that is why I wrote my first message in the first place.\n\nFrankly I believe I am being told by people simply what they believe and what they would like and not at all what is going to be the actual plan in solid practical form, that's the whole point, there is no definitive plan, or so it seems at the moment. KDE and multimedia remains a joke, just look at this: http://www.kde.org/areas/multimedia/ , is that the mecca of KDE multimedia?\n\nOne thing has still remained the same, I wonder how the multimedia eventually really turns out in KDE 4.0 and what to really expect. I remain rather skeptical, but just for the moment."
    author: "ac"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "\" Now are we or are we not going to use xine AND mplayer as back-ends both at the same time?\"\n\nThere are KDE-apps that use Xine, and there are KDE-apps that use Mplayer. But none of those apps (AFAIK) ship with KDE. Maybe one of them will ship with KDE one day, but as of now, they are separate apps that ship outside of KDE.\n\n\"Or was is GStreamer\"\n\nGstreamer is a sound-server. It replaces Arts (that is, if they choose to move to Gstreamer). Gstreamer (or Arts) is not a replacement for Xine on Mplayer.\n\n\"Or is all this just way up, way up in the air?\"\n\nNo official decision regarding the sound-server have been made. We'll see.\n\n\"KDE and multimedia remains a joke, just look at this: http://www.kde.org/areas/multimedia/ , is that the mecca of KDE multimedia?\"\n\nNo. And I don't think KDE and Multimedia are a joke. Arts sucks, but it's on it's way out. And some of the kick-assest multimedia-apps are KDE-apps (Kaffeine, Amarok, Juk, K3b etc.). I think multimedia in KDE is doing just fine. Sure, some of those apps do not ship with KDE as such, but I see no problem with that, as long as they are KDE-apps and they are actively maintained."
    author: "Janne"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-19
    body: "\"Gstreamer is a sound-server\"\n\nNo it isn't, it is like the other part of aRts, the framework for decoding, filtering, etc.\n\nHowever it can output to most of the available sound servers, including aRts.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-18
    body: ">Multimedia on KDE, to be fair, currently is a stand alone music player and a stand alone movie player (non-kde). If you can get things to work, Mplayer and Xine will play everthing you want. From a UI point of view, both these are very different and totaly not related to kde in any way.\n\nRegarding the UI there are e.g. KPlayer, KMplayer and Kaffeine as frontends. I for one don't really need a Multimedia hoK, doing all and everything under the sun. Some VJ application would be fun, though."
    author: "Carlo"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-20
    body: "qt only, but nice... mixxx.\nget it at http://mixxx.sourceforge.net"
    author: "miro"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-21
    body: "I wrote VJ, not DJ. And mixxx suxxx imho :)"
    author: "Carlo"
  - subject: "Re: KDE Multimedia 4.0"
    date: 2004-11-20
    body: "here is what apps I use for multimedia (personal definition\nof multimedia) in kde:\n\nlisten to a single music file: kaboodle\nmanage and play my music collection: juk\nwatch a movie: kmplayer\nlook to a single picture: kuickshow\nmanage my digital foto collection: kimdaba\nedit an image: kolourpaint\n\nI don't feel limited or irritated by kde when doing any\nof the above in kde. So to me kde is on the right track."
    author: "pieter"
  - subject: "NMM?"
    date: 2004-11-18
    body: "Hello,\nis NMM still on the run for a backend of KDE Multimedia? Or is it gstreamer now?\n\nThx,\n\nFlorian"
    author: "Florian"
  - subject: "Re: NMM?"
    date: 2004-11-18
    body: "Both are still in the game, as you will be able to switch between multiple backends (similar to amaroK's engines).\n\nGStreamer will probably become the default backend, but this has not yet been decided.\n"
    author: "Mark Kretschmann"
  - subject: "amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "We have a juicy beta release coming up, and will release as soon as our website is back. \n\nSome of the new features:\n\n* 10-band IIR equalizer for GStreamer\n* MySQL support for the Collection\n* New NMM engine with video support\n* Tags are read from the database if available\n* OSD shows album covers\n* Automatic song lyrics fetcher\n\n\nStay tuned :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "COOL!, thanks a lot, can't wait until the new version is released, Amarok rocks!"
    author: "LB"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "/ * Automatic song lyrics fetcher /\n\n<3. I've always wanted something like that."
    author: "Illissius"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "Question, why doesn't amaroK ship by default with KDE?  I was under the impression that they were asked and turned it down.  That would be a horrible shame considering that it is easily the best audio player for KDE.\n\nBobby"
    author: "brockers"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "\"Shipping by default\" is only a matter of packaging. A packager can always use the latest compatible release from amaroK and package it along the new KDE packages.\n\nBeing a separate module/package allows more releases - to get new features earlier to the users - than being bound to the KDE release cycle, which has a strong focus on the framework rather than on features of single applications.\n"
    author: "Kevin Krammer"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-19
    body: "Yes, I agree. For instance amaroK is now the default audio player in SuSE Linux 9.2. Just as K3B is the default burning application, which is located in kdeextragear as well.\n\nKdeextragear applications _are_ part of KDE. They enjoy all the nice services that KDE offers (CVS, mailing lists, translations..), without the problems that come with the long release cycle of KDE core.\n"
    author: "Mark Kretschmann"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "Jippie! I've been heavy surprised about AmaroK 1.1 which works great and whose functions really make me happy (esp. when having MUCH music ;)  ) If this app becomes even better, why use other players? The last I whished was 'guessing tags from Database' as JuK did (does anyone know, why it doesn't anymore?). If this one is in Amarok, I'll be completely happy! ;)"
    author: "kuDDel"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "JuK still does that -- assuming you mean \"internet\" rather than database.  It's possible that your distribution messed up the packages though as it requires the libtunepimp library; if that's not available JuK doesn't have that feature."
    author: "Scott Wheeler"
  - subject: "Re: amaroK 1.2-beta1 coming soon"
    date: 2004-11-18
    body: "Okay, thanks for the hint, it semms SuSE killed that feature in KDE 3.3.1 for SuSE 9.1, I can't understand why they did so... In KDE 3.3.0@SuSE 9.2 it seems to be implemented. So I'll upgrade my SuSE to downgrade my KDE, and this will add a feature, I had before... Funny world ;)"
    author: "kuDDel"
  - subject: "Automatic song lyrics fetcher!? -hah-!"
    date: 2004-11-18
    body: "Yes, yes, yes!!!\n\n...\n\n...\n\nyes!!!!"
    author: "Darkelve"
  - subject: "Input Lag"
    date: 2004-11-18
    body: "Has anyone else had problems with laggy input on Amarok?\nThe problem I'm having is that I press a button like pause/stop/play, and it takes a while before amarok actually performs the action.\nIt seems to depend on the engine, with the xine engine its much better but the response time is still not \"instant\" like it is with xmms.\n\nAny suggestions on where to fix this?\n\nOh yeah..  and amarok kicks ass!  The only player that is good enough to switch from XMMS for.\n\n"
    author: "Leo S"
  - subject: "Re: Input Lag"
    date: 2004-11-19
    body: "I have this lag problem also. Plus hearing a huge click or tick from time to time when skipping tracks."
    author: "ac"
  - subject: "Re: Input Lag"
    date: 2004-11-19
    body: "Sounds like crossfade is turned on for gstreamer and xine. Go to Settings -> Configure Amarok -> Playback and choose Normal. Apply, then use as per the supplied directions :-)"
    author: "illogic-al"
  - subject: "Re: Input Lag"
    date: 2004-11-20
    body: "Nope, that's not the problem. I already tried that.  Delay is set to 0ms.  \n\nI think it's just a problem with the buffer.  I'm guessing that the engine buffers some data before playing it (even for local files it seems).\n\nNot sure.. but with the xine engine that delay is small enough to be tolerable."
    author: "Leo S"
  - subject: "I personally prefer kaffeine"
    date: 2004-11-18
    body: "if you want a player that do both video and audio then use kaffeine, I would probably be using amarok if it offered video but obviously they don't want to add that and I don't want to use two different apps to play my media files. Plus Kaffeine offers a cool playlist and nice option such as streaming to your friends check it out at http://kaffeine.sf.net"
    author: "Pat"
  - subject: "Re: I personally prefer kaffeine"
    date: 2004-11-19
    body: "I love Kaffeine for playing videos/DVDs, but I think that Amarok is much better for playing audio (because of the library).  Also I couldn't figure out how to do streaming (I just kept getting errors about not having the codec, even though I can play the video I tried on both computers)."
    author: "Corbin"
  - subject: "Re: I personally prefer kaffeine"
    date: 2004-11-19
    body: "I agree. amaroK it's the best thing if you have and want to listen to a huge music collection.\nAnd anyway, I was used to love Kaffeine in the 0.4.x series, I don't really like the new 0.5 layout. As now, it's completely a mess IMO"
    author: "Davide Ferrari"
  - subject: "Re: I personally prefer kaffeine"
    date: 2004-11-19
    body: "Heh. That's interesting, because I had the exact opposite reaction -- 0.5 has an amazing interface, 0.4 is about average.\namaroK and Kaffeine rock :)."
    author: "Illissius"
  - subject: "Re: I personally prefer kaffeine"
    date: 2004-11-19
    body: "What is the problem with new gui?\n\nhttp://l3lx202.univ-lille3.fr/~bellegarde/kaffeine.png"
    author: "gnumdk"
  - subject: "amaroK volume normalization"
    date: 2004-11-19
    body: "I've been using XMMS (and Winamp) for years, mostly because I like the small interface.  I've been testing out the latest SUSE release, though, which uses amaroK as the default audio player.  I've been having some issues with XMMS lately, so I figured this would be a great time to try using something else.\n\nSo far I've been pretty happy with amaroK.  However, there's one feature that makes it almost unusable for me - no volume normalization.  Both XMMS and Winamp offer plugins for this, however I haven't been able to find any way to do this with amaroK.\n\nDoes anyone know of a way to do this?  I usually listen to a very random playlist, so as you can imagine, the volume changes when switching from a soft instrumental track to hard rock can be pretty extreme.  Thanks!"
    author: "Jared"
  - subject: "Re: amaroK volume normalization"
    date: 2004-11-19
    body: "I think probably gstreamer and arts support things like this, but in any case its not build-in in amarok. I tried equalizing with arts, but it didnt work for me - so I'm very happy with the mentioned 10-band equalizer in amarok for the gstreamer-engine."
    author: "superstoned"
  - subject: "Re: amaroK volume normalization"
    date: 2008-11-25
    body: "You might want to check this out:\n\nhttp://kde-apps.org/content/show.php/ReplayGain?content=26073\n\n"
    author: "GR"
  - subject: "Alsasink is better now"
    date: 2004-11-20
    body: "Ronald and Benjamin of the GStreamer team have done some fixes to the alsasink so it should hopefully be a little better than described here now :) Fixed my problem  when used with Amaraok at least.\n\nLets just hope we can get freedesktop CVS up and running again so we can do a new gstreamer plugins release with the fixes in :)"
    author: "Christian Schaller"
  - subject: "amaroK"
    date: 2004-11-20
    body: "Personally think amaroK's excellent for what it is - simple, easy to use - drag & drop your mp3's into the window - good, easy to use application - thanks to devs ;)"
    author: "anon"
  - subject: "Default keyboard shortcuts for audio"
    date: 2004-11-21
    body: "I recently got the Bluetooth HID feature of my K700i to work. This means I can control the computer with my phone. But there are no default, global keybindings for controlling audio in KDE as far as I can tell. I think all audio players (video players as well, probably) should share global keybindings. "
    author: "Claes"
---
Gone are the days when KDE users would lament for an XMMS replacement.
With the likes of <a href="http://amarok.kde.org/">amaroK</a> (down) and <A
href="http://developer.kde.org/~wheeler/juk.html">JuK</a> on the
scene, it would seem that <a href="http://multimedia.kde.org/">KDE Multimedia</a> is gaining new fans.  Which
brings us to <a
href="http://illogical.homelinux.org/prop-amarok.html">this user's
opinion piece on amaroK</a> (<a href="http://illogical.homelinux.org.nyud.net:8090/prop-amarok.html">coral</a>).  It may be illogical... but at least it
has screenshots.  It'll be interesting to see what KDE Multimedia 4.0
looks like.



<!--break-->
