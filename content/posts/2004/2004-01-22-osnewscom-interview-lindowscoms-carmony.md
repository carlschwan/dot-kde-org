---
title: "OSNews.com: Interview with Lindows.com's Carmony"
date:    2004-01-22
authors:
  - "Editor"
slug:    osnewscom-interview-lindowscoms-carmony
comments:
  - subject: "Well,"
    date: 2004-01-22
    body: "It's interesting.\n\nFor an \"Open Source\" company, they sure do have a very \"closed\" development model. Does anyone know what these top secret KDE applications are?\n\nThe other thing that bothers me. Is he using \"Open Source\" as a synonym for free software? I believe he is, but wanted to clarify. It needs clarifying, as the GPL, which I assume he'll be releasing the source code under, is not an Open Source license, but in-fact a Free Software license. (I wait for the argument that the GPL is OSI approved).\n\nI haven't ever actually used Lindows, but perhaps in the future I'll work on some major CVS commits and claim my free copy!\n\nThe only thing that bothers me is the name; whenever I have mentioned it do any of my peers they always reply with: \"Is that a dodgy copy of Windows?\".\n\n\n\n"
    author: "Martin Galpin"
  - subject: "Re: Well,"
    date: 2004-01-22
    body: "> For an \"Open Source\" company, they sure do have a very \"closed\" development model. Does anyone know what these top secret KDE applications are?\n\nThat's pretty much like any other company that has worked with open software like KDE. Most companies don't like announcing prereleases of software, because frankly, half of these softwares get cut before they ever get to market. This is a mistake Lindows did with Windows Compatability when it first started, and I don't think they care to repeat it again. \n\n>  The only thing that bothers me is the name; whenever I have mentioned it do any of my peers they always reply with: \"Is that a dodgy copy of Windows?\".\n \nI haven't tried Lindows yet (I've been using Debian for uh, 5 years now), but I've heard recent versions of Lindows are pretty good."
    author: "anon"
  - subject: "Re: Well,"
    date: 2004-01-22
    body: "You forgot Ximian. Remember how much work they did behind closed doors on XD2?"
    author: "ac"
  - subject: "Saw Lindows in a different light"
    date: 2004-01-22
    body: "I've been skeptical of Lindows since it first started, mostly because of their marketspeak and unrealistic goals (100% windows compatability) once they first started. But this interview was great, it let me see that they actually are here to benefit community projects like KDE. I can understand more of their thinking now. Nice interview."
    author: "anon"
  - subject: "Nvu ?"
    date: 2004-01-22
    body: "\"Kevin Carmony: We have also spent tens of thousands of dollars as the sole sponsor and financier for Nvu, an ftp-integrated, ultra easy-to-use web authoring system, ala Dreamweaver and Frontpage.\"\n\ninteresting this is, though nothing to download yet, but a nice screenshot:\nhttp://nvu.com"
    author: "Thomas"
  - subject: "Cupon Code"
    date: 2004-01-22
    body: "I thought it was very nice of him to offer that discount, but I found the download process still required me to enter my Credit Card No. I would have liked to try Lindows out to experiment with it, but alas I don't feel like giving them that.\n\nWhile I wish them the best, it's still tough to beat free _and_ unencumbered."
    author: "Usual Lurker"
  - subject: "Re: Cupon Code"
    date: 2004-01-22
    body: "You don't need to enter *any* payment details, well at least Ididn't! I had to create an account, and went to http://shop.lindows.com/user/mylindows_download_library.php . From here select \"My Producs\" / \"CD Downloads\""
    author: "Craig"
  - subject: "Re: Cupon Code"
    date: 2004-03-08
    body: "Lindows !"
    author: "ion"
  - subject: "Re: Coupon Code"
    date: 2004-01-22
    body: "Perhaps you missed the link that said \"if you have a coupon, click here.\"  I did the first time around.  I was a little more brave and used my credit card - got right up to the final checkout page.  Having not seen the coupon link the first time I back tracked and until I saw it.  I am now #41 in line to begin my download."
    author: "PaulSeamons"
  - subject: "GAIM?"
    date: 2004-01-22
    body: "They should be investing in Kopete."
    author: "David"
  - subject: "Re: GAIM?"
    date: 2004-01-22
    body: "Kopete is just recently starting to be a viable alternative to gaim. Still it's a bit buggy though (KDE 3.2 RC1). Let's just hope it becomes stable enough for final release and Lindows might make the switch. (though I doubt it.. they alreay have all users used to gaim)"
    author: "ealm"
  - subject: "Re: GAIM?"
    date: 2004-01-22
    body: "I've recently stopped using GAIM and using Kopete. It is good, very good.\n\nI would like to see the contact list made a little more pretty, yet aside from that, it's getting there.\n\nI don't use GAIM anymore, and therefore, to me, it's succeeded."
    author: "Martin Galpin"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "Yup. Kopete (0.8rc1) rocks. For the first time I can chat with my friends on yahoo, icq, msn, in one application. I've tried Gaim, but its gtk interface makes me sick.\n\n"
    author: "nbensa"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "You know you can use different GTK+ themes, right?"
    author: "Miles Robinson"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "Yer, but native Qt apps rock."
    author: "David"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "*shrug*\n\nI do appreciate it when an app uses the toolkit I prefer, but really, calling GTK+ apps ugly doesn't make a whole lot of sense when you can customize the widgets and stuff just as much as you can do in QT."
    author: "Miles Robinson"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "Maybe someone can help me with a Kopete problem.  How do you make it so when you hit enter, it sends the message and not drops down a line.  In yahoo and msn enter sends the message and shift+enter drop down a line.\n\nIt is HIGHLY annoying to have enter just drop down a line when you are involved in a \"normal\" dialog, especially when you're doing multiple tasks and have to move the mouse to click \"Send\".\n\nSorry for the rant, but I just got home and I'm tired and still a bit drunk so sorry.\n"
    author: "SuperPET Troll"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "Hmm, the problem is odd, my kopete uses Shift-enter to send messages..\nDunno if this is configurable, I could not find anything about this in kopete's settings window..\n\nRinse"
    author: "rinse"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "Yes, it is configurable. Settings, Configure Shortcuts. However, I think the default in KDE 3.2 is, Enter sends the message, as you'd expect, so no need to worry about that."
    author: "Balinares"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "Settings --> Configure Shortcuts"
    author: "138"
  - subject: "Re: GAIM?"
    date: 2004-01-23
    body: "I think there's been a lot of dicussion about this. As far as I know it is in the pipeline."
    author: "David"
  - subject: "Re: GAIM?"
    date: 2004-01-24
    body: "> How do you make it so when you hit enter\n\nConfigure Shortcuts.. yeah, it took me a while to find this too.. the default is really annoying to almost all IM users, if any kopete developers are listening. "
    author: "anon"
  - subject: "Nice guy"
    date: 2004-01-22
    body: "Well, I thought Kevin sounded like a nice guy, and Lindows has apparently done a lot for KDE, especially lately. Such stuff is always nice. :)\n"
    author: "Chakie"
  - subject: "re:\"character\""
    date: 2004-01-23
    body: "Haven't used Lindows yet - however - its pretty clear that Lindows support KDE to a degree and share a certain honest enthusiasm for it - fair play to them and thanks for the continued support....."
    author: "sap"
  - subject: "WOW, I can't wait fro version 5"
    date: 2004-01-23
    body: "KDE 3.2, Kernel 2.6, Reiser 4, Xfree 4.4 (I think)! I'm sold right there! "
    author: "Alex"
  - subject: "Great support!"
    date: 2004-01-23
    body: "Can't wait till 5.0, though the 4.5 developer's edition is great; you don't have to CNR a gzillion development packages. If they update CNR more often (they still have KDE 3.0.5 there!!) they will have the _easiest_ and most convenient software installation on any OS I used!\n\n"
    author: "Jasem Mutlaq"
  - subject: "One Thing I'll Say for Them"
    date: 2004-01-23
    body: "At least they've admitted they're not making money - just like everyone else hasn't. Personally I think KDE companies are in good shape. At least they are not leaking tens of millions like Ximian."
    author: "David"
  - subject: "Lindows.com is proud to be associated with KDE!"
    date: 2004-01-24
    body: "I hope you don't mind if I make a post and give some insight into some of the comments here....\n\n\n\"Most companies don't like announcing prereleases of software...\"\n\nYes, this is our reasoning, and yes, we learned our lesson on the whole MS Windows compatibility thing.  =)\n\n-----------------------------\n\n\"I can understand more of their thinking now. Nice interview.\"\n\nAs I mentioned in the Interview, I wish we would have invested more time in sharing information outside of just our user base, but we really were super focused on building the product.  Now, after two years and having a solid product out there, we're ready to venture out and invest more time in sharing our story with everyone, not just LindowsOS users.\n\n-----------------------------\n\n\"interesting this is, though nothing to download yet, but a nice screenshot:  http://nvu.com\"\n\nI've been using an Alpha version of Nvu and it's great!  It's come a long way, even since those early screenshots on the web site.  Look for a beta download in the next few weeks.  You can join a mailing list at the http://nvu.com web site if you'd like to be notified when it's available.  It's not KDE, as it's based on Mozilla's Composer, but I think you'll all still like it a great deal.\n\n-----------------------------\n\n\"They should be investing in Kopete.\"\n\nWe have our eye on it.  We're certainly open to switching if Kopete becomes a better fit for our users.\n\n-----------------------------\n\n\"I've tried Gaim, but its gtk interface makes me sick.\"\n\nWe use a matching theme so that most users can't tell the difference.  Right now LindowsOS has FOUR different architectures in main products (KDE, GTK, Mozilla, and Open Office).  We have done a very good job of making it seem, to the user, that they are the same.  Now, you and I can certainly tell the difference, but we've tried to minimize these as much as possible.  We'd just like to see these lines even further blurred for the user's sake.  We try not to get into \"turf wars,\" as we actually think choice is a good thing, but for the user's sake, we'd like things to at least feel better matched.\n\n-----------------------------\n\n\"Well, I thought Kevin sounded like a nice guy...\"\n\nI hope I am.  =)\n\n-----------------------------\n\n\"its pretty clear that Lindows supports KDE...\"\n\nFor the desktop environment, there's no doubt it's our pick and we're happy to continue supporting it.  We're also anxious to see more and improved KDE apps, which is where we're trying to really help out.  I'm very excited for us to launch our bounty program where KDE developers can get paid to work on KDE apps!\n\n-----------------------------\n\n\"If they update CNR more often (they still have KDE 3.0.5 there!!) they will have the _easiest_ and most convenient software installation on any OS I used!\"\n\nWe're doing something really cool with 5.0 (and something that's vital for our future growth), and that's the concept of multiple warehouses.  Right now, our entire CNR Warehouse must be in sync with the latest version of LindowsOS.  With 5.0, that all changes, because we've overhauled our CNR Warehouse.  This means we can very easily support unlimited number of warehouses.  For example, people can be on LindowsOS 4.5, 5.0, 6.0, X.0, etc. and the CNR experience will match the version they're on.  This new warehouse architecture will allow us to move forward at a much more aggressive pace, because we can let our Insiders (who beta test our products) have access to builds much earlier on WITHOUT breaking CNR for everyone else.  We can even have a special version of the warehouse for each beta release, and so on.  This is all very cool, powerful stuff that will really help us move a lot more quickly and stay current with KDE developments.\n\n-----------------------------\n\n\"At least they've admitted they're not making money...\"\n\nFortunately, however, we are close to break even.  We should hit it this year.  I only mentioned that we were losing money in the Interview, because I want people to understand it's not like we're rolling in money over here!  We're struggling right along side all of you other early pioneers who are trying to help Linux and KDE break out into a larger market.  No one is in six figures over here, even me or Michael, our CEO!  I make a tiny fraction of the salary I'm use to making, and have been at this for 2.5 years.  But, even with our sub-par wages, you'll never find a happier group of 65 people, all thrilled to be here working on Linux!  Like each of you, we're believers, or we wouldn't be here!  (You're all personally invited to drop in anytime and say hi, if you're ever in San Diego, so you can see for yourself first hand what we're up to here.)\n\n-----------------------------\n\nThanks to all the awesome KDE developers and supporters.  You've all done a lot of amazing work.  We're proud and happy to be in the trenches with you.\n\nI hope to see you all at the Linux Desktop Summit 2004 in April (http://desktoplinuxsummit.org).\n\nKevin Carmony\nPresident, Lindows.com, Inc."
    author: "Kevin Carmony"
  - subject: "Re: Lindows.com is proud to be associated with KDE!"
    date: 2004-01-25
    body: "FYI Kopete can now be integrated into KDE 3.2's Universal Sidebar:\nhttp://www.kde-look.org/content/show.php?content=10326\nI think the Universal Sidebar in general has a lot of potential. Maybe you can make use of it. ;)"
    author: "Datschge"
  - subject: "Re: Lindows.com is proud to be associated with KDE"
    date: 2004-01-25
    body: "Yes, these are the sort of reasons that we're excited to get onto KDE 3.2 in our next release!  \n\nKevin\n"
    author: "Kevin Carmony"
  - subject: "Re: Lindows.com is proud to be associated with KDE"
    date: 2004-01-25
    body: "I think something worth developing would be a Siphone plugin for Kopete. Witha account wizard."
    author: "Kraig"
---
<a href="http://www.osnews.com/">OSNews</a> features <A HREF="http://www.osnews.com/story.php?news_id=5758">an interesting interview</a> with President & COO of <A HREF="http://lindows.com/">Lindows.com</A>, Kevin Carmony. They discuss about LindowsOS 5, ReiserFS 4, their KDE involvement, their OSS sponsorships, the Microsoft competition, community misconceptions about LindowsOS and much more. This interview is a lot of KDE interest as Kevin goes through the increasing Lindows.com involvement in the KDE project and what's coming in the future too. Also, for the first time for a Lindows.com product, LindowsOS 4.5 Developer's Edition will be given for free for the next two days for everyone to try it out. Just use the text coupon mentioned in the interview article to get access to the distro (normally priced at $60 US).




<!--break-->
