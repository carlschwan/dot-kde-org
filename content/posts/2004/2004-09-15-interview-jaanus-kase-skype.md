---
title: "Interview with Jaanus Kase from Skype"
date:    2004-09-15
authors:
  - "tmous"
slug:    interview-jaanus-kase-skype
comments:
  - subject: "MSN most popular?"
    date: 2004-09-15
    body: "Wasn't it AIM (or ICQ) ?\n\n./Maciek"
    author: "MacBerry"
  - subject: "Re: MSN most popular?"
    date: 2004-09-15
    body: "It was... 5 years ago.\nMSN is clearly the most used IM service, at least here in Europe. In Holland, MSN is even bigger: according to this article [1], 85% of the internet chatters are using MSN. That's 22 million chats a day, on a population of 16 million people.\n\n\n1. http://www.vnunet.nl/nieuws.htm?id=172709"
    author: "Niek"
  - subject: "Re: MSN most popular?"
    date: 2004-09-15
    body: "> MSN is clearly the most used IM service, at least here in Europe\n\nHmm, not where I live.  I cannot talk for all of Germany\nand I don't have any stats, but not a single one of my friends \nhas an MSN account that I know of.  We've more or less \"standardised\" \non ICQ.  And this has nothing to do with being Anti-MS or something, \nthere are lots of Windows-only users among them.\n\n"
    author: "cm"
  - subject: "Re: MSN most popular?"
    date: 2004-09-16
    body: "Yep! As far as I know MSN isn't the most popular IM service here in Europe. I'm from the Union too. I live in Italia and in my State, like in Germany, most of people do use ICQ. It's not a matter of being Anti-Microsoft, but simply in the EU MSN isn't the most popular service. Bigger EU States (Germany, Italy, UK, Spain, France,...) still use ICQ as their main IM sistem."
    author: "Cesare Augusto de Marco"
  - subject: "AIM in the US/Canada, MSN in the EU"
    date: 2004-09-17
    body: "I'm European too, have friends in the states and Canada and most of my American friends use AIM while most of my European friends use YIM. But,I guess that the most popular IM service in the US/Canada is AIM and in the EU is MSN. Having said that, most geeks I know in the EU/US/Canada don't use MSN."
    author: "anonymous"
  - subject: "Re: AIM in the US/Canada, MSN in the EU"
    date: 2005-09-25
    body: "i am basel in jordan 18 year \ni am very loving for canada and  job because i am active\n(i am want visit canada pleas help me? )\ni am love canada because the people in canada good and the job good\nv17_man@hotmail.com                   \n"
    author: "basel"
  - subject: "Re: MSN most popular?"
    date: 2004-09-16
    body: "Well, I'm Dutch, but living just across the border, in Germany. I can tell you that most of my Dutch friends are using MSN and most of the German people I know have ICQ. "
    author: "XiPHiaS"
  - subject: "Re: MSN most popular?"
    date: 2005-05-25
    body: "msn bommm\n"
    author: "\u00e7akam sana "
  - subject: "Re: MSN most popular?"
    date: 2004-09-15
    body: "I don't know in other countries, but in Spain, MSN is unfortunately, the most popular messaging system. I don't know *anyone* who uses an IM system different than MSN (of course, ignoring geek friends, who use IRC or Jabber)."
    author: "suy"
  - subject: "Re: MSN most popular?"
    date: 2004-09-17
    body: "In Poland, where I live, we have an interesting situation. Poland is completely divided by its own IM systems: Gadu-Gadu and Oxygen (Tlen). American systems like MSN, AIM, ICQ and such are nonexistent here. Although I know 1 (one) person with MSN account, I dont know anyone using ICQ or AIM.."
    author: "QuantumSoul"
  - subject: "Re: MSN most popular?"
    date: 2007-06-28
    body: "Yes , but canada most popular is http://pharmacy.com.md\nThank you"
    author: "Mikel"
  - subject: "Qt does it, not Skype"
    date: 2004-09-15
    body: "> However, there are some interesting utilities \n> we have done, for example for reusing the same \n> language files and translations in Windows and \n> Linux versions, that we will make available to \n> our users.\n\nIsn't that a Qt feature? I use Linguist to create multi-platform translations."
    author: "Johan Thelin"
  - subject: "Re: Qt does it, not Skype"
    date: 2004-09-15
    body: "> Isn't that a Qt feature? I use Linguist\n> to create multi-platform translations.\n\nYes and no. We are reusing the language files from Skype for Windows that is currently implemented in Delphi that uses a different translations format. So we had to bring those over to Qt. But the Qt interface and file formats are very clean so it was pretty easy to do that."
    author: "Jaanus Kase"
  - subject: "Re: Qt does it, not Skype"
    date: 2004-09-15
    body: "That sounds a bit strange to me.\nWhat's the point of using Qt for the Linux version and Delphi for the Windows version.\nQt is a cross-platform toolkit.\nWhy not use it on both platforms?"
    author: "Mr. X"
  - subject: "Re: Qt does it, not Skype"
    date: 2004-09-15
    body: "Maybe you should read the article?"
    author: "Mario"
  - subject: "The article doesn't make it clear"
    date: 2004-09-16
    body: "The artivle doesn't make it clear really.\n\nIt says they want to use the \"native toolkit\". But how is using Delphi using the native toolkit? As far as I know, if you use Delphi you're normally using Borland's toolkit. And how is Qt a \"native toolkit\" for Linux? QT isn't geared any more towards Linux than Windows  or Mac or Qt/Embedded.\n\nAnd the argument still doesn't hold water. When you compile a QT app on Win32, it looks *exactly* like Win32. It basically is native. Same with OSX - it picks up the Coca look.\n\nSo I really don't get it - is it just me or does it sound like they are wasting a whole lot of developer resources here?\n\n\n"
    author: "Jason keirstead"
  - subject: "Re: The article doesn't make it clear"
    date: 2004-09-17
    body: "They simply started with Delphi on Windows and that's what those developers are most familiar with.\n\nJo"
    author: "Jo"
  - subject: "Re: The article doesn't make it clear"
    date: 2005-02-09
    body: "Hi Jason,\n\nDelphi doesn't use Borland's toolkit, but a class library (VCL) that directly interfaces to the Win32 API, and, thus, uses the Windows \"native toolkit\" (USER32.DLL, etc.).\n\nCheers,\n\nDaniel"
    author: "Daniel Polistchuck"
  - subject: "are you able to ascertain what percentage?"
    date: 2004-09-15
    body: "I think other companies would benefit from knowing how many users there might be for a linux version of their application.\n\nWell done Skype regardless though. One day I must even check again to see whether Skype's merchant bank security checks allow me to use my credit card from abroad (kinda key for phoning home I always thought....)"
    author: "c"
  - subject: "Open source API?"
    date: 2004-09-15
    body: "\"Open source API\".. That just cracks me up.\n\ncome on.. how can an api be open source? It's just an interface.\n\n\n\n"
    author: "Frej"
  - subject: "Re: Open source API?"
    date: 2004-09-15
    body: "Well, keep in mind that he's not a programmer.  Programmer's tend to be nit-picky about wording, especially when it involves programming topics.  In this case, I'm sure he meant a \"well-documented API\".  IMHO, that's a reasonable effort to allow 3rd-party tools."
    author: "JasonH"
  - subject: "Great app and some suggestions..."
    date: 2004-09-15
    body: "I used to use a Windows product called Peoplecall. It was the only application that I could not find an equivalent Linux product.\n\nSkype is easier to use than Peoplecall and works beautifully under Linux. I have been calling friends and family all over the world with Skypeout and the sound quality is awesome. Better yet, unlike Peoplecall I do not have to remove my laptop from the router and connect directly to the net, which both was a huge inconvenience and less secure.\n\nSkype is a real killer app for Linux and I, for one, truly appreciate the fact that Skype isn't doing what every other proprietary vendor usually does, which is to be oblivious to the Linux market or to release a second-class app.\n\nI just hope Skype continues to provide timely releases that install great on my Mandrake systems. \n\n\nFinally, a few things that need improvements.\n\nI move between my laptop and desktop. It would be great if there was a way to export my Skype contacts from one to the other so that they are maintained in sync. I know I can copy the whole .Skype folder but that's a bit of a hack and not very elegant for many desktop users, who may never see the hidden folder.\n\nA great feature would be if Kontact would include a field in the address book that would allow you to call your Skype friends by clicking on their contact info. That way, you don't need to keep the contact info in two different places.\n\nEqually nice would be to be able to connect to the skype network within Kopete by using a published API, so that I could do all my IM from one place.\n\nOverall, very impressed and very much looking forward to the 1.0 Linux release.\n\nThanks and keep up the great work.\n"
    author: "Me or somebody like me"
  - subject: "Re: Great app and some suggestions..."
    date: 2004-09-15
    body: "I'd like that Skype button in kontact's addressbook, but I'd also like one for vonage, which should be easy to do.  I'm thinking about writing a python applet which uses dcop (if possible) to do the same style name completion as kmail and then uses the vonage web server to dial the associated phone number."
    author: "TomL"
  - subject: "Spam"
    date: 2004-09-15
    body: "Why am I reading about a proprietary application that does not currently use KDE technology and \"will not be closely integrated with KDE libraries\" in the future either?\n\nPerhaps we've \"got the Dot\" but I'd rather get some KDE news.\n"
    author: "Rob Kaper"
  - subject: "Re: Spam"
    date: 2004-09-15
    body: "I think because some questions are asked regarding KDE and FLOSS, this is relevant in a way."
    author: "wilbert"
  - subject: "Re: Spam"
    date: 2004-09-15
    body: "Yes, at least a question whether they'll offer/support Kopete integration anytime soon should have been asked."
    author: "ac"
  - subject: "Re: Spam"
    date: 2004-09-15
    body: "1. s/Kopete/KDE IM proxy/\n\n2. I don't think the questions were wrong. I actually like the interview, but it's hardly any KDE related news. The best I can make of it in KDE context is an unfortunate PR failure regarding the benefits of the KDE libraries and desktop environment.\n\nI'm hoping Michael Robertson picks this up. Linspire has strongly chosen for a single desktop distribution and I bet they would be keen on working together with Skype to offer a version which is not just good, but great. This would have been something the KDE League could have jumped at when it was around. I'm not sure whether it's eV material but I'll send a copy of this post to the membership.\n\nThe KDE community should not pick a prefered partner though. I appreciate Linspire's vision of the Linux desktop (basically: \"pick KDE and make it better\"), but as a community we should be neutral. If I find time perhaps I could setup a Qt2KDE project which can intermediate between the open development community of KDE and its vendors with regards to KDE support in proprietary applications and with a focus on applications already in Qt. \n\n@Fabrice, Tom: interested in this? I think there are huge PR chances when an interview like this can end with \"yes, I would be interested in using KDE in cooperation with a commercial vendor of it\" instead of \"no thanks\". Project might be a big word, but KDE would greatly benefit if the result of interviews like these is that there _will_ be more KDE software.\n"
    author: "Rob Kaper"
  - subject: "Re: Spam"
    date: 2004-09-15
    body: "\"I'm hoping Michael Robertson picks this up. Linspire has strongly chosen for a single desktop distribution and I bet they would be keen on working together with Skype to offer a version which is not just good, but great. This would have been something the KDE League could have jumped at when it was around. I'm not sure whether it's eV material but I'll send a copy of this post to the membership.\"\n\nMichael Robertson, aside from being the CEO of Linspire, Inc., is also the CEO of SIPphone (www.sipphone.com) which is a competitor to Skype. I don't think SkypeOut users can even call SIPphone users at the present time.\n\nAnand"
    author: "Anand Rangarajan"
  - subject: "Re: Spam"
    date: 2004-09-16
    body: "Both Linspire Inc's own SIP solution PhoneGaim (which is better imo), and Skype are available in CNR."
    author: "ealm"
  - subject: "Re: Spam"
    date: 2004-09-17
    body: "Good idea but a little late Linspire already has Skype in CNR. <a href=\"\n\nhttp://www.linspire.com/lindows_products_details.php?id=12698\">Check it out in CNR</a>\n\nHmm don't understand how to make a html link on this site :("
    author: "CraigB"
  - subject: "Re: Spam"
    date: 2004-09-16
    body: "Re 1).  If any Skype people are reading this and would like to see the 'Skype button' in Kontact I can give you some pointers on how to use the KDE IM proxy libraries to expose your API to KMail, KAddressbook, Konqueror, and any other KDE apps that pick it up (Atlantik, Rob!).  Kopete and Konversation are already there, join the party.\n\nAlternatively you could drive Skype directly from Kopete as a Kopete plugin, using Kopete's existing KIMProxy implementation, but that would be considerably more work."
    author: "Will Stephenson"
  - subject: "Re: Spam"
    date: 2004-09-15
    body: "because it uses Qt, which means KDE and Skype have a common foundation. it is news today when a significant proprietary desktop application is released on Linux, especially one using toolkits which are best described as \"KDE friendly\". this is the kind of news that will help other ISV to gather the courage and investment to follow suit so that one day it isn't news anymore.\n\nand remember how some rather vocal people like to say that KDE isn't viable because Qt requires per-developer licensing for closed source apps which makes it non-viable for such things? well, this kind of news helps dent that strawman pretty effectively.\n\ndoes that explain the relevance clearly enough for you, Rob?"
    author: "Aaron J. Seigo"
  - subject: "Re: Spam"
    date: 2004-09-16
    body: "If this interview confirms anything then that Qt is a failure as a cross platform toolkit. Skype have chosen other toolkits on other platforms. Its nice though that they saw Qt as the \"native\" toolkit for linux."
    author: "M"
  - subject: "Re: Spam"
    date: 2004-09-16
    body: "No. I maintain that proprietary use of Qt is not news, much less KDE news. Opera has been doing it for years and Skype's choice of Qt is not even dedicated, they will not be using it on other platforms.\n\nIt's a bit ridiculous to throw in an argument on per-developer Qt licensing for closed source applications. If we're going to showcase Trolltech customers in KDE PR, we should at least pick the ones that do choose the KDE environment on top of Qt.\n"
    author: "Rob Kaper"
  - subject: "Re: Spam"
    date: 2004-09-16
    body: "Perhaps you should run your own only KDE news site - oh, I remember you already did and failed."
    author: "Anonymous"
  - subject: "Re: Spam"
    date: 2004-09-16
    body: "That sounded a bit sarcastic ...\n\n@Rob: I think you are looking at this a bit too rigid. Anyway I'm always willing to listen to ideas so feel free to email me. Keep in mind that I don't agree on several things you say about how to promote KDE on websites like KDE-apps.org and dot.kde.org. Doesn't stop us from discussing about this. \n\nCiao\n\nFab "
    author: "Fabrice Mous"
  - subject: "Kopete plugin?"
    date: 2004-09-15
    body: "It would be great to have Kopete plugin which provides Skype functionality. Currently, I use 4 different instant messengers simultaneously, and running 4 different applications is not convenient. I understand, that Skype uses proprietary codecs and it's not realistic to expected to see it's source code or protocol specifications in nearby future. But what about providing a binary library with documented API wich implements Skype communication functionality without GUI? This library could be used to build plugins for existing multiprotocol IM clients, such as Kopete, Gaim and others."
    author: "Oleg Girko"
  - subject: "Re: Kopete plugin?"
    date: 2004-09-15
    body: "Perhaps you didn't read the part where he said: \"The API is going to be available to other apps over a remote call protocol when the Skype client is running.\"\n\nRoughly tanslated: \"If you want a Kopete plugin, then write one.\""
    author: "JasonH"
  - subject: "Re: Kopete plugin?"
    date: 2004-09-15
    body: "I prefer to use a skype-library with kopete, with this solution I still need to run two IM-programs (skype and kopete), I prefer to make calls to friends (phone and voip) and only using one IM (kopete)."
    author: "AC"
  - subject: "Re: Kopete plugin?"
    date: 2004-09-17
    body: "I would guess that Skype would like you to run Skype, though. Not \"skype-library\". And they can. That's what \"proprietary\" is all about..."
    author: "Martin"
  - subject: "Re: Kopete plugin?"
    date: 2004-09-20
    body: "I think he's basically saying someone should implement the Skype protocal in an OSS fashion and let Kopete use it."
    author: "Ian Monroe"
  - subject: "Skype plugin for Kopete"
    date: 2004-11-08
    body: "Surely MSN,Yahoo etc are also proprietry and yet there are plugins for those?  I think having a skype plugin/library for Kopete is a great idea and is inevitable."
    author: "Justin Lawrence"
  - subject: "PocketPC"
    date: 2004-09-15
    body: "Does Qt run on PocketPC? (He mentions that the software runs on PocketPC as well). If so, is it the same binary as for Windows?"
    author: "Apollo Creed"
  - subject: "Re: PocketPC"
    date: 2004-09-15
    body: "Windows CE aka PocketPC is completely different from the usual NT line of Windows systems so there's no chance they use the same binary for both."
    author: "ac"
  - subject: "Re: PocketPC"
    date: 2004-09-16
    body: "Skype for Pocket PC is a separate binary app."
    author: "Jaanus Kase"
  - subject: "Some comments about skype"
    date: 2004-09-16
    body: "I've been using skype for quite a few months now, and its great (even if it is closed source :-( ).\n\nI do have a couple of niggles & questions:\n\n1) SkypeOut drops calls and generally is not stable enough to reliable use instead of a converntional telephone. Particularly on the long distance calls I make back to South Africa.\n2) I wish I could set my own password. It sounds dumb, but I have it installed on a bunch of different pc's and itd be much easier to be able to use a password of my choice.\n3) Is there a usb handset that works under linux? I'd like to get one but dont want to waste money on somethig that doesnt work.\n\nMany thanks for a cool app!\n\nTim"
    author: "TimLinux"
  - subject: "Re: Some comments about skype"
    date: 2004-09-17
    body: "Tim,\n\n1. SkypeOut situation is improving constantly, you can keep track of it in our forums\n2. You can change your Skype password in File - Options - Privacy and also on the web\n3. USB handsets should work fine since \"USB audio\" is a pretty standard thing. No advanced functions though, such as transmitting keypad button presses, software volume control etc, unless the manufacturer has a Linux driver which usually is not the case."
    author: "Jaanus Kase"
  - subject: "skype.desktop"
    date: 2004-09-17
    body: "may the sections of Name and Comment in skype.desktop\nhave different languages?\n"
    author: "gnilbeil"
---
Some time ago we made <a href="http://dot.kde.org/1088472154/">a mention of Skype on Linux</a> in one of our short newsflashes called 'Quickies'. We were very pleased by the way it nicely blends itself into the KDE desktop. Now we contacted the <a href="http://www.skype.com/">Skype people</a> where we came in touch with Jaanus Kase who took time to answer our questions.


<!--break-->
      <style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
  
  </style>

<p><b>Please introduce yourself.</b></p>

<p>I am working in Skype project management team, and one of my tasks is to 
manage Skype for Linux - collect feedback from users, work with developers, 
testers and external parties. I don't do actual coding myself, but I do a 
lot of design and quality assurance. I'm using MEPIS, SuSE 9.1 and Java 
Desktop System Release 2, and Windows XP.</p>
              
<p>
<b>When will version 1.0 for Linux arrive?</b>
</p>
<p>
The version 1.0 release that was launched on July 27 was for the 
Windows platform, as well as the <a href="http://www.skype.com/skypeout.html">SkypeOut service</a>. The <a href="http://www.skype.com/download_linux.html">Linux version 1.0</a> 
is on its way and new versions are coming out for closed testing all the 
time and will reach the public once they're ready. There will not be a 
significant leap in features between before-1.0 and post-1.0 versions 
- it's a gradual evolution as we continuously innovate and improve 
the software.
</p>

<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:260px;">
<p><a href="http://static.kdenews.org/fab/screenies/skype/skype9.png"><img src="http://static.kdenews.org/fab/screenies/skype/skype9_small.png" alt="Skype in action" /></a></p>
</div>
<!-- IMAGE BOX RIGHT-->     


<p>
<b>The Linux client doesn't seem to have the full functionality of the 
Windows version. In particular, it lacks <a href="http://static.kdenews.org/fab/screenies/skype/headsets.jpg">the advanced settings tab</a>, which 
limits Skype to the default sound device and makes using a headset 
difficult. Are there plans to more fully develop the Linux client?</b>
</p>



<p>
Yes there are plans and in fact, the sound device selection will be available 
to the public soon. The goal for the Linux version is to bring it on par with 
the Windows version feature- and quality-wise.
</p> 
<p>
<b>The Skype website does mention the amount of downloads but how many 
people do you think are using Skype?</b>
</p>
<p>
Today we know have just over 10.5 million registered users on Skype as we also 
track this information. The concurrent online users figure, which you can see 
in the Skype client, is approaching half a million. These are very 
significant numbers and they are growing all the time.
</p>
<p>
<b>How does Skype compete with for example MSN which has the largest share 
of the instant messaging market?</b>
</p>
<p>
Skype offers free superior sound quality Internet 
telephony. In addition, it includes:
</p>
<ul>
  <li>Conference calling - enables simultaneous and seamless voice communication 
between groups of up to five friends, family or colleagues. The Linux version 
currently has only conference client but will have hosting too.</li>
  <li>Global Directory - the user-built global Skype contacts directory with 
numerous search options and an easy add-a-contact tool</li>
  <li>Customization - My Picture image display</li>
  <li>Mobility - login into Skype account on more than one PC anywhere in the 
world.</li> 
  <li>Multiple Skype accounts on one PC </li>
</ul>
<p>
In comparison with other IM/voice clients, we can ensure:
</p>
<ul>
  <li>Better usability in networks. MSN and many other VoIP providers have voice 
calls, but those cannot penetrate firewalls or NAT. Skype has solved this 
problem. The same goes for other forms of communication (file transfers, 
instant messages) that sometimes don't go through firewalls.</li>
  <li>Better performance. MSN is server-based, meaning that performance suffers in 
peak hours and users simply cannot do voice calls due to server overload. 
Skype calls are truly P2P, involving the distributed network itself for 
routing calls, so it scales up very well and does not suffer from this kind 
of performance problems.</li>
</ul>
<p>
We are working on adding more user requested features to the software, such as 
video calling, etc.
</p>
<p>
<b>Are there any plans to allow the Skype client to connect to other 
networks, e.g. MSN, ICQ, Yahoo!, Jabber! etc., like many Linux IM clients?</b>
</p>
<p>
We believe in interoperability.  However, our main goal is to respond to our 
users' requests and needs, and to focus on continuing to innovate and make 
Skype the best offering for Internet telephony.  Connecting to other networks 
is currently not a user requested feature for us to make it a priority at the 
moment. 
</p>
<p>
<b>How many people are working fulltime on Skype?</b>
</p>
<p>
We have approximately 45 people working on Skype.
</p>
<p>
<b>Why did you consider writing software for Linux?</b>
</p>
<p>
Skype for Linux was one of the most requested developments from our user base, 
but also because we see Linux as an important emerging PC desktop platform. 
This is illustrated by the fact that many major vendors are starting to offer 
Linux systems (HP, Sun, IBM to name just a few), and that many companies, 
government institutions and local governments in Europe, Asia and elsewhere 
have announced their migration to Linux. The proportion of Skype for Linux 
users is still small as compared to those running it on Windows, but we 
expect it to increase over the coming years.
</p>
<p>
Simply put, we want everyone to be able to run Skype and talk to their 
friends, family and colleagues, regardless of what platform they use or 
whether they have a computer at all. Embedded and mobile devices, some of 
them on Embedded Linux, are an important future development path for Skype.
</p>
<p>
<b>Skype is released under a proprietary license. It was mentioned in the 
forums that you may be experimenting with an open source API. Can you 
tell us a little more about that?</b>
</p>
<p>
The API is going to be available to other apps over a remote call protocol 
when the Skype client is running. Other apps can then call Skype functions 
such as "call this contact", "send IM to this contact" and Skype also 
notifies other apps about incoming calls, IM-s and other events. A simple 
usage scenario would be to have a Skype Name field in address book 
applications and a button for calling those persons. If you then click it, a 
Skype call is placed.
</p>
<p>
<b>You invite people to contribute translations for Skype via the <a href="http://forum.skype.com/bb/viewforum.php?f=18">forum</a>, 
which then become the property of Skype. Have you considered a more open 
community approach, such as that used by the KDE Project?</b>
</p>
<p>
We see the forum as an open community and we encourage it very much - people 
upload translations, other people can immediately download and modify them. 
In general, we try to maintain a friendly relation with various Linux 
communities including KDE, and some Skype for Linux developers also 
contribute to KDE apps as their hobby projects.
</p>
<p>
<b>Are any of those hobby contributions related to Skype in any way?</b>
</p>
<p>
Currently no.
</p>

<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:260px;">
<p><a href="http://static.kdenews.org/fab/screenies/skype/skype8.png"><img src="http://static.kdenews.org/fab/screenies/skype/skype8_small.png" alt="Create conference..." /></a><br />
Create conference...</p>
</div>
<!--/IMAGE BOX LEFT-->

<p>
<b>Have you considered contributing to Qt or any other Free Software 
projects as part of Skype's development?</b>
</p>
<p>
We might do that if we get results in our work that would be of value to the
community as well as OK to publish according to our license and IP
protection policy. However, currently Skype is UI-wise not a very complex
project and most of the development effort goes into the proprietary P2P and
voice engine that are proprietary code. However, there are some interesting
utilities we have done, for example for reusing the same language files and
translations in Windows and Linux versions, that we will make available to
our users.
</p>




<p>
<b>If an open source Skype client (not necessarily your one) could be 
released as part of KDE, you would have your product shipped in almost 
every Linux distribution, and localized by KDE translators. Would that not 
outweigh the financial benefits of your closed source model?</b>
</p>
<p>
Short answer: no. Getting shipped in any Linux distribution is still a small 
percentage of users, as compared to Windows users. However, having said that, 
we keep a close eye on the Linux community and its various business models. 
We are truly multiplatform, already having Windows including Pocket PC, now 
Linux and upcoming Mac, and we learn from all the users and business models 
of all the platforms and try to pick the best experiences from each. 
</p>
<p>
<b> Skype currently uses Qt for the Linux GUI. Have you considered using
Qt for all three clients, and possibly integrating Skype for Linux with the
KDE libraries?</b>
</p>
<p>
We have considered it, but currently we stay with the native toolkits for
each platform - those that our team members have most experience with.
However, the world is changing constantly and we cannot say with certainty
what UI toolkits we will be using in the next year or five years. We will
always be using the toolkit that is the most efficient for us to work with
and provides the best user experience.
</p>
<p>
Skype for Linux will not be closely integrated with KDE libraries, at least
not the core part of it. While there might be add-ons, developed by either
Skype or the users that have KDE-specific functions, we already have users
who work with Skype for Linux on many different window managers and desktop
environments, and therefore we want to stay quite desktop-independent.
</p>






