---
title: "Docs Competition Deadline Approaching"
date:    2004-09-30
authors:
  - "qteam"
slug:    docs-competition-deadline-approaching
comments:
  - subject: "Using KDE as Root"
    date: 2004-09-30
    body: "Don't."
    author: "Martin"
  - subject: "Re: Using KDE as Root"
    date: 2004-09-30
    body: "So you have a higher authority on your machine :) \n\nAnyway .. a nice initiative from the kde-docs and kde-quality\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Using KDE as Root"
    date: 2004-10-01
    body: "If you want to educate many people in the use of root, call it what it is - an administrator."
    author: "David"
  - subject: "Re: Using KDE as Root"
    date: 2004-10-01
    body: "good point \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Using KDE as Root"
    date: 2004-09-30
    body: "I think we have a winner"
    author: "superstoned"
  - subject: "I want, will I ?"
    date: 2004-09-30
    body: "is this the list of pages left to write ?\n\n"
    author: "anonymous writer"
  - subject: "Re: I want, will I ?"
    date: 2004-09-30
    body: "The list above is of all of the sections. You can still win if you send in one of the topics already covered by other entries, but of course it would make more sense to do different ones :)"
    author: "Quality Team"
---
If you haven't got your entry in for the <a href="/1095589644/">documentation competition</a> yet, it's 
time to start! The deadline for entries is this Sunday (October 3rd). Remember, just write 
one page for the <a href="http://users.ox.ac.uk/~chri1802/kde/userguide-tng/">new User Guide</a>, in plain text, and send it to 
<a href="mailto:kde-doc-english@kde.org?subject=Competition entry">kde-doc-english@kde.org</a>. We've already received four high quality entries covering kdebugdialog, launching programs, a konsole 
introduction and editing config files. Read on for a reminder of the list of possible topics. Don't miss your chance to grab some free O'Reilly gear!


<!--break-->
<p>You can write on any of the following topics:</p>
<p>
<b>== Part I: The Desktop == </b>
</p>
<p>
<pre>
Advanced Window Management
The System Tray
Launching Programs
Controlling Programs -> Standard Toolbar Layout
                     -> Keybindings
Opening and Saving Files
Configuring Notifications
Configuring Toolbars
</pre>
</p>
<p>
<b>== Part II: KDE Components ==</b>
 </p>
<p>
<pre>
Removable Disks
Playing Music -> Intro to aRts
              -> Performance tuning, sharing devices
Playing Movies
Using KDE as Root
Switching Sessions
Networking With Windows
Shared Sessions
Setting Up a Printer
Printing From Applications
PDF Files
Fonts - Installing and Configuring
Konsole Introduction (not necessarily the whole section)
Hand-Editing Configuration Files
Scripting the Desktop
</pre>
</p>
<p>
<b>== Part III: KDE and the Internet ==</b>
 </p>
<p>
<pre>
Konqueror -> Intro to the browser
Fine Tuning your Browsing Experience
Internet Shortcuts
Intro to Messaging
</pre>
</p>

