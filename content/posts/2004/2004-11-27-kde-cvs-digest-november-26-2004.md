---
title: "KDE CVS-Digest for November 26, 2004"
date:    2004-11-27
authors:
  - "dkite"
slug:    kde-cvs-digest-november-26-2004
comments:
  - subject: "Knode/Konqueror"
    date: 2004-11-27
    body: "I requested that Knode be \"supplied\" with a Konqueror tab. Anyone know whether this will be implemented?\n\nCb.."
    author: "charles"
  - subject: "Re: Knode/Konqueror"
    date: 2004-11-27
    body: "What is a \"Konqueror tab\"? What shall it do within KNode?"
    author: "Anonymous"
  - subject: "Re: Knode/Konqueror"
    date: 2004-11-28
    body: "to enable him open web pages without launching konqueror anew!"
    author: "jon"
  - subject: "Re: Knode/Konqueror"
    date: 2004-11-28
    body: "khtml integration for HTML postings?"
    author: "Anonymous"
  - subject: "Re: Knode/Konqueror"
    date: 2004-11-28
    body: "Precisely"
    author: "jon"
  - subject: "Re: Knode/Konqueror"
    date: 2004-11-28
    body: "Even if that idea was technically correct, it is still a terrible one."
    author: "Ian Monroe"
  - subject: "bugs"
    date: 2004-11-27
    body: "The thing i like the most about KDE development now , is\nthere are more BUGS closed that new Bugs are found !!!!!\n\nthat very nice !!!\n\n\nthx"
    author: "chris"
  - subject: "Re: bugs"
    date: 2004-11-27
    body: "is this is a sign of a mature infrastructure, or of a newly invigorated push, to get down to 0 bugs, in other words, to bug-fixing in real-time? :D it makes to want you to get you involved in fixing bugs in kde.. oh, if only I had the time.. :-/ well, I could probably squeeze it in.. guess I'm just lazy, like most of us. hats off to those heroic bug-fixers of late!! thanks!!!!"
    author: "mark dufour"
  - subject: "Re: bugs"
    date: 2004-11-27
    body: "I don't think that more bugs gets fixed than usual (except perhaps khtml) but more duplicate, unconfirmed or outdated bug reports are closed."
    author: "Anonymous"
  - subject: "Re: bugs"
    date: 2004-11-27
    body: "khtml is active again?"
    author: "jmk"
  - subject: "Re: bugs"
    date: 2004-11-27
    body: "Was it ever dead? See the khtml backports for KDE 3.3.2: http://tinyurl.com/5nthc"
    author: "Anonymous"
  - subject: "Re: bugs"
    date: 2004-11-27
    body: "Not dead .. just less active. But hey, this looks good."
    author: "jmk"
  - subject: "Re: bugs"
    date: 2004-11-28
    body: "But KDE_3_3_BRANCH don't let me use allofmp3.com :-/ I'm not sure, can be layer stuff (css) or JavaScript, whatever the download button never get active anymore. Hey, I still have there some $ credit ;-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: bugs"
    date: 2004-11-28
    body: "What's your point? Use http://bugs.kde.org."
    author: "Anonymous"
  - subject: "Re: bugs"
    date: 2004-11-28
    body: "Ho. ho. \"Here is something broken, you need to registrate and pay money, if you want to check if this is a bug\" I dont think that any developer take notice of such a bug report. Thats the point and sometimes the dot is useful to find some collaborators for testing searching and writing a useful bugreport :-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: bugs"
    date: 2004-11-28
    body: "No, you cut & paste from the pages to highlight the actual problems.\n\n\"See this snippet here? It renders different i Mozilla and Konqueror, and I think that the former is correct. It is also necessary to visit site xx\"\n\nIt's the least you can do to help our hero developers get started on the problem."
    author: "Jonas"
  - subject: "Re: bugs"
    date: 2004-11-28
    body: "Make sure you allow popups for the site. That did the trick for me.\n"
    author: "Anonymous"
  - subject: "Re: bugs"
    date: 2004-11-28
    body: "well, i know for a fact that there are more valid kicker bugs being closed with patches than there have been traditionally. the kde pim people have also been doing a great job with bug triage, and there are generally a lot of bug fixes being done. so, i wouldn't be so pessimistic as to say that there aren't more bug fixes than usual."
    author: "Aaron J. Seigo"
  - subject: "Thanks"
    date: 2004-11-30
    body: "Aaron,\n\nFixing bugs does not always win you headlines but it makes a huge difference in the lives of everyday users of KDE. For that I thank you and all the other KDE developers who seem to have tempered their love for new features, which I also share, with the need to create a platform that is easy to maintain and as bug-free as possible.\n\nWhile I do a lot of work to bring free software to new people through GNU/Linux community computer lab installations and through the work of mialug.org, my efforts pale in the face of what KDE developers do daily,so I wanted to take a little time out of my day just to say thank you.\n\nI am yet to take part at a KDE conference, but that's in my to-do list.\n\nTake care,\n\nGonzalo"
    author: "Gonzalo Porcel"
  - subject: "SVG"
    date: 2004-11-27
    body: "Good to see that there is work on SVG.\nWhat is the current status of ksvg2, kdom, kpainter?"
    author: "Anonymous"
  - subject: "Online X-Face converter"
    date: 2004-11-27
    body: "See http://www.dairiki.org/xface/"
    author: "wilbert"
  - subject: "Re: Online X-Face converter"
    date: 2004-11-28
    body: "This technology looks really cool. One could have a \"face\" like in the webforums.\nJust wondering, how many email clients support it? Does Outlook?"
    author: "blacksheep"
  - subject: "Re: Online X-Face converter"
    date: 2004-11-29
    body: "Here's an X-face picture of Konqi:\n\nX-Face: -h`\"2)}C+`RDTm?8\\\"w)[b_+XZi;hK$?Y.W+@(.r*&*X*,{v\\!DI|xd/\\fq$&BP%wzHp<5B\n _$[z;&w0MLNUi<DgXBGk(5_:ZXe`Z7/L*|L?\"y)'Q??*uXFga+hGMk|3l4Vs;<kz{|D(Y>Kr[ZKqrB\n G\"r,7$52*4*_SqHMr:\"QL"
    author: "faloaj"
  - subject: "Re: Online X-Face converter"
    date: 2004-11-29
    body: "And this is the KDE Logo:\n\nX-Face: 'aG(ui;S~fng7r.;2k_wUbMT}Rl19T%DF#NG~HFmI.+EWuRvArH`1{H(`<p#v0O_i(DwDb\\\n V+{)BN=/G6Dz`ETo$_83;<x1jqV4@IEVsJD',E+MSs\\v&t\"Fv6`8=P_P\\l\"--JAm~GrM].)u#OA:97\n zFHuf@Uxm/y4X[i"
    author: "falonaj"
  - subject: "XFace: Finally?"
    date: 2004-11-29
    body: "Glad to see NeXTMail is still 15 years ahead on this feature."
    author: "Marc Driftmeyer"
  - subject: "Re: XFace: Finally?"
    date: 2004-11-29
    body: "Why? Does it include holographic faces?"
    author: "Anonymous"
---
In <a href="http://cvs-digest.org/index.php?issue=nov262004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=nov262004">experimental layout</a>):
Extended password dialog can define length and strength thresholds.
Dell laptop buttons plug-in for <a href=" http://www.kde.me.uk/index.php?page=kmilo">KMilo</a>.
As-you-type spellchecking with aspell.
<a href="http://kaddressbook.org/">KAddressbook</a> import/export filter <a href="http://www.gmx.net/">GMX</a> addressbook format.
<a href="http://www.contactor.se/~matsl/HowtoCreateXFace.html">X-Face</a> support for <a href="http://kmail.kde.org/">KMail</a> and <a href="http://knode.sourceforge.net/">KNode</a>.
New blogging resource for <a href="http://korganizer.org/">KOrganizer</a>.



<!--break-->
