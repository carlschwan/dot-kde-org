---
title: "KDE-CVS-Digest for August 27, 2004"
date:    2004-08-28
authors:
  - "dkite"
slug:    kde-cvs-digest-august-27-2004
comments:
  - subject: "As always"
    date: 2004-08-28
    body: "Great work on the digest!"
    author: "am"
  - subject: "multimedia framework"
    date: 2004-08-28
    body: "\"Work started on a common multimedia interface to various backends\"\n\nGreat!"
    author: "mohasr"
  - subject: "KSpread XML schema changing"
    date: 2004-08-29
    body: "I notice the XML schema for KSpread is changing.  Are there any plans to continue supporting the old schema, so users can import their old documents and export to users who still use previous versions of KOffice?"
    author: "Burrhus"
  - subject: "Re: KSpread XML schema changing"
    date: 2004-09-01
    body: "Yes, the KOffice 1.3 formats will continue to be supported (but of course no new features...)\n\nOnly the future of saving to the KOffice 1.1 formats is not sure, as mostly it is only saving the 1.3 format as .tar.gz, but that does not mean that KOffice 1.1 could read it correctly.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "My name in the digest"
    date: 2004-08-29
    body: "First, I have to agree with everybody else:  Grrrrreeeaat work!\n\nBut: Why does everybody have their full names with their fixes, but I have my login (ingwa)? Is this a problem with the full name field of my CVS account? "
    author: "Inge Wallin"
  - subject: "Re: Refer to bug..."
    date: 2004-08-29
    body: "And another thing.\n\nI see that my entry has two different \"Refer to bug...\" sections.  Is this automatically added by the CVS digest program?  I thought you had to do it manually within the commit log."
    author: "Inge Wallin"
  - subject: "Re: Refer to bug..."
    date: 2004-09-01
    body: "The CVS-Digest system tries to find any digit sequence and turn it into a bug number.\n\nThat is different from the KDE CVS, where you indeed need to give the right CCMAIL.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: My name in the digest"
    date: 2004-09-01
    body: "You entry in kde-common/accounts seems right."
    author: "Nicolas Goutte"
  - subject: "Re: My name in the digest"
    date: 2004-09-01
    body: "What might be the problem then? "
    author: "Inge Wallin"
---
In <a href="http://cvs-digest.org/index.php?issue=aug272004">this week's KDE CVS-Digest</a>:
<a href="http://kopete.kde.org/">Kopete</a> Groupwise support ready for testing.
<a href="http://digikam.sourceforge.net/">Digikam</a> adds oil-painting and charcoal drawing effect plugins.
Two new kioslaves; kio-trash and kfiledevice for disk size and usage.
<a href="http://www.koffice.org/kexi/">Kexi</a> now supports subforms.
Work started on a common multimedia interface to various backends.

<!--break-->
