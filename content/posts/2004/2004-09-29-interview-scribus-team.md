---
title: "Interview with Scribus Team"
date:    2004-09-29
authors:
  - "fmous"
slug:    interview-scribus-team
comments:
  - subject: "golem.de"
    date: 2004-09-29
    body: "The link to the German Version on golem.de:\nhttp://www.golem.de/0409/33794.html"
    author: "MaX"
  - subject: "to editor"
    date: 2004-09-29
    body: "better Vanek than Van&ecaron;k"
    author: "jose"
  - subject: "Re: to editor"
    date: 2004-09-30
    body: "better use working browser + font."
    author: "Asdex"
  - subject: "Re: to editor"
    date: 2004-09-30
    body: "And even better yet, if W3C standards are respected!\nNot a browser/font problem, but a coding error"
    author: "Cinabrium"
  - subject: "Looks good"
    date: 2004-09-30
    body: "I must say that Scribus does look very good, and frokm my testing a few weeks back it I found no problems with it. It does take a while to get used to the way Scribus works, but I like the general idea.\n\nHowever, I found Scribus to be really slow, especially when moving frames or doing other \"basic\" activities that involved the main canvas being redrawn. I'll however give Scribus a spin when we make our annual wall calendar from our digital photos. I think it's going to be more fun than OpenOffice. :)\n"
    author: "Chakie"
  - subject: "Re: Looks good"
    date: 2004-10-01
    body: "That's true. Scribus is extremely slow when using drag'n'drop: moving/resizing objects. It's completely horrible when I try to move the nodes of a silhouette of somehting.\n\nBut well, no one is perfect. Thanks for your work, guys!"
    author: "MacBerry"
  - subject: "Need Good Tutorial"
    date: 2004-09-30
    body: "Great Product!\n\nNeed a good comprehensive tutorial based on the version 1.2. The available good tutorial (original one was in french) was based on 1.1 which has changed considerably in functionality and the user interface.\n\nMaybe the original author could redo the tutorial for version 1.2."
    author: "Eric"
  - subject: "Re: Need Good Tutorial"
    date: 2004-09-30
    body: "That would really be nice indeed. \n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Need Good Tutorial"
    date: 2004-09-30
    body: "Got a good one from http://www.bhairon.com/gnulinux.html. Also available from scribus.net. They must have just added it!"
    author: "Eric"
  - subject: "Re: Need Good Tutorial"
    date: 2004-10-02
    body: "Seen http://docs.scribus.net ???? More to come there btw..."
    author: "Craig Bradney"
  - subject: "Booklets"
    date: 2004-09-30
    body: "Hi,\n\nCan I use Scribus if I want to create A5 booklets printed on A4 paper, or A4 booklets printed on A3 paper? If my booklet has 48 pages, will page 48 and page 1 (2 and 47, 46 and 3, etc) be printed on two halves of a page? So I can just print it double sided, fold it in half and staple it?\n\nIf this works, is it then possible to add a graphic that spans from page 2 to page 3. So half of it is on page 2, the other half is on page 3. Is this taken care of automatically? I don't know if this is a good practice, but one sees it often in magazines and it is rather cool.\n\nJo"
    author: "Jo"
  - subject: "Re: Booklets"
    date: 2004-10-06
    body: "I don't know if scribus supports that, but to me that sounds mor like a job for pstops (a little tool made esp. for things like that)"
    author: "furanku"
  - subject: "Re: Booklets"
    date: 2005-01-12
    body: "It's on the Roadmap for 1.3 I think. I don't know if they're going for simple 'build booklet' functionality or more advanced imposition facilities.\n\nYou can build simple booklets using Cups and filters - I forget the details but I can explain if you respond to this message. No facility to adjust for creep as far as I could see unfortunately.\n\nProbably lots of other ways of doing this too."
    author: "Joeboy"
  - subject: "Re: Booklets"
    date: 2005-11-14
    body: "I'd really like to see a template/wizard/whatever for making books/booklets in Scribus. I think it's a wee bit odd that a command line program is all that is viable for making these booklets ;)\n\nKeep up the good work scribus. "
    author: "Sam Murray"
  - subject: "Re: Booklets"
    date: 2005-11-14
    body: "http://wiki.scribus.net/index.php/How_to_make_a_booklet"
    author: "Joeboy"
  - subject: "Re: Booklets"
    date: 2005-11-15
    body: "Many thanks. \nI'm using GNOME, not KDE but I'm sure there's a way around that. I'll try to get kprinter installed.\nSam"
    author: "Sam Murray"
  - subject: "Re: Booklets"
    date: 2006-01-25
    body: "This hint is for printing double-sided pages on a single-sided printer, not composing a booklet."
    author: "jscarry"
  - subject: "Re: Booklets"
    date: 2006-04-20
    body: "There is a command line package called pdfbook that does this very thing from PDFs. If you don't mind installing latex, I think it's what you're looking for.\n\nhttp://www.ctan.org/tex-archive/support/pdfbook/\n\nBovin"
    author: "bovin"
  - subject: "Scribus coders threatens by Quarck?"
    date: 2004-09-30
    body: "Some rumors on a conference in Bordeaux said that some developpers of Scribus managed to reverse-engineer the file format of Quarck express, and thet they have received death letters... Is it true?"
    author: "zoobab"
  - subject: "Re: Scribus coders threatens by Quarck?"
    date: 2004-10-01
    body: "I seriously doubt that (I don't know if it's true or not) but given how friendly Quark reps tend to be, maybe it's not that farfetched. ;-D"
    author: "regeya"
  - subject: "Funding"
    date: 2004-10-03
    body: "For me the question is important how to fund the project. How can an infrastructure be created which assures that the financial supply for development is there. "
    author: "Bert"
  - subject: "Re: Funding"
    date: 2004-10-04
    body: "well, there could be some financial support, but the project has come far without money. as have alot OSS projects. a sponsor whould be great, though."
    author: "superstoned"
  - subject: "speed=new paradigm"
    date: 2004-10-07
    body: "two aspects of speed exist when it comes to scribus.\n\nThe first: people complain scribus is slow when working on text in frames. True, but here is how you can get even faster speed than with traditional dtp apps. Use the story-edtior. think of it as a high-speed no-nonsense wordprocessor built into scribus. Don't touch text in frames, use this. And use the Properties palette. Fly-by-wire.\n\nThe second: remember the venerable pagemaker took almost a decade for an upgrade from version 6 to version 7. quark though much faster, took the traditional 'several years' for an upgrade too. scribus works on an almost daily upgrade. that is how the developers got those 700 bugs and wishlists fixed. i have been watching the development speed of scribus, when it was a squiggling sperm in a petri-dish. Someobody should just do a story on this process, speed, and rapid evolution, where developers and end-users merge into one gooey mess to scratch their personal itches every night.\n\nQuite remarkable."
    author: "linuxlingam"
  - subject: "Very impressive!"
    date: 2004-10-09
    body: "I'm hoping someday to find a way to get it to work with my workplace's current workflow, and convince my company that this is viable, in that order.  I've almost convinced myself to try doing some prepress work through Scribus at work in this coming week (wish me luck!)  I've been very impressed by Scribus, particularly its PDF support.  I've been using Scribus to set up templates for printing digital photos on my Linux machine, then printing the prints out either here at home on a decent Winprinter or for taking the PDFs to work to print prints on an Epson 1280 Photo.  Rock on.\n\nThe thing is, the way things are set up now we have a specific list of settings for one particular version of QuarkXPress (4.11, sadly), a particular version of Acrobat (5, sadly) and have gone as far as having a Quark template to use to prepare pages for a particular Konica imagesetter.  The main obstacle is that this was all set up by some of our corporate guys, and I have no idea how hard it will be to set this up.  Here's the difficulty:\n\nApparently when we prepare individual pages, it's enough to use EPS.  However, then we'll pull two pages into art frames in Quark, then print PostScript to a file using a particular PPD.  We then use Distiller to convert this to a PDF using settings mandated by our corporate techies.\n\nNow, I tried a little experiment a while back: I used a PPD intended for a LaserWriter 16/600 instead of the PPD intended for a Konica EV-jetsetter 9xxx-series (I don't work with it, so I don't know the actual model number) imagesetter, and everything went fine and dandy.  Not sure if that's enough, but I'm certainly hoping.  I just hate to waste (allegedly tight) company resources on an experiment on actual content.\n\nAnyone out there have any experiences working with Scribus in real print shops?"
    author: "regeya"
---
Used in production workflow of <A href="http://www.twintiertimes.com/">a commercial newspaper</A>! Chosen as as one of their '<A href="http://www.trolltech.com/products/hotnew/scribus.html">Cool Applications</A>' by Trolltech! 
  Nominated as finalist in the category 'Best Office Software' for the <A href="http://www.linuxformat.co.uk/awards/">Linux Format Awards 2004</A>! More than 750 bugs/feature requests closed since the 1.1.2 version! The professional DTP application <a href="http://www.scribus.org.uk/">Scribus</a> is steaming ahead! Curious about the Scribus team? We have an interview for you originally conducted by the kind people of <A href="http://www.golem.de/">Golem.de</A> just before the Scribus 1.2 release.



<!--break-->
<style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
  
  </style>

<p><strong>Scribus moves forward to a stable release 1.2. What was the premier
focus during the development?</strong></p>
	
<p><strong><em>Craig Bradney:</em></strong>  To bring Scribus up to a level of usability, reliability and
functionality where all levels of users can use Scribus as a serious DTP
program on the GNU/Linux and *NIX platforms. We've closed approximately
700 bugs/feature requests since we put in the bug tracker after 1.1.2
was released. It's easy to see we have really put in the hours over about
10 months to knock the bugs on the head and introduce some really great
features to Scribus.</p>



<p><strong>Scribus addresses an area with well established competitors. What's
your main goal - to fill a gap in within the software portfolio for
Linux or to drag people away from other dtp software?</strong></p>

<p><strong><em>Craig Bradney:</em></strong> Both. The DTP area of software on Linux is not well supported so there's
certainly a gap to fill there. There are many people that just have that
last area of needed software to be filled on Linux so they can jump ship
into the FOSS world. This has been the case for some of our developers,
and also for many many users we have spoken to.</p>

<p><strong><em>Peter Linnell:</em></strong> In the end, we really do not think in terms of "competitors", more:
"What can we provide to our end users and for ourselves to make a great
DTP application which is primarily, though not exclusively for Linux?"</p>

<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:375px;">
<p><a href="http://static.kdenews.org/fab/screenies/scribus/scribus4.png"><img src="http://static.kdenews.org/fab/screenies/scribus/scribus4_small.png" alt="Scribus in action" /></a><br/>
<em>The pages on the canvas show off the transparency tricks and gradients
now in Scribus</em></p>
</div>
<!-- IMAGE BOX RIGHT-->     


<p><strong>Do you plan to support other operating systems than Linux?</strong></p>

<p><strong><em>Craig Bradney: </em></strong>  Scribus currently runs on various flavours of *NIX. We know people have
tried Scribus on up to 128bit processors, and on BSDs, AIX, Mac OSX via
Fink, and Windows 2000 via Cygwin. We do have plans for a native version
of Scribus on Mac OSX and Windows, although no firm dates or development
processes have been put in place for those at this point. Linux is our
focus for now.</p>
 
<strong><em>Peter Linnell: </em></strong>  Scribus works very well with newer Macs on Fink in my experience. We
 have a really good maintainer for our fink packages, Martin Costabel.

<p><strong>Scribus took a jump start and got lot of attention with the release
of Scribus 1.0. But, is the software ready for a production use today?</strong></p>

<strong><em>Craig Bradney: </em></strong>  Yes, for sure. Certainly there will always be things people need and of
course the needs are higher at the top end of the DTP market. We know of
countless semi-professional magazines and personal publications in
production with Scribus. In more recent times we had also the pleasure
of helping a weekly commercial newspaper (20,000+ copies) in the USA get
off the ground using Scribus.

<p>Scribus 1.2 is lights years ahead of 1.0. We are hoping a lot of people
can now switch to Linux using Scribus for their DTP work, or finally
just achieve great results with a DTP program rather than trying to
achieve these kind of results with a wordprocessor.</p>

 
<p><strong><em>Peter Linnell: </em></strong> One of my magazine publishing clients has also allowed me to
install Linux workstations and Scribus alongside their specially
configured DTP workstations, both Mac and Win2k. They have been really
really supportive of allowing me full reign in a real world pre-press
department of a 4 color magazine. This has been invaluable to test and
to study Scribus' behaviour along side and within other DTP
applications.</p>

<p>I have done quite a bit of testing of Scribus PDF and PS files with
specialist pre-press pre-flight applications, as well as an advanced Fiery
RIP. These pre-flight tools examine the files for conforming to published
specs and commercial printing norms. Scribus files are *highly* conformant. In
my professional opinion the output is in some areas superior to commercial DTP
applications. We also have three commercial printers on our mailing list who
have verified the same results using imagesetting equipment.</p>



<p><strong>Who is developing Scribus and is any company directly supporting the
project?</strong></p>

<p><strong><em>Craig Bradney: </em></strong>  In order of joining the project:</p>

<ul><li>Franz Schmid (Germany) - original author, main coder, "Our Linus"</li>
<li>Peter Linnell (US) - docs writer, tester, webmaster for www.scribus.net, DTP IT consultant</li>
<li>Paul Johnson (UK) - code tester, reviewer, has done lots of work on performance and profiling, setup CVS and the Salford web server.</li>
<li>Craig Bradney (Australian living in Luxembourg) manages IRC and the bug tracker, bug testing, docs proofing, webmaster for docs.scribus.net</li>
<li>Petr Vanek (Czech) -  has written the special typography plug-ins, and other plug-ins, also works on the Python Scripter API within Scribus.</li>
<li>Riku Leino (Finland) - wrote the new templates plug-in, the html importer plug-in and is working on the text importer API.</li></ul>

<p><ul>
<li>The School of Music, Media and Performance, University of Salford, UK -
webhosting, CVS and FTP</li>
<li>Netraverse (with Win4Lin for testing purposes).</li>
<li>Linux New Media AG (Germany) made it possible to present Scribus at
CeBit 2004 and support us with nice articles in their magazines :)</li>
<li>Peter and Craig have their own companies which help support Scribus
in various ways. We are discussing how to offer commercial support for
Scribus in the future.</li>
</ul></p>

<p>Mostly the project moves ahead under its own steam fuelled by the
enthusiasm of the team and the various contributors, translators, and
help and encouragement from users out there. Of course, we are all lucky
to have supportive families too!</p>


<p>We have spent countless hours every single day since 1.0 was released.
Adding up those hours would be too scary to do. We have added some great
team members and had the support of contributors who all put in as much
to the project as they can.</p>


<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:375px;">
<p><a href="http://static.kdenews.org/fab/screenies/scribus/scribus1.png"><img src="http://static.kdenews.org/fab/screenies/scribus/scribus1_small.png" alt="languages" /></a><br />
<em>A look at the new story editor in Scribus 1.2 with Chinese text. Scribus supports Unicode extensively. Indic Script and full CJK support are on the todo list.</em></p>
</div>
<!--/IMAGE BOX LEFT-->



<p><strong>What about compatibility with other DTP software applications?</strong></p>

<p><strong><em>Craig Bradney: </em></strong>  In terms of import/export type functionality, none really, but thats not
just a "we can't do it" answer. It fits more to the idea developed from
experience that even commercial companies with full access to sources of
older versions of products just can't get import and export filters 100%
correct even with their large teams and budgets. In most people's experience it's faster and a much more accurate result is achieved when
you take the leap and switch software.</p>

 
<p><strong><em>Peter Linnell: </em></strong>  DTP files internally are very complex. Far more than most application
files. Second, EPS and PDF  is a more common means of export/import. Scribus
does very very well on that mark, in some cases more capable than commercial
DTP apps. For example, many EPS files can be imported, then edited as
native Scribus objects. No other page layout application can do that.
Third, the commercial print world (or at least the smart ones, in
my opinion) is embracing PDF as an exchange format. PDF solves many
interchange/cross-platform issues, especially fonts. Scribus, with its
powerful PDF exporter makes cross-platform issues disappear.</p>

<p>Those who say we should have a importer for DTP app X, belie their
inexperience with DTP file conversions. I have rarely had good luck with them,
except for the simplest files. Even importing between different versions of
the same app can be tricky. Moreover, this would require reverse engineering
a closed and complex format. It is our thought that precious developer time
is better spent on making Scribus better.</p>

<p>The fact that Scribus generates excellent PDF is the true "compatibility"
test. In some cases, it is not Scribus, but other DTP apps which are lacking
in newer PDF feature support. We have run across this in supporting the
handful of newspapers which are now using Scribus. Their printers were using
older versions of pre-press tools which could not support all the PDF
features Scribus is capable of creating. This has also been an issue for the 
latest versions of other commercial DTP apps. We have made notes in the docs
addressing this issue.</p>

<p><strong><em>Craig Bradney: </em></strong>  As for usage patterns, we want the general methods to be similar for DTP
users to come across to, but most of all we want our features and ideas
to shine through very strongly.</p>

<p>Where we have and will concentrate in future versions is on support of
formats that do make sense to give access to. For example, we have a new
import system, thanks to Riku, which allows us, or anyone, to write simpler
code and make an import filter for your favourite text or graphic file
format.</p>

<p><strong>What are the most important needs that you plan to address after the
1.2 release? Will there be a 1.2.x series?</strong></p>

 
<p><strong><em>Peter Linnell: </em></strong>  Yes, Franz has already branched 1.3 and 1.2.x. So we plan to release a
1.2.1 version in the coming weeks. Mostly bugfixes, but also new import
plug-ins from Riku. Petr Van&ecaron;k is working on extending the Python scripter.</p>


<p><strong><em>Riku Leino:</em></strong> With 1.2 we started on single plug-in system to group those plugins
together which handle the same type of things. The first such plug-in group
done was the Get Text plug-in API for importing formatted text to a text
frame. After I finished the html and csv importer plugins, there is now
an OpenOffice.org Writer importer under development which will most likely
be included with 1.2.1.</p>

<p><strong><em>Craig Bradney: </em></strong> 
To name just a few:</p>

<ul>
<li>Higher end DTP needs such as true on-screen joined facing pages to name
just one</li>
<li>PDF 1.5 support</li>
<li>Higher level of CMYK image import support</li>
<li>More import filters for text formats and image formats</li>
<li>Support for larger organisation usage (leaving this one wide open.. we
have nice plans shall we say)</li>
</ul>

<p>The wish list for the 1.3.x development series goes on forever.. we
discuss all the nice things we want for hours some days. Look forward to
an updated roadmap later this year :)</p>
  
  
  












