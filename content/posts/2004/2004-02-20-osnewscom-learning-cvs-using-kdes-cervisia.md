---
title: "OSNews.com: Learning CVS Using KDE's Cervisia"
date:    2004-02-20
authors:
  - "Editor"
slug:    osnewscom-learning-cvs-using-kdes-cervisia
comments:
  - subject: "More shameless plugs"
    date: 2004-02-20
    body: "who is the editor on this article?"
    author: "John"
  - subject: "Re: More shameless plugs"
    date: 2004-02-20
    body: "Do you think that it's edited or want to know who submitted it under the alias \"Editor\"?"
    author: "Anonymous"
  - subject: "Shameless? Nope!"
    date: 2004-02-20
    body: "You do know how the web works for news site entities, right? If you do, then re-read your comment.\n\nNice article btw."
    author: "Mambo-Jumbo"
  - subject: "Re: Shameless? Nope!"
    date: 2004-02-20
    body: "Yup, nice article. It has lots of stuff that The Cervisia Handbook could benefit from :-D\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Shameless? Nope!"
    date: 2004-02-20
    body: "True. I will incorporate the guide before the strings freeze."
    author: "cwoelz"
  - subject: "Incomplete name."
    date: 2004-02-20
    body: "Could someone please correct my name to Carlos Woelz, or Carlos Leonhard Woelz? Nobody calls me Leonhard :)."
    author: "cwoelz"
  - subject: "Re: Incomplete name."
    date: 2004-02-20
    body: "Done.  Not sure what happened there."
    author: "Navindra Umanee"
  - subject: "Printer Prettyfied Version"
    date: 2004-02-20
    body: "I took the liberty of taking the 'print' version form OSNews and fixing the markup and making a couple of stylesheets. The print stylesheet should make printing nicer.\n\nI sent a copy to the OSNews people as well.\n\n'hello? this is 1996 we called to see if we can have our made-for-printers Web pages back'\n\nhttp://steamedpenguin.com/design/CSS/fixingosnews/cervisia.html"
    author: "SteamedPenguin"
  - subject: "Removal notice"
    date: 2004-02-20
    body: "Actually, I have to remove this. Sorry lads and lasses"
    author: "SteamedPenguin"
  - subject: "Qutstnading!!!"
    date: 2004-02-20
    body: "This is really great! Excellent Job Carlos. I'm going to use this to make my life easier by referencing the link to any questions about using CVS on our user list. I think a lot of people assume it's difficult because it's a bit of a new concept to them and developers use it... therefore it must be some kind of black magic. ;-) I was amazed how easy it was to set up my own local CVS repository the first time. I showed my wife how to do it and she started keeping everything in CVS.\n\nThe idea of making applications CVS aware is very intriguing! I can imagine koffice files sent as XML and a parse diff routine that tell you this kpread cell has been added, changed or reformatted as well as viewing an annotated kword document. A versioning system could produce distrubuted office computing on a level we only dreamed of. Even more interesting is that as of KDE 3.2 Cervisia is now a combination of the visual interface you see and the underlying cvsservice which communicates via DCOP. We will be working with this in extending Quanta but actually this makes CVS integration much more accessible for any KDE app.\n\nMy thanks Carlos. Cervisia with CVS is a great tool for users that often people either don't see the value of or are intimidated by and don't try. I think this tutorial will make a difference for a lot of people."
    author: "Eric Laffoon"
  - subject: "Re: Qutstnading!!!"
    date: 2004-02-22
    body: "True. And there are apps like LyX that has been CVS aware for a long time. CVS support really makes writing docs a breeze!"
    author: "AC"
  - subject: "An idea for a tutorial"
    date: 2004-02-21
    body: "How about a tutorial on how to remove Cervisia from Konqueror, since it is not needed by any regular user I know.\n\nThat would be really useful."
    author: "rizzo"
  - subject: "Re: An idea for a tutorial"
    date: 2004-02-21
    body: "This tutorial is easy.\n\n1) Uninstall the kdesdk package. (Or better yet, uninstall Cervisia package if your distribution splits the kdesdk, Mandrake does that).\n2) You are done.\n\nMakes no sense to install something you don't use. People install a lot of stuff and then say: Hey, there is a lot of stuff and extensions in my computer I don't use!"
    author: "cwoelz"
  - subject: "Re: An idea for a tutorial"
    date: 2006-03-26
    body: "Please tell me, what do I do then?\nMy distro doesn't split kdesdk.\n\nI need KDevelop, but not CVS.\nI don't like the way that Cervisia intrudes in my Konqueror.\n\nI've tried removing all files from /opt/kde and subfolders named *cervisia*, but now Konqueror keeps complaining that \"Library files for libcervisiapart.la not found in paths\".\n\nGah...\n\n(This is extremely annoying, kind of reminding me of the browser war era and how there was no way to remove IE4 from your Windows installation...)\n"
    author: "mbest"
  - subject: "Re: An idea for a tutorial"
    date: 2006-03-26
    body: "KDevelop is not in kdesdk module, so where's the problem?"
    author: "Anonymous"
  - subject: "Cervisia ?"
    date: 2004-02-21
    body: "Sorry but I think cerivisia is a poor cvs client. I am using TortoiseCVS on windows and I wish we had anything in KDE that reaches this level of usability. Things that are pretty annoying in cervisia include:\n- file are marked as unknown although it is perfectely clear that the files are unmodified (according to the date of last modification)\n- no possiblity to perform more than one operation at the same time, no possiblity to queue multiple operations\n- poor handling of adding directories (TortoiseCVS will propose you to add any file of a directory in a similar case and perform all the necessary actions in one row)\n- the actions that are possible to perform on a file do not take into account the state of the file. For example, it should not be possible to add a file that is already in CVS, or commit a file that is up to date.\n- the icons are confusing with the one of konqueror when using it as a kpart.\n\nI am really disapointed and hope that either it will improve or that we will get a better cvs client anyday."
    author: "Philippe Fremy"
  - subject: "Re: Cervisia ?"
    date: 2004-02-21
    body: "Cervisia is not only a CVS client. It is a VCS backend and front end, at is is by all means not complete yet. Fill meaningful wishes with the ideas you might have."
    author: "cwoelz"
  - subject: "Re: Cervisia ?"
    date: 2004-02-22
    body: "Just a question since your blunt and extreme approach to describing other developers' work seems to be a common feature of most of your posts: Do your Rosegarden users jump at you like this as well?"
    author: "Datschge"
  - subject: "Re: Cervisia ? Oops!"
    date: 2004-02-22
    body: "Hey Datschge, maybe you should relax a bit. You're now lumping all French men into one if the are critical of programs. ;-) I believe you're thinking of Guillaume Laurent who codes Rosegarden. Philippe Fremy has done some very cool writings on toolkit comparisons, some interviews and other projects in KDE.\n\nWhile I would agree with you that it would be nice if developers could phrase their lamentations better (and we complain about users) it's really not fair to give him a hard time for wishing it was more feature complete. Especially when you're confusing him with someone else. ;-)\n\nWe could ask him for some patches though. ;-)"
    author: "Eric Laffoon"
  - subject: "Uhuhuh..."
    date: 2004-02-22
    body: "Actually it's a little more embarassing than that. I know who those two are, I guess having been tired and slightly angered led me to mix up the two persons (I linked to a different case where actually both were involved). I appologize to Philippe for the misled part of my response and pledge not to write responses anymore when I'm about to go to bed, heh."
    author: "Datschge"
  - subject: "Re: Cervisia ?"
    date: 2004-02-23
    body: "I am sorry that my post is so rude. I am just expressing my frustration."
    author: "Philippe Fremy"
  - subject: "Re: Cervisia ?"
    date: 2004-02-22
    body: ">- file are marked as unknown although it is perfectely clear that the files are unmodified (according to the date of last modification)\n\nThis is untrue. You might not have changed the file (last modification date) but another developer could have checked in a new version. So the status is unknown as long as you don't update it (F5 key). BTW there is an option to automatically update the status of your files when you open a working directory or on start-up. After activating this option you will never see the unknown status again.\n\n> - no possiblity to perform more than one operation at the same time, no possiblity to queue multiple operations\n\nAs I said there is cvsq (see BR #64649) until I had the time to implement it.\n\n> - poor handling of adding directories\n> - the actions that are possible to perform on a file do not take into account the state of the file.\n\nWhere are the bug reports? Sorry but I'm really bad in mind-reading.\n\n> - the icons are confusing with the one of konqueror when using it as a kpart.\n\nCan't fix that because I'm a real bad artist. If you want better icons you could vote for BR #67805, #67806, #67807, #67808 and #67809.\n\n\nI'm sorry that your disappointed but you should also give us sometime to try to improve this. When I took over this project it was hardly maintained and the functionality was firmly coupled to the GUI. With KDE 3.2 we took the first steps to improve this but it will take time.\n"
    author: "Christian Loose"
  - subject: "Re: Cervisia ?"
    date: 2004-02-23
    body: "> This is untrue. You might not have changed the file (last modification date)\n\nI said that the file is unmodified. Usually, you are interested in this feature to know which file you have changed on you hard disk and which file you should commit or diff. Knowing that a developer has changed the file meanwhile is a less common need.\n\nThe wording is probably the cause of the issue here. Maybe you could change it to \"locally unmodified\" which does not imply anything wrong.\n\nI don't want to issue a CVS request each time I launch cervisia, just to know that a file that I did not touch is not modified.\n\n> cvsq\n\nSorry, but this workaround does not suit me. I have a full connection so I do not need to postpone commands. I just want to issue more than one at the same time, which a simple fork could do. Besides, there is no point in using a graphical cvs client if I have to run a command in a termninal program.\n\n\n> > - the actions that are possible to perform on a file do not take into account the state of the file.\n \n> Where are the bug reports? Sorry but I'm really bad in mind-reading.\n\nI did mention that in the mail I sent.\n\nFor the bug reports, I don't want to file many wishes when my basic needs are not fulfilled. There is no point in spending time for describing many wishes if the developer has very little time for even basic useful features.\n\nI really wish I had more time to hack on tortoise cvs and bring it to KDE.\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Cervisia ?"
    date: 2004-02-23
    body: ">I said that the file is unmodified. Usually, you are interested in this feature to\n>know which file you have changed on you hard disk and which file you should commit\n>or diff. Knowing that a developer has changed the file meanwhile is a less common\n>need.\n\nPress F5?"
    author: "cwoelz"
---
<a href="http://www.osnews.com/">OSNews</a> is featuring <A HREF="http://www.osnews.com/story.php?news_id=6096">a detailed guide</A> written by Carlos Leonhard Woelz on how to use CVS, a version control system also used for developing KDE, using KDE's CVS client <a href="http://www.kde.org/apps/cervisia/">Cervisia</a>. It is a seven page long well-described article with many screenshots useful to both newbies and experienced users.
<!--break-->
