---
title: "NewsForge: Hot LyX"
date:    2004-09-25
authors:
  - "fmous"
slug:    newsforge-hot-lyx
comments:
  - subject: "Cool app but"
    date: 2004-09-25
    body: "Well LyX is a cool app, not for me though.\nFirst it could be so much better if it was a KDE app, in fact something like this could probably share some of the Koffice code, and probably contribute for it too. \nSecond, personally I like to type my own LaTeX. I keep a close look on Kile, it is comming along nicely, but it still doesn't even come close to (X)Emacs + AucTex and some more plugins. Best luck to Jeroen Wijnhout et al. \n"
    author: "John LaTeX Freak"
  - subject: "This must be joke."
    date: 2004-09-25
    body: "I thought dot.kde.org is my kde news source. What does lyx doing here?\n\nI've tried it. It merge the problems of latex and common word processor and gives nothing positive."
    author: "arael"
  - subject: "Re: This must be joke."
    date: 2004-09-25
    body: "It has a Qt frontend."
    author: "Anonymous"
  - subject: "Re: This must be joke."
    date: 2004-09-25
    body: "While i might agree that it doesn't necessarily fit on a KDE site, I have to say that I have found LyX to be an excellent program. Of course, I wouldn't recommend it to someone as a replacement for a word processor, but as a replacement for straight LaTex (which is heavily used within the scientific writing community) it has been fantastic."
    author: "David"
  - subject: "Re: This must be joke."
    date: 2004-09-25
    body: "I don't know that this is the reason, but Matthias Ettrich was the original author of LyX.\n\nIf you don't know who Matthias Ettrich is, may God have mercy on your soul.\n\nhttp://www.linuxgazette.com/issue57/correa4.html\n\nAlso, there's a Qt frontend.  I've often wondered what happened to the KDE frontend, though I suspect it disappeared when the GNU/Zealots were hammering away at the QPL."
    author: "regeya"
  - subject: "Re: This must be joke."
    date: 2004-09-25
    body: "Now I know him :). I'm not very interested in names anyway, but he deserves respect.\n\nQt frontend... I see. \n\nThanks. (May the God have mercy on my soul) :"
    author: "arael"
  - subject: "Re: This must be joke."
    date: 2004-09-26
    body: "The original KDE frontend KLyX, was more accurately a fork for KDE. Don't think it was ever ported back into the main LyX tree, and I think it pretty much died when LyX evolved. \n\nThe next KDE frontend was started when the LyX team started to make LyX GUI independent, but unfortunately this also died. One of the reason I will guess,  the manpower was better used to finish the Qt frontend. And since qt now can use the KStyles it looks better on the desktop. I think the code are in cvs still. \n\nA KDE frontend had been nice tho, with KIO and KDE sessionmanagment. Probably better to base it on the Qt frontend rather than bringing the old KDE code up to speed.   "
    author: "Morty"
  - subject: "Re: This must be joke."
    date: 2004-09-26
    body: "> A KDE frontend had been nice tho, with KIO and KDE sessionmanagment. \n\nCorrect, so where are the volunteers? :-)"
    author: "Anonymous"
  - subject: "LyX QT frontend"
    date: 2004-09-27
    body: "I use LyX for most of my professional work today. LyX is not perfect but it is a very reliable application with a large community of users ready to help. A KDE frontend would be very nice. At least, correcting the clipboard code in LyX so that it conforms to the freedesktop standard would be nice and make it function with Klipper. The LyX development team is looking for a QT specialist to help them enhance the very primitive LyX clipboard."
    author: "Charles de Miramon"
  - subject: "Re: This must be joke."
    date: 2004-09-28
    body: "LyX has a quality that I have yet to see in Word or Kword: stability and perfect crash recovery (for when Xwindow crashes and takes everything with it, cause LyX itself does not crash).\n\nThis is a really good point when writing a 150 pages thesis.\n\nLyX is a great app. One of the real \"killer app\" out there."
    author: "oliv"
  - subject: "Private Consultant"
    date: 2004-10-12
    body: "After ten years of latex, lyx was a great improvement, the more so that one can add latex code any time. It really cuts down typing in things like tables and graphics.\n\nIt is a killer app but very few know about it. Thats a shame. I think high school students would benefit from it.\n\nThe indexing is very much less work and the references, along with tkbibtek, are a breeze.\n\nHard to know what more one would want from a free program. Pdflatex works outstandingly and the new windows version seems to work reliably.\n"
    author: "samar singh"
---
Dave Fancella tried the <a href="http://www.lyx.org/">document processor LyX</a> and writes <a href="http://www.newsforge.com/article.pl?sid=04/09/15/1345248">his findings</a>: "<em>A common problem with word processors today is that they force users to deal with typesetting, a skill that is about as useful to a writer as metalworking is to a mechanic. This focus on typesetting means that writers have to spend too much time dealing with the way their documents look. To make matters worse, many documents are shared in a variety of different formats requiring additional time wasted in export, conversion, and quality control on the finished product. To help get around these obstacles, Linux users can turn to a document processor called LyX. LyX is optimized for writing and takes the chores of typesetting out of the writer's hands and places them in a competent professional: <a href="http://www.latex-project.org/">LaTeX</a>.</em>"

<!--break-->
