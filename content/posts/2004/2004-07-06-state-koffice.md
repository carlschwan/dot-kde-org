---
title: "The State of KOffice"
date:    2004-07-06
authors:
  - "blamb"
slug:    state-koffice
comments:
  - subject: "KOffice is great"
    date: 2004-07-06
    body: "KOffice is a really great set of apps. I like it much better than OpenOffice."
    author: "M"
  - subject: "Re: KOffice is great"
    date: 2004-07-06
    body: "KOffice still just has too many bugs and missing features to be usable for everyday work. I have been burned too many times by its bugs in the previous releases to entrust anything important to it.\n\nThe first time I tried to use it for anything serious, the application was KWord. I was working on my resume. I was able to view the document, but printing would mysteriously fail. The KOffice people blamed it on Qt's PostScript rendering bugs. That's good information for a developer, but users don't care about the technical reasons that their documents won't print. They just know that their data is trapped, unable to get from the computer onto the page.\n\nRecently, I decided to keep track of my food consumption with KSpread. When I entered that I had cottage cheese at 7pm, KSpread actually stored 6:59:59pm and displayed 6:59pm. In my case, no one is going to see this, but there is no way I could use this in business. Imagine the poor secretary who tries to create a schedule in KSpread and has to explain to people why they're scheduled to take a break at 6:59pm instead of 7pm! (This bug still exists in CVS as of a couple days ago, despite having several duplicate bug reports about the same bug that were filed months ago!)\n\nKOffice might be light and fast, but it's too buggy and incomplete for anything useful."
    author: "Marcus"
  - subject: "Re: KOffice is great"
    date: 2004-07-07
    body: "I have to agree.  I had the same problem with a KWord document that wouldn't print.\n\nThere is no hope for KOffice till the Qt PostScript bugs and glyph spacing bugs are fixed -- we need WYSIWYG rather than WYGIWYS :-) and stuff needs to print.\n\nBut, there is nothing wrong with KOffice except for its version number.  It should be about 0.7 or so.  For that version number it is a good application.\n\nAnother Qt based problem is that it doesn't find all of your Type1 fonts because there is an inconsistency between the naming methodology of Type1 and TrueType.  Another case of Design twice and Code once vs. Code first design later (when it doesn't work).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "What KOffice most needs..."
    date: 2004-07-06
    body: "Is a logo.  There's no KOffice icon."
    author: "Jonathan Riddell"
  - subject: "Keep up!"
    date: 2004-07-06
    body: "We really need KOffice! I'm all for making OpenOffice more KDE friendly and all, we need it badly as a short term solution. A great KOffice would be so much better though!\n\nThanks all developers and keep up the good work!"
    author: "Strasky"
  - subject: "Can't be done"
    date: 2004-07-10
    body: "OpenOffice is multi-platform and GPL'ed. It is impossible to us QT in a GPL multiplatform program no QT no KDE. That is why you will never see a GPL version of KOffice running under Windows."
    author: "LWATCDR"
  - subject: "I love kOffice"
    date: 2004-07-06
    body: "kOffice is a great set of applications. I use at least one every day. I especially love the memory footprint (or the lack thereof :) and the integration with the rest of my desktop environment (not integration in the way that MS Office integrates with Windows either). It achieves that elusive balance between not irritating the user with \"features\" (read: clippy) and maximum usability. \n\nKeep it up, we really do need kOffice. "
    author: "Erich"
  - subject: "Mail merge and KDE PIM?"
    date: 2004-07-06
    body: "Just a random thought I had whilst reading through... couldn't KWord be combined with KAddressbook for mail merge? It'd make sense... maybe it can already be done?\n\n(a happy KWord user...)"
    author: "Tom"
  - subject: "Re: Mail merge and KDE PIM?"
    date: 2004-07-06
    body: "A mailmerge plugin for kabc exist for KWord CVS HEAD.\n\nPorting it back has currently 2 problems:\n- message freeze (but despite the 20+ messages, there could be solutions)\n- the developer has currently not time to do it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Mail merge and KDE PIM?"
    date: 2004-07-07
    body: "That's a shame. It'd be good to see it implemented. The gradual merging in functionality of Kopete and kabc is a good model for other apps to follow, IMO."
    author: "Tom"
  - subject: "openo"
    date: 2004-07-06
    body: "why are office programms moving so slowly forward ?\n\nopenoffice is slow in releasing updates so is koffice."
    author: "chris"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "Complexity coupled with lack of developers?\n\nA lack of people needing the functionality that are programmers? I at one time used a wordprocessor extensively, but now KMail and a programming editor are all I need. No itch no scratch.\n\nKOffice is progressing steadily. One sticky issue that can consume developer time is file compatibility. Now KOffice uses OASIS, which allows use of other tools for conversion. We should see a new formula engine in KSpread soon.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: openo"
    date: 2004-07-06
    body: " A lack of people needing the functionality, that are programmers?\n\nyes i think so too. so want is the functionality that only NON-Programmers use ? it will be never implemented... perhaps something with money...\n\ni hope that there are enough people to do an office suite , since microsoft is making alot of money from its office products. So everyone needs it , no-one programms it ?."
    author: "chris"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "For non-programmers who badly need a certain feature in Kontact we've created the possibility to pledge a certain amount of money for this feature (cf. http://www.kontact.org/shopping/). This way you can encourage the participating contributors (yes, so far only one developer officially participates) to implement your most-wanted feature as soon as possible. Since this system is still very young it's hard to tell at this point in time whether it will work.\n\nIf the system works then I see no reason why it shouldn't also work for KOffice (provided there will be developers willing to implement improvements for money)."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "Because noone is interested in developing Office applications (=lack of developers - just count the number of KOffice developers...)? \nWhy: maybe missing recognition, frustrating comments in boards like this, thousands of lines of \"bad\" code/design to manage = lots of effort small effect, something to work on that is \"sexier\", missing knowledge about how to do that, developers think it's hard to write office applications,...\nThe first four applied to me, I'm also working on something else now (but want to go back to KOffice when this project is done)"
    author: "Norbert"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "Yes, sometimes a high frustration level is needed..."
    author: "Nicolas Goutte"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "The main problem of KOffice is to find developers with time.\n\nThere are only 3 professional developers (David, Laurent, Lukas), which most of the time have other tasks to do too. As for the non-professional developers, mostly we are people with limited time and/or limited hardware only.\n\nPart of the problem of finding new people is that the code can be complex and as such it seems that some potential developers are afraid to touch KOffice. :-(  However this situation leads that experienced developers need also to do tasks that less expert developers could do too.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "KOffice is a bit complex, that's a fact. However, if _I_ can hack my way around and do something that looks like adding value, anybody can. I didn't know any C++ when I started on Krita... (On the other hand, I have a fair bit of experience with big software projects, that's true, being a software developer by profession, although I've never worked on anything really graphical.)\n\nBut in the end KOffice is far smaller than other projects in its domain, so it's a good place to work if one finds the problem domain interesting. In fact, I picked Krita from all the other image apps out there because it was relatively small and easy to get into.\n\nI guess what I want to say is: if someone is interested in any of the things a KOffice app tries to do, dive in. For instance, if vector graphics are hot, then extending Karbon is probably the best way to learn about that topic. Karbon is small, clean, well-designed and has a lot of room for improvement. The same holds for all the other apps..."
    author: "Boudewijn Rempt"
  - subject: "Re: openo"
    date: 2004-07-06
    body: "Perhaps I should put again a link to the task pages:\nhttp://www.koffice.org/developer/tasks/\n\nEspecially to show that there is many tasks for may kinds of developers.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Krita isn't awakening"
    date: 2004-07-06
    body: "It's wide awake! Since October we've added: loading gimp brushes, Wacom tablet support, a brush tool, pen tool, airbrush tool, clone tool, fill tool, line tool, ellipse tool, box tool, image rotation, scaling and transforming, CMYK and GrayA color models, brightness, contrast and colorize plugins, more composition operatators. Not to mention bugfixes, lots of work on the internals and developer documentation (http://koffice.kde.org/developer/krita) and lots of stuff I've already forgotten.\n\nWe're working on: better access to image data for plugins, selections, previews for dialogs, channel depth > 8 bit, core improvements like easier access to image data.\n\nFinally, there's about half a dozen people regularly contributing to Krita's progress. With a bit of luck we'll have a first alpha done this autumn. And as soon as I figure out how to create them, I'll put up SuSE 9.1 RPM snapshots up for download."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "Is this app similar to KolourPaint? Or what's the difference?\n\n>I'll put up SuSE 9.1 RPM snapshots\nThank you."
    author: "Axel"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "Krita is intended to be to Kolourpaint what Photoshop or Paintshop Pro are to MS Paint. That is, Kolourpaint is a nice, light, quick image editor that doesn't support layers and other colour models than what your screen supports, and Krita is a big monster application that supports layers, colour models, deep images. Of course, currently you will probably have an easier time with Kolourpaint, since that's actually stable.\n\n(I'll only put up snapshots if and only if I can figure out how to create rpm's \nof a KOffice application without packaging all of KOffice... Currently I haven't got a clue, but I'm looking into it.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "Cool!\nGIMP is (or better: was ;-) the only GTK app I use, so I'm very excited to hear that Krita is making progress!\n\nAny screenshots? ;-)"
    author: "i_love_the_dot"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "> Any screenshots? ;-)\n\nOh nevermind, i just found'em some posts below :-)"
    author: "i_love_the_dot"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "Screenshots of the devlopment version at on the Koffice Web Site:\n\nhttp://www.koffice.org/krita/screenshots.php\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "I just want to add that it is amazing how much progress Krita made since Boudewijn started hacking on it. Krita has a very nice brush tool which works quite well, also a nice airbrush tool and there is also a line tool. Composing an image from various layers does already work, also zooming in and out. Krita can load and save various image types including the GIMP format. Krita will definitely become a great alternative to GIMP if it continues to progress like this! I really recommend to check it out! (The instructions to do so can be found at http://developer.kde.org/source/anoncvs.html). A big thanks to the developers of Krita!"
    author: "Michael Thaler"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "This info simply screems for a couple of screenshots..."
    author: "UglyMike"
  - subject: "Re: Krita isn't awakening"
    date: 2004-07-06
    body: "http://koffice.kde.org/krita/screenshots.php... But I see I should add a 'june' screenshot, too. Will do right now."
    author: "Boudewijn Rempt"
  - subject: "MS Windows version?"
    date: 2004-07-06
    body: "What about Windows version?"
    author: "Petr Balas"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "You will have to port it yourself"
    author: "Ian Whiting"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "And there you have my number one reason why I don't use koffice.\n\nThe three main apps I use daily are OpenOffice, Thunderbird and Firefox.  They are cross-platform, and are an absolute godsend for people like me who have to flop back and forth between operating systems.  Ok, I also use Dreamweaver on Windows and Quanta in KDE, but they are very similar now - enough so that for me they are almost the same app.  Other fence-hoppers include Gimp, Sodipodi...\n\nThe main reason OpenOffice is the dominant F/OSS player in the office applications segment is cross-platform availability.  Why should I have to install Linux in order to use F/OSS software?  Most folks are not willing to fdisk their machine just to try out a new email client...\n\nMy reasons for bringing this up are quite simple - writing F/OSS only for *nix or *bsd is no better than Microsoft only writing apps for Windows.  This \"us or them\" mentality goes against the F/OSS way of seeing things, no?"
    author: "spacemonkey"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "Why free software people should write free applications for free for propietary/monopolistic/expensive/insecure/predominant operating systems? Because you use it?\n\nIf you denfend the propietary software model and bought Windows, so go and pay for MS Office. Otherwise, be coherent and supoortive and keep using free systems."
    author: "charles dickens"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "Whilst many do write software exclusively for free operating systems for political reasons, I don't accept your analogy.\n\nPersonally, I believe in the free operating system, and I would therefore hope that efforts be directed to furthering this cause, rather than developing for both. However, it's sometimes a pragmatic decision not to go cross-platform: for design choices, performance reasons, and so-on. I suspect all of the Unix-only development (such as KDE), will eventually further the cause of the entire OS, it's just going to take longer; you have to evangelise the entire OS rather than individual applications.\n\nPersonally, I do use OpenOffice.Org, but it's for no other reason than document compatiability. I support the standardisation of OASIS as an open document format for all free office suites (OOo, KOffice, Abiword...), and would hope thateven some proprietary vendors were to interoperate rather than build themselves their own little corner. Does anyone know is there is any progress on that?\n\nFinally, will OASIS become (my apoligies if I'm behind the times) the default KOffice document format(s)?"
    author: "Martin Galpin"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "Yes: http://dot.kde.org/1061919133/\n\nWhich is great, because that means Windows users will be able to edit my files. Of course, if I only want them to read my files, I send them as PDFs."
    author: "James O"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "> Ok, I also use Dreamweaver on Windows and Quanta in KDE, but they are very similar now - enough so that for me they are almost the same app.\n\nI'm going to choose to take that as a compliment rather than an insult. ;-) I would also note that it invalidates the context of your argument and places it instead on how well the application serves you and interacts with applications on other platforms, not whether it runs there. I would also note that a number of our users are former Dreamweaver users who feel that Quanta is already vastly superior for their uses. I suspect others would be surprised if they really explored the potential of Quanta and it's related tools.\n\nIn fact any perception of equitable similarity is going to change before the end of the year. Version 3.3 of Quanta will be adding a built in PHP debugger, numerous enhancements, event actions automation, a greatly enhanced Kommander, vastly superior team development and more including resource sharing across our user base with KStuff. Areas that we hope to include in 3.3 but will certainly be in a BE release this fall include our new conceptual design interface and architecture called Rapid Object Templates which is the first true innovation in web development in years, as well as a messaging and annotation system, task and user interface personalities and more. We have a lot we are working on.\n\nI can say two things to Dreamweaver users with absolute conviction.\n1) By 2005 if you are using Dreamweaver even part of the time you are costing yourself 10%-40% of your time (with the possible exception of heavily graphics oriented work or flash)\n2) If you would like some real innovation and advancement you would find making a contribution to our efforts to be much more an investment than a donation. Our current limitations are capital to meet at aKademy and resources to increase sponsored developer time.\n\nLook at how long Dreamweaver has been around and how big of a company produces it. Quanta was released in the start of 2000 and did not begin serious development until mid 2002 when I sponsored Andras. You're probably not running a CVS version so that's some puttering about and less than two years development. Do the math. If we could extend that more we could blow by Dreamweaver at an incredible rate. We do our entire sponsorship annually for less than what a really good web developer can bill in a month. Of millions of users we have 11 sponsors and 24 individual contributors this year... and you are comparing Quanta as on a par with a $500 package. Many of our users are making money in less time and with less frustration *and* saving hundreds of dollars on software and upgrades. I am spending so much time away from my business working on Quanta/Kommander that I now find myself struggling to find funding to get to aKademy in a month and a half where I've been invited to give a class and... I don't think anyone has signed up because they don't yet see where it benefits them. What's wrong with this picture?\n\nThe real problem continues. Developers are not born with innate skills but people contributing their efforts are willing to learn in order to achieve their goals. Free to use never means free to develop. A tiny fraction of a percent of the community make things happen and many others give critical analysis while failing to see just how amazing the accomplishments are given the miniscule resources... and how big of a difference they could make by doing something or making a donation.\n\nHonestly, in the developer to user relationship, I've never looked down or talked down to users, but I'm beginning to feel that users don't really respect our team and what we're doing. When we produce something better it is like they are flatly amazed we could do that and somehow think we have reached out pinnacle. It is like users don't really believe we can actually beat the best of the commercial tools. I have a burning desire to make KDE and Linux the first choice for web developers... and a growing exasperation with a community that doesn't seem to get it. They seem to see glass ceiling for FLOSS, or we're good enough so why bother. I see us as just getting to the really exciting part where we can make all this happen and the last thing I want is to do it half assed. I smell blood in the water! I want to move in for the kill! I want to make it happen... but it's beginnig to feel like I'm about to kill myself and my finances in the effort. I would like to see a few more users to think rationally about the biggest room in the world being room for improvement. What we need in FLOSS is for more users to grasp the basic concept that their interests are being served by FLOSS and therefore it is in their interest to support it in a real and tangible way.\n\nQuanta is an example of an application what would have died as a GPL program if I had not held on to it. If I died today I think it would continue... but as for making it better? When I add up developers, testers, sponsors and contributors I'd say there are roughly 100 people who really deeply care about making it better. If I just wanted to be as good as Dreamweaver I never would have worked on Quanta. I would have given up and bought Dreamweaver.\n\nSometimes success can only be achieved by not hoping someone else pays the price for it. Financial stresses aside, I'm finding it difficult to accept how few people care about us becoming the first application that is best in class across all platforms, has a broad appeal and is native to KDE. We will have a hell of a time doing it at our present levels of support and I don't want to be R&D for Macromedia."
    author: "Eric Laffoon"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "In case you haven't noticed, there are a lot of F/OSS written only for Windows, such as Filezilla, Miranda IM, eMule, DC++, ABC, K-Meloen, 7Zip, etc."
    author: "ac"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "\"The main reason OpenOffice is the dominant F/OSS player in the office applications segment is cross-platform availability. Why should I have to install Linux in order to use F/OSS software? Most folks are not willing to fdisk their machine just to try out a new email client...\"\n\nWhy bother talking about Linux at all then?\n\nUnfortunately, you are extremely misguided if you think that the success of Open Office is down to it being cross-platform. The simple reason is that it is the best open source/free software office suite around, functionally speaking. Pure and simple. If it was KOffice that was functionally so complete, everybody would be using KOffice.\n\n\"My reasons for bringing this up are quite simple - writing F/OSS only for *nix or *bsd is no better than Microsoft only writing apps for Windows. This \"us or them\" mentality goes against the F/OSS way of seeing things, no?\"\n\nCross-platform is good, but in the end people want a nice integrated environment."
    author: "David"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "As far as I know, one of the main reasons for KOffice not being available for Windows is, that QT is not available on Windows as GPL. A GPL version of QT was originally only available for *nix'es. \n\nWhen QT was GPL'ed for Mac OS X that also created the foundation for the current project to port KDE App's to Mac OS X.\n\nCurrently a developer has to buy the commercial QT license to develop QT based Windows Apps.\n\nThe above is my understanding. Please correct me if I have got it all wrong :-)"
    author: "Claus"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "99% correct. Here's the other 1%...\n\nQt for X11/Unix is under a dual license, QPL and GPL. This allows Qt to be used by Open Source but not GPL applications, such as KWin, Cervisia, etc.\n\nThere are \"non-commercial\" versions of Qt for Windows. One is available on the Trolltech site but is outdated (2.3.0). The other comes with a Qt programming book and is current (3.2.1). The distribution of the runtime libraries is allowed."
    author: "David"
  - subject: "Re: MS Windows version?"
    date: 2004-07-06
    body: "Errr? Not by GPL applications? I think you're mistaken there. Almost all the software I run and write is GPL-ed, and based on Qt."
    author: "Andre Somers"
  - subject: "Re: MS Windows version?"
    date: 2004-07-07
    body: "I think it was just bad wording.  He meant that the GPL/QPL'ed Qt can be used by applications that the only GPL'ed Mac version can't be.  But then the example -- kwin -- was incorrect since it's GPL'ed.\n\nBut i.e. Klipper, KSirc and a handful of other things are under the original Artistic license which isn't GPL compatible."
    author: "Scott Wheeler"
  - subject: "No, I haven't forgotten you, KOffice"
    date: 2004-07-06
    body: "I'm still checking out KOffice from time to time.\nThere's a big need for a lightweigt, easy to use word or spreadsheet app. I think it was a great move to switch to the oasis format. Would it be possible to get around hacking own MS Office import filters for KOffice by piping the MS Office file through OO.org and than simply opening the oo.org-oasis file?"
    author: "thomas"
  - subject: "Re: No, I haven't forgotten you, KOffice"
    date: 2004-07-06
    body: "I've seen that discussed, and I'm afraid that despite the looks and functionality, Open Office is an absolute abomination code wise to develop with. Nothing is adequately separated, you can't re-use it elsewhere. It is a total mess. If you want to know some of the gory details, read this:\n\nhttp://ooo.ximian.com/hackers-guide.html\n\nNo wonder C++ gets such a bad name.\n\nBack to the subject, the Open Office filters are just too tied to Open Office and way too large, unholy and undocumented to be used by any other projects. Sad, but true."
    author: "David"
  - subject: "Re: No, I haven't forgotten you, KOffice"
    date: 2004-07-07
    body: "No, this time you read the grand-parent a little too fast. He does not suggest using OO's filters - which has been suggested over and over again. He suggests something like \"soffice -writer --print_doc_to_stdout_as_oasis input.doc\" and opening the resulting oasis file with kword. Note: I have no idea, if OO Writer supports something like this."
    author: "AC"
  - subject: "Re: No, I haven't forgotten you, KOffice"
    date: 2004-07-07
    body: "That definitely isn't going to work because you need the filters, and what they read and save, integrated with the features in the application. IT ISN'T GOING TO WORK."
    author: "David"
  - subject: "Re: No, I haven't forgotten you, KOffice"
    date: 2004-07-10
    body: "What?   They mean requiring OpenOffice to be installed, and using openoffice itself to convert it.  It would work fine but is rather inelegant. ;-)"
    author: "kundor"
  - subject: "release"
    date: 2004-07-06
    body: "Does anyone else think the major reason why KOffice doesn't get as much notice as it deserves is because it has a different release schedule than KDE?\n\n-Benjamin Meyer"
    author: "Ben Meyer"
  - subject: "Re: release"
    date: 2004-07-06
    body: "hmmm, I dont know. it is possible. but if Koffice should follow the KDE release schedule, it might be delayed by the whole of KDE, it might delay the whole KDE, or be forced to bring out an not-yet-finished version. and it might have to wait with bringing out a new, updated version. No, I whoulnt include it."
    author: "superstoned"
  - subject: "Re: release"
    date: 2004-07-06
    body: "Would it really?  With ten time as many applications many just as complicated as those in KOffice why would KOffice cause major delays?  KOffice shouldn't have major refactoring every release. \n\nI know that nine times out of ten I don't install KOffice because it isn't with the rest of KDE.  I don't think the KOffice packages have ever been created for debian stable, only the core gets built (talking about kde3 here).  You have to go out of your way to remember to install KOffice.\n\nAs a KDE developer I find myself rarely ever even checking out KOffice because of its discontinuance with the rest of KDE."
    author: "Ben Meyer"
  - subject: "Re: release"
    date: 2004-07-06
    body: "The main problem with releasing KOffice at the same time as the rest of KDE are lack of developers, as most of the KOffice developers also works on core KDE they spend most of their time close to releases fixing bugs in KDE. Like David Faure. If someone wants to check CVS commits to KOffice they probably will se a major decreese around KDE releses. What KOffice realy need are more developers, even more if you want to sync relese with the rest of KDE."
    author: "Morty"
  - subject: "Re: release"
    date: 2004-07-06
    body: "Is that a chinken and a egg problem?  If KOffice is released with KDE it will get more developers, but it can't be released with KDE because it doesn't have enough developers."
    author: "Benjamin Meyer"
  - subject: "Re: release"
    date: 2004-07-06
    body: "Yes, it can be seen so too.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: release"
    date: 2004-07-06
    body: "KOffice was in KDE at KDE 2.x times or so. Despite that it had little attention. (It got a little better when it was split of KDE to get its own timeline.)\n\nFor example, it would be useless to release a new KOffice with KDE 3.3, as the OASIS porting is not finished and it would still not solve main problems that Qt4 is supposed to help to solve (as Qt4 is supposed to be designed as such.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Look at What is in KOffice"
    date: 2004-07-06
    body: "KWord is pretty easy to use and KSpread is one of the most usable spreadsheet apps I've seen. Everything is just - there. I've seen someone who had never even seen KSpread before in their lives select some cells, click on Data and sort them as if they had done it for years. That never happened with Calc or Excel. Yes, I know KOffice still needs development and that some things in KWord, KSpread and the main applications make it a bit iffy to use full time but you can't knock what is currently there.\n\nHowever, has anyone took a look at what is in it as a whole? There is Kivio (which I think is a killer app personally), Kugar for reporting and Kexi looks as if it is coming along nicely. Yes, I know some people criticize Kexi, mostly for the way it is developed, but functionally you can't argue with it.\n\nWhen everyone is disappearing into hole of hype over open source software, KDE never ceases to amaze me in terms of the ordinary, mundane, sometimes boring, common sense functionality there is. Critically, it is the sort of boring stuff that everyone needs.\n\nAll it needs now is time, developers and perhaps a bit of investment :)."
    author: "David"
  - subject: "I've been choosing KOffice for a long time"
    date: 2004-07-07
    body: "For a long time I've been choosing KOffice over OpenOffice... OpenOffice makes my computer seem 10 year old in term of speeds and the ui is too complex.\nI think KOffice is a great set of apps, specially, KWord and KSpread (the two most used by me).\nSo, please, KOffice developers, go on developing, you are doing a great job!!!!"
    author: "Pupeno"
  - subject: "Koffice rules"
    date: 2004-07-07
    body: "When will KOffice switch to OpenOffice-Formats? this will make life a lot easier as the Openoffice transformation filter of Wordfiles can be used."
    author: "hein"
  - subject: "Re: Koffice rules"
    date: 2004-07-08
    body: "Take a look at the feature plan for KOffice 1.4 (especially the in progress section):\nhttp://developer.kde.org/development-versions/koffice-features.html"
    author: "Christian Loose"
  - subject: "Re: Koffice rules"
    date: 2004-07-08
    body: "Well, if you only want the OpenOffice Writer format and not OASIS, I suggest that you look at KOffice 1.3.2 (in process of being released.)\n\nIt is perhaps not the native format, but the OOWriter to KWord filter is getting better (mostly backports from CVS HEAD.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Video Editing"
    date: 2004-07-07
    body: "Is there something like video editing software for Linux/KDE?n Or a software that supports something like Flash?"
    author: "martin runde"
  - subject: "Re: Video Editing"
    date: 2004-07-08
    body: "gg: \"kde video editing\""
    author: "anonymous"
  - subject: "Re: Video Editing"
    date: 2005-06-24
    body: "I did exactly that, and this was the first result! :P"
    author: "Casey"
  - subject: "I was surprised by the review - it seemed unfair"
    date: 2004-07-08
    body: "Kspread: I don't recognise the negatives you mention.  I've got no idea what a pivot table is for (has anyone?) but for your intro I might have thought this was a troll... Similarly, on rehearsing timings I'm in government and I get to sit through a lot of presentations.  I'm adapting Andrew Lang's comment on statistics \"he used presentations as a drunk uses a lamp post - for support rather than illumination\" \n\nGenerally I think charting from spreadsheets is p..s poor, particularly the difficulty of producing real y= f(x) type graphs\n\nPersonally, I think for complicated stuff stick to a database  \n\nKOffice: I was pleasantly surprised when I last opened an MS Word doc on my SUSE desktop: KOffice sprung into life and worked (though when I opened a complicated/long one it screwed- but I'm confident that given time things will improve).  It is also getting quite good at editing pdf files - a useful and unmentioned feature.  I don't think it generates contents pages and I remember a million years ago having a word processor thingy including dictionary (wordstar?) on one 3.5 inch disc (those were the days) and it generated indexes automatically - a nifty feature  \n\nKvivio - whatever, don't use it, don't know what it's for \n\nKrita - Kolourpaint seems to be moving along faster - perhaps there's room for combining? \n\nKugar (see above) spare me from generated reports\n\nI particularly like the fact that KOffice follows totally the *nix philosophies (I always know where to find how to generate a pdf, it's in CUPS...) \n\nI contribute by regularly paying for SUSE updates (or is that just KDE?)  Not too sure about how to report bugs (never sure it isn't me...)"
    author: "Gerry"
  - subject: "Re: I was surprised by the review - it seemed unfair"
    date: 2004-07-08
    body: "As written elsewhere in this discussion, Krita is more complex than Kolourpaint (16 bit deep channels, CYMK...), so of course it is slower to progress.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: I was surprised by the review - it seemed unfa"
    date: 2004-07-08
    body: "If you think a pivot table is not necessary in an office suite spreadsheet, then you haven't used an office suite.\n\nCharting does not have to be complex. It can be really simple. Sometimes, you do not need a very complicated chart.\n\nThe biggest problem with an office suite, is that people generally use very different features. so if you feature set is smaller, it becomes less useful to a lot of people. An office suite needs to have a lot of features, and you cannot run away from that. OOo benefits by being available on a nuber of platforms, which is Koffice's biggest disadvantage. Even Gnumeric is now being ported to windows, because its good to have momentum."
    author: "Maynard"
---
When was the last time you took a look at <a href="http://www.koffice.org/">KOffice</a>, KDE's native office suite? <a href="http://www.zurgy.org/kde/koffice.html">This article</a> looks at the good, and the bad, in the latest version of the 1.3 series. Although OpenOffice.org grabs most of the limelight KOffice has been steadily improving, with a low memory footprint and tight integration with <a href="http://www.konqueror.org/">Konqueror</a> you might find useful.



<!--break-->
