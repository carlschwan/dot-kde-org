---
title: "The People Behind KDE: Alexander Kellett"
date:    2004-04-26
authors:
  - "Tink"
slug:    people-behind-kde-alexander-kellett
comments:
  - subject: "Netherlands"
    date: 2004-04-26
    body: "I heard KDE will also get translated to Frysk, a dutch regional language. So, does he speak Frysk or low saxon?  "
    author: "gerd"
  - subject: "Re: Netherlands"
    date: 2004-04-26
    body: "unfortunately not, only been in nederland for the last 3 years \nso i'm not even very good at nederlands itself yet :P\n\ncheers,\nAlex (half dutch half english)"
    author: "lypanov"
  - subject: "Re: Netherlands"
    date: 2004-04-26
    body: "What about esperanto..."
    author: "Axel"
  - subject: "Re: Netherlands"
    date: 2004-04-26
    body: "I am sad too, that Esperanto is not part of the distributions anymore!\n\nThink about it: EU is getting larger with 1.5. and we need a really good\ncommon language!\n"
    author: "atrink"
  - subject: "Ah, Bindings!!"
    date: 2004-04-26
    body: "...I'm still waiting for...\n...drumroll...\na FreePascal binding!!! ;-)\nWell, I know this is still far away but this\nwould be WAY cool.\nFreePascal is now very advanced, supports inline\nassembler code, compiles extremely fast, and more.\nIf only there would be some way to write KDE apps\nwith it.\n\n"
    author: "Martin"
  - subject: "Re: Ah, Bindings!!"
    date: 2004-04-26
    body: "What are you waiting for?  I hear KDE's systems for generating language bindings are second to none.  If you want FreePascal bindings, and nobody else is working on them, then get cracking!  FreePascal isn't going to bind itself you know :-)"
    author: "Spy Hunter"
  - subject: "Re: Ah, Bindings!!"
    date: 2004-04-26
    body: "\"I hear KDE's systems for generating language bindings are second to none\"\n\nYes, it would be possible to generate Pascal code from the KDE headers via the kalyptus tool, assuming FreePascal has OO features. \n\nBut can FreePascal interface directly to C++, or does it need to go via C bindings? If so that's a major problem. And as Pascal isn't a dynamic language, you wouldn't be able to use the 'SMOKE' library.\n\nAshley Winters, who designed SMOKE, has proposed a library for using static languages with SMOKE, called 'Mirror'. If there was enough demand for static language bindings he might implement that."
    author: "Richard Dale"
  - subject: "Re: Ah, Bindings!!"
    date: 2004-04-26
    body: "That's the problem. FreePascal is an OO-Language with (non-trivial)\nOO features but it cannot interface directly with C++. This is the\nmajor hurdle right now unfortunately AFAIK."
    author: "Martin"
  - subject: "Re: Ah, Bindings!!"
    date: 2004-04-28
    body: "The nasty evil part is trying to interface Qt with any language lacking usable argument-based method dispatching. What the hell am I supposed to name the 20 different insertItem() functions in a language limited to one set of arguments per function name? (fortunately for all, the O'CamlQt bindings have no problems here)\n\nThat alone is enough to stop any serious effort at generating a programmer-friendly C binding. As far as making a programmer-unfriendly C binding for the purpose of improving performance in static languages by allowing JNI-style calls into the wrapper library -- I'd need to see some really bad C# benchmarks, wherein Gtk# is kicking our ass 20x. :)"
    author: "Ashley Winters"
  - subject: "Re: Ah, Bindings!!"
    date: 2004-04-28
    body: "\"..fortunately for all, the O'CamlQt bindings have no problems here\"\n\nAnd I thought this was another one of our jolly kdebindings April Fools :). Woo hoo! - a functional language using a GUI binding based on message passing OOP.\n \n\"I'd need to see some really bad C# benchmarks, wherein Gtk# is kicking our ass 20x. :)\"\n\nYup no problem, I think I can deliver that sort of thing - I think the initial version may well run at about that speed. Especially for me as the jit doesn't work on PowerPC, and I have to run everything with mint.\n\nOn the other hand, perhaps gtk# is an example of premature optimisation over design/implementation flexibility. The tortoise and the hare?"
    author: "Richard Dale"
  - subject: "Re: Ah, Bindings!!"
    date: 2004-04-26
    body: "Lazarus\n\nhttp://lazarus.freepascal.org"
    author: "gerd"
  - subject: "pic"
    date: 2004-04-26
    body: "A lypie pic in which he is not smiling (or fainting).\n\nThis is what I call cognitive dissonance. I can't deal with it."
    author: "taj"
  - subject: "Re: pic"
    date: 2004-04-27
    body: "But it look human on this one ;)"
    author: "Mathieu Chouinard"
  - subject: "Minor correction - Alex is not weird he is 'wierd'"
    date: 2004-04-26
    body: "That is his preferred spelling..\n\nHe did more for the ruby bindings than he said in the interview - great stuff Alex!"
    author: "Richard Dale"
  - subject: "Re: Minor correction - Alex is not weird he is 'wi"
    date: 2004-04-26
    body: "Is there some hidden meaning here ?\n\nAccording to http://www.lcfanfic.com/faq_grammar.html\n\"Wierd is not a word. The correct spelling is Weird.\"\n\nAlso my English/German dictionary doesn't know a word wierd.\n\nDid you mean wired ~ connected ?\n\n"
    author: "Harald Henkel"
  - subject: "Re: Minor correction - Alex is not weird he is 'wi"
    date: 2004-04-27
    body: "http://home.pacific.net.au/~turner23/wierd.html"
    author: "Martin"
  - subject: "Re: Minor correction - Alex is not weird he is 'wi"
    date: 2004-04-27
    body: "Thanks for the input :-)"
    author: "Harald Henkel"
  - subject: "Re: Minor correction - Alex is not weird he is 'wi"
    date: 2004-04-27
    body: "\"Is there some hidden meaning here ?\"\n\nNo, weird is the correct spelling, 'wierd' isn't a proper word. Hence, 'wierd' is weirder ;)\nAlex used that spelling once on the kdebindings list, so it's just me being silly - I left off a smiley perhaps.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Minor correction - Alex is not weird he is 'wi"
    date: 2004-04-27
    body: "The Dutch spelling would be 'wierd' because that's the way it's pronounced and it's popular in the Netherlands to spell it that way. I guess my spelling checker picked it up and changed it to weird.\n\n--\nTink\n\n"
    author: "Tink"
  - subject: "Re: Minor correction - Alex is not weird he is 'wi"
    date: 2004-04-27
    body: "ooohh, wow. your probably right on this tink! \ni've always wondered why i spell it \"wrong\" every time :P\n\nAlex"
    author: "lypanov"
  - subject: "Re: Minor correction - Alex is not weird he is 'wierd'"
    date: 2004-04-27
    body: "thank u richard :)\nfor the \"great stuff\"\nand for the preferred spelling :P\n\nAlex"
    author: "lypanov"
---
This week <a href="http://www.kde.nl/people/">The People Behind KDE</a> travels back to the 'Low Lands' or Netherlands where we visit the guy who is known for his 'gruesome green shorts', wants to marry into a KDE family and describes himself as 'very weird'. Are you anxious to find out more about this colorful personality? Than read all there is to know about <a href="http://www.kde.nl/people/alexander.html">Alexander Kellett!</a>

<!--break-->
