---
title: "KDE Dot News: Mailing Lists Relaunched"
date:    2004-01-11
authors:
  - "numanee"
slug:    kde-dot-news-mailing-lists-relaunched
comments:
  - subject: "You can't do that!"
    date: 2004-01-11
    body: "You wrote:\nWe own Andreas Pour ...\n\nHey, slavery is forbidden by international laws! ;)"
    author: "J\u00f6rgen S"
  - subject: "Re: You can't do that!"
    date: 2004-01-11
    body: "True, but it still exists... http://www.iabolish.com/ (flashturbation warning!)"
    author: "Jilks"
  - subject: "Re: You can't do that!"
    date: 2004-01-11
    body: "International Law and treaties does not really stop some countries or rulers."
    author: "a.c."
  - subject: "Re: You can't do that!"
    date: 2006-07-03
    body: "I don't know would that include the US.  You know the Geneva convention, rights to a fair trail.  No, no rights as long as the president says you are a military combatant (alias POW)."
    author: "Alex vaughn"
  - subject: "Re: You can't do that!"
    date: 2004-01-11
    body: "OWE, not own, learn how to read man."
    author: "somekool"
  - subject: "Re: You can't do that!"
    date: 2004-01-11
    body: "Looks like it was typo and corrected. :)"
    author: "Navindra Umanee"
  - subject: "Re: You can't do that!"
    date: 2004-01-11
    body: "Sush!  How do you think all the KDE stuff has been advancing so fast recently!?!\n\nDon't let them know they can stop whenever they want...\n\n;)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: You can't do that!"
    date: 2006-06-27
    body: "well you are right, "
    author: "geor.ch"
  - subject: "Zerowing"
    date: 2004-01-11
    body: "All your Andreas Pour are belong to us."
    author: "Chris"
  - subject: "Re: Zerowing"
    date: 2004-01-12
    body: "Conclusive proof that /. is bad for your health."
    author: "Beeman"
  - subject: "Re: Zerowing"
    date: 2008-07-17
    body: "Chronic polygon fondling is bad for you, /. rules forever!  \n----\n\u00a8Whats everyones beef?  I do not want to take over the world anymore, just a small corner of the solar system......\u00a8"
    author: "Clayton"
  - subject: "MR"
    date: 2004-03-09
    body: "I have just purchased Knoppix 3.2 bootable OS cd to use as a 'Data recovery Tool'.\nI am a Hardware Technician who usaully works with Windows NT,2000,XP.\nI am a NOVICE at using Linux KDE and need advice/help with the BASIC`s of Linux KDE etc, I hope there are some very patient people out there to HELP me in my quest ie: web sites and Books.\nThankyou"
    author: "David Caradoc-Hodgkins"
  - subject: "Re: MR"
    date: 2005-10-30
    body: "do people respond to these type of things? I wouldn't mind helping someone out, but it looks like its just spam."
    author: "Vlad Blanton"
  - subject: "Re: MR"
    date: 2006-04-14
    body: "I'm not to fermiller with Knoppix I'm using fedora core 4\nusing kde desktop I Did find a web site \nhttp://www.kde-forum.org there you can find help\nor I can help you configure KDE Desktop"
    author: "Wilhelm VonHoftseder"
  - subject: "interested in the kde program"
    date: 2004-03-09
    body: "i have window 98  in this computer but ik also want kde in this one nd the OTHER COMPUTER i have, so i can transfer data back  and forth. i  am looking to  start a business with this program nd 2 computers... charlie,,, marie lutman"
    author: "charliecharlie"
  - subject: "Re: interested in the kde program"
    date: 2004-03-09
    body: "I HAVE REDHAT/KDE (3) DISC FORM FREEGEEK  IN PORTLAND,ORE I HAVE BEEN SPENDING TIME WORKING AT THERE PACE TO EARN A  COMPUTER  WITH UNIX PROGRAM    ON  IT, BUT I HAVE 2. I HAVE THESE DISC BUT I DON'T KNOS IF I CAN ADD THEM TO A WINDOW 98 PROGRAM.. CONTACT ME .... NO REGISTATION NO. ON IT.. BURNED CD.....MARIE CHRLIECHARLIE.\n\n"
    author: "charliecharlie"
  - subject: "Re: interested in the kde program"
    date: 2004-07-23
    body: "I have a celeron box I have been trying to install SuSE on to no avail...any suggestions are aprreciated (it's an old compaq). "
    author: "Monti CIski"
  - subject: "Re: interested in the kde program"
    date: 2006-01-09
    body: "Please ask this question on the suse-linux-e att suse daut com mailing list."
    author: "Shriramana Sharma"
  - subject: "Re: interested in the kde program"
    date: 2007-08-27
    body: "Hi, \nA two spys called linda campbell ,kim johnson and an international student from israel ,managments major   work with a homosexual \ncanadain policing officers including fire fighters,RCMP ,ambulance cars drivers inoreder to \nattack ,torture and kill muslims new immigrants in public and canadain citizens in form of \n(holocaust) using wireless weapons causes cancer ,heart attacks,brain damages ,MS,body pain \n,coma ,vamting,kidiney faiture ,flu , and other body damages and msytrious illness  ,we need \nyour help to stop these crimes against humanty  hapening in canada ,alberta \n,cities of lethbridge and calgary committed by the canadain policing system and the government of canada,the israelate student was ssuccesful in killing two canadain citizens in Lethbridge city ,Alberta ,canada  ,linda campbel is university of lethbridge \ngraduant fine art department ,music major,addictive to drugs like marijwana  ,born in british columbia BC canada . \n"
    author: "terrela smith"
  - subject: "KDE  congress"
    date: 2007-10-06
    body: "\nSalve, scrivo per partecipare all'invito del congresso del KDE. Lavoro per www.enel.com e www.enel.it e dobbiamo rivoluzionare gli schemi dei siti per dare un'informazione autorevole in forma nuova e competitiva.\n\nSpero di vincere la selezione, un saluto\nFlavia"
    author: "Flavia"
---
I'm pleased to announce that the <a href="https://mail.kde.org/mailman/listinfo/dot-stories">dot-stories</a> and <a href="https://mail.kde.org/mailman/listinfo/dot-headlines">dot-headlines</a> mailing lists are finally back online.  For those of you who don't know, dot-stories is <i>the</i> list to be on if you wish to receive the latest KDE Dot News in your inbox, and dot-headlines is the list you should subscribe to if you wish to receive the headlines only.


<!--break-->
<p>
The mailing lists were previously hosted for <a href="http://dot.kde.org/995506251/">almost three years</a> by <a href="http://www.kde.com/">KDE.com</a>, which has recently been having financial troubles and is now seeking sponsors.  Please contact <a href="mailto:webmaster@kde.com">the webmaster</a> if you can be of assistance.
<p>
We owe Andreas Pour a dept of gratitude for easing the transition from KDE.com to KDE.org by providing both the code as well as assisting the transfer of subscribers from the old lists to the new ones.

