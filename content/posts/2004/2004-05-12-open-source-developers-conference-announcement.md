---
title: "Open Source Developers' Conference Announcement"
date:    2004-05-12
authors:
  - "gstaikos"
slug:    open-source-developers-conference-announcement
comments:
  - subject: "More Details"
    date: 2004-05-11
    body: "This conference is a great opportunity to do \"2 in 1\" since OLS follows.  Register soon to get the lower rates!  The conference will likely pick up where the X11 conference in Boston left off in April, and any who followed along will know that it was very informative and productive.  I think we'll have more opportunity for practical (\"hacking\") sessions at this conference, especially for those who stay through OLS.  Let's show a nice KDE turnout!"
    author: "George Staikos"
---
The organizing committee would like to announce the <a href="http://www.desktopcon.org/" target="_new">Desktop Developers' Conference</a> in Ottawa, Canada, July 19-20th 2004, the two days preceding the <a href="http://www.linuxsymposium.org/" target="_new">Ottawa Linux Symposium</a>.

The Desktop Developers' Conference is targeted at those developers who are contributing to the development of Free Desktop environments, application integration, and free operating system infrastructure intended to improve the desktop user experience.

It is important for people involved in the KDE community to attend in order to work together with other projects on the "bigger" picture of Linux on the desktop.  If you are able to attend, please contact <a href="mailto:staikos@kde.org">George Staikos</a> who will be coordinating the KDE presence.


<!--break-->
