---
title: "LWN.net: The Grumpy Editor's Guide to Graphical Mail Clients"
date:    2004-07-09
authors:
  - "binner"
slug:    lwnnet-grumpy-editors-guide-graphical-mail-clients
comments:
  - subject: "well"
    date: 2004-07-09
    body: "they are right ;-)\n\nkmail IS great, and getting better."
    author: "superstoned"
  - subject: "Check out the user's comments"
    date: 2004-07-09
    body: "It is not so much the article that caught my attention but the high ratio of users choosing and admiring KMail."
    author: "Phoenix"
  - subject: "KMail rocks"
    date: 2004-07-09
    body: "Yes, KMail kicks butt, even in the current version on sid, which lacks good IMAP I still use it for that.  Currently, I have issues like IMAP folders showing new mails in brackets, but not showing them in the actual message lists; filters not working for IMAP; and the slow speed of moving messages is a little worrisome, too.\n\nI know most of the IMAP stuff has been re-written, and that it now supports filters.  But I'm not sure whether sieve has to be installed on the server and seperately configured for KMail to use it.  Does anyone know whether it'll all just work now?\n\nAnyway, as I say, KMail rocks.  I'd have to recommend it over anything else out there for desktop mail.  Especially with how it integrates into Kontact with 3.2.  Run KMail, run the full suite: it's your choice.  And the way the KMail kicker icon minimises the whole of Kontact when you're using it.  Gotta love those little touches that show off a good underlying design :)"
    author: "Lee"
  - subject: "remarks on Kmail"
    date: 2004-07-09
    body: "I use the latest sid too. I get several comments about Kmail:\n- It doesn't permit to set the Sent/Draft/ Folders and use it's default. Since i use IMAP, i will choose to save Sent in the IMAP Sent folder not the local filesystem one.\n- I really don't understand why the default config, come a filesystem folder inbox and others .. think about people that dont use this kind of stuffs (IMAP again). \n- The thread view isn't good as mutt .. \n- And config can drive me crazy to find the right option\n- Why i can't set my custom headers too display in message view (perhaps i need to display my Spam filtering headers)\n- How could i set another browser than konqui ?\n- Is there any plans to add the filter (subject etc etc) feature found in evolution or mozilla ? (in the main view)\n- Any plans to support IMAP iddle ? (i don't test the CVS for a while)\n\n\nKmail is a my favorite mail reader. I really enjoy the 'transport / identity' feature which give a lot of power, and this is damm clear (compare to others). I think Kmail can be used by advanced users, need again some work to be an advanced mailer. \n\nKeep it on going ! Please !"
    author: "Jkx"
  - subject: "Re: remarks on Kmail"
    date: 2004-07-10
    body: "- It doesn't permit to set the Sent/Draft/ Folders and use it's default. Since i use IMAP, i will choose to save Sent in the IMAP Sent folder not the local filesystem one.\n\nYes it does:\n\nSettings -> Configure Kmail -> Identities -> Choose identity -> Modify -> Advanced -> Adjust Sent-mail/Draft Folder.\n\n - I really don't understand why the default config, come a filesystem folder inbox and others .. think about people that dont use this kind of stuffs (IMAP again).\n\nPersonally, I find it useful to have a local mail store, just some more places to store my important mail. Submit a wish on http://bugs.kde.org\n\n - The thread view isn't good as mutt ..\n\nWhats wrong with it? Perhaps submit a wish/bug about it.\n\n - And config can drive me crazy to find the right option\n\n?????\n\n - Why i can't set my custom headers too display in message view (perhaps i need to display my Spam filtering headers)\n\nTake a look at this bug and vote for it:\nhttp://bugs.kde.org/show_bug.cgi?id=16270\n\n - How could i set another browser than konqui?\n\nI don't have a way to do that, perhaps submit a wish/bug about the \"Component Chooser\" in control center to let people choose their own web browsers? Perhaps you could work out some hack with a shell script but thats not very nice.\n\n - Is there any plans to add the filter (subject etc etc) feature found in evolution or mozilla ? (in the main view)\n\nI'm not sure exactly what your asking here, but if your talking about IMAP filtering, KMail 1.5.9 (KDE 3.1 i think) support sieve. If your mail server doesnt support sieve, this is the bug your after:\n\nhttp://bugs.kde.org/show_bug.cgi?id=50997\n\n - Any plans to support IMAP iddle ? (i don't test the CVS for a while)\n \nLook/Vote at/for this bug:\nhttp://bugs.kde.org/show_bug.cgi?id=67504\n \nThe major point being is that this is not the place to put this stuff, Developers rarely look here and the best way to get what you want to see in the product is to file a bug.\n\nCheers,\nChris."
    author: "Chris Smith"
  - subject: "Re: remarks on Kmail"
    date: 2004-07-10
    body: "> - And config can drive me crazy to find the right option\n\nIf you have suggestions for improvement then file a wish/bug report via Help->Report Bug\n\n> - Why i can't set my custom headers too display in message view (perhaps i need to display my Spam filtering headers)\n\nBecause for an unknown reason the person who was working on this didn't finish the work.\n\n> - How could i set another browser than konqui ?\n\nRead the FAQ: http://docs.kde.org/en/3.2/kdepim/kmail/faq.html#id2898200\n\n> - Is there any plans to add the filter (subject etc etc) feature found in evolution or mozilla ? (in the main view)\n\nI guess you are talking about the quick search which is one of the new features of KMail 1.7 (KDE 3.3)."
    author: "Ingo Kl\u00f6cker"
  - subject: "I've been using KMail for years"
    date: 2004-07-10
    body: "... and I don't have plans to change it. I love how it evolves (funny, huh?!) and the few features I'm missing now seems to come with KDE 3.3.\nI know more people that use it and they love the kontact integration, they love that they can *CHOSE* to run it as a full featured PIM suit or as a standard mail client, I love that too.\n--\nGo KMail's dev, go! :D"
    author: "Pupeno"
  - subject: "Re: I've been using KMail for years"
    date: 2004-07-10
    body: "Yes, the Kontact integration is quite a nice bonus.\n\nI can't wait to see the next versions of both these apps... Kontact still needs some polishing, but it's off to a good start."
    author: "Mikhail Capone"
  - subject: "External Editor?"
    date: 2004-07-10
    body: "All versions I've seen of kmail so far have not had external editor support.\n\nIs it possible either to call an external editor (gui vim) or perhaps utilize\nkvim within kmail in the latest version?"
    author: "Laure Mars"
  - subject: "Re: External Editor?"
    date: 2004-07-10
    body: "KMail has support for an external editor since ages (at least since KDE 2.0):\n\nhttp://docs.kde.org/en/3.2/kdepim/kmail/configure-composer.html#configure-composer-general\n\nSome versions ago someone wrote a patch to use kvim in KMail. I have no idea whether the patch has been updated to work with the current version of KMail."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: External Editor?"
    date: 2004-07-11
    body: "KDE 3.3 has Kvim part - so pretty much anywhere in KDE where a KDE part can be used to edit text, vim can be used. And most definitely within KMail.\n\nOsho"
    author: "Osho"
  - subject: "Re: External Editor?"
    date: 2004-07-12
    body: "You can try kyzis (http://www.yzis.org/) - a vim-like editor for kde. You will have to set it as the default editor in kcontrol."
    author: "krutsy the klown"
  - subject: "Re: External Editor?"
    date: 2004-07-13
    body: "Dude, this has been a feature in Kmail for ever.  "
    author: "brockers"
  - subject: "Re: External Editor?"
    date: 2005-02-21
    body: "What about embedded kvim in kmail ? I heard it was done in 3.2 but still I'm using 3.3 and don't find it done :-(\n\nRegards,\n\nrrs"
    author: "Ritesh Raj Sarraf"
  - subject: "Thunderbird -> KMail"
    date: 2004-07-11
    body: "Is it possible to import mails from Thunderbird into KMail?"
    author: "Apollo Creed"
  - subject: "Re: Thunderbird -> KMail"
    date: 2004-09-21
    body: "http://kmail.kde.org/unsupported/mozilla2kmail.pl\n\nJust copy it in your mozilla maildir (~/.mozilla/default/<some code>/Mail/<your mailbox>) and run it.\n\nHope, this will help you.\n\nSebastian"
    author: "Sebastian Roennau"
  - subject: "Re: Thunderbird -> KMail"
    date: 2005-02-10
    body: "I use Thunderbird from WinXP and Mandrake - accesses the same Mail folder. Is there a way to connect kMail and use this instead of Thunderbird while I\u00b4m booted to Linux?"
    author: "FlakJakk"
  - subject: "Re: Thunderbird -> KMail"
    date: 2005-02-11
    body: "Great script!\n\nFirst kmail crashed on me, when I tried to import the second Thunderbird Profile in a row (I had first renamed the imported folder). It might have been, because there was still an empty Mozilla file after the rename, or it might have been because there were too many mbox folders around.\n\nAnyway, I removed the empty ~/Mail/Mozilla file and moved all the imported messages into the corresponding kmail Maildir folders. After that I didn't have any trouble importing the second thunderbird directory tree."
    author: "Peter Shaw"
  - subject: "Re: Thunderbird -> KMail"
    date: 2006-01-08
    body: "Tip: Compact Folders first, and it works like a charm.\n\nNow to see how KMail / spamassassin works.."
    author: "David Hunter"
  - subject: "Re: Thunderbird -> KMail"
    date: 2005-01-08
    body: "Works fine, thanks."
    author: "M:ke"
  - subject: "Re: Thunderbird -> KMail"
    date: 2006-02-21
    body: "kmailcvt has a module for tbird now"
    author: "Aniruddha Shankar"
  - subject: "Re: Thunderbird -> KMail"
    date: 2006-08-19
    body: "Does this script work for Windows T'bird installations -> Linux KMail?  long ago, i tired of Outlook and have been using Thunderbird.  Now i'm totally tired of windows altogether.\n\nthanks."
    author: "MJ Klein"
  - subject: "mh support"
    date: 2004-07-12
    body: "I tried KMail 3.2.1 and decided not to use it because it doesn't use mh mail folders.  Instead, KMail wants to copy them into its own collection of folders.  That means that I can't use both mh and KMail on the same collection of e-mail and expect changes I make with mh to be known to KMail and vice versa."
    author: "David Talmage"
  - subject: "Re: mh support"
    date: 2004-07-21
    body: "Use an IMAP account and set up both kmail and mh to use it. Not?"
    author: "Anonymous Coward"
---
As part of its <a href="http://lwn.net/Articles/grumpy-editor/">Grumpy Editor series</a>, <a href="http://lwn.net/">LWN.net</a> looks at <a href="http://lwn.net/Articles/91308/">graphical mail clients</a>. <a href="http://kmail.kde.org/">KMail</a> is titled the most configurable and flexible graphical email client. Criticism includes missing features which can be already found in <a href="http://dot.kde.org/1089232721/">KDE 3.3 Beta 1</a> like HTML composing and better usage of external spam filters. In summary it's found to be one of the best mail clients available.
<!--break-->
