---
title: "Report: KDE at FOSDEM Brussels, 2004"
date:    2004-02-25
authors:
  - "fmous"
slug:    report-kde-fosdem-brussels-2004
comments:
  - subject: "Pardon my bad english"
    date: 2004-02-25
    body: "Scince I am not a native english speking person I dinden't understand this sentence \"while Keith Packard's \"XFree86\" talk was standing room only!\".\nDoes it man there was no one else in the Romm listening or The whole Place was crowded so you only could stand around to listen?\nthanks,\nKarl\n\n"
    author: "Karl"
  - subject: "Re: Pardon my bad english"
    date: 2004-02-25
    body: "The second -- the speech was very well attended. "
    author: "John Alamo"
  - subject: "Re: Pardon my bad english"
    date: 2004-02-25
    body: "So were there anything outside new license issue? ;-)\n\nHopefully at least someone brought up moving the xfree86 to GPL... ?\n\nPs. How compatible/portable Qt would be to the freedesktop.org version (using OpenGL renderer)?"
    author: "Nobody"
  - subject: "Re: Pardon my bad english"
    date: 2004-02-26
    body: "At least parts of it should be LGPL."
    author: "CE"
  - subject: "Re: Pardon my bad english"
    date: 2004-02-26
    body: "Well, the freedesktop.org version is a regular X server. Qt/KDE already works just fine on the fd.o server. Now to take full advantage of the new server, they'd need a new Qt painter that could do anti-aliased drawing via Cairo.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Pardon my bad english"
    date: 2004-02-25
    body: "Karl? German? I think you can translate it to german like that:\nW\u00e4hrend (oder \"Bei\"?) Keith Packards \"XFree86\" Vortrag waren nur Stehpl\u00e4tze verf\u00fcgbar."
    author: "panzi"
  - subject: "KTurtle presentation"
    date: 2004-02-25
    body: "If you were not at FOSDEM and would like to see KTurtle presentation, it's online here:\nhttp://edu.kde.org/kturtle/presentation/index.php\nGreat job, Cies!!!"
    author: "annma"
  - subject: "Re: KTurtle presentation"
    date: 2004-02-25
    body: "Scott's C++ pitfalls presentation can also be found online, here: \nhttp://developer.kde.org/~wheeler/cpp-pitfalls.html\nGreat job, Scott (:"
    author: "je4d"
  - subject: "Re: KTurtle presentation"
    date: 2004-02-25
    body: "Actually it was kind of the other way around -- it was like, \"Oh, well, I made this webpage last week -- I guess I'll do a talk on that...\"  :-)"
    author: "Scott Wheeler"
  - subject: "Re: KTurtle presentation"
    date: 2004-02-26
    body: "Found a typo, \"decleration\". \n(Just do a search :))"
    author: "Source"
  - subject: "Re: KTurtle presentation"
    date: 2004-02-26
    body: "Actually annma put that presentation on the web :) I wasnt even aware of it beeing webbed allready... "
    author: "cies"
  - subject: "Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "Why did Ketih Packard talk about Xfree, and not his own (well, FreeDesktop.org's) Xserver?"
    author: "janne"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "He spoke on:\n- XFree86 licening issues\n- f.d.o X-server, and the future of it\n- he showed some things he had working allready\n- and he told about things that were not even created\n\nIt was a good, funny talk."
    author: "cies"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "* Keith has his own hack server called Kdrive where he prototypes his work.\n\n* Kdrive is for research and is not a replacement.\n\n* Freedesktop.org has a copy of the last XFree86 under the 1.0 license.\n\n* If XFree86 don't drop the 1.1 license, most people and distros will continue hacking on the version at Freedesktop.org using the old 1.0 license.\n\nDoes that answer your question?\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "\"Does that answer your question?\"\n\nUh, no. In case you didn't know, I was talking about this:\n\nhttp://www.freedesktop.org/Software/xserver\n\nXserver is based on Kdrive, but it improves and builds upon it. It's not related to Xfree (apart from that they are both X-servers). While Keiths Xserver is not a drop-in replacement for Xfree yet (it's still relatively young), it is improving and there are people using it instead of Xfree RIGHT NOW. There will be an official release of Xserver sometime in the future.\n\nI'm not that interested in Xfree, Xserver seems to be where the action is. And since Keith Packard is it's lead architecht, I would have guessed that he would talk about it instead of Xfree."
    author: "janne"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "This link here tries to explain the recent history:\n\nhttp://www.osnews.com/story.php?news_id=6157\n\n...but it doesn't answer the question about \"X Server\".\n\nWatching the presentation, I had the big impression that Keith was planning on adding these extensions to the XFree86 code base.\n\nTo be honest, I don't know where X Server fits into the picture.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "As I understand it, kdrive is a small X server for handhelds.\n\nxserver is Keithp's research prototype. It is not intended as a mainstream X server, it is just to experiment with composition managers, transparency, et al.\n\n- je4d"
    author: "je4d"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "\"Watching the presentation, I had the big impression that Keith was planning on adding these extensions to the XFree86 code base.\"\n\nHe's no longer involved in Xfree-developement (he was kicked out), so I don't see how he could add to Xfree codebase."
    author: "janne"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "Maybe this means add to XFree 1.0 licensed codebase?"
    author: "Jos\u00e9"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-26
    body: "Forking XFree 4.3rc2 is easy."
    author: "Anonymous"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-27
    body: "Why should he fork Xfree, when he already has Xserver, that does many thing Xfree does not do? Xfree is a monstrocity that could use a replacement.\n\nTrue, Xserver is not as mature as Xfree is, but that is to be expected. It is a relatively young project. But that does not mean that it should be scrapped!"
    author: "janne"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-27
    body: "XFree86 is established and has more hardware drivers."
    author: "Anonymous"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-02-27
    body: "Windows is more established and it has more hardware-drivers, maybe we should all concentrate on improving Windows instead of concentrating on Linux?\n\nJust because something is \"established\" is not a good enough reason to think about replacement. Xfree may be \"established\", and it shows! it's developement is stagnating. Without any competition, they have no reason to really improve."
    author: "janne"
  - subject: "Re: Why Xfree and not Xserver?"
    date: 2004-08-20
    body: "we cant concentrate on improving Windows\n\nwindows certainly has no competition, it's developement is stagnating."
    author: "chris"
---
A group of KDE hackers went to the <a href="http://www.fosdem.org/">FOSDEM event</a> to represent KDE. FOSDEM is a two-day summit were leading developers in the Free Software community come together. It is an event held by developers, for developers where you can attend many interesting presentations, mingle and hold discussions with other projects.



<!--break-->
<p>Some (KDE) people already arrived on Friday night and <A href="http://www.kde.nl/archief/Fosdem2004/0001.jpg">got together</A> 
in downtown Brussels. We had a nice time drinking beers like <A href="http://www.duvel.com/uk/Products.htm">Maredsous and Duvel</A>
in a pub called Manneken Pis, close to Grand Place and across from the 
<A href="http://en.wikipedia.org/wiki/Manneken_Pis">famous statue</A>.</p>
 
<p>Saturday 10:30, FOSDEM 2004 kicked of with a talk from Tim O'Reilly. Not completely back in the land of the living you can 
find those <A href="http://www.kde.nl/archief/Fosdem2004/0004.jpg">KDE hackers</A> 
(left to right: Cies Breijs,  Simon Edwards, Adriaan de Groot, Otto Brugman) attending that talk. After the O'Reilly talk, 
Richard Stallman held a talk about the Free Software Foundation.</p>

<p>Every year the event follows some kind of theme and this year it is accessibility which been chosen as a main
theme and so some of the tracks were dedicated to accessibility software. FOSDEM 2004 had a friendly and inspiring atmosphere, 
where hackers <A href="http://www.kde.nl/archief/Fosdem2004/0007.jpg">moved from one room to another</A> to attend the talks. 
You could find all kind of developers staffing their booth and telling you about their project. </p>

<p>KDE's main speaker, Gunnar Schmi Dt from the <A href="http://accessibility.kde.org/">KDE Accessibility Project</A>, 
told about the progress accessibility is making in KDE, the new accessibility features in KDE 3.2 and the upcoming 
<A href="http://accessibility.kde.org/developer/bridge.php">bridge between Qt and AT-SPI</A>. Very good contact 
was made with the <A href="http://developer.gnome.org/projects/gap/">GNOME Accessibility Project</A> and 
other accessibility authors and will very likely lead to better inter-operatibility in the future.</p>

<p>Furthermore we noticed a great interest in "Linux on the Desktop".
Robert Love's "Kernel and the Desktop" talk was very well attended, while Keith Packard's "XFree86" talk 
was standing room only! </p>

<p>Speaking of talks, some KDE hackers also <A href="http://www.kde.nl/archief/Fosdem2004/0014.jpg">gave talks</A> 
in our Developers' Room about the following topics:</p>

<ul>
  <li>Basic KDE technology by Adriaan de Groot</li>
  <li><A href="http://www.kde.nl/archief/Fosdem2004/0042.jpg">C++ Pitfalls by Scott Wheeler</A></li>
  <li><A href="http://www.kde.nl/archief/Fosdem2004/0016.jpg">KConfigXT by Adriaan de Groot</A></li>
  <li><A href="http://www.kde.nl/archief/Fosdem2004/0041.jpg">KTurtle by Cies Breijs</A></li>      
</ul>


<p>KDE hackers invited Daniel Veillard to do a talk on <A href="http://xmlsoft.org/XSLT/">libxslt</A> in the 
KDE Developers' Room. The network connectivity in the KDE Developers' Room was well enjoyed by several Gnomies 
who sneaked in. :-) </p>

<p>Please check out <A href="http://www.kde.nl/archief/Fosdem2004/images.html">our photos</A>, because 
<A href="http://www.kde.nl/archief/Fosdem2004/0052.jpg">we spotted a new convert</A>. :-)</p>

<p><font size="-1">Thanks to Simon Edwards, Olaf Jan Schmidt and Phillipe Schroll for their contributions to this small write-up.</font></p>

