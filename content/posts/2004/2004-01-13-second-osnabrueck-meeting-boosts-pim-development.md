---
title: "Second Osnabrueck Meeting Boosts PIM Development"
date:    2004-01-13
authors:
  - "dmolkentin"
slug:    second-osnabrueck-meeting-boosts-pim-development
comments:
  - subject: "WebDAV support?"
    date: 2004-01-13
    body: "My employer uses MS Exchange (which seems to be very popular at least in the USA). Unfortunately I am unable to schedule meetings with KOrganizer in KDE 3.1.4 (although I have been able to download my calender from the Exchange serve! Great Job!), nor have I been able to use The Exchange Server's Global Address Book. \n\nDoes anyone know the state Exchange/webDAV support in 3.2, is anything being planned for 3.3? Currently Evolution + Connector seem to be the only option.\n\nThanks. You guys are doing a great job!"
    author: "dapawn"
  - subject: "Re: WebDAV support?"
    date: 2004-01-13
    body: "for what I've read, there is partial Exchange support, but it will be disabled in 3.2. coming for 3.3, probably."
    author: "david"
  - subject: "HTML mail support!"
    date: 2004-01-13
    body: "Yaaay!  I can't wait to finally ditch Mozilla mail.  I don't care if it's off by default, as long as it's an option!\n\nAlso: I'm assuming it'll send multipart/alternative with plaintext.  Much as I like HTML mail, HTML mail without a plaintext fallback is evil."
    author: "ac"
  - subject: "Re: HTML mail support!"
    date: 2004-01-13
    body: "Your assumption is correct. KMail will never send HTML only messages.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HTML mail support!"
    date: 2004-01-14
    body: "What about displaying incoming HTML messages with embedded images?\n\nKDE technology makes it reasonably easy, I think, I guess it's just a kioslave that ask Kmail for the MIMEd image in the message and gives it back to the KHTML viewer part...\n"
    author: "Julio Cesar Gazquez"
  - subject: "Re: HTML mail support!"
    date: 2004-01-14
    body: "It's on our todo list. It's more likely that we'll simply change the cid: URLs in the DOM tree to local file: URLs than that we'll solve this with a kioslave."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HTML mail support!"
    date: 2004-01-13
    body: "Will this feature use the HTML WYSIWYG editor being developped for Quanta (I don't remember its name... Kafka ?) ?"
    author: "Macolu"
  - subject: "Re: HTML mail support!"
    date: 2004-01-14
    body: "No. At least not at first. A new Komposer framework is in development which will allow to plug several editor parts into the composer."
    author: "Ingo Kl\u00f6cker"
  - subject: "Error in story."
    date: 2004-01-13
    body: "> Last year the big decision was to make KolabClient the successor to Kontact. \n\n   Do you not mean Kontact the successor to KolabClient? :-)"
    author: "George Staikos"
  - subject: "Re: Error in story."
    date: 2004-01-13
    body: "And maybe\n\n> invited the KDE PIM team to a meeting\n\nshould have been some of the European members of the KDE PIM team.\n"
    author: "Don"
  - subject: "Re: Error in story."
    date: 2004-01-13
    body: "Note that we had to pay all our expenses (travel, accommodation) ourselves. Also note that members of the KDE PIM team came up with the idea for this meeting and just asked Intevation to host it (because of the good experiences of the first meeting in Osnabr\u00fcck). So you could say that we invited ourselves. ;-) Intevation generously agreed to host the meeting and I take this opportunity to thank them for this.\n\nSo it's not Intevation that excluded anyone from the meeting. Instead the organizers of the meeting (maybe falsely) assumed that non-European members of the KDE PIM team wouldn't have had the resources (money, vacation from work) to participate.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Error in story."
    date: 2004-01-14
    body: "The kdepim team of course consists of many more people than those present at the Osnabrueck meeting and we actually did ask some key non-european kdepim developers if they would like and be able to join us.\n\nOne of the great things about KDE is that it is a working virtual community which brings together people from all over the world. As nice and useful personal meetings are, we shouldn't forget that this is only one small piece of what makes KDE successful.\n"
    author: "Cornelius Schumacher"
  - subject: "Re: Error in story."
    date: 2004-01-14
    body: "Sounds reasonable to me."
    author: "Joe"
  - subject: "Re: Error in story."
    date: 2004-01-14
    body: "Even more precise: \nKontact will be enabled to act as a Kolab Client."
    author: "Bernhard Reiter"
  - subject: "Version names"
    date: 2004-01-13
    body: "Will the separate PIM release really be called KDE PIM 3.3?  IMHO that's a bad idea.  If the PIM apps will still be released with KDE, then when KDE 3.3 comes out there could be version confusion (\"Are you using KDE PIM 3.3 or KDE 3.3 PIM?\").  If they will no longer be a part of normal KDE releases, then they should take their own version numbers and not try to stay in sync with KDE's."
    author: "Spy Hunter"
  - subject: "Re: Version names"
    date: 2004-01-13
    body: "It's unfortunately the best of the bad cases. If we chose anything else, it would be a problem to later decide we would come back into the KDE release cycle. It's also possible that we decide to keep releasing kdepim separately from the other KDE parts.\n\nIt's quite uncertain wether there will be a KDE 3.3 release or it will be 4.0, so it might not be a problem at all. Should there be a KDE 3.3 release in, say, august, kdepim will most likely not be released with it.\n\nBo."
    author: "Bo Thorsen"
  - subject: "Re: Version names"
    date: 2004-01-13
    body: "Why not merge the KDEPIM 3.3 stuff into kde 3.2.1 and then you don't have to call it anything?\n\nThe naming issue will be very confusing.  Waiting a month or 2 won't kill anyone and will allow it to settle in."
    author: "put off release until kde 3.2.1"
  - subject: "Re: Version names"
    date: 2004-01-13
    body: "Read http://pim.kde.org/development/meetings/20040102/release.php (especially \"Versioning scheme for separate kdepim release\").\n\nAlso we are talking about 4-5 month, not about 1 or 2 month.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Version names"
    date: 2004-01-13
    body: "Why not 3.2b ?\n"
    author: "Pedro Alves"
  - subject: "Re: Version names"
    date: 2004-01-13
    body: "> Also we are talking about 4-5 month, not about 1 or 2 month.\n\nSo why not wait some more and release it with kde 3.3?\n\nbtw, a shorter release-cycle for kde, about 6-9 month, would be nice!\nOne year is soooooooo long..."
    author: "ac"
  - subject: "TOTALLY AGREE"
    date: 2004-01-14
    body: "KE should have a release cycle no longer than 6-7 months for intermediate releases. Only big point releases like 4.0 should take a year."
    author: "Alex"
  - subject: "Re: Version names"
    date: 2004-01-14
    body: "We don't want to release in 6-9 months and KDE 3.2 is still not released after almost 12 months. Currently nobody knows when KDE 3.3 will be released (if ever because maybe we directly go for KDE 4.0).\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Version names"
    date: 2004-01-13
    body: "Then why not 3.2.01?  Then when KDE 3.2.1 is released kdepim can still release 3.2.1 which will just be a more stable 3.2.01 and wont confuse people.  Besides there is nothing more fun then a very minor version bump that has major functionality.  It is like winning free stuff for a user."
    author: "Benjamin Meyer"
  - subject: "Re: Version names"
    date: 2004-01-14
    body: "You mean winning free bugs, don't you?"
    author: "Cornelius Schumacher"
  - subject: "Re: Version names"
    date: 2004-01-14
    body: "My opinion is that KDE 3.3 will turn into KDE 4 like you suggest, so I agree it probably won't be a problem.  It could still be confusing anyway, but I guess your reasoning makes sense.  As long as people realize it could be a problem, that's fine."
    author: "Spy Hunter"
  - subject: "Nifty!"
    date: 2004-01-13
    body: "I'm glad knode will be integrated.  I would love to be able to mix and match my mail folders and news folders.  Even if this is not in this release, it will likely push it towards that capability.\n\nI just got an iPaq.  I've been kicking around trying to get it to sync (I don't own a Windows machine - just various *nix).  Raki, the KDE sync tool for SynCE almost works, but it would be ideal if it was built right into KDE PIM.  Any idea if there has been any work on syncing Windows PocketPCs?\n\nIf there hasn't, I'm likely going to be installing Linux on it soon...  :)  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Nifty! -> Raki"
    date: 2004-02-19
    body: "I'm also a owner of a Windows Mobile 2003 pocket pc (Dell Axim X5).\n\nThere is only one f#cking need, to keep windows xp running on my system!\n\nI need MS Outlook to sync my:\nEmails,\nCalender,\nTodo\nand Notes\n\nwith Palm.\nOkay, I also use the mobile favorites (which are setup via Internet Explorere) and  Avantgo (a port of avantgo would also be nice!)\n\nIs there any plan or at least motivations, to integrate Pocket PCs also into KDEPIM ?"
    author: "Rene"
  - subject: "Re: Nifty!"
    date: 2005-11-16
    body: "same here, the only reason i have a windoze box now is to sync my pocketpc that i use all day long at work :)\ni've yet to get it to sync with my fedora core 4 box\n- jim \nif anyone succeeds, please email me!"
    author: "jim"
  - subject: "About KPilot"
    date: 2004-01-13
    body: "Can I upload my KOrganizer schedule/calender to KPilot to my Palm and vice versa?"
    author: "anonymous"
  - subject: "Re: About KPilot"
    date: 2004-01-13
    body: "Yes, with kpilot, were you can active and configure the KO rganizer Calendar and Todo conduits. It's working fine with me."
    author: "jukabazooka"
  - subject: "wilhelmshafen"
    date: 2004-01-13
    body: "I got to know that a parallel event, the LinuxInfoTage(?) in Wilhelmshafen will be videoconferencing with Chemnitz. "
    author: "gerd"
  - subject: "Different is the Same ?"
    date: 2004-01-13
    body: "Can someone tell me the differences between Kolab & Kroupware ?\nWhat about Kontact & KDEPIM ?\n\nThese apps look the same to me, but have different websites. If they ARE the same, why have different name ?"
    author: "Onion"
  - subject: "Re: Different is the Same ?"
    date: 2004-01-14
    body: "Kolab is the new name for Kroupware since it was criticized before. KDEPIM is the respective module in KDE CVS which includes all the PIM applications like KMail, Korganizer etc. and Kontact. Kontact is a shell integrating many of the stand alone PIM applications."
    author: "Datschge"
  - subject: "Re: Different is the Same ?"
    date: 2004-01-14
    body: "Kroupware was the name for work done under a (now finished) contract\nby Intevation, Erfrakon and Klar\u00e4lvdalens Datakonsult.\nKolab is the Free Software project which came out of it.\nIt has a server and a client component.\n\nSome KDE's PIM applications can act as a Kolab Client."
    author: "Bernhard Reiter"
  - subject: "Re: Different is the Same ?"
    date: 2004-01-14
    body: "Kolab is the groupware server. Kroupware was the name of the project to write a KDE client for Kolab based on the existing applications from the kdepim CVS module (KMail, KOrganizer, KAddressBook). The result of this project is in the kroupware_branch in CVS. It never got released as part of KDE.\n\nWith KDE 3.2 kdepim will include Kontact which integrates KMail, KOrganizer, KAddressBook and more into a single application. In the next release Kontact will include the Kolab client functionality.\n"
    author: "Cornelius Schumacher"
  - subject: "Re: Different is the Same ?"
    date: 2004-01-14
    body: "Kolab is the name of the Free Software project.\nThere are implementations of the server and clients.\nKontact will become the KDE Kolab Client."
    author: "Bernhard Reiter"
  - subject: "Crystal SVG 1.0 is out on kde-look"
    date: 2004-01-15
    body: "Get it while its HOT :)\nReally liked the new KOffice icons"
    author: "PBY"
---
<a href="http://dot.kde.org/1043346607/">Last year</a> in January, <a href="http://www.intevation.de">Intevation</a> invited the <a href="http://pim.kde.org">KDE PIM team </a>to a meeting for a hackfest to their offices at <a href="http://www.osnabrueck.de">Osnabrueck</a>. This was such a great success, that everyone was glad Intevation invited several European KDE PIM team members a second time exactly one year later to polish and fix up Kontact and its components for the upcoming KDE 3.2 release. <a href="http://pim.kde.org/development/meetings/20030103/group.php">Last year</a> the big decision was to make Kontact the successor to KolabClient. <a href="http://pim.kde.org/development/meetings/20040102/group.php">This year</a> the plan was to make a <a href="http://developer.kde.org/development-versions/kdepim-3.3-release-plan.html">a roadmap for future KDE-PIM Development</a>. The developers took the opportunity to discuss complicated issues in detail and sit together for brainstorming or in order to fight evil bugs.

<!--break-->
<br>
Meanwhile, Cies Breijs revamped the <a href="http://www.kontact.org">Kontact homepage</a>. It's now back with a new design and more content and will be updated more frequently now. Thanks a lot Cies! Other thanks are due to Bernhard Reiter and Jan-Oliver Wagner and the rest of the Intevation 
staff for hosting the meeting.
<p>
<a href="http://www.kontact.org">Kontact</a> most of all has seen a lot of polishing to make sure it shines in its initial release. Several bugs have been tackled, KNode is now a true Kontact part, Kontact replaces all PIM applications seamlessly when running. Furthermore work has been put into speed improvements with ICal and IMAP and stability. GUI consistency fixes were not, of course, forgotten.
<p>
In order to be able to commit new features and ideas that arose during the meeting, osnabueck_branch was created. This branch will be merged back to to main development line ("HEAD") after the freeze for 3.2 is over. Improvements in this branch include the port of many config dialogs to KConfigXT and KCModules to allow for a smoother integration of config options in the next version of Kontact. KPilot integrates into Kontact and wishlist items have been implemented. For those interested in details, the developers <a href="http://pim.kde.org/development/meetings/20040102/individual.php">summed up</a> their activities during the meeting.
<p>
The developers decided on a seperate release for the KDE PIM applications. Cornelius Schumacher of <a href="http://pim.kde.org/components/korganizer.php">KOrganizer</a> and KitchenSync fame will act as release coordinator for KDE PIM 3.3. The developers agreed on a set of features (HTML composing, KitchenSync integration, Kolab and eGroupware Integration and Client-side IMAP filtering) as well as on a <a href="http://developer.kde.org/development-versions/kdepim-3.3-release-plan.html">release schedule</a>. Of course, features not listed on the release plan can also be added if they are mature enough.
<p>
To speed up compiling, Stephan Kulow and Michael Matz from <a href="http://www.suse.com">SUSE</a> generously provided an early version of <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/icecream/icecream/README?rev=1.2&content-type=text/x-cvsweb-markup">icecream</a>, a <a href="http://distcc.samba.org">distcc</a>-based tool for centralized distributed compiling which proved to be really helpful.
<p>
The KDE PIM Team will meet again in March at the <a href="http://www.tu-chemnitz.de/linux/tag">Chemnitzer Linux-Tag 2004</a> for a meeting with developers of groupware server projects to discuss and implement Kontact integration to enable you to make Kontact the best-connected groupware client available!
