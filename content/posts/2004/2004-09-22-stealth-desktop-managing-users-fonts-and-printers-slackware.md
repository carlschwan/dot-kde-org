---
title: "The Stealth Desktop: Managing Users, Fonts and Printers in Slackware"
date:    2004-09-22
authors:
  - "esanchez"
slug:    stealth-desktop-managing-users-fonts-and-printers-slackware
comments:
  - subject: "What stealth means"
    date: 2004-09-22
    body: " The Stealth Desktop is so stealthy that noone seen it or use it ;-)\n\nHow many paid or volunteer developper use Slackware and are working on KDE ?"
    author: "Anonymous forward"
  - subject: "Re: What stealth means"
    date: 2004-09-22
    body: "Slackware rules ! :-)\nI use it and develop for KDE (ok, not much time in the last months)\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: What stealth means"
    date: 2004-09-22
    body: "Just think of this... what platform was used to perform the KDE usability study recently presented here on the Dot? ;)"
    author: "Eduardo Sanchez"
  - subject: "Re: What stealth means"
    date: 2004-09-22
    body: "wow , Slackware as two kde users , lol ;-)"
    author: "Anonymous forward"
  - subject: "Re: What stealth means"
    date: 2004-09-23
    body: "I switched to Slackware one week ago :-) Really nice: simple and sensible."
    author: "wilbert"
  - subject: "Re: What stealth means"
    date: 2004-09-24
    body: "If you'll ever try debian, you'll be sold. I was."
    author: "ac"
  - subject: "Re: What stealth means"
    date: 2004-09-25
    body: "Hey, some people don't trust Debian.  I use it, and I don't.  I just happen to trust it more than I trust other distributionss, and I happen to like to have some automation. ;-D"
    author: "regeya"
---
In the <A HREF="http://www.ofb.biz/modules.php?name=News&amp;file=article&amp;sid=327">third installment</a> of the Stealth Desktop series about Slackware Linux, Eduardo Sánchez builds upon the previous steps of <A HREF="http://www.ofb.biz/modules.php?name=News&amp;file=article&amp;sid=315">Part I</a> and <A HREF="http://www.ofb.biz/modules.php?name=News&amp;file=article&amp;sid=317">Part II</a>. Continuing where those parts left off, he introduces the subjects of user, font and printer management in Slackware showing how KDE can help on these tasks.


<!--break-->
