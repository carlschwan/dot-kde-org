---
title: "Crystal Sources United!"
date:    2004-04-16
authors:
  - "awessels"
slug:    crystal-sources-united
comments:
  - subject: "One Umbrella For All"
    date: 2004-04-15
    body: "It's nice to have all the crystal icons in one place. I searched for hours trying to find the SVG version in February. Now I finally have it =) Though, I think I will stick with the PNGs, tehy don';t render all that great when their tiny.\n\nAlso, I lvoe the new design for http://wiki.kdenews.org"
    author: "Alex"
  - subject: "Re: One Umbrella For All"
    date: 2004-04-16
    body: "I'm very pleased to see that Luci finally got around to re-design the wiki! It looks great now, much more usable than before.\n\nThanks Luci :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: One Umbrella For All"
    date: 2004-04-16
    body: "Yes the Wiki looks very sharp these days. \n\nThanks Luci\n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Interesting choice of headlines"
    date: 2004-04-16
    body: "Interesting choice of headlines."
    author: "krackhead"
  - subject: "Re: Interesting choice of headlines"
    date: 2004-04-16
    body: "Nothing to loose but your chains."
    author: "Ian Monroe"
  - subject: "Status of SVG in KDE?"
    date: 2004-04-16
    body: "What's the status on SVG-rendering in KDE? I know that Konqueror can display SVG-pictures, but how about using SVG elsewhere (everywhere!) in KDE? SVG-buttons, SVG-icons etc. etc."
    author: "janne"
  - subject: "Re: Status of SVG in KDE?"
    date: 2004-04-16
    body: "If I point my Konqueror browser to a .svg file (using KDE3.2.2 & Konqeuror 3.2.2 (Debian unstable seems to have been very quick with that)) I see no picture rendered at all.\n\nAm I missing something?"
    author: "ac"
  - subject: "Re: Status of SVG in KDE?"
    date: 2004-04-16
    body: "Yes. Install ksvg (it's in kdegraphics)"
    author: "Sad Eagle"
  - subject: "Re: Status of SVG in KDE?"
    date: 2004-04-16
    body: "Both where installed, but the embedding was set to KHTML in stead of ksvg (default settings on Debian unstable here). But yes it renders now, but slower then in Sodipodi and there seem to be less colours then in Sodipodi. Sorry for going off-topic here."
    author: "ac"
  - subject: "Re: Status of SVG in KDE?"
    date: 2004-04-16
    body: "nevermind what I said.. it works great, but it does seem to render kind of slow"
    author: "ac"
  - subject: "Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "As far as I'm concerned, KDE (I'm still running 3.1.5 at office and 3.2 at home) doesn't yet know that USB storage devices are not the same as HDDs. Although it could be probably introduced without much hassle -- like, count /dev/hd* as HDDs and /dev/sd* as USB storage devices, and show the appropriate icons -- this is not a good idea to distinguish them like that.\n\nSo basically there's no USB storage device icon in KDE at all. Well, I didn't like that, because I use CompactFlash/SD/MMC cards to carry data around a lot, so I have created an icon for an external card reader and fitted it onto my desktop. Tried to keep them consistent with general Crystal look, too. You can see both mounted and unmounted icons for CompactFlash at http://www.livejournal.com/community/ru_icons/113102.html\n\nThe question is whether these icons can be included into the Crystal package or not. I didn't even bother to put them onto kde-look.org..."
    author: "oujirou"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "Well, a external harddisk is also an USB storage device, as a flash memory, as a digital camera, as mp3 player...\n\nSo what is the ultimate icon for an USB storage device? Obviously there is no single ultimate icon, since it depends on the attached device."
    author: "Yaba"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "Wouldn't it be just great if KDE learned to distinguish between such devices so we could apply our creative skills and draw icons for them? :)"
    author: "oujirou"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "project Utopia, with its HAL+DBUS approach, will likely be the solution to this. it's being written, and it will (someday) work. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "For as far as I learnt: it works!\n\nIt only needs to get implemented by distributions, and...\n...by desktop environments.\n\nSomeone told me the the next (9.2, 10.0 or X) version of SuSE will use D-BUS and have a HAL. He also told me that it will likely be out later this year.\n\nI'm really looking forward to it :)\n\nCies."
    author: "Cies Breijs"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "Wow .. these icons look really really cool.  Please *do* post them at kde-look.org and I'm going to use them now I have also some CF-cards which I mount. I can assure them that some people will definitely wanna use them (like me)\n\nThanks for these icons .. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "No, at least on my machine /dev/sd[abcd] are SCSI harddrives, and definitely not removable USB gadgets. It's a bit unfortunate that the USB stuff shows up a SCSI disks as it causes some confusion.\n"
    author: "Chakie"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "This is exactly what I meant when I wrote that it would be a bad idea to implement device division like that.\n\nWe have two SCSI hard drives in one of the office servers, so I know. :)"
    author: "oujirou"
  - subject: "Re: Obvious, but unsupported icons?"
    date: 2004-04-16
    body: "Please send them to kde-artists@kde.org"
    author: "Ante Wessels"
  - subject: "Kernel icon?"
    date: 2004-04-16
    body: "Why is there no special icon for vmlinuz? When I point my cursor at the kernel, Konqueror says: \"Unknown\".\nAn x-ray of Tux? A picture of Linus?"
    author: "reihal"
  - subject: "Re: Kernel icon?"
    date: 2004-04-16
    body: "First thing you need is a MIME type (File Association) for that file.\n\nSo, first add something like:\n\n\tapplication/x-boot.image\n\tapplication/x-boot.map\n\tapplication/x-boot\n\nto the global file associations.  You can do it as root and copy them (check the file permissions).\n\nYou start them with: \"x-\" because they are your own extensions -- not registered MIME types.\n\nThen you can add whatever icons you want for these MIME types.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "264 icons in 1,6 Mb"
    date: 2004-04-16
    body: "..that's 264(!) great icons in only 1,6 Mb !!"
    author: "ac"
  - subject: "Re: 264 icons in 1,6 Mb"
    date: 2004-04-17
    body: "Yes, bzipped they are very small!"
    author: "Ante Wessels"
  - subject: "becomes kde then slower?"
    date: 2004-04-16
    body: "If I use SVG icons instead of PNGs?\n\nI really love KDE, especially 3.2 release. But looking always to competition I must determine, that Gnome 2.6 is really a bit faster (with an awful spastic filemanager \"feature\") than KDE 3.2. Human noticable faster, not only benchmark numbers. Not to much, but a little bit.\n\nThen, I installed IceWM+ROX2 combo and this really rox. I'm become a little bit anxiety if I click on an icon in Rox2 because it open's \"too fast\". It feels, a open window jump out of my monitor to me (sorry, can't describe it better in my poor english)\nDoesn't ever know how fast my PC can be (2.4ghz/1gb-ram/udma5)\n\nSo now my question: is it good for KDE if I use everywhere (where I can) SVG-Icons to be faster or should I stay with PNG's (and I really think it look's nice for me)??\n\nThanks for answers!\n"
    author: "anonymous"
  - subject: "Re: becomes kde then slower?"
    date: 2004-04-17
    body: "\"But looking always to competition I must determine, that Gnome 2.6 is really a bit faster (with an awful spastic filemanager \"feature\")\"\n\nYou can switch spatial Nautilus off and use it in standard \"browsing\" mode."
    author: "Janne"
  - subject: "License problems..."
    date: 2004-04-23
    body: "The lack of a clear license for these files is a real problem. The downloaded tar doesn't include one, and no clear statement of license exists.\n\nAny help?"
    author: "Soulhuntre"
  - subject: "Re: License problems..."
    date: 2004-04-24
    body: "LICENSE is the file to look for."
    author: "Ante Wessels"
  - subject: "Re: License problems..."
    date: 2004-04-24
    body: "Thanks, I finally found a version I can use by downloading the kdelibs tarball. No other version of the crystal SVG files I found had an included licence file."
    author: "Soulhuntre"
---
Sources for the Crystal icon set are everywhere. They are at many places in KDE's CVS, so many, it's hard to download them. Artists more skillful with sketchbooks than CVS, will be gladly surprised that Frans Englich wrote a script which collects them all, and that Philip Scott provided a high speed <A href="http://www-jcsu.jesus.cam.ac.uk/~pgs31/kdestuff/">server for the resulting zip</a>.


<!--break-->
<p>Crystal sources may be at your home. Of course, visionary Everaldo and over productive Torsten made most icons, still, other artists made contributions too. For whatever reason, some of these sources are missing. If you ever made a Crystal SVG icon, and see the source is not in this archive, please send the vector source to: kde-artists@kde.org. And of course, Everaldo too is interested in your Crystal icons: "Everaldo" &lt;everaldo@everaldo.com&gt;</p>

<p>Crystal sources are certainly to be found at the SUSE server: SVG sources not fully ready to commit to CVS, and even Everaldo's Illustrator files! See the <a href="http://wiki.kdenews.org/tiki-index.php?page=Icon+Guide">Icon Guide</a> for more information about this.</p>

<p>The sources are especially useful for artists, developers and for experimentation. Many icons, especially the smaller ones are hand-fixed after they are exported to pixels. Running the sources directly on your system will probably give poorer results than using the <a href="http://www.kde-look.org/content/show.php?content=8341">PNG version</a> of the Crystal SVG set released by Everaldo himself.</p>

<p>Crystal sources may pop up in your head. Crystal SVG is one of the most popular icon set around. Join this project, help to make it more complete, and know your work installed on computers all over the world! A good starting point is the Icon Guide.</p>

<p>Which icons are missing? See <a href="http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=&long_desc_type=allwordssubstr&long_desc=&product=artwork&component=general&version=unspecified&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bugidtype=include&bug_id=&votes=&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&order=Reuse+same+sort+as+last+time&cmdtype=doit&newqueryname=">the buglist</a>.
<br><br>
With such a wealth of sources, your destiny is... to be inspired!
<br><br>
Have fun!
</p>


