---
title: "KOffice 1.3 Released"
date:    2004-01-27
authors:
  - "wbastian"
slug:    koffice-13-released
comments:
  - subject: "Karbon14"
    date: 2004-01-27
    body: "Is there any active development on Karbon14?"
    author: "MaX"
  - subject: "Re: Karbon14"
    date: 2004-01-27
    body: "A bit, yes. The author of Karbon14 has been *very* busy with KSVG, I guess Karbon will get more attention in the furure."
    author: "Dik Takken"
  - subject: "Re: Karbon14"
    date: 2004-01-28
    body: "should mean karbon is well integrated with ksvg :)\n\nBut why is karbon in koffice and not in kdegraphics? Illogical surely: the bitmap editor kpaint is in graphics, not office, so why is the vector editor different?"
    author: "Peter Robins"
  - subject: "Re: Karbon14"
    date: 2004-01-28
    body: "Well, the bitmap editor Krita is also in KOffice..."
    author: "Boudewijn Rempt"
  - subject: "Re: Karbon14"
    date: 2004-01-28
    body: "I think it would be nice to have something like KDE Graphic Suite or KStudio. A collection of programs for graphic work like KOffice. With \n- vector programm\n- pixel programm\n- a autotrace.sf.net frontend\n- a SANE frontend\n- a gphoto frontend\n- an image viewer\n- image organizer"
    author: "Tobias"
  - subject: "Re: Karbon14"
    date: 2004-01-28
    body: "+1 for this\n\nWould be great if the PyQT and PyKDE bindings would go into the KDE standard-distribution. Then a lot more people (like me) would be able to contribute code to the KDE-project, something like an image-organizer would profit from a lot of input of different people."
    author: "Michael"
  - subject: "Re: Karbon14"
    date: 2004-01-28
    body: "\"a gphoto frontend\"\n\nThere is camera:/ in konqueror.  :)"
    author: "AM"
  - subject: "Re: Karbon14"
    date: 2004-01-29
    body: "To have a \"ral\" gphoto frontend there is digikam (http://digikam.sourceforge.net/) which is pretty powerful. With all it's plugins of version 0.6 it's becoming a real suit..."
    author: "Birdy"
  - subject: "CVS"
    date: 2004-01-27
    body: "Is ther a WebCVS browser for KOffice?"
    author: "Martin"
  - subject: "Re: CVS"
    date: 2004-01-27
    body: "For what do you need it? I mean, you have eg. Konqueror as the browser, no need for a special integration in KOffice.\n\nOr do you simple mean: http://webcvs.kde.org/cgi-bin/cvsweb.cgi/koffice/"
    author: "Philipp"
  - subject: "Re: CVS"
    date: 2004-01-27
    body: "http://webcvs.kde.org/cgi-bin/cvsweb.cgi/koffice/ was what I was searching for."
    author: "MaX"
  - subject: "screenshots"
    date: 2004-01-27
    body: "Congrats, but some of the screenshots are sooooooow ugly..."
    author: "datadevil"
  - subject: "Re: screenshots"
    date: 2004-01-27
    body: "Personally I don't make prettiness my primary concern when I'm picking out, say, a spreadsheet application..."
    author: "Shane Simmons"
  - subject: "Re: screenshots"
    date: 2004-01-27
    body: "I agree.  First of all, because it is an office suite, all the screenshots should be taken using the same theme.  Hopefully the current theme, and not the kde 2.x theme or beos theme.  \n\nScreenshots are supposed to convey good things about the application, these convey nothing but uglyness.\n\nAlso, I thought the issue with the lack of anti-aliasing on bold and italic buttons was resolved years ago.  I guess not.  "
    author: "rizzo"
  - subject: "Re: screenshots"
    date: 2004-01-27
    body: "That issue has been resolved, at least in my KDE 3.2 RC1 installation...\nCheck out the attached screenshot."
    author: "Spy Hunter"
  - subject: "Re: screenshots"
    date: 2004-01-28
    body: "Ahhh.. another Plastik fan?"
    author: "Malcolm"
  - subject: "Re: screenshots"
    date: 2004-01-28
    body: "Yup. Keramik is definitely an interesting and unique style, but I find Plastik easier on the eyes for everyday usage.  Thin Keramik also looks interesting but I haven't taken the time to install it and try it out.  Plastik comes with KDE now so it takes no effort to switch."
    author: "Spy Hunter"
  - subject: "Re: screenshots"
    date: 2004-01-27
    body: "I was going to rebuttle this, but then I did went and checked out all of the screenshots.  The applications themselves are fine, but they are quite inconcistant in widget theme, widget color(s), icon set and window manager theme.  Maybe it wouldn't be a bad idea to re-generate them using the latest release and KDE 3.2 :)  Are the files in the screenshots located cvs (if not, why not :)?  Might there even be a way to automate the capture of screenshots?\n\n-Benjamin Meyer  "
    author: "Benjamin Meyer"
  - subject: "Re: screenshots"
    date: 2004-01-27
    body: "I agree. I think I only saw one screenshot that used the current default KDE theme. The crappy quick 5 second PNG picture, KDE RULEZ, Wazzzzaaaauupp!!!, poorly laid out spreadsheets/photos/etc needs to go and visually appealing content needs to replace it. Screenshots with a huge number of toolbars needs to be reduce to the essentials (simplicity).\n\nAn office suite should show consistency across the applications .. these screenshots do not show that. \n\nA new release announcement should show off new features visually .. I'm hard pressed to see them in these shots (ie alpha channel images in KPresenter).\n\nHow does KDE-Enterprise ever expect to be successful with this level of official PR material? For as talented as the KDE developers are, they need equally as talented PR/Marketing people that can take all that is so great about KDE and get users and non-users truly excited about it."
    author: "John Alamo"
  - subject: "Re: screenshots"
    date: 2004-01-28
    body: "well maybe they need more PR/Marketing people. Ever thought of that :)\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: screenshots"
    date: 2004-01-28
    body: "> well maybe they need more PR/Marketing people. Ever thought of that :)\n\nHold on a second there Fab... this is a pretty revolutionairy thought. The point of the conversation is about what *other* people should do... you're getting uncomfortably close to suggesting that our large and productive number of contributing critics should take on the mantle of (shudder) performer on this great stage. Great googly moogly man! Do you realize the terrible commitment and risk here? I think you should stop making such controversial veiled suggestions... before you upset some critics and they don't show up for work tomorrow... especially if their busy helping a project. For shame!"
    author: "Eric Laffoon"
  - subject: "Re: screenshots"
    date: 2004-01-28
    body: "hehe..pretty funny.. i'm doing my share tho i think with packaging kde for cAos (www.caosity.org), but maybe someone else has time? "
    author: "datadevil"
  - subject: "Re: screenshots"
    date: 2004-01-28
    body: "It's not a bug, it is a feature :P"
    author: "Shrei"
  - subject: "Re: screenshots"
    date: 2004-01-29
    body: "If you want better screenshots on koffice.org and you are a volunteer to take care of it, then contact us on one of the koffice mailing lists.\n\nOtherwise it is and will remain provisory.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Grrr"
    date: 2004-01-27
    body: "seems Slashdotted. Or should I say: K-Dotted?\n"
    author: "Mike"
  - subject: "Re: Grrr"
    date: 2004-01-27
    body: "This should be posted on slashdot.\nIsn't this the only open source office suite developed from scratch?\n\nI mean, open/star-office was proprietary in the beginning..."
    author: "KA"
  - subject: "Re: Grrr"
    date: 2004-01-27
    body: "There is even Gnome Office."
    author: "MaX"
  - subject: "Re: Grrr"
    date: 2004-01-27
    body: "Yeah right..."
    author: "OI"
  - subject: "Re: Grrr"
    date: 2004-01-27
    body: "GNOME Office is an ancient joke, just go and try to find a reference to it on http://www.gnome.org - not even the GNOMEs believe in it."
    author: "Anonymous"
  - subject: "Re: Grrr"
    date: 2004-01-27
    body: "Abiword is a very solid word processor. Gnumeric blows away all of the spreadsheets I have used on Linux. It has better support for Excel files than OpenOffice.org."
    author: "Jarefri"
  - subject: "Re: Grrr"
    date: 2004-01-27
    body: "These are single applications. There is no integration to an office suite."
    author: "Anonymous"
  - subject: "Re: Grrr"
    date: 2004-01-28
    body: "agreed.. however, gnumeric and abiword almost have nothing to do with each other :("
    author: "anon"
  - subject: "Re: Grrr"
    date: 2004-01-28
    body: "> Abiword is a very solid word processor\n\nWell, yes and no.\nIn a fair comparison, it is a solid text editor with extra features (fonts etc..).\n\nBut, it does not have the frame capabilities of eg KWord.\nAnd it doesn't integrate with anything.\nThis is what people would expect from an office tool today.\n\nAbiword is a great standalone app though! :-)"
    author: "OI"
  - subject: "Re: Grrr"
    date: 2004-01-29
    body: "No, that is what people expect of an office suite when they want to bash GNOME office.\n\nI have used Office suites quite extensively, and can state with reasonable authority that most people do not use the intergration features of an Office suite. In fact, they use them as they used them 10 years ago. As standalone apps. People rarely use the features that are constantly hailed as the next big thing. If they really did, people would not even talk about KOffice when talking about good office suites. Because it offers nothing. The last time I tried Koffice, or kspread to be specific, I could not vlookup, hlookup, lookup or a bunch of other VERY IMPORTANT functions. How they heck do you use a spreadhseet without being able to lookup. It is a bunch cells, and looking up is the most basic thing I do with a spreadsheet. Now Gnumeric, there is a spreadsheet. Better than oocalc and some people rate it higher than Excel in some ways. Intergration can come later, I am sure its not too difficult. The infrastructure is all there. The problem is that many of these GNOME office apps are also meant to be cross platform anyway, and they were all developed by people to scratch an itch as it were. \n\nAbiword is very good at what it does, and is developing logically forward. I think integration is good, but I rate a complete coverage of Excel functions far higher than integration any day."
    author: "Maynard"
  - subject: "Re: Grrr"
    date: 2004-01-29
    body: "You do have a point there.\n\nBut don't forget the aim of Koffice. From the start, to make an integrated suite\nfor quality compositions.\n\nAnd with frame based KWord, integrating sheets etc from KSpread is great.\nEspecially for making desktop publishing swift.\nThis is partly why this project is so cool.\n\nAnd also, that few users actually use a feature doesn't mean that they don't need that feature. It might just mean that it's not obvious how to use it. Usability. If there is an easy way to use eg integration, users could just as quickly start using it saying \"Hey, great feature!\"."
    author: "OI"
  - subject: "Re: Grrr"
    date: 2004-01-30
    body: "Can you put a single KSpread cell in a KWord document?\n\nCan you have a figure in the middle of a sentence in a KWord document and have it linked to a cell in a KSpread file?\n\nWhy does each application have its own file format?\n\nHow do I make a chart with KChart using the figures in a KSpread file?\n\nIf we want a true integrated office suite, we need to design one, and then implement one (probably well over 90% of the work done so far can be used).\n\nKDE provides the technology to have a truly integrated office suite.  But, we need to use it.  If we can somehow accomplish this we will have *THE* killer application. \n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Grrr"
    date: 2004-01-30
    body: "I have to agree with you and disagree with you.\n\nKOffice is lacking a lot of features -- to the point that I can't see why it has a version number > 0.5.\n\nHowever, since with KParts, integration should be the major feature of KOffice.  This should have been developed first.\n\nNow as it stands, version 1.3 lacks both features and real integration.  \n\nWhere do we go from here?  I think that the integration is the most important issue.  However spreadsheet functions seem like an independent development track which could be implemented without affecting the main development path.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Direction of KOffice?"
    date: 2004-01-27
    body: "I think we're really going to have to get our heads together and come up with some ways to fund KOffice more and push it forwards. There is some stuff in here that is not in Open Office, and Kivio in partiular, looks marvellous. You can't tell me that stuff like Kivio and Kugar are not going to useful to businesses.\n\nQuite frankly, with the time, resources and manpower available, KOffice is a huge advertisement for the development power of Qt and KDE."
    author: "David"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-28
    body: "I'd really love to. I just have all my time and funding wrapped up in Quanta. However it's also a great advertisement, and unlike office products is aimed at a less mature moving target. So it can more easily out pace windows market leaders. I wish I had resources for both, but never understimate how many web developers there are on the web. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-29
    body: "Yes, we need to design first and code second -- rather than the other way around.\n\nSpecifically about Krita: Should this be an application suitable for artists, or should it just contain the basic paint functions needed to make office presentation slides and for desktop publishing?\n\nKarbon14 & Krita: I think that these should be unified into a single application -- if they remain separate applications and they have the features which I want, they will have a lot of redundant code.\n\nKiller application: I realize that KOffice was assembled from several applications, but I do not understand why it is structured as separate applications since they are not really applications but rathere KParts.  It would be much better to have it structured as a single application with a single file format like gobeProductive.  This doesn't have to be an either/or question; it would be possible to develop that type of interface in addition to the current separate application interface.\n\nNOTE: if you don't know what gobeProductive is, there is a preview:\n\nhttp://www.gobe.com/Trial/gobeProductive_Trial.msi\n\nThis will run with WINE, but you have to install MSI first.  Be sure that you have fonts installed in your fake: \"C:\\Windows\\Fonts\" directory.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-29
    body: "After about five years of development, I don't think it's a good idea to stop development on Krita and go back to the design stage... I think it's an amazingly bad idea, in fact. If you want to design a paint app from the ground up, fine, please do. And if you want to base it on the KOffice libs, nobody will stop you. And when you're done and it's better than Krita, I won't oppose swapping your app into KOffice, and Krita out -- no problem.\n\nMeanwhile, I'll just quietly (or rather, not quietly, but that's because I'm a verbose kind of person) go on hacking Krita in my amateur way, first trying to implement what's needed most to do simple pixel-mangling, learning how this entire graphics wheeze works, and then going on to make it the application I feel is still missing from the entire OSS/Free Software portfolio. Rather than the app you feel is missing. Can't help that, the chemise being closer than the trousers.\n\n(By now Krita now supports the layer effect thingy -- a bit limited, but that's just because I haven't implemented fancy composition operators like 'bumpmap' yet (or rather, haven't ported them from GraphicsMagick yet), and a Krita image can now consist of layers of different image types -- a grayscale layer over a CMYK is now entirely possible. Still no eraser too, though.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-29
    body: "I think that you confuse the application which your are developing -- which IIUC will be a great application -- with the question of what should be included in KOffice.\n\nShould KOffice include a paint application that will ultimately be a larger application than The GIMP?  While such an application will be a nice addition to the list of KDE applications, I simply do not think that it belongs in KOffice.  A possible solution to this would be to have a light version included with KOffice and the larger application be a stand alone application.\n\nThe same idea probably applies to Karbon14.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-29
    body: "No worries for the moment... Krita won't become bigger than the Gimp for years."
    author: "Boudewijn Rempt"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-30
    body: "Why would we set some size restraints on what may be included in KOffice? If it's useful to people I don't see any problems including it.\nYou do talk a lot considering the amount of work you're doing..."
    author: "Peter Simonsson"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-30
    body: "> Why would we set some size restraints on what may be included in KOffice?\n\nBecause it is an *office* application, not an artists painting application.\n\n> If it's useful to people I don't see any problems including it.\n\nIt it is too complicated -- like The GIMP -- it won't meet the needs of those users that normally use an office suite.  My perception of the needs of office users might not be correct, but I think that it is a reasonable position.\n\n> You do talk a lot considering the amount of work you're doing.\n\nI fail to see your point.  I think that we could use several more people to think about things and discuss their thoughts.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-30
    body: "Don't worry -- it's just one of James' favourite hobby horses. His flogging the beast now and then does no harm -- after all it's not talk that determines what happens, and I see very little danger of the powers that be (if they be, which I sometimes doubt) deciding to evict Krita for being too capable. For being too immature, incomplete and so on after five years of development, yes. But not for being too capable. Not on your nelly.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Direction of KOffice?"
    date: 2004-01-30
    body: "For the point of combining a vector app with a bitmap app you realy dont have to, when speaking of Krita and Karbon. You only have to connect the dots :-). Both apps use KOffice libs, eg they are kparts and they have layers. Then the only ting needed is to have the apps accept a layer wich is an kpart. \n\nAs I don't have a clue of the code involved perhaps I'm talking nonsens :-)\n"
    author: "Morty"
  - subject: "Gentoo Luvin"
    date: 2004-01-27
    body: "http://www.grebowiec.net/ebuilds/koffice-1.3.ebuild\n\nput that in /usr/portage/app-office/koffice and enjoy :)\n\nemerge koffice-1.3.ebuild digest\n\nemerge -pv /usr/portage/app-office/koffice/koffice-1.3.ebuild\n\nIt is building for me now and I am happy!"
    author: "GweeDo"
  - subject: "Re: Gentoo Luvin"
    date: 2004-01-27
    body: "And if you happen to have Debian (Unstable?), use this:\n\ndeb http://rs.fuzz.nl/muesli/686/kde_head/ unstable/\n\nIt will also install KDE 3.2 CVS, however."
    author: "Daan"
  - subject: "Re: Gentoo Luvin"
    date: 2004-01-27
    body: "I am running Mepis Linux (a debian offshoot) and I have added \nthe following line to the end of my /etc/apt/sources.list file:\n\ndeb http://rs.fuzz.nl/muesli/686/kde_head/ unstable/\n\nThen I ran \n% apt-get update\n\n% apt-get -V upgrade \n\nAnd it did not find the kde-3.2 or koffice 1.3.\n\nDo I have to do something else to pick up the packages path \nyou mentioned?\n\nthanks"
    author: "Shami"
  - subject: "Re: Gentoo Luvin"
    date: 2004-01-31
    body: "I tried the steps you suggested, but no luck for me on compiliing.  After downloading the ebuild and trying to run \"emerge koffice-1.3.ebuild digest\", I get an error message stating that no masked or unmasked packages exist to satisfy the dependencies for digest.  Running your second command failed as well.  I'm really anxious to try 1.3.  Any suggestions?\n\nPaul....."
    author: "cirehawk"
  - subject: "kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-27
    body: "donalbain@norman:~> kword\nkdecore (KIconLoader): WARNING: Icon directory /home/donalbain/.kde/share/icons/crystal/ group  not valid.\nkdecore (KIconLoader): WARNING: Icon directory /home/donalbain/.kde/share/icons/crystal/ group  not valid.\nkoffice (lib kofficecore): WARNING: /home/donalbain/.local/share/applications/kde-kword.desktop: no X-KDE-NativeMimeType entry!\nkoffice (lib kofficecore): ERROR: Couldn't find the native MimeType in kword's desktop file. Check your installation !\n\nHow can I fix it?\nThanks. D."
    author: "Donalbain"
  - subject: "Re: kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-27
    body: "Remove /home/donalbain/.local/share/applications/kde-kword.desktop\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-27
    body: "> Remove /home/donalbain/.local/share/applications/kde-kword.desktop\n \n> Cheers,\n> Waldo\n\nGod bless you Waldo! I have been having trouble running Kword built from CVS for some time and had no idea about this directory. I removed it virtually everywhere else. It's up like a charm! Time to build the latest. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-28
    body: "It is worth to mention that this bug was fixed in KDE, so it won't happen anymore with 3.2. Moreover, we added a workaround in KOffice as well, in case somebody runs into the same problem with older KDE libraries."
    author: "Lukas Tinkl"
  - subject: "Re: kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-29
    body: "Hi Waldo!\n\nGreat! Works like a charm! \n\n(And also thanks for all your work on KDE! It's such a pleasure to work with it! 3.1.95 looks very very promising!)\n\nD."
    author: "Donalbain"
  - subject: "Re: kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-29
    body: "Any idea how to solve that problem in Mandrake 9.2 ?\n\nkoffice (lib kofficecore): ERROR: Couldn't find the native MimeType in kword's desktop file. Check your installation !"
    author: "OI"
  - subject: "Re: kword does not start w/ SUSE 9.0, KDE-3.1.95"
    date: 2004-01-29
    body: "You could look in the archives of koffice and koffice-devel mailing lists: http://lists.kde.org\n\nIt seems that we start to understand what the problem is that it would be fixed in KOffice CVS HEAD (but not much protable back, as it is a too intrusive change.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "KDE 3.2"
    date: 2004-01-27
    body: "\nSeems to be out RSN...\nCan anyone give \"us the crowd\" a hint when it will come out..."
    author: "ac"
  - subject: "Re: KDE 3.2"
    date: 2004-01-27
    body: "You don't need a hint - there's a public release plan.\n\nhttp://developer.kde.org/development-versions/kde-3.2-release-plan.html\n\nFor those too lazy to copy/paste the link above, the release date is the 2nd of Febuary (of course, it might change between now and then but let's hope not :)\n\n"
    author: "another ac"
  - subject: "Great Stuff"
    date: 2004-01-27
    body: "The girl constantly moans about me forcing OpenOffice on her (well it's not forced, she can buy her own computer with MS Office if she likes, or Crossover even ;-).\n\nHaving just tried out new KWord I have high hopes she'll not be moaning anymore. I love the default simple look and the fact that it has all the functions she (and I) need for general word-processing.\n\nLooking forward to trying out the rest of the suite.\n\nps any way to set it to save to OOo format by default?"
    author: "Max Howell"
  - subject: "Re: Great Stuff"
    date: 2004-01-27
    body: ">Having just tried out new KWord I have high hopes she'll not be moaning anymore.\n\n\nhummm, I will probably be shot for this (was off at /. recently), but are you sure that is what you wish to do?   :)"
    author: "a.c."
  - subject: "Re: Great Stuff"
    date: 2004-01-28
    body: "heh, ;-)\n\nKOffice - ruins life in the bedroom."
    author: "Max Howell"
  - subject: "Re: Great Stuff"
    date: 2004-01-29
    body: "No, there is not a way to use OO file formats as default.\n\nI would not even recommend to always save as OOWriter in KWord, as for example table support is missing.\n\n(saving in RTF would be better but there OOWriter has some known problems.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "KOffice Beta"
    date: 2004-01-27
    body: "Tried the KOffice 1.3 beta on mandrake 9.2\nReally promising stuff guys."
    author: "OI"
  - subject: "How about boolean support in kSpread"
    date: 2004-01-28
    body: "Boolean is really nice to have when you are making big Truth tables. Open Office does Boolean very well, and all you need to do is set the atomic trith values, then assign the formulas to the other columbs and boom, you get a truth table in 5 minutes....got to love that.\n\nplease kspread folks...get the boolean cell format in soon so I can use koffice!!!"
    author: "Jeremy"
  - subject: "Re: How about boolean support in kSpread"
    date: 2004-01-28
    body: "You know how to use zero's and one's and eventually format them to display strings 'True' and 'False', right? :)"
    author: "Lukas Tinkl"
  - subject: "KOffice works on _some_ desktops"
    date: 2004-01-28
    body: "Not wanting to troll, but:\n\nOpenOffice is cross platform.\nKOffice is not cross platform.\n\nHow can KOffice compete if it works on only some desktops?\n\n"
    author: "ac"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-28
    body: "How cross platform is MS Office? ;)\n\nAFAIK, KOffice will make the OO.o document format the default format and they say   it can already import and export to it (don't know if it's complete?). If they are using the same document format, it doesn't matter that much what office suite you're using.\n\nAnother pretty interesting new feature is this: \"Also new is the ability to import PDF files into KWord and make changes to the document.\"\n\nI'm not aware of any other Office suite that can do that?"
    author: "Joergen Ramskov"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-28
    body: "> Another pretty interesting new feature is this: \"Also new is the ability to import PDF files into KWord and make changes to the document.\"\n\nThat must be the best feature of the year among all possible word processors."
    author: "OI"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-29
    body: "Sounds like it's not quite ready yet:\nhttp://tinyurl.com/2kgxu"
    author: "Joergen Ramskov"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-28
    body: "depends on how you see it; KOffice was recently ported to MAC ... and KDE works on Cygwin, so I guess KOffice works in Cygwin also (?)\nThis makes KOffice work on: Linux, MAC and Windows. KDE also works on Solaris - so now you have it everywhere where OpenOffice.org runs. And KDE also runs on some other UNIX systems AFAIK. Seen this way you have KOffice running on more platforms than OOo.\n\ngreetings,\nRaphael"
    author: "raphael"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-29
    body: "Does anyone know if KOffice will run directly on Windows using U/WIN?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-28
    body: "Openoffice is not integrated into the desktop, it has to create its own widget set, and can't reuse kde components.\nKOffice is and can.\n\nHow can OpenOffice compete if it has to redo everything poorly?\n\n"
    author: "JohnFlux"
  - subject: "Re: KOffice works on _some_ desktops"
    date: 2004-01-28
    body: "By being better technology?\n\nLet me ask a question. Can you use OOWriter as a part in your application? Easily?\n\nYou can use the various KOffice components in applications you build. KWord, KChart, etc. They are designed that way. The KDE framework is designed for that sort of thing. Just look at the huge numbers of vertical apps available for windows, and you see the advantage of a component based architecture.\n\nUsing KWord as a word processor under another desktop environment gives you a nice word processor. Within KDE, it becomes a very powerful component of a very powerful desktop.\n\nDerek"
    author: "Derek Kite"
  - subject: ":(("
    date: 2004-01-29
    body: "Oh dear.. I keep getting this:\n\"checking for KDE... configure: error:\nin the prefix, you've chosen, are no KDE headers installed. This will fail.\nSo, check this please and use another prefix!\"\n\nI'm using this:\n./configure --prefix=/usr/local/kde --libdir=/usr/lib/kde3\n\nWhich is where my kde installation is, and the location of the kde3 libraries.. what am I doing wrong?\n\nLaura.  Having blonde moments again."
    author: "Laura"
  - subject: "Re: :(("
    date: 2004-01-29
    body: "Did you install from binary packages like rpm's?  If so, perhaps need to install the development packages as well.\n\nIf you have already installed these (or installed from source), the only thing I can think of is how odd it is that kde is in /usr/local and the kde libs are in /usr/lib instead of /usr/local/lib.  Then again, that may be normal.  I wouldn't know since I put kde in /opt. :)"
    author: "duderonomy"
  - subject: "Re: :(("
    date: 2004-01-29
    body: "Check your $KDEDIR and $KDEDIRS environment variables.\n\n(However it could be other problems too.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Kspread functions"
    date: 2004-01-29
    body: "Is thre any place I can find an up to date list of all the worksheet functions Kspread supports right now. I think their site is more a marketing thing than something really informative. This type of info should be there."
    author: "Maynard"
  - subject: "Auto-generated project presentations with Perl"
    date: 2004-01-31
    body: "The source data include project schedule, CVS commit info, bug database, and billing data. From these it would be nice to create presentations and web-pages. There are three somewhat different presentation audiences, short $/hour-oriented executive summary for senior management, schedule-oriented presentation for the customer, and gory details for project team meetings. The presentation material should be put to intranet and extranet for archiving purposes and to benefit the people not present in the meetings.\n\nThe obvious solution could be Perl/Python scripts generating the KPres/KWord XML and HTML (PHP?) files. In the more indirect solution the scripts call the KPart functions or DCOP.\n\nWould this be possible in near future?"
    author: "A. C."
  - subject: "tuxpaint"
    date: 2004-01-31
    body: "Huups? No paint program?\n\nSo I will have to use Tuxpaint?"
    author: "God Level"
---
KOffice 1.3 has been released today. See the
<a href="http://www.koffice.org/releases/1.3-release.php">release notes</a>,
<a href="http://www.koffice.org/announcements/announce-1.3.php">announcement</a>
and the complete <a href="http://www.koffice.org/announcements/changelog-1.3.php">list of changes</a>.


<!--break-->
<p>
KOffice 1.3 consists of the following applications:
<ul>
<li><a href="http://www.koffice.org/kword/">KWord</a> - A frame-based word processor
(<a href="http://www.koffice.org/kword/screenshots.php">screenshots</a>)</li>
<li><a href="http://www.koffice.org/kspread/">KSpread</a> - An advanced spreadsheet application
(<a href="http://www.koffice.org/kspread/screenshots.php">screenshots</a>)</li>
<li><a href="http://www.koffice.org/kpresenter/">KPresenter</a> - A full-featured presentation program
(<a href="http://www.koffice.org/kpresenter/screenshots.php">screenshots</a>)</li>
<li><a href="http://www.koffice.org/kivio/">Kivio</a> - A Visio®-style flowcharting application
(<a href="http://www.koffice.org/kivio/screenshots.php">screenshots</a>)</li>
<li><a href="http://www.koffice.org/karbon/">Karbon14</a> - A vector drawing application
(<a href="http://www.koffice.org/karbon/screenshots.php">screenshots</a>)</li>
<li><a href="http://www.koffice.org/kugar/">Kugar</a> - A tool for generating business quality reports</li>
<li><a href="http://www.koffice.org/kchart/">KChart</a> - An integrated graph and chart drawing tool
(<a href="http://www.koffice.org/kchart/screenshots.php">screenshots</a>)</li>
<li><a href="http://www.koffice.org/kformula/">KFormula</a> - A powerful formula editor
(<a href="http://www.koffice.org/kformula/screenshots.php">screenshots</a>)</li>
</ul>
</p>
<p>
Big improvements have been made in KOffice 1.3 with respect to
interoperability with other office file formats. It is now for
example possible to import as well as export OpenOffice.org
documents. Also new is the ability to import PDF files into KWord
and make changes to the document. Support for Microsoft document
formats has improved as well.
</p>