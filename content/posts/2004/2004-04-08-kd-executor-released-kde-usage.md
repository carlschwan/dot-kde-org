---
title: "KD Executor Released for KDE Usage"
date:    2004-04-08
authors:
  - "jpedersen"
slug:    kd-executor-released-kde-usage
comments:
  - subject: "Why?"
    date: 2004-04-09
    body: "What's the use of this?\nWhat does it provide that normal test units and dcop scripts can't?"
    author: "John Tapsell"
  - subject: "Re: Why?"
    date: 2004-04-09
    body: "Simulation and recording of user actions is something that DCOP and our current tests can't really manage. This has the potential to be very useful.\n"
    author: "Richard Moore"
  - subject: "Re: Why?"
    date: 2004-04-09
    body: "Just exactly what are normal tests?\n\nOh Well.  What this type of tool can do?  \n\nI see two things.\n\n1.  Regression testing: A suite of tests can be developed that every release MUST pass before it is kicked out the door.\n\n2.  If you have some sort of intermittent problem this would allow you to do the same thing over and over till it screws up.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Why?"
    date: 2004-04-09
    body: "regression testing in 'normal' code (non-gui) isn't difficult (although if this tool can reduce the burden, I'd be interested in how).\n\nRegression testing the actual gui... I suppose, but I'm not convinced that in practice it is needed.\n\nIf you have some intermittent problem again you could just run your normal regression tests a lot directly calling the functions, and not needing to do the testing through the gui. \n\n"
    author: "John Tapsell"
  - subject: "Re: Why?"
    date: 2004-04-09
    body: "The purpose of this tool *IS* to test the GUI.\n\nRegression testing is very important.  See for example:\n\nhttp://bugs.kde.org/show_bug.cgi?id=73379\n\nRegression testing should have caught this bug.  Without it, KDE-3.2.0 was released without it being completely fixed.\n\nIntermittent problems are usually in the GUI code -- unknown interactions with other stuff.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "This is certain to come up (again and again)"
    date: 2004-04-09
    body: "As a free community I strongly feel it would be a regressive step to accept KD Executor in it's non-free state, regardless of it's technical merits. Perhaps initially efforts should be focused on exploring the problems Klar\u00e4lvdalens Datakonsult AB face with GPL'ing (or compatible license) KD Executor?"
    author: "Martin"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Agree.\n\nExactly how may it be useful for me, a KDE application developer?\n\nHow hard would be to create similar functionality for kdesdk? Is it worth the time?"
    author: "Cloaked Penguin"
  - subject: "Usefullness"
    date: 2004-04-09
    body: "Imagine you are debugging a problem that happens in a KDE application, after creating a document, creating 2 objects, doing some action, and then opening a dialog and clicking somewhere.\n\nFor every little code change you make and want to test, you have to perform the above steps again and again.\n\nWith kdexecutor, you can do \"kdexecutor myapp\", set to recording, perform all the actions above, and it gets saved to a .kdx script.\nThen you can make a one-liner script that launches kdexecutor on myapp, to replay all the actions at a very fast pace.\nThis saves a lot of time, since the application is now very quickly brought to the state where the bug can be seen."
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Is that an acknowledgment that it would be worth developing a similar application under the GPL or that you support accepting the non-free software? (or perhaps it's neither...)\n\nWhat are your thoughts?\n\n"
    author: "Martin Galpin"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "I don't think its helpful to open source development to get to political with companies that are trying to assist open source development. Linux uses bit keeper in kernel development and its not open so what the problem? getting to cared away with licence issues only turns people off to the whole thing."
    author: "Kraig"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "...and as I'm sure you are aware, the Bit Keeper issue has been a very controversial one.\n\nForgetting the importance of licensing is something we must not do as free operating systems move to the mainstream. To do so would be to forget what the FOSS movement is about, and why it began. Do we want to end up right back where we began?"
    author: "Martin Galpin"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Yeah, ok. When you code in the kernel, you can have an opinion. If this helps KDE, then so be it. It is more than what we had before, so we aren't losing anything. Shut up and code."
    author: "Joe"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "I'll shut up, but probably won't be coding this until I find a \"crash me\" sequence related to user input.\n\nAnd even then, maybe a separate app will not be the \"right way\" and I will help improve dcop instead. Or recode my app's dcopness to hack a way through.\n\nThe idea is cool, but not cool enough (for me) as to justify not having it straight in kdesdk."
    author: "Cloaked Penguin"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "FWIW, I agree with him.  A \"shut up and code\" approach is exactly what'll get your software owned by some company you've never heard of."
    author: "Guest"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Tell me exactly how using kdexecutor gets your software owned by some other company? This is nonsense.\n\nAll in all, it's pretty sad. KDAB makes a gift to the KDE community, and people bitch about non-free-software.\nYet when Lindows gave KDE developers access to their (closed source, too) OS, it was pretty cool.\n\nAnd when Insure gave KDE developers a license to use their tool... well, the dot didn't exist at the time, so people had nowhere to rant about non-free-software.\n\n:("
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Because people are worried.\n\nSay that people put a lot of time and effort into making regression tests for their projects, and then the company asks for more money, or goes bankcrupt, or anything like that.\n\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "What does this have to do with using KDExecutor in KDE?\n\nNobody is asking for money in that situation, it's a *free* (as in beer) gift.\n\nIf the company goes bankrupt, well, I'm quite sure the source code would then be released.\n\nBut anyway, we're not forcing anyone to use it, nor to put a lot of effort creating lots of regression tests with it. I see it much more as a tool to assist debugging, and in such a case there is no long-term commitment to the tool."
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "The license expires in 3 months - after which you have to hope that they will give the next one away for free.\n\nAnd you are \"quite sure the source code would then be released\".  Well I don't see any contract on that like bitkeeper has...  Why risk all the development on making tests on a \"quite sure\" ?\n\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "The license expires in 3 months only because it's a beta version.\nAfter that we (KDAB) will release the final version, and we surely want people to upgrade to that one to avoid getting bug reports on the old version.\nThe plan is that the final version will have an unlimited (in time) version available for KDE developers.\n\nNo, there is no such contract for \"releasing the source code\" yet. Come on, it was pretty important to have one for Qt, since all of KDE is based on it, but one for a testing tool? Why do you need such a contract just to use KDExecutor to automate a few testcases for easier debugging?\nOh wait, I forgot. You criticize but don't even work on KDE..."
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Why do you say I don't work on kde?\nI haven't done a huge amount sure, but I have plenty of patches in kde and a few in the linux kernel, and tons of specialist open source software.\n\nDespite the merits of the software, you can't have an free project rely on the whims of a closed source company.  What stops this company waiting till 6months down the line when there are hundreds of hours of tests written, and then demanding a large amount of money for the software?  Or changing the license, forcing everyone to accept the the new license? (Of the tool, not the tests of course).  Or if the company goes bankcrupt?\nAnd so what if they say they won't.  What if they get bought out by another company who does.  Or sell the product to another company.\n\nNo, it's too risky for a too little gains.\n\nLook at bitkeeper.  It was finally accepted because bitkeeper wrote several contracts to ensure that it will always be around (opensourced if the company goes down).  It was also accepted because there was no other tool, and writing one would be a huge amount of effort.\n\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "When will you understand that KDAB is only composed of KDE developers?\nWhy would we (I work for KDAB) demand money from our fellow KDE developers, or change the license to hurt them??? We're all KDE developers, and up to now we rather trusted each other, and we would certainly not want to lose this trust.\nBesides the idea doesn't even hold commercially (no kde developer would pay to use the tool, that's for sure).\n\nOnce again, this is only about a tool that helps debugging.\nI don't suspect KDE developers will write hundreds of tests with it; we (KDE, this time) are not known for spending too much time on regression testing, unfortunately - otherwise there would already be much more unit testing in kdelibs... The \"dependency\" on kdexecutor is only in your mind.\n\nThe whole point is to make our fellow KDE developers benefit from a useful tool, while still being able to sell the tool to companies.\nCan people stop seeing evil conspiracies every time the word 'company' is mentionned?"
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "\"What stops this company waiting till 6months down the line when there are hundreds of hours of tests written, and then demanding a large amount of money for the software? Or changing the license, forcing everyone to accept the the new license? (Of the tool, not the tests of course). Or if the company goes bankcrupt?\"\n\nCan you answer it? What if this happens? What's the problem? I don't get it."
    author: "Source"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Ah sorry.  My point was that you could end up with a lot of test scripts that you couldn't use because you can no longer use the software.\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "What's the problem if they change the license in the future? KDE developers will still have a valid license for the version they are using. No one can take tht away from them. "
    author: "Anonymous"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Take KDE default icons, for instance. They are free as in speech but developed with non-free tools (not even as in beer!): http://linuxcult.com/?m=show&id=157\n\nSame in this case, there is a non free tool to do the job, any developer is free to get a licence and use it if he wants too, (including KDE developers). Now they can get it for free.\n\nAnd how are they risking development by using it? If for some reason they have to stop we'll be back to where we were before... \"Ah! but it would be better to have a free as in speech tool!\". Yeah, there are many apps around missing in KDE, and we can't blame it on commercial apps giving free licences because they aren't.\n\n\n\n"
    author: "Source"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "I don't know about the icons.\nIf the icons are based on propreitry file formats, then that is dangerous as well.  Look at .gif for example.\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "The icons were created with commercial tools.\nThe icons are free.\n\nUnderstand where I'm getting at?"
    author: "Source"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "First of all, you probably mean proprietry.\nSecond of all, you missed my point about the file format being the important bit.\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "No I mean commercial! The software used by everaldo to make the icons is commercial. The format is not proprietary. \n\nAnd that's my point exactly, the important bit is the file format not beeing proprietary, not which tools were used. "
    author: "Source"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Would you please quit whining about the license stuff? Don't use the tool if you don't like the license, but let others enjoy it if they need something like that. You don't even seem to care a bit about these kinds of tools, so please stop flaming KD for giving the KDE community a gift.\n "
    author: "Chakie"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "> If the company goes bankrupt, well, \n> I'm quite sure the source code would then be released\n\nMaybe Klar\u00e4lvdalens Datakonsult AB and KDE e.V. could set \nup an agreement like the \"KDE Free Qt Foundation\"\nto remove any doubt?   I've seen the foundation being mentioned \nquite a few times in discussions as a strong argument for Qt. \n\nMaybe develop a standard procedure for this kind of situation?  \nThis could encourage other companies to do the same...\n\nBut even if they don't...  a gift is a gift and the offer is appreciated. \n\n"
    author: "cm"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "This is actually quite funny when you realize that the CEO of KDAB is a board member of KDE e.V.   ...\n"
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Even president of KDE e.V."
    author: "Anonymous"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Right. So Kalle would be effectively signing a paper with himself. LOL.\n\nI think people should just stop and realize that KDAB can only be acting in KDE's interests, given that KDAB _is_ only KDE developers."
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "> Right. So Kalle would be effectively signing a paper with himself. LOL.\n\nWell, why not?  :)\nLegal stuff is sometimes strange...\n\n\n> I think people should just stop and realize that KDAB can only \n> be acting in KDE's interests, given that KDAB _is_ only KDE developers.\n\nYes, I know.  The point is that \na) written guarantees seem to be important for some people\n(I'm *not* talking about myself in this case, I'm not worried \nabout KD Executor)\nand \nb) I can imagine situations where the current owners of the company\nare no longer free to release the source code, e.g. in case of bankruptcy. \nFor example, in Germany there's someone called \"Insolvenzverwalter\" \nwho chooses what to do with a bankrupt company's assets, AFAIK, \nso that suppliers' claims are satisfied as much as possible. \n(IANAL, though.)\n\n"
    author: "cm"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "I see your point.\nThis makes sense.\nI'll talk to Jesper and Kalle about it.\n"
    author: "David Faure"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "People are making decisions based on emotions and not logic. These are the GNU fanatic types."
    author: "Kraig"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "> Because people are worried.\n\nOh please, give us a break. I'm reading this thread and it's clear that absolutely none of the people complaining about this have any clue on regression testing or even software development in general. They are arguing for the sake of it.\n\nWe're extremely grateful that KDAB made this tool freely available, and we'll certainly be using it for Rosegarden (for the record, nobody in the rg dev team was stupid enough to complain about the license)."
    author: "Guillaume Laurent"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Well, I don't think we should get into arguments about licenses. If there is a need for a free version then there will be one. They will then just have to accept that they will sell less licenses as a result. If people like this and feel fine, then there will not be so much need for a free version because people will be happy. Make sense?"
    author: "David"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "Sounds good.\n\nI'm getting flamed here because I bring up concerns on just accepting proprietry tools in the development cycle.  And my concerns are practical.\n\nAs long as there are no patents in the fileformat, and it can be easily rewritten if the company goes bad, then sure, go for it.\n\n\n"
    author: "John Tapsell"
  - subject: "Re: Usefullness"
    date: 2004-04-09
    body: "I think you're underestimating the usefulness of this tool.  What if it came with every KDE installation and was activated by the Report Bug menu item?  Users could submit bug reports with full testcases, extremely easy to reproduce, with hardly any effort.  Also, KDE could have a suite of automated regression/crash tests using a tool like this.  Shame about it not being GPL though."
    author: "Spy Hunter"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "KD Executor has another kind of freedom you failed to mention.\n\n1) You are free to use it.\n\n2) You are also free not to use it.\n\nUnfortunately, KD Executor also takes away your freedom:\n\n3) You are not free to forbid others from using it freely.\n\nSo I hope you understand this, and are able to live with the fact that others may use this tool even though you yourself don't want them to.  A free world is sometimes hard to live in..."
    author: "ac"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "If you're thinking I was forbidding, then you are misunderstanding. I am simply voicing my concerns for our community. As a community however, we should stand as one and that is why it's necessary to have these discussions."
    author: "Martin Galpin"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Actually, it's more like he who codes (tests) decides."
    author: "ac"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Not to question where you are in the community, I'm just not familiar with you. On the other hand when you talk about who the people behind Klar\u00e4lvdalens Datakonsult AB are you are talking about people who have been central to the KDE community for years, who also happen to have a business in professional software development. They have been involved with a number of projects including the contracts by the German government where they contributed back to the community with the Kolab server and were involved with changes to Kmail and some of the work that lead to Kontact IIRC. One of their developers is the guy who came up with kparts, and if you know the history there and the work being done by some of these developers you'd probably be singing their praises for how key their contributions have been and continue to be for KDE.\n\nI am as big of an advocate of free, as in speech, as any reasonable person. It was really my love of free software that caused me to take my own money, at one of the most personally and financially difficult times of my life, and sponsor ongoing development of Quanta. As much as I believe in free software I don't doubt these people's motivations for releasing to us, free as in beer, for a second. As much as I wish all such tools arrived free, as in speech, I believe it is in my best interests as a KDE developer and user for these guys to be able to make such decisions on a business basis. The simple reason is, it helps enable them to continue to produce KDE software that is free, as in speech, which they have a long and distinguished record of doing.\n\nAll I see is a company that has put time effort and money into their passion for KDE and worked their contracts to enable them to return software to the community. In this case they saw this as the only way they could make the gift, and in fact they did gift it to us. If you have a problem with that fine. Don't use it. All I can look at is who is doing what, and the reputation and commitment to the community of these developers is as strong as they come. It really sucks to take your time and money, gift it to people in the form of software and then be criticized because it fails to conform to someone's ideals. The alternative here would be for them not to have gifted us.\n\nLet's keep in mind that what is being offered here is free access to a tool that is being commercially developed to help improve professional software development on our platform. Think about that. That brings users and developers to KDE. This can also help us improve free, as in speech, KDE software. When people I admire a great deal offer to help me for free I don't squabble about the fact that they need to make a living too.\n\nAs a KDE software developer I want to say thank you guys. I can appreciate the process you went through in this decision and that you didn't have to do it. I for one appreciate it. I encourage those who don't like it to exercise their freedom to ignore the gift of this software. I only hope doing so doesn't cause unnecessary bugs to be left in their software for their users to endure, assuming they are actually developing software.\n"
    author: "Eric Laffoon"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Thanks, Eric.\n\nIt's good to see that some people understand what a gift is.\n\nAll the best,\nDavid."
    author: "David Faure"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "You can understand and appreciate gifts at the same time as questioning their nature."
    author: "Martin Galpin"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "But doing that publically is highly unpolite toward those who a giving the gift. If you are questioning the nature of one's gift you can still reject it, dragging the sponsor into mud merely for \"questioning the nature of a gift\" is a joke."
    author: "Datschge"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Doing it publically is both inevitable and necessary, it's something we need to debate."
    author: "Martin Galpin"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "You are basing your argument on the unspoken assumption that they have nothing to gain from this.\nThey are probably doing this to advertise their product (not a bad thing).\n\n\nBesides, just because it's a gift, doesn't mean it's a good thing overall.\nTry giving linus a gift of code, and see if he'll add it to the kernel without interrogating the code.\n"
    author: "John Tapsell"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-11
    body: "Well, the \"unspoken assumption\" you mention is actually part of the meaning of the word \"gift\" at least where I live (Germany). Words (luckily) seems not to be watered down that much here, so for instance one isn't allowed to make ridiculous offers like \"buy two, get one for free\". \"Gift\" with the purpose of getting hidden gains to the donor are called \"Nepp\" which is a very negative term (closest translation would be \"rip-off\").\n\nAlso while discussing about \"gifts\" even with the extended range of meanings the term apparent has in English people should keep in mind that KD Executor is meant as a tool easing the testing of apps during development, so the whole \"clueless children are lurked into a trap\" freedom-rip-off case doesn't apply, neither are normal users a target nor does this app add a hard dependency without which a project would die.\n\nThe only valid issue I see is what's going to happen to KD Executor in the theoretical case of bankruptcy of KDAB, David Faure mentioned above they'll look at this issue and I trust them to find a good solution for that as well."
    author: "Datschge"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Hi Eric,\n\nvery well said. I think there is nothing much to add. You are doing a great job explaining the background and have the authority. \n\nThat said, you likely agree that nonetheless, either Trolltech themselves or KDE will come up with something like the too anyway. \n\nEverybody would have to admire the easy reproducing of bug reports and integration into KDE. Somebody in the community will want to replace it with something free sooner or later anyway.\n\nAnd that said, thank you for the tool. It's a help NOW.\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "It says exactly why it will not be GPLed, because you cannot sell GPL software for the software itself (not the support). Since anyone with the first GPL copy is free to release it and give it to others. It doesn't make sense commercially.\n\nI think the anti-commertionasim attitude in the \"open\" source community is immature at best. Remember that those people working in the company need to pay their bills and support their family, GPL doesn't give them that.\n\nThe only (or main) method to generate income from a GPL software is from support/training contracts. To suggest some company can sell a GPL software for the software itself is a complete joke.\n"
    author: "Jasem Mutlaq"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "For some people, to suggest some company can sell software for the software itself (open or closed) is a complete joke. That people see pure software companies as an anachronism. "
    author: "Cloaked Penguin"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "And it's also entirely unethical, of course :)"
    author: "Guest"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "To be blunt: I couldn't care less if it's open source. If it is useful I will use it. If someone writes something that does the job decently that is open source then I might switch.\n\nThe problem with GPL-ing a tool like this is that people would use the GPL version and not pay. It's not like a library where it is linked into the distributed code, so there is no way to prevent people ripping you off.\n"
    author: "Richard Moore"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "...and to be blunt: \"Value your freedom, or you will lose it, teaches history. ``Don't bother us with politics,'' respond those who don't want to learn.\" (RMS, http://www.gnu.org/philosophy/linux-gnu-freedom.html)."
    author: "Martin Galpin"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "Yeah, but I don't think open-ness of code is a moral issue. It's nice sure, but hardly essential. I use the GPL and LGPL because they're convenient, not because I agree with RMS and philosophy.\n\n"
    author: "Richard Moore"
  - subject: "Re: This is certain to come up (again and again)"
    date: 2004-04-09
    body: "...and to add something to it: \"Value your friends, or you will lose them\".\n\nYou are either from outside anyway or you have no idea how misplaced you sound by stating this kind of quotes."
    author: "Datschge"
  - subject: "Squish"
    date: 2004-04-09
    body: "How does this compare to Squish (http://www.froglogic.com/squish)? Do they compete or complement each other?"
    author: "Anonymous"
  - subject: "have something similar"
    date: 2004-04-09
    body: "I've had similar functionality for a while now. I started implementing the\nevent trail record/playback functionality back in 2001, or maybe earlier.\n\nIt is rather easy to trail-enable each Qt application. Basically all you \nhave to do is inherit QApplication and override the notify(..) function.\n\nThere you can watch for events that are directly generated by the user,\nsuch as mouse moves & clicks, keyboard presses, window resizing, \nwacom tablet events, etc... The events that are directly generated can\nbe saved to a file. Later, the application can be rerun and play back the\nsaved events, thereby reproducing a crash or whatever. I use this all the\ntime in the program I am working on.\n\nCheck it out if you care (Please don't kill my DSL line)\nhttp://www.aragog.com/~paul/homedir/cxx/bernstein/the_trail.hxx\nhttp://www.aragog.com/~paul/homedir/cxx/bernstein/the_trail.cxx\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: have something similar"
    date: 2004-04-09
    body: "This looks interesting, I'll check it out.\n"
    author: "Richard Moore"
  - subject: "Deciding on Free/Open Source"
    date: 2004-04-09
    body: "Guys, if you don't believe in the GPL, at least have the guts to say so.  No one's convinced by this \"we wanted to, but just couldn't\" approach.  Everyone releasing Free software weighs the pros and cons.  The difference is that some of us aren't as self-motivated as others."
    author: "Guest"
  - subject: "Re: Deciding on Free/Open Source"
    date: 2004-04-09
    body: "I'd like to remind you that KDAB provides two of its commercial components, KDChart and KDGantt, as GPL libraries.\nFor libraries it's quite doable, to release them as GPL, and sell it for commercial application developers. Everyone wins.\n\nBut for an application? If KD Executor was GPL, we wouldn't sell it at all anymore. The GPL doesn't make any difference between \"using the tool for free software\" and \"using the tool for commercial apps\"...\n\nSo don't give me \"you don't believe in the GPL\" or \"self-motivated\" arguments.\nApparently you are one of the few people who work on free software all day, all week and doesn't care about eating and paying your rent? Yeah right.\n\nIt's quite funny to see someone question the self-motivation (related to KDE) of Kalle (president of kde-e.v., kde developer since the very beginning)\nand his employees (we're all KDE developers!)."
    author: "David Faure"
  - subject: "Re: Deciding on Free/Open Source"
    date: 2004-04-09
    body: "Well, I think that saying that a company like Klar\u00e4lvdalens Datakonsult doesn't respect the GPL, given their track record of KDE development, is rather crap to be honest. Software has to be funded and has to have a working business model - whether through a community effort or through a licensing type approach.\n\nFor this reason I hope that Trolltech never LGPLs Qt for any reason whatsoever. They could think about a small developer addition, but if I'm developing my own proprietary software I want to make sure Qt continues to be really actively developed."
    author: "David"
  - subject: "Limited use"
    date: 2004-04-09
    body: "The problem with testing GUI apps isn't in being able to push the buttons automatically.  It's that there are an almost infinite number of possible combinations of button presses.  Recording a particular sequence of presses solves very little of this issue.  You either need to push every available button in every possible window in a random manner, or you need to release it to a large audience with random intentions."
    author: "Guest"
  - subject: "Re: Limited use"
    date: 2004-04-09
    body: "I agree that it'd be a lot of effort to cover a GUI app thoroughly \nusing those automated tests.  I colleague of mine has a similar \nproblem testing a web app. \n\nBut I still think it is a useful tool:  \n- It ensures that the normal functionality is in order. \nGrave errors will pop up early.  \n- Every time a bug from bugs.kde.org is fixed a new test could \nbe created to \"prove\" it and to make sure it stays fixed. \n- It's probably helpful during debugging if a bug is reproducible \n  but requires a lot of steps to do so. \n\n"
    author: "cm"
  - subject: "Oh Dear"
    date: 2004-04-09
    body: "When I see flamewars like this I'm none too happy about the future of free and proprietary software. We aren't going to see free software absolutely everywhere and proprietary software developers must realise that they do not have a divine right to sell software. If there is demand for a free version, there will be one, no question about that. Software does need to be funded, whether that is with money or with spare time and hard work. It isn't free, but people only ever see the money.\n\nI'm rather disappointed that people cannot use a bit of software without thinking that there is some massive conspiracy theory, and it doesn't make me hopeful of the future. Do we want software companies to write proprietary software on KDE, as an extension to the great free software we have?"
    author: "David"
  - subject: "Re: Oh Dear"
    date: 2004-04-09
    body: "No."
    author: "Debian User"
  - subject: "Re: Oh Dear"
    date: 2004-04-09
    body: "Of course we do.\nWould you rather they keep developing software on Windows?\n\nI'm happy everytime I see a new commercial product for KDE. It proves KDE is a good desktop, and good development framework."
    author: "David Faure"
  - subject: "Re: Oh Dear"
    date: 2004-04-09
    body: "### I'm happy everytime I see a new commercial product for KDE. It ###\n### proves KDE is a good desktop, and good development framework. ###\n\nI fully agree. \n\nRegarding \"aKaDEmy\", I am thinking of even including a special section/track\ninto the 2nd weekend (\"User and Administrator Confernce\") sessions and the \nrelated Call for Papers to highlight such \"ISV\" commercial product developments \nusing Qt and/or KDE. There are lots and lots of them, we just need to get a few\nto showcase their products during the event with a presentation to make the\npublic more aware of it. It would also help to present Linux in general as\na viable desktop system.\n\nI hope there will be some submissions covering this aspect once the CfP is\nout. I hope each of you who has personal contacts to ISV Qt/KDE developers\nwill appruach them in person and point them to the CfP...."
    author: "Kurt Pfeifle"
  - subject: "Re: Oh Dear"
    date: 2004-04-09
    body: "Well I can tell you that I would love to see more proprietary software written with Qt and for KDE. Qt is a fantastic development toolkit, and hopefully, with all the great freedesktop stuff like QtGTK etc, and a way of making Java apps fit in we will have development options in KDE for more than just Qt.\n\n\"Would you rather they keep developing software on Windows?\"\n\nI develop on Windows, and frankly, I'm looking to see the back of it at any point in the future. When Longhorn comes around we are going to see many companies faced with a difficult decision about where they go, and they are going to be forced into that decision. It isn't going to be pretty, and it isn't going to do the IT industry any favours but it needs to happen. I hope there will be great free software alternatives with the ability for sustainable and sensible proprietary software to thrive."
    author: "David"
  - subject: "THANK YOU!"
    date: 2004-04-09
    body: "I think KDAB has done a great thing by presenting KD Executor as a gift for OSS. I know that the tool is proprietary, yet as long as it is used to make OSS I would not mind. The most important goal is to improve OSS like KDE, hwo you get there is less significant.\n\nI also think we can trust KDAB. As David Faure has said over and over again, the company is composed of KDE developers, there is no reason for them to act against KDE. Event he president of KDAB is the president fo KDE e.V. so there is nothing to worry about yet. My only concern is about the future, what if the company goes out of business, what if it has an ownership or so many more people are hired that only a minority is composed of KDE developers. These are the fears everyone has when there is a commercial entity. Therefore, I STRONGELY SUGGEST that if KDAB wants OSS to stop being reluctant on using their tools simpyl because KDAB is a proprietary company they should make a pact which states that OSS will ALWAYS have free acess to KD Executor even if ownership changes. Also, if the company goes out of business, KD Executor needs to be released as OSS. (LGPL, GPL, BSD?) I am aware that this would mean that the president of KDAB would be signing a pact with himself and much of KDAB would be signing the pact with themselves, yet this is the only way to be able to fully use KD Executor and maybe even establish a framework on it with no worries.\n\nOr maybe the easiest thing to do is just to release KD Executor under a dual license as Trolltech has done for Qt. GPL for OSS and proprietary for proprietary software. If the company goes out fo business or changes ownership in the distant future, there would still be the GPL version. THIS IS THE PROVEN METHOD TO WORK and BEST FOR BOTH WORLDS. KDAB will get much more publicity, testing and widespread use of their tools while KDE developers will get better tools to make KDE releaseas of even higher quality.\n\nTHAN YOU AGAIN!\n\n "
    author: "Alex"
  - subject: "Re: THANK YOU!"
    date: 2004-04-09
    body: "> Or maybe the easiest thing to do is just to release KD Executor \n> under a dual license as Trolltech has done for Qt. \n> GPL for OSS and proprietary for proprietary software. \n\nI'm afraid that model doesn't work here.\nThere's an essential difference between Qt and KD Executor: \n\nQt is a library that is *linked to* when creating products. \nA company needs to buy a commercial license in order to \nwrite closed-source software (== income for TT). \n\nKD Executor *is* a product that can be *used* without such restriction \neven to auto-test closed source software (== no income for KDAB if it were GPL)\n\n"
    author: "cm"
  - subject: "Re: THANK YOU!"
    date: 2004-04-09
    body: "To make it more clear:  \nThere is no such thing as a \"GPL for OSS\". \nIt's either GPL or it's not.\n\nThe GPL does not discriminate against the *use* of \nGPLed tools in closed-source software development.\n\nImposing such an *additional restriction* would itself violate \nthe GPL and would thus create tons of new license problems. \n"
    author: "cm"
  - subject: "Can't they invent a new license?"
    date: 2004-04-10
    body: "A license that is the LGPL but only if the code it is used to test is open source. Or would that be relying too much on people's honesty? That would be impossible to enforce I guess... "
    author: "Alex"
  - subject: "Re: Can't they invent a new license?"
    date: 2004-04-10
    body: "Then that wouldn't be the LGPL.  You can't add additional restrictions to the license and still call it LGPL, and by the FSF's definitions it wouldn't even be Free Software with your suggested modifications."
    author: "Scott Wheeler"
  - subject: "Grmbl"
    date: 2004-04-09
    body: "We have a bunh of free equine orthodontists in the house!\n\nThis thread just pissed me off. Ergo, I intend to write a tutorial on the use of this thing as soon as I come back, and evangelize its use to hell and back.\n\nWhatever it is that this thing does ;-)\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Grmbl"
    date: 2004-04-09
    body: "How pissed are these free equine orthodontists? Do they just get their kicks by complaining about stuff, or are they willing to put their money where the horse's mouth is? If the latter, I am willing to create (or help create) an open source testing and automation tool, if people will pay me to do so. And to avoid all \"GPL is hostile to business\" arguments, I would release it under the BSD license. If you're serious about your concerns, contact me off-forum."
    author: "David Johnson"
  - subject: "autogenerated screenshots"
    date: 2004-04-17
    body: "could kdexecutor be used to automate the generation of screenshots?\n\ni guess, it would need some sort of \"break point\" support or something like that\n\nhhm..."
    author: "bangert"
---
<a href="http://www.klaralvdalens-datakonsult.se/">Klarälvdalens Datakonsult AB</a> has released <a href="http://www.klaralvdalens-datakonsult.se/?page=products&sub=kdexecutor">the first beta version of 
KD Executor 2.0</a>, our tool for testing and automation. KD Executor is a record and playback tool for Qt and KDE
applications. In addition, it contains a test environment which uses
this record and playback tool for testing Qt and KDE applications. We
are proud to release a free version (free as in beer, not as in speech) of this tool to the KDE community. 

<!--break-->
<p><b>Why free as in beer and not free as in speech?</b></p>
<p>Klarälvdalens Datakonsult AB has always been very dedicated to Open Source
Software development, all of our employees are involved in KDE development,
and most have a record of open source involvement in the order of magnitude
of 10 years.</p>

<p>Much of our software is dual-licensed under the GPL and a commercial
license, and therefore it has been a great worry for us that we have
so far not been able to make KD Executor available under GPL.</p>

<p>We have reached the conclusion that this is likely never going to
happen, as this would make it very difficult, if not impossible, for
us to sell any commercial licenses, but fortunately we have found a
way to still make it available to our fellow KDE programmers.</p>

<p>To use it you need to set a license key (the version will expire in three
months at which time the final 2.0 version should be released; that
version will not be time-bombed).
To use this key, put the following lines in your .zshrc or equivalent 
<b>export KDABEvalKey=69e9ec5ff9e9689c</b></p>

<p><b>KD Executor mailing lists</b></p>

<p>Those of you with an interest may wish to <a href="http://www.klaralvdalens-datakonsult.se/mailman/listinfo/kdexecutor-interest">join the mailing list</a> dedicated to KD Executor discussions.
