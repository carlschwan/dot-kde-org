---
title: "aKademy Hosts First Unix Accessibility Forum"
date:    2004-08-26
authors:
  - "ateam"
slug:    akademy-hosts-first-unix-accessibility-forum
comments:
  - subject: "KDE TTS"
    date: 2004-08-26
    body: "If you are interested in what is happening on the KDE Text-to-Speech synthesis front, go here:\n\n  http://accessibility.kde.org/developer/kttsd/index.php"
    author: "Gary Cramblitt (aka PhantomsDad)"
  - subject: "Re: KDE TTS"
    date: 2004-08-26
    body: "It would be fantastic if Konqueror support speech synthesis using kttsd as the last Opera will do.\n\nkhmtl-plugin can do this but I am talking of voice-* CSS attributes.\n\nhttp://my.opera.com/community/dev/voice/\nhttp://www.voicexml.org/specs/multimodal/x+v/12/spec.html\nhttp://www.w3.org/TR/css3-speech/\nhttp://www.ibm.com/software/pervasive/multimodal/demos.shtml"
    author: "Shift"
  - subject: "Translation"
    date: 2004-08-26
    body: "Currently no OSS corpus based rough translation engine exists. This is very sad. We often use Systran (=Babelfish), but I think automatic translation could benefit a lot from the open contribution model.  "
    author: "gerd"
  - subject: "Opencyc"
    date: 2004-08-27
    body: "http://www.opencyc.org/\n\nPerhaps it is an option to use a database like cyc to model knowledge about  KDE. This could improve help functions. Integration of a gigantic project like opencyc may be very fruitful for KDE and Accesibility."
    author: "Frankie"
  - subject: "Logical desktop"
    date: 2004-08-27
    body: "http://logicaldesktop.sourceforge.net/"
    author: "Frankie"
---
On Sunday and Monday the first <a href="http://accessibility.kde.org/forum/">Unix Accessibility Forum</a> took place 
 at <a href="http://conference2004.kde.org/">aKademy</a> and was said by participants to be extremely successful. 
 The most notable thing was that amongst all participants there was a good spirit of cooperation and 
consensus that standards for assistive technologies 
 would ensure success in the accessibility of graphical user interfaces like <a href="http://www.kde.org/">KDE</a>.








<!--break-->
<p><IMG src="http://accessibility.kde.org/images/kdeap_logo2.png"  align="right" border="0"></p>

<p>The forum, organised by the <a href="http://accessibility.kde.org/">KDE Accessibility Project</a>, had representatives 
from IBM, Sun Microsystems, Novell, GNOME, Trolltech and Free Standards Group and others. Several projects and companies were 
given the opportunity to discuss these standards for assistive technologies like X Accessibility, assistive device 
support, speech synthesis and more.</p>

<p>In the slipstream of this event several interviews were conducted, one of which has already been published on 
<a href="http://news.com.com/Open-source+developers+focus+on+accessibility/2100-7344_3-5323812.html?tag=nefd.top">CNET</a>. </p>





