---
title: "KDE Wins 2003 LinuxQuestions.org Members Choice Awards"
date:    2004-01-11
authors:
  - "wbastian"
slug:    kde-wins-2003-linuxquestionsorg-members-choice-awards
comments:
  - subject: "Kmail"
    date: 2004-01-11
    body: "KMail came second, just beat by evolution, only 34 votes in it."
    author: "Leon Pennington"
  - subject: "Also Strong"
    date: 2004-01-11
    body: "Konqueror immediately behind Mozilla stuff, KOffice second behind OpenOffice.org and kwin as second best window manager. :-)"
    author: "Anonymous"
  - subject: "Re: Also Strong"
    date: 2004-01-11
    body: "kwin would have won if more KDE users actually *knew* they are using it.  With KDE+kwin, everything just works so well that you don't even know kwin is there (unless you need to know)."
    author: "Haakon Nilsen"
  - subject: "Kate"
    date: 2004-01-11
    body: "I forgot to mention kate, came second to vi, beating emacs into third. More of a gap, but then kate is GUI."
    author: "Leon Pennington"
  - subject: "Re: Kate"
    date: 2004-01-11
    body: "yes, i hope they split GUI and non GUI editors next year. "
    author: "anon"
  - subject: "lq_2003_awards"
    date: 2004-01-11
    body: "As an LQ member, (over 90,000 members now) just like to say, thanks to everyone at KDE for the D.E - glad the result came through on our site - 3.1.4 on Slack is awesome - thanks again  - the 3.2 / 2.6 combination is gonna fly........."
    author: "lq_addict"
  - subject: "I'm a LQ member too..."
    date: 2004-01-11
    body: "I'm a LinuxQuestions member too...\n\nBut, what supprised me where the comments: A lot for Xcfe- and GNOME'ers took the courage to, not only vote, but also write a comment in which they recommend/ explain their choise.\n\nLuckily the poll was only for members, and one can vote only once :-)\n\n\n> the 3.2 / 2.6 combination is gonna fly.........\n:-)"
    author: "cies"
  - subject: "Re: I'm a LQ member too..."
    date: 2004-01-11
    body: "Can't help feeling that some of the people who voted for XFce were just jumping on the bandwagon - it might be responsive when doing simple things, but its also primitive, buggy and crashes when doing more \"flexible\" things - why struggle with it when everything's to hand in KDE/Konqueror (it has improved a little to be fair to the deveopers though, and good luck....)\n\nAs a general point, it'll be interesting to see how people re-evaluate the KDE/GNOME/XFce/others on the various Linux forums over the next 6 months as 3.2 comes into play :-) - fair chance that a few old myths will be quashed......"
    author: "lq_addict"
  - subject: "Re: I'm a LQ member too..."
    date: 2004-01-12
    body: "KKDE is also buggy, crashes sometimes. But - it`s not primitive and therefore the winner."
    author: "author"
  - subject: "Bluefish vs Quanta"
    date: 2004-01-11
    body: "I haven't really used Bluefish, but how is it different from Quanta apart from using GTK+? \n\nAlso, is it just me or does the Bluefish website (http://bluefish.openoffice.nl/) look 10 times better than Quantas (http://quanta.sourceforge.net/)?"
    author: "Dan"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "Yep, their site is very nice!"
    author: "Jilks"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "> I haven't really used Bluefish, but how is it different from Quanta apart from using GTK+? \n\nI haven't used Bluefish much. It's actually at least a year or two older than Quanta. At least recently the question would include which version of GTK you were building it for because different feature sets and stabilities were available on the different ports. I believe they pretty much do the whole program where as Quanta leverages KDE's better object architecture so we use Kate's editor part and KHTML for preview. Bluefish got some recognition and respect early on while Quanta was younger but people who have used both tell me there is currently no comparison. Of course you'd expect that as they're using Quanta.\n\nThe key difference between the two in my opinion is in fundamental design approach. Quanta uses a highly optimized parser to deliver a number of features in seamless and consistent performance. Bluefish introduced similar features which I don't think have the same depth in integration. For instance in Quanta you can add support for a new programming language with an XML file and you can read in a new XML DTD directly. Bluefish is not as versatile here. Quanta also uses plugins for XML, XSLT debugging, imagemap editing, mutli-file find and replace, CVS management and other features with kparts. It also provides dialog building with Kommander and extensive scriptability including DCOP information interchange. Quanta is highly focused productivity as well as supporting diverse users. Quanta is also developing visual page design facilities based on KHTML. Simply put, Quanta is enjoying the huge advantages KDE offers for application design. That is why we are developing on KDE in the first place.\n\nI really doubt that this contest will be anywhere near as close in 2004 because we have not been focusing on just adding this or that. We have been focusing on  very carefully planned design that allows us to leverage many new features. Expect the next major version of Quanta to dramatically mature.\n\n> Also, is it just me or does the Bluefish website (http://bluefish.openoffice.nl/) look 10 times better than Quantas (http://quanta.sourceforge.net/)?\n\nWhat can I say? A site redesign is in the works for the coming month but for the most part we have been focusing more on building the tool than just building a web site. I think that shows. We are getting the nod from users. You're going to be looking at Quanta a lot more than our site... I hope."
    author: "Eric Laffoon"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "Full ack from me as somebody who used Bluefish before. It is not bad, but quanta has a powerful feel.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "I just tried Bluefish, it's ok, but it feels slower, I don't know if it is just the program or GTK itself and I haven't found anything that I need which Quanta does not already have. There are some things that I wish Quanta would have but Bluefish doesn't have either, such as a WYSIWYG mode, but I hear it is in the works, not sure about Bluefish.\n\nI also would much rather have a program well integrated into KDE and using the KDE framework than one that reinvents the wheel because this way, many of Quanta's parts will improve without special effort from Quanta developers, such as the KHTML component.\n\nAlso, I know that building the program is more important than building the website, but the website of a tool used to make websites could to some people reflect the quality of the program. Now I know that Quanta is great, but looking at the website I had a much lower opinion of it.\n\nAnyway, THANK YOU!"
    author: "Dan"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-12
    body: "> I just tried Bluefish, it's ok, but it feels slower, I don't know if it is just the program or GTK itself\n\nThere is no guarantee of speed in a program without the designer giving it attention. KDE deserves more credit than I have room for here since you can't go faster than what you're made of, but the real hero here is Andras Mantia. He devoted several man months in 2003 optimizing the parser, looking for memory leaks, reducing CPU idle usage and generally making quanta feel crisp. At the same time KDE internals have been consistently improving. Additional performance has been coming from improvements in gcc. KDE has been benefitting from evolutionary refinement, as opposed to revolutionairy redesign, for years now.\n\nPeople love to throw the word \"bloat\" around but the irony is that we have been falling behind on our feature schedules to insure our performance. One of the first things people tell me trying out Quanta is how crisp it feels, yet I think it has more going on underneath than any other tool of it's type. \n\nBeer is sold with catchy slogans and 30 second sound bites. Good software requires a lot more careful thought and unlike beer it actually can be less filling and taste better.\n\n> and I haven't found anything that I need which Quanta does not already have. There are some things that I wish Quanta would have but Bluefish doesn't have either, such as a WYSIWYG mode, but I hear it is in the works, not sure about Bluefish.\n \nThere are other features too which I think will prove even more exciting. However I'd like to make sure that VPL (Visual Page Layout) is not taken lightly. It's a huge undertaking and it's only possible because 1) we have KHTML and 2) the KDE architecture makes it straight forward to extend it by subclassing. It takes many man years to develop a good HTML rendering engine so finding one is the only timely option. If we were to copy KHTML we would fork it and then be stuck with a maintenance headache. VPL is possible because KHTML developers are making a public interface to the edit features we need. We dynamically build on top of that, extending the same software you view web pages in. \n\nIt's easy to say \"so what, I knew that\" but even with that there are numerous factors integrating it with Quanta. The layer of code that interfaces Quanta and KHTML, as well as the interface being built in KHTML has taken several people the better part of a year to get to a basic level of service. Quanta and KHTML both have maturity that makes this practical after years of development. I should mention that even changes in Kate have critical to facilitate VPL. So without a lot of things coming together as they did we would be years from behind where we are.\n\nWhen you use Quanta with VPL remember that no other grass roots Linux tool has this. You could draw a page in Mozilla's editor or Star Writer but you cannot consider them serious web development tools."
    author: "Eric Laffoon"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-12
    body: "\"When you use Quanta with VPL remember that no other grass roots Linux tool has this. You could draw a page in Mozilla's editor or Star Writer but you cannot consider them serious web development tools.\"\n\nWhile it's not yet finished, Nvu (http://nvu.com/) seems to be an enhanced Mozilla-composer. I don't know if it will be able to \"match\" quanta. It just came to my mind that it exists (will exist?).\n\nciao\n"
    author: "Birdy"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "Hey Eric,\n\nDon't forget my offer!"
    author: "cwoelz"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "It's on my do list. I'm actually formulating the initial foundation plan outlining requirements, thumbnailing content and assigning team responsibilities. Once I rebuild the server I have my site CVS on I will begin assembling team members and giving them accounts. I have been less active than usual the last week with an ear infection so I'm another week behind schedule."
    author: "Eric Laffoon"
  - subject: "Re: Bluefish vs Quanta"
    date: 2004-01-11
    body: "I think it would be very professional if you did the quanta site redesign using the new kafka html editor -- if you're not really into the complete manual approach or not too far along with the redesign of course.  This way there is easy proof of its power and usefulness.  It also shows that you trust it and are confident in its use.  Hmm ... perhaps it's already being used ;)  Nice work, I love quanta and am thouroughly pleased it beat bluefish."
    author: "Jesse"
  - subject: "It was about time"
    date: 2004-01-11
    body: "Quanta is a great editor and wasn't getting all the prizes it deservers.\nThe kate/kwrite foundation it's build over, plus nice tag editing and good php support just makes this program rocks."
    author: "Iuri Fiedoruk"
  - subject: "Re: It was about time"
    date: 2004-01-11
    body: "Thank you. Obviously we love Quanta because it's our baby. We're actually very critical of it because we can never come close to building what we want as fast as we think of it. It's nice to have people say good things about your project but I pretty much do the same thing as when everybody said it was dead. When I get done looking around I put my head down and go to work because it still doesn't match the vision in my head. It will be fun to look at this in another year though. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: It was about time"
    date: 2004-01-12
    body: "Hi\n\nI have never used quanta but thanks anyway. I took a look at it sometime back and its nice to see the amount of work that has gone into it. I really hope the VPL stuff would come up quickly so that i can start designing pages more visually\n\nA few of the developers like you and Aaron Siego really hang out a long time in the mailing lists and other forums like the dot. I appreciate it\n\nregards\nRahul Sundaram"
    author: "Rahul Sundaram"
  - subject: "Re: It was about time"
    date: 2004-01-12
    body: "Thank you. That's very kind of you. I really like the Dot, but there are times I throw my hands up in the air and realize I have to get back to work and not spend forever here. However when Quanta is in the story line I make sure to watch so I can answer questions. BTW Aaron is one of my favorite KDE answer guys too. One of these days he'll answer my last email and I'll find out if he lives close enough we can get together and hang out some time. ;-)"
    author: "Eric Laffoon"
  - subject: "Thanks!"
    date: 2004-01-12
    body: "Thank for all of you who supported Quanta+ (and I'm not talking only about this poll).\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Thanks!"
    date: 2004-01-12
    body: "> Thank for all of you who supported Quanta+ (and I'm not talking only about this poll).\n\nMe too?\n\n;-)"
    author: "Eric Laffoon"
  - subject: "Re: Thanks!"
    date: 2004-01-12
    body: "Congratulations to you all:\n\nAndras, Eric, Nicolas, Jan, the KHTML team (thanks to you too, Apple developers) and the Kate team.\n\nYou really really rock, and I am proud to be part of this community."
    author: "cwoelz"
  - subject: "2002 / 2003 comparison"
    date: 2004-01-12
    body: "OK, let's see how KDE is doing, even considering that last release was a year ago, and that KDE 3.2 is not released yet:\n\nDesktop environment: KDE\n2003: 55.93%\n2002: 59.14%\nComment: Not bad, considering all the bashing KDE got and that XFCE went fro, 0% to 11%. They took fare more from other desktop environment.\n\nWeb Editor: Quanta\n2003: 49.26%\n2002: 47.65%\nComment: Quanta took the lead, and Quanta 3.2 is not out yet. Congrats!\n\nMail: Kmail\n2003: 24.21%\n2002: 26.76%\nComment: Yes, but the distance to the leader (Evolution) is smaller now, as tunderbird emerged as a contender and took votes from all others (but less from Kmail). And Kontact will have two releases this year. I am curious to see what next year will bring! Kontact rock!\n\nBrowser: Konqueror:\n2003: 13.56%\n2002: 15.61%\nComment: We have to work on that. KHTML rocks. It is fast, reliable and full featured. We have a world class engine, and the technology to build applications fast, based on kparts. A browser task oriented app could kill here: just see how firebird took the lead. But, wait, why don't we start it now? ;)\n\nOffice:\n2003: 7.85%\n2002: 10.16%\nComment: Koffice has a great future, as it is lean and is going for a standard formats where available. Also, koffice has _great_ applications that are not very well known. It will be a great and competitive office, so don't expect world dominance today. OpenOffice and Abiword/Gnumeric are also great. Thank you to all koffice developers. Let's see what we can do in the poll next year, as Koffice 1.3 is fresh. I think if koffice is _stable_ enough, it will do much better, as it offers today great features already.\n\nAll in all, a great year for KDE, and I think 2004 will be even better."
    author: "cwoelz"
  - subject: "Re: 2002 / 2003 comparison"
    date: 2004-01-12
    body: "\" Comment: We have to work on that. KHTML rocks. It is fast, reliable and full featured.\"\n\nand not to forget, it's the buggiest rendering engine around."
    author: "author"
  - subject: "Re: 2002 / 2003 comparison"
    date: 2004-01-12
    body: "KHTML is currently being developed by KDE hackers and Apple.\nThe KDE developers are working in a testing framework, and it is already working very well.\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/khtmltests/\n\nExpect the (few) existing bugs to be hammered out during the point (3.2) releases, and KHTML in the next KDE (3.3?) will be even more reliable. Report the bugs you find in bugs.kde.org. If you provide a testcase, it will be added to the testing framework.\n\nI guess you are not interested in all this info, but maybe other people are."
    author: "cwoelz"
  - subject: "Re: 2002 / 2003 comparison"
    date: 2004-01-12
    body: "> I guess you are not interested in all this info, but maybe other people are.\n\nYour information is good, but he is just trolling. Most of the Apple developers were involved with and fond of Mozilla but still chose KHTML as the rendering engine. I won't go into why. I will just say that when you look at the number of developers on KHTML and the time in development it is nothing short of astonishing how far it's come. It is probably the youngest browser engine contesting a top spot and it works very well. The few pages I've had any trouble with were hideous and generally suffered from poorly written Javascript or other problems. Ironically KHTML seems to compensate for things I would have guessed would break it. \n\nIt's easy to spout off anonymously, but as a real person here's an indisputable fact. The most popular rendering engine out there is so buggy it even has petitions from users over things that haven't been fixed for years... even though free solutions are available and they have billions in the bank.\n\nhttp://www.petitiononline.com/msiepng/petition.html\n\nFor the uninitiated, you can write W3C compliant pages to break ANY browser. There are two sides to any equation. If rendering engines required strict W3C compliance to render then most of the net would not render! It is because everybody would consider THAT a bug that rendering engines are so complex to write.\n\nMajor props to all the KHTML team. You guys rock!"
    author: "Eric Laffoon"
---
KDE won the 
<a href="http://www.linuxquestions.org/questions/showthread.php?s=&threadid=116365">
Award for Desktop Environment of the Year</a> in the 2003
<a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>
<a href="http://www.linuxquestions.org/questions/forumdisplay.php?forumid=37">
Members Choice Awards</a> with a convincing 55% of the votes.
Quanta also won an award in the 
<a href="http://www.linuxquestions.org/questions/showthread.php?s=&threadid=116384">
Web Development Editor of the Year</a> category with 49% of the votes. Congratulations go to everyone who has helped to make this software such a success and a big "Thank You" goes to all of our users who have taken the time to vote for their favourite desktop. With KDE 3.2 coming out soon we hope that we can make 2004 an even bigger success for KDE than 2003 has proven to be!

<!--break-->
