---
title: "KDE-CVS-Digest for June 25, 2004"
date:    2004-06-26
authors:
  - "dkite"
slug:    kde-cvs-digest-june-25-2004
comments:
  - subject: "Thanks Derek !"
    date: 2004-06-26
    body: "As everybody, I have to thank you for this nice digest, I'm not on kde maillist, but enjoy the digest, and compile KDE from CVS almos every week.\n\nThanks !!"
    author: "First Thank ?"
  - subject: "OT: Yahoo will make KDE-based Messenger"
    date: 2004-06-26
    body: "Mail from their main yahoo-messenger-for-unix-hacker:<br/><br/>\n\n<a href=\"http://groups.yahoo.com/group/ymessenger/message/2230\">http://groups.yahoo.com/group/ymessenger/message/2230</a>\n<br/><br/>\n\noh, and thanks to Derek for his work!\n"
    author: "me"
  - subject: "Very happy"
    date: 2004-06-28
    body: "I always wanted the Python bindings to be in the official KDE and this is great news as currently the ones I downloaded are not working. =( Anyway, python and Python Qt are simply amazing and I hoep their use in KDE and eslewhere will rise dramtically.\n\nI'm also glad that YahoO! is diching their hideous and dysfunctional GTK 1.2 client and rewritting it for KDE =) I'm sure they will be pleased with Qt as well as KDE's ease of development. "
    author: "Alex"
  - subject: "Re: Very happy"
    date: 2004-06-28
    body: "A whole bunch of packages for PyQt/PyKDE have been release lately. Try download again (or just wait for 3.3 to appear, that's if you _can_ wait. :-) )<br/><br/>\n\n<a href=\"http://sourceforge.net/project/showfiles.php?group_id=61057\">http://sourceforge.net/project/showfiles.php?group_id=61057</a>\n<br/><br/>\ncheers,\n<br/><br/>\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "PyQT/KDE: it's about time!"
    date: 2004-06-28
    body: "Finally! I've been waiting for this day for at least two years. I look forward to a flood of KDE-ized Python apps. "
    author: "Rodion"
  - subject: "window rules..."
    date: 2004-06-28
    body: "Are great! A bit of documentation would be good though ;)"
    author: "m."
  - subject: "Re: window rules..."
    date: 2004-06-29
    body: "You are more than welcome to help with it :)."
    author: "Lubos Lunak"
  - subject: "Re: window rules..."
    date: 2004-06-30
    body: "Yeah, but before I have to understand how it works :)"
    author: "m."
  - subject: "KDE_3_3_0_BETA_1"
    date: 2004-07-01
    body: "This has now been tagged as stated in the CVS Digest.\n\nI updated to this with CVS and several packages wouldn't build.  I'll have to try again to see which ones were hard errors and which ones were just being difficult.\n\nI also notice that KDE-3.2.3 is rather buggy to the point that KDE-3.2.4 is needed.\n\nSo, I wonder, are we in a hurry to kick KDE-3.3.0 out the door?  Is this a good thing?  If it fixes all of the new bugs (regressions) in KDE-3.2.3 then I suppose it is.  But, we need to be careful because my impression is that KDE's quality control has been slipping in the last few releases.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: KDE_3_3_0_BETA_1"
    date: 2004-07-01
    body: "> I also notice that KDE-3.2.3 is rather buggy\n \n Examples?\n \n > So, I wonder, are we in a hurry to kick KDE-3.3.0 out the door? Is this a good thing?\n \n The competition doesn't sleep. And all the new kdepim 3.3 features have to be released soon."
    author: "Anonymous"
  - subject: "Re: KDE_3_3_0_BETA_1"
    date: 2004-07-03
    body: "> Examples?\n\nI have filed some bug reports and added my CC to ones that already existed.\n\nI have been advised that the 3.2 branch will be closed so I was waiting to test the 3.3.0 BETA before filing more.\n\nThere are some problems with KWin focus behavior and I have had problems changing file permissions with a KFM window opened as SU -- theses are the two that come to mind.\n\n> The competition doesn't sleep.\n\nYes, but I regard Windows as the competition.  For many users the main complaint with Windows is that it is buggy.  Therefore, I consider a bug free KDE more important than a lot of new features.  The issue is how to release new features without the product becoming more buggy.\n\nEspecially important is to avoid new releases with regression bugs -- new features that don't work quite correctly are much less of a problem as long as you can turn them off.  \n\nWe have had very noticeable (and therefore serious) regressions in the 3.2 branch and this should be avoided in the 3.3 branch.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
In <a href="http://cvs-digest.org/index.php?issue=jun252004">this week's KDE-CVS Digest</a>: <a href=" http://www.python.org/">Python</a> bindings for <a href="http://www.trolltech.com/products/qt/index.html">Qt</a> and KDE are now in <a href="http://developer.kde.org/language-bindings/">kdebindings</a>.
<a href="http://amarok.kde.org/">amaroK</a> now has JavaScript scripting.
kutils adds incremental find. kwin adds window specific settings GUI.


<!--break-->
