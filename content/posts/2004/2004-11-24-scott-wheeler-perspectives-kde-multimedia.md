---
title: "Scott Wheeler: Perspectives on KDE Multimedia"
date:    2004-11-24
authors:
  - "esanchez"
slug:    scott-wheeler-perspectives-kde-multimedia
comments:
  - subject: "What about jack?"
    date: 2004-11-24
    body: "I haven't tried it myself, but wouldn't that be close enough to the goals of aRTs? Any ideas?"
    author: "anon"
  - subject: "Re: What about jack?"
    date: 2004-11-24
    body: "jack is only for routing (plugging) audio and midi streams, it will provide a simple way to play an audio file.\n\ni think jack can easily be used underneath, or on top of gstreamer/NMS/MAS. Allthough gstreamer/NMS/MAS provide functionality that is similar to jack (at least for audio).\n\nI dont know how any of these systems compares to arts. I dont know nor understand arts, i just seem to use it...\n\n_cies."
    author: "cies breijs"
  - subject: "Re: What about jack?"
    date: 2004-12-01
    body: "Jack is the standard for professional audio applications on Linux. It is the right choice for 24 bit audio, high sample rate, good routing and serious real time. Jack makes Linux a viable platform for musicians. For example Jack allows to combine au audio sequencer for virtual synthesizers and reverb. components.\n\nJPS."
    author: "Jean-Paul Smets"
  - subject: "Sounserver"
    date: 2004-11-24
    body: "One thing I can't seem to get my brain around are since neither Gstreamer or NMM are soundservers, what are going to be used as soundserver if KDE goes for one of them. A soundserver are still needed right? Are you planning too go for a cut down soundserver only version of aRts, Esd , Jack or something else? I can't remember to have seen this questions addressed anywhere. Whats the plan, curious and slightly comfused."
    author: "Morty"
  - subject: "Re: Sounserver"
    date: 2004-11-24
    body: "With the advent of modern audio frameworks like Alsa, sound servers are not really needed any longer for the majority of users. The main purpose of sound servers is the mixing of multiple audio streams, which can either be done in hardware, e.g. with a soundcard like SBlive, or in software, e.g. with Alsa's \"dmix\" plugin. \n\nTo my knowledge there are still some platforms that do not support native mixing (e.g. Solaris), but most modern platforms (Linux, FreeBSD) provide such a facility, which is much more elegant than a clunky soundserver. Those users that still need a soundserver could still select it, of course, since multimedia frameworks like GStreamer allow to select different output plugins. But there's no need to install a sound server by default on every box.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Sounserver"
    date: 2004-11-24
    body: "What really needs to be done is a device-driver level client/server... I have my workstation setup to use ASLA... I have an xterminal I use to log occasionally... In order to get sound on the xterminal, I have to reconfigure my various apps to use aRts/esd/whatever.. And every app is different.. Some support ESD, some Arts, etc.. So to get my apps to work, I have to have ESD and arts running.. What a waste!\n\nALSA is very good at emulating OSS, so all my apps work with it.. if we can get a \"remote PCM\" device driver, alot of my issues would disappear..\n\nThat being said, I think there was some work towards that.. I looked a week or so ago, and found some thread about aserver or somesuch.. I looked briefly at code, and it looks like the beginings of it..\n\n--Garion"
    author: "garion"
  - subject: "Re: Sounserver"
    date: 2004-11-24
    body: "Is it theoretically possible that, with the correct kernel modifications, we could one day access device files on remote computers via for example NFS - that is, set your app, running on a server, to use /mnt/myterminal/dev/dsp (or even symlink the server's /dev/dsp to it) and get sound on the \"myterminal\" machine...?\n"
    author: "Apollo Creed"
  - subject: "Re: Sounserver"
    date: 2004-11-24
    body: "No.\n\nA device file (like /dev/dsp) is nothing more than two numbers.\n\n[ralsina@roberto edubook]$ ls -l /dev/dsp\ncrw-------  1 ralsina root 14, 3 feb 23  2004 /dev/dsp\n\n14 and 3 in this case. Those numbers are the same for every device file on every box. Accessing a device file over nfs still accesses your own soundcard (try it)."
    author: "Roberto Alsina"
  - subject: "Re: Sounserver"
    date: 2004-11-24
    body: "Thanks for the answer. I wonder if this could be considered a Unix design flaw (I've more than once wanted to access devices in this way, serial ports connected to different pieces of homemade hardware on remote machines, for example).  Then again, the whole Unix device file model is often criticized for being a bit ad-hoc...\n\n"
    author: "Apollo Creed"
  - subject: "Re: Sounserver"
    date: 2004-11-24
    body: "It's not that it can't be done. It's just that it can't be done that way.\n\nFor example, if you want to do this for serial ports, you use remserial\n\nhttp://lpccomp.bc.ca/remserial/\n\nOf course there's no such thing as a pty for sound devices, but one can only hope ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Sounserver"
    date: 2004-11-25
    body: "The question was something *like* NFS, so the answer should be \"yes, if the file system in question was extended to handle device nodes\". Linux has the NBD which handles block devices. Writing a networked file system that could handle char devices should be trivial. But, no, I don't know of any existing ones. People generally use sound servers instead."
    author: "JonasB"
  - subject: "Re: Sounserver"
    date: 2004-11-25
    body: "On NFS you access a NFS filesystem, which contains files. When you open the files, NFS maps the actions to the remote server.\n\nUsing NBD, you have a device, which is a NBD device. Not a filesystem.\n\nIt's not at all like NFS, since it makes no attempt to look like a filesystem.\n\nIf you access the NBD device over NFS, it is again your local NBD device. That's because when the kernel is told \"read from this device file\" he doesn't care at all on what filesystem such file is. It's the wrong layer of abstraction.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Sounserver"
    date: 2005-05-17
    body: "Isn't this what NAS - \"Network Audio System\" is for?"
    author: "Daniel"
  - subject: "Re: Sounserver"
    date: 2005-05-17
    body: "Sorry, but:\n\na) You are posting in a months-old thread.\n\nb) You are very confused."
    author: "Roberto Alsina"
  - subject: "gstreamer"
    date: 2004-11-24
    body: "what about api?  is there a KDE api for gstreamer or is juk and amarok using the ugly c api?"
    author: "ac"
  - subject: "Re: gstreamer"
    date: 2004-11-24
    body: "Yeah we use the ugly C API."
    author: "Mark Kretschmann"
  - subject: "Re: gstreamer"
    date: 2004-11-24
    body: "Hmmm, then I think that is a big minus for gstreamer."
    author: "ac"
  - subject: "Re: gstreamer"
    date: 2004-11-24
    body: "Well, there are bindings for KDE, you're free to use them."
    author: "Mark Kretschmann"
  - subject: "Re: gstreamer"
    date: 2004-11-24
    body: "The KDE bindings are stuck at Gstreamer 0.6, so both amarok and juk use the native c-interface."
    author: "Carewolf"
  - subject: "Re: gstreamer"
    date: 2004-11-24
    body: "In short, gstreamer KDE is not well-maintained.  NMM is looking better and better."
    author: "ac"
  - subject: "Re: gstreamer"
    date: 2004-11-24
    body: "I've tried GStreamer with both Amarok and Juk, and apart from some quirks with a few mp3 files for no apparent reason and some timing issues it works OK.\n\nI've also tried them with NMM, and I have to say NMM is one cool bit of software. It's a bit of work to compile and the nice NMM guys provide binaries, but it works pretty well, sounds good and the latency is pretty good. For some reason, it was far worse with GStreamer whilst doing other things but I haven't been able to quantify it. It probably isn't GStreamer but some of the ways in which interfacing is done between Juk/amarok/GSteeamer.\n\nHowever, the cool things you can do with NMM over a network is also pretty mind-blowing, and it all works pretty great.\n\nSeriously though, if the bindings cannot catch up with the GStreamer current version in a reasonable amount of time so that you're ending up using a C API then there really isn't a whole lot of point in bindings or putting effort into maintaining them. I've never bought the \"Oh, you can make bindings for it\" argument with GStreamer unfortunately. NMM as a default and native C++ base is looking by far the better option.\n\nI know people desperately want KDE to use GStreamer by default because of the sake of some touchy-feely Gnome compatibility thing, but it just doesn't work like that. If it isn't maintainable to have clean bindings and interfaces with GStreamer now, then imagine what it's going to be like when GStreamer could be used by default and heavy development is going on for a KDE release..... Did people say that something better than arts is required?!"
    author: "David"
  - subject: "Re: gstreamer"
    date: 2004-11-25
    body: "I second your view, in each and every point you made. I admit that I did not tried yet NMM (nor gstreamer) by myself, but technically NMM really looks like a better alternative:\n- No binding needed,\n- C++, so it should integrates with kde's internationalisation easily,\n- Network tranparency\n- Clean API (from what i saw in doxygen docs)\n\nAnyway, from what I saw in the article, kdemultimedia team is considering developping a middle layer between KDE and any sound back-end. This is a good idea too. Once that done it would be possible to swicth from Gstreamer to NMM or anything else.\n"
    author: "po"
  - subject: "Re: gstreamer"
    date: 2004-11-25
    body: "I think so, too. Unfortunately the NMM guys provide one fat tarball, instead maybe core, plugins, qt-tools and some of the plugins are not up to date, to say the least. One can't say, they make it easy for interested people."
    author: "Carlo"
  - subject: "Re: gstreamer"
    date: 2004-11-25
    body: "To be fair, it is made up of plugins - they just provide most of them in one installation package."
    author: "David"
  - subject: "Re: gstreamer"
    date: 2004-11-25
    body: ">they just provide most of them in one installation package.\n\nI did not say anything else."
    author: "Carlo"
  - subject: "Re: gstreamer"
    date: 2004-11-26
    body: "\"Unfortunately the NMM guys provide one fat tarball, instead maybe core, plugins, qt-tools and some of the plugins are not up to date\"\n\nIn providing an easy to install tarball not every component is necessarily going to be up to date. You can't get any easier than that really, and installing GStreamer and all the right plugins is certainly not easy.\n\nYou complained about there being one tarball to download, then you complained that things are not up to date etc. You didn't grasp that you can't have everything."
    author: "David"
  - subject: "Re: gstreamer"
    date: 2004-11-26
    body: ">You complained about there being one tarball to download, then you complained that things are not up to date\n\nRight, this are two issues, I had with the software, when trying it, both more or less independent from each other. I had the idea to provide ebuilds, but not as a single one with a thousand use flags and dependencies. This would mean that I'd need to re-tar it in smaller pieces or let the whole tarball be unpacked for every single ebuild. Also I'm (and the users are) not interested to downgrade a whole bunch of media libs and recompile other applications, which depend on them, just to test NMM. The latter point is the reason, why I dropped the idea."
    author: "Carlo"
  - subject: "Re: gstreamer"
    date: 2004-11-26
    body: "I think it is very reasonable of the NMM maintainers to include as many dependecies as possible to make quick testing easy, for example allowing to install into a user writeable prefix, etc.\n\nWhen you are creating packages for installation under control of a packagemanager, you have to decide if you want to make your life easy, i.e. create one package per upstream tar ball, or create  a couple of packages to fit into the rest of the package repository and therefor making the life of the other users easy.\n\nOf course distributions with traditionally fine grained packages are more work to package for, but the high flexibility of what to install is what makes them so popular among their users.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: gstreamer"
    date: 2004-11-25
    body: "This is something of a chicken-and-egg problem though.  When I looked at updating the bindings I realized that it'd be a couple weeks of work.  Not too bad, but not a weekend project.  They'd need to be several thousand lines of code.  And there'd be exactly one application using them, where the native GStreamer code is all of about 150 lines of code.  And they wouldn't be packaged anyway.\n\nIn terms of time it simply wasn't worth the investment for me since I have about 8 times as many projects as I have time for as is.\n\nNow things there change for the KDE 4 picture.  If we're seriously looking at having bindings that are used by more than JuK, then it becomes worth the time to create them.  It's not particularly hard, just a bit tedious."
    author: "Scott Wheeler"
  - subject: "Noatun"
    date: 2004-11-24
    body: "Are anyone actually using Noatun? I accidentally encounter it whenever I reinstall and e.g. need to test sound/video, but always seem to be able to crash it (Mostly when playing with the skins and plugins, though). I have too many media files in proprietary formats it does not seem to handle, the whole GUI turns unresponsive as it goes through the playlists to reject the different media files. And three keyboard buttons to go to fullscreen, but not the same buttons to exit fullscreen? Okay, turning into incoherent rant, but Kaboodle and Noatun really are two annoying programs. Amarok is great for music, and hopefully I'll settle down for Kaffeine or Kmplayer soon for video. The fast forward/backward in movies with the scroll wheel you have in gmplayer is superior to anything else, though. IMHO, etc."
    author: "anon"
  - subject: "Re: Noatun"
    date: 2004-11-24
    body: "Actually I use noatun all the time. I am currently using the one from kde 3.3.1 in debian sid and the last time it crashed was several  years ago. About 90% of my music are oggs that I encoded and the rest are mp3s that I encoded long ago.  I just have not had the issues with noatun that others have had. However I have used amarok and just did not like it. \n\nIn amarok I found no way to just click on a song once and have it play but I can do that with the noatun default playlist. Amarok also did so many things that I had to spend time trying to figure out how to disable since I just want a playlist for music that I can jump around in. I don't want music catagorized, etc. In the noatun default playlist it is easier for me to just load up a directory to play (all my music is already catagorized) and have it start playing. I don't know about the various guis for noatun since I don't really use any of them. When noatun starts you can click on the little icon for it in your system tray and it will minimize to that icon. You can then use the keyz plugin for it to control it from the keyboard so you don't have to bother with it and you can also click on the playlist to quickly jump around.\n\nI don't do much in the way of videos. I have watched various trailers in kaboodle with no issues though so it works well enough for what I have used it for."
    author: "Kosh"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: "I used to use Noatun with Hayes. Now I use AmaroK and am very happy with it.\n\n> since I just want a playlist for music that I can jump around in.\n\nAmaroK -> \"Settings\" -> \"Configure AmaroK\" -> \"General\" -> uncheck \"Show Player Window\"\n\n\n> In the noatun default playlist it is easier for me to just load up a directory to play (all my music is already catagorized) and have it start playing.\n\nDepending on how big you music collection is. Loading of large directories can be insanely slow in Noatun.\n\n> When noatun starts you can click on the little icon for it in your system tray and it will minimize to that icon.\n\nSame with AmaroK.\n\n> You can then use the keyz plugin for it to control it from the keyboard so you don't have to bother with it and you can also click on the playlist to quickly jump around.\n\nAmaroK -> \"Settings\" -> \"Configure Global Shortcut\"\n\nCharles\n"
    author: "Charles Philip Chan"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: ">> since I just want a playlist for music that I can jump around in.\n \n> AmaroK -> \"Settings\" -> \"Configure AmaroK\" -> \"General\" -> uncheck \"Show Player Window\"\n\nThat will get rid of the player window but it still leaves the problem that you can't single click on an item in the playlist and have it start playing. Everything else in kde seems to be single click by default so why should that be double click? When I click on a folder in kmail, a message in kmail, a directory in konqueror, one of the songs in the noatun playlist all of those are single click by default.\n\n>Depending on how big you music collection is. Loading of large directories can be insanely slow in Noatun.\n\nI really have not experienced that. I will load up a directory structure which has 400 or so songs in it in < 4 seconds which seems easily fast enough. All my music is organized so when I want to play a different kind of music I load up that directory instead.\n\nI know you can configure amarok to do some of these other things that noatun can do however it can't do some of the things that I really want, namely that you need to doubleclick it instead of singleclick and none of the rest of the features really matter to me that it has. \n\nI have used the keybindings in amarok I have used the arts and gstreamer output plugins but overall the various features it has I just don't need very much. The playlist not being single click is the biggest issues for me and the other issue I have is getting the playlist to be as narrow as the default noatun one which I usually leave up all the time. Amarok is a nice media player but it does a lot of stuff that I don't need and it makes the most common way I play music a lot harder. \n\nThis is a screenshot of my desktop  http://aesaeion.com/mydesk   The large black areas are because the monitors are not all the same size."
    author: "Kosh"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: "How do you highlight files if clicking on them once starts playing it?"
    author: "Ian Monroe"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: "What do you need to hightlight the files for?"
    author: "Kosh"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: "to move, copy etc them...\n\nI have KDE set up for doubleclick, so I dont dislike this behaviour, but you are right - at least it should follow the settings in this.\n\nand you load a directory (wait 4 secs) every time you want other music? try juk... just (double...)click a playlist (and you always see them on the left) and it plays. if you want a simple and usefull interface, try juk. amarok is a bit worse imho on usabillity (at least they should replace their playlist tab with juk's playlist system, eg drag'n'drop, a collection playlist etc. whould make it much easier)."
    author: "superstoned"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: ">to move, copy etc them...\n\nWell I don't move, copy etc from within the playlist however it is easy to move stuff you click once and hold the button down and drag the item where you want it in the playlist. The same thing works in konqueror, just click a file, group of files, directories etc and drag them where you want them and it asks if you want move, copy or link. \n\nTo get properties of stuff in konqueror just right click on the item. I have never really liked the single click to select and double click to activate idea that is in windows.\n \n> I have KDE set up for doubleclick, so I dont dislike this behaviour, but you >are right - at least it should follow the settings in this.\n\nThat is my point. I want it to follow the system settings so that when you doubleclick it works the way you want it to and when I singleclick it works the way I want it to instead of doing its own thing.\n \n> and you load a directory (wait 4 secs) every time you want other music? try\n\nWell considering my playlists will play for hours and load in 4 seconds and that I will switch them maybe 2-4 times/day the 4 seconds does not seem to cost much.  :)\n\n >juk... just (double...)click a playlist (and you always see them on the left) >and it plays. if you want a simple and usefull interface, try juk. amarok is a >bit worse imho on usabillity (at least they should replace their playlist tab >with juk's playlist system, eg drag'n'drop, a collection playlist etc. whould >make it much easier).\n\nI will look into juk again the last time I used it I did not like it very much either. All my music is arranged in nested directories so I really just want to be able to click on a directory and have it start playing the stuff in it and show all the items it found in a playlist so I can jump around in it. I don't ever actually save a playlist in any of these applications since I can't see a reason for doing it. Juk and amarok are both nice applications but they seem pretty heavy weight for what I want to do and many of the features in amarok at least get in the way of my using it.\n\nI just want the media player to respond to keybindings so I can stop, start, pause, skip, next song, previous song etc from hitting key combos so I don't have to take my hands off the keyboard or click on that application to do that stuff. I want the playlist to be very simple and just load up a list of songs based on a directory structure and allow me to click on a song and have it start playing. I also want the playlist to be minimal enough that I can easily fit it on one of my screens all the time along with some other apps. Right now noatun can do all of those things and gives me no stability problems."
    author: "Kosh"
  - subject: "Re: Noatun"
    date: 2004-11-26
    body: ">I want the playlist to be very simple and just load up a list of songs based >on a directory structure and allow me to click on a song and have it start >playing. I also want the playlist to be minimal enough that I can easily fit >it on one of my screens all the time along with some other apps. Right now >noatun can do all of those things and gives me no stability problems.\n\nI think you should continue to use noatun... if you arange your music yourself in directory's, dont want to shuffle songs from several directory's or an album-randomplay function, but do - well, just what you described, than use noatun...\n\nI want to be able to choose music by band, sometimes by genre, another time by just an arbitrary set of things so I make playlists and use the searchfuntion juk/amarok have to offer. eg you type in \"pink floyd\" hit enter and it will only play songs matching pink floyd. in amarok I have a 'clever' playlist on 'electronic' music. only electro/trance/etc will be played if I hit it. Or I want oldies? yup, clever playlist for that. I never have to organise my music, just trow new songs in the mp3 map, and they will be added to the appropriate clever playlists. and I have several 'handmade' playlists, which I use, too... noatun can't do that, can it ;-) so - different player for different style of use..."
    author: "superstoned"
  - subject: "Re: Noatun"
    date: 2004-11-29
    body: ">I will look into juk again the last time I used it I did not like it very much either. All my music is arranged in nested directories so I really just want to be able to click on a directory and have it start playing the stuff in it and show all the items it found in a playlist so I can jump around in it. I don't ever actually save a playlist in any of these applications since I can't see a reason for doing it. Juk and amarok are both nice applications but they seem pretty heavy weight for what I want to do and many of the features in amarok at least get in the way of my using it.\n\nOne way to do this, is put all the directories into juk, and use the tag guesser on all of them. (I'm guessing it's nested somewhat like mine is with artist or album being in there?) In that case, set juk's left colum to tree view, and then you can get to it by album, artist, genre. \n\n>I just want the media player to respond to keybindings so I can stop, start, pause, skip, next song, previous song etc from hitting key combos so I don't have to take my hands off the keyboard or click on that application to do that stuff. I want the playlist to be very simple and just load up a list of songs based on a directory structure and allow me to click on a song and have it start playing. I also want the playlist to be minimal enough that I can easily fit it on one of my screens all the time along with some other apps. Right now noatun can do all of those things and gives me no stability problems.\n\nCtrl-Alt+ Left/Right go to previous/next songs in juk, and mapping things is quite easy with khotkeys (on another computer I've got windows + z,x,s,a to control volume, plus forward/next.) I think it maps to media keys, if they are in the layout by default. (Though usually they aren't by default, and you will have to mess around with the keyboard settings in kcontrol, or other tools)\n\nI unfortunately, don't know of a way to show the playlist graphically, but, it should be easy for someone else to write a small monitor. \n\ndcop juk Collection playlistTracks \"`dcop juk Collection playlist`\"\n\ngets a list of files (with path) If you want title, \n\necho \"`dcop juk Collection playlistTracks \\\"\\`dcop juk Collection playlist\\`\\\"`\" |  xargs --replace dcop juk Collection trackProperty {} Title\n(for some reason it was being a bit annoying about directly using the results, and such, hence the use of echo, xargs and a |)\n\njuk's dcop interface is quite nice in general.\n"
    author: "James L"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: "Hopefully by holding the Ctrl key when clicking, as that is the standard way of doing it."
    author: "David Walser"
  - subject: "Re: Noatun"
    date: 2004-11-25
    body: "ctrl-clicking them.\n\nBasic design: use the simple action for the most common task.\n\nThat\u00b4s why you don\u00b4t ctrl-alt-shift-triple-click to open."
    author: "Roberto Alsina"
  - subject: "Usability is not always picking consisitency"
    date: 2004-11-25
    body: "The problem with CTRL-click is nobody knows about it, that why every other UI system in the world uses single click for select, double click for activate. And that's why we do it in amaroK. We don't believe in KDE's standards here."
    author: "Max Howell"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-25
    body: ">The problem with CTRL-click is nobody knows about it, that why every other UI >system in the world uses single click for select, double click for activate. >And that's why we do it in amaroK.\n\nI have a couple comments for this. Windows does support single click to activate and you can use ctrl-click to select at any time. It has supported that since at least windows 98. More importantly when you use those settings the programs OBEY them. \n\nYou may find the doubleclick method to make the most sense for you however kde has that setting because there are people that like both ways and an app should respect the wishes of the person using it. If someone did not like the single click way they would have set their kde desktop to doubleclick for many other reasons already and if your app respected their settings it would work as it does now. However amarok does not respect these settings and so it is not consistent with the rest of the environment and in my view that lowers its usability. I have the system set to single click for a reason and I expect the apps to obey those settings. If I wanted doubleclick I would have set it that way. What is next having konqueror do double click in file manager mode even when the system is set to single click? Why don't we throw more consistency out the window and kde can just remove the setting and every app can figure out what it wants to do. \n\n>  We don't believe in KDE's standards here.\n\nI have to admit that with this view I hope that amarok NEVER becomes a standard part of kde until that is fixed. It is important that applications respect the system since inconsistenty is even worse. What you are essentially saying is that in order for me to use amarok I have to remember that is works differently then EVERY other kde app that I have of which all the rest seem to obey those settings. What a wonderfully usable app to go do its own thing and ignore the system settings. Yeah that sure makes it more usable. \n\nStandards are important since even if you could make your app slightly better by ignoring some of them you make the whole system far worse off as users have to remember how every application behaves. Having each application with a gui designed entirely around its task makes the system unusable if there are no standards. If you want changes made the correct way is to work with those that do the standards and kde and see if you can get changes made that you believe will help many applications and not just yours. If you are only doing it for your app since it is a special case then really consider what you are doing.\n\n\"Special cases aren't special enough to break the rules.\"\n    - The Zen of Python, by Tim Peters"
    author: "Kosh"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-26
    body: "Summarizing Kosh's message:\n\"You may find the doubleclick method to make the most sense for you, however kde has [a user-defined] setting because there are people that like [it] both ways and an app should respect the wishes of the person using it.\"\n\nWhich is a good point."
    author: "Ian Monroe"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-26
    body: "JuK does the same. Single-click selects and double-click runs"
    author: "Carewolf"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-26
    body: "I accept your points. We are after the best possible interface for our users. The playlist is absolutely central to amaroK, it is vital it is easy and pleasant to use.\n\nI believe we please a lot more users by going this route. For instance we have a lot of GNOME users now, one of our high vote bug reports is one for fixing systray usage under GNOME. Single-click would really annoy them.\n\nHowever I believe that we must make this optional, so don't worry. It'll hopefully be the case soon that everyone will be at least satiated. Consistency is important, you are right. But you still have to weigh it against how the interface feels. Nothing is black and white.\n\nFinally, we are happy in extragear, and have no desire to become the KDE-default media player.\n\nThanks for the comments."
    author: "Max Howell"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-25
    body: "You have it exactly backwards: Nobody knows about ctrl-click *because* every other UI system in the world uses single click for select."
    author: "Roberto Alsina"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-26
    body: "Well this isn't exactly the case, if every OS used single click methods there would be a lot of people who would not figure out how to select listview items. The major players realise this. It's the same as the number of people who don't know about CTRL-C/CTRL-V and thus can only ever copy and paste if there are toolbar/menubar items for it.\n\nWhen you hide interface like that a good number of people never find it. With something as important to amaroK as the playlist, it's central component, we can only offer a good user experience to all users by going double-click.\n\nWe don't want to annoy people, this is why we chose this!"
    author: "Max Howell"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-26
    body: "How is double-click any less hidden? You know double click because you were told to use it, that\u00b4s all :-)"
    author: "Roberto Alsina"
  - subject: "Re: Usability is not always picking consisitency"
    date: 2004-11-26
    body: "well, then, make it configurable by just conforming to the kde standard... then users can set it to doubleclick or singleclick in the kde config screen and you guys dont have to worry about it, and dont have to add another option in amarok."
    author: "superstoned"
  - subject: "Noatun works well in Slackware 10"
    date: 2004-11-25
    body: "of course with win32 codecs installed from mplayer's site."
    author: "Fast_Rizwaan"
  - subject: "Re: Noatun works well in Slackware 10"
    date: 2004-12-02
    body: "I beg to differ, noatun is klunky and under performs compared to almost every other media player I've used and considering it's been around for about as long as mplayer and xine I can't see why it shouldn't learn something from them. They both load incredibly fast and are excellent at playing almost all media types. I'm sticking with kmplayer and just waiting it to develop a little more functionality, failing that there's always xine."
    author: "Nick Boughton"
  - subject: "Why a soundserver at all?"
    date: 2004-11-24
    body: "OSS could only be opened by one process IIRC, but alsa does not have this\nlimitation.\n\nSo why can't kde apps just use alsa directly?\n\nOnly problem I see would be missing network transparency, and this could easy\nbe done by a kde deamon that would redirect sound from remote clients to alsa\nagain...\nKDE apps running locally would/should only talk to alsa directly.\n\nbtw arts sucks as playing mp3 with juk on my p3-450 uses about 20% cpu constantly... :-(\n\n"
    author: "ac"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-24
    body: "- OSS doesn't have the limitation of being used by only one program, some hardware does-- e.g, using OSS and a SBLive, you can use it more than once without any sound server\n- ALSA (and OSS) are Linux specific, so they can't be used directly without other choices. That's why the plugin frameworks of gstreamer and NMM help."
    author: "smt"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-24
    body: "The soundserver is there to allow to programs to play sounds simultaneously on soundcards who don't support this. Neither GStreamer or NMM can help with this as they are on a higher level, they need a output pluggin connecting to some kind of \"soundserver\" either a soundcard running alsa/oss/whatever who can handle multiple streams or some kind of sw soundserver like aRts/esd/whatever. In lots cases you still ends up with the need of a soundserver as most PC don't have the hardware."
    author: "Morty"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-25
    body: "Again, Alsa and other modern audio frameworks provide software mixing when no hardware support is available. For Alsa this is handled by the \"dmix\" plugin. These built-in mixing facilities make sound servers unnecessary in most cases (see discussion above).\n\nHere's a link providing some information on dmix:\nhttp://opensrc.org/alsa/index.php?page=DmixPlugin\n"
    author: "Mark Kretschmann"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-25
    body: "I still think this dmix stuff is ridiculous and over-engineered.  Really, this stuff should be handled transparently behind /dev/dsp and the usual sound devices instead of requiring user-intervention through the use of asoundrc and wrappers."
    author: "Navindra Umanee"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-25
    body: "The whole point of dmix is transparency. All the user needs to do is use the dmix device instead of e.g. hw:1. No more intervention is needed, and certainly no \"wrappers\" are required.\n\nIn fact, a sound server rather fits the definition of a wrapper.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-25
    body: "And what's aoss for?  That's a wrapper as far as I know i.e. dmix requires wrappers to /really/ work, yet everyone happily claims that it somehow doesn't.  \n\nI'm *no* fan of soundservers. What's I'm saying is that alsa should do its blackmagic behind /dev/dsp and not concern users with arcane stuff like dmix, aoss, hw:1 and so on."
    author: "Navindra Umanee"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-26
    body: "aoss preloads a library that emulates an oss device. So you can use older/propietary apps which don't have native alsa support. The app thinks it talks to oss, but in fact it talks to alsa. realplayer is a prominent example where this is useful."
    author: "Bausi"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-26
    body: "I know that.  My point is that you need it.  There are many such prominent examples and less prominent examples (such as practically every other portable Unix sound app in existence).\n\nIf everything used ALSA it could have been fine.  But like aRts, that hasn't happened and is not the reality of the situation."
    author: "Navindra Umanee"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-26
    body: "That surely means that alsa and the dmix plugin is still the best solution though however. At least in some circumstances, and its an increasing number, no \"wrapper\" is required.. if you're suggesting a soundserver is a better solution i think in every circumstance a \"wrapper\" would be required. What i believe was suggested was to encourage all programs to use alsa directly and eliminate this problem completly... you're correct in pointing out all programs don't do this all ready otherwise that suggestion wouldn't have been made!\n\nMartin"
    author: "Martin"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-26
    body: "It's not the best solution.  The best solution is to support multiple access to the sound device transparently.  How to put this.  The best solution is the solution where the user does not have to concern himself *at all* with ALSA technical details.  \n\nIf you meant that it's the best solution available now, I have no idea.  It certainly doesn't work for me.  It probably requires me to read tons and tons of manuals and to compile new kernels to get it to work.  ALSA has been around for ages, and still for all practical purposes my /dev/dsp only lets one application access it...\n\nI don't understand your point about aRts vs ALSA.  aRts does the same work that dmix does on that end. If you use the aRts API, the output gets mixed with other applications that use the aRts API.  It's the same thing with dmix, no?  Only difference is that you use the ALSA API instead of the aRts API and dmix takes care of the mixing.\n\naoss is then similar to artsdsp.\n\nAnd btw, what about portable Unix sound?  Encouraging all apps to use ALSA directly is as realistic as encourage all apps to use aRts directly.  Most commercial applications will be forced to support both OSS and ALSA.\n"
    author: "Navindra Umanee"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-26
    body: "QUOTE: \"It's the same thing with dmix, no? Only difference is that you use the ALSA API instead of the aRts API and dmix takes care of the mixing.\"\n\nSorry to barge in, but dmix has much more limited mixing capabilities than arts. For one, it requires that all applications outputting to dmix use the exact same sample rate. This means that any application outputting audio must, in principle, have the ability to resample its audio on-the-fly. Arts does that automatically, so app-writing becomes easier (ie. you don't have to implement it yourself).\n\nThis related to the larger point that \"direct\" ALSA is *NOT* an viable alternative to arts/gst/nmm. The main reason is that they are simply operating at different levels of abstraction: arts/gst/nmm take care of downsampling, mixing, multistream sync, file formats, etc. etc. whereas alsa basically ONLY takes care of mixing. Essentially, replacing arts with ALSA would mean that all applications would have to go back to implementing their own mp3/ogg decoders, their own latency-compensation, etc. In other words: Not a good idea.\n"
    author: "Mr. Fancypants"
  - subject: "Re: Why a soundserver at all?"
    date: 2006-06-11
    body: ">In other words: Not a good idea.\n\nI think the point was:\n>The best solution is to support multiple access to the sound device transparently.\n\nAs such, the 'difference' between arts and dmix is simply a matter of features. If mixing were handled with 'arts features', but in the underlying ALSA device drivers (assuming it were possible), then ALL problems with device access conflicts would be simultaneously eliminated, and arts/esd/etc. would still run with no problems (for legacy purposes) since they each \"think\" that they're directly accessing the sound device. Similarly, the OSS emulation modules would be mixed into the data stream automagically, since mixing is done at the device level.\n\nI assume something of this sort is possible, since Windows seems to do just fine. Unless, of course, it's a matter of Windows just using one standard soundserver whereas Linux has many that don't coexist nicely - an excellent argument against OpenSource, incidentally, even though I love the idea and the customizability (except for ALSA).\n\nIn fact, ALSA is a major motivation for me to use Windows right now. Dmix fails whenever CPU load goes above 70% (causing all related sound applications to either freeze or go into 'silent fast-forward' mode), and I can't even get arts to run through ALSA/dmix - and if I run it through ESD, which DOES work with dmix, initializing arts brings CPU load to 100% momentarily, killing dmix and causing ESD to hang. This is probably related to the fact that I get skipping problems at the same CPU load when using ALSA devices directly without going through the dmix plugin.\n\nBy comparison, on the same hardware, Windows just keeps on pluggin' on no matter what I do, no matter the CPU load, and sometimes even keeps on going after the rest of the system has crashed. I assume MacOS works as well as Windows does, if not better. So why isn't Linux up there competing with the rest of the gang?"
    author: "Angry Linux User"
  - subject: "Why a soundserver at all? Remote users."
    date: 2006-09-10
    body: "Many of us use LTSP to allow multiple users.  Workstation sound works by running esd on the workstation.  Esd adds lots of latency, but works fine for desktop noises and audio players.  You can even watch movies on a workstation if the video card support XV (but it saturates a 100baseT LAN).\n\nVia LTSP, our family runs 4 workstations (built from junked 400-800Mhz celerons and 17\" CRTs) on a single $400 server (Dell 500SC).  The only problem is explaining to the kids why they can't download and run all the Windows only binaries offered on the net.\n\nI would like to see esd feed into jack on the console.  Then jack would handle realtime stuff, and esd would handle all the noises and audio players."
    author: "Stuart Gathman"
  - subject: "Re: Why a soundserver at all?"
    date: 2004-11-24
    body: "you might check if you have the right software installed, I dont remember exactly, but google will be able to tell you about it: arts CAN use external software for playing music, which will decrease its processoruse with some huge factor..."
    author: "superstoned"
  - subject: "My view about most problematic issues..."
    date: 2004-11-24
    body: "I think that major obstacle for multimedia (and desktop generally) is not a lack of good apps, nor the kernel itself. On user side end, KDE is really fulfeatured, and eye-candy. On the low-level side, kernel is rock solid. What we miss is \"midle tier\". Actually, we don't miss it, but it is somewhat problematic.\n\nFor instance, big problem used to be font rendering. It is solved now (now only patent issues prevent you from using hints in TT fonts), but all of you remember those \"fonts look ugly\" topics.\n\nOn other fields: For instance, you may change mouse acceleration easily in KDE, but you cannot change a mouse resolution. It is not some KDE problem - problem is (correct me if I am wrong) that there is xset option that sets acceleration, but there is no xset option to set resolution. Also, I was very confused why I cannot set alt-shift to act like ctrl-shift-K in KDE (it does not work in RH 7.{0,1,2,3}, RH8, RH9, Suse9.{1,2}). Finally, I found that not to be KDE related problem, but related to X system. I hope that many problems related to X will be solved now when we have X.org instead of that laggy XFree team.\n\nAlso, arts deamon is a thing that we will not miss once it goes away. Ok, this is actually a kernel issue; but mixing server sounded like a bad idea from the scratch, no matter that no other workaround existed.\n\nOne thing that I will not miss if it goes away is cdrecord. (K3b and other stuff are just shells around it.) Cdrecord is beast for itself. I used to have page in my (paper) notebook where it was written how to use it. I am BsC in EE, and I use CLI 15 years now, but still, I was not able to remember cdrecords bizzare parameters. And it had some wierd behavior with Matsushita drive, spitting a message when noone (i.e. no shell) expected, forcing me to use it from the command line. Not something you would do for fun, believe me.\n\nAnd there are many things like this. Fortunately, all these mentioed stuff are progressing in good direction now. Thing that worries me is why it cannot be faster? Or maybe I just compare everything to the speed of KDE develoment;)\n\nJuct my .02$\n\nTomislav"
    author: "Tomislav"
  - subject: "Status of Noatun"
    date: 2004-11-25
    body: "Just so you know, Noatun is still being actively developed and we plan on numerous improvements (probably for KDE4) to how Noatun behaves.  This is evidenced by the new development branch that was just created.\n\n1) To those who are complaining about crashes in Noatun, there was a release of what I call the \"Magic Crashing Version\" -- basically, something changed in Qt that we didn't find until it was too late, then didn't know how to fix (eh.. workaround) for ages (this was some interaction with KDialogBase, and resulted in quite a lot of complaining from me!).  That was the only crash I was aware of.\n\n2) About skipping in the playback.  That is now quite thankfully resolved, as of the 3.3 release, KDE has switched aKode which handles it much better.  It also helps to use a Linux 2.6 kernel, as 2.4 has incredibly awful disk IO.\n\n3) Noatun is not optimised as a video player.  Video is supported quite nicely, and I personally am able to play pretty much every format I can throw at it.  You need to have xinelib installed at the time.  But calling it a \"Media Player\" I've decided is rather innacurate, because I use it just as something to give myself music over the course of the day.  And I should mention that I use it a lot.\n\n4) Yes, I can play shoutcast, and as of KDE 3.4 (or maybe it's 3.3, I don't remember :) - it will/does also get the icecast song title.\n\n5) If you're unhappy with the playlist, please consider using my latest experiment in playlistitude, Oblique, in kdeaddons.  There's also FlexPlaylist which is a bit more customisable.\n\n6) If you can crash it, and you don't use Gentoo with all sorts of insane optimisations, and you actually installed arts correctly (it, sadly, needs to be installed in the same prefix as noatun), then I will assume you're a lying troll unless you report a bug. I honestly do not know how to crash noatun as of KDE 3.3.0.  In fact, I don't even know how to crash the HEAD cvs branch.\n\n7) Remember, nobody is forcing you to use it.  When I, and any other Noatun developer writes it, we write what we want to use ourselves.  If you are not happy with what we write, then you are free to either make a suggestion to improve it, send a patch, or just use another program.\n\n8) Just because you don't like it doesn't mean other people don't.\n\n9) I play mp3, ogg, mpc, flac, and DivX files quite regularly without problems, if you can't maybe something wasn't installed?\n\nAgain, yes, Noatun is a \"different\" sort of program. It's different from the ground up by design, we like what we have, and since we are our favourite users, we will do what we want!\n"
    author: "charles samuels"
  - subject: "I loved Noatun"
    date: 2004-11-25
    body: "I loved it, also because it resembles the old WinAmp a lot, it looked pretty good and was very usable. And it was very customizable too. Only to add songs to playlist was a bit awkward, but I am talking about the Noatun version included with Mepis (the Mepis version before it became SimplyMepis). I use amaroK now, it was provided by SuSe and it is very good too, but I would like to try Noatun sometime again nevertheless. Noatun has a product page somewhere?"
    author: "Darkelve"
  - subject: "Re: I loved Noatun"
    date: 2004-11-25
    body: "noatun.kde.org -- it's hardly updated, however, because we rather be coding C++ than playing with HTML :)\n\n-Charles\n"
    author: "charles samuels"
  - subject: "Re: Status of Noatun"
    date: 2004-11-25
    body: ">>Again, yes, Noatun is a \"different\" sort of program. It's different from the ground up by design, we like what we have, and since we are our favourite users, we will do what we want!\n\nNo hard feelings, but, do what you want for yourselves then, but do not shove it down default KDE installed base it's throat.\n\nDo you accept many, many people want to see Noatun OUT? Do you accept that, or are you stubbornly going to stick with the: \"WE are our favorite users\" and \"that's what KDE users will have to accept\", nevermind other, much, much more practical and prooven projects being embraced in a 'mass' sigh of relieve?"
    author: "ac"
  - subject: "Re: Status of Noatun"
    date: 2004-11-25
    body: "nah, if noatun is better now as it was the last time I tried (must have been the I-love-to-crash-version) why not let it stay? maybe give it plugins for an juk-like playlist, and some amarok-features, and juk/amarok might even become obsolete :D\n\nI'm goin' to install & try noatun now..."
    author: "superstoned"
  - subject: "Re: Status of Noatun"
    date: 2004-11-25
    body: "and I didnt like it at all. not easy-to-use playlists (kde 3.3.1), no easy all-in-one skin (a skin that shows playlist and controls in 1 window), no quick searching (like juk and amarok have), no playlist handling (eg you have to click thourgh several windows to load another playlist, instead of just selecting it in juk, or rightclick->load in amarok.\n\nno, its nice compared to xmms, but xmms sucks compared to juk and amarok. once you got used to their nice interfaces, no way of going back..."
    author: "superstoned"
  - subject: "Re: Status of Noatun"
    date: 2004-11-25
    body: "try Oblique, in kdeaddons.\n\nThe only thing it doesn't have is an \"all in one\" UI.  That's something that will be addressed in the future.\n"
    author: "charles samuels"
  - subject: "Re: Status of Noatun"
    date: 2004-11-25
    body: "I think you missed my points #7 (Nobody is forcing you to use it) and #8 (just because you don't like it doesn't mean everyone doesn't)"
    author: "charles samuels"
  - subject: "Multimedia and Mediaplayer plugin"
    date: 2004-11-25
    body: "I am very happy with all the discussion about multimedia, but I have\na simple question that I never could answer: what is the status of\nvideo playing in KDE (or Linux in general) for embedded video with\nM$ mediaplayer plugins? I don't know how to setup this (if it is really \npossible) and look that I like to setup software by hand! KDE should\ntry to fill this gap, because there is a huge amount of people who like\nto see media in embedded players -- this is the reason why my wife\ndoesn't use linux (at least the excuse she uses!!)  It would be good\nto have at least manuals explaining how to do this for the multimedia-non-\ninitiated. The best would be to have mplayer/xine integrated to play\nembedded MS video in KDE out of the box."
    author: "Oliveira"
  - subject: "Re: Multimedia and Mediaplayer plugin"
    date: 2004-11-25
    body: "kaffeine AND kmplayer can do this both, embedded in firefox AND konqueror. what are you complaining about?\n\nusing an old version of these apps? or you dont have these plugins installed? what distribution are you using? it should work out-of-the box on a recent and userfriendly distribution..."
    author: "superstoned"
  - subject: "Re: Multimedia and Mediaplayer plugin"
    date: 2004-11-25
    body: "<i>what distribution are you using? it should work out-of-the box on a recent and userfriendly distribution</i>\n\nClearly it is not in this case. Surely, this is not the only case.\n\nHowever way you put it, multimedia and just working out the box is something which is going to take some time and solving of many (legal) issues.\n\n"
    author: "ac"
  - subject: "Re: Multimedia and Mediaplayer plugin"
    date: 2004-11-25
    body: "well, never had much troubles with mepis linux."
    author: "superstoned"
  - subject: "Re: Multimedia and Mediaplayer plugin"
    date: 2004-11-25
    body: "neither with mandrake, by the way. and stock-debian just required the installation of some packages. knoppix does it, fine, if I recall rightly. so, what distribution does it this bad? red hat I guess? alwasy sucked at multimedia... just dont use it..."
    author: "superstoned"
  - subject: "Re: Multimedia and Mediaplayer plugin"
    date: 2004-11-25
    body: "Gentoo works perfectly. I just 'emerge kaffeine' and voil\u00e0, also after emerging some codecs with dubious legal status the Apple movie trailers in Quicktime play directly in my Konqueror browser."
    author: "Jonas"
  - subject: "soundserver process"
    date: 2004-11-25
    body: "One of practical problems with arts is that it runs as a process (which blocks all other programs trying to access soundcard directly). This is because, only one process can access soundcard at a time (I don't know why most distros do not configure software mixing in alsa by default?). Problem arises when I want to use application with bad arts support (like with mplayer volume keys don't work).\n\nAll soundservers suffer from this problem and IMO its best to use alsa (with software mixing. dmix?)."
    author: "Nilesh Bansal"
  - subject: "Graphics equalizer"
    date: 2004-11-25
    body: "Where can I get graphics equalizer for amarok (with gstreamer backend)? I can't find much around about it-freedesktop.org went down and is back... but it lost some content, and amarok.kde.org isn't back yet :(.\n\nCheers!\nVirgil"
    author: "Virgil Nisly"
  - subject: "Re: Graphics equalizer"
    date: 2004-11-25
    body: "amaroK CVS HEAD features a 10-band graphic equalizer for xine and GStreamer. Check out CVS, or wait for 1.2-beta1 (soon).\n"
    author: "Mark Kretschmann"
  - subject: "Re: Graphics equalizer"
    date: 2004-11-25
    body: "Thanks! \nI saw that, but it didn't make any sense to me until you said something about it. Checking out cvs now:)\nVirgil"
    author: "Virgil Nisly"
---
<a href="http://www.ofb.biz/">Open for Business</a> associate editor Eduardo Sánchez <a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=338">interviewed</a>  <a href="http://developer.kde.org/~wheeler/">Scott Wheeler</a>, creator and lead developer of <a href="http://developer.kde.org/~wheeler/juk.html">JuK</a> and a member of the <a href="http://multimedia.kde.org/">KDE Multimedia</a> team to find out where the KDE multimedia department is headed in general, and concerning a replacement for <a href="http://www.arts-project.org/">aRts</a>, more specifically.


<!--break-->
