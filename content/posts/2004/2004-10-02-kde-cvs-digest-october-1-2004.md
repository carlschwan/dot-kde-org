---
title: "KDE CVS-Digest for October 1, 2004"
date:    2004-10-02
authors:
  - "dkite"
slug:    kde-cvs-digest-october-1-2004
comments:
  - subject: "Thanks as Usual !"
    date: 2004-10-02
    body: "Thanks Derek !\n\nIs anybody having problems to compile kde from CVS with newer gcc versions ??\n\n3.3 amd 3.4 gave me a lot of problems with all the packages, including kdelibs, incomplete objects mainly\n\n"
    author: "GaRaGeD"
  - subject: "Re: Thanks as Usual !"
    date: 2004-10-02
    body: "No problem at all compiling HEAD with gcc (GCC) 3.4.0 and 3.4.1 on another machine!"
    author: "annma"
  - subject: "Re: Thanks as Usual !"
    date: 2004-10-04
    body: "No, and in fact most, if not virtually all developers use gcc 3.3 or 3.4 so compile errors caused by this would be unlikely."
    author: "Chris Howells"
  - subject: "Re: Thanks as Usual !"
    date: 2004-10-04
    body: "If you have still problems, perhaps use another anonymous CVS server.\n\n(It seems that there were some mirroring problems, as at least WebCVS was up to 4 days behind.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Problems compiling"
    date: 2004-10-04
    body: "I have had the same problems under Slackware 10, with its default gcc 3.3.4\nThe only solution I've found (so far) is to just delete the incomplete objects and restart the build from the work/pkgname directory (or a subpackage directory)\n"
    author: "solid_liq"
  - subject: "Re: Problems compiling"
    date: 2004-10-04
    body: "I have tried that, even delete the whole package, usually compiling again crashes on the same file.\n\nI will switch to another CVS server, maybe that can help :-)"
    author: "GaRaGeD"
  - subject: "New layout"
    date: 2004-10-02
    body: "To be honest, I absolutely hate the new layout.\nI hope the old layout is kept ( at least as an option).\nI like the fact that you currently have everything on one single\npage and just have to scroll down to read everything. On saturday\nmorning I usually take my cup of coffee and take my time to read the\nwhole digest.\nWith the new version I would have to click for each section which is\nquite annoying.\n\nOtherwise: Thanks as usual Derek for this weekly update. It's very much\nappreciated.\n"
    author: "Martin"
  - subject: "Re: New layout"
    date: 2004-10-02
    body: "You are not the only one. Fear not, the 'all in one' layout will remain.\n\nThe existing layout, especially the table of contents, is too constrained. I would like to be able to break the classifications down even more, such as wish list, minor bugfixes, backports, branches, usability, documentation, etc. Maybe more. In any case, it won't fit within the existing system. That was one of the motivations for breaking things up into smaller chunks.\n\nOnce I have some free time, I intend to come up with some kind of all in one layout.\n\nFeedback is always welcome.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: New layout"
    date: 2004-10-03
    body: "This (the old layout remaining) is a very good news! Thank you Derek, I really prefer this one too!"
    author: "MacBerry"
  - subject: "Re: New layout"
    date: 2004-10-03
    body: "I think you are too harsh.\n1. This is still experimental and promised features will make it very handy.\n2. This is great for tracking important changes in history."
    author: "m."
  - subject: "Re: New layout"
    date: 2004-10-03
    body: "> On saturday morning I usually take my cup of coffee and take my time to read > the whole digest.\n\nHey, that's my way to start in the day, too. So, I also don't like the new version... \n\nred"
    author: "red"
  - subject: "Re: New layout"
    date: 2004-10-03
    body: "I like it, it gives a quicker overview. Maybe the two layouts can coexist?"
    author: "Hans"
  - subject: "KmPlot and KEduca are kparts!"
    date: 2004-10-02
    body: "KDE-Edu had seen 2 applications becoming kparts: KmPlot and KEduca. Also lots of work has been done on KTouch from updating the code to porting the data files on xml format.\nWell done to the KDE-EDU developers and welcome to the new maintainers, Andreas and Henrique!"
    author: "annma"
  - subject: "Re: KmPlot and KEduca are kparts!"
    date: 2004-10-06
    body: "In my moves through the KDE jungle I've managed to get the KDE-EDU stuff compiled over the past couple of days. KTouch is very promising, although I think it's a bit late for me now!"
    author: "David"
  - subject: "Kolourpaint"
    date: 2004-10-03
    body: "Thanks for the improvements in Kolourpaint!"
    author: "Mikhail Capone"
  - subject: "Konversation home page"
    date: 2004-10-03
    body: "The SourceForge home page for Konversation is somewhat out-of-date.  The correct link is\n\n  http://www.konversation.org/\n\n"
    author: "Gary Cramblitt (aka PhantomsDad)"
  - subject: "Re: Konversation home page"
    date: 2004-10-03
    body: "Why does the old homepage still exist/doesn't redirect?"
    author: "Anonymous"
  - subject: "Can somebody explain"
    date: 2004-10-03
    body: "\nCan somebody explain what a \"multimedia framework\" is, and what it's supposed to do?\n\n"
    author: "cbcbcb"
  - subject: "Re: Can somebody explain"
    date: 2004-10-03
    body: "Multimedia is like networking: There you also have different networks, different protocols, routes, ports, security problems. A multimedia framework is something like ifconfig, iptable, netstat and route for multimedia data ;-)\n\nWatching the different akademy video streams the presentation of NMM was IMHO the best and the most impressive one - spontaneous applause during the demos. This stuff looks like made for KDE.\nProblem as Derek pointed out: its open source but a more in-house development style and is not community based. Another negative point: It does not support all the platforms KDE can runs  \n\nBye\n\n  Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Can somebody explain"
    date: 2004-10-03
    body: "would it help for decision, if I made ebuilds fom NMM ?"
    author: "HelloWorld82"
  - subject: "Re: Can somebody explain"
    date: 2004-10-05
    body: "there are quite a few people (myself included) that would be more inclined to play with NMM if there were ebuilds for it available, so yes, they would definitley be useful. But be aware that they won't be easy - NMM is quite difficult to build."
    author: "Jeff Snyder"
  - subject: "Re: Can somebody explain"
    date: 2004-10-03
    body: "List off the different things you need to do with sound on a computer.\n\nWrite raw sound data to the card. What drivers? alsa or OSS? Decoding mp3 or ogg. You may want to be able to seek within the sound stream, ie. jump back a few seconds to listen again. System sounds for an error dialog. You may want to record a sound, apply some effects to the sound, ie. change volume, mix, or make it sound like an echo chamber. And while playing you want a bunch of lines on your screen to dance in time with the music. Etc.\n\nYou could write an application that reads an ogg file, decodes it, uses the OSS drivers to make noise. Another application that reads mp3's, provides mixing control, then uses alsa. Very quickly you end up with a mess that is unmaintainable and limited in function.\n\nSo multimedia frameworks were created, where these same functions could be plugged in. You end up a string of plugins, starting with a file reader, decoder, mixer, maybe some effects, and finally the plugin that receives raw sound data and writes to the OS driver. You use alsa rather than OSS? Swap out the driver plugin. Ogg instead of mp3? Swap out the decoder plugin.\n\nNetwork multimedia systems have the capabilities to put a network between the file input and the hardware output. So you read the file, the data is sent over the network to another machine that mixes the data stream and plays on it's hardware.\n\nDerek"
    author: "Derek Kite"
  - subject: "gstreamer"
    date: 2004-10-04
    body: "Is gstreamer not a network multimedia frameworks? I guess it doesn't have network capability, right? "
    author: "Ian Monroe"
  - subject: "Re: gstreamer"
    date: 2004-10-04
    body: "Of course!\n\nThe \"g\" in \"gstreamer\" stands for \"GNOME\" which stands for \"GNU Network Object Model Environment\".  So network capability is definitely a given."
    author: "ac"
  - subject: "Re: gstreamer"
    date: 2004-10-05
    body: "Actually, it is not built with network transparency as a goal, because 99% of the time, network transparency is not necessary for multimedia, so it does not impose it where not necessary. But network transparency can be added through plugins. GStreamer is designed to be veyr lightweight. Most of the functionality comes through plugins, so the core is less than 1MB.\n\nNetwork transparency is very possible without pain. No extra latency and so on."
    author: "Maynard"
  - subject: "Re: gstreamer"
    date: 2004-10-05
    body: "Really?\n\nDerek (who is not from Missouri, but wants to be shown)"
    author: "Derek Kite"
  - subject: "Re: gstreamer"
    date: 2004-10-06
    body: "Well, I am from Missouri. I installed the gst-editor today and it certainly appears to have listed 'utilities' to exchange info over TCP or UDP. I'm not really sure if you can use these as-is or not, network transparency isn't listed on GStreamer's feature list (which is why I asked).\n\nYou can also sink the audio into arts or jack (as opposed to putting it directly to your sound card)."
    author: "Ian Monroe"
  - subject: "Re: gstreamer"
    date: 2004-10-06
    body: "I've been using GStreamer as my main output medium for the past week, and it's very promising. You've got independent packages, so it doesn't pull in lots of unnecessary stuff and it's desktop independent, despite the 'G' thing (people get a fright with that sort of thing for some reason).\n\nI've even managed to get NMM built (what a sense of achievement!) and that looks really, really promising also. There's some incredible things that can be done with it, and some were demonstrated at akademy I think.\n\nNetwork transparency is not needed most of the time, but as you get more local area networks and the barriers between computers fall down I think it will be necessary over the next couple of years.\n\nIf the Gnome, KDE and Freedesktop people can agree then great. If not, then I hope people will sensibly state the reasons why and that it doesn't turn into a war. However, it seems as though the KDE Multimedia Framework would support multiple back-ends (correct me if I'm wrong), and that really would be cool!"
    author: "David"
  - subject: "No fright with gcc, glibc, and GPL"
    date: 2004-10-06
    body: "so why be afraid of gstreamer?\n\nIt's not tied to gnome in any way."
    author: "AntiGuru"
  - subject: "Re: No fright with gcc, glibc, and GPL"
    date: 2004-10-06
    body: "glib\n"
    author: "anonymous"
  - subject: "Re: No fright with gcc, glibc, and GPL"
    date: 2004-10-07
    body: "glib does not depend on GNOME."
    author: "Anonymous"
  - subject: "Re: No fright with gcc, glibc, and GPL"
    date: 2004-10-06
    body: "I know it isn't. People talk about glib, but you're going to have to write it in something."
    author: "David"
  - subject: "glib isn't gnome-specific either"
    date: 2004-10-07
    body: "or even GTK specific."
    author: "AntiGuru"
  - subject: "Network..."
    date: 2004-10-03
    body: "Network integrated Multimedia stresses PDA support. Hope KDE's support for handhelds such as Palm will improve. \"Palm desktop\" software looks ugly but has the best usability for a contact management software. Unfortunately only windows."
    author: "Bert"
  - subject: "What is \"{AUTHORLINES}\" in the statistics section?"
    date: 2004-10-03
    body: "thanks derek."
    author: "1234"
  - subject: "Lab rat #345346"
    date: 2004-10-04
    body: "Experiments must be terminated sometimes. Especially when they are unusable at 800x600..."
    author: "erroneous"
  - subject: "Re: Lab rat #345346"
    date: 2004-10-04
    body: "Using such small screens may be a a risk for your eyes health. So whatever you're complaining about, maybe it's helping you. Btw, I got a 800x600 lappie, and KDE works just fine on it. I don't use it, maybe because it's such painful for my eyes to use that screen with any OS anyway."
    author: "the jumpy gnome on systray"
  - subject: "Re: Lab rat #345346"
    date: 2004-10-05
    body: "Using a low resolution doesn't imply using a small screen... I set up a computer for my parents at 800x600 on a 19\" CRT screen. At any higher resolution, the fonts just get too small. Sure, I can adjust the font size for text generated on my computer, but what else could I do about all those stupid web sites that are \"optimized\" for 800x600 resolution, or have all their text embedded in fixed resolution bitmap graphics?"
    author: "AC"
  - subject: "Re: Lab rat #345346"
    date: 2004-10-07
    body: "That's why instead of (or in addition to) doing text zoom, Konqueror should be able to do a true zoom that zooms images and scales CSS sizes as well.  That way you could enjoy sharper high-resolution fonts without making everything else tiny."
    author: "Spy Hunter"
  - subject: "Re: Lab rat #345346"
    date: 2004-10-07
    body: "That would be awesome, especially for some web comics."
    author: "Ian Monroe"
  - subject: "kde 3.3.1"
    date: 2004-10-05
    body: "i have read that kde 3.3.1 would be released last weekend but it wasn't \nwhen will it be released"
    author: "ismail"
  - subject: "Re: kde 3.3.1"
    date: 2004-10-05
    body: "Tagged, not released."
    author: "Anonymous"
  - subject: "Re: kde 3.3.1"
    date: 2004-10-06
    body: "No, the 2nd of October 2004 was the date where the code should be ready to be packaged. KDE 3.3.1 was supposed to be released around next weekend.\n(See: http://developer.kde.org/development-versions/kde-3.3-release-plan.html )\n\n(But as there are a few details that currently do not work, please do not hold your breath!)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: kde 3.3.1"
    date: 2004-10-10
    body: "thanks for your answer"
    author: "ismail"
---
Highlights of this week's <a href="http://cvs-digest.org/index.php?issue=oct12004">KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=oct12004">experimental layout</a>): XML auto-indenter in <a href="http://kate.kde.org/">Kate</a>.
Rendering speed-ups in <a href="http://kolourpaint.sourceforge.net/">Kolourpaint</a>.
New media:/ KIO slave.
Improved SQL parser in <a href="http://www.koffice.org/kexi/">Kexi</a>.
<a href="http://www.konversation.org/">Konversation</a> adds support for SSL.
Summary of <a href="http://www.networkmultimedia.org/">Network-Integrated Multimedia Middleware</a> from the <a href="http://conference2004.kde.org/">aKademy</a> presentations.
<!--break-->
