---
title: "Product Excellence Award for KDevelop 3.0"
date:    2004-01-22
authors:
  - "hfernengel"
slug:    product-excellence-award-kdevelop-30
comments:
  - subject: "typo in the 3rd link"
    date: 2004-01-22
    body: "typo in the 3rd link\n\ncies (kdevelop3 -gideon- fan)"
    author: "cies"
  - subject: "Re: typo in the 3rd link"
    date: 2004-01-22
    body: "okay, okay, okay, the right url is http://www.linuxworldexpo.com/ :)"
    author: "Harald Fernengel"
  - subject: "Good Stuff"
    date: 2004-01-22
    body: "The guys at Ximian have had to rip off Sharp Develop!"
    author: "David"
  - subject: "Re: Good Stuff"
    date: 2004-01-23
    body: "Well, \"rip off\" is (more than) a little harsh. Porting a GPL Windows .Net IDE written in C# to Linux and GTK# would seem to be easier than rewriting the whole thing from scratch. Besides, the Sharp Develop guys have done something similar as well, basing their #WT on Java's SWT."
    author: "Anon"
  - subject: "Lets open a can of whoopass!!"
    date: 2004-01-22
    body: "GREAT!\n\nI love it when a KDE proj. opens up a big can of whoopass for all them other \"losers\" ;-D\n\n/kidcat"
    author: "kidcat"
  - subject: "Best Open Source Project"
    date: 2004-01-22
    body: "But the award to this Real-thing as best Open Source project is a pitty. IMHO KDE or Gentoo would be a better choice.\n\nBUT... congrats to the Helix player team. Next time we will win (again) :-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
---
The KDevelop team is happy to announce that <a href="http://www.kdevelop.org/">KDevelop</a> 3.0 won the <a href="http://www.linuxworldexpo.com/linuxworldny/V40/index.cvn?ID=10240"> Product Excellence Award</a> for Best Development Tool at <a href="http://www.linuxworldexpo.com/">Linuxworld New York 2004 conference & Expo</a>. This is the first award for the new codebase of KDevelop 3.0, stay tuned for the release :)




<!--break-->
