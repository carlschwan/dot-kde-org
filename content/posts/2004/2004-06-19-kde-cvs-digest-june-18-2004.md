---
title: "KDE-CVS-Digest for June 18, 2004"
date:    2004-06-19
authors:
  - "dkite"
slug:    kde-cvs-digest-june-18-2004
comments:
  - subject: "Fonts"
    date: 2004-06-19
    body: "Can anyone tell me why fonts in KDE look blury with anti-aliasing which when disabled, the fonts are ugly? Using the same hardware, a site in M$ Windows XP looks good. What can be done? I have tried ALL fonts and even down loaded a number of them with no marked difference. I even tried changing the screen resolution. Hope somebody can help me out.\n\nCb.."
    author: "Charles"
  - subject: "Re: Fonts"
    date: 2004-06-19
    body: "What's the relation of your question to the news?"
    author: "Anonymous"
  - subject: "Re: Fonts"
    date: 2004-06-19
    body: "AFAIK that's because MS and Adobe have some patents which make it impossible for FOSS to render Fonts clean AND patent free.\nBut AFAIK there is a patch for xft which violates this patents you can use when software patents are not legal in your contry.\n\n(hope my english is not to bad...)"
    author: "panzi"
  - subject: "Re: Fonts"
    date: 2004-06-19
    body: "Actually, it isn't because of the patent issue. You can't patent skill and hard work.  Creating high quality display fonts is difficult and requires skills and interests not readily available in the open source community. It is easier for the open source community to continue to depend on the handful of MS fonts available to it, and the fonts released by Bitstream, rather than creating new fonts and a better display engine."
    author: "enloop"
  - subject: "Re: Fonts"
    date: 2004-06-20
    body: "a. Go learn what font hinting is\n\nb. Go learn why the same font displayed without hinting looks worse\n\nc. Go learn how you can enable hinting, if it hapens to be legal for you\n\nd. Before being all pompous in a post, learn what the hell you are talking about."
    author: "Roberto Alsina"
  - subject: "Re: Fonts"
    date: 2004-06-20
    body: "huh, enable hinting can be illegal?\nwhy don't I know that or did kde ever point that out to me anywhere.\n\nconfused...\n\nBut I do know it is possible to make fonts look good in linux.\nNot to start a flamewar, but gnome seems to have done this the right way.\ntheir fonts look superiour.\n\nKDE can also look great, you just need to find the correct fonts."
    author: "Mark Hannessen"
  - subject: "Re: Fonts"
    date: 2004-06-24
    body: "In KDE3.3 you will be able to set the Xft hinting level - this is what GNOME is currently able to do. For now, simply add:\n\nXft.hinting: 1\nXft.hintstyle: hintfull\n\n...to your ~/.Xresources\n\n'hinting' can be 1 (on), 0 (off). 'hintstyle' can be hintnone, hintslight, hintmedium, or hintfull\n\nThis is seperate to the FreeType hinter issue. FreeType can be compiled to use the \"bytecode\" hinter, which has some patent issues - or it can use its own auto-hinter."
    author: "Craig"
  - subject: "Re: Fonts"
    date: 2004-06-24
    body: "I think there is some correct and some incorrect information in the posts above. Please look at http://www.freetype.org for facts and authentic information.\n\nEnabling hinting is not illegal. Many fonts (Truetype specifically) have hints which assist the font-rendering software in proper visualization of that font. Using these hints is not patented. What is patented is a particular algorithm that uses the BYTECODE in TrueType fonts in a specific manner to display beautiful fonts. Look at the following link for details.\n\nhttp://freetype.sourceforge.net/patents.html\n\nOne can develop another algorithm that uses these hints and try to display better fonts. In fact, the freetype2 font rendering libraries have such a hinter, called autohinter, which is open-source and patent-free. Most distributions ship a version of freetype2 libraries in which BYTECODE interpreter is disabled (as they should).\n\nNow, if your country does not have software patents; then you can go to http://www.freetype.org; download their latest release sources, read the instructions in it to see how to change one line in a .h file to enable BYTECODE interperter and install it. You will have much better fonts. As mentioned in other posts here, make sure that you have turned on hinting etc. and hintstyle is full. Also, make sure that your DPI is 96 (sear for DPI in /var/log/XFree86.0.log) as most truetype fonts are best rendered at 96 DPI.\n\nOsho"
    author: "Anonymous"
  - subject: "Re: Fonts"
    date: 2004-06-19
    body: "For a start you can install yourself some good anti-aliased (within the fonts themselves) fonts. Anti-aliasing and good font handling can get you so far but if your fonts are crap and unsmooth to start off with, don't bother complaining."
    author: "David"
  - subject: "So will work to import XML files?"
    date: 2004-06-19
    body: "I am waiting for a long time for JavaScript implementation in Konqueror to support importing on XML documents, as described here:\n\nhttp://www.quirksmode.org/dom/importxml.html\n\nDoes today's news means that this is possible from now and we still have to wait?"
    author: "Ela"
  - subject: "telephone app in kde"
    date: 2004-06-19
    body: "I recently wondered why there is no real telephone app in KDE. As I am no coder, I can obviously neither create one nor expect others to write it for me.\n\nBut there are so many apps for KDE, whats the reason why there's none for making phone calls? Integrated into Kontact, this would surely be a killer-app?!\n\nDoes anyone know of such a project? Maybe with different backends for VoIP, CAPI (ISDN), modem etc.? Especially interesting now that KDE has a bluetooth framework and the kbthandsfree application, you could use your bluetooth headset to make calls."
    author: "anon"
  - subject: "Re: telephone app in kde"
    date: 2004-06-19
    body: "just installed kphone... might help?"
    author: "superstoned"
  - subject: "Re: telephone app in kde"
    date: 2004-06-19
    body: "unfortunately not, since its \"only\" a SIP-phone and doesn't support CAPI or modems."
    author: "anon"
  - subject: "Re: telephone app in kde"
    date: 2004-06-24
    body: "gnomemeeting runs fine under kde."
    author: "salsa king"
  - subject: "Re: telephone app in kde"
    date: 2004-06-25
    body: ", but doesn't support using capi or modems."
    author: "anon"
  - subject: "Re: telephone app in kde"
    date: 2004-06-25
    body: "> But there are so many apps for KDE, whats the reason why there's none for making phone calls?\nNon of the spare time developers needed it? Or no company released an existing app to the public? Maybe no distro found it a key applications worth developing it?\nBtw, 'I'm not a coder' is such a shitty excuse. Set up a site with the documentations, attract others, learn some C++.."
    author: "koos"
  - subject: "Re: telephone app in kde"
    date: 2004-07-01
    body: "While your response lacks diplomacy it is more true than people realize. I had not coded in C++ prior to heading up the Quanta project. I have contributed very little of the of the C++ code because the fact is I also just don't have the time to get good at it, but I did learn C++ for Quanta. Between Quanta and Kommander I have two of the top rated apps at kde-apps.org, but I could have said \"I am not a coder\" and neither would exist today.\n\nNews flash for those who have lost touch with reality... People are not born writing code any more than you were born speaking your mother tongue. If you are not incapable of learning that merely leaves desire or motivation. I have already addressed skills and time. So pick a new excuse from...\n\"I'd like it but I don't want it that bad\"\nor\n\"I wish I had it but I'm too lazy to do anything about it\"\n\nPlease recognize that in the course of human endeavors most of our limitations are self imposed and the only real thrill in life is finding the outer edge of our limitations. Most people are irrationally concerned about being perceived as inadequate, when in fact those who face challenges recognize their inadequacy and accept it in others. Those who live with the cowardly illusions that all who step forward are ten feet tall seek comfort where there is none, in the least challenging posture they can find.\n\nTake courage. Take action!"
    author: "Eric Laffoon"
  - subject: "Re: telephone app in kde"
    date: 2004-06-26
    body: "Try www.asterisk.org\nAnd let's hope someone will implement a KDE front-end..."
    author: "Marco Menardi"
  - subject: "Re: telephone app in kde"
    date: 2004-07-01
    body: "> Try www.asterisk.org\n> And let's hope someone will implement a KDE front-end...\n\nSomebody else? Do you really want to wait around for that? Why don't you do it? It's mostly point and click with Kommander.\nhttp://kde-apps.org/content/show.php?content=12865"
    author: "Eric Laffoon"
---
In <a href="http://cvs-digest.org/index.php?issue=jun182004">this week's KDE-CVS Digest</a>:
Large image viewing improved.
More integration between <a href="http://kaddressbook.org/">KAddressbook</a> and <a href="http://kopete.kde.org/">Kopete</a>.
Continued <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">OASIS</a> file format loading and saving work in <a href="http://www.koffice.org/">KOffice</a>.
<a href=" http://www.xulplanet.com/references/elemref/ref_XMLSerializer.html">XMLSerializer </a> merged from <a href=" http://www.apple.com/safari/">Safari</a>.
<a href="http://www.kdetv.org/">kdetv</a> adds deinterlacing.
New <a href="http://www.konqueror.org/">Konqueror</a> RSS feed plugin.


<!--break-->
