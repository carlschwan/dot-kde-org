---
title: "KDE @ LinuxTag 2004: A Great Success Bodes Well for aKademy"
date:    2004-07-03
authors:
  - "Dre"
slug:    kde-linuxtag-2004-great-success-bodes-well-akademy
comments:
  - subject: "Portugal! Oh yeah..!"
    date: 2004-07-04
    body: "http://ariya.pandu.org/gallery/linuxtag2004/football.jpg\n\nPortugal 0 - 1 England\n\nlololololololololololol\n\nPortugal forever!!!!!! winner ! hehehehehe\n\nThx KDE developers for this nice piece of work!"
    author: "Portuguese developer"
  - subject: "Re: Portugal! Oh yeah..!"
    date: 2004-07-04
    body: "I bet you didn't like it when the DOJ decided to throw the game against MS, but here you are cheering your nations victory due to similar circumstances... "
    author: "hm "
  - subject: "Re: Portugal! Oh yeah..!"
    date: 2004-07-05
    body: "> Portugal forever!!!!!! winner !\n\nIIRC they didn't win yesterday. :-)"
    author: "Anonymous"
  - subject: "Gallery Link List"
    date: 2004-07-05
    body: "http://www.infodrom.org/Debian/events/LinuxTag2004/pictures.html"
    author: "Anonymous"
---
 "The tenth annual LinuxTag proved to be a very successful event for the
forty-plus KDE developers who presented their award-winning desktop
environment to more than 16,000 enthusiastic visitors at 'Europe's largest
OpenSource Event'."  So begins Torsten Rahn's excellent report on KDE's
presence at <a href="http://www.linuxtag.org/2004/">LinuxTag 2004</a>.


<!--break-->
<p />
<h3 align="left">KDE @ LinuxTag 2004: A Great Success Bodes Well for aKademy</h3>
<p align="justify">
June 30. 2004 (Karlsruhe, Germany).
The tenth annual LinuxTag proved to be a very successful event for the
forty-plus KDE developers who presented their award-winning desktop
environment to more than 16,000 enthusiastic visitors at "Europe's largest
OpenSource Event".  As expected, the upcoming
<a href="http://conference2004.kde.org/">KDE Community World Summit 2004</a>
("aKademy"), which will be held between 21-29 August, 2004 in Ludwigsburg
(Stuttgart Region), Germany, strongly influenced many of the talks and
presentations by the KDE team.
</p>
<p align="justify">
As usual, the KDE booth in the exhibition area was crowded. Six to eight
demopoints were available for LinuxTag visitors to learn about the current KDE
3.2 release, accessibility, the <a href="http://dot.kde.org/1088363665/">KDE
FreeNX</a> project, personal information management (KDE PIM), the
award-winning <a href="http://www.kdevelop.org/">KDevelop</a> IDE as well as the
future of KDE. Many users also took the opportunity to personally provide developers
with valuable feedback and suggestions. KDE developers used the hacking area to
work on KDE FreeNX, jointly develop new ideas and generally hack on KDE.
Of course LinuxTag also offers the opportunity to meet some KDE developers from
outside Germany.  As special guests, David Faure (KDE France) and Fabrice Mous
(KDE-NL) attended the fair.

</p>
<p align="justify">
Some related software projects, who had been invited to the KDE booth to enhance
cooperation, took advantage of the opportunity.  Members of the
<a href="http://www.qtforum.org/">QtForum.org</a> project helped to spur
interest in KDE and Qt development. Oliver Fels from the
<a href="http://opie.handhelds.org/cgi-bin/moin.cgi/">Opie</a> team demonstrated
syncing the PDA / handheld GUI environment with KDE.  And in a joint effort
with the <a href="http://www.skolelinux.no/">Skolelinux</a> project, the KDE
Team demonstrated the rapid development progress of free educational software.
LinuxTag's Social Event on Friday was another good opportunity
to meet and talk (and have a beer!) with members of other projects or companies.
And just like last year, on Thursday night KDE, SkoleLinux, QtForum.org, Gnome
and Debian team members dined together.

</p>
<p align="justify">
Dozens of KDE related talks took place at the conference area.
Kurt Pfeifle and Fabian Franz made numerous presentations and speeches which
covered <a href="http://www.knoppix.org/">Knoppix</a> and the NX-Technology,
which generated a lot of buzz.
In their speeches, Ralf Nolden, Klas Kalass and Daniel Molkentin presented
the Enterprise Desktop KDE and provided an overview of new features in KDE
3.2.
Bo Thorsen gave a talk on Kolab which was very well received. In their presentation
"KOffice - The Road Ahead", David Faure, Ariya Hidayat and Lucijan Busch provided
interesting details about the state and future of the KDE Office Suite. And Eva
Brucherseifer demonstrated the ease of migration to the Qt multiplattform
framework in her speech.

</p>
<p align="justify">
The KDE Project would like to thank <a href="http://www.hp.com/">Hewlett
Packard</a>, <a href="http://www.fujitsu-siemens.com/">Fujitsu Siemens</a> and
<a href="http://www.transtec.de/">Transtec</a> for providing the booth with
hardware, and <a href="http://www.novell.com/linux/">Novell</a> /
<a href="http://www.suse.com/">SuSE</a>,
<a href="http://www.credativ.de/">Credativ</a>, <a href="http://www.basyskom.com/">basysKom</a>,
<a href="http://www.froglogic.com/">Froglogic</a>,
<a href="http://www.trolltech.com/">Trolltech</a> and
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> for
their support.
Last but not least we'd like to congratulate the dedicated LinuxTag
Organization Team on a successful event.
We all look forward to meet you at LinuxTag 2005!

</p>
<p align="justify">

Photos, photos, photos:

</p>
<ul>
  <li><a href="http://ariya.pandu.org/gallery/linuxtag2004/">http://ariya.pandu.org/gallery/linuxtag2004/</a>
  <li><a href="http://www.sebastian-bergmann.de/gallery/linuxtag2004/aba">http://www.sebastian-bergmann.de/gallery/linuxtag2004/aba</a>
  <li><a href="http://devel-home.kde.org/~tackat/LT2004/images.html">http://devel-home.kde.org/~tackat/LT2004/images.html</a>
  <li><a href="http://www.pl-berichte.de/berichte/lt2004/fotos/index.html">http://www.pl-berichte.de/berichte/lt2004/fotos/index.html</a>
</ul>


