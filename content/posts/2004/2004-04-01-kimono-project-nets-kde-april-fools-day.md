---
title: "Kimono Project Nets KDE [April Fools' Day]"
date:    2004-04-01
authors:
  - "rmoore"
slug:    kimono-project-nets-kde-april-fools-day
comments:
  - subject: "wtf?"
    date: 2004-04-01
    body: "I can't help feeling Novell has something to do with this?\n\nhttp://linuxtoday.com/news_story.php3?ltsn=2004-03-31-026-26-NW-DT-NV\nhttp://www.heise.de/newsticker/meldung/46148\n\nSome strange things are going on?  I am just a user but I am not sure if this is good or bad. I will choose to trust in the KDE developers at the moment and wait and see what happens."
    author: "KDE User"
  - subject: "KDE dadaist perspective"
    date: 2004-04-01
    body: "Is this art? Or is it useful software? Or is it non-useful software trying to make a point?\n\n\"A person who is seized by terror will not speak of a 'frisson nouveau' a mere novel thrill, will not shout 'bravo', and will not congratulate the artist upon his originality.\"\n\n\t- Eric Auerbach"
    author: "Richard Dale"
  - subject: "Hey, April 1st!!!"
    date: 2004-04-01
    body: "Anyone noticed today is April first? Of course this is a joke!!!!!!!!!!"
    author: "Tiago"
  - subject: "Re: Hey, April 1st!!!"
    date: 2004-04-01
    body: "But is it? It's not here in US. I am looking at the code write now both KaXul and the Mono bindings seem to be real.  I am not convinced."
    author: "ac"
  - subject: "Re: Hey, April 1st!!!"
    date: 2004-04-01
    body: "Ah, but the best April Fool's jokes are those with just enough truth to make you wonder.  :)"
    author: "phoenix"
  - subject: "Re: Hey, April 1st!!!"
    date: 2004-04-01
    body: "On the Internet, April fools starts at 00:00 April 1 GMT.  KaXUL and the QT C# bindings are both real and have been around for some time now, but the idea that they will be the main focus of any upcoming KDE release is nothing but a joke, of course."
    author: "Spy Hunter"
  - subject: "Re: Hey, April 1st!!!"
    date: 2004-04-01
    body: "No, the problem of integrating the KaXul layer with the Mono bindings has been establishing a World Wide network of 'Ontological GUI Metaphysicians'. We've had to do in secret, and what with the recent worldwide concern with people with odd views, I've been reluctant to be too public.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hey, April 1st!!!"
    date: 2004-04-01
    body: "Considering recent happenings I doubt whether it is a joke, and all of the above exists and is actually feasible. The date is of course, a bit unfortunate..."
    author: "David"
  - subject: "WHY???"
    date: 2004-04-01
    body: "This will only make KDE bigger, sslower and bloated!!  Even GNOME won't switch to Mono.  You have _got_ to kidding me.  I am so disappointed.  I guess its true what they say about KDE copying Windows.  Why did I switch at all.  :("
    author: "ac"
  - subject: "Re: WHY???"
    date: 2004-04-01
    body: "Nobody said that the whole KDE will be coded in C#. All this has nothing to do with Windows, although it would be good to have Qt# cross-platform and forget about Windows.Forms."
    author: "pete"
  - subject: "Re: WHY???"
    date: 2004-04-01
    body: "Look at the date. It's a joke.\n\nThe only truth behind it is that Qt C# bindings do exist (and have existed for months).\n\n"
    author: "bero"
  - subject: "Re: WHY???"
    date: 2004-10-18
    body: "People like you get right on my tits! You're a moron! You clearly know nothing about the internal workings of managed code and the .NET CLR at all.\n\nI'd love to see the whole of KDE written in managed code. The source code would be easy to read, easy to write, and it would be JUST AS QUICK you ignorant prat. And because of all these things it would evolve at a much faster rate and everyone would benefit (including you, you miserable C-loving prick). In fact, the only force holding these things back are the facist Linux gimps (like yourself) who are scared of evolution and want everything to run on their 33MHz 486 because they're too tight arsed to splash out a few hundred quid on a modern machine. If you actually bothered to have a look at .NET, how much it speeds up development, and how much smaller (yes, smaller; MSIL does a lot more per-instruction than x86) it makes your applications then perhaps you'd realise, as I'm sure some of you Linux nerds are starting to do, that Microsoft isn't the source of all evil and that you can actually benefit from embracing some of the technologies that they are partly responsible for creating.\n\nJust to summarise, in case that wasn't clear enough... one day you'll grow out of your \"real programmers use assembly language\" mindset and make something *useful* in a proper language (C#, Java, VB.NET, etc.).\n\nRichard.\n\nP.S. I bet you're one of those people who spells Microsoft with a dollar sign too."
    author: "Richard"
  - subject: "Merry April 1st."
    date: 2004-04-01
    body: "So funny to watch all the fish bite on this one.  I bought it for a second until I saw how in depth they were going.  Then I glanced at the date."
    author: "PaulSeamons"
  - subject: "huh?"
    date: 2004-04-01
    body: "Rich, could you please qualify your position on this? There have been many, many reasons outlined before as to why we _SHOULDN'T_ be implementing dot-NET. Where are you coming from here?\n\n-jf"
    author: "Jeff D"
  - subject: "Re: huh?"
    date: 2004-04-01
    body: "DAMN!!!!\n\nfoiled by GMT!"
    author: "Jeff D"
  - subject: "April Fools!"
    date: 2004-04-01
    body: "Had me going at first, but then:\n\"shake the KDE developers out of a rut\"\n\"using a real object oriented language like C#\"\n\"inherent advantages of a managed common runtime will be indisputable\"\nmade me check the date.\n\nApril Fools!\n\nP.S. I love the automatic spellcheck on webforms. You KDE developers rock!\n"
    author: "dapawn"
  - subject: "Sigh"
    date: 2004-04-01
    body: "Its 11:45pm on March 31 here in Atlanta. This interweb thing makes April fool's a very long and drawn out process. \n\nThough, kaxul definitely seems real, and kimono is definitely real. Interesting way to PERVERT THE TRUTH guys!\n"
    author: "Rayiner Hashem"
  - subject: "Re: Sigh"
    date: 2004-04-01
    body: "Kimono isn't real. :P"
    author: "truth"
  - subject: "Re: Sigh"
    date: 2004-04-01
    body: "It is (sort of). See http://lists.kde.org/?l=kde-cvs&m=107966511804849&w=2\n"
    author: "SadEagle"
  - subject: "Re: Sigh"
    date: 2004-04-01
    body: "good, at last a project with a nice code name :)\n"
    author: "ik"
  - subject: "Re: Sigh"
    date: 2004-04-01
    body: "Moreover, there are quite countries where first of April is a normal day.\nFor example, here at Spain, the equivalent day is 28 of December, which is called \"D\u00eda de los santos inocentes\"."
    author: "Cesar"
  - subject: "Qt# project Speaks of Sabotage"
    date: 2004-04-01
    body: "Check osnews: Qt# project Speaks of Sabotage\nhttp://www.osnews.com/story.php?news_id=6550"
    author: "Mambo-Jumbo"
  - subject: "Re: Qt# project Speaks of Sabotage"
    date: 2004-04-01
    body: "I am Adam Treat the creator and principle developer of the Qt# bindings.  There was no sabotage.  Marcus is overly sensitive at times.\n\nRichard Dale is one of the original KDE binding authors and responsible for the Qt Java bindings as well as Objective-C, QtC, and KDEC.  He is also working on the Ruby bindings, the new Smoke library and the new proxy/smoke based C# bindings.  He is doing great work and I look forward to seeing this progression.\n\nRichard and I have talked and he is planning on using the bulk of the current Qt# bindings where applicable."
    author: "manyoso"
  - subject: "a language like C#"
    date: 2004-04-01
    body: "\"a real object oriented language like C#\" ...yeah... damn... wtf?\n\nWell, I'm really thick-skinned with april fool jokes cause my sisters birthday is today, april 1st, but this one still drove me mad. Well done, guys. :-)\n"
    author: "Thomas"
  - subject: "Wow!"
    date: 2004-04-01
    body: "We're getting XML in the UI? Finally! Now we can get rid of that darned Designer and stop drawing forms. I can't wait until we finally can use an OO language too. This is really exciting!\n\nOf course this would have looked better if it wasn't still 9:45 PM, March 31st here in Oregon. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Wow!"
    date: 2004-04-01
    body: "But dude, \"darned\" designer is already in XML! If you feel like doing it, go \nahead and start editing that in vi, but most will still find designer a better\nway to edit .ui xml files.\n\nQt also gives you facilities to load XML files dynamically converting them into\nuser interfaces:\nhttp://www.esrf.fr/computing/bliss/qt/html/designer-manual-5.html#2-3-2\n\nOfcourse you wont be able to provide custom slot implementations dynamically, \nbut if that is what you want you would choose QSA:\nhttp://doc.trolltech.com/qsa-1.0/qsa-3.html\n\n-- \nAmit Upadhyay\n\n"
    author: "Amit Upadhyay"
  - subject: "Re: Wow!"
    date: 2004-04-01
    body: "Dude, it's a joke. It's April Fools..."
    author: "Crouching Tiger"
  - subject: "Re: Wow!"
    date: 2004-04-01
    body: "Another one bites the hook on April 1st. You didn't happen to note that I'm the project leader for Kommander, did you? Kommander is based on Qt Designer and creates the XML .ui files as *.kmdr files that run with kmdr-executor. So Kommander is a no compile, optional scripting language, mini application building dialog tool. It does DCOP too. BTW I'm also the project leader for Quanta Plus which means you're not real likely to find me editing in vi. ;-)\n\nIn case it's still not clear. I was kidding."
    author: "Eric Laffoon"
  - subject: "Re: Wow!"
    date: 2004-04-01
    body: "Yeah, right. When has Eric Laffoon ever posted anything without inserting a plug for Quanta?\n\nNo fooling me today...!"
    author: "Otter"
  - subject: "Re: Wow!"
    date: 2004-04-01
    body: "No Quanta plug? 23.782% of posts, counting the 68.542% of the time it was on a story about Quanta.\n\nThis time however the interesting factor was because of how it relates to Kommander, and son of a gun, all these things are related to the fact that we use KDE tools and technologies, which was the jist of the humor. The Kommander plug came only when it was apparent somebody didn't get the joke. The Quanta plug was done by you when you read it in where it was not. I can only assume it's some kind of obsession you have with Quanta since there was nothing at all about Quanta in it. Thanks for the plug, I guess... It's nice to have something to plug that you don't have to call the plumber for.\n\nIs a little wit too much to ask for on April fools? Try to grasp the joke before countering. Enough of this, I have more entertaining and important things to do. ;-)"
    author: "Eric Laffoon"
  - subject: "This is NOT a April Fools!!!"
    date: 2004-04-01
    body: "This is not an April Fools.. OSNEWS has a story about it at http://www.osnews.com/story.php?news_id=6550\n\nAPRIL FOOLS!!\n"
    author: "AC"
  - subject: "Congratulations"
    date: 2004-04-01
    body: "You managed to hoax OSNews :) I tip my hat to you, sir.\n"
    author: "Rayiner Hashem"
  - subject: "Mono"
    date: 2004-04-01
    body: "Actually, I think Mono for KDE would be a good thing (assuming there is no problem with copyrights and such things).  I would like to program for KDE, but I am currently put off by the complexity of C++. I have done various things in Java (which is a nice language), so going to Mono would probably be fairly easy."
    author: "Vevaxel"
  - subject: "Re: Mono"
    date: 2004-04-01
    body: "Exactly. I think there are innumerable people like you (and I). Widespread and standardized mono libraries would lower the threshold to contribute (remember, mono is not only C# -- libraries for it can be used by any mono supported language, so you'd get a number of new language bindings for free).\n"
    author: "Haakon Nilsen"
  - subject: "Re: Mono"
    date: 2004-04-02
    body: "In the Kimono C# KDE bindings the method names start with an upper case letter. If I want to use the C# binding as a producer, and a java consumer binding does the CLR have an automagical method renaming scheme?\n\nAm I right in thinking that method name overloading resolution is language specific in the CLR? Here's a problem - C# has unsigned ints, while java has only unsigned. The KDE api features methods overloaded on only that minor type difference.\n\nI think people grossly under estimate the difficulties of producing a common cross-language library without compromises. But that is what we've been attempting to do with Smoke. It doesn't happen by the sprinkling of magic marketing speak 'fairy dust'. Anyone remember CORBA - just wrap an object in language neutral IDL, and bingo! Language independent components..\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Mono"
    date: 2004-04-01
    body: "http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/kdejava/"
    author: "anonymous"
  - subject: "Re: Mono"
    date: 2004-04-01
    body: "\"Actually, I think Mono for KDE would be a good thing\"\n\nThe April Fool's joke was funny the first several dozen times. Now it's getting old. If you're not joking, then you obviously don't realize that Ximian (the Mono people) consider Qt to be obsolete."
    author: "David Johnson"
  - subject: "RE: Mono people consider QT to be obsolete."
    date: 2004-04-04
    body: "...and they would be right."
    author: "Anonymous"
  - subject: "RE: Mono people consider QT to be obsolete."
    date: 2004-04-04
    body: "Hi God. :)"
    author: "teatime"
  - subject: "Why not?"
    date: 2004-04-01
    body: "Mono is NOT at Microsoft technology, it's just based on a MS technology, to say that MS controls mono would be incorrect, so I don't really see why it shouldn't be implemented.\n\nBut, I think you're right about Novell having a hand in this, since Ximian is using C#, and Novell has made QT their standard GUI developing framework, this makes sense."
    author: "SF"
  - subject: "Re: Why not?"
    date: 2004-04-01
    body: "> Mono is NOT a Microsoft technology\nwine is not a Microsoft technology\nsamba is not a Microsoft technology\nbut it's all about Microsoft \"technology\"\nstrange, this is..."
    author: "Thomas"
  - subject: "Re: Why not?"
    date: 2004-04-01
    body: "Well, that is a thin line of perception that we will have to navigate."
    author: "David"
  - subject: "Re: Why not?"
    date: 2004-04-01
    body: "Mono itself is not no. However it is based on a Microsoft technology with huge legal questions surrounding it. It is linked to part of Microsoft's .Net C# etc and so Microsoft controls the direction of it and a game of perpetual \"catch-up\" will ensue. There is just _NO_ way around that. If you think the .Net infastructure is the cats pajamas then you will use Microsoft's implementation as it will be the one that is new and shiny. \n\nWhat I find incredibly hilarious is that a slew of GNOME folks say Mono is the greatest thing since sliced bread but how is it any different then the failed Harmony project that was deemed to be a waste of time due to perpetual \"catch-up\" with the latest and greatest Qt? I haven't seen it looked at from this angle and this most likely not the right place to ask it either. But I do find it amusing."
    author: "jhohertz"
  - subject: "Bad date ..."
    date: 2004-04-01
    body: "I swear, if this is an april fools, I will hold a grudge on Richard Moore forever, it's NOT funny! Something like Kimono would be fantastic for KDE, but the \"quote\" from Ettrich sounds manufactured (\"time to shake things up a bit\"? right).\n\nNot funny.\n"
    author: "Haakon Nilsen"
  - subject: "Re: Bad date ..."
    date: 2004-04-01
    body: "The projects are real, however, KDE 3.3 being the focus for kimono isn't.. KDE 3.3 will be a short release before KDE 4.0"
    author: "anon"
  - subject: "Re: Bad date ..."
    date: 2004-04-01
    body: "I'm sorry I've been laughing all evening about this news item - it's really surreal. Please don't attack Richard M - I love it!\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Bad date ..."
    date: 2004-04-01
    body: "Ok, I call off my grudge :)  The bindings and tools are indeed real.  I'm just a little frustrated with the whole April 1st surrealism; you never know what's real, and something is real but just exaggerated, and who knows what to believe anymore!  Come, April 2nd! :)"
    author: "Haakon Nilsen"
  - subject: "Remember Parrot? And vigor?"
    date: 2004-04-01
    body: "These started out as April Fools jokes. They became real projects. Fear the worst."
    author: "Isotopp"
  - subject: "pesce d'aprile!"
    date: 2004-04-01
    body: "just the title"
    author: "maX"
  - subject: "KWin window tabbing"
    date: 2004-04-01
    body: "On a somewhat related note ... <a href=\"http://www.root.cz/clanek/2125\">http://www.root.cz/clanek/2125</a> , it's in Czech, but the pictures don't need comments (yes, I got caught too).\n\n"
    author: "Lubos Lunak"
  - subject: "Re: KWin window tabbing"
    date: 2004-04-01
    body: "That looks very interesting! Is it something similar to Fluxbox's window grouping?\n\n(Of course, I hope it is not an April fools thing...)"
    author: "Vevaxel"
  - subject: "Re: KWin window tabbing"
    date: 2004-04-01
    body: "you know you want to implement that in kwin =)_"
    author: "anon"
  - subject: "Re: KWin window tabbing"
    date: 2004-04-03
    body: "I realise this is a joke, but I would love to have that. Pleasepleaseplease, Lubos?"
    author: "Anon"
  - subject: "Yes it Look Interesting But..."
    date: 2004-04-01
    body: "\"It is likely to slow things down at first, but in the longer term the benefits of using a real object oriented language like C# with the inherent advantages of a managed common runtime will be indisputable.\"\n\nLet's not fall into any hype please. That is the worst thing that can be done. I think Mono can certainly be useful if used in the right way (i.e. having a community implementation and ignore what Microsoft are doing), but I just don't like what some people are coming out with about it. There seems to be some paranoia that we need to somehow copy from what Microsoft is doing. I just don't understand the rational behind a clone of Avalon and anything else Microsoft may, or may not, be doing. Most of it is kept up by hype, and it's not as if we are going to have native compatibility with any of it so we need to accept that now.\n\nI think it is a safe bet that this has a lot to do with work at Novell/Suse about integrating many technologies (as the work on Qt# seems to have got back up and running out of nowhere), but of course, no one will say anything :).\n\nI say yes if it is good compelling technology and the community can have its own implementation, and it is used for those reasons, rather than \"we need this because MS will put this in...\". This will mean putting native compatibility with MS .NET very low down on the list of priorities, because it just isn't an achievable goal. This must be a community technology, and it must be better and more compelling than anything Microsoft has. The use of XUL and KaXuL above is a good example, and a step in the right direction. I read the presentation on KaXuL and it looks very useful, so I'm chuffed to see it being used.\n\nPut simply, if this can be used and marketed in a way that says that this is a better, more useful way of using .NET principles together with exclusively open source technology and toolkits, I think a lot of good can come from it. If it is perceived by developers as simply a way of maintaining compatibility with native MS.NET, it will fail spectacularly because it puts Microsoft in the driving seat technologically and in mind share. It will also do an immeasurable amount of damage to the image of open source development that may not be recoverable.\n\nGo forward, but be safe and, above all, be sensible with it. If done right, and if it can create and gain mindshare in people, then this should really, really cheese some people off in the north-west of the United States."
    author: "David"
  - subject: "Re: Yes it Look Interesting But..."
    date: 2004-04-01
    body: "\"It is likely to slow things down at first, but in the longer term the benefits of using a real object oriented language like C# with the inherent advantages of a managed common runtime will be indisputable.\"\n\nThen again I really just can't stomach this statement. \"Slow things down a bit\" has April fool written all over it. You bastards! You take real, believable technology that could absolutely definitely be used to do all this and turn it into something that is quite believable. If this is a joke, it is very, very good and if it isn't your timing stinks. Go on, I give up."
    author: "David"
  - subject: "Re: Yes it Look Interesting But..."
    date: 2004-04-01
    body: "No, this is definitely pants - but it looks good!"
    author: "David"
  - subject: "Hell, Rich this isn't even the end of the start.."
    date: 2004-04-01
    body: "I just went for the way C# has reproduced java bug for bug:\n\t- Static methods, no class methods with dynamic dispatch\n\t- Classes are not first class citizens\n\t- No way of adding to an existing class, aka mixins Objective-C delegates\n\t- Meaningless, over-complicated class hierarchies (ContextBoundObject huh?).\n\nI've desperate to think of a way to incorporate java style event listeners into KDE for years, but the technical problem of integrating such utterly naff technology with KDE has been impossible to date.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-01
    body: "What do you mean by \"Classes are not first class citizens\"?  Did you mean to say functions?"
    author: "Navindra Umanee"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-01
    body: "In Smalltalk or Objective-C or ruby you can send a message (ie invoke a function call if you think that way) to an instance of a class in exactly the same manner. In those languages the target of the message could either be an instance or a class, it doesn't make any difference. \nBut in C# or java, invoking a static method is a  different sort of thing. This is a design flaw, it screws up remoting with static methods.\n\nIf class B inherits from class A, then class A's class methods will be overriden by class B's class methods. In contrast in java or C# there is no dynamic despatch on static methods. So you need this for a factory pattern. I think the C# framework has a factory interface, but they are solving the problem in the wrong way.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-01
    body: "Ah I think I see what you mean.  Thanks for explaining!"
    author: "Navindra Umanee"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-02
    body: "April 2nd\n\nActually when I read through my explanation it isn't very clear. I'm afraid I drunk too much wine while laughing at all this last night\n\nBut it is certainly true that java/C# static methods are poor compared with classes as instances being sent class methods as messages. Classes should be objects, and just like any other sort of instance. \n\nI'm sure there must be a good write up about this somewhere out on the web, that would do a better job of explaining than I could.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-01
    body: "\"I just went for the way C# has reproduced java bug for bug\"\n\nOf course it has! You need to remember that C# essentially started the day after Sun won its Java case with Microsoft. They weren't allowed to embrace and extend Sun's Java specs, so they created their own.\n\nDeep within Microsoft's psyche is the fundamental fear of third party products. They need to control everything they use. If they can't buy it they clone and extend it. They're like an anti-Debian. They've got an implicit social contract that everything running on a Windows system will be controlled by Microsoft. C# is nothing more than their version of Java that they get to control."
    author: "David Johnson"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-01
    body: "Happy April 1st!"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-02
    body: "> - No way of adding to an existing class\n\nApparently this is planned for the next version of C# (no joke, heard this from a colleague at work yesterday). Along with generics and anonymous functions."
    author: "Guillaume Laurent"
  - subject: "Re: Hell, Rich this isn't even the end of the start.."
    date: 2004-04-02
    body: "TIMELINE: April 2nd\n\n\"Apparently this is planned for the next version of C# (no joke, heard this from a colleague at work yesterday). Along with generics and anonymous functions.\"\n\nI think you can split the source of a class over more than one file, which is a different feature to Objective-C categories or ruby mixins. They are dynamic runtime based features.\n\nI think the combination of anonymous delegates as closures with iterators looks very nice.\n\nWhat I've read about Java 1.5 hasn't sounded very impressive (an apparently underwhelming generics implementation is supposed to be the star. I think that the C# 2.0 anonymous delegates are more interesting.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "> I think you can split the source of a class over more than one file\n\nYes, it's not dynamic as in ObjC or Ruby, but it should still be a useful feature (more than just a commodity to avoid large files, that is).\n\n> I think the combination of anonymous delegates as closures with iterators looks very nice.\n\nYes. Very Ruby-ish, very very useful.\n\n> What I've read about Java 1.5 hasn't sounded very impressive [...]\n\nI totally agree. Java 1.5 is basically a poor catch-up of C#, which has been IMHO a very impressive language from the start. Java tried to draw lessons from C++ but pushed it too far, and C# really restores the balance, while also drawing lessons from Java itself. I really hope C# will be available in full on Linux one way or another.\n\nIdeally, Novell would drop GTK, standardize on Qt/KDE, then put resources on Mono so it becomes useable for real work, and later on port (not bind) Qt to C# (or may be a binding would be acceptable after all, but offhand I'm not so sure it would)."
    author: "Guillaume Laurent"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "\"Yes, it's not dynamic as in ObjC or Ruby, but it should still be a useful feature (more than just a commodity to avoid large files, that is).\"\n\nYes, the feature that I think would be really useful is Aspect Oriented programming to customise the behaviour of an existing class. The non-fiction version of Kimono is based on a multi-language AOP environment with C#/Mono or Java/gcj as the primary languages. You might be able to write advice in ruby, perl as well as the primary language, as ruby, perl, java and C# will share the same common runtime in the Smoke library.\n\n\"Yes. Very Ruby-ish, very very useful\"\n\nAh, ruby now there's a *really* interesting language..\n\n-- Richard\n "
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "> The non-fiction version of Kimono is based on a multi-language AOP environment with C#/Mono or Java/gcj as the primary languages.\n\nI'm eager to see how this one comes out.\n\n> Ah, ruby now there's a *really* interesting language..\n\nIsn't it ? :-) I think it's by far the \"sexiest\" langage I've ever dabbled with (not that I've learned dozens, of course, but still). One of the very few which really make me *want* to code something with. I would say it completely obsoletes Python but in the long run it would seem that Python's more rigid syntax gives it an edge when it comes to long-term maintenance (especially when you're working on somebody else's code). It certainly obsoletes Perl, though.\n"
    author: "Guillaume Laurent"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "I would desribe ruby as a 'painterly masterpiece', the code and design is made up of very simple pieces, but the parts all seem to fit together in a coherent whole. That's like a master painter uses apparently simple brushstrokes. Just look at an individual brush stroke, and you can't see anything special - look at the whole painting and you can.\n\nThe non-fiction, already existing QtRuby and Korundum ruby bindings are a great way to write Qt or KDE apps. There've been various fixes along with DCOP support for the KDE 3.2.2 release. \n\nI haven't had much time to write anything big, but Alex Kellett has, and I really think they look very tidy and maintainable. The Smoke runtime has over 950 classes, and 29000+ methods, so the combination of the full api with ruby is potentially a killer RAD environment.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "\"...so the combination of the full api with ruby is potentially a killer RAD environment.\"\n\nWell, let's rephrase that. It WILL be a killer RAD environment. Please, no Visual Basic implementations...."
    author: "David"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "> I would desribe ruby as a 'painterly masterpiece'\n\nVery nice metaphor. :-)\n\n> so the combination of the full api with ruby is potentially a killer RAD environment\n\nWell, if my stint with gtkmm has taught me anything, it's that :\n- bindings *must* be distributed as a standard part of a platform, otherwise nobody will use them\n- bindings are great for quick'n drity prototypes or proof-of-concepts, but they break down very quickly when you delve into details where lies the proverbial Devil.\n\nThere could be exceptions to that last rule, but I'm pessimistic by nature on these issues. :-)\n"
    author: "Guillaume Laurent"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "The Smoke library is autogenerated as part of the kdebindings configure. It can then be used by perl, ruby, java and c#. The java and C# dynamic proxy interfaces will also be autogenerated as part of kdebindings configure.\n\nTo have a look at the current state of C# code generation, check out kdebindings from the HEAD branch. Edit smoke/kde/generate.pl.in and change the string '-fsmoke' to '-fkimono'. Then configure kdebindings with the '--enable-smoke=kde' option. The .cs sources will be generated in smoke/kde.\n\nYou'll see why we don't have to have 10-15 people hacking away on SourceForge for a year before they excitedly announce version 0.15 of xx language binding. I call that the 'knitting circle' approach to language bindings, slow and sociable but not particularly effective if you have production code depending on all those regular manual updates.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "I haven't looked at kdebindings yet. Are there sample perl/ruby progs in it ?"
    author: "Guillaume Laurent"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "Sample ruby programs are under kdebindings/qtruby and kdebindings/korundum.\n\nPerlQt/PerlKDE bindings that also use the Smoke library aren't in the KDE cvs, although it would obviously be brilliant to have them there.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Hell, Rich this isn't even the end of the star"
    date: 2004-04-02
    body: "\"Ah, ruby now there's a *really* interesting language..\"\n\nYer, Ruby is a really interesting language. It certainly has the name but it doesn't seem to have the hype to carry into 'mainstream' areas. Pretty sad really. Hopefully someone or something can do something about that :)."
    author: "David"
  - subject: "OS News validates it as non-April Fools Joke"
    date: 2004-04-01
    body: "Wow. Because osnews has it it must definately _NOT_ be an Aprils Fools joke and MUST be valid.\n\nDid you actually read the article on OS News? \n\n<paste>\nThe aim of the Kimono project on KDE is to write a complete wrapper for KDE/Qt using the Mono framework and is based on early work by KDE bindings hacker Richard Dale. Kimono uses KaXul, a fully XML-based representation of the UI, the Dot reports.\n</paste>\n\nThe _Dot_ reports. So they are feeding off _THIS_ article. Either you were being sarcastic and I didn't pick up on it for which I apologize, or you really need to dig deeper.\n\nTo the Dot, you had me going there for a minute too. :)\n"
    author: "jhohertz"
  - subject: "Re: OS News validates it as non-April Fools Joke"
    date: 2004-04-01
    body: "My connection blows. Didn't see you were joking. \n\nIt is I who am the retard! Sorry.\n"
    author: "jhohertz"
  - subject: "Re: OS News validates it as non-April Fools Joke"
    date: 2004-04-02
    body: "To be fair. OSNews took the bait too. The article text has changed since it was posted. Hats of to Richard Moore for the excellent prank :)\n"
    author: "Rayiner Hashem"
  - subject: "Meh."
    date: 2004-04-01
    body: "Why must every tech-related site do incredibly stupid April Fools' Day jokes?"
    author: "Shane Simmons"
  - subject: "Re: Meh."
    date: 2004-04-01
    body: "Maybe to fool the techies once in a while?"
    author: "Thomas"
  - subject: "Mono support"
    date: 2004-04-01
    body: "Well, probably not a April joke as it is possible. "
    author: "Bert"
  - subject: "Well Done"
    date: 2004-04-01
    body: "Yer this was a real stickler, because you really had to look at it to see how ridiculous the bottom two paragraphs are. At first glance the whole lot is totally believable because the projects are actually real and feasible. It also had triple the impact because of all the recent shenanigans.\n\n\"It is likely to slow things down at first, but in the longer term the benefits of using a real object oriented language like C# with the inherent advantages of a managed common runtime will be indisputable.\"\n\nVery funny! So I take it .NET and Mono are taking over completely then?\n\nVery good. It got me.\n\nPS. Anybody fancy taking up this project now?"
    author: "David"
  - subject: " Parrot was a Perl/Python April Fools Joke, too"
    date: 2004-04-01
    body: "Becareful people. While structured like a silly AFJ Perl6 got its start as a silly AFJ about a common runtime for Perl and Python (captialization optional). Now look  at it."
    author: "rjamestaylor"
  - subject: "Bah, too easy!"
    date: 2004-04-01
    body: "For those of you who haven't figured it out, yes this is joke... but all the tools described are real, so those who support it could make it happen. :-)\n\nHappy April 1st!\n"
    author: "Richard Moore"
  - subject: "Re: Bah, too easy!"
    date: 2004-04-01
    body: "I think that all of the stories that are jokes should be labeled as such.  Otherwise, many people get confused, and the website loses credibility."
    author: "Anonymous"
  - subject: "support"
    date: 2004-04-01
    body: "They seem to have been planning this for a while, theres even funding and all. See the press release about financial support for the project here...\nhttp://www.gatesfoundation.org/kimono"
    author: "Joseph H Camel"
  - subject: "C# incompatible with Mozilla"
    date: 2004-04-02
    body: "\"... Kimono will entail migrating to KaXul, a fully XML-based representation of the UI ... in the longer terms the benefits of using a real object oriented language like C# ... will be indisputable.\"\n\nI think the language we should be using for writing apps is JavaScript, not C#. Unlike C#, JavaScript implements a completely object based system featuring prototypes. Also, because JavaScript is an interpreted language, the need to compile KDE will disappear.\n\nKDE is currently written in a slow and obsolete language (C++), and compiling it can take days. This has prevented many users from trying KDE, and upset because they cannot upgrade to the latest version.\n\nLook at this:\nhttp://developer.kde.org/language-bindings/js/index.html\nhttp://dot.kde.org/986101966/"
    author: "There is no April Fool's Day"
  - subject: "nice one"
    date: 2004-04-02
    body: "Hopefully you are not against mono just besauce its a \"Ximian\" project."
    author: "qwerty"
  - subject: "AAAAAAAARRRRRGHHHHHHHHHH"
    date: 2004-04-03
    body: "Does really is it a \"Ximian\" project? ARRRRGHHHH...\n\n"
    author: "Frank Kafka"
---
Following the success of
the <a href="http://www.kde.org/announcements/announce-3.2.php">KDE 3.2 release</a>, the KDE project is pleased to announce a new ambitious project dubbed 'Kimono'. The aim
of the project is to write a complete wrapper for KDE/Qt using the <a href="http://www.go-mono.com/">Mono</a> framework and is based on <A href="http://members.shaw.ca/dkite/mar192004.html#Development%20Tools">early work</a> by <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/">KDE bindings</a> hacker extraordinaire <a href="http://dot.kde.org/983311036/">Richard Dale</a>. In addition to using Mono for the back-end work, Kimono will entail migrating to <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kaxul/">KaXul</a>, a fully XML-based representation of the UI. Kimono will be the main focus of the forthcoming KDE 3.3 release.





<!--break-->
<p>
<i>"We pretty much got things right with 3.2, so it's time to shake things
up a bit,"</i> said KDE project leader Matthias Ettrich. <i>"Exploring new frameworks will
help shake the KDE developers out of a rut, and lead to lots of new
ideas."</i>
<p>
It's not clear at the moment what the time frame for the Kimono project will be, so don't expect much in the short term. It is likely to slow things down at first, but in the longer term the benefits of using a real object oriented language like C# with the inherent advantages of a managed common runtime will be indisputable.













