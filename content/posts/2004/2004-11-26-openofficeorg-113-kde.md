---
title: "OpenOffice.org 1.1.3-kde"
date:    2004-11-26
authors:
  - "jholesovsky"
slug:    openofficeorg-113-kde
comments:
  - subject: "wow!"
    date: 2004-11-26
    body: "Congratulations, Jan.  I had no idea you could build OOo to support both KDE and GNOME at the same time!  Does it use GNOME icons under GNOME and KDE icons under KDE too?\n\nIs this what Novell Linux Desktop uses?"
    author: "ac"
  - subject: "Re: wow!"
    date: 2004-11-26
    body: "No, this version has just KDE icons. NLD version has Gnome icons in Gnome and KDE in KDE, and more features (gnome-vfs, Evolution connector, ...)"
    author: "Jan Holesovsky"
  - subject: "Re: wow!"
    date: 2004-11-26
    body: "Cool.  So OOo in NLD is also KDE-integrated.  You get KDE NWF and filedialog?  Any plans to support KDE IO/Slaves?\n\nJan, you just plain rock!"
    author: "ac"
  - subject: "Re: wow!"
    date: 2004-11-28
    body: "Question:\n\nPerhaps I'm info-blind, but, does this \"KDE 'Version'\" include Crystal only or does it actually inherit the icon theme that the (L)user is currently using?  (I'm sorry Everaldo!) I, personally, can't stand Crystal.  I use Noia-Warm.  Would OOo built on my main machine (gentoo) use Noia icons, or Crystal?\n\nI'm sorry to bother you about a trivial a question as this, but I hadn't noticed a better place to post the question.\n\nThanx, all!\nM.\n"
    author: "Xanadu"
  - subject: "Re: wow!"
    date: 2004-11-28
    body: "It's fixed to the included Crystal icons."
    author: "Anonymous"
  - subject: "what about FreeBSD?"
    date: 2004-11-28
    body: "KDE runs smoothly on FreeBSD! I would love to see a kde-ized version of openoffice running on FreeBSD :)"
    author: "anonymous"
  - subject: "No download address?"
    date: 2004-11-26
    body: "\nThe mirrors do not seem to sync, yet. Is there an address we can d/l OOo_1.1.3-kde_LinuxIntel_install.tar.gz ?\n\n"
    author: "G\u00f6rkem \u00c7etin"
  - subject: "Re: No download address?"
    date: 2004-11-26
    body: "ftp.kde.org"
    author: "Anonymous"
  - subject: "Re: No download address?"
    date: 2004-11-26
    body: "Thanks, the exact ftp URL is:\n\nftp://ftp.kde.org/pub/kde/packages/ooffice/\n\n"
    author: "G\u00f6rkem \u00c7etin"
  - subject: "RPMs"
    date: 2004-11-26
    body: "\"The new systems that build their packages from ooo-build (e.g. SUSE 9.2) do not need this package; check whether you have the KDE file dialog in your OOo before installing.\"\n\nIf SUSE packaged it, why not make those RPMs available at \nftp://ftp.kde.org/pub/kde/packages/ooffice/"
    author: "John SUSE  Freak"
  - subject: "Re: RPMs"
    date: 2004-11-26
    body: "Because they are SUSE 9.2-specific? The RPMs will become available when the whole SUSE 9.2 RPMs become available (aka SUSE 9.2 FTP Version)."
    author: "Anonymous"
  - subject: "Re: RPMs"
    date: 2004-11-26
    body: "Kind of, I'm using OO.org 9.1 rpms in SUSE 8.2... I'm not sure there is anything particular about SUSE OO.org rpms. But yes of course your answer is right, I just thought of it because I keep checking the FTP server for OO.org 9.2 rpms..."
    author: "John SUSE  Freak"
  - subject: "Re: RPMs"
    date: 2004-12-03
    body: "Suse 9.2 FTP-Installation will be available in mid January 2005, afaik\nJsut to help you save some time looking... ;)"
    author: "T.K."
  - subject: "Building from source"
    date: 2004-11-26
    body: "Where are the latest instructions for building this from source?  The kde.openoffice.org page seems to be outdated.  It was giving me 1.1.1."
    author: "George Staikos"
  - subject: "Re: Building from source"
    date: 2004-11-26
    body: "Try http://ooo.ximian.com/ - Hacker's Guide, About ooo-build.\n\n> The kde.openoffice.org page seems to be outdated.\n\nHe already said that. Thanks for repeating."
    author: "Anonymous"
  - subject: "localization"
    date: 2004-11-26
    body: "What about localization? Can I use this package with German menues?\nWhen will OOo start to use some modular translation method like KDE does since its beginning...?!"
    author: "Michael"
  - subject: "Re: localization"
    date: 2004-11-28
    body: "I support that!\nFor a lot of us, a English-only release is not very useful.\nAnd as everybody knows, compiling OOo requires a large computer and a lot of time, so getting the sources DIY is not a desirable approach to many (I'm still writting this in a 1998's computer, go figure).\n"
    author: "Shulai"
  - subject: "OpenOffice Ximian"
    date: 2004-11-26
    body: "I'm running OpenOffice Ximian 1.3.6 built with KDE supports under Gentoo..is your project related to the one I'm talking about? They seems to do the same thing, so I definetely think so, but you never can tell...:)"
    author: "Davide Ferrari"
  - subject: "Re: OpenOffice Ximian"
    date: 2004-11-27
    body: "It is, read the links of the story."
    author: "Anonymous"
  - subject: "2.0"
    date: 2004-11-27
    body: "I use OO 2.0 alpha version m62\nIt is very usable and stable and a huge advantage over 1.1.3\n\n1.1 was lala\n\n2.0 will rock\n\nonly the Icons look bad but kdefied ... nothing can stop us now..."
    author: "gerd"
  - subject: "Re: 2.0"
    date: 2004-11-28
    body: "I think that OO alpha is still _not_ usual a all. \nReally only use it for testing purpose ! "
    author: "HelloWorld82"
  - subject: "bug ?"
    date: 2004-11-27
    body: "it does not put carecteres as: \u00e1 \u00e0 \u00e3 \u00e2 \u00e4.\nthese are used characters in the Portuguese language."
    author: "Alexandre"
  - subject: "Re: bug ?"
    date: 2004-11-27
    body: "Hi Alexandre.\nIt's working for me.\nI'm using KDE 3.3.1 on Debian (unstable)."
    author: "Marcelo Barreto Nees"
  - subject: "Re: bug ?"
    date: 2004-11-28
    body: "what distro you are using?\nI am using gentoo + kde 3.3.1.\n\n(voc\u00ea \u00e9 brasileiro?)"
    author: "alexandre"
  - subject: "Eita (pfffeew)"
    date: 2004-11-29
    body: "O cara acabou de dizer que usa Debian e voc\u00ea perguta qual distro? (the guy just sai he uses Debian and you ask which distro?)"
    author: "Zab Ert"
  - subject: "Re: bug ?"
    date: 2004-11-29
    body: "Hi Marcelo!!\n\ni saw you're using debian... i'm using kde 3.2 and i tryed to install OOo from the package above... how can i get it working? i 've insatlled with ./setup in a folder /etc/local/OpenOffice and i've tryed ./soffice ... i can see something (splash) and than it crash.... could you help me ?"
    author: "daniele"
  - subject: "Re: bug ?"
    date: 2004-11-29
    body: "Hi Daniele.\n\nI just did the following, as root:\n# apt-get remove openoffice.org\n# tar -zxvf OOo_1.1.3-kde_LinuxIntel_install.tar.gz\n# cd OOo_1.1.3-kde_LinuxIntel_install\n# ./install\n\nand then, as a common user:\n$ /usr/local/OpenOffice.org1.1.3/setup\n\nTo open the writer (for instance):\n$ /usr/local/OpenOffice.org1.1.3/program/swriter\n\nAre you using Debian woody (stable)?"
    author: "Marcelo Barreto Nees"
  - subject: "Re: bug ?"
    date: 2004-11-30
    body: "I'm using sarge (testing) , that is to be upgraded ... i sw some packages are considered stable...\n\nI've tryied  several different installations .. also just installing it as common user, to use it for ex i run ./soffice, i saw the splash and than as i tryed to run an app (swriter) or whatever... it crashed.\n\nI'll install it again and i'll report the konqueor's error exactly\n\ncu"
    author: "daniele"
  - subject: "Re: bug ?"
    date: 2004-11-30
    body: "OOo chashes for me when I run ./soffice too.\nBut it works if I run swriter, for example.\n\nWhat occurs if you start swriter this way?\n$ /usr/local/OpenOffice.org1.1.3/program/swriter\n"
    author: "Marcelo Barreto Nees"
  - subject: "Re: bug ?"
    date: 2004-11-30
    body: "Weel i found the reason.... it crashed if i used swriter or ./swriter, i could run soffice and ./soffice but when i chosed new file opening draw, swriter impress and so on ... then it crashed.... i thought it was becouse of the windows style , and kcontrol --- windows style --- changeg in \"keramic\" --- and it worked . the style i used was \n\n\"kwin-style-baghira_0.5h-1sarge_i386.deb\"\n\nunder kde 3.2.\n\nNow i'm using a different baghira style (0.5.2) and it works perfectly.\n\ni was thinking It would be nice if in the next release of kde-openoffice was integrated the \"top mac style menu\"  ... \n\nAnd what about icons.... but this is another story....\n\nthanks marcelo it was a good stimulus to know someone was using this OOo under debian."
    author: "daniele"
  - subject: "Re: bug ?"
    date: 2004-11-30
    body: "Baghira is also responsible for several other filed application crash reports at bugs.kde.org. Better don't use it."
    author: "Anonymous"
  - subject: "Re: bug ?"
    date: 2004-11-30
    body: "yes but i love it.... now it works perfectly ... \n\nPS: i'll open a discussion about icons .... based on this:\n\nhttp://kde-look.org/content/show.php?content=7131&vote=good&tan=96629828 \n\nhttp://www.kde-look.org/content/show.php?content=8529\n\ntake a look ... "
    author: "daniele"
  - subject: "Re: bug ? (IMPORTANT, PLEASE FIX THIS)"
    date: 2005-02-28
    body: "Yes, I noticed that too. Looks like when passing non english character file names from KFileDialog (?) on to OOo to actually open the file the name gets mangled due to some charset incompatibillity and an error message is produced.\n\nIf I try to open a file named \"file_with_\u00f6.sxw\" in my home dir I get an OOo error dialog saying:\n\n\"Error loading document file:///home/bjorn/file_with_%F6.sxw: /home/bjorn/file_with_%F6.sxw does not exist.\"\n\nHope this helps the one who will solve this for all us non english users.. I'm to lazy/stupid/lazy and lazy.. ;)\n\nSayonnara!\n\n"
    author: "Bj\u00f6rn Sp\u00e5ra"
  - subject: "Re: bug ? (IMPORTANT, PLEASE FIX THIS)"
    date: 2006-02-16
    body: "Giuseppe said to me :\n\nThere is a bug report for this in qa.mandriva.com, and sound a problem in the kde filepicker, but the same happens into 2.0.1-1mdk but I don't have a patch (and the same bug seems persisting even in latest milestone m156).\n\nNote that you can change the dialog box either from env var:\n\nexport OOO_FORCE_DESKTOP=kde\n\nor\n\nexport OOO_FORCE_DESKTOP=gnome\n\nbefore running ooffice2.0, either from menu Tools/Option/General => Use OpenOffice.org dialogs."
    author: "Patrice Gauvin"
  - subject: "Mac support"
    date: 2004-11-27
    body: "Just a funny brainwave, but as Qt is available for the Mac as well, could this ease an OpenOffice.org port for the Mac that doesn't suck like the X11-version does?\nDoes anybody know whether that's planned for the 2.0 version or something?"
    author: "Arend jr."
  - subject: "Re: Mac support"
    date: 2004-11-27
    body: "Last I heard 2.0 was supposed to have native mac support. I hope it's true.. The X11 version is barely usable with only one mouse button."
    author: "Pete"
  - subject: "Fedora Core 3"
    date: 2004-11-27
    body: "> The KDE file dialog seems to hang OOo on Fedora Core 3 when it has Preview on (F11 in the dialog), but most probably it is a Fedora bug (treats unrecognized file types as sound).\n\nThe same bug happens here with Fedora's openoffice.org-kde-1.1.2-10.i386.rpm package and selecting eg OOo file types. Must be a messed up KDE packages."
    author: "Anonymous"
  - subject: "Screenshots????"
    date: 2004-11-27
    body: "Has anyone already made a series of *good*, *meaningful* screenshots of this killer thingie? \n\nCould these be made available publicly? So that we can point the press and all interested parties to them?\n\nThat breakthrough is really, really beautiful. But marketing-wise, KDE sucks. Again."
    author: "KDE Fan"
  - subject: "Re: Screenshots????"
    date: 2004-11-27
    body: "http://kde-apps.org/content/show.php?content=17334\nhttp://kde-apps.org/content/show.php?content=17332"
    author: "Anonymous"
  - subject: "Re: Screenshots????"
    date: 2004-11-27
    body: "> So that we can point the press and all interested parties to them?\n\nWhy does the \"we\" not include creating screenshots?\n\n> But marketing-wise, KDE sucks. Again.\n\nWho are you that you have the right to complain? What did you offer to help?"
    author: "Anonymous"
  - subject: "Re: Screenshots????"
    date: 2004-11-27
    body: "http://slashdot.org/article.pl?sid=04/11/27/1458225\nhttp://www.osnews.com/comment.php?news_id=8993"
    author: "ac"
  - subject: "Re: Screenshots????"
    date: 2004-11-27
    body: "I don't see *any* screenshot linked in those."
    author: "Anonymous"
  - subject: "Re: Screenshots????"
    date: 2004-11-27
    body: ">> But marketing-wise, KDE sucks. Again. <<"
    author: "ac"
  - subject: "Re: Screenshots????"
    date: 2004-11-27
    body: "Perhaps it would help if you write some accompanying text rather than just dumping links? :-)"
    author: "Anonymous"
  - subject: "Re: Screenshots????"
    date: 2004-11-30
    body: "I hope this is good enough for you (SuSE 9.1)\nhttp://www.infis.univ.trieste.it/~paulatz/OOo-1.1.3.jpg"
    author: "paulatz"
  - subject: "KDE (Crystal) icons"
    date: 2004-11-27
    body: "Does that mean that the icons are not installed as themeable?\n\nI note here that the FreeDeskTop.org icon theme specifications requires that all applications support, in addition to any other icon themes that they support, the fall back icon theme HiColor.\n\nThis is not just a OOo-KDE issue, it is a problem with many KDE applications.\n\nDespite clear instructions:\n\nhttp://developer.kde.org/~larrosa/iconthemes.html\n\n\"The main point here is that you _must_ install icons to hicolor, and then you can _optionally_ install icons to other icon themes (kdeclassic, ikons, locolor, crystalsvg, etc.).\"\n\nmany KDE applications do not have the required HiColor icons.  This appears to be, at least in part, do to a misunderstanding about the exact meaning of \"default\" icon theme.  Crystal is the current \"default\" icon theme but it is not the fall back icon theme and it must not be treated as though it is the only or standard (unthemed) icon theme.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE (Crystal) icons"
    date: 2004-11-28
    body: "> Does that mean that the icons are not installed as themeable?\n\nCorrect. Looking forward you volunteering to implement it."
    author: "Anonymous"
  - subject: "Re: KDE (Crystal) icons"
    date: 2004-11-28
    body: "In fact James is already working in that area (not OOo, but other stuff).  And I think he has a point when he says the app developers should get it right in the future.  After all, it's not more work to install icons in the correct location instead of the wrong one (this is true at least for KDE apps, dunno about OOo).  Fixing incorrect icon locations *is* additional work. \n\nWhat he tried here was increasing the awareness for the problem.  "
    author: "cm"
  - subject: "Re: KDE (Crystal) icons"
    date: 2004-11-30
    body: "i sow the OOo icons are  \"bmp\" and not \"png\":\n\nfor the su ./setup --->/usr/local/OpenOffice.org1.1.3/share/config/symbol\n\n\nfor me -----> /home/daniele/.openoffice/share/config/symbol/\n\ni found this script:\n\nlinks : \n1) http://www.kde-look.org/content/show.php?content=8529\n2) http://www.kde-look.org/content/show.php?content=7131\n\ni 've tryed it ... i'm not a developer ... it's simplier if you try it than explain... but it's seems it can't work for this OOo, it generate a single bpm with xml file in this folder\n\n/home/daniele/.openoffice/share/config/soffice.cfg/\n\nbut actually the icons are in \n\n/home/daniele/.openoffice/share/config/symbol\n\ni think it could be a good idea to fix .... \n\nPS* are you sure it's just needed to put the icons (png)in hicolor? ... OOo is linked to bpm ..... or not O_o?"
    author: "daniele"
  - subject: "WIll it hurt gnome?"
    date: 2004-11-27
    body: "So if i compile and install this onto my gnome 2.8 ubuntu system, it won't hurt it, will it? and will it work right?"
    author: "Abel"
  - subject: "Re: WIll it hurt gnome?"
    date: 2004-11-27
    body: "What do you want with this build if you want to compile with ooo-build? Just choose to compile a full GNOME integrated version then."
    author: "Anonymous"
  - subject: "Re: WIll it hurt gnome?"
    date: 2004-11-28
    body: "I think this version is building now on the ubuntu for Hoary so tomorrow or Monday it will be available with a simple refresh and update."
    author: "Olafur Arason"
  - subject: "Other window managers"
    date: 2004-11-28
    body: "I really like the KDE look of OpenOffice.org, but it looks really bad in other window managers.\nWhat do I do to enable OO.org's KDE mode under other window managers (e.g. Window Maker)?\n"
    author: "Alex"
  - subject: "Re: Other window managers"
    date: 2004-11-28
    body: "Set OOO_FORCE_DESKTOP=kde"
    author: "Anonymous"
  - subject: "OOo ... KDE?"
    date: 2004-11-29
    body: "Pardon my ignorance. I've been using OOo for a couple of years at least having started on a Windows platform and moved to Linux (SuSE) with KDE within the past nine months.\n\nWhat is meant by this talk about KDEizing OOo?"
    author: "J. E. Lang"
  - subject: "Re: OOo ... KDE?"
    date: 2004-11-29
    body: "See the slides here: http://artax.karlin.mff.cuni.cz/~kendy/ooo/SUSE_Labs_Conference-2004/"
    author: "Jan Holesovsky"
  - subject: "Re: OOo ... KDE?"
    date: 2004-11-29
    body: "Very nicely done and explained good job !"
    author: "Khalid"
  - subject: "Mr."
    date: 2004-11-29
    body: "Could someone give an overview about the status of the project and the current state, about icon theming, gnome/kde integration in the same build, etc. (once kde.openoffice.org is outdated)?!"
    author: "R\u00f4ney Eduardo O. Santos"
  - subject: "Looks cool and all in the screenshots..."
    date: 2004-11-29
    body: "...but how do you compile without pam!??!\n\nI don't have pam and don't want it.  I applied the patch to allow compilation without pam (for OOo 1.1.2, I believe) and it gets past the part where it was dying on pam.  But now it's giving me a bunch of lip about 093-norwegian-translation.diff patch failing."
    author: "MIV"
  - subject: "1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-11-30
    body: "The installer looks fine and the programs, such as swriter open and look great.  It's just when I try to File | Open or <CTRL>+O, the dialog *never* appears.  Other dialogs like File | Print do seem to work.\n\nI appreciate any suggestions. \n\nJeff"
    author: "Jeff Cann"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-11-30
    body: "P.S.  When I try to Save or Save As, the app hangs."
    author: "Jeff Cann"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-11-30
    body: "What happens if you run \"./kdefilepicker\" and then \"exec <enter>\"?"
    author: "Anonymous"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-01
    body: "./kdefilepicker: error while loading shared libraries: libsal.so.3: cannot open shared object file: No such file or directory\n"
    author: "Jeff Cann"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-01
    body: "set LD_LIBRARY_PATH=~/OpenOffice.org1.1.3/program before"
    author: "Anonymous"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-10
    body: "So did this work for anyone?  I'm running this with VectorLinux 4.3 (Slackware based).  Since I couldn't open or save files, it seemed to be a nice start, but only eye-candy at this point. I've since gone to OOo-1.1.4rc but would migrate backwards if I could get this version to work properly.  For me, at least, the icon set makes OOo so much more useable.  My wife likes it too. :-)\n"
    author: "Tim NIiler"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-13
    body: "Not for me (I'm running RH 9.0). I tried setting LD_LIBRARY_PATH but with no luck, I still can't use the Open and Save dialogs.\n\nI'm still looking around for a solution though, I really like the kde-integrated interface, let alone it eliminates some printing problems I had with greek fonts.\n"
    author: "Kostas Magkos"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-13
    body: "> So did this work for anyone?\n\nThis was not intended to \"fix\" OOo but make the \"kdefilepicker\" debugging work for Jeff/someone. But as nobody tries it and reports back, how shall it be fixed?"
    author: "Anonymous"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-13
    body: "Fair enough...I will try it again and send in the debug info sometime this week."
    author: "Tim Niiler"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "ok, after setting LD_LIBRARY_PATH I ran kdefilepicker and I get:\n\n[kmag@pluto program]$ ./kdefilepicker\nkdefilepicker, an implementation of KDE file dialog for OOo.\nType 'exit' and press Enter to finish.\nexit\nkdefilepicker: relocation error: kdefilepicker: undefined symbol: _ZN7QString7reserveEj\n"
    author: "Kostas Magkos"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "What Qt version do you have installed? Only Qt 3.1.1 of RedHat 9?"
    author: "Anonymous"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "yes, that's right"
    author: "Kostas Magkos"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "The same when issuing exec:\n\n[kmag@pluto program]$ ./kdefilepicker\nkdefilepicker, an implementation of KDE file dialog for OOo.\nType 'exit' and press Enter to finish.\nexec\nkdefilepicker: relocation error: kdefilepicker: undefined symbol: _ZN7QString7reserveEj\n[kmag@pluto program]$ ./kdefilepicker\n"
    author: "Kostas Magkos"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "> kdefilepicker: relocation error: kdefilepicker: undefined symbol: _ZN7QString7reserveEj\n\nSeems to be a symbol introduced by Qt 3.2. So it requires Qt 3.2 (or even 3.3)."
    author: "Anonymous"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "Qt > 3.1 requires a KDE upgrade (currently 3.1-13) on my RH 9. Since this is just my desktop at work I have to think twice before doing anything messy and time-consuming. :-(\nDo you have the slightest idea how easily or diffuculty such an upgrade should go?\n\n\nThanks anyway for your help!"
    author: "Kostas Magkos"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: "> Qt > 3.1 requires a KDE upgrade (currently 3.1-13) on my RH 9\n\nWhy that? Qt 3.3 is backward compatible.\n\n> Do you have the slightest idea how easily or diffuculty such an upgrade should go?\n\nWhy not install Qt 3.3 in parallel to your current Qt version and write a little script with runs OOo with that Qt installation?"
    author: "Anonymous"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-14
    body: ">> Qt > 3.1 requires a KDE upgrade (currently 3.1-13) on my RH9\n>\n>Why that? Qt 3.3 is backward compatible.\n\nIt's a redhat dependency thing: qt 3.3 depends on redhat-artwork > 0.100 and redhat-artwork 0.100 needs kdebase > 3.2 and then you pretty much need to upgrade the whole kde system.\n\n>Why not install Qt 3.3 in parallel to your current Qt version and write a little script with runs OOo with that Qt installation?\n\nbut still I have to upgrade redhat-artwork....\n\n"
    author: "Kostas Magkos"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-20
    body: "Installed on Slackware 9.1. Same issues in \"Open\" and \"Save as\" dialog. I have \n set LD_LIBRARY_PATH as recommended in the post BUT when I try ./kdefilepicker I've a new error message:\n\n./kdefilepicker: /usr/lib/./libgcc_s.so.1: version `GCC_3.3' not found (required by /opt/OpenOffice.org1.1.3/program/libstdc++.so.5)\n\nit seems the package was compiled with GCC3.3 and requires this version of libraries.\n\nAbout to rollback to 1.1.1... 'cause no time and no disk space to upgrade to Slackware 10 and/or to upgrade to GCC3.3....Damn..."
    author: "MaxBaldiz"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-20
    body: "Try the 1.1.4 release.  It is currently working for me just fine.  There are no crashes, and they are using their old binary installer."
    author: "Tim NIiler"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2005-01-18
    body: "Okay...Here's the synopsis.\n\nBack in December, I didn't have a chance to do this and needed the space on my harddrive so I deleted OOO-1.1.3.  Since then, I downloaded ooo-build and tried to do it all myself.  Everything was peachy in the build except for some complaints about the psprint_config patch (which I implemented manually) and LD_LIBRARY_PATH not finding KDE (I set that manually to include /opt/kde/lib), and then everything compiled. \n\nSo this morning, I come down and setup my newly baked OOO to see what happened.  And everything is fine EXCEPT FOR THE FILE-OPEN DIALOG.  \n\nThinking that I just wasted a bunch of time, I am about to delete the thing when I remember the LD_LIBRARY_PATH comment here and note that I also had the same issue in the middle of compiling.  I set the variable, and when I restart OOO, the filepicker works fine!\n\nThanks!\n\nMy impression is that I could have just downloaded the build from here instead of doing it myself and this solution would have still worked.  On the plus side, OOO now starts in under 5 seconds on my box. :-)"
    author: "Tim Niiler"
  - subject: "Re: 1.1.3-KDE on RHAT 9.0 File Open Dialog ?"
    date: 2004-12-01
    body: "BTW - the non KDE 1.1.3 version of Open Office works fine on this same box."
    author: "Jeff Cann"
  - subject: "Debian"
    date: 2004-12-08
    body: "Users of Debian can do\napt-get install openoffice.org-kde\nif experimental is in /etc/apt/sources.list"
    author: "Qerub"
  - subject: "no suitable windowing system found, exiting"
    date: 2004-12-19
    body: "Hello,\nthe installation of OOo-kde-1.1.3 fails with the following message:\n\nno suitable windowing system found, exiting\n\nI work with SuSe 9.1 and kde3.2.3. The SuSe version of OOo-kde-1.1.1works fine\nand OOo-1.1.3 works as well.\nI like the kde version, so I would be very pleased, if someone can help me.\n\nThanks Christoph"
    author: "Christoph Sch\u00fctz"
  - subject: "Re: no suitable windowing system found, exiting"
    date: 2004-12-20
    body: "Read the story! It's listed under \"Known problems\"."
    author: "Anonymous"
  - subject: "Re: no suitable windowing system found, exiting"
    date: 2004-12-20
    body: "Sorry! Just read the README included in the package, but it seems to be the original OpenOffice one. \nIt works all well now.\nThank you Christoph."
    author: "Christoph Sch\u00fctz"
  - subject: "Re: no suitable windowing system found, exiting"
    date: 2005-01-17
    body: "So, and where can I find that 'known issues' item at all? It's not in the readme"
    author: "Jack"
  - subject: "Re: no suitable windowing system found, exiting"
    date: 2005-02-12
    body: "Look at the top of this page..\n"
    author: "Bj\u00f6rn Sp\u00e5ra"
  - subject: "Re: no suitable windowing system found, exiting"
    date: 2005-05-03
    body: "to debian, \n\nOnly Install lib:\n\nsudo apt-get install libstartup-notification0"
    author: "SuporteTecnicoID"
  - subject: "Re: no suitable windowing system found, exiting"
    date: 2005-12-06
    body: "On Mandriva, you need to also install libsndfile"
    author: "Richard Neill"
  - subject: "1.1.4?"
    date: 2005-02-22
    body: "where is one for 1.1.4?  a binary."
    author: "yoyo"
---
<a href="http://www.openoffice.org/">OpenOffice.org</a> 1.1.3 <a href="http://kde.openoffice.org/">with KDE integration</a> is now available for download. It also features a lot of other improvements over the stock OOo (including the GNOME integration bits; but do not be afraid, it does not link against Gtk+ in KDE, and vice versa), because it is built from the <a href="http://ooo.ximian.com/ooo-build.html">ooo-build</a> codebase.






<!--break-->
<p>Download: <a href="http://download.kde.org/download.php?url=packages/ooffice/OOo_1.1.3-kde_LinuxIntel_install.tar.gz">Installation set for Linux i386</a> (~80MB).</p>

<p>Features:
<ul>
<li>The current stable version of OpenOffice.org with many ooo-build patches and improvements</li>
<li>KDE Native Widget Framework</li>
<li>KDE (Crystal) icons</li>
<li>KDE file dialog (Open, Save As)</li>
<li>KDE splash screen by Dariusz Arciszewski</li>
<li>Gtk+ NWF and file dialog when executed in Gnome</li>
</ul>

Known problems:
<ul>
<li>You need libstartup-notification installed, otherwise it fails to run with "no suitable windowing system found, exiting."</li>
<li>The KDE file dialog seems to hang OOo on Fedora Core 3 when it has Preview on (F11 in the dialog), but most probably it is a Fedora bug (treats unrecognized file types as sound).</li>
<li>The new systems that build their packages from ooo-build (e.g. SUSE 9.2) do not need this package; check whether you have the KDE file dialog in your OOo before installing.</li>
</ul></p>

<p>The project's homepage <a href="http://kde.openoffice.org/">http://kde.openoffice.org</a> is a bit outdated at the moment, but the work still continues. The main concern is OOo 2.0 now, see the <a href="http://ooo.ximian.com/ChangeLog">ooo-build ChangeLog</a>.</p>

<p>Help of an artist is needed for OOo 2.0: It contains a lot of new icons, the default ones are not acceptable for modern KDE desktop. Please drop me a mail if you are able (and willing) to draw some of them. :-)</p>



