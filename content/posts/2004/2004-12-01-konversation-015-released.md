---
title: "Konversation 0.15 Released"
date:    2004-12-01
authors:
  - "kteam"
slug:    konversation-015-released
comments:
  - subject: "great news"
    date: 2004-12-02
    body: "I love konversation, waiting for debian to pack it for me and to test the scripting engine, btw which scripts are supported? xchat?"
    author: "felipe"
  - subject: "Re: great news"
    date: 2004-12-02
    body: "Debian packs are already at http://debian.houseofnate.net"
    author: "cartman"
  - subject: "Re: great news"
    date: 2004-12-02
    body: "cool, i'm compiling it myself right now (couldn't resist) but i guess i'm giving those deb's a try as well.\n\nwhat about scripting stuff?"
    author: "felipe"
  - subject: "Re: great news"
    date: 2004-12-02
    body: "We don't have a scripting engine yet but its planned. But we got many scripts that makes Konversation using fun like /weather if you have KWeather installed and /juk , /amarok for guess for what? :)\n"
    author: "cartman"
  - subject: "kontact "
    date: 2004-12-02
    body: "are there screenshots of the kontact integration? how exactly is it integrated?"
    author: "me"
  - subject: "Re: kontact "
    date: 2004-12-02
    body: "There is no \"container\" integration, at least last time I checked. It's the useful features that count. Konvi, just like Kopete, makes use of KIMProxy, which allows for assigning nicknames to contacts. as a consequence, as soon as you read a mail from someone in the addressbook, you will see his online status (along with a pic if you have). Most of this requires Kontact HEAD, though."
    author: "Daniel Molkentin"
  - subject: "Re: kontact "
    date: 2004-12-02
    body: "The picture requires Kontact HEAD, but the online status is already shown in Kontact 1.0.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kontact "
    date: 2004-12-02
    body: "Well, I didn't get it working for when you have the image as a url, but it should work in Kontact 1.0 if the image is included in the contact.\n\nBtw, the fact that it works with kontact 1.0 is because Bille managed to get the changes required in about a week before Kontact 1.0 release :)\n"
    author: "JohnFlux"
  - subject: "Re: kontact "
    date: 2004-12-02
    body: "argh, me being stupid, i misread.  You were talkinga about the picture in kmail, not in konversation.  My bad."
    author: "JohnFlux"
  - subject: "Handbook (with screenshots)"
    date: 2004-12-02
    body: "http://docs.kde.org/en/HEAD/kdeextragear-2/konversation/\n"
    author: "Gary Cramblitt (aka PhantomsDad)"
  - subject: "Re: Handbook (with screenshots)"
    date: 2004-12-14
    body: "Just curious if this handbook should be available with my Suse 9.1 (Supplementary Update) installation of 0.15?  The file is not found when I try to access via Help....   Nice to be able to review it here - Thanks."
    author: "IronMan"
  - subject: "HIG compliance"
    date: 2004-12-02
    body: "The last time I checked this app it was quite functional\nbut the UI was horrible. Especially needed is\na KMDI/IDEAL interface. The IDEAL mode like in KDevelop etc.\nwouldl really be great for an IRC app IMO. Imagine \nstatus windows, channel lists etc. to be dockable to the border."
    author: "Martin"
  - subject: "Re: HIG compliance"
    date: 2004-12-02
    body: "KMDI support is actually in the works.Possibly coming in 0.16."
    author: "cartman"
  - subject: "Re: HIG compliance"
    date: 2004-12-02
    body: "You should check again, the UI is now absolutely wonderful. Slick and gorgeous. Everything is well spaced, and the usability is frankly ingenious.\n\nI switched from irssi yesterday, so should you!"
    author: "Max Howell"
  - subject: "Ingenious???"
    date: 2004-12-02
    body: "While I will agree Konversation is a great IRC client, I think you should give credit where credit is due. Konversation's UI design is hardly \"ingenious\", it is basically a QT port of XChat's UI, which has been around for much, much longer.\n\nThat said, hats off to Konversation, which is a great client in and of itself!"
    author: "Jason Keirstead"
  - subject: "Re: Ingenious???"
    date: 2004-12-02
    body: "What parts of XChat's UI are genuine?"
    author: "Anonymous"
  - subject: "wong!"
    date: 2004-12-02
    body: "Actually you're incorrect, it is a QT attempt at reproducing the AMirc irc client interface. AMirc was an irc client availale for amiga, both Xchat and Konversation have been modelled on this client. \nhttp://www.vapor.com/products/?prod=amirc"
    author: "illogic-al"
  - subject: "KDE HIG status"
    date: 2004-12-02
    body: "Hey Folks\n\nWhat's the status for KDE-HIG ? A little browsing on google gives me a feeling that it is at an early status ? Is this a priority for KDE 4 ? (I mean, a usability revision based on the HIG or some other guideline)\n\nThanks, and cheers,"
    author: "MandrakeUser"
  - subject: "Re: KDE HIG status"
    date: 2004-12-02
    body: "Allright, I found some more:\nhttp://usability.kde.org/hig/\n\nYes, a guideline is being created right now. In the meantime, I would still like to know if this is going to be a priority for KDE 4\n\nThanks !"
    author: "MandrakeUser"
  - subject: "Konvi RAAAAAAAAAAWKS"
    date: 2004-12-02
    body: "_almost_ as hard as amaroK rocks.\nIt's a seriously solid app though. I've been using cvs for months now seeing the changes and it's been absolutely awe inspiring to see the dedication these guys've put in. Kudos to all involved ;-)\nNow gimme back my pre-release nicklist icons!"
    author: "illogic-al"
  - subject: "Re: Konvi RAAAAAAAAAAWKS"
    date: 2004-12-02
    body: "Wait for Christmas! ;)"
    author: "cartman"
  - subject: "Re: Konvi RAAAAAAAAAAWKS"
    date: 2004-12-03
    body: "> Now gimme back my pre-release nicklist icons!\n\nYes, I prefer those, too. Fortunately, you can easily override the official ones easily: Just create a directory ~/.kde/share/apps/konversation/icons/crystalsvg/16x16/actions/ and put the old icons there (obtainable by doing \"cvs up -D2004/11/20\" in kdeextragear-2/konversation/images/nickicons/. The current icons are a bit too obtrusive for my taste, especially the big, dark, colored default (= redundant) icon. The pre-release icons are elegant and unobtrusive, almost Art Deco/Jugendstil-like. They look much better in my color setup.   :-)"
    author: "Melchior FRANZ"
  - subject: "dcc with kio"
    date: 2004-12-02
    body: "Shin worked hard on the dcc with kio, and it shows.\nOne cool thing is to do:\n\n/dcc send nick http://www.slashdot.org\n\n:)"
    author: "JohnFlux"
---
After months of heavy development we are pleased to announce the release of Konversation 0.15! <a href="http://konversation.sourceforge.net/">Konversation</a> is a simple and easy to use IRC client. New features include a brand-new <a href="http://kontact.org/">Kontact</a> integration, better KDE HIG compliance, DCC rewritten with KIO, support for SSL IRC servers and much more!


<!--break-->
<p>Featured Changes:
<ul>
<li>Improved settings dialog</li>
<li>New network based server settings</li>
<li>SSL IRC server support</li>
<li>Ported socket code to KNetwork, should solve most connection problems</li>
<li>KIO-fied local I/O on DCC send/receive</li>
<li>Full irc:/ URL support (channel name & password now supported in url)</li>
<li>Get default username and ident information from system for new users</li>
<li>Improved display of bi-directional text</li>
<li>Support for bouncer prefixes in nick completion</li>
<li>Scripts now work via "/script" or "/exec script"</li>
<li>XChat-like "/charset" command</li>
<li>Per-channel encoding settings</li>
</ul></p>
<p>New Options:
<ul>
<li>"Open Watched Nicks Online panel on startup"</li>
<li>Stay in systray all the time (hides window from taskbar)</li>
<li>Get own IP from IRC server for DCC send/chat</li>
<li>OSD positioning</li>
<li>Automatically issue /WHO to display away status of nicks in nick list</li>
<li>DCC port range support</li>
</ul></p>

<p>
Debian, SUSE and Mandrake packages are in progress, however you can <a href="http://osdn.dl.sourceforge.net/sourceforge/konversation/konversation-0.15.tar.bz2">download the source tarball</a>. </p>

<p>Unfortunately www.konversation.org is down at the moment, but please refer to our temporary page at <a href="http://konversation.sourceforge.net/">http://konversation.sf.net</a>.
</p>

<p>We are greatly driven by user feedback, so if you wish to see a new feature in Konversation please join us on our IRC channel at <a href="irc://irc.kde.org/konversation">irc.kde.org, #konversation</a>.</p>

<p>--The Konversation Team.</p>




