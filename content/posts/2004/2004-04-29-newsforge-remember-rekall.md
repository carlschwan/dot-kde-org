---
title: "NewsForge: \"Remember Rekall?\""
date:    2004-04-29
authors:
  - "binner"
slug:    newsforge-remember-rekall
comments:
  - subject: "Hmm"
    date: 2004-04-29
    body: "I wanted to use rekall, and then could not work out if I could download a version for windows.\nI searched google, looked around.  Trolltech really need to update and fix their website for rekall.\nOf course, rekallrevealed.org was hacked, so I might have just gotten a bad impression..\n\nPlease, please trolltech - put a windows version somewhere.  If you want to charge for it, then _make it clear_.  Add it to rekallrevealed.org under download that you have to pay for it, and add a very simple way for me to pay for it.\n\n"
    author: "John Tapsell"
  - subject: "Re: Hmm"
    date: 2004-04-29
    body: "s/Trolltech/theKompany/g"
    author: "Aaron J. Seigo"
  - subject: "Re: Hmm"
    date: 2004-04-30
    body: "opps, yeah my bad ;)\n"
    author: "John Tapsell"
  - subject: "Re: Hmm"
    date: 2004-04-29
    body: "You can't have a GPL version of Rekall for Windows since there is no GPL version of Qt for Windows.  We have a Windows version available for sale simply at www.thekompany.com/products/rekall/"
    author: "Shawn Gordon"
  - subject: "Re: Hmm"
    date: 2004-04-30
    body: "\"You can't have a GPL version of Rekall for Windows\"\n\nYes you can. Someone buys a version of Qt and then releases their software under the GPL license. It isn't likely to happen, but people write GPL'd software for Windows by buying Visual Studio. Quite how Trolltech would react to this I don't know, as they think GPL'd software on Windows is a bit hazy."
    author: "David"
  - subject: "Oooh..."
    date: 2004-04-29
    body: "I suppose that was Total Rekall not final Rekall :)"
    author: "Vinnie The Pooh"
  - subject: "Web"
    date: 2004-04-29
    body: "Here's an idea...\nAs well as forms, let it create web forms, and integrate it with a webserver. :)\nThen you can have one program to do both a fat client and be a server for a thin browser based client.\nSorta like domino..\n"
    author: "John Tapsell"
  - subject: "Re: Web"
    date: 2004-04-29
    body: "That's alaready a subproject started within Kexi project. More info will be \navailable on: \n\nhttp://www.kexi-project.org/wiki/wikiview/index.php?Web.\n\nPre-alpha sceenshot:\n\nhttp://www.linux-magazin.de/Artikel/ausgabe/2004/04/kexi/abb2.jpg\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: Web"
    date: 2004-04-29
    body: "Will there be any integration of kexi and rekall?"
    author: "John Tapsell"
  - subject: "Re: Web"
    date: 2004-04-30
    body: "We all know that 'integration' term is so uncertain, do we?\n\n-If you mean: data-exchangeability - yes, at least using DBMS functionality.\n\n-If you mean: database schema portability - maybe yes (at least a subset, probably using XML schema that have to befined within OASIS)\n\n-If you mean code merging, here are some problems: 1) Kexi is KDE app, Rekall -plain Qt app; 2) Kexi is plugin-based, Rekall not so deeply, AFAIK; 3) licensing issues: Kexi -LGPL on *nix, win32/64, Rekall -GPL on *nix.\n\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: Web"
    date: 2004-04-30
    body: "Thanks.\n\nNot to put down the wonderful things that theKompany does, but it would have been that little bit nicer if they used LGPL so that it could be integrated in KDE, and projects such as kexi.\nBut I guess they thought that wouldn't have been a good thing commerically.\n"
    author: "John Tapsell"
  - subject: "Re: Web"
    date: 2005-02-08
    body: "Why would full blown software be released under library-gpl?\nIt appears you don't fully understand these licenses and what each of their purpose is."
    author: "broken_bazooka"
  - subject: "Re: Web"
    date: 2004-04-30
    body: "Sorry, a another reply...\nSince you are the copyright holders, would it be possible for you (kexi) to release a win32 binary? (Presumably not under the lgpl)\n\nHmm are you allowed to release a win32 binary (and source) under the lgpl since you can't release the win32 QT source code?"
    author: "John Tapsell"
  - subject: "Re: Web"
    date: 2004-04-30
    body: "Just look at following site on the top right ;-)\n\nhttp://iidea.pl/~js/qkw/\n"
    author: "anno"
  - subject: "Re: Web"
    date: 2004-04-30
    body: "Rekall *is not* a plain-qt app. It can be built that way, but the KDE versions is, well, KDE:)\n\nRekall *is* plugin based:) Anyone who would like to write a plugin for Rekall, feel free to go ahead! EMail me and i'll tell you what is what.\n\n\nmike\nwww.rekallrevealed.org\n"
    author: "mikeTA"
  - subject: "tora"
    date: 2004-04-29
    body: "I just want to remind you of\n\nTOra\n\none of the best data base admin tools. Ihn my humble opinion Rekall could be a good program but it has to be modular, so that a true framework can be created, I just remind you of Gideon."
    author: "gerd"
  - subject: "Forms?"
    date: 2004-04-29
    body: "http://www.gnuenterprise.org/gallery/forms.php\n\nIs there an integration plan?"
    author: "Gerd Br\u00fcckner"
  - subject: "who cares about Rekall?..."
    date: 2004-04-30
    body: "...when we have kexi coming and it looks really amazing I don't mean to flame but seriously kexi's gonna kick ass :)"
    author: "Pat"
  - subject: "Re: who cares about Rekall?..."
    date: 2004-04-30
    body: "Rekall exists now in a fully usable form. Kexi may well kick a lot of butt when it's done, but for now Rekall is an amazing tool to have available."
    author: "Greg"
  - subject: "Re: who cares about Rekall?..."
    date: 2004-05-02
    body: "Well I'm rather impartial on this, except to say that we need something 'Access like' to be able to knock up good databases, queries, forms etc. Kexi looks very interesting, as does Rekall, so I'm happy to have them both. The commercial Rekall will not be a problem for most people as it is so cheap that will be free software to many businesses and organisations!"
    author: "David"
---
<a href="http://www.newsforge.com/">NewsForge</a>'s editor Joe Barr <a href="http://software.newsforge.com/software/04/04/20/1823249.shtml?tid=150&tid=72&tid=82">spent  some time</a> with the current development version of Rekall 2.2.  <a href="http://www.rekallrevealed.org/">Rekall</a> is a dual-licensed GUI database front-end with aspirations of becoming Linux's answer to Microsoft Access (screenshots of <a href="http://www.rekallrevealed.org/toplevel/screenshots/tables.shtml">tables</a>, <a href="http://www.rekallrevealed.org/toplevel/screenshots/forms.shtml">forms</a> and <a href="http://www.rekallrevealed.org/toplevel/screenshots/debugger.shtml">debugger</a>). He finds it already very usable today and predicts a bright future. The release of the final Rekall 2.2 version is expected for late April or May.

<!--break-->
