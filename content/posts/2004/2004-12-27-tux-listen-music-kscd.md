---
title: "TUX: Listen to the Music with KsCD"
date:    2004-12-27
authors:
  - "mgagn\u00e9"
slug:    tux-listen-music-kscd
comments:
  - subject: "Broken link"
    date: 2004-12-27
    body: "The link to the article is broken.\n"
    author: "MdIig"
  - subject: "Re: Broken link"
    date: 2004-12-27
    body: "\"Miguel de Icaza is gay\"\n\nReally? So tell him I can take care of fulfilling his pretty wife's desires if you don't mind."
    author: "charles dickens"
  - subject: "Re: Broken link"
    date: 2004-12-27
    body: "Must have missed that one."
    author: "David"
  - subject: "Re: Broken link"
    date: 2005-01-03
    body: ">\"Miguel de Icaza is gay\"\n>\n>Really? So tell him I can take care of fulfilling his pretty wife's desires if >you don't mind.\n\n^ i love the internet ! "
    author: "some guy"
  - subject: "Re: Broken link"
    date: 2004-12-27
    body: "> Miguel de Icaza is gay\n\nReminds me of drunk Finns singing 'Mats Sundin \u00e4r homosexuell' in\nSweden/Finland ice hockey game after he had scored twice against us\n:)\n"
    author: "Anonymous Coward"
  - subject: "link"
    date: 2004-12-27
    body: "http://www.tuxmagazine.com/node/1000037\n\nI have always liked kscd. It does its one job well."
    author: "Ian Monroe"
  - subject: "Re: link"
    date: 2004-12-27
    body: "This is a case in point. Kscd is good because it does something\nwell delimited: playing CDs. With the new multimedia programs\nlike Kafeine and stuff I never know well what they are capable until\nI cannot open the file/media I need. I think KDE should make\nclear what application is used for which multimedia type (of course\nthis decision can change depending on the advances on mm players,\nbut it should be clearly made in each version of KDE).  Clearly, this will\nnot prohibit people from use other programs, they just need to install\ntheir prefered application, but as a matter of policy and user interface\nthis distinction should be made."
    author: "anonymous"
  - subject: "Re: link"
    date: 2004-12-27
    body: "Kaffeine uses libxine so it can play any format libxine can. i'm curious could you please tell us what file you can't open with kaffeine? you probably don't have the w32 codecs installed cause xine and mplayer can play every file formart I'm aware of."
    author: "Pat"
  - subject: "Re: link"
    date: 2004-12-31
    body: "I guess it is some sort of configuration problem, then. It happens also to myself; to be able to open a video in xine and not kaffeine (for instance, wmv)."
    author: "blacksheep"
  - subject: "digital playback"
    date: 2004-12-27
    body: "I read \u00abIf you are using KDE 3.3 (like me) you see an option 'Use direct digital playback'\u00bb, that's a really good news :)"
    author: "Capit Igloo"
  - subject: "I would like to hear somethings from users"
    date: 2004-12-27
    body: "Digital Extraction is good, but I would prefer to hear somethings from peoples using this. I am the author. Please write about your expirience with scratched disk. I saw very often an error, \"switch to single frame DMA\" after this, and no sound to hear after it. Is switch between tracks interactiv enough? Have anybody any problems with audio skips? Uses anybody this function on SUN?\n\nRegards\nAlex"
    author: "Alex Kern"
  - subject: "Re: I would like to hear somethings from users"
    date: 2004-12-27
    body: "The biggest problem is that the sound device is blocked if I don't play the cd. Thats annoying. Also sometimes I get scrambles after the first cd but I'm not sure it's a problem with the player. Maybe its the cdrom. After a reboot its gone. I have more than one cdrom s it would be nice to have a easy choice. Ok, enough criticism, I must say I like your program very much. It does simply what I want."
    author: "Marco Bubke"
  - subject: "Re: I would like to hear somethings from users"
    date: 2004-12-27
    body: "I've given an additional try to KSCD after having seen this article, and I must say that I like what I see, especially the addition of \"direct digital playback\", absence of which has made me stop using KSCD before.\nNevertheless, I think there is always a place for improvement. I'd like to see the following things added to KSCD:\n- Both graphical ( some drop-down box) and command line possibility to choose the desired CD-ROM drive, in case there are several ( I'd like to associate KSCD with audio CDs via hotplug script)\n"
    author: "Alex"
  - subject: "Re: I would like to hear somethings from users"
    date: 2004-12-29
    body: "I agree completely on the multiple CD-ROM thing.  For one, it takes some work to find what your linux vendor named your cdrom and its even more work to find which CD-ROM goes to a particular device name.  Second, I'm sure there is some way to \"search\" for cdroms, especially since k3b not only finds your cdroms, but it tells you about all of the writing capabilities, speeds, and everything!  Finally, hiding the cd-rom setup in a settings windows makes it impractical to quickly switch between different CD-ROMS.  For example, if you just copied one audio CD, you cannot quickly listen to the recording that is in your burner.\n\nI have been tempted to write this into KSCD for over a year & I think I already put it in as a wishlist like 2 years ago.  I keep thinking, \"Surely, the next release will have a drop-down of available cd-roms.\"  But every time a new release comes, you're still forced to type \"/dev/cdrom\" and \"/dev/dvd\" whenever you want to switch.  It seems someone who already has the code on their machines could hammer this out pretty quickly.  Things to look out for, however, is that \"searching for cd-roms\" has to work on all the platforms that KDE runs and probably has to have the ability to manually enter a device anyways."
    author: "Henry"
  - subject: "Re: I would like to hear somethings from users"
    date: 2005-01-02
    body: "Mandrake has KsCD in their hotplug scripts, insert a CD in any drive and KsCD gets started with that drive passed as the $DEV.  Works a treat..."
    author: "Odysseus"
  - subject: "Re: I would like to hear somethings from users"
    date: 2004-12-30
    body: "I've got a lot of trouble with audio skips when using KsCD, so I normally don't use it. I've got the \"digital\" checkbox set, otherwise there's no sound at all. alsaplayer has no trouble playing the same CDs (which are not scratched, btw.) in the same device (a Toshiba 1802 DVD-ROM, connected as slave on the second IDE channel). I /think/ I've tried the CDs in my DVD burner (which is master on that channel) and it skipped as well, but I'm not 100% sure and can't test it right now.\n\nRegards, Ren\u00e9"
    author: "Ren\u00e9 Ga\u00df"
  - subject: "CDDB"
    date: 2004-12-31
    body: "Has someone else problems with CDDB support?\n\nI mean, I get CDDB information, but I cannot submit it.\n\nOne year ago my submissions started gettig rejected when they used accented characters (\u00e0 \u00e8 \u00e9...). Ie:\n\n Invalid DB submission: garbage character on line 34\n\nThen, three months ago, ALL submissions where rejected:\n\n Invalid DB submission: unexpected end on line 1\n\nAny idea? Right now I use Mandrake 10.1 Official, KDE 3.2.3, KsCD 1.4.1"
    author: "V\u00edktor"
  - subject: "Re: CDDB"
    date: 2005-01-03
    body: "I have had the same problem on Gentoo."
    author: "Jonas"
  - subject: "Re: CDDB"
    date: 2005-01-10
    body: "I'm seeing this problem with KsCD 1.4.1 on Slackware 10.0.\n"
    author: "Trane Francks"
  - subject: "Re: CDDB"
    date: 2006-10-25
    body: "Actually I've got another problem: my KsCD doesn't want to read any CDDB entry (everything is set correctly: server and protocol). Does someone know what do you? It's really annoying......\n\nAnd one more thing (well, it's not really connected with the topic, but maybe someone will answer this) - when I'm listening to music, KsCD sometimes stops. Just stops playing and everything I can do is to start playing Cd one more time.\n\nLooking forawrd to hearing some hints from you...\nBartek"
    author: "Bartek"
  - subject: "[OT] LinuxQuestions.org awards"
    date: 2005-01-01
    body: "vote for KDE!\n\nhttp://www.linuxquestions.org/questions/forumdisplay.php?s=&forumid=62"
    author: "anon"
  - subject: "In the other news;"
    date: 2005-01-02
    body: "Coming up, moving mouse cursor under X! (for fun and profit!)\n\nSeriously, I think that this article is way too for beginners. If you already \n- have managed to get your linux/whatever system to boot (power switch!) \n- have done the cabling (power cords etc.) \n- have putten the cd to the right drive \n- know how to start konqueror and browse www.tuxmagazine.com or dot.kde.org you probably can already figure out how to play cd's thank you.\n"
    author: "Anonymous"
  - subject: "Worse than Microsoft's"
    date: 2005-01-03
    body: "Well. i have two cd-rom drives installed. I simply can't play Audio cd's with kscd. Iit seems that the tiny app is confused by the presence of  t w o  drives. And: Even MSWin98's cdplayer was able to select the cdrom device I want to play from. Moreover, the kioslave audiocd:// has different bugs in playing mixedmode cd's than kscd. Do both know each other?"
    author: "Sebastian"
  - subject: "playing audio CDs on Mandriva 2005 LE"
    date: 2005-12-15
    body: "Ouvrir KsCD /cliquer sur Extras / Configurer KsCD /cocher la case: \"utiliser la lecture num\u00e9rique directe\"(chez moi \u00e7a marche avec alsa, sinon, choisir arts) /appliquer /OK\n(pour pouvoir modifier cela, le CD doit etre stopp\u00e9)"
    author: "obs-psr"
  - subject: "Re: playing audio CDs on Mandriva 2005 LE"
    date: 2006-01-15
    body: "[english (well.. sort of) below]\nKsCD persiste \u00e0 me proposer /dev/hdd comme p\u00e9riph\u00e9rique (au lieu de /dev/cdrom1). De plus je ne sais pas du tout quoi mettre comme 'p\u00e9riph\u00e9rique audio \u00e0 s\u00e9lectionner'\n\nKsCD keeps /dev/hdd as /dev/cdrom1 (i guess). Moreover I havn't got a clue how to fill the field 'select an audio device'...\n\nThanks for your help"
    author: "Titen"
---
If you are looking for an easy to use program to listen to your favorite CDs then you should try KsCD. KsCD comes with KDE and it will most likely be installed by default. You can find it in the Kicker menu under 'Multimedia'. Life does not get much easier than this! Read <a href="http://www.tuxmagazine.com/node/1000037">the full article</a> (with screenshots) at <a href="http://www.tuxmagazine.com/">TUX Magazine</a> for more details.


<!--break-->
