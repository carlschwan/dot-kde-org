---
title: "New Howto on Developing a Plugin Structure for an Application"
date:    2004-02-25
authors:
  - "jpedersen"
slug:    new-howto-developing-plugin-structure-application
comments:
  - subject: "Kde homepage link"
    date: 2004-02-25
    body: "\nHow about gadering all of the how-tos in a single site and making a link to it from the kde homepage?\n\n"
    author: "Mario"
  - subject: "Re: Kde homepage link"
    date: 2004-02-25
    body: "Try developer.kde.org.\n"
    author: "Richard Moore"
  - subject: "Re: Kde homepage link"
    date: 2004-02-25
    body: "still there are more kde howtos on the internet then those found at developer.kde.org. So collecting (and branding) them would be a good idea. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Kde homepage link"
    date: 2004-02-25
    body: "Start collecting right here: http://kde.ground.cz/"
    author: "Anonymous"
  - subject: "Re: Kde homepage link"
    date: 2004-02-25
    body: "As Richard points out, these are available from developer.kde.org, and as soon as time permits, I'll also put this howto there."
    author: "Jesper Pedersen"
  - subject: "AT LAST!!!!!!!!!!!!!!"
    date: 2004-02-29
    body: "YES YES YES!!!!!!\n\nI've been searching the internet for weeks for a plugin tutorial!!!!!  \n\nTHANK-YOU THANK-YOU THANK-YOU!!!!!!"
    author: "Judd Baileys"
  - subject: "Re: AT LAST!!!!!!!!!!!!!!"
    date: 2004-03-02
    body: "me too, but for perl :( nothing comparable found..."
    author: "me"
  - subject: "Where did the tutorial go"
    date: 2004-03-02
    body: "I wanted to have a read, but it is now gone.\n\nSucky.\n"
    author: "anon"
  - subject: "Re: Where did the tutorial go"
    date: 2004-03-03
    body: "http://developer.kde.org/documentation/tutorials/developing-a-plugin-structure/index.html"
    author: "Henrique Pinto"
---
I needed to learn how to develop a plugin structure for my baby <a href="http://ktown.kde.org/kimdaba/">KimDaba</a>, and what better way is there to learn, than to write <a href="http://www.blackie.dk/KDE/plugin-howto/">a howto about it</a>? So I did, and with the help from the real experts (Simon Hausmann, David Faure, g++, et al), I'm pretty convinced this will be of value to anyone who either wants to add a plugin structure to an application, or who wants to write a plugin.





<!--break-->
