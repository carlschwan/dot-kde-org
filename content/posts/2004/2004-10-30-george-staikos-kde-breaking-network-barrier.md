---
title: "George Staikos: KDE Breaking the Network Barrier"
date:    2004-10-30
authors:
  - "binner"
slug:    george-staikos-kde-breaking-network-barrier
comments:
  - subject: "I'm the first"
    date: 2004-10-30
    body: "I'm the first!!! :P"
    author: "ellhnas1821"
  - subject: "Re: I'm the first"
    date: 2004-10-30
    body: "We're all posted out to /. already.\nhttp://slashdot.org/article.pl?sid=04/10/29/1544210"
    author: "ac"
  - subject: "Shell Integration?"
    date: 2004-10-30
    body: "I mentioned this on Slashdot, but I'll mention it here too, as things there have a tendency to get buried:\n\nIt would be extremely cool if someone could integrate these features into a KDE Shell. Then, lets call is KASH (K Advanced Shell), would allow me to just cd into another protocol. I could type 'cd ftp://ftp.kde.org' and I'd be ready to grab something, rather than having to fire up lftp and remember how its commands differ from those in bash. I could also copy across protocols as easy as 'cp somefile file://~/' (copying something from an ftp directory to where I am).\n\nI know that KDE is a graphical environment, but that doesn't mean it needs to stick to only that. I'd love to have access to some of these KDE features when I don't have X going."
    author: "jameth"
  - subject: "Re: Shell Integration?"
    date: 2004-10-30
    body: "Your wish is already partially possible.\nExample to copy a file via ssh:\nkfmclient copy fish://<host>[:<port>]/remote/path/to/file /local/path/to/[file|dir]\n\n:)paxcal\n"
    author: "paxcal"
  - subject: "Re: Shell Integration?"
    date: 2004-10-31
    body: "Exactly!!! This is exactly what I was thinking about a few days ago: I wish it was possible to use (all) the KIO features (imho _THE_ killer feature underneath the very beautiful KDE GUI) on the console. That could mean that the KIO slaves should no longer be bound to the GUI.\nJust an example: My absolute favorite is the audiocd:/ ioslave and since I (accidentally unfortunately) discovered it, I'm glad I don't need it know how to use cdparanoia and oggenc again. For now I just open a Konqueror window, split it in two parts, change to \"audiocd:/Ogg Vorbis\" in one part and to music directory in the other, and drag the ogg files there.\nWhat I wish was possible is to open a shell, cd to \"audiocd:/Ogg Vorbis\" and cp *.ogg ~/my/favourite/music\nIs there a wish already filed for this? How could it be implemented? With a complete new shell like Kde Advanced SHell as suggested? Or maybe just with a plugin to any existing shell like bash?\nMaybe I could even volunteer in helping making this reality. but I'm afraid I won't be able to do this alone.\n\nRegards,\n\nxtian0009\n\nPS. btw does someone know how to autoload the sg module when needed eg by the audiocd ioslave?"
    author: "xtian0009"
  - subject: "Re: Shell Integration?"
    date: 2004-10-31
    body: "You can mount kioslaves with the KIO Fuse Gateway (http://wiki.kde.org/tiki-index.php?page=KIO+Fuse+Gateway) and use them from the shell.\n\n'fuse_kio --nice-mtab /your/mountpoint fish://user@host/'\n\n"
    author: "Christian Loose"
  - subject: "Re: Shell Integration?"
    date: 2004-11-04
    body: "Assuming I understand this right, it really doesn't do the same thing. This allows me to mount most anything, which is a step forward from having to use separate tools for everything, but it still doesn't give me seamless console integration.\n\nAlthough it isn't exactly what I want, thanks for pointing it out, I'll give it a spin and, if it's as good as you imply, probably keep using it until someone adds what I suggested before.."
    author: "jameth"
  - subject: "Re: Shell Integration?"
    date: 2004-11-04
    body: "Do you know if there's something like an automounter for fuse in general or fuse_kio in particular?\n"
    author: "cm"
  - subject: "kfmclient can do"
    date: 2004-11-07
    body: "http://developer.kde.org/documentation/other/kfmclient.html\nlike paxcal suggested..\nvery nice tool.\njust set up an alias and your done.\n(can even be set to f.e. override commands only in Konsoles)"
    author: "thatguiser"
  - subject: "The problem with KDE's KIO..."
    date: 2004-10-30
    body: "KIO is perhaps one of the greatest features of KDE. The problem with it is that an article like this one is news to so many people. KIO has been out since KDE 2.0 was released. Google KIO and see how it has been promoted, discussed and documented. It looks like it should be in an \"Undocumented KDE Features\" book. Even in the 3x series after I expressly instructed people on our suggestion page not to ask for an FTP manager I still get this request. Every time fish comes up on our mailing list it's news to some long time user.\n\nThe fundamental tennet in GUI design is that you must give a visual queue to the user of what can be done. Otherwise it's an Easter egg. KIO is an Easter egg. People aren't generally going to snoop all through all the system information when they first fire it up and if they do it may not register the first time. When browsing the web you do get your http:// indication as well as ftp:// if you link to FTP. The problem is that we have no visual protocol selection indication in the konqueror or the file dialogs. So you must first know it is possible and second know what protocol you want and if it is available. Plus it's more typing than you should need to do.\n\nA solution is obvious. For one thing possibly adding protocol auto completion in an empty location bar, but this could be confusing with what else is there, and it does not visually prompt. A better option would be to add a protocol segment to the address. This would be more elegant because you could auto complete or drop down your protocol and begin typing your address after. It could still behave the same defautling to file/http as it does, but it would visually introduce people to the fact that KDE is network transparent.\n\nThis may not come up so much in desktop applications, but with web development it comes up a lot. The fact is though that virtually every KDE application would still benefit from all users being instantly aware that they could access files on other protocols. Adding context help to the protocol selector could then bring this feature to supreme value. As it is, this is only useful for those indoctrinated to our \"secret rituals\"."
    author: "Eric Laffoon"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-30
    body: "Indeed, Tom and I did such a thing not so long ago (it's in a wiki state for various reasons):\n\nhttp://www.newtolinux.ukfsn.org/wiki/index.php/KDE%20is%20so%20cool%20because...\n\nJust today I was managing some webpages through the sftp kioslave and thinking how cool it was. Just dragging files from one konq tab to another, and being able to quickly edit files in kate by simply opening them and saving them."
    author: "Robert"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "Amen to that!\nI teach computing (Web programming and databases mostly) and I've shown my students how editing code on a remote server is easy with Kate.\nQuite many of them wanted to develop pages with Linux just because of this. \n\nKio is absolutely incredible! The only thing that bothers me with KIO is the floppy usage. My students get errors when trying to umount a floppy, and honestly I don't understand why we _still need to mount floppies.\nMy students would also find it very easy to use drive letter ioslaves (a:/ for floppy, d:/ for the first cdrom)\n\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "Has anyone here mentioning opening remote files in Kate tried Quanta's remote projects? You can view your project tree on the server and if it's a local project you can view your upload profile tree on the server in 3.3. All of this is with KIO and selectable protcols.\n\nAs for mounting your floppies, you still use floppies? ;-) Build the kernel with automount and configure your /etc/fstab to automount. I don't use floppies but I don't mount CDROMs or DVDs manually.\n\nAs for using drive letters... Come on... There are ways to alias things but how lame are drive letters? A name is intuitive, but what is H:? I like to create links like so...\nln -s /mnt/cdrom $HOME/cdrom\nSo I can access it at ~/cdrom. This actually makes sense. Installing it at D:, and having it moved to E: because I installed a new hard drive is assinine. It's part of your file system. Why should you care about the physicality as opposed to an address? What drive letter should your web site be, or google.com? Why not refer to it as ABZH:? Actually they have this type of naming on the net as 255.255.255.255 but we alias it with a name. Drive letters would never be proposed for an intelligently designed network file system, just a hobbiest single user Basic platform.\n\nI'm a little disappointed that the response to my post is that KIO is cool. Yes KIO is cool, but next month when this article is buried on the web and a new user comes to KDE they will NOT find it until someone points it out to them. KIO needs the following:\n\n1) Promotion in KDE releases (besides the 2.0 release)\n2) A visual element in the GUI to make it known to new users\n3) Quick selection of protocols in Konqi and file dialogs"
    author: "Eric Laffoon"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "Come to think of it, why don't one of you guys get in contact with o'reilly and see if they'll let you write 'KDE: The Missing Manual' (a la MacOS X)."
    author: "Robert"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "> As for mounting your floppies, you still use floppies? ;-) Build the kernel\n> with automount and configure your /etc/fstab to automount. I don't use\n> floppies but I don't mount CDROMs or DVDs manually.\n\nit was clear from your post that you don't use floppies. automount does not work. it periodically tries to unmount. just having \"cd /mnt/floppy\" is enough to stop automounting from working.\n\nthat's why there's supermount-ng.\n\nsubmount is another project but with more bugs than supermount-ng even though it's supposed to be more stable (although i have never had problems with supermount-ng)."
    author: "ab"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "The most intresting thing to me is CSS editing. On windows there is a very nice tool names style studio (http://www.style-sheet.com), can or will Quanta integrate CSS editing/preview etc functionality? Or is a special tool needed for that?"
    author: "Hein"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-01
    body: "Eric,\n\nI use Quanta remote projects all the time (as well as my students who develop web with Quanta), and I like Quanta very much.\n\nAbout floppies: I personally don't use floppies, but my students who study computing basics do. They need to do Computer Driving Licence exam (http://www.tieke.fi/ajokortti_english.nsf/?open), which tests basic computing skills. In module 2 (CDL level A, module 2) they need to do file management test which starts with formatting a floppy, and making folders/files onto a floppy. By reading comments here I heard for the fist time it's possible to use floppies without mounting. I'm looking into it an configuring this in classroom once I know how to do it. By the way, quite many people still use floppies for archiving documents. I know it's stupid, but they still do.\n\nAbout drive letters: they are familiar to users (newbeginners). I teach people from age 16 -> 50? and all of them are familiar with drive letters. We all know why. It's a fact, and we need to make our DE for the users. If aliasing a:/ -> floppy:/ helps, lets do it! From ideal usage philosophy drive letters are not intuitive. They are commonly used (in 90 % of desktops) and familiar to users. I don't need them personally, I'm just telling you about something that makes learning about KDE faster.\n\nYour idea of having dropdown (combobox) for selecting protocols is a very good idea! We definetly need some visual way of browsing to KIO slaves.\n\nEleknader\n\n\n"
    author: "eleknader"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-01
    body: "About your driveletters: I really fail to see the point of emulating an inferior way of referencing devices. What's wrong with floppy? Is that less clear than 'a:/'?"
    author: "Andre Somers"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-02
    body: "The KIO floppy: is emulating the drive letters anyway, for examples:\nfloppy:/a/ for the first floppy\nfloppy:/z/ for a ZIP drive\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-01
    body: "Another problem with KDE's KIO is that even KDE developers apperently are not aware of them.\nFor instance, opening a file on a ftp-server does not open it in the associated application, but downloads it first to /tmp/kde-user, and then this temporary file is opened.\n\nThis happens because the developer is not aware that his application is capabable in using kioslaves to open the file directly, so they don't put the flag '%U' in the startcommand of their application in the corresponding .desktop file.\n\nEven the official kde-packages often lack the precense of %U in the .desktop files.\n\n"
    author: "ac"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-02
    body: "I guess you've already filed corresponding bug reports at bugs.kde.org to make the developers aware of this."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-03
    body: "Indeed, but I don't use all kde-applications, so there are probably a lot of other apps that still don't have this feature turned on.\n"
    author: "ac"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "The problem with KIO slaves and all that is not the lack of knowledge; it's also a lack of explanation in the documentation as to WHAT they are and WHAT they are supposed to do. Personally, I haven;t a clue, and I am afraid of them, because I have no idea what they can do, and whether they can be overtaken by someone remotely to get control of my machine.\n\nNow, before you say, hogwash, let me tell you folks, it happened. Linux was used to grab control of my Windows machine not so very long ago, and the persons who did it used I/O ports, KIO slaves, the KDE Wallet, and a host of other Linux programs. At the time, I had no idea what was going on, but they corrupted my bios with this stuff. All right. So I am paranoid. \n\nWhich is one reason I (like those students) would sure like disc drive locations and or letters just to find stuff when I need it. As it is, the system is a nightmare to negotiate, because drives are hidden most of the time, and I have no idea how to get my floppy when I need it. In fact, my system has two, and it doesn't seem to recognize that fact. I can't get these drives to register, much less to use them.\n\nIn short, Linus is still full of bugs despite all the crowing about the greatness of Mandrake.\n\nCall me if you can help--or think it might be useful to know more.\n"
    author: "Freda"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "I've already added your post to my \"favorite rants\" file. Let's take a five second look at the various things you are rambling about.\n\nFirst, you are afraid of KIO slaves and don't know what they are. Did it ever occur to you to click on the article that is the focus of this entire discussion?\n\nSecond, you are babbling KIO slaves were used, along with \"I/O ports, KIO slaves, the KDE Wallet\" to take control of your spyware-ridden Windows machine. Of course, Windows doesn't use I/O ports, so that can't be a problem. I can easily see how \"KIO slaves\", local protocol handlers on someone else's machine, could somehow go out into the INTARWEB and \"take control\" of your machine, and Dear God! they even attacked you with their encrypted password management tool using KDE Wallet! How could your poor machine have resisted their locally stored encrypted password archive?! Do you even know what the words you are using mean? Please, please hit up Google for some definitions before you spout this crap in public. No, I'm not going to explain them to you.\n\nDisk letters, great stuff. I always think of Drive U: when I want attached network storage, or is that Drive T:? So much easier than /mnt/fileserver-backup. If you use a recent desktop distro like SuSE or whatever, you may notice that attached storage (USB drives, CDROM's, and whatnot) show up in a very friendly \"My Computer\"-esque box, where you click on the pretty pictures to get at your files. Very easy. I'm sure even \"students\" can manage it, mine certainly do.\n\nI suspect that Linus is indeed full of bugs, but I sincerely hope for his sake that they are merely his normal microbial flora, and not nasty ones with legs. If you were trying to talk about the operating system kernel Linux, it does indeed have bugs, as do all large software projects, but your complaints have nothing to do with the kernel. It sounds like you are just unwilling to learn how to use a different operating system, and I don't see why your angst should be limited to this OS. "
    author: "Glenn M"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "Try floppy:/ or floppy:/a/\nShould also work for zip drives and stuff (floppy:/z/ I think)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "I used to put \"My Computer\" (devices:/ ) and \"Network Neighberhood\" (lan:/) icon on the desktop for my fellow windoz friend.\n\nall my friend where able to use both without linux knowledge or the need for help."
    author: "Mark Hannessen"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "Yes, I've done this too.\n\nToo bad these are not default on KDE desktop! Perhaps I should check if there's a wish and if not, make one. The fact that they are not by default there makes it a difficult for me to teach students to use ~/Desktop/My Computer: It will not be there on any other systems that these at school. a:/ would be the standard they know on Win and would find familiar.\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-01
    body: "I don't think lan:/ is a good default imho. Afaik lan:/ works using lisa daemon, which is a light version of nmap ;)"
    author: "lanmap"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "Right.\n\nThe correct word is _should_.\n\nMy students get 'unable to umount' errors when they've used floppy:/ ioslave. Some programs seem to leave the floppy device into state that it can't be umounted. Sometimes they need to logout to get around this. Not very convinient. I've not yet managed to reproduce this to find out what it is that causes the problem.\n\nEleknader\n\n\n"
    author: "Eleknader"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-01
    body: "fuser is there to help you:)\n\nfuser -v /mnt/floppy\n\nthere you go..\n"
    author: "Miro"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-11-02
    body: "Miro,\n\nI'm afraid you did not understand the problem. \n\nI can use floppies without any probmlems, my newbeginner students have difficulties. They do not understand the mounting and unmounting procedure, and I honestly think they should not need to understand.\n\nSomeone mentioned programs that help using floppies and cdroms without mounting (supermount, submount). The best solution I've found so far is submount. The bad news is you need to compile the linux kernel module and a daemon. After compiling and installing submount the floppies and cdroms can be used just as easily you can use them on Windows.\n\nI still have problems with Konqueror, it locks cdrom so that I have to wait few minutes before I can eject cdrom. Beside this small problem submount is very good package. I hope it will get into main Linux kernel.\n\nEleknader\n"
    author: "eleknader"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "You demonstrate the lack of knowledge about kio. Just use floppy:// to access your disks! It is a kioslave that uses mtools and thus does not mount ort unmount. q.e.d.\n\nAlso floppy uses drive names like floppy:/a/\n\nIt will not help with your cdrom access problems though."
    author: "KIO - floppy:  No need to mount!"
  - subject: "Re: The problem with KDE's KIO..."
    date: 2004-10-31
    body: "You're right; good HCI means that KIO must be visible to the user. They shouldn't have to read a manual to find it. \n\nIt would be nice if each KIO slave could have it's own wizard. For example, the \"fish\" KIO slave (terrible name though, it should be called ssh) would pop-up with a window explaining what it does (very important) and give you text input boxes for your username, host and password. It would then connect to a ssh server once you enter your data and click \"OK\".\n\nThese wizards could then be accessed through a menu item in konqueror, the 'start' menu or perhaps some konqueror view (which would ironically be a KIO slave) which shows a list of all the wizards available. For example, it wouldn't just say \"fish\", it would say something like \"Connect to SSH server...\", \"Listen to or encode music from a CD...\", \"Read man pages...\" etc. This would be visible to new users, introduce them to all the KIO slaves and explain to them what they do and how to set them up.\n\nThe way they are hidden from the user at the moment is such a waste of a killer feature."
    author: "Bob Page"
  - subject: "I'm second that"
    date: 2004-10-31
    body: "I've always tought the network integration of KDE was very powerfull and clean for the user.\nIt's just a shame I don't know most of the network kio that exists ;)"
    author: "Iuri Fiedoruk"
  - subject: "Too bad..."
    date: 2004-10-31
    body: "I still get \"Examining File Progress...\" all over when you open or move files around using fish/ftp, etc.  Why can't KIO stuff be transparently shared between all applications instead of \"Initializing Protocol...\" all over?  When I hit Cancel on subsequent dialog boxes, it still opens the file, but that's annoying.  Should I open a bug?"
    author: "Jeff Pitman"
  - subject: "KIO isn't perfect"
    date: 2004-10-31
    body: "First I must admit that I often enjoy using KIO, copying files in Konqueror with fish:// is really cool.\n\nBut: Have you ever tried to view a movie from a Samba network-share? KIO tries to get the whole file and creates a temporary file on the local file-system. This really sucks! Waiting for 700 MB just to view the first 5 minutes isn't efficient. Viewing the same file from a smbmounted directory starts instantly instead!\n\nSo KDE should add better mounting features to compensate the flaws in the KIO-architecture.\n"
    author: "Henning"
  - subject: "Re: KIO isn't perfect"
    date: 2004-11-01
    body: "I would instead like to see that the problem (\"flaws in the KIO architecture\" as you call it) is fixed, not the symptoms worked around. That seems to be the saner route to follow, if possible of course."
    author: "Andre Somers"
  - subject: "Re: KIO isn't perfect"
    date: 2004-11-01
    body: "That is probably because the media player youre using either can't handle kios-slaves, or the startcommand for the player in the corresponding .desktop file does not contain a %U in its startcommand.\n%U tells kio/kde/whatever that the application can open URL's.\n"
    author: "ac"
  - subject: "Re: KIO isn't perfect"
    date: 2004-11-01
    body: "I noticed this behavior with noatun and kaffeine.\nThe problem is, that KIO does not provide all the features of the source file-system. KIO has no seek() method, it only can get smaller parts of the file with data().\nBut with better mounting capabilities (in Konqueror and in the file dialog) every application including xine, mplayer or xmms could handle a file from a network share without problems. \nProtocols that cannot be mounted currently sould be re-implemented in something like fuse"
    author: "Henning"
  - subject: "Re: KIO isn't perfect"
    date: 2004-11-01
    body: "try kplayer.. can open films from KIO slaves 2 versions ago:)\nmuch much better than anything else I tried."
    author: "Miro"
  - subject: "Re: KIO isn't perfect"
    date: 2004-11-01
    body: "Oh thanks I will look at it. But I don't think this a real solution for the KIO-problem ;)"
    author: "Henning"
  - subject: "All nice and dandy..."
    date: 2004-11-05
    body: "but why did I have to read this to find out how to access audio cds in Konqeuro?  Windows XP allows this without needing to read an article to find some obscure code to type in the address bar---it's right in the explorer tree.  audiocd:/, btw, doesn't make sense at all.  Why not popup a dialog asking \"which file format, ogg, mp3, wav?\" instead of fragmenting the source of one device into 7 different parts?\n\nI wish KDE would dump the sidebar interface Konqeuror has (like XANDROS' File Manager BASED on Konqeuror is) and put all these in a graphical tree like XP/Xandros XFM.  Konqueror is powerful but it just doesn't make sense sometimes. \n\n(KHTML rocks though, as speedy as Opera)\n\nScreenshot of XFM: http://www.pdpcomputing.ltd.uk/images/xandros/XFM1.jpg\n\nPlease KDE, you have the best DE in the world out, just polish it up >:|"
    author: "Tyyuan"
  - subject: "Re: All nice and dandy..."
    date: 2004-11-05
    body: "XFM is not based on Konqueror. Also you can help polishing it up, no need to make a grim face."
    author: "Datschge"
---
In his bi-weekly column "<a href="http://osdir.com/News+index-catid-206.phtml">KDE: From the Source</a>" at O'Reilly's <a href="http://osdir.com/">OSDir.com</a> George Staikos talks in <a href="http://osdir.com/Article2159.phtml">the current issue</a> about KDE's fully networked desktop protocol handlers as well his favorite local ones. To quote George: "<i>Microsoft Windows and Mac OS X have a long way to go to catch up with the robust, transparent functionality that KDE has provided since version 2.0.</i>"


<!--break-->
