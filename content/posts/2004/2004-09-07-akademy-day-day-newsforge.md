---
title: "aKademy Day by Day on NewsForge"
date:    2004-09-07
authors:
  - "tchance"
slug:    akademy-day-day-newsforge
comments:
  - subject: "Excellent coverage Tom!"
    date: 2004-09-07
    body: "The aKademy guys really cared for all of us who did not manage to go, I must say. The coverage of aKademy, (including your articles, the wiki pages, the presentations offered in text and ogg formats, and the other dot articles) was simply outstanding.\n\nThanks to all who helped in this effort (Fab, Tom, Phil, Fluendo, Kurt, Binner and a lot of people I did not mention). I really appreciated.\n\nCheers!"
    author: "Carlos Leonhard Woelz"
  - subject: "Re: Excellent coverage Tom!"
    date: 2004-09-07
    body: "I second this. I particularly enjoyed the videos, giving faces and voices to the various contributors.\n\nThanks.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Excellent coverage Tom!"
    date: 2004-09-08
    body: "\nYes, who can not agree with you two. The coverage is much appreciated.\n\nBTW, Tom, you write great.\n\n\nCheers,\n\n       Frans\n\n"
    author: "Frans"
  - subject: "Tech Journalism"
    date: 2004-09-08
    body: "This is how Tech Journalism should be. I want to complement you.\n\nThank your for worthwhile articles that were fun and teaching to read. Please keep up the good work.\n\nYours, Kay"
    author: "Debian User"
---
Today <a href="http://www.newsforge.com/">NewsForge</a> published the last of my daily articles from <a href="http://conference2004.kde.org/">aKademy</a>. To read more about what happened in Ludwigsburg each day, dig into the articles from <a href="http://www.newsforge.com/article.pl?sid=04/08/24/120256">Saturday 21st</a> and <a href="http://www.newsforge.com/article.pl?sid=04/08/24/216258">Sunday 22nd</a> (the <a href="http://conference2004.kde.org/sched-devconf.php">developers' conference</a>), <a href="http://software.newsforge.com/software/04/08/26/1314233.shtml">Monday 23rd and Tuesday 24th</a>, <a href="http://software.newsforge.com/software/04/08/27/1253249.shtml">Wednesday 25th</a>, <a href="http://software.newsforge.com/software/04/08/30/2028209.shtml">Thursday 26th</a> and <a href="http://business.newsforge.com/business/04/09/02/160256.shtml">Friday 27th</a> (the <a href="http://conference2004.kde.org/sched-marathon.php">coding marathon</a>), and finally <a href="http://business.newsforge.com/business/04/09/02/1653253.shtml">Saturday 28th</a> and <a href="http://trends.newsforge.com/trends/04/09/07/1351233.shtml">Sunday 29th</a> (the <a href="http://conference2004.kde.org/sched-userconf.php">users' and administrators' conference</a>). Many thanks again to Giovanni Venturi, Stephan Binner, Sven Guckes, Michael Prokop, Peter Rockai and to anyone else <a href="http://wiki.kde.org/tiki-index.php?page=Pictures+%40+aKademy">whose photos</a> I used in the series!




<!--break-->
