---
title: "KDE-CVS-Digest for August 6, 2004"
date:    2004-08-07
authors:
  - "dkite"
slug:    kde-cvs-digest-august-6-2004
comments:
  - subject: "New artwork"
    date: 2004-08-07
    body: "I really like the new artwork. Especially the new KDE splash screen, the KDevelop splash and the KPilot one are very nice. Good job Arciszewski and others :)"
    author: "Niek"
  - subject: "Krita"
    date: 2004-08-07
    body: "Perhaps most people don't take notice (yet) but\nthe good progress of Krita is IMHO one of the hottest things\ncoming to the KDE desktop and/or Linux.\nThough GIMP has enormous functionality and has existed\nfor years let's face it: Most people never\nliked its cluttered UI. Stone-age file dialogs and missing \nCMYK functionality did the rest to render Linux completely \nuseless for all people who had to do even the simplest \n(CMYK or other) graphics editing.\nMost time was spent on defending the status quo ignoring the\nfact that many users just did not want it - period.\nAnd the way GNOME goes nowadays I wouldn't wonder if they will\nnow be removing functions until they've got a GPaint because\nthat's of course a lot more user-friendly, isn't it?\nWell, big thanks to everyone working on Krita for finally turning\non the light switch at the end of the tunnel!"
    author: "Martin"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "It should get even better when Qt4 comes along."
    author: "David"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "I'm excited about Qt 4, don't get me wrong... But I'm already dreading the porting effort... It's the same with OASIS file formats. KOffice is moving towards OASIS at a fair clip, but there isn't a standard for complex raster image file formats. This is the chance to set the standard for once, and I'm well aware of it. But adding functionality, even functionality as basic as selections, is taking all the time the people working on Krita can spend... And I wouldn't be surprised if there weren't as many people working on Krita nowadays as on the rest of KOffice together. \n\nNew libs, new versions of libs have their advantages, but they seldom lead to an increase of functionality. For a large part it leads to nothing but churn. Microsoft has elevated the API churn to an art, to the point where it's impossible for a third party developer to keep up with the way a Windows app ought to look. With Microsoft it's intentional, with Qt and KDE it isn't, but it's often just as hard. Only last month we removed one of the last deprecations dating from KDE 2.x...\n\nSo, in the end... Krita will get better, but possibly despite Qt 4, not because of it."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "Why Krita depends on Koffice? Ok, it's definition what belongs to an Office Suite but O think Krite should developed indpendent from Koffice without any dependency on Koffice.\n\nWhy? \n1. Because it's more a Design Application than an Office Application. Who is working with such an App in an Office? \n2. You don't have to compile the whole Koffice to get Krita run. At the Moment Krita doesn't run with Koffice 1.3.2. Krita does compile very well but If I start it, nothing happens because the office lib's are too old.\n\nRather make a Design Suite together with Karbon and Scribus but with independent libs. That's my 2 cents"
    author: "Frank"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "This idea has come up before, mostly with the argument 'Krita is too complex. Complex apps don't belong in an entry-level office suite like KOffice. Krita should go out of KOffice.'. Personally, I don't care. The KOffice conveniences are nice to have, and if I am not personally fond of apps that ask me whether I want to open a recent file, any file, or create a new file on startup, I'm not sufficiently irked by that particular part of the KOffice conventions to wad nt to take Krita out.\n\nTaking Krita out of KOffice and moving it somewhere else would take a lot of real work, and a lot of politicking. I'm inclined to save my energy for making Krita better within KOffice.\n\nAs for the libs dependencies: since Krita isn't released at all, I'm not particularly interested in being compatible with anything but KDE cvs. By the time Krita 1.0 is really released, we're probably looking at a Qt 4 dependency, with gcc 3.4 necessary to compile it."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-08-09
    body: "'Krita is too complex. Complex apps don't belong in an entry-level office suite like KOffice. Krita should go out of KOffice.'\n\nI wonder who said that; it certainly wasn't me.\n\nWhat I said was that Krita didn't belong in an Office suite.  Neither does Karbon14.  What an office suite needs is a basic graphics application that has sufficient features to produce graphics for DTP and presentations. A KOffice version of KolourPaint would probably be sufficient if it supported layers and editing of object properties (like XFig).\n\nHowever, that does not mean that Krita and Karbon14 shouldn't depend on the Koffice Libraries.  The question is whether these libraries provide needed functionality and whether such applications should be able to function as a Koffice Part.\n\nPerhaps what is needed is to include the Koffice Libraries in KDE.  And, it appears to me that ALL Koffice applications should be able to stand alone (if they have the libraries available -- this is probably already the case).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-09
    body: "If you want to produce graphics for DTP then you need a vector drawing application (like XFig or Karbon14), but you definitely don't want a pixel drawing application like KolourPaint. For presentations a pixel image might be sufficient, but for anything that's printed on paper you want vector images."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "Actually, there are tradeoffs which become less and less important as the printer resolution increases.\n\nWhat is clear is that it is easier to draw with an interface like XFig than with a typical paint program.\n\nThe best solution will be a program that is able to handle both bitmap and vector drawing.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "No troll, will Krita ever be installable on windows like the Gimp currently is on windows?"
    author: "ac"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "I rather doubt it. I certainly won't be spending money to buy Qt for Windows to produce binaries for an OS that I haven't used for anything but taxation software in a decade. I haven't followed the KDE port to Windows using Cygwin's X11 libs, but I guess that won't be acceptable... I tend to keep a wary eye for the introduction of X11 dependencies that may make it harder to port Krita to OS X -- but even there, I'm too glad with Adrian's great work on tablet support to care all that much, and I think Krita has never been in Benjamin's great KOffice port to OS X."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-08-08
    body: "But I guess you should be able to run/port it under cygwin without to much trouble..."
    author: "Mark Hannessen"
  - subject: "Re: Krita"
    date: 2004-08-08
    body: "Oh yes, that should be pretty easy. Not that I'm going to spend time on it, mind, but if someone wants to try, I'd welcome their patches."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-08-07
    body: "We're aiming for an alpha-quality release in September, but it'll probably be later than that. At that moment, Krita should sport layers, filters, selections, cmyk, rgb and grayscale and really good tablet support. It'll be our very first public release, after five or six years of discontinuous development..."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-08-08
    body: "I didn't even know such a thing as Krita existed a few minutes ago, but I've had a look at the website and read this thread and it seems very promising. I can't wait to try it out.\n\nKeep up the good work!"
    author: "Mikhail Capone"
  - subject: "Re: Krita"
    date: 2004-08-09
    body: "> ... and missing  CMYK functionality ... .\n\nPerhaps someone could make clear exactly what they mean by this and when it is necessary.\n\nYour screen is additive so The GIMP uses an additive color model.\n\nOTOH, if you want to simulate watercolor painting, then you need a subtractive color model (perhaps this is what people mean by CMYK).\n\nWhat The GIMP actually lacks is full support for transparency.  If you use transparency, it does support subtractive color.\n\nHowever since exactly the same colors are represented by RGB and CMYK, there is still some question of exactly why CMYK is needed.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-09
    body: "> However since exactly the same colors are represented by RGB and CMYK, there is still some question of exactly why CMYK is needed.\n\nafaik, the CMYK color model represents far less colors than RGB. If the application supports CMYK, you get an on-screen idea, what a printout may look like. If it doesn't you may be disappointed by the printed results of your work, because you were working with RGB where it looked good and never cared about the reduced CMYK color space."
    author: "Thomas"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "You, as have the others, are confusing screen to print color matching with CMYK.  CMYK is not color matching, you will get exactly the same screen colors with RGB, HSV, and CMYK.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "Of course, representing the color information in CMYK is only the first step into the direction of professional picture editing. What comes next is to be able to apply color-profiles while you work to get a preview for color matching. If you're lucky to have a calibrated sreen in front of you, you get a good impression what the print will look like.\n\nYou may than save your work as a CMYK TIFF file with the color range fitting to the targeted output device.\n\nDunno, but I think there's already an OSS implementation for color-matching available:\nhttp://www.littlecms.com/about.htm"
    author: "Thomas"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "Sorry but I don't understand why using CMYK is needed for color matching.\n\nAFAIK:\n\n1.\tYour screen uses the RGB model to display.  If you have a CMYK file, it must first be converted to RGB to display it.\n\n2.\tThe printer drivers for color printers do not accept CMYK files as input.  So, the same thing -- you need an RGB file to print.\n\nAs I said, the color gamut of the screen is larger than the color gamut of the printed output.  Therefore, it is the screen that needs to be matched to the printed output -- you can't match the printed output to the screen.  So, I'm not exactly clear on how you would save the image \"with the color range fitting to the targeted output device\".  Since you don't print to the actual output device, but rather to the software (commonly called a) driver, this wouldn't really be relevant in any case.\n\nSo, AFAIK, you can match a screen image to the printed output just as well using RGB since the screen image is always RGB.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "PostScript (from level 2 on) can handle CYMK natively. Some high-end PostScript printing machines do not even accept PostScript RGB raster images.\n\nAnd a professional printer cares about the printed product. So he wants the printed product as best as possible on his screen (and not the other way around).\n\nSo clearly the colour system that matters is CYMK. But as there is no CYMK monitor (and is not likely to exist in near future) CYMK needs to be approximated in RGB (but based on CYMK data).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "> PostScript (from level 2 on) can handle CYMK natively. Some high-end \n> PostScript printing machines do not even accept PostScript RGB raster images.\n\nThis is very interesting.  It raises more questions.\n\nWhat does the printer, or the \"driver\" do with the CMYK image?  That is, does it print it directly or try to add the color corrections for RGB to the CMYK data?  If the printer or the \"driver\" doesn't make these color failure (ink is not perfect) corrections, then you would need an application that was able to do it.  I think that we are talking about really high end professional stuff here and I was really talking about matching your screen to your desktop ink jet printer.\n\n> And a professional printer cares about the printed product. So he wants the \n> printed product as best as possible on his screen (and not the other way \n> around).\n\nYes, quite correct.  You want the screen image to look like the printed image.  However, CMYK is not in any way needed to do this.  What is needed is to match the screen color gamut to the printed color gamut.  The best way to do this is to load a pallate representing the printed image parameters.  You must modify the screen image because the printed image color gamut is smaller than the screen image's color gamut.\n\n> So clearly the colour system that matters is CYMK. But as there is no CYMK  \n> monitor (and is not likely to exist in near future) CYMK needs to be \n> approximated in RGB (but based on CYMK data).\n\nPerhaps this makes more sense if you say that the monitor uses additive color and the printer uses subtractive color.  I have read of experiments to make a subtractive flat screen, but it is currently not a practical product.  There is no approximation when CMYK is converted to RGB -- there is a 1:1 algorithm for the theoretical color spaces.  What is approximated is a screen image that is based on the color failure and gamut limitation of the printer ink.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-12
    body: "I am not sure to understand \"color correction for RGB to the CMYK data\".\n\nA colour printer is printing with inks like Cyan, Magenta, Yellow and blacK, so there is no RGB here. (Good, the inks are not perfect, but as far as I know it is easier to tell that for example an ink is 98% cyan, 1.5% magenta and 0.5% yellow and to correct the data from that point of view than doing a similar work in RGB.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Krita"
    date: 2004-08-12
    body: "> ... but as far as I know it is easier to tell that for example an ink is 98% \n> cyan, 1.5% magenta and 0.5% yellow ...\n\nActually not, the inks are subtractive so:\n\n\tCyan is MINUS Red\n\tMagenta is MINUS Green\n\tYellow is MINUS Blue\n\nThe color failure in the ink is that in addition to the color which is it supposed to subtract that it also subtracts (absorbs) some of the other colors.  Therefore, color failure is described in terms of RGB.  So the ink will absorb some percentage of each color and they don't add up to 100% since the three colors are independent.  That is, a perfect Magenta (minus Green) ink would absorb:\n\nRed\t0%\nGreen\t100%\nBlue\t0%\n\nBut ink is not perfect so the Magenta ink does not absorb 100% of Green (this reduces the size of the printed image's color gamut and is why the Black ink is needed) and it absorbs a small amount of Red and Blue as well (which reduces the tonal range of the printed image)  - when you mix Cyan and Blue inks, you don't get as pure of a Green as you do on the monitor.\n\nThe result is that there would be a 3 x 3 matrix to convert the theoretical RGB values to the RGB values of the color that will actually be printed.  When this matrix transform is applied to the RGB values in the file and the result is displayed on the screen, it should (closely) match the printed image -- all without using CMYK -- there is no need for CMYK because a given RGB value will always print the same on the printer.  The values in the 3 x 3 matrix will vary from printer to printer (this matrix is the [linear part of the] color profile for the printer).  The use of only the 3 x 3 matrix assumes that the driver software for the printer will correct for any non-linearity in the printing process.\n\nYes, our applications need a color print preview using a color profile for the printer, but CMYK is neither necessary nor sufficient to do this.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "Your screen uses pixels for display. If you have a vector graphics file, it must first be converted to pixels to display it."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-08-09
    body: "gg:cmyk\n\n3rd hit: http://www.webopedia.com/TERM/C/CMYK.html\n\"Short for Cyan-Magenta-Yellow-Black, and pronounced as separate letters. CMYK is a color model in which all colors are described as a mixture of these four process colors. CMYK is the standard color model used in offset printing for full-color documents. Because such printing uses inks of these four basic colors, it is often called four-color printing. \n\nIn contrast, display devices generally use a different color model called RGB, which stands for Red-Green-Blue. One of the most difficult aspects of desktop publishing in color is color matching -- properly converting the RGB colors into CMYK colors so that what gets printed looks the same as what appears on the monitor.\"\n\n5th hit: http://dx.sheridan.com/advisor/cmyk_color.html\n\"[...] Also, because printing processes such as offset lithography use CMYK (cyan, magenta, yellow, black) inks, digital art must be created as CMYK color or must be converted from RGB color to enable use.\"\n\nIn other words, support of the CMYK color model is essential for an application which aims for professional users."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Krita"
    date: 2004-08-10
    body: "> One of the most difficult aspects of desktop publishing in color is color \n> matching -- properly converting the RGB colors into CMYK colors so that what \n> gets printed looks the same as what appears on the monitor.\n\nThis appears to be the problem.  This statement would be true if it didn't include any reference to RGB or CMYK.  Just substitute 'screen' for 'RGB' and 'printed' for 'CMYK' and you will have correctly stated the problem:\n\n'One of the most difficult aspects of desktop publishing in color is color matching -- properly converting the screen colors into printed colors so that what gets printed looks the same as what appears on the monitor.'\n\nThe reason that this is a problem is that the color gamut for printed colors is smaller than the color gamut for screen colors (displayed on your monitor).  It has nothing to do with RGB or CMYK\n\n> In other words, support of the CMYK color model is essential for an  \n> application which aims for professional users.\n\nAnd the question remains: Why?\n\nYes, color (screen to print) matching is essential for an application which aims for professional users.  But, CMYK will not produce color matching since it is just another model to represent exactly the same screen colors.  So, why is it essential except for those that can only think in CMYK?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "statistics overview in diagrams"
    date: 2004-08-07
    body: "Regarding these statistics found in the digest:\nCommits:\n3633 by 226 developers, 608151 lines modified, 1673 new files\nOpen bugs:\n7020\nOpen wishes:\n6734\nBugs opened:\n320 in the last 7 days\nBugs closed:\n351 in the last 7 day\n\nWould it be possible the digest keeps a log of these per digest release? This way it could add a history overview of these stats in an overview diagram (on it's site, not in the digest itself). This way you could see in a timeline how development of KDE grows (or shrinks) over time. Or something like this. I dunno, great digest anyway!"
    author: "ac"
  - subject: "Re: statistics overview in diagrams"
    date: 2004-08-07
    body: "This is on my todo list. It would be interesting to see a graph of the activity over time. The commit statistics can be generated any time, but the bug stats are taken when the script is run.\n\nhttp://cvs-digest.org/index.php?stat&period=aug62004\ngives complete statistics for the week. Click on the Statistics header in the digest.\n\nDerek"
    author: "Derek Kite"
  - subject: "KTracer looks nice "
    date: 2004-08-07
    body: "Enrico Ros did some amazing stuff again. I haven't tested ktracer but judinging from the screenshots it looks very nice :-)\n\nhttp://robotics.dei.unipd.it/~koral/KDE/ktracer/"
    author: "MK"
  - subject: "Re: KTracer looks nice "
    date: 2004-08-08
    body: "indeed sexy screenshots, jeez.\n\ni hope i'll be able to shoot at classes in the future! This will be a reason for me to start 'gaming' again :)"
    author: "cies"
  - subject: "KDE graphic apps"
    date: 2004-08-08
    body: "The Krita website and in particular http://www.koffice.org/krita/faq.php clearly mentions that Krita is to be considered a _painting_ application and not an image editing application. Does that make all comparisons to the Gimp useless?\n\nCurrently we have:\nGimp - gtk+\nInkscape - gtk+\n\nTwo already good graphics apps. How likely is it we will ever see true KDE native apps which can replace either? Is this a case of basically hoping someone will begin with such a project?"
    author: "ac"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "Where do you read that \"not an image editing application\" part? \nFrom the page you have given:\n\nWhat is Krita?\n\nKrita, [...] is the painting program and image editor for the KOffice Suite. It is intended to be suitable for all your image creation and editing needs, such as creating original art, making images for websites, touching up scanned photographs or anything else you might need to do with bitmapped images."
    author: "cm"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "... before you ask, sure I've read the following:\n\"If you are looking for a tool primarily to apply effects to existing images or photos [...] other software may be more suitable.\"\n\nI read that as \"the focus is not on image editing\".  Doesn't mean you cannot use it for that. "
    author: "cm"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "Why do you not just try out krita? It is really easy, just do the following:\n\nexport CVSROOT=:pserver:anonymous@anoncvs.kde.org:/home/kde\ncvs login\ncvs co koffice\n\nYou need the kdelibs development files to compile krita. On Debian do the following:\n\napt-get install kdelibs4\n\nThe do the usual ./configure --prefix=/usr/local (on Debian), make, make install.\n\nKrita already works quite nice. There is a brush tool, a pen tool, a line, a rectangle and an ellipse tool, an airbrush, you can work with layers as in Gimp, you can work with a zoomed image, you can alpha-blend two layers and their are some filters. You can even use a tablet to paint with Krita (I did not try that because I do not have one) and there is a plugin to scan images with your scanner. Some things make krita crash, but many things already work quite well and it is really impressive how much Krita improved in the last couple of months. And Krita already has some features that GIMP does not have like CMYKA support.\n\nIn my opinion, Krita is one of the most exciting KDE projects, right now"
    author: "Michael Thaler"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "Wacom Graphire pads are quite cheap nowadays and the difference in ease of use compared to a mouse is astonishing. I really found it worth the money -- of course, getting a graphire and not having a good app to use it with (at that time, the Gimp's tablet support was horrible) was the impetus for me to get involved with Krita. Nowdays, thanks to Adrian Page's work, Krita is a really good choice if you want to daub a bit with pen & tablet.\n\nOn the subject of the CMYK plugin: it exists, but it's really buggy at the moment. The design is sound, but the implementation needs to be finished before release. Preferably by someone who needs cmyk, and therefore knows how it's supposed to work. Me, I don't even have a colour printer, so I cannot use CMYK for anything useful."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-09
    body: "Wacom tablets are great!  I have a feeling mine's too big though.. it's 9x12.. \n\nTwo big issues with it:\n\n1) even under Gimp, there are too many jagged jitters that crop up if you're trying to draw a smooth curve.  So I like how Sodipodi lets you draw a curve in vector form first.  Perhaps Krayon can incorporate this feature?\n\n2) Sigh... I just wish that my wacom tablet /worked/ with my logitech mx700 under kernel 2.6.  This is ridiculous! no one ever posts on the Wacom mailing list anymore. Boooo!\n\n\n- Roey\n\n\n\nremote the turtles in my address to reply."
    author: "Roey Katz"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-09
    body: "First, it's Krita nowadays, because a German lawyer decided to 'protect' the trade name 'crayon'... Anyway, Krita's current code uses bezier curves to smooth the trajectory of a hand-draw curve, and I can draw as neat a curve with Krita as with plain pencil and paper, maybe even better. It's not the same trick as with Sodipodi, because that's a real vector app with all the scaffolding that needs, but it's pretty good."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-09
    body: "> Wacom Graphire pads are quite cheap nowadays ... .\n\nI suppose that since you use a graphic pad that you won't be adding features to allow drawing without one (like XFig).\n\nHINT: you don't need a graphics pad to use a CAD application.\n\n> I don't even have a colour printer, so I cannot use CMYK for anything useful.\n\nThis appears to be a serious misconception.  Your printer uses CMYK ink, but you print an RGB image file to it (or actually to the printing software).\n\nI presume that when people say CMYK that they mean subtractive color. \n\nTransparency is subtractive color, so I'm not sure exactly what is meant here.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-09
    body: "> This appears to be a serious misconception. Your printer uses CMYK ink, but you \n> print an RGB image file to it (or actually to the printing software).\n\nYes, you send an RGB image to the printer, but the printer *prints* that image using CMYK. And if your graphics software was not presenting the image to you on the screen taking that into account, what your image looks like printed can be significantly different than what is shown on the screen.\n\nTHe whole point of CMYK support in graphics applications is to \"What you see is what you print\", or at least, as close as possible.\n\nAll professional graphics artists that are potentially targetting printed media (read: nearly all of them) need CMYK support in their applications."
    author: "Jason Keirstead"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-10
    body: "As I thought, this is not understood.  There is a 1:1 mathematical correspondence between the CMYK and RGB color models.\n\nThe difference between the screen and the printed page is not between RGB and CMYK.  The screen must be additive and printing must be subtractive.  It is the difference between the fact that the screen uses phosphors and the printer uses ink.  As a result, your screen has a larger color gamut.\n\n> The whole point of CMYK support in graphics applications is to \"What you see > is what you print\", or at least, as close as possible.\n\nActually, this would be color matching -- something which is clearly needed.\n\nIt probably comes as a great surprise to you that you can select the exact same screen colors using the RGB, HSV, and CMYK color models.  After all, they are all displayed on your screen using RGB -- aren't they??\n\nhttp://www.cs.rit.edu/~ncs/color/a_chroma.html\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-10
    body: "No, it does not come as a surprise. You still seem to be missing the point. The point is that CMYK cannot physically represent as many colors as RGB, and those that it can are represented in a subtly different way to the eye since it is a subtractive model, and it is based on light reflection rather than emission. So you need to do to the following for professional publishing\n\n1. Take the printed RGB document and transform it to CMYK as closely as possible. This is known as \"color matching\", and is normally done in the printer driver, since it is the only portion of the system that can take in a RGB document and output it to the printer as CMYK - all other parts of the system output to the printer as RGB, as you said earlier.\n\nMost all windows printer drivers have good color matching algorithms. I do not know the state of these in Linux.\n\n2. BEFORE any of that takes place, a *good* app will alreay know that the printer is limited to CMYK and thus take measures to ensure that the picture you see on the screen is how it will be printed before it even reaches the driver, so that there are no surprises. This normally requires applying transformations to the image as it is rendered on screen to give you a more accurate representation of the to-be-printed output, or working with a different color palette altogether that represents the CMYK palette."
    author: "Jason Keirstead"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-10
    body: "> The point is that CMYK cannot physically represent as many colors as RGB\n\nYes, that is certainly the point and you are wrong about that.\n\nWhat is correct is that (as I have said in other posting) the printer using ink on paper can not represent as wide a range of color as your screen can.  This is not because the screen is RGB and the printer is CMYK, it is because the ink on the paper is not as good at representing color as the phosphors on your monitor screen.  This has nothing to do with the fact that the screen is additive (RGB) and the printed image is subtractive (CMY[K]).  If you are using subtractive color filters (CMY) you can produce all of the colors that you can produce by mixing (additive) light (RGB) limited only by the quality of the filters.\n\n> Most all windows printer drivers have good color matching algorithms.\n\nYes, the driver has algorithms.  These are not really color matching algorithms, they compensate for the fact that the ink is not perfect (color failure).  But these do not compensate for the fact that the color gamut of the printed image is smaller than the color gamut of the monitor screen.  For that you need to use color matching software -- and that is what color matching is: trying to approximate the printed image on the monitor screen.\n\n> a *good* app will already know that the printer is limited to CMYK\n\nActually, what it needs to know is that it is limited to ink on paper, and as I said, the application should have a color matching preview.  But, color matching and a color print preview are not CMYK, they are color matching.  This is best done by loading a pallet that approximates the color gamut of the printer.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE graphic apps"
    date: 2007-05-25
    body: "No, you are not right about compatibility between rgb and cmyk colors.\nif you see one of this beutiful diagrams of RGB color space, and CMYK color space,you can see that some parts of the figures describing color spaces did not overlap.\nthe conclusion is that there are colors in CMYK and RGB color spaces that can not be represented between two color models.\nThat's why the designers that make graphics for printing, must have simulation of CMYK color on the RGB monitor - because there is probability of using \"Incompatible colors\"."
    author: "Pavel Kalinov"
  - subject: "Re: KDE graphic apps"
    date: 2007-05-25
    body: "sorry :)) I did not see the date of previous postings :)) ..."
    author: "Pavel Kalinov"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "I like this. kolourpaint for just a small edit, and krita for the serious work."
    author: "superstoned"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "If you want something to compete with Inkscape/Sodipode then you need to start helping with Karbon. It might be a good idea to ask Rob Buis beforehand whether he intends to use KCanvas (from kdenonbeta) in Karbon instead of the current libart backend. Karbon is for vectors.\n\nKrita is for rasters. My personal area of interest is painting, image creation. Preferable with something that mimics or even models natural media like paint, ink or graphite.\n\nIn essence, though, Krita is a raster editor, like I said. That means that Krita is suitable for editing existing images like photographs or screenshots. There are already useful filters and tools for those tasks.\n\nBut as soon as we've got the basics for raster image editing nailed down, we will move to more interesting things, like >8 bit/channel images (think OpenEXR or Photogenics), paint (DaB, Impasto, Corel Painter, Art Rage, etc.) and ink.\n\nComparisons with the Gimp are a bit useless because we don't share the same goals. I want Krita to occupy the niche Corel Painter has in the Windows world, not Photoshop. Only there'll be differences. Besides, the Gimp has a head start that's impossible to overtake. The Gimp was started (depending on Motif) before even work on Qt was begun. We have to find our own niche, based on the interests of the developers."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "Hmm...really bad to hear. Why do you want to fill a niche? Obviously KDE is heavily lacking from an App like Photoshop. So after Years of non-development there is new activity and now you come with the goal to fill a niche instead of make the so needed Photoshop like App for the KDE-Framework.\n\nWhat does it matter that Gimp is so much ahead? We have absolutely no Program on Qt/KDE Basis, that's it what matters.\n\nI don't want to Troll around here but now I'm really disappointed about your view and Goal for Krita. I belive the very most People want a Photoshop like Krita instead of Painter like. What a Pity that I set my hope in Krita."
    author: "Frank"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "Because that's what I'm interested in myself. I am more interested in a Painter-like application (but done right) than in a Kimp. Like it or not... Tell you what: our first release will probably not contain anything Painter-like yet. You can then fork that one, and move towards a Photoshop or Gimp clone. And I'll continue towards implementing the application I want to have. Fair deal?"
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "If I could, I would. But unfortunately I'm an Artist not a Programmer. I can only hope that there are a few \"KDE-loving\" Developers who also want to have a Photoshop like Applikation."
    author: "Frank"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "You could surprise yourself... I didn't have any experience with C++ when I started working on Krita (although I've had rather a lot of experience with other languages). Anyway, if we disappoint you, you could always give it a try. You may turn out to be a demon coder as well as an artist. I mean, I code, sketch, paint and write novels. All equally amateurish, that's true, but it does show that there's no essential dichotomy between code and art."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-08
    body: "The last 2.1.3 dev version of Gimp has proper separation between libs and UI. So we can expect in the future a KDE interface for GIMP.\nIf Krita makes painting easier and with really the kind of painting tools you can find in Painter (proper charcoal or watercolor painting tools) then it will indeed make a lot of sense.\n\nRichard"
    author: "Richard Van Den Boom"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-09
    body: "That's very interesting. A KDE interface for GIMP (with an improved UI, maybe?) would be great."
    author: "Mikhail Capone"
  - subject: "What is so wrong with Gimp's UI?"
    date: 2004-08-10
    body: "Seriously I don't get it.   I look at the screen shots of Krita and I can't see the difference really.  People want to rewrite a whole application just to get MDI ?? \n\nBut if you dedicate a single virtual desktop to running Gimp  it's practically the same thing as MDI anyway."
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: What is so wrong with Gimp's UI?"
    date: 2004-08-10
    body: "I don't like MDI anyway. :-)\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What is so wrong with Gimp's UI?"
    date: 2004-08-10
    body: "No. I don't want to rewrite the Gimp to get MDI. Krita isn't MDI as is usually understood either, nor is any other KOffice application. All are SDI applications. Additionally, some (KPresenter, KSpread, Krita) support the concept of pages. You can have a collection of images in one Krita document.\n\nAnyway, I admit to code re-use from the Gimp, but I'm not trying to write a Kimp... I'm afraid I'm beginning to sound like Patrick Julien when I'm saying that, but it's true. I want a high-end raster graphics app with an emphasis on painting. That doesn't mean it won't do what most people use Photoshop for, because you need to get that right first..."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-09
    body: "Richard is bit too sanguine about the advent of a KDE frontend to the Gimp. Nobody is working on making it happen."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-10
    body: "Hi,\n\nI probably didn't express myself well. Apparently, proper separation between UI and libs has just been reached in Gimp 2.1.3 so I don't expect anyone to have started working on it yet. When this separation will be stable, though, I would not be surprised that some team gathers to do the job.\nIn any case, my post was not at all to claim your work is doomed, if this was not clear. I'm very interested in a software that reaches painting capabilities of Painter, especially with \"natural\" tools (watercolor for instance). And since apparently Gimp devs gave up the idea, I'm very pleased to see another project aiming that goal.\nGood luck.\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: KDE graphic apps"
    date: 2004-08-10
    body: "I see nothing wrong with his persuing his own goals for his application.\n\nBUT, we need to realize that it is going to be an artist's application.\n\nAs you say, we need something like PhotoShop.\n\nWhat I would like to see is something like KIMP but with all of the features of XFig added along with the ability to produce both vector and raster output file formats (along with XML that could contain both PNG, JPEG and SVG).\n\nThis would be a killer application.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
In <a href="http://cvs-digest.org/index.php?issue=aug62004">this week's KDE CVS-Digest</a>:
KTracer, a realtime execution path tracer was committed to kdenonbeta.
<a href="http://www.koffice.org/kexi/">Kexi</a> can save table design changes, and the MySQL driver was reintroduced.
KFormDesigner, part of Kexi, is now a KPart.
<a href=" http://www.koffice.org/krita/">Krita </a> adds dynamic brush scaling.
<a href=" http://www.kdetv.org/">kdetv </a> includes a number of new filters: color inversion, chromakill, mirror image, sharpness.
Kifi now shows found network details in real time.


<!--break-->
