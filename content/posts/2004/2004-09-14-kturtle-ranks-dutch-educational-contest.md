---
title: "KTurtle Ranks in Dutch Educational Contest"
date:    2004-09-14
authors:
  - "a"
slug:    kturtle-ranks-dutch-educational-contest
comments:
  - subject: "Great!"
    date: 2004-09-14
    body: "I've long wished for a Dutch-language programming application/game for my kids. Rebecca wants to be a hacker, but English is a big stumbling block for her. I've tried to translate Squeek, but that's rather a big task. And logo... I think they're going to love this."
    author: "Boudewijn Rempt"
  - subject: "Re: Great!"
    date: 2004-09-15
    body: "Also, kturtle is available in Dutch, including the manual, so you can start learning your daughter hacking today :)\n\nRinse"
    author: "Rinse"
  - subject: "Re: Great!"
    date: 2004-09-16
    body: "Well, Naomi tells me the application is in Dutch, but not the manual... She doesn't seem to mind, she's messing about with it right now."
    author: "Boudewijn Rempt"
  - subject: "Re: Great!"
    date: 2004-09-16
    body: "It is at least available online:\nhttp://docs.kde.org/nl/HEAD/kdeedu/kturtle/\n\nYou can also download the docbook and from cvs \n(kde-i18n/nl/docs/kdeedu/kturtle) and place it in ~/.kde/share/doc/HTML/nl/kturtle/\nif things go right, khelpcenter wil render the Dutch manual instead of the english one :)\n\nRinse"
    author: "Rinse"
  - subject: "Re: Great!"
    date: 2004-09-17
    body: "Thanks!"
    author: "Boudewijn Rempt"
  - subject: "Hmm... Misannouncement"
    date: 2004-09-14
    body: "The prize is not given yet. And the order is not fixed.\nIf, Anne-Marie \"annma\" Mahfouf, you would have asked me, or have read the real announcement (in Dutch):\nhttp://www.linuxmag.nl/nl/4137085f61440#a41370aad80c4d\nyou would have noticed all this...\n\nI'm so sorry but I, the main developer of KTurtle and the one who subscribed for the contest, would like to have this message of the dot.\n\nThe 1st of October I'll be going to get the prize, what ever rank it may be.\n\nI'm off now, more hacking on KTurtle, I conpletely refactored the interpretor last few days.\n\nPlease save yr congrats for later,\nCies.\n"
    author: "cies"
  - subject: "Re: Hmm... Misannouncement"
    date: 2004-09-14
    body: "We don't delete posted articles, but I'll amend it and offer to repost an announcement on the 1st when you get the results."
    author: "Navindra Umanee"
  - subject: "Re: Hmm... Misannouncement"
    date: 2004-09-14
    body: "annma is not the one to blame. That would be me. Afaics there is no change in the ranking of the applications and the result appears to be final when I read http://www.linuxmag.nl/nl/4137085f61440#a41370aad80c4d.\n\nBut I should have checked with you. Apologies !\n\nFab"
    author: "Fabrice Mous"
  - subject: "kturtle is not a very good name."
    date: 2004-09-15
    body: "Sorry.  If you want to go with the k theme, how about kashyap?  That's tortoise (same thing :-) in Sanskrit."
    author: "General Zod"
  - subject: "Re: kturtle is not a very good name."
    date: 2004-09-15
    body: "Are you proposing to change the name of kturtle to kashyap?\nHmm... intresting.\nDo you really think 'kashyap' will make a good impression on kturtle target audience?"
    author: "cies"
  - subject: "Re: kturtle is not a very good name."
    date: 2004-09-15
    body: "to the Sanskrit speaking ones, maybe ;-P"
    author: "Aaron J. Seigo"
  - subject: "Re: kturtle is not a very good name."
    date: 2004-09-20
    body: "Who I guess would live with the Latin speaking children. :)\n\nBut given that (as the thread about the Danish-speaking user makes clear) only a portion of the audience is going to know what a 'turtle' is anyways, why not. K<word> titles have always been kinda lame, it would be better to use words that begin with K. Though renaming programs isn't a good idea generally."
    author: "Ian Monroe"
---
<a href="http://edu.kde.org/kturtle/">KTurtle</a> has been selected as one of the winners in a <a href="http://www.linuxmag.nl/nl/4137085f61440#a41370aad80c4d">Dutch Educational contest</a>! The jury was very pleased with the looks of KTurtle, good configuration options and a very nice manual. <i>"Some renewed attention to LOGO is very much welcome"</i>. We congratulate Cies and the KDE Edu team on this achievement. You can read <a href="http://edu.kde.org/kturtle/news.php#itemKTurtlewon3rdprizeinDutchEducationalcontest">the full announcement</a> hosted on the <a href="http://edu.kde.org/">KDE Edutainment website</a>. 






<!--break-->
