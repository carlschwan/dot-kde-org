---
title: "Qt 4 Preview 2 Highlights Accessibility; D-BUS Bindings for Qt 4"
date:    2004-09-24
authors:
  - "bharry"
slug:    qt-4-preview-2-highlights-accessibility-d-bus-bindings-qt-4
comments:
  - subject: "Cool"
    date: 2004-09-23
    body: "The D-BUS stuff looks damn nice!\n\nFirst Post :-)"
    author: "bugix"
  - subject: "Re: Cool"
    date: 2004-09-23
    body: "Hammerkrass I'd say ;-))\n\nAlex, second, downloading now\n"
    author: "aleXXX"
  - subject: "Re: Cool"
    date: 2004-09-23
    body: "Maybe the binding look nice. The freedesktop DBUS implementation is some shitty C though...\nIts like building a villa in a swamp."
    author: "M"
  - subject: "Re: Cool"
    date: 2004-09-23
    body: "So? The user doesn't care what you call a \"shitty language\". It works, it works well, and makes the desktop easier and more functional to them, so they will use it."
    author: "ca"
  - subject: "Re: Cool"
    date: 2004-09-23
    body: "...until the get hacked or crash!\nC libraries are in the insecurity news all the time.\n\nC is a disease and KDE made the right decision not to use C but at least a subset of C++. Now we have a C rewrite of DCOP by the gnomes. Its two steps backwards and a waste of time. "
    author: "M"
  - subject: "Re: Cool"
    date: 2004-09-23
    body: "> ...until the get hacked or crash!\nC libraries are in the insecurity news all the time.\n\nWhile it's true that C++ provides quite a bit more safety over C, it's still as easy to produce insecure code with C++ as much as it is with C. \n\nPerhaps we should move away from systems-level languages like C and C++.\n\n> Now we have a C rewrite of DCOP by the gnomes. Its two steps backwards and a waste of time.\n\nI don't think it'll be a waste of time in the future as more and more system-level daemons start using dbus. Oh yeah, being able to interoperate with various GNOME apps is good too. "
    author: "anon"
  - subject: "Re: Cool"
    date: 2004-09-23
    body: "> While it's true that C++ provides quite a bit more safety over C, it's still\n> as easy to produce insecure code with C++ as much as it is with C. \n\nDo you actually read what you type? You dont make sense. \n\nIt is way easier to write (and read!) C++ code than C, while C++ can even be slightly faster than C.\nBut my point is not that C++ is the best language ever made (even though its pretty good), my point is that you should not use a 30-40 years old technology like C for todays programs.\n\n"
    author: "M"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Since C is an almost complete subset of C++, most C statements are valid C++ and your niffty C++ code probably uses quite some elements from that subset. The anonymous poster is right in saying that producing insecure C++ code is very easy. Of course you can always compensate this insecurity by using strict conventions, but this is possible in C as well. Bashing some project just because it is developed using a certain language is quite unhelpful to say the least. Remember your trustworthy Linux kernel is C code as well and even the C++ based KDE had security issues in the past. That being said I consider myself a C++ programmer but not the kind of language fascist that you seem to be."
    author: "Anonymous"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "I cant agree with this. It not the existing features that make C bad (outdated) its the missing ones! You can use better programming strategies that dont exist in C.\n\nAnd I wouldn't really call the linux kernel trustworthy..."
    author: "nl"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Ahh I love the C vs. C++ fight.\n\nI wonder if any of you people have even looked at the dbus code ?? - I have and it looks well written, so why not just accept that some cool programs is written in C and others in C++ (and Haskell, and Java, and C#, and ....). \n\nAs for the 30-40 year old technology, I can tell you that this old stuff powers most of the operating system kernels around the world today (inluding Linux, FreeBSD and big parts of the NT kernel) - but then again you can always try to convince Linus, Alan and the rest of the Linux gang to rewrite it in C++.\n\nOhh do you really mean that reading C++ is easier than C ?. Given that the grammars/lexical defenitions for C++ is much more complex than C, and most people writing programs in C++ only have marginal understaning of the language."
    author: "Lars Roland"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "> I wonder if any of you people have even looked at the dbus code ?? - I have and it looks well written, so why not just accept that some cool programs is written in C and others in C++ (and Haskell, and Java, and C#, and ....). \n\nlast time I checked there was glib code pasted into it, but that was like a year ago or so. Overall I wasn't impressed.\n\n> Ohh do you really mean that reading C++ is easier than C ?\n\nSure it is. Every C++ program follows roughly the same rules. On the other hand for every C library that I use you have to learn a new \"object emulation model\" or some more or less crazy API."
    author: "peter neuberger"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "> C++ is much more complex than C\nYeah, try reading the boost libraries. Even the STL files, in/usr/include/c++/, can be hard to follow. But don't get me wrong, the result is _very_ powerfull both in features as in speed. But indeed KDE's includes are easy to read.\nActually I was quite happy to only have to recompile libfam and Qt/KDE, when upgrading gcc from 3.3 to 3.4. Not to mention the compile time difference. Long live C, and may every library, that I don't have to use, be implemented in C. And which I do use, well mostly it doesn't really matter.\n\nWhat's really a swamp is building on top of win32, because you don't know what beneath you (and you get sucked away as soon as it becomes too popular)."
    author: "koos"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "I find the stl easier to read than most C libraries(lets say glib). Qt/KDE you can almost read like a book.\n"
    author: "stl"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "You probably choose the most expensive super market, because the vinegar is much cheaper there then the bordeaux wines from price fighter mega store next door :-)"
    author: "koos"
  - subject: "Re: Cool"
    date: 2004-09-25
    body: "No, I chose to drive a car(C++) rather than riding on a horse(C) to work. \nBut seriously, I know it is hard to compare C libs to C++ libs because there is not very often 2 libraries for the same thing. I chose stl vs glib, because they do roughly the same, I/O, containers, string."
    author: "stl"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "> Oh yeah, being able to interoperate with various GNOME apps is good too. \n\nit's not just GNOME. HAL (hardware abstraction layer) uses DBUS as well to notify from (the Linux) kernel space out about hardware devices (including hotplugging). DBUS is useful to all sorts of apps, not just GUI apps. it is, IMHO, the obvious and logical progression from DCOP, which proved within the confines of the desktop the utility and power of a purpose-built lightweight IPC mechanism, out to the OS in general. people complain about how the desktop is its own little disconnected world on UNIX, and DBUS is one step towards bridging that gap. \n\nso, it's not *just* desktop interop, it's a way to bring a meaningful communication stack across the OS. the ATK DBUS bridge is one such example."
    author: "Aaron J. Seigo"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: ">people complain about how the desktop is its own little disconnected world on UNIX, and DBUS is one step towards bridging that gap.\n\nEmmmh.Perhaps this fact that the gui isn't linked to deep with the os saves us from the experience of hardlocked machines like Windows, if gui apps crash.\n\nMaybe DBUS brings us more possibilities for the Unix desktop, but i hope we don't have to accustom with daily reboots or security holes that find their way to the os through the desktop.I would prefer the desktop as a separate thing (like it is) with not to much power over the os."
    author: "Heiko"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Interacting with a messaging bus and deeply integrating poorly abstracted code into the kernel are about as similar as installing phone lines are to building on top of another house. Getting a message or not getting a message where a client is in trouble is not the same as the client code blocking the kernel when it crashes because they are directly linked.\n\nThe key here is that a messaging system can provide the benefits with a huge buffering effect reducing the issues to a fraction of the problems possible with deep integration. This should affect your need to reboot about as much as a web site going down unless it's done very poorly."
    author: "Eric Laffoon"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Don't feed the trolls..."
    author: "Claus Rasmussen"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "<I>\"...until the get hacked or crash!\"</I>\n\nNo, even then they won't care. See the success of MS Windows, dispite the hacks and crashes. Ease of use, that's the most important thing. They couldn't care less about your language flamewars."
    author: "ca"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "You didnt understand anything. Its not a language flamewar, its just that the situation on linux et al. is very disapointing technologywise. We are not in the position to cheat IBM once more as MS did, neither has linux the marketshare to afford low quality C libs like Windows.\nEveryone is creating modern frameworks in something nice like C++/ObjC(++)/Java/C# or whatever. Look at MacOS X, Sun, or even BeOS. Only free software is mostly crapped up with C. KDE/Qt is one of the few exceptions and that is the reasons why it attracted many developers."
    author: "M"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Instead of spitting on C, where is the C++ variant then. DCOP never made it outside KDE AFAIK. And DCOP has it drawbacks, eg. an DCOP object is tied to its application object name. Recently I started using kmix for volume control. But where is the kmix object, inside kicker, somewhere else, or not running at all? These are question you _don't_ want to care about when just wanting to control the volume. Besides, KDE is obsessed with multi-platform. Read the threads, nobody dares to implement hotplug/sysfs, or how are they called, events.\nThanks to Qt taking the lead (and of course work from the dbus/kernel developers!!!), we'll finally have these in KDE."
    author: "koos"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Then you should have said so instead of calling it a \"shitty language\" with no further explanation.\n\nAnyway, C++ has its share of problems. The lack of a stable ABI being the most important one. GCC 2.95 vs 3.0 vs 3.1 vs 3.1.1 vs 3.2 vs 3.4, all have different C++ ABIs. KDE apps compiled with 3.2 won't work on 3.4 distributions unless the user recompiles QT and other libraries."
    author: "ca"
  - subject: "Re: Cool"
    date: 2004-09-25
    body: "Not too long ago the C++ ABI was standardised. I am not 100% sure but I think its based on an aggreement between gcc and intel."
    author: "M"
  - subject: "Re: Cool"
    date: 2004-09-26
    body: "The C++ ABI was *already* standardized. But it's so complex that nobody has managed to do it correctly. A while ago, both GCC and Intel found bugs in their compiler so they 'fixed' them and now produce a different ABI. How long before they find more bugs and break ABI yet again?"
    author: "ca"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Hm seems like you have no idea about programing. C is low level langauge, it has those fancy *(c++) = *(--b) operators for a good reason (move-post increment instructions), so it is just natural that something low level as D-BUS is implemented in C. Whats more, writing bindings to C++ is hard..."
    author: "miro"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "> Whats more, writing bindings to C++ is hard...\n\npot. kettle. black.\n\nIt seems like you have no idea about programming.  Making such stupid general statements about C++ libraries and bindings.  It is perfectly feasible to implement libraries elegantly in C++ and write beautiful bindings to them."
    author: "ac"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "When you want to write bindings to C++ libraries it's always neccesary to make a intermediate library in C to convert it to a plain API (CLX for Example, need Pascal bindings for QT), and this way isn't beautiful for me, although QT library in elegantly implemented in C++."
    author: "Ivan Deras"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "Not if you are using something like Smoke."
    author: "Roberto Alsina"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "The problem with bindings is the ABI not C++ itself. Name mangling, virtual (method) table structure, memory alignment and calling conventions are the main issues that must be solved for a working binding. And who known better about these ABI things than the compiler for the API itself. So instead of finding out how to map the C++ ABI to Delphi' ABI and invest additional time when a new gcc is released (gcc had a really inconsitent ABI in the past), you \"simply\" write a C wrapper shared object in C++ and recompile this when gcc has changed.\n\nIt is possible to map the C++ ABI of gcc 2.95 to Delphi and it is possible to map the ABI of gcc 3.1 to Delphi ABI.\nName mangling can be resolved by Delphi's shared object import concept \"function name(parameter): RetType; cdecl; external soName name 'symbol';\" plus the c++filt utility.\nVMT mapping works as long as the base class has a virtual method declared before a field, so the C++'s VT pointer is at offset 0. (QObject works perfectly). Otherwise you must use the obsolute \"object\" keyword (from TurboPascal days). It is the corresponding type for C++'s \"class\" keyword, but it is obsolute and said to have many bugs (I never found one).\nFor gcc 2.95 you must declare two dummy virtual methods to move the VMT offset of the first real virtual method 8 Bytes down.\nFor gcc 3.x the dummy method is not necessary (and would lead to wrong calls)\nMemory alignment is no problem at all. With Delphi you can adjust it {$ALIGN 4}\nAnd the calling convention is furtunatelly the \"cdecl\" one.\n\nBut even if you implement all this (it works, I have tested it last year with QObject and QWidget) two issues remain:\n1. What is with inlined methods/functions, they are not exported by the shared object. You must find them and reimplement them in Delphi by hand. And C++ programmers tend to use many inline function (at least Qt has lots of them)\n2. What is with templates? Templates are always inlined. They are not possible in Delphi except you write all template specializations by hand. Qt does not have that many (what about Qt4 ?) but eight or more are even too many to keep in sync with newer Qt versions.\n\n\nSo you see it is possible with limitations. The perfect solution would be a very small C wrapper (templates?, inline functions) and a native ABI map. But when you already have a C wrapper why not add all things to the wrapper and simply use a C interface from the binding language to the C++ code.\n\nIf there would be a common ABI all these issues would not exist. .NET for example has such a common ABI. The intermmediat language is designed to support more than one programming language. That's the reason why beside C# there are also VB#, Delphi.NET, ...\n\nI wish gcc would export an additional \"common ABI\" for classes and templates like joliet is for iso9660. It is additional but if you cannot read it you can use the \"old\" one (ok the comparision is not good). Another example is COM under Windows. You can use it in C++, VB, Delphi and even in all .NET languages. And if that \"common ABI\" would be controllable by a class modifier that would be really great. For example: Q_EXPORT __commonabi class MyClass {} In this case gcc would create a function pointer table for all non-private methods.\n\nstruct CommonAbiClass\n{\n  char* name;\n  CommonAbiClass* bases; // array\n  CommonAbiItem* methods; // array\n  void* reserved; // for future extensions\n};\n\nstruct CommonAbiFuncItem\n{\n  void* myfunc;\n  char* name;\n  paramItem** params;\n  type retType;\n  void* reserved; // for future extensions\n};\n\nAnd as fields should not be accessed directly all could be handled through these function pointer tables. And you would have an included RTTI for each public and protected method. Even bindings could be generated without the source code.\n\nI'm dreaming, I know.\n\n\n\nRegards,\n\nAndreas Hausladen"
    author: "Andreas Hausladen"
  - subject: "Re: Cool"
    date: 2004-09-25
    body: "You have some interesting ideas here, but it is all very compiler specific and so a practical binding could never be implemented this way. Also how do override C++ virtual methods in a Pascal class using this sort of technique? To me it looks like it only solves the problem of calling C++ methods from Pascal and not callbacks for virtual methods or slot invocations going the other way."
    author: "Richard Dale"
  - subject: "Re: Cool"
    date: 2004-09-25
    body: "I have a working Pascal binding (http://tinyurl.com/55cd9) with signal/slot support and virtual method overriding. \n\n\nThe \"common ABI\" could have (as generated by the compiler itself) an additional void** for virtual methods that point to the virtual table entry.\n\n\n> but it is all very compiler specific and so a practical binding \n> could never be implemented this way.\n\nThat's why I wrote: \"I'm dreaming.\""
    author: "Andreas Hausladen"
  - subject: "Re: Cool"
    date: 2004-09-26
    body: "Well I looked at the .png. \n\nBut then I see you're working on Qt#/Bugtussle, and I've been fishing around looking at those docs - interesting. I hadn't heard of it until Adam Treat mentioned it in his blog a few weeks ago. I hope by KDE 3.4 at least one of the three(!) C# bindings projects succeeds in wrapping the KDE api. There will be a lot more people interested in a C# binding than Pascal I would have thought. \n\nI haven't done anything on my Kimono C# project for a while, because I've been too busy with ruby. And I really ought to spend some time getting the KDE java bindings improved a bit - so I'm not sure if I'll have come up with anything worthwhile by KDE 3.4. I don't think multiple C# bindings implementations are a problem, but it might be a good idea to ensure they are source code compatible if that was possible. Then a C#/KDE app written with one variant would compile and run with the other two.."
    author: "Richard Dale"
  - subject: "Re: Cool"
    date: 2004-09-26
    body: "> I hope by KDE 3.4 at least one of the three(!) C# bindings projects succeeds in wrapping \n> the KDE api. There will be a lot more people interested in a C# binding than Pascal I\n> would have thought.\n\nWhen Adam is back we start to mix Bugtussle with Binge. Bugtussle is good in reading the doxygen XML files and Binge is good in writing C# bindings.\nBut at the moment I'm writing a C++Parser in C# to remove doxygen dependencies. Doxygen is a documentation tool and as such it is not really a good base for writing bindings. And the lastest doxygen makes template generation impossible because the xml base class entities lost the template parameters.\nQByteArray derives now from QMemArray instead of QMemArray<char>.\nAnd I'm not in the mood to rewrite the APILoader code every time doxygen changes."
    author: "Andreas Hausladen"
  - subject: "Re: Cool"
    date: 2004-09-24
    body: "NO!  It's NOT! These are lies perpetuated by people with language agendas (e.g. GNOME trolls) Look at libfam and look at all manner of languages and environments using it even though it's implemented in C++.\n\nhttp://oss.sgi.com/projects/fam/"
    author: "ac"
  - subject: "Re: Cool"
    date: 2004-09-26
    body: "FAM exports a C interface, not C++.\n\nLanguage agendas? If anything, people like you have language agendas and deserve to be called trolls. There are GTK/GNOME apps out there that are written in C++, or Python, or whatever. They don't say \"C++ sucks\", they provide bindings for many languages. On the other hand, people like you say \"C sucks, I don't care what legitimate reason you have to use C, you are an idiot for using C\"."
    author: "ca"
  - subject: "Re: Cool"
    date: 2004-09-26
    body: "> FAM exports a C interface, not C++.\n\nExactly. Keyword: exports.  Implemented in C++.  Now I hope next time you can hold an informed conversation instead of making stupid statements against C++.\n\n>  \"C sucks, I don't care what legitimate reason you have to use C, you are an idiot for using C\".\n\nIt's not me saying this. It's the GNOME camp that's saying this.  What do you think Mono is?  People over there are fed up with C.  I don't give a damn about how you feel about C, it's irrelevant."
    author: "ac"
  - subject: "Re: Cool"
    date: 2004-09-25
    body: "While it is still better to write the library in C++ and provide some simple C wrapper if a binding is really needed. Just look at QtC, its not so hard and you only need to wrap up the public API. \nMany languages don't need bindings for C++ as there is already some compiler technology to mix them. C++, objC, Java can be mixed to some extent with gcc for instance."
    author: "M"
  - subject: "Re: Cool"
    date: 2004-09-25
    body: "\"Just look at QtC, its not so hard and you only need to wrap up the public API. \n Many languages don't need bindings for C++ as there is already some compiler technology to mix them.\"\n\nThe QtC library has been retired and replaced by Smoke. It's not true that just because gcc supports java or Objective-C you don't need bindings for them.\n\n\"its not so hard and you only need to wrap up the public API\"\n\nUmm, I would like to say language bindings are easy, but they're not. Ashley Winter's Smoke library design is one of the most innovative things in KDE in my opinion. A long, long way ahead of what I managed with my first attempts at language bindings with QtC (as part of the effort to implement Objective-C bindings).\n\n\n\n"
    author: "Richard Dale"
  - subject: "Re: Cool"
    date: 2006-04-29
    body: "You could also use Fake Objects using C Structs or Pascal Records.\nLooks a bit messier than true OO code but it works. The balance between OO and procedural code, while still having the power to export from a library. Messier than OO but cooler than OO :-)\n\nI've attached a file which shows an example of a Pascal code with a few Fake Objects using plain records.  Private data in the fake object is accessible if you want to shoot your foot, but is labelled very clearly using _private. Similar things have been done in the C language using _private or private_.\n\nCall me messy but this was just a little experiment of mine. Note how I fake \"self\". Objects have a self available and in order to make \"self\" available using records/structs, you must pass a dummy Self parameter throughout some of  the functions. \n\nThe example file attached shows how to use a Pointer to a record or a regular record without being a pointer. See FakeObject versus TRecord in the attached file. EXE demo is included on win32 - I'll make a linux elf some time later. \n\nThis example was done in delphi but should be easily compiled in FPC too. CompactUtils uses true objects, ignore that - the fakeobject examples are in the FakeObject directory.\n\n\n\n"
    author: "L505"
  - subject: "Amazing speed of Development"
    date: 2004-09-23
    body: "It's amazing at what speed Qt is being developed.\n\nI dare say, that apart being premier choice on Linux already, with say QT 4.2, there will complete breakthrough making Qt and KDE the de facto standard for new apps.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Amazing speed of Development"
    date: 2004-09-23
    body: "That's a bummer. You mean the \"world-domination\" plug-in won't be out 'till 4.2?"
    author: "Rayiner Hashem"
  - subject: "Re: Amazing speed of Development"
    date: 2004-09-23
    body: "Give me a second. I will post the first attempt on kde-apps tonight. So far it will be a continent-domination applet for kicker. Hope that's okay for you."
    author: "Sebastian"
  - subject: "Re: Amazing speed of Development"
    date: 2004-09-24
    body: "does it work with the universal taskbar?"
    author: "anon"
  - subject: "docs"
    date: 2004-09-23
    body: "I just had a look at the Qt4 docs, what I'm still missing is a separation in the docs by the libraries, i.e. Qt Core docs, Qt GUI docs etc.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Nice!"
    date: 2004-09-23
    body: "Judging from the DBUS Qt4 tutorial, it looks like that API is a LOT cleaner than than the dbus-C api. I'll have to say that Trolltech does a wonderful job in designing sane API's. "
    author: "anon"
  - subject: "You will all bow down to Matthais"
    date: 2004-09-23
    body: "http://ktown.kde.org/akademy/Matthias_Ettrich_Designing_Qt-style_APIs_audio.ogg"
    author: "Justin Karneges"
  - subject: "Re: You will all bow down to Matthais"
    date: 2004-09-23
    body: "Video: http://ktown.kde.org/akademy/Matthias_Ettrich_Designing_Qt-style_APIs_video.ogg\nSlides: http://conference2004.kde.org/slides/matthias.ettrich-designingqtstyleapis.html\nTranscript: http://conference2004.kde.org/transcripts/matthias.ettrich-designingqtstyleapis.php"
    author: "Anonymous"
  - subject: "Download link ?"
    date: 2004-09-23
    body: "Somehow I can't find a link to download Qt 4 TP2 ...\nAny hints ?\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: Download link ?"
    date: 2004-09-23
    body: "Follow the link \"download page\"? Just an idea..."
    author: "Anonymous"
  - subject: "Re: Download link ?"
    date: 2004-09-23
    body: "Oh well. A long day and some beer...\n\nAlex\n"
    author: "aleXXX"
  - subject: "DCOP"
    date: 2004-09-23
    body: "DCOP was part of KDE, not Qt... Since there is a replacement in Qt now, will DCOP be going away? Or is this not really a replacement?"
    author: "AC"
  - subject: "I dont think..."
    date: 2004-09-23
    body: "that particular decision has been made yet. I think the arrival of dbus bindings within KDE will help more KDE developers use and compare DBUS with DCOP. I personally am interested in DBUS, but I wasn't willing to touch it until there was  a saner API developed for it. Thank you trolls!"
    author: "anon"
  - subject: "KJSEmbed for Qt4"
    date: 2004-09-23
    body: "I hope to port the standalone version of KJSEmbed (QJSEmbed) to the technology preview in the next few weeks. That should give us an idea of how hard is to port code that is intimately tied to the internals of Qt to the new release. My initial investigations with the first preview indicate that this should be possible with a minimum of pain.\n"
    author: "Richard Moore"
  - subject: "D-BUS replaces Bonobo?"
    date: 2004-09-23
    body: "You missed some important news:\nhttp://trolls.troll.no/~harald/accessibility/\n\nI don't understand too well but it looks like there is now a way to replace Bonobo with D-BUS in AT-SPI/ATK stuff?  This is great news!  I wonder how Sun/GNOME will react to that."
    author: "ac"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-23
    body: "I don't think AT-SPI and ATK has ever used Bonobo.. how could Mozilla, Java/Sun, etc,, use AT-SPI then? "
    author: "anon"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-23
    body: "Bonobo is an implementation of CORBA.  Mozilla and Java would use CORBA to talk to AT-SPI/ATK.  Correct me if I'm wrong, this stuff is all way too complex and over-engineered for me to understand any of it.\n\nOne of the problems KDE has with AT-SPI/ATK.  Now Harry has provided a replacement implemented in D-BUS.  This could be a challenge to Sun which has been really reluctant to adopt D-BUS for AT-SPI/ATK last I was told."
    author: "ac"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "Correct me if I'm wrong, but wasn't Bonobo a set of CORBA interfaces, not a implementation of CORBA. I know that AT-SPI uses CORBA, but I don't think it uses Bonobo. I don't think ATK uses CORBA other than using it to interface with AT-SPI (still not using Bonobo). I beleive that Qt goes through ATK, which uses dbus, so it doesn't have to speak CORBA with AT-SPI.\n\nCorrect me if I'm wrong."
    author: "anon"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "Right.  So hopefully this means we can get rid of the CORBA dependency from KDE and that Sun will cooperate.  Else it might mean forking ATK?  I don't even know if this is viable."
    author: "ac"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "I don't think you can ever remove CORBA dependencies from ATK because ATK itself tries to save programmers from the CORBA-madness that is AT-SPI. Unfortunatly, AT-SPI is intimatly tied with CORBA and it'd be hard to make a replacement for it as it is the only software package in the UNIX platform that vendors will likely support accessible hardware with."
    author: "anon"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "We removed the CORBA dependency from KDE before KDE 2.0 was released, the process was something like this:\n\n1. Torben's first KParts demo'd at KDE I. Based on shared libraries with each host having knowledge of the base classes it embedded and no IPC.\n\n2. The second version developed for KDE 2.0 which was based on CORBA. This was removed in the months preceding KDE II with pretty much all of it removed by the end of the conference. This version was never part of a final release of KDE.\n\n3. The current version of KParts based on shared libraries with DCOP for IPC and KTrader for picking components to load. This version can use virtual base classes to provide component independent interfaces such as that provided for text editors (hence the KVim part).\n\n"
    author: "Richard Moore"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "I meant the dependence on CORBA that KDE/Qt would have if it depended on accessibility technologies that require CORBA."
    author: "ac"
  - subject: "DOH!"
    date: 2004-09-23
    body: "Of course, I don't mean bonobo, I mean ORBIT."
    author: "ac"
  - subject: "correction"
    date: 2004-09-24
    body: "s/bonobo/orbit/g"
    author: "ac"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "from my conversations on the various accessability email lists with folks from Sun, they aren't exactly warm to the idea of replacing CORBA with DBUS within the scope of ATK. they've put alot of work into their Java layer and OOo for the CORBA stuff, and i think we all know what it's like to have our original implementation concepts modified in the scope ofa  larger group of people: we all get very attached to our \"babies\". but the guys doing the work at Sun are quite good at what they do and i'm sure at the end of the day we'll find a solution that everyone is comfortable with."
    author: "Aaron J. Seigo"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "That's pretty charitable of you. It's the fault of companies like Sun that CORBA didn't die an early death like it deserved. There are good APIs, bad APIs, and then there is CORBA...\n\n/bitter CORBA programmer"
    author: "Rayiner Hashem"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "As far as I know, OMG is developing complete new C++ bindings for CORBA. Maybe they will get them \"right\" this time. That would be pretty useful since CORBA is still the only universally usable distribution middleware around, at least in my opinion."
    author: "Random KDE User"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "If OMG wants to fix CORBA, they should start by going to Konqueror, selecting the current standard, and hitting \"Shred.\" It is that broken."
    author: "Rayiner Hashem"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "\"Shred\" was removed from Konqueror."
    author: "Henrique Pinto"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "And maybe they should stop using a stupid name like \"Oh My God\".  It's really immature and childish."
    author: "ac"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-25
    body: "Did you miss out the smiley, or don't you know that it stands for 'Object Management Group'. Still with the CORBA design they ended up with, it might be a better acronym after all :)"
    author: "Richard Dale"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-26
    body: "A joke, of course. :)"
    author: "ac"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "I had a conversation with you at aKademy where you said you had reservations about the serialisation that DBUS used, and I couldn't quite understand what you thought the problem was. But it sounds to me as though you're more enthusistiac about DBUS than you were then. I agree that CORBA is a complete dog, but perhaps using IIOP might not be a bad idea as a transport layer, which java RMI also uses I believe. Does anyone know what the pros and cons were of choosing the marshalling used by DBUS over IIOP, or DCOP's QDataStream stuff?"
    author: "Richard Dale"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "The main problem with this is that IIOP is primarily designed for *networked* procedure calls, while the primary use case for DCOP/DBUS is IPC between processes running on the *same machine*.\n\nUsing a fundamentally network-oriented framework such as CORBA as transport for an accessibility toolkit (the most local thing you could imagine) indeed appears as, hmm, a bit of a strange idea. You'd think the pain of developping against GNOME's CORBA madness of an API would have taught a lesson or two..."
    author: "Anonymous Coward"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "Anybody care to comment?\n\nI thought Kparts was KDE's answer to CORBA and dcop was something different all together. Hmmmm.... developer.kde.org isn't responding.\n\nBut dcop was designed as a structure to facilitate interprocess app communication, and kparts --or rather a kpart, was designed to be a sharable/embeddable component/object. This seems very different to me, but I'm not a devel.\n\nBut wasn't dbus supposed to be just like dcop, but with a little broader scope (to include kernel events like usb-drives, etc)? Am I stupid, but this doesn't sound like embeddable COM-like components which i thought corba was supposed to do."
    author: "Jeff"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "From my (limited) understanding of CORBA, CORBA would do the job of BOTH dcop and kparts. You would, of course, need a set of CORBA interfaces to make something like kparts, sorta like what Bonobo does. "
    author: "anon"
  - subject: "Re: D-BUS replaces Bonobo?"
    date: 2004-09-24
    body: "I second what the above poster has said. Nowadays, CORBA does both. I do not really know the history of CORBA good, but I think it started a bit different out. It was, and is, basically a distribution middleware. That is, it enables the distribution of objects on multiple machines/contexts and communication between them. That is, the emphasis lies somewhat on distributed computing. Whereas D-BUS and DCOP are message oriented middleware. The emphasis there lies, you guess it, in enabling applications to exchange messages. Not as a means for distributed computing, but more for the tasks like you see KDE is using DCOP for.\n\nThat is at least the impression I have where the emphasis of both technologies lies."
    author: "Random KDE user"
  - subject: "New signals and slots features"
    date: 2004-09-24
    body: "I think you can now connect signals and slots across threads, and you can either emit signals synchronously as now, or asynchronously. Nice. I looked at the online docs, but it doesn't seem to have been written up yet. Connect now looks like this:\n\nbool QObject::connect ( const QObject * sender, const char * signal, const QObject * receiver, const char * member, Qt::ConnectionType type = Qt::AutoConnection ) \n\nSo I assume you describe the signal behaviour with different values of the Qt::ConnectionType enum."
    author: "Richard Dale"
  - subject: "Re: New signals and slots features"
    date: 2004-09-25
    body: "Looking at the QMetaObject code etc. it looks like signals and slots are now handled via events (as I described in my blog entry about TP1). This means it should gain from the thread handling of the new event handling.\n"
    author: "Richard Moore"
  - subject: "Re: New signals and slots features"
    date: 2004-09-25
    body: "I see - that wasn't obvious from looking at the QMetaObject online docs. Like kjsembed, qtruby makes use of the undocumented 'QUObject' class so I'll be interested to hear how you get on with porting that stuff to Qt4."
    author: "Richard Dale"
---
<a href="http://www.trolltech.com/">Trolltech</a> has announced the <a href="http://www.trolltech.com/newsroom/announcements/00000176.html">availability of the second Qt 4 Technical Preview</a>. Key highlights include <a href="http://doc.trolltech.com/4.0/qt4-accessibility.html">support for AT-SPI</a>, bringing <a href="http://accessibility.kde.org/">accessibility</a> to the Unix and Linux desktop for people with disabilities. Read <a href="http://doc.trolltech.com/4.0/index.html">the online documentation</a> or jump to the <a href="http://www.trolltech.com/download/tech_previews.html">download page</a>.





<!--break-->
<p>Together with this tech preview, Harald Fernengel released a preview of a brand new set of <a href="http://www.freedesktop.org/Software/dbus">D-BUS</a> bindings. Using Qt's meta object system, broadcasting signals and invoking methods in other applications is now a breeze. D-BUS signals can be connected to any slot. No preprocessor and no special wrapper classes are necessary. You can download the library at the <a href="http://trolls.troll.no/~harald/dbus/">Qt D-BUS tutorial page</a>.</p>




