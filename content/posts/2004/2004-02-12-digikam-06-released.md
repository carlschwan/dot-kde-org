---
title: "Digikam 0.6 Released"
date:    2004-02-12
authors:
  - "Diginymous"
slug:    digikam-06-released
comments:
  - subject: "cool"
    date: 2004-02-12
    body: "export to HTML gallery feature?  easy way of renaming all the pics (eg: picture1.jpg, picture2.jpg) or keeping the original camera filenames if wanted?\n\nhot stuff!"
    author: "ac"
  - subject: "Re: cool"
    date: 2004-02-13
    body: "Maybe the renaming could be done trough KRename integration, like it is done in showimg. Would be really cool.\n\n"
    author: "dom"
  - subject: "Is Gphoto2 working with linux kernel 2.6??"
    date: 2004-02-12
    body: "Nothing much works here at all as far as digital cameras are concerned.\n"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Is Gphoto2 working with linux kernel 2.6??"
    date: 2004-02-12
    body: "I access my Canon S300 using Digikam 0.6, with Gphoto2 and the 2.6.2 kernel without any problems."
    author: "Deleon"
  - subject: "Re: Is Gphoto2 working with linux kernel 2.6??"
    date: 2004-02-12
    body: "My Canon EOS 10D is doing fine in Linux :) Haven't exactly tested it with Gphoto, cause the cam itself (believe it or not) only have a USB-1.1 interface. But I'm using a highspeed USB-2.0 card-reader for dumping the pictures. And mass-storge-units like card-readers are supported by gphoto (and by regular mount/umount for the sake of the matter). DigiKam-0.6 was _TRULY_ a great release. Go on and download, it's about _THIIIIIIIIIIIIS_ much better than 0.5 was."
    author: "Helge \u00d8yvind Hoel"
  - subject: "Re: Is Gphoto2 working with linux kernel 2.6??"
    date: 2004-02-14
    body: "Works perfectly with my Fuji   602\nyou must be doing somrthing wrong!!\n"
    author: "vernr"
  - subject: "Re: Is Gphoto2 working with linux kernel 2.6??"
    date: 2004-02-27
    body: "Hi,\n\nI can use gphoto2 with 2.6.2, .... but it is very strange. As soon as I call gphoto2 to list the camera files the kernel starts again in 'autodetecting' the USB device (It calls the binary configured in /proc/sys/kernel/hotplug). This is very anoying as it costs a lot of performance. Up to now I can not explain why this is the case ......\n\ncu"
    author: "Andreas Zickner"
  - subject: "Re: Is Gphoto2 working with linux kernel 2.6??"
    date: 2004-03-22
    body: "not for me.\nIt worked fine with 2.4.x and know (same syntax) it doesn't\nI've got this : \ngp_port_read: Connection timed out \nIO error\n"
    author: "max_cqft"
  - subject: "digikam is great"
    date: 2004-02-12
    body: "A few months ago I discovered digikam, and I've found it a very nice tool. I remember I was talking to a friend about how digital camera tools weren't very good on Linux (mostly because of the shoddyness of gtkam), and now I can't say thats the case anymore :)\n\nOh yeah, whatever happened to the kamera ioslave? I rememeber than in kde 2."
    author: "anon"
  - subject: "Re: digikam is great"
    date: 2004-02-12
    body: "> Oh yeah, whatever happened to the kamera ioslave? I rememeber than in kde 2.\n\nThankfully, it's called camera now. A little easier for new users to discover, and a little more professional name :)."
    author: "Matthew Kay"
  - subject: "Re: digikam is great"
    date: 2004-02-13
    body: "There were nice digital photo management tools for a long time now. Check out gThumb, for example of one."
    author: "anon2"
  - subject: "Why isn't this in KDE releases?"
    date: 2004-02-12
    body: "Why isn't Digikam in KDE yet? Why is it hosted on sourceforge? Is it that the\nmaintainer doesn't want or can't go with KDE release dates? Or is it that he\nhas not been invited to join into the project?\n\nAdvantages for digikam if hosted in KDE CVS: no need to look for translators\nof this fine software. It will be picked up automatically by the translation\nteams. It will be in 60 languages soonish, instead of 7 only..."
    author: "Kurt Pfeifle"
  - subject: "Re: Why isn't this in KDE releases?"
    date: 2004-02-12
    body: ">  Is it that the maintainer can't go with KDE release dates?\n\nkde-extra-gear is cool for stuff like that. "
    author: "anon"
  - subject: "Re: Why isn't this in KDE releases?"
    date: 2004-02-12
    body: "btw, kdeextragear has many dead apps - kreatecd, kchat, smssend, kaudiocreator (even if its in kdemultimedia - it sucks coz it cant grab on-the-fly - so people rather use audiocd:/ or cdparanoia | lame) \ni can't understand why they don't get removed\n\ninstead admins should add to it such apps as\ndigikam, kmymoney2, filelight"
    author: "Nick Shafff"
  - subject: "Re: Why isn't this in KDE releases?"
    date: 2004-02-12
    body: "The admins don't do anything against the will of the apps' developers."
    author: "Anonymous"
  - subject: "Can digikam be used as a general archiving tool?"
    date: 2004-02-12
    body: "Ie, if I go the analog route and digitalize my pictures with a scanner, can I use digiKam for archiving them on my computer?"
    author: "he-sk"
  - subject: "Re: Can digikam be used as a general archiving too"
    date: 2004-02-12
    body: "yes,\n\nthe digital camera interface is just an addon. there was a time when digikam used to be only a gphoto2 frontend. now its goals are ambitious and it strives to be a complete photo management application\n\npahli_bar"
    author: "pahli_bar"
  - subject: "Red Eye removal"
    date: 2004-02-12
    body: "I looked through the plugins to see if any of them can do red eye removal, but couldn't find anything. This would be a really great feature. The only tool for this on Linux that I have found so far, is a plugin for the Gimp that doesn't work very well."
    author: "Ralf"
  - subject: "Re: Red Eye removal"
    date: 2004-02-12
    body: "I can only second that. IMHO Red Eye Removal is _the_ killer feature for any private user who mainly uses it for his private digital pictures. It's the _only_ reason  I still process digital stills on a Windoze(tm) OS.\n\nApart from that, Kudos to the team for a great app!"
    author: "Dirk"
  - subject: "Re: Red Eye removal"
    date: 2004-02-12
    body: "this feature is planned as a plugin. expect it to be present in 0.7 release\npahli_bar"
    author: "pahli_bar"
  - subject: "Re: Red Eye removal"
    date: 2004-02-15
    body: "Please conform to expected standards and see to it that BSD will have a red-eye enhancer. Likewise, the removal should not be available on said machine. :)"
    author: "a.c."
  - subject: "Re: Red Eye removal"
    date: 2004-02-13
    body: "My killer plugin would be (purple) chromatic fringing removal.  I've got the steps to remove it in gimp down pat, but having it automated would be nice."
    author: "Corrin"
  - subject: "Re: Red Eye removal"
    date: 2004-07-24
    body: "I couldn't agree more. There are two features that I as a home user with a digital photo camera really need:\n1. easy renaming of a bunch of files such as present in gthumb and (as I recently learned) via the krename plugin.\n2. red eye removal (found no easy linux way to do this so far).\n\nWould be really great if Digikam would implement these features by default!\n\nCiao,\n\nWim"
    author: "Wim Coulier"
  - subject: "Re: Red Eye removal"
    date: 2004-07-24
    body: "Red Eye removal is now in the CVS version of digikam, along with a ton of other great features."
    author: "Ralf"
  - subject: "This is just excellent!"
    date: 2004-02-12
    body: "I have an older digital camera, a Kodak DC 215 Zoom. (I'll probably get a newer one soon.) Oddly it was well supported under gphoto but not ghpoto2. I just got the latest gphoto2 and built Digikam. Previously I had to load a program with a rather clumsy interface to download the images from a serial port. Because I didn't particularly like the image editing it provided and images often needed a little work I would then open Gimp, which drives me insane when I go to edit 20+ photos and my screen fills up with images. I just got the new Gimp and while it's improved it also switches all the dialog buttons around... so now I'm constantly cancelling operations and wondering why they didn't happen. (Right or left isn't as much right as playing three card monty with the ok and cancel buttons is just wrong.) Photos have been a lot less fun than they should be, and to top it all off I'm glad I only have an 8 MB memory card because gphoto is such a memory hog it can bring my system to it's knees and I can't even imagine 100 gimp windows open. \n\nDigikam to the rescue! Not only did it provide a lot more and work with my organized photo directories, it download and deleted images and did an excellent job of basic photo editing with a decent interface... all without screen clutter, excessive system load or other annoyances. Now it's time to try out the plugins. Even cooler... the plugins are kparts... I'm interested to see if I can run them in Quanta for web work too. ;-) My only dissapointment is that it downloaded images slower than gphoto. \n\nHats off to the digikam developers! This is an excellent program! BTW I agree with Kurt. This should be in KDE. Also, as a note to the developers, it's really great when you do a CVS update and find that one of the really sharp people in KDE has come through and cleaned up a spelling error or fixed some little thing before it even became a problem."
    author: "Eric Laffoon"
  - subject: "Re: This is just excellent!"
    date: 2004-02-12
    body: "> Even cooler... the plugins are kparts... I'm interested to see if I can run them > in Quanta for web work too. ;-) \nthe plugins are not exactly kparts (even though digikam uses the kparts interface to load and merge the plugins). so don't expect to be able to embed it in any application.\n\n> My only dissapointment is that it downloaded images slower than gphoto.\ndigikam is as fast as the commandline gphoto2 at downloading/deleting pictures from the camera, since both of them depend on the libgphoto2 library. i'm assuming you are comparing it to the gphoto1.\npahli_bar\n"
    author: "pahli_bar"
  - subject: "Re: This is just excellent!"
    date: 2004-02-14
    body: ">> My only dissapointment is that it downloaded images slower than gphoto.\n>  digikam is as fast as the commandline gphoto2 at downloading/deleting pictures from the camera, since both of them depend on the libgphoto2 library. i'm assuming you are comparing it to the gphoto1.\n \nThat's correct, and I might add a bit weird. However since I'm not worried any more about my system getting sluggish from excessive memory usage it's less of an issue. I can go do something else while I wait. I'll have a card reader probably soon and the additional support in digikam is great.\n\nIf I could make one request... I shoot my catnip under high pressure sodium lighting and it comes off yellow. I have a system of filtering I do with gimp to offset the discoloration. If you guys have filtering like this in digikam at sometime you will be my heros."
    author: "Eric Laffoon"
  - subject: "Re: This is just excellent!"
    date: 2004-02-13
    body: "I hate the flipping around of buttons in gnome.\nAnd you just can't talk about that with gnome people, they have this mantra that people read from left to right, so buttons should be from left to right in order of importance. It comes frm so called gui experts.\n\nThe thing that annoy me a lot is that:\n1/ I'm a personn of habits, like everybody is. So respect my habits, please.\n2/ I don't use my reading faculties to find a button, I use my geographical search habilities. Which means I look for a visual point to stop my gaze, and search for the button from here. Buttons being in the lower right part of the windows, i use the lower right corner of the window, and search buttons from here, meaning I look from right to left.\n3/ they keep on saying that persons with no computer use prefer that. Who in the western world is still a \"person with no prior computer use\", and is going to be on gnome first? This lack of pragmatism is incredible!\n\nThe thing that infuriates me the most is that whenever i try to just explain that to a gnome developper, he justs keeps on repeating the mantra that gui experts say it's best, without listening to me, a user. It infuriates me to the point that I havent even installed gnome on my desktop for one year. \nAnd, no, I havent ever been part of the kde/gnome trollwars. I used gnome till kde2, and switched on a pragmatical basis, just like I was using gnome during kde1 on a pragmatical basis (it kept crashing on my pc, I hadnt the skill at that time to find what could be wrong)."
    author: "djay"
  - subject: "Re: This is just excellent!"
    date: 2004-02-13
    body: "Yeah, the button order was one of the reasons I switched from GNOME pre-2.0 to KDE pre-3.0 (I was a diehard GNOME 1.0/1.2/1.4 user). I stayed with KDE for it's own merits. "
    author: "fault"
  - subject: "Better than camera:/ ?"
    date: 2004-02-12
    body: "I've used digikam some time, till I discoved that I had just to (after a quick configuration at the control center) access the url camera:/ to see my photos in konqueror. Drag'n drop them to another konqueror window. Now with some cool plugins like \"batch renaming\" and \"html album\" I'll take a look at it again."
    author: "Paulo Eduardo Neves"
  - subject: "Re: Better than camera:/ ?"
    date: 2004-02-12
    body: "if all you want to do is access the pictures from the camera, then camera:/, gphoto2 or gtkam might be a better option. digikam provides an integrated interface to address most of the needs of digital camera owners: organizing photos as albums, adding comments, setting dates, rotating images, raw image conversion, ..., well you get the idea. \npahli_bar"
    author: "pahli_bar"
  - subject: "Balance correction"
    date: 2004-02-12
    body: "Thank you for this great app. Would it be possible to create a plugin for balance correction, like this this one for the gimp?\n\nhttp://www.geocities.com/lasm.rm/wb2.html"
    author: "AC"
  - subject: "Re: Balance correction"
    date: 2004-02-13
    body: "added to my TODO list :)\npahli_bar"
    author: "pahli_bar"
  - subject: "OT: Vote for KDevelop at Unesco!"
    date: 2004-02-12
    body: "KDevelop is better than Eclipse! \nhttp://www.unesco.org/webworld/portal_freesoftware/Software/Development_Tools/Integrated_Development_Environment__IDE_/index.shtml\n"
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "KDevelop has refactoring facilities ? :-)\n\n(get real : kdevelop is a good IDE, but from the previous generation - compared to Eclipse or Intellij Idea, it's obsolete)."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "I'm real - is it necessary for a modern IDE to consume more then 240 Mb of RAM on Standby? Thus it makes real development impossible since there is no space left for helper apps or even the app you develop on. In addition its too slow for real fun. And considering the good, clean framework of kdevelop it's not impossible to see the missing features soon - in KDevelop."
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "You're seeing this from the hobbyist point of view. In the office, you just give 1Gig of RAM to your Java devs and off they go. The time gain recoups the investment after a week."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "No I'm seeing it from a software engineer's point of view. We switched a project from Java/AWT to C++/Qt since Java's performance and memory consumption is quite horrible and on the other hand programming with Qt is quite more productive than programming with Java. And our customers like to have their RAM for their data and not for the program alone too..."
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> We switched a project from Java/AWT to C++/Qt since Java's performance and memory consumption is quite horrible and on the other hand programming with Qt is quite more productive than programming with Java.\n\n\nFor GUIs, yes. Otherwise, no (and on WinXP Java GUIs run fine, it's only on Linux that they suck so badly - though that doesn't remove the fact that designing a GUI is easier with Qt than with Swing).\n\n\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "We're developing under XP for XP,Linux,Irix :-)\nJava's bad performance and memory consumption isn't bound to a plattform or a VM, its bound to the very basic principles of Java itself :-)"
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "I'm the only developer using Linux in the project, all my colleagues are running XP. We all have similar configurations. I can see the difference when they start Idea or Eclipse compared to my machine. Memory consumption is relatively identical (around 240Mb for Idea), but the reactivity difference is like night and day. Of course Java is much more resource hungry than C++, nobody disputes that. XP just copes with it better, UI-wise (or the Windows version of the JDK is better implemented, whatever)."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "Again: we're developing under XP!!!\n(At home I've running and developing for Linux pure!)"
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "I understood you the 1st time.\n\nTry running one of the Swing samples on Linux and XP, if you can't see the difference then you've got a problem with your setup.\n\nThat doesn't mean a large image processing Java app will fly on XP and crawl on Linux, there are other factors in that case, like huge mem consumption or programming mistakes. One of the main problems of Java, shared with C and C++, is that the obvious way of doing some things is rarely the right one."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "To make it clear: we switched an XP java project to C++/Qt despite of the poor performance. Having experiences in both languages now I can state that programming C++ in a modern way is at least as safe as programming in Java. Taking the good template support (we need fast templates, such the Generics introduced with Java1.5 aren't an acceptable solution) and valgrind into account we can deliver safer binaries as class files since you don't know what bugs the VM or HoSpot may introduce. "
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> Having experiences in both languages now I can state that programming C++ in a modern way is at least as safe as programming in Java.\n\nThen you need more experience :-).\n\nI'm sorry, but again you're considering only your own special case here. Statistically, programmers are way more productive in Java than they are in C++. That's a fact. You may be an exception (I am too, given that I'm much more familiar with C++/Qt than with Java's libraries), but I think your apparent dislike of Java prevents you from being objective. What you say is, basically, that \"programming in C++ while making no mistakes is at least as safe as programming in Java\".\n\nOf course you can reduce the risk of mem leaks and corruptions if you use the right data structures. The problem with C++ is that you have to build these structures first. Qt and the STL give you quite a bunch of them, but in any project you'll still have to add your own at some point. That won't replace a GC, and that's only one aspect of the overall problem.\n\n(and yes, generics in JDK 1.5 are a joke, everybody knows that)\n\n> we can deliver safer binaries as class files since you don't know what bugs the VM or HoSpot may introduce. \n\nOh, because you know what kind of bugs a compiler or a lib may introduce ? And valgrind works on SGI hardware too ?\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "Is it wrong to assume that a program which is clean (from valgrinds viewpoint) on a x86 wouldn't be clean on the Irix too?\nImportant for us are heavenly fast and powerful libraries as the Blitz++, especially with the integrated asserts for the debug version of our programs.\nAnd yes, I'm a bit annoyed from Java. Personal experience, I admit."
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> Is it wrong to assume that a program which is clean (from valgrinds viewpoint) on a x86 wouldn't be clean on the Irix too?\n\nCompletely wrong. First of all it means that you're assuming valgrind catches everything - it doesn't. Second, you're forgetting CPU specificities. Third, the OSes and libs are different too.\n\n\n> Important for us are heavenly fast and powerful libraries as the Blitz++\n\nThen you indeed couldn't have picked a worse choice than Java for your app. At best you could have tried a Java GUI over a C++ core, but with Qt that would not really have been worth the hassle."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "The current Eclipse C++ plugin has refactoring facilities? \nYou can program in pascal, ada, ruby, python, bash, etc with Eclipse? \nEclipse ships with support for subversion, cvs, clearcase, perforce?\nCode completion with support for Qt signals and slots?\n...and, and, and...\n\n--> http://www.kdevelop.org/index.html?filename=features.html\n\n(get real: you should do some research before you call something obsolete)"
    author: "Christian Loose"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "yes.. but kdevelop still doesn't have refactoring :("
    author: "fault"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "Why didn't you write one? :("
    author: "Christian Loose"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "Still no excuse for Guillaume Laurent for his annoying repeated eclipse trolling wrt KDevelop even though eclipse can't offer it for C++ either (thus is not even on topic)."
    author: "Datschge"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "Oh please, grow up. The original post said \"Kdevelop is better than Eclipse\". This is just a plain stupid assertion. At best you could say that KDevelop is a better C++ IDE than Eclipse, and even then I'm not sure a strict side-by-side comparison (that is, actually testing the advertised features) would confirm that. That doesn't mean KDevelop sucks, Eclipse just has a lot more resources devoted to it.\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "Sorry, but the one who needs to grow up is you. You are throwing around terms like \"get real\", \"previous generation\" and \"obsolete\" while attacking an awarded KDE application in a KDE forum, and you still expect to be taken seriously? Get real. =P"
    author: "Datschge"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "If you take this as an \"attack\", then yes, you need to grow up."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "No, I personally take it as a incredible waste of time on your part. I would rather like to see you developing Rosengarden, promoting its use and ideally leading an effort of building a decent audio and midi framework within KDE. The thread you started here with your unnecessary anti-KDevelop advertisement is both childish and trollish and not helping anyone here."
    author: "Datschge"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "It is not \"anti-kdevelop advertisement\". I consider kdevelop to be a good tool. Believe it or not I actually hope to use it instead of xemacs for Rosegarden development at some point. But to claim that it's better than Eclipse is not good publicity. Likewise we wouldn't claim that Rosegarden is better than Cubase, because that would be giving users false expectations.\n\nThat said, you're right, it is a childish waste of time on my part, but I'm not much in a coding mood today.\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "No, the C++ plugin for eclipse is lacking refactoring and I'd be amazed if it ever had it given how complex the issue gets for C++ (compared to Java).\n\nAs for the rest of the features, all those you're listing are hardly important (and I have to wonder how well tested they are, too), except for code completion, which is a must-have.\n\nUnderstand me, I'm not disparaging KDevelop, but to say it's better than Eclipse is just silly. Both simply don't play in the same league. Kind of like saying Rosegarden is better than Cubase, if you like :-).\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "> Understand me, I'm not disparaging KDevelop, but to say it's better than Eclipse is just silly. Both simply don't play in the same league.\n\nThis might be true for Java but when you see the whole package then KDevelop *is* better than Eclipse today.\n\nSo I would say choose the IDE that fits your needs. As a Java programmer I would probably use Eclipse too but as a C++ developer I choose KDevelop over Eclipse anytime."
    author: "Christian Loose"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-13
    body: "> As a Java programmer I would probably use Eclipse too but as a C++ developer I choose KDevelop over Eclipse anytime.\n\n\nLikewise, no arguing about that. But if Eclipse would get the same kind of features for C++ that it has for Java, I'd switch instantly."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "And if any of the current IDE's would get the features some of the Smalltalk/Lisp/Dylan IDEs have had for uears, I'd switch instantly too. But that ain't going to happen, not in my lifetime.\n\nThe point is that KDE is a C++ platform. The relative merits of IDEs are judged by their adeptness at handling C++ code, not Java code.\n"
    author: "Rayiner Hashem"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "Since Java is only usable for smaller programs which aren't used frequently 'cause of the bad performance and memory consumption you're right - KDevelop and Eclipse play in different leagues - KDevelop for real programs and Eclipse for small apps...\n\nPS: Isn't it funny to see how badly the Java guys try to integrate the \"uninteresting\" features of C++? Wonder how long it will takes before Microsoft cleans up Java's edge with C# when the SUN-down is perfect :-)"
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> Since Java is only usable for smaller programs which aren't used frequently 'cause of the bad performance and memory consumption you're right\n\n\nWhat's the color of the sky on your planet ? We all have Eclipse or IntelliJ Idea running for weeks on at my workplace (either on WinXP or Linux, though XP is much better for Java dev than Linux). The project I'm in has 40 programmers. Yes, we need half a gig and 2GHz P4 at least. So what, hardware is cheap, development time isn't. Such a setup is very common in the industry.\n\n> Isn't it funny to see how badly the Java guys try to integrate the \"uninteresting\" features of C++?\n\nJava made the mistake of oversimplifying things. MS seem to have reached a good balanced with C#. If the current market trends keep going, C# will accomplish what Java failed to do, that is turning C++ into a niche language, like C++ did for C."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "It's well-known that the wishes of the users grows more rapid than the capabilities of the hardware. Considering this, the combination of a cross-plattform toolkit like Qt - which reduces development time rapidly, IMHO even more than Java - and a performant language is the killer duo for the future :-).\nBesides: We're working in the medical imaging domain - and there we couldn't get enough power (there's always something you can do if you have some power left - use more accurate models,...) and free memory :-)\nAnd it's funny to see how much HW power you need under Java to achive the same results as with a P5 machine @200 Mhz :-)\n\nSorry, but you're a bit out-dated :-)"
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "I like C++ better too. Java seems to be a step backwards in technology. C++ is simply the most advanced language today. C# has nothing that C++ didn't already have for 10 years. C++ is a free, but Java and .net are proprietary platforms. Java is not platform independent as many like to think, but ties you tightly to the Java platform. Java can't interact with any other languages and has only a sucky C interface. Java is totally pathetic if you look at it closer.\nC#/.net is only little better. At least the templates are real templates there, but again its not cross platform as you get hooked up to M$ which nobody really wants. at least C++ can be a valid .Net language. \nI really wished someone would continue the c++ VM that was started once. With GC becoming a c++ standard in 2005 C++ will stay the coolest language anyway. No need for Java/C# crap"
    author: "Nick"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> C# has nothing that C++ didn't already have for 10 years.\n\nShow me introspection, delegates, serialisation, GC, class loading, remote method calls in C++ ? You're going to reply to me that it's all there if you use the right libraries, which is precisely the point : C# has all these in standard. Please, you're just acting like the Gnome zealots who used to argue that C++ had nothing you could already do in C, lacking both objectivity and technical knowledge."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-16
    body: "Just that C++ has actually more language features than C#. Delegates is just signals/slots light. Where is multiple inheritance in C#? MI is much more useful than introspection...\nA Gnome zealot I am certainly not! But I grant you that C# is better than Java. If it only had more C++ features and wouldn`t depend on a VM...\n"
    author: "Nick"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-16
    body: "> Just that C++ has actually more language features than C#\n\na) few people see this as an advantage\nb) the question is *which* features\n\n> Where is multiple inheritance in C#?\n\nYou generally don't need it (not always, but in most cases).\n\n> MI is much more useful than introspection...\n\nOh absolutely. And a spoon is so much more useful than a screwdriver.\n\n> A Gnome zealot I am certainly not!\n\nNo, but you think like one, even if it's on different subjects."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-16
    body: ">Oh absolutely. And a spoon is so much more useful than a screwdriver.\n\nThats exactly the point I am making. I use a spoon several times per day, a screwdriver not very often.\n \n> b) the question is *which* features\n\noh my god! its like you are reading my mind :). .Net does a few things different than C++ and has a feature more where c++ lacks one and the other way around, but in the end they are pretty close! And C++ development hasnt stopped. GC among other things will come soon. (inofficially its already there...)\n\nthe step from C -> C++ was a huge innovation. C++ -> C# is only a little step. For the free software community it would be much better to build on C++ than on C#, and guess what, my favorite Desktop KDE does just that :)\n\n \n"
    author: "Nick"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-16
    body: "Out of curiosity, could you detail your work experience in C++ ? How long have you been using it ? What kind of software have you written ? What C++ books have you read ?\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-16
    body: "Hi Guillaume!\nDo you think anybody except us is still reading this? Why don't you try to catch me on #pclasses on freenode.net :)\n\n"
    author: "Nick"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-16
    body: "I'd rather keep it here. So ? What's your C++ experience ?\n\nAnd also, I suggest you take a look at these excellent series on interviews between Anders Hejlsberg, Bill Venners and Bruce Eckel :\nhttp://www.artima.com/intv/csdes.html\n\nEspecially this one, the part entitled \"Components are first class\" :\n\nhttp://www.artima.com/intv/simplexityP.html\n\n"
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "GC would hopefully only an option (e.g. memory manager in the standard libs) since GC is one of many possibilities to do the trick and not the right choice in all scenarios. Mathematic toolboxes are using memory pools, RT applications allocate and deallocate by programmers command. It's one of Java's shortcoming to be fixated on one memory managment system. \nVirtual machines are another problem since they cost a lot of resources and performance. Imagine a KDE where each permanently running program uses their own VM! Besides this, the VM concept prevents you from using the full power of your machine when you need it (e.g. for multimedia, games etc.) or using your machine in a more economic manner (e.g. OS throttling down the CPU if the full power isn't required - see the impact on the uptime of laptops!).\nAll in all I think that Qt has the correct answer - an intuitive cross-plattform toolkit for rapid development of performant programs easily deployable on different plattforms."
    author: "Ruediger Knoerig"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> We're working in the medical imaging domain\n\nThat's a very narrow domain with extreme performance requirements. It's quite obvious that Java is not suitable for your task. However your experience can't be generalized to the rest of the industry."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: ">What's the color of the sky on your planet ? \n\nObviously another than on yours:\n>that is turning C++ into a niche language, like C++ did for C.\nStatistics? C a niche language? Probably like COBOL, right?\nI actually read (some time last year) that there gets more new COBOL code written than java, for example.\nYour prediction is just naive imho, the industriy doesn't change to fancy new languages, when there are millions of working lines of code and they won't in the future, especially not to .net, is my prediction ;)."
    author: "Rischwa"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "> Your prediction is just naive imho, the industriy doesn't change to fancy new languages, when there are millions of working lines of code\n\n\nThese are called \"legacy applications\". That \"new COBOL\" is called \"maintenance\". It's true that some industries still haven't changed to more current languages (oil is one example if I recall correctly), for many reasons. But that doesn't mean they're not looking at what's going on and not considering it eventually."
    author: "Guillaume Laurent"
  - subject: "Re: OT: Vote for KDevelop at Unesco!"
    date: 2004-02-14
    body: "KDevelop is primarily a C++ IDE. The language is a bitch to parse (the number of correct C++ parsers in existence can be counted on one hand) and its syntax and expression flexibility make refactoring non-trivial. So of course KDevelop doesn't have refactoring --- its not a Java/Smalltalk IDE!\n"
    author: "Rayiner Hashem"
  - subject: "Very nice program"
    date: 2004-02-13
    body: "It's now my favourite Linux image viewer, it's no acd see (yet) but it's veryu good, I'm not even using Mosfet's Pixie Plus anymore."
    author: "Alex"
  - subject: "Re: Very nice program"
    date: 2004-02-13
    body: "From what I remember, Gwenview is modeled after Acdsee.\n\nIs there some overlap here, or are Digikam and Gwenview complimentary?"
    author: "Justin Karneges"
  - subject: "Re: Very nice program"
    date: 2004-02-13
    body: "comparing digikam and gwenview to commercial applications, i would say gwenview is similar to acdsee and digikam is more like iphoto. digikam's main focus is end user ease of use. (but we are not skimping on the features part :))\npahli_bar"
    author: "pahli_bar"
  - subject: "Non GPhoto cameras !?!?!"
    date: 2004-02-13
    body: "How does it handle cameras that uses the usb mass storage interface instead of gphoto?\n\nOr is this program not ment for newish usb storage type of cameras? Which I must say is a pity,\n\n\n   Digiboy"
    author: "digiboy"
  - subject: "Re: Non GPhoto cameras !?!?!"
    date: 2004-02-13
    body: "Firstly, GPhoto supports these types of cameras, and secondly, Digikam isn't tied to GPhoto; it can manage photos from anywhere.  No need to use three exclamation points and two question marks."
    author: "Spy Hunter"
  - subject: "Re: Non GPhoto cameras !?!?!"
    date: 2004-02-13
    body: "from the faq:\nhttp://digikam.sourceforge.net/docs.html#q3\npahli_bar"
    author: "pahli_bar"
  - subject: "Well done!!"
    date: 2004-02-13
    body: "Very nice!!\n\nIt's actually the first appplication that allows me to pull pictures of my Creative PC_CAM 300! The other GPhoto2 frontends can detect it but never find any pictures...\n\nDigikam is a great help for me (one less reason to dualboot....)\nTogether with the spca50x driver, I have (finally!!) a fully working webcam-with-photo-capability. Life is grand.\n\nI installed DigiKam from Cooker on Mandrake 10-b2. \n\nThe only problem remaining is that I dont get thumbnails of the pics in the camera. (cli has a line about \"unable to show JPEG\" or so...)"
    author: "UglyMike"
  - subject: "Re: Well done!!"
    date: 2004-02-14
    body: "Be sure that both gphoto and libgphoto are compiled with EXIF support.\nNo EXIF, no thumbnails...\n"
    author: "Bastian"
  - subject: "Digikam is a wonderful application"
    date: 2004-02-13
    body: "I want to thank you the developers for this application. I use it with my Canon IXUS 400 and it works wonderfully. I archive my photographs in collections with Digikam, and since it supports EXIF information I know how and when the photographs were performed.\n\n I have one question which has already been done by others, but not answered:\n - Why is digikam not included in KDE?. As others have said if the developers want to keep their release cycle, kdeextragear is the module for that.\n\nI wait impatiantely for that planned plugin for removing red eyes. This is something that Digikam needs.\n\nThanks again, and think about including Digikam in the KDE CVS."
    author: "Pablo de Vicente"
  - subject: "Icons"
    date: 2004-02-13
    body: "Just installed Digikam from the SuSE 9.0 APT Repository. Is it just me or are all of the toolbar icons in the main application window missing?\n\n(Icons ARE displayed in the camera window.)\n\n"
    author: "Anonymous"
  - subject: "Re: Icons"
    date: 2004-02-13
    body: "Just downloaded the latest source tarball from the digikam site and copied the files in pics/ to the global share/apps/digikam/icons/hicolor/. Works fine now.\n"
    author: "Anonymous"
  - subject: "Great program!"
    date: 2004-05-06
    body: "I'm using Digicam quite a bit now.  A couple features would be very useful.\n\n1. Automatic rotation using the EXIF information from the camera.  My canon PowerShot SD100 encodes the camera orientation in the picture.\n\n2. More features for exporting albums, i.e. multiple pages, background effects, like what PixiePlus can do."
    author: "Aaron Williams"
---
After nearly one and half years of development <a href="http://digikam.sourceforge.net/">Digikam</a> 0.6 and its <a href="http://digikam.sourceforge.net/plugins.html">plugin package</a> have been released. Digikam is a simple <a href="http://digikam.sourceforge.net/screenshots.html">digital photo management application</a> which makes importing and organizing digital photos a "snap". The photos can be organized in albums which are automatically sorted chronologically. An easy to use interface is provided to connect to your camera and preview images and download and/or delete them. Behind the scenes Digikam utilizes <a href="http://gphoto.org/">gPhoto2</a> to access your camera if it has no normal file system.



<!--break-->
<p>Other new features of this version are:
<ul>
<li>Simple Plugin architecture so that new plugins can be written for added functionality</li>
<li>Comments can be added for photos which are shown in the thumbnail display (along with file size and file modification date)</li>
<li>XML is used for saving the comments</li>
<li>Four different thumbnail sizes</li>
<li>EXIF information viewer</li>
<li>Lossless JPEG rotation support</li>
<li>Fast photo viewer/editor with keyboard shortcuts and basic image editing features like rotation, cropping, gamma/brightness/contrast correction (all without losing EXIF information)</li>
</ul>


