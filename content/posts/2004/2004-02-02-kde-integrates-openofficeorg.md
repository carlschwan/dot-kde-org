---
title: "KDE integrates OpenOffice.org"
date:    2004-02-02
authors:
  - "fmous"
slug:    kde-integrates-openofficeorg
comments:
  - subject: "kio integration"
    date: 2004-02-02
    body: "On linux we already have some kind of kde ioslave integration into OOo (opening remote files via kioslaves from konqueror in OOo). Saving works also.\nI'd consider it beta quality:\n\nhttp://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "I forgot to mention:\n\nIMO good KDE integration of OOo is one of the most important projects for the future of KDE.\nVery cool work ! :-)\n\nAnd thanks to SUSE !\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "hmm yes, i'm very happy to hear that they employed him 1 week before. it seems like they still support kde - even if novell already owns ximian ..."
    author: "jo"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "> even if novell already owns ximian\n\nXimian is going to develop for KDE."
    author: "Asdex"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "Reference for this claim?"
    author: "Anonymous"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "I'm pretty sure it was a joke.."
    author: "another ac"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "http://news.com.com/2100-7344_3-5151359.html?tag=nefd_top\n\nIf ximian is now being split up and Richard is put in charge, who knows what will happen.."
    author: "jmk"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "wow, three articles, all today :) (I guess i'll have to take that \"it's a joke\" comment back)\n\nFWIW, there were some articles saying nat & miguel were taking over desktop stuff at novell - but i think this trumps them. Either way, it's gonna make a nice flame-fest over at slashdot when it hits  ;)"
    author: "another ac"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "Maybe, but Markus Rex from Suse now is head of Novell's Linux-Desktop activities.\nhttp://www.heise.de/newsticker/meldung/44246"
    author: "Asdex"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "Miguel and Nat are not anymore in charge of it? :-)"
    author: "Anonymous"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "Just from http://www.suse.de/en/company/press/press_releases/archive04/suse_bu.html\n\n\"Rex, previously SUSE's vice president, Research and Development, will assume his new duties immediately. As general manager of the SUSE LINUX business unit, reporting to Chris Stone, Novell vice chairman - Office of the CEO, Rex will lead the development of SUSE LINUX, from the DESKTOP to SERVER, and will work with other Novell business units to deliver a complete Linux solution stack. Rex will also assume responsibility for Novell's Linux DESKTOP activities.\"\n\nWell, usually I trust information that comes from the source more than some people who need to comment on slashdot just to gain attention, so ... ;-)\n\nTackat"
    author: "tackat"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "The news.com.com.com.com article said Markus Rex was becoming general manager of Novell Europe?"
    author: "Roberto Alsina"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "This seems to be wrong, he gets General Manager of Novell's SUSE LINUX Business Unit: http://www.suse.com/us/company/press/press_releases/archive04/suse_bu.html"
    author: "Anonymous"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "That, too.\n\n->http://www.suse.de/en/company/press/press_releases/archive04/suse_bu.html"
    author: "Asdex"
  - subject: "Re: kio integration"
    date: 2004-02-02
    body: "Well, actually it makes sense.\n\nWith the Qt-Gtk style and the possibility to use KDE file dialogs in Gtk apps, is is in fact now possible to make KDE-apps, in Gtk. Weird, huh?. This is because they will look (and probably soon feel) like any other KDE-app. \n\nBasically, if you're on KDE, you have the option of developing in either toolkit. More options for you in many aspects. Why choose less options?\n\nAm I wrong?"
    author: "OI"
  - subject: "KOffice"
    date: 2004-02-02
    body: "\"First of all, the KDE NWF and KDE vclplug have to be stabilized. Then I can integrate KIO and KDE dialogs to OOo and continue with further efforts, be it KParts, Kiosk or other KDE features. I am afraid it won't happen before 2005.\"\n\nWhat value does KOffice offer over such an ambitious and extensive hack?"
    author: "ac"
  - subject: "Re: KOffice"
    date: 2004-02-02
    body: "Speed, usability(?),  better frame-support (kword), and more little details...\n"
    author: "Birdy"
  - subject: "Re: KOffice"
    date: 2004-02-02
    body: "\"What value does KOffice offer over such an ambitious and extensive hack?\"\n\nThe day OOo integrates with KDE (almost) as good as KOffice, KOffice might be (almost) as usable features-wise. So the answer is about the same as to why we need khtml when we got gecko already. With KOffice vs OOo the situation is somewhat the same as with Gecko vs khtml - one featureful, but big and non-initiutive solution, and one not as featureful, but better integrated and lighter. If you can go with the second, it will probably do better. "
    author: "ealm"
  - subject: "Re: KOffice"
    date: 2004-02-02
    body: "Just donwload 'm both (KO 1.3 and OOo 1.1) and compare..."
    author: "cies"
  - subject: "Re: KOffice"
    date: 2004-02-02
    body: "Besides speed, lower resource usage, cool features (like KWord's frames) and native KDE integration, KOffice has Kivio, Kugar and Kexi. Greater use of components (e.g. kchart and kformula) and libraries encourages third party development; I've already seen a few KOffice-based apps out there and expect to see more in the future. And if/when KPlato, Krita and Karbon get to production quality, we can add a few more things to the pile."
    author: "Aaron J. Seigo"
  - subject: "Re: KOffice"
    date: 2004-02-02
    body: "Personally, I see it the other way around. As soon as KOffice supports the OpenOffice.org file formats and its good filters, what value does OOo offer over KOffice anymore?"
    author: "Arend jr."
  - subject: "Re: KOffice"
    date: 2004-02-02
    body: "Aside from all the other good answers here... OOo offers solid basic applications primarily focused on word processing and spreadsheets with secondary apps in presentation, drawing and from there it's less worth mentioning. It's good for migrating from MS Office. On the down side, even when it integrates with KDE it's still cumbersome, slow and a lot more than people need. Also it doesn't have some of the other apps mentioned. Fortunately we won't have to select one or the other because we will be able to choose based on our personal taste and the task at hand.\n\nIf you want to ask where the future lies though Koffice has huge potential. It's small, fast, light and innovative. It's hard to say what the future holds but Koffice is compelling. With a KDE OOo and Koffice we would be able to offer something there is all to little of on the office landscape today... solid choices."
    author: "Eric Laffoon"
  - subject: "Re: KOffice"
    date: 2004-02-03
    body: "I just hope that all of this debate ends up with applications that integrate well together.\n\nI'm talking about copying and pasting a Kivio diagram seamlessly into an Impress presentation... You know a bit like Windows - dare I say without being shot.\n\nI believe the K suite offers some excellent apps, but I believe OOo is going to be the mainstream wordprocessor, spreadsheet and presentation tool, specifically in large installations in government and corporations -- not least of all due to those organisation's Microsoft Office legacy -- and the huge punt to support OOo by the likes of Sun, IBM and Novell.\n\nAs such the K apps need to work well with OOo to gain broad acceptance from lesser mortals (ie. those who don't read news on the kde.org site ;-)\n\nMy two cents worth anyway..."
    author: "alek"
  - subject: "Re: KOffice"
    date: 2004-02-04
    body: "But eric, the left toolbar is very confusing"
    author: "gerd"
  - subject: "Fantastic"
    date: 2004-02-02
    body: "This project looks really, really nice.  I'm thrilled that Jan was hired by SuSE and is still able to work on this project.  \nI'm so very excited for this release and can't wait to test and report bugs once it's ready :)\n\nAnd thanks a bunch for the interview Fab!\n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: Fantastic"
    date: 2004-02-02
    body: "thanks Kenny \n\nCiao \n\nFab"
    author: "Fabrice Mous"
  - subject: "KDE related OOo bug (reported)"
    date: 2004-02-02
    body: "Since we're talking about KDE/OOo integration I thought I'd let you know abuot this OOo bug I reported that affects KDE users.\n\n\"DnD pictures from Konqueror to OODraw problem.\"\nhttp://www.openoffice.org/issues/show_bug.cgi?id=23526\n\nShould this kind of thing be reported to the KDE.OpenOffice.org project?\nI know it doesn't really fit any of the projects, but on the other hand, people with experience in both KDE and OOo may be a good help in its resolution, and it does have an impact in OOo usability under KDE.\n\nJ.A."
    author: "Jadrian"
  - subject: "Re: KDE related OOo bug (reported)"
    date: 2004-02-03
    body: "No, this kind of bugs should be reported to the respective OOo components, e.g. component \"Drawing\" in this case (it is correct in your bug report :) ). But if you encounter for example a button misbehaviour when using KDE NWF, then report it to the \"KDE\" component.\n\nJan"
    author: "Jan Holesovsky"
  - subject: "Re: KDE related OOo bug (reported)"
    date: 2004-02-03
    body: "Ok, thanks!"
    author: "Jadrian"
  - subject: "qt is cool"
    date: 2004-02-02
    body: "All big projects are going to use qt\n\nftp://ftp.trolltech.com/qt/qtmozilla/README\n\nother interesting link:\nhttp://www.telegraph-road.org/writings/oo_in_any_language_myth.html"
    author: "Nick Shafff"
  - subject: "Re: qt is cool"
    date: 2004-02-02
    body: ">> All big projects are going to use qt\n>>\n>> ftp://blah...\n\nDon't talk shit please.\n\nThe date of that file is 20-10-1998.\nMozilla is not going to use Qt.\n"
    author: "bleb"
  - subject: "Re: qt is cool"
    date: 2004-02-02
    body: "> The date of that file is 20-10-1998.\n\nTrue, it's been a long time since someone worked on Mozilla Qt support, it has even been removed from trunk CVS as it was unmaintained.\n\n> Mozilla is not going to use Qt.\n\nThat's not said. It's true that currently there's no support, but it can be re-established any time if someone does work on it.\nMozilla has good ability to use different toolkits (currently Xlib, GTK+ and GTK+2 are supported and in CVS, GTK+ being the default used one), and there's nothing against adding Qt to that (as long as the port is maintained). We (the Mozilla community) would be very glad to see better KDE integration, so if someone is out there and wants to help, it's appreciated (I don't want to discourage you to help on OOo intergration though ;-).  Look into http://bugzilla.mozilla.org/show_bug.cgi?id=140751 (+dependencies) and the netscape.public.mozilla.at newsgroup as staring points.\n\nIt would be really nice to see much better integration of major apps as OOo and Mozilla and KDE (besides having e.g. KOffice and Konqui browser as well). The Open Source communites are great, let's make them really come together!"
    author: "KaiRo"
  - subject: "Re: qt is cool"
    date: 2004-02-02
    body: "er, sorry, the newgroup name ends in .qt, not .at, of course"
    author: "KaiRo"
  - subject: "Re: qt is cool"
    date: 2004-02-12
    body: "anyhow, i'm not going to use mozilla untill\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=105843\nget fixed"
    author: "Nick Shafff"
  - subject: "Kopenoffice for Macos X"
    date: 2004-02-02
    body: "Will it be possible to run Kopenoffice through KDE instead of X11 on Mac? "
    author: "geert"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-02
    body: "Probably, with some work, since complicated KDE apps already work with the native OS X Qt.  It would be amusing if the KDE port of OOo finished before the native OS X port did.\n\nIn general, Qt on OS X needs a lot of work though, and apps produced with it look and feel really ugly and non-native.  As I was looking at some of the OOo screenshots I realized that there were many GUI 'mistakes' in them, but I'm just not trained to look at tiny pixel errors in KDE apps.  Whereas when Safari 1.2 makes the search field two pixels taller and insets the search text a bit, I can tell immediately. I guess the Mac just tends to inspire that kind of attention to detail, in me at least."
    author: "foo"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-02
    body: "> Qt on OS X needs a lot of work though, and apps produced with it look and feel really ugly and non-native.\n\nDid you try Qt 3.3 Beta 1? From its changelog: \"Numerous visual improvements.\""
    author: "Anonymous"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-03
    body: "> In general, Qt on OS X needs a lot of work though, and apps \n> produced with it look and feel really ugly and non-native\n\nI'm aware of two 'look' problems with Qt on OS X, one is the pulsing color of Qt/Mac default buttons is slightly different from other OS X apps. The other is that Panther style tabs aren't supported, (this is an appearance manager limitation other applications are affected by it also).\n\nIf you can provide specific look criticisms (screenshots could help) then I'm interested.\n\nIn general Qt doesn't draw widgets on the Mac, instead it delegates this responsibility to OS X via the Appearance Manager API. This API has the limitations mentioned above.\n\nPlease note that Qt/Mac applications ported from other OS' won't always look native, they might use Windows toolbar icons, fixed layouts with spacing and placement inconsistent with the Aqua style guidelines, etc.\n\nDon\nKDE Software Engineer,\nQt/Mac Software Engineer,\netc.\n"
    author: "Don"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-03
    body: "> If you can provide specific look criticisms (screenshots could help) then I'm interested.\n\nforgive me if any of these are kde or apps bugs...\n\n- the toolbar dragger should not appear on OSX.. usually toolbars can be moved throughb context menus (in KDE as well)\n\nthe toolbar button seperators don't look right on http://ranger.befunk.com/screenshots/qt-mac-apps/flashkard.png .. see http://ranger.befunk.com/screenshots/qt-mac-apps/kfind.png how Terminal.app does it..\n\n- the Splitters aren't drawn OSX-y..\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kdict.png\nversus http://www.cocoatech.com/images/demo/customize.jpg\n\n- tab text not aligned well\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kcontrol.png\n\n- combo text not aligned well\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/klettres.png\n\n- kmines bug?\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kmines.png\n\n- the statusbar frame collides with the dragger... kstatusbar bug?\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kjumpingcube.png\n\n- more text alignment problems.. this time in listview\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kmail.png\n\n- tab needs to be moved up one or two pixels imho.. or blended in\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kopete.png\n\n- text not clipped in combo?\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kbruch.png\n\n- konsole tabbar bug?\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/konsole.png\n\n- khexedit find button bug\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/khexedit.png\n\n- calander navigation buttons not centererd\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/korganizer.png\n\n- bottom line edit not panther like\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kcharselect.png\n\n- stripes not panther like\n\nhttp://ranger.befunk.com/screenshots/qt-mac-apps/kbruch.png\n"
    author: "anon"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-03
    body: "Thanks, for the more detailed level of feedback.\n\n> - the toolbar dragger should not appear on OSX.. usually toolbars can be\n> moved throughb context menus (in KDE as well)\n...\n> the toolbar button seperators don't look right on \n> http://ranger.befunk.com/screenshots/qt-mac-apps/flashkard.png .. see \n> http://ranger.befunk.com/screenshots/qt-mac-apps/kfind.png how Terminal.app\n> does it..\n\nI don't think there is a single standard look for toolbars on OS X. Different Apple applications choose different toolbar styles, and third party Apps use different looks again. I don't think the Terminal.app style toolbar is appropriate for KDE apps on OS X, I think a TextEdit.app style toolbar, with smaller toolbar widgets, is more appropriate for a KDE ToolBar style for OS X.\n\nBut yes I can't see any OS X app with a toolbar dragger, that could go. The toolbar button separators I think may be part of the appearance of KToolBar rather than QToolBar. In which case fixing that would require a KDE style rather than Qt style change.\n\n> - the Splitters aren't drawn OSX-y..\n\nLooks valid.\n\n> http://ranger.befunk.com/screenshots/qt-mac-apps/kdict.png\n> versus http://www.cocoatech.com/images/demo/customize.jpg\n> \n \nUnfortunately I don't follow here.\n\n> - tab text not aligned well\n>\n> http://ranger.befunk.com/screenshots/qt-mac-apps/kcontrol.png\n> \n> - combo text not aligned well\n>\n> http://ranger.befunk.com/screenshots/qt-mac-apps/klettres.png\n \nYes, I see several of the issues you've mentioned are of a text not aligned well nature. Further investigation is required to determine if these are KDE, Application or Qt problems. Sam might be a better judge but personally I can't tell without investigating these issues on a case by case basis. Each case like this has to be examined to see if it can be reproduced in a small Qt program (indicating Qt bug), or whether the problem is in the application (KDE/App bug). \n\nAs a TT guy I'm inclined to assume they are KDE/App bugs until proven otherwise.\n\nAs a KDE guy I hope some of those alignment issues are actually Qt bugs, so they can be fixed in one place.\n\n> - kmines bug?\n> http://ranger.befunk.com/screenshots/qt-mac-apps/kmines.png\n\nNormal OS X buttons come in one of two sizes, anything else is a style guide violation. So that app has to change here. (Resize the pixmap smaller, use a toolbutton instead of a normal button, or find an entirely different UI solution).\n \n> - the statusbar frame collides with the dragger... kstatusbar bug?\n\nSure kstatusbar style issue. On OS X status bars can't have frames like that as it collides with the dragger. I suspect a KDE OS X frameless status bar style is the solution here.\n \n> http://ranger.befunk.com/screenshots/qt-mac-apps/kjumpingcube.png\n> \n> - more text alignment problems.. this time in listview\n> \n> http://ranger.befunk.com/screenshots/qt-mac-apps/kmail.png\n> \n> - tab needs to be moved up one or two pixels imho.. or blended in\n\nAlignment problems, these need to be investigated on a case by case basis. \n\n> http://ranger.befunk.com/screenshots/qt-mac-apps/kopete.png\n> \n> - text not clipped in combo?\n\nNot sure what this one refers to.\n \n> http://ranger.befunk.com/screenshots/qt-mac-apps/kbruch.png\n> \n> - konsole tabbar bug?\n> \n> http://ranger.befunk.com/screenshots/qt-mac-apps/konsole.png\n> \n> - khexedit find button bug\n> \n> http://ranger.befunk.com/screenshots/qt-mac-apps/khexedit.png\n> \n> - calander navigation buttons not centererd\n> \n> http://ranger.befunk.com/screenshots/qt-mac-apps/korganizer.png\n \nAlignment issues. Lots of them. Need case by case examination. (I'm a little out of the loop, there may be alignment fixes in Qt 3.3 I'm not aware of).\n\n> - bottom line edit not panther like\n> \n> http://ranger.befunk.com/screenshots/qt-mac-apps/kcharselect.png\n\nYou mean it's too short vertically, could be a fixed height in the app.\n \n> - stripes not panther like\n> \n> http://ranger.befunk.com/screenshots/qt-mac-apps/kbruch.png\n\nAh, I can't see anything wrong there, I guess I don't know what I'm looking for well enough. I do recall a stipes on panther fix being made for the latest release.\n\nThe splitter not being OS X style is a valid criticism. The other issues I think require KDE style or application fixes, or more investigation before they can be considered Qt bugs.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-03
    body: "> The splitter not being OS X style is a valid criticism. The other issues I think require KDE style or application fixes, or more investigation before they can be considered Qt bugs.\n\nThanks for looking :)\n\ngreat job for TT on Qt/Mac overall though"
    author: "anon"
  - subject: "Re: Kopenoffice for Macos X"
    date: 2004-02-02
    body: "not with NWF (OpenOffice 1.1), but possible with native Qt (OpenOffice 2.0).. then again, a native aqua OpenOffice is planned, but not until 2006 ( :( )"
    author: "dood"
  - subject: "Alternative Funding"
    date: 2004-02-02
    body: "It is good that Suse is funding this project, but I was wondering about other people who may be interested. I would have thought Lindows would have been interested in this project greatly.\n\nGiven that this is a long-term project and Suse may not be around that long (we can't be naive to the possibility - there will be a large amount of pressure to stop this eventually) projects like this need to continue."
    author: "David"
  - subject: "Re: Alternative Funding"
    date: 2004-02-02
    body: "> I would have thought Lindows would have been interested in this project greatly.\n\nWhat's the problem? Lindows can still contribute to this project and speed up its development. There is no difference to e.g. both SUSE and Lindows supporting and promoting ReiserFS."
    author: "Anonymous"
  - subject: "KDE 3.2"
    date: 2004-02-02
    body: "Now you've done it.  I'm gnawed off my own leg in anticipation.  Are you happy now?"
    author: "ac"
  - subject: "Re: KDE 3.2"
    date: 2004-02-02
    body: "No, let's wait until tomorrow what you make next."
    author: "Anonymous"
  - subject: "Re: KDE 3.2"
    date: 2004-02-03
    body: "They have delayed KDE3.2 for at least five months pal. Then after that, at least 5 more release canidates. Sorry about your limbs."
    author: "ac"
  - subject: "Re: KDE 3.2"
    date: 2004-02-03
    body: "You must be talking about KDE 3.1."
    author: "Anonymous"
  - subject: "(K) OOo 2.0"
    date: 2004-02-02
    body: "HAHAHAHAHAAHA!!!\n\ncan't wait for that release..\n\nif OO is Qt, KDE based, ... that means the application will be FASTER !!!!\n\ni dont use OO 1.1, because it takes 10 mins to open on my P2-600\n\nlong live to KDE.\n\n"
    author: "Mathieu"
  - subject: "Re: (K) OOo 2.0"
    date: 2004-02-02
    body: "I don't think it will be faster.\nMaybe it draws faster (if at all).\nBut it surely won't start up faster.\nMore certain is the other way round, because it now needs to load itself and Qt as well.\nBut Qt is quite fast, so you won't notice this additional delay.\nWhich only occurs when no other Qt app is running..."
    author: "SegFault II."
  - subject: "Re: (K) OOo 2.0"
    date: 2004-02-02
    body: "OOo Qt won't have to load the VCL toolkit, which is a pig.  So yes it will have to load Qt but it won't have to load VCL."
    author: "ac"
  - subject: "Re: (K) OOo 2.0"
    date: 2004-02-02
    body: "VCL ??? Borland VCL ?? \ni did not know...\n\nwell, yeah, if they just add Qt, that won't be faster, but if don't use anything else than Qt anymore... that will me darn faster ;)\n\n"
    author: "Mathieu"
  - subject: "Re: (K) OOo 2.0"
    date: 2004-02-03
    body: "> But it surely won't start up faster.\n> More certain is the other way round, because it now needs to load itself and Qt as well.\n\nCall me ignorant (well, I am, it's been years since I programmed anything...), but couldn't Qt be made to load upon KDE start? (Honestly, I thought it already worked this way, since it's, duh, common to all KDE apps)\n\nJust my curiosity.\n\nAgain.\n\nThx.\n\nPS: BTW, great work, Jan Dewd, you got a really nice idea! And one of those screenshots has the menu option \"Fail\". Almost killed me! :-)"
    author: "Gr8teful"
  - subject: "Re: (K) OOo 2.0"
    date: 2004-02-03
    body: "OOO too slow at launch:\nI had the same problem and i've found that deleting the file \nOOinstall/share/psprint/pspfontcache was THE solution.\nNote that this problem was with the 1.0.1 version...\nafter 2 ou 3 launches this files was 3 ou 4 megs........\nso i made a little script which erases this files after quitting\nOO\n\n\n\nDjam\u00e9\nps : I should have fill a bug report but I hate those bugzilla apps.....\n"
    author: "djam\u00e9"
  - subject: "Re: (K) OOo 2.0"
    date: 2004-02-04
    body: "Not that much faster. Bit less memory taken, better response times hopefully.  \n\n10 minute startup delays suggest a hardware or sware screwup. No memory, Old/Bad/overworked HDD, hea-vy background load?\n\nFor reference, I get startup times of well less than 20 seconds when starting Openoffice 1.1 and adaquate response, under Fedora under VMware under Win2K on an 800 Mhz laptop with a low load, 512 Mb ram and a new hdd. \n\nAs for a QT only version, if it ever comes up, my vote is against. I like QT but I like having Lgpl'ed Openoffice on multiple OS's more. There's no GPL'ed QT dev library under windows, which might hinder debugging and it's acceptance.\n\n(flamebait, Gtk maybe :-)"
    author: "anon cwrd"
  - subject: "kde mozilla"
    date: 2004-02-02
    body: "is there similar effort with respect to mozilla? That will be awsome too...."
    author: "mathi"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "Someone would need to resurrect the Qt branch of Mozilla again.. \n\nsee http://bugzilla.mozilla.org/show_bug.cgi?id=178987 if you're interested.\n\nBut I think 2004 will be the year that khtml gains massive ground with gecko, with webcore 1.2 and khtml 3.3 coming out later this year. http://weblogs.mozillazine.org/hyatt/ is a good place to find info on stuff Apple is doing wrt. khtml."
    author: "dood"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "Well, see for my comment a bit above that already covers that issue (Mozilla integration) from my view as active Mozilla contributor.\n\nI hope to see two open source engines being able to share ideas and force each other to get better and better in the future ;-)\nOn the other hand, I don't think all those people want to use KHTML/Konqueror over Gecko/Mozilla (like many don't use KDE Multimedia apps but XMMS), as they like Mozilla in some way (or are contributors like me), or don't like their file manager being their browser, and still want nice integration with their favourite Desktop environment.\nGood competition is always a nice thing to have, usually that helps both competitors (and remeber that e.g. Dave Hyatt, senior Mozilla hacker, now working at Apple on KHTML, is still listed as co-maintainer of Mozilla Firebird). Two open project with similar goals, exchanging ideas (and perhaps even some code), do make the OSS world better and even more free - it's about being able to choose, right?\nAnd, well, Mplayer and xine seem to work together to a certain extent as well ;-)"
    author: "KaiRo"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "here's nothing preventing anyone from creating a KHTML-based web-only application.\n\nMy personal guess is it's a two-week project for a usable app, since all the network and display code is done already. All that would have to be written (or ripped off Konquy ;-) is the shell, bookmark handling and small chunks of code.\n\nA 5KLOC project, if that."
    author: "Roberto Alsina"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "What I would like to have instead of a bookmark menu something like the kcontrol ioslave. Icons and folders for bookmarks and bookmark folders. Shouldn't be hard to do... Um -- perhaps it already exists?"
    author: "Boudewijn Rempt"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "I personally just use the toolbar thing in the universal sidebar. Saves me from having to open konqueror, *then* open bookmarks :-)"
    author: "Roberto Alsina"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "I'm afraid that it takes longer to display the root bookmarks menu than it does to open konqueror on /opt/kde3/bin for me... And the bookmarks sidebar thingy doesn't support drag and drop of bookmarks to folders. Oh, well -- I guess it's just a matter of personal preference: I like having a file-browser interface for a lot of things, and I would love it for bookmarks. \n\nI am looking at the settings:/ protocol to see whether I could marry it with the bookmarks code, but I haven't high hopes of actually doing something, since I wasn't planning on hacking anything but Krita tonight."
    author: "Boudewijn Rempt"
  - subject: "bookmark sidebar (was.Re: kde mozilla)"
    date: 2004-02-03
    body: "\"And the bookmarks sidebar thingy doesn't support drag and drop of bookmarks to folders.\" \nThere would be a simple solution: make keditbookmarks into a kpart\nhttp://bugs.kde.org/show_bug.cgi?id=53191"
    author: "testerus"
  - subject: "Re: kde mozilla"
    date: 2004-02-04
    body: "You could try to rip out the code from\n\nhttp://www.boddie.org.uk/david/Projects/Python/KDE/Docs/IOSlaves.html\n\nand rewrite it in C++ for improved performance."
    author: "Anonymous Custard"
  - subject: "Re: kde mozilla"
    date: 2004-02-04
    body: "Cool! Thanks for the link."
    author: "Boudewijn Rempt"
  - subject: "Re: kde mozilla"
    date: 2004-02-03
    body: "http://www.konqueror.org/embedded/"
    author: "c"
  - subject: "Re: kde mozilla"
    date: 2004-02-03
    body: "> My personal guess is it's a two-week project for a usable app\n\nmore like 2 day project.. I made a khtmlpart-based browser ( http://dot.kde.org/1010344585/1010399909/1010406855/1010409031/1010470469/ ) circa kde 2.2 that supported tabs (before konq did in kde 3.1) and manual session handling, etc... unfortunatly, I lost the source forever in a hd reformat since i never released it.\n\nI might still make another khtmlpart-based browser though.. I have a a lot of UI ideas from safari and the new omniweb :)\n\n"
    author: "fault"
  - subject: "Re: kde mozilla"
    date: 2004-02-02
    body: "There is an Crystal icon set."
    author: "gerd"
  - subject: "Keramikzilla"
    date: 2004-02-02
    body: "You may use Keramikzilla, a Mozilla skin that matches the Keramik style:\n\nhttp://www.kde-look.org/content/show.php?content=8685\n\nPlastikzilla will also be the next project when I finish Keramikzilla."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Keramikzilla"
    date: 2004-02-03
    body: "Hi,\n\nthis sounds very interesting, please contact me directly via email: \nneundorf at kde.org\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Keramikzilla"
    date: 2004-02-03
    body: "Plastikzilla would be cool\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Keramikzilla"
    date: 2004-02-03
    body: "Wow... Now this looks very nice - er, you want to visit us at the Mozilla dev room at FOSDEM in Brussels this year? ;-)\n\nNow this would be (less cross-platform but) easier to do in an ideal world where the Qt port of Mozilla lives happily and even has nsITheme implemented so that Mozilla could use widget styling directly from Qt/KDE :)"
    author: "KaiRo"
  - subject: "Re: kde mozilla"
    date: 2004-02-03
    body: "yep...\n\nand kde xmms and kde gimp would be nice too... "
    author: "anonymous"
  - subject: "icon bar"
    date: 2004-02-02
    body: "How can we get rid of the vertical icon bar in the OOWriter window and get dockable icon bars?"
    author: "gerd"
  - subject: "Apple"
    date: 2004-02-04
    body: "Come on Apple....about time you guys did the same thing!"
    author: "Rob"
---
In this interview, Jan Holesovsky, author and leader of the
<a href="http://kde.openoffice.org/">KDE.OpenOffice.org</a> project, now employed by <a href="http://www.suse.com/">SUSE</a>,  gives us a glimpse of what to expect in terms of <a href="http://www.openoffice.org/">OpenOffice.org</a> integration
on your KDE desktop.  The future sure looks bright!





<!--break-->
<p><a href="http://static.kdenews.org/content/kde-ooo-interview/www.xs4all.nl/%257Eleintje/stuff/OOo-KDE/2.png">
<img hspace="10" vspace="10"  width="300" height="240" src="http://static.kdenews.org/content/kde-ooo-interview/www.xs4all.nl/%257Eleintje/stuff/OOo-KDE/2thumb.jpg" align="right"/></a>
<strong>Please give us some background information about yourself.</strong></p>

<p>I am 27 years old, living in Prague, the Czech Republic, graduated from 
<a href="http://www.mff.cuni.cz/toUTF-8.en/">Charles University</a>,
also in Prague.</p>

<p>When not sitting in front of the computer keyboard, I am usually in hills or
mountains with my friends---trekking, climbing, mountain biking or
cross-country skiing depending on the season.</p>

<p><strong>What's the focus of this project and how did it start?</strong></p>

<p>It began in July 2003 when I read <a href="http://www.root.cz/clanek/1716">an article</a>
about <a href="http://ooo.ximian.com/">Ximian's work</a> on
improving OpenOffice.org. There was also a note about a (now unmaintained)
Bonobo OOo integration project, and I was wondering, whether anybody
had started a similar project for KDE.</p>

<p>I had just graduated, and I wanted to travel in October, so instead of
searching for a job I was reading the OOo documentation and programming
<a href="http://kde.openoffice.org/cuckooo/index.html">cuckooo </a>. The initial version
was available in the middle of August, so I still had some time left before
the travelling. I started the "<a href="http://kde.openoffice.org/ooo-qt/index.html">OpenOffice.org Qt port</a>",
now "KDE vclplug", because I wanted to replace all the OOo GUI toolkit "VCL" with its Qt implementation. I
finished a proof of concept a few days before I left. When I returned, I wanted
to continue my OOo work as an employee.</p>

<p>I started <a href="http://kde.openoffice.org/nwf/index.html">KDE NWF</a> in December when
I saw <a href="http://lists.kde.org/?l=kde-devel&m=107123771419550&w=2">the big demand</a>
for OOo Native Widget Framework
<!--[was: https://mail.kde.org/mailman/private/kde-devel/2003-December/018616.html but is not public]-->
on kde-devel@kde.org.</p>

<p><strong>How many people are working on this?</strong></p>

<p>Currently, I am the only developer. Lukas Tinkl sent me a patch to NWF that
corrected behavior of combo boxes and introduced tab bars.</p>

<p><strong>Are you paid to work on this project?</strong></p>

<p>The search for work resulted in a contract with <a href="http://www.suse.com/us/index.html">SUSE</a>.
I became their employee the last week, and now I am paid to continue the KDE OpenOffice.org
integration. Here I want to thank Holger Schroeder once more. He donated the money that
covered the first part of my Native Widget Framework development.
All the work on cuckooo and KDE vclplug (OOo Qt port) I did as a volunteer.</p>

<p><strong>Is there a website? Is there a CVS repository somewhere?</strong></p>

<p><a href="http://kde.openoffice.org/index.html">kde.openoffice.org</a> is the main page of the integration
project. It is an official <a href="http://incubator.openoffice.org/">OOo "Incubator" project</a>,
so it has all the possible support from the OpenOffice.org side.</p>

<p>There is no separate CVS repository for the project. KDE Native Widget
Framework is already in the OOo CVS repository
(<a href="http://gsl.openoffice.org/source/browse/gsl/vcl/?only_with_tag=cws_srx645_nativewidget1">branch cws_srx645_nativewidget1</a>);
KDE vclplug (OpenOffice.org Qt port) will hopefully appear in the OOo CVS
repository as well.</p>

<p><strong>When looking at the website it seems that there are 3 subprojects:
<a href="http://kde.openoffice.org/cuckooo/index.html">Cuckoo</a>,
<a href="http://kde.openoffice.org/ooo-qt/index.html">OpenOffice.org Qt port</a>
and <a href="http://kde.openoffice.org/nwf/index.html">KDE NWF</a>.
Can you describe the technical differences between the three projects
that are part of kde.openoffice.org</strong></p>

<p><a href="http://static.kdenews.org/content/kde-ooo-interview/openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik1.png">
<img hspace="10" vspace="10"  width="300" height="240" src="http://static.kdenews.org/content/kde-ooo-interview/openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik1thumb.jpg" alt="test" align="right"/></a>
The idea of cuckooo is to use the <a href="http://api.openoffice.org">UNO API</a>
of the OpenOffice.org to access its
functionality from <a href="http://konqueror.kde.org/">Konqueror</a> so that people could
browse to MS Office documents which would
<a href="http://static.kdenews.org/content/kde-ooo-interview/artax.karlin.mff.cuni.cz/%257Ekendy/cuckooo/screenshots/doc.jpg">open directly in the browser's
window</a>. Cuckooo itself is a KPart which controls an instance of OpenOffice.org
running in the background and displaying to the KPart's window.</p>

<p>KDE vclplug, formerly known as OOo Qt port, has the goal to get OOo controlled by Qt events and drawn by Qt
painting methods instead of pure X calls. It is developed for
<a href="http://development.openoffice.org/releases/q-concept.html">OOo 2.0</a> and it is still
quite experimental stuff. It does not support KDE styles, but once the
Qt events are working, it will be possible to use standard KDE dialogs (e.g.
the file dialog).</p>

<p>OOo NWF uses the <a href="http://gsl.openoffice.org/files/documents/16/1286/vclNativeWidgetProposal.sxw">Native Widget Framework</a>
to provide OOo with the
<a href="http://static.kdenews.org/content/kde-ooo-interview/artax.karlin.mff.cuni.cz/%257Ekendy/cuckooo/screenshots/OOo-KDE-NWF-listboxes.jpg">look</a>
<a href="http://static.kdenews.org/content/kde-ooo-interview/openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik1.png">of</a>
<a href="http://static.kdenews.org/content/kde-ooo-interview/openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik2.png">the</a>
<a href="http://static.kdenews.org/content/kde-ooo-interview/www.xs4all.nl/%257Eleintje/stuff/OOo-KDE/4.png">theme</a>
that the user chose for his KDE desktop. The framework uses the QStyle API to draw its widgets the same way
KDE/Qt would. OOo with NWF does not have a Qt event loop. It is developed for
OOo 1.1, but it will be used in 2.0 as well; then it becomes part of the KDE
vclplug subproject.
</p>

<p><strong>Is there any other work being done besides these three subprojects?</strong></p>

<p>You might be interested in
"<a
href="http://www.kde-look.org/content/show.php?content=7131">OpenOffice.org
1.1 toolbar icon themes</a>",
a script and a <a href="http://www.kde-look.org/content/preview.php?file=7131-3.png&name=OpenOffice.org1.1+toolbar+icon+themes">set of KDE icons</a>
for OpenOffice.org by Rohit Kaul. There is also "<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/koffice/tools/kfile-plugins/ooo">OOo kfile-plugin</a>"
by Pierre Souchay (screenshots <a href="http://static.kdenews.org/content/kde-ooo-interview/souchay.net1.nerim.net/images/screenshot1.png">1</a>,
<a href="http://static.kdenews.org/content/kde-ooo-interview/souchay.net1.nerim.net/images/screenshot2.png">2</a>,
<a href="http://static.kdenews.org/content/kde-ooo-interview/souchay.net1.nerim.net/images/screenshot5.png">3</a>).
</p>


<p><strong>What is the current state of KDE integration?</strong></p>

<p>The current version of cuckooo (0.3) seems to be stable and I am not going to add
new functionality in the near future. KDE NWF is nearing completion. I expect a fully usable version in February.
KDE vclplug is a long term solution, but it will take quite a lot of time to
finish. I would like it to be a part of OOo 2.0 which is estimated in Q4 of
2004 or in the beginning of 2005.</p>

<p><strong>Does it seem feasible to ditch the abstract GUI of OOo and work directly
with Qt?</strong></p>

<p>No, OOo will still have to have its own GUI layer. However there are plans to
define a more abstract OOo GUI toolkit "<a href="http://gsl.openoffice.org/files/documents/16/848/Toolkit2.sxw">Toolkit2</a>"
based on UNO calls. Services of "Toolkit2" would be implemented in toolkit which is native
for given platform (e.g. Qt for KDE). Unfortunately, due to the complexity of
the rewrite it is planned as an "after 2.0" work.
See <a href="http://gsl.openoffice.org/servlets/ReadMsg?msgId=788784&listName=dev">these two</a>
<a href="http://gsl.openoffice.org/servlets/ReadMsg?list=dev&msgNo=766">messages</a> for more information.</p>

<p><strong>Will "integration" include support for important low level KDE features like
the Kiosk framework sometime?</strong></p>

<p>First of all, the KDE NWF and KDE vclplug have to be stabilized. Then I can
integrate KIO and KDE dialogs to OOo and continue with further efforts, be it
KParts, Kiosk or other KDE features. I am afraid it won't happen before 2005.</p>

<p><strong>Will there be a KDE "version" of OOo in such that it looks and ACTS like a
KDE app?</strong></p>

<p>That is my intention. :) But it will take a lot of time. NWF is the first
iteration, and vclplug will help; Toolkit2 is hopefully the definitive
solution.</p>

<p><strong>A lot of people that have looked into developing OpenOffice.org found it very complicated,
what is your experience?</strong></p>

<p>It is really a huge amount of code. Luckily it is quite well modulized, so
once you get used to it, you do not encounter big problems. And if you do, you
still have <a href="http://ooo.ximian.com/lxr/">OpenOffice.org Cross-Reference</a> handy.
If you want to know more about OOo development, try
<a href="http://development.openoffice.org/digest">CPH's excellent CVS digest</a>,
mainly the <a href="http://development.openoffice.org/digest/2004_w02.html">first one with the introduction</a>.</p>

<p><strong>How is it to work with OpenOffice.org people? Does Sun have a big influence
over the development?</strong></p>

<p>I like the way the OOo people cooperate very much. They are very supportive
and every volunteer is welcome. KDE.OpenOffice.org couldn't have happened without
the support from the OOo side. I can't really say anything about Sun's influence over the development,
as my only contact with Sun on an official level was when I signed the
<a href="http://www.openoffice.org/contributing.html">JCA</a>
(a copyright assignment system).</p>

<p><strong>Where do you see KDE.OpenOffice.org going and what are your plans for it?</strong></p>

<p>The mission statement of KDE.OpenOffice.org says "The goal is to provide tight
(but optional) integration of the OpenOffice.org to the KDE environment
beginning with KDE look and feel and ending with KDE data sources." I believe it can be achieved.</p>

<p><strong>What is the project most in need of now?</strong></p>

<p>Time. It is the only limiting factor at the moment. :)</p>




