---
title: "First Krita Preview Release"
date:    2004-09-27
authors:
  - "brempt"
slug:    first-krita-preview-release
comments:
  - subject: "more features"
    date: 2004-09-27
    body: "Don't know what happened but the original announcement had more features listed:\n\n* Loading and saving of many image formats\n* Painting with Gimp brushes, generated brushes or text.\n* color picker, pencil, brush, airbrush, eraser, duplicate, filter-brush, lines, rectangles, ellipses and text tools.\n* Filling with solid colors, patterns or gradients\n* Selecting, copying (and pasting into Kolourpaint)\n* Scaling with one of five cool scaling algorithms\n* Image/layer rotating.\n* Adjusting brightness, contrast, gamma, color, saturation.\n* Working with layers (adding, moving, rotating)\n* Blur, sharpen, mean removal, emboss, edge detection, invert filters\n* Tablet support.\n* scan/screenshot image input\n* tool icon, crosshair or arrow cursors\n* grayscale and RGB color models (and a buggy CMYK model)\n"
    author: "anon"
  - subject: "Re: more features"
    date: 2004-09-27
    body: "> Don't know what happened but the original announcement had more features listed:\n\nWhat original announcement? http://tinyurl.com/7xr28 doesn't have traces of above."
    author: "Anonymous"
  - subject: "Re: more features"
    date: 2004-09-27
    body: "I think anon means the README.PREVIEW file I send round to ask for comments. That one is in the packages. I did a different announcement for the Dot, shorter, snappier and more upbeat. Not that there hasn't been some fierce editing going on with what I wrote... Mostly quite good, so I haven't got a problem with it, but if you want to read what I wrote, read the kde-announce mail."
    author: "Boudewijn Rempt"
  - subject: "Small problem"
    date: 2004-09-27
    body: "Krita _is_ very promising. But using KOffice-CVS I have since some time a problem: I can not load bitmap files :-/\n\nbash-2.05b$ krita /home/schnebeck/EOS/Test/IMG_0236.JPG\nkio (KSycoca): Trying to open ksycoca from /var/tmp/kdecache-schnebeck/ksycoca\nkoffice (lib kofficecore): kritapart.desktop found.\nkio (KTrader): KServiceTypeProfile::offers( KOfficePart, )\nkio (KTrader): Returning 11 offers\nkio (KTrader): KServiceTypeProfile::offers( Krita/CoreModule, )\nkio (KTrader): Returning 0 offers\nkoffice (lib kofficecore): KoDocument::openURL url=file:/home/schnebeck/EOS/Test/IMG_0236.JPG\nkoffice (lib kofficecore): kritapart.desktop found.\nkoffice (lib kofficecore): KoDocument::openFile /home/schnebeck/EOS/Test/IMG_0236.JPG type:image/jpeg\nkio (KTrader): KServiceTypeProfile::offers( KOfficePart, )\nkio (KTrader): Returning 11 offers\nkoffice (filter manager): KoFilterEntry::query(  )\nkio (KTrader): KServiceTypeProfile::offers( KOfficeFilter, )\nkio (KTrader): Returning 64 offers\nkoffice (filter manager): Filter: KOffice-Export f\u00fcr XSLT doesn't apply.\nkrita: No decode delegate for this image format (/home/schnebeck/EOS/Test/IMG_0236.JPG).\nkoffice (lib kofficecore): KoMainWindow::addRecentURL url=file:/home/schnebeck/EOS/Test/IMG_0236.JPG\nkoffice (lib kofficecore): [KoMainWindow pointer (0x8234ca8) to widget krita-mainwindow#1, geometry=1222x726+50+207] Saving recent files list into config. instance()=0xbfffef88\nkparts: Part::~Part 0x822b478\nkdecore (KLibLoader): The KLibLoader contains the library libmagickimport (0x8290d10)\nkdecore (KLibLoader): The KLibLoader contains the library libxsltexport (0x82bd360)\nkdecore (KLibLoader): The KLibLoader contains the library libkritapart (0x81e0bc0)\n\n\nUsing krita I would like to encourage the krita team to focus on features gimp has not established yet especially a working 16bit/channel framework.\n\nI hope there will be a generic painting API f\u00fcr krita and collaboration with kipimodules and digikams imageplugins. If there is a nice scripting host for krita I would like to port my gimp scriptFu to krita as soon as possible :-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Small problem"
    date: 2004-09-27
    body: "I think the image import problem is caused by a problem with your image magick libraries. Perhaps you've got the wrong version installed, or maybe two versions, and the header files of one version are used, but Krita is linked with another version.\n\nWe're designing for 16bit/channel images, but haven't added those yet. However, doing so means that we cannot use the kipi plugins, since those are based on QImages which are passed between app and plugin. If we were to use those, we'd need to convert the current layer to a QImage (losing data), filtering and then converting back. It could be done of course, but I guess it would be best to put a kipi connector in a plugin. Could be a nice job for someone interested in getting involved.\n\nFinally, Cyrille Berger has started work on ksjembed plugin, but it's not advanced enough to actually do anything."
    author: "Boudewijn Rempt"
  - subject: "Re: Small problem"
    date: 2004-09-27
    body: "... works after recompiling ImageMagick and KOffice :-)\n(A little bit slow with a 3000x2000 pixel bitmap on a 1GHz Athlon)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Small problem"
    date: 2004-09-27
    body: "Yes... And it would get worse when adding more layers. We have two problems here: we need to load all of the image into memory before work can begin. Ideally, we'd support deferring loading until parts of the image are touched, initially showing a preview. It's on the todo. The second problem is that we have cool tile manager for the data that's already loaded, but we don't support swapping in and out yet. And currently nobody has been brave enough to try and implement that part of Patrick Julien's design. \n\nThere are also some inefficiencies in the accessing of image data. We're working on those, though. I'm currently writing performance/stress tests so we can make informed performance improvements."
    author: "Boudewijn Rempt"
  - subject: "KIMP"
    date: 2004-09-27
    body: "I wonder why Boudewijn is always stressing that it's\nnot meant to be a KIMP (i.e. a concurrence to GIMP).\nI know that he likes to build a real paint tool\n(like Fractal Painter) for KDE. But I'm quite\nsure the majority of people will see it as GIMP replacement\nand everyone should be honest enough to admit that.\nOne day ago he posted here that he'd do a fork once\nthe code is matured enough. I sincerely hope that he will\nchange his mind and won't let this happen. The high\nconfigurability of the KDE desktop has shown that it can\nadapt to various different needs (novice and professionals,\nWindows users and Windows haters, etc). Why shouldn't it\nbe possible to create an application which is good for painting\nAND can be used for image editing, too? This would be a\nbig advantage for people who need both and shouldn't be that\ndifficult to realize (i mean in contrast to 2 different apps).\nAll the dulicated effort should rather go into higher\nconfigurability. So IMHO a fork would be completely unneccesary. \nBut perhaps I got him wrong on that issue. Ignore my post\nif that is the case..."
    author: "Martin"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "I'd second that, and include myself amongst those who hope that Krita will become a GIMP replacement, and I also think this is how it will be seen."
    author: "mikeTA"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "KIMP was the name of the idea to make a KDE frontend for GIMP. But as Krita does not share code with GIMP, the name is not suitable. Krita is neither a port nor a frontend of GIMP.\n"
    author: "Birdy"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "He he, the German K-menu says: Bildbearbeitung (krita), so this means image manipulation.\n\nI think must users in a world of digital imaging (scanners, photos) need a good IMP and GIMP2 is really good. I like it but I miss stuff like dcop, KDE UI and a more powerful scripting language, something like kommander. But functions like a channel mixer (LAB color code) and (for me as a photographer) Canon RAW-file support are there. I deeply miss stuff like 16bit/channel or grouping layers. That is not much and I know the Gimper are working on this and they still try to separate functions from GUI. So if krita steps in an wants to have early adopters, it should offer IMHO functions GIMP as not - like attaching a filter to a brush, thats cool stuff! If the aim of krita is (only) building a strict painting tool you will provoke many wishlist reports for krita ;-)\n\nBye\n\n  Thorsten (rebuilding KOffice with a new version of ImageMagick) "
    author: "Thorsten Schnebeck"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "Cyrille Berger already added painting with filters to Krita. It's not perfect yet, but yesterday, Cyrille and Bart Coppens have been working on making it better. When I mentioned the possibility of soon having a 'sharpen' filter brush in Krita to a photoshop user he started getting really impressed.\n\nBy the way, Thorsten, would it be possible for you to make a couple of 16-bit raw images available? I don't have a camera that can produce them, and if I want to take a look at working on 16bit/channel capabilities, I need a few test images.\n\nIn other news, the zooming bug that's still in the preview is now fixed in CVS. I really tried to get that done before the packaging, but only last night I figured out what to do about it. Oh well, it'll be in this night's nightly package."
    author: "Boudewijn Rempt"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "Yes, I know that I can attach a filter to a brush. And this is really cool stuff :-)\n\nThe Canon EOS RAW format has 12bit per channel but the great dcraw tool can change RAW into 48-bpp PSD (Adobe Photoshop) or 48-bpp PPM files. I can use convert to make a 16Bit tiff from the PPM. I will put in on a website and mail the URL to the mailing list.\n\nHTH\n\n  Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "/* Cyrille Berger already added painting with filters to Krita. It's not perfect yet, but yesterday, Cyrille and Bart Coppens have been working on making it better. When I mentioned the possibility of soon having a 'sharpen' filter brush in Krita to a photoshop user he started getting really impressed. */\n\nWhat is really important is a easy framework for filters/plugins. This has to be seen as splitted from Krita development. Image mainipulators are used in image viewers, digital camery tools ecc. as well. \n\nIt is always superiour when developers are able to control their own ressources, see kde-look.org ecc.\n\nA plugin bazaar, this is what we need."
    author: "Gerd"
  - subject: "Re: KIMP"
    date: 2004-09-28
    body: ">I like it but I miss stuff like [...] a more powerful scripting language\n\nHey! Gimp supports Script-Fu, Perl and Python! Do you really mean Python and Perl are not powerful scripting languages?"
    author: "oliv"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "Well, there is still room for an application based on GEGL, isn't it?\nhttp://www.gegl.org"
    author: "testerus"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "I took a serious, hard look a GEGL when I first began Krita. GEGL just isn't done enough to do anything with; it looks a lot like a semi-abandoned second-system effect demo. Anyway, in Krita's root dir there's file called IMAGE_LIBRARIES with our assessment of a lot of image libraries currently available, and pro's and cons about their use."
    author: "Boudewijn Rempt"
  - subject: "GEGL"
    date: 2005-06-17
    body: "For your general information, GEGL development has been restarted as of about a month ago. They're about finished pulling off the cobwebs and blowing out the dust now."
    author: "Leon Brooks"
  - subject: "Re: KIMP"
    date: 2004-09-27
    body: "It is not the \"KIMP\" simply because KIMP was a distinct project. KIMP's goal was to create a KDE GUI for GIMP, replacing the GTK+. But there was too much coupling  between the GIMP and GTK. After all, GTK originally stood for \"GIMP Toolkit\". The situation may have improved with GIMP 2.0, but the KIMP project long ago fell to the wayside. Here's hoping Krita has a bright future..."
    author: "Brandybuck"
  - subject: "compile problem"
    date: 2004-09-27
    body: "krita:\n..\nmake[4]: Entering directory `/opt/krita/krita-preview1/krita/ui/dialogs'\nif /bin/sh ../../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../../.. -I./.. -I./../../core -I./../../core/tool -I..  -I  -I/opt/kde3/include -I/usr/lib/qt3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -O2 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL-DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT wdgmatrix.lo -MD -MP -MF \".deps/wdgmatrix.Tpo\" \\\n  -c -o wdgmatrix.lo `test -f 'wdgmatrix.cc' || echo './'`wdgmatrix.cc; \\\nthen mv -f \".deps/wdgmatrix.Tpo\" \".deps/wdgmatrix.Plo\"; \\\nelse rm -f \".deps/wdgmatrix.Tpo\"; exit 1; \\\nfi\nwdgmatrix.cc:1:21: kdialog.h: No such file or directory\nwdgmatrix.cc:2:21: klocale.h: No such file or directory\nwdgmatrix.cc:17:23: knuminput.h: No such file or directory\nwdgmatrix.cc: In constructor `WdgMatrix::WdgMatrix(QWidget*, const char*,\n   unsigned int)':\n..."
    author: "Stephan"
  - subject: "Re: compile problem"
    date: 2004-09-27
    body: "Are you really sure you do have the kdelibs headers installed?"
    author: "Boudewijn Rempt"
  - subject: "Re: compile problem"
    date: 2004-09-27
    body: "yes. /opt/kde3/include/kdialog.h\nmy config option:\n./configure --prefix=/opt/kde3\nhmh.."
    author: "Stephan"
  - subject: "Re: compile problem"
    date: 2004-09-27
    body: "Strange... Other KDE applications do compile?"
    author: "Boudewijn Rempt"
  - subject: "Re: compile problem"
    date: 2004-09-27
    body: "yup"
    author: "Stephan"
  - subject: "Re: compile problem"
    date: 2004-09-27
    body: "Try to change koffice/krita/ui/dialogs/Makefile.am\n\nThere is a line with \nlibkisdialogs_la_METASOURCES = AUTO\ntry to replace it with only:\nMETASOURCES = AUTO\n\n(As far as I know, the library version of METASOURCES = AUTO is not supported anymore.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: compile problem"
    date: 2004-09-28
    body: "the same problem.. :(\n\nThe difference is in output: missing automake-1.7\nbut automake 1.8.x is installed!\nln -s /usr/bin/automake /usr/bin/automake-1.7 \nfix this output but not the compile prob.."
    author: "Stephan"
  - subject: "Re: compile problem"
    date: 2004-09-28
    body: "Ah, yes, sorry. I had not thought about the problem that you have to re-create the Makefiles. So if you have a different version of automake, it will not work.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "koffice?"
    date: 2004-09-27
    body: ". . . make Krita part of KOffice 1.4 . . .\n\nwhy KOffice and not kdegraphics? isn't this graphics software?"
    author: "Peter Robins"
  - subject: "Re: koffice?"
    date: 2004-09-27
    body: "Because it's already a KOffice application, and has been one for five years."
    author: "Boudewijn Rempt"
  - subject: "goddamn cool video of Krita"
    date: 2004-09-27
    body: "I don't know why this damn damn cool video isn't in the announcement but check this out:\nhttp://linuxreviews.org/news/2004/09/27_krita_kde_koffice_preview_available/\nhttp://linuxreviews.org.nyud.net:8090/news/2004/09/27_krita_kde_koffice_preview_available/krita.mpeg\n"
    author: "KDE User"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "Apparantly that site has been slashdotted, so here are the links to the originals:\nhttp://www.bartcoppens.be/krita.mpeg\nhttp://www.bartcoppens.be/krita2.mpeg"
    author: "Bart Coppens"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "What video format, both xine and mplayer (recent versions) choke on the second (recommended) video."
    author: "KDE Fan"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "I never knew about it... Nobody ever tells me anything. Pout.\n\nBut, seriously, nicely done, Bart -- and it's quite interested to actually see people their workflow. Seeing the way you handle the docker overflow, I wonder whether moving towards Kivio's dockers wouldn't be a good thing, at least for the way you work.\n\nHow did you capture them? I wouldn't mind seeing a few other other people work with Krita in this way. It should help streamlining Krita's UI.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "This way of using the dockers gives me a bit more free space, so I have a better view of what I'm doing. And this way I could also resize krita somewhat in order to make the download not too big.\n\nThe capture tool I used is xvidcap: http://xvidcap.sourceforge.net/"
    author: "Bart Coppens"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "Cool -- with the next release we must do something like that again. Have you looked at Kivio's dockers?"
    author: "Boudewijn Rempt"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "Not yet, but I'll have a look."
    author: "Bart Coppens"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "Make sure you give them a chance to disappear and appear automatically. The dockers are not entirely bugfree, so they may need a bit of nudging now and then."
    author: "Boudewijn Rempt"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "Can Krita not use KMDI? I really like the Ideal modes where the widgets hide automatically after you clicked on them. But KMDI also allows you other modes."
    author: "Michael Thaler"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "KMDI would be cool, but Krita is limited through the KOffice framework."
    author: "Sven Langkamp"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "We've looked into that, but in the end, kmdi is not compatible with KOffice, and Kivio's dockers are. Plus, kivio's dockers look & feel more like palettes than kmdi tabs that are mostly the whole length or height of their dock area. "
    author: "Boudewijn Rempt"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "They look very nice, but I didn't see them appear or disappear? Is this maybe a feature that's only in CVS and not yet in 1.3?"
    author: "Bart Coppens"
  - subject: "Re: goddamn cool video of Krita"
    date: 2004-09-27
    body: "I wouldn't know, it's been a year and a half since I ran a non-CVS KOffice. I'll ask Peter Simonssen."
    author: "Boudewijn Rempt"
  - subject: "Speed"
    date: 2004-09-27
    body: "Actions like image scaling take really much time.\nFor example the scaling shown in video 2 takes several seconds. \n\nMaybe one should have a look at MPlayer which contains highly optimized versions of the same scaling algorithms as the ones Krita is using.\n\nThe scaling algorithms from Video2 is a Mitchel algorithm and Krita needed several seconds. MPlayer can do the same scaling with the same algorithm in ~ 1/100 second. (MPlayer supports all the other scaling algorithms, too)\n\nhttp://www.mplayerhq.hu/"
    author: "Asdex"
  - subject: "Re: Speed"
    date: 2004-09-27
    body: "There are still some, ahem, unoptimized bits of code. Especially the bits to access pixels are not all that optimized. But Michael and Cyrille are working on, are working on it. And, mplayer uses mmx and stuff, and we're still working in plain C++. No doubt we'll be all right on the night, and I'm really happy that we have such good quality scaling already."
    author: "Boudewijn"
  - subject: "Re: Speed"
    date: 2004-09-27
    body: "I think the scaling code is quite efficient. Unfortunately the scaling code acceses the pixel data in a very inefficient way. This is what makes to code so slow. I think the code would be a lot faster if it would use iterators to access the code. Unfortunately the code now iterates over columns and not rows and to use iterators efficiently, the code has to iterate over rows. In prinicple it is no problem to rewrite the code, so that it iterates over rows instead of columns and use iterators to access the pixel data then. I hope this will speed up the code a lot, but I am not sure when I have time to do that. So if you like to participate, this is probably not a task that is too hard to do.\nMPlayer probably uses some hand-optimized assembler code and I will certainly not try to code the scaling code in assembler."
    author: "Michael Thaler"
  - subject: "Re: Speed"
    date: 2004-09-27
    body: "> I think the scaling code is quite efficient. Unfortunately the scaling code acceses the pixel data in a very inefficient way.\n\nMPlayer has some nice examples how to acceses the pixel data in a very efficient way, too. They try to avoid cache thrashing and so on.\n\n> MPlayer probably uses some hand-optimized assembler code\n\nThat's true but it not only holds hand-optimized assembler code but plain c, too.\n\n> and I will certainly not try to code the scaling code in assembler.\n\nMy point was: Mplayer has optimized C and assembler (x86 + some altivec code) versions of all your scaling algorithms. So should not \"try to code the scaling code in assembler\". Copy & paste should be enough. MPlayer's scaling filters do understand not only YUV but RGB32, too.\n\n"
    author: "Asdex"
  - subject: "Re: Speed"
    date: 2004-09-28
    body: "I don't think you are adequately aware of the vagaries of copy & pasting. Perhaps Michael could take ideas and algorithms from mplayer, but it's highly unlikely that it would be a simple matter of copying and pasting. Programming is more complicated than that."
    author: "Boudewijn Rempt"
  - subject: "Re: Speed"
    date: 2005-03-06
    body: "Wait, you can't just copy and paste code between systems and have it work?\n\nBut SCO said you can! They were lying?\n\nI'm selling my shares right now!"
    author: "joe lorem"
---
<a href="http://www.koffice.org/krita/">Krita</a>, formerly known as Krayon,
formerly known as KImageShop, never known as nor intended to be the Kimp, is
available for your testing pleasure.







<!--break-->
<p>For the first time since development started in 1999, Krita is complete
enough to be packaged as the first preview release. Building on the great foundation
laid by the original team, the enthusiastic work by John Califf and the thorough
architecture designed by Patrick Julien, the Krita developers have been working
real hard during the last year to bring you a firework of interesting
features.</p>

Those include (but are of course not limited to):
<ul>
<li>painting with Gimp brushes or image filters</li>
<li>gradients &amp; patterns</li>
<li>excellent tablet support</li>
<li>world-class image scaling</li>
</ul>

<b>Note:</b> This is not even an alpha release. Stuff is broken, not implemented
at all or even not even intended to be implemented. We've been making
really great progress, but there's plenty left to do -- a challenge to someone
wanting to get their hands dirty with a paint app that's still small enough to
understand.</p>

<p>Installing Krita might interfere with your existing KOffice installation,
as it depends on KOffice CVS. But if you're curious or anxious to
know how far we've come in the past year (or five), download the <a href="http://ktown.kde.org/~danimo/krita/preview1/">packages</a> Daniel
Molkentin has prepared and made available.
Because Krita is very actively developed, with several CVS commits per day, we
also provide <a href="http://ktown.kde.org/~danimo/krita/nightly/">Krita nightly
snapshots<a>. SUSE packages will be available soon.</p>

<p>If you don't feel up to compiling, but still want to gaze at the look & feel
(described by as "very clean, very easy to find your way around" by an expert
Photoshop user) you can go to the <a
href="http://koffice.kde.org/krita/screenshots.php">screenshots</a> and sate
your curiosity.</p>

<p>For now, all I want to do is to thank all the people who have worked on
Krita: Michael Koch, Matthias Elter, Andrew Richards, Carsten Pfeiffer and
Toshitaka Fujioka for starting this amazing project, John Califf for impelling
the project,
Patrik Julien for designing Krita's current architecture and finally the current
crew:</p>

<ul>
  <li>Boudewijn Rempt (maintainer, stuff that doesn't work)</li>
  <li>Sven Langkamp (GUI, especially the dockers & the color wheel)</li>
  <li>Cyrille Berger (Filters, tools, core stuff)</li>
  <li>Adrian Page (Painting, tablet support, gradients, core stuff, fixes all
over the place)</li>
  <li>Clarence Dang (zoom, shape tools)</li>
  <li>Dirk Schoenberger (code cleanups, tool shortcuts)</li>
  <li>Bart Coppens (Fills, previews, text tool)</li>
  <li>Michael Thaler (Scaling, rotating)</li>
  <li>Camilla Boemann (core stuff)</li>
  <li>Daniel Molkentin (Packaging this release)</li>
</ul>

<p>We really hope to keep up the pace, and make Krita part of <a href="http://www.koffice.org/">KOffice</a> 1.4, to be
released near the end of the first quarter of 2005.</p>







