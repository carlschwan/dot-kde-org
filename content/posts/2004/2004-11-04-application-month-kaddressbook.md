---
title: "Application of the Month: KAddressBook"
date:    2004-11-04
authors:
  - "a"
slug:    application-month-kaddressbook
comments:
  - subject: "LDAP support unusable IMHO"
    date: 2004-11-04
    body: "First I have to state that the kaddressbook is a great application. Great work!!\n\nI just take the opportunity to bring some focus to the current weaknesses of the LDAP support (which IMHO can be seen as critical since LDAP seems to be the only real standard for exchanging addresses).\n\nSee my bug-report at http://bugs.kde.org/show_bug.cgi?id=92246 (got no response so far)."
    author: "Ben"
  - subject: "Re: LDAP support unusable IMHO"
    date: 2004-11-04
    body: "I'd second this comment.  The addressbook is a nice tool, but without robust and more tightly integrated LDAP support, it is useless to me."
    author: "John Dell"
  - subject: "Re: LDAP support unusable IMHO"
    date: 2005-05-05
    body: "I've read from the documentation that we're supposedly able to write addresses/contacts to a LDAP server (but can we specifically configure which LDAP server we want to write to?). I'd tried exporting the contacts into a LDIF file, but many details were missing, like the JPEG photo & the IM addresses.\n\nHave anyone thus far managed to use KAddressBook to write to a LDAP server of their own choice (such as OpenLDAP)?"
    author: "cwlee"
  - subject: "Write letters to a contact in KAddressbook"
    date: 2004-11-04
    body: "I was looking lately to the possibility to reuse the KAddressbook database to write a letter in KWord. It would be nice to be able to select a contact in Kaddressbook and press a button to fire KWord with a letter template and a nice wizard asking me which sender address do I want to use, if I need an envelop, etc. I thought it was already possible but could not manage to do it.\n\nAnother place where KAddressbook could be better integrated in the desktop is having the possibility to visualize all messages sent or received from a contact.\n\nFinally, it would ne nice to have a mechanism to format the output of an address. Today, KAddressbook puts the zipcode after the city but in France it is the other way round. Maybe having a text configuration file with all the formatting for the different countries would make it easier to accomodate for all the little differences about addresses.\n\nAll in all, an addressbook is the center of a lot of people work today. It is great that we have found in Tobias an active maintainer and that KAddressbook has become so much more powerfull and more user friendly than the crude (but fast) list widget we had in KDE 2\n\nThanks Tobias for all the work done...\n\n"
    author: "Charles de Miramon"
  - subject: "Re: Write letters to a contact in KAddressbook"
    date: 2004-11-04
    body: "I agree, this would be a very nice idea."
    author: "Ali Akcaagac"
  - subject: "Re: Write letters to a contact in KAddressbook"
    date: 2004-11-05
    body: "Hi Charles,\n\nthe mailmerge functionality is already implemented in current version of kword. Just start KWord, select Extra->Setup MailMerge->Open Available->AddressBook.\n\nNow you have additional address specific place holders which you can use for mail merge.\n\nThe zip code position is on my TODO.\n\nThanks for the comments\n\nCiao,\nTobias"
    author: "Tobias Koenig"
  - subject: "Re: Write letters to a contact in KAddressbook"
    date: 2004-11-05
    body: "Is this only with head? In my kword (1.3.4 - suse9.1) there is no such thing.\n\n\nAre there such clear pointers for ldap (on kolab2) to because thats another thing I like to test (but don't no how).\n\nThanks Tobias for the great work on KAddressbook."
    author: "rogo"
  - subject: "Re: Write letters to a contact in KAddressbook"
    date: 2004-11-05
    body: " Talking about zip codes.. I just looked at the locale files that comes with glibc, and they have all sorts of information like format of date, 24h vs. am/pm, curency symbol before or after amount, whatever decimal is \",\" or \".\", etc.\n\n Now technically speaking.. shouldn't address formats go there too, so kab can format them automatically according to the country they are located in (assumed that distroes install *all* locales)? As opposed to a checkbox that just hard-swithches the order, so it looks right when I snail-mail people in France, but wrong when sending to the US.\n\n I know that to the end user it doesn't matter whatever it's the one or the other that does the job, but I think it's important with respect to the principle about not duplicating work in several similar projects (kab & gab (or what it's name is)).\n\n Does people agree to this? Does someone have the ability/time to impliment this? Does someone have the guts to ask the GNU ppl if this is an idea they are willing to play along with?\n\n justMyTwoCent::opinion(TM);\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Write letters to a contact in KAddressbook"
    date: 2004-11-06
    body: "Since it's difficult to get stuff like this in glibc, we (KDE) provide such a list ourself.\nFor every country you can create an address template which is used for the output of a formatted\naddress.\n\nCiao,\nTobias\n"
    author: "Tobias K\u00f6nig"
  - subject: "\"name, christian name\" sequence for print output "
    date: 2004-11-05
    body: "there are about 40 different choices of output sequencing, but no option to\nthe \"standard\" sequence of a (German) phone book: all entries with the same\nname are ordered by christian name.\nAs I got a lot of \"family\" entries, it would be nice to have those entries\nordered by christian name.\nKeep up the good work\nBernd"
    author: "rzbkmr"
  - subject: "Re: \"name, christian name\" sequence for print output "
    date: 2004-11-06
    body: "Please file a whishlist item on http://bugs.kde.org\n"
    author: "Tobias K\u00f6nig"
  - subject: "I think it's ugly"
    date: 2004-11-07
    body: "I might be the only one who thinks this, but I think kaddressbook is quite ugly."
    author: "Mike"
  - subject: "Re: I think it's ugly"
    date: 2004-11-07
    body: "And for sure I'm not the only one who think this, but I think you are a troll."
    author: "Davide Ferrari"
  - subject: "Re: I think it's ugly"
    date: 2004-11-08
    body: "Ah, so opinions differing from the norm are not allowed? I don't give a monkey's what you think."
    author: "Mike"
  - subject: "Re: I think it's ugly"
    date: 2004-11-08
    body: "offering no specific detail for your complaint is not helpful.\nPlease watch your language.\nBernd"
    author: "rzbkmr"
  - subject: "Address Book freezes"
    date: 2004-11-17
    body: "Except in Kontact, whenever I click Address Book, everything on the screen locks up.  I have never happened to see anyone else claiming this problem, so have never read a reason nor a cure."
    author: "albrtijo"
  - subject: "Re: Address Book freezes"
    date: 2005-10-19
    body: "I've experienced this problem too, but I found that my eZTrust Antivirus/Internet Security was at fault, it was just going nuts and consuming large amounts of CPU time. Disabling the real-time scanner solved the problem instantly. If you're using eZTrust I recommend changing to another virus scanner application."
    author: "darkwarden"
---
Again after a short time of absence we finally finished a new version of the "Application of the Month" series. 
This time we take look at the underadvertised addressbook application within KDE, <A href="http://www.kaddressbook.org/">KAddressBook</A> which is currently maintained and developed by Tobias Koenig. We are proud that we can now also offer a <A href="http://www.kde-france.org/article.php3?id_article=123">French version</A> of Application of the Month next to the usual languages we always offer: <a href="http://www.kde.de/appmonth/2004/kaddressbook/index-script.php">German</a>, 
<a href="http://www.kde.nl/apps/kaddressbook/">Dutch</a> and <a href="http://www.kde.org.uk/apps/kaddressbook/">English</a>. Do you like these series? Do you think we should write more about cool KDE applications? Please, <a href="mailto:kde-appmonth@kde.org">contact us</a> if you want to help! <strong>UPDATE</strong>: Our friends from <a href="http://www.kdehispano.org">KDE-Hispano</a> have made a <a href="http://www.kdehispano.org/node/511">translation to Spanish</a>.



<!--break-->
