---
title: "The KDE Wiki Has Moved"
date:    2004-03-01
authors:
  - "numanee"
slug:    kde-wiki-has-moved
comments:
  - subject: "The KDE Wiki is really powerfull"
    date: 2004-03-01
    body: "The more I work with the KDE wiki, the more I like it.\nIt is powerfull and easy to use. And Luci is an excellent maintainer, and really cares for the site.\n\nThe wiki is also an excellent tool to integrate people to the KDE community. Easy access is one of the reasons behind the success of kde-look.org and kdeapps.org. Openess is the reason KDE has one of the most vibrant, efficient communities out there. And we can make it even better ;)"
    author: "cwoelz"
  - subject: "What kind of web application uses 150 queries ?"
    date: 2004-03-01
    body: "[ Execution time: 3.69 secs ]   [ Memory usage: Unknown ]   [ 150 database queries used ]   [ GZIP Enabled ]   [ Server load: 1.98 ]"
    author: "Vincent"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-01
    body: "tikiwiki :)"
    author: "anon"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-01
    body: "A REALLY inefficient one."
    author: "Ian Whiting"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-01
    body: "Maybe it would make sense to cache the wiki?  Dunno, I'd have to check the tikiwiki FAQ and consult luci to see if it's practical."
    author: "Navindra Umanee"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-02
    body: "perhaps we could try <a href=\"http://sourceforge.net/projects/turck-mmcache/\">Turck MMCache</a>.\nfor more info about boosting TikiWiki see <a href=\"http://tikiwiki.org/TikiBoosting\">http://tikiwiki.org/TikiBoosting</a> and <a href=\"http://tikiwiki.org/DbPerformance\">http://tikiwiki.org/DbPerformance</a>.\n"
    author: "luci"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-01
    body: "yes, unfortunatelly it's too much still, but it's better than before.\ni'll try to reduce the number of modules displayed by default on the site to minimum - i think it will help reduce the number of queries this way a bit...\n\nluci"
    author: "luci"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-02
    body: "You could also try to enable the MySQL query cache. Since it's using so many queries."
    author: "Vincent"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-02
    body: "i've enabled TikiWiki wiki cache feature. it's much better when the page gets cached now."
    author: "luci"
  - subject: "Re: What kind of web application uses 150 queries ?"
    date: 2004-03-02
    body: "Please reduce queries by caching them globally, like with eg. http://www.danga.com/memcached/"
    author: "Datschge"
  - subject: "AWESOME!!"
    date: 2004-03-02
    body: "WOW! \n\nI was just looking at http://developer.kde.org/ GREAT JOB on the redesign! Also KDe WIki is very nice :)"
    author: "Alex"
---
<a href="http://luci.ground.cz/">Lukas Masek</a> (aka luciash d' being) <a href="http://wiki.kdenews.org/tiki-read_article.php?articleId=11">has announced</a> the successful completion of the move of the <a href="http://wiki.kdenews.org/">KDE Wiki</a> to the KDE Dot News server.  With the move, KDE Wiki gains not only more computing resources, but also a new domain name in the form of <i>wiki.kdenews.org</i>.  Many thanks to luci and of course to our sponsor <a href="http://pem.levillage.org/">Pierre-Emmanuel Muller</a> of <a href="http://www.levillage.org/">LeVillage</a>.



<!--break-->
