---
title: "KDE Developer's Corner: Common Programming Mistakes"
date:    2004-03-24
authors:
  - "zrusin"
slug:    kde-developers-corner-common-programming-mistakes
comments:
  - subject: "Fabulous!"
    date: 2004-03-24
    body: "Some of those tidbits are like gold! I'm especially annoyed at not realising the QValueList iterator one. I should have realised that at some point."
    author: "Max Howell"
  - subject: "Re: Fabulous!"
    date: 2004-03-24
    body: "I agree, these are pure gold!  There are a lot of these \"mistakes\" in kstars...looks like I have some work to do :)\n"
    author: "LMCBoy"
  - subject: "Question: someCString.length()"
    date: 2004-03-24
    body: "As an old FORTRAN programmer, I know that you shouldn't do this:\n\n    QCString someCString = ...; \n    for( int i = 0; i < someCString.length(); ++i ) {\n        //Do something\n    }\n\nYou should do:\n\n    QCString someCString = ...; \n    int temp = someCString.length()\n    for( int i = 0; i < temp; ++i ) {\n        //Do something\n    }\n\nBut, my question is:  doesn't the optimizing compiler take care of this?\n\nAnd my pet programing mistake.  What is wrong with the example??\n\nAnswer: The index of a loop should be: \"unsigned int\" (as should: \"temp\" in my example!).  And when you get the warning about comparison between signed and unsigned integers, you should fix it.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Why does it matter if you make them unsigned?  Do they not still take up the same memory space?  Isn't there no use for the extra positive ints?  Just curious."
    author: "Jay"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "I'm not sure, but I guess you'll run into trouble when the loop gets very large, larger then 2^(len(int)-1) iterations, because the last bit is used for positive/negative for normal ints, and is just another bit for unsigned ints."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "I guess that writing code correctly is part of the difference between being a software engineer and being a hacker.\n\n:-)\n\nBut seriously, it will help you find bugs, unless you simply ignore the compiler warnings.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "The thing with using uints, is that while using a Uint will allow higher values than an int, we're still talking 32 bit numbers here. If you really are worried about large loop indices, then use a long at least. Mind, even then, there is always potential to overflow the type.\n\nHonestly, as a programmer, you should always be aware of the potential iterations of any given loop, and ultimately, you should put checks in place in very large loops to ensure that you can't roll over."
    author: "Dawnrider"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "longs aren't longer than ints on x86; they are 32 bits.  C's types are dumb.  IMHO there should be int8, int16, int32, int64, and int128.  That way there would be no confusion about the length of types, and ints wouldn't magically change size and break all your code when you recompile for Alpha or x86-64 or 286 or Z80."
    author: "Spy Hunter"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Include C99's stdint.h, and you get types like uint8_t, uint16_t, etc..."
    author: "AC"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-26
    body: "The point is that length() returns an unsigned int that could be >= 2^31 which will be interpreted as a negative number.\n\nFunny thing is that since the loop comparison involves a signed and un unsigned  the signed will be cast to unsigned before comparison and the loop will go on as expected. There might be other problems with an \"negative\" index, but not in the example.\n"
    author: "Robin"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "How do you think compiler should optimise that ?\nThis would be rather strange, since that method can be:\n\nfor( ;a.bla();a.bleble() )\n{\n\ta.do_something_nasty();\n}\n\nin example above a.bla() can be testing for something, and return true or false. \nSo there is no way compiler should even approach optimisation.\nI know that code above  suffers from - can be neverending story. But it's just example :-D\n"
    author: "gregj"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "The compiler can easily check whether the string is modified inside the loop. If it's not modified then the value of string.length() will be calculated only once.\n\nAnd in your example: If bla(), bleble() and do_something_nasty() are all const (i.e. they don't modify a) then a.bla() can (and will) be calculated before the loop. I only wonder what happens if one of those methods change a mutable member variable of a.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Because of exactly this compilers must not rely on 'const' as method attributes for optimization purpose, and that's exactly why blah() will be called each time.\n"
    author: "Anonymous"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Two things:\n\n1. The compiler doesn't see the implementation of QCString::length() and therefore can't check that\n2. QCString::length() doesn't do:\n\nint QCString::length() const {\n   return const_cast<QCString*>( this )->mLen++;\n}\n"
    author: "Marc Mutz"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "This is an issue that has been discussed a million times already in several fora. The best description of the problem and its subtleties can be found at the venerable Guru of the Week site:\n  <A HREF=\"http://www.gotw.ca/gotw/081.htm\">Constant Optimization?</A>\nIt is a little dated if you are using a globally optimizing compiler along with monolithic applications, but for the rest of us mere mortals, it is accurate. For KDE, assuming the use of dynamic libraries, it is accurate.\n\nHmm, I can't select anything other than \"Plain Text\" for encoding with Konq from HEAD."
    author: "Ravi"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "> Hmm, I can't select anything other than \"Plain Text\" for encoding with Konq from HEAD.\n\nThat's because dot.kde.org only offers \"Plain Text\" to mortals."
    author: "Waldo Bastian"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "> How do you think compiler should optimise that ?\n\nOne of the things an optimizing compiler does is move invariant (constant) expressions outside of the loop.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: " Yes, but how is the compiler to know that the expression is invariant? If \n.length() is an inline it may be able to work it out from the fact that someCString \nis never updated, but if it isn't it's hard to see how this would work. Or does C++\nhave some way of telling the compiler that length() has no side-effects?"
    author: "ac"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Yes of course. QCString::length () is defined as\nuint length () const"
    author: "Eggert Ehmke"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Well, this means that lenght() does not modify (unmutable) members of the object it is applied to. It does not mean that lenght() has no side effects (it can modify global variables, for instance)."
    author: "glettieri"
  - subject: "way of telling that length() has no side-effects"
    date: 2004-03-24
    body: "just put const: it indicates exactly that (at least for that invocant, recipient or object [this] :-), length() will not change anything that is not marked mutable... ;-)\n\nsize_t QCString::length() const {\n  ...\n}\n\n"
    author: "Humberto Massa"
  - subject: "Re: way of telling that length() has no side-effects"
    date: 2004-03-24
    body: " Yes, but that's not enough for the compiler to treat it as invariant. You'd need to say\nthat it didn't touch any global state either."
    author: "ac"
  - subject: "Re: way of telling that length() has no side-effec"
    date: 2004-03-25
    body: "Just add 'return random();' to that member function, and you cannot rely on that returning the same number every time.  The function still won't modify the object, but it illustrates why const doesn't allow the compiler to optimize away calling length each time through."
    author: "CK"
  - subject: "Re: way of telling that length() has no side-effec"
    date: 2004-03-25
    body: "True.\n\nHelp is at hand, however. gcc allows you to specify that a function called with the same arguments always returns the same values, and thus can be optimised out of a loop, by use of __attribute__((const)). This is a gcc extension specifically for cases like the above. \n"
    author: "Jon"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "I disagree about using the temp variable.  There are two obvious reasons to use it (a) avoid function call and computation overhead of length() call--or whatever you use to limit the loop--by calling once and saving/caching, and (b) in concurrent environments length() could change during the loop, so you're screwed.\n\nwell, most libraries (stl most definitely) inline the call to length()/size(), and the function itself just returns an attribute of the class, so there is no advantage over the caching method you state.\n\nfor (b), you'd probably want to use a mutex or something like that to shield in that situation anyway.\n\nSo basically what that method does is create an unnecessary obfuscation and more code to read.\n\nunsigned int may seem more correct, however (a) most places in the code don't need this kind of protection (you know you won't go that high), (b) in situations where it will, there's probably a smarter constraint (don't forget that architectures with different word sizes define unsigned int differently!), and (c) no way I'm going to type more than I need."
    author: "matus telgarsky"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "> well, most libraries (stl most definitely) inline the call to \n> length()/size(), and the function itself just returns an attribute of the\n> class, so there is no advantage over the caching method you state.\n\nThe problem is that QCString is a pretty stupid class (it's a normal C-string with reference counting).\n\nhttp://doc.trolltech.com/3.3/qcstring.html#length says:\n\"Returns the length of the string, excluding the '\\0'-terminator. Equivalent to calling strlen(data()).\"\n\nSo QCString::length() is not O(1) but O(n) where n is the length of the string and thus highly inefficient.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "well, that sucks.  I'm not a QT guy, so my comment was uninformed in this context..."
    author: "matus telgarsky"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-25
    body: "It appears that the problem is ... .\n\nI suppose that this isn't the best place to state that C syntax sucks.  But it does.  One of the main offenders is the 'for' loop construct.  This can be used for many kinds of loops, but code efficiency suffers.  If we used an indexed loop, then the problem wouldn't exist because it is required that the parameters for an indexed loop ARE constants.\n\nQCString someCString = \"...\"; \nSTART(N=0, someCString.length())\n//Do something\nLOOP: END\n\nMuch better.  And, you don't have to define: \"N\" because it isn't really a variable (but it is unsigned).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "> So basically what that method does is create an unnecessary obfuscation and \n> more code to read.\n\nI think that my point is that you no longer have to do this because the compiler will do it for your.\n\n> unsigned int may seem more correct\n\nYes that is why you do it, because it is correct.  Integers that can never be less than 0 should be unsigned.\n\n> no way I'm going to type more than I need.\n\nYes, it is much better to hunt for bugs than to take the extra effort to write the code correctly.\n\n:-)  And I guess that you, therefore, would never want to try PL/Fiv.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "Hi,\n  I was careful to say \"may _seem_ more correct\", because it's naive to assume that unsigned int is a good idea on all architectures, and especially in all contexts.  Usually there is a better choice if you are worry about overflow, like ptrdiff_t, size_t, class::type, etc.\n\n  oh and while I'm being anal, isn't using != rather than < much prettier in many loops?  I use that styling very often, and definitely whenever dealing with sequential incrementation of integers.."
    author: "matus telgarsky"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-24
    body: "It's not only about the overflow. Suppose you want to do something like\n  textfield.setlength(i);\nwhere i is a loop index\n\nNow Textfield::setlength should really be defined as\n  setlength(unsigned int l)\nbecause you don't want to have to handle the case of a negative length for a textfield.\n\nIf you have warnings turned on the call to textfield.setlength(i) will give you a warning. And you really want to avoid warnings because too many of them will start to hide those that you really should see because they indicate a possible bug.\n\n"
    author: "Anonymous"
  - subject: "Re: Question: someCString.length()"
    date: 2004-03-26
    body: "yes yes, agreed, I write that way all the time for the reason you stated.  But the fragment in question was just part of a routine, and it's possibly we could know we don't need the extra amount of int afforded to us this way.\n\nbut yeah for any API type stuff, whenever I take something vaguely like an offset I use unsigned so that the compiler can help me.\n"
    author: "matus telgarsky"
  - subject: "Re: Question: someCString.length()"
    date: 2005-08-09
    body: "Am i right to propose this oslution :\n\n\nQCString someCString = ...;\nfor( unsigned int i = 0; someCString.data()[i] != 0; ++i )\n{\n    // Do something\n}\n\n\nThis way i values are visited sequentially,\nand (if someCString has a \\0 ending char) then\nstring length (string end) is tested with a O(1) complexity.\n\nConstraint : the string size can stay unmodified,\nor can increase, but should not decrease.\n\nTo always respect the constraint : someCString could be const (if possible)\n\n"
    author: "Paul Balez"
  - subject: "Good Stuff"
    date: 2004-03-24
    body: "This is a good thing to see happening, and I hope you can perhaps turn this into a series.\n\nAlthough I'm not familiar with the Qt/KDE aspects (yet!), one thing I will say is that people 'just hacking' (and people who should know better :)) ignore compiler warnings all too often. \"Oh, well, it compiles and the application runs...\". \"Oh it's OK, it'll get caught by the compiler.\". Those warnings are there for a purpose, and more often than not, are the sign of redundant (and/or inefficient) code - something that you really do not want in a project the size and scope of KDE. The worst case scenario is that your code is compiled with a different compiler version, or on a different platform, and the compiler throws a wobbler. A very real problem is that in a project the size of KDE the problems tend to multiply the more that people do this, and remember that people may actually be reusing your code and depending on it. Please, if possible, try and work for a handful of minutes to get them darn warnings down to zero, if possible. You'll be satisfied with a cleanly compiling bit of code, and it will save you (and others) a ton of time and effort in the future.\n\nSomething that also came home to me in the 'QString is Null' example (a bit of a mild example in this context) was that people do not use the existing methods available to them enough. If you reuse existing methods and code available as much as possible then you spend less time getting something done, you know where any problems occur, if and when they do, you know how to solve them much quicker, more developers can help you because they are using the same methods and the same code as you.... There is also a responsibility on you to try and expose as much useful code to others as you can, and make sure that everyone knows about it :). Because of the size of a project like KDE, if you do these things, the advantages, speed and efficiency just multiply exponentially. I'm sure we all know this anyway.\n\nNone of this is code-specific in any way, but then again, good, productive programming never is - it is unseen a lot of the time unfortunately.\n\nJust a couple of pennys' worth."
    author: "David"
  - subject: "Helpful GCC options"
    date: 2004-03-26
    body: "As something useful for open source developers; compile releases with \"-O -Wall -Werror\", and make sure it compiles (obviously, -O could be -O2 or -Os, or whatever). This turns all warnings into errors, and makes sure you can fix them. Where a warning is something silly (some lints require you to cast the result of printf() to void), it forces you to put something into the code that makes your intent quite clear; -Wall only turns on warnings that make your code unclear, and thus helps with making sure it's correct. -W also helps with finding errors, since it warns where your code is doing something where the \"natural\" interpretation doesn't fit the spec.\n\nIf you're developing code, it's well worth turning on as many warnings as possible (\"info:/gcc/Warning Options\" for KDE users who've got the info documentation installed), and using -Werror to make sure that your release gives no warnings on your compiler. It won't catch every bug, but it'll force you to look at code that's not 100% clear and perfect.\n\nFWIW, when I'm finishing code, I use \"-O2 -Wall -Werror -W -Wfloat-equal -Wundef -Wshadow -Wpointer-arith -Wcast-qual -Wwrite-strings -Wconversion -Wredundant-decls\". It's a lot of warnings, but even though I'm the only user of my code, it cuts down on the trouble I have if ever I want to go back to an old project and play with it. Plus, if ever I think my code is worth releasing (usually it's not), I've got it into a nice state."
    author: "Simon Farnsworth"
  - subject: "Good Stuff"
    date: 2004-03-25
    body: "A nice article IMHO, two things came to my mind while reading it:\n\n- Regarding the NULL pointer issue, in particular setting 'ptr' to 0 after calling delete on it (or rather, what it points to): it might be a good idea to mention QGuardedPtr in that context (IIRC QGuardedPtr does just that, deleting something and setting the pointer to zero in the dtor. Just the name is a little suboptimal).\n\n- Regarding iterators: a very common mistake I spotted is that iterators are acquired from temporary lists. Usually, this happens if some function returns a list (e.g. a QValueList) by value and you fail to make a local copy. So, assuming you have a function 'QValueList<int> numbers();', you shouldn't do\n\nQValueList<int>::ConstIterator end = numbers().end();\n\nbut\n\nQValueList<int> list = numbers();\nQValueList<int>::ConstIterator it = list.end();\n\nor something like that, I hope the point I'm trying to make became apparent.\n\n- Frerich\n\n\n"
    author: "Frerich Raabe"
  - subject: "Good Stuff"
    date: 2004-03-25
    body: "A nice article IMHO, two things came to my mind while reading it:\n\n- Regarding the NULL pointer issue, in particular setting 'ptr' to 0 after calling delete on it (or rather, what it points to): it might be a good idea to mention QGuardedPtr in that context (IIRC QGuardedPtr does just that, deleting something and setting the pointer to zero in the dtor. Just the name is a little suboptimal).\n\n- Regarding iterators: a very common mistake I spotted is that iterators are acquired from temporary lists. Usually, this happens if some function returns a list (e.g. a QValueList) by value and you fail to make a local copy. So, assuming you have a function 'QValueList<int> numbers();', you shouldn't do\n\nQValueList<int>::ConstIterator end = numbers().end();\n\nbut\n\nQValueList<int> list = numbers();\nQValueList<int>::ConstIterator it = list.end();\n\nor something like that, I hope the point I'm trying to make became apparent (the problem is that in the former version, the iterator gets invalidated at the end of the line since the list it was acquired for got destroyed, as it's a temporary).\n\n- Frerich\n\n\n"
    author: "Frerich Raabe"
---
I'm very happy to announce a new document for inspiring KDE hackers, entitled "<a href="http://developer.kde.org/documentation/other/mistakes.html">Common Programming Mistakes</a>". The document aims to combine the experience of many of the top KDE developers about the Qt and KDE frameworks dos and don'ts. The way they were usually passed on to the next generation was by letting the youngsters make the mistakes and then yell at them in public. We will go over things, which are not necessarily bugs, but which make the code either slower or less readable. The document will be expanding as we see the need for it.




<!--break-->
