---
title: "KDE 3.3 About to Finish: Public Release Candidate 2"
date:    2004-08-10
authors:
  - "skulow"
slug:    kde-33-about-finish-public-release-candidate-2
comments:
  - subject: "Hope the kio slaves get fixed before final."
    date: 2004-08-10
    body: "e.g. http://bugs.kde.org/show_bug.cgi?id=66411 makes sftp unusable.\nAnd there are many more bugs that make kio slaves unusable in practice, see http://tinyurl.com/63l28"
    author: "testerus"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-10
    body: "Unusable, yes. And please fix these bugs too: <insert_link_to_all_bug_reports>"
    author: "Anonymous"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-10
    body: "trolls usually post anonymous..."
    author: "[Lemmy]"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-10
    body: "Good that you gave your full name and email address. You are not familiar with irony, or?"
    author: "Anonymous"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-10
    body: "Well for things to be fixed you need a developer with time, knowledge and a SSH access. It seems that these conditions are currently not met for FISH and sftp.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-10
    body: "> http://bugs.kde.org/show_bug.cgi?id=66411 makes sftp unusable.\nIt is prefectly usable for me since I do not have to worry about encoding issues. If people that use the software and experience the same issues as you do not step up and offer to fix such problems, it will never get fixed. I personally started maintaining the sftp io-slave on a part-time basis because it was completely neglected and abondened by the original author.\n\n> And there are many more bugs that make kio slaves unusable in practice, see \n> http://tinyurl.com/63l28\n\nPlease stop making nonsense argument like this. Just because there are bugs in some io-slaves, which BTW might or might not be still valid, does not mean that they are \"unusable in practise\". Did you even bother to go through some of these bugs and check which ones are valid and which are not ?"
    author: "Dawit A."
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "Why would someone want to use umlauts in file names? "
    author: "Norman Urs Baier"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "Yes, what a stupid thing to do. Especially since the whole filename encoding issue is just so perfect under Linux - who needs to know which character encoding was used for a filename? And didn't you know that if your first language employs both ASCII and non-ASCII characters, it's just so cool to drop in transliterations and bizarre combinations of ASCII characters instead of using such \"archaic\" characters as \u00fc and \u00f8; it just shows how \"elite\" you really are.\n\nReality check! Ignoring the 0.0001% who consider themselves \"elite\" in the way described above, most people whose first language uses non-ASCII characters are likely to find the lack of proper support for their language *unfathomable* and *inexcusable*.\n\nAnd as for the Linux filesystem issues, I imagine that it's unlikely that they'll be resolved for real-world users any time soon, thanks to a combination of buck passing, indecision, and the lead time on distros taking any decent solution up after people finally get their 'rses in gear."
    author: "The Badger"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "I do not quite understand your argument. I am a russian speaker, and I have files named in both russian and english letters. Ever since I have given up on the encoding war and switched to utf-8 I don't have any problems with file naming."
    author: "Jeld The Dark Elf"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-13
    body: "Yes, but switching to UTF-8 is a big problem in itself. Moreover, last time I checked, there wasn't any widespread mechanism in place in Linux (which is a pretty major platform for KDE, after all) to force users to actually use UTF-8 when naming files - to the kernel and most filesystems, filenames are just byte strings and users can choose different locales and blast in character combinations which are illegal in UTF-8 but which look alright in each user's own chosen character encoding.\n\nIt's a nasty situation, but significant projects like KDE should be putting pressure on the Linux people (amongst others) to fix the insanity."
    author: "The Badger"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "This seems to be a big problem in KDE or maybe mimetypes in general (well big as soon as you start to dig).\n\nIf you look at the X clipboard most things will put text as a mime type of \ntext/plain;encoding, where encoding is something like UTF8 etc...\n\nAlthough thease 'extended' mime types are used quite frequently, try telling KDE[QT] your document is type text/plain;UTF8, not just text/plain."
    author: "Oliver Stieber"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "As this question sometimes pops up I\u0092ll try to clarify this problem.\n\n1) Normal users often want to use file names that describes the content of a file in their native language.\n\n2) Calling all characters that carry dots or circles \u0093umlauts\u0094 somewhat transforms the question to a esthetical one, which it in no way is. I will use Swedish as the example:\n\n The Swedish alphabet consists of 28 characters; in addition to the once shared with English we have 3 additional vowels:  \u00e5, \u00e4 and \u00f6. These are as related to a or o as i are to u in English. \u00c5 is written as an A with a circle on top while U is written as to I:s connected by an arc at the bottom. I do not know how to further emphasise this so I conclude by again stating that they are separate letters pronounced totally different from a and o. \n\nIf these letters can\u0092t be used the language loses 3 out of 9 vowels. The effect is the same as if you would remove two vowels from written English (2/5). For a demonstration try replacing all u:s and o:s with a:s in any English text. Some words will change their meaning while others will just turn into gibberish. \n\nI have a colleague whose last name is \u0093\u00c5s\u0094 meaning ridge as in \u0093mountain ridge\u0094. In this situation his name would be transcribed as As meaning \u0093carcass\u0094, in Swedish used as a derogatory term. This is funny the first 3 times but I guess he appreciates systems where he is able to use his real name. \n\nNormal users expect to be able to use any computer system in their native language including naming files, especially if the user interface is translated, which is exemplary done in the case of Swedish. \n\nBest regards\n/Olle."
    author: "Olle H\u00e5stad"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "Well, your explanation made sense to me, probably because I've been acquainted with the facts you provide for some time. Indeed, I imagine that they are well and willingly understood by everyone except the Commodore 64 brigade."
    author: "The Badger"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: "P\u00e5gen, \u00c5kerman and so forth show very effectively how important the \u00e5 is in names, they just write a for their international products. This is the strategie I know also from my girlfriend. Theoretically it would be possible to write ao and oe instead of \u00e5 and \u00f6. Which probably is also the way these letters were constructed some years ago, any calligrapher around? \nPersonnally, I insist on being able to use special caracters (for simplicitys sake called umlauts before) in normal text, but we are talking about filenames here. I have big difficulties to show any comprehension to people wanting spaces and special caracters in their filenames. This searching for trouble."
    author: "Norman Urs Baier"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-11
    body: ">P\u00e5gen, \u00c5kerman and so forth show very effectively how important the \u00e5 is in names, they just write a for their international products. \n\nIt is possible to just use A instead of \u00c5 but this transforms a name with a meaning as in your examples to just names (without meaning). Another example of this is the Swedish company \u0093Sk\u00e5nska cementgjuteriet\u0094 (meaning Scanian cement foundry) changing their name to the internationally more useful name \u0093Skanska\u0094 (without any meaning). But this in not very useful when the most important thing you want to preserve is the meaning of the word, as in a filename. Otherwise all files could be known only by the inode number.\n\n>This is the strategie I know also from my girlfriend.  Theoretically it would be possible to write ao and oe instead of \u00e5 and \u00f6. Which probably is also the way these letters were constructed some years ago, any calligrapher around? \n\nIf you by \u0093some years\u0094 refer to the 14th century you are correct. :) \nIt is possible to use aa, ae and oe to transcribe Swedish text. This is however neither used nor easily understood by normal people. Remember that it has to be practical for the average user. \n\n>Personnally, I insist on being able to use special caracters (for simplicitys sake called umlauts before) in normal text, but we are talking about filenames here. I have big difficulties to show any comprehension to people wanting spaces and special caracters in their filenames. This searching for trouble.\n\nI would not call it special characters, as they are not more special than, say g is, to a Swede. When talking about special characters you adopt a nationality. To a Swede W could be considered a special character as it is only used in foreign words and names. \n\nWhen saving a document in MS word, the save dialogue box will default to a name consisting of the first words from the text. This suggested name often describes the document fairly well and most users use this as the filename with minor changes (and, yes I know this is to ask for trouble when moving the file between different OS:s, but not for that user on his own system). As a grad student I used to be responsible for the computers in my research department, mainly consisting of windows computers (95 to 2000) with some Macs (Os 8 to X) and linux servers. The working language for everybody was English. Most people however preferred to give files names that made the files easily found and identified. A longer name consisting of several words in the users native language fulfils those demands. Over the years the interchange of files went progressively smoother, not as a result of me constantly preaching the gospel of \u0093English file-names without spaces\u0094, but as a result of improved interoperability of the computer systems. \n\nThe point of this is: \nIt is much easier to make the computers adapt a naming scheme that is comprehensible for normal people than to constantly re-educate people to the ever changing capabilities of computers. I know of the problems in doing so, but it has to be done sooner or later.\n\nI often use fish or sftp to access DNA-sequence files saved from a dedicated Mac Os9 sequencing computer on a Win2000-server through an smb-mount on my linux-server to my kde laptop, which I work on through a cygwin X-server on my windows XP workstation. Well, it is not optimal but this is the way it is in many universities and the KDE end of it usually works like a dream. \n\nIt is however very difficult for me to make the rest of the department adjust to my \u0093safer and superior\u0094 file-naming scheme, when their intuitive and simple method of just describing the file (meaning, not name) in Swedish works perfectly for them on their computers.\n\nBest regards\n/Olle."
    author: "Olle H\u00e5stad"
  - subject: "Re: Hope the kio slaves get fixed before final."
    date: 2004-08-12
    body: "Your reply was diplomatically well written in a way that I just agree, especially as we are both considering the KIO Slave behaviour a bug.\nUnfortunately we did not get any further what the solution concerns. How much coding effort would it take to eliminate that bug?"
    author: "Norman Urs Baier"
  - subject: "Please report regressions here."
    date: 2004-08-10
    body: "bugs.kde.org has approx. 7000 open bug reports, we can't fix those before KDE 3.3\n\nWe do like to fix anything that got broken by KDE 3.3 though. If there are things that used to work in KDE 3.2.x but that no longer works in the KDE 3.3 beta / RC, please post it here."
    author: "Waldo Bastian"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "what is the reason of this rush release?"
    author: "James"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Why do you think the release is being rushed?"
    author: "Amadeu"
  - subject: "Gnome Fear"
    date: 2004-08-11
    body: "It's SuSE vs. Ximian at the Novell Coral!"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "If we can release a KDE 3.3.0 today that is better than KDE 3.2.3, why should we wait with it till tomorrow?"
    author: "Waldo Bastian"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "I think that it depends -- depends on how long till the next release.\n\nThe GIMP released 2.0.0 which was rather buggy but they have already released 5 (count them FIVE) bug fix releases.  This is the release early and often approach.  Perhaps 2.0.0 should have been called a release candidate.\n\nOTOH, if we intend to be waiting like 6 months till 3.3.1 then we should try to get more of the bugs fixed before it is released.  This approach is more like commercial software.\n\nIt also depends on how stable our customers expect an X.y.0 release to be.  If they expect stable, then we could still release 3.3.0 today, but we should call it an RC.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "KDE 3.3.1 will be rather released 6 weeks, not months, after KDE 3.3."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-14
    body: "We can only hope that in 6 weeks that at least the serious problem in the window manager can at least be turned off and the the desktop icon spacing problem is fixed.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Please report regressions here."
    date: 2004-08-16
    body: "you know i'm getting pretty sick of your pissing and moaning JRT. yes your finding bugs and your a help to the KDE QA team but god damn you can be an arrogant sod. Perhaps instead of proclaiming tiny bugs in the mailing lists as \"SHOWSTOPPERS\" you should get a clue, chill out and start being more polite to the developers that make linux useable."
    author: "ac"
  - subject: "Re: Please report regressions here."
    date: 2004-08-16
    body: "I don't piss and I don't moan.  I try to be direct and tell the truth as I see it.  For some reason, developers take this as a personal insult.\n\nIn my not at all humble opinion, it is the developers that need to get a clue.  They need to somehow see things from the users perspective.  I am sorry that I don't have humble opinions -- that is the personality that I was born with and I am probably too old to change it.\n\nAs I see it, a significant number of developers have been from somewhat to very rude to me and this has resulted in a somewhat negative attitude on my part.  Perhaps this is because I forgot to kowtow to the developers.  I would be happy to treat the developers as equals, but I get the impression that they are not willing to concede to such a large upgrade to my status.\n\nI realize that there is a large culture gap between an engineer and the hackers, but I am willing to try to meet them half way.  Perhaps if you could explain what you expect in the way of being \"polite\" it would help.  I was trained as an engineer.  I learned that culture and part of that is being direct and objective.  If others see this as being impolite then it is actually their problem.\n\nI think that the best example is that I took the time to explain to someone exactly what was wrong with his application and what needed to be changed to fix it and he took it as a personal insult.  This is the culture gap -- another engineer would have thanked me for the help (after a while he figured out that it *was* help).\n\nAnyway, since the 3.1.0 release we have had various problems with some of the releases ranging from things that wouldn't compile to serious regressions.  To a professional engineer, this is totally unacceptable.  I will say it straight out to all the developers: this is NOT the way to do things.  They really need to get a clue and figure out how to avoid this in the future.  If we expect for governments and major corporations to adopt out software for their desktops, we MUST have better quality control.  Why is it so hard for the developers to understand this -- it is obvious to me.\n\nThere is no excuse for the problem with the vertical icon spacing and the cute little comment in the code is indicative of the problem with the attitude of some of the developers.  This should have been a show stopper because it is (1) a serious bug and (2) a regression.  If you think that it is a tiny bug, it is your attitude that needs adjusting.  Many users have many icons on their desktop.  When they upgrade and open their desktop for the first time, their icons will be sprayed all over the place.  There is nothing that upsets users more than this happening.\n\nAs I said elsewhere, the broken \"feature\" in KWin that you can't even turn off is more than just a major bug.  It indicates that the development model needs some work -- this should have never been committed to the main branch in CVS before it was designed and tested.\n\nIn the case of the 3.3.0 release there is a simple solution to the problem, call it 3.3.0RC1 and then in 6 weeks fix all of the serious bugs and regressions and release 3.3.0.  But that is just a quick fix, we need to develop better methods.  If we don't, the new releases will get worse rather than better -- like that other wellknown desktop software did after the 3.10 release.\n\nI am here to help.  If, as it appears, the developers don't want help, then I am willing to just give up on KDE if that is what they really want.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Please report regressions here."
    date: 2004-08-16
    body: "I am not a KDE developer, but I can understand why software developers find you irritating.  The programmers do deserve your respect, they are the ones putting their time and effort into development; it is no small amount of work.  \n\nIf you want bugs fixed, you need to do more than find them; you must convince someone with the power to fix it that it should be fixed.  Getting rude is not the answer.  Proclaiming yourself to be a peer to the programmers in this project, when the amount of work you put in is a fraction of the work that they put in, is not the way to do it.  How about doing something useful such as establishing priorities?"
    author: "Ben Damm"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "It was decided to release KDE 3.3 before aKademy.\n\nSo perhaps the decision was good, perhaps it was bad but it is not the time anymore to discuss about it (that was at last in May 2004 to do.)\n\nThere is still KDE 3.3.1 (and later) and this is not the end of the world.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "> It was decided to release KDE 3.3 before aKademy.\n\nIf applicable, not just for the sake of it. But at the moment there doesn't seem to be a reason not to reach this goal."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "> There is still KDE 3.3.1 (and later) and this is not the end of the world.\n\nExactly, there is always an \"But you can't release with this bug!\" outcry before every major release. But what shall the developers do if no patch to fix it is available (after days or even weeks). It has to be released eventually."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "You misunderstand the release process. We are not looking for perfect software, otherwise there wouldn't ever be a release. 3.3 has fixes and improvements function wise that you and others may enjoy or need. So development is stopped for a bit, or slowed down, everything is stabilized to a reasonable degree, and it is released.\n\nNow you and others can use it, find flaws or shortcomings, fix them and forward patches.\n\nThen 6 or so months from now another release will contain your bug fixes.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: ">bugs.kde.org has approx. 7000 open bug reports, we can't fix those before KDE 3.3\nRight. But the broken kio stuff is rather annoying. The amount of bugs and the average response time doesn't really encourage to report new (in fact old) bugs. I don't want to insult anyone - of course - but I'm holding bug reports back, because I see not much sense in it, to report them... Btw.: We had a Gentoo user, who reported that holding down <F4> when running konqueror is a fast way to start lots of konsoles. Did he report the bug/is this fixed in 3.3?"
    author: "Carlo"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "<20 konsole windows later> Nope, it still does that."
    author: "Waldo Bastian"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Only 20? Lucky. I had a couple more when I tested it over here. \n\nWell, it is to be expected since on my quite busy box it took a while to appear, so I tried again :)\n\nJaco"
    author: "Jaco"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "40 konsoles later...\nOh damn man. havent known that. bad thing *g*"
    author: "Hans-Peter Schadler"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "I thought it didn't work here, so I tried a few times.. Got 208 konsoles a few minutes later. Arghh! \nDie die die."
    author: "Allan S."
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "The problem is that keyboard shortcuts are subject to the keyboard repeat rate.\nShould not be too hard to fix, but that might break applications that depend on such behavior, so it is not something we are going to fix for KDE 3.3 any more.\n\nThe problem also exists in KDE 3.2.x, it is not new in KDE 3.3"
    author: "Waldo Bastian"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Wow, I had to reset my machine after pressing F4 for three seconds. It became completely unresponsive because of the heavy swapping. After 15 minutes, I pressed the reset button..."
    author: "Dik Takken"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "hmm, couldn't reproduce it here on my gentoo box.\n\n<f4> opens just one xterm, and this gets the focus immediately, so the next F4 key events go to xterm and does not open another one.\n\nbut maybe that's just when using xterm's or Ion as the window manager."
    author: "MaxAuthority"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "xterm loads faster than the keyboard repeat rate. Konsole takes a few seconds."
    author: "Dik Takken"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "And what's the problem with that?\n\nIf I press ctrl+alt+backspace on someone else's machine, my X session resets.\n\nIf you don't like the default behaviour, change it. (in my opinion, it's better to have default keybindings than to have to assign them by yourself)"
    author: "Peter"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Why don't you look up it yourself at http://bugs.kde.org if he reported it?"
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Did you ever search for \"F4\"? Even in conjunction with another keyword it is hard to estimate, if your result set doesn't include the bug or if it does not exist; And the bug is an ugly one, but not so important that it's worth a dupe. Also we have better things to do, than to forward _all_ bugs Gentoo users find."
    author: "Carlo"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "> Did you ever search for \"F4\"?\n\nI wouldn't do that, \"Konqueror open terminal\" seems to more appropriate.\n\n> Also we have better things to do, than to forward _all_ bugs Gentoo users find.\n\nAnd the KDE developers have better things to do than to scan the mailing lists of all distributions for descriptions of possible KDE bugs. If you don't care to report it then don't moan that it stays unfixed!"
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: ">If you don't care to report it then don't moan that it stays unfixed!\nI don't moan. And definitely not about unreported bugs. I'm aware that fixing bugs is not (always) simple and that some bugs/wishes need major changes to the relevant source code (and time). It's just that the perception from the outside is a steadily growing bug number and more features (which isn't bad), but a lot of remaining odd bugs from one version to the next."
    author: "Carlo"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: ">perception from the outside...\n\nAnd the perception from the inside is a continuous flow of bugs and desired features. KDE is noteworthy for accepting contributions from newcomers. If you are interested in a stable KDE, patches are welcome.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "http://bugs.kde.org/show_bug.cgi?id=60625\n\nI've had this bug reported for about a year now.  The same \"repeated keyboard shortcuts\" happens with just about all shortcuts, including control+w to close a tab in konqueror.\n\nSometimes you accidently hold it down for a half second longer than you meaned to, closing all your opened tabs, or at least the good majority."
    author: "Sean Lynch"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "> The amount of bugs and the average response time doesn't really encourage to\n> report new (in fact old) bugs\n\nWhile reporting old bugs doesn't make sense, I've had rather good luck with reporting new ones. When I've kept pursuing them until the problem can be reproduced, I've never had to wait more than a release cycle for a fix. The process is a bit like raising an infant: if you expect him/her to start talking tomorrow, you'll be disappointed, but with some patience and investment on your part the progress over time is remarkable."
    author: "TDH"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: ">While reporting old bugs doesn't make sense\nDepends on the definition. A bug in 3.1 still to be found in 3.2 or 3.3, but never reported, is an old bug to me. You don't want to get it fixed?"
    author: "Carlo"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "this is clearly my experience too."
    author: "superstoned"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "ack 20+ console windows\n\n- Gentoo 2004.2\n- KDE CVS compiled Sun Aug  8\n- Fluxbox WM \n\n(yes, no KDE running, konqueror&, hold F4, screen filled with konsoles)"
    author: "xiando"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "\"bugs.kde.org has approx. 7000 open bug reports, we can't fix those before KDE 3.3\"\n\nHow many of those bugs are still relevant? I mean, how many of them are from old version of KDE that are not that widely used anymore? Or bugs from old version that have been fixed in newer version? Maybe the reports should be checked for reports that are not relevant anymore. That way we would have a bit more realistic idea about the number of bugs."
    author: "Janne"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Indeed. And the good news is that everyone can help with that! Go over to bugs.kde.org check out your favorite bugs and use the comment field to report whether the issue can still be reproduced with KDE 3.3!\n\nOldest bugs first please."
    author: "Waldo Bastian"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "\"And the good news is that everyone can help with that! Go over to bugs.kde.org check out your favorite bugs and use the comment field to report whether the issue can still be reproduced with KDE 3.3!\"\n\nYou know what? I will be doing exactly that :)!"
    author: "Janne"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "in advance: thank you. \n\nthe more people combing the bug database and providing useful feedback (stress on the word \"useful\") the better."
    author: "Aaron J. Seigo"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "perhaps someone can organize a bug cleansing day--- it's worked well for the gnome and kontact folks."
    author: "anon"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "It works well? bugzilla.gnome.org (not including eg Evolution) has 12600+ open reports (including wishes, KDE has 13700+). So not really such a difference and bug days don't seem to be the solution everyone is despairingly seeking for such report masses."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Look at /unconfirmed/ bugs at gnome compared to kde. "
    author: "anon"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Unconfirmed: GNOME 3300+, KDE 8800+. Dunno what conclusion you can draw from this: Does GNOME confirm (many) wishes? Are GNOME confirmed reports regularly retested if they are still current?"
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "> Maybe the reports should be checked for reports that are not relevant anymore.\n\nYou're really the first one with this idea. Please start doing that immediately!"
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "I will. How about you?"
    author: "Janne"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "I will watch your comments."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "This is something even non-programmers can do, and for any bug database. It helps programmers spend more time coding, too.\n\nSo, search on an area you're familiar with using, and get testing!\n\nIf the bug is still valid, but incomplete, reducing it to the simplest testcase possible helps.\n\nIf the bug is no longer valid, comment with which relevant versions you have, and how you tested.\n\nDon't just make comments of the form \"I NEED THIS FIXED KTHX BYE\" though. It makes the bugs harder to read, and therefore means they are *less* likely to be fixed quickly."
    author: "a mysterious anonymous poster"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Roughly 750 bugs are from KOffice, so they are not relevant for KDE 3.3\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "If you start like this, then there are also k3b, valgrind, boson and other wrongly counted into the 7k amount. :-)"
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "While I'm at it - Kate/Settings/General Options/Startup:\n\nenable all three checkboxes.\n\n1. edit some file, save and close kate, delete the file, open kate, nice useless warning!? btw. could such warnings be added to a log tab, instead having to click  a pupup dialog out of the way?\n2. open kate, open another file with kate from cmd - great: did I say I want a second instance? No.\n3. play around with the above and watch the file list handling\n4. still too much broken/ugly syntax highlighting"
    author: "Carlo"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "I have a problem with KOrganizer crashing right when I try to start it. And I can't exit from fullscreen in any KDE app. Other things work very nicely thanks!"
    author: "Senatore"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "> And I can't exit from fullscreen in any KDE app\n\nOuch, confirmed. Bad one."
    author: "..."
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Not confirmed, try qt-copy with patches applied."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Tried with qt-copy from KDE CVS and both problems are still here. Are there any extra patches I need to apply?"
    author: "Senatore"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "I meant those in qt-copy/patches/ which are to be applied with apply_patches script."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "Using QT 3.3.3 from qt-copy now and all patches applied all kde recompiled and both problems are still here :(\n\nAnd it looks like I'm not alone with these two bugs. They have been reported some time ago:\nhttp://bugs.kde.org/show_bug.cgi?id=84979\nhttp://bugs.kde.org/show_bug.cgi?id=85877"
    author: "Senatore"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Is this a 3.3 regression (horrid rendering)? AFAIK this used to work. However, its msnbc so you never know :/\nhttp://www.msnbc.msn.com/id/5614334/\n\nAnother sure-to-be regression on my boxes seems to be that parts of icons occasionally vanish (especially when using konqueror as a file manager - folders, or parts of a folder icon, disappear until mouse is moved on top of them). Oh - and it might be the same bug - while writing this edit boxes (name, email, attachment) disappear in similar manner (post reply form). Box is SUSE 9.1 with latest Nvidia shonk."
    author: "jmk"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Konqueror's icon view, using my regular user account, doesn't show any previews anymore. A fresh user's konquerer does. There must be something wrong with the config -- but I'm not sure what."
    author: "Boudewijn Rempt"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "The same for me.\nIt does  it on Mandrake 10.0 (kde 3.2.0 pre) and on KDE 3.3 beta.\nIt stopped working when I asket for bigger previews.\n\nGerard"
    author: "Gerard Delafond"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "> Konqueror's icon view, using my regular user account, doesn't show any previews anymore\n\nSame here."
    author: "Anonymous"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-10
    body: "Ok, I haven't tried KDE3.3_rc2 so it's possible that the problems I have discovered in KDE3.3_beta2 are already solved. I have noticed three things that worked in 3.2 but not in 3.3(beta2).\n- in kopete from KDE3.2 I was able to save the password \"insecure\" and therefor didn't need to use kwallet for kopete. I have kopete in my autostart so it's kinda anoying to enter a password everytime I login. The real solution is to make it possible in kwallet to allow no passwords (bug 78505), but in KDE3.2 I was able to bypass kwallet for kopete. In 3.3 (beta2) I needed to disable kwallet because a mandatory (extra) login was too anoying. Unfortunately I need to remember (or administer) logins on websites now. I think this is a degradation in usabillity compared to KDE3.2\n- although it doesn't bother me to much but I noticed that flash doesn't work anymore in KDE3.3_beta2. Needless to say this worked fine in KDE3.2\n- It's now necessary in Remote Desktop Connection to have rdesktop 1.3.2 installed if you want to connect to a RDP-client. Unfortunatelly rdesktop 1.3.2 hasn't been released yet. I know this problem will solve itself eventually. We are using KDE for our administrator servers in the company I work and use Remote Desktop Connection a lot to administer our clients. We won't migrate to KDE3.3 before rdesktop 1.3.2 is released or the distribution we use (Gentoo) provide a patched rdesktop. This worked in KDE3.2 as well.\n\nOf course there are more wishes/bugs to fix in KDE3.3 (+ 7000), but you explicitly asked for \"degredations\" compared to KDE3.2.\n\nJust for the record; I think KDE3.3 will be an outstandig release and I'm looking forward to the final release. Besides the items I mentioned above I have noticed a lot of improvements that will make KDE even better."
    author: "AC"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-10
    body: "Forgot one thing. I noticed that in KDE3.3 (beta2) I have lost the \"Metadata-tab\" when I view the properties of an OGG-file (vorbis). When I boot with KDE3.2 I have no trouble viewing the metadata of OGG-files."
    author: "AC"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-11
    body: "There haven't been any changes to the Ogg KFile plugin since KDE 3.2 (other than some capitalization changes).  Make sure that you installed kdemultimedia with 3.3 and that you have the appropriate libraries installed when rebuilding (i.e. read the configure output)."
    author: "Scott Wheeler"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-11
    body: "Unless the library requirements have changed for KDE3.3 I'm all set. I have build KDE3.3(beta2) including kdemultimedia on the same machine as KDE3.2 (gotta love Gentoo slots) I can see the metadata from all other filetypes (e.g. mp3, avi) but not ogg. When I load KDE3.2 I can see the metadata from ogg-files just fine. I haven't checked the configure output but if nobody else have problems with ogg-metadata then it will be a problem with my setup (or the Gentoo-ebuild, but I find that hard to believe). Anyway if I install the RC and still have problems I will diagnose a little better and report it to bugs.gentoo.org or bugs.kde.org depending on configure output.\n\nThanks for the feedback."
    author: "AC"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-11
    body: "I have also problems with meta information. Initially Konqueror icon view does not show meta info tooltips. But when you change to Info List View you see that kfile plugins are working! When switching back to Icon View you also have meta info tooltips there - until you press e.g. Reload then they are gone again."
    author: "Anonymous"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-11
    body: "Kopete Issue - THe reason this has never been looked at is because the problem is really in KWallet, as you point out. However. there is a trick you can do to work around this - if you happen to have an old copy of your $KDEHOME/share/config/kopeterc around. \n\nKopete will check that file for the unencrypted passwords *before* it querys the wallet. So, if you set RememberPassword=true for all those accounts, and copy over the obfuscated password field, Kopete should not prompt you for the wallet.\n\nFlash Issue - Flash works fine for me in current KDE CVS. Are you using the latest flash plugin? Does it show up in your Konqueror plugins listing?"
    author: "Jason Keirstead"
  - subject: "Re: Things that worked in KDE3.2 but not in KDE3.3"
    date: 2004-08-11
    body: "> However. there is a trick you can do to work around this - if you happen to have an old copy of your $KDEHOME/share/config/kopeterc around\n\nNice tip, I don't have an old copy around but I can make a new one. I will try this later.\n\n> Flash Issue - Flash works fine for me in current KDE CVS. Are you using the latest flash plugin? Does it show up in your Konqueror plugins listing?\n\nI have version 6.0.81 installed and the plugins show up in the Konqueror plugin listing. I will try later with version 7.0.25.\n\n\nThanks for the feedback."
    author: "AC"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Requirements update (http://www.kde.org/info/requirements/3.3.php)\n\nSeems that kdemultimedia (namely, JuK) now would like to have tunepimp and musicbrainz installed, neither of which are listed on the above page.  Also, taglib is a biggy for that packages, and it is also not listed on that page.\n\nCan we get an update to the requirements on the status of those libraries (and any others that people can think of that aren't listed...)"
    author: "Caleb Tennis"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Open Motif which is listed as a dep of kdebase can be removed, Iebelive, as the nsplugins are now free of that dependency."
    author: "Caleb Tennis"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "1) In kmail, when clicking the URL to display a html mail, it doesn't show the html, but the attachment icon. If you click again on the html document in the message structure viewer, it works again.\n\n2) I just tried to type in the URL bar in konqueror (RC1), but it wouldn't work. With a new konqueror, it does work.\n\n3) kget segfaults when starting: *** KGet got signal 11\n\nI compiled it in gentoo\n\n\nGood luck, and many thanks for a great desktop environment"
    author: "Marko van Dooren"
  - subject: "Re: Please report regressions here."
    date: 2004-08-10
    body: "Oh yeah, and none of my keyboard shortcut work anymore until I make a change to a shortcut (even setting it to the same value) and apply it.\n\nGreetz"
    author: "Marko van Dooren"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Kget has been having the same trouble here also, with all of the kde 3.3 releases."
    author: "JW"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "2) and 3) were fixed, dunno about 1)"
    author: "Lukas Tinkl"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "3) is not fixed, KGet still crashes when started from KMenu.\nHowever, it works fine when started from Konsole or from\nwithin Konqueror.\nThe really strange thing in KGet is that the crash handler\n(DrKonqi) is never shown... "
    author: "Thomas McGuire"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "Personally I've found KGet pretty unstable in beta 2 - it segfaults quite regularly while downloading, and next time it gets started the download is gone, although it seems to resume correctly most of the time when you re-add it. It doesn't seem to matter where you start it from, although I usually start it from within Konqueror.\n\nI am just installing rc2 and will submit a bug with more info, assuming there isn't one there already (last time I looked at b.k.o. there were quite a few crash bug entries logged against KGet)."
    author: "Paul Eggleton"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "This is now fixed in RC2 or in HEAD.\nIt crashed exactly every 10 minutes, when KGet tried to save the transferlist."
    author: "Thomas McGuire"
  - subject: "Re: Please report regressions here."
    date: 2004-08-13
    body: "This does seem to be the cause, although it is not fixed in RC2 (Tobias Koenig's bugnote says KDE_3_3_0_BRANCH and HEAD).\n\nhttp://bugs.kde.org/show_bug.cgi?id=86280"
    author: "Paul Eggleton"
  - subject: "REGRESSION: Kweather"
    date: 2004-08-11
    body: "Countries that were available in kweather in kde 3.2.x are gone.\n\nFor example:\n\nAsia/Thailand (had several cities in kde3.2.x) is gone.\n\nThere is a bug report that is not progressing much:\nhttp://bugs.kde.org/show_bug.cgi?id=70745"
    author: "JW"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "You make a good point that regressions are more important to fix.\n\nWhen I report what I think is a regression, I start the title in bugzilla with:\n\nREGRESSION:\n\nI would suggest that this be made the standard practice and that bugzilla be programed to have a canned search (like JJ) for this.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "KSysguard flickers extremly when system is under 100% load, worked fine a few weeks ago (I think)."
    author: "Thomas McGuire"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "1) Bug #82090 is a regression.  If I want to use multiple tabs and ever want to scroll the first tab, I need to use one of two workarounds:\n\nOpen a dummy first tab that I will then close after I have two real tabs open.\nOr, deselect 'Hide the tab bar when only one tab is open' so that the tab bar is always visible.\n\nIf I don't do one of these, scrolling on the first tab results in newly visible areas being black.\n\n2) I also had the problem that the Name, Email, and Title edit boxes only had the left 10 pixels or so visible until I moved the mouse over them which resulted in a repaint.  That bug may have been around in 3.2, though, too, so it may not be a regression."
    author: "AC"
  - subject: "Re: Please report regressions here."
    date: 2004-08-16
    body: "The repaint problem is not specific to Konqi.  Every once in a while, kmail does it to me too."
    author: "Jeff Stuart"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "There is an irritating bug in KDEGames/Kreversi\n\nWhen you start a new game, you can not choose the color. Instead you start out as black and if you want to play white you can choose \"Game->Switch Sides\" in the Game menu.  When you do this, the computer says something like \"Your score will not go into the highscore list (I am not at home right now so I can't check the exact message).  \n\nThis is generally ok, since switching sides could be seen as cheating.  It is, however, *not* OK before the first move.  This way you can never play white and get your score into the highscore list, at least not on the first move.\n\nThis worked fine in 3.2.   "
    author: "Inge Wallin"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Thanks, fixed."
    author: "Waldo Bastian"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Insane rendering with any computerworld article. Example :\nhttp://computerworld.com/softwaretopics/os/linux/story/0,10801,95163,00.html"
    author: "jmk"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "Hello,\n\nI have some one line patches to fix some issues:\n\n== Universal SideBar flickers too much ==\n   http://bugs.kde.org/show_bug.cgi?id=86884\n   Patch: http://bugs.kde.org/attachment.cgi?id=7057&action=view\n\n== Fix QTimer delay in KIconDialog ==\n   http://bugs.kde.org/show_bug.cgi?id=86680\n   Just the Patch: http://bugs.kde.org/attachment.cgi?id=7067&action=view\n\n== Correct wrong icon size in KPropertiesDialog ==\n   http://bugs.kde.org/show_bug.cgi?id=86257\n   Patch: http://bugs.kde.org/attachment.cgi?id=7056&action=view"
    author: "Gustavo Sverzut Barbieri"
  - subject: "Re: Please report regressions here."
    date: 2004-08-11
    body: "The gg: shortcut still does not work with my minicli (Alt+F2). There is an open bugreport for that. #84757"
    author: "Jay Pee"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "Haven't searched for bugs on most of these yet, but...\n\n\"Align to grid\" for desktop icons is way off.  There is at least one icon worth of space between icons.\n\nKDCOP hangs on startup, after loading the UI but before populating the list.\n\nKPilotDaemon does not automatically restore itself on startup, even if I have automatic session saving on and the appropriate pref in KPilot settings.\n\nThe splash screen (a very nice one, I might add!) doesn't show the percentage indicator until close to the very end.\n\nFinally, even though it's not exactly a regression since 3.2 didn't have this at all, the Google search field in Konqueror doesn't always appear.  Right now it seems to be hidden when I first open a window, but appears after I minimize and restore the window.\n\nI'm using the SuSE-built packages that are now available."
    author: "Hooded One"
  - subject: "Forgot two..."
    date: 2004-08-12
    body: "Message dialogs in Kate (asking if you want to save a document, for example) are of the wrong dimensions.\n\nKonqueror periodically does not respond to double- or middle-clicking on files or folders to open them."
    author: "Hooded One"
  - subject: "Re: Forgot two..."
    date: 2004-08-12
    body: "> Konqueror periodically does not respond to double- or middle-clicking on files or folders to open them.\n\nIn list views? Watch if it works if you click on the text of the file/folder."
    author: "Anonymous"
  - subject: "Re: Forgot two..."
    date: 2004-08-12
    body: "Mmm... nope.  I was using list view and clicking on the text in the first place.  I can most reliably trigger this bug by clicking once on a file/folder to select it, then trying to double-click that one.  But this doesn't always trigger the bug, and sometimes it gets triggered by something else.\n\nHowever, I did just try clicking on the icon part in list view, and that appears to work fine every time, even when clicking on the text has just failed.  Freaky."
    author: "Hooded One"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "> The splash screen doesn't show the percentage indicator until close to the very end.\n\nThat's intentional, it shows the progress for the last item (restoring session) as in previous versions."
    author: "Anonymous"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "Ah, ok.  That makes some sense, I suppose.  I'm not quite sure what's the point of having the progressbar for just one part of the startup process though.  Actually, I'm not even sure the progressbar is necessary at all, since the icons already fulfil that function."
    author: "Hooded One"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "The progressbar shows progress during the last phase (i.e. last icon), during which all session saved apps are restored. Since the amount of those varies, you can't have icons for those indeed. It used to work that way until KDE3.2.0, when replacing the old KSplash with KSplashML broke it (and the progressbar until the recent fix really didn't fullfil any useful function)."
    author: "Lubos Lunak"
  - subject: "Re: Please report regressions here."
    date: 2004-09-02
    body: "\"Align to grid\" for desktop icons is way off. There is at least one icon worth of space between icons.\n\nA lot of space that is really annoying. Isn't away to change this manually?"
    author: "soda"
  - subject: "Re: Please report regressions here."
    date: 2004-08-12
    body: "arts crashes \nintermittend droppouts of sound when it does not crash"
    author: "Andreas"
  - subject: "KGet icon can't be removed from Konqui toolbar"
    date: 2004-08-16
    body: "As subject says, it's a regression or maybe I'm too dumb to find the way to remove this not useful (to me) button on my Konqueror web-browser"
    author: "Vide"
  - subject: "KsCD volume slider doesn't work any more in DP"
    date: 2004-08-16
    body: "While it works perfectly while reproducing CD in the \"classic\" way, the volume slider in KsCD doesn't work at all in digital playback mode."
    author: "Davide Ferrari"
  - subject: "Groupware/Exchange"
    date: 2004-08-10
    body: "With the 3.2 release there was a lot of hoopla around the Exchange functionality and how it has been pushed back for the KDE PIM 3.3 release, which was due to follow a \"month or two afterwards\".\n\nAs we are nearing final 3.3 release, there is not much, if anything, said around the Exchange functionality and a nice clear integration. So I'll ask it here:\n\nWithout WebDAV access (Outlook Web Access enabled on the server), can I access my calendar and respond to appointments via Kontact? I'm running 3.3 Beta 2 on my SuSE box and have looked/serached far and wide for anything of the old Kolab nature that will help us break the bounds and interact in the corporate environment. Well, when our company eventually moves to the OWA enabled server in the next 6 months, I know that I could try the Kontact Exchange plugin and/or give Evolution *cough* a go. (Nothin against Evolution, I just like Kontact, personal preference...)\n\nJaco"
    author: "Jaco"
  - subject: "Re: Groupware/Exchange"
    date: 2004-08-10
    body: "The Exchange stuff in Kontact still needs WebDAV enabled on the server, since this is the only protocol with at least some docs. \n\nI am the one who developed the initial Exchange plugin. It was planned to include Exchange support as an full Calendar resource for KDE 3.3 (which means transparant support for Exchange, work with your Exchange calendar as if it were a local calendar), but I have unfortunately not had any time to work on it since more than half a year, and it will be at least a couple of months before I will resurface again. I know that has been work on the Exchange resource because OpenGroupware (?) is using (almost) the same protocol, but I haven't followed it.\n\nAnyways, the source is open, go hack!\n\nJan-Pascal"
    author: "Jan-Pascal van Best"
  - subject: "Re: Groupware/Exchange"
    date: 2004-08-10
    body: "Yip, source is open, but I even don't have enough time lately to support my own OSS projects :) (So that one is pretty slim...)\n\nThanks for the update.\n\nJaco"
    author: "Jaco"
  - subject: "Re: Groupware/Exchange"
    date: 2004-08-10
    body: "What I'd really like to see is a kmail for support for exchange over webdav."
    author: "Greg "
  - subject: "Re: Groupware/Exchange"
    date: 2004-08-11
    body: "Note that just because you are moving to an OWA server does not mean you will have WebDAV.\n\nThe server admin can enable OWA without enabling DAV. My work server is set up this way."
    author: "Jason Keirstead"
  - subject: "Can someone please re-write the announcement above"
    date: 2004-08-10
    body: "Please?"
    author: "Nadeem Hasan"
  - subject: "Re: Can someone please re-write the announcement above"
    date: 2004-08-10
    body: "Please don't tell us why and how."
    author: "Anonymous"
  - subject: "Re: Can someone please re-write the announcement above"
    date: 2004-08-10
    body: "It's barely intelligible to those who like to read genuine English."
    author: "Anonymous"
  - subject: "Re: Can someone please re-write the announcement above"
    date: 2004-08-10
    body: "If you can't already tell, then you don't need to know :)"
    author: "Nadeem Hasan"
  - subject: "I have a tendancy to agree!!"
    date: 2004-08-10
    body: "If someone wants to use this, go ahead :-)\n\nAnnouncement:\n\nThe latest version of KDE beta 3.3 has been released. \n\nThis version has already received a lot of feedback and has been deemed stable enough for a public release candidate. The KDE team requests that all testers try and break this release as soon as possible, as the bug reports are invaluable to the developers.\n\nPlease note that binary packages will not be available for this version. Then source code can be downloaded from download.kde.org (or alternately use the excellent Konstruct build tool).\n"
    author: "Andrew"
  - subject: "My most anticipated feature..."
    date: 2004-08-10
    body: "...is the ability to debug PHP is Quanta. I hope I will be able to track a variable's value as code is executed. This wasted lots of my time in the past. Kudos to Eric and his team."
    author: "charles"
  - subject: "Re: My most anticipated feature..."
    date: 2004-08-18
    body: "Quanta will have the ability to debug php with the help of Gubed PHP Debugger, http://gubed.sf.net (we've implemented a general debugger plugin system, which would allow also for other debuggers, PHP or not, but currently theres only one plugin available)\n\nUnfortunately, we didnt have time to include all the features we wanted, but the basic stuff like checking the value of variables and stopping at breakpoints is there.\n\nMake sure to let the developers know of how it works for you!\n\n/linus"
    author: "Linus"
  - subject: "Re: My most anticipated feature..."
    date: 2005-04-22
    body: "a tutorial \"Debugging PHP scripts with Quanta Plus and Gubed PHP Debugger\" can be found here:\n\nhttp://www.very-clever.com/quanta-gubed-debugging.php"
    author: "tom"
  - subject: "screensaver are not working on suse 9.1"
    date: 2004-08-10
    body: "Hi,\n\nDid a update tru apt-get - synaptic to 3.3.0-1, after that the screensavers are not working any more?????\nWith 3.2.92-1 all was oke.\n\n\nThanks Gerrit Jan"
    author: "GJ Eldering"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-08-10
    body: "Could be a binary package problem. Work fine here."
    author: "Anonymous"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-08-21
    body: "Same here with 3.3 Final version. Suse 9.1. "
    author: "Julio"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-08-21
    body: "I have the same problem....\nOnly the fireworks works!"
    author: "GJ Eldering"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-08-25
    body: "After upgrading to KDE 3.3 only few of them are working for me..."
    author: "Beeblebrox"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-08-25
    body: "this will fix it (as root)\n\nln -s /usr/X11R6/lib/xscreensaver /usr/lib\n\nln -s /usr/X11R6/lib/X11/xscreensaver/config/ /etc/xscreensaver"
    author: "GJ Eldering"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-08-25
    body: "G.J., thanks a 1,000,000 :)\nIt's working perfectly now (another note to my personal Linux FAQ)."
    author: "Beeblebrox"
  - subject: "Re: screensaver are not working on suse 9.1"
    date: 2004-09-15
    body: "Nice GJ,\n\nIt worked for me too, and now my day is saved. I just forgot the space in ..../xscreensaver(space)/usr/...., but when i got that it worked perfect.\n\nThanks."
    author: "Linux Newbee"
  - subject: "RC *2*?"
    date: 2004-08-10
    body: "Greetings, peeps,\n\nIt's RC*2* already? What happened to RC1?"
    author: "Anonymous Coward"
  - subject: "Re: RC *2*?"
    date: 2004-08-10
    body: "http://download.kde.org/unstable/3.3.0rc1/src/ - but it was intentionally not announced."
    author: "Anonymous"
  - subject: "Re: RC *2*?"
    date: 2004-08-11
    body: "Surely that's contrary to the whole point of release candidates?"
    author: "Jim"
  - subject: "Re: RC *2*?"
    date: 2004-08-11
    body: "apparently flash or something didn't work and that was a showstopper so it couldn't have been a true \"release candidate\""
    author: "a"
  - subject: "Re: RC *2*?"
    date: 2004-08-11
    body: "There were some significant issues that we realized between the time that the tarballs were produced and when an announcement would have been done.  There was a problem with selection in KHTML, LiveConnect in Flash and on most systems audio didn't work at all.  As such it was decided to not announce it, but wait for RC2."
    author: "Scott Wheeler"
  - subject: "Re: RC *2*?"
    date: 2004-08-11
    body: "There were a couple of obvious show stoppers.\n\nThey were fixed and another rc was prepared.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: RC *2*?"
    date: 2004-08-11
    body: "Alright, I see! Thank you all for your answers. :)\nI'll be installing and testing RC2 as soon as it's unmasked in Gentoo."
    author: "Anonymous Coward"
  - subject: "Re: RC *2*?"
    date: 2004-08-11
    body: "-rc versions will never get unmasked in portage. You'll have to use ACCEPT_KEYWORD=\"~x86\" (or whatever fits your system)."
    author: "Marc"
  - subject: "Re: RC *2*?"
    date: 2004-08-12
    body: "Don't confuse masking and marking as stable. My /etc/portage/package.keywords already accepts all unstable (~x86) KDE packages. Actual masking in $PORTAGEDIR/profiles/packages.mask is another thing entirely. The RC2 ebuilds will remain unstable, but Caleb Tennis (our thrice-blessed Gentoo deity of things KDE) usually unmasks them after a few days of testing.\n\nIn fact, I went and unmasked them manually yesterday, and everything compiled, installed and now runs just fine."
    author: "Anonymous Coward"
  - subject: "Works fine here"
    date: 2004-08-11
    body: "Everything works fine here, no bugs at immediate sight.\n\nlooks like some minor bugs only triggerd in special cases.\n\nchris"
    author: "chris"
  - subject: "Slashdot Anyone?"
    date: 2004-08-11
    body: "Hello,\n\nJust wondering why this hasn't been posted on slashdot.  "
    author: "Samuel S. Weber"
  - subject: "Re: Slashdot Anyone?"
    date: 2004-08-11
    body: "Perhaps because nobody submitted it? Perhaps because Slashdot is not Freshmeat?"
    author: "Anonymous"
  - subject: "Re: Slashdot Anyone?"
    date: 2004-08-11
    body: "http://developers.slashdot.org/article.pl?sid=04/08/11/0614245"
    author: "Anonymous"
  - subject: "screenshots"
    date: 2004-08-11
    body: "Hey guys, who will post some kde rc2 shots?"
    author: "saulo"
  - subject: "Re: screenshots"
    date: 2004-08-11
    body: "http://wiki.kde.org/tiki-index.php?page=Reviews"
    author: "Anonymous"
  - subject: "More Screenshots 3.3 RC2"
    date: 2004-08-11
    body: "Please scroll to the bottom since the page contains Screenshots from MorphOS, GNOME and KDE.\n\nhttp://www.akcaagac.com/index_desktop.html"
    author: "Anonymous"
  - subject: "bookmark folders unusable"
    date: 2004-08-11
    body: "This bug on bookmark folders beeing not usable is very very annoying !\n\nWas it fixed in kde 3.3 ?\n\nhttp://bugs.kde.org/show_bug.cgi?id=77047"
    author: "Pascal"
  - subject: "Re: bookmark folders unusable"
    date: 2004-08-11
    body: "As you can see my note in the bug report, this bug still exists in 3.3 RC2.\n\nOsho"
    author: "Osho"
  - subject: "About to finish?"
    date: 2004-08-11
    body: "\"About to finish\" sounds scary. The rest sounds very like Genglish, too. Although I'm no native english speaker, I'll try:\n\nKDE 3.3 on the Doorstep: Public Release Candidate 2\n\nAfter having received a lot of feedback on our KDE 3.3 Betas, a public Release Candidate is now finished. Since we wanted testing to begin as soon as possible, we didn't wait for vendor binaries. Please build and test the sources or use <a href=\"http://developer.kde.org/build/konstruct/\">Konstruct</a>. The Release Candidate is available at <a href=\"http://download.kde.org/unstable/3.3.0rc2/src/\">download.kde.org</a>."
    author: "Annno v. Heimburg"
  - subject: "Low Resolution"
    date: 2004-08-11
    body: "I wanted to point out , that KDE is not very usable in 800x600.\n\nfor example you use this resolution if you output on a TV.\n\nmost config dialog are too huge , and you have to use ALT key all the time.\n\n\ni think you cannot file bugreports for this , because there are sooo may dialogs affected."
    author: "chris"
  - subject: "Re: Low Resolution"
    date: 2004-08-11
    body: "Dialogs that do not fit a 800x600 screen are not in compliance with the KDE User Interface Guidelines, and this is as serious as a bug. But I don't see that many dialogs too big. Feel free to report the style guide violations to the developers."
    author: "Henrique Pinto"
  - subject: "Re: Low Resolution"
    date: 2004-08-11
    body: "Also be sure to check the dpi value X Window System propagates (in case you didn't fix it with kdm). The higher the dpi value propagated the bigger the text will be. And since the dialog windows' size adapt to their content dialog windows can get huge this way."
    author: "Datschge"
  - subject: "Regression: tooltips pop up too often"
    date: 2004-08-11
    body: "Rather annoying on me the last week CVS: tooltips in Konqueror's icon view pop up often while opening the context menu or even when another window already has the focus.\n\nSee: http://bugs.kde.org/show_bug.cgi?id=86968"
    author: "wilbert"
  - subject: "Binaries"
    date: 2004-08-11
    body: "Seems that now binaries for Slackware 10 and Yoper are available."
    author: "Anonymous"
  - subject: "mediacontrol & amaroK"
    date: 2004-08-11
    body: "Does anyone know if 3.3 the mediacontrol applet has support for amaroK?  \n\nBobby"
    author: "brockers"
  - subject: "Re: mediacontrol & amaroK"
    date: 2004-08-11
    body: "It seems that it has (XMMS, Noatun, amaroK, JuK)."
    author: "Anonymous"
  - subject: "Re: mediacontrol & amaroK"
    date: 2004-08-13
    body: "tks for the reply"
    author: "brockers"
  - subject: "Showstopper? in iconview clickable area too large"
    date: 2004-08-11
    body: "Icons sometimes fire if I click above them. See bug http://bugs.kde.org/show_bug.cgi?id=86693\nScreenshot: http://bugs.kde.org/attachment.cgi?id=7022&action=view"
    author: "wilbert"
  - subject: "Dualhead support severly broken"
    date: 2004-08-12
    body: "Firstly:\nKnode seems to be a lot faster to use, thanks for that!\nEven if the bug with the text you just read disappearing when an autoupdate of the articles is done is still there.\n\nWorse is the the toolbars doesn't work appear at all in the second monitor of a dualhead installation. These did have problems before, but now they aren't there any more (Of course the previous problems are gone then ;-))\n\nBest Regards"
    author: "Dag Nygren"
  - subject: "SUSE Binary Packages"
    date: 2004-08-12
    body: "ftp://ftp.suse.com/pub/suse/i386/supplementary/"
    author: "Anonymous"
  - subject: "Bugs 48786 & 79596"
    date: 2004-08-13
    body: "The problem with the window manager persists - Focus stealing \"prevention\" \"steals\" new windows that should have focus.\n\nAnd, it is not possible to turn this \"feature\" off -- you can try to turn it off but setting: \"Focus stealing pervention level:\" to: \"None\" doesn't cure the problems with The GIMP & Adobe Acrobat Reader.  If these applications are running, the windows for new files opened are not raised -- they appear to go to the bottom rather than the top (where they should be).\n\nTo me, this appears to be an example of where our development model goes wrong.  This new \"cool\" feature was added to the code tree without adequate design or testing and now we have a window manager that is seriously broken.  Perhaps Linus is right about CVS (at least in some cases) patches *are* better.  \n\nIn any case, to prevent similar problems in the future there needs to be a way to develop and test such new features WITHOUT adding them to the code tree -- if they don't work they shouldn't show up in the next release branch.  That is, IIUC, there is no simple way to go back to the KWin from 3.1.x which didn't have this problem.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Bug 79932"
    date: 2004-08-14
    body: "http://bugs.kde.org/show_bug.cgi?id=79932\n\nWill this be fixed in the 3.3.0 release?\n\nThis is a serious regression for people that have a lot of icons on their desktop.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "artsd starting when its not supposed to"
    date: 2004-08-15
    body: "This bug has been there for a long time. \nartsd starts automatically when downloading a media file from the internet (even when artsd is disabled in the kde control center). Is this one fixed yet ?"
    author: "artsd_makes_life_harder"
  - subject: "bugs"
    date: 2004-08-16
    body: "So far I have discovered that:-\n The Kdict applet for the taskbar has disappeared.\n The Kmail interactive spellchecker won't work with aspell\n\nRunning on Gentoo unstable.\n\nI would _very much_ like to be able to reverse the mouse wheel direction \nto change the font size in Konqueror. i.e. Top of the wheel towards me to\nenlarge the text, because that is the default way Mozilla works, and it is conter-intuitive for me to push the wheel away from me when I want to have a closer look."
    author: "Christopher Sawtell"
---
We have already received a lot of feedback to our KDE 3.3 Betas, so we can release a public Release Candidate. We want to have this tested as soon as possible, so we didn't wait for vendor binaries. So please test the sources if you have experience in this (or use <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>). 
You can download the Release Candidate from  <a href="http://download.kde.org/unstable/3.3.0rc2/src/">download.kde.org</a>. 









<!--break-->
