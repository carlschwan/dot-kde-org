---
title: "Quickies: Kile, Quanta, Independent Qt Tutorial, KDE-NL, KDE China"
date:    2004-02-02
authors:
  - "fmous"
slug:    quickies-kile-quanta-independent-qt-tutorial-kde-nl-kde-china
comments:
  - subject: "kde china website not correctly rendered"
    date: 2004-02-01
    body: "The KDE CHINA Website ist not correctly rendered in my Konqueror , there are serveral Glyphs missing , that are only represented by a box.\n\nAre there more People where it is not correctly rendered ?\n\ni am using kde 3.2 rc\n\nchris"
    author: "not rendered correctly"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-01
    body: "Looks perfectly fine here, are you sure you have the necessary fonts containing all Chinese glyphs on your system?"
    author: "Datschge"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-01
    body: "You must have the right fonts installed to render Chinese!\nIt renders correctly for me (SuSE 8.2 KDE 3.2.0 RC1)\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-01
    body: "Hmm, where can the fonts in question be found?"
    author: "Anonymous Coward"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "I don't think it has anything to do with not having the right fonts installed. KDE kinda suck when it comes to detecting fonts, I'm sure kde China displaid perfectly with your mozilla which mean u have the right fonts installed. I had the same problem, it worked with mozilla but not konqueror so I had to add them myself with the kcm module."
    author: "Pat"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "The site sets the correct charset in its meta tag so no auto detection is involved at all. He is clearly missing the right font if some characters are showed as squares (do a google search for different sources of Chinese fonts if you miss them on your system)."
    author: "Datschge"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "You may need to set up your font substitution tables (strictly speaking part of qt, not kde). You can use kcharselect to see whether you have the fonts/glyphs. Chinese chars are around table 80. If you have fonts like Fangsong Ti or Song Ti, they will/should! display the right chars for those tables. If western fonts don't, use qtconfig to add Fangsong Ti or whatever to the western fonts. KDE Taiwan are using Tahoma (don't ask me why), so if you add (Fang)Song Ti to Tahoma and restart konqueror, it should then display.\n\nThese substitutions are stored in ~/.qt/qtrc"
    author: "Peter Robins"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "is there a way to do this automatically ? no editing or clicking involved ?\n\nthe china website displays perfectly in mozilla, the glyhps are not antialiased like the arabian one , but ....\n\n\nchris"
    author: "chris"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "Its always been the opposite for me, works in Konqueror and not in Mozilla without me trying as I don't know any languages in other alphabets. So it doesn't really make any difference in my case, but if I do need to browse a site I do like seeing those pretty characters and, in some languages, playing with right-to-left highlighting. (-:"
    author: "Ian Monroe"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "i think its not a fonts problem (it works in moz, doesn't work in konq), but a QT problem. i see the problem when using qt 3.2 and kde 3.2, but it goes away when i go to qt_copy 3.3 (also, kopete starts showing other glyphs right with qt_copy 3.3). Unfortunately, it seems 3.3 is not yet stable..."
    author: "ik"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-02-02
    body: "There is a bug with the Qt font substitution system.\n\nIf you have other glyph fonts (such as Japanese, Korean or full form Chinese) and they appear in the font substitution list before the simplified Chinese fonts then they will be used to render the text.\n\nThese other character sets do not contain all the characters necessary to render simplified Chinese and therefore some glyphs will be missing.\n\nThe font substitution list is in your .qtrc file.\n\nGood luck.\n\nJethro"
    author: "Jethro Cramp"
  - subject: "Re: kde china website not correctly rendered"
    date: 2004-03-16
    body: "Nothing just a testing on this form."
    author: "hi"
  - subject: "C't article 'bout k3b"
    date: 2004-02-01
    body: "Hey ho!\nThere was also a small article about k3b in the issue before.\nJust check out C't 2/2004 page 58 for a small review of \"the probably most comfortable burning program for linux\". :-)\n\nRischwa"
    author: "Rischwa"
  - subject: "Re: C't article 'bout k3b"
    date: 2004-02-01
    body: "yes, I've read that too, and I have to agree!\nGood work, Sebastian Trueg&Co!"
    author: "dhaumann"
  - subject: "Re: C't article 'bout k3b"
    date: 2004-02-02
    body: "I second that. Before K3B I was having to switch between different more or less annoying package, but now it's like it's too easy. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: C't article 'bout k3b"
    date: 2004-02-02
    body: "\nAnd I second that. k3b is so intelligent, and so userfriendly, and so usable, and so capable, etc, etc.\n\nThanks Sebastian, do you get paid for this? You really keep up the activity, according to kde-cvs..\n\nCheers,\n\n\tFrans\n\n"
    author: "Frans Englich"
  - subject: "Multilanguage CMS"
    date: 2004-02-01
    body: "In my opinion language support has to be integrated into the main site rather than prioviding a bunch of national website that are out of date.\n\nAlso easier translation tools for long texts are needed. t7e perhaps, haven'zt tried it yet."
    author: "Elektroschock"
  - subject: "Re: Multilanguage CMS"
    date: 2004-02-02
    body: "Plone can do this well. It is best opensource CMS available.\nTry it on http://test.plone.org\n\nSome tweaks would be needed to run huge site, but it is not so hard and plone gurus on irc.freenode.net (channel #plone) can help a lot.\n\nbtw: there was a bug in konqueror before Xmas  which was causing plone website to look funny. so if it looks ugly, upgrade  now :)"
    author: "alekibango"
  - subject: "Re: Multilanguage CMS"
    date: 2004-02-02
    body: "Well, the Typo3 CMS is also very capable when it comes to multi-language. Every snippet (article, newsitem, whatever) can be submitted several times, e.g once in english, then some editor submits the german version and so on.\n\nWhen someone with language \"german\" views a page, then all the german entries get display. If for some entry the german entry does not exist, then the english (or whatever you set up as default language) will be shown. This way you won't have an information-loss if you're a non-native speaker.\n\nhttp://typo3.org"
    author: "Holger Schurig"
  - subject: "Dear Eric"
    date: 2004-02-01
    body: "http://quanta.sourceforge.net/todo/display.main.php\n\nWhat features issued of this site had to be postponed to the 3.3 release of Quanta? "
    author: "Gagamemnon"
  - subject: "Re: Dear Eric"
    date: 2004-02-02
    body: "> What features issued of this site had to be postponed to the 3.3 release of Quanta? \n\nI think the idea was to post to the site for interview questions... you must be looking for a shortcut. ;-) I'll answer anyway. First of all much of what didn't make it into 3.2 will be out soon in Quanta BE releases. They are marked development so we won't get flack if a new feature is not fully complete but they are stable. Also, we're considering releasing Quanta 3.3 before the KDE 3.3 release. We don't have to have the same release numbers and most KDE apps don't. What we have to do is adhere to freezes in the KDE schedule. External releases allow users to benefit from new features as they become usable but do not benefit from translation and and other release benefits.\n\n* Templates - directory templates and installed templates as well as an online template repository is scheduled for early this year\n* Project docs made it in but you have to generate them. Doc generation via the parser and configuration is under review\n* Getvars - probably nobody knows what it is, but it's enhanced html form debugging tools\n* Site Planner - It's morphed into two separate projects and will not likely show up before mid year at the soonest\n* New CSS tools did make it in, but updates have since arrived and integration for much better CSS use is in work. Look for this early this year.\n* Supplemental file data \n* Enhanced undo/redo - we had problems with this and back burnered it since Nicolas was also working on VPL\n* Toolbar Dropdowns - this would be similar to the recent file toolbar button and would have an extra interface in actions to allow listing tags or scripts. I seem to be the only person excited about this. Is anyone else?\n* Multiple upload profiles - using multiple instances, separate projects or changing settings is it for now.\n* VPL - It's in there, but it still has a few rough edges. Look for a more refined version that makes it easier to work with CSS and do other tasks early this year.\n\nThere are a lot of other new things we're working on now too. I apologize for the site being behind. I'm dealing with moving my business site after having been shafted by my hosting provider and we are looking to do a complete redesign this month of the site."
    author: "Eric Laffoon"
  - subject: "Quanta question numer #2"
    date: 2004-02-01
    body: "Eric,\n\nhow do you think can multilanguage environments be managed. We do have the text.en.txt text.gr.txt format, but how can we manage the problem. \n\nWhere do the tools have to be improved?\n- webeditors?\n- network tools for translators?\n- distributed translation environments?\n- better multilanguage support for CMS??\n- standards?\n\nWhat can be done?"
    author: "Gagamemnon"
  - subject: "Re: Quanta question numer #2"
    date: 2004-02-02
    body: "> how do you think can multilanguage environments be managed. We do have the text.en.txt text.gr.txt format, but how can we manage the problem. \n\nThat's a really loaded question. I'm probably not the best person to ask because I'm not currently doing it... though I'm considering doing that on our new site.\n\n> Where do the tools have to be improved?\n> - webeditors?\nI don't think you're going to want me to offer you a \"mandated\" solution because it would constrain your options. In that regard Quanta does offer you extensive abilities to customize it and build script and dialog extentions.\n\n> - network tools for translators?\nkbabel?\n> - distributed translation environments?\nI'm at a loss here...\n> - better multilanguage support for CMS??\nSeveral people on my team refer to CMS as \"Can't Manage S***\". Effectively these tools try to become everything instead of offering pluggable extensible functionality. That could be why there's not a consensus tool and so many people write their own.\n> - standards?\nThat's an ugly thought unless you can tell me how you will do that without really messing with existing standards like HTTP and XHTML.\n\nLike many design factors it comes down to how the development team wants to define what they do. KDE and Quanta both have additional guidelines that constraing the project development and are consistent with lead developers, good design and our objectives. This is where project vision comes in. You have to define a framework, and you probably won't like someone else's. for instance, it's not too difficult to develop an abstracted layout using PHP and CSS. Then you include your content files into your structure files and manage your look and layout with CSS. That way you develop one structure, as many looks as are required and language content in parallel. That would be my approach. Default to /content/en/ and allow switching to /content/de/ or whatever.\n"
    author: "Eric Laffoon"
  - subject: "Re: Quanta question numer #2"
    date: 2004-02-03
    body: ">> - better multilanguage support for CMS??\n> Several people on my team refer to CMS as \"Can't Manage S***\".\n> Effectively these tools try to become everything instead of\n> offering pluggable extensible functionality. That could be why\n> there's not a consensus tool and so many people write their own.\n\nAny plans to change that? I can't imagine that there are no projects for CMS-(brick like)-modules. Maybe they are not visible because there are so many CMS? Including support for such an CMS in Quanta could mean a huge boost for a small CMS project."
    author: "testerus"
  - subject: "Great job on the kde.nl site!"
    date: 2004-02-01
    body: "It's actually useful now, go figure :)\n"
    author: "Emiel Kollof"
  - subject: "Quanta assesibility"
    date: 2004-02-04
    body: "What aid can be provided for colorblind people\n- \"color check\" assistence for webdesigners?\n- help for color blind to detect whether a color was red or green\n"
    author: "gerd"
---
We were <a href="http://lists.kde.org/?l=kde-promo&m=107494508625145&w=2">notified</a> about a
small article about <a href="http://kile.sourceforge.net/">Kile</a> in the recent issue of
<a href="http://www.heise.de/ct/">c't</a>, Germany's largest IT magazine.
Check the table of content <a href="http://www.heise.de/ct/inhalt.shtml">online</a>, and/or
buy the magazine and check out page 55.
The <a href="http://www.digitalfanatics.org/projects/qt_tutorial/">Independent Qt Tutorial</a> has been updated with two
completely new chapters about <a href="http://www.digitalfanatics.org/projects/qt_tutorial/chapter08.html">file handling</a>
and <a href="http://www.digitalfanatics.org/projects/qt_tutorial/chapter09.html">XML</a>
and numerous small fixes. Eric Laffoon, the project leader of <a href="http://quanta.sourceforge.net/">Quanta Plus</a>,
has been asked by <a href="http://www.techdigest.org/">techDigest</a> to do an interview. They are
<a href="http://www.techdigest.org/modules.php?op=modload&name=News&file=article&sid=324&mode=thread&order=0&thold=0">collecting some questions</a>
for this interview as well. <a href="http://www.kde.nl/personen/vertalers.html#wbsoft">Wilbert Berendsen</a>
reported that the website <a href="http://www.kde.nl/">www.kde.nl</a> has undergone a
metamorphosis to serve the KDE community in Belgium and the Netherlands much better. More on websites:
Shiyu Tang <a href="http://lists.kde.org/?l=kde-www&m=107565126727986&w=2">mentioned</a> on the kde-www mailinglist
about the launch of a <a href="http://cai7.com/kde/">KDE-China website</a>.

<!--break-->
