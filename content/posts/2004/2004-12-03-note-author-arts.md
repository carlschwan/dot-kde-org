---
title: "A Note From the Author of aRts"
date:    2004-12-03
authors:
  - "jriddell"
slug:    note-author-arts
comments:
  - subject: "Thanks indeed!"
    date: 2004-12-03
    body: "Although there recently has been a mood that Arts should be dumped, I think we still owe Stefan a big Thank you! Arts has powered the multimedia in KDE for many years, and though it has it's problems, it isn't that bad at all. Sure, it may get replaced by something more modern and fit to the broad desktop-usage that is envisioned for KDE, but in the meantime, it has served us well. Thank you Stefan!"
    author: "Andre Somers"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "absolutely! thanks!\n\nbesides the current arts ranting... it's still able to provide stuff no other piece of software in this area achieved, yet. but as stefan explained, it's a sound-serving-decoding-coffee-cooking-doing-everything solution, which is hard to maintain. thanks for all your effort!\n\nanyways, i'm looking forward to kde4, i'm sure we'll come up with a nice multimedia backend framework (i'm buzz-wording today *g*) by that time.\n\nregards,\nmuesli"
    author: "muesli"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "What applications are at the frontline to replace aRTs in KDE. Where can one get info? What does that mean for future KDE multimedia development? I heard the the \"analyzer lag\" in amaroK was due to aRTS. What are aRTS's other problems?"
    author: "charles"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "read some other topic here on the dot...\n\nbut to give a short answer:\n-info: search the dot and google\n-future: well, another solution has to be found...\n-yep, arts isnt that fast, so the lag was due to arts.\n-read the doc and the comments on the web. mostly: high latency, hi processor use, hard to work on.\n-so KDE (and the whole FSworld) is looking for a replacement. NNM (or was it NMM?), MAS and Gstreamer seem to be in the picture. amarok and juk already support gstreamer, amarok also supports both other soundservers."
    author: "superstoned"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "no, that lag is not due to arts, because arts is not \"not that fast\"\n\nThere's always going to be an inherent latency when you're pumping audio from software to the sound card's dma buffers on a non-realtime multitasking operating system.  The way to solve that problem is to add a delay between the decoder and the visualisation that's equal to that of the decoder and the speakers.  aRts cannot be held to blame if the application doesn't do that.\n\n-Charles\n"
    author: "charles samuels"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "ok, I stand corrected :D\n\nbut why has it so much higher latency than other players? and why does it skip so often?"
    author: "superstoned"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "The latency is completely configurable between \"as little as possible\" to \"as much as reasonably possible\"  And certainly less than XMMS does.  I've seen players that take a bit to respond after a pause or seek, arts doesn't do this to any noticeable amount.\n\n\"It skips\" so often because mpeglib (our default mp3 decoder in KDE <=3.2) didn't buffer disk accesses very well and couldn't read the mp3 data quickly enough from the disk, decoding it or sending it to the sound card wasn't the problem, but Linux 2.4's disk IO is incredibly bad (it's far worse than even I think it is!).  It's much much much better in Linux 2.6.\n\nWe solved this problem by getting a new decoding plugin \"aKode\" (which does Vorbis, MP3, and also musepack), and buffers disk access much more.  Furthermore, it does some magic calls to the kernel that say \"I'm going to read in order, so buffer all you can, and unbuffer it as soon as I read it.\"  These things pretty much have eliminated the problem entirely.\n\nIt helps to also have a preemptive kernel, SCSI harddrives, ...\n\n-charles\n"
    author: "charles samuels"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "well, I haven't had any problems lately, exept for amarok, which seems to keep arts output open, blocking most other applications. but thats my main problem, now. the high cpu use and skipping have been fixed, mostly. the only problem I have with arts is that some apps can't use it natively so I have to use artsdsp (but alas, it works that way)."
    author: "superstoned"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "That would be another bug in amaroK (which, btw, I long ago fixed in noatun).  Amarok has to use arts correctly so that it releases /dev/dsp.  Report a bug.\n\nWhen I press stop in noatun, arts will release /dev/dsp shortly thereafter (as per how I've configured it)."
    author: "charles samuels"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-03
    body: "Keep in mind, that like arts itself, amaroK lacks a maintainer for its arts plugin."
    author: "Ian Monroe"
  - subject: "Re: Thanks indeed!"
    date: 2004-12-04
    body: "> arts isnt that fast, so the lag was due to arts\n\nThe problem is, that aRts' scope function always reads from the start of the sound buffer. So, if have a big buffer configured because you want to prevent skips (see kcontrol/Sound & Multimedia/Sound System/Skip prevention/Sound Buffer) then you get for several frames the same scope data delivered again and again. aRts isn't slow at that. It serves the same old and stale scope data quickly. It just *seems* as if it were slow. If you make the sound buffer as small as possible, then you'll see that there's no noticeable lag any more. One could probably have worked around that, but the sparse documentation didn't offer an apparent solution."
    author: "Melchior FRANZ"
  - subject: "Re: yup, thanks Stefan!"
    date: 2004-12-07
    body: "Don't be sad by the way arts is being treated now, it really made a difference and served a noble purpose."
    author: "Darkelve"
  - subject: "Reliable Stability"
    date: 2005-02-11
    body: "I have been developing a DJ application for a few years, and have found the biggest problems to be in using driver abstractions - alsa, jack, oss, and arts. aRts has always been a reliable test bed that I can fall back on when the others don't work. After a year of using the native alsa API, I'v fallen back on arts again to get my app working. Latency is a problem with arts, but My application code needs to be profiled right now, not the driver code!\n\nThanks!"
    author: "Patrick Stinson"
  - subject: "Good riddance"
    date: 2004-12-03
    body: "Netcraft confirms it, aRts is dead. Finally the old horse is dying, most end users hate aRts, it gives nothing but trouble and latency.\n\nThough i agree that it was a good idea, it needs to be replaced with something else from scratch, maybe gstreamer.\n\nHere's looking to kde 3.4"
    author: "Anonymous Coward"
  - subject: "I just hope that whatever replaces it..."
    date: 2004-12-03
    body: "... is split into two seperate components - a library for KDE that handles all the multimedia stuff, and a sound server, and that the library *can* or *can not* use the sound server based on the user's choice.\n\nI mean, I fully understand why a sound server is needed for people without hardware mixing, but for all of us who do, having to run a sound server to provide mixing capabilities inferior to those of our card is nonsense.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: I just hope that whatever replaces it..."
    date: 2004-12-06
    body: "Jep, i think thats the way to go. Which would make arts a thin layer on systems which support mixing and a little thicker layer on systems where this is not possible."
    author: "anon"
  - subject: "dmix"
    date: 2004-12-03
    body: "For Software mixing you can use Alsa dmix, there is no need for a Soundserver to do that."
    author: "jambo"
  - subject: "Re: dmix"
    date: 2004-12-03
    body: "Cool.  How do I install ALSA on my Solaris Sparcstations?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: dmix"
    date: 2004-12-03
    body: "with an axe?"
    author: "c0p0n"
  - subject: "Re: dmix"
    date: 2004-12-03
    body: "Who on earth listens to audio on a sparcstation?\nGet a radio. It has more computing power, too.\n"
    author: "Anon"
  - subject: "Re: dmix"
    date: 2004-12-04
    body: "like this: http://www.hansmi.ch/articles/gentoo-linux-ss4"
    author: "standsolid"
  - subject: "Re: dmix"
    date: 2004-12-04
    body: "oops didn't read the \"Solaris\" part.  but still..."
    author: "standsolid"
  - subject: "Re: dmix"
    date: 2004-12-04
    body: "The problem I have with dmix is that it does not work very well.  It can only do two channels.  It makes my audio sound bad (from what I have read this is to do with my sound drivers).  It seems to randomly crash.  It is hard to setup (anything more than just works is hard).  It seems to have compatibility issues with apps.\n\nThe most worrying thing about dmix is how slow its development seems to be going.  It was introduced in alsa 0.9.0 on 2003-03-02.  It has only recently got to the usable state in alsa 1.0.6.  And even now it has so many problems.  This to me says that it A) is really hard to develop and debug, or B) it is not well maintained.  Neither are good things.\n\nSome of these new sound servers like polyaudio work great today.  It does provide other neat features like network transparency, playing music across multiple devices, and plugins.  But the main thing is that it works.\n\nTo me the best would be to use something like gstreamer throughout kde.  That way the default audio sink and kde does not have to bind itself to any one solution.  The people with good soundcards can use an alsa sink, the terminal people can use a network transparent sound server, and the people who don't use linux can use whatever they need to for mixing."
    author: "theorz"
  - subject: "Re: dmix"
    date: 2004-12-06
    body: "What people think about polypaudio (http://0pointer.de/lennart/projects/polypaudio/)?\n"
    author: "Alexey Morozov"
  - subject: "Re: dmix"
    date: 2004-12-04
    body: "ahh yeah, and it will decode mp3, ogg, flac etc. and of course it will give me an equalizer, various ways to pipe my sound-output to somewhere else and all this with an easy to use API.\nUhm... wait, no, that's arts, not dmix."
    author: "mETz"
  - subject: "Solution - in kernel mixing"
    date: 2004-12-04
    body: "AFAIK NetBSD will get in kernel sound mixing soon. Combine that with clonable audio devices and you don't need any sound daemon anymore."
    author: "ahoi"
  - subject: "Re: Solution - in kernel mixing"
    date: 2004-12-07
    body: "ALSA's dmix plugin provides this in userspace, which is far better than putting non-kernel related code into the kernel.\n\nThis solution would also seem to fit the bill. OSS is obsolete."
    author: "ajs"
  - subject: "What about jack ?"
    date: 2004-12-06
    body: "Jack ( http://jackit.sourceforge.net ) was designed from the ground up with professional audio work in mind. I use arts' jack plugin, but a better idea would be to interface directly to jack."
    author: "Robert Wallner"
  - subject: "Re: What about jack ?"
    date: 2004-12-08
    body: "It's designed for professional audio work, not desktop use and network streaming. Different goals need different solutions."
    author: "mikeyd"
  - subject: "Re: What about jack ?"
    date: 2006-03-15
    body: "Jack Output? \nShould be fairly easy with\nhttp://bio2jack.sourceforge.net/"
    author: "mozai"
  - subject: "many thanks!"
    date: 2006-04-09
    body: "I just have to say I think everyone that is demeaning aRts is insane... I have used aRts continuously for years and i would say at most i have one minor problem every several months. Take that in comparison with the rate at which stupid comments surface here and I'd happily take my chances with aRts.\n\nIn addition, it's frustrating to see people parrot back the *exact same problems* he has already mentioned in such a disparaging way. Perhaps it isn't all-things-to-all-people, but it is free software.\n\nIn any event many thanks! I will continue to enjoy the use of aRts while I look forward to a very bright F&OS future.\n\n"
    author: "linus_baltimore"
---
In a recent post to <a href="https://mail.kde.org/mailman/listinfo/kde-core-devel">kde-core-devel</a> Stefan Westerfeld, author of the KDE multimedia framework <a href="http://www.arts-project.org/">aRts</a>, announced that <a href="http://lists.kde.org/?l=kde-core-devel&m=110200188523177&w=2">he would no longer be maintaining it</a>.  He has written a <a href="http://www.arts-project.org/doc/arts-maintenance.html">note on further development of aRts</a> explaining some of the problems with aRts' design decisions.  aRts was a pioneering system and a major help in bringing multimedia to Free Software, thanks to Stefan for making it happen.



<!--break-->
