---
title: "Quickies: Google Search Bar, Icons, KolourPaint and more"
date:    2004-03-08
authors:
  - "fmous"
slug:    quickies-google-search-bar-icons-kolourpaint-and-more
comments:
  - subject: "Good stuff"
    date: 2004-03-08
    body: "The google search bar looks like good stuff. I'm still amazed with the number of people who have used KDE for 3 years, who don't know about konqueror's web shortcuts (e.g, gg: ).. this seems to make it a lot more visible, good job.\n\nThe new design of kdevelop.org and developer.kde.org are both sleek and professional, good job!"
    author: "anon"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "Well, I wouldnt need it. typing in just the search words in the adress bar whould search too - what whould I need a google bar for???\n\nbut alas - maybe usefull for ppl who dont know that.\n\nalthough we'd better teach them about it???"
    author: "superstoned"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "I agree"
    author: "_"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "It should be more like Safari, have a little search icon in the (or next to?) the address bar and click on that when you want to search for the text in the address rather than look up a website."
    author: "Abdulla"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "Let's hope that there will be an easy way to turn it off for those for whom it simply wastes space."
    author: "Anonymous"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "is there something in kde you _cannot_ configure?"
    author: "mark dufour"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "I take that back. does anyone know how to disable the 'applications', 'actions' labels in kmenu?? this is a false dichotomy, and I prefer a separator with mine."
    author: "mark dufour"
  - subject: "Re: Good stuff"
    date: 2004-03-13
    body: "Agreed. I'd like to disable them as well. Anyone know how?"
    author: "anonymous"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "Easy, just don't install it ;)\nIt's only an optional plugin..."
    author: "Arend jr."
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "There needs to be a way to activate/deactivate plugins without installing/uninstalling them."
    author: "ac"
  - subject: "Re: Good stuff"
    date: 2004-03-08
    body: "Well, you can also remove it by going to Settings->Configure toolbars..."
    author: "Arend jr."
  - subject: "Kolourpaint in Gentoo"
    date: 2004-03-08
    body: "KolourPaint is already available as an ebuild for Gentoo users.\n\n$ emerge kolourpaint\n\n...that should be about it."
    author: "Pilaf"
  - subject: "Re: Kolourpaint in Gentoo"
    date: 2004-03-10
    body: "You gotta love gentoo ^^"
    author: "Styx"
  - subject: "Kolourpaint improvement"
    date: 2004-03-08
    body: "One thing that would make Kolourpaint better than MS Paint.\nMake it possible to select an area larger than what is shown on screen.\nIs this possible?"
    author: "OI"
  - subject: "Re: Kolourpaint improvement"
    date: 2004-03-10
    body: "> Make it possible to select an area larger than what is shown on screen.\n> Is this possible?\nWill be soon :)\n"
    author: "Clarence Dang"
  - subject: "waste of time"
    date: 2004-03-08
    body: "Are splashscreens really needed? there are so many graphics that have to be redesigned (for instance in the games package), there is a lack of Icons (for example the use of identical Icons in the creen configuration). Why do we have to waste our time on unneccessary splash screens that don't provide any function?"
    author: "Gerd"
  - subject: "Re: waste of time"
    date: 2004-03-08
    body: "because it is fun? \n"
    author: "Fabrice Mous"
  - subject: "Re: waste of time?"
    date: 2004-03-08
    body: "This subscribes to the incorrect notion that human resources can be spent like cash on whatever project is most pressing. It also ignores that it doesn't take much to make a splash screen, as seen by the 23 entrees.\n\nI just opened up digikam, it doesn't seem to me that the load time on my 1 and half year old computer is long enough to warrant a splash screen (perhaps it would be different if I had more galleries). But its their project, not out of beta yet, they can do as they wish."
    author: "Ian Monroe"
  - subject: "Re: waste of time?"
    date: 2004-03-08
    body: "Splashscreens are nice - as long there is an option to turn them off. It's a waste of ressources and bad usability. Splashscreens are an ugly import from the windows world."
    author: "Andre"
  - subject: "Googlebar"
    date: 2004-03-08
    body: "I like it, but I think it needs a space before the adressbar. Right now it looks like it's part of it."
    author: "Alex"
  - subject: "Awesome redesigns!"
    date: 2004-03-08
    body: "I already knew that the developer website was redesigned and it definitely looks and feels better.\n\nI'm even more pleased to see Kdevelop's website redesigned. A great tool like that needs a good website! Good job!"
    author: "Alex"
  - subject: "God bless KolourPaint"
    date: 2004-03-09
    body: "I know it's just a simple app, but now the only program that I've missed from Windows the past five years (mspaint) finally has a better substitute in Linux.\n\nThanks all!\n   Paul Schmeltzer"
    author: "Paul Schmeltzer"
  - subject: "Re: God bless KolourPaint"
    date: 2004-03-09
    body: "I don't like it. I don't like Opera's either. There's no space for typing anything. And it takes away space from the address field. You have to type anyway - so how hard is it to prefix with gg:?"
    author: "Claus"
  - subject: "Re: God bless KolourPaint"
    date: 2004-03-09
    body: "Exactly. I hate the google bar on Firebird.  Waste of space and doesnt provide any useful advantage.  Just type in your search in the address bad.. Don't even need to use the gg:"
    author: "Leo S"
  - subject: "Could this plugin also implement type-ahead-find?"
    date: 2004-03-09
    body: "...because I saw in the screenshot, that you can select \"find in page\". If this search is incremental and when it finds a link, it selects it, so that you just need to press enter to follow the link, we could close one of the most popular whishlist item \"type-as-you-find\" for konqueror.\n\napart from this possible usage, I don't think its that usefull since you already can search in the addressbar (even without gg:, when set in the preferences). But as it's a plugin only and can be turned off, it's a real good thing for some users."
    author: "MaxAuthority"
---
Some progress has been made on implementing the <A href="http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/screenshots/googlesearchbar.png">Google search bar</A>,
a Konqueror plugin which can be found in 
<A href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdeaddons/konq-plugins/searchbar/">CVS</A>. 
It uses the default searchprovider as selected in Konqueror under the Web Shortcuts settings, so
it's not limited to Google only. 

A new <A href="http://wiki.kdenews.org/tiki-index.php?page=Icon+Guide">KDE icon guide</A> based on the former 
KDE icon guide written by Torsten is available at the <A href="http://wiki.kdenews.org/">KDE Wiki site</A>. 
You can find information on icon making, submitting icons to the KDE project, technical problems with SVG icons, 
references, software and much more. Check out the
<A href="http://wiki.kdenews.org/tiki-index.php?page=Icon+Guide#id468401">Icon Boot Camp</A>!

You couldn't try out KolourPaint because you are still running KDE 3.1? No excuses anymore for you because
a recent backport of KolourPaint to KDE 3.0 &amp; 3.1 has been made and now KolourPaint 1.0.1 supports all KDE 3.x releases.
You can find source code and packages at the <A href="http://kolourpaint.sourceforge.net/">KolourPaint website</A>.

Well speaking of releases, <A href="http://digikam.sourceforge.net/">Digikam</A> is shaping up for 
the upcoming 0.6.1 release and the project is voting on a 
<A href="http://digikam3rdparty.free.fr/splashscreens_for_0.6.1/">new splashscreen</A> for this release.

Finally, just hot off the press we've received word about the redesign of the popular KDE developer websites
<A href="http://www.kdevelop.org/">www.kdevelop.org</A> and 
<A href="http://developer.kde.org/">developer.kde.org</A>.  Enjoy.
<!--break-->
