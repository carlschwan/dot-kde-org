---
title: "QtRuby and Korundum Bring eXtreme RAD to KDE"
date:    2004-09-20
authors:
  - "rdale"
slug:    qtruby-and-korundum-bring-extreme-rad-kde
comments:
  - subject: "What bindings in general need is:"
    date: 2004-09-20
    body: "one killer application using them. Right now, I have a feeling that people use the bindings more for prototyping and quick apps for themselves and convert to C++ later on.. that's one thing that makes bindings great, but hurts them in the long run. \n\nJust my 2 cents on this, anyone agree?"
    author: "anon"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "My hope is that apps will spring up like mushrooms all over. Developing KDE applications with Korundum is so easy it feels like cheating. \n\nThere are certainly several good apps using PyQt/PyKDE out there. For instance, the TreeLine outliner, Eric IDE and an interesting sounding app called One Finger are all written in PyQt. I hope now that PyKDE is in the kdebindings cvs and part of every release along with ruby, it will help start to change people's perception of KDE being a C++ centric development environment.\n\nI believe C# and java are systems programming languages, which are more of a tidied up version of C++ than RAD languages. There isn't anything wrong with them, but they just aren't the same thing as python or ruby. That's why java has been successful on the server and a complete flop on the client, simply because server side programming needs systems programming languages and the client needs more dynamic RAD languages. Objective-C is a very dynamic language, which is why Apple's Cocoa development environment works so well. It couldn't be implemented using java, and although there are java bindings they don't get used much."
    author: "Richard Dale"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "> Developing KDE applications with Korundum is so easy it feels like cheating. \n\nThat's exactly what I feel when using PyQt. You write the code and you are wondering why it is so simple. Then you just run it and it works. The absence of a compile cycle is a real pleasure because the code works quicker. And languages like python or ruby are easier to manipulate than C++. No ';', no need for parenthesis inside every if/while, no variable type declaration, very few type errors, uniform API. It really feels easier.\n\nI have converted none of my PyQt applications to C++ and I see no reason why I would. "
    author: "Philippe Fremy"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "I love it the way the rubyists and pythonistas seem to be in violent agreement here.. :)"
    author: "Richard Dale"
  - subject: "Pure Lazyness"
    date: 2004-09-20
    body: "Python and Ruby may be nice but not enough if you're looking for pure lazyness. To achieve that we'd need Haskell bindings :D"
    author: "Pure Lazyness"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "what about basic or Delphi?\n\nMost people here celebrate Python and Ruby because they achieve the same what visual Basic and Delphi provided for years."
    author: "gerd"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "You are joking right ? I don't know for Delphi, but Visual Basic has some inner limitations (limited class model and other stuff) that python and ruby do not have.\n\nFor example, I sometimes like to do a dynamic class inheritance. I have also seen python used as a functional language, which I doubt Visual Basic could ever do.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "\"they achieve the same what visual Basic and Delphi provided for years\"\n\nNo, not at all - both python and ruby are a long, long way ahead of what Basic and Delphi provide. Basic started off as a simpified FORTRAN, pascal (ie the Delphi language) started off as a simplified Algol. Ruby and python don't share those ancestors, instead they are more branches off various AI research languages. I started programming in 1976 learning the language POP-11 on an AI course, and of all the languages I've used in the next quarter century ruby is the most similar. Even in the mid-1970s I regarded Basic (and FORTRAN) as an outdated pile of crap and I've always been puzzled as to why Basic subsequently became so popular in the 80's.\n\nMatz the ruby designer isn't familiar with POP-11 as far as I know, and so it is only a coincidence that ruby is similar. \n\nPython and ruby are languages for the 21st century, whereas Basic and pascal are both well past their sell by dates - I personally feel it is time to move on. Blocks, closures, continuations, list processing, pattern matching - that's ruby's realm and it used to be the exclusive preserve of AI languages like lisp (or POP-11 based on the lamda calculus like lisp, but with an Algol look)."
    author: "Richard Dale"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "I've always felt that Delphi was much more closely related to Modula3 (which is related to Pascal, which is related to pseudocode for programming which is arguably related to Algol).\n\nRuby and Python are certainly \"post compiler\" languages.  They do things in a way that ignores any considerations to the structure of the language compiler or interpreter.  Other languages are tied to certain concepts regarding the underlying architecture and mechanics of the processor and machine.  There are still some legacy concepts pulled over (hopefully the good ones), but they are very distinct from languages like C or C++... or even proper BASIC.\n\nIncidently, this somewhat supports the idea that C (and derived) languages have their place as the underlying layer - if only to keep out system level extensions that would otherwise constrain a high level language.  Use C (and kin) to build up from the processor to get to a level where easy tools can be used.  In that sense, KDE and (BSD|Linux|Unix) are basically device drivers for the abstracted language."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Pure Lazyness"
    date: 2004-09-21
    body: "\"I've always felt that Delphi was much more closely related to Modula3 (which is related to Pascal, which is related to pseudocode for programming which is arguably related to Algol).\"\n\nIf you read up on James Gosling's background you'll find he didn't have any object oriented programming experience before designing java and imposing it on the rest of us. He implemented a uscd-p system pascal compiler system on dec vax to solve some problem he had in the early 80's I believe, and just used that idea in conjunction with knowing the C++ had big problems, to invent java.\n\nI feel Anders Hejlsberg (Delphi/C# designer) has more intellectual horsepower than Gosling, and certainly would have known about Modular-3 when implementing Delphi. Even so, when I attended a Delphi introduction presentation in 1996 or so, I can only remember feeling utterly underwhelmed. I was a fulltime NeXTStep programmer used to the dynamic runtime of Objective-C, and just couldn't understand why a warmed over Object Pascal would appeal to anyone..\n\n"
    author: "Richard Dale"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-22
    body: "As opposed to a warmed over Object C? Smalltalkers on the other hand wonder what glue you've both been sniffing."
    author: "D"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-22
    body: "Yes, java doesn't seem to have been influenced by Smalltalk because maybe James Gosling didn't know anything much about it. Which is a shame - he wouldn't have come up with static methods instead of proper class methods with dynamic despatch. And classes would have been objects that you send a 'new' message to create an instance. That design error has been carried through to C# - I really don't like static methods.\n\n\"Resist the urge to overuse static methods.\":\n\nhttp://www.ftponline.com/javapro/2004_09/magazine/columns/proshop/"
    author: "Richard Dale"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-23
    body: "I have a feeling much of the reason is due to Borland's wide set of users that used TurboPascal and ilk to write real apps.  Much like a C programmer moves to C derived languages easily, Delphi was easy for somebody used to Borland's Pascal offerings.\n\nIncidently, Objective-C was specifically the reason I kept referring to \"C derived languages\".  I'd love to use it, but haven't had a project that was appropriate.  Other than a few single file programs one step beyond \"Hello World\", I haven't played with it.  Most of my time these days is spent with PHP and enforcing proper coding practices on developers with bad habits."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "Well Delphi would not achieve that, Haskell on the other hand is *Purely* functional and uses *Lazy* evaluation :) I was going for a easy harmless joke. Guess it didn't work."
    author: "Pure Lazyness"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "I got it. :)"
    author: "ac"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-20
    body: "So, when do you start one ? An OCaml binding would be welcome too. Given that there is a binding generator for already a few languages (Java, C#, ruby and perl if I am correct), it should not be that hard to add a few more."
    author: "Philippe Fremy"
  - subject: "Re: Pure Lazyness"
    date: 2004-09-22
    body: "Ashley Winters, the guy who designed the Smoke library did start on an OCaml binding. I don't know how far he got with it."
    author: "Richard Dale"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "I for one, having working for an entire year on a big commercial proyect 80% done in Python would welcome optional variable type declaration. Yes, pychecker helps a lot to catch sintantic errors that a compiler would catch in C++ (and a lot of other errors) or using not previously (auto)created vars and things like that, but it just can't catch:\n\nvar = 50\n[...some hundreds locs here...]\nvra = var + 1 # You'r fscked here\n\nor...\n\nmyReallyMustBeAList = [var1, var2, 34]\n[...bla...]\n# Woop! I really mean to do \"someOtherList.append(34)\" but \n# I've been working for 12 hours, you know...\nsomeOtherList = 34 # Now someOtherList is an Int\nmyReallyMustBeAList = someOtherList\nmyReallyMustBeAList[4] # Fscked up you are!\n\n\nI think Python 2.4 will allow you to declare the types of accepted/returned vars of functions and methods (using new python decorators); it's a start, but I would love to have _an option_ to enforce type declarations to avoid these kind of stupid but sometimes very hard to catch errors; it would help a lot in managing big proyects like mine. \u00bfDo you need some auto-declared var to do some dynamic magic or a function/method to act as a template? Just don't declare it (95% of vars aren't like these anyway). Declaring the type of vars, and not allowing them to change without explicit conversion, would also allow for some agresive optimizations, too.\n\nOn the other side, I love python, it's incredibly productive, the sintax is very clear and the libs are awesome."
    author: "juanjo"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "I agree 100%. For every big python project, the dynamic typing becomes a problem. I enforce type arguments on all my functions but that's clearly not enough.\n\nActually, there are very few circumstances where I would change the type of my variable dynamically. I consider that bad programming style. So a static typing system would be ok and certainly remove lot of problems."
    author: "Philippe Fremy"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "What about at like the start of the Python script you some way declare that all variables must be declared.  And then create a new 'dynamic' type so in case you want some variable to still be dynamic and all it could be.  Also if in the start it isn't declared that all variables must be declared, it acts like it does now.\n\nThat way everyone wins!  I think..."
    author: "Corbin"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "Well, say that go Guido ! Next time I'll interview him, I'll ask for sure :-)\n(shameless plug: http://www.freehackers.org/fosdem2002/guido.html)"
    author: "Philippe Fremy"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-21
    body: "That's not unlike what soft-typing languages do. In those languages (Common Lisp,  many Schemes, Dylan), a type declaration is an optional constraint. If you put it in, it can help the compiler optimize the code, and check for errors. If you leave t out, you can still use full dynamicity. The usual development style with such languages is you write your \"prototype\" in a fully-dynamic, general manner, and then gradually convert that into your production code by adding declarations, refactoring, and optimizing.\n"
    author: "Rayiner Hashem"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-21
    body: "That's in my list of reasons I not really like python.\nAlso you can count the sloppy OO style, and even the tabbed blocks, even admitting it is nice to force people to write cleaner code).\nI like more Pike, sadly it is soooo unknown and underused...\n"
    author: "Shulai"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-21
    body: "<BLOCKQUOTE>\n\"That is to say, if a program compiles in a strong, statically typed language, it just means that it has passed some tests. It means that the syntax is guaranteed to be correct (Python checks syntax at compile time, as well. It just doesn't have as many syntax contraints). But there's no guarantee of correctness just because the compiler passes your code. If your code seems to run, that's also no guarantee of correctness.\n\nThe only guarantee of correctness, regardless of whether your language is strongly or weakly typed, is whether it passes all the tests that define the correctness of your program.\"\n</BLOCKQUOTE>\n<a href=\"http://www.mindview.net/WebLog/log-0025\">Bruce Eckel</a>"
    author: "Ochoto"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-21
    body: "Oh, you mean what Perl solved with 'use Strict'. Sorry, I couldn't help myself.. :) One other thing that could actually help which isn't really what you're looking for is design-by-contract. I believe Python has some implementation of it, but I've only played with the Perl one. It's a little bit invasive, but can solve errors similar to type errors in the dynamic language realm. Check it out, you might like it ..."
    author: "Jonas B."
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "That's the commonly held view but I think it is (mostly) wrong. Scripting languages are certainly good for prototypes and quick apps, but in my experience with PyQt I can't recall anybody converting to C++ later on. They might have started a project with that intention, but soon realise that there is no point.\n\nI see the Python/Ruby vs. C++ issue as being the same as the C vs. assembler issue 25 years ago - when you look back on it in a few years time you'll wonder how you managed to write anything useful in C++.\n\nRichard's point is exactly right. Hopefully the increased exposure of alternative Qt language bindings will help challenge current perceptions. I'm aware of many more commercial PyQt applications than open source applications. The driver in the commercial world is programmer productivity/cost rather than any perceived architectual advantages or disadvantages."
    author: "Phil Thompson"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "I *know* I have read of such a project very recently.  Scribus, perhaps?\n\nIt is rare, however."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "Only the first versions of Scribus were written in PyQt.\nBut the text rendering was to slow and it was rewritten in C++.\nPerhaps it would have been better to replace only certain\nwidgets with C++.\nBut now there is still a possibility for scripting Scribus with Python"
    author: "Henning"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "If only Pyrex worked well with C++ you could write those pieces in almost-python and get almost-C++ speeds :-P"
    author: "Roberto Alsina"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "Why doesn't pyrex work well with c++? Why does it not work with PyKDE/PyQt?"
    author: "Humberto Massa"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "Quoting the Pyrex docs:\n\n-----\n\nFuture Plans\n\nC++ support. This could be a very big can of worms - careful thought required before going there.\n\n------\n\nAnd it doesn't work well with PyQt/PyKDE because there is no simple way to pass a C++ object to a Pyrex extension and use it from Pyrex.\n\nSure, you could use Pyrex for code that only needs to use integers and floats, but how much code like that is in your average Qt application?"
    author: "Roberto Alsina"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "Replacing certain widgets with C++ versions would have been my suggestion. This is similar to what was done with eric (the PyQt IDE). The quickest way to add a good programmer's editor was to port Scintilla to Qt, produce Python bindings for it, and write the rest of the IDE in Python.\n\nAnother common approach of commercial users when they want to revamp an existing C++ application is to isolate the core functionality in a library (if it's not already structured as such), produce Python bindings for it, and rewrite everything else (particularly the GUI) using PyQt."
    author: "Phil Thompson"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "Yep. It is exactly what I am doing at the moment for my job. We have a little C++ library that does some of the irreplacable system code. We wrap it with boost and use python everywhere else. It is a _very_ productive language. I also love the way I can quickly write complicated tests or the way I can make arguments expclicit:\n\nCompare:\nacs = generate_acs( ACS_ALWAYS, (ACS_AUTH, KEY_1), ACS_NEVER, ACS_NEVER )\nCreateFile( 300, 0x3F00, acs )\n\nwith:\nacs = generate_acs( read=ACS_ALWAYS, write=(ACS_AUTH, KEY_1),delete=ACS_NEVER )\nCreateFile( size=300, id=0x3F00, access=acs )"
    author: "Philippe Fremy"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "I think there are two \"hurts\" for bindings. Depending on your perspective they could be major, minor or trivial hurts, but I have tripped and bashed my knee on them in the past. The first is that you limit your pool of maintainers and code contributors. I don't know Ruby, so I can't help with a project written in Ruby without first learning the language. The second is that you imposing additional software (vm's, interpreters, runtime libs, etc) on the user. \n\nThe last thing I want to do is to discourage anyone from coding in any non-C/C++ language. But these language do come at a price and we should be aware of them so that we can balance those costs with the costs of using C/C++."
    author: "Brandybuck"
  - subject: "Re: What bindings in general need is:"
    date: 2004-09-20
    body: "The extra runtime requirements used to be a much bigger problem than it is nowadays. In the early days of PyQt you really couldn't ask ordinary users to try to install PyQt themselves. I was constantly helping prospective users of Kura, my language description software, to install Python, Qt, PyQt and everything in the right order. Nowadays, most distributions package a useful version of PyQt, and there are far fewer problems."
    author: "Boudewijn Rempt"
  - subject: "Oh what joy"
    date: 2004-09-20
    body: "This is great!  I recently discovered Ruby and I've been hoping to use it with KDE.. and now I can!\n\nRuby is an amazingly wonderful language and I urge everyone to take the time to learn it, even if you won't use it.  It's really quite a fun language to use."
    author: "KDE User"
  - subject: "Re: Oh what joy"
    date: 2004-09-20
    body: "I agree. I tried to learn both Python and Ruby a \nyear ago. I found Ruby was the better language for me\nbut never learned more than the basics simply because\nI had (and have) no real need for a scripting language :-)\n\nBut in case such a need emerges I would always choose\nRuby, probably with Korundum."
    author: "Carsten"
  - subject: "Re: Oh what joy"
    date: 2004-09-20
    body: "I'm not sure myself that I like Python as much as I did a few years ago. The language has grown a lot, adding cool & interesting concepts for which I, unfortunately, never had any use. Well, yes, microthreads and continuations were useful for one particular class of simulation. Now I'm mostly working in C++ and Java, and I miss Python's ease of use and expressive power, but well, it won't do for the large systems we are building at work, and doing a paint app in Python is not really possible, I found."
    author: "Boudewijn Rempt"
  - subject: "Re: Oh what joy"
    date: 2004-09-20
    body: "Boudewijn, could you take a guess as to how much of code you write for Krita is CPU-bound pixel pushing vs GUI stuff? I'm guessing that 70% - 80% of the code is  GUI stuff and not speed critical. (At least that is what I noticed the last time I wrote a graphics application in the early 90s, in 680x0(!!) assembler. oh the joy of being young and having too much spare time. :-) )\n\nWhat did you think of the idea of writing the canvas/image/document/pixel-pushing class in C++ and the rest of the code on top using Python? What made you not go that route? just curious.\n\ncheers,\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Oh what joy"
    date: 2004-09-20
    body: "I'm not sure of the actual ratio... Let's take a look. Krita is, according to sloccount about 50.000 lines of actual code. \n\nThe UI subdir is 5000 lines -- these are the special widgets, dialogs and so on. Stuff that would be easy to do in Python. \n\nThe tools subdir is 4079 lines. A nasty mix of ui code and event handling. The event handling is critical: we handle way more events per mouse movement than an ordinary app, in order to draw an accurate line. That wouldn't be doable in Python.\n\nThe plugins subdir is 2971 lines. A mixture of plugin admin code (which wouldn't be needed in Python, UI code, and finally code to filter pixels.\n\nThe modules subdir contains the color models, complete with code to composite pixels. This is 1918 lines, and it is too slow as it is in C++. We might need to use some mmx/sse/altivec assembly from the Gimp here.\n\nThe core is 18.577 lines. This includes a lot of code that simply ties slots to signals in the main view, but also all the data handling, which is complex and performance sensitive for an application that pretends to do more than wrap QImage. Much of it is also infrastructure definition: factories for paint operations, resources, tools -- some of it would not be needed in this form if we were using a language that has a class object. The main view class and the main document class could be done in Python, and perhaps the resource loading. But the image import/export/loading/saving, the painting is very critical.\n\nIn the end, I think that doing the interface in Python would not make much difference, because it isn't the largest part of the code in terms of work needed to get going. Coding a Qt interface is not much more work to do in C++ than in Python. It's data structures and application logic that are not performance critical where Python shines. After all, when you've used Qt Designer, most of the interface work is already done.\n\nNot that I did have a choice: Krita is by now five years old, and I merely picked up the threads were they'd fallen to the ground. I never contemplated doing a complete redesign, especially not since that would place Krita outside KOffice.\n\nThe paint application I tried to do in Python before I discovered Krita could be worked on was completely in Python. I found that Python was too slow and memory-hungry for the pixel-pushing part, and just when I had resigned myself to doing that bit as a core C++ library, I found Krita, and never did it. Otherwise it might have been a mixture of gcj-compiled Java and interpreted Jython code all using qtjava."
    author: "Boudewijn Rempt"
  - subject: "Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "Not to be trolling, but, what exactly are the benefits of this over KJSEmbed (other than say, being able to use Ruby because you like it more)? To me, I see this has the distinct disadvantage of depending on a non-kde scripting engine - while KJSEmbed uses a totally, 100% KDE/QT engine (KJS) that every KDE install already has.\n\n"
    author: "Jason keirstead"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "Unfortunately, kjsembed is not yet part of kdelibs (it's still in kdebindings, afaik), so it's not something \"everyone already has\". (Hopefully, this will change.)"
    author: "teatime"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-23
    body: "Eh. He was saying that every KDE install has KJS."
    author: "Qerub"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "QtRuby/Korundum (and PyQt/PyKDE) give you very complete coverage of the Qt/KDE apis. With around a 1000 classes and 30000 methods or so, they are 'heavy duty' eXtreme RAD environments. Kjsembed gives you a much smaller subset of the Qt/KDE api, and so it is more lightweight; the library is smaller and faster to load, and there is less to learn with just a subset of the target apis. If you can master PyKDE or Korundum, then great, but in spite of the simplicity of python or ruby, they do have *very* large apis and an associated steep learning curve, although not so steep if you follow the tutorials in the KDE ruby bindings Developers Corner..\n\nEvery KDE install has ruby and python bindings from 3.3 onwards, and so the argument about ubiquity no longer applies.\n"
    author: "Richard Dale"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "And don't forget all the high quality 3'rd party library's you get access to with Python and I presume the same is true for Ruby.   "
    author: "Morty"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "QtRuby vs KJSEmbed ? The answer is simple: one is for ruby, the other for javascript. Both languages are fundamentally different. While not familiar with any of them, I know enough about their general characteristics to tell you that they do not appeal to the same programming style. Javascript is more simple, ruby is more complex. Some will say that javascript is too simple to do complicated tasks, other will tell you that learning ruby is too complex to do simple (or complex) tasks. It is all a matter of taste.\n\nYou can also take into account which community is around the language. Lot of web developers are with Javascript, lot of programming guys are with ruby. And so on ..."
    author: "Philippe Fremy"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "IMO ruby is the simplest language I know of, some people call it executable pseudo code :-)\n\nWeb developers use javascript since ruby/perl/python aren't supported (AFAIK) as client-side web-scripting language.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-20
    body: "That was the line I always used to sell Python to management... Look! It's a design document you can run :-). I'm really happy that there's so much choice nowadays in bindings. Personally, if I have a suitable problem, I'd still go for PyQt, but I like my languages with as few punctuation marks as possible. Double colons remind me too much of the medical imagery I'm writing apps for nowadays.\n\nAnyway, Cyrille Berger is working on a kjsembed plugin for Krita, and I'm going to get in touch with Jim Bublitz one of these days to ask him whether he can help me with a Python plugin."
    author: "Boudewijn Rempt"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-21
    body: "Ruby may still need the occasional :: (especiall in RubyQt), but overall its fairly sparse in the punctuaction dept. No semicolons I guess being the biggest difference, as well as using begin and end to define most functions."
    author: "Ian Monroe"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-21
    body: "Note than you can use an 'include Qt' or 'include KDE' directive if you prefer to have Widget over Qt::Widget, or Application over KDE::Application in your code. Thats much like the 'using' directive for C++ namespaces:\n\nrequire 'Korundum'\ninclude Qt\ninclude KDE\n \nabout = AboutData.new(\"p2\", \"Hello World\", \"0.1\")\nCmdLineArgs.init(ARGV, about)\na = Application.new\nhello = PushButton.new( a.i18n(\"Hello World !\")\na.mainWidget = hello\nhello.show\na.exec\n"
    author: "Richard Dale"
  - subject: "Re: Benefits over KJSEmbed?"
    date: 2004-09-21
    body: "Oops, syntax error, it should be:\n\nhello = PushButton.new(a.i18n(\"Hello World !\"))\n\nAnd PushButton is ambiguous, does it mean a QPushButton or a KPushButton in this case? So maybe use the include directive with care.."
    author: "Richard Dale"
  - subject: "PhpQT / PhpKDE"
    date: 2004-09-20
    body: "I would really love to see PHP bindings.  Especially now that PHP5 is stable.\n\nI know the first response will be that I should write one.  I would love too, I just can't find any docs for Smoke (I think that is the preferred kde bindings method).\n\n-Jackson"
    author: "Jaxn"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-20
    body: "I would like to see that too.  Though I haven't even heard of smoke, so obviously your ahead of me!\n\nI need to try out PHP5 some time..."
    author: "Corbin"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-21
    body: "I keep meaning to try and write up some Smoke documentation. When I started on the ruby bindings last year I used a paper by Germain Garand which he posted on the kdebindings mailing list. I haven't been able to find it since, and keep meaning to ask Germain to possibly check it into the kdebindings cvs.\n\nHow much man effort is involved in implementing a new language binding? I would say say about 6 man months at the minimum. I've been working on the ruby bindings for over a year - I started in July 2003, and it's taken me until about now to announce the first release. I've done some other work on java and C# bindings, but really most of the last year has been spent working fulltime on getting Smoke working with the KDE classes, and implementing the ruby bindings. You can't really do a language binding in your spare time. Compare and contrast the ruby-gnome project - they have 16 or 17 developers. But their approach depends on splitting up the work, and hand crafting the bindings for each class - I call that the 'knitting circle' approach. Each developer doesn't need to be that skilled, and they can do it in their spare time.\n\nFor KDE 3.4 the Smoke library needs to be more modular, and broken up so that it is easy to implement plugin apis for the various KDE projects - kontact, kmail, kate and kdevelop for instance. The Amazing Lypanov suggested a way to do this at aKademy, and I've had some discussion since with Joseph Wenninger of the kate project, so I'm happy it can be done and put on the 3.4 release plan.\n\nI used to think that KDE should have as many language bindings as possible, but I'm no longer sure if that's a good idea. I now think we have to focus on perl, python, ruby, java and C# and basically ignore everything else. If someone wants to give up six months and work on php bindings, then fine, but otherwise it won't happen. It takes much more effort to learn the Qt/KDE api than it does to learn the syntax of a language like php. So if you're a php programmer with server side experience, it's easier to learn ruby while you're learning the Qt/KDE api."
    author: "Richard Dale"
  - subject: "Re: PhpQT / PhpKDE, KConfigDialog"
    date: 2004-09-21
    body: "I kinda like using PHP for shell scripting, cause its syntax is so easy. I wouldn't mind some PHP KDE/Qt bindings mostly for such small jobs. Granted, I'd rather have the Ruby KDE/Qt bindings, I haven't played around with them for several months its good to see there's been plenty of progress."
    author: "Ian Monroe"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-21
    body: "\"focus on perl, python, ruby, java and C# and basically ignore everything else\" but that leaves out the one scripting language that is arguably more popular than any of the ones just listed. For all the individual worth of the various languages mentioned, I'd put money on PHP being the more approachable, therefor generally usable, language for desktop fiddling by the masses. Perhaps I am biased and when I say \"the masses\" I really mean me but I really do suspect that if a PHP + Qt/KDE gui building app was available it would see more and faster desktop trinket innovation than from any other language.\n\nWith Qt4 beta SDKs available and the release of PHP5, now would be a good time to put some effort into some PhpQT / PhpKDE project. Anyone seriously interested ? "
    author: "markc"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-21
    body: "I am sure you gonna find someone. However, after spending a few months trying to fix OTPC[1] I can tell you: a 15KLOC app in PHP is going to be an amazing feat. Not pretty, but amazing.\n\n\n\n[1] Other people's perl/php code"
    author: "Roberto Alsina"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-22
    body: "Like I said before, I am ready and willing to do PHP bindings.  \n\nI will make you a deal Richard...\n\nYou get that smoke documentation done and I will do PHP bindings (with the help of friends like Marcus Whitney).\n\n-Jackson"
    author: "Jaxn"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-22
    body: "Yes I certainly don't want to discourage anyone, only point out it's quite a lot of work. I've just emailed Germain Garand about some Smoke notes he had, and he's sent them to me, so I'll try and get it written up and put in the kdebindings cvs and on the KDE Developer's Corner site. I've been meaning try and get Smoke written up better for a while.\n\nYou can subscribe to the kde-bindings@kde.org mailing list and ask bindings questions there. To use Smoke with PHP, it will need to have some sort of 'catch-all' function which is called if a particular method can't be found. In ruby that's 'method_missing' and in perl it's 'autoload'. So you catch method calls there, and then forward them onto Smoke.\n\nclass MyClass\n\n    def method_missing(methId)\n        puts \"can't find method: #{methId.id2name}\"\n    end\n\nend\n\nobj = MyClass.new\nobj.foobar -> can't find method: foobar\nobj.baz -> can't find method: baz\n\nI don't know if that makes sense - you can call any old method on obj, and it will be trapped by method_missing, and print an error message in this case. The first step is to try and do something similar in PHP.."
    author: "Richard Dale"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2004-09-27
    body: "http://us2.php.net/manual/en/language.oop5.overloading.php\n\nNote the __call magic method that does exactly what you are describing.  We're ready :)"
    author: "Marcus Whitney"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2005-01-13
    body: "So it's early 2005 and I am wondering if there has been any progress realizing a  php-qt application anywhere on the planet ?\n\nBetter still php-kde. I just did a google search and this is one of the few pages that came up :-) I guess I can always try again in 2006."
    author: "markc"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2005-12-21
    body: "I did some work on it and got some of the Smoke stuff working, but then had to put it aside for other stuff (i.e. stuff that feeds my family).\n\nIt would be cool though."
    author: "Jackson"
  - subject: "Re: PhpQT / PhpKDE"
    date: 2007-06-13
    body: "Well Well, half way thru 2007 and still no sign of a PhpKDE. There is a php-qt project that has been picked up, although there is still no viable release that I am aware of, and it is still not a PhpKDE.\n\nAnyone heard of anything out there somewhere, particularly based on KDE4 ?\n"
    author: "markc"
  - subject: "Gentoo?"
    date: 2004-09-21
    body: "I'm trying out the Gentoo distribution. Which package should I emerge to get QtRuby?"
    author: "Jonas B."
  - subject: "Re: Gentoo?"
    date: 2004-09-21
    body: "Alex the qtruby co-author would know:\n\nhttp://lypanov.blogspot.com/2004_06_06_lypanov_archive.html#108661246168614204\n\nIs it kdebindings?"
    author: "Richard Dale"
  - subject: "kdebindings"
    date: 2004-09-21
    body: "Yes, its kdebindings make sure you get the 3.3.0 version which is marked ~x86. At least the last time I installed kdebindings it didn't install ruby stuff, but it did when I emerged 3.3.0 over night."
    author: "Ian Monroe"
  - subject: "C++ and Qt"
    date: 2004-09-21
    body: "Besides various bindings it would be nice to have some advanced C++ features in Qt as well. I am especially missing: \n\n1) exceptions\n2) multiple inheritance from two QObjects\n3) better use of namespaces \n\nA library without exceptions in 2004 is a bit poor really."
    author: "M"
  - subject: "Re: C++ and Qt"
    date: 2004-09-21
    body: "You sure? When I built Qt last night I used: configure -no-g++-exceptions. I wonder if you change this to yes, do you get your exceptions?\n"
    author: "Morty"
  - subject: "Re: C++ and Qt"
    date: 2004-09-21
    body: "No that has nothing to do with Qt its a only a compiler option. You would need some additional (though trivial) code in Qt. Exceptions are a much better way to do error handling, since you don't have to check for errors based on the return value of each function you call, but can simply monitor a block of code. Another advantage is that exceptions can be thrown during object creation in the constructor and that the return value of a function can be used for something else then an error/status value."
    author: "M"
  - subject: "Re: C++ and Qt"
    date: 2004-09-22
    body: "> You would need some additional (though trivial) code in Qt\n\ntrivial? The code itself might be trivial, but it's by far not trivial to make a class exception safe. Take a look at the amount of articles about exception-safety on http://www.gotw.ca.\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: C++ and Qt"
    date: 2004-09-23
    body: "Ok, agree you have to be a programmer to be able to do it."
    author: "M"
  - subject: "Re: C++ and Qt"
    date: 2004-09-21
    body: "I'm going to guess its because Qt is portable and perhaps (until recently?) exception handling hasn't been portable enough. But I don't know really."
    author: "Ian Monroe"
---
For those of you not in the know, <a href="http://www.ruby-lang.org/en/20020101.html">Ruby</a> is a relatively new general purpose scripting language that is surprisingly expressive, allowing developers to prototype and develop powerful applications in a short time.  Now with <a href="http://developer.kde.org/language-bindings/ruby/">QtRuby and Korundrum</a>, that power and expressivity has increased: You can sketch out pretty interfaces with <A href="http://www.trolltech.com/products/qt/designer/">Qt Designer</a> and automatically create Ruby code with the rbuic tool. Or do amazing things with DCOP without needing preprocessors, makefiles etc -- just type in your Ruby script and be in control of your desktop.  In fact, you can find a fairly complete description of all the features supported by QtRuby and Korundrum over at the <a HREF="http://developer.kde.org/language-bindings/ruby/">Ruby bindings section</a> of the <a href="http://developer.kde.org/">KDE Developer's Corner</a>.

<!--break-->
<p>
I've also translated a couple of tutorials into Ruby. There is a Ruby version of 
 <a HREF="http://developer.kde.org/language-bindings/ruby/tutorial/tutorial.html">Qt Tutorial #1</a>, and the corresponding Ruby code is in 
 qtruby/rubylib/tutorial/. That's the one where you build a game to fire a cannon at a target; it explains the basics of Qt widgets such as using layout managers, and how signals and slots work. 
<p>
For KDE, there is a Ruby translation of a <a HREF="http://developer.kde.org/language-bindings/ruby/kde3tutorial/index.html">KDE 3.0 tutorial</a>
  originally written for C++ by Antonio Larrosa Jim&eacute;nez. 
  The sources are in korundum/rubylib/tutorials/. In nine steps it takes you from simple Hello Worlds in Qt and KDE, right up to building a customized browser using the KDE::HTMLPart component which communicates with a bookmarks app over DCOP.
<p>
 
The QtRuby and Korundum bindings have been backported to Qt 3.1.x and KDE 3.1.x, and so now it should be possible to build them on any KDE installation from the last year or two, right up to the current KDE 3.3. You can obtain recent CVS snapshots on the Rubyforge 
 <a HREF="http://rubyforge.org/projects/korundum/">QtRuby/Korundum site</a>.

