---
title: "KDE-CVS-Digest for June 4, 2004"
date:    2004-06-05
authors:
  - "dkite"
slug:    kde-cvs-digest-june-4-2004
comments:
  - subject: "Thanks"
    date: 2004-06-05
    body: "Thanks once again for CVS-Digest, Derek.\n\n<-------->\nIt has become clear that any bugs in KDE software have some hidden significance. In the interest of full disclosure, I want to inform you that any bugs in this issue, known or unknown have the purpose of demoralizing the Tampa Bay Lightning, messing with their minds, so they lose tonights game. Go Calgary.\n<--------->\n\nHeh, go Ville, go Miikka, Go Toni!!! :) "
    author: "138"
  - subject: "Ode to Enrico Ros"
    date: 2004-06-05
    body: "The man,\nFirst he made us some pretty,\nSome very pretty,\nOpenGL analyzers,\nFor amaroK.\n\nAnd NOW!\nHe doth speed up so many,\nSo very many,\nDifferent pieces,\nOf KDE.\n\nWow!\nWhat a guy!"
    author: "Max Howell"
  - subject: "Re: Ode to Enrico Ros"
    date: 2004-06-05
    body: "FULL ACK!\n\nEnrico you rock! Keep up the excellent work!"
    author: "me"
  - subject: "Re: Ode to Enrico Ros"
    date: 2004-06-05
    body: "Don't forget his nice KFireSaver3D."
    author: "Anonymous"
  - subject: "Re: Ode to Enrico Ros"
    date: 2004-06-06
    body: "Here's a thought..\n\nWhat if juk and amarok came together..\nSo juk could optionally use amarok to actually play the songs.  Amarok and juk would share the play list..\n\nThat way amarok can have a very simple playlist system, with juk there if you need something heavy weight, and juk can have a simple play system with amarok for the graphics and equalisers and whatever else it has."
    author: "johnflux"
  - subject: "Re: Ode to Enrico Ros"
    date: 2004-06-06
    body: "What about integrating Apollon/giFT with JuK, so the media player also have file sharing support... Are there some IP issues with that, potentially detrimental for KDE?"
    author: "Martin Galpin"
  - subject: "Re: Ode to Enrico Ros"
    date: 2004-06-06
    body: "Well, for now we can't even get Apollon into kdeextragear because people think we would be a potential target for lawsuits :("
    author: "Arend jr."
  - subject: "Re: p2p combined with media player"
    date: 2004-06-06
    body: "The idea sure is enticing, but I'd rather not have my name listed in such an application :)\n\n--\nMark."
    author: "Mark Kretschmann"
  - subject: "Re: Ode to Enrico Ros"
    date: 2004-06-06
    body: "Thanks, but it's so embarrassing to see my name in this page! :-P\nI have to thank you, Max, for working on the best audio player (amarok is a sufficiente reason for me to use kde, I love it's context browsing feature and the player's layout :-) and for providing good code to the community (see make_it_cool: kdenetwork/kget/browserbar.* to know how shamelessy robber I am ;-).\n\nBy the way, my only merit is to own a p4 mobile that can be slowed down to 300MHz (with simple khotkeys shortcuts), where all the slow things become more visible. Then using \"valgrind --tool=calltree\" I see where wasted cpu cycles go to and it's so easy to spot problems in code using \"kcachegrind\" with relative percentage costs. A BIG THANKS to VALGRIND/CACHEGRIND authors for providing such powerful tools even to inexperienced users & programmers ^_^.\n\nAnyway thanks must be given to the *core* people.. Leo, Waldo, David, Stephan, Lubos, Zack .. just to name the ones I read of more often while working on the 5-10 files I'm used to analyze. Sometimes I optimize a loop while they optimize a whole infrastructure that get rids of tens of loops :-) That's coolness!\nFinally thanks to the enthusiastic kde comminuty\nback to work now.. ;-P\n"
    author: "Enrico"
  - subject: "Funny"
    date: 2004-06-05
    body: "LOL! Some bugs are _really_ fun:\n\nhttp://bugs.kde.org/show_bug.cgi?id=61632\n\nThis is my new favorite after\n\"KWeather only shows sand storms in Sidney\"\nor sth. like that I found in the CVS digest\nsome time ago.\nProbably not everyone here share my humour but\nI laughed a lot about those bugs..."
    author: "Martin"
  - subject: "No red spot on moon"
    date: 2004-06-05
    body: "Or this one: http://bugs.kde.org/show_bug.cgi?id=34295 - watch the reporter's address!"
    author: "Anonymous"
  - subject: "ad : vote for that bugs"
    date: 2004-06-05
    body: "I found that wishes in bugs.kde.org : they are just _good<_ !\n\nhttp://bugs.kde.org/show_bug.cgi?id=82562\nhttp://bugs.kde.org/show_bug.cgi?id=82581\n\nVote for them !!!\n\nAnd continue the nice work..."
    author: "Pinaraf"
  - subject: "Internationalization Status?"
    date: 2004-06-06
    body: "Anyone knows why the statistics of the internationalization status on the cvs-digest are different from the status on http://i18n.kde.org/stats/gui/HEAD/toplist.php ?"
    author: "AC"
  - subject: "Re: Internationalization Status?"
    date: 2004-06-06
    body: "If I remember what I read correctly, the translation file status on the i18n site is updated automatically every hour or two.  The digest itself isn't that recent.  Changes caused by proof reading documents, etc. can cause there to be more new translations needed, and since everyone is gearing up for a release, and the deadline for translations is later than for major code changes these changes may become more common in the next little while."
    author: "Tim Ginn"
  - subject: "Re: Internationalization Status?"
    date: 2004-06-06
    body: "It's fixed now. I was getting the stats from \n\nhttp://i18n.kde.org/stats/gui/HEAD/top10.php\n\nwhich are obviously out of date.\n\nThanks\n\nDerek"
    author: "Derek Kite"
  - subject: "kdepim advances"
    date: 2004-06-06
    body: "it is great to see more konnectors being written for kde-pim. With each possibility to synch with another software (be it egroupware, kolab, syncml, files, opie etc), kontact's value increases greatly!\n\nThank you so much!"
    author: "me"
  - subject: "OT: Please could you vote for this bug"
    date: 2004-06-06
    body: "To make konqueror just better:\nhttp://bugs.kde.org/show_bug.cgi?id=77561"
    author: "Yves"
  - subject: "and for this one too :p"
    date: 2004-06-07
    body: "http://bugs.kde.org/show_bug.cgi?id=15848\n\nIt's such an old wish, I'm not a programmer, so I'm asking if it is problematic adding this feature with the current implementation of Konqi. It's the only big thing I miss from Mozilla Fx, and looks like I'm not the only one :o)"
    author: "Lok"
  - subject: "Yesterday"
    date: 2004-06-07
    body: "Yesterday we had a IRC talk at FFII about groupware solutions for our work. As there are so many different servers and clients what do you recommend us?\n\n- different projects with events and calendars\n- access from multiple operating systems\n- for about 300 people maximum\n- access via web client as an alternative (needed for those in Parliament with IE/NT and strong Firewall)\n\nThere seems to be a lack of standardisation in interfaces of groupware. \n\nI-Calendar-Format for data exchange? \n\nWe currently use plone for event management, but it takes a lot of time, so it is only used for key events.\nhttp://plone.ffii.org\n\nI would rather want to use an \"offline\" client and sycronize it with a usual calendar. \n\nSo Kolab/eGroupware/OpenGroupware and so on, what do you recommend? What Servers are mature and mutliplattform?"
    author: "Andr\u00e9"
  - subject: "kitchensync"
    date: 2004-06-07
    body: "anyone successfully used kitchensync ? can you please report your impression and what you did ?\n\nchris"
    author: "chris"
  - subject: "Re: kitchensync"
    date: 2004-06-07
    body: "Nope. Kitchensync is much too under development. Just be patient,\nit seems the situation is about to stabilise soon"
    author: "MacBerry"
  - subject: "Re: kitchensync"
    date: 2004-06-07
    body: "its marked as *done* in the 3.3 feature plan."
    author: "chris"
  - subject: "I hope that's a joke then..."
    date: 2004-06-08
    body: "I have been CVS upp'ing kitchensync evry day for the past 3 weeks hoping *someday* I could sync my new Zarus.\n\nSo far I have had 0 success...."
    author: "Jason Keirstead"
  - subject: "Re: kitchensync"
    date: 2004-06-08
    body: "indeeed. kitchensync as a framework is done. now, what we need are connectors...\nand here, there is quite a lot of work to do"
    author: "MacBerry"
  - subject: "Re: kitchensync"
    date: 2004-06-08
    body: "i wondered where my sms are going to be synced with. anyone an idea. With knotes ? i dont think thats good. perhaps something like an sms container app is needed.\n\nchris"
    author: "chris"
---
In this <a href="http://cvs-digest.org/index.php?issue=jun42004">week's KDE CVS-Digest</a>:
<A href="http://www.kdevelop.org/">KDevelop</A> has a new Qt Designer port. 
<A href="http://edu.kde.org/kstars/">KStars</A> adds support for Philips webcams.
Two more icons sizes are added to <A href="http://www.konqueror.org/">Konqueror</A> file mode.
The PIM Kitchensync supports syncing <A href="http://www.kolab.org/">Kolab</A>, <A href="http://www.egroupware.org/">eGroupware</A> and <A href="http://opie.handhelds.org/">OPIE</A> addressbooks.
Crypto improvements in <A href="http://kmail.kde.org/">KMail</A>.
KWin adds per window settings. 


<!--break-->
