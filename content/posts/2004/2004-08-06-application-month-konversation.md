---
title: "Application of the Month: Konversation"
date:    2004-08-06
authors:
  - "kstaerk"
slug:    application-month-konversation
comments:
  - subject: "Very nice IRC-Client"
    date: 2004-08-06
    body: "I switched to Konversation a while ago, mostly because XChat doesn't autoaccept DCC Transfers anymore on my system. I haven't missed XChat since then.\n\nThe Konversation Interface is nice and clean. Only the handling of DCC Chat Sessions could be better. The whole GUI blocks if you get alot of messages through a DCC Chat Connection; same for login into a Server, the GUI blocks and nothing is shown in the statuswindow, until the connection is completly ready.\n\nSo many thanks to the developers for this application!"
    author: "Devlin"
  - subject: "The latest version rocks"
    date: 2004-08-06
    body: "The latest version of konversation I'm using, simply rocks. Why? Because uses OSD (on screen display) to show me what people are talking, and I don't have to switch desktops or windows to look at this. For me, this feature is ___great___."
    author: "suy"
  - subject: "Nice"
    date: 2004-08-06
    body: "Just installed it, and actually like it better than xchat :D\nThere are still a few rough edges, it could use a few usability improvments too, overall it's already pretty good though.\n\nP.S.: I installed SuSE 9.1 RPM in SuSE 8.2, works fine :)"
    author: "John IRC Freak"
  - subject: "Re: Nice"
    date: 2004-08-07
    body: "I'm currently using xChat, but I'll give Konversation a shot.\n\nI've been trying to get rid of GTK apps and go all QT to save RAM and ressources on my ancient hardware. Right now GAIM and xChat are the last two big ones left to replace.\n\nHopefully Konversation development is progressing nicely and it will soon be part of the official KDE distribution so that more people can use it, fix it, improve it, etc.."
    author: "Mikhail Capone"
  - subject: "Re: Nice"
    date: 2004-08-07
    body: "That makes two of us. I use Kopete already and I works pretty fine, Xchat is still here just in case but I'm doing quite fine with konversation (even though it has its quirks). It's the Gimp I probably won't get rid of anytime soon..."
    author: "John IRC Freak"
  - subject: "To save even more ram"
    date: 2004-08-09
    body: "...why not just use Kopete for IRC?\n\nIt's upcoming 3.3  version can do almost everything Konversation can."
    author: "Jason Keirstead"
  - subject: "Screenshots"
    date: 2004-08-06
    body: "Do the screenshots on their homepage reflect the\ncuzrent version? Then I would rather not try it.\nWhy oh why do IRC-Clients always have such a horrible\nuser interface. Why is noone able to write a decent\nuser interface for chatting? Konversation looks\ncompletely different than any other KDE app.\nNo toolbar, lots of strange labeled buttons (DeOp) etc.\nThis is not necessary but a sign that noone had\na really clever idea on how to make a good interface.\nWelcome to Geek-ville. Sigh. Other KDE apps are really\nwell done UI-wise in comparison to open source as well\nas commercial apps. Not even Windows has such a high\nconsistency between apps. \nCurrently I'm using mIRC on WINE which is quite bad but \nthe most usable choice if you want DCC. Once it works properly \nwith Kopete I'll switch. I would prefer an app dedicated\nto chatting however. I think a completely new approach\nin terms of UI should be taken. mIRC is not really good\neither but the one-eyed among the blind.\nWhy can't there be an application which looks to the user just\nsimilar to a web-based chat. Everyone can use those but I have\na hard time explaining mIRC to my family. It should especially\nbe more inituitive: For example a start screen should appear\nwhich says: Select the IRC-Network you want to connect to:\n(List of all *major* networks and description of their strengths\nand weakneses) plus button \"Enter user defined network\"\nNext: Enter your nickname plus checkbox \"Remember next time\"\nNext: There MUST be some obvious way to search for channels\notherwise this is useless for a novice user!!"
    author: "Martin"
  - subject: "Re: Screenshots"
    date: 2004-08-06
    body: "The interface is quite configurable. You can enable a toolbar and disable the quick buttons."
    author: "Anonymous"
  - subject: "Re: Screenshots"
    date: 2004-08-06
    body: "You should definitely see X-Chat Gnome: http://xchat-gnome.navi.cx/\n\nIt's a front-end for X-Chat, which tries to comply to the Gnome HIG. It has a much cleaner front-end than a lot of other IRC clients."
    author: "Xunil"
  - subject: "Re: Screenshots"
    date: 2004-08-07
    body: "huh? Konversation is more or less a 1:1 adaption of xchat.\n\nBoth definitely look different from other apps of their DE but the interface has a high usability (real usability not that \"I sat my 3yr old in front of inDesign and he couldn't replicate the layout of time magazine in 1 hour, you guys suck\" that's all too often substituted)\n\nBut Konversation lacks a few features to replace x-chat for me:\n-Notifications on a per channel basis should be off by default (or there should be an option so they're off) I have one channel where I want a beep on channel message and 20 where I don't. That's a lot of clicking in Konversation\n-Ports for DCC. I'm behind a firewall, nuff said\n-Hide join/part messages (Am I blind? I'd think that feature is too basic to be not implemented yet but I can't find it\n-A better server list (network centered, not server centered, more than 1 autojoin on connect, etc)\n\nNow I'd really like to use konversation (I use kde, konversation has a number of improvements over xchat (the dropdown menu in the nick dialog, regexps in the channel list, osd, some others I've forgotten) so I hope Konversation catches up soon (should be possible if you look at how fast it evolved in the last few months)"
    author: "anonymous coward"
  - subject: "Re: Screenshots"
    date: 2004-08-07
    body: "argh how could I forget the most important of them all:\n\nthe \"tabular view\" of xchat - the separating line between nicks (on the left) and the msgs they posted (on the right) \n\nIt's *much* easier to follow channel traffic this way"
    author: "anonymous coward"
  - subject: "Re: Screenshots"
    date: 2004-08-07
    body: "> Hide join/part messages (Am I blind? I'd think that feature is too basic to be not implemented yet but I can't find it\n\nI didn't find it either, so I wrote a wishlist item for that a while ago:\nhttps://sourceforge.net/tracker/index.php?func=detail&aid=891965&group_id=53539&atid=470696"
    author: "Melchior FRANZ"
  - subject: "Re: Screenshots"
    date: 2004-08-10
    body: "Configure Konversation.../Behavior/Chat Window/[x] Hide Join/Part/Nick events"
    author: "Anonymous"
  - subject: "Re: Screenshots"
    date: 2004-08-06
    body: "You should try it out before you make pre-emptive conclusions. The interface is very nice. Far better than any other IRC client I have used at least.\n\nPerhaps the fact that nobody has made an IRC client that looks simple to people just looking at the screenshots suggest that it is hard to do so. Konversation is a client that has tried hard to make a decent UI and I am sure it will improve further with time.\n\nI think your request is reasonable, for an ultra-simple IRC client. But perhaps IRC is just too complex to ever be expressed as simply as IM clients can be."
    author: "Max Howell"
  - subject: "KDE 4"
    date: 2004-08-06
    body: "I hope Konversation replaces KSirc in KDE 4 - a run-time dependency on Perl is just sick."
    author: "Anonymous"
  - subject: "Re: KDE 4"
    date: 2004-08-07
    body: "You've never used fish before, have you?  "
    author: "AC"
  - subject: "Re: KDE 4"
    date: 2004-08-08
    body: "Isn't that a remote run-time dependency? :-)"
    author: "Anonymous"
  - subject: "Re: KDE 4"
    date: 2004-08-08
    body: "Also perl isn't required.  It's just used to make it faster if it is there.  "
    author: "JohnFlux"
  - subject: "Addressbook"
    date: 2004-08-06
    body: "Kontact is also the first irc client to integrate with kaddressbook.\nI'm working with bille (kopete guy) and getting the two to use standard tooltip, custom fields, irc server-group lists, and irc nick.\n\nIn the next release, you'll be able to run konversation, run kaddressbook, and from the addressbook see who is online :) If you make an association in kopete it will show up in konversation.  From konversation you can associate any number of nicks with an addressbook contact.  And so on. :)\n\nIt's almost done, I just need to finish a refactoring to make it efficent when transmitting the nick status changes to addressbook. (online, offline, away, etc).  But it works at the moment."
    author: "JohnFlux"
  - subject: "Re: Addressbook"
    date: 2004-08-06
    body: "Kontact an IRC client? Seems too much integration confuses you. :-)"
    author: "Anonymous"
  - subject: "Re: Addressbook"
    date: 2004-08-06
    body: "damn, and I was concentrating on not using the wrong words too.. ;)\n\n/me mutters"
    author: "JohnFlux"
  - subject: "Not true"
    date: 2004-08-09
    body: "I assume you mean Konversation.\n\nAnd this isn't true. Kopete integrated with KAddressbook before Konversation."
    author: "Jason Keirstead"
  - subject: "Re: Not true"
    date: 2004-08-09
    body: "Okay okay, I didn't count kopete as an irc client :P\nBesides, konversation is better integrated than kopete at the moment."
    author: "JohnFlux"
  - subject: "Re: Not true"
    date: 2004-08-10
    body: "How so?\n\nI find this really hard to believe since the DCOP interface used to sync with KABC was written by a Kopete developer, was based on the Kopete DCOP interface, and Kopete was the first app using it fully :P"
    author: "Jason Keirstead"
  - subject: "Re: Not true"
    date: 2004-08-10
    body: "hum, given that I keep slipping up on everything I say, I should really just quit my losses and give up now.\n\nBut what the hell...\n\nBille (kopete devel who did kopete interface to kaddressbook) has done all the grunt work with his kimproxy and kimiface work.\n\nIn konversation, it uses 'kimiface', like in kopete, to expose the state of people online/offline etc.  However what konversation does it once they are linked, use that information to show things like the user's information from the addressbook as a tooltip when you hover over.\nIt also stores the information on which nick is associated with which addressbook, in the actual addressbook.  So if you go into kaddressbook, open a contact, go to IM page, and add a new IRC contact, it will be updated immediately in konversation when you save that contact.\n\nKopete does replicate its information to kaddressbook, but currently doesn't do much in pulling the information back out again..\n\nSome of the other things are a bit more subtle.. like from konversation when you edit a contact, it uses kaddressbook via dcop, and things like that. Hmm hard to explain.\n\nAnyway, all this aside, I'm working hard on this, for both konversation and kopete, and hopefully my coding will be better than my PR, spellings and grammar.\n\n:)\n\nJohnFlux"
    author: "JohnFlux"
  - subject: "Would be good, if........."
    date: 2004-08-18
    body: "If it was a new way to use IRC...... It is only a skin..... and irt look like a cheap copy of Xchat...... I hope it will improve... i tried it.. I found no5thing but a KDE look alike GUI over an Xchat client... with 50% less features...... And well it is not a too buggy client, it still not do much....Keep working on  it..... i wish you well, but atm, I will stick with Xchat, or even IIrc..... but the GUI is cute..... as every KDE interfaces are..... \n\nWill try it later Ciao!  \t"
    author: "Jacques Levesque"
---
A new issue of the series "Application of the Month" has been released. It covers <a href="http://konversation.sourceforge.net/">Konversation</a> and interviews its maintainer Dario Abatianni. Konversation is a user friendly IRC client for KDE. You can read it in <a href="http://www.kde.nl/apps/konversation/">Dutch</a>, <a href="http://www.kde.nl/apps/konversation/en/">English</a> and <a href="http://www.kde.de//appmonth/2004/konversation/index-script.php">German</a>.


<!--break-->
