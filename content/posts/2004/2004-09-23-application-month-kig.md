---
title: "Application of the Month: Kig"
date:    2004-09-23
authors:
  - "ateam"
slug:    application-month-kig
comments:
  - subject: "Vector graphics"
    date: 2004-09-23
    body: "Congratulations, Kig really seems like a nice program. I was wondering if there is any overlap or the possibility of sharing code between Kig and Karbon. Would it be possible to extend Kig to a full vector graphics app like xfig, or would that be too ambitious for the current codebase?"
    author: "AC"
  - subject: "Application of the Month: Kig"
    date: 2006-05-09
    body: "I worked with other interactive geometry and I think that kig can be better. That program is Dynamic Geometry (it is ukranian project for scools). It will be better, I think, if kig get some functions from this progect."
    author: "Nivus"
---
A new issue of the series "Application of the Month" has been released. It covers an application from the <a href="http://edu.kde.org/">KDE Edutainment Project</a> called <a href="http://edu.kde.org/kig/">Kig</a> together with a (bit outdated) interview with its maintainer Dominique Devriese. Kig is an interactive program which you can use to study geometric figures and their relationships. If you want to help us creating this monthly series or you want to translate it to your own language please <a href="mailto:kde-appmonth@kde.org">contact us</a>. Enjoy App of the Month in <a href="http://www.kde.nl/apps/kig/">Dutch</a>, <a href="http://www.kde.org.uk/apps/kig/">English</a> and <a href="http://www.kde.de/appmonth/2004/kig/index-script.php">German</a>. 



<!--break-->
