---
title: "KOffice 1.3.4 Released"
date:    2004-10-27
authors:
  - "ngoutte"
slug:    koffice-134-released
comments:
  - subject: "kspread PDF export"
    date: 2004-10-27
    body: "The PDF files created with Kspread(print file, pdf) looked ugly \nin Acrobat reader and the print-outs were  bad earlier.  Has this improved \nin the latest version ?  Comparitively the PDF-exported files \nfrom gnumeric prints exceptionally well.\n\n\nAsk"
    author: "Ask"
  - subject: "Re: kspread PDF export"
    date: 2004-10-27
    body: "I guess not. \"The KOffice team is happy to bring you the fourth _bugfix_ package\" (Emphasis added)\n\nChangelog for KSpread:\n\n* Make the Control-C key work again (#87369) \n\n* Make columns and rows that are not on the top-left side of a sheet sizable again. (Bugs #84495 and #89782) \n\n* Fix painting of the gridlines, when a row or column is resized."
    author: "Martin"
  - subject: "Re: kspread PDF export"
    date: 2004-10-27
    body: "Some users still claim that printing from KSpread is awful. :-(\n\nAs the KSpread developers are trying currently to re-write the core part of KSpread, I doubt that they will have time to fix it for KOffice 1.3.x (unfortunately).\n\n(Of course, volunteers are always welcome, but here I think that it is not so simple.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: kspread PDF export"
    date: 2004-10-28
    body: "To get good PDFs with KWord, don't use TrueType fonts.\n\nThe best PDFs are made with the fonts that come with the old Windows version of Acrobat Reader:\n\nftp://ftp.adobe.com/pub/adobe/acrobatreader/win/3.x/ar302.exe\n\nand DO NOT embed the fonts.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Yes"
    date: 2004-10-27
    body: "first post :)\n\nKOffice rocks..."
    author: "hoirkmann"
  - subject: "Re: Yes"
    date: 2004-10-27
    body: "> first post :)\n\nAlmost.\n"
    author: "Martin"
---
The <a href="http://www.koffice.org/">KOffice</a> team is happy to bring you the fourth bugfix package that builds upon the previous 1.3.x versions. The main goals of this release are <a href="http://www.koffice.org/news.php#itemSECURITYxpdfintegeroverflowinKOffice13x">to fix the integer overflows</a> in KWord's PDF import filter and to be able to compile KOffice again on KDE 3.1.5 and Qt 3.1.2. See the <a href="http://www.koffice.org/releases/1.3.4-release.php">release notes</a> and the <a href="http://www.koffice.org/announcements/changelog-1.3.4.php">list of changes</a>.



<!--break-->
