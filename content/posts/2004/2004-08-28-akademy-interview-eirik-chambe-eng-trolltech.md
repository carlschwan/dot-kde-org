---
title: "aKademy Interview: Eirik Chambe-Eng of Trolltech"
date:    2004-08-28
authors:
  - "ateam"
slug:    akademy-interview-eirik-chambe-eng-trolltech
comments:
  - subject: "Opera"
    date: 2004-08-29
    body: "He said that the Linux version of Opera was made with Qt, this makes me think that the other versions aren't. Don't all of Opera's versions use Qt 3?"
    author: "Alex"
  - subject: "Re: Opera"
    date: 2004-08-29
    body: "No, only the linux version of Opera uses QT.\n\nAlso, from version 7, very little of QT is used. The earlier versions of Opera for linux had their entire UI built with QT, but now as little as possible of QT is put to use. For example, the menus are QT, but things like window contents, dialogs, buttons etc are built using Opera's own crossplatform UI engine called Quick.\n\nIf it was easily possible, Opera would most likely stop using QT completely, but some services that it offers (like easier printing) is a daunting task to reimplement so Opera still links with QT."
    author: "Opera"
  - subject: "Re: Opera"
    date: 2004-08-29
    body: "> If it was easily possible, Opera would most likely stop using QT completely\n\nMight we know why?"
    author: "Opera User & Fan"
  - subject: "Re: Opera"
    date: 2004-08-30
    body: "Well, it would make the linux port independent of any 3rd party UI framework. \nWhich could mean: faster app, less dependencies, less resource usage, smaller download, easier building etc..  note the \"could\" though. \n\nAnyway, currently Opera benefits from various QT services and it seems that will continue AFAIK."
    author: "Opera"
  - subject: "Re: Opera"
    date: 2004-09-04
    body: "How on earth would using your own cross platform library make opera smaller and faster than if it just used QT/any other 3rd party toolkit!?!? I don't get why so many companies/projects wants to invent their own way of doing things. My favorite example of why this is a bad thing is mozilla, talk about being slow, and as an added bonus, looking and feeling alien on all platforms. I think some people forget that code reuse doesn't only include code you wrote yourself ;)\n\nI know mozilla is a platform, and not \"just\" a browser, but i would dare say that very few people actually care about that. And yes, i find firefox to be slow as well. (not the renderer, the actual user interface)"
    author: "Troels"
  - subject: "Re: Opera"
    date: 2004-09-04
    body: "Why re-invent?\nThey probably think its cheaper to write code themselves then to licence Qt for Windows/Mac and in that way have their Linux version run on Windows without additional developments.."
    author: "Thomas"
  - subject: "Re: Opera"
    date: 2004-08-30
    body: "\"No, only the linux version of Opera uses QT\"\n\nNot even the native FreeBSD port? One would certainly think otherwise..."
    author: "David"
  - subject: "Re: Opera"
    date: 2004-08-30
    body: "Imagine Opera would use Qt on all platforms and for everything, then it would not be such a (as u feel it) burden to use.\n\nLuckily u don't use gtk"
    author: "Opera Licensee"
  - subject: "It's Qt, not QT!"
    date: 2004-08-30
    body: "QT=Quick Time\n\nQt= ~\"cute\". Not \"Q.T.\" but \"cute\". Maybe you can get away with \"cutie\", but that's pushing it."
    author: "Jason"
  - subject: "Where's the non- commercial Qt version for ms Win"
    date: 2004-08-31
    body: "Non commercial project benefit from the availability of a non - commercial Qt they say.\nThe only difficulty is that the Windows side of Qt is likely *only* commercial.\n\nOnly proven Linux/Windows projects like Psi get official Qt Linux and Windows support."
    author: "Dalo"
---
Continuing our series of interviews for <a href="http://conference2004.kde.org/">aKademy</a>, we caught up with Eirik Chambe-Eng, President and Co-Founder of <a href="http://www.trolltech.com/">Trolltech</a>, to pick his brain about Qt and Trolltech.  Eirik apologized for "getting carried away" with our questions, describing himself as "passionate about these issues". So read on and enjoy...
<!--break-->
<p><b>What was your reason to develop Qt back in 1994 and to distribute this
as the only product with your company?</b></p>

<p>Co-founder of Trolltech Haavard Nord and myself worked together on
software development for ultrasound equipment at the regional hospital in
Trondheim, Norway, in 1990. We realized that the tools available for cross
platform development at the time were very bad, to use a diplomatic term. </p>

<p>We saw that developers using these tools used up to half of their time
finding out how to use the tools and in writing glue code. There seemed to
be no fun in this type of programming.</p>

<p>If this was the standard, we thought that we could do a lot better and
create the best cross platform C++ development tool in the world. So, in
1994 we created Trolltech to be able to do just that. Our mission was to
restore the passion and fun in programming. We created Trolltech so we
could turn Qt into a professional product.
</p>
<p>
With no other employees, developing Qt was a full time job for Haavard and
myself in the beginning. We secured consultancy contracts that allowed us
to use and test Qt in a commercial setting, while financing the further
development of the product and the company. Gradually the revenue source
shifted from consultancy contracts to license sales. It took 5 long years
before we could live from license sales alone.
</p>
<p>
<b>How many employees does Trolltech have at the moment and how many of
them are software developers?</b>
</p>
<p>
Trolltech today counts 92 employees worldwide, including 30 on the Qt
support/developer team and 12 Qtopia developers. We've had a decade of
uninterrupted growth, both financial and in terms of head count, and we
are planning to continue hiring both developers and non-technical
functions moving forward. 
</p>
<p>
Actually, we are always on the lookout for exceptionally good software
developers. And when we hire we always ask "Show us your code." Everything
else is secondary. Some of our very best developers have no degree, or
relevant professional experience outside Trolltech, others have PhDs. The
thing they have in common is that they are exceptionally good at writing
C++ toolkit code. I actually don't think I would have qualified for one of
our developer positions today.
</p>
<p>
<b>Why do you give Qt away for free for open source projects (e.g. the KDEdevelopers)? Do you have enough paying customers?</b>
</p>
<p>
Trolltech was one of the first companies to use the dual licensing model
(I believe we were number two, and we didn't know about the model used by
the other guys).
</p>
<p>
When we started Trolltech we were fascinated by Linux and the idea of free
software. At the same time we had neither the expertise nor the finances
to do sales and marketing. It was really a very natural and logical thing
for us to give away Qt for free for free software projects (open source as
a term didn't exist back then).
</p>
<p>
Our reasoning was that most open source developers wouldn't/couldn't
purchase software for their projects. So wouldn't it be best for all
parties if we gave away Qt to them so they could fall in love with it and
spread the word? It's the fact that software costs nothing to produce
(i.e. copy) that changes all the traditional rules of business and
production. 
</p>
<p>
The open source community plays an important role in ensuring the
stability and quality of our products. We have received, and continue to
receive, valuable input from the community that allows us to improve on
the product beyond the capacity of in-house resources. In return, the open
source community has full access to a commercial grade cross platform
development tool. It works and I think it's a beautiful model.
</p>
<p>
This is a process that also benefits our commercial customers, and there
is a growing awareness of the value of dual licensing. In addition to the
tens of thousands of open source developers out there, Trolltech has almost
4400 commercial customers contributing to the company's steady growth in
income.
</p>
<p>
If we have enough customers? We have enough to ensure a very healthy
financial situation and sustain a profitable company. But of course we
want more. All the programmers out there struggling with MFC and other API
challenged toolkits deserve better.
 </p>
<p>
<b>You made a very user-friendly graphical environment with Qtopia. How do
you judge the future of linux at mobile devices?</b>
</p>
<p>
Thank you. Qtopia is a very exciting project for Trolltech, and we are in
no doubt that Linux on mobile devices will become increasingly important,
particularly in the Asian markets.
</p>
<p>
Linux offers a level of flexibility and power on handheld devices
unrivaled by Symbian and Windows, and with Qtopia we are aiming to
provide simple, intuitive and user friendly interfaces for Linux-based
consumer electronics.   
</p>
<p>
Mobile devices are going to be the next big breakthrough for Linux.
</p>
<p>
<b>More and more people use a Mac with Aqua. How is native development
therefore supported by Qt?</b>
</p>
<p>
Well, Qt uses Aqua on Mac OS X, so Qt applications are native. 
</p>
<p>
<b>What are, beside of KDE, the most successful projects made with Qt?</b>
</p>
<p>
KDE is of course our favorite showcase given its size, quality  and
popularity.
There are, literally, many thousands of applications out there made with
Qt. I can mention 3 of them that are probably well known to most people.
Adobe used Qt to develop their Adobe Photoshop Album, a stand-alone
Windows application.
The Linux version of the Opera browser is made with Qt.
The popular Internet telephony software Skype was ported from Windows to
Linux using Qt.</p>

<p><b>Thank you for your time answering our questions.</b></p>


