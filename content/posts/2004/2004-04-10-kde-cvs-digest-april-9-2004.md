---
title: "KDE-CVS-Digest for April 9, 2004"
date:    2004-04-10
authors:
  - "dkite"
slug:    kde-cvs-digest-april-9-2004
comments:
  - subject: "Backports :-) ?"
    date: 2004-04-10
    body: "\nMany Thanks to Derek for his !\n\np.s.\nIs there a chance these konqueror optimizations will be backported into\nthe branch?\n\nIs the KDE 3.3 release in late summer still of actuality?"
    author: "ac"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-10
    body: "I second that. Please backport the anti-flicker patches!"
    author: "Anonymous"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-11
    body: "What does \"anti-flickering\" means ? What are these patches for ?"
    author: "Nicolas Blanco"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-11
    body: "Read http://robotics.dei.unipd.it/~koral/KDE/kflicker.html\n\nThe patches claim to remove a lot of the (perceived) slowness in KDE. Excessive repaints that make the interface flicker. Compare a konqueror file view to thta on Win XP. It is not slower, but it sure looks less smooth.\n\n"
    author: "Anonymous"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-11
    body: "After reading this page, the enhancements seem to be great !\n"
    author: "Nicolas Blanco"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-11
    body: "\"..to be able to catch the problem I changed the cpu frequency to 316.6 MHz and ran konqueror under valgrind..\"\n\nWow! Now that's dedication!\n\nThanks, Enrico! :)"
    author: "teatime"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-11
    body: "I compiled KDE 3.2.1 on my gentoo box with the anti-flickering patches and I'm experiencing some konqueror crashes... :(\n\nUnless I'm wrong or I've done something wrong, these patches will have to be fully tested before integration...\n"
    author: "Nicolas Blanco"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-11
    body: "I hope the qt patches will be incorporated soon enough. Otherwise, a fork would teach them ;)"
    author: "ac"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-10
    body: "I doubt that it will be backported. It is new code, and needs some testing. Optimizations don't typically get backported, only bugfixes.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-10
    body: "Not entirely true.  Optimizations can get backported when they are safe.  The problem is that we need testing to prove that they are safe. I don't feel comfortable backporting my IMAP optimizations yet, but it could happen at some point.  The real problem is that the overall gain from optimizations is not worth the risk of the possibility of introduced bugs in many cases.  It's better to work and be slow, than be fast and not work."
    author: "George Staikos"
  - subject: "Re: Backports :-) ?"
    date: 2004-04-13
    body: "100% ACK. Safety first.                                             \n"
    author: "Andy"
  - subject: "Kexi"
    date: 2004-04-10
    body: "Sounds like Kexi development has been ramping up. I can't wait for the release :)"
    author: "Paul Eggleton"
  - subject: "My commits"
    date: 2004-04-10
    body: "People might wonder why I made so much commits this week :-) I moved KHangMan and KLettres data files in the corresponding i18n modules.\nIn KDE 3.3, KHangMan will have only english data plus the user language if available in his i18n. The other languages will be downloaded via the new KNewStuff library (now in kdelibs). Same for KLettres.\nThat will slim the kdeedu module which was quite big due to KLettres sounds files.\nFor the user, getting a new language data will be very easy, 3 clicks and the new data will be installed and ready to use.\n\nI also worked a bit on KmPlot (a function plotter in kdeedu) in order to fix the code. And I also fixed things in KTurtle, currently in kdenonbeta (a Logo interpreter).\n\nQuite a lot of work is done in the kdeedu module, KStars and Kig being amazing applications while the other programs also mature."
    author: "annma"
  - subject: "Re: My commits"
    date: 2004-04-10
    body: "Hmm. The edu package is very nice. The only thing it lacks is a geography trainer :-) and a proper derive/winfunktion/mathematica replacement. \n\n(a symbolic interpreter can be build by a prolog parser for instance)\n\nKeduca is nice. I prefer to edit the XML files manually, that's faster. \n\nPerhaps a GUI for R could create a kind of SPSS clone. I don't have time right now to work on a software.  "
    author: "Andr\u00e9"
  - subject: "Re: My commits"
    date: 2004-04-10
    body: "Thanks! KGeography is being developed in kdenonbeta so one of your wishes is nearly fullfilled :-)"
    author: "annma"
  - subject: "Re: My commits"
    date: 2004-04-11
    body: "i would prefer not to implement a new parser but use existing free computer algebra systems (http://www.xs4all.nl/~apinkus/yacas.html,  http://maxima.sourceforge.net/) and try to buid a nice KDE frontend for them.\n"
    author: "pipesmoker"
  - subject: "Re: My commits"
    date: 2004-04-11
    body: "Yes, as annma says i'm developing kgeography in cvs, i would be happy to receive some comments about it (annma comments are good, but the more the merrier). If you would like to test it but don't know how to do it, don't hesitate to contact me (mail is on my name)"
    author: "Albert Astals Cid"
  - subject: "What is kdrive? What is DDX?"
    date: 2004-04-10
    body: ">Fredrik H?glund committed a change to kdebase/konsole/konsole\n>- Added support for \"real\" transparency on X servers that have a\n>  32 bit visual format with an alpha channel. \n...\n>This currently only works with the kdrive DDX on freedesktop.org.\n>In addition to the kdrive server you need Qt 3.3 and a running\n>composite manager to use this feature.\n\nWhat is kdrive? What is DDX? Where can I download them?\n\n"
    author: "anonymous"
  - subject: "Re: What is kdrive? What is DDX?"
    date: 2004-04-10
    body: "kdrive is a special driver for KDE implemented directly in the X server kernel; this may gain speed at the expense of stability but is the sort of thing that has worked well for Microsoft.  DDX is simply Microsoft's Direct Draw for X.\n\nHope this helps. ;-)"
    author: "ac"
  - subject: "Re: What is kdrive? What is DDX?"
    date: 2004-04-11
    body: "he-he :)"
    author: "ik"
  - subject: "Re: What is kdrive? What is DDX?"
    date: 2004-04-11
    body: "April fools having passed already, I don't know but, Are you serious?"
    author: "Maynard"
  - subject: "Re: What is kdrive? What is DDX?"
    date: 2004-04-10
    body: "kdrive is Keith Packard X11 server (formerly known as TinyX) which is used as a testing ground for new X extensions. Look at http://freedesktop.org for more informations about it and other intereesting stuff being developed."
    author: "Luciano"
  - subject: "What exactly is KDOM?"
    date: 2004-04-11
    body: "Will it be used in KHTML?\nCan it be used for any type of application that needs a document to reside in a DOM-tree and rendered in some way?\nWhere can I find more information?"
    author: "wilbert"
  - subject: "Re: What exactly is KDOM?"
    date: 2004-04-11
    body: "http://24.71.25.33:80/diff.php?file=kdenonbeta/kdom/DESIGN&version=1.1#kdenonbeta%2Fkdom%2FDESIGN\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: What exactly is KDOM?"
    date: 2004-04-16
    body: "Ouch, that file is completely outdated, and just contains \nvarious snippets of stuff Rob and me had in mind. Better\ndon't read it :)\n\nKDOM is a result of some years ksvg development which showed\nus the design is not good, so we started a redesign\nof khtml's dom code -> kdom.\n\nMfg Niko\nFeel free to mail me for more information...."
    author: "Nikolas Zimmermann"
---
In <a href="http://members.shaw.ca/dkite/apr92004.html">this week's KDE CVS-Digest</a>:
<A href="http://xmelegance.org/kjsembed/">KJSEmbed</A> adds support for KParts and QComboBox.  Beginnings of next
generation user guide. More IMAP and icon view optimizations. <A href="http://www.koffice.org/kexi/">Kexi</A> now supports forms.
KIMProxy, a library to enable IM from any application.
CSS ECMA bindings added in KDOM.
<!--break-->
