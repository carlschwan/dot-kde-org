---
title: "Interview KPDF Icon Contest "
date:    2004-10-11
authors:
  - "fmous"
slug:    interview-kpdf-icon-contest
comments:
  - subject: "Great new maintainer"
    date: 2004-10-11
    body: "Some great things have been happening with KPdf since Albert Astals Cid has taken over kpdf. The support for pdf table of comments is a particularly great addition. Thanks for the hard work."
    author: "Greg Gilbert"
  - subject: "what's up on branch"
    date: 2004-10-11
    body: "<p>Albert and me are working on the already mentioned kpdf_experiments branch. If someone want to take a look at what's going on here are some links:</p>\n\n<ul>\n<a <li><a href=\"http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_singlepage_contents.png\">http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_singlepage_contents.png</a></li>\n<li><a href=\"http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_continous_search.png\">http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_continous_search.png</a></li>\n<li><a href=\"http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_2p_continous_popup.png\">http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_2p_continous_popup.png</a></li>\n<li><a href=\"http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_2pages_fitPage.png\">http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_2pages_fitPage.png</a></li>\n</ul>\n\n<p>Look at screenies name for description.</p>\n\n<p>Next here is the TODO list of the branch:<br />\n <a href=\"http://tinyurl.com/49ey6\">http://tinyurl.com/49ey6</a></p>\n\n<p>Feel free to add anything you miss.</p>\n\n<p>Enrico</p>"
    author: "Enrico Ros"
  - subject: "Re: what's up on branch"
    date: 2004-10-11
    body: "Cool ! :-))\nSeems this is becoming as fullfeatured as the acrobat reader :-)\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: what's up on branch"
    date: 2004-10-11
    body: "I love it, great work!"
    author: "Mark Hannessen"
  - subject: "Re: what's up on branch"
    date: 2004-10-11
    body: "Just tried it and fell in love with it already. It has replaced acrobat reader for me, despite its experimental state. Very promising!"
    author: "Melchior FRANZ"
  - subject: "Does this pdf file works?"
    date: 2004-10-12
    body: "Can anybody please test kpdf with this file http://db1.wdc-jp.com/isij/pdf/198906/is290503.pdf\n\nOnly acroread renders it fine, kpdf/kghostview screw up the display atleast on kde 3.3. I guess it is something to do with the Type 1 font. Hopefully, if it works fine, i would immediately sync to kpdf cvs :)\n\nWaiting for your reports......... "
    author: "trucker"
  - subject: "Re: Does this pdf file works?"
    date: 2004-10-12
    body: "Yes, it works on kpdf that will come with KDE 3.3.1 (to be released shortly) and later releases.\n\nBUT if you have a pdf that does not work with kpdf, please, the next time use http://bugs.kde.org to report it.\n\nAlbert"
    author: "Albert Astals Cid"
  - subject: "Does it allow zoom using an outline?"
    date: 2004-10-11
    body: "In Acrobat the 'zoom text' (to zoom in such a way that all text is still visible) does not work if you have continues pages; for that they use the zoom on outline.\nBasically you drag an outline of the area you want to see and it zooms showing only that area.  Very usefull to have...\n\nSomething I also don't see in the screenshots is the 'hand' tool.  I read pdfs a lot on my laptop. Its actually an e-book reader with KDE on it :)  Its keyboard is detatchable and the whole screen is a touch-screen.\nWhat I really need in such an environment is to be able to just press and move the page like the hand-tool in acrobat allows you to. (kghostview allows this as well).\n\nKGhostview recently changed their keybindings for page-navigation to follow the styleguide; I'm compiling the KPDF now, so I'm not sure if you already did this; but I'd like to point out that it would be a good idea to follow this:\nhttp://developer.kde.org/documentation/standards/kde/style/keys/cursorKeys.html\n\nLast point; please consider allowing a vertical tab idea for the panel you currently have on the left (contents/thumnails/...), this panel is really bad from a usability point of view and the Konqueror implementation of that got lots of people wanting a better alternative. We now have a simpler and better one in the 'navigation panel [F9]' of konqueror.  It may not be the best for KPDF; but the outlook-bar you have now is not something I'd like to use."
    author: "Thomas Zander"
  - subject: "Re: TODO -- wrong zoom buttons order"
    date: 2004-10-11
    body: "If you want to reverse the +/- of zooming in many kde apps I'm all for it, please send an email to kde-usability with that request if you find any opposition from the relevant developers.  I'm sure to back you up on that from a usability perspective."
    author: "Thomas Zander"
  - subject: "Re: TODO -- wrong zoom buttons order"
    date: 2004-10-11
    body: "There is also a bug (whishlist) report for that same purpose at:\nhttp://bugs.kde.org/show_bug.cgi?id=74284"
    author: "jadrian"
  - subject: "Usability fixlet."
    date: 2004-10-11
    body: "You have the edit and the view menus reversed (according to your screenshots) see;\nhttp://developer.kde.org/documentation/standards/kde/style/menus/index.html"
    author: "Thomas Zander"
  - subject: "Re: Usability fixlet."
    date: 2004-10-11
    body: "The KDE framework should just handle that sort of thing automatically.  Then developers don't have to bother about trivial things like ordering and the KDE desktop will be consistent nonetheless."
    author: "ac"
  - subject: "Re: what's up on branch"
    date: 2004-10-11
    body: "Another possible feature is to support videos inside PDF files.\nI discovered recently that you can use this.\n\nLaTeX beamer allows to create such files:\nhttp://latex-beamer.sourceforge.net\n\nThat's really neat for slides... and sometimes useful."
    author: "K\u00e9vin Ottens"
  - subject: "One feture I would love."
    date: 2004-10-13
    body: "I would love a \"magic marker\" tool, give me the ability to mark important parts of an pdf like I would do on paper in a book or document. This is a tool, at least for me, witch would increase the usefulness of pdf's and on screen reading of them tremendously. One of the reasons I prefere dead tree whenever I have to read important stuff. Like the day before exams I usually just flipped through the books scanning the yellow parts:-) "
    author: "Morty"
  - subject: "More about exporting"
    date: 2004-10-14
    body: "In a previous post I suggested export to another formats keeping the format, and surprisingly is in the TODO list! (well, it appears like an imposible, but it's better than nothing).\n\nI've been thinking on it, and I don't know if it is feasible, but here it's my suggestion. If the text and the images can be exported, they could be allocated in the right position using an OCR (http://www.linux-ocr.ekitap.gen.tr/). The process could be:\n1.- Export text and images\n2.- Make a PNG from the pdf\n3.- Use a OCR in the PNG.\n4.- Allocate the exported text in the position indicated by the OCR\n5.- Allocate the images in the right position (this could be made easily?)\n\nWell, maybe still it's a dream, but it's worth a try.\n\n[Catalan mode ON]\nVaja, m'acabe d'enterar de que hi ha un catal\u00e0 al KDE! Salutacions a Albert desde Val\u00e8ncia! Ja m'agradaria a mi poder colaborar tant...\n[Catalan mode OFF]\n"
    author: "David Cuenca"
  - subject: "KPDF & search"
    date: 2004-10-26
    body: "I think that what's really missing is the ability to search \n(in i18n way) within PDF files. Maybe KPDF is the wrong tree to bark on, but this is really a huge feature which makes KPDF incomplete."
    author: "me"
  - subject: "Why is it an Ebook Reader?"
    date: 2004-10-11
    body: "Is is it more of an Ebook Reader than a PDF file viewer? There's no documentation on the web, but it's k*pdf*. Does it handle PostScript? Does it handle real ebooks, like the ones that PG creates? It's not a bad thing to be a PDF file viewer, but let's call a spoon a spoon."
    author: "David Starner"
  - subject: "but chaos is good.."
    date: 2004-10-14
    body: "because supporting the discord is important.\nA spoon should be a knife, a fork a spoon and a knife a fork.\n\u00a1Heil Eris!"
    author: "thatguiser"
  - subject: "OT: Novell, take N+1"
    date: 2004-10-11
    body: "http://www.novell.com/documentation/nld/index.html?page=/documentation/nld/userguide_kde/data/front.html\n\nGreat - on KDE desktop we have Gnome applications. Evolution, Gaim, Firefox,\nOpenOffice, Totem and god knows what else (even the mixer seems to be from it).\nSo i guess the ximian folks made their way all right :/.\n"
    author: "anonymous coward"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "What say SUSE on this?  Ximian control SUSE? :("
    author: "ac"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "There is only Novell now. The words \"Ximian\" and \"Suse\" are not longer used.\nIt's now Novell Evolution and Novell Linux Desktop.\n"
    author: "Sven Langkamp"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "> The words \"Ximian\" and \"Suse\" are not longer used.\n\nStrange that there is an upcoming \"SUSE 9.2 Professional\" product then."
    author: "Anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "Ok, the product is still called SuSe. Even though the Suse - \"a Novell business\" shows the direction. In the Novell Desktop documentation SuSe is only mentioned once in the introduction and they even replaced the chameleon by the red N."
    author: "Sven Langkamp"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "SUSE 9.2's KDE has the chameleon while the GNOME desktop has the Novell logo. Now let's talk about whose identity is disappearing, SUSE or Ximian (same for the websites)."
    author: "Anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "In the logo case I was referring to the Novell Desktop. For the website: Did you have a look at ximian.com? It's redirected to Ximian site novell.com, "
    author: "Sven Langkamp"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "SUSE says that NLD will have both KDE and GNOME desktops, both integrated with OOo and Novell products, without a default desktop."
    author: "Anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "But what about Totem, Gaim and Firefox?  Each of these has a superior KDE replacement.  Totem, come on that's a complete joke.  Kopete is more than competitive.  If Firefox is Firefox KDE then that's okay but it should be Konqueror."
    author: "ac"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "Yes. Looks like a severely crippled KDE. (Are they even using Konqueror as the file manager?)\n\n"
    author: "Martin"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "The GNOME user guide says Nautilus.\nhttp://www.novell.com/documentation/nld/index.html?page=/documentation/nld/userguide_kde/data/front.html\n\nIn the KDE guide does it only call \"File Manager\""
    author: "Sven Langkamp"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "> The GNOME user guide says Nautilus.\n\nGNOME uses Nautilus as file manager? What a shock.\n\n> In the KDE guide does it only call \"File Manager\"\n\nI bet KDE uses Konqueror. :-)"
    author: "Anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "of course it uses konqueror...the big \"evil\" that everyone is discussing is the arrangement of the menu items and the web browser in the first enterprise desktop that is not even released yet...wait it out. See what happens. Work hard."
    author: "anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "> In the KDE guide does it only call \"File Manager\"\n\nIf i had to bet - this is nautilus in KDE. At least looks hell of a lot\nlike it.\n\nSo by 'combining' these two desktops they mean that only kde application\nworth taking is k3b. Great.\n\nWell so much for supporting two desktops. They don't, really.\n"
    author: "anonymous coward"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "Huh, they clearly say it's Konqueror in the text."
    author: "ac"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "Well well. Seems like the \"my baby\" syndrome is all over the place. Too bad for Novell."
    author: "KDE User"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "First of all, most of those are NOT gnome applications and that choice of applications, in general, is hardly surprising. \nTake OpenOffice and Firefox for instance, they are desktop agnostic and probably the best choice for most users. That's why it makes sense to work in both fronts, developing native apps but also integrating this kind of applications into KDE, at least as a short term strategy. And it *is* being done! SuSE 9.2 will feature KDE (open/save) dialogs in OpenOffice for instance, and seems like a KFirefox might be on its way. \n"
    author: "John KDE Freak"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "it seems they are not taking away any functionality. Rather they are defining added valued enterprise features, which very well might change with time, considering the fast pace of development. Don't forget that the desktop they have to beat until now is BlueCurve from RedHat. The biggest question is whether they will try to simply copy that, or move on an make something better."
    author: "Iam Nuts"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "> Great - on KDE desktop we have Gnome applications.\n\nOr vice versa? :-) Calling help in GNOME applications on SUSE 9.2 and current NLD starts khelpcenter."
    author: "Anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-11
    body: "and the default burn program seems to be K3B (Good choice!)"
    author: "interestedParty"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "My bet: they exist as is until they can be replaced :(."
    author: "anonymous coward"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "AFAIK it was the Ximian team who suggested using K3B, so you are wrong."
    author: "kwwii"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "That sucks.\n\nI want an *integrated* desktop... and kdepim can give it to me *today*! Thanks, developers..!"
    author: "Jonas"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "They only try to use the best application. Regardless which desktop is used.\n\n* Evolution: Well - it's clearly more mature than Kontact. But SuSE seems also actively supporting Kontact (http://www.suse.de/de/company/suse/jobs/suse_pbu/developer_kde.html) let's hope for the future...\n* Gaim: It is not really that better, if it is, than kopete. But it is not nearly as integrated in KDE as kopete is. I do not understand this decision.\n* Firefox: In \"real world\" it has less problems than konqueror. KHTML still needs a lot of work with Javascript, CSS,...\n* OpenOffice: If one wants to read MS Office documents (as nearly everone has to) there is no other way than OpenOffice. But it has KDE's file dialog :-)\n* Totem: ?!? It's just another frontend. Kaffeine or KPlayer would do the job just as good as totem. Why the hell do they choose it for the KDE Desktop?\n\nEverything is pointing to one direction. SuSE is going for multiple Desktops (KDE , GNOME) but single applications. They pick the best of both worlds and use it regardless which desktop is running. But using kopete in GNOME makes no sense, just like using totem in KDE, as there are better \"integrated\" solutions for each desktop available.\n\nBut let's see it from another point of view. KDE as desktop seems to be the favorite for SuSE. But GNOME seems to have the better applications. Maybe KDE's development should shift it's focus from the infrastructure to the applications. And maybe it should be more open for 3rd partys (is there any important KDE application not hosted in cvs.kde.org?)\n\nAll in all it's a little bit scaring to see that many unnecessary GNOME/GTK application in their KDE Desktop. Their good KDE support is my only reason to use SuSE (so far...)."
    author: "Birdy"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "Good to know that SUSE still runs KDE as defualt. Buy a SUSE to support this.\n\nThe NLD seems to be more for enterprise clients...so we need to change what people think of KDE by improving applications to \"enterprise quality\" which will complete the functionality aspect of KDE and close the circle making KDE the clear winner."
    author: "anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "> Firefox:\n\njust recently KDE-ified, the commits having been made to Mozilla's CVS in recent days\n\n> OpenOffice\n\nKDE-ified quite nicely these days, and getting more so with each release.\n\n> Gaim: It is not really that better, if it is, than kopete. But it is not\n> nearly as integrated in KDE as kopete is. I do not understand this decision.\n\nit was probably picked because it currently supports Novel Groupware (the upcoming Kopete does as well, but it's not out in a release yet) and it integrates with Evolution (though I think they way they accomplished that is rather inferior compared to KDE's generic IMProxy stuff).\n\n> is there any important KDE application not hosted in cvs.kde.org?\n\nhrm. depends on the definition of \"important\", really. is Apollon important? is Scribus important? (and yes, Scribus is a Qt app, but Gaim is a Gtk+ app ;). K3B is part of kde-extragear, but is definitely a \"third party\" app, complete with its own devel team and independent releases. or how about the new Taskjugler frontend? that's new, obviously, but also definitely \"third-party\" and not in KDE's cvs. as it fills the \"project management\" niche which is largely unfilled on the Free/Open Source Software desktop, i expect it to become more important.\n\nand if watch the app release digests on linuxtoday.com it's easy to see who consistently has the most third-party desktop application releases."
    author: "Aaron J. Seigo"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "The truth is that Kmail and Kopete have groupwise support in the NLD. If the Evolution client does indeed support the protocol properly I do not know. I must admit that I have never tested anything other than the KDE stuff and I know that it does work and that is all I need to be happy. Once again KDE developers have out-shined themselves.\n\nI think that the decision to use one high-quality web browser on both desktops is somewhat clear, although I have heard much discussion from both sides. I think that supporting something like Kfirefox is the right idea, but that is my personal opinion and we will have to see what the enterprise market wants in comparison to what we can offer them.\n\nMost important in this market is the quality of the \"enterprise applications\" in KDE, and the over integration. I think that in the end this will be the deciding factor, but once again that is my personal opinion."
    author: "kwwii"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "> I think that the decision to use one high-quality web \n>browser on both desktops is somewhat clear\n\nFirefox even works on Windows and maybe (no idea) on Mac.  I'm thinking that that is what Aaron was referring to.\n\n> I think that supporting something like Kfirefox is the right idea\n\nNote that the user interface of firefox is contradicting many usability standards of KDE (KDEs standards are clearly harder to reach).\nAs an easy example; the keyboard shortcut 'ctrl-w' is delete last word in firefox' textfields, but close tab/window outside.  This kind of ambiguous action is never accepted in KDE. And if it were it would not by mistake close the window/application without a warning.\n\n"
    author: "Thomas Zander"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: ">Firefox even works on Windows and maybe (no idea) on Mac.\n>I'm thinking that that is what Aaron was referring to.\n\nYes, and I was trying to agree :-)\n\n>As an easy example; the keyboard shortcut 'ctrl-w' is delete\n>last word in firefox' textfields, but close tab/window...\n\nFunny, in my Firefox in OSX it is apple-w to close tab/window too...the apple=ctrl thing on mac - the the ctrl-w has no function.\n\nSo this leads us to think that the window manager should decide on all of the keyboard shortcuts, not the programs. The freedesktop spec should help clarify this."
    author: "kwwii"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-13
    body: "Ctrl-W does nothing special for form fields - it is globally \"close tab or window\" in firefox, just to clarify things. It might have been delete last word (very clearly a unix commandline relic) but it is currently not so.\n\n//Tuomas\n"
    author: "Tuomas Kuosmanen"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "Mozilla and OpenOffice are both \"desktop independent\". I'm happy to see more \"KDE-ification\" of these, and that SuSE uses the \"KDE-ification\" instead of \"GNOME-ification\".\n\nScribus is one of the rare KDE/Qt applications I'd count as a important/major KDE independent one. K3B is still bound near to KDE. Having more projects doing their own release cicle etc. just like K3B or KOffice would propalby bring some movement to the KDE application development. \nMaybe KDE should concentrate to a solid and woderful base / infrastructure for application developers. So for KDE 4 there could be a split of KDE into kdelibs+kdebase+??? and individual development of kdepim, kopete, kdevelop, ...\nWhat's about comercial applications? They seem mostly to be written with GTK. Is the Qt license the only reason for comanies to choose GTK instead of Qt?\n"
    author: "Birdy"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "> Mozilla and OpenOffice are both \"desktop independent\". I'm happy to see more \"KDE-ification\" of these, and that SuSE uses the \"KDE-ification\" instead of \"GNOME-ification\".\n\nSorry, to disappoint you: SUSE uses both. If you start their OpenOffice.org under GNOME you will get GNOME icon, style and file dialog. If you start it under KDE you will get KDE icon, style and file dialog."
    author: "Anonymous"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "This is no disappointment. I'm pleased to see the KDE support (for me). And I'm sure GNOME users are glad to see GNOME being also supported.\nI would only be disppointed if there would be only GNOME support. The same would be the case in the other way round I guess. So SuSE did a pretty good job here.\n"
    author: "Birdy"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "> What's about comercial applications? They seem \n> mostly to be written with GTK.\n\nI doubt this. I'm not aware of too many major commercial applications. Especially not of commercial applications that you could build a business plan upon.\n\n> Scribus is one of the rare KDE/Qt applications \n> I'd count as a important/major KDE independent one. \n> K3B is still bound near to KDE. Having \n\nHave a look at kde-apps.org and linuxtoday. During the last months or year the amount of applications that were developed for KDE exceeds Gtk-applications by a factor of 2-3.\n\n> Is the Qt license the only reason for comanies \n> to choose GTK instead of Qt?\n\nThe commercial support and the high quality of Qt is the reason why there are so many commercial applications.\n\n"
    author: "Torsten Rahn"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-13
    body: "vmware is one commercial application that used to be Motif but is GTK now."
    author: "Boudewijn Rempt"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "> currently supports Novel Groupware (the upcoming Kopete does as well, but it's not out in a release yet)\n\nFYI, the Kopete version on SUSE 9.2 supports Novell Groupwise."
    author: "Anonymous"
  - subject: "I see it a little differently"
    date: 2004-10-12
    body: "Suse is walking a fine line. \n\nThey need to put out a product that works now, which dictates certain choices. Other than that, they have the Ximian developers that would probably leave if none of their work was used, and the Suse Kde developers who would do the same thing.\n\nGive it some time. Novell has some case histories to learn from (Redhat who essentially got out of the commercial desktop) SUN who is having real trouble getting anyone to believe, IBM, SUSE who build a successful desktop business and UnitedLinux. Free software developers are a notoriously crotchety bunch, and it seems Suse is trying to keep most people happy because they know who holds the reins of power.\n\nIf Novell wants to distribute free software, they need people to write it for them (and others). They seem to be figuring out that they better not piss off a large portion of the developers they depend on. It would be nice if it was all Kde all the way, but that isn't realistic.\n\nI must say that this development is the most interesting I've ever seen in the history of the computer business.\n\nDerek"
    author: "Derek Kite"
  - subject: "Wonderful"
    date: 2004-10-12
    body: "Ximian produce some shitty screenshots that would make any corporate worker blow their brains out, and produce a very spartan guide that puts KDE in the worst possible light - and people think it's all Gnome. Par for the course.\n\nSuse 9.2 is the NLD as far as I'm concerned."
    author: "David"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "The information shown on the page mentioned is out of date and in the meantime incorrect/incomplete. It was not meant to be put online in this state. This will either be removed or corrected in the near future.\n\nAnd to ease the doubts it will be Kopete and Kontact in KDE not Evo and GAIM\n\n"
    author: "kwwii"
  - subject: "Re: OT: Novell, take N+1"
    date: 2004-10-12
    body: "If this is correct: Yipee!\nKaffeine instead of Totem too, and SuSE did it the correct way I'd say.\n"
    author: "Birdy"
  - subject: "Icons are great but..."
    date: 2004-10-12
    body: "Icons are great but will peaople be using apps like KPDF? It's a great app but looking at the new Suse 9.1 it seems like they're taking away more and more of KDE. There's no Kontact, but novell evolutoin etc. Will KPDF even be there in the future? (is it now??)"
    author: "KDE User"
  - subject: "Re: Icons are great but..."
    date: 2004-10-12
    body: "What the hell's Suse and Novell got to do with KDE applications?"
    author: "David"
  - subject: "Re: Icons are great but..."
    date: 2004-10-12
    body: "WTF? \"New SuSE 9.1\"?? Lol! How can you expect anyone to take you seriously?"
    author: "John KDE Freak"
  - subject: "Hmm..."
    date: 2004-10-13
    body: "I thought this would be a discussion about the icons for Kpdf. Before you post something please read 'Important stuff' at the bottom of the page:\n\n- Please try to keep posts on topic.\n- Please do not post offtopic .... comments. Repeat offenders will be sanctioned.\n\n---------------------------------\n\nThis is not the place for posts about Novell, Gnome & SuSE."
    author: "wygiwyg"
---
Some time ago KDE-Look.org <a href="http://dot.kde.org/1095261317/">launched an icon contest</a> where artists could submit an icon to be used for KPDF in the next KDE version. It seems <a href="http://www.kde-look.org/news/index.php?id=127">the contest has now been prolonged</a>. Curious about this icon contest I contacted the initiator Albert Astals Cid and some jury members to ask them some questions.
<!--break-->
<style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
</style>
  
<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16242&file1=16242-1.png&file2=&file3=&name=kpdf+icon" target="_blank"><img src="http://static.kdenews.org/fab/screenies/iconcontest/16242-1_small.png" alt="KPDF icon" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16242">'kpdf icon'</A><br />
by AndrewColes</em></p>
</div>
<!-- IMAGE BOX RIGHT-->    

<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16463&file1=16463-1.png&file2=&file3=&name=KPDF+Cliche" target="_blank"><img src="http://static.kdenews.org/fab/screenies/iconcontest/16463-KPDFClicheICON_small.png" alt="KPDF Cliche" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16463">'KPDF Cliche'</A><br />
by DrFaustus</em></p>
</div>
<!-- IMAGE BOX RIGHT-->     

<p><strong>First of all because you guys are the jury, could you introduce yourself?</strong></p>

<p><strong><em>Enrico Ros: </em></strong> I'm Enrico Ros, hacking on KDE on spare time which is a fun and creative thing to do. 
Now I'm involved in KPDF 'restyling' that means bringing it to a good level 
or at least made it meet users expectations. As of now most of the frequently 
requested features have been implemented (search, links, continuous mode, 
centered pages, better zooming, faster thumbnail generation, ..).</p>

<p><strong><em>Albert Astals Cid: </em></strong> I am a computer science student in Barcelona (Spain). I began translating KDE 
to Catalan language on 2002. On 2003 I began fixing various small bugs of the KDE applications. I took over KPDF maintainership on late August 2004. I also like helping everywhere I can.</p>

<p><strong><em>Kenneth Wimer: </em></strong> My name is Kenneth Wimer. I have worked in R&D at SUSE LINUX for more than 5 years taking care of screen graphics and usability, as well as the coordination and technical
aspects thereof. Working on KDE goes hand in hand with my daily work at
SUSE.</p>


<p><strong><em>Frank Karlitschek:</em></strong> My name ist Frank Karlitschek. Â I'm the guy behind <A href="http://www.kde-look.org/">KDE-Look.org</A>, <A href="http://www.kde-apps.org/">KDE-Apps.org</A> 
and <A href="http://www.gnome-look.org/">Gnome-Look.org</A>. I also work in the KDE artwork team. 
In the daytime I work as a director operations for a big internet agency. 
(www.dmc.de)</p>

<hr>


<p><strong>Why are you trying to collect icons through an icon contest?</strong></p>

<p><strong><em>Enrico Ros: </em></strong> It's for sure a good way to collect bright ideas from the artists community at 
<A href="http://www.kde-look.org/">KDE-Look.org</A>. We're seeing good entries and some good concept art. Sometimes looking 
at sketches can carry new ideas through the programmers and users mind too :-).</p>

<p><strong><em>Albert Astals Cid: </em></strong> The current KPDF icon is not good. So I thought a contest was the only way to get a new icon given I don't have any artistic abilities.</p>

<p><strong><em>Kenneth Wimer: </em></strong> This contest should spur interest in KDE as well as making it easier for
"newbies" to join the club :-) Many users are overwhelmed by the breadth
and complexity of the project at first but contests like this will help
to draw people to the project while offering them an easy way to start
contributing to KDE.</p>

<p><strong><em>Frank Karlitschek:</em></strong> KDE is a community project. It is important for KDE to motivate users to 
contribute to KDE. Together we can build the best desktop of the world. ;-)</p>



<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16307&file1=16307-1.png&file2=16307-2.png&file3=16307-3.png&name=kpdf" target="_blank"><img src="http://static.kdenews.org/fab/screenies/iconcontest/16307-1_small.png" alt="KPDF_icon_2" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16307">'kpdf'</A><br /> 
by Quickly</em></p>
</div>
<!--/IMAGE BOX LEFT-->
<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16346&file1=16346-1.jpg&file2=&file3=&name=Kpdf+icon+by+Jakez" target="_blank"><img src="http://static.kdenews.org/fab/screenies/iconcontest/16346-1.jpg" alt="Kpdf icon by Jakez" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16346">'Kpdf icon by Jakez'</A><br /> 
by jakez</em></p>
</div>
<!--/IMAGE BOX LEFT-->

<hr>
<p><strong>Does the contest only apply to icons? Because we've seen a couple of submissions with a splashscreen as well.</strong></p>

<p><strong><em>Enrico Ros: </em></strong> It was meant to be an icon only contest but since we have seen such a good 
response (we can count 29 submissions on KDE-Look and the contest isn't even over). We will try to use as many graphics as we can!
If we had and idea of the overall response we could have made the contest 
rules more strict and asked to provide icons for program internals, so that 
the whole new KPDF interface was themed too.  Hopefully we can tackle this next time.</p>

<p><strong><em>Albert Astals Cid: </em></strong> Currently KPDF starts so quickly I don't think a splash screen would be any 
benefit.</p>

<p><strong><em>Kenneth Wimer: </em></strong> This specific contest is really only for an icon although
the design of the icon will most likely be used on a splash screen or for
the program in some way, so it doesn't hurt submitting extra graphics as
well.</p>


<p><strong><em>Frank Karlitschek:</em></strong> The contest is about icons. But help is needed in a lot of areas. Don't 
hesitate to submit your work.</p>

<hr>

<p><strong>There are <a href="http://www.kde-look.org/news/index.php?id=123">rules</a> for submissions. One of them is that the source files are required to be in SVG or SVGZ. Why is that?</strong></p>

<p><strong><em>Enrico Ros: </em></strong> If the icon was generated from an SVG or other formats it's important to 
have the source. It may happen that we need a custom sized icon, or a scalable image to blend somewhere in the page.
As for rating, if I have to choose from 2 good icons, one only available at 
48x48px and the other as SVG, there is no doubt, I'll go for the second.</p>

<p><strong><em>Albert Astals Cid: </em></strong> SVG is a vectorial graphics format, it means the image can be scaled to any 
size without looking bad. Another rule is trying to be "<a href="http://www.kde-look.org/content/show.php?content=8341">Crystal compatible</a>" that is because Crystal is the official KDE icon set.</p>

<p><strong><em>Kenneth Wimer: </em></strong> Yes, there are definite rules. SVG (or SVGZ) must be included. Although
certain small icons still need to be prepared by hand we are striving towards a system in which all icons can be rendered 
automatically from the SVG sources.</p>

<p><strong><em>Frank Karlitschek:</em></strong> SVG is the icon format of the future. The plan is to switch all icon from png 
to svg. All new icons should be SVG.</p>

<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=2&id=16287&file1=16287-1.png&file2=16287-2.png&file3=&name=KPDF_icon_2" target="_blank"><img src="http://static.kdenews.org/fab/screenies/iconcontest/16287-2_smaller.png" alt="KPDF_icon_2" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16287">'KPDF_icon_2'</A><br /> 
by dadeisvenm</em></p>
</div>
<!--/IMAGE BOX LEFT-->

<hr>
<p><strong>Why are you concerned about the Adobe trademark issue as KDE mime for PDF uses the Adobe logo as well?</strong></p>


<p><strong><em>Albert Astals Cid: </em></strong> Quick answer: Just because Adobe did not sue Everaldo yet it does not mean 
that they never will do it.</p>

<p>Longer answer: We think It is clearly different using the logo in a mimetype 
than using it in an application. Using it in a mimetype just means "Hey, look 
that is a PDF", that is not bad for Adobe by any means. Using it in an 
application seems to mean "Hey look I am endorsed by Adobe" so it is clearly 
bad for Adobe as KPDF is in direct competition with Acrobat Reader.</p>

<p><strong><em>Enrico Ros: </em></strong> Apart from the legal issue, I'd like to highlight the fact that by forbidding to 
use the Adobe stylized logo we enhance creativity. Take a look at new entries on KDE-look.org and you'll see what I mean.
I think it's good to made it clear through the artwork that KPDF is more an 'Ebook Reader' than an 'Adobe Acrobat file Viewer'.</p>

<hr>
<p><strong>What do you think of the submissions so far?</strong></p>

<p><strong><em>Enrico Ros: </em></strong> The quality is high and it even has some peaks :-). I'm sure that when 
Albert created the contest, he didn't expect such a high response rate. For me even 2-3 icons to choose from seemed 
great, but now we have (wow) 29 entries. </p>

<p><strong><em>Albert Astals Cid: </em></strong> I did not think we would get so many (29 at the moment) of them and I am happy 
to be wrong. </p>

<p><strong><em>Kenneth Wimer: </em></strong> The submissions are great! We are happily pleased to see such a
positive reaction to the contest. This proves that contests like this can work really well.</p>

<strong><em>Frank Karlitschek:</em></strong> We have a lot of great submissions so far. But there is still a chance for new 
submissions, of course. 

<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16209&file1=16209-1.png&file2=16209-2.png&file3=16209-3.png&name=KPDF66" target="_blank"><img src="http://www.kde-look.org/content/m1/m16209-1.png" alt="KPDF66 " /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16209">'KPDF66'</A><br /> 
by salahuddin66</em></p>
</div>
<!--/IMAGE BOX LEFT-->

<hr>
<p><strong>Can you mention some particular submissions what you like or dislike about them?</strong></p>

<p><strong><em>Enrico Ros: </em></strong> Well, I won't name sets because the constest is still running, but I think 
that some of them have a very good concept idea underlying the design. Others are done smoothly and they are great looking. All 
the splash screens artworks are well done. Of course there is something I dislike, mainly because don't 
blends well on desktop with default icon style. I must confess you that I have some entries I like so much, but it will be really hard to decide.</p>

<p><strong><em>Albert Astals Cid: </em></strong> I reserve my comments for now :D</p>

<p><strong><em>Kenneth Wimer: </em></strong> The two icons that caught my eye are KPDF Pack and
Bookish. The first fits very well into the crystal theme and the second
contains a visual metaphor which explains the functionality of the
program well although there are other entries which are very good and
might appeal more to the other jury members :-)</p>

<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16146&file1=16146-1.png&file2=&file3=&name=another+kpdf+icon" target="_blank"><img src="http://www.kde-look.org/content/m1/m16146-1.png" alt="another kpdf icon" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16242">'another kpdf icon'</A><br />
by mart</em></p>
</div>
<!-- IMAGE BOX RIGHT-->    

<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:150px;">
<p><a href="http://www.kde-look.org/content/preview.php?preview=1&id=16189&file1=16189-1.png&file2=&file3=&name=KPDF+BOOKCON" target="_blank"><img src="http://static.kdenews.org/fab/screenies/iconcontest/16189-kpdfbookcon_small.png" alt="KPDF BOOKCON" /></a><br />
<em><A href="http://www.kde-look.org/content/show.php?content=16189">'KPDF BOOKCON'</A><br />
by meNGele</em></p>
</div>
<!-- IMAGE BOX RIGHT-->    

<hr>

<p><strong>So when does the contest close now?</strong></p>

<p><strong><em>Albert Astals Cid: </em></strong> 15th of October at 12:00 CEST (mid day in Central European Standard Time). Also check the <a href="http://www.kde-look.org/news/index.php?id=127">announcement at KDE-Look.org</a>.</p>

<hr>

<p><strong>In which KDE release can we expect this new icon to be used?</strong></p>

<p><strong><em>Albert Astals Cid: </em></strong> We missed KDE 3.3.1 so it will not be in it :-D
Probably it is going into KDE 3.4, as introducing a new icon in the middle of 
a x.y.Z release could confuse our users.</p>

<p><strong><em>Enrico Ros: </em></strong> I'll import the icon in the "kpdf_experiments" branch (where the new and 
powerful KPDF is growing) as soon as we have a winner, but for HEAD it will have to wait a bit longer. If the icon is 
imported into HEAD you'll see it maybe already on KDE 3.3.2.</p>

