---
title: "Waldo Bastian Wins aKademy Competition"
date:    2004-08-29
authors:
  - "ateam"
slug:    waldo-bastian-wins-akademy-competition
comments:
  - subject: "re"
    date: 2005-05-10
    body: "wow,so handsome!"
    author: "aNDY"
---
We have a winner! Our <a href="http://dot.kde.org/1093543464/">recent competition</a> turned out to be more than possible for Waldo Bastian, who picked up his prize this morning from Phil Rodrigues. Based upon his recent <a href="http://conference2004.kde.org/tut-kiosk.php">tutorial</a> and <a href="http://conference2004.kde.org/cfp-userconf/waldo.bastian-kde.kiosk.mode.php">presentation</a>, the entry covers KDE's KIOSK framework and administration tools, and will add an entirely new section on introducing administration to the User Guide.
<!--break-->
<img src="http://static.kdenews.org/fab/events/aKademy/pics/waldo_and_phil.JPG" alt="Waldo and Phil" title="Waldo and Phil" width="350" height="467" align="right">

<p>
Waldo, an employee of SuSE and veteran KDE hacker, chose <a href="http://www.oreilly.com/catalog/hackpaint/index.html">Hackers and Painters</a> by Paul Graham. In the photograph he is receiving the prize from Phil.
</p>

<p>
Lauri Watts and Phil (from the documentation team) were pleased with the entries, and look forward to future submissions. To improve the quality of the documentation, the KDE Project is keen to receive full entries or even small corrections or amendments from users, to avoid making it too difficult for non-developers to understand. If you'd like to contribute a section of the User Guide, and get your fifteen minutes of KDE fame, please e-mail: <a href="mailto:kde-doc-english@kde.org">kde-doc-english@kde.org</a> </p>

<p>We may run more competitions in the future, open to all, with books and t-shirts as prizes, to encourage more people to submit documentation, artwork and other forms of contribution. Look out for announcements on the dot!
</p>