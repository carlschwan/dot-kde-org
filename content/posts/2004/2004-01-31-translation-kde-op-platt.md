---
title: "Translation: KDE op Platt!"
date:    2004-01-31
authors:
  - "elektroschock"
slug:    translation-kde-op-platt
comments:
  - subject: "plattschnacker"
    date: 2004-01-30
    body: "What comes next?\n\nKlingon? Romulan?"
    author: "tux-42"
  - subject: "Re: plattschnacker"
    date: 2004-01-30
    body: "[quote]\nWhat comes next?\n\nKlingon? Romulan?\n[/quote]\n\nDon't tempt them."
    author: "bmfrosty"
  - subject: "Re: plattschnacker"
    date: 2004-01-30
    body: "someone already started klingon quite a while ago.."
    author: "someone else"
  - subject: "Go on with this great project! :)"
    date: 2004-01-31
    body: "Wow, that's good news!\n\nI am impressed to see the Plattd\u00fc\u00fctsch initiative\nand whish you very good progress.\n\nOne of the next steps could be the creation of a\nMuselfr\u00e4nkisch project, to be more precise:\n\n----> IMhO having KDE in L\u00ebtzebuergesch\n----> just would be absolutely GREAT.\n\nAKA Luxembourgeois, lingua Luxemburgese.\nhttp://www.luxembourg.co.uk/nutshell.html#Population\n\nThe people in Luxembourg use to promote their language by saying:\n\n    \"Mir w\u00f6lle bleiwe wat mir sin\"\n\nStrange enough the same habit is not so popular in Germany:\nhere we are suffering from braindead people who would tell\nyou that using your dialect is 'old fashioned' and 'not modern'.\n\nHow can speaking my mother's / father's language be not modern?\n\n:-D"
    author: "Karl-Heinz Zimmer"
  - subject: "Nordish by Nature"
    date: 2004-01-30
    body: "http://www.k-web.ch/funline/songs/nordish_by_nature.htm"
    author: "Stonki"
  - subject: "Re: Nordish by Nature"
    date: 2004-01-31
    body: "I was told they were from Wilhelmshaven.. "
    author: "God Level"
  - subject: "Re: Nordish by Nature"
    date: 2004-01-31
    body: "That's wrong...\n\nBoris (aka K\u00f6nig Boris) - Hamburg\nMartin (aka Doktor Renz) - Hamburg\nBj\u00f6rn (aka Schiffmeister) - Pinneberg\n\nGreetings from Hamburg :-)\n\n"
    author: "Christian Loose"
  - subject: "Re: Nordish by Nature"
    date: 2004-01-31
    body: "Oh, I was told they 'moved to' Hamburg."
    author: "God Level"
  - subject: "Klapprekner..."
    date: 2004-01-30
    body: "...LOL - that's just crazy.\nAnyway, guess it won't help these languages much too survive.\nEven the pressure on German is increasing every day.\nJust look at the many words we already use from English,\nlike Computer, Call-By-Call, Outdoor Fashion or other words which just\nrecently have started to become popular (many because of youth\nTV stations): Performance, Music Act, Feature, and so on. \nBut it's not only about being \"trendy\" (just another word).\nEnglish words are very often shorter and more international which\nhelps communication within Europe a lot. Many companies want\nto develop their TV ads or product packages for many countries\nat once. So at least Platt and eventually (admitted: after many generations)\nGerman will be doomed as well.\nBad thing is: Many aspects of this \"patchwork\" many of us like \nabout Europe will be lost.\nGood thing: Communication between each other will be easier.\nWho hasn't tried to ask for the right train on a station in Paris or\nAmsterdam? We use English for that already - otherwise it all would be\nquite complicated.\n\n\n\n"
    author: "Mike"
  - subject: "Re: Klapprekner..."
    date: 2004-01-30
    body: "What a horrible thought...I am a native English speaker, and while English-ueber-alles would help me personally, the loss of very important languages (and important related culture, poetry, and literature) is depressing.\n\nEnglish is a very common \"universal language\", granted, but it's important to note that it's almost always spoken as a SECOND language.  It's not displacing people's first languages, and I fear for the day it does.\n\nTo put things in some context, Turkish was also once a common \"second language\" when it was at the height of its empire.  Now it isn't.  People didn't change their primary languages--they just learned the second language that was most useful for their era.\n\nLong live Niederdeutsch (and Welsh, and...)!\n"
    author: "ac"
  - subject: "Re: Klapprekner..."
    date: 2004-01-30
    body: "To put things in some context, Turkish was also once a common \"second language\" when it was at the height of its empire. Now it isn't. People didn't change their primary languages--they just learned the second language that was most useful for their era.\n--------\n\nRight. Hungary was under Turkish domination for 150 years, after came the Hasburgs, german was the official language, now I live in Romania and still Hungarian is my first language. That's after 400+ years when the Turkish domination started.\nSo, I support such initiatives and the more languages are supported is better for everybody. Being the same is not always the best thing.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Klapprekner..."
    date: 2004-01-30
    body: "Good point.  At the point of a gun, people do change their first languages.  I was actually referring to people who actually voluntarily started speaking Turkish in France, Germany, etc to communicate with the Ottomans.\n\nHopefully we are past the point in history where people are compelled to change their native languages.  I'm not entirely confident of that though.\n\nAnyway, there's nothing inherently good or bad about any language.  If it succeeds in communicating ideas between two people, it's effective.  It is in the nature of languages to change and to borrow from each other.  Change is not a sign of weakness in the language, but rather the method by which languages have always survived.\n\nBest of luck to KDE's internationalization efforts."
    author: "ac"
  - subject: "Re: Klapprekner..."
    date: 2004-01-30
    body: "Asking for the right train in Amsterdam should be doable in German, although I doubt I could ask for my right train in Germany... (Not a real problem for me, since I speak German and some French too.) However, I guess you are right, languages are changing and incorporating new foreign words. This is not a new thing however. In Dutch, there are heavy French influences as well. A language is not a static object. It is alive, it develops. In the speach of the young people, you now see more and more words originating from Papiamento, Turkish and Moroc emerging, simply because immigrants start to mingle more and more into society. That is not a bad thing, and although you should not forget your own language and give in too easily to using foreign words where perfectly good native words are available, you should not be too frightened to let the language change either. Look at how France is behaving in this respect to see how not to handle it..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "Yes. This is hapenning to English as well, but in a different way. For example, people now apply any prefix or suffix they fancy to any word, and use all words as though they were verbs. This is very flexible, and a useful trait for non-native speakers, but not always very pretty to look at.\n\nEric Raymond says this is an orginally _hackish_ trait, which is now spreading into common usage. He claims that English is turning into a pure positional grammar, like Chinese.\n\nI don't know what to think about that. I personally think it is a little like Esperanto, which is the perfect second language. I feel a little sorry that everyone has to speak English to be closely involved with the FOSS world as this is a limiting factor, and is not fair on non-native speakers. I wish everyone would learn Esperanto at school since it is about as perfect as a natural language can be, is easy to express yourself in, and would be so much quicker for everyone to learn."
    author: "Dominic Chambers"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "Jes, Esperanto!\n\nEven if English is spoken by about 25% of the people all over the world (like Chinese) and Esperanto \"only\" by estimated 15-45M people (~ 0.1%), I am sure that Esperanto will get the dominant language!\n\n2 reasons:\n\n1. Effort\n\nExperiments have shown that learning Esperanto is 4times (jes, 4 = 400%) easier\nthan learning English (learning English for 1 year at school is equivalent to\nlearning Esperanto for 4 years. Or more realistic, at least here in Austria, \nlearning English for 8 year at school is equivalent to learning Esperanto for 32 (!!) years)\n\n-->\nLet's calculate now the effort, if a language should be spoken by all people all over the world:\nFor English: (1-0,25) * 4 (=Effort) = 3\nFor Esperanto: (1-0,001) * 1 = 1\n\nWe see, that English will never have a chance to be spoken by all people, because the effort is 3times higher.\n\n2. political reasons\n\nEsperanto is an absolutely neutral language, not connected to any country or\npolitical alignments.\nIn contrary English is completely associated with some certain countries and political alignments.\nHonestly, do we really belive that Iraqis, Libyans, Iranis, Afghanis and a lot of other people will like to learn English?\n\nBTW, last sommer I spent my holidays in France/Normandie and my car stopped working suddenly. I had great problems to communicate with some people, they didn't speak English or German and I do not speak French. And this happened at\na place only about 100km away from the mother-country of English.\n\nregards\n\nAndreas\n\n"
    author: "trink"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "I don\u00b4t get it. I learnt english with 0 (zero) yars of schooling, why haven\u00b4t I learnt Esperanto already? ;-)\n\nOh, I got it, there are no movies in Esperanto. Ok, there are like 3 of them. And the radio doesn\u00b4t give Esperanto songs much airtime."
    author: "Roberto Alsina"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "http://www.imdb.com/title/tt0059311/\n\nShatner.  That's right, Shatner.  So far, this is the only reason I've even seen to learn Esperanto.  I like languages with redundancy, complexity, and irregularity--makes for better poetry."
    author: "ac"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "> learning English for 1 year at school is equivalent to\n> learning Esperanto for 4 years.\n\nThen I would prefer learning English if it only takes 1 year to be at the same status like leraning esperanto for 4 years! *g*\n\nYou just mixed it up ;-)\n\nSCNR\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Changing languages"
    date: 2004-01-31
    body: "Dutch as a very extreme example of how much a language can change in a relatively short period of time. Dutch people will have difficulty reading texts written in 1900, and only a few will understand texts written in 1800.\n\nOf course the Netherlands are a small trading nation with lots and lots of internationnal contacts. It's hard to find any Dutch person younger than 50 who isn't speaking english. This makes the language very influential for the Dutch.\n\nI am even starting to make the same errors as native english speakers, like mixing up they're, there and their ;-)"
    author: "Erik Hensema"
  - subject: "Re: Klapprekner..."
    date: 2004-01-30
    body: "Throughout history there has typically been a \"lingua franca\", but the dominant language has only recently become English.  There's no reason to expect that it will remain English forever.\n\nKudos to the Platt team, I think it's good for humanity to retain diversity and cultural identity as possible, and language is a huge part of this.\n"
    author: "LMCBoy"
  - subject: "Re: Klapprekner..."
    date: 2004-01-30
    body: "I don't think so. The most important language today is probably Chinese. But i will never speak or learn chinese. Don't forget Hindi...\n\nHowever these languages are underrepresented. Unlike German for instance. \n\n7 % of the worlds web pages are in German! \n\nI think the Germans shall stop to \"autocolonialize\". Language diversity is important. And German is not that inimportant as most Germans think. Same is valid for Dutch."
    author: "elektroschock"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "Why is chinese important? Unless you intend to go there, almost every chinese person you need to speak to will speak english (or spanish, or whatever)."
    author: "Roberto Alsina"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "English is as difficult for Chinese people as Chinese for us. Chinese by the way is a very simple language. Mandarin, Beijing dialect, Kontonese ecc. Chinese languages are spoken in CHINA by billions. Of course, a chinese you will meet outside china will speak the language of his host country or english or chinese. But even in HongKong English is on the decline."
    author: "Andre"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "As I said, unless you intend to go there, in which case you may want to learn chinese or stick to tourist traps (which I heard now include about 20% of the chinese territory ;-)\n\nI doubt learning chinese is as easy for us as learning english for them, but not knowing chinese, that\u00b4s just my ignorance speaking. Well, that and that a language consisting of one-syllable words must have some sort of trick to it."
    author: "Roberto Alsina"
  - subject: "Re: Klapprekner..."
    date: 2004-02-01
    body: "Actually, most words in Mandaring Chinese are two syllables, and there is a strong tendency to three syllables. And there is an enormous number of set phrases that are in really frequent use, the constituent parts of which often make little sense in isolation. I had three main problems learning Chinese -- the tones, the script (not difficult, just a lot of work) and those set phrases. "
    author: "Boudewijn Rempt"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "We didn't meet the same chineese...."
    author: "Pierre"
  - subject: "Re: Klapprekner..."
    date: 2004-01-31
    body: "What would be the odds of that! ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Klapprekner..."
    date: 2004-02-02
    body: "Maybe every country should make an effort and teach their children to speak a few more languages. It would bind countries together, teach other people's culture and so on. But it does not always seem to been in the politics best interest to educate the masses. Just a thought."
    author: "pat"
  - subject: "Re: Klapprekner...Probably"
    date: 2004-02-02
    body: "Well, it *is* in the politics best interest, unless yo are ruled by aliens who are not part of your own society, although they may not agree with it.\n\nOn the other hand, knowing a language is not the same thing as knowing a culture at all. And I say that knowing five languages, but only one culture."
    author: "Roberto Alsina"
  - subject: "Re: Klapprekner..."
    date: 2004-02-04
    body: "True, but at least a language gives you a key to understanding its country's culture. "
    author: "pat"
  - subject: "Re: Klapprekner..."
    date: 2004-02-05
    body: "German, Dutch, and other Europian languages are *not* under threat of being extinguished. However, influence from English will pollute our languages severely. At least this is what happened historically.\n\nIn the middle ages, Latin was very popular. So, a lot of germanic languages started to import latin words. In the end, native words disappeared. Take for example the word colour. Except for German, all germanic languages use the latin word for it. (I.e. Dutch: kleur, Swedish: kolorit) Somebody know the original non-latin word for it? It is \"varuw\".\n\nIt happened with english as well. Originally, english was very much a pure germanic language. This, however, changed under influence of French. English grammar became much more illogical and a lot of words, even very basic ones, were replaced with French words.\n\nIt is very likely that this will happen with German, Dutch and other languages as well. A lot of English words will be imported, grammar will erode (i.e. words like download and upload simply do not fit well in Dutch & German grammar). The languages will survive, but wether we should be happy with it is doubtfull.\n"
    author: "Dani\u00ebl Mantione"
  - subject: "Multi-lingual"
    date: 2004-01-30
    body: "Being English I actually envy many European people as to how well they can speak English (even better than us) and can also speak their own native language and others to. With America, I think we live in a somewhat closed and ignorant world regarding our language.\n\nOf course being English I complain about the many American spellings creeping into our language, but I suppose everyone has something to complain about when it comes to their own language. I think projects like these are great, as it proves that if there is a language out there it can be kept alive and people can use it. English may come first these days, and these projects may server no other purpose than educational value, but they can be done and they are there and that's what counts. There are projects like this in the open source world for Welsh, presumably Gaelic, although I haven't checked, and other languages from all over the world. We may eventually get some sort of Universal Translator out of this - I wouldn't laugh.\n"
    author: "David"
  - subject: "Re: Multi-lingual"
    date: 2004-01-30
    body: "Languages are not static. They live, they develop. Being a native german speaker I appreciate very much the plattdeutsch initiative taken. Even if some languages disappear throughout the ages - it doesnt matter. As long as things keep moving. I love the many varieties of the regions in Europe. When I came the first time to England I was very astonished about evrybody speaking proudly in his local accent or even language (Welsh e.g.). That has been very different in Germany for years. Dialects were misunderstood as sociolects and banned - in TV, school and daily life. Now it is coming back, thanks god! \nBy the way - not everybody in Germany speaks English that good. It still is a minority. English is just a sort of living Esperanto for lots of people. They do not really get the meanings, and lots of misunderstandings happen. As a salesman I'have made the experience that things go best as one of the partners can keep his native language. So at least one of them really knows what he is really talking about. \nLets come together in english, but keep your homeregion up and be proud of it, colour the life of all of us!\nPerhaps we should change to the biggest native language on earth: chinese. ;-)\nhave a lot of fun\nMichael"
    author: "Michael Eichst\u00e4dt"
  - subject: "Re: Multi-lingual"
    date: 2004-01-31
    body: "David,\n\nPlease remember that the modern English today has been influenced by so many other cultures. You guys over on the island had French governments after 1066, but yet, you don't speak French. Hadn't you had French occupants, we would probably still speak the same language. Or it would be as close as Dutch and Platt German. \n\nBeing a professional linguist, I can add to this discussion that our modern dialects are in no way degenerations of standard languages. It's the other way round. Dialects offer a richness in semantics and partially also in syntax which is not possible to use in standard languages. \n\nWhen I studied Middle English (Chaucer, Canterbury Tales) my Bavarian dialect was a great help, since there have been many changes up to modern English. Bavarian partially still has the kind of pronounciation and morphological properties which were used at Chaucer's times and before. I'm sure Platt also has. \n\nFor this reason I can only praise every effort to support dialects, since they offer chances to understand and read languages thich have already died out. They are like live telescopes which offer us a look into the past.\n\nAs we see, language changes, which is a natural process. And if there is the ethnic will to survive as a cultural identity, every culture can survive. Don't worry, English in not like BORG language... :-) even though it influences other cultures... \n\nRegards,\nMarkus"
    author: "Markus Heller"
  - subject: "GNU/Linux Informationstage 2004"
    date: 2004-01-30
    body: "Moin, Moin :-)\n\nFollowing the link http://www.lug-whv.de\nyou can read:\n\n\"Die LUG WHV e.V. veranstaltet am 6./7. M\u00e4rz 2004 in der Fachhochschule in Wilhelmshaven die 2ten GNU/Linux Informationstage 2004, kurz LIT 2004.\"\n\nHere on top you wrote 5/6 March. [Editor: this has been corrected]\n\n??\n\nBye\n\n  Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: GNU/Linux Informationstage 2004"
    date: 2004-01-30
    body: "Sorry,\n\n\na good link to add is also:\nhttp://www.lug-whv.de/?menu=202004000601\nwith detailled German information about the event.\n\nPlease write to the editors."
    author: "elektroschock"
  - subject: "I have been trying to do something similar..."
    date: 2004-01-30
    body: "for the Dialect/language of my home town, Genoa. However, Zeneise does not have any ISO code, as far as I know. For me this is just a fun/learning project, mind you. I went so far as cloning part of the italian translation directory, and translating a handful of strings, however, I can't get KDE to use my new catalogues. That is, I managed to have a \"Zeneise\" entry in the control panel, but the catalogs do not seem to be loaded correctly. Does anybody have any hint for me?"
    author: "Luciano"
  - subject: "Re: I have been trying to do something similar..."
    date: 2004-01-30
    body: "Oh, so that\u00b4s what Zeneise was. I always wondered why these guys called themselves Xeneizes: http://www.bocajuniors.com.ar/\n\nThe Genoa connection makes it all make sense, since that neighborhood was full of Genoan immigrants :-)"
    author: "Roberto Alsina"
  - subject: "Re: I have been trying to do something similar..."
    date: 2004-01-31
    body: "Oh! That indeed seems to be the case. In fact, there is even a famous (well, in Genoa) song in dialect about an emigrant (who quite possibly went to Argentina, since his son was speaking spanish...) longing for home! "
    author: "Luciano"
  - subject: "Re: I have been trying to do something similar..."
    date: 2004-01-31
    body: "1) you need good (i.e. up-to-date) po-templates from kde-i18n/templates. These fit to a current KDE. Best for a start would be release candidate 1 with its template directory.\n\n2) create /opt/kde3/kde-i18n/xxx with xxx being your iso code, or (if you do not have one) invent one and use that for the time being\n\n3) copy the files from kde-i18n/nds or similar: they contain makefile templates (makefile.am) and an important file \"entry.desktop\". Edit this file: insert your language name. This file will be installed in the KDE locales directory and makes your language selectable in the KDE language selector in the control center\n\n4) add xxx (your language code) to kde-i18n/subdirs. Limit subdirs to the languages that you really need, otherwise you can get a good cup of coffee for the following:\n\n5) do \"make Makefile.cvs\" in kde-i18n. This (thanks to the file subdirs) will generate a configure in xxx.\n\n6) do \"./configure --prefix=/opt/kde3\" (or whereever your kde base directory is)\nThis generates the makefiles in the subdirs of kde-i18n/xxx.\n\n7) cd to xxx\n\n8) make and \"make install\" will install your po-files.\n\n9) this works fine for all po-files except the desktop-files. Their content needs to be merged into the applink-files and need special treatment by scripty. The results of this will only be seen by \"cvs update\" on the respective kde subdirectory (e.g. kdebase or kdeedu) the day after. There might be tricks to do that by hand, but at the moment I do not have experience with that.\n\n10) for quick tests (like I did for kdelibs.po) use kde-i18n/en_GB (british english) and just replace po-files there with your po-files in genuese. make and make install. Choose british English in the control center. Your strings will appear (except of course for the applinks of the desktop-files). kdelibs.po shows up pretty much everywhere, konqueror is a good start too.\n\nTell me if that works, or if you encounter further problems with this.\n\nHeiko"
    author: "Heiko Evermann"
  - subject: "I think it's fantastic"
    date: 2004-01-31
    body: "Having a Mennonite background myself, and having lived in the heart of Paraguay for one year, I can say that Plattdeutsch is still very alive (you just have to look in the right places).  It is, however, in decline.  My parents still speak it fluently and often.  And I'm willing to bet that a Plattdeutsch version of KDE will be used by at least a few people."
    author: "David"
  - subject: "latin kde?"
    date: 2004-01-31
    body: "hi!\n\nis there anything like a \"latin-kde\"-team?\nif yes, could anyone point me to an email-address or any url?\n\nthx!\n\n - deucalion\n\nPS: Nette Idee mit dem Platt :)\nWie w\u00e4rs mit einem \u00dcbersetzungsteam f\u00fcr Hochdeutsch (zB Mittelostbayrisch, wird in Bayern, \u00d6sterreich etc. gesprochen)?"
    author: "deucalion"
  - subject: "Re: latin kde?"
    date: 2004-01-31
    body: "Wie bitte?\nMittelostbayrisch in \u00d6sterreich? Da spricht man \u00f6sterreichisch!\n1945 ist l\u00e4ngst vorbei!\n\nKleine Kostprobe eines Arbeitskollegen aus DE: Flickenteppich\nRichtig heisst's: Fleckerlteppich\n\n;-)\n\nAndreas"
    author: "trink"
  - subject: "Re: latin kde?"
    date: 2004-01-31
    body: "Dazu ein zitat:\n\n\nSymptomatisch ist, dass die Sicherheitsl\u00fccke durch das Einspielen so genannter \"Patches\" - was nichts anderes als \"Flicken\" hei\u00dft - geschlossen werden soll. Nach einiger Zeit hat man dann wom\u00f6glich einen ganzen \u0084Flickenteppich\", \u00fcber dessen Stabilit\u00e4t sich jeder seine eigenen Gedanken machen sollte. \n\nBundesministerin der Justiz Zypries auf der Systems in M\u00fcnchen\n\nNow think about the fact that the department of Justice will have its say about patents on software..."
    author: "Andre"
  - subject: "Re: latin kde?"
    date: 2004-03-09
    body: "from wikipedia.de (look for bairisch):\n\nmittelbayrisch (in Niederbayern, Oberbayern, Ober\u00f6sterreich, Nieder\u00f6sterreich und Burgenland)\ns\u00fcdbairisch (in Tirol, K\u00e4rnten und Steiermark)\n\nThis has linuistic reasons and nothing to do with 1945."
    author: "Michael Kiermaier"
  - subject: "Re: latin kde?"
    date: 2004-01-31
    body: "You could have looked in the I18N team overview: http://i18n.kde.org/teams\nand you would have found the Latin I18N team:\nhttp://i18n.kde.org/teams/index.php?a=i&t=la\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: latin kde?"
    date: 2004-01-31
    body: "Have a look at http://i18n.kde.org/teams/ and look for Latin. There is a small group of enthusiasts, but there has been little to none progress for quite some time.\n\nHeiko"
    author: "Heiko Evermann"
  - subject: "Q: Do you need to speak Platt to join the effort?"
    date: 2004-02-01
    body: "Or can you improve your very limited Platt as you go along, typing on your Klapprekner? :-)\n\nSounds like a fun project. Wouldn't mind helping out, when I have time, but I don't really speak Platt (yet?) except for the few phrases every Fischkopf does.\n\ncheers,\nRoland"
    author: "Roland"
  - subject: "Re: Q: Do you need to speak Platt to join the effo"
    date: 2004-02-01
    body: "I think contributions are welcome. No, this is no \"fun project\", whoever says so spits on our cultural heritage and independence. Ho!\n\nYou can review phrases, you can contribute to our dictionary (which is more or less out of date), you can contribute to Heikos PO files. Kannst uns dein Senf zu geben. Du kannst bei Friesathlon helfen zu zeigen wie man mit KBabel umgeht. kein problem.\n\nFischk\u00f6pf is a pejorative phrase used by Southern German ignorants but very uncommon over here. \n"
    author: "Elektroschock"
  - subject: "greets in luxemburgish to the project initiators"
    date: 2004-02-02
    body: "Moin,\n\nech sin ganz frou mol sou eng Initiativ matzekreien. Meng Sprooch as selwer eng Oofwandelung vum Platt an ech wollt iech jhust soen dat ech deck stolz op iech sin dat der sou eppes op d'Been stellt."
    author: "patrick"
  - subject: "Congratulations"
    date: 2004-02-02
    body: "From the team at http://translate.org.za we wish you luck.  It's lots of hard work but very rewarding.  I love the translatethon idea, we hope to be doing something similar in South Africa this year - I hope you'll give us all your info on the logistics of making it happen.\n\nIf you want to translate other things like Mozilla and OpenOffice using PO format then have a look at http://translate.sf.net for our tools."
    author: "Dwayne"
---
Thomas Diehl has created <a href="http://i18n.kde.org/teams/index.php?a=i&t=nds">a new language project branch</a> for <a href="http://www.evermann.de/kde_op_platt/">a Plattdüütsch (Low Saxon) translation of KDE</a> (ISO code: nds). The language is spoken in Northern Germany, the Netherlands and even in parts of the Mennonite Community in the United States and Paraguay. There are several millions of Plattdüütsch speakers, but Low Saxon -- stuck in the middle of German, English (Anglo-Saxons!) and Dutch -- is on the decline in favour of official languages such as German or Dutch.  

<!--break-->
<p>
Started as a comprehensive Linux translation project by Jürgen Lueters quite a while ago, it was decided that KDE would be translated first. This is due to the popularity of KDE, although Gnome will follow later. The platt-internalization project has Sven Hertzberg of Gnome-Germany on board. Gnome-de also <a href="http://platt.gnome-de.org/">hosts the current mailing list</a> of the project. 
<p>
Up to now 94% of strings in the basic files are translated, but the project will have to start sustainable translation with more contributors. Contributions, reviews and user testing is very much appreciated to make the dream of KDE op platt real. Project coordinator <a href="http://www.evermann.de">Heiko Evermann</a> counts on you. 
<p>
At the <a href="http://www.lug-whv.de/?menu=202004000601">Gnu/Linux Informationstage event (6/7  March)</a> in <a href="http://www.wilhelmshaven.de">Wilhelmshaven</a>, Germany a project member, Thomas Templin of
<a href="http://www.lug-whv.de/">LUG WHV</a>
will organize the first Friesathlon. Friesathlon refers to Hackathlons and the Frisian people. A Friesathlon, that means to him a Low Saxon translation contest and introduction to interested users from the Plattdüütsch Community. His objective is to show them how to use KDE translation tools and showcast KDE-op-platt. His secret conspiracy plan is to get more people involved.
<a href="http://www.gnuwhv.de">Thomas Templin</a> still needs some volunteer staff workers for the event. Probably it will be possible to create a webstream-connection to a parallel Chemnitz Linuxshow in Saxony, so Saxons will also get a grip of Low Saxon, a language slightly more popular than German in Saxonian dialect. :-) 
<p>
There is no unified Plattdüütsch dialect, so the project will have to develop its own dialect. The project chose to be close to the dialect used in public media. Despite of public discrimination (sometimes regarded as a rural German dialect) there is little Low Saxon content in public radio broadcasting. However it will depend on the translators to create a common ground. Professional linguistic advice is also welcome. The project is assisted by the well-known linguistic Institut für Niederdeutsche Sprache, Bremen.
<p>
The translation project also creating a database of computer words. There is no Plattdüütsch computer vocabulary yet despite words like
Rekner - Computer,
Klapprekner - Laptop, Notebook.
The project hopes to add some more unique expressions, enrich and modernize the language. Some guys of the project even want to learn Low Saxon by translation efforts. A great experience. Translation work is a good approach to dig deeper into the phenomenon of language diversity and semantic richness.
<p>
Plattdüütsch (Low Saxon) is a regional language recognized by the European Regional and Minority Language Charter. It is applied in some federal states in Germany, so that a government agency is obliged to reply to citizens in Low Saxon. As the application of the European charter is still weak in Germany a KDE translated to Plattdüütsch may increase the adoption of Linux/KDE in administration. The <a href="http://www.bmi.bund.de">BMI (German federal ministry of interior)</a>, very much in favor of Linux, has to write year reports about the applications of the Charter, so maybe we could get funding from this government agency.
<p>
Although speakers of Low Saxon are able to communicate in at least one other language such as German, Dutch, Dansk, English it is a competitive advantage to have a desktop environment available in your native language. Many young people don't speak the language anymore, but it easy to learn, and easy to understand, so it makes you feel at home. We don't underestimate the geek-factor of KDE-op-platt (KDE in Low Saxon). Especially for the Frisian people it can assist them to keep their identity despite carrying a Dutch or German passport. Unlike other people groups in Europe Frisians are non-militant despite continuous public language discrimination. 
<p>
Conservative approaches of the past are commonly rejected by the project, because they only enforced the colonialization of our culture. We don't want "back to the roots", we want a full-featured and modern desktop. And living in coastal areas for centuries there is even much more experience with shells :-). According to the old Frisian slogan "Lever dod as slaav"(Better dead than slave) we need a free operating environment that enables us to exercise and hack our culture. This is the benefit of an Open Source licensed desktop environment in comparison with closed source. 