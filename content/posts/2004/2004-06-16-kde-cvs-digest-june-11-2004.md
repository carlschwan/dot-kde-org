---
title: "KDE-CVS-Digest for June 11, 2004"
date:    2004-06-16
authors:
  - "dkite"
slug:    kde-cvs-digest-june-11-2004
comments:
  - subject: "Digikam"
    date: 2004-06-16
    body: "Digikam: redeye correction plugin\n\nVery nice! :-)\nBut please don't make in Digikam everything a plugin. Its not that good for the UI.\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Re: Digikam"
    date: 2004-06-16
    body: "If it's done properly then the users won't even notice that everything is a plugin. So making everything a plugin isn't necessarily bad for the UI. I haven't tried Digikam yet, so I can't judge how seamless/transparent for the user plugins are integrated in Digikam."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Digikam"
    date: 2004-06-16
    body: "Digikam is the best image organicer for Linux, but there are a lot of things I'm missing. I think Ettore made a very complead list for f-spot of things a good image organicer need. \nhttp://cvs.gnome.org/viewcvs/f-spot/TODO?rev=1.6&view=auto\n\nP.S. Ettore died, and there is no active f-spot devoelloper at the moment."
    author: "MaX"
---
In <a href="http://cvs-digest.org/index.php?issue=jun112004">this week's KDE CVS-Digest</a>: More Enriconian optimizations to <a href="http://www.konqueror.org/">Konqueror</a>.
<a href="http://www.trolltech.com/products/qt/index.html">Qt</a> only <a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> made easier.
<a href="http://kolourpaint.sourceforge.net/">Kolourpaint</a> adds zoom.
Kitchensync adds ability to sync calendar resources.
<a href="http://korganizer.kde.org/">KOrganizer</a> adds a journal editor.
<a href="http://digikam.sourceforge.net/">Digikam</a> adds image editor plugins.
<a href="http://www.koffice.org/">KOffice</a> continues work on <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">OASIS</a> file format save and load.
<a href="http://amarok.kde.org/">amaroK</a> adds streaming support using <a href="http://gstreamer.freedesktop.org/">GStreamer</a>.
<a href="http://www.kdevelop.org/">KDevelop</a> adds win32 Qt templates.
<!--break-->
