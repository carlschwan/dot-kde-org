---
title: "KDE-CVS-Digest for May 14, 2004"
date:    2004-05-15
authors:
  - "dkite"
slug:    kde-cvs-digest-may-14-2004
comments:
  - subject: "kdenonbeta getting a lot of love recently"
    date: 2004-05-15
    body: "Looks like a lot of changes to projects on kdenonbeta these days.. will probably make for exciting kde 3.3 and 4.0 releases\n\nto new kdepim/kdenetwork projects like akregator (RSS/RDF/Atom feed reader), gadget (web services app, sorta like Sherlock)\nbetween architectural improvements, such as kspell2, kimproxy, kdom, ksvg2\nto new kdeedu apps such as kgeography, klatin\nto new kdelife? apps like krecipies\nto new development apps like unsermake and icecream (distcc fork)"
    author: "anon"
  - subject: "Re: kdenonbeta getting a lot of love recently"
    date: 2004-05-15
    body: "you know what? its all *extra* for me... 3.2.x was for me \"final\". it had what I needed from a desktop environment. Of course I'm excited, and very happy to see the development goin' on. but I'm not gonna run CVS or alpha's (maybe beta) - just waitin' and enjoying my current experience with 3.2.x! I'm sooo happy with that release... thanks guys, and - of course, pleas continue, but: you've surpassed *for me* the line of \"this is what I need to work nice\" and I guess thats the case for alot ppl. thats great. the more use KDE, the more will be interested, start developing, start helping, or maybe donating."
    author: "superstoned"
  - subject: "Something is bad, when..."
    date: 2004-05-15
    body: "Fixed Bug 43172 (thanks to Beineri for telling me about it, it had 436 votes!)\n(\"konqueror is ignoring umask when creating new files\")\n\nSomething is bad when developers are ignoring voting system on b.k.o\nMore understandably would be reaction \"those stupid lamers don't know what is a real bug, don't have time to mess with it\" than ignoring it.\n\nThere are not so many bugs with >20 votes on them (comparing to all of open reports of course).\n\nm."
    author: "m."
  - subject: "Re: Something is bad, when..."
    date: 2004-05-15
    body: "Pardon? It actually got this attention because it had so many votes.\n\n> There are not so many bugs with >20 votes on them (comparing to all of open reports of course).\n\nOnly 400 ;-)... And a part of them are actually hidden feature requests (like top-bugger #41514).\n\n\"I don't request a feature, just make my homepage with CSS3 render correctly!\"\n"
    author: "Anonymous"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "Why is #41514 a feature request? It is a very real problem that Kmail blocks the UI and I don't even see something remotely sounding like a feature request. And it has nothing to do with CSS3."
    author: "Anonymous"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "Because it doesn't require just a small bugfix but a new internal design/implementation?"
    author: "Anonymous"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "Ever heard of threads?!"
    author: "AC"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "And you call changing everything to threads a bugfix?"
    author: "Anonymous"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "I know. But you make it sound as if the reporter wanted to get a feature by disguising it as a bug - this is true."
    author: "Anonymous"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "Dunno. There are users who do this or intentionally file a bug report with \"crash\" severity because they think it makes it more important."
    author: "Anonymous"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-16
    body: "Or alternatively, \"qApp->processEvents();\" will process GUI events in the middle of a function."
    author: "David Saxton"
  - subject: "Re: Something is bad, when..."
    date: 2004-05-15
    body: "> Something is bad when developers are ignoring voting system on b.k.o\n\nThey *aren't* ignoring it.. for every bug on the \"The most hated bugs\" that exists today, there are 5 more bugs that used to have 300+ votes that have been fixed. "
    author: "fault"
  - subject: "chm viewing with KDE "
    date: 2004-05-15
    body: "\"Added CHM (aka html help) documentation plugin. Supports only catalogs, does not show topics, indices and does not support full text search. Any help is welcome.\"\n\nThis is so cool ... \n\nSometimes I have some HTML help files I need to view and I use xchm[1] for this. Now I'm looking forward to use that documentation plugin. Are these plugins also usable outside of KDevelop? (sorry if that's a stupid question).\n\nFab\n\n[1]http://xchm.sourceforge.net/\n"
    author: "Fabrice"
  - subject: "Re: chm viewing with KDE "
    date: 2004-05-15
    body: ">Are these plugins also usable outside of KDevelop? \n\nActually, yes. At least come KDE-3.3, KDevAssistant will be available. It's a standalone app (much like the Qt assistant) that lets you find (via a navigation tree, index or full text search - capabilites varies between documentation plugins) and view any documentation for which there is a plugin."
    author: "teatime"
  - subject: "Something's wrong ;-)"
    date: 2004-05-15
    body: "Hi Derek,\n\n Seems that something is wrong with the new script. You can imagine that many commits actually affected more files, not a simple .kdevelop or ChangeLog file. From my commits:\n\"... to kdewebdev/quanta/ChangeLog\"\n\"... to kdewebdev/quanta/quanta.kdevelop\"\n\"... to quanta/ChangeLog\"\n\nBut\n\"...to kdelibs/i\" is also strange. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Something's wrong ;-)"
    date: 2004-05-15
    body: "Indeed something is wrong. It is due to my vain attempt to view cvs data as atomic. The indexing scheme uses the timestamp, which is the same for all the different files committed in an atomic (multi file) commit, or is most of the time. On one of your commits there is a second difference between the two files, which borked the index. Odd that only your commits showed up that way. You must be special :)\n\nSo obviously I have to broaden the test for same commit, checking timestamp and user and part of the comment. I needed a good reason to delve into that code anyways.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Something's wrong ;-)"
    date: 2004-05-16
    body: "Could it be that this is also the reason why the extragear modules (not sure about the others) are listed only sporadically in the commit summary? I am more than sure that there were more than no commits in extragear-3 last week for example ;)"
    author: "Fred Sch\u00e4ttgen"
  - subject: "Great work, but a few wishes."
    date: 2004-05-16
    body: "I love KDE as a desktop environment, and I think the developers are doing great work - but there are a couple of things I wish they could do in the future.\n\nFirst, I wish all important applications were somehow \"branched\" at each release.  It would be cool to be able to update my Kmail that came with KDE 3.2.2 with a new version of Kmail with more features.  The same goes for other applications.  \n\nWhat KDE unfortunately seems to lack is single KDE applications getting updated and released all the time, without having to track CVS.  If we look at Gnome, we see that single applications get released/updated all the time with feature requests, bug fixes, and so forth.  I think KDE should have some of that too.  It's great coolness to be able to track only your favorite applications without having to update everything to head..\n\nIf this already is possible, then it's not entirely clear to me how .."
    author: "arcade"
  - subject: "Re: Great work, but a few wishes."
    date: 2004-05-16
    body: "Apps in KDE Extragear(1/2/3) are released on their own schedule."
    author: "teatime"
  - subject: "Re: Great work, but a few wishes."
    date: 2004-05-16
    body: "In the middle/long term, it would be nice if the kde core would be discoupled from the kde applications.\n\nThis way, the \"kde core\" (like kdelibs, konqueror, kicker, kdesktop, kwin and some others) would have longer release cycles, and the applications would be released independently.\n\nThe \"kde core\", or should it be called the \"kde platform\", has matured very\nmuch in the last year.\nIf the applications would be released seperately from the core, they could have their own release schedules and evolve faster maybe, instead of being bound to the kde core release schedule.\n"
    author: "ac"
  - subject: "Re: Great work, but a few wishes."
    date: 2004-05-16
    body: "You are aware of all implications? Like more complicated development due to different schedules/freezes, more difficult installation, an army of more or less capable release managers, either splitting of translations in hundred packages or bloating every program archive with every available translation, ..."
    author: "Anonymous"
  - subject: "Re: Great work, but a few wishes."
    date: 2004-05-16
    body: "OK, ac, what would that bring you? As far as I see it, KDE is evolving very fast. Now, all these people (well most of them), who develop KDE, do it in their free time. I do not think they would have more free time to develop their apps faster than they do it now and I do not think any other release cycle would help them code faster. However, maybe I am just trying to apply my own limitation on these people, but this is my view.\n\nCheers\n\nZoltan"
    author: "Zoltan Bartko"
  - subject: "Re: Great work, but a few wishes."
    date: 2004-05-16
    body: "Well this sort of already happened. The kdepim module has been on its own release cycle for kdepim 3.3 for a while now, though recently they decided that their planned release was close enough to the kde 3.3 planned release that they would just sync up again. "
    author: "Greg Gilbert"
  - subject: "k3b"
    date: 2004-05-16
    body: "Does anyone know if k3b will be integrated into KDE 3.3, instead of remaining in extragear?"
    author: "Kaarthik Sivakumar"
  - subject: "Re: k3b"
    date: 2004-05-16
    body: "There are no public plans for this at the moment."
    author: "Anonymous"
  - subject: "Re: k3b"
    date: 2004-05-16
    body: "its not possible due to the speed of k3b development. it would be pretty hard to keep it in sync\n"
    author: "getit"
  - subject: "anoncvs.kde.org down?"
    date: 2004-05-16
    body: "Is anonymous@anoncvs.kde.org down?\n\nSince a few days I (and some others) can't checkout from that server anymore, first I got the error message:\n\ncvs [login aborted]: end of file from server (consult above messages if any)\n\nand since 2 days the login comletely hangs without any errormessage. I switched to another mirror, but since this is the premium server mentioned in the KDE documentation for anonymous CVS:\n\nDoes anyone know what happend?"
    author: "furanku"
  - subject: "Re: anoncvs.kde.org down?"
    date: 2004-05-16
    body: "> Is anonymous@anoncvs.kde.org down?\n\nYes."
    author: "Anonymous"
  - subject: "Re: anoncvs.kde.org down?"
    date: 2004-05-18
    body: "anoncvs.kde.org is back under another host."
    author: "Anonymous"
  - subject: "Re: anoncvs.kde.org down?"
    date: 2004-05-19
    body: "Wich?\n\nSince this a public cvs Server for anonymous users, it would be a good idea if the host name is not a secret."
    author: "furanku"
  - subject: "Re: anoncvs.kde.org down?"
    date: 2004-05-19
    body: "The DNS host name didn't change."
    author: "Anonymous"
---
In this <a href="http://cvs-digest.org/?issue=may142004">week's KDE CVS-Digest</a>: More work on <a href="http://www.kdevelop.org/">KDevelop</a> documentation tools, adding a TOC plugin. 
Khtml text-decoration mostly brought up to CSS1 standards.
<A href="http://kde-bluetooth.sourceforge.net/">KBlueTooth</A> adds utilities to search for services and send faxes.
<a href="http://kopete.kde.org/">Kopete</a> adds rich text editor capabilities.


<!--break-->
