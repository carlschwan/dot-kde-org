---
title: " KDE Ships January Updates to Plasma Workspaces, Applications and Platform"
date:    2013-01-02
authors:
  - "sebas"
slug:    kde-ships-january-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "Thank you; Dolphin Rename"
    date: 2013-01-03
    body: "KDE continues to grow, and it truly gets better with each release. Again, thanks you to all the team members for all the hard work for those of us that can't. It is much appreciated.\r\n\r\nDoes anyone else have a problem with Dolphin Inline Rename turning off as you click on Klipper to get some of the file name pieces? The old pop-up dialog for rename stayed open and one could easily go to Klipper to get pieces to input.\r\n\r\n"
    author: "\u2020MaNNa"
---
Today KDE <a href="http://kde.org/announcements/announce-4.9.5.php">released</a> updates for its Workspaces, Applications, and Development Platform. These updates are the last in a series of monthly stabilization updates to the 4.9 series. 4.9.5 updates bring many bugfixes and translation updates on top of the latest edition in the 4.9 series and are recommended updates for everyone running 4.9.4 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone.

The <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&bugidtype=include&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.9.5&order=Bug%20Number&list_id=367576">list of 36 recorded bugfixes</a> include improvements in the Dolphin file manager. KDE's development platform has received a number of updates which affect multiple applications. The changes are listed on KDE's issue tracker. For a detailed list of changes that went into 4.9.5, you can browse the Subversion and Git logs. 4.9.5 also ships a more complete set of translations for many of the 55+ supported languages. To download source code or packages to install go to the 4.9.5 Info Page. If you would like to find out more about the KDE Workspaces and Applications 4.9, please refer to the 4.9 release notes and its earlier versions.