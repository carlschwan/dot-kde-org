---
title: "KDE Commit-Digest for 24th March 2013"
date:    2013-04-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-24th-march-2013
---
In <a href="http://commit-digest.org/issues/2013-03-24/">this week's KDE Commit-Digest</a>:

<ul>
<li><a href="http://pim.kde.org/">KDE-PIM</a> implements scam protection</li>
<li><a href="http://www.sharpley.org.uk/node/14">LightDM</a> adds an ability to install themes</li>
<li>Work on <a href="http://nepomuk.kde.org/">Nepomuk</a>-Controller QML applet and DataEngine</li>
<li><a href="http://skrooge.org/">Skrooge</a> gets new "capitalize" function in the "Search & Process" panel</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> on Android—known as <a href="http://blogs.kde.org/2013/03/24/coffice-calligra-android-available-now">Coffice</a>—sees active development with multiple new features</li>
<li>Print Manager includes more information in the print job view</li>
<li><a href="http://docs.kde.org/development/en/extragear-base/kcontrol/wacomtablet/introduction.html">Wacom tablet support</a> allows the user to invert the scroll direction of the touch device again.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-03-24/">Read the rest of the Digest here</a>.