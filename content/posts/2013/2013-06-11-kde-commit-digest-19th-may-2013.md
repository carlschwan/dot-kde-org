---
title: "KDE Commit-Digest for 19th May 2013"
date:    2013-06-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-19th-may-2013
---
In <a href="http://commit-digest.org/issues/2013-05-19/">this week's KDE Commit-Digest</a>:

<ul><li>In KDE Workspace, Battery Monitor improves the way multiple batteries are displayed; KWin introduces cross-fading with previous pixmap and adds Wayland seat and backend support; a new"Configure" button for notifications that opens KNotify settings for the app that emitted the notification</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> gets a new Data menu and sees multiple menu reorganizations</li>
<li><a href="http://solid.kde.org/">Solid</a> add support for the Power Supply property</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> introduces NotificationMessageV2 that supports batch operations on set of entities</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds resource categorizing with tags</li>
<li><a href="http://amarok.kde.org/">Amarok</a> gains an audio analyzer visualization applet, fixes playing of tracks passed on command line; pressing enter when searching collections now adds found tracks to the playlist and clears the search bar</li>
<li>NetworkManagement adds initial Wimax support</li>
<li><a href="http://kopete.kde.org/">Kopete</a> gains a new history plugin based on sqlite</li>
<li>Print manager can filter the jobs again.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-05-19/">Read the rest of the Digest here</a>.