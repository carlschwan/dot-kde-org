---
title: "KDE Commit-Digest for 27th January 2013"
date:    2013-02-17
authors:
  - "mrybczyn"
slug:    kde-commit-digest-27th-january-2013
---
In <a href="http://commit-digest.org/issues/2013-01-27/">this week's KDE Commit-Digest</a>:

<ul><li>Basic code completion support and other improvements in QML/JavaScript language plugin for <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Optimizations in <a href="http://nepomuk.kde.org/">Nepomuk</a>, <a href="http://pim.kde.org/">KDE-PIM</a>, <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Working with multiple monitors gets better for Wacom tablet</li>
<li>New and additional names for <a href="http://www.ngcicproject.org/">NGC/IC</a> objects in <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li>Work on matrix operations in analitza (KDE Edu library for adding mathematical features)</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> implements Undo command for paragraph settings.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-01-27/">Read the rest of the Digest here</a>.