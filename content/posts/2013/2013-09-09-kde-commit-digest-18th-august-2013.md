---
title: "KDE Commit-Digest for 18th August 2013"
date:    2013-09-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-18th-august-2013
---
In <a href="http://commit-digest.org/issues/2013-08-18/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://kate-editor.org/">Kate</a> gets initial Vim macro support</li>
<li><a href="http://www.digikam.org/">Digikam</a> sees more work on improved multicore support</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> gains a new command line tool: kdepim/console/calendarjanitor that can scan all the calendar data for buggy incidences; adds a debug dialog for search</li>
<li>In KDE Frameworks, hotplug dataengine is ported to <a href="http://community.kde.org/Plasma">Plasma 2</a>, and more</li>
<li><a href="http://userbase.kde.org/KDE_Wallet_Manager">KWallet</a> sees work on GPG backend, ready for real-life testing.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-08-18/">Read the rest of the Digest here</a>.