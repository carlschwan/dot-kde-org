---
title: "Report from the freedesktop summit"
date:    2013-04-17
authors:
  - "dfaure"
slug:    report-freedesktop-summit
comments:
  - subject: "It is great that a D-Bus"
    date: 2013-04-17
    body: "It is great that a D-Bus standard has finally been agreed on!"
    author: "Celeste Lyn Paul"
  - subject: "Awesome news!"
    date: 2013-04-18
    body: "the fact that all groups gathered to talk about ideas and solutions and to collaborate between each other for the same objective. I even find this better than the actual objectives talked about. I hope this will be done more often and create more standards between all Desktop environments."
    author: "Luis Alvarado"
  - subject: "Too much common sense"
    date: 2013-04-18
    body: "It seems that way too much common sense is pouring out of this post.\r\n\r\nWhat's next? Single keyring accessible by DBUS? Single thumbnails cache with agreed structure? When will this madness end and everyone goes back to happily reinventing the wheel in their respective platforms?"
    author: "Vincent Vega"
  - subject: "This should be an annual event.."
    date: 2013-04-18
    body: "I think this should be an annual event.. (or 6 months) This is a great thing. Because recently more and more desktop users and companies are reaching to Linux. But they all have to face the incompatibility issues. If all things are compatible, everyone can use their favorite things. So, make this an event. Perhaps this is the most important event held recently."
    author: "Vishwaje"
  - subject: "Secret Storage API specification project"
    date: 2013-04-18
    body: "What about http://lists.freedesktop.org/archives/authentication/2009-July/000002.html ?"
    author: "Anonymous"
  - subject: "How does launching"
    date: 2013-04-18
    body: "How does launching applications with D-Bus work, what loads the actual binary at the end?"
    author: "teho"
  - subject: "dbus autolaunch"
    date: 2013-04-19
    body: "The dbus daemon starts the actual binary if it's not running yet, using the .service file located in e.g. /usr/share/dbus-1/services/\r\nThis already works and is already used for \"daemons\", what's new is using that for GUI applications."
    author: "dfaure"
  - subject: "Re: This should be an annual event.."
    date: 2013-04-19
    body: "Yes, that was definitely the conclusion at the end of the meeting: \"let's do this again, in 6 or 12 months\""
    author: "dfaure"
  - subject: "Re: Too much common sense"
    date: 2013-04-19
    body: "I suppose this was ironic :)\r\n\r\nBut I want to say that the two things you're listing already exist.\r\nSingle keyring: spec exists, implementation from KDE will be in KF5. Single thumbnails cache: spec and code is already in place (and has been for a long time). http://standards.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html  (heh, spec from 2001)"
    author: "dfaure"
  - subject: "re: dbus autolaunch"
    date: 2013-04-19
    body: "Interesting; I don't know if dcop had this but qcop certainly did (in a much simpler form, of course)."
    author: "bluelightning"
  - subject: "Re: Re: Too much common sense"
    date: 2013-04-22
    body: "Hmm, don't tell guys from GNOME, that KF5 is going to implement this. Once they see it, they will immediately call it deprecated (even though they were the first to formulate this standard) and a threat to their \"product identity\".\r\n\r\nAs for the thumbnail standard. Do both Nautilus and Dolphin use the same cache?"
    author: "Vincent Vega"
  - subject: "Seconded"
    date: 2013-04-23
    body: "This is the greatest collaboration I've heard in a very long time. Widely-accepted standards will save a lot of rework. "
    author: "Andy Jackson"
  - subject: "Is there anywhere I could"
    date: 2013-04-30
    body: "Is there anywhere I could read about the new desktop file cache format?"
    author: "Kalev"
  - subject: "Great News"
    date: 2013-05-06
    body: "This is the best news I read ever since many years. I hope that sanity can be the common factor, that could be the true \"Unity\" :)"
    author: "sebelk"
---
During the week of 8 April 2013, developers from the <a href="http://kde.org/">KDE</a>, <a href="http://www.gnome.org/">GNOME</a>, <a href="http://unity.ubuntu.com/">Unity</a> and <a href="http://razor-qt.org/">Razor-qt</a> projects met at the <a href="https://www.suse.com/">SUSE</a> offices in Nürnberg to improve collaboration between the projects by discussing specifications. A wide range of topics was covered.

There was agreement on a specification for a <a href="http://www.freedesktop.org/wiki/Software/dbus">D-Bus</a> interface to be implemented by applications. Pending implementation, applications are now capable of being launched using D-Bus activation instead of executing a binary. Changes were also agreed for the desktop entry specification for applications to advertise this capability.

We reached agreement on a modification to the trash specification to allow for an efficient means of determining the size of all items in the trash (to warn the user when the size is getting too large).

A new file format was defined to cache and index the contents of all .desktop files within a particular directory. This new format will allow efficient full-text search over desktop files as well as efficient lookups of various other kinds (for example, identifying which apps support a given file type) while increasing performance by reducing disk seeks. It will also reduce memory consumption because it can be shared by all processes using <a href="http://en.wikipedia.org/wiki/Mmap">mmap</a>.

The in-development kernel D-Bus implementation was presented at the meeting. Representatives from the desktop environments made suggestions to improve the kernel API to facilitate implementation of libraries.

We discussed the future of accountsservice and how, going forward, the project will be sensitive to the needs of desktops other than GNOME. This included specific discussions regarding implementation of storing user locale in the service as well as providing an extension mechanism for structured storage of arbitrary key/value data, without needing to patch the service.

There were initial discussions (with no concrete results) on a wide range of other topics including D-Bus session management APIs, a replacement for X11-based startup notification, application intents and "portals", exporting action groups on D-Bus and adding actions to context menus in the file browser.

Perhaps most importantly we have come to agreement on a plan for improving the maintenance of freedesktop specifications going forward. One representative from each of GNOME, KDE and Unity will form a joint maintainer team. This team will monitor and participate in conversations on the xdg list and decide when consensus has been reached. The intention is to revive the usefulness of the xdg list as the primary point of communication between desktop projects.