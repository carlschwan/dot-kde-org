---
title: "Second Beta of Plasma Workspaces, Applications and Platform 4.11"
date:    2013-06-28
authors:
  - "kallecarl"
slug:    second-beta-plasma-workspaces-applications-and-platform-411
comments:
  - subject: "Thanks folks!"
    date: 2013-06-30
    body: "Thanks to all the people who are putting in the effort to get KDE 4.11 ready!  It really looks like a smoother version of KDE 4.10, which is a compliment since the KDE team has gotten the big stuff right and now they are just adding polish."
    author: "DonCornelius"
---
The second beta of the 4.11 releases of Plasma Workspaces, Applications and Platform is available. The <a href="http://www.kde.org/announcements/announce-4.11-beta2.php">beta 2 release announcement</a> has highlights, links to release details and download instructions. The development focus is on bug fixing, polishing and general stabilization.

<div align=center style="padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/411betascreenshot.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/411betascreenshotB.png" /></a><br />Changes to KWalletManager and Okular</div>

With the large number of changes, the 4.11 releases need to be tested thoroughly to ensure quality and an excellent user experience. Actual users are critical to the polishing efforts, because developers simply cannot test every possible configuration. Please consider joining the 4.11 team by installing the beta and <a href="https://bugs.kde.org/">reporting any bugs</a> so they can be squashed before the final release.

Check out <a href="http://www.kde.org/announcements/announce-4.11-beta2.php">the full announcement</a>.