---
title: "KDE Commit-Digest for 7th April 2013"
date:    2013-04-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-7th-april-2013
---
In <a href="http://commit-digest.org/issues/2013-04-07/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> can now read & save .asx playlists, becomes power-management aware: it can inhibit automatic system suspend and pause on resume (configurable); reads playlist files asynchronously when possible</li>
<li>A new simple template to create a <a href="http://community.kde.org/Plasma">Plasma</a> applet with Plasma QML components</li>
<li><a href="http://skrooge.org/">Skrooge</a> adds monthly reports and the main page uses the standard KDE font</li>
<li><a href="http://www.kdenlive.org/">Kdenlive</a> adds reverse clip to clip actions</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> sees big code re-organizations: splits out screen handling from Workspace into own-class Screens, and separates Activities-related code out from Workspace</li>
<li>Work on <a href="http://www.calligra-suite.org/">Calligra</a> on Android (<a href="http://blogs.kde.org/2013/03/24/coffice-calligra-android-available-now">Coffice</a>) continues.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-04-07/">Read the rest of the Digest here</a>.