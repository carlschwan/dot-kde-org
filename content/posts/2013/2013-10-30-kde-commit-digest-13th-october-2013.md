---
title: "KDE Commit-Digest for 13th October 2013"
date:    2013-10-30
authors:
  - "mrybczyn"
slug:    kde-commit-digest-13th-october-2013
---
In <a href="http://commit-digest.org/issues/2013-10-13/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/Telepathy">KDE Telepathy</a> adds an ability to edit the name of your favorite chat rooms for quick joining</li>
<li>In <a href="http://skrooge.org/">Skrooge</a>, operations can be split by date</li>
<li><a href="http://techbase.kde.org/Projects/Kooka">Kooka</a> does image transformations in a background thread to reduce wait time</li>
<li><a href="http://okular.org/">Okular</a> supports more transformations, including the ones dictated by Exif metadata</li>
<li>Bug fixes in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://dolphin.kde.org/">Dolphin</a>, <a href="http://kontact.kde.org/kmail/">KMail</a></li>
</ul>

<a href="http://commit-digest.org/issues/2013-10-13/">Read the rest of the Digest here</a>.