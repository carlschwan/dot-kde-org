---
title: "KDE Commit-Digest for 9th June 2013"
date:    2013-06-30
authors:
  - "mrybczyn"
slug:    kde-commit-digest-9th-june-2013
---
In <a href="http://commit-digest.org/issues/2013-06-09/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://www.calligra-suite.org/">Calligra</a>, the tag categories the user can define for various resources will update for any resource of the same type; in <a href="http://krita.org/">Krita</a>, Pixel Selection can be converted into a Vector Selection</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> makes it possible to pull includes and defines from the <a href="http://www.cmake.org/">CMake</a> targets</li>
<li>Battery applet animates the battery icon when charge is below 5%; batteries are sorted by name</li>
<li><a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma Media Center</a> sees much work on youtube videos</li>
<li><a href="http://www.kde.org/applications/internet/kget/">KGet</a> merges work from GSoC 2012 on Metalink HTTP Headers</li>
<li><a href="http://utils.kde.org/projects/kgpg/">KGPG</a> sorts keys by domain name of email address</li>
<li>Optimized calendar handling in <a href="http://pim.kde.org/">KDE-PIM</a>, file indexing in <a href="http://nepomuk.kde.org/">Nepomuk</a>, gradients in Calligra, and Analyzer drawing in <a href="http://amarok.kde.org/">Amarok</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-06-09/">Read the rest of the Digest here</a>.