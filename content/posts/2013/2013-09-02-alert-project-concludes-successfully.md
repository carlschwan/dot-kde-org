---
title: "ALERT Project concludes successfully"
date:    2013-09-02
authors:
  - "Stuart Jarvis"
slug:    alert-project-concludes-successfully
comments:
  - subject: "Congratulations!"
    date: 2013-09-12
    body: "It's great to hear that this project was a success! We need these success stories, because I think KDE and the scientific community can greatly benefit from each other."
    author: "Thomas Pfeiffer"
---
Back in the last months of 2010, the <a href="http://alert-project.eu">ALERT Project</a> began. Partly funded under the European Union's 7th Framework Programme, the project followed in the footsteps of research efforts such as NEPOMUK. Its objectives were to help open source developers to work more effectively and to produce better software by improving bug tracking, resolution and software quality tools.

The ALERT Project brought together partners from seven countries, including large software companies such as Atos, medium size enterprises and research institutes and universities. It also included KDE e.V. as a partner, specifically for the purposes of guiding the project and providing a test case for the software that would be produced. As part of this process, three KDE experts, Adriaan de Groot, Dario Freddi and Stuart Jarvis were contracted to another project partner (Corvinnno Technology Transfer of Hungary) to provide consultation services on the needs of free software developers and to facilitate communication with KDE e.V. and the KDE community. The KDE experts were chosen through an open selection process, with the opportunities advertised on KDE.News.

<h2>Kick-off meeting</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/alert01.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/alert01sml.png" /></a><br />ALERT Project Overview</div>An initial kick-off meeting took place in October 2010, as the partners got a chance to meet each other and discuss objectives and timescales. The project really got started during a meeting in January 2011 where the KDE experts attended for the first time and got to meet the other participants. It became clear that many of the partners had limited experience with open source software and so it was important for KDE to be involved and give advice on issues such as licenses acceptable to free software developers, the importance of code re-use and availability of source code.

Over the next two years, the partners and KDE experts met approximately every three months, with frequent email exchanges and online chats in the meantime. KDE's involvement in the early stages was mostly in the form of contributing to documents describing the problems that should be solved. For example, Bugzilla is a popular tool to track bugs, but for large projects such as KDE there are problems with duplicate reports and with reports being filed to the wrong team (it is not always easy for a user to understand that an apparent problem with a web browser failing to show pages is actually due to a separate software handling wireless connections). With this in mind, the KDE experts decided to focus on Solid (the KDE software components dealing with hardware interaction) as a base for KDE's testing of the ALERT software.

<h2>Interactions with KDE</h2>

After the initial stages, Adriaan left the project due to other commitments and Dario and Stuart took over the work between them. The research partners demoed ways of gathering information about bugs from diverse resources such as mailing lists, wikis and forums and showed how natural language processing and stack trace analysis could help to identify duplicate bug reports. One partner even began profiling developers, identifying developers suited to solving particular bugs and bugs suited to the skills of a particular developer.

Concerns over privacy and data rights led to intensive discussions between research partners, the KDE experts and KDE e.V. board. The experts were however eventually able to resolve these by advising the ALERT partners on how to access data via public sources and open protocols (for example, RSS) rather than requiring database dumps that might have contained sensitive data.

<h2>Developing and testing the software</h2>
By early 2012, most of the software components were working, but as individual entities (each partner had taken responsibility for particular functions and written their own components and interfaces). A message bus enabled communication between components, but there was no user-facing single, consistent user interface. Identifying this as a shortcoming with the aid of the KDE experts, the partners diverted resources to build a unified user interface and this was first demoed in the summer of 2012, becoming ready for the first set of user trials in the autumn. In parallel, work began on automating installation of the diverse components and on providing a ready-built virtual machine. KDE experts provided extensive testing and feedback on the latter.

The public profile of ALERT was also raised during this time, with partners and the KDE experts appearing at the OpenWorld Forum in Paris and presenting the ALERT software for the first time in public.

In the last months of 2012, the first set of user trials began. The KDE experts continued to focus on Solid as the main use case and recruited Solid team developers to try out the ALERT tools via the ALERT user interface as part of their normal activities. This meant using ALERT to hunt for duplicate reports, using it to gather additional information on a bug from mailing lists, wikis or the forum and even getting customized notifications on particular patterns of activity in Bugzilla or the source code repository. The feedback was returned to the project partners over the Christmas period in 2012 and led to further developments early in 2013 before a second trial period in March 2013.

Feedback from the trials was largely positive. The project has been judged successful and even highlighted as a success story by the EU Commission. Although the initial project is over, many of the partners intend to continue developing their components and the software as a whole. One research partner, the Libresoft group at the University Rey Juan Carlos, has spun-out a company to further develop the software. They have already done some work in integrating ALERT capabilities into Bugzilla, so that some of the benefits can be accessed without the need to learn new software or access a separate system.

<h2>Conclusions</h2>
Overall, the project has provided a very positive experience and has helped to raise the profile of KDE (and free software in general) among some of the partners. Discussions have already taken place about possible future collaborations. The ALERT software is fully operational now and can be downloaded from the ALERT website, either as source code (with some binaries) or as a ready set-up virtual machine. Some configuration is needed to get started, as detailed in the user manual. Future developments will hopefully further streamline the software and make its deployment easier. KDE will continue to watch developments and consider a wider deployment of the software as it matures.

Thanks to every member of the KDE community that has participated and helped make this long term achievement possible. The KDE community is open to further experiences in research programs like this one.