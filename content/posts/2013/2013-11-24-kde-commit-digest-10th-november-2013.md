---
title: "KDE Commit-Digest for 10th November 2013"
date:    2013-11-24
authors:
  - "mrybczyn"
slug:    kde-commit-digest-10th-november-2013
comments:
  - subject: " KColorSchemeManager supports changing color scheme in apps"
    date: 2013-11-27
    body: "Nice to hear that. Does this mean, one could exclude single applications from dark themes? This would be a wonderfull addition as i only stopped using dark theme because of a few applications that cannot handle it well. "
    author: "Till Sch\u00e4fer"
---
In <a href="http://commit-digest.org/issues/2013-11-10/">this week's KDE Commit-Digest</a>:

<ul><li>In KDE Frameworks, new KColorSchemeManager supports changing color scheme in apps</li>
<li><a href="http://kate-editor.org/">Kate</a> adds an option to flash matching brackets</li>
<li><a href="http://krita.org/">Krita</a> allows multibrush to use an angled axis and have an option to show the axis; new implementation of the brush outline display</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> sees major update of Ekos and KStars FITSViewer tool</li>
<li><a href="http://userbase.kde.org/Trojit%C3%A1">Trojita</a> adds support for more decoding formats.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-11-10/">Read the rest of the Digest here</a>.