---
title: "Digia to sponsor Akademy and Qt Contributors Summit"
date:    2013-03-21
authors:
  - "kallecarl"
slug:    digia-sponsor-akademy-and-qt-contributors-summit
---
Digia, the largest Qt contributor, is the 2013 Platinum Sponsor for the co-hosted <a href="http://akademy2013.kde.org/">Akademy</a> and <a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">Qt Contributor Summit</a> in Bilbao. Digia is responsible for all Qt activities including product development and commercial licensing and, together with the Qt Project, open source licensing under the open governance model.

<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/akademy2013LogoTransparent.png" /></div>
Traditionally, there is a strong Qt community presence at Akademy, the annual conference of the KDE community because of the close technical and social ties between the two projects. The Akademy program always includes Qt topics. Last year, there was a special full day Qt Quick workshop. This year, the collaboration will go even further by holding the main conferences of both KDE and Qt communities < a href="http://dot.kde.org/2013/02/14/akademy-and-qt-contributors-summit-join-forces">in the same place at the same time</a>.

"This joint conference will foster interaction, knowledge transfer and technical progress in a productive, open, innovative atmosphere where new and veteran contributors can meet to influence the future direction of Qt", says Knut Yrvin, Qt Community Manager at Digia. Digia and KDE share a common interest and a strong commitment to Qt.

Community and commercial development of Qt have been closely associated for many years. The <a href="http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a> ensures that Qt is free software and is also available licensed for commercial use. Thoughtleaders Tuukka Turunen (Digia) and Mirko Böhm (KDE e.V.) recently co-authored <a href="http://creative-destruction.me/2013/03/15/benefits-of-co-existing-open-source-commercial-software/">an essay on the benefits of dual licensing</a>.

The Qt Project was launched in October 2011, allowing both companies and individuals to contribute to the development of Qt under open governance. Since then, there has been a strong stream of contributions from the free and open software community and commercial enterprises. About 40 percent of Qt commits are made by commercial and free software entities other than Digia.

The KDE Community has always been at the forefront of trying, developing, using and adapting Qt capabilities in KDE projects, accounting for the largest external use of Qt. Many developers in the core Qt R&D teams of Trolltech, Nokia and now Digia have come from the KDE Community.

This symbiotic development process ensures that Qt is improved continuously and that it adapts to a wide range of use cases. This is amplified by the cross platform nature of Qt, which supports application development across different form factors, operating systems, diverse input methods, and a wide range of use cases and requirements.

The structure of Qt development ensures that support for new use cases is included in Qt, along with the software itself. These complete contributions come from free software communities such as KDE, and from companies in more than 70 different industries. Qt is the top-ranked cross platform framework in terms of developer satisfaction according to VisionMobile's survey of 2,400 developers. It also scores significantly higher than other tools for its performance.

Digia is committed to keeping Qt as the top cross platform framework on the planet. Thanks to community and commercial cooperation, we are doing this together successfully.

Additional sponsors are invited for both events. Please check the sponsor pages for <a href= "http://akademy2013.kde.org/sponsoring">Akademy</a> and <a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki/Sponsoring">Qt Contributor Summit</a>.

<h2>Register soon</h2>
Bilbao, the host city, will be busy during the first part of July. People planning to attend Akademy are encouraged to <a href="http://akademy2013.kde.org/register">register</a> and make travel plans as soon as possible. QtCS attendance and registration information is below in the "About the Qt Contributor Summit" section.  

<h2>About Akademy</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. The annual Akademy conference provides the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE.

Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other kinds of KDE contributors to celebrate the achievements of the past year and determine future directions. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The <a href="http://manifesto.kde.org/">KDE Manifesto</a> guides these efforts. The KDE Community welcomes companies with an interest in KDE technology.

For more information, please contact <a href="http://akademy2013.kde.org/contact">The Akademy Team</a>.

<h2>About the Qt Contributor Summit</h2>
Qt Contributor Summit (QtCS) is in its third year after the successful launch of Qt Open Governance in 2011. It is a hands-on gathering to get things done and plan ahead, and is a free of charge, invitation-only event. This year, the KDE Community has invited QtCS to be co-located with Akademy, influencing, improving and creating Qt excellence for the future.

You'll be invited to this conference by a Qt contributor. You register session(s) at the QtCS 2013 on the <a href=http://qt-project.org/groups/qt-contributors-summit-2013/wiki">Qt Project Web wiki</a>. Contact the organizers on the email-list: marketing@qt-project.org.