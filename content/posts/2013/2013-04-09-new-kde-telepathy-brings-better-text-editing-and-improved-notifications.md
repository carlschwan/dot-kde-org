---
title: "New KDE Telepathy brings Better Text Editing and Improved Notifications"
date:    2013-04-09
authors:
  - "David Edmundson"
slug:    new-kde-telepathy-brings-better-text-editing-and-improved-notifications
comments:
  - subject: "meta contacts"
    date: 2013-04-09
    body: "Any chance to have meta contacts? Really a lot of users are stuck with kopete just for this (including me). I have gtalk, msn, icq and facebook and very often I have the same guy on all of these protocols... multiply for 100 and you can imagine how long my contact list is...."
    author: "Andrea Z."
  - subject: "Like"
    date: 2013-04-09
    body: "I just gave it another test. Looks good :-) Seems like it is really up for being a Kopete replacement soon. The main thing I am missing is contact grouping and a connection to Kaddressbook. Wasn't this in the making already?"
    author: "mark"
  - subject: "KPeople"
    date: 2013-04-09
    body: "This will be solved once the first version of the \"KPeople\" library (currently under development) is out and KDE Telepathy is ported to use it."
    author: "einar"
  - subject: "Smiles"
    date: 2013-04-09
    body: "button for choose smiles requested :)"
    author: "Alex_123"
  - subject: "Better handling of dropped network connections?"
    date: 2013-04-09
    body: "As per the subject - everytime I try telepathy I end up disabling it as it is hopeless at reconnecting/reautherising after the net connection drops, plus endless notifications about that. \r\n\r\nSince I use wireless and hibernate/sleep my PC regularly happens multiple times during the day."
    author: "Lindsay Mathieson"
  - subject: "How to add en_GB dictionary"
    date: 2013-04-10
    body: "I suppose this question also applies for other dictionaries. How does one add a dictionary to KTp? I tried to look for a manual for a how to somewhere but couldn't find one. If it's switching the application language I already have the kde-l10n-engb package installed, but am unable to select it in KTp.\r\n\r\nThanks for all the hard work!"
    author: "Rewarp"
  - subject: "OTR"
    date: 2013-04-10
    body: "One of the things that keeps me from migrating away frm Kopete, or having to look at non-KDE solutions, is a need for OTR-enabled clients."
    author: "Samir Nassar"
  - subject: "That would ideed be very cool :-)"
    date: 2013-04-11
    body: "I am looking forward to OTR support too."
    author: "mark"
  - subject: "Confirm"
    date: 2013-04-11
    body: "I ran into this issue, too. Plus there are Telepathy processes left after i logged out. I have to mention that i have built Telepathy with gnome-keyring support."
    author: "greggel"
  - subject: "Privacy options"
    date: 2013-04-11
    body: "I've tried to switch from Pidgin to Telepathy a few times.  Then I get russian hackers asking me if I want to talk on my old hotmail (MSN Messenger) account.  I much prefer whitelisting to blacklisting...blacklisting bots is just futile.  Please add basic privacy options like the old \"Accept messages only from people on my contact list\".  MSN will be taken out back behind the barn and shot in the head soon enough, but if you can't rely on the individual service to provide privacy options, then it falls back to the client."
    author: "Aoxo"
  - subject: "Very nice"
    date: 2013-04-13
    body: "Just tried. Really nice. Moved my history from Kopete :)"
    author: "Nikita Krupenko"
  - subject: "Settings->Configure Toolbars "
    date: 2013-04-13
    body: "Settings->Configure Toolbars -> Add Smiley\r\n\r\nI'll try and make this more prominent."
    author: "David Edmundson"
  - subject: "http://community.kde.org/KTp"
    date: 2013-04-13
    body: "http://community.kde.org/KTp/RepeatedDiscussions/OTR"
    author: "David Edmundson"
  - subject: "Settings -> Toolbars shows ->"
    date: 2013-04-13
    body: "Settings -> Toolbars shows -> Language Toolbar"
    author: "David Edmundson"
  - subject: "Tarballs"
    date: 2013-06-01
    body: "I see 13 tarballs in the download directory of telepathy.\r\nHow about making it a single tarball?"
    author: "Alan"
  - subject: "Design flaw"
    date: 2013-06-29
    body: "I've just moved to kde and not sure if I'm missing something but atm the way I have to interact with telepathy doesn't make much sense. IM Contacts are fine, got the sys-tray with a keyboard shortcut and all but I open the Text UI, join a few jooms, chat to a few people and the only thing I can do with that window is to minimize it. I close it and all my rooms are gone... I connect to hipchat that my company uses for internal international communications and I have to join lots of rooms at startup. And I can only re-join them one by one which makes it pretty useless for anything else that a quick facebook chat. Is there another application that would integrate better into the whole Telepathy platform better than Text UI?"
    author: "Vigintas"
  - subject: "Contact Plasmoid Protocol Icon"
    date: 2013-07-09
    body: "So far I really like KTp, it integrates all my accounts very nicely. However the contact list plasmoid doesn't show the protocol icon for each contact, so when you have the same contact on multiple accounts, it can be confusing. Could this be added in?\r\n\r\nOne thing I really miss from when I was using pidgin is skype avatars. In KTp, all skype contacts are just replaced with the default avatar instead of the correct avatar."
    author: "Scott"
  - subject: "ibus-unikey"
    date: 2013-07-10
    body: "Hi,\r\nI've just installed ktp 0.6.2. And I can't input my vietnamese language into ktp-text-ui (using ibus-unikey which is my favorite).\r\n\r\nIs there a bug, can someone help fix it?\r\n-James"
    author: "Jamesvn"
  - subject: "Getting help/fixing bugs"
    date: 2013-07-10
    body: "There are other KDE resources that are more effective than Dot story comments for getting help. In addition, using these other support resources make it possible for other people to benefit from solutions.\r\n\r\nYou can look on bugs.kde.org to see if this has been reported as a bug, and the status if it has been.\r\n\r\nPlease visit forum.kde.org. There is a section there about KDE Telepathy, which is monitored by KTp developers."
    author: "kallecarl"
  - subject: "Thanks."
    date: 2013-07-12
    body: "Thanks.\r\nI'll check them out.\r\n~James."
    author: "Jamesvn"
  - subject: "Gadu Gadu"
    date: 2013-10-19
    body: "I would use it if there was <a href=\"http://en.wikipedia.org/wiki/Gadu-Gadu\">Gadu Gadu</a> support :( Really need this one."
    author: "fatino"
  - subject: "Gadu Gadu"
    date: 2013-10-22
    body: "Nope... I'm afraid there is not much alternative to using Kadu for GG (it can handle also GTalk and Facebook Chat). It integrates with KDE quite well:\r\nhttp://www.kadu.im/w/English:News"
    author: "PTR"
  - subject: "Metacontacts in KDE Telepathy 0.7"
    date: 2013-10-25
    body: "Metacontacts is implemented! http://www.sharpley.org.uk/node/68"
    author: "Murz"
  - subject: "OTR and Auto-reply on Away"
    date: 2013-12-14
    body: "I would use KDE Telepathy once it has OTR (off the record) messaging support.\r\nEven an auto-reply on away support would have been acceptable.\r\n\r\nSo far, I only see minor UI updates and message parsing features after months of waiting for a new release."
    author: "Skeith"
  - subject: "OTR is a must"
    date: 2014-01-27
    body: "With everthing we have learned about the NSA's blanket collection of internet communications and the fact that SSL to any server is useless given the fact that the very same service providers are more than likely handing the unencrypted communications directly to them anyway, OTR is an absolute must before I switch from Kopete. "
    author: "Bobbb"
  - subject: "OTR is a reason why pidgin stays"
    date: 2014-04-21
    body: "i was paranoid even before it was not paranoid and i support your message."
    author: "nescius"
---
Good news, everyone! The KDE Telepathy team has just released version 0.6.0 of KDE Telepathy (KTp), KDE's instant messaging suite. This version brings a number of new features as well as a large amount of bug fixes.

<h2>About KDE Telepathy</h2>
KDE Telepathy is an instant messaging suite built upon the <a href="http://telepathy.freedesktop.org/">Telepathy</a> framework. KDE Telepathy provides text chat, video calls and file transfer services over many popular instant messaging platforms including Facebook Chat and Google Talk.  In addition KTp provides libraries for embedding instant messaging or real-time collaboration inside your application.

KDE Telepathy follows the philosophy that communication is a service offered to applications and the desktop. A more in-depth technical explanation can be read over <a href="http://www.aosabook.org/en/telepathy.html">here</a>, but in practice, this means that KDE Telepathy consists of independent components. This allows KDE Telepathy to be well integrated with Plasma by providing plasmoids and runner interfaces as well as providing the traditional contact list and chatting application. This level of desktop integration allows endless possibilities and greater freedom in the way in which you interact with your contacts. Boundaries of Telepathy however do not stop at communication, the next step is collaboration, for example games can take advantage of Telepathy and provide a multi-player capabilities without the hassle of setting up network connections.

<h2>New Features</h2>
This release brings in a number of major new features. An overview:

<strong>Kopete log migration</strong>
KTp now imports logs from Kopete accounts into our log format. For new KTp users this will be asked if they wish to import when they create an account, existing users can also import logs by opening the log viewer.

<img src="http://dot.kde.org/sites/dot.kde.org/files/log_import.png" style="width:80%"/>

<strong>Clearer message notifications</strong>
KTp had some feedback to improve the notifications of new messages. KTp now shows an icon in the contact list when a new message arrives, change the icon in the system tray, and for group chats show who is typing.

<img src="http://dot.kde.org/sites/dot.kde.org/files/new_messages.png" style="width:80%"/>

<strong>Better text editing</strong>
The chat window now features tab completion for group chats, as well as text navigation for editing messages.

KTp has made adding emoticons easier too, with a new optional emoticon toolbar.

<strong>Advanced notifications</strong>
KTp now supports setting different notifications for each of your contacts. This means it is possible to set an optional notification  if your favourite friends come online, or play sounds when messages arrive from certain contacts but not others.

<img src="http://dot.kde.org/sites/dot.kde.org/files/notifications.png" style="width:80%"/>

<strong>Improved password and security management</strong>
KTp is now able to connect to password protected jabber rooms, a much requested feature. We have also improved our connection certificate handling, now using KDE SSL certificates manager and allowing the user to override invalid certificates.

<strong>Under the hood changes and cleanups</strong>
A lot of our effort has been spent in a big refactoring under the hood, getting ourselves ready for the future as well as bringing speed and stability closing over 85 bug reports in 0.6.

0.6.0 features completely redone connection error notifications and other important UI areas.

<strong>Core filtering plugins</strong>

KTp has an extended range of message plugins to make chatting more dynamic and interactive.

<em>Text messages can be formatted in bold or italics</em>

<img src="http://dot.kde.org/sites/dot.kde.org/files/formatting.png" style="width:80%"/>

<em>Youtube links are show and can be played directly in your chat window</b>

<img src="http://dot.kde.org/sites/dot.kde.org/files/youtube.png" style="width:80%"/>

<em>Links to bugzilla are shown inline with the bug title and resolution</b>

<img src="http://dot.kde.org/sites/dot.kde.org/files/bug.png" style="width:80%"/>

<em>When sending messages can use your KDE webshortcuts to make it quicker to send links</b>

<img src="http://dot.kde.org/sites/dot.kde.org/files/search_replace.png" style="width:80%"/>

<em>Messages containing your name are highlighted and a special notification with sound can be emitted. This is especially useful if you lurk in conference rooms</b>

<img src="http://dot.kde.org/sites/dot.kde.org/files/highlight.png" style="width:80%"/>

<h2>Getting 0.6</h2>
After this huge list of awesome new things, you might want to try KTp for yourself! So, where to get it?

First of all, the source tarballs are available from <a href="http://download.kde.org/stable/kde-telepathy/0.6.0/src/">the KDE download servers</a>. This contains our full set of applications and applets.

Most distributions, including <a href="http://software.opensuse.org/search?q=ktp">openSUSE</a> (in <a href="https://en.opensuse.org/KDE_repositories#Updated_applications_only">the KDE-extra repository</a>), <a href="https://apps.fedoraproject.org/packages/s/kde%20telepathy">Fedora</a> and <a href="https://launchpad.net/~telepathy-kde/+archive/ppa">Kubuntu</a>, have packages available already or will have, soon. Note that you will need the full set of packages for KTp to work properly. In most distributions, that means installing the right repository and getting the ktp* packages.

Of course, if you find issues or think of possible improvements, you can either <a href="https://bugs.kde.org/enter_bug.cgi?product=telepathy&format=guided">file a bug</a> or <a href="http://community.kde.org/KTp/Getting_Involved">join development</a>!

<a href="http://dot.kde.org/sites/dot.kde.org/files/ktp_all.png">
<img src="http://dot.kde.org/sites/dot.kde.org/files/ktp_all_small.png" style="width:80%"/>
</a>
