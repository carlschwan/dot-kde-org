---
title: "Pre-order Akademy 2013 T-shirt"
date:    2013-05-23
authors:
  - "kallecarl"
slug:    pre-order-akademy-2013-t-shirt
comments:
  - subject: "Women's T's?"
    date: 2013-05-24
    body: "Will there be women's t-shirts?"
    author: "odysseus"
  - subject: "Yes"
    date: 2013-05-24
    body: "Yes, it lists on the the akademy t-shirt webpage and the pre-order system that there is the option of standard or fitted t-shirts.\r\n\r\nThe fitted style is suitable for some women, while others may prefer to wear the regular/standard style."
    author: "sealne"
---
For the first time this year, attendees can <a href="http://akademy2013.kde.org/tshirt">pre-order an official Akademy 2013 shirt</a> (with picture), pick it up at Akademy 2013 in Bilbao and pay a reduced price. This year's distinctive design is based on the <a href="http://en.wikipedia.org/wiki/Lauburu">Lauburu</a>, an ancient Basque symbol.

<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

It's hard for the Akademy Team to make an accurate estimate of how many of each size of shirt to order. Some sizes sell out quickly, while there may be leftovers of other sizes. 

If you are definitely coming to Akademy 2013, you can pre-order the size of t-shirt that you want, then pick it up and pay during the conference. Pre-ordered t-shirts have a reduced price of <strong>€12</strong> (normally €15). <strong>T-shirt pre-orders are open till 31st May</strong>. Pre-order a shirt and make things easier for yourself and for the Akademy Team.

<strong><a href="http://akademy2013.kde.org/register">Register</a></strong> now for Akademy; there is an option for pre-ordering a T-shirt during the registration as well as more details about the shirts. You are encouraged to <strong><a href="http://akademy2013.kde.org/travel">book your travel</a></strong> and <strong><a href="http://akademy2013.kde.org/accommodation">accommodation</a></strong> soon, because <a href="http://akademy2013.kde.org/">Akademy 2013</a> will take place during a busy time in Bilbao (including <a href="http://www.bilbaobbklive.com/2013/en/">BBK Live</a>).

<h2>Akademy 2013 Bilbao, the Basque Country, Spain</h2>

For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.