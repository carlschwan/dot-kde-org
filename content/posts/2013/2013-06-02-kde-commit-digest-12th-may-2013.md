---
title: "KDE Commit-Digest for 12th May 2013"
date:    2013-06-02
authors:
  - "mrybczyn"
slug:    kde-commit-digest-12th-may-2013
---
In <a href="http://commit-digest.org/issues/2013-05-12/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.digikam.org/">Digikam</a> adds video properties search in Advanced Search tool</li>
<li><a href="http://okular.org/">Okular</a> better supports annotations in rotated pages</li>
<li>In KDE Workspace, sorting of tasks is possible by activity; the KDE Classic cursor theme becomes an image-based theme</li>
<li>Easier drag and drop of items in <a href="http://dolphin.kde.org/">Dolphin</a> Places</li>
<li>Network Management shows connection details</li>
<li><a href="http://skrooge.org/">Skrooge</a> adds a possibility to open report from dashboard widgets.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-05-12/">Read the rest of the Digest here</a>.