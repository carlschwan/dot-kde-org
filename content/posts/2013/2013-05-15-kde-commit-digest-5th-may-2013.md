---
title: "KDE Commit-Digest for 5th May 2013"
date:    2013-05-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-5th-may-2013
---
In <a href="http://commit-digest.org/issues/2013-05-05/">this week's KDE Commit-Digest</a>:

<ul><li>Multiple changes in <a href="http://www.digikam.org/">Digikam</a>, especially around the full screen mode</li>
<li><a href="http://amarok.kde.org/">Amarok</a>'s MusicBrainz tagger can now help choose the best tags possible</li>
<li><a href="http://kate-editor.org/">Kate</a> further improves the Vim mode by allowing the emulated Vim command bar to auto-complete from words in the document using CTRL-Space</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> makes default OpenPGP file extension configurable</li>
<li><a href="http://k3b.org/">K3b</a> informs the user if the inserted medium is not suitable</li>
<li><a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> adds an <a href="http://en.wikipedia.org/wiki/Infiniband">Infiniband</a> config</li>
<li>Optimizations in memory usage in KDE-PIM.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-05-05/">Read the rest of the Digest here</a>.