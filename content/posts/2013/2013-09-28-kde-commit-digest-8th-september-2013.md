---
title: "KDE Commit-Digest for 8th September 2013"
date:    2013-09-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-8th-september-2013
---
In <a href="http://commit-digest.org/issues/2013-09-08/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://edu.kde.org/marble/">Marble</a> adds integration with <a href="http://owncloud.org/">ownCloud</a>, settings are available in Preferences</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds easy finishing to Path and Select Path tools, fixes loading of Scale and Rotate of the predefined brushes and other fixes</li>
<li><a href="http://dolphin.kde.org/">Dolphin</a> prevents interface freezes if there are many files inside a directory or if directory access is slow for other reasons</li>
<li><a href="http://userbase.kde.org/KWin">KWin 4.11.2</a> improves memory footprint if color correction is disabled.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-09-08/">Read the rest of the Digest here</a>.