---
title: "KDE Commit-Digest for 17th March 2013"
date:    2013-04-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-17th-march-2013
---
In <a href="http://commit-digest.org/issues/2013-03-17/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.calligra-suite.org/">Calligra</a> sees improvement in read-only mode handling and initial code for COffice (Calligra for mobile); Fill Tool is ported to strokes</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> adds an option to limit the item age in Google Calendar; introduces theme manager</li>
<li>Work on <a href="http://nepomuk.kde.org/">Nepomuk</a> Service 2</li>
<li><a href="http://docs.kde.org/development/en/extragear-base/kcontrol/wacomtablet/introduction.html">Wacom tablet management</a> sees large GUI rework and more utility classes</li>
<li><a href="http://techbase.kde.org/Projects/Edu/Artikulate">Artikulate</a> adds course skeleton</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> includes updated Branch Manager dialog.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-03-17/">Read the rest of the Digest here</a>.