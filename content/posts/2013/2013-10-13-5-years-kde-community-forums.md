---
title: "5 Years of KDE Community Forums"
date:    2013-10-13
authors:
  - "einar"
slug:    5-years-kde-community-forums
comments:
  - subject: "5 Years"
    date: 2013-10-13
    body: "Congrats! A real achievement milestone. KDE Community Forums is a valued asset, one I hope remains for years to come."
    author: "Snowhog"
  - subject: "The Forums are nice and all, but ..."
    date: 2013-10-14
    body: "The Numbers you name look impressive, but over a span of 5 years? And these numbers even include the imported posts from former forums, so there is actual even less activity.\r\nIMHO this is due to missing promotion of the forums in KDE apps. Where do users nowadays go to find help? - in forums. If I go to the Help menu in Dolphin, Krunner, Kate, Calligra, ... etc. where can I find anlink to forums.kde.org? - NOWHERE!\r\n\r\nSame with Brainstorm, there is so little activity it is almost dead. This can hardly be considered as a success. One gets actually more response in the bug tracker... :(\r\nHow should ppl. know there is a forum for suggestions, if it is not linked from within the application?\r\n\r\nI wish the forum good luck for the next five years, but if these should be *alive* years, there _must_ be more promotions from within KDE software..."
    author: "The"
  - subject: "You make it sound like they"
    date: 2013-10-15
    body: "You make it sound like they're dead - but they aren't. And about adding links to the help menu: the component to do so is in the KDE Development Platform, which is currently frozen for KDE Frameworks 5 work. So for now, IIRC, it can't be added."
    author: "einar"
  - subject: "Website links"
    date: 2013-10-15
    body: "Yes, making them as apparent as possible is a good idea. It is good to hear it is on its way finally.\r\n\r\nAt least, edu.kde.org has a link under the \"Support\" menu item. Okular also has such a connection, perhaps other websites, too.\r\n\r\nhttp://okular.kde.org/contact.php\r\n\r\nSo, it is at least good. :)"
    author: "Laszlo Papp"
  - subject: "Well considering Brainstorm,"
    date: 2013-10-16
    body: "Well considering Brainstorm, it certainly is (as good as) dead and it definitely has failed to replace BKO (bugs.kde.org) for feature requests.\r\n<cite><em>And about adding links to the help menu: the component to do so is in the KDE Development Platform, which is currently frozen for KDE Frameworks 5 work. So for now, IIRC, it can't be added.</em></cite>\r\nI had hoped these two links can be added within the 4.x series ..."
    author: "Well"
---
The KDE Community makes software; most of the people in the Community are software developers. However, this large and extensive community includes more than just the people who write software. Among the many non-technical contributors there are translators, bug finders and fixers, writers, promoters, and those who provide user support. User support is critical to KDE's success, because it provides a way for technical issues to be properly reported to people who can do something about them.

The <a href="http://forum.kde.org/">KDE Community Forums</a> is the main way users can receive support from KDE. The Forums have more than 40,000 threads and over 200,000 posts. With a large number of registered users, the Forums make up the foundation for KDE user support and communication about software and applications. On October 12, 2013, KDE Forums reached a significant milestone: their five year anniversary. 

<h2>How did it all begin?</h2>
The KDE Community Forums were founded to address the need for proper user support communication. Not all users were familiar or practiced with the communication channels then in use such as mailing lists and IRC. A few unofficial forums related to KDE technology had appeared on the Internet. Unfortunately, these had problems like spam and lack of maintenance. These issues prompted the creation of proper official forums. Staffed by a few community contributors, the KDE Forums began their evolution toward a valuable place in the KDE Community.

<h2>Turning points</h2>
For KDE developers, forums were rather alien with a completely different workflow from what was in use. So acceptance took some time. But the Forum staff didn't stay idle. They started some initiatives to promote contributions to KDE, such as "Klassrooms" where a small group of people ("students") would do simple tasks—coding and non-coding—over the course of one or two weeks, including small patches and screencasts. Initiatives like the System Settings rewrite originated in the Klassrooms. The Klassrooms were quite successful.

The forum staff, inspired by an idea of <strong>Sayak "sayakb" Banerjee</strong>, built a system to handle feature requests so that they could be screened and discussed prior to the submission to KDE's bug tracker. This system, <a href="http://forum.kde.org/viewforum.php?f=83">KDE Brainstorm</a>, provided for a space for useful interactions between users and developers.

Even with these successes, the Forums were still not firmly established within the KDE Community itself. And then the Forum staff was contacted by the <a href="http://amarok.kde.org/">Amarok</a> team to explore the possibility of moving their already-established forum to a new home. With efforts from both sides, topics and users were successfully migrated. This was just the first step. More and more community members asked for forums to be created in order to provide more convenient support for users. The KDE Forums had become an important part of the KDE Community.

<strong>Myriam Schweingruber</strong> from the Amarok team writes:
<blockquote>
The Amarok Team moved our forum under the KDE hood several years ago and we are very happy about the move. All messages and accounts were moved successfully, and reactivating old accounts in the new forum went without problems. Besides having a common forum for KDE-related projects, we have also seamlessley integrated multilingual forums in Chinese, French and Spanish. For users it is certainly a great advantage to find all KDE projects at the same location. There's no longer any need to search individual websites for support forums. Our contacts with the Forum administrators are fantastic, they are very responsive and maintain the Forums extremely well.
</blockquote>

<h2>Strengthening bonds in the community</h2>
Thanks to the Forums, ties between developers and users became stronger. This was clear when the Krita Forums opened, offering not only a place for discussion, but also a way to showcase artwork made with the software. Currently, it is one of the most active areas of the KDE Community Forums, regularly visited by developers, artists and potential users from outside the KDE Community. <strong>Boudewjin Rempt</strong>, a Krita developer and team leader, describes the experience:
<blockquote>
I was initially skeptical since I thought the mailing list was accessible enough for users. I was so wrong! From the moment the Krita forums opened, it started snowballing, especially when we got a gallery section. It's definitely the preferred way for artists to reach out to each other and to the Krita developers.
</blockquote>

Today more external forums are coming under the KDE Community Forums umbrella, the latest being user support for the acclaimed video editor, <a href="http://www.kdenlive.org/">Kdenlive</a>. 

More and more developers are also taking advantage of this infrastructure. As <a href="http://dot.kde.org/2013/04/09/new-kde-telepathy-brings-better-text-editing-and-improved-notifications">KDE Telepathy</a> and <a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a> hacker <strong>Martin Klapetek</strong> says:
<blockquote>
KDE Forums provide us with a great platform to give our users support and to discuss things with all the community. Before we had our forum, users were asking for help in our bug tracker, which is just not right. With an IRC notifier bot and RSS access, we can monitor what's going on in the forums without keeping it constantly open in a browser.
</blockquote>

Non-English speakers are also welcome in the KDE Community Forums, thanks to <a href="http://forum.kde.org/viewforum.php?f=109">several language-specific forums</a>.

<h2>How are the KDE Forums possible?</h2>
This five-year endeavour was only possible thanks to the awesome Forum staff. The continuous exchange of ideas, proposals and opinions from the various staff members gave a tremendous push toward integration of the Forums with users and developers. It has truly provided a home for all KDE Community members. The author is part of the Forum administration team, and he is proud of working daily with awesome people. 

Of course, the KDE Forums continue to evolve. Large changes are on the horizon for KDE software with KDE Frameworks 5. And of course the Forums will keep pace, showing how great a FOSS community can be.