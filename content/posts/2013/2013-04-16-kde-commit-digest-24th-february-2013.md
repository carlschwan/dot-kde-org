---
title: "KDE Commit-Digest for 24th February 2013"
date:    2013-04-16
authors:
  - "mrybczyn"
slug:    kde-commit-digest-24th-february-2013
---
In <a href="http://commit-digest.org/issues/2013-02-24/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://okular.org/">Okular</a> gains a plain text backend</li>
<li>Kmenuedit sorts entries in the sub-menus</li>
<li>Smoother window drawing in <a href="http://userbase.kde.org/KWin">KWin</a></li>
<li><a href="http://amarok.kde.org/">Amarok</a> adds keyboard shortcuts to different types of seeks</li>
<li><a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> fixes missing password field in WPA2 password dialog.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-02-24/">Read the rest of the Digest here</a>.