---
title: "A Year of the Linux Desktop"
date:    2013-07-04
authors:
  - "Stuart Jarvis"
slug:    year-linux-desktop
comments:
  - subject: "Login-Manager"
    date: 2013-07-04
    body: "The login manager is a themed slim, right?  "
    author: "slime"
  - subject: "stantontas@fastmail.co.uk"
    date: 2013-07-04
    body: "Great feature, lovely news, thanks for this :)"
    author: "Stan Tontas"
  - subject: "A year of the Linux desktop"
    date: 2013-07-04
    body: "For those not hampered by RPM :) - You could also try Debian 7 (wider choice of applications preconfigured) or the DebianEdu/Skolelinux Debian blend which also have LTSP, LDAP and other solutions thought through for schools and colleges of various sizes"
    author: "Andy Cater"
  - subject: "Crapita"
    date: 2013-07-04
    body: "<a href=\"http://www.capita.co.uk/\">Capita</a> stifling open standards? Say it ain't so! \r\n\r\n[<strong>Editor note</strong>: Capita had a <a href=\"http://www.reuters.com/article/2009/10/19/idUS38867+19-Oct-2009+PRN20091019\">contract to provide home access</a> for low income families from <a href=\"http://en.wikipedia.org/wiki/Becta\">BECTA</a> (British Educational Communications and Technology Agency). BECTA's plans were widely seen as outdated and seemed to favor large, entrenched companies rather than free and open technologies and smaller, more entrepreneurial organizations. There were claims that the program had \"serious anti-competitive implications\". BECTA was disbanded in 2011.\r\n\r\n<a href=\"http://mclear.co.uk/\">John McLear</a> has an extensive background in education-related technology, particularly free and open software. <strong>/Editor note</strong>]"
    author: "john mclear"
  - subject: "It's almost undoubtedly a"
    date: 2013-07-05
    body: "It's almost undoubtedly a themed KDM... because why wouldn't you just use the default and put a theme on it, especially when it's so easy with KDE?"
    author: "dmikalova"
  - subject: "I think Gentoo Linux best for"
    date: 2013-07-05
    body: "I think Gentoo Linux best for student"
    author: "Anonymous"
  - subject: "Hah! So cool!"
    date: 2013-07-05
    body: "I'm glad to see Linux in a school environment."
    author: "Andrew"
  - subject: "Interesting article"
    date: 2013-07-05
    body: "An interesting read. Should be made a reference for other schools contemplating the switch."
    author: "Fitzcarraldo"
  - subject: "KDE / Linux in schools"
    date: 2013-07-05
    body: "Brave and admirable step. It's about time the stranglehold and expense of Windows was challenged. This will not only save money, but will teach students that there is more to computers than Windows, and that it's the generic skills that are useful, not the use of microsoft software.\r\nI hope this example spreads, for everyone's sake"
    author: "Ian Drury"
  - subject: "or?"
    date: 2013-07-05
    body: "Or, it could be lightdm with the qt bindings/themes :)"
    author: "Tiao"
  - subject: "glow"
    date: 2013-07-06
    body: "How many students changed the default blue glow around the windows to something nicer (say, black)?"
    author: "Wil Palen"
  - subject: "SIMS client for linux"
    date: 2013-07-07
    body: "I would like to provide this for free. \r\n\r\n[Editor: This offer has been forwarded to a person involved with the KDE Education Project.] "
    author: "Liang"
  - subject: "Reverse engineering configuration, asking on StackExchange"
    date: 2013-07-07
    body: "About propagating documentation: I have recently found about <a href=\"http://devstructure.com/blueprint/#create\">Blueprint</a> configuration management tool that creates configuration by reverse-engineering changes, i.e. automating your \"taking a basic machine, making a change and then going through the dot files to see what had been affected\".\r\n\r\nA question: did you try to use StackExchange (<a href=\"http://unix.stackexchange.com/\">Linux & Unix</a>, ServerFault, SuperUser) to ask questions, in addition to openSUSE and KDE forums?"
    author: "Jakub Nar\u0119bski"
  - subject: "Awesome!"
    date: 2013-07-07
    body: "I was a Windows engineer for many years installing servers on stock trading floors when I was a tech/consultant in the past. I had the opportunity to install some old linux versions as well. Then later on I worked a lot with Mac servers and reminded me very much of Linux but with much limitations. Later in years when I started my business I started working with CentOS and have to say, it blows every server I ever installed out the door. Its free and very secure. I applaud this school for taking the steps to support open source as the corporate OS's are losing its grip on what the users want. I'm also glad to read that the students are embracing Linux. I see it becoming the future as these other OS's lose their place in ranks. Another nice note is, no more viruses. You can lock down the system for what it was installed for which is education. Congrat's"
    author: "BrainScanMedia.com, Inc."
  - subject: "Cool!"
    date: 2013-07-09
    body: "That's cool, using Linux in School."
    author: "LMK"
  - subject: "Wine details?"
    date: 2013-07-31
    body: "Which apps do you use with Wine?  How well do they work?\r\n\r\nAnd which apps would you like to use with Wine, but can't yet because it isn't working well enough?"
    author: "Dan Kegel"
  - subject: "Schools Information Management System!"
    date: 2013-08-01
    body: "<code>If there were a SIMS (Schools Information Management System) client for Linux, converting the whole school could easily have been considered.</code>\r\n\r\nI am suprised that they didn't look at <a href=\"http://schooltool.org/\">SchoolTool</a>\r\nIt is a open-source web based System for tracking students grades, attendance, behavior, information, etc..."
    author: "Elliott Saille"
  - subject: "SIMS project"
    date: 2013-08-02
    body: "Have you thought about a school project to develop a SIMS?  Open it up to ex-students as well, particularly any studying tertiary IT.  A cash prize would be incentive for them."
    author: "Simon Slater"
  - subject: "Scripts on the server"
    date: 2013-08-02
    body: "Can you write something more about settings tuning scripts that you are running on your server? Which KDE options need to be changed to improve performances with NFS?"
    author: "Davide Bettio"
  - subject: "Public Computers"
    date: 2013-09-14
    body: "City Central Library in Stoke on Trent has just stopped its users accessing their USBs because of hacking on their computers. Seems a bit much for whatever was going wrong. I can't see them migrating from Windows 7 unfortunately. Makes them unusable though, as far as I am concerned.\r\n"
    author: "Michael McNeil"
  - subject: "WHSG - Linux"
    date: 2017-04-18
    body: "<p>The Only Way is Open Source - The Big Linux Switch. Available @ https://www.smashwords.com/books/view/718734</p><p>Write up about how the change to Linux happened back in 2012 all the way through to present day (2017) with Linux still in full swing.</p><p>It's an epub file, views fine in calibre (not sure about other e-readers though).</p>"
    author: "J"
---
Around a year ago, a school in the southeast of England, Westcliff High School for Girls Academy (WHSG), began switching its student-facing computers to Linux, with KDE providing the desktop software. The school's Network Manager, <b>Malcolm Moore</b>, contacted us at the time. Now, a year on, he got in touch again to let us know how he and the students find life in a world without Windows.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/p9040687small_0.jpg"  /><br />A room full of Linux at WHSG (<a href="http://www.whsg.info/index.php/extracurricular/it-and-software/open-source-in-action">image by WHSG</a>)</div><b>Stu:</b> Hi Malcolm, thanks for agreeing to the interview. Could you tell us a bit about the school and your role there?

<b>Malcolm:</b> <a href="http://www.whsg.info">Westcliff High School for Girls Academy</a> is a <a href="http://en.wikipedia.org/wiki/Grammar_school">selective Grammar School</a> with a <a href="http://en.wikipedia.org/wiki/Sixth_form">Sixth Form</a> of about three hundred and forty students. It was founded in 1920 as a co-educational school in Victoria Avenue, Southend, and moved to its present site in 1931. Since then the school has grown to its present size of around 1095 girls.

The IT Support department consists of three staff: myself, Paul Antonelli and Jenny Lidbury. My role is that of Network Manager. The IT Support department covers provisioning and support of all IT-related equipment within the school. This includes 200 teacher machines, 400+ student machines, 33 IMacs, 100+ laptops and a few Android tablets. We also support all the multimedia devices such as projectors, interactive whiteboards and TVs, etc.

<b>Stu:</b> Whose idea was it to switch computers over to Linux? What were the reasons for doing so?

<b>Malcolm:</b> We have used Linux as the OS for our Email server, VLE (Virtual Learning Environment) and web site for a while since I had used it before at my previous position in the financial industry. It was my idea to move the students' PCs to Linux as it was becoming increasingly obvious that with the size, cost and complexity of IT increasing seemingly exponentially, ultimately something had to give and professional pride would not let it be the quality of the systems we support. We tested a small set-up of sixty machines and got feedback from the students, adjusted it a bit and then tried again and so on. Once we had gone through this loop a number of times with Red Hat/Fedora and SUSE/openSUSE set-ups and we were satisfied, I put my proposal to the Senior Leadership Team.

The motivation was initially both cost and philosophical, in that, even in an outstanding school, funds are always going to be limited (politicians don't seem to get the saying 'If you think education is expensive, try ignorance'). The cost of using Windows is high but not always obvious, Windows carries a lot of baggage that bumps the cost up considerably over a Linux environment. The philosophical angle was probably the philosophy of pragmatism. We wanted to offer the best IT systems and education possible with the funds available. Money spent on essentially promoting Microsoft Windows and Office to students can be better spent on old-fashioned things like teachers and actual education. Subsequently and fortuitously, the UK government threw out the old ICT syllabus, which was based largely on teaching students how to use Microsoft Office, and told schools to go for a more computer studies-based syllabus, which meant that we were in a position to hit the ground running so to speak.

<b>Stu:</b> Was there any resistance to the idea and how was this overcome?

<b>Malcolm:</b> Surprisingly, very little. The Senior Leadership Team grilled me in two long meetings which was fun! Once you actually take a step back from the misconception that computers = Windows and actually seriously think about it, the pros clearly outweigh the cons. The world is changing very quickly. There is a survey that reports in 2000, 97% of computing devices had Windows installed, but now with tablets and phones, etc., Windows is only on 20% of computing devices, and in the world of big iron, Linux reigns supreme. We specialize in science and engineering and want our students to go on to do great things like start the next Google or collapse the universe at CERN. In those environments, they will certainly need to know Linux.

<b>Stu:</b> What choices did you make for the software and why? Was any new hardware needed? 

<b>Malcolm:</b> We started out with the basic theory that the students had to like the interface, so 'pretty is a feature' was required for the workstations. For IT staff, stability is practically everything for the servers. Whilst I know there are many people who have favorite distributions, I only really know the RPM-based ones. If we had more resources, we could have looked at more, but we only tried Red Hat/Fedora and SUSE/openSUSE combinations. In the end, the SUSE/openSUSE won because of their KDE software support. Firstly, we did not want the change to be too much for students to handle and KDE's Plasma can be made to look very familiar. Secondly, during our testing, we encouraged students to try both KDE Plasma and GNOME. Plasma was by far the winner in terms of user acceptance. [The final software choice was openSUSE 12.2 and Plasma Desktop 4.10 -  Ed]. 
<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/p9040732small_0.jpg" /><br />One of WHSG's desktops (<a href="http://www.whsg.info/index.php/extracurricular/it-and-software/open-source-in-action">image by WHSG</a>)</div>

As far as the workstations go, no new hardware was required. One of our main reasons to go to Linux was that it runs well on older hardware. The usual merry-go-round of replacing 400 student machines every 3 or 4 years is a horrendous cost. Many schools just simply can't afford that in these days of austerity. With the performance we have now, I intend to run these machines until they fall to bits!  I would suggest to anyone however that they make sure they have a good network before embarking on this (see below for specific advice).

<b>Stu:</b> How did the switch-over go? Were there technical problems and how were they overcome? 

<b>Malcolm:</b> The switch-over was done during the summer holiday of 2012. At that point we encountered no significant technical issues, although that isn't to say we didn't have any later!

<b>Stu:</b> Was there any software missing compared to the old systems? 

<b>Malcolm:</b> We currently have students running Linux and staff running Windows 7. If there were a SIMS (Schools Information Management System) client for Linux, converting the whole school could easily have been considered. As it stands, that could have easily been an overreach. Nothing is missing as far as educational software for Linux, but we have retained a couple of Windows applications which we run under WINE so that students with work in progress can move slowly to alternative applications. One thing that is interesting is the use of the Raspberry Pi and the suchlike in schools. The Pi team stated that one advantage of using the Pi is that students can experiment without destroying the school or family PCs. With Linux, students can experiment now. Our ICT department is already teaching programming to students from year 7 [around age 11 - Ed.], and in our environment, the worst thing they can do is crash their own account. Even if they completely destroy their area, it can be restored in minutes and will not affect the next person using the machine.

<b>Stu:</b> Did you contact KDE or openSUSE for assistance? If so, how was the response? 

<b>Malcolm:</b> I have frequently contacted both KDE and openSUSE through the forums and bugzilla sites; both were exceedingly helpful. openSUSE forums can be a bit hostile at times when others think the questions are poorly worded or not well thought out. This, happily, is not the case in KDE forums where everyone has been very polite and helpful. In openSUSE's defense, some of the questions I posted <em>were</em> not done well. However, as I said before, there are only three of us and sometimes RTFM isn't an option. There just aren't enough hours in the day. If I can post something and get an answer even if it seems dumb to others, it is a great help. If we had to learn everything about Linux, this project would never have happened, we would still be RTFM! Despite being called an idiot occasionally, we got good working answers to all our questions, so I can thoroughly recommend the forums even if it is necessary to be a bit thick-skinned at times.

<b>Stu:</b> What do the students, parents and staff think of the change? 

<b>Malcolm:</b> Younger students accept it as normal. Older students can be a little less flexible. There are still a few that are of the view that I can get rid of Microsoft Word when I can pry it from them. Staff are the same (although it is surprisingly not age-related). Some are OK and some hate it. Having said that, an equal number hate Windows 7 and nobody liked Windows 8. I think the basic problem is that Windows XP is a victim of its own success. It works fairly well from a user point of view, it's been around practically forever, and people don't like change, even some students, oddly. Once we decided to go ahead, a special newsletter was sent out to all parents. We probably had less than half a dozen who disagreed, maintaining that learning Office was a more useful skill. Whilst I accept their views, I would argue that an 11 year old student starting with us in September 2014 will probably not reach the job market until 2024 or there about. What will Office 2024 look like? Your guess is as good as mine, but good basic skills and a logical and analytical way of dealing with computers will be good for a lifetime.

<b>Stu:</b> One year on, what worked and what didn't? What would you do differently or advise another school to do differently? 

<b>Malcolm:</b> It would have been nice to say it all worked perfectly, but it didn't. The first half term was terrible. The primary problem was system speed and particularly logging into KDE Plasma. Our tests only had 60 or so machines in use as it was difficult to round up enough students at lunchtime and after school to really thrash the system. Plus while we were testing, we were still having to maintain the 400 student Windows XP machines. The bottom line is that Linux will run well on an old tin box, but if you have <a href="http://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol">LDAP</a> authentication and NFS home directories—as you certainly will have in a school or business environment, you must have a gigabit network. It will run with 100Mb, but it will be an unpleasant experience as we discovered to our cost. To that end, we had to replace about eight switches to bring our whole network up to gigabit everywhere (OK, it was planned to happen anyway, but I would have rather done it at my leisure!). Additionally some things in KDE software do not lend themselves well to having NFS home directories, although I know it has been addressed to some extent in later versions. We now have several scripts that we run on our servers that force some KDE options to take the load of our network Once we had worked all this out and fixed it over the next half term holiday things quieted down a lot thankfully.

<b>Stu:</b> How could KDE make such a switch easier? 

<b>Malcolm:</b> Documentation! You can configure KDE in every way imaginable using the GUI, but admins need to set up defaults for all users. The openSUSE defaults are OK for home or stand-alone users, but they need a bit of adjustment in a school. In the end we did it by taking a basic machine, making a change and then going through the dot files to see what had been affected (this and bothering Ben Cooksley in the KDE forums). It was hard work! The problem here—and I'm not sure there is an easy answer—is that with Linux, and now to some extent with Windows, the technology moves so fast that documentation is out of date before it's printed or even written in some cases.

<b>Stu:</b> Which applications (KDE or otherwise) have been particularly impressive? In which areas are applications lacking? 

<b>Malcolm:</b> With the exception of about half a dozen students who are using GNOME, everybody loves the fact that now they can configure their desktops and applications. Most admins lock down Windows as it is fairly easy for people, particularly students, to butcher Windows.We have taken the view that we want to get back to a PC being a personal computer, so students can configure it any way they like as it gives a sense of ownership of their desktop. We have restrictions on configuring a machine inappropriately or in a way that is detrimental to work. Students generally get one desktop reset before we will 'have words'. Allowing this is a novel idea in schools. In the beginning, some of the desktops were configured to destruction! Now that the novelty has worn off the desktops are more sane, and we haven't had to reset an account back to a more tasteful blue in months. In this respect, it is a great success as students are now taking responsibility for their work environment and how they achieve tasks rather than be told, "Here is a generic Windows and Office. Use that." 

<b>Stu:</b> Any other comments or observations on the experience?

<b>Malcolm:</b> Has it given me sleepless nights, yes. Has it nearly driven me insane, yes. Would I do it again... in an instant!

<b>Stu:</b> Thank you very much - and best of luck in the future!

The example of Westcliff High School for Girls Academy gives us plenty to think about. Linux, with KDE software, can clearly work in such an environment, but there are still challenges in deployment and getting used to a new system. Malcolm's experiences underline the importance of the KDE forums in welcoming and supporting new users to bring free software to ever larger audiences.