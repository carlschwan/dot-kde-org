---
title: "KDE Ships Release Candidate of Applications and Platform 4.12"
date:    2013-11-30
authors:
  - "tsdgeos"
slug:    kde-ships-release-candidate-applications-and-platform-412
comments:
  - subject: "Cannot compile kde 4.11.97"
    date: 2013-12-02
    body: "Hi,\r\n\r\nI'm trying to compile 4.11.97, the 4.12 pre release, but get errors like:\r\n\r\nCould NOT find KDE4Workspace (missing:  KDE4Workspace_CONFIG)\r\n\r\nI see that the package kde-workspace is missing in the list of available packages at:\r\n\r\nftp://ftp.kde.org/pub/kde/unstable/4.11.97/src/\r\n\r\nAnyone aware of this issue.\r\n\r\nStef"
    author: "Stef Bon"
  - subject: "There's no kde-workspace 4.12"
    date: 2013-12-03
    body: "As the text says \"This release does not include Plasma Workspaces, which was frozen for new features in 4.11.x.\" so you have to use kde-workspace 4.11.x"
    author: "tsdgeos"
---
KDE has made available the Release Candidate of the new versions of KDE Applications and Development Platform.

In the past, major releases usually included all <strong>three elements of the complete software family produced by KDE</strong>. This release does not include <strong>Plasma Workspaces</strong>, which was frozen for new features in 4.11.x. In addition, the <strong>Development Platform</strong> has seen only minor changes for a number of releases in anticipation of <a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a>. So this release is mainly about improving and polishing <strong>KDE Applications</strong>.

A partial list of changes in this release can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.12_Feature_Plan">4.12 Feature Plan</a>. A more complete list of the many improvements and changes will be available with the final release in the middle of December.

The 4.12 releases that need a thorough testing in order to improve quality and user experience. A variety of actual users is essential to maintaining high KDE quality, because developers cannot possibly test every configuration. User assistance helps find bugs early so they can be squashed before the final release. Please join the 4.12 team's release effort by installing the Release Candidate and <a href="https://bugs.kde.org/">reporting any bugs</a>. The <a href="http://kde.org/announcements/announce-4.12-rc.php">official announcement</a> has information about how to install the Release Candidate.