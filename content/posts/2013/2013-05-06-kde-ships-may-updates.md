---
title: "KDE ships May updates"
date:    2013-05-06
authors:
  - "kallecarl"
slug:    kde-ships-may-updates
comments:
  - subject: "KDE is the Best!"
    date: 2013-05-07
    body: "KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! \r\nKDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! \r\nKDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! \r\nKDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! \r\nKDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! \r\nKDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! KDE! "
    author: "Zubin Singh Parihar"
  - subject: "Re: KDE is the Best!"
    date: 2013-05-08
    body: "+(1 x 10^(<a href=\"http://en.wikipedia.org/wiki/Graham's_number\">Graham's Number</a>))"
    author: "georgelappies"
  - subject: "The Best...."
    date: 2013-05-09
    body: "THE MOST COMPLETE AND BEAUTIFUL GRAPHIC ENVIRONMENT!!!"
    author: "Elton"
  - subject: "Really Beautiful!"
    date: 2013-05-10
    body: "Beauty of Beauty! Meaning of Beauty is KDE!\r\nHandsome means KDE!\r\n\r\nLove Means KDE!"
    author: "P M Reddy"
  - subject: "Beauty, Love, Handsome means KDE!"
    date: 2013-05-10
    body: "Beauty means KDE\r\nLove means KDE\r\nHandsome means KDE\r\nNature means KDE\r\nAnything you love in the world is KDE"
    author: "P M Reddy"
  - subject: "the Mother"
    date: 2013-05-16
    body: "KDE is the Mother of all BREED (Beautiful, Reliable, Extensible, Excellent, Desktops).\r\n\r\nEnough said."
    author: "Abe"
---
KDE has <a href="http://www.kde.org/announcements/announce-4.10.3.php">released May updates for its Workspaces, Applications and Development Platform</a>. These are the third monthly stabilization updates to the 4.10 series. The 4.10.3 updates bring bugfixes and translation updates on top of the 4.10 release and are recommended updates for everyone running the 4.10 series. It will be a safe and pleasant update for everyone.

The releases include more than 75 recorded bugfixes to Kontact (KDE's Personal Information Management suite), the Window Manager KWin, and others. Changes are listed in KDE's issue tracker; Subversion and Git logs provide a detailed list of changes. Links to these and installation information are available in the <a href="http://www.kde.org/announcements/announce-4.10.3.php">release announcement</a>.