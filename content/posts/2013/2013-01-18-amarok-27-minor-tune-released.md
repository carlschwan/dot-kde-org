---
title: "Amarok 2.7 \"A Minor Tune\" released!"
date:    2013-01-18
authors:
  - "Mamarok"
slug:    amarok-27-minor-tune-released
comments:
  - subject: "Good Work!"
    date: 2013-01-19
    body: "From what I read here Amarok sounds very polished. A mayor step forward. I am quite enthusiastic about this release. However, there is one thing which concerns me: The (Preliminary) Nepomuk Plug-in. Or rather one detail about it.\r\n\r\nI have been waiting for Nepomuk support in Amarok for a long time. It is logical to integrate Nepomuk in Amarok, since Nepomuk is a system wide technology.\r\n\r\nIt concerns me that all files scanned by Nepomuk are displayed separately in a \"Nepomuk Collection\" (according to <a href=\"http://userbase.kde.org/Amarok/Manual/Organization/Collection/NepomukCollection\">userbase</a>). Why is that so? I don't see any point in separating files scanned by Nepomuk from the ones scanned by Amarok's Collection Scanner. In fact, I don't see the point of sorting music based on technology; it should be based on categories such as the genre of music. Or whether it is on my hard drive, on a flash device or on a CD. That makes more sense.\r\n\r\nSo, if you could think this over, I would appreciate it.\r\n\r\nKeep up the good work!"
    author: "Burke"
---
A New Year, a new Amarok! The Amarok Team is proud to present the new Amarok 2.7, codenamed "A Minor Tune".

This version comes with the following brand new major features, developed during last year's Google Summer of Code:
<ul style="list-style: none;">
<li><strong><a href="http://userbase.kde.org/Amarok/Manual/Organization/Collection/NepomukCollection">Preliminary version of the Nepomuk Collection plug-in</a></strong></li>
<li><strong><a href="http://strohel.blogspot.com/2012/12/amarok-27-beta-to-be-released-soon-try.html">Statistics synchronization between collections and Last.fm</a></strong</li>
</ul>

While originally scheduled before the end of 2012, our code monkeys enjoyed fixing bugs during the holiday period. So we delayed the release a few weeks to let them shine. It turned out great:
<ul style="list-style: none;">
<li><strong>The File Browser's behavior and appearance was greatly improved.</strong></li>
<li><strong>Audio CD playback was resurrected </strong> (at least with the recent phonon-gstreamer).</li>
</ul>

We weren't lazy even before this. This release comes with an impressive number of bug fixes: a total of over 470 bugs were closed since the 2.6 release, of which exactly 100 are direct bug fixes (with a commit link). Over 30 feature requests were granted directly or indirectly as well.

We are also proud to ship a completely updated handbook, with updated screenshots and help pages for the new features. In this context, our thanks goes to the Google Code-In students who helped with updating the handbook. Several Code-In students helped test Amarok extensively, both with the 2.7 beta1 release and the development version. The students also helped move two old wiki instances to a new server, verifying the articles to be currently relevant with working links. In total, 10 students worked on a total of 47 tasks for Amarok. Great work indeed! 

For more information about the changes with a detailed ChangeLog as well as download links, please refer to the release article on the <a href="http://amarok.kde.org">Amarok website</a>.