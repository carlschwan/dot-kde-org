---
title: "KDE Commit-Digest for 13th January 2013"
date:    2013-01-26
authors:
  - "mrybczyn"
slug:    kde-commit-digest-13th-january-2013
---
In <a href="http://commit-digest.org/issues/2013-01-13/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/KWin">KWin</a> adds "visible" property on EffectWindow needed by various effects and improves resolving whether a window is on local machine</li>
<li>In <a href="http://pim.kde.org/">KDE-PIM</a>, Facebook resource is now installed by default</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> gets a new tool to automatically fix trivial errors in source files</li>
<li><a href="http://amarok.kde.org/">Amarok</a> resurrects Audio CD playback, at least with phonon-gstreamer</li>
<li><a href="http://www.kde.org/applications/multimedia/juk/">Juk</a> supports reading/writing Ogg Opus tags</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> migrates to Nepomuk2</li>
<li><a href="http://extragear.kde.org/apps/kipi/">Kipi</a> sees work on the infrastructure to upload to galleries in Imageshack.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-01-13/">Read the rest of the Digest here</a>.