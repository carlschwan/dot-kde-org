---
title: "KDE Commit-Digest for 1st September 2013"
date:    2013-09-20
authors:
  - "mrybczyn"
slug:    kde-commit-digest-1st-september-2013
---
In <a href="http://commit-digest.org/issues/2013-09-01/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://community.kde.org/Frameworks">KDE Frameworks</a>, a new utility function can paint icon overlays; a basic declarative (QML) interface for <a href="http://solid.kde.org/">Solid</a> is added as well as support for keyboards and pointers in udev backend; more porting effort in progress</li>
<li>All <a href="http://marble.kde.org/">Marble</a> plugins are ported to Qt5</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> does not allow items to be indexed in parallel: the CPU usage for virtuoso is too high</li>
<li>in <a href="http://uml.sourceforge.net/">Umbrello</a>, associations can now be drawn with different layouts</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds support for choosing the composite op for Indirect Painting mode</li>
<li><a href="http://www.kde.org/applications/games/knetwalk/">Knetwalk</a> is ported to QML and adds an option to show the solution.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-09-01/">Read the rest of the Digest here</a>.