---
title: "KDE Commit-Digest for 20th October 2013"
date:    2013-11-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-20th-october-2013
---
In <a href="http://commit-digest.org/issues/2013-10-20/">this week's KDE Commit-Digest</a>:

<ul><li>Improvements to KPasswordDialog</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> adds File Extractor for the binary MS-Office formats (doc, xls, ppt)</li>
<li><a href="http://utils.kde.org/projects/kgpg/">KGPG</a> shows more information</li>
<li><a href="http://okular.org/">Okular</a> improves searching code</li>
<li>Multiple modules re-enabled in <a href="https://community.kde.org/Frameworks">KDE Frameworks 5</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-10-20/">Read the rest of the Digest here</a>.