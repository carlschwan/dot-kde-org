---
title: "Open Hardware for KDE"
date:    2013-12-25
authors:
  - "kallecarl"
slug:    open-hardware-kde
comments:
  - subject: "universities"
    date: 2014-01-23
    body: "Just a comment, hope not so naive: open hardware is something I think many professors/students would value. Have you had any contact with universities/schools?"
    author: "Phorious"
  - subject: "education"
    date: 2014-01-23
    body: "Yes, there is quite a bit of interest from educational groups. We're talking to some now and they tend to see the value immediately, however the decision making is not swift and they tend to purchase for future class cycles. Getting Improv and such hardware into schools will take time, though we're working on it. \r\n\r\nThis is another way Improv can be really important for KDE, by being an inroad into such teaching environments and getting Free software that includes Qt and KDE technology in front of young people."
    author: "aseigo"
  - subject: "Ordering from outside Europe/US"
    date: 2014-01-23
    body: "Any news on when the Make Play Live/Vault store will be taking purchases from countries outside Europe or the US? I would be happy to pay now and get an Improv when they start shipping in a couple of months."
    author: "os"
  - subject: "In Europe you don't invalidate your warranty"
    date: 2014-01-23
    body: "In Europe we legally have the ability to install new or different software on our devices without invalidating the warranty. \r\n\r\nOnly if it can be proved that the software you installed caused the damage would your warranty be invalidated, such as if due to poor power management you overheated and damaged the device. The logic is the same as if the device stops working due to a manufacturing or design fault it is covered by the warranty but if you drop your device in the toilet it wouldn't be as the damage was caused by you.\r\n\r\nMatija \u0160uklje and Carlo Piana wrote a good <a href=\"http://matija.suklje.name/rooting-and-flashing-your-device-does-not-void-the-warranty-in-eu\">blog post</a> on this."
    author: "sealne"
  - subject: "Availability in other areas"
    date: 2014-01-23
    body: "Possibilities in other areas are being explored. In some places, this will be straightforward. There just needs to be a known local resource (individual or company) that can receive bulk shipments, assemble feature boards and CPU cards, package and send individual orders.\r\n\r\nIn some countries, there are import restrictions that promote local manufacturing. That's a good thing, and Improv openness and cooperation supports it. However, it takes longer to set up than sending assembled devices.\r\n\r\nWhere would you want Improvs to be offered?\r\n\r\nIf you are interested in bringing Improvs to somewhere outside North America or Europe, please contact us through <a href=\"http://www.vaultechnology.com/store/\">the Improv Store</a> or on the <a href=\"http://discuss.makeplaylive.com/\">Improv forum</a>."
    author: "kallecarl"
  - subject: "X-Selling as Entertainment Center"
    date: 2014-01-23
    body: "Maybe it could be a great idea to get help from another side: the guys from OpenELEC (or similar projects).\r\nI realy believe that 1 of the reasons of the success of the Raspberry Pi comes from his ability to run XBMC out of the box.\r\n\r\nI would like to contribute one Improv to the OpenELEC-Team but i am not sure about the Allwinner-SoC you used. The i.MX6 is already supported and has better documentation. If you believe, that the Allwinner on Improv could handle it, just drop me a line and i will do my best, to get in contact with those guys."
    author: "Peter Pan"
  - subject: "Education"
    date: 2014-01-24
    body: "Is there any plan on including documentation/manuals/tutorials on developing drivers or hardware using the improv? An open device would make an excellent starting point to walkthrough computer engineering or low level programming; having a defacto guide would make introducing people to it much easier (or at least more straightforward: buy improv, look at this website, follow the walkthrough corresponding to your interests.)"
    author: "Tom13"
  - subject: "XBMC"
    date: 2014-01-24
    body: "There is already some support for XMBC:\r\nhttp://linux-sunxi.org/XBMC\r\nmoreover Allwinner is the only chip to have a prototype reverse engineered - really open - video decoder:\r\nhttp://linux-sunxi.org/Reverse_Engineering/Cedar_Status"
    author: "Diego"
  - subject: "Donate toward a tablet?"
    date: 2014-01-24
    body: "I'd really like to get a tablet that's fully FOSS. However you don't deliver in my region just yet. The only other way to give you money is by donation.\r\n\r\nWhat if I could donate money and eventually cash it in for a tablet? I can't buy one yet but I'd donate $15-20 a month if that means that when you can finally deliver to India, my donations can count towards the cost of a device."
    author: "Nick"
  - subject: "Donate towards device cost"
    date: 2014-01-24
    body: "Thank you for your interest and commitment to FOSS.\r\n\r\nAvailability of the fully open tablet has been purposely kept out of this discussion about open hardware. It will not be introduced publicly until it is a proven design ready for full scale manufacturing. It's not there yet.\r\n\r\nFor this request and any others that are outside the order flow, please <em><strong>contact the Improv team directly</strong></em> through the donation link in the original article above. We're a bunch of regular people who are committed to free and open technology. We will do anything we can to make this hardware available to whoever wants it.\r\n "
    author: "kallecarl"
  - subject: "No Bitcoin, and so much information required..."
    date: 2014-01-24
    body: "It's a pity the donation form is so complex. Would you please setup a way to accept donations in Bitcoin? It makes it very easy for anyone (even w/o a bank account/credit card) and it requires less steps/personal information."
    author: "Fabian Rodriguez"
  - subject: "Bitcoin donations"
    date: 2014-01-24
    body: "I would like to support the project as I really like openhardware and would like to see how different approaches work together. Can you please create an official Bitcoin address to accept also the donations via an open protocol? :)"
    author: "MMatthias"
  - subject: "Bitcoin donations"
    date: 2014-01-24
    body: "Please <em><strong>contact the Improv team directly</strong></em> through the donation link in the original article above. No merchant account has been set up for bitcoin, so please contact us directly to make a donation with Bitcoin."
    author: "kallecarl"
  - subject: "Shipping of already ordered units"
    date: 2014-01-25
    body: "I ordered an Improv early (7 weeks ago) and my question after reading is: Will it be delivered as advertised (late Jaunuary)? Thank you for your adventurous endeavor into the lowlands of hardware makers and vendors."
    author: "Ferdinand Thommes"
  - subject: "Vault does not support payment from India"
    date: 2014-01-25
    body: "I would like to donate from India. However The vault payment page does not have India in its country list."
    author: "srix"
  - subject: "Team contact"
    date: 2014-01-25
    body: "Call me stupid,  but how can we contact the team? The article has a  lot of links but none seems to redirect to a official team page?"
    author: "MMatthias"
  - subject: "team contacts"
    date: 2014-01-25
    body: "The confusion is not your fault. We didn't make it easy.\r\n\r\n<a href=\"http://www.vaultechnology.com/contact\">Contact us</a> about purchasing and store issues. Any messages are shared with all relevant team partners. There is also a contact link in the top menu.\r\n\r\n<a href=\"http://makeplaylive.com/#/who-we-are/contacts\">General contact</a> for the Make Play Live Partner network."
    author: "kallecarl"
  - subject: "Other payment options"
    date: 2014-01-25
    body: "I apologize for my oversight.\r\n\r\nThere are now payment options available. Payments through PayPal from India have had some difficulties due to actions taken by the central bank. Perhaps someone in India knows of better solutions. Please let us know."
    author: "kallecarl"
  - subject: "shipping timeframe"
    date: 2014-01-25
    body: "The thanks go mainly to Aaron Seigo and his intrepid quest for openness and freedom. Of course others have played major parts in the drama, while still others have been cheering more-or-less from the sidelines.\r\n\r\nNo one is more eager than the people on the team to get Improvs into people's hands. The first Improv order was placed on November 25, 2013. When they first became available, there was a reasonable expectation that the working and fully sourced devices could be shipped within 2 months. That was overly ambitious. There continue to be challenges and obstacles.\r\n\r\nThree things about this:\r\nAaron and the Improv team are on top of the hardest parts of the project. There is no technical risk; Improv is working and component availability is assured. Logistics issues are handled. Money is now the focus. It would be an easy thing to take investor money and give them a piece of the action and some control. We don't want to do that; Improv is a grass roots project. \r\n\r\nWe believe that there are enough people who understand the value of openness to the extent that there will be sufficient grass roots financial support. After all, the unproven Ubuntu Edge project concept got commitments of more than $12 million in one month. Surely KDE and the wider open technology community can raise 1/100th of that amount. In fact, Improv will directly benefit other open technology projects--such as Ubuntu Edge--by significantly reducing hardware risk and time to market.\r\n\r\nSecond, the people on the Improv team are committed to the success of the project. Compared to the hardships already overcome, the rest of what needs to be done is a walk in the park. \r\n\r\nThird, this undertaking has a high degree of difficulty. Large, well-financed, well-known companies are having a hard time breaking the Apple/Google lock on the market. The current Improv efforts are the most visible publicly, but they are far, far from the most difficult things that Aaron and the Improv Team have already overcome.\r\n\r\nAll money collected is escrowed. Anyone can get their payment back in full by asking. We prefer that people stick with the project, and share the commitment to it. Buy another one to give to someone less fortunate. Donate. Tell people about the issues of open hardware. Blog. Seeing this kind of support is highly motivational to the Team. But be clear that the Team intends to succeed.\r\n\r\nAs soon as there is definite information about shipping timeframes (within the next few days), everyone who has purchased Improvs will be notified."
    author: "kallecarl"
  - subject: "How free is this device?"
    date: 2014-01-26
    body: "Hi! :)\r\n\r\nI really-really want to help such project succeed, by spreading, financing and overall contributing to it.\r\n\r\nMy question is: In terms of software and hardware freedom, how does Improv's current status and future goals compare to other similiar projects, such as:\r\n\r\n<ul>\r\n  <li>Raspberry Pi</li>\r\n  <li>BeagleBoard</li>\r\n  <li><a href=\"https://www.olimex.com/\">OLinuXino</a></li>\r\n</ul>\r\n\r\nMaybe you can think of other such projects worthy of mentioning.\r\n\r\nLike Improv, all of the above work with a GNU operating systems.  Like Improv, some of them, like BeagleBoard and OLinuXino, have the board schematics published.\r\n\r\nUnfortunately, like Improv, the most important parts are \"black boxes\", such as the processor.  Like Improv, those devices require non-free software to fully function.  For example:\r\n\r\n<ul>\r\n  <li>All of them require non-free software for video and GPU to work</li>\r\n  <li>The WiFi of OLinuXino requires non-free software</li>\r\n  <li>Raspberry Pi can't even start without non-free software!</li>\r\n</ul>\r\n\r\nI'm very sure Improv requires non-free software for the GPU.  Is this the case?  Does anything else requre non-free software?\r\n\r\nCan I use Improv with a fully free GNU operating system, including a free Linux kernel, such as Linux-libre or the kernels Debian provide?  If I can, what else will not?\r\n\r\nWhen describing Improv, these things should be clearly explained, if the goal is a device that provides freedom.  Is this the goal?\r\n\r\nWhat's mentioned in the site is <strong>very vague</strong> and unspecific.  I really wish these things are clearly explained when presenting Improv, such as here and in the website.  Don't you agree?\r\n\r\nAll good to you! :)"
    author: "Martin of LibTec"
  - subject: "Re: How free is this device"
    date: 2014-01-26
    body: "\"Unfortunately, like Improv, the most important parts are \"black boxes\", such as the processor\"\r\n\r\nThere is no way that will ever change until there are successful projects that are aiming in that direction. \"Successful project\" implies working with what we have right now and moving forward incrementally. Sometimes we expect 100% solutions and, in doing so, avoid the 98% solution that can over time deliver 100%.\r\n\r\n\"I'm very sure Improv requires non-free software for the GPU. Is this the case?\"\r\n\r\nOnly to have *hardware accelerated* graphics do you need to use non-free software. If you can live with a non-accelerated framebuffer, you can go that way.\r\n\r\n\"if the goal is a device that provides freedom\"\r\n\r\nThat's the goal, yes. However, there are several dimensions to \"freedom\" in this context. Those include:\r\n\r\n* open hardware\r\n* Free software\r\n* open community development processes\r\n* the ability for others to realistically build on the platform\r\n\r\nFor products like BeagleBoard and OLinuXino (which you mentioned), they have schematics, sure ... but are *you* able to take those and start producing a new product tomorrow? That's one of the challenges faced: unless you have significant resources and expertise in dealing with the manufacturing side, it isn't going to happen and those schematics are ~useless. They are nice to have, much better than not, and those with the connections, resources and expertise can make use of them.\r\n\r\nWe'd like to empower people who don't have those resources and expertise to start building things, and not just living-room projects but actual products.\r\n\r\nAside from that, most of the boards developed out there have little to no transparency in the software development process. With Improv, we've done all of that in the open and continue to do so. People are welcome to participate, and they do.\r\n\r\nThe hardware freedom aspect is the hardest nut to crack and we've gone as far as we can with this iteration. Future iterations, should there be any (which depends largely on these earlier steps finding enough support), can take further steps in that direction. This will be the slowest and hardest path, but we can make it with support.\r\n\r\nWhile working towards that, there are huge open green fields of challenges that simply are not being addressed, such as the above noted issues.\r\n\r\nIf you are happy with the status quo of closed development, proprietary layers of icing and no path to manufacturing for the \"mere mortal\" then Improv has little to offer you. If you think development of device software should be open, that full software stacks should be provided, that don't have proprietary icing that gets in the way of making a real device, or that more people should have the ability to take an idea through to manufacturing, then Improv is the only thing going right now.\r\n"
    author: "aseigo"
  - subject: "Re: How free is this device"
    date: 2014-01-26
    body: "Thank you for your answer! :)\r\n\r\nI think you are jumping to conclusion for some of my statements and questions.  I'm not blaming Improv developers for processors which are black boxes.  I'm stating facts which aren't made clear in Improv descriptions.  I want them to be clear. If your goal is freedom, then you should tell people about the current issues. If you hide them, then it seems like you don't care about resolving them. In order for issues to be settled, they should be at least be mentioned and discussed.\r\n\r\nI wish this discussion to be productive .  I want know the current status and goals of Improv.\r\n\r\nI can live without hardware accelerated graphics.  Does anything else require non-free software to work?  Things like booting, video, audio, SATA, USB, ethernet, pins, or anything else.\r\n\r\nCan I use Improv with a fully free GNU operating system including a free Linux kernel, such as Linux-libre or the kernels Debian provide?  If I can, what will not apart from hardware accelerated graphics?\r\n\r\nTo end on a positive note. It seems to me that Improv is the most freedom oriented project of this type. But please, answer my questions. :)"
    author: "Martin of LibTec"
  - subject: "Thanks for the overview"
    date: 2014-01-26
    body: "Thanks for letting people, who ordered already, know what they are up to. I am happy with my order, just wanted a timeframe. Keep up the good work."
    author: "Ferdinand Thommes"
  - subject: "foo"
    date: 2014-01-29
    body: "Sounds like the bootloader is free:\r\n\r\nhttp://aseigo.blogspot.com.au/2013/11/introducing-improv.html\r\n\r\nSATA devices (hard drives) contain non-free firmware:\r\n\r\nhttp://spritesmods.com/?art=hddhack\r\nhttp://www.bunniestudios.com/blog/?p=3554\r\n\r\nSame with many of the chips, at least until OpenCores/Qi Hardware takes over the hardware industry."
    author: "foo"
  - subject: "Working with others?"
    date: 2014-01-29
    body: "Are you working with others in this space? For example:\r\n\r\nhttp://en.qi-hardware.com/\r\nhttp://milkymist.org/\r\nhttp://www.bunniestudios.com/blog/?p=3265\r\nhttp://opencores.org/\r\nhttp://openrisc.net/\r\nhttp://altusmetrum.org/\r\nhttps://en.wikipedia.org/wiki/List_of_open-source_hardware_projects\r\n\r\nI would really like to see a larger organisation like Debian for the hardware industry; non-profit, open to participation, everything public, taking upstream hardware designs, building hardware, distributing and selling it and giving back to upstream communities."
    author: "foo"
  - subject: "x86 Tablet"
    date: 2014-03-26
    body: "Wouldn't it be better, to make an tablet with x86 technologie (maybe like an Acer Iconia W510)? \r\nI think it could not be absolutely open but all linux programs could run on it?\r\nOr are there other reasons/problems?"
    author: "Karsten"
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/OppositeofOpen.png" /></div>
From its beginning, KDE has been a leader in innovation in free (libre) and open source software (FLOSS), but there is a threat to that leadership in one of the fastest growing areas of technology. The advantages of free and open development and use are clear for software; now closed and proprietary strategies have become standard in other kinds of technology. The need for technology freedom has moved from software to other more corporate-controllable areas—notably hardware and the Internet.

As was the case when KDE started, community-developed, freedom-oriented technology is necessary to break the stranglehold of large companies that are more committed to managers and investors than to users. But this won’t be easy and it can’t be left to a few people. The entire KDE Community has a stake in the outcome. For that matter, this should be a concern to anyone who develops free and open software, anyone who uses it, anyone who benefits from it. And that includes just about everyone using technology today.

<a href="http://aseigo.blogspot.com/2013/11/introducing-improv.html">New hardware has been announced</a> that addresses the need for openness beyond software. Community help is needed to support a generous, far-sighted open hardware project involving mostly KDE people and certainly following KDE principles. Please consider <a href="http://www.vaultechnology.com/store/donate">contributing financially to open hardware for KDE</a>.

More of the story follows...

<h2>The threat of proprietary & closed</h2>
The Internet is under threat from companies that seek unfair leverage with their massive investments...investments, by the way, that are already well compensated. The nature of these companies is such that every possible means must be used to extract value.

The <a href="http://techcrunch.com/2013/01/05/the-fifth-horsemen-of-tech-samsung/">digital hegemony</a> of several U.S. companies (Amazon, Apple, Facebook, and Google) plus Samsung dominate technology. All of these companies depend to a great extent on free and open software. Microsoft tries (and fails) to stay in this league with such schemes as using its monopoly position to force conditioned users to adopt Windows 8—a mobile phone GUI blown up to a touch interface on a 19" monitor, positioned by clever marketers as "platform convergence".

What these companies are doing is not wrong; it’s the way most things work these days.

KDE’s leadership is an opportunity to extend free and open technology, providing creative minds unlimited room to innovate. Mainstream tech companies try to do this without disrupting their profits or stock prices. We are fortunate to have such freedom.  

<h2>Plasma Active</h2>
Nine years ago, KDE started planning for a shared technology base for all types of computers. In September 2011, Plasma Active <a href="http://kde.org/announcements/plasma-active-one/">was released</a>. It shared almost all the underlying code base of the other Plasma Workspaces, along with an innovative user interface specifically designed for tablets and the way they are used. KDE quietly offered platform convergence well before Microsoft or Canonical <a href="https://www.linux.com/news/embedded-mobile/mobile-linux/702049-ubuntu-for-tablets-joins-canonicals-convergence-crusade">jumped on the bandwagon</a>.

Plasma Active fits well with KDE’s original goal. As Matthias Ettrich wrote in the announcement of KDE:
<blockquote>
"The idea is to create a GUI for an ENDUSER" and "IMHO a GUI should offer a complete, graphical environment. It should allow users to do everyday tasks with it, like starting applications, reading mail, configuring the desktop, editing some files, delete some files, look at some pictures, etc. All parts must fit together and work together."
</blockquote>

Plasma Active is free and open software, readily available to install on any tablet. But it has been installed on only a few types of tablets, and requires higher than average technical know-how to install and maintain.

<strong><big>Why?</big></strong>

Virtually all tablets on the market have either Google’s Android operating system or Apple’s iOS. Neither is truly free and open. Apple technology is closed and proprietary...Apple’s business model. Android is 23% open according to <a href="http://www.visionmobile.com/blog/2011/12/infographic-the-open-governance-index/">VisionMobile</a>. Installing a different operating system and user interface means violating warranty terms. In addition, there is no standard version of the Android operating system even with the same version number. These operating systems and user interface designs are controlled by Apple, Google and Samsung (which sells approximately 40% of all Android devices). These companies have no interest in making their hardware run KDE software. In fact, doing so would be contrary to the fundamental purpose of such enterprises.

The environment for Plasma Active is far different (and more restricted) than that for other KDE software. With any commercially available desktop or laptop, it is simple to install and run KDE and other free and open software. While there may be some occasional hassles with wireless or graphics, those are easily overcome. Plasma Active comes standard on the open hardware platform called Improv.

<strong><big>Software can’t be free and open if its hardware is closed and proprietary. Improv is as open and free as possible.</big></strong>

<h2>The tablet market</h2>
In October 2013, Gartner reported that global tablet sales would grow 53.4% for the year, and PC shipments would be down over 11% from the previous year. By 2015, tablets and PCs will sell about the same level.

Users will continue to want the kind of software KDE provides for traditional PCs, for several reasons. (<a href="https://conf.kde.org/en/Akademy2013/public/events/17">Jos Poortvliet’s presentation at Akademy 2013</a> has some background.) KDE is viable for the foreseeable future...in the desktop and laptop space. But not for tablets, the fastest growing and highly visible personal computing segment.

Several free and open projects have been started to address the need for alternatives to the Android/iOS market dominance in tablets and other devices. Those projects have faced difficulties that point to the daunting nature of challenges to the Google, Samsung and Apple mobile oligopoly. Other projects such as CyanogenMod have chosen the venture capital route to try and compete. The fundraising goals are substantial:
<ul>
<li>Jolla – €200 million</li>
<li>CyanogenMod - $30 million</li>
<li>Ubuntu Edge – projected a requirement for $32 million</li>
<li>Tizen – multimillion dollar project sponsored by the <a href="http://www.linuxfoundation.org/">Linux Foundation</a> and supported by Intel and Samsung</li>
</ul>

<strong><big>Where do these projects stand?</big></strong>

Jolla began offering a smartphone in Finland at the beginning of December 2013. Their tablet operating system has been exhibited but is not commercially available. A <a href="http://www.forbes.com/sites/ewanspence/2014/01/12/jolla-review-some-rough-edges-but-this-linux-smartphone-shows-promise/">mainstream journalist</a> reports that the Jolla smartphone is a "work in progress" that still has some rough edges, and refers to the 'beta' nature of the handset and software.

Prominent venture capitalists have <a href="http://techcrunch.com/2013/12/19/cyanogenmod-quickly-raises-another-23-million-led-by-andreessen-horowitz/">made substantial investments in CyanogenMod</a>. So at least for the moment CyanogenMod is doing fine. They will have to capture major market share to satisfy venture capital investors...time will tell. This professional investment establishes a substantial value for CyanogenMod as a company and hints at the attractiveness of the device market. A market in which there's a danger of KDE being irrelevant. 

Canonical tried to crowdfund a smartphone to round out their converged computing initiative. Against a <a href="http://techcrunch.com/2013/08/22/edge-crowdfunding-fail/">goal of $32 million, there were commitments of about $12 million</a>. Canonical hinted at backing from major hardware suppliers, but this news was light on detail. 

Samsung was expected to launch a <a href="http://gadgets.ndtv.com/mobiles/news/samsung-to-reportedly-launch-first-tizen-phone-at-mwc-2014-ui-screenshots-leak-461071">Tizen phone at Mobile World Congress</a> in February. Now it appears that Tizen will not challenge Android and iOS this year after all. A Samsung switch to Tizen would be a blow to Android, but it would be good for Samsung’s already rich bottom line. And would further entrench the oligopoly.

According to the tech news site Gigaom, both <a href="http://gigaom.com/2014/01/17/major-setbacks-for-two-new-smartphone-oss-tizen-and-ubuntu-touch/">Tizen and Ubuntu Touch have been set back</a>. However with its substantial, prestigious backing, Tizen is almost certain of being successful.

All of these projects are associated to some degree with free and open software; their funding experiences—successful or not—indicate the potential value of the device industry. None of the organizations promise the degree of freedom and openness typical of KDE.

<h2>"The KDE Tablet"</h2>
Several years ago, KDE developers confronted 2 questions:
<ul>
<li>How can we ensure that KDE software is relevant to computer users today and tomorrow? KDE development teams are addressing this in various ways.</li>
<li>However, without proper hardware, some kinds of software development are not possible (for example, Qt on Android). What hackable ARM-based hardware exists that supports KDE software out of the box?</li>
</ul>

<strong><big>The answer was "NONE".</big></strong>

So in early 2012, Aaron Seigo announced the Spark (later renamed "Vivaldi") tablet, which would be produced by the Make Play Live (MPL) project (comprised mostly of people and companies associated with KDE). It would make the necessary hardware available.

Many readers will be familiar with <a href="http://lwn.net/Articles/504865/">the background</a>. Plucky Aaron and his MPL team have faced <a href="http://www.tgdaily.com/mobility-features/66283-kde-devs-delay-vivaldi-linux-tablet">significant challenges</a>. One of the most difficult things to overcome has been the nonchalance of hardware suppliers about open source licensing. In addition, suppliers changed components without notice or consultation. In short, it has been an ongoing battle to produce hardware that would run Plasma Active out of the box.

In fact, Aaron and his small hardware development team were forced to engineer hardware from scratch. According to Aaron, there will be an open hardware tablet; it’s a question of when it will be available.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ImprovSideView.png" /></div>
<h2>Improv</h2>
In the mean time, the efforts to produce an open hardware tablet revealed a need for general hardware development expertise for free and open projects. The Vivaldi lessons could be applied more broadly to all manner of hardware development.

Out of this realization, the MPL hardware development team created <a href="http://www.linux-magazine.com/Online/Features/MakePlayLive-Releases-New-Single-Board-System">Improv</a>.

Improv has two parts:
<ol>
<li>An interchangeable card with a dual core 1 GHz ARM processor, 1 GB memory, 4GB NAND flash storage, Micro SD card reader,</li>
<li>The standard connector on this card plugs into a feature board that provides access to I/O functions, including USB, HDMI, SATA, VGA, and a 44 pin DIL with a range of I/O possibilities.</li>
</ol>

Improv hardware drawings are open and readily available, software is covered by free and open source licenses, and interfaces are well-documented. In other words, Improv is open hardware, as open as it can be given that all graphics processing units (GPU) are closed and proprietary.

More information and detailed specifications are available at <a href="http://makeplaylive.com/#/open-hardware/improv/discover">the MakePlayLive website</a>. Improv comes with the <a href="http://merproject.org/">Mer operating system</a>, the lean Core Linux distribution that is a direct descendent of MeeGo. <a href="http://makeplaylive.com/#/open-hardware/improv/customize">Additional software configurations</a> are available, in fact encouraged.

Improv has been designed, prototyped, tested and retested. It can’t be bricked by installing other software or experimenting with configurations; there's no need to root the device. Concepts prototyped on Improv can be turned into complete, custom products using the same hardware.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/improvPMC.jpg" /><br />Improv & Plasma Media Center</div>
Improv is done. It’s ready. In typical KDE fashion, Improv was accomplished while others were saying what they were gonna do.

Aaron said this about the Improv:
<blockquote>
Improv is hardware produced *for* free software rather than hardware that *happens to run* free software. It supports a range of software from a standard modern Linux user space all the way up to a full featured desktop. Openness for hardware and software is the goal rather than an accident or a market result.
</blockquote>
<blockquote>
For the KDE Community—KDE software on the device is just part of the picture. The device itself is a gift to KDE. We made Improv so we could have such a device for KDE.
</blockquote>
<blockquote>
Improv is a hardware template, a starting point for new products without requiring the resources of a large company with an in-house hardware team. Use it at home for a personal server or other project. It’s perfect in a school setting for education. But it can also be used to create entirely new products, experiment and prototype, and manufacture if there is demand. Improv is designed to grow from idea to finished product, all on the same hardware/software platform.
</blockquote>
<blockquote>
The know-how and manufacturing chain that has been assembled for Improv is available to anyone who wishes to build upon it. Rather than starting from scratch, Improv is a ready-made starting point for product development and creation.
</blockquote>

Improv is a product that can open the doors to the world of ubiquitous, device-centric computing for KDE and other free and open projects. No more waiting for a big vendor to be kind and take our needs into consideration. No more trying to shoehorn KDE software into devices with proprietary lock-in.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/improvKDEWebsite.jpg" /><br />Improv & Konqueror</div>
<h2>I understand...how do I contribute?</h2>
That’s the pressing dilemma. With software, it’s easy for developers to contribute. A lot of people make their first contribution to free and open software with a single patch. Anyone can download the code and work with it. Start small. There's room for many contributors.

Hardware development is different; it involves physical pieces and is done in chunks. For example, board layout with multiple components and complicated routing is a one-person job.

Aaron and the small team have succeeded at creating the hardware. No further contributions are necessary towards its development. Improv works and works well.

However, there is another big difference between hardware and software—cost. Creating software has no out-of-pocket expense beyond the initial investment in a computer. Distributing one more copy of a KDE application has virtually no associated cost. On the other hand, hardware has a direct cost. Designing a printed circuit board is mostly done in software. But there is a cost to prototype and produce each copy of that physical board.

Aaron and a few others have personally paid these development costs. As can be inferred from the budgets mentioned above, Improv hardware development has not come cheap. There are no venture capitalists handing out money on this project. This is not a get-rich-quick scheme. It is a many year project, involving considerable personal sacrifice on behalf of KDE and free/open technology. Improv is based on generosity, not greed.

The team had high expectations that Improv pre-production sales would be enough to cover these expenses. They will eventually, but people want to get their hands on Improvs now. Delivery delays harm the project. 

<h2>Please lend a hand</h2>
Funding is needed for the direct costs associated with manufacturing: electronic parts, feature board assembly and CPU cards.

Hundreds of people have already supported the project by buying an Improv.

<strong><big>You can help...</big></strong>
Consider buying an Improv, even if you don’t plan to play with it. Give it to a student who has just started learning about technology.

Company engineers might use Improv as a platform for building a custom product. It serves well for prototyping, and can mature gracefully to market readiness. Most importantly, Improv can reduce a hardware development schedule by many months with substantial cost savings.

Please consider <a href="http://www.vaultechnology.com/store/donate">donating to the project</a>. Donations will only be used for direct manufacturing costs. Any money contributed beyond the goal of $125,000 will be used to produce Improvs for education.

Improv works. Please help push it from proven-design-ready-for-manufacturing to full production.

<strong><big>Take a stand for digital choice. A stand for what KDE has proven to be successful—free and open wins.</big></strong>