---
title: "Akademy 2013 - The Gatherings"
date:    2013-07-18
authors:
  - "kallecarl"
slug:    akademy-2013-gatherings
---
<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

After the presentations during the first two days of Akademy, people moved into action. Birds of a Feather sessions and workshops provided the opportunity to discuss, plan and hack. Being Akademy, there was also time away from work for relaxation and camaraderie. Akademy attendees, if there is anything missing, please add in comments.

<h3>Translation</h3> 
hosted by <strong>Luigi Toscano</strong>
Having a BoF is great because translators usually don't talk that much. For bigger problems it helps to have face-to-face time. There were 7 translators at the BOF translating a couple of languages. There is an ongoing effort to introduce some common booking functionality on the translation website. A GSoC project is addressing this.

The group discussed the proposed 3 month release cycle. The translation team wants to be able to do all translations offline; they need someone to work on this. They also talked about finding ways to communicate more, and proposed that translators just fix the context for messages (i18nc). They will bring up the point on a public communication channel to raise awareness. They also want to find ways to do more promotion about the way translators work.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDEStudentsMentorsBoFMKBY.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEStudentsMentorsBoFMKBY700.jpg" /></a><br />Mentor BoF<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h3>Promo</h3> 
hosted by <strong>Jure Repinc</strong>, reported by <strong>David Narvaez</strong>
Worked on release notes for the 4.11 releases. Some work has already been done; more was added. Sebas talked about the target audience and importance of release notes. The group discussed various issues with release announcements: reaching different kinds of people; updating the wiki; too much or not enough content; difficulty of compiling the release notes. The group talked about making developers aware of the need for good information, and discussed the possibility of creating videos and guides on how to create release-content-grade promo material for developers. Promo efforts will probably require more time to address all important issues.

<h3>Promo workshop</h3> 
by <strong>Jake Edge</strong>
<a href="http://dot.kde.org/sites/dot.kde.org/files/JakeEdgePromoWkshp.odp">Jake's slides</a> speak for themselves.

<h3>Kubuntu</h3>
The Kubuntu team held a Developer Summit with audio and IRC access for people who are not at Akademy. Major hacking ensued. VLC hackers spoke about the packages they want in the new release.

<h3>Usability</h3>
<strong>Thomas Pfeiffer</strong> and <strong>Jos Poortvliet</strong>
Usability testing with people who had no knowledge of the app under test (apps reviewed: kscreen, kstars, NetworkManager plasmoid). The tests involved a task and a series of questions. They provided important feedback about usability. Two developers and 3 usability people--<strong>Thomas Pfeiffer</strong>, <strong>Lamarque Souza</strong>, <strong>Jan Grulich</strong>, <strong>Björn Balazs</strong>, <strong>Heiko Tietze</strong>--brainstormed about how to improve the usability of the NetworkManager plasmoid. Jan will implement the proposed changes. 

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><embed src="http://www.youtube.com/embed/FjuVND3VAoM" width="420" height="315" allowfullscreen="true"></embed><br />Watch the usability test video</a></div>

<h3>Conflict resolution</h3>
We talked about making sure that the leadership of KDE knows that they are the leadership; they have power and should do their best to keep our channels friendly and positive. We then spent most of the remaining time talking about situations where easy measures are not enough. We will be sending out proposals to mailing lists.

The KDE e.V. Board talked about the next conf.kde.in--how to make it work, how to make it <a href="http://dot.kde.org/2013/03/07/kde-meetup-2013-india">awesomer than the 2013 KDE India Meetup</a>.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/togethernessMKBY700.jpg" /><br />Conflict resolution?<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h3>Fiduciary Licensing Agreement (FLA)</h3>
hosted by <strong>Matija Šuklje</strong> and <strong>Armijn Hemel</strong>
Many people came to <a href="http://dot.kde.org/sites/dot.kde.org/files/Akademy_2013_Matija_%C5%A0uklje_FLA.pdf">find out more about the FLA</a>. Both Matija and Armijn from the FSFE legal team--together with the KDE e.V. Board--answered questions about the FLA. People had the opportunity to sign the FLA; around 10 people did. Cornelius asked that everybody who hasn't done so should sign the FLA, because it gives KDE e.V. more clarity and definition about overall licensing. Projects like VLC were there to discuss their FLA, and to share their <a href="http://www.jbkempf.com/blog/post/2012/How-to-properly-relicense-a-large-open-source-project">experience about re-licensing</a>.

<h3>Jolla and Sailfish OS</h3>
<strong>Jolla</strong> went over their SDK and the controls that Sailfish OS has. They started to code a demo application but weren't able to finish the app in the short time available. The group talked about how to write apps for Jolla and other systems and licenses.

<h3>Frameworks Office Hours</h3>
There's a lot happening with Frameworks in parallel with the Qt Contributors Summit. People got help setting up compilation of KDE Frameworks.

<strong>David Faure</strong> and <strong>Thiago Maceira</strong> talked about the remaining things that should go into QtCore before Qt-5.2 for KDE Frameworks 5 (QCommandLineParser, logging with categories, and natural comparison for sorting strings). It's looking good for all three; there are some things that need to be done in order to move forward with each of them.

<h3>Improving KDE personas</h3>
hosted by <strong>Thomas Pfeiffer</strong>
The goal of this group was to create a basic set of personas for the whole of KDE which most KDE applications would be able to use. App developers could select personas they want to serve. Two people were there from the team of the Linux distribution (LiMuX) used by the Munich government. They will get usage information about their users. 

Personas are useful, and even more so if they are accompanied by concrete usage scenarios and tasks specific to each application. And they are only useful if developers actually use them. There was some discussion about how to spread awareness on usability among developers.
<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/CanoeingJRBYSA700.jpg" /><br />KDE Personas seeing Bilboa the best way (by water)<br /><small><small>by Jonathan Riddell (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h3>KDE Desktop Human Interface Guidelines (HIG)</h3> 
hosted by <strong>Aurélien Gâteau</strong>
The group got some actual work done on the HIG. They analyzed the current state of the Guidelines, and worked on finding a cleaner and better structure for the documentation. The BoF attracted some new contributors who will help get new screenshots that are consistent and follow the Guidelines.

<h3>KDE e.V. Future</h3>
We talked about the future of KDE and got a lot of useful input about areas that need to be considered. No final decisions were made. It will be an ongoing process.

<h3>Frameworks 5 Communication</h3> 
hosted by <strong>Sebastian Kügler</strong>
A communication strategy is needed for the big releases next year: Frameworks 5 and Plasma Workspaces. For each the group looked at target groups and key messages:
<ul>
<li>Frameworks 5 - The intended audience consists of KDE developers, Qt app developers, Qt contributors and tech journalists. Key messages: no need to pull in the whole desktop as a dependency; well maintained; predictable releases; clear contribution channels; vendor neutrality; portability; Frameworks Qt addons (fit well with existing Qt code, easy for Qt developers to use); modular; licensed under the LGPL; useful for real world applications.</li>
<li>Plasma Workspaces - Split into Plasma 2 and Plasma 2.0. Plasma 2 is elegant, organic and intuitive; device specific UI instead of dumbing down to lowest common denominator UI; intended audience includes: users, developers, contributors, mainstream tech journalists. Plasma 2.0 is more detailed and positioned as evolutionary, not revolutionary. Work is proceeding with the concept "Plasma 2 is for you *if* ..."), which involves getting feedback from the expected audience.</li>
</ul>

The Plasma team is also working on a secure QML plasmoid. The private API is done and needs to be tested. It's already technically possible to integrate security measures into Plasma Workspaces before the 2.0 release. There might also be an existing alternative ("QML URL Interceptor"), which could also be used to limit network access (like QNetworkAccessManager).

There were discussions about the intricacies of KPluginFactory with the possibility of getting rid of KService and KSycoca.

<h3>Speech engine</h3> 
hosted by <strong>Peter Grasch</strong>
The group discussed problems and challenges with open source speech recognition. They came up with concrete use-cases to guide further development. Several people expressed interest in joining the project.

<h3>Mentoring program</h3> 
hosted by <strong>Lydia Pintscher</strong> and <strong>Cornelius Schumacher</strong>
There were about 30 students and mentors who have been involved in Google Summer of Code, Google Code-in, Season of KDE, Summer of Code in Space and Outreach Program for Women. Both students and mentors talked about their experiences and discussed possible improvements. It was apparent that many students move on to become an integral part of the KDE community, often becoming mentors themselves more students. There was some discussion about how to deal with difficult students.
<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/IMG_7187_v1.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/IMG_7187_v1_wee.jpg" /></a><br />Mentors (click for larger)<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h3>KDE Edu</h3>
Summary of the state of the project. A lot of work is being done on artikulate and kstars. Two teachers provided insights about what is really required in practice. There was discussion about getting individual, self contained installers for Windows. Some discussion about possible KDE Edu sprint.

<h3>Akonadi</h3>
<strong>Dan Vratil</strong> is taking over as the Akonadi maintainer. The group reviewed some of the more complicated patches that are outstanding and discussed issues to be addressed including server-side change notification filtering and server side change recording, which takes away round trips and extra load on the client side.

<h3>Incubator</h3> 
hosted by <strong>Kévin Ottens</strong>
The group discussed what we want to do to with projects that want to get under the KDE umbrella. How should they be integrated, what these projects should learn while they are going through the incubator, things that KDE can provide (such as infrastructure). The group also wants to look at other free and open community incubators (such as Mozilla, Apache).

<h3>Multimedia</h3>
Tomahawk and Amarok people meeting and working together. Mostly hacking Phonon ("pulling out crappy code")and fixing issues related to gstreamer.

<h3>Special thanks</h3>
Thanks to the many people who contributed to this story. Thanks to <strong>Kenny Duffus</strong> for the idea of a BoF wrap-up session at the end of each day. The wrap-up cannot include everything that happens during the day, because people are working in the hallways, the Coffee Shop, talking over meals or during the get-aways. Expect to see results from this extra-Akademy work over the next year.
<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/IllyaMartinPingPong700MKBY.JPG" /><br />Youth (Kovalevskyy) vs Elder (Klapetek)<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h3>About Akademy 2013 Bilbao, the Basque Country, Spain</h3>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.