---
title: "Krita Support Services Now Available"
date:    2013-04-25
authors:
  - "ingwa"
slug:    krita-support-services-now-available
---
KO GmbH announces extensive support services for Krita, the award-winning graphics application. Krita is an advanced paint application with a complete set of professional paint tools that can handle extremely large images effortlessly. It is particularly well-suited for special effects work in the movie industry. 

Krita has been used in several Hollywood blockbuster movies such as <em><a href="http://en.wikipedia.org/wiki/G.I._Joe:_Retaliation">G.I. Joe 2</a></em> by VFX leaders such as <a href="http://digitaldomain.com/">Digital Domain</a>. It is useful for many different tasks including concept art, matte painting and <a href="http://en.wikipedia.org/wiki/Rotoscoping">rotoscopy</a>. 

Until now Krita has lacked a structured support offering, something that KO GmbH sees as an opportunity. KO GmbH employs several of the lead developers of Krita. Simon Legrand, 3D generalist at <a href="http://www.dneg.com/">Double Negative</a> in London says: "It's a testament to the quality and productivity of Krita that we have been able to use it to such an extent already before the availability of company support for it." Boudewijn Rempt, maintainer of Krita and CTO at KO GmbH adds, "There is a lively community around Krita and we are always working with the needs of our users before our eyes." 

<h2>About Krita Studio</h2>
More information about Krita Studio can be found at: 
 * http://www.kogmbh.com/products.html 
 * http://www.kogmbh.com/support.html

<h2>About Krita</h2>
Krita is an open source, award-winning paint application for artists of all skill levels from amateur to professional. It is used extensively to create stunning art, examples of which can be seen at <a href="http://www.deviantart.com/">deviantART</a> and <a href="http://www.krita.org/">the Krita website</a>. During the past year, there has been a focused effort at making Krita suitable for movie work with such extensions such as support for <a href="http://opencolorio.org/">OpenColorIO</a> color management and special brushes for rotoscopy. 

There are other new features which make Krita work well with other leading VFX industry tools like <a href="http://www.thefoundry.co.uk/products/nuke/">Nuke</a>, <a href="http://www.autodesk.com/products/autodesk-maya/overview">Maya</a> and <a href="http://www.blender.org/">Blender</a>.

Krita is a trademark of the Stichting Krita Foundation. More details about Krita can be downloaded from http://www.krita.org/ or http://www.kogmbh.com/krita. There is an extensive brochure with Krita information at http://www.kogmbh.com/krita/insight.pdf. 

Krita is part of the <a href="http://www.kde.org/">KDE Project</a>, which is one of the largest free and open software communities in the world. The KDE Community produces Plasma Workspaces for desktop, netbook and tablet computers, a Qt-based Development Platform, and many applications.

<h2>About KO GmbH</h2>
KO GmbH provides services around the KDE productivity and creativity suite <a href="http://www.calligra-suite.org/">Calligra</a>, which includes Krita. KO has created office productivity applications for several generations of Nokia smartphones such as the N900 and N9. It has also extended Krita with features for touch-based painting for Intel tablets and Ultrabooks, an application called Krita Sketch that is available free of charge from <a href="http://www.appup.com/app-details/krita-sketch">Intel AppUP</a>.