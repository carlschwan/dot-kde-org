---
title: "KDE Releases Beta of Plasma Workspaces, Applications and Platform 4.11"
date:    2013-06-13
authors:
  - "unormal"
slug:    kde-releases-beta-plasma-workspaces-applications-and-platform-411
comments:
  - subject: "Any news about Akregator2?"
    date: 2013-06-13
    body: "In the feature plan it says that the move to Akregator2 is in progress. With KDE 4.10 the situation was the same, but it did not happen. Is there any news about this? Now want to push it, since another broken PIM-application after Kmail would not be nice. But I am interested to know. Would be also very exiting, since as I read it would include Owncloud news app integration."
    author: "mark"
  - subject: "Items for kwin"
    date: 2013-06-14
    body: "58 \"Done\" items for KWin. Impressive.\r\n\r\n[Troll comment removed. --Editor]"
    author: "Rajendra"
---
Today KDE <a href="http://www.kde.org/announcements/announce-4.11-beta1.php">released the beta of the new 4.11 versions</a> of Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

<div align=center style="padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/411betascreenshot.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/411betascreenshotB.png" /></a><br />Changes to KWalletManager and Okular</div>

The 4.11 releases include many substantial changes and improvements, including:
<ul>
<li>Deeper integration of Qt Quick in Plasma Workspaces</li>
<li>Faster Nepomuk indexing; massive performance optimizations, such as reading data is 6 or more times faster than previously</li>
<li>Kontact improvements—faster indexing, new theme editor, a lot of bug fixes</li>
<li>KWin—many OpenGL improvements, work on an OpenGL 3.1 core context and improved robustness, optimizations aimed at reducing CPU and memory overhead</li>
</ul>
More improvements can be found in <a href="http://techbase.kde.org/Schedules/KDE4/4.11_Feature_Plan">the 4.11 Feature Plan</a>. 

With the large number of changes, the 4.11 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.11 team by installing the beta <a href="https://bugs.kde.org/">and reporting any bugs</a>.

Check out <a href="http://www.kde.org/announcements/announce-4.11-beta1.php">the full announcement</a>.