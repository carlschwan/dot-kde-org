---
title: "KDE Commit-Digest for 23rd June 2013 "
date:    2013-07-29
authors:
  - "mrybczyn"
slug:    kde-commit-digest-23rd-june-2013
---
In <a href="http://commit-digest.org/issues/2013-06-23/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://nepomuk.kde.org/">Nepomuk</a> adds new Indexer for EPub files</li>
<li><a href="http://edu.kde.org/kalgebra/">KAlgebra</a> supports fullscreen mode</li>
<li><a href="http://techbase.kde.org/Projects/Edu/Artikulate">Artikulate</a> gains new language models</li>
<li><a href="http://krita.org/">Krita</a> implements a global alpha lock "Preserve Layer Alpha"</li>
<li>In <a href="http://amarok.kde.org/">Amarok</a>, volume fadeout is now also available for pause</li>
<li>libkface adds a new preprocessing algorithm</li>
<li>Optimizations in Nepomuk, <a href="http://dolphin.kde.org/">Dolphin</a> and <a href="http://rekonq.kde.org/">rekonq</a></li>
<li>GSoC work in Nepomuk and <a href="http://www.digikam.org/">Digikam</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-06-23/">Read the rest of the Digest here</a>.