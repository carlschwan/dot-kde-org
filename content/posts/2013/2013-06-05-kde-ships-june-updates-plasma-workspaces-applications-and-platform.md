---
title: "KDE Ships June Updates to Plasma Workspaces, Applications and Platform"
date:    2013-06-05
authors:
  - "tsdgeos"
slug:    kde-ships-june-updates-plasma-workspaces-applications-and-platform
---
Today KDE released <a href="http://www.kde.org/announcements/announce-4.10.4.php">updates for its Workspaces, Applications and Development Platform</a>. These updates continue the series of monthly stabilization updates to the 4.10 series. 4.10.4 updates bring many bugfixes and translation updates on top of the 4.10 release and are recommended updates for everyone running the 4.10 release series. As this release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone.

The over 50 recorded bugfixes include improvements to the Personal Information Management suite Kontact, the File Manager Dolphin, and others. The changes are listed on KDE's issue tracker. For a detailed list of changes that went into 4.10.4, you can browse the Subversion and Git logs.

To download source code or packages to install, go to the 4.10.4 Info Page. To find out more about the 4.10 versions of KDE Workspaces, Applications and Development Platform, please refer to the 4.10 release notes.
