---
title: "K\u00e9vin Ottens - Akademy 2013 Community Keynote"
date:    2013-06-01
authors:
  - "tcanabrava"
slug:    kévin-ottens-akademy-2013-community-keynote
---
Kévin Ottens is a long-time KDE hacker, known as ervin on IRC and email. He contributes to the KDE Community at large, with a strong emphasis on API design and frameworks architecture. He was instrumental in developing the <a href="http://manifesto.kde.org/">KDE Manifesto</a>, a process that started during Akademy 2012.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ervin_wee.png" /></div>

Kévin has a PhD in computer science which led him to concentrate on ontologies engineering and multi-agent systems. Kévin's job at <a href="http://www.kdab.com/">KDAB</a> includes developing research projects around KDE technologies. He lives in Toulouse, France where he serves as a part-time teacher at the <a href="http://en.univ-toulouse.fr/our-strengths">University</a> he attended.

<h2>Community Keynote - An Open Field of Opportunity</h2>
Our online conversation touched on some of the things that Kévin will be exploring in the <a href="https://conf.kde.org/en/Akademy2013/public/events/48">KDE Community keynote</a> talk during Akademy 2013 in Bilbao (13–19 July).

<strong>Tomaz</strong>: What things do you do that don't involve work or programming? What you do to have fun?

<strong>Kévin</strong>: Well, I work from home so I have to do other stuff to stay sane, most notably rock dancing, yoga (not much nowadays but I've got to pick up again soon) and aikido. Aikido helps a great deal with my own equilibrium these days. I also shoot photos when I find time, but I try to go the minimalist route more than the usual geek "throw more gear at it" approach. And I read... lots. I stopped counting the amount of books I eat in a year

<strong>Tomaz</strong>: There are a few people in the KDE Community who actively try to open doors for other people. KDE people who devote time to help newcomers, including Sandro Andrade, David Faure, Mauricio Piacentini and you.

<strong>Kévin</strong>: Yes, and that's a good thing. It's important to bring newcomers to the doors, (I'm bringing a student over this year again), I've been trying to bring students since I started the University projects. Akademy is important to show them the full KDE experience.

<strong>Tomaz</strong>: I helped some students from Greece, who initially were unfamiliar with KDE development. Now they are quite active in the KDE Community. Giorgos and Antonis Tsiapaliokas, twin brothers who are working on Plasmate.

<strong>Kévin</strong>: I didn't know you helped them get started, I met them at the Plasma sprint in Nuremberg last month.

<strong>Tomaz</strong>: What about your projects with the students in France?

<strong>Kévin</strong>: I have a nicely working setup now and that's thanks to the professors trusting me (Henri Massié and Bernard Cherbonneau). I pick several projects, and let teams of students volunteer on one of the proposed projects. I think it's important that they choose and don't just get an assignment. In fact making a mandatory project assignment prematurely kills the fun. So there's a risk that maybe one day no one will pick what I propose, but it never happened so far.

<strong>Tomaz</strong>: At the University here I tried to do some KDE internships with four students. None of them worked for more than a week, and the university wasn't too helpful either.

<strong>Kévin</strong>: At the University in Toulouse, we ask our students to work in teams. I think that helps to make it work on a longer term. They're not alone and they get peer pressure by being 4 or 5 in the "same boat"

<strong>Tomaz</strong>: Some universities don't trust open source. They block ssh, git, svn ports and other things. Does that happen at your university?

<strong>Kévin</strong>: No, it's very liberal here. The campus network is almost directly connected to the Internet. There's almost no filtering...definitely no port filtering, the admins just make sure the bandwidth is not brought to its knees.

<strong>Tomaz</strong>: That's something wonderful. Can you show some of your students' works?

<strong>Kévin</strong>: For this year, it's in the form of new features for Tomahawk and Calligra. For Calligra, check out the git branches with the name "toulouse" in them.  For Tomahawk, go to https://github.com/ISI-Peasy.

For projects from previous years, take a look at the <a href="https://www.desktopsummit.org/sites/www.desktopsummit.org/files/university-collaboration.pdf">slides</a>[pdf] from my talk on University Collaboration at the Desktop Summit in 2011.

Since 2007, the students have worked on the following projects: umbrello, kplato (now calligra plan), kopete, kscd, ksirk, kapman, kbugbuster, akonadi, kdevelop, mediawiki qt binding, calligra stage, calligra author and tomahawk. Not that all of it made it into official releases of course.

<strong>Tomaz</strong>: It's impressive that you have real-work, kde-work, students-work and free time for dancing, aikido and such. A lot of people claim, “I don't have free time,” when they just do 'real-work' what's the secret?

<strong>Kévin</strong>: Well I don't have free time either :D The secret? I think it is to actively try to eliminate waste. If it's not useful, just don't bother... Which is why I generally walk away from bikeshedding or such. I just refuse to enter anything that would drain my energy in an area I'm not interested in. Saying "no" is quite hard especially in our age of streamed information...the whole "<a href="http://xkcd.com/386/">Someone is wrong on the internet</a>" thing.

That's why I said earlier that aikido is quite important to me. It's not one of those competitive sports (especially in the federation I'm in); it's a lot about working on your own ego. I find it helps with the energy draining events. They're stressful and you enter them mainly because you're fooled by your own ego. It requires constant work. If you're not careful, you can fall in the ego trap again quite easily.

<strong>Tomaz</strong>: Do any of your students stick with OSS programming after the classes are over?

<strong>Kévin</strong>: Some do, it's a minority of course, as some of them get sucked in their job once they get out of the university, and that's ok. If they get a better job just because of those projects even if they don't keep contributing, I'm happy. I think the very first mission is to give the tools to those students to have a nice and fruitful career. But some of them fall in love with the community and are it in their jobs. Or they keep contributing in addition to real-work, sometimes just a couple of years, sometimes longer. Our University program has had three OSS "poster child" students since I started.

<strong>Tomaz</strong>: How long have you been involved with the KDE Community?

<strong>Kévin</strong>: I started in 2003, but I did not attend Kastle (The third KDE contributor meeting; I was still too new of a contributor.) But I attended the first Akademy in 2004, and never missed one since then. I followed Kastle on IRC though, and got my first contributions integrated in the next official release during Kastle in fact.

<strong>Tomaz</strong>: That's amazing. Did you ever believe that there would be hard work during a “convention”?

<strong>Kévin</strong>: Of course :) The whole distributed nature of free software is both a good thing and a limitation, because you reach further away in different cultures, so it makes it easier to be innovative and such. It's a limitation because the throughput is necessarily lower when distributed. That's why I never doubted that hard work could be done in KDE conventions. The limitation is lifted for a short while so the Community is necessarily at full steam.

<strong>Tomaz</strong>: At Akademy 2012, you were honored with an Akademy Award for your work on KDElibs. What's happening now on it?

<strong>Kévin</strong>: It's getting to the next stage. kdelibs was nice, but its development model always forced it to be an "insider tool". In other words, it was practical to use only for people within KDE which is fine, there's great stuff in there. The downside was that it was less attractive for reuse, and so less of an attractor for outside contributions. It was also harder to reuse in the typical "desktop" scenario. 

So the idea is to move from a single "package" to a collection of products; we're making a collection of small frameworks, which are very focused feature-wise. Now it's easier to say "I use Qt and want those extra neat widgets". With kdelibs, you'd either fork a couple of files, or pull in lots of dependencies (even runtime ones). Tomorrow you'd just link to a small KWidgetsAddons lib (for instance). This is a much much better solution for third parties, so we can expect more use from third parties (and in turn get more contributions from them). It's also much much better for the KDE Community to port their software in different contexts because dependencies can be managed with a finer granularity.

<strong>Tomaz</strong>: What are your expectations as a Keynote speaker?  Can you tell us a bit about your talk...without spoiling the fun?

<strong>Kévin</strong>: I expect to not screw it up :D Really though, it's a new kind of exercise for me, so I hope I'll be up to the level of previous keynote speakers. I'll talk a bit of the <a href="http://manifesto.kde.org/">Manifesto</a> as I think it is the beginning of the next stage of KDE's evolution. I had the good fortune to be part of its creation. So I'll cover what I think it means and what doors it opens for our community. The talk will not be very technical and and will show a potential path for the next 5 years roughly. The ideas will be somewhat prospective, a bitof a crystal ball exercise. I might get it all wrong and we'll do something totally different. But for a community keynote, I think it's important to be inspiring and see that we're sitting in an open field full of opportunities.

<strong>Tomaz</strong>: A few years back, GNOME was the biggest “other” desktop. Now it seems that Unity and KDE are dominant. What do you think about this change?

<strong>Kévin</strong>: There's been a lot of fragmentation in the FOSS desktop space. It turns out that KDE is now the biggest (if we trust the numbers... and there are usually large error margins in the studies). I think it's because of our very good engineering culture and because of the cohesion within the community. It's a sign that we're healthy, and I'm glad about that.

<strong>Tomaz</strong>: What would you say to universities that have students who want to go to Akademy or to know more about open source, but the university doesn't have a commitment to openness?

<strong>Kévin</strong>: I would tell students and interested faculty to try it nonetheless. Hijack the system as you can, and build trust. Trust is the important part in my experience. The first year I did much more work than necessary. None of that extra work was used, but it reassured people to know it was there. And then the project outcomes spoke for themselves, and this extra work was not needed in the following years.

<strong>Tomaz</strong>: Anything else?

<strong>Kévin</strong>: For last words I think this will do: I'm honored to have been chosen for the community keynote this year. I've been in love with the KDE Community for almost a third of my life now, I want to show that love on stage.

<h2>Register Now</h2>
<strong><a href="http://akademy2013.kde.org/register">Register</a></strong> now for Akademy 2013. You are encouraged to <strong><a href="http://akademy2013.kde.org/travel">book your travel</a></strong> and <strong><a href="http://akademy2013.kde.org/accommodation">accommodation</a></strong> soon, because <a href="http://akademy2013.kde.org/">Akademy 2013</a> will take place during a busy time in Bilbao (including <a href="http://www.bilbaobbklive.com/2013/en/">BBK Live</a>). With intense involvement from people in the KDE Community, Akademy 2013 promises to provide a strong impetus for continued KDE innovation and industry leadership. 

<h2>Akademy 2013 Bilbao, the Basque Country, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.