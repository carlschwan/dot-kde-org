---
title: "Akademy 2014 - Brno, Czech Republic"
date:    2013-12-19
authors:
  - "kallecarl"
slug:    akademy-2014-brno-czech-republic
---
Where is the next Akademy? In Czech, "KDE je příští Akademy?" as 'kde' means 'where' in Czech.
<a href="http://akademy.kde.org/2014">Akademy 2014</a>, the annual KDE community summit, will take place from <strong>6-12 September in Brno, Czech Republic</strong>.

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Brno2.png" /><br />Brno, Czech Republic</div>
<strong><big>How did your team get involved in Akademy? What are your connections to KDE? Please tell us about yourselves.</big></strong>
We are a group of open and free software enthusiasts. Some are students; others work as software developers, testers or have other roles in various open source projects. We all share one passion—KDE. Some of us work or have worked on KDE as part of our day jobs, some of us contribute in our spare time. Some are KDE users; what would be a better way to start participating in the KDE community than organizing Akademy? Many of us are members of the Fedora KDE SIG—the team responsible for KDE on Fedora. In the past, some of us participated in organizing other events. Bringing the KDE community to Brno has been our dream for a long time. We finally had the opportunity and happily grabbed it.
<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/BrnoTeam2.png" /><br /><strong>The Brno Team</strong><br />Daniel Vrátil, Iveta Šenfeldová, Jan Grulich, Jaroslav Řezník, Jozef Mlích<br />Luigi Toscano, Lukáš Tinkl, Martin Bříza, Martin Holec, Martin Kolman</div>
<strong><big>Why do you want to help organize Akademy? What do you expect?</big></strong>
We see our effort as just another kind of contribution to KDE. As active contributors, we understand the need for community members to meet in person to discuss and plan for the future. From past years, we know that Akademy is not just about software. It's first of all about the people. The KDE community is unique. We want to express our gratitude for people’s hard work by hosting this event, and giving all the great KDE people a way to foster friendships and relationships that shape the community. And thanks to many college and university ICT departments and IT companies located in Brno (and elsewhere in the Czech Republic), we believe that Akademy in Brno is a great opportunity for students, experts and others in the local community to get in touch with KDE people and benefit from KDE’s work. We are also certain that KDE will benefit from all the experiences in Brno—people, culture, hospitality.

<strong><big>Tell us about Brno. Why is it the place for Akademy?</big></strong>
Brno is the second largest city in the Czech Republic. It's the administrative center of the South Moravian region and the Czech capital of judicial authorities: the Constitutional Court, the Supreme Court, the Supreme Administrative Court and others.

Brno is a beautiful city with many historical attractions, such as the Veveří castle from 11th century, the Špilberk castle from 17th century, St. Peter and Paul Cathedral originally built in 11th century and many others. There will be plenty for Akademy attendees to do when they break for leisure.

With 13 universities, 33 IT departments and research facilities, and over 120,000 students, Brno is a center of education and science—a great place to attract new people to KDE. Akademy will take place at the <a href="http://www.feec.vutbr.cz/">Faculty of Electrical Engineering and Communication at Brno University of Technology</a>, about 20 minutes from the city center by public transportation.
<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2014Venue.jpg" /><br />Akademy Venue at Brno University of Technology</div>
<h2>About Akademy</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Intense workshops at the conference bring those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are exploring possibilities involving free and open source technologies.

2014 will see the 12th edition of Akademy, when once again a few hundred Free Software enthusiasts will gather for 2 days of talks and 5 days of workshops and coding sessions. For more information, please contact <a href=" mailto:akademy-team@kde.org">the Akademy team</a>.