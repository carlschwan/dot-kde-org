---
title: "KDE Commit-Digest for 14th April 2013"
date:    2013-04-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-14th-april-2013
---
In <a href="http://commit-digest.org/issues/2013-04-14/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://pim.kde.org/">KDE-PIM</a> extends support for Facebook notifications: whenever the user receives a notification, it's displayed with direct links and an option to mark all notifications as read; work starts on grammar checker</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> expands the References section by adding features to insert hyperlinks, bookmarks and links to bookmarks</li>
<li><a href="http://userbase.kde.org/NetworkManagement">Network Management</a> autoconnects to a VPN when a device gets connected; work in progress on mobile broadband wizard integration</li>
<li><a href="http://userbase.kde.org/Rocs">Rocs</a> adds script engine documentation widget feature</li>
<li>Work on Youtube support in <a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma MediaCenter</a></li>
</ul>

<a href="http://commit-digest.org/issues/2013-04-14/">Read the rest of the Digest here</a>.