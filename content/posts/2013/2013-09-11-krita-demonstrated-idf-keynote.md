---
title: "Krita Demonstrated at IDF Keynote"
date:    2013-09-11
authors:
  - "jospoortvliet"
slug:    krita-demonstrated-idf-keynote
---
KO GmbH today presents Krita Gemini for Windows 8. Krita Gemini is a fusion between Krita Sketch and Krita Desktop: Krita Gemini switches seamlessly between the full-featured desktop/laptop user interface and the sketch interface, which is optimized for tablets.

Inge Wallin, COO at KO GmbH explained:  <em>"The open development process usually employed by KO and supported by Intel allows early community feedback as well as access to the vast repository of code and knowledge in the KDE community, shortening the development cycle and improving results"</em>

KO GmbH has been working closely with Intel on this project. During development, Intel commented, <em>"Krita Gemini is a brilliant example of how developers should utilize the option of a convertible device by using both tablet and desktop mode. The switching between the two modes works seamlessly."</em>.

Intel supported the work on Gemini as a show-off for the new generation of Intel-powered 2-in-1 devices which can switch between desktop and touch mode. There was a live demonstration of Krita Gemini during the keynote by Intel's Douglas Fisher today at IDF 2013.

<h2>Seamless performance</h2>
Boudewijn Rempt, Dan Leinir Turthra Jensen and Arjen Hiemstra from KO GmbH have developed Krita Gemini during the past two months with support from Intel, <a href="https://projects.kde.org/projects/calligra/repository/show?rev=krita-sketchgl-rempt">working in the open</a> on KDE infrastructure.

The work was made easier not only by having experienced developers involved but also by the modular design of Calligra, already capable of providing functionality to the user through different user interfaces. Gemini presents a seamless marriage of a QML based gui with a QWidget based gui, showing a perspective for the form factor switching envisioned for future Plasma Workspaces.

With the right hardware, the switch is fully automatic and truly seamless. Check out the following video in which Timothee Giet uses Krita Gemini:
<iframe width="640" height="480" src="//www.youtube.com/embed/qiSGuCzRZFk?rel=0" frameborder="0" allowfullscreen></iframe>

Krita Gemini includes support for the AVX2 instruction set that was introduced with Intel's new Haswell series of processors, shortening the processing time for many filters and creating a more fluid view port. Part of the development effort was spent on improving the <a href="http://code.compeng.uni-frankfurt.de/projects/vc/">Vc library</a> by Matthias Kretz, adding <a href="http://gitorious.org/vc/ahiemstras-vc/">support for Haswell's AVX2 vector extensions</a> which will be merged into an upcoming release.

<h2>Get it</h2>
Download <a href="http://www.kogmbh.com/download.html#kritagemini">Krita Gemini for free</a> from the KO GmbH website. It works with Windows Vista, 7 and 8.

The team is targeting the work done on Gemini for inclusion in the main Krita branch over the coming months so all Krita users on all platforms can benefit.