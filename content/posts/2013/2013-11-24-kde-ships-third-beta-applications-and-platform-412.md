---
title: "KDE Ships Third Beta of Applications and Platform 4.12"
date:    2013-11-24
authors:
  - "unormal"
slug:    kde-ships-third-beta-applications-and-platform-412
---
KDE has released the third beta of the 4.12 versions of Applications and Development Platform. With API, dependency and feature freezes in place, the focus is now on fixing bugs and further polishing. Your assistance is requested.

In the past, major releases usually included all <strong>three elements of the complete software family produced by KDE</strong>. This release does not include <strong>Plasma Workspaces</strong>, which was frozen for new features in 4.11.x. In addition, the <strong>Development Platform</strong> has seen only minor changes for a number of releases in anticipation of <a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a>. So this release is mainly about improving and polishing <strong>KDE Applications</strong>.

A partial list of improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.12_Feature_Plan">4.12 Feature Plan</a>. A more complete list of the improvements and changes will be available for the final release in the middle of December.

This last beta release needs a thorough testing in order to improve quality and user experience. A variety of actual users is essential to maintaining high KDE quality, because developers cannot possibly test every configuration. User assistance helps find bugs early so they can be squashed before the final release. Please join the 4.12 team's release effort by installing the beta and <a href="https://bugs.kde.org/">reporting any bugs</a>. The <a href="http://www.kde.org/announcements/announce-4.12-beta3.php">official announcement</a> has information about how to install the betas.