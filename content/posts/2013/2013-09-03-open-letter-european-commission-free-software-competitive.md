---
title: "Open Letter to the European Commission: Free Software is competitive"
date:    2013-09-03
authors:
  - "kallecarl"
slug:    open-letter-european-commission-free-software-competitive
comments:
  - subject: "Good point in original article: bundling of proprietary applicat"
    date: 2013-09-03
    body: "While the statement that Android is distributed \"below cost\" applied to the open source part may be problematic. However, the proprietary parts of Android (or the parts that users would perceive as part of Android, like Google Play Store), are distributed \"below cost\" and are used to impose Google's policies on device makers. This is highly worrying: Imagine KDE wanting to bring out a \"KDE on Android\" phone, but not being allowed to install Google Play Store at all if Marble is the default mapping application instead of Google Maps.\r\n\r\nIn conclusion, I think the overall message of the statement by FairSearch is correct, if applied to both open source and proprietary parts of Android."
    author: "Marcus"
  - subject: "I do not agree"
    date: 2013-09-04
    body: "While that's a bit ridiculous that the complaint has been filed by an entity in which Microsoft-Nokia is a prominent member, I do agree with some aspects of the complaint. The complaint is not about Android's openness, rather the opposite: it's about the phone makers being forced to install PROPRIETARY apps and store from Google to sell an Android product.\r\n\u00abBut in reality, Android phone makers who want to include must-have Google apps such as Maps, YouTube or Play are required to pre-load an entire suite of Google mobile services and to give them prominent default placement on the phone\u00bb\r\nSo the compliant here is that Google is dominating advertisement and services market over a closed community open source OS. But the \"open source\" software here is really at the side of the problem; it would be the same if the OS would be closed (and that's why Google doesn't matter releasing Android as open source: they really do care only about advertisement and services)."
    author: "Diego"
  - subject: "It is not about Android or Google"
    date: 2013-09-04
    body: "<p>Hi Markus,</p>\r\n\r\n<p>Please understand that our statement is in no way to be understood as support for Google Business practices outside of releasing things under and complying to the GPL.</p>\r\n\r\n<p>I am certain that I speak for the vast majority of KDE hackers when I say that we greatly prefer the open Mer or other embedded/mobile Linux'es to port our apps to or hack on over Android, on a variety of levels:</p>\r\n<ul><li>technically - having a full and familiar Linux userspace available</li>\r\n<li>legally - having decent, open drivers (Google does, in our eyes, far too little to ensure manufacturers shipping Android comply fully with the license requirements)</li>\r\n<li>philosophically - we don't like how the 'open' nature of Android is limited to after-release code dumps from Google Inc. as opposed to an open and participative development process</li></ul>\r\n\r\n<p>However. No matter what we think of the parties involved in this dispute (on both sides), what we do NOT want is that the outcome of this legal battle can, in ANY way, be construed to mean that distributing open source software constitutes anti-competitive behavior or predatory pricing. That would be extremely harmful for Free Software, Innovation in the IT world and society in general. Hence this open letter.</p>"
    author: "jospoortvliet"
  - subject: "It is not about Android or Google"
    date: 2013-09-04
    body: "Hi Diego,\r\n\r\n<p>Please <a href=\"http://dot.kde.org/comment/118089#comment-118089\">see my comment to Markus</a> above. You make a very valid point and we agree with it completely, however, we do not intend to support Google/Android in this aspect. We are worried about other potential outcomes of this trial.</p>"
    author: "jospoortvliet"
  - subject: "I don't agree with the"
    date: 2013-09-04
    body: "I don't agree with the assertion that device makers are forced to install proprietary applications yet alone Google's.\r\nThe quoted sentence even states the fact that this is voluntarily: \"... maker who want to...\"\r\n\r\nMicrosoft and Nokia should know better than to claim Google apps such as Maps are \"must-have\" when their very own devices have a different map provider, etc.\r\n\r\n"
    author: "kevkrammer"
  - subject: "Want to sell = want to install Google applications"
    date: 2013-09-04
    body: "Yes, the installation of Google's proprietary applications is voluntary. But in reality, do you think users would buy a mobile phone advertised as an Android phone without access to the Play Market?\r\n\r\nThe fact that Nokia's phones have a different map provider is also a choice made to turn users to Nokia's platform. And it's not only Maps, but the whole integration into Google services. "
    author: "Marcus"
  - subject: "Windows Phone is even worse"
    date: 2013-09-04
    body: "Windows Phone is even worse in this regard, because a windows phone device needs to meet certain minimum specs and has to have 3 hardware buttons. Every mobile platform (iOS, Windows Phone, Android) comes with their own (proprietary) apps pre-installed. Its just that Micorsoft is mad, because they can't seem to get a foot in the door (of mobile devices). \r\nMicrosoft's best piece of trolling ever!"
    author: "Anonymous"
  - subject: "Sometimes 70% is enough, sometimes 90% is not..."
    date: 2013-09-04
    body: "Far from me defending Google, but I don't remember FairSearch complaining for others (and maybe more evident) monopoly excesses and their effects on the market:\r\n<a href=\"http://www.pcper.com/files/imagecache/article_max_width/news/2013-07-06/Desktop Operating System Market Share 2013.jpg\">Desktop market share</a>\r\nI'm eager awaiting for a complaint for such a macroscopic market distortion, which will get our complete attention and support."
    author: "Giancarlo"
  - subject: "The end result"
    date: 2013-09-05
    body: "The complaint is - taken to its logical conclusion - is yet another attack on GPL or similar license models for commercial software. By adding, for example KDE or Gnome to a pay-for operating system, the vendor could be accused of predatory pricing as it isn't charging (potentially) for KDE or Gnome.\r\n\r\nAlso, in this case it seems that Google is already complying with the same ruling that Microsoft suffered years ago, in that Google Maps or Play is - indeed - uninstallable by the user from the device without it breaking (cp Internet Explorer earlier)."
    author: "Shieldfire"
  - subject: "Defending fairsearch by making it about Android"
    date: 2013-09-06
    body: "Please note, regardless of what Android does, the issue with these claims is the precedent they would set. The precedent would hurt free software, even if EU comission focus only on the proprietary parts of Android."
    author: "vexorian"
---
The KDE community is deeply concerned by the wrong notion contained in a recent complaint to the European Commission. The Fairsearch initiative <a href="http://www.fairsearch.org/mobile/fairsearch-announces-complaint-in-eu-on-googles-anti-competitive-mobile-strategy">claims that "distribution of Android at below-cost" could constitute anti-competitive behaviour or predatory pricing</a>. Mirko Böhm produced a <a href="http://dot.kde.org/sites/dot.kde.org/files/KDE-Fairsearch-claim-response_0.pdf">response</a> (PDF) for the KDE Community.

In part, the Fairsearch complaint is an attempt to reduce the strong competition of Free Software platforms with proprietary offerings. KDE and other free software projects would be adversely affected by a misinformed decision about Android. The Internet itself (a basis for the Fairsearch complaint) is largely a product of Free Software. The KDE Community in one of the largest Free Software communities in the world, a global collaboration of companies and individuals building a free platform and creating programs in an openly governed development process.

The predatory pricing claims against Free Software platforms in the complaint to the European Commission are wrong in substance and hurtful to collaborative development and open innovation. Volunteer-driven Free Software communities rely on regulators to protect open innovation against the vested interests of strong proprietary players.

Members of the KDE Community encourage the European Commission to understand that Free Software truly fosters innovation and increases competition. At stake is the liberty of our contributors and those of other communities to collaboratively create and distribute software as a common good.