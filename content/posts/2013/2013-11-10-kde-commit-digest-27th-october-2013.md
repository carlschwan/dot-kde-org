---
title: "KDE Commit-Digest for 27th October 2013"
date:    2013-11-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-27th-october-2013
---
In <a href="http://commit-digest.org/issues/2013-10-27/">this week's KDE Commit-Digest</a>:

<ul><li>Big change in screen management: krandr is removed and replaced by kscreen</li>
<li>New backends in <a href="http://edu.kde.org/cantor/">Cantor</a>: Scilab backend returns with syntax highlighting, tab-completion and more; new Python2 backend is added</li>
<li><a href="http://kate-editor.org/">Kate</a> adds advanced completion list filtering: abbreviation and "contains" matching</li>
<li><a href="http://skrooge.org/">Skrooge</a> now imports (csv, qif) support date format like: 31Dec2012</li>
<li><a href="http://okular.org/">Okular</a> adds audio and video support for epub</li>
<li><a href="http://dot.kde.org/2012/08/21/sflphone-kde-client-joins-kde-family">Sflphone KDE</a> gets fully implemented global presence tracking</li>
<li><a href="http://games.kde.org/game.php?game=kreversi">KReversi</a> is ported to QML.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-10-27/">Read the rest of the Digest here</a>.