---
title: "KDE Commit-Digest for 7th July 2013"
date:    2013-08-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-7th-july-2013
---
In <a href="http://commit-digest.org/issues/2013-07-07/">this week's KDE Commit-Digest</a>:

<ul><li>KDE Telepathy adds metacontacts and other new features</li>
<li><a href="http://krita.org/">Krita</a>'s canvas input is now configurable</li>
<li><a href="http://kdenlive.org/">Kdenlive</a> adds undo command for add effect action</li>
<li>Kdelibs adds a new <a href="http://solid.kde.org/">Solid</a> backend for windows which is much faster and much more reliable</li>
<li>Kwebkit adds a new option: Zoom to DPI allows users to automatically scale content to match desktop resolution</li>
<li>Optimization of item removal in <a href="http://dolphin.kde.org/">Dolphin</a>, memory usage in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>GSoC work in <a href="http://edu.kde.org/kstars/">KStars</a> and KReversi.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-07-07/">Read the rest of the Digest here</a>.