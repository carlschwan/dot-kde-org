---
title: "KDE Ships Second Release Candidate of Plasma Workspaces, Applications and Platform 4.10; Announces Third Release Candidate"
date:    2013-01-04
authors:
  - "jospoortvliet"
slug:    kde-ships-second-release-candidate-plasma-workspaces-applications-and-platform-410
comments:
  - subject: "Is there a list of known open bugs in the 4.10 RC?"
    date: 2013-01-07
    body: "This would be useful."
    author: "mark"
  - subject: "KDE bugzilla"
    date: 2013-01-09
    body: "Surely KDE bugzilla has a list...bugs.kde.org"
    author: "bluelightning"
---
Today KDE <a href="http://kde.org/announcements/announce-4.10-rc2.php">released</a> the second release candidate for its renewed Workspaces, Applications, and Development Platform. Meanwhile, the KDE release team has decided to schedule a 3rd release candidate to allow for more testing, pushing the 4.10 releases into the first week of February 2013. Further testing and bug fixing will allow KDE to deliver rock-stable products to millions of users world wide.
<p align="justify">
Some of the highlights of 4.10 include:
<ul>
    <li>
    <strong>Qt Quick in Plasma Workspaces</strong> -- Qt Quick is continuing to make its way into the Plasma Workspaces. Plasma Quick, KDE's extensions on top of QtQuick, allow deeper integration with the system and more powerful apps and Plasma components. Plasma Containments can now be written in QtQuick. Various Plasma widgets have been rewritten in QtQuick, notably the system tray, pager, notifications, lock & logout, weather and weather station, comic strip and calculator plasmoids. Many performance, quality and usability improvements make Plasma Desktop and Netbook workspaces easier to use.
    </li>
    <li>
    <strong>New Screen Locker</strong> -- A new screen locking mechanism based on QtQuick brings more flexibility and security to Plasma Desktop.
    </li>
    <li>
    <strong>Animated Wallpapers</strong> -- Thanks to a new QtQuick-based wallpaper engine, animated wallpapers are now much easier to create.
    </li>
    <li>
    <strong>Improved Zooming in Okular</strong> -- A technique called tiled rendering allows Okular to zoom in further while reducing memory consumption. Okular Active, the touch-friendly version of the powerful document reader is now part of KDE Software Compilation (the combined releases).
    </li>
    <li>
    <strong>Faster indexing</strong> -- Improvements in the Nepomuk semantic engine allow faster indexing of files. The new Tags kioslave allows users to browse their files by tags in any KDE-powered application.
    </li>
    <li>
    <strong>Color Correction</strong> -- Gwenview, KDE's smart image viewer and Plasma's window manager, now supports color correction and can be adjusted to the color profile of different monitors, allowing for more natural representation of photos and graphics.
    </li>
    <li>
    <strong>Notifications</strong> -- Plasma's notifications are now rendered using QtQuick. Notifications themselves, especially concerning power management, have been cleaned up.
    </li>
    <li>
    <strong>New Print Manager</strong> -- Setup of printers and monitoring jobs was improved thanks to a new implementation of the Print Manager.
    </li>
    <li>
    <strong>Kate, KDE's Advanced Text Editor</strong> received multiple improvements based on user feedback. It is now extensible using Python plugins.
    </li>
    <li>
    <strong>KTouch</strong> -- KDE's touch-typing learning utility has been rewritten and features a cleaner, more elegant user interface.
    </li>
    <li>
    <strong>libkdegames improvements</strong> -- Many parts of libkdegames have been rewritten, porting instructions for 3rd party developers are <a href="http://techbase.kde.org/Projects/Games/Porting_to_libkdegames_v5">available</a>.
    </li>
    <li>
    <strong>KSudoku</strong> now allows printing puzzles.
    </li>
    <li>
    <strong>KJumpingCube</strong> has seen a large number of improvements making the game more enjoyable.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan">4.10 Feature Plan</a>. As with any large number of changes, we need to give 4.10 a good testing in order to maintain and improve the quality and user experience when users install the update.

<h2>Testing</h2>
<p align="justify">
KDE is running an extra detailed Beta Testing Program throughout the 4.10 beta and RC releases. To participate, get the latest KDE 4.10. Instructions for installing the latest test release can be found <a href="http://community.kde.org/Getinvolved/Quality/Beta/4.10/Installing">here</a>. </p>
<p align="justify">
Some areas of KDE deserve extra testing attention this time around. We have checklists for testing in general, and recommendations for special attention on the <a href=http://community.kde.org/Getinvolved/Quality/Beta/4.10#Testing">wiki</a>.
</p>

<h2>Get Involved</h2>
<p align="justify">
The KDE Beta Testing Program is a way for anyone to contribute to the quality of KDE software. You can make a difference by carefully testing the programs that are part of the KDE 4.10 beta and assist developers by identifying legitimate bugs. This means higher quality software, more features and happier users and developers.
</p>
<p align="justify">
The Beta Testing Program is structured so that any KDE user can give back to KDE, regardless of their skill level. If you want to be part of this quality improvement program, please contact the Team on the IRC channel #kde-quality on freenode.net. The Team Leaders want to know ahead of time who is involved in order to coordinate all of the testing activities. They are also committed to having this project be fun and rewarding. After checking in, you can install the beta through your distribution package manager. The KDE Community wiki has instructions. This page will be updated as beta packages for other distributions become available. With the beta installed, you can proceed with testing. Please contact the Team on IRC #kde-quality if you need help getting started. </p>