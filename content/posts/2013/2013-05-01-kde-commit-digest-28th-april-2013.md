---
title: "KDE Commit-Digest for 28th April 2013"
date:    2013-05-01
authors:
  - "mrybczyn"
slug:    kde-commit-digest-28th-april-2013
---
In <a href="http://commit-digest.org/issues/2013-04-28/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> can now be configured not to transcode from MP3 to MP3 (for example) and more</li>
<li><a href="http://techbase.kde.org/Projects/Edu/Artikulate">Artikulate</a> implements native sound recording in editor</li>
<li><a href="http://www.digikam.org/">Digikam</a> adds Pixel Size and Aspect Ratio filters to Text filter and Advanced Search dialog</li>
<li>Search now uses https for web shortcuts</li>
<li><a href="http://kate-editor.org/">Kate</a> adds <a href="http://en.wikipedia.org/wiki/OpenCL">OpenCL</a> highlighting and preliminary implementation of <a href="http://www.cmake.org/">CMake</a> auto completer</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> implements file-backed layers; <a href="http://blogs.kde.org/2013/03/24/coffice-calligra-android-available-now">Coffice</a> adds zoom.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-04-28/">Read the rest of the Digest here</a>.