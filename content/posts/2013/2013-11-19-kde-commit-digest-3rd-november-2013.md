---
title: "KDE Commit-Digest for 3rd November 2013"
date:    2013-11-19
authors:
  - "mrybczyn"
slug:    kde-commit-digest-3rd-november-2013
---
In <a href="http://commit-digest.org/issues/2013-11-03/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://edu.kde.org/ktouch/">KTouch</a> there is an easy way to train on self-supplied texts, resolving a feature regression in a much improved way</li>
<li>Systemtray makes the grid view more dynamically sized, removes pulse animation when battery is low to save energy even more</li>
<li>KCachegrind adds "cost per function call" column in the call view</li>
<li>KDEConnect improves package dispatch to the different plugins</li>
<li><a href="http://konversation.kde.org/">Konversation</a> makes it possible to disable notifications for certain highlights</li>
<li>Important optimizations in <a href="http://kate-editor.org/">Kate</a> code completion and <a href="http://dolphin.kde.org/">Dolphin</a> item sorting.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-11-03/">Read the rest of the Digest here</a>.