---
title: "Akademy 2013 - Well Done"
date:    2013-07-23
authors:
  - "kallecarl"
slug:    akademy-2013-well-done
---
<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

<a href="http://akademy2013.kde.org/">Akademy 2013</a> in Bilbao, the Basque Country, Spain wrapped up on Friday, 19 July. According to one long-time KDE contributor, it was "A most awesome event."

<h3>Gratitude</h3>
Thanks to: <ul>
<li><a href="http://akademy2013.kde.org/sponsors">our sponsors</a></li> 
<li><a href="http://ev.kde.org/">KDE e.V.</a></li>
<li>the Akademy Team, led by <strong>Claudia Rauch</strong> and <strong>Kenny Duffus</strong></li> 
<li>the local team, led by the incomparable <strong>Dani Gutiérrez Porset</strong></li>
<li><a href="http://www.kde-espana.es/">KDE España</a> and <a href="http://es.kde.org/akademy-es2013/">Akademy-es</a></li>
<li><a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">the co-located Qt Contributors Summit</a></li>
<li>local Bizkaia governments and transportation agencies</li>
<li>the warm and welcoming local people, businesses and community</li>
<li>Akademy attendees from around the world</li>
<li>the entire KDE Community</li>
</ul>

<h3>Share the Akademy Experience</h3>
Individually contributed <a href="http://community.kde.org/Akademy/2013/Blogs">blogs</a> and <a href="http://community.kde.org/Akademy/2013/Photos">photos</a> in the <a href="http://community.kde.org/Akademy/2013">Akademy 2013 wiki</a> provide a multi-faceted view of Akademy 2013. Other blogs on <a href="http://planetkde.org/">Planet KDE</a> add still more. Short of being there, these resources provide a delightful sense of Akademy 2013 in Bilbao and the Akademy experience in general.

Matija Šuklje of Free Software Foundation Europe wrote, "...special kudos to all the KDE community for the interesting talks, all the fun discussions, parties, knowledge and of course software (in the widest sense). You’re a very interesting and open bunch, and one of the nicest groups of people I’ve come across."

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/RelaxErlaxazioaJRBYSA.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/RelaxErlaxazioa700JRBYSA.jpg" /></a><br />Relaxation - Erlaxazioa   (click for larger)<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h2>Presentations - Videos</h2>
Previous Dot articles about Akademy 2013 described <a href="http://dot.kde.org/2013/07/14/akademy-2013">Day One activities</a>, and the presentations and events of <a href="http://dot.kde.org/2013/07/15/akademy-2013-day-two">Day Two</a>. 

<strong>Video are now available!</strong> Look at the presentation details in the <a href="https://conf.kde.org/en/Akademy2013/public/schedule">Akademy schedule</a>. Many of the speakers have also provided slides of their presentations.

<h2>Planning, Details, Hacking</h2>
Another earlier Dot article presented the first two days of <a href="http://dot.kde.org/2013/07/18/akademy-2013-gatherings">Birds of a Feather Sessions, Workshops and camaraderie</a>. On Thursday and Friday (18 & 19 July), there was more Akademy with not as many people, but with no less intensity and commitment.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/HostelLobby700MKBY.JPG" /><br />Hacking at the Hostel after midnight<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h3>Solid</h3>
hosted by <strong>Alex Fiestas</strong>
Discussed support on Windows, Internet connectivity detection, and hotplugged portable device naming. Plasma Active NetworkManager is looking a lot better thanks to usability work.

<h3>KPeople</h3>
hosted by <strong>David Edmundson</strong>
The intention was to promote collaboration with KDE-PIM developers and determine missing components. The BoF was incredibly productive! Mapped out plans for integrating KPeople into the Address Book, Kmail, and other applications.

<h3>KDE Telepathy</h3>
hosted by <strong>David Edmundson</strong>
The group worked on 0.7, including integration with KPeople. It's possible that there may be a release on September 21st. KDE Telepathy is taking on some maintainership tasks, due to upstream changes. Some GStreamer dependency issues are being resolved; video doesn't work in some distributions when they want to ship conflicting versions.

<h3>KDE Frameworks 5 (KF5)</h3>
reported by <strong>John Layt</strong>
There was a discussion about how to decide when KF5 is ready and picking a legitimate release date. Work is in progress on the KDE-UI and the KIO/GUI split. Some consideration of versioning - one version across all libraries vs one version per library (similar to Qt). 

The group talked about the "Famous" Tier 4: should each library be in its own repository? Doing so would make it easier for users of the libraries to conceptualize that the libraries can be used independent of each other. The tooling is a Work In Progress.

There was a healthy debate on C++11 support and usage: should be mandatory or optional? The group decided to use a subset that can work on all platforms that we want to support to attract developers who aren't yet caught up.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/HostelLobbyFloor700MKBY.JPG"><br />After midnight - why party when we can hack?<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

The Frameworks dependency tree is complicated. And it's a lot flatter now, especially since the Randa sprint in September last year.

The group discussed various aspects of setting a release deadline. There was a tussle between "Release it when we get done" and "Set a deadline and get it done sooner due to pressure". June 2014 is the conservative target; April or May would be the target to pressure developers or that might be the timeframe for a preview release. There is some push to avoid nasty PR if a self-imposed deadline is slipped. An actual release will likely happen sometime around October or November.

<h3>Student participation in Free Software projects</h3>
The group met to address the opportunity of engaging students through internships. They made a list of projects that need some love, not just during summer, but for the whole year. There should be some structure for signing internship agreements with students.

<h3>Plasma Active</h3>
hosted by <strong>Shantanu Tushar</strong> and <strong>Sebastian Kügler</strong>
Task-centric workflows was a topic of discussion. It was proposed to use QML to create task-specific reduced UIs as well as extended UIs for special tasks which only specific users need. They would be shared in browser extensions, and the author would be responsible for bug fixes.

There was a discussion about creating a UI to connect applications to create flows similar to <a href="http://en.wikipedia.org/wiki/Yahoo!_Pipes">Yahoo Pipes</a>. The short term goal is create flows by using scripts. These would be intended primarily for developers, but can be shared and used by normal users. <a href="http://en.wikipedia.org/wiki/Apple_Automator">Apple Automator</a> had a similar approach but failed. It was supposed to be used to completely automate tasks and thus was very complex. In our implementation, we do not automate tasks but rather connect different applications to create a tool optimized for users to complete a task, so it's much simpler than Automator. We have a new recruit to the PA team (yay!)

<h3>Development Plan for Plasma Active 4</h3>
Plasma Active 4 will be developed in sprints, each focused on a specific topic.
<ul>
<li>Files</li>
<li>Alarms</li>
<li>Integration</li>
<li>Books</li>
<li>Browser</li>
<li>Keyboard</li>
<li>News / MicroBlog</li>
<li>Share Like Connect</li>
<li>Power Management</li>
</ul>

Major changes in Plasma Active 4
<ul>
<li>Remove recommendations drawer in PA 4; it is not really useful yet.</li>
<li>Remove location naming plasmoid in PA 4; users don't get what it is currently used for.</li>
<li>Remove Contour Daemon</li>
</ul>

<h3>Planning for Plasma Active 5</h3>
It will be based on the 4.11 releases or KDE Frameworks 5 (KF5)/Plasma 2, depending on the KF5 schedule. Planned features include webAccounts (UI to add account, share on social networks via Share Like Connect) and KDE Telepathy Active.

<h3>Plasma Media Center</h3>
hosted by <strong>Shantanu Tushar</strong>
Work started on a new startup screen and creation of user profiles. There was some feedback from the <a href="http://dot.kde.org/2013/07/18/akademy-2013-gatherings">usability workshop</a> on Monday, along with related fixes and suggestions.

<h3>Okular</h3>
hosted by <strong>Albert Astals Cid</strong>
Several topics were covered:
<ul>
<li>Annotation eraser - needs work on undo/redo</li>
<li>Background color provided by KColorScheme (System Settings)</li>
<li>Tabbed interface - not everyone likes it, but could work if someone does the patches</li>
<li>Add option to show Table of Contents (if present) in sidebar on startup instead of thumbnails</li>
<li>Make TextDocument_Generator threaded. Good idea, test to make sure it's thread-safe and not crashing randomly</li>
<li>Selection tool: copy/extract as vector graphic by calling "pdftocairo"</li>
<li>API for backend supporting selection-svg-export</li>
<li>Dialog with cancel button with 1 second delay</li>
<li>Best-fit zoom. User feedback said: Yes! Group worked on settings problem.</li>
<li>SVG support for Epubs - seems good. Will be reviewed and tested</li>
<li>Patches, triaged some irrelevant bugzilla entries</li>
<li>New tiling engine; multi-threaded and super cool</li>
</ul>  

<h3>Language Learning Personas</h3>
from <strong>Andreas Cord-Landwehr</strong>
The group discussed typical target users for language learning applications in KD-Edu (currently not many) and opportunities for expanding with the following types of users:
<ol>
<li>Professional user who is very motivated to learn a new language(e.g., for job)</li>
<li>Hobby-user who has time to learn a language, perhaps he or she wants to be able to speak basic phrases on holiday</li>
<li>Student who has to fulfill teachers' demands or pass exams</li>
</ol>
There are more possible target groups, but they are currently out of scope.

Orthogonal to users, <a ref="http://en.wikipedia.org/wiki/Common_European_Framework_of_Reference_for_Languages"> the language competence level</a>appears to be useful to determine at the state of a language learning process where KDE applications can help.

There was a discussion of ideas for a possible future language learning desktop application or activity, and how to integrate language learning sources from the web.

<h3>Artikulate</h3>
from <strong>Andreas Cord-Landwehr</strong>
There was an explanation of how Artikulate works and how one can contribute (slides will be available soon in Andreas's blog; check planetkde.org). A person from Greece wants to create a course in the Greek language (apparently only from what he read at the blog posts at PlanetKDE.org). Several KDE contributors promised to help with their languages.

Within the next 3 months a first major version will be released that includes all important features. The Artikulate team is planning to be part of the next major KDE release.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/CoolinItJRBYSA.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/CoolinIt700JRBYSA.jpg" /></a><br />Coolin' It   (click for larger)<br /><small><small>by Jonathan Riddell (<a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA</a>)</small></small></div>

<h3>About Akademy 2013 Bilbao, the Basque Country, Spain</h3>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.