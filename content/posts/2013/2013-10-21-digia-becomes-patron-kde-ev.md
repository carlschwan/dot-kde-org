---
title: "Digia becomes Patron of KDE eV"
date:    2013-10-21
authors:
  - "sealne"
slug:    digia-becomes-patron-kde-ev
---
<div align=right style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/digia_0.png" /><br />&nbsp;<br /><a href="http://ev.kde.org/supporting-members.php"><img src="http://dot.kde.org/sites/dot.kde.org/files/patron_small.png" /></a></div>
We are happy to announce that Digia is joining KDE e.V. as a <a href="http://ev.kde.org/supporting-members.php">Patron</a>.

Digia has been a supporting member and Akademy sponsor since 2012, and now has become a Patron. Digia is the company responsible for commercial Qt licensing, as well as product development and open source licensing together with the <a href="http://qt-project.org/">Qt Project</a> under open governance.

Since Digia took over the Qt business from Nokia in late 2011, the company has been in close contact with the KDE Community and KDE e.V., a registered non-profit organization that represents the KDE Community in legal and financial matters. Both organizations worked together to organize the <a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">Qt Contributors Summit 2013</a>, which took place in parallel with <a href="http://akademy2013.kde.org/">Akademy</a> in Bilbao, Spain last July.

Lydia Pintscher, Vice President of KDE e.V., said:
<blockquote>We are very happy that Digia is becoming a Patron of KDE e.V. and showing their support for one of the largest projects in the Qt ecosystem. This is a sign of commitment for an open and free future of Qt. We are looking forward to more future collaboration both in contributions to the Qt code base itself as well as the Qt Project in organizational matters.</blockquote> 

Lars Knoll, Digia Qt CTO and Qt Project Chief Maintainer stated:
<blockquote>The KDE Community plays a key part in growing the Qt ecosystem for all types of developers and use cases. We are pleased to be a Patron of KDE e.V. and to continue our work together to foster the further spread of the Qt technology.</blockquote>

There are many ways to <a href="http://community.kde.org/Getinvolved">get involved</a> with KDE; KDE e.V. supporting members contribute financially. Individuals can <a href="http://jointhegame.kde.org/">Join the Game</a>, while companies can provide financial support at a <a href="http://ev.kde.org/supporting-members.php">variety of levels</a> or by sponsoring events like <a href="http://akademy.kde.org/">Akademy</a>.

Through financial contributions, such as Digia becoming a Patron, KDE e.V. is able to pay the hard costs of one of the largest free and open software projects in the world. These funds keep KDE servers running, cover meeting expenses, provide resources for volunteer contributors to organize and attend conferences and trade shows, and protect KDE legal interests—all managed by KDE e.V. in support of the KDE Community.

Thank you Digia for your financial assistance to KDE and for our effective collaboration on the Qt cross-platform application framework. 