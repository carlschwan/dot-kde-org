---
title: "KDE Commit-Digest for 15th September 2013"
date:    2013-09-30
authors:
  - "mrybczyn"
slug:    kde-commit-digest-15th-september-2013
---
In <a href="http://commit-digest.org/issues/2013-09-15/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://phonon.kde.org/">Phonon</a> implements VLC 2.2's equalizer and a subtitle API</li>
<li><a href="http://skrooge.org/">Skrooge</a> adds new categories and account types</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> has a new interface for migrators</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> improves usage of threads in the interface</li>
<li><a href="http://userbase.kde.org/Telepathy">KDE Telepathy</a> adds support for <a href="http://community.kde.org/KTp/libkpeople">KPeople</a> to LogViewer, this allows (among others) browsing history of entire personas instead of individual contacts</li>
<li><a href="https://projects.kde.org/projects/playground/network/kte-collaborative">Kte-collaborative</a> sees work on an overlay layer: on opening a document, displays an overlay over the view which indicates connection and synchronization progress</li>
<li>Optimizations in KDE Frameworks, <a href="http://amarok.kde.org/">Amarok</a> and others.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-09-15/">Read the rest of the Digest here</a>.