---
title: "Akademy 2014 - Brno, \u010cesk\u00e1 republika"
date:    2013-12-19
authors:
  - "dvratil"
slug:    akademy-2014-brno-česká-republika
---
Kde (KDE) bude příští Akademy? <a href="http://akademy.kde.org/2014">Akademy 2014</a>, každoroční summit komunity KDE se bude konat <strong>od 6. do 12. září v Brně</strong>!</p>

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Brno2.png" /><br />Brno, Česká republika</div>

<strong>Řekněte nám něco o sobě. Jaké jsou vaše vazby na KDE?</strong>
Jsme skupinka nadšenců do otevřeného a svobodného softwaru. Někteří jsou studenti, jiní pracují jako vývojáři, testeři, či jinak přispívají do různých open source projektů. Všichni ale sdílíme jednu vášeň&nbsp;&#8212 KDE. Pro některé z nás je KDE náplň naší práce, někteří příspíváme ve volném čase. Něktěří z nás jsou jen uživatelé, ale jak se lépe zapojit do komunity KDE, než organizováním Akademy? Mnozí z nás jsou členy Fedora KDE SIG&nbsp;&#8212 týmu zodpovědného za KDE ve Fedoře. V minulosti se také někteří podíleli na organizaci podobných konferencí a akcí. Přivést KDE komunitu do Brna byl již dlouho náš sen, a když jsme konečně dostali příležitost, bez váhání jsme se ji chopili.

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/BrnoTeam2.png" /><br /><strong>Brněnský tým</strong><br/>Daniel Vrátil, Iveta Šenfeldová, Jan Grulich, Jaroslav Řezník, Jozef Mlích<br/>Luigi Toscano, Lukáš Tinkl, Martin Bříza, Martin Holec, Martin Kolman</div>

<strong>Proč chcete pomoci s organizací Akademy? Co od Akademy očekáváte?</strong>
Vnímáme naše úsilí jenom jako další způsob, jak přispět do KDE. Chápeme, že komunita se potřebuje čas od času setkat osobně a prodiskutovat plány do budoucna. Z minulých let také víme, že  Akademy není jen o softwaru. Je především o lidech. KDE komunita je unikátní a mi chceme poděkovat lidem za jejich tvrdou práci tím, že zorganizujeme tuto událost a dáme jim příležitost rozvíjet a budovat přátelství a vztahy, které tvoří naši komunitu. V Brně (a vůbec v celé České republice) je mnoho IT firem a fakult se zaměřením na informační technologie. To podle nás dělá Akademy v Brně skvělou příležitostí pro studenty, odborníky i&nbsp;ostatní komunity jak se dostat do kontaktu s KDE komunitou a těžit z její práce. Jsme si také jistí, že KDE komunita bude těžit z brněnské kultury a pohostinnosti.

<strong>Řekněte nám něco o Brně. Proč je to perfektní místo pro Akademy?</strong>
Brno je druhé největší město v České republice. Je administrativní centrum Jihomoravského kraje a je sídlem důležitých justíčních úřadů: Ústavního soudu, Nejvyššího soudu, Nejvyššího správního soudu a&nbsp;dalších.

Brno je také krásné město s mnoha pamětihodnostmi, jako je hrad Veveří z 11. století, hrad a pevnost Špilberk Katedrála sv. Petra a Pavla a mnoho dalších. Pro účastníky Akademy nabízí Brno spoustu přiležitostí k oddechu i zábavě. 

Se 13 univerzitami, 33 fakultami a více než 120 tisíci studenty je Brno centrem vzdělání a vědy&nbsp;&#8212 perfektní místo pro přilákání nových lidí do KDE. Akademy bude probíhat na <a href="http://www.feec.vutbr.cz/">Fakultě elektrotechniky a&nbsp;komunikáčních technologií VUT v Brně</a>, asi 20 minut hromadnou dopravou od centra.

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2014Venue.jpg" /><br />Fakulta elektrotechniky a komunikáčních technologií VUT v Brně</div>

<h2>O Akademy</h2>
Po většinu roku KDE&nbsp;&#8212 jedna z nějvětších komunit svobodného a otevřeného softwaru&nbsp;&#8212 funguje online pomocí emailů, IRC, diskuzních fór a mailing listů. Akademy davá všem přispěvatelům možnost setkat se osobně, budovat sociální vazby, pracovat na konkrétních technologických problémech, zvažovat nové návrhy a posilovat inovativní, dynamickou kulturu KDE. Akademy sdružuje umělce, designéry, vývojáře, překladatele, uživatele, spisovatele, sponzory a mnoho dalších přispěvatelů, aby oslavili úspěchy z&nbsp;předchozího roku a určili směr pro rok následující. Intenzivní workshopy jsou příležitostí, jak tyto plány přenést do reality. KDE komunita vítá všechny firmy a společnosti, které staví na KDE technologiích nebo zkoumají možnosti, které svobodné otevřené technologie nabízí.

Rok 2014 bude už 12. ročníkem Akademy, kdy se opět stovky nadšenců do svobodného softwaru sejdou na dva dny přednášek a pět dní workshopů a programování. Pro více informací, kontaktuje prosím <a href="akademy-team@kde.org">organizační tým Akademy</a>.