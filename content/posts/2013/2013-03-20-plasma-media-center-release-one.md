---
title: "Plasma Media Center - Release One"
date:    2013-03-20
authors:
  - "sinnykumari"
slug:    plasma-media-center-release-one
comments:
  - subject: "Awesomeness"
    date: 2013-03-20
    body: "Really awesome. I'm glad this project has its deserved 1.0 release. I'm looking forward to giving it a try but I'm already sure you all did great job. Congrats!"
    author: "Alessandro Diaferia"
  - subject: "Congratulations!"
    date: 2013-03-20
    body: "Great that you made the 1.0 release! I'll try it out soon and give feedback!"
    author: "Thomas Pfeiffer"
  - subject: "sadly I  can't open this at"
    date: 2013-03-20
    body: "sadly I  can't open this at all .. Chakra Linux KDE 4.10.1"
    author: "zm1990s"
  - subject: "Raspberry Pi"
    date: 2013-03-20
    body: "Is there a build ready to go on a Raspberry Pi?"
    author: "Chris Hills"
  - subject: "Awesome!"
    date: 2013-03-20
    body: "Congrats and <strong>thank you!</strong> I can't wait to get back home and test this baby out!!"
    author: "Pascal G"
  - subject: "WOW"
    date: 2013-03-20
    body: "Is amazing!! Nice!! Cool!! Beautiful!!"
    author: "elav"
  - subject: "Thanks!"
    date: 2013-03-20
    body: "Thanks to all. Credit goes to all of us. We did it :D"
    author: "Sinny Kumari"
  - subject: "Packaging Plasma Media Center"
    date: 2013-03-20
    body: "I forgot to mention in packaging stuff in the article. People who do packaging, please package Plasma Media Center and make installation easier for people.\r\n\r\nFor further info, please contact us on the <a href=\"https://mail.kde.org/mailman/listinfo/plasma-devel\">plasma-devel MailingList</a>.\r\n\r\nThanks"
    author: "Sinny Kumari"
  - subject: "Dual head"
    date: 2013-03-20
    body: "Nice! Is it possible to run this in the second screen (TV) while having regular KDE desktop on the the first screen (monitor)? "
    author: "Naranek"
  - subject: "MythTV support, live TV"
    date: 2013-03-21
    body: "I'd love to see support for MythTV or VDR!\r\n"
    author: "M"
  - subject: "I have a question!"
    date: 2013-03-21
    body: "Nice work guys. Is the guy from Bangarang working on this with you? His project is awesome, is built using all of KDE's pillars and it overlaps a lot with the work you're doing, just wondering if he's involved at all?"
    author: "Bob"
  - subject: "Not a package yet"
    date: 2013-03-21
    body: "We don't have a package yet, but following the build steps you can build it very easily on Raspbian. The only caveat is that video playback won't be that smooth (yet), but music and photos should work just fine."
    author: "shantanu"
  - subject: "Just move it"
    date: 2013-03-21
    body: "Just launch Plasma Media Center and use Alt+drag to move it to the other screen, thats what I do at home."
    author: "shantanu"
  - subject: ":/"
    date: 2013-03-21
    body: "Sadly no, at least as of now. We'd love to work together, actually I see that there are lots of KDE multimedia apps have a lot of overlap as you said. We should do some kind of multimedia framework/pillar for KDE so that we can do this properly. I plan to have a BoF at <a href=\"http://akademy2013.kde.org/\">Akademy</a> about this.\r\nThanks a lot for the idea :)"
    author: "shantanu"
  - subject: "Even better"
    date: 2013-03-21
    body: "If you can try it out, it will be awesome, and even better if we can have someone package this for distros :)"
    author: "shantanu"
  - subject: "Issue?"
    date: 2013-03-21
    body: "Hi!\r\nCan you please tell us that what problem you are facing while running it on Chakra?"
    author: "Sinny Kumari"
  - subject: "This is really awesome!"
    date: 2013-05-31
    body: "This is really awesome!\r\nI'm running it on Chakra! Keep up the good work! "
    author: "Salboz"
---
We are proud to announce the first release (1.0.0) of Plasma Media Center. Built on Plasma and KDE technologies. Designed to offer a rich experience to media enthusiasts.

<h2>Features</h2>
<div style="float: right; border: 1px solid grey; padding: 1ex; margin: 1ex">
<a href="http://dot.kde.org/sites/dot.kde.org/files/pmc1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pmc1wee.png" /></a>
<a href="http://dot.kde.org/sites/dot.kde.org/files/pmc3.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pmc3wee.png" /></a> 
<a href="http://dot.kde.org/sites/dot.kde.org/files/pmc4.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pmc4wee.png" /></a>
<br />PMC screenshots - click for larger</div>
KDE's Plasma Media Center (PMC) is aimed towards a unified media experience on PCs, Tablets, Netbooks, TVs and any other device that is capable of running KDE software. PMC can be used to view images, play music or watch videos. Media files can be on the local filesystem or accessed with KDE’s Desktop Search. This stable release of PMC has a basic set of features:
<ul>
<li>Browse the local filesystem for media</li>
<li>Use KDE Desktop Search to get a unified view of all available media</li>
<li>View photos from flickr, picasa</li>
<li>Create playlists from available media, play sequentially or randomly</li>
<li>Developers can create custom plugins for PMC!</li>
</ul>

<h2>Videos & more screenshots</h2>
<a href="http://www.youtube.com/watch?v=Duf-jk9yOU4">PMC on a tablet</a>
<a href="http://www.youtube.com/watch?v=ZED3N0hQ7nI">Desktop PMC</a>
<a href="https://picasaweb.google.com/117964207644299576171/PlasmaMediaCenter100Release">More screenshots</a>

<h2>Install</h2>
<a href="http://download.kde.org/stable/plasma-mediacenter/1.0.0/src/plasma-mediacenter-1.0.0-1.tar.gz.mirrorlist">Get the 1.0.0 release</a>. Ubuntu and Fedora <a href="http://sinny.in/node/25">build instructions</a>. 

<h2>Wiki</h2>
To know more about Plasma Media Center, check out <a href="http://community.kde.org/Plasma/Plasma_Media_Center">the wiki</a>.

<h2>Bugs</h2>
Please report bugs or issues at the <a href="https://bugs.kde.org/enter_bug.cgi?product=plasma-mediacenter&format=guided">KDE Bug Tracker</a> or catch up with the PMC team in the #plasma channel on freenode IRC.

<h2>Newest stuff</h2>
Get the <a href="https://projects.kde.org/projects/playground/multimedia/plasma-mediacenter/repository">latest and greatest from PMC</a>.  

<h2>Coming Up</h2>
There are already lots of improvements in PMC that will be part of an upcoming release. Please file feature requests at http://bugs.kde.org/ or contact PMC developers at plasma-devel@kde.org.

<h2>Thanks</h2>
To Aaron Seigo, Alessandro Diaferia, Deepak Mittal, Fabian Riethmayer, Lukas Appelhans, Marco Martin, Onur-Hayri Bakici, Shantanu Tushar, Sinny Kumari, Sudhendu Kumar, Sujith H and everyone else who made Plasma Media Center possible.

Please give PMC a try and let us know how we can improve it. Keep the comments coming :)