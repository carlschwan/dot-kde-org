---
title: "Akademy-fr in Toulouse, 23 and 24 November 2013"
date:    2013-11-16
authors:
  - "morice-net"
slug:    akademy-fr-toulouse-23-and-24-november-2013
---
Akademy-fr takes place in Toulouse next week. <a href="http://www.toulibre.org/akademyfr-2013">Akademy-fr</a> is the meeting of KDE France. 
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Toulibrelogo.png" /></div>

We welcome contributors, users, people who want to become either of those, anyone interested in free and open source software, freedom and community. This is an opportunity to learn about the latest from KDE, to discuss technical points with technical contributors, and to discover how to use the wide range of KDE software.

Akademy-fr is associated with <a href="http://2013.capitoledulibre.org">Capitole du Libre 2013</a>, which is becoming a really big and important event. Look at <a href="http://2013.capitoledulibre.org/programme.html">the program</a>, plenty of KDE software presentations and workshops are listed.

For more information, <a href="mailto:"jeannicolasartaud@gmail.com">send an email</a>, check out <a href="http://morice.ipsquad.net/blog/?p=73">this blogpost</a> or refer to the <a href="http://www.toulibre.org/akademyfr-2013">official Akademy-fr website</a>.

Thank you for reading.
