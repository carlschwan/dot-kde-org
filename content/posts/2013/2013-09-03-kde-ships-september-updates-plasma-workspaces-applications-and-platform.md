---
title: "KDE Ships September Updates to Plasma Workspaces, Applications and Platform"
date:    2013-09-03
authors:
  - "jospoortvliet"
slug:    kde-ships-september-updates-plasma-workspaces-applications-and-platform
---
Today KDE <a href="http://www.kde.org/announcements/announce-4.11.1.php">released updates for its Workspaces, Applications and Development Platform</a>. These updates are the first in a series of monthly stabilization updates to the 4.11 series. As was announced on the release, the workspaces will continue to receive updates for the next two years. This release only contains bugfixes and translation updates and will be a safe and pleasant update for everyone.

<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/notifications3_crop.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/notifications3_crop-wee.png" /></a><br /><small>Notification configuration</small></div>

More than 70 recorded bugfixes include improvements to the Window Manager KWin, the file manager Dolphin, and others. Users can expect Plasma Desktop to start up faster, Dolphin to scroll smoother, and various applications and tools will use less memory. Improvements include the return of drag-and-drop from taskbar to pager, highlighting and color fixes in Kate and MANY little bugs squashed in the Kajongg game. There are many stability fixes and the usual additions of translations.

A more complete list of changes can be found in <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">KDE's issue tracker</a>. For a detailed list of changes that went into 4.11.1, you can also browse the Git logs.

To find out more about the 4.11 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href='http://www.kde.org/announcements/4.11/'>4.11 release notes</a>.