---
title: "KDE Commit-Digest for 17th November 2013"
date:    2013-12-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-17th-november-2013
---
In <a href="http://commit-digest.org/issues/2013-11-17/">this week's KDE Commit-Digest</a>:

<ul><li>In KDE Frameworks 5, multiple modules and applications ported, including the Locale Systemsettings module</li>
<li>Plasma Calendar now uses lazy loading</li>
<li>Multiple improvements in <a href="http://kate-editor.org/">Kate</a> plugins, especially in the <a href="http://www.python.org/">Python</a> ones</li>
<li><a href="http://techbase.kde.org/Projects/Kooka">Kooka</a> sees improvements in the Auto Select user interface and the scan previewer</li>
<li>New Calendar Applet in KDE Frameworks</li>
<li>In <a href="http://krita.org/">Krita</a>, various improvements to the crop tool, improved and faster mirroring device mirroring</li>
<li>More improvements in the dialog for organizing collections in <a href="http://amarok.kde.org/">Amarok</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-11-17/">Read the rest of the Digest here</a>.