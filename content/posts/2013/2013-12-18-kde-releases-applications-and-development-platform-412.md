---
title: "KDE Releases Applications and Development Platform 4.12"
date:    2013-12-18
authors:
  - "unormal"
slug:    kde-releases-applications-and-development-platform-412
comments:
  - subject: "Akonadi dbus problem"
    date: 2013-12-22
    body: "It seems the Akonadi dbus problem is caused by the proprietary Nvidia driver. A workaround is to downgrade to an older Nvidia driver or using the Nouveau driver."
    author: "thomas"
  - subject: "Each good article must have  a posting date  "
    date: 2013-12-18
    body: "Reading this article, which is a BIG announcement give us no clue as when KDE 4.12 has been released. Any press release MUST be accompanied by a release date, otherwise is almost irrelevant.   Either change the underlying software to one that automatically does that or switch to good writing habits."
    author: "Bogdan"
  - subject: "date shows to the left of the title"
    date: 2013-12-19
    body: "This Dot story points to the actual announcement (http://kde.org/announcements/4.12/), which shows the date in the first line. Stories on the Dot have used this date format for quite a long time. \r\n\r\nThank you for your concern."
    author: "kallecarl"
  - subject: "date"
    date: 2013-12-19
    body: "You can see the date on the URL of the article."
    author: "Santiago"
  - subject: "More stable"
    date: 2013-12-19
    body: "More stable every release but when it will actually be stable?"
    author: "Rsh"
  - subject: "KDEPIM and Akonadi"
    date: 2013-12-19
    body: "Well, it hasn't fixed the Akonadi Dbus problem that's plaguing PIM elements for a week or so "
    author: "Ian"
  - subject: "kworkspace is missing!"
    date: 2013-12-19
    body: "kworkspace tarball is missing from the download page/download servers."
    author: "Mladen"
  - subject: "The workspaces are frozen and"
    date: 2013-12-30
    body: "The workspaces are frozen and did not release. This is just KDE Applications 4.12 - and the libs are upgraded in release numbering for packagers' convenience."
    author: "jospoortvliet"
  - subject: "Nothing ever is... only"
    date: 2013-12-30
    body: "Nothing ever is... only obscure usage patterns and broken configurations should run into trouble by now. Still worthy of bug reports and patches of course...."
    author: "jospoortvliet"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://kde.org/announcements/4.10/screenshots/plasma-4.10.png" /></div>

The KDE Community is proud to <a href="http://kde.org/announcements/4.12/">announce the latest major updates to KDE Applications</a> delivering new features and fixes. With Plasma Workspaces and the KDE Platform frozen and receiving only long term support, those teams are focusing on the technical transition to Frameworks 5. The upgrade in the version number for the Platform is only for packaging convenience. All bug fixes and minor features developed since the release of Platform 4.11 have been included. This release is translated into 52 languages; more languages are expected to be added in subsequent monthly minor bugfix releases. The Documentation Team updated several application handbooks for this release.

<h2>KDE Applications 4.12 Bring Huge Step Forward in Personal Information Management and Improvements All Over</h2>
This release marks substantial improvements in the KDE PIM stack, giving much better performance and many new features. Kate added new features including initial Vim-macro support, and games and educational applications bring a variety of new features. The <a href="http://kde.org/announcements/4.12/applications.php">announcement for the KDE Applications 4.12</a> has more information.

<h2>KDE Platform 4.12 Becomes More Stable</h2>

This release of KDE Platform 4.12 only includes bugfixes and minor optimizations and features. About 20 bugfixes as well as several optimizations have been made to various subsystems, including KNewStuff, KNotify4, file handling and more. Notably, Nepomuk received bugfixes and indexing abilities for MS Office 97 formats. A technology preview of the Next Generation KDE Platform, named KDE Frameworks 5, is coming this month. Read <a href="http://dot.kde.org/2013/09/25/frameworks-5">this article</a> to find out what is coming.

<h2>Spread the Word</h2>
Non-technical contributors are an important part of KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the 4.12 releases. Report bugs. Encourage others to join the KDE Community. Or <a href="http://jointhegame.kde.org"Join The Game</a> and support the nonprofit organization behind the KDE community.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.12 releases.

Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from twitter, youtube, flickr, picasaweb, blogs and other social networking sites.