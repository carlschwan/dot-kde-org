---
title: "Fast, Mobile, Accessible-Akademy 2013 Sessions"
date:    2013-04-25
authors:
  - "kallecarl"
slug:    fast-mobile-accessible-akademy-2013-sessions
---
The Akademy 2013 Program Committee is proud to present the <a href="https://conf.kde.org/en/Akademy2013/public/schedule">Akademy 2013 Presentation Schedule</a>. Reflecting the scope of KDE, the schedule includes a wealth of interesting, timely and valuable topics. From the proposals submitted, 32 were selected to address currently relevant topics of most interest to the KDE Community.

<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

<h2>Focus Areas</h2>

At Akademy 2013, there will be a good mix of development, community, accessibility, legal, artwork and more in three presentation tracks—<strong>practices</strong>, <strong>new ideas</strong> and <strong>community</strong>. Some of the highlights are:
<ul>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/1">fast software</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/14">lightweight software</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/9">mobile after Nokia</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/16">cool accessibility stuff</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/43">hard-core tech</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/31">planning and organizing for quality</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/39">new contributor showcase</a></li>
<li><a href="https://conf.kde.org/en/Akademy2013/public/events/41">social frameworks for effective software projects</a></li>
</ul>

<h2>Full schedule overview</h2>

Please check out all of the <a href="https://conf.kde.org/en/Akademy2013/public/schedule">sessions</a> offered during the first two days of Akademy. Keynote talks will be announced soon.

The <a href="http://akademy2013.kde.org/program">four days</a> following presentations, keynotes and special events will consist of Birds of a Feather (BoF) sessions, other workshops and hacking sessions. Collaborators are encouraged to begin considering topics for those sessions. Special workshops may also be offered. There will be opportunities for attendees to schedule BoFs before and during Akademy.

<h2>Attend Akademy 2013</h2>

If you're planning to attend Akademy 2013 in Bilbao (or have been to Akademy in previous years), please blog about your experiences, your reasons for attending, and your expectations for 2013. "I'm going" badges are available at the <a href="http://community.kde.org/Akademy/2013">Akademy 2013 Bilbao wiki</a>. Your insights and commitments may make the difference between someone attending or not.

Akademy offers a rare opportunity for inspired, intensive work on important projects. Yours could be one of those. <strong><a href="http://akademy2013.kde.org/register">Register</a></strong> now and <strong><a href="http://akademy2013.kde.org/travel">book your travel</a></strong> and <strong><a href="http://akademy2013.kde.org/accommodation">accommodation</a></strong> soon, because <a href="http://akademy2013.kde.org/">Akademy 2013</a> will take place during a busy time in Bilbao (including <a href="http://www.bilbaobbklive.com/2013/en/">BBK Live</a>). 

<h2>The Akademy 2013 Program Committee</h2>
Thanks to the Program Committee for Akademy 2013 Bilbao, representing different areas of expertise and experience within KDE:
<ul>
<li><strong>Dario Freddi </strong>maintains several KDE components, and represents KDE as a speaker or promoter at events worldwide.</li>
<li><strong>Kevin Krammer </strong>has been a KDE contributor for over a decade and is primarily active in user support and developer mentoring.</li>
<li><strong>Lydia Pintscher </strong>is on the Board of Directors of KDE e.V. She is a member of KDE's Community Working Group and one of the admins of KDE's mentoring programs.</li>
<li><strong>Marta Rybczyńska </strong>can be usually found in embedded and kernel development. She is on the <a href="http://commit-digest.org/">KDE Commit Digest</a> team and coordinates KDE Polish translation efforts.</li>
<li><strong>Thiago Macieira </strong>started serious open source work with KDE back in 2001. He has been a full-time Qt developer for 7 years and is currently the Qt Core Maintainer in the Qt Project.</li>
</ul>

Dario Freddi commented on the program selection process: "This year's program brings a wonderful mixture of topics: from deeply technical to community-focused talks, every aspect of KDE is well represented by an amazing set of speakers. To add more value, we are delighted to have several talks representing the wider KDE and Qt ecosystem, such as Mer, BlackBerry and Razor-qt. The quality of proposals was outstanding. It has been a tough choice to leave out some great proposals. Thank you to the whole community for giving us a hard time in deciding what, we expect, will be an outstanding Akademy!"

<h2>Akademy 2013 Bilbao, the Basque Country, Spain</h2>

For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.