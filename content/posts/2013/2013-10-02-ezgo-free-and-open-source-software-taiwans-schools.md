---
title: "ezgo - Free And Open Source Software In Taiwan's Schools"
date:    2013-10-02
authors:
  - "Franklin"
slug:    ezgo-free-and-open-source-software-taiwans-schools
comments:
  - subject: "Episode 2 of \"What do you want to eat tomorrow\""
    date: 2013-10-02
    body: "If you like the video above, here is the episode 2:\r\n\r\nhttps://www.youtube.com/watch?v=JFRrtsO4iLA\r\n\r\nWith English subtitles (click the caption \"cc\" button)."
    author: "Franklin"
  - subject: "Great Read"
    date: 2013-10-02
    body: "I really enjoyed this article. It makes me think about how this could be applied to American education systems."
    author: "CheeseBurg"
  - subject: "Fantastic!"
    date: 2013-10-02
    body: "Way to go, Republic of China!"
    author: "Gonzalo"
  - subject: "You're very welcomed to"
    date: 2013-10-03
    body: "You're very welcome to contact us for this.  We'd like to share our experiences and would like to know how you do it too."
    author: "Franklin"
  - subject: "I agree"
    date: 2013-10-03
    body: "but wish this would be happening in Canada too."
    author: "mrdr"
  - subject: "Suggestion: Include Debian as a base distribution as well"
    date: 2013-10-03
    body: "Firstly, congratulation, for such a huge success. Ezgo story is truely inspiring.\r\n\r\nIf you have enough man power to support anthor distro as a base distribution, I wish to suggest you guys to include Debian as a base distribution as well. Not a debian user myself, but strong believe that debian has a lot to offer, and I am sure that other non debian users will agree as well. (I am not intending to start a distro war, other distros are great as well. ) After supporting ubuntu, supporting debian might not be hard.\r\n\r\nHope Ezgo will be more successful in the future as well."
    author: "krazedkrish"
  - subject: "Man power is an issue"
    date: 2013-10-04
    body: "Yes, using Debian as a base distribution should not be too far different from using Kubuntu.  DebianEdu is also like this.  Just that we need a live system.  I used to spend some time studying debian live building system but with no luck.\r\n\r\nMan power is an issue too.  Currently there is only one system (customizing KDE, repackaging DVD iso, ...) developer for ezgo - yes, me.  Other developers are for art, wallpaper, promoting, testing, ... etc.\r\n\r\nBTW, One of our target is also like this:  Install your own native distribution, and install an \"ezgo\" package to make it become ezgo (at least the application menu, and maybe wallpaper).  It should not be too difficult if just change the application menu.  However it still takes some time to study."
    author: "Franklin"
  - subject: "Thank you"
    date: 2013-10-05
    body: "Wish our experiences can be useful not only to ROC(Taiwan) but also to other countries all over the world."
    author: "Franklin"
  - subject: "We need this in South Africa too"
    date: 2013-10-07
    body: "South Africa is a land of many challenges, and a solution like this could go a long way to help solve the problem of poor education and help the nation thrive. The country has pronounced its intention to use Open Source in favour of proprietary software, but alas this has remained a pronouncement and has not translated into action. Hopefully this project will help point the way for others."
    author: "Gladly"
  - subject: "We can together find the way"
    date: 2013-10-08
    body: "Yes, Improving the education for people who don't have enough resources is a target for ezgo.  Currently we have some projects running.  If you are interested in it or ezgo, feel free to contact with me at:  franklin  at  goodhorse  dot   idv   dot   tw.  I'll introduce our team and partners to you."
    author: "Franklin"
  - subject: "Unity is now gone and KDE's position is more solid than ever"
    date: 2017-07-28
    body: "<p>And now Unity is no more, thus validating your experience with your users.</p><p>I'd like to hear more about ezgo, now 4 years later.</p>"
    author: "Michael"
  - subject: "KDE Plasma is still the main desktop system in the latest ezgo"
    date: 2019-01-08
    body: "<p>We are still working on that.&nbsp; We published ezgo14 last July, which KDE Plasma is still the main desktop system.&nbsp; Besides, the latest ezgo can be based on Kubuntu, Lubuntu and Debian Stretch now.</p><p>Besides, we are now working to make ezgo become a Debian pure blend so that we may get ezgo packages directly from Debian.</p>"
    author: "Franklin"
---
Free and Open Source Software in Taiwan has made impressive strides thanks to the work of the '<em>ezgo</em>' team. They have put together a pre-configured set of Free and Open Source (FOSS) software that makes it easy for teachers and students to get up and running. The New Taipei City government has decided to install ezgo 11 on 10,000 PCs for elementary schools, bringing thousands of students in contact with Linux, KDE and educational Free Software. The ezgo team has written up an account of <em>ezgo</em> and how it came to be.

<h2>First Attempts at Spreading Freedom and Openness</h2>
About ten years ago, though free and open source software (FOSS) had been in Taiwan for nearly ten years already, there were still very few people who knew what FOSS was - most of them technical users. Most people used Microsoft Windows. The computer classes in all school levels in Taiwan still taught Microsoft Office only. Our idea of promoting FOSS at that time was: <em>tell people to "replace" (illegal!) commercial software with FOSS</em>.

We told teachers to use and teach Open Office instead of Microsoft Office. We told people to use Linux instead of Microsoft Windows. We hoped people would understand and teach their students or children about the spirit of freedom, and the spirit of <em>"just for fun"</em>. However, it also means that we asked teachers and people to use software and an operating system they were <strong>not familiar with</strong>. No surprise, we failed through and through.

Our failure told us that if we just told everyone about the spirit of freedom, the spirit of FOSS, nothing would change. After lots of discussions we came with a different idea.

<h2>A different idea</h2>
<div style="width: 400px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure1_400.jpg" /></a><br />Free software inside and outside computer classes</div> 

In Taiwan, there is only one computer class every week. The other 30 classes are for mathematics, science, Chinese, etc. If we promote FOSS only for use in computer classes, we have no way to succeed, because computer classes are unimportant in Taiwan's education, and in the entrance exam of senior high schools or universities, there's no subject named "computer". That's why we can't get FOSS just into computer classes—it doesn't show the real value of FOSS. People would think that FOSS is the responsibility only of computer teachers. But is that true? No, of course not. The value and the spirit of FOSS should be the concern of every teacher, even students and parents, not only computer teachers.

Now the major problem became: how to get all the teachers to understand that? In this way, we attempted to escape from the narrow view of computer teachers. We started to consider the needs of teachers of all subjects. We wanted teachers to see how FOSS "serves" tutoring and learning for all kinds of subjects, instead of only asking people to use FOSS to replace commercial software. We found that <a href="http://www.stellarium.org/">Stellarium</a>, a free and open software software about astronomy, is a good example. So we started to use such ideas to promote FOSS in campuses. We kept looking for software for education like Stellarium. We then discovered that such educational software was available for all kinds of subjects, much, much more than we expected.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; "><iframe width="420" height="315" src="//www.youtube.com/embed/6ADAYpcb2IU?rel=0" frameborder="0" allowfullscreen></iframe><br />Clever video from some very creative teachers in I-Lan County, Taiwan, <br />showing how FOSS like Stellarium can be used in their classes.</div>

We searched for educational software for many different subjects, and translated them into Chinese. Then, we created a live DVD called ezgo, collected the educational software and sorted it out.

<h2>What is ezgo?</h2>
ezgo is a project. ezgo is a live system. ezgo is an important milestone in Taiwan's FOSS promotion. However, ezgo is NOT a new Linux distribution. There have been enough Linux distributions all over the world. We don't need a new ezgo Linux distribution.

The design concept of ezgo is a focus on the applications of FOSS. No matter which Linux distribution we use, we must run an X window system, then run a desktop environment like GNOME or KDE. Furthermore, no matter what desktop we are running, we must use all kinds of software. Before Windows 8, we had to click the Start Menu button to find and launch application software. Under GNOME 2 or KDE, we have a start button showing an application menu. So, we don't care about what Linux distribution or what desktop we're using. We just focus on the applications.

<div style="width: 700px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure0_700.jpg" /></a><br />Release party for ezgo X</div> 

The main value of ezgo is how we sort the application menu. We add a category label in front of the application name. Then we sort the name in the menu. For example, when a user clicks the menu icon and sees an application called "Kig". Who knows what "Kig" is? Well, in the KDE launcher, there might be a short description of this application. But what if we just used a simple launcher? Also, how can we show applications in this system that are of the same kind? Therefore, in our ezgo menu, we added a category label, changing "Kig" to "Mathematics_Kig", and sorted it in the "Education" menu, so users can easily identify that "Kig" is an education application about mathematics, even if they have never used Kig. Also, they can easily know how much similar software is in the ezgo system.

<h2>The history and evolution of ezgo</h2>

Initially, ezgo was not like as it is now. The first version of ezgo was only a software collection, collecting many applications that can be run under a Windows system. With ezgo version 3, Ubuntu 6.06 was released around that time. We decided to promote Linux, so we published two CDs, one for the original ezgo 3, the other running Ubuntu 6.06. ezgo 4 also has two CDs. With ezgo 5, we started to use a DVD, which contains applications for Linux and Windows.

<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure2.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure2_wee.jpg" /></a><br />ezgo 6</div> 
<strong>ezgo 6</strong>
From ezgo 6, we started to think about the following questions: how could we add some localized thoughts and ideas in Linux? And could we assign a special meaning to each version of ezgo to promote it more easily?

Starting from its introduction, the main value of ezgo is to let people be fully aware of what is in the system. Around that time, Microsoft released Windows Vista. So we had the idea that we should have our own slogan to represent the spirit of the project. The first slogan, which was used in ezgo 6, was "X Window Westart". We also designed an icon: a single wing, meaning freedom.

Why only one wing instead of a pair of wings? As you can see, it is only a left wing. What do you think about this "left wing"? Yes, it's about copyright and copyleft. We wanted to tell people that you couldn't fly with only one wing. You wouldn't fly if the world has only copyright. We also need a left wing—no, not "THAT" left-wing in social science, but copyleft so that we could fly freely. This design was to remind people of this wing, which had not been possible for people in Taiwan all these years.

<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure3.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure3_wee.jpg" /></a><br />ezgo 7</div> 
<strong>ezgo 7</strong>
Then, we went to ezgo 7 around the time that Microsoft released Windows 7. ezgo 7 versus Windows 7. When we were designing ezgo 7, we wanted to remind people that both commercial software and free software exist in this world. We wanted to help people understand the real needs and values of software applications. We don't need to use one to replace the other. We don't want to be blocked; this is not a single choice question. We can have both at the same time! So in ezgo 7, our slogan was "Free, Win Win". We wanted people to understand that there is not only commercial software, but also free and open source software. If people can understand what they want, and can have the freedom to choose, it can be a win-win situation.

However, there is still a little trick in this slogan. Can you find it? "Free, Win Win" also means "Free Wins Windows". This was not an intentional part of the design. We became aware of it when a user pointed it out. Which explanation do you like? It's up to you. About the icon design, we used a butterfly with "Free" on its wing. Do you see it?

<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure4.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure4_wee.jpg" /></a><br />ezgo 8</div> 
<strong>ezgo 8</strong>
ezgo 8 had a new theme. Do you see anything special? Here are some hints: What do you think it is in the left bottom? Do you see that it is actually a "giant's shoulder", forming a curvy "L"? Yes, we wanted to say that ezgo stands on the shoulders of giants, and this "giant" is based on Linux. So we put an "ezgo" on the giant's shoulder, along with our slogan "This is the right way"! The letter "r" is turned to the left. It's not always right to turn right! Sometimes it's better to turn left … back towards the giant. The top of the letter "z" is a telescope to signify looking far into the distance from the giant's shoulder. 

<strong>ezgo 9</strong>
ezgo 9. We associated it with "night". A college student on our developing team gave us a good analogy between free and commercial software: FOSS is like a starlit night, while commercial software is like the light pollution from cities. People who live in big cities too much of the time aren't aware of the beauty of a quiet, tranquil night full of stars. If we could leave big cities and lie on the grass without any light pollution, we could enjoy this beautiful sky full of stars, freely. Do you like this analogy? So the design of ezgo 9 was based on the beautiful sky at night, without all the lights from cities.
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure5.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure5_wee.jpg" /></a><br />ezgo 9</div> 

We put a slogan in the center of this wallpaper, "Before the future comes, ezgo9". After the dark night, a new day comes. We are advancing to the future. However, many people had heard of the project "The Future School"? Recently in Taiwan there are many "future classrooms" based on Windows solutions. We found that those "future projects" only talk about the "future". We couldn't afford the future that is so expensive - 3 million NT dollars (one New Taiwan dollar is worth about .025 euros or .035 US dollars) a year per future classroom in Taipei! Therefore, in ezgo 9 we reminded people that we are not against a beautiful future. We want such a future to be realized and to have it benefit everyone. But people shouldn't forget that before the future comes, we can do very much and benefit many people with free and open source software.

<strong>ezgo X</strong>
Finally, the latest version: ezgo 10. Well, we called it ezgo X. Why? X means 10 in the Roman Numbering System. Besides, we found that when a name contains X, it is more powerful, like X-Men :)  First we decided to use "ezgo X" instead of "ezgo 10", and then we came up with design ideas related to X. Eventually we decided to use crossed railroad tracks. It’s a railway turnout. We want ezgo X to be seen as a turnout, helping people to change their ways from commercial software to FOSS. The slogan In Chinese told people to “stride across, and you may find new possibilities”. It is not certain that we will see anything special or beautiful if we stride across this gap, but if we don’t take the opportunity, we will never see different views. Our slogan in English said: No Cross, No Crown.
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Figure6.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Figure6_wee.jpg" /></a><br />ezgo X</div> 

<h2>ezgo and KDE</h2>

As mentioned before, ezgo itself doesn't care what distribution or desktop system it uses. No matter what desktop we used, we included many good KDE applications, especially for education, in our ezgo system. Nevertheless, whether or not it made a big difference, we needed to choose a Linux distribution and a desktop system.

Before ezgo 9, we used Ubuntu or Fedora as our base distribution. For a desktop, we used GNOME 2. One main reason was that GNOME 2 was the default desktop for Ubuntu and Fedora. Another reason was that people here were used to Windows-style operation, so the desktop system couldn't be too far away from it, or we would face a strong resistance when promoting it to a new user.

However, things changed when Ubuntu changed their default desktop from GNOME to Unity. Unity's operation was too far from Windows at the time. We could imagine that there would be a great impact to ask people who were used to Windows to use a desktop system like Unity or GNOME 3. (Now you can see how people react against Windows 8!) So we discussed and argued a lot about changing the desktop. Some partners thought that since we could still choose to use GNOME 2, we should keep the desktop unchanged for ten years. Some thought that sooner or later we would have to face the need to change the desktop. Finally we decided to change the default desktop to KDE.

Therefore, since ezgo X, we have used Kubuntu as our base distribution. Fortunately many KDE developers helped and taught us about how to customize KDE for our needs. This change also fit the slogan in Chinese: "stride across, and you may find new possibilities." We wanted people to stride across this gap and find new interesting things in KDE.

This summer, New Taipei City government decided to use a free and open source operating system instead of Microsoft Windows in their new 10,000 PCs for elementary schools. ezgo 11 was selected as their pre-installed OS. So there will be many students using KDE in their classes in the new school year.

<h2>ezgo's Challenges</h2>

ezgo is more and more successful each year. However we still have challenges. We believe that KDE may face the same challenges too.

1. The trend towards tablets: More and more teachers use tablets in their classes. In Taiwan, there are some experimental projects running in elementary schools, using tablets and apps for homework and communication between parents and teachers. Currently, ezgo is still mainly introducing traditional PC applications, plus some open content running on the web. We may need more good free and open source apps running on different tablets. Also, we may need to change our thoughts and ideas for developing ezgo and promoting FOSS again to fit the trend.

2. Ubuntu's big change: In Taiwan, there is a great ratio of Linux users using Ubuntu. Since Ubuntu 11.04 they started to use the new Unity desktop, which totally discarded the idea of application menus. However, the main idea of ezgo, as we explained above, was based on application menus. That's one of the main reasons why ezgo switched to KDE. Nevertheless, we still face some resistance from Ubuntu and Unity users. By the way, we need to think about if it's also a trend that application menus will be eliminated. It is an interesting topic to discuss, and may also influence KDE's future.

<h2>Conclusion</h2>

Thanks to so many FOSS developers in the world for bringing such a beautiful and free world to us. To be honest, Taiwan is still a desert in the world of FOSS, but we're doing our best to create more and more oases. When we introduced FOSS for education, along with publicly licensed tutorials like <a href="http://phet.colorado.edu/en/about">PhET</a>, almost all the teachers' eyes were shining. Their shining eyes and smiles are very strong motivation for us.

If you are interested in using and promoting FOSS in schools, you're very welcome to contact us. We'd also like to know how other countries promote FOSS in their schools. Also, for the challenges we face, any suggestions or advice is very much appreciated.

We're working on a more beautiful and free future for our kids, and we know that everyone can do it too.

<em>Thanks to Eric Sun, who wrote some of the original content of this article in Chinese. </em>