---
title: "Thank You Akademy Sponsors"
date:    2013-06-19
authors:
  - "kallecarl"
slug:    thank-you-akademy-sponsors
---
Akademy is a non-commercial event, free of charge for all who want to attend. Generous sponsor support helps make Akademy possible. Most of the Akademy budget goes towards travel support for KDE community members from all over the world, contributors who would not be able to attend the conference otherwise. The wide diversity of attendees is essential to the success of the annual in-person Akademy conference. Many thanks to Akademy sponsors!

<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

Sponsors receive benefits beyond the intrinsic rewards of supporting a worthy cause. They have the opportunity to:
<ul>
<li>Work with key KDE and Qt contributors, upstream and downstream maintainers and users of one of the foremost User Interface technology platforms.</li>
<li>Meet other leading Free and Open Software players. Besides the KDE Community, many other Free Software projects and companies participate in Akademy.</li>
<li>Get inspired. Akademy provides an excellent environment for collecting feedback on new products, or for generating new ideas.</li>
<li>Support Free Software and Free Software technologies. Much of the actual work of Akademy is done in new application development, hackfests and intensive coding sessions. These activities generate value that goes far beyond KDE and Akademy.</li>
<li>Be part of the KDE Community. KDE is one of the largest Free and Open Software communities in the world. It is also dynamic, fun-loving, cooperative, committed, creative and hard-working.</li>
</ul>

Many Akademy sponsors also support the <a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki/">Qt Contributors Summit</a>,which is colocated with Akademy on July 15th and 16th. KDE and Qt have been closely associated since KDE began over 15 years ago. The KDE Community has always been at the forefront of trying, developing, using and adapting Qt capabilities, accounting for the largest external use of Qt. Many developers in the core Qt R&D/commercial teams have come from the KDE Community.

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/digia.png" /></div>
<h2>Platinum Sponsor</h2>
Akademy 2013 is generously supported by <strong>Digia</strong>. Digia is responsible for Qt activities worldwide, including product development, commercial and open source licensing, working together with the Qt Project under the open governance model.

<h2>Gold Sponsors</h2>
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/blackberry.png" /></div>
<strong>Blackberry</strong> is a pioneer and global leader in the mobile device industry and introduced Qt-based devices, the Z10 and Q10, earlier this year. 

Speaking for many Akademy sponsors, a BlackBerry spokesperson said, “We are pleased to sponsor the Qt Contributors Summit and KDE Akademy. We appreciate being part of this vibrant community that makes it so easy to create cross-platform apps. At BlackBerry we've embraced open standards and created an open platform to make it easy for Qt developers to bring their apps to our growing user base. We look forward to collaborating with everyone at the conference!”

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/bluesystems.png" /></div>
<strong>Blue Systems</strong> is a company providing strong support for Free/Libre Computer Technologies. Blue Systems sponsors several KDE projects and distributions like Kubuntu and Netrunner. Blue System's goal is to offer software solutions for people valuing freedom and choice.

Thank you for your important contributions towards making Akademy and the Qt Contributors Summit happen!

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/GooglefroglogicICSOINRHMetroTRANS.png" /></div>
<h2>Other Akademy Sponsors</h2>

Another big "thank you" to our Silver sponsor <strong>Google</strong>, a major user and supporter of open source software. Google supports KDE's software development with their annual Summer of Code and Code-in programs

Akademy Bronze sponsors are:
<strong>froglogic</strong>, developers of Squish, an automated cross-platform testing tool for GUI applications used by more than 1,500 companies world-wide.
<br />
<strong>ICS</strong>, a leading supplier of Qt extensions, testing tools and migration services. ICS offers professional services for Qt from training to full product development and delivery.

<h3>Other Supporters</h3>
The event is also supported by the <strong>Open Invention Network</strong> (OIN) and by <strong>Red Hat</strong>.

<strong>Metro Bilbao</strong> is the official transport sponsor for the conference.

<h3>Become a Sponsor</h3>
It is still possible to <a href="http://akademy2013.kde.org/sponsoring">become an Akademy sponsor</a>. Please consider supporting KDE and Akademy.

<h2>Akademy 2013 Bilbao, the Basque Country, Spain</h2> 

For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities. 

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.