---
title: "Akademy 2013 Keynote: Jolla's Vesa-Matti Hartikainen"
date:    2013-06-26
authors:
  - "tcanabrava"
slug:    akademy-2013-keynote-jollas-vesa-matti-hartikainen
comments:
  - subject: "Privacy"
    date: 2013-07-03
    body: "\"Privacy should be a basic human right. All governments should respect that right and use only proper legal processes. The Jolla phone is based on open source. It will provide enough tools and visibility for its users to be sure that they are not spied on.\"\r\nIt is nice to read this. So far I hadn't heard any statement on this from Jolla. Unfortunately it is not very specific. If there is a Q&A after the keynote, could someone ask some more details about this? Will it be possible to install and update free apps Jolla's appstore without having an account or being logged in? Will the Jolla appstore-app not transmit and store IMEI, phone number and network provider of it's users? Will the Jolla phone not support silent sms (which is normally not part of the OS, but of the hardware)? Will the default settings of the phone not be set to automatically copy the phone contents into the cloud? Will there be easy backup/restore solutions for local backups onto a pc or sd cards or cloud solutions of my own choice (like owncloud)? Will it be possible to control what data apps transmit and require easily and by default? It would be really great to have this and this would be the main reason for purchasing a Jolla phone."
    author: "mark"
---
This Akademy <a href="https://conf.kde.org/en/Akademy2013/public/events/79">keynote talk</a> is based on Jolla and their Sailfish OS. It will cover project history, software architecture and collaboration between Jolla and various open source projects such as Qt, Mer, and Nemo Mobile. It will address the user interface concepts used in Sailfish OS and highlight the benefits of using Qt Quick to build the user experience in Sailfish OS.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Vesa-Matti300.jpg" /></div>

Vesa-Matti (veskuh on IRC) has over 9 years of experience in research and development of mobile devices. His most recent work has been developing frameworks and applications on Qt-based mobile platforms such as MeeGo and Jolla. Currently he is an engineer at Jolla, where he develops Sailfish OS applications and works on the user interface.

I chatted with Vesa-Matti on IRC about Jolla, good mobile phones, community developed software, freedom and other things. Akademy 2013 will be his first. He lives in Tampere, Finland where Akademy 2010 took place but did not attend. KDE and Jolla have many things in common.

<strong>Tomaz</strong>: You work for Jolla. Were you on the original N9?

<strong>Vesa-Matti</strong>: Yes, I worked on N9 almost from the beginning.

<strong>Tomaz</strong>: It was the best mobile phone ever.

<strong>Vesa-Matti</strong>: Yes, I love it too. We are trying our best to make a phone even better than the N9.

<strong>Tomaz</strong>: How is it to live in Finland?

<strong>Vesa-Matti</strong>: Heh. In June it's fine, we have sun and good weather. During the winter not so nice. The good side of winter is that you can just sit at home with a good feeling and code.

<strong>Tomaz</strong>: I had the most amazing time in Finland in 2010 at Akademy in Tampere, definitely a place that I want to visit again. I was amazed at how the sun was still high at 3:00 a.m. Beautiful city, beautiful place.

<strong>Vesa-Matti</strong>: Yeah, I live in Tampere. Finland is really nice this time of year. Although sometimes it's a bit difficult to sleep with the light. We have a Jolla office here where there's a lot of UI design and development.

<strong>Tomaz</strong>: Would you tell us about the creation of Jolla?

<strong>Vesa-Matti</strong>: It's a long story. I'll try my best. 

When Nokia chose to go with the Windows Phone, it also chose to ship the N9, but not any other MeeGo phones. There were some long term plans for MeeGo, so Nokia kept some people working on it. For example, we did the MeeGo Community Edition for N900.

When Intel left MeeGo, the community really took over. There were two projects after MeeGo: Nemo Mobile, which was about to deliver a mobile OS, and Mer which produces a core distribution for mobile Qt platforms. That's where the Jolla technology comes from. There were people who wanted to continue making MeeGo products. They founded Jolla, and when they were evaluating what the best technology would be, the community-based MeeGo continuations were the perfect solutions.

To have something unique, Jolla started to build its own UI on top. The software stack needed a lot of polish, and even some re-architecting. We continue to do those in the open projects.

<strong>Tomaz</strong>: When Jolla started, N9 was the only true Qt phone in the market. Now we have the Blackberry 10, Jolla, and others. What's the difference between from Jolla and the other Qt mobile phones that are popping up?

<strong>Vesa-Matti</strong>: For Jolla, Qt is a first class citizen. For developing apps using QML, we put a lot of effort into making them as good as possible. We have an awesome team working on the Sailfish Silica component set. It includes many of the original core developers of the QML language and runtime. And we have really experienced app developers from N9 and other Nokia projects. On the middleware level, a lot of the lower level APIs now have quite good Qt bindings for C++  developers.

<strong>Tomaz</strong>: What are your hobbies when you are not programming or making Jolla rock?

<strong>Vesa-Matti</strong>: Well, I do traditional Finnish stuff. Some team sports, beer and sauna. In Finland, sauna is not a place, it's an event.

<strong>Tomaz</strong>: What sports? Is chasing bears bare-handed a traditional sport?

<strong>Vesa-Matti</strong>: Sure, I'd love to do that :) But no. Just regular activities like badminton, tennis, salibandy or floorball, a kind of indoor hockey.

<strong>Tomaz</strong>: Recently the world was shocked to know that the U.S. government spies on almost all computers, and big companies give data to them. This damaged some of the 'feelgood' factor of some companies like Facebook and Google. What is your view on that topic, and how do the Jolla phones act in that regard?

<strong>Vesa-Matti</strong>: I'm quite troubled by the whole thing. It's clear that the U.S. government sees citizens of other countries as second class people. So when they explained the thing, they said that they have some controls and that they do not spy on their own citizens (or at least not as much).

Privacy should be a basic human right. All governments should respect that right and use only proper legal processes. The Jolla phone is based on open source. It will provide enough tools and visibility for its users to be sure that they are not spied on. I think governments in Europe are more reasonable so the laws here are better from the consumer point of view. So I think it helps that Jolla is based in Europe. 

Another thing is that the phone is developer friendly; you will be able to really check what is happening on your device.

<strong>Tomaz</strong>: Talking about friendliness, how is it to work at Jolla?

<strong>Vesa-Matti</strong>: We have three offices and plenty of people across globe in home offices. People are skilled, passionate, and quite nice to work with. Most people work long hours because they love what they are doing. Sometimes it's stressful, like before a big launch event. But then at the launch, it's so fun to see everyone relax and enjoy the feedback. We use IRC heavily, because people are so spread out. People have been free to choose where to focus. Most of the time people tend to work on areas where they have most expertise.

There is not much bureaucracy or process. Of course, there is some, but for engineers, it doesn't get in the way. Of course it's demanding since being a small company, we all have to do a lot to make it all happen. So it is a really really interesting workplace.

<strong>Tomaz</strong>: How open is the company for newcomers? For example, students and open source developers who want to work <em>with</em> the company, not <em>for</em> the company.

<strong>Vesa-Matti</strong>: That's a good question. We say that we are all about the community. We try to be as welcoming as possible in IRC, in the events we attend, in mailing lists, etc. But it can always be better.

Most collaboration happens in the community projects—Nemo Mobile or Mer. Getting involved in these projects may take some effort, but once a person is able to do something, usually their contributions are very welcome. It also works the other way around. Plasma Active is using Mer as core, and a couple of N9 apps have benefited from Nemo work. We try to recognize and thank people from the community, and do little things like invite people to events or send small gifts once in a while. Pretty much all engineers are on freenode in the relevant IRC channels. So far it has worked quite well. We want the collaboration to be even better when the product is finalized.

<strong>Tomaz</strong>: Have you ever gone to a KDE meeting or gotten involved with open source before Jolla? 
 
<strong>Vesa-Matti</strong>: No, I've not been involved with KDE. My open source work pretty much started with Nokia and MeeGo, before that I mostly just used various OSS stuff.
 
<strong>Tomaz</strong>: So it's your first time going to Akademy? Not even in Tampere?

<strong>Vesa-Matti</strong>: Yes, my first time. When the event was in Tampere I did not attend. I heard about it, but did not really even know what it was.

<strong>Tomaz</strong>: Do you have any expectations about KDE People and Akademy?
 
<strong>Vesa-Matti</strong>: I'm interested in hearing what they have to say about us and how they see the other newcomers to the mobile industry.
 
<strong>Tomaz</strong>: How about you personally? Expectations about Bilbao, the KDE Community and Akademy? Are you going to the presentations, the hacking sessions, the traditional hug sessions or the social events?
 
<strong>Vesa-Matti</strong>: I haven't checked the schedule much. I'm probably going to listen to any presentations about the apps I'm using, and any presentations about Plasma Active and mobile stuff. Qt sessions are interesting. And those others you mentioned too. And I'm really curious to see the Guggenheim and the city in general. 

<strong>Tomaz</strong>: There is a rich mix of cultures at Akademy. People from all over the world, talking a babel fish kind of English and trying to do nice things. Program, play, party. Akademy is not just about the software, but also the community. It's a way to know your fellow hackers better and go for drinks together :)

<strong>Vesa-Matti</strong>: At Nokia we had a similar kind of mix I imagine. Although it's much different in an office setting. I will enjoy meeting people and hanging around with them.

<strong>Tomaz</strong>: Be open minded. And hug people. At Akademy, everybody hugs :)

<strong>Vesa-Matti</strong>: For a regular Finnish engineer like me, it will require some mental effort to get into that mode :) 

<strong>Tomaz</strong>: Being an engineer, it's quite easy, you are good at math. So it's all about the lowest common denominator.
 
<strong>Vesa-Matti</strong>: Ah, I see. Everybody loves hugs :) I'm a typical Finnish engineer. In person maybe bit more talkative than other Finnish engineers but still, you know, Finnish :) 

<strong>Tomaz</strong>: Ah, how hard is to get my hands on a Jolla phone? I would love to try one of those :)

<strong>Vesa-Matti</strong>: We will have some kind of a developer program. Don't know about the details yet. The SDK gives some idea of how things work, but it is no substitute for the real thing. 

<strong>Vesa-Matti</strong>: Well my colleagues are leaving for a pint. I'm going to join them.

<strong>Tomaz</strong>: Thanks for your time.

<strong>Vesa-Matti</strong>: Yep. Thanks. I'm looking forward to Akademy :)

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/TomazBurningHand300.jpg" /></div>

(Editor note: This is another work of art by Tomaz Canabrava - Interview Magician.)

<h2>Register Now</h2>

There is still time to <strong><a href="http://akademy2013.kde.org/register">register</a></strong> for Akademy 2013. You are encouraged to <strong><a href="http://akademy2013.kde.org/travel">book your travel</a></strong> and <strong><a href="http://akademy2013.kde.org/accommodation">accommodation</a></strong> soon, because <a href="http://akademy2013.kde.org/">Akademy 2013</a> will take place during a busy time in Bilbao (including <a href="http://www.bilbaobbklive.com/2013/en/">BBK Live</a>). With intense involvement from people in the KDE Community, Akademy 2013 promises to provide a strong impetus for continued KDE innovation and industry leadership. 

<h2>Akademy 2013 Bilbao, the Basque Country, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.