---
title: "Akademy 2013 Day Two"
date:    2013-07-15
authors:
  - "kallecarl"
slug:    akademy-2013-day-two
---
<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

Akademy 2013 in Bilbao finished its first day in grand style with a party and great music. Day Two had another impressive line-up of talks. Day Two official business wrapped up with sponsor presentations and the Akademy awards.

Saturday after Akademy, people partied at Kafe Antzokia, some stayed until the sun came out. <strong>Las Dos D</strong> opened with traditional Cuban music, followed by <a href="http://adarrots.com/"><strong>Adarrots</strong></a>, a well-known Basque folk group playing Basque Celtic Fusion. We enjoyed the Basque folksongs; Basques in the audience sang along. The music and free&easy atmosphere got people up and moving. The musicians told stories through their music and their motions. At the end of their performance, the crowd wouldn't let them go. Dancing went on and on.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/adarrotsBand.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/adarrotsBand700.jpg" /></a><br />Adarrots (click for larger) <small><small>by Jonathan Riddell</small></small></div>

The Akademy atmosphere is inspiring. It is taking place in an exotic location with a profound energy and excitement. The Guggenheim Museum can be seen from the open terrace of the Akademy venue. Bilbao is a fusion of beautiful green hills, modern structures and venerable Spanish buildings. In a way, it resembles the KDE Community, an amalgamation of people from all over the world, people from different facets of life, different cultures. A beautiful city and a beautiful community. 

<h2>The presentations - Day Two</h2>
<strong>Samikshan Bairagya's</strong> talk, "How I used QML to make KStars more interesting" had appeal even to the non-coder. To Samxam "interesting" means "interesting for the Kstars user" referring to what the viewer wants to see in the sky. In his GSoC project, he provided a way to choose what to see in the sky map: stars, planets, nebulae, etc. He used QML to create this easy-to-use interface.

<strong>Oihane Kamara's</strong> talk covered the use of Marble in scientific projects. The presentation included multiple illustrations, including the final program prototype with real data. The discussion after the talk dealt mostly with Python bindings in Marble.

<strong>Timothée Giet</strong> gave a great demonstration about the power of Krita. He started drawing the KDE mascot, using many Krita capabilities, rotating the drawing space, using various brushes and colors to make a beautiful version of Konqui. Krita is an amazing application.

<strong>Jos van den Oever</strong> and <strong>Friedrich W. H. Kossebau</strong> shared "Slices of Calligra". They talked about problems with documents stored and edited in the Cloud, and proposed some solutions. They also explained <a href="http://www.webodf.org/">WebODF</a>, a JavaScript library to add Open Document Format (ODF) support to websites and mobile or desktop applications. WebODF is easy to implement and works with Calligra Words. We had the opportunity to test collaborative editing in OpenODF during the talk. Finally there's an online collaborative text editor similar to Etherpad but better in many ways.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/D_edVisheshMartin.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/D_edVisheshMartin300.JPG" /></a><br />vHanda can't stop hacking <small><small>by Martin Klapetek</small></small><br />David Edmundson, Vishesh, Martin Gräßlin</div>

Typical of Akademy, throughout the presentations, <strong>David Faure</strong> and <strong>Vishesh Handa</strong> sit and hack together. Conflicts between the KMail GUI and Nepomuk are being sorted out. It is an interesting pair - Vishesh, jumpy and talkative, David, quiet and laid-back.

After the coffee break, it was time for the <strong>Student Programs</strong> presentations (Google Summer of Code, Season of KDE, Google Code-In and Outreach Program for Women) moderated by <strong>Lydia Pintscher</strong>. The presentations covered Tomahawk, KDE-Telepathy, Keyboard, Marble, collaborative text editing, connecting your phone to your KDE and a local adjustment tool for digiKam. It was amazing to see how exuberant students have made interesting and useful contributions to KDE through the various mentoring programs.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/IllyawithGCIMentors90.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/IllyawithGCIMentors700.JPG" /></a><br />Illya (in the Chrome shirt) with mentors (click for larger)  <br /><small><small>by Oksana</small></small></div>

<strong>Illya Kovalevskyy</strong> was one of two KDE grand prize winners for Google Code-in 2012. He made a short presentation on his GCI experience. Illya said, "Before I started GCI, I thought I knew Qt, now I realize that I did not. During GCI, my Qt skill jumped by 300%. Before GCI, I contacted my geeky friends if I had problems when programming, now I can solve things myself by just reading appropriate documentation." Illya ended with "The KDE Community was extremely, extremely, extremely, I don't even know how to express it" to sudden applause. It was amazing to see this young guy standing there and confidently expressing himself and sharing his experiences. He clearly demonstrated his enthusiasm, his development skills and progress, and also his pride in being welcomed into the KDE community. It was inspiring to listen to him. If you have not participated as a mentor or admin, jump in! The students are wonderful.

The other KDE grand prize winner, <strong>Mohammed Nafees</strong>, did not make a presentation, but he provided some background information about his experience with KDE and Google Code-in. Here's Mohammed's story:

<blockquote>I was introduced to the KDE community in 2011 doing a couple of tasks for Marble during Google Code-in. After GCI '11, I polished my programming skills and continued working on Marble. In Google Code-in 2012, I worked with KDE (of course) throughout the contest, and completed 71 tasks. This included tasks for Marble, Pairs, code checker issues and much more.</blockquote>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/MohammedNafees.JPG" /><br />Mohammed Nafees - Google GCI grand prize winner from KDE</div>

<blockquote>
For Marble, I implemented the new Web popup that replaced the previously used TinyWebBrowser. I also implemented the new look and functionality of the Navigation plugin. I implemented the GetHotNewStuff feature in Pairs. The whole GCI program timeline was awesomely exhilarating. I enjoyed doing every task and interacting with my super cool mentors. The whole experience was something that I'll cherish throughout my life. </blockquote>

<blockquote>
Last January, I got my KDE developer account; the beginning of the KDE era of my life. KDE is much more than just a free software organization. It is FAMILY. The people here are so friendly and helpful. I feel extremely privileged to be a part of this community. I was fortunate to be selected as a grand prize winner of Google Code-in 2012 for KDE. I owe this achievement to the constant support of my KDE mentors. Thanks also to my family who were like my backbone, supporting me throughout the contest. </blockquote>

<blockquote>
Currently I am working on Marble, building the best user experience of Marble on OS X. I am working on a Chrome native client port of Marble so that Chromebook users can also use and enjoy Marble. </blockquote>

<blockquote>
Being able to attend Akademy 2013 is another dream come true. It is highly enjoyable to interact with my fellow community members. I met all my mentors in person and I feel so lucky to be here. It's a pleasure meeting everyone, and I'm looking forward to the upcoming events of Akademy.</blockquote>

<strong>Lydia</strong> remained on stage and started her talk "Negotiation Theory for Geeks" or How to Get What You Want with a Minimum of Misery. She talked about the relation between developers and the art of negotation. Key take-aways were that you should always focus on interests not on positions. Talk to each other and actively listen to what the other has to say. Proper communication and understanding the other person's ideas patiently while keeping an open mind is important. Consider ahead of time a fallback position if the negotiation fails—a BATNA (Best Alternative to a Negotiated Agreement); it doesn't make sense to settle for less than your BATNA. When there is trouble, TALK to the person, CARE about the other person, and look for common ground.

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://byte.kde.org/~duffus/akademy/2013/groupphoto/"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2013-group-photo700.jpg" /></a><br />Akademy 2013 attendees (click for larger) <small><small>by Knut Yrvin</small></small></div>

Before lunch, everybody gathered right outside the Akademy venue. A photograph of the entire group was captured against the beautiful backdrop of Bilbao including the Guggenheim. Along with the usual nerdherding, <strong>Knut Yrvin</strong> showed his award-winning breakdancing skills to entertain us (blaming his age for refusing to dance on his head).

<h2>Kevin Ottens on KDE Democracy - Community Keynote</h2>
In the beginning, the Kool Desktop Environment was about software, not community. Then, software modules were created and teams formed around them. At an earlier KDE gathering, Matthias Welwarsky said, "KDE is not a project, it is an ongoing process". The KDE rebranding a few years ago came from acknowledging that KDE is the community, not the software. So we moved from working ON the Kool DE to working FROM KDE. Now that we have the environment, are we done?

Democracy? It is a journey, not a destination. Democracy needs citizens. Who are the KDE citizens? All of us from users who are actively engaged to all developers.

What is a KDE project? A definition was needed, internally and for projects that are considering joining KDE. Our common root is VALUES: open governance, free software, inclusivity, innovation, common ownership and end user focus. Many projects make one community. We're fortunate to have both cohesion and diversity.

So, we wrote a Manifesto together. What are the effects of the Manifesto? Is it the end of the process? No, new projects are joining.

What's next? This was a stepping stone; we must complete the work on democracy. Our culture needs governance documentation; for both Community and Project. Now, we rely on auto-organization, which is good, but we must not allow it to devolve into chaos. It's important to Respect the Elders (based not on age, but experience). Some turnover is good, but we retain wisdom. We need Alliance Management, such as we have with Qt. Perhaps we need a "Foreign Office" to handle these alliances. In Q&A, there was a discussion about elders. What makes someone an elder? Is KDE a meritocracy?

There are ongoing conversations about how the Community can make further progress.

In their talk <em>People in KDE: metacontacts for all KDE apps</em>, <strong>Martin Klapetek</strong> and <strong>Vishesh Handa</strong> (The Excited One) spoke about the value of multiple contact sources, not just KDE stuff. With Nepomuk, data already on the system can be reused. KPeople is a library that wraps around Nepomuk data. It is flexible, specific, provides ready-to-use information and invokable actions (such as a phone call from a widget). It can include live information, such as when people are online. Kpeople includes:
<ul><li>PersonsModel, the main model, which fetches only what is needed; extensible with plugins for external data</li>
<li>PersonData fetches all information on a single contact; loaded by id or URI</li>
<li>simple API</li>
</ul>
The current state is a stable API, usable from QML. KPeople will be released with next KDE Telepathy in September. A new address book integrated with KDE-PIM integration is planned. It is open to be extensible by design.

ThreadWeaver is a programming library developed for KDE by <strong>Mirko Böhm</strong>. It provides an easy way for developers to take advantage of multi-core processors. In ThreadWeaver, the workload is divided into individual jobs, and the relationship between jobs is defined (completion order and priority). From that information, ThreadWeaver works out the most efficient way to execute jobs. Krita has implemented visual filter previews using ThreadWeaver to prevent GUI lockups. Mirko used several examples to illustrate the basic usage patterns: single jobs and sequences. He also did a survey on the features people want to have added.

"Speech recognition" alone is not an application, according to <strong>Peter Grasch</strong>. There is considerable differences between specific use cases. Yet there must be a goal to focus on or the scope of this project is too wide. The first focus of the project was on dictation (writing texts). Peter gave a brief introduction about application internals. The  demo used the Acoustic model from Voxforge (voxforge.org) and language models from a variety of sources. Speech recognition generally is not sufficiently robust. There are a lot of easy opportunities available. Beyond those, there is a lot of hard work to do. The technology is interesting and rewarding, the project could use more people who are up to the challenge.

<strong>Milian Wolff</strong> spoke about making applications run faster. The motivation: Faster seems better; it should run everywhere (not just on desktops), and do more. Prepare for <em>Apps on Speed</em>: always enable optimization and debug symbols. testing, testing, testing-make benchmarks to test against. Prevent regressions, keep functionality-don't over-optimize or micro-optimize (almost all of your work should be in what makes a real difference, not in making tiny improvements). Knowledge is king; the better you know your code, the better you can optimize. Get advice from the old-timers. A better algorithm often yields more performance than optimizing a bad alternative. Be aware that faster code might be slower for smaller datasets. Tooling: use the right tool for the job. Some examples: <a href="https://perf.wiki.kernel.org/index.php/Main_Page">Linux Perf</a> (huge potential, needs UI, preferrably KDE); <a href="http://en.wikipedia.org/wiki/VTune">Intel VTune Amplifier</a> (not Free, free for non-commercial use, good UI); Valgrind: Callgrind and Massif

The technical talk <em>Qt Quick Tooling</em> by <strong>Kai Koehne</strong> presented the various tools available to KDE developers. Kai would like to see KDE developers embrace and extend the tooling infrastructure. There are tools for coding, debugging, profiling, and deploying.

<h2>Akademy Awards</h2>
At the end of the day, it was time for our traditional Akademy closing ceremony.

<h3>Sponsors</h3>
First, the sponsors came on stage, one by one, to tell us about themselves. <strong>Digia</strong> engineer <strong>Kai Koehne</strong>, representing our Platinum sponsor, explained that Digia has great expectations for the future of Qt, and that KDE plays a major role in the Qt ecosystem. 

Team Lead <strong>Sebastian Kügler</strong> asked the whole <strong>Blue Systems</strong> (Gold sponsor) team to stand up so we could see what an awesome team they have working all over the KDE code base. He drew a lot of applause; when he remarked that we'd clap at anything, he got even more applause. 

<strong>Vladimir Minenko</strong> from <strong>Blackberry</strong> (the other Gold sponsor) went over Blackberry 10's architecture and remarked that Blackberry is very happy to support KDE, and said that they are on the look-out for new employees. 

Silver sponsor <strong>Google</strong> had no representative at the closing session. The KDE Community recognizes the exceptional support that Google provides to KDE with Google Summer of Code, Google Code-in, Akademy sponsorship and other smaller sponsorships.

Bronze sponsor <strong>Froglogic GmbH</strong> is a long time KDE supporter, delivering GUI testing solutions around Qt, as well as Java, Android, Mac and HTML to customers around the world. 

<strong>ICS</strong> is a North American service provider around Qt technologies. They stressed that they are fans of KDE and even more of Qt. 

<strong>Cornelius Schumacher</strong>, KDE e.V. Board President, thanked the <strong>Council of Bilbao</strong> and the <strong>Biscay Provincial Council</strong> for their practical and financial support. 

<strong>Open Invention Network</strong>, a patent licensing pool for free and open friendly enterprises and Akademy Supporter, had Linux Defender <strong>Armijn Hemel</strong> on stage to give a quick and dirty overview of how to protect ourselves against malicious use of trademarks and patents. 

<strong>Red Hat</strong> (another Akademy Supporter) didn't really need to introduce themselves, but they did point out that Red Hat is always looking for qualified engineers. 

There was an acknowledgement of the support from the <strong>University of the Basque Country</strong> and the <strong>Faculty of Engineering</strong>. Many thanks for providing a wonderful venue and for your hospitality. Akademy attendees are grateful for the Metro tickets provided to every person by <strong>Metro Bilbao</strong>. Metro Bilbao also showed an Akademy Welcome banner on 230 screens in 40 Metro stations.

<div align=center style="padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/akademy_metro95.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/akademy_metro350.jpeg" /></a><br />Bilbao welcomes Akademy<br /> (click for larger)</div>

<h3>Akademy Awards</h3>
After the sponsors, the <a href="http://community.kde.org/Akademy/Awards">Akademy Awards</a> committee took the stage. Last year's winners <strong>Lydia Pintscher</strong>, <strong>Kévin Ottens</strong> and <strong>Nicolás Alvarez</strong> (<strong>Camilla Boeman</strong> couldn't make it to Akademy 2013) presented the following awards:
<ul>
<li>Best Application: <strong>Eike Hein</strong> for his work on Konversation</li>
<li>Best Non-Application: <strong>Vishesh Handa</strong> (a very enthusiastic person; you all know who he is) for taking over the Nepomuk maintainer hat and rocking at stabilizing the beast</li>
<li>Jury's Award: <strong>Timothée Giet</strong> for shaping the future and community of Krita. <a href="http://krita.org/item/167-thimothee-giet-has-been-awarded-by-the-akademy">Krita.org interview</a>; also on Planet KDE</li>
<li>Jury's Award: <strong>Kenny Duffus</strong> for being the memory and soul of Akademy</li>
</ul>
<br />
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2013_akademy_awards.jpg" /><br />Timothée, Kenny, Eike fill-in, Vishesh  <small><small>by Knut Yrvin</small></small></div>

<h3>The Organization Award</h3>
The Organization Award went to <strong>Dani Gutiérrez and the whole Akademy team</strong> for organizing a wonderful Akademy. As a final surprise, Dani asked key people at Akademy onto the stage. From our youngest participants to accomplished contributors, he assembled a wide variety of KDE folk. Suddenly, music came from the side of the stage and a person appeared playing flute and drums, followed by a Basque dancer in traditional dress who demonstrated a few local dances. Dani explained that this ritual was to honor guests in the Basque Country. It has been presented for centuries to kings, queens and other luminaries! After the dance, the dancer greeted everyone on stage and left to huge applause followed by rhythmic clapping to the music that continued to play.

<h2>Special thanks</h2>
Thanks to the people who contributed to this story: Baltasar Ortega, Devaja Shah, Jos Poortvliet, Kenny Duffus, Marta Rybczynska, Matěj Laitl, Peter Grasch, Torrie Fischer and Valorie Zimmerman.

<h2>About Akademy 2013 Bilbao, the Basque Country, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.