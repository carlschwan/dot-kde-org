---
title: "KDE Commit-Digest for 3rd February 2013"
date:    2013-03-03
authors:
  - "mrybczyn"
slug:    kde-commit-digest-3rd-february-2013
---
In <a href="http://commit-digest.org/issues/2013-02-03/">this week's KDE Commit-Digest</a>:

<ul>
<li><a href="http://www.kdevelop.org/">KDevelop</a> adds basic support for type-defined function pointers; introduces fast documentation access; improves performance by caching the hash of indexed set nodes</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> adds a new effect: Glow on approaching Screen Edge; Non-composited Outline is ported to XCB</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> can now open <a href="http://en.wikipedia.org/wiki/Keyhole_Markup_Language">.kmz files</a></li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds an ability to transform layers recursively with a Transform Tool</li>
<li><a href="http://marble.kde.org/">Marble</a> sees work on a plugin to draw polygons and placemarks on a map</li>
<li><a href="http://skrooge.org/">Skrooge</a> shows buttons on dashboard widgets to discover the contextual menu.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-02-03/">Read the rest of the Digest here</a>.