---
title: "KDE Commit-Digest for 10th February 2013"
date:    2013-03-25
authors:
  - "mrybczyn"
slug:    kde-commit-digest-10th-february-2013
---
<ul><li><a href="http://community.kde.org/KTp">KDE Telepathy</a> fixes having a notification "a new contact has added you" even when you are offline, adds .desktop-based tubes channel approver</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> changed DBus interface to the FileIndexer for status messages</li>
<li>Improvements in kscreen API</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> changes as to when config ui are in a docker and when it is in a tool</li>
<li>Work on new dashboard in <a href="http://skrooge.org/">Skrooge</a></li>
<li><a href="http://amarok.kde.org/">Amarok</a> optimizes dynamic playlist generation.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-02-10/">Read the rest of the Digest here</a>.