---
title: "KDE Commit-Digest for 16th June 2013 "
date:    2013-07-21
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-june-2013
comments:
  - subject: "KDE Telepathy supports Hangouts?"
    date: 2013-07-23
    body: "KDE Telepathy's authentication supports Google accounts\" = support for Hangouts?"
    author: "User from Poland"
---
In <a href="http://commit-digest.org/issues/2013-06-16/">this week's KDE Commit-Digest</a>:

<ul><li>In kdelibs, kfiledialog remembers settings correctly</li>
<li>Plasma applet for Network Management adds OpenConnect VPN plugin</li>
<li><a href="http://en.wikipedia.org/wiki/KDE_Telepathy">KDE Telepathy's</a> authentication supports Google accounts</li>
<li><a href="http://kopete.kde.org/">Kopete</a> activates webcam preview in settings dialog only if webcam tab is selected</li>
<li><a href="http://zanshin.kde.org/">Zanshin</a> adds DataStoreInterface that allows to decouple <a href="http://pim.kde.org/akonadi/">Akonadi</a> access from the rest of the application (useful for testing, for example)</li>
<li><a href="http://www.periapsis.org/tellico/">Tellico</a> Collection Manager allows Bibtex text to be drag/dropped on the main window to import</li>
<li><a href="http://skrooge.org/">Skrooge</a> imports Microsoft Money documents (.mny), both protected and not</li>
<li>Bodega server (Plasma Active Appstore) sees much work, including implementation of posting of assets, publishing and "Partner Manager" role</li>
<li>Optimizations in <a href="http://userbase.kde.org/Trojit%C3%A1">Trojitá</a>, <a href="http://pim.kde.org/">KDE-PIM</a> and <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>GSoC work in <a href="http://edu.kde.org/kstars/">KStars</a> and <a href="http://www.kde.org/applications/games/kreversi/">Kreversi</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-06-16/">Read the rest of the Digest here</a>.