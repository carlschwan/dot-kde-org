---
title: "KDE Release Structure Evolves"
date:    2013-09-04
authors:
  - "smartboyhw"
slug:    kde-release-structure-evolves
comments:
  - subject: "Typo in the detailed schedule"
    date: 2013-09-04
    body: "Typo in the detailed schedule. KDE Workspaces 4.11 released in 2013 not 3013"
    author: "Anonymous"
  - subject: "Fixed"
    date: 2013-09-04
    body: "Thanks for catching this! Fixed."
    author: "oriol"
  - subject: "Plasma Workspaces Version Number?"
    date: 2013-09-04
    body: "I know that while the upcoming Plasma Workspaces will be the second major release (hence version \"2\"), would it be better to just call it Plasma Workspaces 5 to be more consistent with the use of Qt 5 & KDE Frameworks 5?   \r\n Another reason for using \"5\" instead of \"2\" is that the old version is being listed as \"4.11\" while the new version is listed as 2, which can be a little confusing.\r\n\r\nOne more thing: You use the term \"Plasma Workspaces\" in the body of the article, but then jump to using \"KDE Workspaces\" in the release schedule table... are the two interchangeable or is there some inconsistency here?\r\n"
    author: "DonCornelius"
  - subject: "Not decided yet..."
    date: 2013-09-04
    body: "There's afaik no decision about the naming of Plasma yet. Thing is, the developers would like to not have people call random parts of what we do \"KDE 5\" as you can never know what that refers to. We also make Amarok, Calligra, Valgrind, Bodega - and not too long ago, Qt-on-Android and ownCloud were KDE projects. When saying \"KDE 4.9\", did you include owncloud? Why not?\r\n\r\nMoreover, it IS plasma 2. The second Plasma. Just like KMail is at version 2.x now ;-)\r\n\r\nAnd about the Workspaces - that's the various form factors. Plasma Active (tablet), Plasma Desktop (guess...), Plasma Media Center etcetera. Compare with Microsoft Windows Phone  <-> KDE Plasma Desktop. KDE (who made it) Plasma (technology/platform) Desktop (form factor).\r\n\r\nSo, saying KDE Workspaces or Plasma Workspaces - or KDE Plasma Workspaces, yeah, that is all interchangeable...\r\n\r\nLemme see if I can add a picture :D"
    author: "jospoortvliet"
  - subject: "Plasma Frameworks 2"
    date: 2013-09-04
    body: "<cite>\r\n> No new features will be added to KDE Workspaces 4.11, and the bugfix releases will happen regularly, even after the release of Plasma Frameworks 2.\r\n</cite>\r\n\r\nLooks like you got confused by all the new terminology :)"
    author: "Victor"
  - subject: "Discussion on plasma-devel"
    date: 2013-09-04
    body: "We are currently discussing naming and release number on the Plasma devel mailing list (see https://mail.kde.org/pipermail/plasma-devel/2013-August/026230.html ). There are many ideas on the version number. There are overall many interesting concepts out there and version numbers seem to be not that important any more (tell me which Firefox version you are using without looking it up!)."
    author: "mgraesslin"
  - subject: "Bugfixes vs features in KDE Workspaces 4.11"
    date: 2013-09-04
    body: "KDE Workspaces 4.11 will get bugfixes even after Plasma Workspaces 2 is released. \r\n\r\nKDE Workspaces 4.11 will not receive new features."
    author: "kallecarl"
  - subject: "Plasma Frameworks 2?"
    date: 2013-09-04
    body: "I hope the mention of a \"Plasma Frameworks 2\" (in the first paragraph of the section \"Plasma Workspaces 2\" was a mistake. Otherwise the confusion would be perfect ;)"
    author: "Thomas Pfeiffer"
  - subject: "Fixed"
    date: 2013-09-04
    body: "Thank you."
    author: "kallecarl"
  - subject: "3013"
    date: 2013-09-04
    body: "Imagine the amount of bugs that can be fixed in a millennium ;)"
    author: "hussam Al-Tayeb"
  - subject: "New features..."
    date: 2013-09-05
    body: "Do QML rewrites of part of the desktop count as \"new features\"? I'm running the QML Desktop experimental containment with KDE 4.11, with no major issues."
    author: "Alejandro Nova"
  - subject: "I doubt it"
    date: 2013-09-05
    body: "There will most likely be minor features like in the KDE 3.5.x series, but I somehow doubt that a QML rewrite of a part of the desktop counts as a 'minor feature'. Most efforts are aiming for Plasma 2 anyway and while that will be all QML, it will be Qt5/Frameworks 5 based and those are sufficiently different that I doubt the devs will want to move things between Plasma Workspaces 2 and 1."
    author: "jospoortvliet"
  - subject: ":)"
    date: 2013-09-05
    body: "Made me laugh really hard :D"
    author: "MirzaD"
  - subject: "re: versions and names"
    date: 2013-09-05
    body: "True, most may not know what version of Firefox they have, but they do know and will identify the version of KDE they are using.  In that sense, I think version number actually is very much more part of the identity of KDE in many users minds today even if it is less so for other projects, and this I think was the original posters point.\r\n"
    author: "David Sugar"
  - subject: "KMail is not at version 2.x."
    date: 2013-09-06
    body: "KMail is not at version 2.x. The Aknonadi-based version is at 4.x (jumping directly from 1.x). The big Welcome Screen is pretty clear on that issue.\r\nKMail2 was released in alpha quality with KDE 1.1 and then shelved. KMail 3.x was skipped."
    author: "Anonymous"
  - subject: "I'm wrong, you're right."
    date: 2013-09-06
    body: "You're right, my bad. The developers refer to it as kmail2 (see bugzilla, too) but the official version number follows the Applications release."
    author: "jospoortvliet"
  - subject: "release numbering"
    date: 2013-09-06
    body: "I'd strongly agree that these naming conventions are getting difficult, especially in light that (1) the KDE structure is being further divided into separate projects, and (2) developers can't even use the same numbering system as how they market the product (specifically, kmail2 vs KMail 4.x).  It gets difficult when reporting bugs and the version I'm running isn't even listed in the bug tracker (kopete, rekonq, and kmail have all had this issue for me).  Is the numbering different?  is my package old? is this unsupported software?\r\n\r\nI'd support moving everything to being named \"5 .x\" to reflect that this is based on Qt5 and is designed for the KDE Plasma version that was built with Qt5 (Plasma 2 in this case, but it'd be called Plasma 5 to reflect the 5 idea).\r\n\r\nLooks like the KDE Project has a great future, which I am excited for, but this confusing division of parts and the confusing numbering scheme seem to be shooting the project in the foot.  It's easy to discuss the Microsoft desktop (XP or 7, that's all it takes) or Apple releases (10.7).  They've marketed this to intentionally be an easy concept.\r\n\r\nThe concept of multiple FLOSS *Nix desktops is already confusing to most people, but to say \"Oh you'll love the Plasma Workspaces 2, of course that's newer than Workspaces 4.11, and running KDE Framework 5 applications...\"  Seriously?  There is a reason for marketing, which I dislike, but they wrap this stuff in a far more coherent and acceptable whole."
    author: "lefty.crupps"
  - subject: "firefox 23"
    date: 2013-09-06
    body: "I'm running Firefox 23, I know this because I see release info with One Product and One Release Number discussed on Google News, on Slashdot, on Ars, on Wired.\r\n\r\nKDE Plasma Workspaces 2 with KDE Framework 5 and KDE Applications 5.1 is not going to get non-technical people interested.  The naming convention is too ugly; people want simple."
    author: "lefty.crupps"
  - subject: "Please"
    date: 2013-10-01
    body: "Please don't turn KDE into tablet pandering garbage, and please do not try make it look and feel like OSX.\r\n\r\nWe are counting on you to keep the desktop environment useful and productive.\r\n\r\nGod speed."
    author: "daps"
---
With the upcoming Frameworks 5, the KDE Release Team will decouple the release cycles of Workspaces, Applications and Platform. While Applications will have more releases in the 4.x series, the Plasma Workspaces team has decided to focus on Workspaces 2. The recently released 4.11 version of Plasma Workspaces will have Long Term Support. Meanwhile, work on Frameworks 5 is progressing, making KDE technologies ready for the future. What does all this mean?

<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/releaseplan.png" /></div> 
<h2>Release plans</h2>
As mentioned in the <a href="http://www.kde.org/announcements/4.11/plasma.php">announcement</a>, the Plasma Workspaces 4 series will no longer receive major feature work. The Plasma Workspaces team is focused on the transition to new capabilities made possible by Qt 5. KDE Applications will continue to be released in the 4.x series, while Platform and Workspaces only receive fixes as part of the release cycle. Users are advised to continue with the 4.x series of KDE Platform, Workspaces and Applications. Frameworks 5 (the new identity of Platform) and Plasma Workspaces 2 are under heavy development and best suited for developers and savvy users who can deal with frequent stability issues. People are free to use early versions of Frameworks 5 and Workspaces 2. However, there is a high risk of breakage.  

<strong>Decoupling</strong>
After the 4.x series, the KDE Release Team expects the releases of the Frameworks, Workspaces and Applications to diverge. Separate release cycles will benefit both users and developers. Individual components can skip releases if they require a longer development cycle. Separate cycles will encourage developers to have an always-releasable master while work goes on porting to KDE Frameworks 5. There is more emphasis on continuous integration and other automated testing to improve development work.

<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/frameworks.png" /></div> 
<h2>Frameworks 5</h2>
KDE Frameworks 5 (note the plurality of framework<strong>s</strong>) is the successor to KDE Platform 4, bringing significant technical differences and a change in focus. It will be the first release of KDE libraries based on Qt 5, which brings significant improvements to users. New technologies are being introduced and libraries are being cleaned up, reviewed and brought up to date with new standards. At the same time, the team is making the development platform more modular and making it easier to reuse solutions in a wider range of platforms and devices, including desktop and mobile. Technologies such as <a href="http://qt-project.org/doc/qt-5.0/qtqml/qtqml-index.html">QML</a> allow KDE developers to take advantage of a leading graphics rendering engine, and allow for more organic and fluid user interfaces across devices.

<div style="width: 302px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDE%20Frameworks%205%20clean.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE%20Frameworks%205%20clean-wee.png" /></a><br />Frameworks 5 separates libraries into modules with clear dependencies.</div> 
<strong>Benefits for all Qt developers</strong>
An important goal of KDE Frameworks 5 is to bring the benefits of KDE technology to Qt5 users outside the KDE Community. Libraries are split into distinct components, making it possible for Qt developers to take components without dragging in other unnecessary libraries. Functionality is also being moved 'down the stack' to Qt libraries, which is made possible by the openness of the Qt Project.

<strong>Schedule</strong>
The Frameworks team plans on releasing a first preview of Frameworks 5 in December 2013 and aims for a final release in the first half of next year.

More information on Frameworks 5 will continue to be available as more detailed articles, blogposts and technical papers are published about KDE Frameworks 5. "<a href="http://vizzzion.org/blog/2013/01/the-road-to-kde-frameworks-5-and-plasma-2/">The Road to Frameworks 5</a>" offers more details.

<h2>Plasma Workspaces 2</h2>
The current KDE Workspaces 4.11 will be supported for at least 2 years with bugfixes and translation improvements while development work is focused on Plasma Workspaces 2. No new features will be added to KDE Workspaces 4.11; however bugfix releases will happen regularly, even after the release of Plasma Workspaces 2.

<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma2.png" /></div> 
<strong>Improvements</strong>
Plasma Workspaces 2 will be a new version of KDE Workspaces based on KDE Frameworks 5 and Qt5. Plasma Workspaces work takes advantage of the source and binary compatibility break of Qt5 to do necessary updates to the architecture.

The biggest change in Plasma Workspaces 2 is the move to <a href="http://qt-project.org/doc/qt-5.0/qtquick/qtquick-index.html">Qt Quick</a> and the dynamic QML user interface language. Plasma Workspaces 2 implements Plasma Quick, which is Qt Quick plus a number of Plasma integration components for themes, compositor interaction, internationalization, data access and sharing, configuration, hardware and more. The new release brings developers a leaner Plasma Development API and dependency chain, making development easier and more efficient. For users, the move to Quick and QML delivers consistently fluid interfaces with lower resource usage and better adaptability to a wide variety of devices.

<strong>Schedule</strong>
The Plasma team plans a first preview release of the new Plasma Workspaces 2 in the first quarter of 2014, targeting a final release in the second quarter.

Read more <a href="http://aseigo.blogspot.ch/2013/05/plasma-workspaces-411-long-term-release.html">about Long Term Support</a> and <a href="http://dot.kde.org/2013/04/24/plasma-pow-wow-produces-detailed-plans-workspace-convergence">plans for Plasma Workspaces 2</a>.

<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/applications.png" /></div> 
<h2>Applications</h2>
KDE Applications will continue to have feature releases in the 4.x series, combined with future bugfix releases of KDE Platform 4 and KDE Workspaces. For the convenience of release management and packaging, version numbers will most likely continue to increase together. KDE Applications 4.12 are scheduled for release on December 18, 2013.

<strong>Schedule</strong>
The release schedule for 4.13 will be created after the release of Applications 4.12, after the Release Team evaluates the results of the shorter 4.12 release cycle. At present, the 4.13 Applications release is scheduled to happen in the second or third quarter of 2014.

There is currently no concrete plan for porting KDE Applications to KDE Frameworks 5, although some teams have started experimenting with this. A full scale move to Frameworks 5 is not expected until the new Frameworks and Plasma platforms are ready. Users can rely on at least 2 more releases of KDE Applications with the current underlying 4.x technologies.

<h2>Timeline and Plan</h2>
In summary, change is coming, and the KDE Community is committed to providing a stable, reliable desktop before, during and after the transition. KDE Workspaces 4.11 will be supported for at least 2 years while the Team prepares for the first version based on Qt5 and Frameworks 5, planned for the second quarter of 2014. The KDE Frameworks team will release the first preview of KDE Frameworks 5 by December 2013, with a final release planned for the first half of 2014. Applications will stay on the 4.x series with the 4.12 release scheduled for the end of 2013 and at least one more release in mid-2014.
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Schedule2.png" /></div> 
<table cellpadding="2" style="border-top-width:2px; border-top-style:solid; border-top-color:#ccc; 
border-bottom-color:#ccc; border-bottom-style:solid; border-bottom-width:2px;" >
<Caption>Detailed schedule</Caption>
<tr><th>Release</th><th>Expected date</th></tr>
<tr><td>KDE Frameworks 5 preview</td><td>December 1, 2013</td></tr>
<tr><td>KDE Frameworks 5 release</td><td>first half 2014</td></tr>
<tr><td>KDE Workspaces 4.11</td><td>released August 14, 2013 (LTS for two years)</td></tr>
<tr><td>KDE Workspaces 2 preview</td><td>1st quarter 2014</td></tr>
<tr><td>KDE Workspaces 2 release</td><td>2nd quarter 2014</td></tr>
<tr><td>KDE Applications 4.12</td><td>December 18, 2013</td></tr>
<tr><td>KDE Applications 4.13</td><td>Q2/Q3 2014</td></tr>
</table>

These are major technological transitions for KDE. Schedules will be adjusted as necessary based on actual experience.


<em>Thanks to the KDE promo and release teams for help with writing and information.</em>