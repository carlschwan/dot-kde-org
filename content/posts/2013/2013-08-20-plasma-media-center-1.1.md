---
title: "Plasma Media Center 1.1"
date:    2013-08-20
authors:
  - "shaan7"
slug:    plasma-media-center-1.1
comments:
  - subject: "Nice"
    date: 2013-08-23
    body: "Looks nice."
    author: "Dennis"
  - subject: "Satelite"
    date: 2013-08-28
    body: "DVB-S?"
    author: "Jacobtey"
---
<div style="width: 310px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/pmc1.1-1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pmc1.1-1-wee.png" /></a><br />Plasma Media Center 1.1</div>
The KDE community is happy to announce the release of 1.1 for KDE's <a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma Media Center</a> &mdash; your first stop for media and entertainment created by the awesome KDE folk. Plasma Media Center is designed to provide an easy and comfortable way to watch your videos, browse your photo collection and listen to your music, all in one place.

<h2>New Features and Improvements</h2>
Users who tried the 1.0 release and gave feedback or reported bugs were a big help for the work towards 1.1. A major new feature is YouTube integration but the team also introduced smaller features and implemented many improvements in the flow of actions and keyboard support.
<div style="width: 310px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/pmc1.1-youtube.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pmc1.1-youtube-wee.png" /></a><br />New YouTube Mode</div>

<h3>YouTube Integration</h3>
The biggest new feature in this release is the brand new <strong>YouTube mode</strong>. The shiny new YouTube mode lets you search for videos on YouTube and watch them, right inside the media  center.

<h3>A better playlist</h3>
The playlist now <strong>shows more details</strong> including the artist name as well as duration of the media, wherever available. The team also added Drag and drop support for removing and rearranging media in the list and integrated a playlist shuffling feature.

<h3>Ease of use</h3>
Another major area of focus has been the ease of use. Plasma Media Center is meant to be used with a variety of input devices, from pointing (mouse) to keyboard and remote control. And it should be intuitive and obvious to use. A few prominent improvements:
<div style="width: 310px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/pmc1.1-playlist.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pmc1.1-playlist-wee.png" /></a><br />Improved Playlist</div>
<ul>
<li><strong>Complete keyboard/remote-like keys support</strong>: There were reports of hiccups in key navigation at places, from problems with the focus going haywire to keys not working. For 1.1 keyboard navigation has been reworked entirely using QML's recommended way of handling keys, and the team can now confidently promise you a butter-smooth operation.</li>

<li><strong>Better keyboard navigation for All Music mode</strong>: The All Music mode is one of the most complicated modes, going by the implementation. While it took blood and sweat and tears, keyboard navigation is now fully implemented and tested.</li>

<li><strong>Places integrated into browsing flow</strong>: The places panel that appeared in 1.0 was rather clumsy to use and has now been integrated with the normal browsing flow. This makes it feel far more natural and of course lets you actually enjoy the benefits of quickly finding your favorite media.</li></ul

<h3>And more</h3>
Of course, there are many more small enhancements and bugfixes all over. The team is are confident that PMC has become a far better and more usable product and, reflecting that, they made it possible to associate PMC with music and videos on your system!

<div style="padding: 1ex; margin: 1ex; border: 1px solid grey;">
<iframe width="230" height="165" src="//www.youtube-nocookie.com/embed/w2g4hsYBZPE" frameborder="0" allowfullscreen suggestedQuality="large"></iframe>  <iframe width="230" height="165" src="//www.youtube-nocookie.com/embed/VZppWD_8IV8" frameborder="0" allowfullscreen></iframe>  <iframe width="230" height="165" src="//www.youtube-nocookie.com/embed/pKxvuIimsAU" frameborder="0" allowfullscreen></iframe>
<br />Videos on YouTube of PMC in action [<a href="http://www.youtube.com/watch?v=w2g4hsYBZPE">1</a>] [<a href="http://www.youtube.com/watch?v=VZppWD_8IV8">2</a>] [<a href="http://www.youtube.com/watch?v=pKxvuIimsAU">3</a>] </div>
<h2>The future</h2>
Planning and work has already started on <a href="http://community.kde.org/Plasma/Plasma_Media_Center/Release_1.2">PMC 1.2</a>. Some highlights of our plans:
<ul><li>completely redesigned "All Music" mode, with more information and Album covers , even better navigation</li>
<li>improved settings screen (and options) for adding media collection for Indexing</li>
<li>Implement search for local file browsing</li>
<li>folder previews for folders containing images</li></ul>
This promises, again, to be an exciting release. If you're interested in the above or other features, join us in hacking on this! The team hangs out on IRC in the <a href="irc://freenode.net/#plasma">#plasma channel</a> on freenode, ask how you can get involved or start reading <a href="http://community.kde.org/Plasma/Plasma_Media_Center">the wiki page</a> where  you can find info on everything around the code.

<h2>Get it</h2>
Some distributions (<a href="http://www.kubuntu.org/news/plasma-mediacentre-1.1">Kubuntu</a>) will be offering packages of PMC 1.1 however you can also <a href="http://sinny.in/node/25">install</a> from <a href="http://download.kde.org/stable/plasma-mediacenter/1.1.0/src/plasma-mediacenter-1.1.0a.tar.bz2">source (tbz2)</a>

<h2>Thanks! And keep helping!</h2>
As always, thanks to all the developers, testers and people for giving useful feedback on improving Plasma Media Center. The team hopes you will enjoy your use of it. If you have found any bug in PMC or want to have your favorite feature included in future release please <a href="https://bugs.kde.org/enter_bug.cgi?product=plasma-mediacenter&format=guided">file a bug</a>.