---
title: "Volunteer at Akademy 2013 in Bilbao"
date:    2013-06-17
authors:
  - "kallecarl"
slug:    volunteer-akademy-2013-bilbao
---
Are you attending Akademy 2013 in Bilbao? Do you want to get something special out of your Akademy experience?
<div style="width: 250px; float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy2013logoYear.png" /></div>

Be a volunteer. The Akademy Team needs motivated people who are passionate about Free and Open Software, especially KDE, one of the foremost communities of any kind in the world. There is a variety of tasks to fit with diverse interests and skills.

Are you interested in helping to make the conference memorable for attendees? Do you want to work with the Akademy Team to produce the best Akademy ever?

<h2>What's In It For You?</h2>

Volunteering puts you in the middle of the action. It's a great opportunity to make new friends and contacts. Being an Akademy volunteer strengthens your ties to the KDE Community.

Develop social and relationship skills. Some people are naturally extroverted, but many technical contributors have a hard time meeting new people. Volunteering at Akademy gives you the opportunity to work with a friendly, dedicated team that is committed to your success.

While building new skills may be beneficial to some people, it's not a requirement for choosing to volunteer at Akademy. Your task-related skills will be useful, but the most valuable skills you bring may be an open mind and a positive attitude, a willingness to do whatever is needed.

Make a difference. There's joy in serving others and helping to improve the KDE Community.

Volunteers receive an attractive, exclusive Akademy 2013 Team t-shirt. You will stand out at future KDE events and local free and open software meetings.

<h2>Take action</h2> 
<a href="http://community.kde.org/Akademy/2013#Volunteers">Sign up to volunteer</a> at Akademy 2013 in Bilbao.

More details and sign up instructions are available at the Akademy 2013 site on the <a href="http://akademy2013.kde.org/volunteer">Call for Volunteers Page</a>.

<h2>Akademy 2013 Bilbao, the Basque Country, Spain</h2> 

For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2013.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities. 

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.
