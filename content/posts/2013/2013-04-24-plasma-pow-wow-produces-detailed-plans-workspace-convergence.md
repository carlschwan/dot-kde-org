---
title: "Plasma Pow-wow Produces Detailed Plans for Workspace Convergence"
date:    2013-04-24
authors:
  - "sebas"
slug:    plasma-pow-wow-produces-detailed-plans-workspace-convergence
comments:
  - subject: "awesome"
    date: 2013-04-26
    body: "thanks to you all !"
    author: "Ilja"
---
<div style="width: 354 px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/code-review.jpg" /></a><br />Code review during the Plasma sprint</div> 

<p>Last week, members of the Plasma team met in Nürnberg, Germany to discuss open questions on the road to Plasma Workspaces 2. The meeting was kindly hosted by <a href="http://www.suse.com">SUSE</a> and supported by the <a href="http://ev.kde.org">KDE e.V.</a>. For the Plasma team, the meeting came at a perfect point in time: porting of Plasma to a new graphics has commenced, is in fact well under way, and has raised some questions that are best discussed in a high-bandwidth setting in person.
</p>
<h2>Graphics Stack Options</h2>
<p>
The developers laid out the plans for an eventual release of Plasma Workspaces 2, based on KDE Frameworks 5 and Qt5. Wayland is also part of this plan. Starting with Plasma Worskpaces 2, Wayland will join X11 as a supported windowing environment, and all new development is taking this into consideration. Wayland is a replacement for most of the functionality which is today offered by Xorg. Wayland simplifies the graphics stack significantly with the goal of making every frame shown on screen perfect, something that is hard to achieve reliably with X11. Wayland's leaner graphics stack and improved security model also make it more appropriate for use on modern devices. As such, it is widely seen as the successor to X11 in the Free software ecosystem. Most of the gruntwork needed for this transition is already offered in Qt, however, there are still quite some X11-dependent code pathes in the KDE Frameworks, especially in KWin and the Oxygen widget style. While KWin is making excellent progress towards being able to be used as Wayland compositor, the future of the Oxygen widget style is still an unsolved problem.
</p>

<h2>Definition of Done</h2>
<p>
As with every transition, a clear <a href="http://community.kde.org/Plasma/PW2IsDoneWhen">definition</a> is needed of an important milestone: When is all of this ready to be used? The highest priority for the team is not disrupting the user experiences, so the baseline is to not introduce regressions in primary workflows when users update from KDE Plasma 4 to the upcoming release.
</p>
<p>
The idea is to not introduce a new user interface, but streamline the <a href="http://en.wikipedia.org/wiki/Design_language">design language</a> of the current workspace. Common and consistent elements for the user experience will be defined, and made available as <a href="http://community.kde.org/Plasma/lookAndFeelPackage">packages</a>. This goes along with less granularity in the workspace setup options, but fear not: The user will be able to mix and mash these elements to create a customized user experience. To this end, the hackers introduced a new type of Plasma packages: Look and Feel. A look and feel package combines code for UI elements such as the application switcher, activity switcher, splash screen, logout dialog and lock screen into a single package. The Look and Feel package can be swapped, either along with the shell package, or independently, allowing for great freedom in the choice of UI while delivering a great deal of consistency. This way of organizing the "vocabulary of the workspaces design language" will allow quick changes of the entire theme and allow downstream partners to ensure greater consistency at lower effort for their products. For a highly customized user experience, a tool will allow the user to easily remix the Look and Feel of the workspace.
</p>


<h2>One Shell to Rule All Devices</h2>
<p>
A generic shell is one of the things that is already in place. Rather than having one binary per workspace, with slightly different mechanics, the Plasma developers have merged these code bases into one. Elements of the <a href="http://community.kde.org/Plasma/shellPackage">shell</a> such as default configuration, setup of panels, toolbox, containments et cetera are now defined in a so-called shell package. The generic shell itself does not hold any assumption about the target device, or use case. The UI elements are all defined in QtQuick-based code. This makes the shell easier to hack on and more maintainable, while allowing for greater flexibility for both, users and developers.
</p>
<p>
One aspect of Plasma's device convergence strategy is transparent switching of the workspace User Interface. While Plasma does offer different workspaces as it is already (Plasma Desktop, Plasma Netbook, Plasma Active, Plasma Mediacenter), these pieces are still distinctive programs right now. The plan is to move all these different user experiences into Plasma packages, which can be switched at runtime. The switching itself can be manually invoked, but also based on heuristics such as input and output devices plugged in.
</p>
<p>
A user could, for example, plug a keyboard and a mouse into her tablet, and the workspace would automatically be switched from a Tablet and touch-focused UI to a more traditional Desktop setup. Connecting a TV to the device could swap the workspace to a media center interface.
</p>
<p>
Some of the code taking care of this is already in place, but not in a state yet where it could be demonstrated.
</p>

<h2>Quicker</h2>
<p>
The way for doing UI in Plasma Workspaces 2 is to use QtQuick. This allows us to offload graphics rendering to the GPU, making better use of graphics hardware, freeing the CPU for other tasks and conserving battery life, as the GPU is much more efficient in these operations.<br />
It does not require OpenGL drivers, however. Graphics on systems that do not support OpenGL can fall back to XRender or fully CPU based rendering. The worst case scenario is that the rendering will be done on the CPU, which is how the desktop layer is rendered today and so have the performance as it does now. To achieve performance parity even on systems which lack properly accelerated graphics drivers, usage of "heavyweight" graphics effects will be kept to a minimum. In the end, a system that is capable of running the current version of Plasma will also be able to run Plasma Workspaces 2, most likely with even better performance.
</p>
