---
title: "Plasmate 1.0: A Plasma Workspaces SDK"
date:    2013-03-05
authors:
  - "Aaron Seigo"
slug:    plasmate-10-plasma-workspaces-sdk
comments:
  - subject: "Debian packages"
    date: 2013-03-05
    body: "Any word on the possibility of Debian/Kubuntu packaging for Plasmate?"
    author: "Ken"
  - subject: "Excellent"
    date: 2013-03-05
    body: "Just what I need to finally experiment with plasma development.\r\n\r\nSuggestion - perhaps link to the plasma-devel subscription page (https://mail.kde.org/mailman/listinfo/plasma-devel) rather than just the mailto for the list address. [Editor note: added - thank you]"
    author: "Blackpaw"
  - subject: "Project Neon PPA has Plasmate"
    date: 2013-03-06
    body: "Project Neon PPA has Plasmate .DEB"
    author: "juancarlospaco"
  - subject: "UI Editor?"
    date: 2013-03-06
    body: "Just tested it, and looks promising. How about adding some kind of UI editor for Plasma widgets?\r\nIt would be great for beginners who want to start tinkering with Plasma/QML etc."
    author: "lokster"
  - subject: "Great! :)"
    date: 2013-03-06
    body: "\u00abPlasmate follows the UNIX philosophy of \"do one thing, and do it well\"\u00bb\r\n\r\nAh, if all developers would think the same... we would have a quarter of the projects we have for so many things but most of them well done (home many QT browsers are there around, 4, 5? And how many work well, none?)\r\n\r\nAnyway, a nice program to start developing widgets. I suppose it must be more limited than \u00abreal\u00bb programming, no? Anyway is a great program, necessary, I'd say, to let non programmers who have cool ideas participate in the KDE's development and enrichment. Thanks for your work."
    author: "MEDINT for Ch\u00e1vez"
  - subject: "Wrong version in About?"
    date: 2013-03-10
    body: "Could it be that in Help > About Plasmate the version string is wrong? i only get plasmate 0.1alpha3."
    author: "Anonversionus"
  - subject: "Cutting Edge"
    date: 2013-03-11
    body: "These are the tools that make the lives of EVERYONE so awesome! "
    author: "Jacky Alcine"
  - subject: "Excellent"
    date: 2013-03-19
    body: "Experimentation commences in 3...2...1..."
    author: "Ken"
  - subject: "Awesome!"
    date: 2013-05-23
    body: "I love this and I hope this becomes a default app in future kubuntu distros!"
    author: "Iqscope"
---
<p>The Plasma Workspaces team is excited to announce the first stable release of Plasmate: an add-ons SDK that focuses on ease of use.

<div align=center style="padding: 1ex; margin: 1ex; font-size: small;"><a href="http://makeplaylive.com/~aseigo/plasmate1.0/mainwidgets.png"><img style=" border: solid 1px lightgrey;" src="http://makeplaylive.com/~aseigo/plasmate1.0/mainwidgets_small.png"><br/>Plasmate 1.0</a></div>

<p>Plasmate follows the UNIX philosophy of "do one thing, and do it well". As such, it is not a general purpose <a href="http://en.wikipedia.org/wiki/Integrated_development_environment">IDE</a> but rather a tool specifically tailored to creating Plasma Workspace add-ons using non-compiled languages such as QML and Javascript. It guides each step in the process, simplifying and speeding up project creation, development, adding new assets, testing and publishing. The goal of Plasmate is to enable creating something new in seconds and publishing it immediately.</p>

<p>The launch page provides a running start. From here one can create a new Plasma widget, DataEngine, Runner search plugin, SVG desktop theme, window switcher (or "alt-tab") plugin, desktop effect or window manager script. Previous projects are also listed, and can be continued with a single click. Importing existing projects from disk or from <a href="http://kde-apps.org">KDE Apps</a> is also fast and easy.</p>

<div align=center style="padding: 1ex; margin: 1ex; font-size: small;"><a href="http://makeplaylive.com/~aseigo/plasmate1.0/startpage.png"><img style=" border: solid 1px lightgrey;" src="http://makeplaylive.com/~aseigo/plasmate1.0/startpage_small.png"><br/>New, open and import in one simple interface.</a></div>

<p>The project environment reflects the creative workflow. Shortcuts for managing save points, previewing, installing and publishing, keeping project notes, reading documentation and managing assets are all within easy reach. As part of this workflow-based approach, saving progress to disk is handled automatically. A revision control system is transparently integrated to create, view and roll back to save points.</p>

<div align=center style="padding: 1ex; margin: 1ex; font-size: small;"><a href="http://makeplaylive.com/~aseigo/plasmate1.0/timeline.png"><img style=" border: solid 1px lightgrey;" src="http://makeplaylive.com/~aseigo/plasmate1.0/timeline_small.png"><br/>Using git has never been so easy!</a></div>

<p>Plasmate provides automatic packaging and editors for metadata and other configuration files. Thus the focus is on creating rather than mastering all the implementation details involved in how a Plasma package is assembled. Image viewers, an advanced text editor, debug output viewer and an interactive live preview complete the experience.</p>

<div align=center style="padding: 1ex; margin: 1ex; font-size: small;"><a href="http://makeplaylive.com/~aseigo/plasmate1.0/metadata_editor.png"><img style=" border: solid 1px lightgrey;" src="http://makeplaylive.com/~aseigo/plasmate1.0/metadata_editor_small.png"><br>Editing project metadata made easy.</a></div>

<div align=center style="padding: 1ex; margin: 1ex; font-size: small;"><a href="http://makeplaylive.com/~aseigo/plasmate1.0/kconfigxteditor.png"><img style=" border: solid 1px lightgrey;" src="http://makeplaylive.com/~aseigo/plasmate1.0/kconfigxteditor_small.png"><br>Creating a configuration file.</a></div>

<p>When the project is ready to be distributed, the Export Project system will create a compressed package which can be saved to disk, installed locally, published online or even installed to a device over the network via SSH for quick and easy prototyping. Combined with the project import capabilities of the start page, an existing add-on from KDE Apps can be imported, modified, tested it on multiple devices locally and then uploaded as a new creation to KDE Apps in a matter of minutes.</p>

<div align=center style="padding: 1ex; margin: 1ex;"><a href="http://makeplaylive.com/~aseigo/plasmate1.0/projectmananger.png"><img style=" border: solid 1px lightgrey;" src="http://makeplaylive.com/~aseigo/plasmate1.0/projectmananger_small.png"><br/><span style="font-size: small">Publish freely wherever and whenever.</a></div>

<p>Plasmate integrates several excellent Free Software tools in order to provide these features including <a href="http://kate-editor.org/">Kate</a> for text editing, <a href="http://git-scm.com/">git</a> for revision control and <a href="http://www.gnupg.org/">GnuPG</a> for <a href="http://xkcd.com/1181/">cryptographic signing</a>. Also bundled with Plasmate are a set of helpful development tools including a DataEngine viewer (plasmaengineexplorer), a stand-alone Plasmoid viewer (plasmoidviewer), a wallpaper plugin viewer (plasmawallpaperviewer) and a remote widgets browser (plasma-remote-widgets-browser).</p>

<p>A screencast demonstrating the features of Plasmate 1.0:</p>

<div align=center><iframe width="560" height="315" src="http://www.youtube.com/embed/yLjj46oWRFw" frameborder="0" allowfullscreen></iframe></div>

<p>Plasmate has been made possible through the combined efforts of 46 different developers and numerous translators who have localized Plasmate in a variety of languages. The current maintainers are Giorgos and Antonis Tsiapaliokas. Find and interact with Plasmate developers in #plasma on irc.freenode.net or on the <a href="https://mail.kde.org/mailman/listinfo/plasma-devel">plasma-devel@kde.org mailing list</a>.</p>

<p>Download the <a href="ftp://ftp.kde.org/pub/kde/stable/plasmate/1.0/src/plasmate-1.0.tar.gz">source code for Plasmate 1.0</a> or clone the source code repository via the <a href="https://projects.kde.org/projects/extragear/sdk/plasmate">Plasmate project page</a>.</p>