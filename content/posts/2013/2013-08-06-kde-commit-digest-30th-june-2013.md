---
title: "KDE Commit-Digest for 30th June 2013"
date:    2013-08-06
authors:
  - "mrybczyn"
slug:    kde-commit-digest-30th-june-2013
---
In <a href="http://commit-digest.org/issues/2013-06-30/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> sees a series of menu changes in the menus: playlist-related actions are harmonized, in Collection Browser levels: "Artist" level is renamed to "Track Artist", its behaviour is changed and it replaced by "Album Artist" by default</li>
<li><a href="http://www.krita.org/">Krita</a> can smooth the pressure in Weighted Smoothing mode</li>
<li>KDE Telepathy adds an option to always ask for file transfer destination directory</li>
<li><a href="http://akregator.sourceforge.net/">Akregator</a> support nested xml:base attributes</li>
<li>Optimizations in <a href="http://pim.kde.org/">KDE-PIM</a>, <a href="http://nepomuk.kde.org/">Nepomuk</a>, <a href="http://www.kde.org/applications/multimedia/juk/">JuK</a>, kdegames, <a href="http://www.digikam.org/">Digikam</a>, <a href="http://dolphin.kde.org/">Dolphin</a></li>
<li>GSoC work in <a href="http://plasma.kde.org/">Plasma</a>, <a href="http://edu.kde.org/kstars/">KStars</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-06-30/">Read the rest of the Digest here</a>.