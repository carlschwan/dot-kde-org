---
title: "KDE Outreach Program for Women"
date:    2013-04-13
authors:
  - "valoriez"
slug:    kde-outreach-program-women
---
We are pleased to announce that KDE will take part in the <a href="https://community.kde.org/OutreachProgramForWomen">Outreach Program for Women</a> (OPW) this year. OPW started in 2006 with an intention to reach talented women who are passionate about technology, but who may be uncertain about how to start contributing to free and open software projects. Since its beginning, OPW has included commercial and non-profit organizations that are leaders in free and open software.

The KDE Community is offering one internship in the program, which runs from June 17 to September 23, 2013. The internship is sponsored by <a href="http://www.kdab.com/">KDAB</a>. The <a href="https://live.gnome.org/OutreachProgramForWomen#Application_Process">well-documented application process</a> introduces collaboration right from the beginning. It starts with choosing a project and finding a mentor. The applicant makes a small contribution (perhaps a bugfix or documentation edits), and a reviewer provides feedback on that work. When these steps are complete, an application can be submitted. The application deadline is May 1st.

The KDE Community supports the Outreach Program for Women because we want to make strong connections with talented people who will recognize the value of continuing to contribute to KDE. The Program encourages people to work on testing, design, documentation, marketing, and other areas, because there's more to technology than software code. OPW is particularly important because we are committed to increasing the number and proportion of women in free and open source technology.

Prospective interns who are interested in knowing more about contributing to KDE should consult <a href="http://techbase.kde.org/Contribute">the wiki page on Contributing</a> and the <a href="http://flossmanuals.net/kde-guide/">KDE Guide</a>.

Most KDE communication is done through mailing lists. KDE has two mailing lists for our mentoring programs:
<ul>
<li><a href="https://mail.kde.org/mailman/listinfo/kde-soc">KDE SoC</a> for Google Summer of Code, Google Code-in and Season of KDE</li>
<li><a href="https://mail.kde.org/mailman/listinfo/kde-women">KDE Women</a> for women interested in getting started and staying involved in the KDE Community.</li>
</ul>

IRC is also an important communication vehicle for KDE. Counterparts of the mailing lists are on irc.freenode.net: #kde-soc and #kde-women. Program coordinators are available on both mailing lists and in both IRC channels: Lydia Pintscher, Nightrose on IRC and Myriam Schweingruber, Mamarok on IRC.

Some useful project ideas have been collected at the bottom of the <a href="https://community.kde.org/OutreachProgramForWomen">OPW page on the wiki</a>. What about an awesome idea that is not among the listed ideas? That's cool. We love that! Contact mentors early on (contact information is included on the OPW wiki page). They will help ensure that your project is realistic and within the scope of KDE. For coding tasks, please refer to the Google Summer of Code <a href="https://community.kde.org/GSoC/2013/Ideas">Idea Page</a>. All of them are eligible for the Outreach Program for Women as well.