---
title: "4.10 Release of Plasma Workspaces, Applications and Development Platform"
date:    2013-02-06
authors:
  - "devaja"
slug:    410-release-plasma-workspaces-applications-and-development-platform
comments:
  - subject: "KDE 4.10"
    date: 2013-02-06
    body: "Great! Good luck!"
    author: "CSRedRat"
  - subject: "Congratulations!"
    date: 2013-02-06
    body: "Thank you, KDE team, for one more release of such fine software. Here's to it and for the ones to come!"
    author: "Anonymous___"
  - subject: "Awesome!"
    date: 2013-02-06
    body: "Congratulations! I'm downloading as I am writing this. :)"
    author: "mutlu"
  - subject: "Just the best!"
    date: 2013-02-06
    body: "KDE goes from strength to strength. Nothing on any other platform matches it. Congratulations on what is clearly the best release yet. :)"
    author: "A user"
  - subject: "Congratulations!"
    date: 2013-02-06
    body: "I've been testing this new release since the latest beta releases. It's a solid and stable release with important bug fixes and new features.\r\nI already upgraded all my systems to KDE 4.10."
    author: "Josep"
  - subject: "Help us spread the news"
    date: 2013-02-06
    body: "Don't forget to spread the news about this awesome release:\r\n* http://news.ycombinator.com/item?id=5175700\r\n* http://www.reddit.com/r/freesoftware/duplicates/17zshw/kde_software_compilation_410_released/\r\n* http://www.fsdaily.com/EndUser/KDE_Software_Compilation_4_10_Released\r\nAnd of course on twitter, identi.ca, Facebook, Google+ and everywhere you can"
    author: "Jure Repinc"
  - subject: "Best KDE ever!!!"
    date: 2013-02-06
    body: "KDE 4.10 is the best KDE ever!!! RC3 was super stable for me. But I found one regression in 4.10 final [edited: bug reported at bugs.kde.org]\r\n\r\nOther than that everything is perfect :)\r\nThanks to all KDE devs out there. You rock!"
    author: "nikola"
  - subject: "thank you"
    date: 2013-02-06
    body: "I'm a newcomer to KDE since  4.7, and obviously stayed because it's an amazing desktop. Based on my experience I'm pretty confident this release will not disappoint either."
    author: "istok"
  - subject: "Just amazed"
    date: 2013-02-06
    body: "Wow... 4.10 is the best KDE version ever. Everything goes smooth, and looks faster than ever. Love the new notifications, the menu, the look&feel. Congratulations!"
    author: "Jai"
  - subject: "Kmail in 4.10 is amazingly fast!"
    date: 2013-02-06
    body: "I don't know if it is kmail/akonadi itself or related to nepomuk, but after upgrading to 4.10 on kubuntu, selecting a new mail in kmail displays it instantly! Fantastic work all!"
    author: "Christian"
  - subject: "Excelente!!!!"
    date: 2013-02-07
    body: "Excelente!!!! \r\nme encanta el trabajo y todas las funciones de KDE... nunca eh podido estar mas comodo trabajando con Linux!!!"
    author: "gabriel"
  - subject: "Well done!!"
    date: 2013-02-07
    body: "Congratulations to all of you. You just did a great job.\r\nUpgraded to KDE 4.10 today and everything works perfectly.\r\n\r\nKeep up the the amazing work.\r\n"
    author: "Sieg"
  - subject: "thank you"
    date: 2013-02-07
    body: "i have shared this on my weibo account. There are many people love KDE in China. Thank you for your great job."
    author: "diseng"
  - subject: "Beautiful.."
    date: 2013-02-07
    body: "Good News, thank you all for the hard work..;)"
    author: "thiyagi"
  - subject: "Substantial improvements?"
    date: 2013-02-10
    body: "Substantial improvements to KDEPIM?? Sadly it seems as broken as before... :( The KDE team should really think about their focus. Shouldn't a stable PIM, akonadi, nepomuk be a thousand times more important that putting all that energy into this qt-quick stuff? Once again with this update not really improving anything I'm seriously thinking of ditching KDE after being a loyal user for 15 years now...It's sad :( But I'm tired of this constant state of bugginess. Sorry to spoil the party..."
    author: "Redm"
  - subject: "The KDE team should really"
    date: 2013-02-11
    body: "<blockquote>The KDE team should really think about their focus. Shouldn't a stable PIM, akonadi, nepomuk be a thousand times more important that putting all that energy into this qt-quick stuff? </blockquote>\r\nI hope you realize that there is no connection between doing QtQuick on the desktop (and KTouch) and bugs not being fixed in KDEPIM. Even if we would go to the extreme and forbid all feature development until all bugs in PIM are fixed not even one bug more would be fixed. Why? Because QML is not C++. A developer who knows QML does not need to have the skills to also work on PIM issues in C++.\r\n\r\nAnd of course KDE is a very volunteer driven community. There is no manager who can tell people to work on PIM instead of other stuff. Everybody is free to work on what suits them best. So why should a developer who doesn't care about PIM (being a web mail user) be forced to work on a field like that?\r\n\r\nThat said: KDE is a community, so also <strong>you</strong> could help to make PIM better. Nobody is blocked from developing on it and I just had the experience that it's quite easy to get a patch into KMail.\r\n\r\nDon't tell others what they should do, do it yourself :-)"
    author: "mgraesslin"
  - subject: "Basics of Community-Driven Software"
    date: 2013-02-15
    body: "Seeing it's such an illustrious release for all and I've seen some comments \"staining it\", I'm gonna rehash familiar \"inconvenient truths\" that everybody who loves KDE has heard. I say this in good will, since bad \"hyperbole publicity\" obscures other users from all the good things about KDE.\r\n\r\nI've been where you are, Average Person. Maybe you're used to software you bought. You get it, you install it, and it should <em> just work </em>. If it doesn't work and you don't know why, you're used to customer service and filing requests to get your software working perfectly. You probably feel entitled to it: after all, you paid good money and made the effort of installing this new software. So it is natural to feel disgruntled when something doesn't work. After all it costs time to fix it, and time is money. \r\n\r\nWelcome to Community Software.\r\n\r\nIn fact, money is THE KEY to all this, since you didn't pay KDE one single cent for the software. In fact, no user ever pays for it! So where do the resources come from to fix bugs, add features, etc? From <strong>VOLUNTEERS</strong>. Yup, people like you and me are part of the \"workforce\" behind KDE. Sure, companies invest in some developers here, and that's a GOOD THING. But this is still a community, so the rules you're used to don't apply here. You can't \"call customer support\" when you have a problem. There is no warranty the software will work. And there is no \"mediating corporation\" that configures KDE to work flawlessly with any particular device. All these things are provided by volunteers. I stress\u2014<strong>VOLUNTEERS</strong>.\r\n\r\nIf you bash KDE to the ground for a bug, you're bashing volunteers who put a lot of effort into creating a software <em>No One Will Ever Pay Them For</em>. Think about it. Would you like a horrific bashing for a favor you did to someone? That's how developers feel whenever they have to read \"stinky comments\" made by disgruntled people. Some have had such bad experiences with these sorts of comments that they stop looking at users' opinions altogether, simply to escape undeserved insults. This in turn furthers the cycle, alienating developers from positive feedback from other users, which lowers the probability of solving the problems you have. If you refrain from your bashing, and adopt a cooperative, respectable, and <strong>proactive</strong> stance to solving your particular problem, you'll get better results.\r\n\r\nTIP: When having an issue, 1) search for your problem on the Internet, 2) <a href=\"http://www.catb.org/esr/faqs/smart-questions.html\">ask nicely for a fix</a>, and 3) have empathy for the person who may be able to solve your problem.  They're people just like you, and they <em>don't earn their living from giving you support.</em>\r\n\r\nAlluding to the positive comments here, this is a *community*. We do this because we love it. If you find a bug, a friendly, correct, and cooperative report to <a href=\"https://bugs.kde.org/\">bugzilla</a> will be quite well received. Learn how to do it correctly, and you'll find it far more valuable than anything else we can offer in terms of support. Developers are straight to the point, since they have little time and tons of work to do. But if you're cooperative and work with them, you'll help each other overcome your individual problem -- not just for you, but for others as well.\r\n\r\nPlease don't call something \"non-working\" simply because your particular difficulty is not fixed. Surely you can't mean that <em>all of KDE</em> is worthless because of one thing that doesn't work the way you think it should? If you actually do feel that way, you'd best go. You are not welcome here. You will be probably be happier someplace where no mistakes are made or <a href=\"http://en.wikipedia.org/wiki/The_squeaky_wheel_gets_the_grease\">squeaky wheels get the grease</a>.\r\n\r\nKDE is closer to ideal than many other software collections around. So let's spend our energy celebrating another great release. With patience and a smile for the quirks left to solve! After all, if we achieved perfection, our work would be finished :-)"
    author: "Anonymous"
---
The KDE Community is proud to <a href="http://kde.org/announcements/4.10/">announce the 4.10 releases of KDE Plasma Workspaces, Applications and Development Platform</a>. This release combines the latest technologies along with many improvements to bring users the premier collection of Free Software for home and professional use. Special thanks to the KDE Quality Team for managing a comprehensive testing program for this release. Their work greatly assisted developers and played a key role in maintaining quality and stability. The highlights of the release are presented below. The <a href="http://kde.org/announcements/4.10/">full announcement</a> has more details and delightful screenshots.

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://kde.org/announcements/4.10/screenshots/plasma-4.10.png" /></div>

<h2>Qt Quick boosts Plasma Workspaces</h2>
The Qt Quick framework has been deployed in many Plasma Workspace components. In general, consistency, stability and ease of use have been improved considerably. There are improvements and new features across many of the elements that make up Plasma Workspaces. The <a href="http://kde.org/announcements/4.10/plasma.php">4.10 release of Plasma Workspaces announcement</a> has more information.

<h2>Additions and enhancements in KDE Applications</h2>
Kate, Konsole, Kopete and KDE PIM applications have received substantial improvements. Picmi is a new single player logic puzzle game, and changes have been made in many KDE games to deliver smoother gameplay. KTouch (the typing tutor) has undergone a transformation and offers a superior better learning experience. Marble has a new space orbiter visualization, along with many other nifty additions and improvements. The <a href="http://kde.org/announcements/4.10/applications.php">announcement for KDE Applications</a> has more information.

<h2>Development Platform - New and Improved with Qt Quick</h2> 
The expanded use of <a href="http://doc.qt.digia.com/qt/qtquick.html">Qt Quick</a> provides substantial benefits for developers. Many components have been updated to use Qt Quick exclusively in their user interfaces. Work continues on the creation of a comprehensive SDK for Plasma. The <a href="http://kde.org/announcements/4.10/platform.php">announcement for the KDE Development Platform</a> has more information.

<h2>Spread the Word</h2>
Non-technical contributors are an important part of KDE's success. While mega proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the 4.10 releases. <a href="https://bugs.kde.org/">Report bugs</a>. Encourage others to join the <a href="http://community.kde.org/Getinvolved">KDE Community</a>. Or <a href="http://jointhegame.kde.org/">Join The Game</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.10 releases.

Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from identi.ca, twitter, youtube, flickr, picasaweb, blogs and other social networking sites.