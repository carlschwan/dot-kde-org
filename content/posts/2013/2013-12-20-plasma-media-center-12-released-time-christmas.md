---
title: "Plasma Media Center 1.2 Released In Time For Christmas"
date:    2013-12-20
authors:
  - "sinnykumari"
slug:    plasma-media-center-12-released-time-christmas
comments:
  - subject: "Congratulations!"
    date: 2013-12-26
    body: "Well done guys, excellent work!  Looks great."
    author: "Bob"
  - subject: "looks amazing!!!"
    date: 2013-12-28
    body: "looks amazing!!!"
    author: "olo"
  - subject: "media server libraries?"
    date: 2013-12-29
    body: "Looks very nice.\r\n\r\nCan PMC access media server libraries such as plex or xbmc?"
    author: "curious to know"
  - subject: "Hopefully it will improve"
    date: 2014-01-04
    body: "Very basic at the moment but hopefully it will improve with more functionality and features. "
    author: "Pete M"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMC1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMC1_wee.png" /></a><br />Welcome to PMC 1.2</div>The KDE community has a Christmas gift for you! We are happy to announce the release of KDE's Plasma Media Center 1.2—your first stop for media and entertainment created by the Elves at KDE. Plasma Media Center is designed to provide an easy and comfortable way to watch your videos, browse your photo collection and listen to your music, all in one place. This release brings many refinements and a host of new features, making consuming media even easier and more fun.

<h2>New Features and Improvements</h2>
Working with feedback from users <a href="http://dot.kde.org/2013/08/20/plasma-media-center-1.1">since the previous release</a>, the team has implemented many cool new features and a variety of improvements and bug fixes.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCCover1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCCover_wee.png" /></a><br />PMC Artist Images</div> 
<strong>Improved Music Mode</strong>
A lot of effort went into improving the All Music mode so that you will have a good time navigating through your music—search by artist, album or just see it all.

<strong>Artist and Album Cover Retrieval</strong>
From now on, your collection will look better and it will be quicker to locate that album you want to listen to. While PMC already used album covers contained in your music files, this new release enables PMC to go to last.fm and fetch new album covers and artist images!

<strong>Folder Previews for Picture Browsing</strong>
While browsing for pictures, you can now see thumbnails of pictures that are inside folders, making it easier to identify which folders are interesting. This feature was much requested and the team is glad to be able to deliver it in this release.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCFolderPreviews.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCFolderPreviews_wee.png" /></a><br />PMC Folder Previews</div>

<strong>Keyboard bindings for media control</strong>
It's not always convenient to use the mouse or media buttons to control your media playback. You can now also use the keyboard; default shortcuts include <em>(N)</em>ext, <em>(Z)</em>Previous, and <em>space</em> for Play/Pause.
<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCPlaylist.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCPlaylist_wee.png" /></a><br />PMC Playlist</div>
<strong>Multiple Playlist Support</strong>
You want to listen to different kinds of music in different moods and situations. For this it was essential that you be able to create your own playlists to manage your music. This is now possible as the team has built an interface to create and manage these playlists.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCNewIcons.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCNewIcons_wee.png" /></a><br />PMC Icons Were Updated</div>
<strong>Shiny new icons for controller</strong>
The controller icons didn't work well on darker screens so a change was needed there. The new icons are much easier to distinguish.

<h2>Please ask</h2>
If  you requested a feature that is not listed above, feel free to contact us! The team might be working on it already, or might not know about it if there was not a feature request on bugzilla (see "Bugs and Feature Requests" below). You can also leave comments and requests on this article.
<h2>Videos and Screenshots Of What's New</h2>
Below is a video of what's new in this release. You can also <a href="http://www.youtube.com/watch?v=KfBRUogXsz0">click through to Youtube directly</a>.

<iframe width="560" height="315" src="//www.youtube.com/embed/KfBRUogXsz0?rel=0" frameborder="0" allowfullscreen></iframe>

There are more <a href="https://plus.google.com/u/0/photos/117964207644299576171/albums/5958066626175411297">screen shots of Plasma Media Center 1.2</a>.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCScreenshot1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCScreenshot1_wee.png" /></a><br />PMC showing photos from Flickr</div>
<h2>Installation</h2>
You can <a href="http://download.kde.org/stable/plasma-mediacenter/1.2.0/src/plasma-mediacenter-1.2.0.tar.bz2.mirrorlist">get the source here</a>. Please follow <a href="http://sinny.in/node/25">these instructions</a> to install Plasma Media Center on your machine.

For  binary packages, check to see if your distro has them. If you are a  packager (or know someone who is), the team can help with any questions regarding packaging. Currently, ArchLinux (AUR), Fedora, OpenSuse and Ubuntu have packages for Plasma Media Center.

<h2>Learn More and get involved</h2>
To know more about Plasma Media Center, check out <a href="http://community.kde.org/Plasma/Plasma_Media_Center">the wiki</a>. If you want to contribute, <a href="http://community.kde.org/Plasma/Plasma_Media_Center/getting_started_with_PMC">this page</a> will get you started.

<strong>Bugs & Feature Requests<strong>
Found any bug in PMC or want to have your favorite feature included in future release? File a bug to <a href="https://bugs.kde.org/enter_bug.cgi?product=plasma-mediacenter&format=guided">bugs.kde.org</a>. You can use the same link to request features; please indicate "Wishlist" in Severity.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCScreenshot2.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCScreenshot2_wee.png" /></a><br />PMC with more photos</div>
<em>Thanks to our Google Code-In students</em>:
<ul><li><em>Egor Matirov</em> for fixes to Picture Strip</li>
<li><em>Ilkin Musaev</em> for designing icons for the media controller</li>
<li><em>Oskar Jauch</em> for displaying system time on the home screen</li></ul>

<h2>Coming Up</h2>
The team is already hard at work on the next release of Plasma Media Center! Some things to expect in the next release:
<ul><li>Local File Browser Searching</li>
<li>Settings screen for:
<ul>
<li> Configuring media locations (for indexing)</li>
<li>UI Customization</li>
<li>Ignoring media like very small images (icons) or very short audio files (audio from games, apps etc)</li>
</ul>
</li>
<li>Navigation improvements to Picasa</li>
<li>Detailed info about Youtube videos</li></ul>

For detailed release and feature plan of PMC 1.3.0, please take a look at <a href="http://community.kde.org/Plasma/Plasma_Media_Center/Release_1.3">this wiki page</a>.

Thanks to all the developers, testers and people for giving  useful feedback on improving Plasma Media Center. The team hopes you are as excited as they are and will enjoy this release!