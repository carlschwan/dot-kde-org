---
title: "KDE Ships Third Release Candidate of Plasma Workspaces, Applications and Platform 4.10"
date:    2013-01-20
authors:
  - "tnyblom"
slug:    kde-ships-third-release-candidate-plasma-workspaces-applications-and-platform-410
comments:
  - subject: "Semantic desktop priorities"
    date: 2013-01-20
    body: "It is good that the next release brings faster indexing, but indexing speed is of no importance when the memory consumption of virtuoso virtually brings the machine to a halt. \r\n\r\nI have learned to check the process table before cursing <a href=\"http://en.wikipedia.org/wiki/Berkeley_Open_Infrastructure_for_Network_Computing\">BOINC</a>, and it is never BOINC that is to blame... \r\n\r\nGrabbing 1.4 GB of memory, as viruoso-t frequently does, is completely unacceptable. My desktop shut down not long ago. When I logged back in, the memory consumed was nearly 800 MB. I don't know why, and I am not in a position to do anything about it. I don't want to switch to Xfce as I have used KDE software for more than 10 years and there's nothing matching it out there in my opinion. But as an IT worker I need to have a system that doesn't fail me (very often).\r\n\r\nWhy is this relevant for KDE if virtuoso is not KDE software? Because a decision has been made (there doesn't seem to be any option) to rely on virtuoso as the backend for the semantic desktop. What good is it that Nepomuk can index my files in one minute instead of five if my system takes a full minute to respond to a mouse click?\r\n\r\nThere are probably several solutions to this. In my view, the most obvious two would be reducing the memory consumption of virtuoso and a replacement for virtuoso. I hope that these tasks are part of the plan for the next release."
    author: "Kjetil Kilhavn"
  - subject: "semantic desktop"
    date: 2013-01-20
    body: "I am in total agreement. The semantic desktop should be a separate desktop application. In my opinion, most people use a pc to navigate the web, send email, view photo/films. They need three or four apps, not including virtuoso-akonadi-nepomuk-pim.\r\n\r\nWhy should I have a huge application, enabled by default, that creates a detailed database of the contents of my hard disks?\r\n\r\nPeople need beautiful and simple. If professionals or advanced users need a semantic desktop, they could simply install it."
    author: "eticre"
  - subject: "Hunyango animated desktop background"
    date: 2013-01-20
    body: "I love the Hunyango animated desktop background by Marco Martin. The colors are changing softly and It brings a nice dynamic feel to the desktop :-)\r\n\r\nHow can I make animated desktop backgrounds myself?"
    author: "Pascal d'Hermilly"
  - subject: "Not *total* agreement ;-)"
    date: 2013-01-21
    body: "I have nothing against having the semantic desktop enabled by default. I understand that this is necessary due to the choice of using a common data store which facilitated some technical features improving the overall quality of KDE software. My understanding and insight is limited, so this could be wrong.\r\n\r\nBased on my limited knowledge, I think that the problem is partially caused by the focus on getting test results quickly during development. However, as an end user I would gladly sacrifice some indexing and searching speed in order to have the memory consumption reduced considerably, if that is possible.\r\nAt the moment every application is suffering due to the huge memory consumption by virtuoso, despite using nothing for 4 hours but Citrix and Firefox (and BOINC doing its calculations in the background).\r\n\r\nIdeally the virtuoso process would use very little memory until it was really needed, e.g. when a search is performed or when the data and functionality is otherwise used actively by the user."
    author: "Kjetil Kilhavn"
  - subject: "I agree"
    date: 2013-01-21
    body: "I use Nepomuk and love it (I would love it more if it were more agile and worked a bit better), but it's not the same for my mother. She uses her PC almost the same way she uses her mobile: some mail and chat, web navigation, view and share photos... not much more.\r\n\r\nShe only receives around 10 mails a week, so it's easy for her to keep her inbox clean and organized. I'm thinking about removing Akonadi. I read that it's possible, but haven't investigated too much about it. But what I have read leads me to think that uninstalling Nepomuk without breaking something is impossible. \r\n\r\nA lamentable imposition; users should be free to choose. My computing needs aren't the same as my mother's, even though we both love KDE."
    author: "Domine Cabra"
  - subject: "easy usecase for semantic?"
    date: 2013-01-21
    body: "Shouldn\u2019t that be an ideal usecase for nepomuk? Few changes, simple structure. Scanning for changes should be really cheap there.\r\n\r\nIf it isn\u2019t: What is wrong? (in nepomuk)"
    author: "ArneBab"
  - subject: "It's probably a bug"
    date: 2013-01-22
    body: "While I understand the frustration with these kind of problems, there isn't much we can do about it. What you're experiencing is quite simply a bug, nobody is deliberately using 1.4GB of memory. I have virtuoso configured to use 300MB (in the nepomuk settings), and that's about how much it uses.\r\n\r\nSo I recommend filing a bug against your distribution, and help the devs to debug it. In any case, I can assure you no developer ever expects all users to have bleeding edge hardware or GBs of memory lying around. At least for me 4.10 is quite an improvement resource consumption wise, I hope it will work out for you too."
    author: "Anonymous"
  - subject: "Removing the engine?? ;-)"
    date: 2013-01-22
    body: "Jokingly stated: I also think the batteries should be removed in my car, because they make the car too heavy. ;)\r\n\r\nOr we make sure the car is usable.\r\n\r\nRemoving Akonadi and Nepomuk is like that, they are essential to the engine. Of course, these shouldn't consume too much memory, and they can be constrained in System Settings (or something similar such as Configure Desktop) > Desktop Search. However, anything that consumes too much memory is a probably a bug, which should be fixed. But don't displace the engine."
    author: "vdboor"
  - subject: "Nepomuk performance and memory consumption 4.10 is fine"
    date: 2013-01-22
    body: "Is that usecase actually broken?\r\n\r\nTo me, this sounds like a lot of handwaving and making up problems, where there really are none. In order to really do something about performance problems, it unfortunately is not enough to whine, but we need detailed bugreports for that (none of which has seemed to make it into this discussion). As to memory consumption: Nepomuk defaults to 50MB for its virtuoso storage. Does it actually exceed this limit? (If so, we'd need a bugreport.) If you've set it higher, please don't complain before reducing it. That is the *exact* tradeoff people are asking for here.\r\n\r\nLet's put it the other way round: On my 4.10 setup, Nepomuk is entirely unnoticable, and I suppose the same is true for many other users. If you complained about Nepomuk performance before, time to try 4.10.\r\n\r\n(And yes, a few more performance improvements are planned post 4.10.0.)"
    author: "sebas"
  - subject: "Semantic desktop"
    date: 2013-01-23
    body: "I think same as you, I do not use \"semantic-desktop\"  and i am happy :) It's a fast response system on my PC from 2008."
    author: "kcroot"
  - subject: "Re: Removing the engine?? ;-)"
    date: 2013-01-24
    body: "Indeed, don't displace the engine, increase the engine's displacement ;)"
    author: "bluelightning"
  - subject: "nope"
    date: 2013-01-27
    body: "Semantic technology needs to be a background service for all the apps a user needs. Its function is to help every average user to browse files, web, photos and videos."
    author: "MK"
---
On January 18th, KDE released the third release candidate (RC3) for its renewed Workspaces, Applications, and Development Platform.

Highlights of the 4.10 releases include:
<ul>
<li>Qt Quick in Plasma Workspaces -- <a href="http://doc.qt.digia.com/qt/qtquick.html">Qt Quick</a> is continuing to make its way into Plasma Workspaces. Plasma Quick, KDE's extension on top of Qt Quick, allows deeper integration with the system, as well as more powerful apps and Plasma components. <a href="http://techbase.kde.org/Development/Tutorials/Plasma/ShellDesign">Plasma Containments</a> can now be written in <a href="http://doc.qt.digia.com/qt/qdeclarativeintroduction.html">QML</a> (the declarative language in Qt Quick). Various Plasma widgets have been rewritten in QML. Many performance, quality and usability improvements make Plasma Desktop and Netbook Workspaces easier to use.</li>
<li>New Screen Locker -- A new screen locking mechanism based on Qt Quick brings more flexibility and security to Plasma Desktop.</li>
<li>Animated Wallpapers -- Thanks to a new Qt Quick-based wallpaper engine, animated wallpapers are now easy to create.</li>
<li>Improved Zooming in Okular -- A technique called tiled rendering allows Okular to zoom in closely while reducing memory consumption. Okular Active, the touch-friendly version of the powerful document reader, is now part of the KDE combined releases.</li>
<li>Faster indexing -- Improvements in the Nepomuk semantic engine allow faster indexing of files. The new Tags kioslave allows users to browse their files by tags in any KDE-powered application.</li>
<li>Color Correction -- Gwenview, KDE's smart image viewer, and the KWin window manager all now support color correction and can be adjusted to the color profiles of different monitors, making for more consistent representation of photos and graphics.</li>
<li>Notifications -- Plasma's notifications are now rendered using Qt Quick. Notifications themselves, especially power management, have been improved.</li>
<li>New Print Manager -- Printer set up and job monitoring have been improved thanks to a new implementation of the Print Manager.</li>
<li>Kate, KDE's Advanced Text Editor, received multiple improvements based on user feedback. It is now extensible using Python plugins.</li>
<li>KTouch -- KDE's touch-typing learning tutor has been rewritten and features a clean, elegant user interface.</li>
<li>KDE Games improvements -- Many parts of libkdegames have been rewritten. Porting instructions for third party developers are available.</li>
<li>KSudoku now allows for puzzles to be printed.</li>
<li>KJumpingCube has seen many improvements that make the game more enjoyable.</li>
</ul>

More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan">4.10 Feature Plan</a>. We want to give this release a good testing in order to continue to improve the quality and user experience of KDE software.

<h2>Testing</h2>
KDE is running an extra detailed Pre-Release Testing Program throughout the 4.10 process. To participate, get the latest KDE 4.10. Instructions for installing the latest test release can be found <a href="http://community.kde.org/Getinvolved/Quality/Beta/4.10/Installing">here</a>. Some areas of KDE are getting special testing attention this time around. We have testing checklists and recommendations for special attention on the <a href="http://community.kde.org/Getinvolved/Quality/Beta/4.10#Testing">wiki</a>.

The KDE Pre-Release Testing Program is a way for anyone to contribute to the quality of KDE software. You can make a difference by carefully testing the programs that are part of the KDE 4.10 Release Candidate and assist developers by identifying legitimate bugs. This means higher quality software, more features and happier users and developers.

The Pre-Release Testing Program is structured so that any KDE user can participate, regardless of skill level. If you want to be part of this quality improvement program, please contact the Team on the IRC channel #kde-quality on freenode.net. The Team Leaders want to know ahead of time who is involved in order to coordinate testing activities and make the best use of people's time. They are also committed to having this project be fun and rewarding. After checking in, you can install the beta through your distribution package manager. The <a href="http://community.kde.org/Getinvolved/Quality/Beta/4.10/Installing">KDE Community wiki</a> has instructions. This page will be updated as packages for other distributions become available. With the Release Candidate installed, you can proceed with testing. Please contact the Team on IRC #kde-quality if you need help getting started.