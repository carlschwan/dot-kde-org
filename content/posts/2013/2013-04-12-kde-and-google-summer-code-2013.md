---
title: "KDE and Google Summer of Code 2013"
date:    2013-04-12
authors:
  - "valoriez"
slug:    kde-and-google-summer-code-2013
---
We're delighted to announce that KDE has been accepted as a mentoring organization in Google Summer of Code 2013 (GSoC), for the ninth consecutive year. GSoC has been valuable in bringing new developers into the KDE Community and other free and open software projects. And it has been successful at achieving the goal of creating quality code for the use and benefit of all.
<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2013logo.jpg" /></div>

It is time for students to start working on their proposals in consultation with the respective teams. The KDE Community maintains an <a href="http://community.kde.org/GSoC/2013/Ideas">Idea Page</a> that is an excellent starting point. It will be helpful to consider the KDE <a href="http://community.kde.org/GSoC">student guidelines</a>. See also <a href="http://teom.org/2012/03/01/how-to-write-a-kick-ass-proposal-for-google-summer-of-code/">Teo Mrnjavac's suggestions for creating an effective proposal</a>.

Students can create their own projects or base proposals on something from the Idea Page. Either way, it's important to get feedback from the appropriate KDE team well before the submissions deadline. For general questions about KDE's participation in Google Summer of Code, please visit the IRC channel #kde-soc on freenode.net, or <a href="https://mail.kde.org/mailman/listinfo/kde-soc">join the kde-soc mailing list</a>. To discuss a specific idea, please contact the relevant team directly (the <a href="http://community.kde.org/GSoC/2013/Ideas">Idea Page</a> includes contact information).

The <a href="http://www.google-melange.com/gsoc/events/google/gsoc2013">official Google Summer of Code timeline</a> shows critical activities and dates.

In the spirit of providing opportunities for new contributors, this year KDE will also participate in the Free and Open Source Outreach Program for Women, which runs from June to September in parallel with Google Summer of Code. While the Outreach Program for Women shares many goals and methods with Google Summer of Code, the two programs are not directly related. Unlike Google Summer of Code, the Outreach Program for Women allows non-coding contributions. For more information, see <a href="http://community.kde.org/OutreachProgramForWomen">KDE’s Outreach Program for Women wiki page</a>.

KDE will also be hosting Season of KDE 2013, similar to <a href="http://dot.kde.org/2012/04/24/google-summer-code-season-kde-2012-there-place-everyone">the program in previous years</a>. More information will be available within the next few weeks.