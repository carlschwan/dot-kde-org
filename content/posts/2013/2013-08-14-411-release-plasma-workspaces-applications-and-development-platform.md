---
title: "4.11 Release of Plasma Workspaces, Applications and Development Platform "
date:    2013-08-14
authors:
  - "jospoortvliet"
slug:    411-release-plasma-workspaces-applications-and-development-platform
comments:
  - subject: "Help with promotion"
    date: 2013-08-14
    body: "Like, favorite and reshare:\r\n<ul>\r\n<li><a href=\"http://ur1.ca/f210o\">YCombinator</a></li>\r\n<li><a href=\"http://ur1.ca/f20zn\">Facebook</a></li>\r\n<li><a href=\"http://ur1.ca/f2100\">Google +</a></li>\r\n<li><a href=\"http://ur1.ca/f2108\">Twitter</a></li>\r\n<li><a href=\"http://ur1.ca/f210e\">Join Diaspora</a></li>\r\n<li><a href=\"http://ur1.ca/f210v\">Slashdot</a></li>\r\n<li><a href=\"http://ur1.ca/f2116\">Reddit</a></li>\r\n</ul>\r\n\r\n[updated to include link locations - 2013 August 19]"
    author: "JLP"
  - subject: "AWESOME release"
    date: 2013-08-14
    body: "I only tried this release out shortly before the announcement - a few days ago. And it rocks ;-)"
    author: "jospoortvliet"
  - subject: "Always great news, but what about color and phones?"
    date: 2013-08-14
    body: "I haven't seen anything that mentions color management nor MTP. Does that mean that KDE isn't still ready for professional design and photo/video editing tasks?\r\nAnd what about MTP? Has ir been improved? The second RC of 4.11 still was very buggy; trying to manage Android ICS and Jelly Bean phones was a pain and one needed, sadly, to reboot to Windows.\r\n\r\nThanks for your work and time."
    author: "Jos\u00e9 Manuel"
  - subject: "WOW! Dolphin feels really faster!"
    date: 2013-08-15
    body: "Dolphin got really faster with this release. Good work!  Dolphin is 'nearly perfect' now :)\r\n\r\nThe only \"missing feature\" i miss is something like klook (also known as the \"spacebar fast preview\" feature from Mac OS X Finder). I would find this very usefull even in single-click mode when using the keyboard for navigation within the files. Press space to get a big preview, press it again to go back to navigate with the keyboard. It works this way on the Mac, i dont know if they have even a button for this.\r\n\r\nBeside Dolphin i mostly use Konsole and of course KWin. There were fine for me since KDE 3+, but  the polish which goes into this apps is just great!"
    author: "Emmeran Seehuber"
  - subject: "I haven't seen anything that"
    date: 2013-08-15
    body: "<cite>I haven't seen anything that mentions color management</cite>\r\nKWin supports color management since 4.10. Also some KDE applications support color management - Krita comes to my mind as one of those supporting it."
    author: "mgraesslin"
  - subject: "substantial Kopete progress "
    date: 2013-08-15
    body: "Pali Roh\u00e1r provided information about Kopete in the run-up to this announcement. In the rush to create the announcement and have it coincide with the actual releases, the Promo team overlooked this emailed information. We apologize.\r\n\r\nClearly Kopete is seeing a lot of development activity.\r\n\r\nThank you Pali for providing the following information:\r\n<ul>\r\n<li>Kopete migrated from svn to git</li>\r\n<li>Fixed memory leaks , corruptions and NULL de-references</li>\r\n<li>Fixed a lot of Kopete crashes at exit, at logout and when closing modal dialogs</li>\r\n<li>Support for adding new contacts to top level group</li>\r\n<li>Allow inviting users to groupchat by drag-n-dropping</li>\r\n<li>Automatically logout and login with suspend/resume</li>\r\n<li>Activate webcam preview in settings dialog only if webcam tab is selected</li>\r\n<li>New sqlite history plugin (by default old one is used and new is disabled)</li>\r\n<li>SSL support for ICQ protocol (enabled by default for all ICQ accounts)</li>\r\n<li>Updated jabber libiris library from upstream</li>\r\n<li>Fixed many connection problems (especially legacy SSL and logging in)</li>\r\n<li>Support for SCRAM-SHA-1 SASL login mechanism (needed for new servers)</li>\r\n<li>XMPP 1.0 support (fixed login to gmx, facebook and other servers)</li>\r\n<li>Updated jabber google libjingle library for gtalk voice calls</li>\r\n<li>Fixed wizard for registering new jabber account</li>\r\n<li>Support for Jabber XEP-0184: Message Delivery Receipts (sender and receiver of chat messages will see delivery status)</li>\r\n<li>Support for jabber XEP-0199: XMPP ping</li>\r\n<li>Fixed problems with Skype protocol when user sends or receives same/duplicate messages</li>\r\n</ul>"
    author: "kallecarl"
  - subject: "kwin & tearing"
    date: 2013-08-15
    body: "very pleased to see that the tearing problem with my nvidia gtx670 seems to be gone with this release...hovewer not completely\r\n\r\nevery time i reboot the tearing is back and  i have to open system settings > desktop effects > advanced and set the compositing type to opengl 2.0 > apply> back to opengl 3.1> apply to make the tearing go away, its like the settings dont stick after rebooting...\r\n\r\nany idea what i can do ?\r\nalternatively can someone provide me with the terminal command to change the compositing type so that i can make a startup script looking something like:\r\n---\r\ncommand set compositing type opengl2\r\nsleep 1s\r\ncommand set compositing type opengl3.1\r\n---\r\nor something like that?\r\n\r\nalso the keyboard shortcut i use to start the screensaver doesent work anymore. the command that used to work was ' qdbus org.kde.screensaver /ScreenSaver SetActive true'"
    author: "arnold weissenegger"
  - subject: "Prolems after upgrade from KUbuntu Backport PPA"
    date: 2013-08-15
    body: "Hi, there. I'm using KDE for over a year now and am very satisfied with it. Excellent job.\r\n\r\nI even added KUbuntu-ppa Backport to have latest updates of KDE because KUbuntu official PPA is slow with that. \r\nLast version I used flawlessly was 4.10.\r\n\r\nYesterday, Moun software updater announced 4.11 upgrade availabillity which was allowed and went through without a single error (57MB of files was downloaded and installed). After upgrade was finished and computer restarted after login screen I was left with black blank screen instead of plasma desktop ?!\r\n\r\nAfter manual installation (tty1) of plasma desktop (sudo apt-get install plasma-desktop) everything was back to normal again.\r\n\r\nThat is, except the KRDC which was now missing the xfreerdp v. 1.0.2 library (PPA contains only 1.0.1 versions). Since 1.0.2 has some bugs that prevents me from using it, the only solution was to \"downgrade and lock\" the KRDC to previous version.\r\n\r\nI also noticed that suspend/hibernate is fixed now so I had to remove my manual sleep.d script that currupted the new procedure.\r\n\r\nEverything is excellent now. The desktop looks great, feels faster and works stable. Congratulations !"
    author: "Milan Oparnica"
  - subject: "Re: kwin & tearing"
    date: 2013-08-15
    body: "I'd say add a bug report at bugs.kde.org."
    author: "bluelightning"
  - subject: "Terrific release!"
    date: 2013-08-16
    body: "Dunno what you guys did, it worked!\r\n\r\nAverage memory usage (Kontact with most of the modules <em>actually</em> loaded, Kdevelop + kdev-python, Lots of Konsole and Firefox tabs) on my 2009 4GB RAM laptop running Kubuntu 13.04 64bit with the proprietary nvidia driver is down to 2.2GB from 3.2GB.\r\n\r\nEspecially liked that the File Indexing runs in a \"slow\" mode for real now, and that Akonadi automatically downloaded any messages it hadn't before for indexing.\r\n\r\nBest of all of course is that after so many years of cursing at X, screen tearing is finally gone in all cases while compositing!"
    author: "akanouras"
  - subject: "Re: WOW! Dolphin feels really faster!"
    date: 2013-08-17
    body: "The feature you're referring to in OS X is called \"Quickview\". Having that would IMO, be a good add.... sounds like something that should be posted to Bugzilla as a feature request :)"
    author: "greeneg"
  - subject: "Color Management"
    date: 2013-08-18
    body: "Until Qt supports color management natively then it will always be up to the apps to implement it.  Sadly I'm not aware of any plans for CM in Qt 5.2, so it's still a way off.  There is experimental work to support Oyranos in KWin but as far as I'm aware that's not ready for prime-time yet.  There's also the big issue of Oyranos versus colord to be settled."
    author: "odysseus"
  - subject: "Toolz"
    date: 2013-08-19
    body: "As a friend of both Atul and KDE, I have to thank you for your thoughtful dedication. He was a fan and promoter of KDE right from the start, and championed the project in India even when it seemed like everyone was gravitating toward other projects."
    author: "sirtaj"
  - subject: "Glad you like it - and yes, I"
    date: 2013-08-19
    body: "Glad you like it - and yes, I have exactly the same experiences! Real nice :D"
    author: "jospoortvliet"
  - subject: "Just a thought, but the very"
    date: 2013-08-19
    body: "Just a thought, but the very link heavy comment looks super spammy.  I definitely suggest formatting it a bit more to make it not look like it quite so much in the future."
    author: "kyle"
  - subject: "MTP"
    date: 2013-08-20
    body: "MTP works rather nicely; I use it several times a week (well, most weeks anyways) .. however, the MTP kio integration is not part of the SC releases, which is why it is not mentioned here. It is in the \"base\" modules, but still in playground. \r\n\r\nSee: https://projects.kde.org/projects/playground/base/kio-mtp\r\n\r\nI really don't know why it isn't at least in extragear, as it's rather reliable?"
    author: "aseigo"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://kde.org/announcements/4.11/screenshots/plasma-4.11.png" /></div>

The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.

These releases are all translated in 54 languages; we expect more languages to be added in subsequent monthly minor bugfix releases by KDE. The Documentation Team updated 91 application handbooks for this release.

<h2>Dedication</h2>
This release is dedicated to the memory of Atul 'toolz' Chitnis, a great Free and Open Source Software champion from India. Atul led the Linux Bangalore and FOSS.IN conferences since 2001 and both were landmark events in the Indian FOSS scene. KDE India was born at the first FOSS.in in December 2005. Many Indian KDE contributors started out at these events. It was only because of Atul's encouragement that the KDE Project Day at FOSS.IN was always a huge success. Atul left us on June 3rd after fighting a battle with cancer. May his soul rest in peace. We are grateful for his contributions to a better world.

<h2> Plasma Workspaces 4.11 Continues to Refine User Experience</h2>
Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience. The <a href="http://www.kde.org/announcements/4.11/plasma.php">announcement for Plasma Workspaces</a> has more information..

<h2>KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</h2>
This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features. The <a href="http://www.kde.org/announcements/4.11/applications.php">announcement for the KDE Applications 4.11</a> has more information.

<h2>KDE Platform 4.11 Delivers Better Performance</h2>
This release of KDE Platform 4.11 continues to focus on stability. Since Platform 4.9 new features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework. The <a href="http://www.kde.org/announcements/4.11/platform.php">Announcement for KDE Platform 4.11</a> has more information.

<h2>Spread the Word</h2>
Non-technical contributors are an important part of KDE's success. While mega proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the 4.11 releases. <a href="https://bugs.kde.org/">Report bugs</a>. Encourage others to join the <a href="http://community.kde.org/Getinvolved">KDE Community</a>. Or <a href="http://jointhegame.kde.org/">Join The Game</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.11 releases.

Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from identi.ca, twitter, youtube, flickr, picasaweb, blogs and other social networking sites.