---
title: "KDE Commit-Digest for 14th July 2013"
date:    2013-08-19
authors:
  - "mrybczyn"
slug:    kde-commit-digest-14th-july-2013
---
In <a href="http://commit-digest.org/issues/2013-07-14/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/Simon">Simon</a> adds support for continuous recognition with <a href="http://en.wikipedia.org/wiki/CMU_Sphinx">Sphinx</a></li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> adds Github plugin</li>
<li><a href="http://edu.kde.org/kalgebra/">KAlgebra</a> makes it possible to export 3D plots to PDF</li>
<li>KDE Workspace implements keyboard navigation for battery monitor</li>
<li>Network Management gains import/export of VPN plugins configuration</li>
<li><a href="http://skrooge.org/">Skrooge</a> sees new features related to limits on accounts</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> adds a simple ssh sync handler</li>
<li>Optimizations in KDE-Base and <a href="http://pim.kde.org/">KDE-PIM</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-07-14/">Read the rest of the Digest here</a>.