---
title: "KDE Meetup 2013 in India"
date:    2013-02-10
authors:
  - "devaja"
slug:    kde-meetup-2013-india
---
<a href="http://www.gdgdaiict.com/kdemeetup/">KDE Meetup</a> will be the largest KDE event in India since <a href="http://www.shantanutushar.com/content/why-confkdein">conf.kde.in</a> in 2011. It will be held February 23rd and 24th at the <a href="http://www.daiict.ac.in/daiict/institute/about_daiict.html">Dhirubhai Ambani Institute of Information and Communication Technology</a> (DA-IICT) in Gandhinagar. KDE Meetup will be a great opportunity for anyone who is interested in free and open software or who wants to get involved with the <a href="http://kde.org/">KDE Community</a>. 
<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://yashshah.com/blog/wp-content/uploads/2013/02/daiict.jpg"><img src="http://yashshah.com/blog/wp-content/uploads/2013/02/daiict.jpg" /></a><br /><small>DA-IICT</small></div>

The primary focus of the Meetup is to introduce people to the world of possibilities of open source software development with presentations about basic development tools, including <a href="http://en.wikipedia.org/wiki/Qt_(framework)">Qt</a>. This event will also appeal to those who are already familiar with free and open software development, because there will be specialized talks by experts on various topics. There will be hackathons after the talks and presentations, so the event will be enjoyable and fruitful for anyone involved with free and open software, including teachers, developers and students.

In addition, KDE Meetup organizers have the goals of spreading the word about KDE and free/open software in India and encouraging more participation in these efforts, particularly among students in and from India. What better way to spread awareness, knowledge and enthusiasm than a lively and enjoyable event that appeals to students and combines learning and fun? Speaker expertise and the quaint, cool campus environment will help make the event a pleasant and insightful one.

<h2>Event Schedule</h2>
KDE Meetup will be held on the 23rd and 24th of February.

Pradeepto Bhattacharya, Shantanu Jha, Vishesh Handa, Rishabh Arora will be giving talks at the event. Each of them has substantial experience with free and open software and with KDE as well. Presentation topics include: 
<ul>
<li>The KDE Community</li>
<li>Qt development</li>
<li>Plasma Workspaces, Applications and Development Platform</li>
<li>Semantic desktop</li>
<li>Educational applications</li>
<li>and more.</li>
</ul>
In addition, there will be <strong>hands-on workshops</strong> led by experienced developers covering application development with Qt and bug fixing in KDE applications. Following the presentations and training, <strong>hackathons</strong> will be available for those enthusiastic participants who want to take the learning process a notch higher. These sessions—guided by experienced developers— will delve deeper into bug fixing and development. The <a href="http://www.gdgdaiict.com/kdemeetup/agenda.html">agenda</a> has more details. 

<h2>The Venue</h2>
KDE Meetup will be held at the <a href="http://www.daiict.ac.in/daiict/institute/about_daiict.html">Dhirubhai Ambani Institute of Information and Communication Technology</a> (DA-IICT), located in the peaceful city of <a href="http://en.wikipedia.org/wiki/Gandhinagar">Gandhinagar</a>, the capital city of vibrant Gujarat, which is on its way to becoming an IT hub. Located just 17 kilometers from the <a href=" http://en.wikipedia.org/wiki/Sardar_Vallabhbhai_Patel_International_Airport">international airport</a> and served by an efficient public transport system, the lush DA-IICT campus is the perfect environment for this event. With WiFi available throughout the campus and fully equipped computer labs, DA-IICT has an excellent technical infrastructure to support the Meetup. The Institute is one of the top ten technology schools—the best <em>private</em> technology university—in India. In 2012, 17 students from the Institute were selected for the <a href="https://developers.google.com/open-source/soc/">Google Summer of Code</a> (GSoC) internship program (the most selections of any university in India). Eight of those students were selected to contribute to KDE, and much of their work has been included in KDE software releases. 

<h2>Registration</h2>
<a href="http://www.gdgdaiict.com/kdemeetup/register.php">Registration (online)</a> for KDE Meetup is open now. Do not miss the opportunity to be a part of this amazing event.

The early bird student registration fee is ₹400, and regular registration has a fee of ₹600. Early bird registration closes 15th February. Same day registration is available in person at the venue; the registration fee is ₹200 higher on the day of the event. 

<h2>Register soon</h2>
Time is short to take advantage of early registration. Advance online registrations will help organizers plan for the right number of participants. Please register soon.

KDE Meetup is an exciting opportunity for anyone involved with Information and Communication Technology. Free and open software, DA-IICT, KDE, and GSoC offer practical knowledge and experience distinctive in the world. KDE Meetup is being organized primarily by DA-IICT students who have seen the value of these programs first-hand. They wish you a warm welcome.