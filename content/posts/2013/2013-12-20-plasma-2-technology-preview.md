---
title: "Plasma 2 Technology Preview"
date:    2013-12-20
authors:
  - "sebas"
slug:    plasma-2-technology-preview
comments:
  - subject: "Old systemtray?"
    date: 2013-12-20
    body: "Will the old systemtray be available? the new one is way to complex for me to feel comfortable with.\r\n\r\nWhy are you guys making things more complicated? wasn't the systemtray supposed to dissapear and merged with the taskbar? are you going to add all that complexity for something that will held 2 or 3 items?"
    author: "Varghese"
  - subject: "This looks slick !"
    date: 2013-12-20
    body: "Everything looks more polished than in KDE4! Can't wait for KDE5 to appear :D\r\n\r\nThe only thing I don't like is the new system tray, maybe with the time I will get used to it. I wonder why KDE designers like to add so much cluttering into interfaces the new system tray looks like systemsettings, something inside something inside something."
    author: "Ivan K"
  - subject: "New system tray"
    date: 2013-12-21
    body: "First of all, the new notification area is not finished yet, so some of the problems you perceive might be simple bugs, or based on a misunderstanding of the concept from this early version.\r\n\r\nYou'll be surprised that the new notification area is based on the exact workflows of the current one. It ties together the individual parts in the interface, and allows for much quicker transition between the individual items, and less reliance on hard to hit targets. From a mouse interaction point of view, it behaves the same as the old one, you don't need any more clicks to get anything done, in many cases it removes visually jarring transition between individual popup windows.\r\n\r\nI've explained some of the change in more details in this post: https://plus.google.com/+SebastianK%C3%BCgler/posts/DQ33L9Ejq5P"
    author: "sebas"
  - subject: "Re: New system tray"
    date: 2013-12-21
    body: "Well, the naggers will nag. :) It's interesting to see how much KDE evolved since I fled \"the sinking ship\" (v4.0). One of my main concerns is RAM usage, because I easily use 100-200 tabs in 2-3 browsers, plus virtualization and other stuff, so I don't want the OS and DE to hog my RAM, although to be fair they are the ones taking the least. But the more I can save by using a reasonable DE, the better for me. I hope KDE will soon reach a point where it's either lighter on resources or at least easily configurable for this goal.\r\n\r\nI like what I see here and I also hope for under-the-hood improvements. :) Good luck and Merry Christmas!"
    author: "kneekoo"
  - subject: "It is depressing to see how"
    date: 2013-12-21
    body: "It is depressing to see how little the system tray has actually evolved in KDE. It is still crammed with stuff that has no justification of being there at all. Windows had to introduce hidden systray icons because most of the Windows-world icons come from third parties. KDE apparently copied them because they don't want to think about the problem."
    author: "Anonymous"
  - subject: "Please provide constructive feedback"
    date: 2013-12-21
    body: "<cite>Will the old systemtray be available?</cite>\r\nIt's not planned, but Plasma is flexible enough to allow multiple implementations.\r\n<cite>the new one is way to complex for me to feel comfortable with.</cite>\r\nDid you try it to come to this conclusion or did you just watch the video? If you tried it, please provide valid feedback through the bugtracker to show where you see problems.\r\n\r\n<cite>wasn't the systemtray supposed to dissapear and merged with the taskbar?</cite>\r\nNo, only the applications might be merged with the taskbar leaving the systemtray as what it's supposed to be: a system and notification area."
    author: "mgraesslin"
  - subject: " I have tried neon5"
    date: 2013-12-21
    body: "With the experience from the live cd which I assume is in the early states plus the video I re-affirm my previous statement. The new systemtray is showing me always information I don't want, is showing other things that are not requesting attention apparently with no reason.\r\n\r\nSorry if I sound negative but I was quite excited about what I thought it was going to be the simplification of the systemtray but apparently it is not,"
    author: "Varghese"
  - subject: "animations"
    date: 2013-12-21
    body: "I don't like the animations when a window is closed. However the new calender looks better. I hope KDE 5 will be stable."
    author: "Dieter"
  - subject: "I love it! I wonder if retina and super hi resolutions are suppo"
    date: 2013-12-22
    body: "Hi I love what you guys are doing. I think KDE is evolving quite nicely.\r\nWhat I wonder if this iteration of KDE will support high resolution scaling the way OS X does with their system. Adjusting each part of the desktop fonts/bars/window manager/DPI/browsers zoom/ etc to allow the system to look half decent on a retina display is not so great. I wonder if there is a plan to include solution built in on the next KDE since high resolution displays like the Retina and Pixel seem to be the next evolution in displays."
    author: "Cho"
  - subject: "Please be more specific"
    date: 2013-12-22
    body: "<cite><em>The new systemtray is showing me always information I don't want, is showing other things that are not requesting attention apparently with no reason.</em></cite>\r\n\r\nCould you please tell us which information it is always showing which you don't want to see. The systray is supposed to hide items which do not matter at the moment."
    author: "mgraesslin"
  - subject: "Plugin"
    date: 2013-12-22
    body: "You can disable it."
    author: "KDE Fan"
  - subject: "Lack of hardware"
    date: 2013-12-22
    body: "A huge problem for this is, that not all developers do have such a display yet. It's especially difficult to get a high-res screen for a workstations. It's like always: if there is no itch to scratch ;-) Some developers who have such a display started to scratch that itch, though. Whether it will be complete till the first release of Plasma 2 is something I cannot say."
    author: "mgraesslin"
  - subject: "Time to nag"
    date: 2013-12-22
    body: "1. Everything here looks awesome, and I'm looking forward to test. Everyone involved has done a great job, so thank you.\r\n\r\n2. However, there is something that failed here: branding. Bad habits die slowly, and even I have caught myself tweeting and repeating that this is part of some mythical thing called KDE 5. We all know there's no such thing, but PW2 is seen not as something standalone, but as part of KDE 5. \r\n\r\nUnfortunately for some positivist developers, you cannot define by now what does the word \"KDE\" mean. The KDE word became meaningful sometime between KDE 2 and KDE 3, and means an integrated desktop environment. We can repeat, and repeat, the \"true meaning\" of \"KDE\", but for most people, this is \"the first piece of KDE 5\"."
    author: "Alejandro Nova"
  - subject: "Super sluggish"
    date: 2013-12-23
    body: "Maybe I'm getting old and just don't \"get it\", but this preview leaves me totally un-impressed. I pretty much left KDE when KDE4 was introduced. Though there was lots of promises about improvements, this preview tells me is that I made the right decision. The sluggishness/resource intensiveness  in KD4 is profoundly annoying, and this preview tells me it will be carried on into KDE5.  A shame really, the world needs a good OSS DE.\r\n"
    author: "Kolbj\u00f8rn Barmen"
  - subject: "I'm sure there's a setting"
    date: 2013-12-23
    body: "I'm sure that you'll be able to change back, but I wonder if this new one will be default."
    author: "enoop"
  - subject: "I like it"
    date: 2013-12-23
    body: "I think it's a nice solution to a long standing bug. Before if you opened a widget from the system tray it would open almost an inch above the panel. This change looks like it will fix that."
    author: "enoop"
  - subject: "You're barking up the wrong tree."
    date: 2013-12-23
    body: "<p><strong>> the world needs a good OSS DE.</strong>\r\n<p>Cheer up, the world is not as bad as you think ;-) There are some \"light-weight\" DEs out there like LXDE and Enlightenment which don't require hardware acceleration and save some 150MB of RAM. If you're a power user maybe you can try out BodhiLinux (Ubuntu + enlightenment); otherwise, PeppermintOS (Ubuntu + LXDE but simpler than Lubuntu).</p>\r\n<p>I still like KDE 4 better than these although it is indeed heavier. And it's no more than my very personal preferrence thus I won't consider you stupid. As a KDE fan that's sad that KDE 4 doesn't match your expectation but at least I wish you respect others' difference (even though one of three Linux users chooses KDE) and stop provoking other people.</p>\r\n<p><strong>> I pretty much left KDE when KDE4 was introduced.</strong></p>\r\n<p>KDE 4 has been updated very often and the latest version v4.12 is a lot faster and more stable than its early version(s) you might have tried out. Seriously, KDE 4 nowadays helps my everyday work.</p>\r\n<p>What you had seen in the video, both KDE 5 and Plasma 2 in it, are \"technical preview\". And I believe they will get faster and smoother when coming out ;-)</p>"
    author: "Culip"
  - subject: "+1 This is so sadly true... "
    date: 2013-12-23
    body: "This is so sadly true... Been using KDE apps/DE for a long time as on my desktop (now less and less) and I really lacked the snappiness of the Windows, Android, OSX/iOS, even Unity.\r\n\r\nEverything reacts with a shorter or longer delay (tested on many different configurations, nvidia/kde/intel) -be it Alt-tabing between the applications, summonig menus, selecting text, checking mail with Kmail (dog slow comparing to Thinderbird, I would not speak about the Outlook, I hate personaly, but bow due to efficiency and raw speed = <10k mails in mailbox) etc, etc.. Lags all over the places, \r\n\r\nStill waiting but I see no progress regarding the performance, time to move on i believe ..."
    author: "Karol"
  - subject: "To call LXDE and"
    date: 2013-12-23
    body: "To call LXDE and Enlightenment desktop environments is stretching it. Listen, I came from AmigaOS, you know that old archaic operating system that people like to make fun of? Yeah well, it had its merits. I'm used to advanced functional  desktops, where I can script and configure almost any aspect of the DE, where I can create macros that are triggered when I hold certain hotkeys while clicking files, where I can set up magic folders that perform file processing when I drag files into them etc..  LXDE and Enlightenment are visually glorified toys in comparison, they totally lack everything that a desktop _environment_ is about.  KDE3, (or TDE3 if you like, which I maintain my own set of builds for) has way more, and also lots of possibilities that KDE4 just lacks,. And KDE4 is slowmotion even when I have full hardware support and turn off all fancy stuff. I have a 2.8 GHz Quad Xeon with 16GB RAM that I cannot run music studio software under KDE4 on simply because... plasma. Plasma pulls way too much CPU and leaves little back to actual productivity software. With KDE3, no problem whatsoever. v4.12 is not  a lot faster, it is marginally faster, and marginally more stable than earlier versions. It's not like I haven't tried them all, I have.\r\n\r\nSo you belive they will be faster and smoother, I've heard this before, ever since first preview of KDE4, and it has yet to happen.\r\n\r\nNo really, I would much more prefer KDE4+ if Plasma was just kicked out of the picture."
    author: "Kolbj\u00f8rn Barmen"
  - subject: "Re: music studio software"
    date: 2013-12-24
    body: "With my laptop which is vastly less powerful than your machine, I have no trouble running any JACK-using application at the same time as KDE4/Plasma. For example Ardour + Hydrogen + Synth Plugins + DSP plugins etc, all with very low latency and no problems with xruns.\r\n\r\nAs I'm sure you realise, setting up a serious music recording/production environment under Linux can be a challenge, but when I have run into problems in the past it has never been due to KDE or Plasma.\r\n\r\nI realise this comment isn't exactly constructive, but I can't help thinking you're exaggerating there... Can't run music studio software \"simply because... plasma\"? Simply not true.\r\n\r\nAs a (mostly silent) KDE user I'm surprised by the amount of negativity that still gets thrown around about KDE. I notice no sluggishness, I would consider RAM/CPU use to be acceptable for a modern OS/Desktop Environment that provides what it does.\r\n\r\nAnd then you get people like Karol below who chime in and say KDE is slower/less snappy than Windows/Android/etc... No. No, it is not. The very same laptop which runs KDE beautifully really struggles with Windows 7. Also until it died recently I had up to date versions of KDE running fine on an old Pentium. This machine, which struggled with Windows XP, had no trouble running KDE + Firefox + Libreoffice/Calligra. While I wouldn't call it snappy (at least partly due to old, slow IDE disks, 1G RAM), it was entirely acceptable as a general use machine (with all the comforts of a modern KDE, which is much more helpful to me than a 12-year-old WinXP or a basic LXDE/Enlightenment desktop). Certainly I wouldn't use it for advanced music studio software, but it's no quad core Xeon."
    author: "os"
  - subject: "settings"
    date: 2013-12-24
    body: "Have you tried to go to systemsettings and increase the speed of animations? Sometimes is not a problem of efficiency its only defaults are setup to be slow, and people thing that Plasma is slow."
    author: "yeaman"
  - subject: "I beg for pardon but cannot"
    date: 2013-12-24
    body: "I beg for pardon but cannot say I have the same experience with my IBM T43p upgraded with SSD and 2gigs of RAM\r\nKDE was (more less) fine when runing with some light load (lets say 2-3 terminals, firefox <10 tabs) If I put more load on the machine .... it gets slower - like waiting 2 sec after each alt-tab. Boted to Windows 8, 8.1 on the same setup - no problems whatsoever.. even when i get >50 tabs open (apart from swapping when i activate long unused tab) UI is very snappy, much more than with XP it was with.\r\n\r\nSame on my work machine Core2Dui E880,8Gig,SSD,nvidia nvs 295 with dual screen setup - mail with 10k mails, browsers with hundred of pages open, multiple flash /java plugins , 20-30 consoles open, office/communication apps - you get the idea. It was imposible to work with KDE desktop efficiently as the performance wasn't reliable under load (random micro freezes ) - and this was with all the social \"improvements\" switched OFF - I still get the shivers when I recall the day I decided to switch it on, for the test, and ended with the machine/DE freezing when trying to index the files and tens thousands of my emails. Akonadi/nepomuk taken all cpu, memory and forced me to reboot loosing all my current work - a bit of no-go situation for the working pro.\r\n\r\nI am not saying KDE is bad, just have the impression its bit like created to please itself, not to enable the die-hard admins to do their jobs ... \r\nWell - if it suits your goals - I wish all of you good luck and all the best.\r\nKarol"
    author: "Karol"
  - subject: "+1 Alright."
    date: 2013-12-24
    body: "Ok, let me clarify your points . . . you don't like KDE 4 because it's\r\n\r\n1. slow\r\nLike I said that's very true . . . because it's newer and fancier (compared to, say, KDE 3.) I don't care but some people like you do.\r\nTo Karol: I totally agree that windows cycling with [Alt]+[Tab] is way too sluggish; so I often use Expose instead.\r\nI sometimes see <em>flickering</em> when a window with a blurry background (like Homerun) is fading in/out and I think it's more annoying than the sluggishness.\r\n\r\n2. resource eating which affects other software's performance\r\nI don't think it is the case when your computer is idling, in my experience with my system (i3 Sandy 45W, 6GB RAM) which is less powerful than yours. But some KWin features (like wobbly window, fade in/out) may greatly affect the overall performance for sure.\r\n\r\n3. missing features\r\n3.1 hotkeys\r\nI think you can easily configure those on KDE and Dolphin.\r\n3.2 magic folder\r\nYou meant, this guy? http://www.maketecheasier.com/organizing-related-files-in-kde-with-magic-folder/\r\n3.3 (what else...)\r\n\r\nYeah, I hope KDE 4 & 5 will become <em>a lot</em> snappier and smoother too. Maybe they can use more RAM and graphics card . . . I don't know.\r\n\r\nTo Karol:\r\nKmail . . . meh . . . I guess most KDE users are using either Gmail or Mozzarella Thunderbird. I remember that a couple of years ago the UK magazine <em>\"Linux Format\"</em> made a ridiculously severe review of Kmail. Did you write that??"
    author: "Culip"
  - subject: "solved your own problem"
    date: 2013-12-24
    body: "It sounds like Windows 8 is just what you need. Before you go, did you <a href=\"https://bugs.kde.org/\">contribute any bug reports</a>?"
    author: "kallecarl"
  - subject: "Just to reply all the posts"
    date: 2013-12-24
    body: "Just to reply all the posts at once:\r\n1) Yes: I have reported @bugzilla once or twice, when I got some nasty bug, crashing kmix. Get fixed, very happy.\r\n2) No: I have not reviewed KMail, I am generally not good writing stuff, especially in correct manner...\r\n3) My thinkpad (ok my wife's nowadays) is happily running on win8, perfect as a platform to run the firefox/libreoffice.\r\n4) Me, i cannot switch to windows at work as i need correct admin tools, and putty alone is not enough for me..\r\n5) Thunderbird (version shipped with ubuntu since 12.10) get super unstable with ours exchange/imap setup - imap cannot really manage inboxes with 100k+ mails, oddly enough Kmail2/akonadi gets over it without freezing to death (ok, gets slow but is recovering after few seconds the connection to imap goes down) - fine for me as long as it is doing its job.\r\n6) What i try to refer to it is not the problem of the animation speed - I've twaked it to fastest setting/disabled all unnecesary stuff. There is a sub-second (1/5-1/2s) lag between the action (mouseclick) and the beginnign of the movement...\r\n\r\nI have not reported that before because i found it is not important enough and visible enough to everyone to notice. Otherwise it is to difficult for me to describe. In one word - it is not a bug , just annoyance. Well.. with my limited free time what works for me is to switch to already-working alternatives (god praise open-source). I hope to choose a kde more kde apps one day, but sadly it will not be today, nor tomorrow.\r\n\r\ngood luck with your projects and Merry Christmas/Happy Holiday.\r\n\r\nEOT\r\nKarol"
    author: "Karol"
  - subject: "I am quite curious to find"
    date: 2013-12-25
    body: "I am quite curious to find out what the source of your laggyness is, tbh. My laptop with a dualcore low power i5 is usually locked at 800 mhz and I don't use it often so frequently, Nepomuk is indexing full speed all my mail so one CPU is fully used. Still, I notice no lagging at all between switching or dragging windows. On my desktop (admittedly a far faster quadcore with 16gb ram) I have quite a heavy workload - I usually use around 8-10 gb of ram with 6 virtual desktops, each ~5-10 windows. Switching is always butter smooth, only Amarok can be slow to respond sometimes but nothing else is slow, certainly not half a second of lag...\r\n\r\nNow both systems have Intel CPU's and run the latest openSUSE with Plasma Desktop 4.11 and KDE Applications 4.12 - what are you running on and with? Perhaps NVidia or AMD graphics or something?"
    author: "jospoortvliet"
  - subject: "lightweight..."
    date: 2013-12-27
    body: "I do see several rendering bugs in the video. They are in-fact bugs though.\r\n\r\nHopefully they aren't inherent to XCB or anything... I've noticed all Qt 5 apps show some wacky frame when they open a new window. It's possible Wayland will be needed to really get Linux rendering done correctly.\r\n\r\nI don't at-all see this as a 'lightweightness' issue though. We have big computer with lots of memory, if frames aren't being posted smoothly to the screen it's not because the computer is bogged down, it's really some sort of bug. Eg we need correct software, not 'lightweight' software."
    author: "eean"
  - subject: "Video recording is always"
    date: 2013-12-28
    body: "Video recording is always adding stress to the setup, so that we see rendering issues normally not present. I assume that the opening of Kickoff after 6 sec in the video is one of such cases. I once made a video of smooth window resizing and it was extremely unsmooth in the video.\r\n\r\nI'm surprised by your experience with Qt5 applications. I know that there can be issues with QtQuick applications but normal QWidget based applications should not show any problems. It might depend on the Qt 5 version you are using, XSync extension was disabled for KWin before 5.2 IIRC.\r\n\r\nMany issues though will hopefully go away with Wayland. We won't have issues with half rendered buffers any more ;-) And I hope we will at some day have video recording capabilities directly built into the compositor."
    author: "mgraesslin"
  - subject: "Too glassy and wearing"
    date: 2013-12-29
    body: "It is too \"faded\" and fonts are not rendered correctly (there are font size differences between DEs). I get better usability with GNOME3, but especially with MATE. As others said, the new KDE is like a 3D game in itself at first sight, you can play with it and nothing serious. \r\n\r\nBug reports? Sure, but first, you must convince that it is useful for me. Anyway, I don't see the point in having the DE using accelerated hardware to work. Let that feature for the applications and don't burn the processor. Those features are for DE designed for SciFi movies - I got tired very fast by your plasma and stopped using it (anyway, I think is good enough for youngsters who like to show off their system in front of their friends). \r\n\r\nConstructive? When first appeared the wobbly windows effect (and all other effects) I activated all and showed to others.  Got tired on a daily basis and kept only the shadows for the windows and menus for a little spatial effect. That is all. So, I like to have in GNOME/MATE a gtk+ theme like in Ubuntu (Ambiance/Radiance} and a \"layout\" like in OS X for a pleasant DE but I don't have time to sit and admire it - I see it only at start-up and shutdown! Also, I'm someone who shutdown any social services and who have a good firewall (I hate being taken for a fool) on Windows 8.1, LinuxMint and \"afwall\" with the possibility to limit the applications permissions on a rooted Android 4.1.2 which I don't want to upgrade. I use my desktop operating system mainly for work, the Android tablet is enough for news, entertainment, multimedia and social services.   "
    author: "funlw65"
  - subject: "KDE 5"
    date: 2014-01-03
    body: "when is KDE 5 going to be released!"
    author: "Claude"
  - subject: "Never"
    date: 2014-01-04
    body: "The short answer is: never. KDE is the community and not the product. We don't version our community and don't release it. As KDE is the community and not the product I don't know what you referred to with the word \"KDE5\". Mabye you meant the workspaces (Plasma, which this news is about), maybe you mean our frameworks, maybe our applications or everything together."
    author: "mgraesslin"
  - subject: "I was talking about the"
    date: 2014-01-07
    body: " I was talking about the product."
    author: "Anonymous"
  - subject: "To be precise, I am talking"
    date: 2014-01-07
    body: "To be precise, I am talking about plasma 2."
    author: "Anonymous"
  - subject: "Plasma is to be released in"
    date: 2014-04-01
    body: "Plasma is to be released in June or July 2014. Frameworks 5, the old 'KDELIBS' comes about a month earlier and the applications will perhaps do a first release by tend of this year or the beginning of next year. That is also why Martin answered what he did - KDE no longer releases everything at once, so Plasma is what you were asking for ;-)"
    author: "jospoortvliet"
  - subject: "Intel Celeron 847 with 4GB memory"
    date: 2014-04-08
    body: "I have Intel Celeron 847 with 4GB memory and i must say main problem are features like indexing and not graphics effects. On Windows 7 there is option to disable indexing search at all or only some parts of indexing and let to have only basic search function. i know that KDE4 have too option to have only disable index files thats good. what i want say is that in default configuration are more services enabled. i thing KDE need enhance selection dialog (i mean this dialog where is possible to set \"High Display, High CPU and so\") to like\r\n\r\nSlow computer with slow graphics card and slow disk, Slow computer with good graphics card and good sik and so.. what do you think?"
    author: "Luko"
  - subject: "WTF KDE team! NAILED IT! :D"
    date: 2016-12-28
    body: "<p>2 decades I had nothing but wonder and love for GNU/Linux. And over the course of those 2 decades, on occasion, I attempted to switch from Windows to GNU/Linux. Most of the attempts failed, not because of my skill (or lack of), but simply because GNU/Linux as a desktop system was not ready/plolished enough. 2 months ago I finally switched from Windows to GNU/Linux, trying to find my way using Gnome 3. It did the trick for a while (I set up Gnome 3 in a way that was familiar to my old desktop setup), but function was limited, options was limited, and performance slow. Closing a context menu sometimes took 3 seconds, not because of hardware, but because of some faulty design.</p><p>Prejudiced somehow made me always prefer Gnome, I think because of its phylosophy(?).. KDE I disliked (prejudiced!)&nbsp;because of the cartoonisch (childisch?) logo, but most of all, the overly use of the letter K in every program name (I want a system to be \"mine\", not the system to be owned by some dude named e.g. \"Kevin\".. :p or at least don't want it to be owned by someone or something that apparantly is not me :p).. KDE also made me shy away because of too many colors, bells, etc (angry fruit salid)..</p><p>Now..... I switched from Gnome 3 to KDE 5 Plasma and find......... it is increadably FAST, POLISHED, configurable, stable, responsive, eye-candy without getting too flashy, incredible composition...</p><p>WTF KDE team! NAILED IT! :D</p><p>So much thanx! I finally ditched Windows and replaced it for GNU/Linux out of principle, but could not fully enjoy it (on Gnome). Although I still have prejudiced feelings towards the name \"KDE\" (feeling rooted in old experiences), my experiences with KDE tell me a different story every day; loving it! :)</p><p>Hoping one day three wishes will come true: 1) a setting to make the background of the task manager (panel) fully transparent (because there is nothing as beautiful as a full screen painting / beamer screen of a desktop on the wall with just an application menu on the left and a clock on the right, 2) a settings to enable the desktop background in slide show mode fade between backgrounds / paintings, and 3) that one day this amazing piece of software will just be called \"Plasma\" (because it has a good promotional sound to it)</p><p>Cheers! :)</p>"
    author: "Boon"
---
<p>
KDE's Plasma Team presents a first glimpse at the evolution of the Plasma Workspaces. Plasma 2 Technology Preview demonstrates the current development status. The Plasma 2 user interfaces are built using QML and run on top of a fully hardware accelerated graphics stack using Qt5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma 2 is a converged workspace shell that can run and switch between user interfaces for different formfactors, and makes the workspace adaptable to a given target device. The first formfactor workspace to be demonstrated in this tech preview is Plasma Desktop, showing an incremental evolution to known desktop and laptop paradigms. The user experience aims at keeping existing workflows intact, while providing incremental visual and interactive improvements. Some of those can be observed in this technology preview, many others are still being worked on.
</p>

<iframe width="560" height="315" src="//www.youtube.com/embed/DPxmIranCgs" frameborder="0" allowfullscreen></iframe>


<h2>Plasma Shell</h2>

<h3>Architecture &amp; Roadmap</h3>
<p>
While the underlying graphics stack changes fundamentally in the new Plasma edition—moving it to a fully hardware accelerated OpenGL(ES) scenegraph—the user interface components have been ported to make use of this new technology. As such, this is not a rewrite from scratch, but a port to a new graphics system. Plasma 2 Technology Preview builds on top of <a href="http://dot.kde.org/2013/12/17/qt-52-foundation-kde-frameworks-5">Qt 5.2</a>, QtQuick2's OpenGL scenegraph and the <a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a>.
</p>
<p>
KDE Frameworks 5 is a modular version of the KDE Libraries and will be released independently from the Workspace. A preview of KDE Frameworks 5 has been postponed slightly to early 2014, a first stable release is planned for later that year. Together with Plasma's converged Workspace shell, which supports switching between different, modular device-adaptable Workspaces, Plasma is more suitable for deployment on a wider range of devices. The team planses to release the first stable version of Plasma 2 this summer, with an end-user ready desktop Workspace. More formfactor Workspaces, such as <a href="http://plasma-active.org">Plasma Active</a> and <a href="http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas">Plasma Mediacenter</a> are planned to be added as they reach stable ports to Qt5, KDE Frameworks 5 and the Plasma 2 Framework.
</p>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/plasma2tp-calendar.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma2tp-calendar_wee.png" /></a><br />Clock and Calendar</div>
<h3>Current Status</h3>

<p>
Plasma 2 is in heavy development; this tech preview reflects a snapshot of this process. While the basic functionality is there, it contains many known and unknown bugs. The team is working on completing and improving the underlying infrastructure and smoothing out the user experience in more and more workflows. Plasma 2 is "dog-foodable", but not yet fit for wider testing of its functionality. The Plasma team will open the issue tracker in the coming weeks, after most of the show-stoppers have been fixed. Session- and power management services have been ported and are functional. Components that together make up the desktop, such as the task manager, launcher menu, notification area, clock and calendar have basic, but functional ports available. The coming weeks and months will be spent on finishing this functionality, ironing out bugs, visual polish and applying some smaller architectural updates to a number of parts of the workspace experience.
</p>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/plasma2tp-jobs-simple.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma2tp-jobs-simple_wee.png" /></a><br />File Transfers</div>
<p>
Plasma 2 Technology Preview starts up with a basic default desktop layout, providing an application launcher, a pager to manage and switch between virtual desktops, a taskbar, notification area and a clock. It comes with a number of example widgets. All of these components are basically functional, and will be further polished in the coming weeks and months.
</p>

<h2>KWin Window Manager and Compositor</h2>

<p>
The window manager and compositor of the Plasma Workspaces, KWin, has reached a close-to-production-grade quality in this technical preview. This is a very important milestone, given that KWin was the application most difficult to port by the KDE community.
</p>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/plasma2tp-networkmanagement.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma2tp-networkmanagement_wee.png" /></a><br />Network Management</div>

<p>
The porting of KWin was difficult because it made heavy use of low-level windowing system specific API inside the Qt libraries, which was removed due to the introduction of the Qt Platform Abstraction in the Qt 5 releases. More details about the required changes are available in the KWin maintainer's <a href="https://conf.kde.org/en/Akademy2013/public/events/3">Akademy talk</a>. Most of the required API changes were already incorporated in the 4.11 release.
</p>

<p>
The Qt plugin for the X11 windowing system switched from XLib to XCB. This required rewriting large parts of the event filter inside KWin - a step which could only be done after porting to Qt 5. It was completely unknown what kinds of problems would be hit by such a port. There are not many window managers and compositors which have been ported to XCB. During the port the KDE team needed to add <a href="https://codereview.qt-project.org/62205">new features</a> to Qt, was hit by <a href="https://codereview.qt-project.org/66578">regressions</a> and <a href="http://commits.kde.org/kde-workspace/008ac5efabfb99f04813e5dad29c2d0a92d13fc5">bugs</a> both in Qt and the XCB protocol bindings. Given that KWin had to be rebased on top of a new windowing system abstraction inside Qt, it is a great achievement to have a near-production-quality X11 window manager and compositor after such a short time.
</p>

<p>
A third area of unknown issues was the usage of OpenGL inside the compositor and QtQuick. This introduced a completely new area of threading related issues, which are explained in more detail in <a href="http://blog.martin-graesslin.com/blog/2013/11/kwin5-qtquick-2-and-the-caused-changes-for-opengl/">this blog post</a>. Overall these issues are mostly solved, though the Aurorae-based window decorations have not reached production-ready quality; the Oxygen window decoration is recommended at the moment.
</p>

<p>
Although there was lots of porting involved, there are also new features which became available in the technology preview. The window decorations are now able to follow the color scheme of the decorated window—an important feature for the excellent image and photo applications Krita and digiKam by the KDE community that prefer a dark color scheme. This feature is also available through the window rules framework.
</p>
<iframe width="560" height="315" src="http://www.youtube.com/embed/UNkbWcfswbk" frameborder="0" allowfullscreen></iframe>
<p>
In the scope of Google Summer of Code, the configuration module for Desktop Effects was rewritten. It is making strong use of the new QtQuick Controls to enable a more flexible configuration. One of the first new features added to this configuration module is the integration of video previews of the effects. These videos have been created by Google Code-In students.
</p>
<iframe width="560" height="315" src="http://www.youtube.com/embed/qR65XMcpQg4" frameborder="0" allowfullscreen></iframe>

<h2>Getting the Plasma 2 Tech Preview</h2>
<p>
We recommend <a href="http://community.kde.org/Frameworks/Building">building</a> Plasma 2 Tech Preview from our git repositories. Git tags for this tech preview have been created. Packagers can pull the source code with the "plasma2tp" tag from the respective git repositories. Most people will want to regularly update to the latest version of the KDE Frameworks 5 in order to get a constant stream of improvements. This is best achieved with <a href="http://kdesrc-build.kde.org/">kdesrc-build</a>, which automates the fetching, building and installing and updating of the respective source code modules. Regular testing <a href="http://dot.kde.org/2013/12/09/early-kde-plasma-2-images-now-available">ISO images</a> have become available, and are in the process of receiving the last set of updates that have gone in.

</p>
