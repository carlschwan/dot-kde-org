---
title: "KDE Commit-Digest for 24th November 2013"
date:    2013-12-14
authors:
  - "mrybczyn"
slug:    kde-commit-digest-24th-november-2013
---
In <a href="http://commit-digest.org/issues/2013-11-24/">this week's KDE Commit-Digest</a>:

<ul><li>Device notifier works in Plasma 2, <a href="http://userbase.kde.org/Power_Devil">Power Devil</a> ported to KF5/Qt5</li>
<li><a href="http://gwenview.sourceforge.net/">Gwenview</a> gains RAW preview</li>
<li>In <a href="http://extragear.kde.org/apps/kipi/">KIPI</a>, new GoogleDrive and Dropbox export plugins are available</li>
<li>New option in configuration Appearance->Borders->Scrollbars Visibility controls scrollbar visibility in <a href="http://kate-editor.org/">Kate</a></li>
<li>Work continues on urlbar in <a href="http://rekonq.kde.org/">rekonq</a></li>
<li>There are many new optimizations: <a href="http://pim.kde.org/akonadi/">Akonadi</a> database structure changes, memory usage in <a href="http://userbase.kde.org/Trojit%C3%A1">Trojitá</a>, mail directories accesses in <a href="http://kontact.kde.org/kmail/">KMail</a></li>
<li>Akonadi removes unneeded Strigi and ODBC/Virtuoso backends support.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-11-24/">Read the rest of the Digest here</a>.