---
title: "KDE e.V. Quarterly Report for Q4 2012"
date:    2013-03-08
authors:
  - "kallecarl"
slug:    kde-ev-quarterly-report-q4-2012
---
The KDE e.V. report for the fourth quarter of 2012 has been published. It gives an overview of the activities KDE e.V. supported during that period, including reports of various sprints, conferences and projects. 

The feature article by Aleix Pol i Gonzàlez introduces KDE España and describes how the global KDE Community can be realized at a local level. Since 2006, KDE supporters and contributors in Spain have been building an organization and strong relationships with local free and open software groups. Together with the Akademy Team, KDE España and local free software groups are organizing <a href="http://akademy2013.kde.org/">Akademy 2013 - Bilbao</a> in the <a href="http://en.wikipedia.org/wiki/Basque_Country_(greater_region)">Basque Country</a>. Imagine the combination of Akademy, the <a href="http://dot.kde.org/2013/02/14/akademy-and-qt-contributors-summit-join-forces">Qt Contributors Summit</a> and <a href="http://www.kde-espana.es/akademy-es2013/anuncio.php">Akademy-es</a> in Bilbao (the Wow! capital of <a href="http://en.wikipedia.org/wiki/Biscay">Bizkaia</a> in northern Spain).

Read the <a href="http://ev.kde.org/reports/ev-quarterly-2012_Q4.pdf">full report</a> (600kb PDF). 

The work of KDE e.V. would not be possible without our financial supporters. If you want to support the work of KDE but can't contribute directly, please consider <a href="http://jointhegame.kde.org/">Joining the Game</a>.