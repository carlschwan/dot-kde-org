---
title: "KDE Commit-Digest for 20th January 2013"
date:    2013-01-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-20th-january-2013
---
In <a href="http://commit-digest.org/issues/2013-01-20/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://marble.kde.org/">Marble</a> adds Link and NetworkLink tag handing and geodata backends</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> introduces a dedicated borders element in Aurorae</li>
<li><a href="http://community.kde.org/KTp">KDE Telepathy</a> makes it possible to have pinned contacts, merges big model refactor</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> visualizes variables and other inline objects, adds "Fit Text Width" zoom mode</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> moves all folder options into one Properties dialog.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-01-20/">Read the rest of the Digest here</a>.