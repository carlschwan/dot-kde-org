---
title: "KDE Commit-Digest for 31st March 2013"
date:    2013-04-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-31st-march-2013
---
In <a href="http://commit-digest.org/issues/2013-03-31/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://kate-editor.org/">Kate</a> improves the <a href="http://www.vim.org/about.php">Vim</a> mode, including adding the Vim command bar</li>
<li><a href="http://amarok.kde.org/">Amarok</a> now recognizes Data CDs as collections; scripting interface has been enhanced with requested method</li>
<li><a href="http://www.python.org/">Python</a> support in <a href="http://www.kdevelop.org/">KDevelop</a> adds identifier-similarity aware sorting of completion items</li>
<li><a href="http://userbase.kde.org/Kraft">Kraft</a> lets user select his/her own identity from address book in settings</li>
<li><a href="http://userbase.kde.org/Rekonq">Rekonq</a> sees work on session management</li>
<li>Work on threading in <a href="http://www.calligra-suite.org/">Calligra</a> on Android (<a href="http://blogs.kde.org/2013/03/24/coffice-calligra-android-available-now">Coffice</a>) to improve interface response time</li>
</ul>

<a href="http://commit-digest.org/issues/2013-03-31/">Read the rest of the Digest here</a>.