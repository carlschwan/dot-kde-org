---
title: "53 x Awesome - Google Summer of Code and Outreach Program for Women 2013"
date:    2013-06-02
authors:
  - "devaja"
slug:    53-x-awesome-google-summer-code-and-outreach-program-women-2013
comments:
  - subject: "unreadable on FF21 & IE8"
    date: 2013-06-04
    body: "Major parts of the article are unreadable on Firefox 21 and Internet Explorer 8 on WinXP with 1280x1024 resolution, as the various boxes (\"Planet KDE\", \"KDE Look \" etc) are hovering over the text and hide many parts of the text on the right hand side."
    author: "VIctor T."
  - subject: "referred to SysAdmin"
    date: 2013-06-04
    body: "Your comment has been forwarded to the people who take care of these issues. "
    author: "kallecarl"
---
The announcement of the students accepted for Google Summer of Code (GSoC) and Outreach Program for Women (OPW) 2013 opens a new chapter in KDE contribution. KDE has participated in GSoC since its first season in 2005. Every year is special and exciting for both the students and the KDE Community. 
<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2013logoDot.png" /></div>

Outstanding students get to try their hands at real world programming, some for the first time and some as experts. They have the experience of working in a free software community, being part of a top technical team. They have the opportunity to learn, innovate, and do meaningful development work within a leading free and open software project. They also get to contribute to the creation of something that is used by millions of people around the world. Now how awesome is that!

KDE mentors are also excited. Once again, they get the opportunity to teach students, pass on the beacon of knowledge and guidance, and spread the importance and greatness of working in a community. They gently motivate the students and nourish the desire to contribute to free software. It doesn't come as a surprise that many GSoC student participants go on to become mentors in future programs. With such delightfully amazing students and inspiring mentors—along with a marvelous gamut of projects, GSoC and OPW 2013 are bound to be a huge success. 

This year the KDE Community welcomes 53 new contributors: 50 students selected to participate in GSoC 2013 and 3 women selected for the OPW! We extend a hearty welcome to all of you! Congratulations for officially being a part of one of the most magnificent communities ever!

<h2>Google Summer of Code 2013</h2>
This year students will be working on a wide range of coding projects ranging from Amarok to Zeitgeist. The <a href="http://www.google-melange.com/gsoc/org/google/gsoc2013/kde">published list of the accepted student proposals</a> has more details. It's important for students to stay in close touch with their mentors and have a lot of communication. The <a href="http://www.google-melange.com/gsoc/events/google/gsoc2013">GSoC timeline</a> and the Freenode IRC channel #kde-soc are important for keeping up with the details. Happy Summer of Coding!

<h2>Outreach Program for Women 2013</h2>
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/opw-logo.png" /></div>
This is the first year of KDE's participation in the Outreach Program for Women, which promotes the involvement of women in free software and includes both coding and non-coding projects. <a href="https://live.gnome.org/OutreachProgramForWomen/2013/JuneSeptember#KDE">Three women have been accepted to work with KDE</a> this year. 

<h2>Take Heart - Don't Give Up</h2>
To all the students whose proposals were not selected this time – we encourage you to stick with KDE. Proposal selection takes into account many factors, such as how realistic the proposal is, how KDE will benefit, how much the student will learn, how much time the mentor and student will have and others. GSoC is a program that KDE takes seriously, so project selection takes hard thinking. Even if your proposal was not selected, your interest and efforts are greatly appreciated. There is no need for a program or any other formal reason for you to start contributing to free software or to be <a href="http://community.kde.org/Getinvolved">a member of the KDE Community</a>! Join the IRC channels, mailing lists, or discussions forums. Pick a project that inspires you and <a href="http://techbase.kde.org/Contribute">get started</a>! There are absolutely no limits to what you can do!

<h2>Season of KDE 2013</h2>
Again this year, Season of KDE (SoK) will be another opportunity for those whose GsoC proposals were not accepted. SoK is for motivated students with a good proposal and a willingness to learn and get involved with KDE. SoK participants get to work with experienced mentors and hack on KDE for the summer and beyond. Those who complete the program receive an awesome t-shirt and a certificate. If you are interested, please <a href="mailto:kde-soc-mentor-owner@kde.org ">email the admin team</a>.

<h2>Many Thanks</h2>
<strong>A big thank you</strong>. To all of the students who applied for GSoC and OPW this year and worked so hard on your proposals. To the mentors for your guidance. To the KDE Community for your support and help. Special Thanks to <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2013">Google</a> and <a href="http://www.kdab.com/">KDAB</a> without whom the 2013 Google Summer of Code and Outreach Program for Women would have never been possible!