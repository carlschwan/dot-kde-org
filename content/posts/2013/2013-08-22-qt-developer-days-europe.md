---
title: "Qt Developer Days - Europe"
date:    2013-08-22
authors:
  - "jlayt"
slug:    qt-developer-days-europe
---
<a href="http://www.qtdeveloperdays.com/">Qt Developer Days Europe 2013</a>, the premier annual event for Qt developers, is being held in Berlin from October 7th to 9th. The event is co-hosted by <a href="http://qt.digia.com/">Digia</a>, <a href="http://www.kdab.com/">KDAB</a> and <a href="http://www.ics.com/">ICS</a>, in partnership with <a href="http://ev.kde.org/">KDE e.V.</a>. KDE e.V. is the non-profit organization that supports the KDE Community in legal, organizational and financial matters. The KDE Community is a major contributor to the <a href="http://qt-project.org/">Qt Project</a> through development contributions, involvement in the KDE Free Qt Foundation, and co-host of this year's Qt Contributors Summit alongside Akademy in Bilbao, Spain. Several KDE developers past and present will be featured in the <a href="https://devdays.kdab.com/?page_id=382">main conference program</a>. KDE is also organizing the <a href="http://qt-project.org/wiki/Qt-Contributors-Day-2013-Berlin">Qt Contributors Day</a> on Monday, October 7th, and will have a KDE promotion booth in the exhibition area.

<h2>Qt Contributors Day</h2>
The Qt Contributors Day is an opportunity for Qt contributors to meet and discuss the development of the Qt Project. A major topic will be the continuing contribution of KDE developed features into Qt and the development of KDE Frameworks 5. The floor will also be open for any major topics currently affecting Qt development, governance or community, especially those where the KDE Community could make a contribution. The format will be a mix of unconference and hacker space with topics for discussion able to be scheduled during the day, and informal discussions, code reviews and hacking taking place in-between. Space in the Contributors Room is limited; to participate in the Qt Contributors Day or suggest a topic to discuss, please register on the <a href="http://qt-project.org/wiki/Qt-Contributors-Day-2013-Berlin">Qt Contributors Day wiki page</a>. To attend the Qt Contributors Day requires a Qt Developer Days ticket valid for the Monday.

<h2>Sponsored tickets</h2>
As an official partner, KDE e.V. has a limited number of sponsored tickets available to active members of the KDE Community who wish to attend but who cannot afford to do so. These tickets will be allocated on a merit basis in return for assistance at Qt Developer Days, Qt Contributors Day, and the KDE booth. To apply for a sponsored ticket, please email jlayt@kde.org with an outline of why you want to attend and how KDE will benefit from your participation.

The KDE booth will be promoting KDE both as a community for Qt developer participation, and as a source for high-quality Qt add-ons to extend the Qt Project offerings. Assistance is required to run this booth, so if you have a ticket to Qt Dev Days and have time to spare between sessions, please contact jlayt@kde.org to volunteer.

KDE will also have an active role in <a href="http://www.qtdeveloperdays.com/northamerica#.UhYl4hKLdRQ">Qt Developer Days - North America</a> in San Francisco November 6–8. Watch the <a href="https://mail.kde.org/mailman/listinfo/kde-community">KDE Community mailing list</a> for announcements and more information.

See you at Qt Dev Days!