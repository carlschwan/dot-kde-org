---
title: "KDE Commit-Digest for 10th March 2013"
date:    2013-04-21
authors:
  - "mrybczyn"
slug:    kde-commit-digest-10th-march-2013
---
In <a href="http://commit-digest.org/issues/2013-03-10/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> now supports filtering tracks to scrobble to Last.fm (by label), transcoding into <a href="http://www.opus-codec.org/">Opus format</a> (if ffmpeg supports it)</li>
<li>A new model appears to provide easy access to KWin's Clients from QML; better screen edges support</li>
<li>Improved settings handling in <a href="http://community.kde.org/KTp">KDE Telepathy</a></li>
<li><a href="http://kate-editor.org/">Kate</a> adds a filter to project search mode</li>
<li>Work on an initial port of <a href="http://pim.kde.org/akonadi/">Akonadi</a> server to new notifications; a job for discovering all special collections.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-03-10/">Read the rest of the Digest here</a>.