---
title: "Akademy 2013 Call for Presentations and Registration"
date:    2013-02-07
authors:
  - "sealne"
slug:    akademy-2013-call-presentations-and-registration
---
The Call for Presentations for Akademy 2013 is now open. <a href="http://akademy2013.kde.org/">Akademy</a> is <strong>the</strong> KDE community conference. If you are working on topics relevant to KDE or Qt, don't miss your chance to present your work and ideas at the conference from the 13th - 19th July in Bilbao. The days for the main talks are Saturday and Sunday, 13 and 14 July. The rest of the week will be <a href="http://en.wikipedia.org/wiki/Unconference">unconference</a> sessions and workshops.

<a href="http://akademy2013.kde.org/program-committee">The Program Committee</a> is looking for talks on the following topics:
<ul>
<li>Collaboration between KDE, the Qt Project and other projects that use KDE or Qt.</li>
<li>Porting to Qt 5 and KDE Frameworks 5</li>
<li>Mobile/embedded applications, use cases and frameworks</li>
<li>Increasing our reach through efforts such as accessibility, promotion, translation and localization</li>
<li>Presentation of new applications; new features and functions in existing applications</li>
<li>Improving KDE governance and processes</li>
</ul>
Talk proposals that do not quite fit this list will also be considered if they are relevant to KDE.

At Akademy, attendees start new projects, find collaborators and discover new things. This is your chance to show your work and make a splash! 

Please <a href="https://conf.kde.org/Akademy2013/cfp">submit</a> your proposal <strong>before 23:59 on the 15th of March (CEST)</strong>. Speakers will be notified by April 15th.

There will be a separate announcement later for people who are interested in organizing a workshop session, a BoF or a project room.

You can also <a href="http://akademy2013.kde.org/register">register</a> for Akademy now. The event is free of charge but registration is required. 

Akademy attracts people from around the world. Travel and accommodation are important considerations for the Akademy Team and for people who are attending Akademy. The first part of July will be busy in Bilbao; there is a <a href="http://www.bilbaobbklive.com/2013/en/">big music festival</a> taking place from 11-13 July. <b>We strongly encourage you to plan your trip to Akademy 2013 and book your hotel as soon as possible</b>.  

KDE e.V. may reimburse Akademy travel costs according to the <a href="http://ev.kde.org/rules/reimbursement_policy.php">KDE e.V. reimbursement policy</a>. An online request form will be available soon.

<h2>About Akademy</h2>
Akademy is an annual 7-day event, bringing together hundreds of KDE contributors, users and industry partners. It is the place to learn about the latest features and share ideas for future releases of KDE's software. Participants gain in-depth technical insights, meet and work with KDE and Qt experts, and take part in deciding the future direction of KDE.