---
title: "Akademy 2014 Call for Host"
date:    2013-08-31
authors:
  - "kallecarl"
slug:    akademy-2014-call-host
---
The KDE Community is looking for a host for Akademy 2014.

Akademy is the annual gathering of the KDE Community, one of the largest in the world of Free and Open Source Software. At Akademy, KDE people gather to exchange development ideas, plan for the future, and discuss other important issues. It is an extraordinary occasion for creativity, enthusiasm, commitment, close working relationships, innovation and just plain hard work.

<div style="float: center; padding: 1ex; margin: 1ex;"><a href="http://byte.kde.org/~duffus/akademy/2013/groupphoto/"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2013-group-photo700.jpg" /></a><br />Akademy 2013 attendees (click for larger) <small><small>by Knut Yrvin</small></small></div>

Hosting Akademy is a rare opportunity. The local hosting team plays a key and highly visible role in producing this event, being actively involved within the KDE community and with the local community as well. The hosting community gains financially with attendees visiting from all over the world. In addition, the local community can benefit from the value of Free and Open Source Software and the close involvement of one of its premier organizations. People from local businesses, educators, students, government officials and technology enthusiasts are encouraged to attend and are warmly welcomed. As has been proven by previous Akademies, the opportunity is as big and varied as the hosting team can create.

<h2>Akademy Program</h2>
The conference program is usually structured as follows:
<ol>
<li>Friday is for travel, arrival and a pre-registration/get acquainted event.</li>
<li>On Saturday and Sunday, the actual conference takes place. There are 2 or 3 keynotes and 25 to 30 sessions in tracks so that attendees can choose according to their interests.</li>
<li>The remainder of the week is used for intensive coding and workshops for multiple smaller groups of 10 to 50 people.</li>
</ol>
The venue needs to offer a lecture room that fits 300+ people, a smaller lecture room for up to 150 people, and several smaller rooms for workshops.

<div align=center style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDEStudentsMentorsBoFMKBY.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEStudentsMentorsBoFMKBY700.jpg" /></a><br />Mentor BoF<br /><small><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

<h2>Host Requirements</h2>
Akademy requires a location in or near Europe that is easy to reach, preferably close to an international airport. The conference venue itself should be equipped to host attendees comfortably, and provide reliable, high speed Internet access for all activities during the week. Ample hotel accommodations and places to eat should be close to the venue or easily reachable.

Organizing an Akademy can be demanding and requires a significant investment of time and effort. While there will be considerable assistance from people who have experience producing previous Akademies, the local team should be prepared to spend a lot of time on it. Previous hosting teams have found this to be a rewarding project that is recognized and appreciated by the KDE Community. The benefits to KDE, the attendees, the Akademy hosts and organizers, and the local community are immeasurable.

For more detailed information, please see the <a href="http://ev.kde.org/akademy/CallforHosts_2014.pdf">2014 Call for Hosts</a> (pdf). Questions or concerns should be addressed to the <a href="mailto:kde-ev-board@kde.org">Board of KDE e.V.</a> or the <a href="mailto:akademy-team@kde.org">Akademy Team</a>. Applications will be accepted until 1 October 2013.