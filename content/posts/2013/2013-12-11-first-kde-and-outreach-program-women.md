---
title: "A First - KDE and the Outreach Program for Women"
date:    2013-12-11
authors:
  - "Mamarok"
slug:    first-kde-and-outreach-program-women
comments:
  - subject: "Great"
    date: 2013-12-12
    body: "This sort of thing is great to see!  I am very happy this worked out so well.  So KDE is planning to continue its involvement?"
    author: "TheBlackCat"
  - subject: "Absolutely"
    date: 2013-12-14
    body: "I think there is no question that we would like to continue participating in this and similar programs, but it will probably come down to funding.\r\n\r\nWe are very grateful for the support we've got this time, both from the sponsor of KDE's own slot as well as getting those additional slots from the program's general budget."
    author: "kevkrammer"
---
The KDE Community participated in the <a href="http://gnome.org/opw/">Outreach Program for Women</a> (OPW) for the first time this year. It was more successful than expected. KDE got many great applications and mentored 4 students contributing to Free Software. The Outreach Program for Women encourages women to get involved in free and open source software. It provides a supportive community to begin contributing any time throughout the year, and offers focused internship opportunities twice a year with several free software organizations. Unlike the Google Summer of Code (GSoC), the Outreach Program for Women is open to non-students and non-coders.

KDE was glad to attract KDAB as a sponsor for one program slot. 2 other places were supported financially by the OPW sponsor pool that included many prestigious organizations such as Bloomberg, Google and several others. <a href="http://www.gnome.org/">GNOME</a> started OPW and has <a href="http://gnome.org/opw/">more information about the program</a>.

<h2>Krita</h2>
One of the KDE projects that participated in OPW was <a href="http://krita.org/">Krita</a>. <strong>Maria Far</strong> and <strong>Chinkal Naglpal</strong> did a great job for Krita this summer as OPW interns. They set up <a href="http://www.zazzle.com/kritashop">a webshop</a> selling Krita-branded merchandise and helped manage the website. They created a coordinated system for the webshop, filled it with great items, created a database of artists who use Krita, integrated a variety of social networks, and fixed many issues with the website.

Mentor <strong>Boudewjin Rempt</strong> said: 
<blockquote>
I have really felt the results of their work both in terms of new contributors joining and in terms of new users. Financially, the shop isn't bringing in much yet -- drawing people there turns out to be tricky. I particularly enjoyed the series of artist interviews they did -- and are continuing with! It's been a learning experience for me, too!</blockquote>
<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/kritashop700.png" /><br /><a href="http://www.zazzle.com/kritashop">Shop Krita</a></div>

<h2>Artikulate</h2>
Another project that also offered opportunities for OPW participants was <a href="http://userbase.kde.org/Artikulate">Artikulate</a>. Artikulate is a young project, born in the <a href="http://edu.kde.org/">KDE Education playground</a> less than a year ago, and still on the way to its first end-user release. Despite its age, this pronunciation training application attracted two people, <strong>Magdalena Konkiewicz</strong> (OPW participant) and <strong>Oindrila Gupta</strong> (GSoC participant). Their projects had the goal to help drive Artikulate to the first release. Their mentor <strong>Andreas Cord-Landwehr</strong> said: 
<blockquote>During the last three months, Magdalena and Oindrila did great work in polishing the user interfaces, establishing workflows for contributors, creating course material and documentation, and much more. With their work and successful recruiting of new contributors, Artikulate really made a big step forward.</blockquote> 
The most visible contributions—though only a small piece of their work—can be seen in the new configuration dialogs, import mechanisms for courses, and learning statistics. As an immediate result of the work done in OPW, a preview release of Artikulate will be released soon.

<h2>Rewarding outcomes</h2>
The OPW participation was a very rewarding experience for the KDE Community. The close collaboration between interns and mentors also helped to integrate the new contributors into KDE's work and to create a pleasant team experience for them. All four participants want to continue contributing to their respective projects as they unanimously felt very welcome. <strong>Myriam Schweingruber</strong>, OPW coordinator for the KDE Community together with <strong>Lydia Pintscher</strong>, said:
<blockquote>It was a great experience to see the many interesting applications when the program started, and the contagious enthusiasm the four interns showed throughout the duration of the project. I am confident that all of them will continue to be great contributors to our community.</blockquote>