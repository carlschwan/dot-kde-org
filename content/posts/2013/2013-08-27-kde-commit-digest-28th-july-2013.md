---
title: "KDE Commit-Digest for 28th July 2013"
date:    2013-08-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-28th-july-2013
---
In <a href="http://commit-digest.org/issues/2013-07-28/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://techbase.kde.org/Projects/Edu/Artikulate">Artikulate</a> allows downloading courses and using them in the program, loads courses only on demand</li>
<li><a href="http://userbase.kde.org/Konsole">Konsole</a> sees various search improvements</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> adds support for Globally Unique Identifier (GID)</li>
<li><a href="http://amarok.kde.org/">Amarok</a> improves consistency of information displayed in the Recently Played list</li>
<li>Optimizations in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li></li>
</ul>

<a href="http://commit-digest.org/issues/2013-07-28/">Read the rest of the Digest here</a>.