---
title: "KDE Commit-Digest for 4th August 2013"
date:    2013-08-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-4th-august-2013
---
In <a href="http://commit-digest.org/issues/2013-08-04/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://kate-editor.org/">Kate</a> adds an extension to Kate Vim: new shortcut ctrl-e in the emulated command bar, that makes refactoring duplicated complex expressions easier; other search and replace improvements include multi-line text searches and upper/lower-casing replacement captures</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> supports CONDSTORE IMAP extension for incremental flags change synchronization</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> implements server-side notification filtering, adds support for a globally unique identifier for items</li>
<li><a href="http://userbase.kde.org/Artikulate">Artikulate</a> adds multiple recordings for courses</li>
<li><a href="http://krita.org/">Krita</a> implements <a href="http://en.wikipedia.org/wiki/Anisotropy">anisotropic</a> spacing for brushes</li>
<li>KDE Frameworks porting continues, for instance in the new i18n semantics.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-08-04/">Read the rest of the Digest here</a>.