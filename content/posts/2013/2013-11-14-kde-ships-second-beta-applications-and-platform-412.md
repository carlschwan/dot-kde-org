---
title: "KDE Ships Second Beta of Applications and Platform 4.12"
date:    2013-11-14
authors:
  - "tsdgeos"
slug:    kde-ships-second-beta-applications-and-platform-412
---
KDE has released the <strong>second beta</strong> of the new versions of Applications and Development Platform. With API, dependency and feature freezes in place, the focus is now on fixing bugs and further polishing. Your assistance is requested.

This release does not include Plasma Workspaces, which was frozen for new features in 4.11.x. In addition, the Development Platform has seen only minor changes for a number of releases in anticipation of <a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a>. So this release is mainly about improving and polishing KDE Applications.

A list of improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.12_Feature_Plan">4.12 Feature Plan</a>. This is not a full list of all the improvements and changes in the latest release.

The 4.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are essential to maintaining high KDE quality, because developers cannot possibly test every configuration. User assistance is invaluable to help find bugs early so they can be squashed before the final release. Please consider joining the 4.12 team by installing the beta and <a href="https://bugs.kde.org/">reporting any bugs</a>. The <a href="http://kde.org/announcements/announce-4.12-beta2.php">official announcement</a> has information about how to install the betas.