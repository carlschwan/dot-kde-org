---
title: "KDE Ships March Updates to Plasma Workspaces, Applications and Platform"
date:    2013-03-05
authors:
  - "kallecarl"
slug:    kde-ships-march-updates-plasma-workspaces-applications-and-platform
---
KDE has <a href="http://www.kde.org/announcements/announce-4.10.1.php">released updates</a> for its Workspaces, Applications and Development Platform. They are the first in a series of monthly stabilization updates to the 4.10 series. 4.10.1 updates bring many bugfixes and translation updates on top of the 4.10 release and are recommended for everyone running the initial 4.10 release. As this release only contains bugfixes and translation updates, it will be safe and pleasant for everyone.

The release includes more than 100 recorded bugfixes, including improvements to the Kontact Personal Information Management suite, the KWin Window Manager, and others. Nepomuk developers remedied many important crash-causing problems; the overall user experience has improved considerably. Please see the <a href="http://www.kde.org/announcements/announce-4.10.1.php">release announcement</a> for links to the 4.10.1 Info Page, issue tracker, download and installation locations. The announcement also provides information about KDE and how to participate in this leading free and open software project.