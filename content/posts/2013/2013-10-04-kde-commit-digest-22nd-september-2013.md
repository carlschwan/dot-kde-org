---
title: "KDE Commit-Digest for 22nd September 2013"
date:    2013-10-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22nd-september-2013
---
In <a href="http://commit-digest.org/issues/2013-09-22/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://krita.org/">Krita</a> gains Clones Array extension: it creates a number of clones for the current layer, so the user can paint on it as if the tiles were repeated (useful for painting texture tiles for web-games); implements the Select Layer by Mouse Click feature and shows brush outline in line tool (and other tools)</li>
<li><a href="http://skrooge.org/">Skrooge</a> downloads and adds bills as properties by using <a href="http://weboob.org/applications/boobill">boobill (weboob)</a></li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> adds a migration agent that schedules various migrators and exposes an interface for status and control</li>
<li>Bug fixes in <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://www.calligra-suite.org/">Calligra</a> and many other programs.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-09-22/">Read the rest of the Digest here</a>.