---
title: "KDE Commit-Digest for 6th October 2013"
date:    2013-10-17
authors:
  - "mrybczyn"
slug:    kde-commit-digest-6th-october-2013
---
In <a href="http://commit-digest.org/issues/2013-10-06/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://userbase.kde.org/KWin">KWin</a>, screenedges KCM is adjusted to build against Qt5, uses QtQuick2 for the decoration preview list</li>
<li><a href="http://skrooge.org/">Skrooge</a> exports HTML and ODT from tables, gets better performance by avoiding refresh autocompletion on widgets after light transactions</li>
<li>In Calligra, <a href="http://krita.org/">Krita</a> improves performance for <a href="http://gmic.sourceforge.net/">gmic</a> filters</li>
<li><a href="http://sflphone.org/">Sflphone</a>-KDE makes TLS and SRTP features work again</li>
<li>Porting to KDE Frameworks 5 continues.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-10-06/">Read the rest of the Digest here</a>.