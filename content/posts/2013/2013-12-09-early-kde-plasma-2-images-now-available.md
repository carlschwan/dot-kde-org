---
title: "Early KDE Plasma 2 Images Now Available"
date:    2013-12-09
authors:
  - "jriddell"
slug:    early-kde-plasma-2-images-now-available
---
<a href="http://techbase.kde.org/Getting_Started/Using_Project_Neon_to_contribute_to_KDE">Project Neon</a>, the daily builds of KDE Frameworks 5 and KDE Plasma 2 for Kubuntu, has started releasing ISO images for testing. These are very early previews of the next generation of KDE Software. It is strongly recommended <strong>not be installed on a production machine</strong> but can be tested as live images or installed into a VirtualBox or other VM. Crashes and bugs are virtually guaranteed. The only supported upgrade path is to install a later ISO image. <a href="http://kshadeslayer.wordpress.com/2013/12/03/introducing-project-neon-5-isos/">More information on Rohan's blog</a>. Project Neon <a href="http://dot.kde.org/2012/07/24/introducing-project-neon-kvm">introduction on the Dot</a>.

<strong>Experimental. Not for use in production environments.</strong> 
