---
title: "KDE Commit-Digest for 17th February 2013"
date:    2013-04-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-17th-february-2013
---
In <a href="http://commit-digest.org/issues/2013-02-17/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> supports Opus codec with recent <a href="http://developer.kde.org/~wheeler/taglib.html">TagLib</a></li>
<li>Scripted effects and <a href="http://userbase.kde.org/KWin">KWin</a> scripts get a config interface; KWin improves multihead situation; Screenshot effect ported to XCB</li>
<li><a href="http://userbase.kde.org/Rekonq">Rekonq</a> restores Bookmarks menu (in 2.x fashion)</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> improves mobi and epub filter support</li>
<li>Bug fixes in Amarok, Network Management, Kdenlive.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-02-17/">Read the rest of the Digest here</a>.