---
title: "KDE Commit-Digest for 3rd March 2013"
date:    2013-04-21
authors:
  - "mrybczyn"
slug:    kde-commit-digest-3rd-march-2013
---
In <a href="http://commit-digest.org/issues/2013-03-03/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://kontact.kde.org/kmail/">KMail</a> gets an important bug fixed: if more than one mail is received in one batch, mail filtering ignored some mails</li>
<li><a href="http://community.kde.org/KTp">KDE Telepathy</a> improves name display from profiles and plugins, adds KDE global menu support</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> improves Google services support by updating to LibKGAPI2: full support of contacts groups, improved fetching contacts photos, status and progress reporting, should be faster and more stable</li>
<li><a href="http://konsole.kde.org/">Konsole</a> makes search strings per-tab, not per-window</li>
<li>Work on LDAP password storing in <a href="http://utils.kde.org/projects/kwalletmanager/">KWallet</a></li>
<li>Wacom tablet support sees many improvements for button action selection, including widget and dialog</li>
<li>KDevelop's <a href="http://www.ruby-lang.org/en/">Ruby</a> lexer is partially re-written</li>
<li><a href="http://edu.kde.org/ktouch/">KTouch</a> sees a rewrite of lesson text rendering that brings up support for non-Latin languages</li>
<li>Deletion from <a href="http://amarok.kde.org/">Amarok</a> playlist works much faster.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-03-03/">Read the rest of the Digest here</a>.