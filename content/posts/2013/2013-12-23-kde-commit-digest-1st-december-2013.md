---
title: "KDE Commit-Digest for 1st December 2013"
date:    2013-12-23
authors:
  - "mrybczyn"
slug:    kde-commit-digest-1st-december-2013
comments:
  - subject: "Amarok"
    date: 2013-12-24
    body: "I am really happy to see that Amarok will finally read lyrics from tags (as I spent considerable time saving the correct lyrics to all my files, mostly because I have many harder to find songs and mixes, that lyrics often aren't easily found or need to be edited). Now if Amarok had support for ladspa plugins like Audacious, it would nearly be the perfect audio player."
    author: "mrdr"
---
In <a href="http://commit-digest.org/issues/2013-12-01/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> adds advanced features for string formatting completion in <a href="http://www.python.org/">Python</a></li>
<li><a href="http://kate-editor.org/">Kate's</a> vim mode gains even more options</li>
<li><a href="http://amarok.kde.org/">Amarok</a> can read lyrics from tags</li>
<li><a href="http://userbase.kde.org/Telepathy">KDE Telepathy</a> supports <a href="http://cgit.freedesktop.org/telepathy/telepathy-haze">haze</a>/<a href="http://sipe.sourceforge.net/history/">sipe</a> protocol via a new plugin</li>
<li><a href="http://userbase.kde.org/Rekonq">Rekonq</a> adds global zoom setting</li>
<li><a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks</a> continues in klipper, system tray, <a href="http://edu.kde.org/kalgebra/">KAlgebra</a> and more.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-12-01/">Read the rest of the Digest here</a>.