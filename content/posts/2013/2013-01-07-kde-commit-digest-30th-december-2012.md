---
title: "KDE Commit-Digest for 30th December 2012"
date:    2013-01-07
authors:
  - "mrybczyn"
slug:    kde-commit-digest-30th-december-2012
---
In <a href="http://commit-digest.org/issues/2012-12-30/">this week's KDE Commit-Digest</a>:

<ul><li>Google Code-In work fixes various issues found by the EnglishBreakfastNetwork's code checker <a href="http://www.englishbreakfastnetwork.org/krazy/">Krazy</a></li>
<li><a href="http://simon-listens.blogspot.fr/">Simon</a> sees model reworking and restructuring</li>
<li>libkdcraw updates libraw and supports new cameras (<a href="http://www.digikam.org/">digiKam</a>)</li>
<li><a href="http://konsole.kde.org/">Konsole</a> adds an option to disable ctrl+<mouse-wheel> zooming</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, better integration of pencil tool into <a href="http://krita.org/">Krita</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-12-30/">Read the rest of the Digest here</a>.