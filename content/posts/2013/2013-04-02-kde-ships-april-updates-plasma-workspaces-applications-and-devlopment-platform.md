---
title: "KDE Ships April Updates to Plasma Workspaces, Applications and Devlopment Platform"
date:    2013-04-02
authors:
  - "kallecarl"
slug:    kde-ships-april-updates-plasma-workspaces-applications-and-devlopment-platform
comments:
  - subject: "please , make some good changes .."
    date: 2013-04-04
    body: "I and my friends need to see some good changes , it is better to replace now tradional desktop approach ! I feel comfortable in \r\nmetro , unity and gnome 3 running both in my desktop and small tablet , give unified  user experience what is missing in traditional KDE plasma desktop (default). We all know that traditional mate desktop is not the future.  "
    author: "Saurav Goswami"
  - subject: "Funny that you ask to replace"
    date: 2013-04-05
    body: "Funny that you ask to replace the traditional desktop. It's definitely a way KDE can go.\r\n\r\nThe reason I find this funny is, KDE was actually first to make that step. Plasma has all the technology on board to build custom desktops (in fact, you can do that now, notice how the tablet/phone/media center versions are built with the same technology). On the desktop however all hell broke loose, and people demanded a traditional desktop. Now that GNOME and Ubuntu took the step, this question pops up. :-)\r\n\r\nAs this is a bugfix/point release, such change won't be made. However, for KDE5 this is definitely worth exploring IMHO :)"
    author: "vdboor"
  - subject: "I believe the important thing"
    date: 2013-04-11
    body: "I believe the important thing about this is \"choice\". When KDE4 first release it was ready for nothing at all, because it was very unstable and incomplete. Changing the desktop philosophy at the same time without having a real choice was bad. Now I would love to try out and play with different desktop approaches at the same time. From time to time there is blog entry claiming e.g. : See, what Unity does, we can already do without any new code. And I believe this is close to the truth. First, it is complicated, second when it comes to the details it does really come as a polish full experience. What I would love to see is the possibility to switch between different (ready and polished) Desktop approaches with a few clicks. Not only the existing ones, but also a new KDE homegrown one. KDE appears generally ready for it. But the option to return to the present approach should remain."
    author: "mark"
---
KDE has <a href="http://www.kde.org/announcements/announce-4.10.2.php">released updates for its Workspaces, Applications and Development Platform</a>. These updates are the second in a series of monthly stabilization updates to the 4.10 series. 4.10.2 updates bring many bugfixes and translation updates on top of the 4.10 release and are recommended updates for everyone running the 4.10 series. It will be a safe and pleasant update for everyone.

The releases include over 100 recorded bugfixes, improving major applications. Changes are listed in KDE's issue tracker; Subversion and Git logs provide a detailed list of changes. Links to these and installation information are available in the <a href="http://www.kde.org/announcements/announce-4.10.2.php">release announcement</a>.