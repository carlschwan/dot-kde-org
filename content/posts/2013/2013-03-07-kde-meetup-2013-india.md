---
title: "KDE Meetup 2013 - India"
date:    2013-03-07
authors:
  - "kShah"
slug:    kde-meetup-2013-india
comments:
  - subject: "congratulations"
    date: 2013-03-07
    body: "congratulations for organizing such a large event successfully ....  \r\n"
    author: "brijesh"
  - subject: "Fantastic initiative!"
    date: 2013-03-07
    body: "Great work of all of you who have been involved!\r\n"
    author: "kevkrammer"
  - subject: "Wonderful Event"
    date: 2013-03-11
    body: "This was the first event I saw from the start till it was very successful. It made concepts clearer about FOSS culture & the KDE organisation."
    author: "yashness"
  - subject: "Congrat!"
    date: 2013-03-15
    body: "Congrat!"
    author: "mokhtar_one"
---
On the 23rd of February, a crowd of about 330 enthusiastic people gathered to be a part of the first major open source event in the State of Gujarat, India. Some people traveled hundreds of kilometers to attend, coming from places such as Delhi, Durgapur (more than 1800 km), Nainital, Bardoli and Mumbai. Students from <a href="http://www.daiict.ac.in/">Dhirubhai Ambani Institute of Information and Communication Technology (DA-IICT)</a> in Gandhinagar organized the event—<a href="http://www.gdgdaiict.com/kdemeetup/">KDE Meetup 2013</a>. It was an opportunity for passionate students to take their first steps towards becoming true software developers. The two day event was filled with talks on the latest KDE developments, sessions on how to start contributing, coding sessions, hands-on workshops, and a whole lot more, along with a big serving of the magic ingredient - fun!

<h2>Purpose</h2>
The primary motive of the event was to spread the word about KDE and free and open software as far as possible. Its aim was to include more and more students from India in KDE, one of the largest and coolest communities in the world, get them involved with open source development. "I feel really happy and privileged to have the opportunity to tell everyone about open source through such a big platform", said Yash Shah, the main organizer of the Meetup.

<div style="padding: 1ex; margin-left: auto; margin-right: auto;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEMeetupIndiaAuditorium.jpg" /><br />KDE Meetup 2013 - India</div>

<h2>Day 1</h2>
The first day started with a speech from Pradeepto Bhattacharya, a member of the Board of Directors of <a href=http://ev.kde.org>KDE e.V.</a>. He introduced the delegates to the whole new world of KDE, a world which looked complicated to most of his audience at first. His talk inspired people to make a fresh start with software development. 

The next session was conducted by Vishesh Handa about Qt. It was followed by a presentation on Version Control, mainly GitHub. The audience was so engrossed that the next slot on Plasma Workspaces had to be postponed. During the afternoon session, attendees were ready with their laptops, eager to start with free and open software development. People participated with an open exchange of knowledge and ideas. It was informal and friendly. Speakers and volunteers worked hard to address all of the zestful questions. The students were encouraged to explore and ask freely to gain clear understanding. They were rewarded with goodies—Kool KDE-India T-shirts—for their inquisitiveness, attention and energy.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEMeetupIndiaCake.jpg" /><br />Celebrating the KDE 4.10 Releases</div>
After the <em>official</em> end of Day 1, a KDE 4.10 Release Party got started. A delicious and quite pretty cake had Konqi and Katie perched on it. It was cut with the KDE song playing in the background.

<h2>Day 2</h2>
Day Two saw talks on Plasma Workspaces(desktop and tablet) by Shantanu Tushar Jha, Contributing to KDE EduSuite by Rishab Arora and Understanding Nepomuk - Semantic Data by Vishesh Handa. To address the importance of online communication in a large and widespread community like KDE, delegates were introduced to mailing lists and IRC. The students were also encouraged to participate in Season of KDE and Google Summer of Code. 

Parallel to this, a special session was conducted for some students who were nervous about speaking up in an unfamiliar group. They had been preparing to leave the Meetup the previous day. But they decided to stay after <a href="http://pradeepto.livejournal.com/18619.html">talking with Pradeepto</a> and realizing wisdom and compassion in the KDE Community.

The afternoon session was another hands-on opportunity for students to learn how to build and modify the code used in KDE applications and how to apply their own changes. They were guided right from the first step of cloning a repository through the entire process, ending with creating their own patches. The second day was beneficial to participants because after seeing a complete, overall picture, they could come up with polished, creative ideas of their own. Exactly what the event was intended to achieve.

One of the delegates in the workshop said, "It was a really lively and enjoyable event which appealed to the students and combined the core elements of learning and fun."
<div style="padding: 1ex; margin-left: auto; margin-right: auto; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDEMeetupIndiaGroup.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEMeetupIndiaGroupWee.jpg" /></a><br />Speakers and organizers (click for larger image)</div>

<h2>Outcome</h2>
The end result of the conference was satisfying and fulfilling by bringing together people with similar interests and familiarizing students with real-world programming.

The event lived up to its goals—helping people know about KDE, raise awareness about free and open software, and providing them with basic skills and techniques so that they can contribute to open source. KDE also inspired many students to take the initiative of writing blogs and posts.

Most of the delegates who attended the event were Windows users who installed Linux during the event, making the switch to open source. The speakers were confident and experienced, and keen on making the event insightful, interesting and simple for participants.  These were key ingredients which made this event so successful.

KDE Meetup 2013 was a grand accomplishment; the enormous participation was phenomenonal. Pradeepto said that it was an historical event for KDE. There is a newfound inspiration among the delegates, the desire to contribute, to be a part of the largest and friendliest (of course the coolest too) community in the world. All that the delegates had in mind was to learn and explore and innovate. To create something which could be the next revolution.