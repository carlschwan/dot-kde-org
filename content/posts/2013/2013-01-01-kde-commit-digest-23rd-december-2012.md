---
title: "KDE Commit-Digest for 23rd December 2012"
date:    2013-01-01
authors:
  - "mrybczyn"
slug:    kde-commit-digest-23rd-december-2012
comments:
  - subject: "bugs"
    date: 2013-01-04
    body: "Ha, always good to see about 100 more bugs closed than opened ;-)"
    author: "jospoortvliet"
---
In <a href="http://commit-digest.org/issues/2012-12-23/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.calligra-suite.org/">Calligra</a> increases modularity step by step; Words allows for ebook cover images to be added</li>
<li>Batch Queue Manager in <a href="http://www.digikam.org/">Digikam</a> offers more options to end users</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> FileIndexer gives plugins better control over indexing; indexing based on MIME types is now available</li>
<li><a href="http://ktorrent.org/">KTorrent</a> introduces a more user-friendly syndication interface</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> sees work on cookie management in private browsing mode</li>
<li><a href="http://userbase.kde.org/Apper">Apper</a> adds a feature to automatically download updates (without installing) and sees multiple other improvements</li>
<li>PublicTransport gets global QML dashboard.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-12-23/">Read the rest of the Digest here</a>.