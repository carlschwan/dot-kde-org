---
title: "KDE Ships First Beta of Applications and Platform 4.12"
date:    2013-11-08
authors:
  - "tsdgeos"
slug:    kde-ships-first-beta-applications-and-platform-412
---
KDE has released betas of the new versions of Applications and the KDE Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

This release does not include Plasma Workspaces, which was frozen for new features in its 4.11 series. The Development Platform has been virtually frozen for a number of releases in anticipation of KDE Frameworks 5. So this release is mainly about improving and polishing Applications.

A list of many improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.12_Feature_Plan">4.12 Feature Plan</a>. This list is not a full list of all the improvements in the latest release.

With the large number of changes, the 4.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. Real-life user assistance is invaluable to help find bugs early so they can be squashed before the final release. Please consider joining the 4.12 team by installing the beta and <a href="https://bugs.kde.org/">reporting any bugs</a>. 