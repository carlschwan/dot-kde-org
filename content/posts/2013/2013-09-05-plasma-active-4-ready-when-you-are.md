---
title: "Plasma Active 4 - ready when you are"
date:    2013-09-05
authors:
  - "kallecarl"
slug:    plasma-active-4-ready-when-you-are
comments:
  - subject: "KDE Plasma Workspaces"
    date: 2013-09-06
    body: "Here's a thought: Instead of trying to get into the mobile space with a Linux distro and the KDE Plasma Workspace, ever consider developing Plasma Workspace as a launcher app for Android? Just think of having the KDE Plasma Workspace on the Android platform, I'm sure KDE lovers everywhere that run Android devices would put this on their phones in a hurry. Right now, there are a couple of KDE themes, but the launchers they're on don't come close to the KDE experience. Just a thought... "
    author: "Ruel Smith"
  - subject: "Wha..?"
    date: 2013-09-09
    body: "That is like telling them to cease development of KDE as a whole and just make a KDE-like theme for Windows 8."
    author: "Adam Armstrong"
  - subject: "Nice idea!"
    date: 2013-09-10
    body: "Nice idea!\r\n\r\nBrilliant idea. This would make it very easy to experiment with Plasma Active.\r\nI'm not sure about the technical feasibility of this idea, however, user-wise and marketing-wise it seems like a win-win to me."
    author: "vdboor"
  - subject: "???"
    date: 2013-09-19
    body: "We want a truly free alternative to Android, not a [deleted] launcher app!"
    author: "Chris1234"
  - subject: "Promising..."
    date: 2013-09-28
    body: "I'm just waiting for an up-to-date tablet shipping with Plasma Active! (Or a convenient and secure way to install it on my Asus TF300T)"
    author: "Tomdee4"
  - subject: "how to install"
    date: 2013-10-04
    body: "I would like to test this out. I have a touch enabled device (Sony Vaio Duo 11). Currently, I am running Kubuntu 13.04. Does any one know how I can install this along side by current Kubuntu. Something like a PPA would be a boon. Keep up the good work. "
    author: "kamesh"
---
The KDE Community announces the release of Plasma Active 4 (PA4). Plasma Active is a user experience technology stack for consumer electronics. While the default user interface is for tablets, it can be customized to work on smartphones, settop boxes, smart TVs, and touch computing devices such as home automation and in-vehicle infotainment. There are major new improvements to the Files application, an overhaul of the on-screen keyboard and a completely free and open source system based on the <a href="http://merproject.org/">Mer Core</a>. The Plasma Active team invites involvement from people who want to participate in the widespread movement towards mobile computing on open platforms.

<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/pa-logo.png" /></div>

Plasma Active 4 is a stabilization and performance release, the result of developers focusing on fit and finish throughout. This release is intended to complete the evolution of Plasma Active to a polished product relative to its proof of concept first release. The emphasis is now on providing a solid foundation for third party applications, additional content and device adaptations. Plasma Active 4 is well-suited to customization for specific needs, use cases and markets. This release sets the stage for adopting newer technologies such as Qt5 and Wayland in future releases.

<h3>What's so special about Plasma Active?</h3>
Thomas Pfeiffer, Plasma Active User eXperience designer writes:
<blockquote>
"With every iteration of Plasma Active, we're taking another step on the road from the notion of a mobile device as a "bucket of apps" (which dominates all current mobile operating systems) back towards the original notion of mobile devices as "personal digital assistants". On a Plasma Active device, the user's data and tasks are front and center. Everything is an integrated part of the whole system rather than many small disconnected apps that do not talk to each other. This sort of integration is unique to Free Software, because Free Software applications work together for the best user experience possible, instead of individual apps vying for user attention and intended to capture maximum market share.</blockquote>
<blockquote>
"The combination of KDE's semantic technology and user-defined Activities connects elements of data and tasks, projects, events or other kinds of context. No other mobile device gives users these capabilities. Plasma Active applications are versatile tools to complete tasks on the go."</blockquote>

<h3>What Plasma Active 4 offers</h3>
<ul>
<li>Groupware (email and calendar)</li>
<li>Integrated, touch friendly media player and viewer</li>
<li>Reading eBooks and PDFs</li>
<li>Viewing office suite documents</li>
<li>Local file storage</li>
<li>Direct access to files independent of file location</li>
<li>Synchronization with user-controlled remote storage</li>
<li>Tagging and other user-defined metadata</li>
<li>Semantic information management</li>
</ul>

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/PA4snapshot500White.png" /></div>

<h3>New to Plasma Active 4</h3>
<ul>
<li>Overall faster and smoother</li>
<li>A faster and more usable web browser</li>
<li>Improved usability and more functions in the on-screen keyboard</li>
<li>Many improvements to the Filemanager application; it's now faster and scales to large amounts of data. Better tagging and other user-defined metadata, content type browsing and launch. More elegant look and feel.</li>
<li><a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma Media Center</a> is a new all-in-one multimedia experience for music and videos stored on the device as well as those from Internet services.</li>
<li>Synchronization with the <a href="http://owncloud.org/">ownCloud</a> personal cloud storage</li>
<li>User interface improvements to the AddOns app, and a web service for content creators available soon</li>
<li>Device "developer mode" for easier application development</li>
<li>Community support for new device adaptations</li>
<li>Mer Open Build Service: with build.merproject.org OBS custom Plasma Active applications are easy to build. Mer provides faster startup and newer OS components such as kernel and systemd.</li>
</ul>

<h3>Get involved</h3>
There are many ways to get involved with PA4:
<ul>
<li>Install an image on a device or in a virtual environment</li>
<li>Help others get up and running</li>
<li>Test and report bugs</li>
<li>Share questions and assistance in the <a href="http://forum.kde.org/viewforum.php?f=211">Plasma Active forum</a></li>
<li>Join the discussions on IRC or the mailing list</li>
<li>Set up a build environment and start hacking</li>
</ul>

Getting started with hacking on Plasma Active is easier than ever. PA4 is based on <a href="http://qt-project.org/">Qt 4.8</a> and the KDE platform 4.10 releases, so everything specific to Plasma Active can be built on top of the development packages of KDE Platform 4.10 releases from most Linux distributions. It is possible to have the Plasma Active Workspace and Active applications running on a machine in matter of minutes.

Packaging is easy as well. Start by installing the <a href="https://wiki.merproject.org/wiki/Platform_SDK">Mer SDK</a>. It is completely self-contained, and can be dropped on top of any distribution. It doesn’t really “touch” the underlying system.

Please help port Plasma Active to new devices and get more applications written for touch devices. The <a href="http://plasma-active.org/">Plasma Active website</a> has more information and links to resources.

<h3>Thanks</h3>
Plasma Active and its applications are built collaboratively on the following key technologies:
<a href="http://merproject.org/">Mer Core</a>
<a href="https://wiki.maliit.org/Main_Page">Maliit virtual keyboard</a>
<a href="http://owncloud.org/">ownCloud personal cloud storage</a>
<a href="http://www.calligra-suite.org/">Calligra Office Suite</a>
<a href="http://okular.kde.org/">Okular document viewer</a>
<a href="http://userbase.kde.org/Kontact">Kontact PIM suite</a>
<a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma Media Center</a> 

Many thanks to the Plasma Active team for building this exceptional software environment to serve users. It is not possible to single out particular people. The team knows who the heroes are. Thank you.