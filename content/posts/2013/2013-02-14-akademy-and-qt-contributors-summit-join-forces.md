---
title: "Akademy and Qt Contributors Summit Join Forces"
date:    2013-02-14
authors:
  - "kallecarl"
slug:    akademy-and-qt-contributors-summit-join-forces
---
In July 2013, Akademy — the KDE community summit — will host the Qt Contributors Summit (QtCS) in Bilbao, Spain. QtCS is <strong>THE</strong> gathering of the Qt Project contributor community. It will take place July 15th and 16th in the middle of the KDE Akademy week (13-19 July). By co-hosting, KDE and the Qt Project will increase their existing collaboration even further. Holding their annual conferences at the same time and the same place will foster interaction, knowledge transfer and technical progress.
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>

More than 500 upstream and downstream developers will participate in this combined event. This includes key maintainers and top contributors of both projects. The conference will cover many areas of computing, ranging from core library functionality in Qt and KDE to popular user applications used by hundreds of millions of users, integrated with cloud services. All this takes place in a highly productive atmosphere where we combine forces, exchange knowledge, and meet new contributors. 

A combined conference makes sense. There are many strong personal ties and working relationships among KDE and Qt contributors. Meeting face-to-face will be productive for both projects."As part of both the Qt and KDE communities, I've seen how the two have benefited from each other. In the last year and a half, the pace picked up when many KDE developers started working on Qt and certain features inspired by KDE were proposed and accepted into Qt 5. Akademy and the Qt Contributors Summit co-hosting this year means the two communities will have a much bigger opportunity for cross-pollination of ideas." Thiago Macieira, Qt Core Maintainer, Software Architect at Open Source Technology Center, Intel Corporation.

<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/QtCSHacking.jpg" /><br /><small>Qt Hacking - photo by Alexandra Leisse</small></div>

In addition, there is exceptional technical value. KDE depends on Qt and is a major contributor to the Qt Project. With <a href="http://vizzzion.org/blog/2013/01/the-road-to-kde-frameworks-5-and-plasma-2/">Frameworks 5</a>, KDE is merging a substantial amount of code into Qt. These contributions are available and potentially useful to <strong>all</strong> Qt developers. Aaron Seigo, long time KDE developer, said, "Qt and KDE have much in common: a passionate pursuit of excellence, delivery of the best cross platform technology available today, and major library advancements with Qt 5 and Frameworks 5. This event is the natural venue for those interested in Qt, KDE or both to come together in pursuit of collaboration and great development."

QtCS will take place in parallel with the Akademy <a href="http://en.wikipedia.org/wiki/Birds_of_a_feather_(computing)">BoF sessions</a> (done in an unconference-style). This arrangement allows attendees of both events to choose sessions that are most interesting to them. QtCS 2013 is an <a href="http://en.wikipedia.org/wiki/Unconference">unconference</a> where contributors organize their own Birds of a Feather sessions (BoFs). We are expecting more than 40 Qt BoFs and more than 200 Qt developers in addition to those attending Akademy. The Qt Contributors Summit is on Monday and Tuesday (15-16 July). 

Both the Akademy BoFs/workshops and the Qt Contributors Summit sessions will take place at the <a href="http://www.ingeniaritza-bilbao.ehu.es/p224-homeen/en/">Engineering School</a> of Euskal Herriko Unibertsitatea <a href="http://www.ehu.es/p200-hmencont/en/contenidos/informacion/basic_facts/en_inf/basicfacts.html">(EHU; The University of the Basque Country</a>) in Bilbao. 

The Akademy <a href="http://dot.kde.org/2013/02/07/akademy-2013-call-presentations-and-registration">Call for Participation and registration</a> as well as information about Akademy <a href="http://akademy2013.kde.org/sponsoring">sponsorship opportunities</a> have been published. Information about the Qt Contributors Summit is available at the <a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">Qt Contributors Summit wiki</a>.

<h2>About Akademy</h2>
For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. The annual Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. 

Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and map directions for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or those who are considering using it. During Akademy, a few hundred Free Software enthusiasts will gather for 2 days of talks and 5 days of workshops and coding sessions.

For more information, please contact <a href="http://akademy2013.kde.org/contact">The Akademy Team</a>.

<h2>About the Qt Contributors Summit</h2>

Qt Contributors Summit (QtCS) is being held for the third time this year after the successful launch of Qt Open Governance in 2011.  It is a hands-on gathering to get things done and plan ahead, and is a free of charge, invitation-only event. This year, the KDE Community has invited QtCS to be co-located with Akademy, influencing, improving and creating Qt excellence for the future.

Your attendance at the QtCS begins with an invitation from a Qt contributor. Sessions can be registered for QtCS 2013 on the <a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">Qt Project Web wiki</a>. You can contact us on the email-list: marketing (at) qt-project.org.