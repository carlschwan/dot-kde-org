---
title: "Calligra 2.6: Features, Usability, Bugfixes"
date:    2013-02-06
authors:
  - "ingwa"
slug:    calligra-26-features-usability-bugfixes
---
The Calligra team has announced the <a href="http://www.calligra.org/news/calligra-2-6-released/">release of version 2.6</a> of the Calligra Suite, Calligra Active and the Calligra Office Engine. This version provides new features, substantial usability polishing, and bugfixes. Calligra is seeing a lot of development, so many new features are mentioned in the announcement, and the applications are becoming more and more mature. This is the second major update after the initial release of Calligra in April last year.

<h2>New or improved features in Calligra 2.6</h2>
<strong>Calligra Author</strong> is the latest addition to the Calligra application family. It is intended to support the process of creating an eBook from concept to publication. Several new features were developed specifically for Calligra Author, including export options and appropriate text statistics.

The productivity part of the Calligra suite sees improvements to text layout and many minor fixes in dialogs and tools. In addition, there are new features and improvements to the various Calligra elements: <strong>Sheets</strong>, <strong>Stage</strong>, <strong>Flow</strong>, <strong>Plan</strong> and <strong>Kexi</strong>.

The artistic applications in Calligra are the most mature of the Suite and are already category leaders. In particular, <strong>Krita</strong> continues to see speed improvements, increasing support for industry standards and adoption by industry professionals.

<strong>Calligra Active</strong> for tablets and smartphones has seen several improvements. It also benefits from all the improvements in the common parts of Calligra: the libraries and plugins.

<h2>About Calligra</h2>
Calligra, which includes the Calligra Suite and Calligra Active, is part of the <a href="http://kde.org/applications/">Applications from the KDE Community</a>. Please visit <a href="http://www.calligra.org/">the Calligra website</a> for more information including download locations.

 