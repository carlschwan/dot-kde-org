---
title: "KDE Commit-Digest for 21st April 2013"
date:    2013-04-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-21st-april-2013
---
In <a href="http://commit-digest.org/issues/2013-04-21/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> now disables pre-amp slider if it isn't available</li>
<li>Brightness changes are always detected and streamed when hardware changes</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> on Android (<a href="http://blogs.kde.org/2013/03/24/coffice-calligra-android-available-now">Coffice</a>) adds support for Microsoft Word DOC and DOT file formats</li>
<li><a href="http://skrooge.org/">Skrooge</a> supports Bitcoin with automatic rate download</li>
<li>New <a href="http://extragear.kde.org/apps/kipi/">Kipi</a> plugin allows to export to jAlbum</li>
<li><a href="http://community.kde.org/Plasma">Plasma</a> Package supports desktop switcher layouts</li>
<li>New: <a href="http://docs.kde.org/stable/en/kdesdk/kapptemplate/kapptemplate.pdf">Qt5 + QML2 basic template (pdf) for those wanting to start exploring Qt5 programming with QtQuick2</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> adds KML Editor Dock Widget.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-04-21/">Read the rest of the Digest here</a>.