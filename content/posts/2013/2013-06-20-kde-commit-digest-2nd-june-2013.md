---
title: "KDE Commit-Digest for 2nd June 2013"
date:    2013-06-20
authors:
  - "mrybczyn"
slug:    kde-commit-digest-2nd-june-2013
---
In <a href="http://commit-digest.org/issues/2013-06-02/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/Plasma/Kickoff">Kickoff</a> shows new applications for 3 days in a separate submenu "Recently Installed"</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> adds a helper effect for KScreen integration</li>
<li><a href="http://skrooge.org/">Skrooge</a> allows changing operation date through "Search & Process"</li>
<li>In <a href="http://amarok.kde.org/">Amarok</a>, TrackLoader and PlaylistController support for treating remote playlists as streams, OpenGL analyzers appear, analyzers are configurable now</li>
<li>Network Management allows configuring band/channel for ad hoc wifi mode</li>
<li>KDE Telepathy supports persistent chats in the plasmoid</li>
<li>Bug fixes in <a href="http://uml.sourceforge.net/">Umbrello</a>, <a href="http://dolphin.kde.org/">Dolphin</a>, <a href="http://kontact.kde.org/kmail/">KMail</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-06-02/">Read the rest of the Digest here</a>.