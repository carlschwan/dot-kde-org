---
title: "KDE Commit-Digest for 29th September 2013"
date:    2013-10-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-29th-september-2013
---
In <a href="http://commit-digest.org/issues/2013-09-29/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> adds a new tool: RegistersView</li>
<li>In <a href="http://pim.kde.org/">KDE-PIM</a>, KMail gains adblock support</li>
<li><a href="http://krita.org/">Krita</a> adds an option to hide preset strip and/or scratchpad; improves memory efficiency and conversions</li>
<li>Porting to KDE Frameworks and Plasma 2 still in progress, this includes the icon applet; porting to Qt 5 is also in progress for the KCM modules</li>
<li>Work is in progress on systemtray 2.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-09-29/">Read the rest of the Digest here</a>.