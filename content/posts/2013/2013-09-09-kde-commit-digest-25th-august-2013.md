---
title: "KDE Commit-Digest for 25th August 2013"
date:    2013-09-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-25th-august-2013
---
In <a href="http://commit-digest.org/issues/2013-08-25/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/Kleopatra">Kleopatra</a> implements session restore</li>
<li><a href="http://uml.sourceforge.net/">Umbrello</a> adds visual feedback if widget has documentation</li>
<li>In KDE Frameworks 5, <a href="http://konsole.kde.org/">Konsole</a> is ported to KF5/Qt5</li>
<li>GSoC work in <a href="http://www.digikam.org/">Digikam</a>, <a href="http://nepomuk.kde.org/">Nepomuk</a>, and <a href="http://userbase.kde.org/Khipu">Khipu</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-08-25/">Read the rest of the Digest here</a>.