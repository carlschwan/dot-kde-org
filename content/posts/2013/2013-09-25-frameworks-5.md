---
title: "Frameworks 5"
date:    2013-09-25
authors:
  - "smartboyhw"
slug:    frameworks-5
comments:
  - subject: "why isn't Sonnet a 'functional framework' ?"
    date: 2013-09-25
    body: "Is the separation between functional or integrative just on whether they have plugins loaded at runtime or not? \r\n\r\nDeploying Sonnet is trivial. "
    author: "Ian Monroe"
  - subject: "logind and ConsoleKit"
    date: 2013-09-25
    body: "How does KDE frameworks 5 deal with logind, ConsoleKit, etc?"
    author: "Olav Vitters"
  - subject: "my guess would be that a"
    date: 2013-09-25
    body: "my guess would be that a functional framework needs to work on all platforms (due to no external dependecies), while an integrational framework might not"
    author: "kevkrammer"
  - subject: "I am not sure there is a"
    date: 2013-09-25
    body: "I am not sure there is a library respectively a framework interacting with those.\r\nProbably done by KDM and/or the Plasma workspace shell"
    author: "kevkrammer"
  - subject: "integration frameworks"
    date: 2013-09-25
    body: "Integration frameworks provide integration with services on the system. Solid integrates with hardware awareness features on the native OS, while sonnet provides access to the system's spell checking services.\r\n\r\nThe functional frameworks provide a set of functionality in and of themselves that does not change or necessarily rely on operating system / platform service infrastructure. They may, of course, use external libraries (beyond Qt) but these would not be platform services."
    author: "aseigo"
  - subject: "session management / user access"
    date: 2013-09-25
    body: "A lot these features are really only relevant to the workspace, so integration exists in Plasma. Frameworks is not concerned with things like managing sessions, for instance.\r\n\r\nThere are Frameworks, such as KAuthorized which allows applications to (transparently to them) tie into system services such as PolicyKit.\r\n\r\nIt really comes down to: do applications need to worry about this feature set, or is it really a workspace (e.g. desktop shell) issue? The latter all fall into the category of Plasma and are entirely unrelated to Frameworks. Frameworks and Plasma Workspaces are separate things."
    author: "aseigo"
  - subject: "QML"
    date: 2013-09-26
    body: "Is QML interpreted each time on run, or only once on build time? I'm quite not happy with kde 4 then talking about laptop up time from battery. As I correct understand for Qt5 interfaces will only on QML. It should have performance, memory and battery up time penalty. "
    author: "Maksim"
  - subject: "ThreadWeaver"
    date: 2013-09-26
    body: "\"To give those interested a sense of the expected level of quality, documentation and testing, two Frameworks are planned to be released as 'final' with the December preview release: KArchive and KThreadWeaver\"\r\n\r\nI'd say it's ThreadWeaver, not KThreadWeaver.\r\n\r\nGood article!! :-)\r\n\r\nCheers!"
    author: "David Gil"
  - subject: "Other Qt desktops"
    date: 2013-09-26
    body: "With the merge of Razor and LXDE-Qt, it would be great to get them also on board of this project. IMHO, a framework created and maintained by all Qt desktop projects would be a huge gain for the Qt community.\r\n\r\nAre there any discussions about this already?\r\n"
    author: "cloose"
  - subject: "Quite the opposite.., qt5 and qml = performance improvements"
    date: 2013-09-26
    body: "While it is true that interfaces are interpreted at runtime, this mostly only applies to plasma and the workspace. Additionally, the migration to qml not only makes designing interfaces that behave and look as we intend, it does also bring a lot of performance improvements with this. \r\n\r\nTraditionally, with c++, the prior methods of rendering interfaces, even in plasma, were not properly hardware accelerated. That meant burning cpu time to render updates when the gpu can do it far far faster and go back to power saving states. Qt5 brings a lot of improvements in this area, due to implementing items on a scene graph(especially with qt5.2),reduces texture switches, over draw, and reduces draw calls made. \r\n\r\nAlso with QML, it is far easier and more practical to load only what we need when we need it. This simply wasn't practical in c++'s methods of forming interfaces. \r\n\r\nSo basically all of these changes are significant improvements in performance, resource usage, and interface formation. So really your worries are pretty much non existent, I would say. Not only that, but we've really come a long way, able to squash bugs just so much faster, and reduce the maintenance burden of previous c++ code, when it comes to user interfaces in the worskpace(mostly plasma, but also parts of kwin) "
    author: "Shaun Reich"
  - subject: "At load time"
    date: 2013-09-27
    body: "QML is currently parsed at load time but there are plans to enable it at build time as well.\r\nQt5 interfaces can use QtWidgets, QtQuick and any other UI technology they would like to use. Their developers can code them in C++, use something that generates C++, QML, or any other approach their chosen UI technology supports"
    author: "kevkrammer"
  - subject: "Yes"
    date: 2013-09-27
    body: "The folks from Razor/LXDE are following the development of frameworks quite closely."
    author: "kevkrammer"
  - subject: "Thanks for the article"
    date: 2013-09-28
    body: "Thanks for the article, I really enjoyed reading it, especially the \"Introducing Frameworks 5\" and \"History of KDE Libraries\" part."
    author: "Richard__"
  - subject: "Broken link"
    date: 2013-12-11
    body: "In the last section (\"Current state and code\"), the link\r\n\r\nhttps://projects.kde.org/projects/kde/kdelibs/repository/show?rev=Frameworks\r\n\r\ngives me a 404 error."
    author: "A."
  - subject: "link fixed"
    date: 2013-12-11
    body: "Thank you. The link is fixed."
    author: "kallecarl"
  - subject: "\"Places\" in file dialogs"
    date: 2015-03-11
    body: "\"Places\" in file dialogs seems broken now, all my favorites in there are gone and there is no more button to add them. Will this be fixed? Thanks!"
    author: "Zeemvel"
---
A <a href="http://dot.kde.org/2013/09/04/kde-release-structure-evolves">recent Dot article</a> explained changes in the KDE release cycle that will be happening with the upcoming introduction of Frameworks 5. The changes to KDE's core libraries are enough to warrant a name change from 'Platform' to 'Frameworks'. This article provides some background, an overview of the changes, and the benefits and improvements that can be expected from KDE Frameworks 5 for the entire Qt community. A later article will address Frameworks 5 benefits for KDE developers.

The KDE libraries are currently the common code base for (almost) all KDE applications. They provide high-level functionality like toolbars and menus, spell checking and file access. Currently, 'kdelibs' is distributed as a single set of interconnected libraries. Under the KDE Frameworks efforts, these libraries are being methodically reworked into a set of independent, cross platform modules that will be readily available to all Qt developers.

The KDE Frameworks, designed as drop-in Qt Addons, will enrich Qt as a development environment with functions that simplify, accelerate and reduce the cost of Qt development. For example, KArchive (one of the first Frameworks available) offers support for many popular compression codecs in a self-contained and easy-to-use file archiving library. Just feed it files; there's no need to reinvent an archiving function.

The transition from Platform to Frameworks has been in progress for more than 2 years and is guided by top KDE technical contributors. Previews of the first Frameworks are expected to be released in December 2013.

<h2>Introducing Frameworks 5</h2>
Frameworks 5 consists of functional components and are structured in categories and tiers, based on their dependencies.
<br />
<strong>Categories</strong>
The Frameworks can be divided into three categories:
<ul><li><strong>Functional</strong> elements have no runtime dependencies. For example, KArchive (mentioned above) handles compression and decompression for many archive formats transparently and can be used as a drop-in library.</li>
<li><strong>Integration</strong> designates code that requires runtime dependencies for integration depending on what the OS or platform offers. For example, Solid supplies information on available hardware features and may require runtime components to deliver some of the data on some platforms.</li>
<li><strong>Solutions</strong> have mandatory runtime dependencies. For example, KIO (KDE Input/Output) offers a network-transparent virtual filesystem that lets users browse and edit files as if they were local, no matter where they are physically stored. And KIO requires kioslave daemons to function.</li>
</ul>
<div style="width: 360px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kf5_no_tier4_big.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kf5_no_tier4.png" /></a><br />Frameworks 5 separates the KDE libraries into modules with clear dependencies.</div> 

<strong>Tiers</strong>
Frameworks are further divided into tiers, which relate to link-time dependencies. Tier 1 Frameworks can be used independently, while Tier 3 Frameworks can depend on other Tier 3 Frameworks and tiers below them. The diagram shows dependency chains.

The Frameworks are also separated by respecting core/gui distinctions and the different GUI technologies. So it's not uncommon to find a core, a gui and a widget module relating to a given Framework (e.g KConfigCore vs KConfigGui). This way third parties can use only the parts they need and avoid pulling unwanted dependencies on QtGui.

<h2>Show me the code</h2>
To give an idea of what is available in Frameworks 5, a cross-section of Frameworks from different tiers is presented below. These are in various stages of completion and many more are under development.

<strong>Functional Frameworks</strong>
These Frameworks are independent. They have no runtime dependencies, but offer drop-in functionality.
<ul>
<li><strong>KArchive</strong> is a Framework that deals with popular compression technologies such as ZIP, 7ZIP and TAR archives, and offers a compression QIODevice for gzip, bzip2 and xz. It will extract whatever file it's given.</li>
<li><strong>KPlotting</strong> is a simple plotting Framework that offers anti-aliasing, stacking and updating. It takes care of transformation from the data's natural units to screen pixel coordinates and adds axis labels with tickmarks and tick labels.</li>
<li><strong>Threadweaver</strong> makes writing multi-threaded code easier by handling background jobs. Unlike QThreadPool, it features job dependencies and has done signals per job, as well as a global jobsDone signal. It allows threads to be suspended and aborted easily and depends only on QtCore.</li>
<li><strong>KConfig</strong> is a Framework to deal with storing and retrieving configuration settings. It features a group-oriented API. It works with INI files and XDG-compliant cascading directories. It generates code based on XML files.</li>
<li><strong>Extra itemmodels</strong> include:</li>
    <ul>
        <li> recursive filtering model for tree views</li>
        <li>checkable proxy model for trees and lists</li>
        <li>proxy model for restructuring a tree into a list</li>
    </ul>
<li><strong>KCoreAddons</strong> include:</li>
    <ul><li>KJob: a base class for async APIs</li>
        <li>file handling classes: directory watching, backup handling, auto save files</li>
        <li>on-disk sharing of cached data between applications</li>
        <li>text handling classes: split strings at word boundaries and add ellipsis</li>
    </ul>
</ul>

<strong>Integration Frameworks</strong>
These have platform-dependent runtime dependencies for system integration.
<ul>
<li><strong>Sonnet</strong> has a speller and background checker, as well as widgets and spellcheck and configuration dialogs. It is plugin-based and can work with aspell, hspell, hunspell and enchant.</li>
<li><strong>Solid</strong> offers hardware detection and can inform an application about storage devices and volumes, CPU, battery status, power management, network status and interfaces, and Bluetooth. For encrypted partitions, power and networking, running daemons are required.</li></ul>

<strong>Solution Frameworks</strong>
These require their own runtime daemons to function properly.
<ul><li><strong>KIO</strong> lets the user browse and edit files the same way, locally and remotely. It features asynchronous jobs in separate processes and supports a large number of protocols including (s)ftp, samba, webdav and nfs, many compressed file formats, thumbnails, audio-cd music extraction, trash and fish (a file management view over an ssh connection).</li>
<li><strong>KService</strong> is the framework that provides advanced features for plugins, including locating them on disk based on queries. It's useful for component-based architectures where it pulls mandatory runtime dependencies. It also provides capabilities for finding the applications associated with a particular type of file, such as identifying which application is the user's preferred PDF viewer.</li></ul>

<h2>History of KDE Libraries</h2>
When KDE began more than 15 years ago, development was application-driven. Libraries were an afterthought, intended to share work, making development easier and faster. Libraries were added based on simple rules. For example, if a particular functionality was used in more than one place, it was put into a shared library. Sometimes the functionality was an addition to existing Qt classes, other times new libraries were developed for functions such as hardware discovery (Solid) or file access (KIO). The libraries had the additional benefit of improving overall functionality, consistency and stability. These overall improvements provided substantial value, so developers were encouraged to create and use more common library components.

<strong>Issues</strong>
The libraries have grown organically over the years, and for KDE application developers, they were and continue to be useful. However, they are not very fit for use outside of traditional Linux desktop applications. The KDE libraries were designed to be used all at once in one complete bundle (known as "kdelibs"). There were many inter-dependencies between functions. Moreover, launching an application built on the KDE Platform brought in many runtime elements. 

For these reasons, it is hard to re-use KDE Platform 4 components outside of KDE applications. With the emergence of mobile platforms, this is an important consideration. On a modern desktop computer, the footprint of the KDE libraries and the runtime components is not very significant, but things are different on mobile or embedded devices. Developers have become more and more interested in using only the useful bits and pieces in their code. They do not want to pull in a lot of other unneeded components that generate their own dependencies, possibly hard to fulfill on a target platform. Splitting up kdelibs into independent modules would greatly improve their standalone utility.

<strong>Qt and Frameworks</strong>
A group of core developers produced a plan for achieving a greater degree of modularity. From the beginning, KDE has relied on Qt as an upstream resource. So KDE libraries contain many pieces that would be useful in Qt development, if modular capabilities from kdelibs were available. Making these functions available adds value to Qt as a premier development platform. This was a major driver of the decision to make KDE library functions more modular. The introduction of Qt's Open Governance model in late 2011 offered the opportunity for KDE developers to get more closely involved with Qt, KDE's most important upstream resource. And this has produced benefits that are available to every Qt user. Among the new features added by KDE developers are improvements to Qt's platform adaptations, a command line parser, file-type determination, file locking, and a series of X11-related features.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 300px;"><a href="/sites/dot.kde.org/files/GroupPic2011.jpg"><img src="/sites/dot.kde.org/files/GroupPic2011300.jpg#overlay-context=2013/09/23/frameworks-5" ></a><br /><center><small>Randa 2011 - click for larger</small></center></div>
<strong>Frameworks 5</strong>
These contributions to Qt form the basis for further modularization of the KDE libraries. The libraries are moving from being a singular 'platform' to a set of 'Frameworks'. This is the fifth such major KDE architectural transition, and the reasoning behind the name "KDE Frameworks 5". This shift also means that KDE technology will be no longer tied to a single platform, desktop or community. Instead it is a comprehensive set of technologies that becomes available to the whole Qt ecosystem. KDE libraries will become easier to use, and will enrich Qt as a development environment.

Frameworks 5 was started in the spring of 2011 at an architectural strategy gathering in <a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">Randa, Switzerland</a>. Developers from various KDE areas and projects (including several developers from the GNOME project) spent a week in discussion considering the impacts of the Qt 5 release. KDELIBS classes were pulled apart into pieces destined for upstreaming to Qt, deprecating or refactoring. The result of these efforts was a roadmap to Frameworks 5. Sebastian Kügler's <a href="http://vizzzion.org/blog/2013/01/the-road-to-kde-Frameworks-5-and-plasma-2/">blog post</a> provides further insights about the work involved with Frameworks 5.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ervin_wee.png" /><br />Kévin Ottens</div>

<h2>Collaboration and Roadmap</h2>
KDE Frameworks 5 is currently under development by a team of about 20 developers led by Kévin Ottens and David Faure. About half are supported by the four companies that are actively involved in the development of Frameworks 5.

Work on KDE Frameworks 5 is coordinated at the weekly Tuesday 4 p.m. (CEST; UTC+2) meetings on #kde-devel (freenode IRC). Reports from the meetings are sent to the <a href="https://mail.kde.org/mailman/listinfo/kde-Frameworks-devel">Frameworks-devel mailing list</a>. The meetings consist of announcements at the beginning, after which participants share what they worked on during the previous week and what they plan to tackle in the upcoming week. Work is planned and documented on the wiki. In addition to IRC, coordination takes place over the kde-Frameworks-devel mailinglist. The mailing list is also a place to inquire about particular KDE functionality that may be available for Qt development.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DavidFaure_explaining.png" /><br />David Faure  <small><small> - photo by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></small></div>

The project of transitioning to KDE Frameworks 5 is essentially a matter of refactoring a large, mature code base, and work has been in progress for more than two years. A preview release is planned for December 2013, with a final release in the first half of 2014. To give those interested a sense of the expected level of quality, documentation and testing, two Frameworks are planned to be released as 'final' with the December preview release: KArchive and KThreadWeaver. (For developers using Qt 4, KArchive is available as a download from <a href="http://www.davidfaure.fr/kde/karchive-qt4-2.0.0.zip">David Faure</a>. It works on Unix-like systems and reportedly on Windows too.)

<h2>Current state and code</h2>
The current state of Frameworks 5 development is available <a href="https://projects.kde.org/projects/kde/kdelibs/repository/show?rev=frameworks">in the KDE Frameworks repository</a>. The <a href="http://community.kde.org/Frameworks/Overview">KDE Community wiki has an overview </a> of the functionality offered by the Frameworks. The wiki also has <a href="http://community.kde.org/Frameworks">a Frameworks page</a>, which includes a <a href="http://community.kde.org/Frameworks/Epics">dashboard</a> with the current status of Frameworks 5.0.

<a href="http://inqlude.org/">Inqlude.org</a> is another resource for Qt developers. Inqlude offers <a href="http://inqlude.org/edge.html">a view from the Frameworks perspective</a>. The goal of this site is to provide a comprehensive cross reference of KDE Frameworks for Qt developers who are looking for ready-made functionality for their applications. The site, currently in Alpha, is scheduled for an overhaul during October. Additional contributors are warmly welcomed.

Article by Howard Chan and Jos Poortvliet, input from core frameworks developers including David Faure, Kévin Ottens and Sebastian Kügler.