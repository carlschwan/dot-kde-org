---
title: "KDE Commit-Digest for 26th May 2013"
date:    2013-06-16
authors:
  - "mrybczyn"
slug:    kde-commit-digest-26th-may-2013
---
In <a href="http://commit-digest.org/issues/2013-05-26/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> harmonizes playlist-related actions (double-clicking, pressing Enter, middle clicking...); optimization: removing tens of thousands of tracks from a playlist is now much faster</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> simplifies storage code with big performance gains; user data needs to be migrated to the new format</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> updates author profile actions when configuration changes</li>
<li><a href="http://krita.org/">Krita</a> adds Clipboard brush, Isolated Mode for nodes, can convert a paint layer to any mask and back using a menu</li>
<li>libkface introduces new face recognition API.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-05-26/">Read the rest of the Digest here</a>.