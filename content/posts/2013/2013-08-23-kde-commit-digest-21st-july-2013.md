---
title: "KDE Commit-Digest for 21st July 2013"
date:    2013-08-23
authors:
  - "mrybczyn"
slug:    kde-commit-digest-21st-july-2013
---
In <a href="http://commit-digest.org/issues/2013-07-21/">this week's KDE Commit-Digest</a>:

<ul><li> <a href="http://amarok.kde.org/">Amarok</a> fixes organizing/copying/moving tracks in Windows</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> implements git stash show in the Stash Manager, adds PHP 5.4 features</li>
<li>Homerun adds "Add to Desktop" and "Add to Panel" actions</li>
<li>Memory usage optimization in Mixedmaildir in <a href="http://pim.kde.org/">KDE-PIM</a>; memory usage and speed improvements in <a href="http://pim.kde.org/akonadi/">Akonadi</a></li>
<li>In Calligra, significant refactoring in the Predefined Brush engine</li>
<li><a href="http://utils.kde.org/projects/okteta/">Okteta</a> porting to Qt5 and KDE Frameworks in progress</li>
<li>GSoC work in <a href="http://edu.kde.org/kstars/">KStars</a>, <a href="http://nepomuk.kde.org/">Nepomuk</a>, KHipu.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-07-21/">Read the rest of the Digest here</a>.