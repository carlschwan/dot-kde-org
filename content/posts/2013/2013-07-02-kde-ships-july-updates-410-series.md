---
title: "KDE ships July updates to 4.10 series"
date:    2013-07-02
authors:
  - "kallecarl"
slug:    kde-ships-july-updates-410-series
---
Today KDE released <a href="http://www.kde.org/announcements/announce-4.10.5.php">updates for its Plasma Workspaces, Applications and Development Platform</a>. These are the latest in the series of monthly stabilization updates to the 4.10 releases. The 4.10.5 updates contain many bugfixes and translation updates on top of the 4.10 release and are recommended updates for everyone running the 4.10 release series.

Please see the <a href="http://www.kde.org/announcements/announce-4.10.5.php">announcement</a> for details.
