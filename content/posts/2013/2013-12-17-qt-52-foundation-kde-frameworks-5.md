---
title: "Qt 5.2 - Foundation for KDE Frameworks 5"
date:    2013-12-17
authors:
  - "jospoortvliet"
slug:    qt-52-foundation-kde-frameworks-5
comments:
  - subject: "Impressive"
    date: 2013-12-17
    body: "Very nice.  KDE code is everywhere :)"
    author: "_Richard"
  - subject: "Indeed."
    date: 2013-12-19
    body: "Indeed.\r\nIMHO this post would also be worth for (re-)posting on the Qt-Blog, since it's obviously interesting for all Qt-users."
    author: "Richard2"
  - subject: "Fantastic News"
    date: 2013-12-21
    body: "This is fantastic news that will hopefully concrete a solid working relationship to ensure smooth future additions to Qt from KDE developers as well as other Qt developers to improve the speed of creating new Qt applications as well as improve the stability of new applications.\r\n\r\nI hope the KDE developers will also work with Qt to remove all known bad insecure C/C++ functions and Qt functions as well as integrate coding best practices directly into Qt such as performing a all 0s format of RAM each and every time it is allocated as well as performing a all 0s format of RAM during the de-allocation process. \r\n\r\nSome programmers do not like the later of this and like to keep content cached in RAM, I personally prefer a coding style that completely frees RAM upon a graceful exit of the application as well as a crash of the application. I find this provides more overall system performance than caching content in RAM after a application has been terminated.\r\n\r\nI am sure I am not the only person who has been irritated from applications that do not clean out RAM after exiting the program or the function from within the program. One of the most irritating situations I have seen for many years is creating virtual hard drives for applications like VirtualBox which then uses up a massive amount of RAM and does not free it even after exiting the application. This has always led me to have to perform a reboot in order to restore the normal operational speed of my systems."
    author: "Timothy"
  - subject: "Memory"
    date: 2014-07-08
    body: "\".. completely frees RAM upon a graceful exit of the application as well as a crash\" \r\nHow would I do that reliably for crashes caused by a double-free event? And how do discover type of crash without using valigrind-like instruments?\r\n\r\nPS: I am freeing all memory I allocated in my apps, this or other way.  I am with you in irritation if app does not free the memory because the dev was lazy. But also for OSes that properly free process' memeory (quite a few actually), for the future I consider fast (not emergency) exit as an *optimization* when calling billions of destructors just make no sense. This seem to work if I only use destructors do not call I/O operations. And the full destructors code should still exist, it just could be skipped if I know what am I doing.\r\n\r\n"
    author: "Jaros\u0142aw Staniek"
---
On December 12th, the <a href="http://qt-project.org/">Qt Project</a> released Qt 5.2. Congratulations to the Qt community for this great milestone! This version will form the foundation of <a href="http://dot.kde.org/2013/09/25/frameworks-5">Frameworks 5</a>, the upcoming modularized release of the KDE libraries. As part of the Frameworks efforts, KDE devoted considerable effort to integrating valuable KDE technologies into Qt 5.2. This article is intended to give a glimpse at some of KDE's contributions to Qt.
<div style="width: 360px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/2013/09/25/frameworks-5"><img src="http://dot.kde.org/sites/dot.kde.org/files/kf5_no_tier4.png" /></a><br />Frameworks 5 makes the KDE libraries modular.</div> 
<h2>Open Governance</h2>
Contributing to a Free and Open Source Software (FOSS) project is easier when a transparent, open governance model is in place. The Qt community was <a href="http://dot.kde.org/2011/10/21/kde-applauds-qts-move-open-governance">introduced to Open Governance</a> a little over two years ago. Members of the KDE community were heavily involved in the Open Governance development, contributing their experience from KDE and other open source communities. As <a href="http://www.macieira.org/blog/qt-stats/">statistics show</a>, 20% to 50% of the contributions to the Qt codebase comes from community, companies and individuals, showing that Open Governance is successful.
<h2>Code Development</h2>
The KDE Libraries have developed during more than 15 years of work on a wide variety of KDE applications. Sometimes the functionality was an addition to existing Qt classes, other times new libraries were developed for functions such as hardware discovery (Solid) or file access (KIO). As a precursor to the plans of splitting these many libraries into easy reusable components (the work behind <a href="http://dot.kde.org/2013/09/25/frameworks-5">Frameworks 5</a>), the libraries were vetted for code suitable for inclusion in Qt. The goal of this effort was to decrease the complexity of dependencies in Frameworks. For example, by adding timezone support to QDateTime, many core (especially Personal Information Management-related) libraries could depend directly on Qt instead of also needing the KDE localization libraries.
<h2>Contributions</h2>
The code contributions from KDE to Qt varies from small fixes to Qt 5.0's entire <a href="http://en.wikipedia.org/wiki/MIME">MIME type</a> system. Due to the history of much of this code as well as the overlap between KDE and Qt developers, most of these contributions were collaborative. During the process of upstreaming from KDE to Qt, parts of the code were rewritten, new tests and documentation were added, and the code was generally refined. Some code was inspired by minor KDE requests; some Qt classes were taken directly from KDE libraries. Improvement examples:
<ul>
<li>Most advanced CUPS features from KPrintDialog were added to QPrintDialog by Martin Klapetek and Rohan Garg, and were approved by Qt Print maintainer and KDE member John Layt. These include Odd/Even Pages, Multiple Pages Per Sheet, Server-Side Page Selection, Banner Pages and Print Job Scheduling.</li>
<li>Many things KDE applications used to do via KGlobalSettings and KStyle were upstreamed into QStyle and Qt's platform plugin system, which means that pure-Qt QWidget apps now integrate a lot better with KDE and other target platforms.</li>
<li>The Frameworks team upstreamed dozens of widget features from the kdeui library into Qt, including, for example, title support for QMenu, clear buttons in QLineEdit, URL drops in QComboBox, place holder texts in QTextEdit, a new static method in QInputDialog called getMultiLineText(). QColorDialog has a button for scanning the screen colors and picking them up. In addition, it got a new QLineEdit that shows the selected color in hexadecimal. These new features that are so useful for graphics program developers were in the KColorDialog and were reviewed and contributed to Qt by David Gil in order to deprecate the library in KDE.</li>
<li>QtQuickControls, the eventual successor of QtWidgets, has gotten <a href="http://www.sharpley.org.uk/blog/fixing-oxygen-controls">improvements in styling and drawing</a>.</li>
<li>A major feature added was time zone support in QDateTime, a much-requested feature since Qt 1 days. This is very important to KDE as it means there is no longer a need for a separate implementation of KDateTime, KTimeZone, KLocale date parsing/formatting, and other date widgets. This frees many of KDE core and PIM libraries from depending on any other KDE libraries and allows them to depend only on Qt itself; it also makes them far more useful to Qt developers.</li>
<li>Another significant localization feature contributed by KDE was the new QCollator class implemented by Aleix Pol Gonzalez. This new class finally adds locale-aware sorting support to Qt, a feature used by KDE applications such as Dolphin and now available to all Qt developers.</li>
<li>KStandardDirs APIs were upstreamed by adding and extending QStandardPaths, which allows application authors to deal more easily with standard locations on various desktop platforms.</li>
<li>KDE contributors wrote Qt 5.0's MIME type system.</li>
<li>The Qt shortcut system was edited by David Edmundson so that shortcuts are now fetched from the underlying platform. As a result, all KDE Qt applications will now follow the user-configured shortcuts from within KDE Plasma Workspaces.</li>
<li>KDE contributors helped <a href="https://codereview.qt-project.org/#change,64767">resurrect X11 session management</a> handling in Qt 5, which had been lost during the transition to <a href="http://qt-project.org/wiki/Qt-Platform-Abstraction">QPA</a>.</li>
<li>Frameworks developers added QCommandLineParser, a frequently requested feature in Qt: parsing application-defined command-line arguments.</li>
<li>The QFontDatabase::systemFont API makes it possible to query the QPlatformTheme (or the OS) about the standard system fonts for different uses and give a QPlatformTheme the ability to provide a Monospace font.</li>
</ul>
There is a more complete list <a href="http://community.kde.org/Frameworks/Epics/Contributions_to_Qt5">on the KDE development wiki</a>. Due to the extensive efforts of many people, it is not possible to list all of the improvements. 
<h2>Conclusion</h2>
KDE code upstreaming is ongoing, with a variety of improvements planned for Qt 5.3 and further. These code contributions are closely scrutinized to ensure quality and applicability to the Qt community beyond KDE. A Qt library archive is available to all Qt toolkit users. The goal of this archive, called <a href="http://inqlude.org/">Inqlude</a>, is to provide a comprehensive listing of all existing libraries for developers of Qt applications. 

The KDE Community is committed to Qt quality, extensive capabilities and availability for all Qt developers.

<em>A big thanks to Sune Vuorela who proposed the idea for this article and the people who contributed to it, especially David Faure, John Layt and the many KDE developers who reviewed and added to the wiki.</em>