---
title: "KDE PIM Sprint Berlin 2013 - With Cuter Pictures"
date:    2013-04-11
authors:
  - "jospoortvliet"
slug:    kde-pim-sprint-berlin-2013-cuter-pictures
comments:
  - subject: "\"Negative attitudes about KMail are frustrating, especially when"
    date: 2013-04-11
    body: "\"Negative attitudes about KMail are frustrating, especially when they come from within the Community.\"\r\nI can understand this very much, but you also have to understand the people's frustration. It is not about wanting to be destructive. Personally I love KDE and I want to use KDE Applications for everything. If there is no suitable app it is a shame. I stuck very long with Kmail2, but it broke my work flow to much (I wont mention the bug, I guess everyone knows) So I had to switch to Thunderbird, which is not so well integrated into KDE and also misses features. It is just that as a user you are helpless, if one can not change anything. \r\nI would be ready to donate something if that would help and I guess there would be more users willing to that, if there was a kind of plan and timeframe to fix the rest of the major problems. Why not: Programmers compile a list of things they think is needed to be fixed in Kmail combine this with a list of things added by vote by users, create a timeframe and start a fundraiser so that Kmail devs have more time to work on it. It would be so great to have a perspective when Kmail is usable again (and yes, it has improved a lot)."
    author: "mark"
  - subject: "Don't stop. This is brilliant work."
    date: 2013-04-12
    body: "I wouldn't listen to the naysayers about KMail. They don't see the tough problems that the KDEPIM team is aiming to solve, is solving, which are far more challenging than anything else out there. And for those with eyes that can see, the rewards are far greater as well. \r\n\r\nI've been programming for over 30 years and have been dying to jump into KDE. I recognize excellent design and engineering when I see it. I've been using KMail/Kontact since 2008 and have stuck with it through everything without regret, and can see how the benefits of its unified vision are just now beginning to come together. So exciting!\r\n\r\nDon't stop. Keep going. I was silently applauding when 4.10.2 came out and will continue to applaud! Thank you!"
    author: "Michael"
  - subject: "Promotion"
    date: 2013-04-12
    body: "Let me tell you something: I love Kmail! I tried several mail clients and none were really satisfying. Kmail still has its flaws, though, especially with calendar integration. But that google calendar trouble is probably more a Kontact/Akonadi issue.\r\n\r\nPromotion: Although I'm following TheBlueMint on twitter I didn't know about the sprint until afterwards. That's pretty bad, as I really would love to help fix that annoying google calendar bug. And after reading your post I got the feeling that your sprint might have been the best occasion to get started with that. Next time, please promote such events a little bit more.\r\n\r\nRegards\r\nRobert"
    author: "Robert"
  - subject: "Kontact++"
    date: 2013-04-12
    body: "I use Kontact every day, have done so since the KDE 3 days, and I wouldn't want to use anything else. I've been very happy with the bug fixes in KMail2 and the backend code recently. I do think though that as of 4.10.2, mail searching and address book loading and the reliability and performance of the backends behind them still need some attention.\r\n\r\nIt's great to see that KDE PIM team is getting together for these hackfests - KDE seems to do very well at organising these and they can be hugely valuable. Thanks from me and keep doing what you're doing! :)"
    author: "bluelightning"
  - subject: "We usually announce sprints"
    date: 2013-04-13
    body: "We usually announce sprints on the respective developer list, in this case kde-pim@kde.org\r\nHowever, if you send me (krammer@kde.org) an email, I'll make sure that you are contacted explicitly when we do our next sprint.\r\n"
    author: "kevkrammer"
  - subject: "One of the less obvious"
    date: 2013-04-13
    body: "One of the less obvious problems with funding specific development work is that most developers are employed at some company and not all of those are in the market of work-for-hire. And those which are will often need a legally viable entity as their business partner, e.g. for accounting, taxation, etc.\r\n\r\nNevertheless it might be an opportunity for freelance developers or consulting startups to do something like that as a kickstarter project or on a similar crowdfunding platform"
    author: "kevkrammer"
  - subject: "Thanks! :-)"
    date: 2013-04-13
    body: "Thanks! :-)\r\n\r\nOne of the most frustrating things is that the knowledge that the new architecture has so much potential is currently not easy to communicate.\r\nBtw, making it easier to jump in for new developers is one of the design goals ;-)\r\n"
    author: "kevkrammer"
  - subject: "Thanks :-)"
    date: 2013-04-13
    body: "Thanks :-)\r\n\r\nOur sprints are an invaluable tool, not only because it gets KDE PIM developers together and thus decreases communication delay, but also because they now also include developers of projects that KDE PIM code has to interact with very closely, e.g. search and indexing facilities or the up-coming single-sign-on framework.\r\n"
    author: "kevkrammer"
---
<div style="width: 354 px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://community.kde.org/File:Pim_sprint_berlin_2013.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Pim_sprint_berlin_2013_wee.jpg" /></a><br />The Pimsters (<a href="http://community.kde.org/KDE_PIM/Meetings/Osnabrueck_11">names here</a>)</div> Once most people had arrived at the <a href="http://www.kdab.com/">KDAB</a> offices in Berlin, the KDE PIM sprint started around 4 in the afternoon with an introduction by Till Adam. He welcomed everyone and issued a warning: there were only one-and-a-half crates of beer and all KDAB attempts at ordering more had failed. The participants would have to take care of this!

<h2>Friday beginnings</h2>
Cornelius then kicked off self introductions. The introductions suggested an in-crowd meme of "I do some random Kolab stuff" with Jeroen and Kevin claiming to "play around with IMAP", Volker (The Guru) topped everyone by mentioning that he's "done a patch or two to Akonadi". Nobody bothered being more modest than that. Following the introductions, tasks were brainstormed and written on sticky notes in the 'TODO' area on the whiteboard. Getting Beer became Task #1. 

Work started and fun as well. In the hallway, the fussball tables were busy. With over 30 participants, the hacking room couldn't hold everyone comfortably. David, coming late, even had to bring his own table to the meeting and Ingo arrived just in time for dinner at 19:00. We had our evening meal at an Asian place. There was a little trouble fitting in so many people but it was fun. A Spaniard had to tell the Indian guy what to eat ... We'll leave the results to your imagination.
 
<h2>Saturday continuations</h2>
On Saturday, <a href="http://dot.kde.org/2012/10/25/kde-pim-october-sprint">Popcorn once again joined</a> the sprint and attempted to help the PIM hackers hunt bugs while simultaneously entertaining them. During the course of discussions, it was decided to drop the KDEPIM coding style for the KDE Libs coding style. And a list of GSOC ideas was brainstormed. Till gave an update on PIM/KDE on Win/Mac, promising an installer Coming Real Soon™. There was also talk about Frameworks 5; it was decided to wait until the dust has settled a bit before taking KDEPIM fully in that direction. In some areas however, there will be movement right away as kdepimlibs is already being compiled against Frameworks 5 and some Time/Date-related code is going into Qt5. 
<div style="width: 354px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/ShoelaceAndPopcorn.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ShoelaceAndPopcorn_wee.jpg" /></a><br />Prisoner was taken but the laces did not hold.</div>
It had taken a while for Task #1 (Get Beer) to be achieved and the fussball tables were fired up again. There was a Nepomuk discussion about how to improve performance and give a smoother experience to users. The Saturday evening meal was in a Greek place near the office. Popcorn joined her fellow hackers, but didn't have a leash; Jeroen insisted on using his shoe laces for this purpose, which did not survive the night. But for a while, Popcorn was constrained to barking from a corner of the table.

<h2>Sunday endings</h2>
End users were again the focus of the marketing meeting on Sunday. Much work has been done lately to modernize the Kontact architecture. The quest for this sprint was to find the remaining major blocks for Kontact users. The group made a list of the most serious issues; anything that can't be fixed will be disabled until a proper fix can be found. Negative attitudes about KMail are frustrating, especially when they come from within the Community. Yes, KMail has been around a while, and people would enjoy a new mail client based on Akonadi (which shouldn't be too hard). But venerable KMail is still by far the best Free Software mail client on Linux, years ahead of the competition. The negativity feeds a vicious cycle—KMail can be improved, but development work is not satisfying when this general attitude prevails. So developers don't want to work on it. The group discussed ideas for promoting "eat our own dogfood", including "I use KMail" and "I have a good Kontact experience" stickers. Popcorn loved this idea. Suggestions for artwork are welcome!

There were discussions about the future of calendaring, and a <a href="http://community.kde.org/Calendar_API_QML">first draft of QML components was created</a> for calendaring. Sandro Knauß had been annoyed with issues about encrypting and signing emails with unicode characters and had joined the  sprint in order to fix those. Which <a href="https://git.reviewboard.kde.org/r/109272">he did</a>! And there were many more subjects as well as hacking and coding, fussball, and the deep philosophical inquiries expected at any KDE Sprint. As a relatively new participant remarked: <em>I can't wait for the upcoming <a href="http://akademy2013.kde.org/">Akademy</a>. These sprints are awesome and everybody says "wait until you've been at Akademy, it's 10 times more cool"</em>.

<h2>Conclusions</h2>
All in all, barking, epic fussball losses and grouphugs included, it was a fun and productive meeting again. More happened than is reported here ... in summary, good code was written and there is significant progress with KDE PIM. The decision to get as many fixes as possible into the 4.10.2 release will benefit users of most major distributions in the short term. In the longer run, code is being written to bring in new functionality as well, giving users something to look forward to.

We all look forward to meeting again at <a href="http://akademy2013.kde.org">Akademy</a> to assess progress and work further on bringing KDE PIM to enlightened, freedom-loving people around the world.