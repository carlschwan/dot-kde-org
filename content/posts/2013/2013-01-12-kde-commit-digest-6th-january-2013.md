---
title: "KDE Commit-Digest for 6th January 2013"
date:    2013-01-12
authors:
  - "mrybczyn"
slug:    kde-commit-digest-6th-january-2013
comments:
  - subject: "Hinting"
    date: 2013-01-24
    body: "\"Okular offers a GUI to configure text/graphics anti-aliasing and text hinting\"\r\nI have waited so much for this!, wwooaa!! :D\r\nThank you very much"
    author: "Frost"
---
In <a href="http://commit-digest.org/issues/2013-01-06/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://edu.kde.org/marble/">Marble</a> supports KML (<a href="https://developers.google.com/kml/documentation/">Keyhole Markup Language</a>) tag writers in multiple places and improves flexibility in map themes</li>
<li><a href="http://okular.org/">Okular</a> offers a GUI to configure text/graphics anti-aliasing and text hinting</li>
<li>DrKonqi shows date and time of a crashed application</li>
<li><a href="http://www.krita.org">Krita</a> adds QML export and line smoothing support</li>
<li><a href="http://www.kdenlive.org/">Kdenlive</a> screen capture uses <a href="http://ffmpeg.org/">FFmpeg</a> now</li>
<li><a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> distinguishes stop names from IDs in the engine</li>
<li><a href="http://amarok.kde.org/">Amarok</a> is now more responsive loading large directory structures into playlist.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-01-06/">Read the rest of the Digest here</a>.