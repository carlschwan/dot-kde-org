---
title: "KDE Commit-Digest for 11th August 2013 "
date:    2013-09-05
authors:
  - "mrybczyn"
slug:    kde-commit-digest-11th-august-2013
---
In <a href="http://commit-digest.org/issues/2013-08-11/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> now supports direct download of Jamendo albums instead of downloading via BitTorrent; the download is now far more reliable, and on-the-fly transcoding to other formats is possible</li>
<li><a href="http://kate-editor.org/">Kate</a> Vim support adds a new extension: interactive sed replace when the 'c' flag is used when also using the emulated command bar</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> adds overlay rendering support</li>
<li><a href="http://www.digikam.org/">Digikam</a> adds Multi-core support for Metadata Synchronizer tool</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds an API for writing ODF that is generated from the ODF RNG file</li>
<li>Much activity in KDE Frameworks: adjustment of ThumbnailItems to QtQuick2, a new program which takes a .desktop file and writes out a .json file from it and more.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-08-11/">Read the rest of the Digest here</a>.