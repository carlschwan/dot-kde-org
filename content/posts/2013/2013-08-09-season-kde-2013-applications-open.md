---
title: "Season of KDE 2013 Applications Open"
date:    2013-08-09
authors:
  - "David Edmundson"
slug:    season-kde-2013-applications-open
---
<h2>Season of KDE is now officially open for applications.</h2>

To apply head to <a href="http://season.kde.org/">season.kde.org</a> register as a student and click "Submit a proposal"

<h3>What is Season of KDE?</h3>

Season of KDE is a community outreach program, much like Google Summer of Code that has been hosted by the KDE community for five years straight.

It is meant for people who could not get into Google Summer of Code for various reasons, or people who simply prefer a differently structured, somewhat less constrained program. Season of KDE is managed by the same team of admins and mentors that take care of Google Summer of Code and Google Code-in matters for KDE, with the same level of quality and care.

<h3>Who can take part?</h3>

Everyone can apply for Season of KDE. We give preference to those who have applied for Google Summer of Code and to students, but we will gladly consider applications from anyone.

<h3>What do I get out of this?</h3>

A great summer working on a really cool KDE project and gaining valuable experience. If you complete your project successfully you also get a T-shirt, a certificate, and maybe a few other goodies.

<h3>What is the timeline?</h3>

Season of KDE is a flexible project to fit around school terms, work, and other commitments, and start and end dates can be discussed with your mentor. Projects should be completed before the end of the year, a typical Season of KDE project should take around 2 months.


<h3>How do I apply?</h3>

First get in touch with a mentor about your ideas, and what projects they want to run.

Then head to <a href="http://season.kde.org/">season.kde.org</a> and follow the instructions.

<h3>Do I need to have a mentor before applying?</h3>

It is preferred. Ideally, you should contact a KDE sub-project well before applying, ask for feedback on your idea if you have one, and request a mentor directly. A list of KDE sub-project contacts is available on the Google Summer of Code 2013 <a href="http://community.kde.org/GSoC/2013/Ideas">ideas page</a>. You can also apply without a mentor and we will try to find one for you.


<h3>Do I need to have a project idea before applying?</h3>

It is preferred. If you do not have one we will try to find one for you. Keep in mind that the KDE community is pretty big, so you should at least have an idea of which KDE sub-project you wish to work on.

<h3>Do I need to write a proposal like in Google Summer of Code?</h3>

No, but we would like to see a brief project plan describing what you will be working on.

<h3>Is it only for coders like Google Summer of Code?</h3>

We are willing to consider non-coding projects as well including artwork and promotion, but you should definitely get in touch to figure out the details beforehand. The <a href="http://community.kde.org/Getinvolved">KDE Community Wiki</a> describes ways to get involved with KDE that do not require coding.

<h3>I applied for a project in Google Summer of Code but another student got selected for it. Can I still work on it?</h3>

Maybe, but likely not. You should ask the mentor that was assigned to your idea. We can try to find something related for you if you want, or something completely different. Let us know what you wish and we will do our best to accommodate your request.

<h3>Is this an extension of Google Summer of Code or connected to Google?</h3>

No. While Season of KDE is in many ways modelled after Google Summer of Code and administered by the same members of the KDE community, it is completely independent from Google Summer of Code and has no connection to Google whatsoever.

—

For further questions feel free to join our IRC channel #kde-soc on Freenode or email the admin team at <a href="mailto:kde-soc-mentor-owner@kde.org">kde-soc-mentor-owner@kde.org</a>