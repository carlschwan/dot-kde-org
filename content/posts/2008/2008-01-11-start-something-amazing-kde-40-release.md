---
title: "The Start of Something Amazing with KDE 4.0 Release"
date:    2008-01-11
authors:
  - "skuegler"
slug:    start-something-amazing-kde-40-release
comments:
  - subject: "YAY!"
    date: 2008-01-11
    body: "GO KDE 4.0!\n\nWill be trying it out as soon as I get a chance tomorrow morning Sydney time ;)\n\nKDE 4.1 will be even more awesome!"
    author: "Andrew Donnellan"
  - subject: "Looking forward to the return of kdepim"
    date: 2008-01-11
    body: "Hope kdepim comes along soon!"
    author: "Luke-Jr"
  - subject: "Re: Looking forward to the return of kdepim"
    date: 2008-01-11
    body: "It compiles, but does not have nearly the amount of polish and testing that it needs. For this, PIM really needs people who want to help out in a constructive manner -- testing, bugfixing. Although KDAB does a lot of good work, they can't save the world -- or PIM -- on their own.\n\nPS. KPilot in PIM for KDE4 *will* eat your children and set your Pilot to speak LOLCAT irreversibly. This is another area we could use some help on; it would up the number of active developers from zero. If you need an extra Pilot to help testing, let me know and I'll send one. Seriously."
    author: "Adriaan de Groot"
  - subject: "At last!"
    date: 2008-01-11
    body: "Downloading kubuntu packages right now :)\n\nI'd like to say THANKS to anyone that have made this release possible. Nice one!"
    author: "NabLa"
  - subject: "Impressions"
    date: 2008-01-11
    body: "THE BAD:\n* Hmmm how do you add new things to the bottom panel? I removed everything and there is no context menu or anything at all for it... I can't even shut it down to use the old kicker instead (I can fire up kicker, but there is a lot of wasted space at the bottom where the panel sits)... and drag & drop from desktop doesn't seem to be possible (wasn't this one of the Plasma goals?)\n\n* kwin: window compositing seems to be extremely slow, and it corrupts the contents of new windows very frequently... yes, I've got all the proper X configuration and I can run compiz on no XGL very nicely and I've got a nice beefy nvidia card.\n\n* The new menu is awful... The old menu is still there, however, I cannot add it to the panel as stated above... I can't change the size of the panel either, it's insanely thick... I've got a 1280x800 screen so vertical space and clearance is certainly an issue.\n\n* Well, basically, Plasma seems as uncooked as it was a couple of months ago, just slightly prettier... yes, I know all about \"setting the foundations blah blah\", but its quality is beta at the very best... it has crashed a few times (leaving a very nice white screen behind). I'm sure the underlying libs are shiny and nice, but the actual interface is very klunky...\n\n* Oxygen: i wish it differentiated better between the active window and the rest of inactive windows in terms of different top window colour etc... There is a nice composite plugin that does the job, but compositing seems to struggle on my computer.\n\n* I plugged in my iPod and... nothing happened on the widget that was supposed to tell me about it...\n\nTHE GOOD:\n* The new apps are amazing. okular, kopete, etc they all work really well... even if I fall back to KDE3 for a while I'll still use them as they're far superior to KDE3's in most of the cases. Marble is a joy.\n\n* Sound works perfect without the massive overhead of the, gone for good, arts daemon.\n\n* Oxygen: looks really pretty and easy on the eyes. I love it.\n\n/*** SO ***/\nAll those issues are due to the Kubuntu Gutsy packages being crap, or KDE4 being very flimsy?"
    author: "NabLa"
  - subject: "Re: Impressions"
    date: 2008-01-11
    body: "Try to drag from the desktop widget toolbox onto the panel -- it's not quite perfect, but it does work.\n\nThe slowness may be due to a buggy nvidia driver. Nvidia already has released an update in December."
    author: "Boudewijn Rempt"
  - subject: "Re: Impressions"
    date: 2008-01-11
    body: "Cheers Boudewijn, I'll try what you suggest :)"
    author: "NabLa"
  - subject: "Re: Impressions"
    date: 2008-01-11
    body: "Well I'm using the opensuse packages on 10.3 and haven't had any crashes and compositing works snappily on my fairly average go5700 nvidia card (latestdriver).\n\nThe panel is less functional than kicker for the moment but what it does it does well.  I had no problem dragging the lockout applet to it for example, and the menu now allows you to add items to panel or desktop (rightclick to access).  I don't know how good the kubuntu packages are but the suse ones seem to have been polished (if only little opensuse idents here and there) and they have the most paid developers working on it (correct me if I'm wrong please), summary here http://www.kdedevelopers.org/node/3196  and always upto date packages so I'm biased."
    author: "opensuse user"
  - subject: "Re: Impressions"
    date: 2008-01-12
    body: "I can only confirm what you said. I am also using openSuse 10.3. I had a little problem with compositing yesterday (I have a nVidia card) but that also improved after using a tip somebody here gave.\nThe only funny thing that I am experiencing is that the panel flickers and gets transparent sometimes when I am writing in FIrefox. It might be a bug. I might report it later. Otherwise things seem to be quite smooth and stable. I am slso using KDE3 apps without a problem. Just checked my e-mail using Kmail and burned a CD with K3b.\n\nI would also like to say a very special thanks to the openSuse people who made it possible for us to get an almost daily update of the KDE 4.0 packages upto release. Well done :) "
    author: "Bobby"
  - subject: "Re: Impressions"
    date: 2008-01-12
    body: "And today, just one day after release, the suse packages are yet again updated. \n\nps I wish I had never installed kubuntu as a quick fix after a dead HD with gentoo on it. openSUSE you guys rock (oh... and the kde devs ;) )"
    author: "Chaarles Fryett"
  - subject: "Re: Impressions"
    date: 2008-01-12
    body: "My personal impressions:\n\nBAD first:\n- Some apps like ktorrent and dolphin are slower than it's KDE3 counterparts.\n- Plasma.. is still like a baby, it does have potential, but right now it just eat and produces some bad-smelling material ;) Not being able to drag and drop a plasmoid from desktop to taskbar is bad.\n- Oxygen style needs more work. Much of criticism (fair ones in my opinion) that was made about it was not fixed. Having the same color for window title for focused and non-focused windows is horrible, no matter what oxygen team says.\n- Still immature, BUT it is way, way better than the release candidates, so 4.0.1 should be much better. Let's hope it dosen't take more than 2 months to it arrive.\n\nGOOD ones:\n- splash and logout screens are wonderful. It look very professional and in my opinion should serve as an example to other areas to follow.\n- Seems much more simpler than 3.X, this will probally be a good thing for new users once things mature enought for them being able to use it without much bugs and not-yet-done parts.\n- Uses qt4. Currently I belive qt is much superior than KDE (for 2.0 and 3.0 I tought the inverse), so KDE have to catch this quality in the code.\n\nOverall not impressive, but I'm betting KDE 4.1 will be.\nI think this first release should be called KDE 3.9."
    author: "Iuri Fiedoruk"
  - subject: "Re: Impressions"
    date: 2008-01-13
    body: ">Not being able to drag and drop a plasmoid from desktop to taskbar is bad.\n\nFunny thing, something like this was never possible with the old Kicker(You could drag icon/links, but not applets etc). \n\nAnd as far as i know, that kind of functionality are not avaliable in any other desktop either. But Plasma will eventually make it possible."
    author: "Morty"
  - subject: "Re: Impressions"
    date: 2008-01-14
    body: "No, you are wrong - he just used some misleading phrases:\n\nIn KDE3.5,\n(1) it is possible to drag a desktop object to the kicker panel.\n(2) it is possible to frag a desktop object to the pager (kpager2) to be opened there.\n(3) it is possible to drag a taskbar item to the desktop (then the corresponding link/URL/process name will be saved)\n\nKDE3.5 got some nice and handy drag/mouse/key shortcuts, just that most people didn't know..."
    author: "Sebastian"
  - subject: "Re: Impressions"
    date: 2008-01-13
    body: "\"Plasma.. is still like a baby, it does have potential, but right now it just eat and produces some bad-smelling material ;) Not being able to drag and drop a plasmoid from desktop to taskbar is bad.\"\n\nPlasma is still under heavy development we were told so what we see is just the beginning. Still we are the babies, we are the ones who have to learn  how Plasma behaves.\nIf you want to drag and drop a plasmoid (widget) to the panel then you won't be successful by draging it from the desktop. You have to DRAG IT FROM THE PLASMOID DIALOG BOX. Yes the same window (whatever it's called) that you use to add the plasmoids to the desktop is where you have to drag them from to the panel. You can right click on icons in the start menu in order to add them to the panel or desktop :)"
    author: "Bobby"
  - subject: "Re: Impressions"
    date: 2008-01-13
    body: "What I would like to see is \"panel versions of plasmoids\" build in every plasmoid. For example dragging NewsTicker plasmoid into panel should give me a smaller plasmoid with news which would look there better. (AFAIR Aseigo was talking about sth like this so maybe tit will happen :) )\n\nbtw. section \"Plasmoids\" is on kde-look.org instead of kde-apps.org. ATM Plasmoids are all C++ apps which need compilation. So IMO they should be placed in kde-apps.org (on kde-apps.og there are already 2 plasmoids because authors didn't new that proper place is look.org ). Ideally ATM there should by Plasmoid sections  on -look and -apps which content would be the same. \n"
    author: "Koko"
  - subject: "Re: Impressions"
    date: 2008-01-13
    body: "\"What I would like to see is \"panel versions of plasmoids\" build in every plasmoid. For example dragging NewsTicker plasmoid into panel should give me a smaller plasmoid with news which would look there better. (AFAIR Aseigo was talking about sth like this so maybe tit will happen :) )\"\n\nYes, this is explicitly a core goal of Plasma, so you can definitely expect it.  All of the infrastructure is there; applets writers simply need to respect the information provided by the Plasma core and adjust their applets accordingly.  Some already do this, IIRC.\n\n\"btw. section \"Plasmoids\" is on kde-look.org instead of kde-apps.org. ATM Plasmoids are all C++ apps which need compilation. So IMO they should be placed in kde-apps.org (on kde-apps.og there are already 2 plasmoids because authors didn't new that proper place is look.org ). Ideally ATM there should by Plasmoid sections on -look and -apps which content would be the same.\"\n\nFull agreement, here - kde-look.org is the last place I would have thought of looking!\n"
    author: "Anon"
  - subject: "Re: Impressions"
    date: 2008-01-13
    body: "My iPod was detected just fine (kubuntu latest debs).  But my 3rd generation iPod really struggles to speak to linux.  Not sure if it's the iPod being crappy, or the sbp2 firewire driver, but it's never been happy through various iterations of the kernel.  Works fine over usb though (but doesn't charge...)\nI have to say, I'm not keen on the new menu.  But I found it okay to replace it with the old one.\nAs you say, the compositing is dog slow (nVidia 5200 here, works okay with compiz, ack pfft!) even with the NVIDIA_HACK trick but hey, it's only a .0.0 release so I'm sure the optimisation will follow.  Now the artists have finished their generally good work on oxygen, perhaps they could turn their attention to the effects - I had a better \"exploding windows\" effect on WindowMaker 8 years ago, with no accelerated graphics...\nPlus, with a load of files on my desktop those little plasma borders look pretty ugly - hopefully future editions will bring a way to turn them off?  I don't see much need to rotate and resize the icons for the random selection of ready-use files I keep on my desktop.\n\nHowever, congratulations to the developers.  Overall it's a massive achievement and with a bit of optimisation 4.1 will be a stunning general-consumption desktop!"
    author: "Adrian Baugh"
  - subject: "Party!"
    date: 2008-01-11
    body: "Congratulations to the whole community on reaching this very important milestone. May 4.0.0 be the basis for an amazing series of releases!"
    author: "Andre"
  - subject: "Minority Free Software Platforms"
    date: 2008-01-11
    body: "I'll report on \"my\" platforms, as a non-Linux user. FreeBSD is in the \"it compiles, but runtime is buggy\" stage; there is a Qt4 port you can use, and compiling out of SVN has been fairly stable for a long time. Weird konqueror slowdowns have still not been investigated. Lack of manpower is to blame. Some modules fail to compile (multimedia, utils) but I have not looked at them. Join us on #kde-freebsd to help out.\n\nFor OpenSolaris, the situation is the perhaps muddier \"runtime is ok, but I can't explain how to compile it.\" Stefan Teleman just blogged more screenies, but getting KDE4 to compile means a lot of manual work to get the dependencies all compiled with the right C++ libraries. It looks like Qt 4.3.1 is required; Qt 4.3.3 seems to have serious memory leaks in the OpenGL stuff on Solaris, but we haven't had time to track that down either. Join in on #kde-solaris for *that* side of the fence.\n\nKDE4 continues the long KDE tradition of cross-platform support (even if it's not-quite-there on the non-Linux Free Software OSsen -- don't get me started on Plan9 unless you have ported X.org to it recently)."
    author: "Adriaan de Groot"
  - subject: "Re: Minority Free Software Platforms"
    date: 2008-01-11
    body: "\"Some modules fail to compile (multimedia, utils)\"\n\nI am using FreeBSD, and would love to help figure out what's going on with kdeutils. I am able to build it by hand out of svn, with no problems. Unfortunately, my time is almost completely booked until next week. But if you send me a link to the ports, I can poke about if I find some time."
    author: "David Johnson"
  - subject: "Re: Minority Free Software Platforms"
    date: 2008-01-11
    body: "that would be great; ade can use all the help he can get as he's a pivot point right now for both BSD and OpenSolaris as well as being on the KDE e.V. board of directors. oh, and something about a family and a day job. ;) the more people we can get involved with platforms such as FreeBSD, the better! =)"
    author: "Aaron J. Seigo"
  - subject: "Great!"
    date: 2008-01-11
    body: "Congrats to all who worked on it!"
    author: "AIK"
  - subject: "Careful!"
    date: 2008-01-11
    body: "Heh, the promo team managed to surprise me again. I expected a loud hallelujah and praises in this release announcement, it almost feels like pure understatement :D\n\nCongrats to everyone involved! Let's make 4.[0.]1 even more rock!"
    author: "Jakob Petsovits"
  - subject: "Re: Careful!"
    date: 2008-01-11
    body: "Definitely! I'm really happy to see such exhaustive introduction to KDE 4.0. Lately I was looking for good screenshots to show people something, but now I can just point them to kde.org :D\n\nThe visual guide looks good :)"
    author: "Diederik van der Boor"
  - subject: "Re: Careful!"
    date: 2008-01-11
    body: "Yeah, it's a very good announcement, nicely covers everything in an understated way, good job guys.\n\nBut I'm still wonderig about the need for a 'truth in advertising' page detailing what doesn't work so well, what doesn't work at all, and what was deferred to 4.1.  I can hear a million ill-informed bloggers firing up their KBlogger as we speak...\n\nFor my part I need to post something about the printing changes.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Careful!"
    date: 2008-01-11
    body: "there's enough good stuff in the release to concentrate on that for the time being. the blogosphere is actually very positive on this release right now, and 4.1 will end up being an answer-through-action for much of the rest.\n\nthe \"this is the beginning\" communication has been very effective. i even saw it echoed on wired blogs today. people get it. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Careful!"
    date: 2008-01-12
    body: "Perhaps the blogosphere is positive, but public comments seem negative. \n\nJust look at the comments on the CNET article\nhttp://www.news.com/Images-Linux-gets-a-taste-of-Windows-and-Mac/2300-7344_3-6225734.html?tag=nefd.lede\n\nor the discussion on Ars \nhttp://episteme.arstechnica.com/eve/forums?a=tpc&amp;s=50009562&amp;f=174096756&amp;m=704008079831&amp;r=704008079831\n\nor the comments on digg\nhttp://digg.com/linux_unix/KDE4_final_has_been_released_Grab_a_Live_CD_here_to_test_it\n\n"
    author: "Michael Ballantyne"
  - subject: "Re: Careful!"
    date: 2008-01-12
    body: "gnomes and MS employees.\nDon't worry."
    author: "reihal"
  - subject: "Re: Careful!"
    date: 2008-01-14
    body: "Ars Technica is normally a very pro-Linux crowd, and Digg is very mixed.\n\nCNET has been somewhat objective surprisingly in many reviews.  I believe they've been giving the Eee PC great reviews."
    author: "T. J. Brumfield"
  - subject: "Hurray !"
    date: 2008-01-11
    body: "Congrats! KDE-team.. you rock!\n"
    author: "Thomas"
  - subject: "Man I am jumpin :)"
    date: 2008-01-11
    body: "Bundles of congratulations to the devs, you did it people !!"
    author: "Junaid"
  - subject: "THIS IS IT"
    date: 2008-01-11
    body: "/me gives free beer to whole KDE Community!"
    author: "Emil Sedgh"
  - subject: "Re: THIS IS IT"
    date: 2008-01-11
    body: "You mean free beer like FREE Augustiner or just free like \"free\" :D\n\nA big thank you, lots of congrats and praise to the powerful KDE Team!\nI am proud of you guys and happy to have discovered such a wonderful desktop environment that I enjoy using on a daily basis."
    author: "Bobby"
  - subject: "Re: THIS IS IT"
    date: 2008-01-11
    body: "THE BEER IS A LIE!\n\n\n\nNo seriously, as soon as I unbreak portage, I think I'll install this"
    author: "Wyatt"
  - subject: "I love you all..."
    date: 2008-01-11
    body: "I really do!!!"
    author: "j.v."
  - subject: "Re: I love you all..."
    date: 2008-01-11
    body: "Yeah, after all the bashing it is time to give some love!!\n\nGreat job, guys and gals! You are amazing!"
    author: "Jens Uhlenbrock"
  - subject: "OS X and Windows packages"
    date: 2008-01-11
    body: "Excellent! This is truly a great moment. KDE4 can run on Mac OS X and Windows, allowing people that cannot yet switch to a free operating system to use it.\n\nSo where can I get the windows version of KDE4?\n"
    author: "Jos"
  - subject: "Re: OS X and Windows packages"
    date: 2008-01-11
    body: "Sorry to disappoint, but the answer's something like 'in about six months to a year', if you're looking for a polished stable release.\n\nIn the mean time, there's always http://techbase.kde.org/index.php?title=Projects/KDE_on_Windows/Installation if you can live with some  roughness."
    author: "Aneurin Price"
  - subject: "Re: OS X and Windows packages"
    date: 2008-01-12
    body: "This is going to be so cool....\n\nLet's ween people slowly off Windows!!! First they get used to the desktop, then they switch the \"engine\" underneath...\n\nOnce they get used to the new \"body\"/\"interior\", who cares what \"engine\" runs underneath. If it's faster and more stable, even better. :) Let's face it, how many people these days actually look at the engine of their car??? - My point exactly :D\n\n\nSo let's keep the Windows packages coming and stir up the publicity!!!!\n\n\nThank you to all that have been involved!!"
    author: "Max"
  - subject: "Re: OS X and Windows packages"
    date: 2008-01-12
    body: "just in case there is confusion on this point, the workspace is *not* being ported to windows or mac."
    author: "Aaron J. Seigo"
  - subject: "KRunner"
    date: 2008-01-11
    body: "KRunner has a lot of contrast - white on black.  Is this the final design?  Will it be like that in 4.1 as well?\nIt would be nice if it was a bit more soft imho."
    author: "JohnFlux"
  - subject: "Re: KRunner"
    date: 2008-01-11
    body: "IIRC, Ruphy is planning very extensive changes to KRunner for 4.1, visually and internally."
    author: "Anon"
  - subject: "Re: KRunner"
    date: 2008-01-11
    body: "http://wiki.kde.org/tiki-index.php?page=KDE+4.0+Feedback"
    author: "Koko"
  - subject: "Re: KRunner"
    date: 2008-01-11
    body: "Honestly, I love it. It fits the look and feel of my beloved Thinkpad X60s nicey."
    author: "Torsten Rahn"
  - subject: "Re: KRunner"
    date: 2008-01-14
    body: "I think it's ugly.\nIt should look more like the sign-off screen and less like a mix of dark and white that made me remember some obscure windows 3.1 apps :-P"
    author: "Iuri Fiedoruk"
  - subject: "Awesome!"
    date: 2008-01-11
    body: "4.0 is awesome (and its just the beginning :D), thanks to all the people for their hard work!\n\nBy the way, splitted packages for Arch Linux are available here:\n\nhttp://www.kdemod.ath.cx/bbs/viewtopic.php?id=406\n\nThanks again, and have a nice weekend :)\n"
    author: "Jan"
  - subject: "openSUSE wiki bug?"
    date: 2008-01-11
    body: "\"openSUSE 10.3: For openSUSE 10.3, the KDE Team worked on the base technologies of KDE 4. openSUSE 10.3 offers by default a KDE3 desktop with single KDE4 applications. A preview of KDE 4.0 desktop (Beta 2) can also be installed. The aim is to offer an easy way to update to the final KDE 4.0 release once it is available.\"\n\nBeta2? This is outdated right? When I w ill click this button on 10.3 I will get KDE4?"
    author: "Koko"
  - subject: "Re: openSUSE wiki bug?"
    date: 2008-01-11
    body: "I've been using the beta packages for a while, and I just updated and it's installing 4.0 packages, rather than 3.9.x.x so it looks like they already updated the repos..."
    author: "Kane"
  - subject: "Re: openSUSE wiki bug?"
    date: 2008-01-11
    body: "Beta 2 is on the openSUSE 10.3 DVD and the official repository. The KDE4 repository contains the most recent version for a long time ago, now 4.0. The button installs that."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: openSUSE wiki bug?"
    date: 2008-01-11
    body: "The repros have been updated since the 10.01.2008 at 23:33. I have updated my packages since this morning (amost 9 hours ago)."
    author: "Bobby"
  - subject: "Re: openSUSE wiki bug?"
    date: 2008-01-11
    body: "I've just fixed this at http://en.opensuse.org/KDE/KDE4 ."
    author: "Bille"
  - subject: "Re: openSUSE wiki bug?"
    date: 2008-01-12
    body: "thx!"
    author: "Koko"
  - subject: "Congratulations!"
    date: 2008-01-11
    body: "Big thanks to all involved and congratulation to users :)"
    author: "m."
  - subject: "looks good."
    date: 2008-01-11
    body: "Congratulations! Thank you for your great work. \n"
    author: "kdeuser"
  - subject: " Thanks and congrats!"
    date: 2008-01-11
    body: "Thanks a lot for all the hard work that all the KDE people have done to make KDE4 a reality!\n\nCongrats on a major archievement!\n\n\nI look forward to trying it out later today."
    author: "Joergen Ramskov"
  - subject: "KDE4Daily"
    date: 2008-01-11
    body: "Just wondering whether or not this excellent service will continue."
    author: "yman"
  - subject: "Re: KDE4Daily"
    date: 2008-01-11
    body: "Hopefully this is answered on the homepage:\n\nhttp://etotheipiplusone.com/kde4daily/docs/kde4daily.html\n\nbut in a nutshell: With the release of 4.0.0, and in light of the existence of OpenSUSE's build service which can satisfy people's curiosity about KDE on the run up to 4.1, the KDE4Daily will not receive any more updates.\n\nI intend to post a quick \"KDE4Daily post-mortem\" to the KDE developer lists to see if they think it has been of benefit to KDE (there have been a handful of \"false\" bug reports arising from KDE4Daily) and to ask whether they would like to see it return in the run up to 4.1.0.  I'll post the outcome on the homepage in due course.\n\nI'd like to take a moment to thank everyone who helped out with the project and, repeating what I said in the notes for the final KDE4Daily revision, making my first contribution to KDE so pleasant :)\n\nCongrats to the devs on the 4.0.0 release!"
    author: "SSJ"
  - subject: "Re: KDE4Daily"
    date: 2008-01-11
    body: "then all I could say is this:\nAAARRRRRR!"
    author: "yman"
  - subject: "Re: KDE4Daily"
    date: 2008-01-11
    body: "Would be great if this continued, I am not going to install KDE4.0 because I do not consider I usable in a productive environment, but I would like keep tracking the progress (hoping that KDE 4.1 or 4.2 on day be ok)"
    author: "Mark"
  - subject: "Congratulations!"
    date: 2008-01-11
    body: "Congratulations to the entire KDE team with the release of KDE 4! I've been trying it since Beta1 and it looks and works great. Keep up the good work!"
    author: "Erwin Boskma"
  - subject: "yahoo!"
    date: 2008-01-11
    body: "They said it couldn't be done .... Congratz guys! Feels very snappy here!\nThanks all your volunteers for making this possible...."
    author: "Gerwin"
  - subject: "Thank you KDE Team!"
    date: 2008-01-11
    body: "Thanks for your hard work, it will pay off! KDE rocks!!"
    author: "Gian"
  - subject: "Wow"
    date: 2008-01-11
    body: "My eyes are having orgasms just looking at these screenshots! Thank you KDE developers, you are all amazing!"
    author: "Sweeny Lover Of Meat Pies"
  - subject: "Re: Wow"
    date: 2008-01-11
    body: "They will have continuous orgasms if you would install the real thing ;)"
    author: "Bobby"
  - subject: "Re: Wow"
    date: 2008-01-11
    body: "and your ears, too ;)"
    author: "Chani"
  - subject: "Re: Wow"
    date: 2008-01-15
    body: "Um...\n\nThose would be eargasms.\n(sometimes the result of aural secks)\n\n\n\n\n\n\n\n\n\nsorry."
    author: "Johanne"
  - subject: "Again, Congratulations!"
    date: 2008-01-11
    body: "I'd like to join the other posters in saying \"Congratulations\" for the release of KDE 4.0! It is a great day for Free Software."
    author: "Luca Beltrame"
  - subject: "Re: Again, Congratulations!"
    date: 2008-01-11
    body: "Thanks so much!!!"
    author: "vincenzo "
  - subject: "good work"
    date: 2008-01-11
    body: "KDE is true! I like it :D"
    author: "Mihail Mezyakov"
  - subject: "A BIG THANK YOU"
    date: 2008-01-11
    body: "Thank you for this sweet release. Sure KDE4 is going to be a great success. I am currently downloading and installing it on my opensuse 10.3 machine. THANKS once again. Great going KDE team"
    author: "Swaroop Shankar"
  - subject: "Application Cleanup"
    date: 2008-01-11
    body: "what about the Application Cleanup (http://wiki.kde.org/tiki-index.php?page=KDE%204%20Application%20Cleanup) ?"
    author: "Luk`"
  - subject: "Re: Application Cleanup"
    date: 2008-01-11
    body: "many of the things on that list got done, including: okular superceding many of the smaller unmaintained apps in its area of expertise; kedit going away and kate going to kdesdk; the games project did an awesome job here going through every single app very effectively; kpersonalizer is gone; kcontrol replaced; rss is now handled by libsyndicate (though kdepim apps still aren't all ported to it afaik); ksysguard moved into the right places (including being made into a library); krdc has been revamped ... etc. there are some things that didn't get addressed, particularly in areas where the turbulence has been in lower levels (e.g. multimedia with phonon, kdepim with akonadi) .. but we hit a significant number of those goals on that page, which is pretty impressive given that some of that stuff was written by people who may not actually be active contributors to the project ;)"
    author: "Aaron J. Seigo"
  - subject: "Wow!!!!"
    date: 2008-01-11
    body: "Look at kde-apps, there's a KDE 4 KPlayer, just released!\n\n/me dances and hugs KDE"
    author: "Wow"
  - subject: "Re: Wow!!!!"
    date: 2008-01-12
    body: "Yes! We now have a full featured KDE 4 media player!"
    author: "yes!"
  - subject: "Re: Wow!!!!"
    date: 2008-01-12
    body: "Unfortunately, it's illegal to distribute.\nhttp://websvn.kde.org/trunk/extragear/multimedia/kplayer/COPYING-WARNING?revision=731711&view=markup\nhttp://websvn.kde.org/trunk/extragear/multimedia/kplayer/COPYING?r1=710647&r2=760442\n\nIf you wonder why it's not being packaged anywhere, that's why. Complain to the author about his asinine licensing. Packagers have tried and he refused to change it."
    author: "Kevin Kofler"
  - subject: "Re: Wow!!!!"
    date: 2008-01-13
    body: "\"Asinine\" is your opinion, which is fine. My humble opinion is that it is Trolltech who should be (and I am sure are) thinking about Qt licensing upgrade in the near future. After all, KPlayer is not the only Qt based project with a licensing conflict due to Qt incompatibility with GPLv3."
    author: "yes!"
  - subject: "Re: Wow!!!!"
    date: 2008-01-13
    body: "Yes, Trolltech should allow the GPLv3 for Qt. But the fact is that they currently don't, so it's silly to license an application which requires Qt under the GPLv3 only."
    author: "Kevin Kofler"
  - subject: "Re: Wow!!!!"
    date: 2008-02-03
    body: "And now they do!! \nhttp://trolltech.com/company/newsroom/announcements/press.2008-01-18.1601592972\nCode and you shall receive :)"
    author: "SomeGuy"
  - subject: "What's the name?"
    date: 2008-01-11
    body: "I just realized that there is no release name mentioned anywhere. Are names just things for Alphas and Betas?"
    author: "liquidat"
  - subject: "Re: What's the name?"
    date: 2008-01-11
    body: "Kuattro? Khetirie? Man, the possibilities of mangling words of ordinals in various languages are just *awesome*."
    author: "Adriaan de Groot"
  - subject: "Re: What's the name?"
    date: 2008-01-12
    body: "How about Katharsis?"
    author: "reihal"
  - subject: "Re: What's the name?"
    date: 2008-01-12
    body: "++"
    author: "Anon"
  - subject: "Re: What's the name?"
    date: 2008-01-16
    body: "Better save it to 4.1."
    author: "reihal"
  - subject: "Re: What's the name?"
    date: 2008-01-11
    body: "The name is the slogan is \"be free.\"."
    author: "sebas"
  - subject: "Re: What's the name?"
    date: 2008-01-11
    body: "\"will eat your children\" ;)"
    author: "Lee"
  - subject: "Re: What's the name?"
    date: 2008-01-13
    body: "\"Kid Krusher\""
    author: "yman"
  - subject: "Re: What's the name?"
    date: 2008-01-14
    body: "This is great :)"
    author: "anonymous"
  - subject: "Hats off"
    date: 2008-01-11
    body: "Hats off big time to everyone who made this possible. I have followed the entire development process through Planet KDE, and am very confident that with all the skilled and talented people out there, KDE 4 will grow to become something seriously awesome.\n\nSo all you guys (coders, translators, writers, PR folks, everyone involved) should definitely crack open a cold one and celebrate, this is your moment and you've earned it! Again, hats off to all of you!"
    author: "bsander"
  - subject: "Compliments!"
    date: 2008-01-11
    body: "my compliments to all the developers!"
    author: "motumboe"
  - subject: "Thank you!"
    date: 2008-01-11
    body: "Very big thanks to all who contributed and especially for the upcoming KDE 4 series. KDE 3 is/was the best desktop out there and I bet KDE 4 will beat it on all ways."
    author: "bheil"
  - subject: "Applause"
    date: 2008-01-11
    body: "Applause to all of you, who made this version of KDE.\nBest regards,\nMiglen Evlogiev"
    author: "Miglen.com"
  - subject: "Congrats!!"
    date: 2008-01-11
    body: "Well done to everyone that worked towards this.  Roll on 4.1.\n\nKubuntu packages are coming down the line now and building 4.1 on Lenny right now!!\n\nWell done once again."
    author: "TheGreatGonzo"
  - subject: "Congratulations"
    date: 2008-01-11
    body: "I am sharing you happiness and I hope KDE will be successfull as the KDE 3 Series now is. But I also hope it will become more configurable as it is now :(. This blessedly is the only big downer i found yet. I have not investigated this for later Versions. \nSo i am looking forward to the see what you guys make out of it.\n\nnice regards"
    author: "Jochen Becker"
  - subject: "When reading..."
    date: 2008-01-11
    body: "... Matthias Ettrichs post about \"starting a linux gui\" at http://groups.google.com/group/de.comp.os.linux.misc/msg/cb4b2d67ffc3ffce\nand then thinking about how big KDE has become I almost start to cry :) This demonstrates the success of visions and opensource!\n\nThanks everyone involved!"
    author: "Linus Berglund"
  - subject: "Re: When reading..."
    date: 2008-01-11
    body: "It was worth to read, thx!!"
    author: "Dolphin-fanatic :)"
  - subject: "Re: When reading..."
    date: 2008-01-11
    body: "this may sound a bit.. odd. but this is one of the documents i read a couple times a year as part of my \"keeping the context\" ritual. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: When reading..."
    date: 2008-01-11
    body: "Nah. I have started to read it quite frequently myself for that reason, although not the kde context. More like to reassure myself that small things can actually lead to something big."
    author: "Linus Berglund"
  - subject: "Re: When reading..."
    date: 2008-01-15
    body: "An interesting read indeed. \"schnurz-purz-widgets\" made my day! ;-)"
    author: "Robert"
  - subject: "It rocks"
    date: 2008-01-11
    body: "Best relase of KDE! It is fast... on my 512 MB RAM system."
    author: "mihas10"
  - subject: "Re: It rocks"
    date: 2008-01-11
    body: "Mine too :)"
    author: "Ian Monroe"
  - subject: "Re: It rocks"
    date: 2008-01-13
    body: "I'm running it on a VM with 256 MB RAM, and it runs great, except for rendering SVGs."
    author: "yman"
  - subject: "Finally!!!"
    date: 2008-01-11
    body: "Damn!! I missed the dot news for few minutes, and the dot is already full of comments.... Finally after waiting for more than 2 years, party time folks!!"
    author: "fred"
  - subject: "KDE rocks!"
    date: 2008-01-11
    body: "Excellent!\nCongratulations to the KDE team for their great job!Fantastic!\n\nCheers!"
    author: "Whoever"
  - subject: "alignment"
    date: 2008-01-11
    body: "Maybe someone can explain why none of the comboboxes in this shot are aligned?\n\nhttp://img186.imageshack.us/my.php?image=comboboxesyk4.png (annotated to show what I mean)\n\nAnd I only pointed out the alignment of the right side.  The left side of the combo boxes doesn't match up either, and neither does the spacing.  Why are Scale and Crop and Color closer together?\n\nIt's just a little thing, and hopefully not indicative of the broader situation but it probably is.  Widgets and doodads here and there (and I'm glad they are there, the alternative would be further feature paring), but laid out rather haphazardly and without that last bit of polish.\n\nThe two color schemes fighting for dominance bothers me too.\n\nDespite that it looks loads better than the betas, and like something I'll consider using around 4.1 or so.  Congratulations on getting it out.  Sitting on it further at this point probably wouldn't have helped.  It's out, being used, critiqued, poked and prodded.  This is a good thing and I'm glad to see things moving forward."
    author: "MamiyaOtaru"
  - subject: "Re: alignment"
    date: 2008-01-11
    body: "that particular dialog was very, very new and didn't get any polishing love for 4.0.0. i mean, look at the checkboxes at the bottom even. surprised you didn't red ink those too ;)\n\nwe're working on this guy, along with the other plasma dialogs, in the 4.1 branch (well .. trunk)"
    author: "Aaron J. Seigo"
  - subject: "Re: alignment"
    date: 2008-01-13
    body: "\"The two color schemes fighting for dominance bothers me too.\"\nI find that they are complementary, rather than clashing. you could just switch the color settings under System Settings->Appearance->Colors\n\nBTW, where did all the color schemes and wallpapers in KDE4DAILY disappear to? there are many of them I liked. are they included in the official release?"
    author: "yman"
  - subject: "Tanks"
    date: 2008-01-11
    body: "A great thank you goes to all the programers making this dream come true... Thank you very much for your hard work..\n\nKeep it up...it's great\n-Flo-"
    author: "-Flo-"
  - subject: "Greetings and Congrats from Africa"
    date: 2008-01-11
    body: "To all you kde developers i wanna say a big thank you and a job well done. I have been following together with friends via the live cd distros and i must say i was impressed with the massive improvements release after release and the final as well. This is the beginning of more great things to come in the 4 series and we look forward with so much enthusiasm. So from all my folks in Accra, Ghana in West Africa we say \"ayeeko\" (job well done) to Aaron and all you other kde developers out there."
    author: "Mark Adubofour"
  - subject: "I LOVE YOU GUYS"
    date: 2008-01-11
    body: "Thank you guys for all the effort putting this together, thank you all!"
    author: "me"
  - subject: "Where do we report bugs?"
    date: 2008-01-11
    body: "Since this is a release with many changes, there are quite a few bugs. Where do we report them. Some of them are KDE specific, some due to the distro. For example, I am unable to run dolphin in KDE4 (just installed from OpenSUSE 10.3 packages). I get some wierd error, and I don't know where to look for help.\n\n(Soprano) Error occured in thread 3056904416 : \"The name org.kde.NepomukServer was not provided by any .service files\"\ndolphin: symbol lookup error: /usr/lib/libnepomuk.so.4: undefined symbol: _ZN7Soprano4Util10DummyModelC1Ev\n<unknown program name>(9165)/: Communication problem with  \"dolphin\" , it probably crashed.\nError message was:  \"org.freedesktop.DBus.Error.NoReply\" : \" \"Message did notreceive a reply (timeout by message bus)\" \""
    author: "last"
  - subject: "Re: Where do we report bugs?"
    date: 2008-01-11
    body: "bugs.kde.org is always the place; that *particular* one is a link error, and is pretty sureley a build / distro error. Check that your nepomuk packages are up to date."
    author: "Adriaan de Groot"
  - subject: "Re: Where do we report bugs?"
    date: 2008-01-11
    body: "And your soprano packages.  Dolphin works on openSUSE 10.3 - you have some mixed up packages.\n\nIf you are in doubt, start YaST and examine all the packages in the KDE:KDE4 repository there; if they are BLUE there is a newer version available - if they are RED they are newer than is in the repo which means you have something unreleased, hacked, self built - if in doubt, replace it with the repo copy.  You can click a package to toggle it between Keep, Update, and Delete, or right click to change all packages in a list."
    author: "Bille"
  - subject: "Re: Where do we report bugs?"
    date: 2008-01-12
    body: "same problem with my installation after clean install with Opensuse 10.3  1-click install for KDE4.\n\nchecked in  Yast that all packages are updated\n\n"
    author: "Volker"
  - subject: "Re: Where do we report bugs?"
    date: 2008-01-12
    body: "I had the same issue and found the bug:\n\n- use Yast and search for libsoprano4\n- update to most recent version\n- restart KDE4\n\nthis fixed it for me"
    author: "Volker"
  - subject: "Re: Where do we report bugs?"
    date: 2008-03-18
    body: "I've got same problem on debian (sid). I updated libsoprano4 package but it not solved this problem\n\n/usr/lib/kde4/bin$ ./dolphin\n(Soprano) Error occured in thread 3056387872 : \"org.freedesktop.DBus.Error.ServiceUnknown - The name org.kde.NepomukServer was not provided by any .service files\"\n(Soprano) Error occured in thread 3056387872 : \"org.freedesktop.DBus.Error.ServiceUnknown - The name org.kde.NepomukServer was not provided by any .service files\"\nQBuffer::seek: Invalid pos: -1209409168\nkbuildsycoca4 running...\nkbuildsycoca4(2212) kdemain: Reusing existing ksycoca\nQBuffer::seek: Invalid pos: 134850024\n<unknown program name>(2208)/: Communication problem with  \"dolphin\" , it probably crashed.\nError message was:  \"org.freedesktop.DBus.Error.NoReply\" : \" \"Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.\" \"\n\nSorry for my english ;)"
    author: "daris"
  - subject: "YEAH!!!"
    date: 2008-01-11
    body: "Thank you guys!!!!!"
    author: "Marcello Anni"
  - subject: "Am I missing something?"
    date: 2008-01-11
    body: "Just installed KDE4.  So...how do you configure the taskbar?  By default it's stuck at the bottom of the screen and it's huge.  Any ideas?  The \"System Settings\" app doesn't seem to have as many options."
    author: "ac"
  - subject: "Re: Am I missing something?"
    date: 2008-01-11
    body: "Unfortunately, the panel config dialogs were something that missed the release, but I'm told there's a lot you can still configure through the rc file.  Not optimal I know, but the config dialogs will appear in the near-ish future.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Am I missing something?"
    date: 2008-01-11
    body: "the plan is actually to be able to avoid \"dialogs\" per say and do something a lot more like the desktop toolbox for the panel. hopefully we'll be able to avoid new strings as much as possible because i'd like to backport this from trunk/ (4.1) to 4.0 along with taskbar improvements (e.g. multiple lines of tasks)"
    author: "Aaron J. Seigo"
  - subject: "Re: Am I missing something?"
    date: 2008-01-11
    body: "As long as it's configurable, I've got what I need...and as long as it's configurable via GUI, I've got what I want.  I'll leave the details up to you!\n\nI feared an \"executive decision\" had been made that a configurable taskbar was no longer needed.  Thanks for the reassurance!"
    author: "ac"
  - subject: "Re: Am I missing something?"
    date: 2008-01-12
    body: "how can you configure taskbar?"
    author: "wizard580"
  - subject: "Re: Am I missing something?"
    date: 2008-01-12
    body: "non-configurable taskbar? oi vey! please don't tell me you think we, or rather i, am that stupid. =) i freely admit that i can only code (and do everything else on my plate) at a certain pace, and that that pace is obviously not as fast as anyone would like (if i did get close to those expectations, the expectations would surely shift ;) ... but i'm not a complete moron.\n\nsorry if you were concerned that this was the case."
    author: "Aaron J. Seigo"
  - subject: "Re: Am I missing something?"
    date: 2008-01-12
    body: "No offense intended.  Gnome voluntarily jumped off that bridge, so I thought there may be some sort of free software desktop deathwish at work.  Glad to know it doesn't affect KDE."
    author: "ac"
  - subject: "Re: Am I missing something?"
    date: 2008-01-13
    body: "Gnome will eventually follow KDE's lead. Just wait and see ;)"
    author: "Bobby"
  - subject: "Re: Am I missing something?"
    date: 2008-01-24
    body: "You can just drag into desired position such as left right etc.\nThere is also another way to do it using >kcontrol\nhope it helps"
    author: "secret agent"
  - subject: "Wow..."
    date: 2008-01-11
    body: "... my small contributions are finally in the wild.  Scary.  :-)  \n\nBig ups to everyone who've put blood, sweat, and tears into this, especially Mr Seigo who's skin thickness must now be of elephantine proportions.  \n\nIt's the end of an incredible journey, but just the start of another that promises much for the future, a future that the people are already working full-speed on.  Geez you'd think they would at least stop for a few moments to catch breath :-)\n\nSo, does anyone have plans for a launch party in the UK?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Wow..."
    date: 2008-01-11
    body: "finally, some part of me is elephantine in proportion! (those who have met me in person will know what that's so funny; for those who haven't: i'm not exactly the biggest person in the world ;)\n\n> So, does anyone have plans for a launch party in the UK?\n\ni don't know.. you should ping the UK kde reps to find out, or maybe tag Riddel about it =) "
    author: "Aaron J. Seigo"
  - subject: "Thanks & congratulations!"
    date: 2008-01-11
    body: "Congratulations! KDE is the best Linux desktop enviroment, I have used it since years in my laptop.\n\nUsing openSUSE 10.3 + KDE 3.5.8 and will try this new release sun.\n\nThank your very much for your big effort."
    author: "xavier"
  - subject: "Some positive reviews from people who get it..."
    date: 2008-01-11
    body: "\nhttp://blog.wired.com/monkeybites/2008/01/kde-4-brings-im.html\n\nhttp://rackit.gartnerwebdev.com/2008/01/11/kde-40-the-grand-unified-desktop/\n\nhttp://arstechnica.com/news.ars/post/20080111-kde-4-0-rough-but-ready-for-action.html"
    author: "Odysseus"
  - subject: "shoukran, ini ouhiboukom"
    date: 2008-01-11
    body: "thanks, i love you,in my mother tongue ;) and really i am very impressed by the new presentation of kde, i love the new vision \n\n"
    author: "mimoune djouallah "
  - subject: "yatikoum esaha"
    date: 2008-01-11
    body: "thanks guys u rock;)thanks from the heart, i could not find a better word then in my own mother tongue"
    author: "djouallah mimoune"
  - subject: "yuck"
    date: 2008-01-11
    body: "reminds me too much of gnome and the dumbed down systems settings is awful.  Think I will go back to 3.5\n\nbooooooooooo for making KDE a piece of crap.  It's funny really, it looks like a monster, a mix of gnome, apple and vista.  KDE no longer is unique.  "
    author: "anon"
  - subject: "Re: yuck"
    date: 2008-01-11
    body: "\"it looks like a monster\"\nyou looked the same when your mother give you a new live. Then in few years you were unique. So give a KDE4.X.Y series a chance and shut up for now"
    author: "Dolphin-fanatic :)"
  - subject: "Re: yuck"
    date: 2008-01-11
    body: "LOL! You're cool."
    author: "Marc"
  - subject: "Re: yuck"
    date: 2008-01-11
    body: "\"you looked the same when your mother give you a new live. Then in few years you were unique. So give a KDE4.X.Y series a chance and shut up for now\"\n\nWELL SAID!"
    author: "Bobby"
  - subject: "Re: yuck"
    date: 2008-01-11
    body: "I would say it's more unique than ever. :)\n\nOh by the way, congratulations everyone! Be free (from your children?! o_o)"
    author: "Hans"
  - subject: "Gnome and dumbing down"
    date: 2008-01-14
    body: "Gnome intentionally hides options and designs with the assumption that users are idiots and don't know how to configure, let alone use their box.\n\nKDE has always been very flexible.  The problem is that so much of KDE 4 is new, that it is difficult to nail down configuration dialogs until the apps and components themselves are nailed down.  Configuration dialogs will come."
    author: "T. J. Brumfield"
  - subject: "well actually.. it's not even that bad!"
    date: 2008-01-11
    body: "after several failed attempts to compile KDE4-win32 from SVN under WiNE during the last couple of month, two days ago I finally emerged the kde-svn ebuilds (thx to berniyh, philantrop, zephyrus...) and have IT (the coolest thing of 2007) runnnig since then. After some initial weirdities (attention: put '<b>export KWIN_NVIDIA_HACK=1</b>' in your /etc/profile or so if you have an nvidia card, it does make a huge difference!) and fiddling I find this thing totally usable and, despite some strange problems within my xorg-server (cpu hogging, memleaks, crashes in composite mode.. and damn, 7.4 will be released not until march) fast and relatively easy on my memory.. So, <b>thanks and congratulations to all the devs and artists</b>, this really is a great start. And this plasma thingie, it's like the first ipod, nice and sleek but missing several customer feedback loops. I'm totally looking forward to the next few months, and I hope I can join in after my exams and probably fix some of my bug reports myself.\nOh yeah, critical topic, bugs... there are quite some. We should have joined reporting and fixing them back in Beta stage. (Right now, no go in reporting: \"bugs.kde.org is temporarily offline to celebrate KDE 4.0.0 launch.\")\nEven a few grave ones have slipped into the final (your grandmother is probably not gonna hit the konsole one *g), so I expect 4.0.1 and so to be released <em>very</em> soon.. \nHot air aside, this is the start of something great. <b>Everyone FEEEEED the baby!</b>"
    author: "Marcel Partap"
  - subject: "doh."
    date: 2008-01-11
    body: "so I guess html tags do not actually work. And I forgot the screenshot.\nMoving right alooong... HURAAAY! JIPPEE ;)"
    author: "Marcel Partap"
  - subject: "Re: well actually.. it's not even that bad!"
    date: 2008-01-11
    body: "\"put '<b>export KWIN_NVIDIA_HACK=1</b>' in your /etc/profile or so if you have an nvidia card, it does make a huge difference!)\"\n\nThat was a great tip man. I just tried it out and the difference is like night and day. The effects are much smoother now .\nThanks :)"
    author: "Bobby"
  - subject: "!!! Amazing !!!"
    date: 2008-01-11
    body: "Amazing people can only produce amazing results!\n\nCongratulations to everyone involved for what an awesome result KDE 4.0 has proved to be!\n\nThank you all!"
    author: "Nassos Kourentas"
  - subject: "thank you"
    date: 2008-01-11
    body: "just tried it out, congratulations!!\n\nthank you!"
    author: "martin"
  - subject: "KDE 4 seems to be alpha quality..."
    date: 2008-01-11
    body: "This might be just me but I have just installed KDE4 onto my mythbuntu system. The results were way below what I expected. Although this may be my fault because I this system is not quite clean.\n\nFirst I had a ton of files on my desktop. When I mouse over these icons the all highlight. The problem is that they highlight and don't un highlight automatically. (I am not using any modifiers - etc shift or control.)\n\nSecond the desktop effects (rotations and window movement is really choppy.\n\nThird half of the customization options from KDE3 are missing.\n\nFourth amarok started twice when I double clicked on an MP3 file. It then crashed.\n\nFifth did I mention this release was incredibly slow.\n\nHopefully this is because I installed KDE4 on a dirty system. (Mythbuntu 7.10 with Gnome installed.) My system also uses a nvidia geforce 7900gs. (with the proprietary driver).\n\nAlso my brother likes Kolf. However since it moved to SVG it is one of the most buggyest games in the world. (try aiming the ball into a corner. and watch it leave the boundries.)\n\nI will be trying to reinstall KDE4 on my system later (from scratch) and retest KDE4. Hopefully all the issues will be fixed. All of the issues that remain will be reported as bugs to the KDE developers.\n\nEven though the system worked pourly for me, I must admit that it looks amazing.\nPS I don't like the icons becoming widgets!"
    author: "Yevgeniy"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-11
    body: "\"Third half of the customization options from KDE3 are missing.\"\n\nFrom the PLasma FAQ (at least for plasma related features):\n\nQ. I can't find my favorite <insert feature here>!\n\nDon't forget that Plasma is still in its infancy (it's brand new, after all) and that KDE 3 was an extremely polished codebase: it took seven years to get to that, while Plasma had about 18 months to get to its current status. With time, the Plasma developers plan on reintroducing features that are missing and fix regressions. As KDE progresses through the KDE4 cycle, Plasma will improve with it. \n\n(I've been posting it everywhere today...)"
    author: "Luca Beltrame"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-11
    body: "alpha for sure, or whatever comes before that!\n\nI have a fast system, and simply rotating the digital clock plasma applet is choppy. With all this talk about SVG i'm surprised that i see some raster trickery when scaling and rotating, is this going to be changed? Also, i see many places where icons are missing, is there somewhere i can find a list of ones still needing updating so i can contribute? Am i missing some setting for the anti alising of plasma applets? when i rotate them they look jagged and remain that way until i enter settings and click apply. I changed the font and the text didnt center itself, clicking the \"etc\" popped up the calendar half off the screen unmoveable and without the edge transparency. Also... when moving the mouse over different applets that grey box appears differently, on one applet it fades in and you can see the three icons fall down a pixel or two. on another applet it simply just appears and with no icon movement, why is this different? why cant i draw a bounding box (which is also jumpy when it's big!!) around the applets to select them or move in groups? this is basic functionality. sometimes when moving the digital clock the black background disappears and in it's place is a piece of the wallpaper..\n\nalso.. in dolphin when starting things the bouncing k symbol erases everything it touches on screen..\n\ni notice also that there does not appear to be any standard to icon sizes which makes everything look cheap, consistency is key and would simplify things i think..\n\n\ni think i might just wait for 4.1 or later...\n\nps. id take a screenshot but it'll have to stop crashing first!\n\nthe dream has become reality? more like a nightmare"
    author: "mike"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-12
    body: "4.0.0 is for developer and hard core users.. not for anybody!\n\nWait for 4.1 if you search for an kde 3.5 replacement!"
    author: "Stephan"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-12
    body: "ha, i'm anticipating 3.6 more than anything else."
    author: "mike"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-13
    body: "So what was the bloody point in calling it an OFFICIAL release? Why wasn't it just called a beta? The frustrating thing is that right now I REALLY don't like KDE4 and the funny thing is that I don't even know when to come back and check it out again because KDE4 is a so-called official release, which by definition should be more or less flawless or at least fully usable. "
    author: "mkrs"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-13
    body: "Mac OS X 10.0, GNOME 2.0, KDE2.0, KDE3.0 were all \"official releases\", and they all thoroughly *sucked*.  People are either forgetting their history or are holding KDE4.0 to a grossly unfair double-standard."
    author: "Anon"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-14
    body: "Don't forget Windows 1, 2, 3.0, NT 1, 2, 3, and early versions of 4.  Windows ME, XP pre-SP1, and Vista.  All pretty horrible.  Honestly, Windows 2000 and Server 2003 were probably the best out-of-box, initial official releases they did."
    author: "T. J. Brumfield"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-14
    body: "Don't forget Apache 2.0 and Linux 2.6.0 here ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-14
    body: "Alright... But comparing KDE to another KDE only proves that the same mistake has been done more than once. It's no excuse, it's rather a proof of a principle which in my opinion is very wrong. \n\nWhen it comes to MacOS X... Well, a lot of things have changed in Apple since then and I think they've learned a lesson that a brand new product should be shiny and beautiful if you want to sell it, no matter what product you are talking about: a player, a phone or an OS. I do not think that official MacOS 11.0 will be as unusable as official KDE 4.0 is now. The problem is that KDE team have generated a buzz around KDE4 but - contrary to Apple - they did not manage to release a product as good as they said it would be.\n\nGNOME? I don't know, I'm not a GNOME guy, so I can't say anything about that.\n\nAnyway, I think it's partly a KDE devel team's fault - they are the ones who boasted about the new KDE's improvements, speed, ease of use etc. If you are not ready to release a REALLY good product on time, you should either wait or stop boasting about how great things you have done while developing a new program because disappointing people always brings worse effects than giving them an average product."
    author: "mkrs"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-12
    body: "> The problem is that they highlight and don't un highlight automatically.\n\nalready fixed in 4.0.1\n\n> Second the desktop effects (rotations and window movement is really choppy.\n\nthose are actually not the same app coming into play. in either case, this is composite making life miserable for you. if you're using nvidia drivers, make sure you're using the latest from the end of december and use the nvidia hack envvar mentioned earlier in the comments on this story. otherwise, wait for kwin's compositing to mature or just turn it off\n\n> Third half of the customization options from KDE3 are missing.\n\ni call bullshit on this one. if you mean \"half the customizations from kicker\", yes. if you mean \"half the customizations from kdesktop\" your probably not right (haven't counted exactly, but i'm pretty sure plasma is currently beyond its feature count). if you mean \"half the customizations of all kde together\" you're wildly wrong as there are new features all over the place.\n\nso... kicker features: coming.\n\nin the meantime, you may enjoy life more by keeping some perspective.\n\n> Fourth amarok started twice when I double clicked on an MP3 file.\n> It then crashed.\n\namarok2? if so, you do realize that amarok is not release with the kde libs/base/extras packages and that amarok2 is currently pre-alpha?\n\nas for starting twice, that's also been fixed in svn (you have to turn on double click, of course)\n\n> Fifth did I mention this release was incredibly slow.\n\nyou ran out of things at four? ;)\n\n> PS I don't like the icons becoming widgets!\n\nyou may find that if you stop fighting the tools that you'll find ways to work better. note that i didn't say \"make the software work better\" but *you* can work better. now, we've only got a few of the new user visible tools in plasma ready for 4.0, in part because we spent so much time on the traditional components to keep people such as yourself as content as possible in the time we had, but as those come out you'll start to get a much better idea of why the icons being widgets is actually a really smart idea.\n\nno, i'm not going to explain it here. i'd probably do a poor job of it, it's not a trivial topic and i really don't feel like arguing about it with random passers-by either =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 4 seems to be alpha quality..."
    date: 2008-01-12
    body: "\"but as those come out you'll start to get a much better idea of why the icons being widgets is actually a really smart idea\"\n\nAlready it is a pretty good idea because one can easily change the sizes of each individual icon without going to system settings, they can be rotated and closed instead of being deleted. \nIf one locks the widgets then they will look like normal icons :)\n\nFor people with performance problems, it's more of a composite problem. If the effects or turned off or the nVidia hack is applied then the world will change instantly like you mentioned.\nAaron you and the rest of the team are doing a fine job. The vision is unfolding and I can see the light :)"
    author: "Bobby"
  - subject: "Parley"
    date: 2008-01-11
    body: "I would like to thank Frederik Gladhorn and the KDE Edu team for their outstanding work! Frederik has ported or actually rewritte KVocTrain and has put in an incredible amount of work.\nParley is an excellent vocabulary (and partly grammar) trainer and will be even better with the new features implemented in KDE 4.1! A BIG thanks!!!"
    author: "Markus B\u00fcchele"
  - subject: "glad to see Konsole is back!"
    date: 2008-01-11
    body: "Did anyone notice the screenshot at the top?  Konsole now has its rightful place back on Kicker!!"
    author: "KDE User"
  - subject: "Re: glad to see Konsole is back!"
    date: 2008-01-11
    body: "GO gnome"
    author: "Pavel os"
  - subject: "Re: glad to see Konsole is back!"
    date: 2008-01-11
    body: "Sorry to disappoint you, but\n1. Kicker is dead in the 4 series\n2. The screenshot is showing Konsole in the taskbar."
    author: "Hans"
  - subject: "Re: glad to see Konsole is back!"
    date: 2008-01-12
    body: "you can add any program to panel with right click in menu.."
    author: "Stephan"
  - subject: "Congratulations!"
    date: 2008-01-12
    body: "You've all done a terrific job! Ale or lager?"
    author: "Remulus"
  - subject: "Re: Congratulations!"
    date: 2008-01-12
    body: "scotch! =)"
    author: "Aaron J. Seigo"
  - subject: "congrats + randr 1.2"
    date: 2008-01-12
    body: "Thanks to all the KDE developers for this release! I am a KDE user since 0.x and believe KDE 4.0 is a good base for the future.\n\nDoes anyone managed to get a dual head setup running with the new 'intel' X driver and randr 1.2? When I try to add a second screen next to the existing one with the xrandr command line tool, strange things happen (white screens etc)."
    author: "Bram"
  - subject: "Re: congrats + randr 1.2"
    date: 2008-01-12
    body: "yeah, xrander 1.2 does some brilliantly *stupid* things with regards to how it reports via xinerama API. apparently the x.org developers don't understand how implementation details, while useful for configuration tools and the implementation itself, is completely useless and even dangerous to expose to applications using your API. with xrandr 1.2 they've put out a bunch of implementation detail related information to do with screens via the xinerama API, resulting in what really amounts to a practical behaviour change in the API.\n\nit was a remarkably short sighted thing to do, and when i brought it up with the responsible parties very nicely with detailed reasons why ... they replied with \"well, we like it that way. i hope you can get your app to work with it.\" that's nearly a word-for-word quote.\n\nwe'll probably end up working around it in QDesktopWidget, but i still can't believe they thought it was a good idea in the first place. \n\nyes, i'm looking at you keithp."
    author: "Aaron J. Seigo"
  - subject: "Re: congrats + randr 1.2"
    date: 2008-01-14
    body: "Eh, it works for other WMs (and I have been using it with xmonad for months now, without much ado -- it actually works great). I think some other people use it with Flux and possibly others and I haven't heard anyone complaining so far. But then, I haven't looked under the hood. From the user standpoint, it works well enough for me not to have the need to get nosy."
    author: "mornfall"
  - subject: "Re: congrats + randr 1.2"
    date: 2008-01-15
    body: "yups, randr 1.2 even works pretty well in the latest KDE 3.5 releases so their should be a way to fix it. The functionality in randr 1.2 is a huge improvement for users (eg connecting a beamer etc). I guess it just takes some time before KDE is fully compatible with randr 1.2. A configuration utility is also missing.\n\n(thanks for the numerous interesting answers on this website, Aaron)"
    author: "Bram"
  - subject: "Re: congrats + randr 1.2"
    date: 2008-01-15
    body: "Using Qt has spoiled you :)\n\nNot everyone can design APIs, and maybe there are others that really liked it.  I don't know.  The question is if it *can* be used properly or that it really requires a hack.\nIf the latter; you might get them to change behavior.  But your public dissing of this guy might not have bought you cookies ;)"
    author: "Thomas Zander"
  - subject: "Congratulations KDE Team"
    date: 2008-01-12
    body: "I'd like to contragulate KDE Development Team, programmers, designers, documenters, testers, everyone of them, for this release; I believe that every new KDE release is 'the most important', because it shows to the world how open source can be efficient, beautiful, socially responsable, communitarially sustentable; a happy 2008 for every of you with big advance in KDE 4 development!"
    author: "Saulo"
  - subject: "Yes, great. But now I am waiting for KDE 5.0"
    date: 2008-01-12
    body: "It will be the most impressive and amazing release ever done.\n\nCongratulations for all KDE team."
    author: "Confidence guy"
  - subject: "To Matthias Ettrich"
    date: 2008-01-12
    body: "Sir, \n\nWe would like to hear from you. \n\n\n\n\n \n\n\n\n\n"
    author: "Oh Calcutta"
  - subject: "Configurability?"
    date: 2008-01-12
    body: "I downloaded the Kunbuntu live CD image. I guess my first comment is that this took forever @ 40 kb/s; surprisingly there was no listing under the KTorrent search engine for a torrent.\n\nI tested this on a computer with a serial mouse, which, while more Kubuntu's fault than KDE's, the lack of being able to do anything until I pulled the USB mouse out of my main computer and plugged it into the test computer was not an auspicious start. Ya, that's right - a serial mouse wasn't supported \"out of the box\".\n\nI found the launch menu irritating, especially the arrow system. And why is it so short yet so wide? I mean there was a scroll bar in it in the default layout. That's ridiculous. I haven't yet figured out how to configure that sucker, but it needs an overhaul to make it useable.\n\nDolphin seems fine once it is configured and customized, but the default is next to unusable. I had to change just about every setting to something else. Even basic things like file attributes under detail view and the lack of the up button in the default set up. And where are my tabs?\n\nHas Konqueror been stripped of its search bar or is that a Kubuntu thing?\n\nAbout this time I noticed that overall KDE 4 is a lot faster than KDE 3.x, even though it was running from a CD! That I have to say is impressive.\n\nNext I decided to do something about the clock in the tray. What happened to the configuration options? Where's the option to show seconds? Either leave the option in place or take the setting from the system date/time preferences where seconds are set but removeable. And the date - I want to see the numeric date, not the short format date. Even the handy little ability of being able to copy the date/time to the clipboard has been stripped out.\n\nI hope the clock isn't indicative of an overall change in direction towards the GNOME model of stripping out customization options as that's one of the many things about KDE that makes it truly unique and usable desktop. It would be a disappointment if this advantage is just given up."
    author: "David James"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: ">I found the launch menu irritating, especially the arrow system. And why is it >so short yet so wide? I mean there was a scroll bar in it in the default >layout. That's ridiculous. I haven't yet figured out how to configure that >sucker, but it needs an overhaul to make it useable.\n\nThe new menu has been critisized to the extent where it will be removed for sure.\n\n>Has Konqueror been stripped of its search bar or is that a Kubuntu thing?\n\nI think the search bar was in a separate package (kdeextras of something.) so yes, it is a Kubuntu thing.\n\n>Dolphin seems fine once it is configured and customized, but the default is >next to unusable. I had to change just about every setting to something else. >Even basic things like file attributes under detail view and the lack of the >up button in the default set up. And where are my tabs?\n \nWhat did you need to change. I'll think of some possibilities I'm sure were some:\n1. No \"up\" button:\n    This was replaced by the \"breadcrumbs\" system. To go up, just click the button next to your current directory.\n2. \"Breadcrumbs are crumby\":\n    To each it's own. \n"
    author: "Michael"
  - subject: "Re: Configurability?"
    date: 2008-01-13
    body: "> What did you need to change. I'll think of some possibilities I'm sure were >some:\n> 1. No \"up\" button:\n> This was replaced by the \"breadcrumbs\" system. To go up, just click the >button next to your current directory.\n> 2. \"Breadcrumbs are crumby\":\n> To each it's own.\n\nSince I tested it on a live CD, I can't recall exactly, but offhand they included the lack of up and refresh buttons (need this for remote filesystems), changing to a details view, showing hidden files, showing file attributes (this ought to be in the settings or otherwise handled differently since I had to cycle through the menus a few times to get them all...), adding the \"Places\" panel (iirc) so I could get a tree view back, showing the location bar and making that editable (?!), showing the file preview pane and finally showing the console.\n\nI might have missed one or two :)"
    author: "David James"
  - subject: "Re: Configurability?"
    date: 2008-01-13
    body: "> What did you need to change. I'll think of some possibilities I'm sure were >some:\n> 1. No \"up\" button:\n> This was replaced by the \"breadcrumbs\" system. To go up, just click the >button next to your current directory.\n> 2. \"Breadcrumbs are crumby\":\n> To each it's own.\n\nSince I tested it on a live CD, I can't recall exactly, but offhand they included the lack of up and refresh buttons (need this for remote filesystems), changing to a details view, showing hidden files, showing file attributes (this ought to be in the settings or otherwise handled differently since I had to cycle through the menus a few times to get them all...), adding the \"Places\" panel (iirc) so I could get a tree view back, showing the location bar and making that editable (?!), showing the file preview pane and finally showing the console.\n\nI might have missed one or two :)"
    author: "David James"
  - subject: "PLEASE REPLY TO ABOVE POST"
    date: 2008-01-13
    body: "I missed out a letter in my email address on the first reply - so if you reply please reply to the second one to prevent some other person from getting email messages :)\n\nSorry all.\n\nDavid"
    author: "David James"
  - subject: "Re: Configurability?"
    date: 2008-01-13
    body: "\"showing the location bar and making that editable\"\n\nthis exists by default I believe. if you put the mouse pointer over an empty space in the location bar, a vertical black line should appear. click, and it will become editable. in regards to breadcrumb navigation: it's similar to Vista, but way better. if you ever used Vista or GNOME you'll know how to navigate up any number of directories you want at once by clicking on a directory name. you'd also know (if you used Vista, although KDE does this better) that you can click on the arrow near the directory name to list all non-dot-file directories (or whatever they're called) in a drop-down list, and navigate to them by clicking on their entry in the list. try it.\n\nto go back to breadcrumb navigation from text navigation in the address bar, click the yellow button to it's right."
    author: "yman"
  - subject: "Re: Configurability?"
    date: 2008-01-13
    body: "> This was replaced by the \"breadcrumbs\" system. To go up, just click the \n> button next to your current directory.\n\nDUH! :-)  This is simply wrong.  \"BreadCrumbs\" can be used instead of the \"Back\" button.  Yes, there are times when the \"Up\" button and the \"Back\" button have the same result, but that does not mean that they are the same.  The \"Up\" button will still go up when there are NO \"BreadCrumbs\".\n\nIIRC, discussion reached the conclusion that the \"Up\" button should be there in the Konqueror \"FileManager\" profile.  So, it should be in Dolphin as default.  Doesn't much matter to me since the toolbars are easy to configure -- a feature that I use a lot."
    author: "JRT"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: "> why is it so short yet so wide\n\nthis is configurable, and will be freely resizable for 4.1\n\n> configure that sucker\n\nright click on it.\n\n> to make it useable\n\ni'll bet you'd prefer the simple launcher instead. go for it =)\n\n> Dolphin\n\nyou probably aren't the target audience for dolphin. that's why we also have konqueror.\n\n> Either leave the option in place or take the setting from the system\n> date/time preferences where seconds are set but removeable. And the date - I\n> want to see the numeric date, not the short format date. Even the handy\n> little ability of being able to copy the date/time to the clipboard has been\n> stripped out.\n\nyour operating under a very mistaken idea here that the clock was ported. it's not. nothing on the desktop was ported. it's a straight up re-write. consider that kicker is the result of 7 years of work. a good amount of that work was mine, so i'm pretty aware of what it took to get all those features in there. we'll get all the little creature comforts back (and faster if others contribute patches, which many are doing!) and provide a hell of a lot more than was ever realistically possible with kdesktop/kicker.\n\nbut yeah... if you can't manage for a while without being able to copy/paste the time from the clock then i suggest that kde 4.0 is too bleeding edge for your needs and check back later, perhaps in 4.1.\n\nat that point you may wish to ask yourself if you'd rather have waited for the little things to finish out or if you'd rather have been stuck with the kicker/kdesktop concepts instead.\n\n> KDE 4 is a lot faster than KDE 3.x\n\nand to think that this is a first release. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: "> ... nothing on the desktop was ported. it's a straight up re-write. consider \n> that kicker is the result of 7 years of work.\n\nI am at a loss here to understand why 7 years of work were simply thrown out.  Is there no consideration at all given to the reuse of code?  That is supposed to be one of the advantages of an OO language.\n\n> at that point you may wish to ask yourself if you'd rather have waited for  \n> the little things to finish out or if you'd rather have been stuck with the \n> kicker/kdesktop concepts instead.\n\nI can see nothing wrong with the kicker/desktop concept.  IAC, what concerns a user is what he can do with what is presented to them and the current desktop and panel lack many of the features that are present in KDE3.  Perhaps the developers don't understand this, pointing out instead how good the new code is -- this doesn't really matter to users!\n\nYes, I know that there were serious problems in the KDE code for Kicker and KDesktop, but it did what the user wanted and you only ran into the bugs when you tried to configure some of it.  IIRC, AJS told me that he wasn't going to fix bugs that I had reported because he was going to write the new Plasma desktop. :-(\n\nThe only thing that I see new is the widgets -- a feature that I would use only for a clock and then only if there was a way to bring it to the top of the stack of X windows (like a panel in KDE3.4).  With perfect hind site, perhaps it would have been better to implement the existing features first and then add new ones.\n\n"
    author: "JRT"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: "I really don't understand how the people can bring on the same arguments over and over again.\n\nFirst. Aaron maintains/maintained the kicker code. So he should really know it better, if a rewrite was neccessary. He laid his arguments out very well in his blog, maybe you should read it!\n\nBtw. have ever made a bug report concerning your problem with the kicker? If not, you should really think If you are in the position to criticize Aaron...\n\nSecond. Code will get messed up with time, no matter what language you use! Every programmer knows this. Sometimes you need to make clean cut, and use the new knowledge you made over time to start a new, clean and sustainable codebase.\n\nThird and last. They are doing this in their free time. so please be thankful for what you got, even if it's not perfect. Be a little mor patient, you are not perfect either!\n\n"
    author: "James Hancock"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: "> I really don't understand how the people can bring on the same arguments over \n> and over again.\n\nThat is just rhetoric, it means nothing.  I was answering somebody else and I still have the same answere that I had a month ago, but he brought the subject up again.  The fact that I, or someone else, may have said it before does not mean that it isn't valid.\n\n> So he should really know it better, if a rewrite was neccessary.\n\nI am still saying with hindsight that he may have made an error.  Perhaps the job was simply bigger than he thought which is OK, but the finished code is late.\n\n> Btw. have ever made a bug report concerning your problem with the kicker?\n\nDidn't I say I did?  http://bugs.kde.org/show_bug.cgi?id=125894\n\nAlso a few more: 47627, 117868, 115468, and according to bug 132349 many more related bugs.  Many of these have been marked as FIXED but the basic problem remains.  So, I'm not sure if they have actually been fixed.\n\n> Second. Code will get messed up with time, no matter what language you use! \n> Every programmer knows this.\n\nIf you are saying that hacking results in poorly maintained code, I have to agree with you.  However, well maintained code should be reusable.  Anyone that understands the rational for OO language knows that in theory they are eaisler to maintain than procedural languages.\n\nI find your \"Third\" comment irrelevant."
    author: "JRT"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: ">I am at a loss here to understand why 7 years of work were simply thrown out. Is there no >consideration at all given to the reuse of code? That is supposed to be one of the advantages >of an OO language.\n\nIn short Kicker and Kdesktop were reaching their limits, it was designed before compositing, when no one had thought of Desktop Widgets and possibly before panel applets, you can add this stuff to the existing code but sooner or later it becomes obvious (to the programmers) that your trying to attach stuff that the code wasn't designed to handel and it gets harder and harder to include new features. \n\nUltimately you have two choices, keep the 7 year old code and you have a highly functional mature Desktop/Panel setup but you pretty much can't make any major improvements, or start fresh designing from the ground up with 7 years worth of practice and the new codebase will be designed to handle all the new concepts that were just bolted on to the old codebase, and then you have the ability to include evey good new idea that emerges in the next however many years until it becomes obvious that Plasma's visual based desktop was never designed to handle wi-psi and spinal-cord ethernet control*, then the future KDE devs get a lot of insults for deciding to start fresh rather than continue trying to bolt more onto plasma. \n\nI don't blame them for taking the long term view, in a few years no one will remember the problems KDE4 had in its 4.0 days, but we'll all be enjoying the improvement's added to plasma that couldn't have been done on Kicker/KDesktop. \n\n\n* Given what happened to Kicker/KDesktop I imagine extra care has been given to makeing plasma can be updated for as long as possible before a replacement is needed (has it?) but I think I'm safe in saying it will be showing its age when Mind/Computer connections are replacing eyes and hands. "
    author: "Ben"
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: ">  in a few years no one will remember the problems KDE4 had in its 4.0 days\nFive will get you three that will happen in about six months. Things are only going to get better from here on."
    author: "ne..."
  - subject: "Re: Configurability?"
    date: 2008-01-12
    body: "\"Yes, I know that there were serious problems in the KDE code for Kicker and KDesktop\"\n\nAnd that was one of the main reasons for a rewrite like Aaron has explained so many times. He said that cleaning up the old code was almost impossible and would only consume a lot of precious time that one could invest in starting from scratch (which would have to be done sooner or later anyway) and making KDE more future-proof. Another reason was QT4.\nI am 100% sure that it was a good decision and that we will all benefit in the end. Look at OSX for example. It didn't start out perfectly but now it's shining :)"
    author: "Bobby"
  - subject: "Re: Configurability?"
    date: 2008-01-14
    body: "BTW. another example is KBabel/KAider.\n\nKBabel (ported to Qt4) is 50 000 lines of code, while KAider is 11000. (+2000 for xliff in the near future), and KAider outperforms KBabel in terms of features, performance, and every-day usability (still way to go for first impressions)."
    author: "Nick Shaforostoff"
  - subject: "oh man...."
    date: 2008-01-12
    body: "Please don't think that I feel mad and disapprove KDE developers work and everything, I know these things happen and that they put there best work to this and make this at their free time so all of us can enjoy. So yeah, I'll be patient cause I know they will get a great product, they have proven to be good at what they do.\n\nHope to see a better kde in the next update. I know this is the first 'stable' version and almost always with the first versions there are lots of problems.\n\nI don't know, all in all this looks like a temporal reverse in kde, I think this should have been marked as Beta3. But oh well, with all the bugs people are going to open they are going to improve very fast and kick the butt of every Window manager out there because this looks very promising once the bugs are fixed!\n\nHere are my problems  (Maybe this happens because I still have kde3 installed??):\n\nFist of all, You need a MEGA PC to get things working fast (I have an AMD 64 2.2GHz, 1.5 GB RAM, and an ATI x1650 with 256MB maybe the ATI is the problem, cause the drivers suck BIG time). I can't listen to my music and navigate at the same time, IT IS SO SLOW!, I can just do one thing at a time, forget about multitasking! Hell, even the mouse moves with lots of jumps.\n\nThe biggest problem I see is that the kde configuration is to basic, I would of like something like previous versions. I would of like to see something similar to kcontrol (Maybe I can't find it, can someone tell me if there is one?)\n\nThe panel has a huge bug, It has disappear and can't get it back. So I'm currently unable to use KDE4, my experience lasted like 30 min. before this happened T_T. ii also looked around for an option to hide the panel automatically, but to my surprise, didn't find it anywhere, you can't even right click the panel, it feels like they chained down the configs.  \n\nThe start menu, is horrible, to difficult to navigate and can't make it bigger. Would love to see an option to let it like it was in KDE 3.x\n\nCan't sort the icons on my desktop by type, and there is also a Bug, if you put your mouse over an icon and then take it off, sometimes the \"square\" with options won't go away, and if you sort your icons with that \"square\" on, it gets all messy.\n\nKonquest, the game, is a big NO! this game was great, now it isn't. I liked how the properties of the planets were shown at the left, AND there is also a bug, even if you select to play against a computer player, it plays like a human.\n\nGet a crash when trying to open most of the apps.\n\nOk, that was my short experience before the panel disappeared. Maybe all this happens because I still have KDE 3?\n\nEven if what I'm going to type next sounds ironic, It isn't, it's the truth, [cause I know it is really hard to write a program, If I knew how to program better (just know basic C and Assembly, not even C++ T_T) I would help this great community. Unfortunately I only can fill up bug reports T_T and seems like I'm only complaining about things.]\n\nKeep up the great work!!! Keep kicking butts like you've always done! =D\nSerpentus"
    author: "Serpentus"
  - subject: "Re: oh man...."
    date: 2008-01-12
    body: ">> The biggest problem I see is that the kde configuration is to basic,\n\nThis has been explained many times. KDE is now following Gnome's footsteps and is 'dumbing down' the system.\n\nSorry, couldn't resist. Serious answer: This is the 0.0 version as you said. There are still porting to do, and there are also new stuff like Plasma. The Kicker you saw in 3.5.x had been developed several years, give Plasma some time to mature and you'll get your options.\n\n>> I would of like to see something similar to kcontrol (Maybe I can't find it, can someone tell me if there is one?)\n\nSystem Settings\n\n>> The panel has a huge bug, It has disappear and can't get it back. So I'm currently unable to use KDE4, my experience lasted like 30 min. before this happened T_T. ii also looked around for an option to hide the panel automatically, but to my surprise, didn't find it anywhere, you can't even right click the panel, it feels like they chained down the configs.\n\nDelete the config file (somewhere in ~/.kde4 I guess) and you'll get it back.\nThere's no gui to configure it at the moment.\n\n>> Would love to see an option to let it like it was in KDE 3.x\n\nThere is. Dunno if it's still in playground though - see if you have it (in the add widgets dialog)\n\n>> Can't sort the icons on my desktop by type\n\nI think you have to wait for later releases for this feature (and Qt 4.4)\n\n>> Get a crash when trying to open most of the apps.\n\nNot very useful. If you can get a backtrace, post on bugs.kde.org.\n\n>> Maybe all this happens because I still have KDE 3?\n\nNope, don't think so.\n\n>> Unfortunately I only can fill up bug reports T_T and seems like I'm only complaining about things.\n\nSame to me, but we all try hard to contribute, don't we? =)\n\nAnd don't worry about the complaining part, the developers are smart enough to see the difference between constructive criticism and pure trolling - otherwise we wouldn't be here today and celebrating. Cheers to you all!"
    author: "Hans"
  - subject: "Great work everyone"
    date: 2008-01-12
    body: "(so far at least).\n\nLeast there be any misunderstanding, I wanted to say that the developers have done great work (in most cases -- nothing is 100%) on KDE4.  Major (and much needed) work has been done on the multimedia and hardware interfaces, and the libraries have been updated to make use of new features of Qt4.  Some work has been done on Konqueror, but some of it needs considerably more (you can't do everything at once and we shouldn't try). Dolphin has become a mature application.  Etc.\n\nIt is unfortunate that KDEPim didn't make it into the release.  But you will get no argument with me that software shouldn't be release till it is ready (as Linus says).  And, we seem to be missing a multimedia application -- Kaffeine is fine but not Qt4 and not shipped with KDE4.\n\nMy only complaint is that this release was called 4.0.0.  If it had been called the public Beta instead, I would have no problem except for the unfinished desktop; we need to acknowledge that the Plasma project is late -- something that isn't a new problem in the software industry.  So, I suggest that those that complain should consider it at that before them complain.\n\nI will continue to complain about things that don't work -- I do so because I want them fixed.  But, please don't just complain: there are bugs to be reported and I hope that everyone will take the time to report them -- and please keep track of your bug after you report it.\n\nI do have to say that I wonder why applications are thrown away and replaced with new ones.  This seems to be a waste of a lot of code."
    author: "JRT"
  - subject: "Re: Great work everyone"
    date: 2008-01-12
    body: "Kaffeine has never been released with KDE, so no reason to expect it to get shipped with kde 4.0\n\n"
    author: "whatever noticed"
  - subject: "Re: Great work everyone"
    date: 2008-01-12
    body: "Also, kplayer's 0.7 release, which came out today, is build on kde4.\nSo you do have a multimediaplayer for kde4."
    author: "whatever noticed"
  - subject: "Re: Great work everyone"
    date: 2008-01-12
    body: "I missed that -- well you did say that it was today -- so I will give it a try though I do hope that Kaffeine is ported to Qt4 soon."
    author: "JRT"
  - subject: "Re: Great work everyone"
    date: 2008-01-12
    body: "And people accuse me of being a nitpicker (which I like to call attention to detail).  \n\nThe major point is that KDE4 does not include a multimedia player.  I understand that there is a dragon lurking somewhere, but haven't seen it yet.\n\nWill a multimedia player be added to KDE4.x?"
    author: "JRT"
  - subject: "Re: Great work everyone"
    date: 2008-01-13
    body: ">The major point is that KDE4 does not include a multimedia player.\n\nThere is one statement a nitpicker can do much with. Actually the state of multimedia in this regard has not changed much from KDE 3.x. \n\nYou still get Kscd  and Juk, which is a very good music manager/player. Compared to KDE 3.x, you are missing Noatun and Kaboodle. \n\nKaboodle is a sad loss, since it was perfect  for it's intendant use case to simply play a file  when asked. At the moment there is nothing to replace it, but I think Phonon will make such an application simple to create. Adapting Dragon Player for the use case could also be a possibility, as it's predecessor Codeine functionality wise was Kaboodles brother for video files.  \n\nIt must be Noatuns absence that is the lack of multimedia player you refer to. As a music manager/player Noatuns handling of playlists was different from Juk, and it plugin infrastructure was rather flexible and unique. But it's essentially duplication in the music player applications. But Noatun was also able to play some videos, the only application in KDE 3 able to do so(Kaboodle also did at some point I think). A little odd complaint, since in the later releases of KDE 3 it had increasing problems with playing videos. Mostly related to distributions set ups. And most distributions even stopped installing it as default, choosing other applications as default music players.\n\n\n>Will a multimedia player be added to KDE4.x?\n\nAs said, you already have Juk. Dragon Player(Codeine) is ported to KDE 4, and should have a stable release soon. The developer has also given indications he  want it included in kdemultimedia, making it an official part of KDE.\n\nBesides 3rd party KDE applications like Amarok, KPlayer, KMPlayer and Kaffeine are all in the process of being ported to KDE 4 and they will have releases in due time. \n\n"
    author: "Morty"
  - subject: "Please provide howto/help for nvidia users"
    date: 2008-01-12
    body: "Please provide an official howto or something for kde4 users with nvidia cards - there should be a lot of them around. I read things like \"do this\" and \"tweak that\" to make accellerated effects smoother, but for me it just doesn't work.\n\nI have the latest driver and a Geforce6600GT, and i read to this point:\n\n- add \"export KWIN_NVIDIA_HACK=1\" to /etc/profile\n\n- add\nOption \"AddARGBVisuals\"\t\"True\"\n        Option \"AddARGBGLXVisuals\" \"True\"\n        Option \"DynamicTwinView\" \"false\"\n\tOption \"RenderAccel\" \"true\"\n\tOption \"AllowGLXWithComposite\" \"true\"\n...to xorg.conf in section \"Screen\" and/or \"Device\"\n\n- add / don't add\n  Section \"Extensions\"\n\tOption\t\t\"RENDER\"\t\"true\"\n\tOption\t\t\"DAMAGE\"\t\"true\"\n\tOption\t\t\"Composite\"\t\"Enable\"\n  EndSection\n...to xorg.conf\n\nWithout effects, all is smooth. With effects the Desktop Grid is smooth, but minimizing/maximizing, scaling in, moving and rotating of widgets and the overall impression is choppy.\n\nSearching google and testing becomes really frustrating when you get no results, so pleease..."
    author: "Marc"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-12
    body: "Yes, i strongly support that... i have a gf7800gs and i don't know what to do anymore."
    author: "Lemmo"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-12
    body: "Hi there!\nI have a GeForce Go 7200. For me kwin composite works very well - everything runs smooth and I had no crashes so far.\nI'm using Debian unstable with the KDE4.0 packages from experimental.\nNvidia driver version: 169.07-1\n\nMy device section from xorg.conf:\n\nSection \"Device\"\n        Identifier      \"GeForce Go 7200\"\n        Driver          \"nvidia\"\n        BusID           \"PCI:5:0:0\"\n\n        Option          \"NoLogo\"                \"1\"\n        Option          \"RenderAccel\"           \"1\"\n        Option          \"AllowGLXWithComposite\" \"1\"\n        Option          \"AddARGBGLXVisuals\"     \"1\"\nEndSection\n\nno other config files edited!"
    author: "Sepp"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-13
    body: "A HowTo for users with nVidia cads is most urgent. I have used a few tips here including yours for my xorg.conf file and KDE 4.0 is now as smooth as silk. I have 't been using KDE 3.5x since the release of 4.0.\nThe only problem that I am now having is with Firefox.\n\nConcerning your xorg.conf file, openSuse Users should use the value \"true\" instead of \"1\" if they want to try your settings."
    author: "Bobby"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-13
    body: "... smooth as silk with OpenGL-Effects enabled, especially minimizing and dragging around applets?\n\nIf so, please say what you did ;)"
    author: "Marc"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-13
    body: "Marcel Partap wrote: \"put 'export KWIN_NVIDIA_HACK=1' in your /etc/profile or so if you have an nvidia card, it does make a huge difference!)\"\n\nI tried that plus adjusted the xorg.conf file to that of Sepp (only using \"true\" instead of \"1\" on openSuse) and the effects are now really smooth. I am only having problems with Firefox, that the Plasma Panel flickers when I am writing like now.\n"
    author: "Bobby"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-14
    body: "Aaawww... i'm going nuts over this! I installed a clean new ubuntu, then KDE 4.0 with kdm-kde4, then the latest nvidia driver for the GF6600GT, configured xorg.conf and /etc/profiles...\n\nand it's still totally jerky :-( Especially that others say it's totally smooth with them makes my head explode... i tried about everything, and minimizing or dragging things around is at best 5fps.\n\nIt's a shame, i just updated to this card to be able to enjoy kde4 with OpenGL, knowing it wouldn't be that optimised yet. I can play FarCry under linux, but it isn't good enough for kwin4??"
    author: "Marc"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-14
    body: "ok, problem solved.\n\nKDE4 's now running on top of compiz and emerald with a theme i copied from the kwin4-decoration. Looks nice and doesn't eat up my system.\n\nI liked the look'n'feel of the more integrated kwin4 effects better, but not like that. If the kwin guys which i pointed to this thread will do some kind of comprehensive howto for nvidia cards, i'll be happy to test and bug report."
    author: "Marc"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-01-14
    body: "Ido agree that a howto is needed. i personally tried a few tips here and with a little luck it worked butr obviously not for everybody."
    author: "Bobby"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-07-19
    body: "Hello,\n\nI used the Xorg config from above, but with OpenGL the system is getting near 100% CPU load, only XRender works, which doesn't work with many effects, most notable desktop flip, which is sad.\n\nSo +1 to a how to.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Please provide howto/help for nvidia users"
    date: 2008-07-19
    body: "I found the following two pages useful:\n\nhttp://techbase.kde.org/User:Lemma/KDE4-NVIDIA\n\nhttp://techbase.kde.org/User:Lemma/GPU-Performance\n\n"
    author: "MK"
  - subject: "Widget colours"
    date: 2008-01-12
    body: "Black - it's the new last year. Nice work guys!"
    author: "fnord"
  - subject: "Re: Widget colours"
    date: 2008-01-13
    body: "would you be so kind as to explain the intended meaning of your statement, as I do not currently understand it."
    author: "yman"
  - subject: "Fedora Rawhide based KDE 4.0.0 Live CD"
    date: 2008-01-12
    body: "Some news from our KDE Live CD maintainer:\nhttp://www.deadbabylon.de/blog/2008/01/11/livecd-for-rawhide-with-kde-400/\n\nWARNING: This comes from our development branch (Rawhide) which is what will become Fedora 9 in about 3.5 months. You may therefore encounter bugs on that live CD which are not necessarily KDE's fault."
    author: "Kevin Kofler"
  - subject: "Re: Fedora Rawhide based KDE 4.0.0 Live CD"
    date: 2008-01-12
    body: "PS: If you try out that live CD and encounter issues, we would appreciate if you could report them. We cannot fix issues (nor forward them to the right people to get them fixed) we don't even know about. ;-)"
    author: "Kevin Kofler"
  - subject: "Please provide support!"
    date: 2008-01-12
    body: "Hi folks,\n\n\nI like to thank the KDE4 developers for their hard work. I'm using KDE since the first beta (I contributed KBruch) and it was always an exciting journey since that time seeing how the project evolves!!!\n\nI installed the currently release and I came over some problems. I'm not sure if it is related to KDE4 itself or just the kubuntu packages, but I encourage every user to go to the support forums (http://www.kde-forum.org) and try to help out!\n\n\nCheers,\n\n\nSebastian"
    author: "Sebastian"
  - subject: "I CAME!"
    date: 2008-01-12
    body: "this news made me ejaculate a pine scented semen dragon which crept down my monitor until I licked it off and howled like a wolf"
    author: "Uncle Rooster"
  - subject: "Re: I CAME!"
    date: 2008-01-12
    body: "Eewww ;)"
    author: "Marc"
  - subject: "Re: I CAME!"
    date: 2008-01-12
    body: "That's far more information than I needed to know"
    author: "Brandybuck"
  - subject: "Re: I CAME!"
    date: 2008-01-13
    body: "I was trying to eat something while reading the comments here. thanks to you it seems I'll be saving money of food after all."
    author: "yman"
  - subject: "Coolness"
    date: 2008-01-12
    body: "So, while I think that the release has a few annoying bugs here and there that make me want to wait for 4.1 to use it as my main desktop, I think that 4.0 is an absolutely stupendous achievement. It looks great, except for a few areas of theme polish (has anyone noticed a black line that appears when tabs get those scroll buttons?), has spectacular applications (my jaw dropped when I opened okular), and just seems downright cool.\nI thank everyone who worked to bring this great pile of awesomeness to the desktop from the bottom of my heart.\nWay to go guys."
    author: "Ryan"
  - subject: "KDE4"
    date: 2008-01-12
    body: "First reactions:\n\nThis doesn't suck.  Gnome has a lot of catching up to do.  LiveCD runs in a VirtualBox VM with 384MB with no swap.  That's really...really good.\n\nKnotify4 seems to be a pig.  This needs debugging.  Sorry, I am only a whiner and my only programming languages are TTL, Assembler, BASIC, Pascal, and GE FANUC G-codes (forget about C and C++).\n\nI am going to have to play with this more.  As yet, it is not a replacement for 3.5.x, but this is extremely promising.\n\nI take back my whining about Dolphin, but you will pry Konqueror and kioslaves from my cold dead hands.\n\n--\nBMO"
    author: "Boyle M. Owl"
  - subject: "Re: KDE4"
    date: 2008-01-13
    body: "\"you will pry Konqueror and kioslaves from my cold dead hands.\"\nwhy should I? I can get my own copies for free."
    author: "yman"
  - subject: "Congratulations!"
    date: 2008-01-12
    body: "First, congratulations to everybody who worked on this, I'm looking forward to see it working.\nAnd congratulations for the build system, everything compiled very well on my Slackware-current and I think faster than KDE3, great job! It's so easy to compile everything with KDE (and it has been for quite a while), I thought the people responsible for this should also get the thumbs up!\nNow, I get a \"Cannot start D-bus\" message after startx, while the dbus daemon is running... :-( If anyone has a clue, I'm interested, otherwise, I'll have to dig around. I've already found something in the kde forums, but it didn't help me."
    author: "Richard Van Den Boom"
  - subject: "Re: Congratulations!"
    date: 2008-01-13
    body: "If you find the solution, could you please write it here? I've had this problme with SVN for a while (currently doing a refresh install).\n\nMy temporary workaround is to disable the lines in startkde that checks for dbus. I don't get a fancy splash screen, but otherwise everything works OK.\n\nIt's just annoying that when something fails, I don't know if it's because of a bug in KDE or because of my 'hack'.\n\nRunning Slackware 12.0 by the way."
    author: "Hans"
  - subject: "Re: Congratulations!"
    date: 2008-01-13
    body: "OK, that was a bit stupid, it's just that the QT4 bin dir was not in the PATH, so qdbus was not found. Just copy the /etc/profile.d/qt.sh file to a /etc/profile.d/qt4.sh and edit it to replace the /usr/lib/qt dirs by /usr/lib/qt4, and it works.\nNow I just have to figure out why KDE4 seems to believe the screen resolution is 1024x768 and not 1280x800 to set up the taskbar and the upper icon to add widget. :-)\nAnd install a proper driver for my ATI card (apparently a Radeon XPRESS 200M 5955)  to support composite, as the default Xorg driver doesn't seem to work well with it. :-)\nThe first impression is good though. System settings is much better than I feared and Dolphin seems fast well designed. Everything seems snappier than KDE3. Oxygen looks very good. Very promising indeed!"
    author: "Richard Van Den Boom"
  - subject: "Re: Congratulations!"
    date: 2008-01-13
    body: "Thanks! Works like a charm (although I still don't get a splash screen  :P)."
    author: "Hans"
  - subject: "Windows and OS-X ports?"
    date: 2008-01-12
    body: "I know it might be a bit early to ask this, but it's on my mind, so here goes:\n\nWhen will the Windows (XP/Vista), Mac OS-X ports of KDE 4.X.Y be released?*\nIs there a tentative schedule?\n\nTake your time though. I much rather have a stable, feature rich version of KDE, Amarok, the rest of Compiz fusion's features ported to Kwin, cool add-on's for the 4.X branch. :-)\n\n\n\n____\n* Is that actually happening, or was that just an evil rumor?\n"
    author: "Max"
  - subject: "Re: Windows and OS-X ports?"
    date: 2008-01-13
    body: "Ports to Windows are still in a early state. I see periodic blogs about it, there is progress but its slow. I haven't heard anything about OS X, but I did see some screenshots of Koffice running on it."
    author: "Skeith"
  - subject: "Re: Windows and OS-X ports?"
    date: 2008-01-13
    body: "Yea, I had the same question.\n\nWhere do I find  out about the Windows version?"
    author: "Mike"
  - subject: "Win and Mac \"in progress\""
    date: 2008-01-13
    body: "You can build KDE-win and there's an installer, but it's very early. \nhttp://techbase.kde.org/index.php?title=Projects/KDE_on_Windows\nI had fun playing several games and some utilities worked, but had no luck with Dolphin and Konqui.  Your experience will vary.\n\nSimilarly, you can build KDE on Mac OS X and Ranger Rick provided Mac packages, but he wrote \"Let's just say this stuff is still very alpha\".\nhttp://techbase.kde.org/index.php?title=Getting_Started/Build/KDE4/Mac_OS_X\nhttp://ranger.users.finkproject.org/kde/index.php/Home\n\nThe last KDE4Daily Virtual Image works pretty well in qemu on Windows XP for me."
    author: "skierpage"
  - subject: "Re: Windows and OS-X ports?"
    date: 2008-01-13
    body: "Cool... Always wanted to use KDE on Windows... Especially INSTEAD of the Vista manager.\n\nHow come nobody ever hears about this? There is no link on the main page, nothing?\n\nIs this being developed in secret, or what?\n\nAnd when is AmaroK 2 coming out?\n"
    author: "Smiths"
  - subject: "Re: Windows and OS-X ports?"
    date: 2008-01-14
    body: "It doesn't seem like a lot of peolpe would skip the Vista manager/whatever.\n\nUsing KDE software like KOffice etc should be a really good idea though."
    author: "KDE User"
  - subject: "KDE4 First build and look"
    date: 2008-01-12
    body: "For personal/home use I use a modified semi-automated lfs/blfs environment. Currently I have kde 3.5.8 on all systems (never really thought much of gnome). I've been building and trying kde4 since v3.94, and v4.0 is almost there... One thing nice is that its much easier to build than kde3, and MUCH faster to build (1/3-1/2 the build time). Of this could simply be due to missing features. The first thing I do with after installing kde is change the Desktop theme  to something that doesn't look as much like windows.. \"Platinum\" is my first choice .. but still, in kde4.0, attempting a Desktop theme change crashes after the progress bar, without changing... Also, kde4.0 is quite sluggish. Various internal apps like konsole/dolphin/konqueror appear to be working ok (although I've hardly tested them thoroughly), and external apps like mozilla thunderbird/firefox/seamonkey, mplayer, vlc are working as well, so I'm happy. All in all, very impressive... If the marked sluggishness can be reduced (perhaps due to my builds being debug builds? Qt 4.3.3 is also a debug build), it'd be fantastic. "
    author: "John Stanley "
  - subject: "Re: KDE4 First build and look"
    date: 2008-01-13
    body: "> If the marked sluggishness can be reduced\n> (perhaps due to my builds being debug builds? Qt 4.3.3 is also a debug build)\n\nThat depends whether by \"Debug build\" you mean \"Debug only\" (eg. cmake ... -DCMAKE_BUILD_TYPE=Debug) or \"Release with debug info\".\n\nIf you built Qt and KDE without any optimizations then yes, that will have a noticeable effect on performance, but it shouldn't be uncomfortably slow even so (I use debug builds of everything).  If that is the case then something else is up.  For example, apparently there are some problems with NVidia graphics cards on a composited KDE 4 desktop.\n\n"
    author: "Robert Knight"
  - subject: "Compiling..."
    date: 2008-01-13
    body: "Yo!\n\ni wasn't KDE user since 1.0!\nBut with 4.0 KDE gets interesting again.\n\nNow my workstation is gone to compile the whole using kdesvn under Fedora 8.\nI hope it will be successfully!!!!!\n"
    author: "lordbios"
  - subject: "Congratulations! I am looking forward to future"
    date: 2008-01-13
    body: "Congratulations to the KDE team! A huge amount of work has been achieved. I am sure this is the beginning of a successful series.\n\nAnyway I have a few comments, that should not offend the developers as they are just issues I faced with on this very first release. I am sure other people more or less noticed the same problems.\n\nI have used KDE 4.0.0 under Kubuntu 7.10. IMHO, there is some work in order to make it a competitive desktop, considering the current standards. I have noticed several problems (maybe related to Kubuntu packages sometimes?):\n1) The windows manager is slow (resizing a window is not smooth).\n2) The desktop is too \"rigid\", like the K menu that cannot be resized (on the vertical) with the mouse.\n3) Browsing the applications in the K menu is inefficient (large mouse movements, loss of visual \"information\" as only one level in the menu tree can be seen, ...).\n4) A click on \"Add widget\" (Plasma, top right) does not work all the time: you need luck to open the widget window. The applets seem useless. BTW, this would be my main criticism on KDE4 vision: I don't understand all the buzz on Plasma since one uses windows, not a desktop with the outdoor temperature hidden behind the windows... Maybe next applets will get me wrong.\n5) Konqueror sometimes pops up strange empty windows that disappear very quickly.\n6) The Oxygen theme does not seem to be finished. The icons and buttons are very good, but the windows and colors should be improved. In particular, it is not obvious to find the window that has the focus because it is very similar to the background windows: isn't it a usuability issue? I finally switched to Keramik which is more convenient.\n7) Obviously, there is no more kcontrol and the basic configuration panel is limited (but this is maybe Kubuntu work?). I could not find a way to move the task bar on the left (instead of at the bottom): at least, I could not do it with the mouse, which makes the desktop \"rigid\" again.\n8) The fonts of my KDE3 applications are very small; but this is maybe related to Kubuntu management too.\n9) There is something strange with some windows or menus style (like after a right click on the taskbar): there are very rounded, flat and with no frame. This reminds me of the smooth appearance of Gnome (or maybe MacOS actually), and it does not seem really consistent with the usual \"tech\" style of KDE.\n\nAnyway, again, congratulations! The future is bright (but it is the future!). The current state of KDE is interesting but the desktop surely needs to mature."
    author: "Hobbes"
  - subject: "Re: Congratulations! I am looking forward to future"
    date: 2008-01-14
    body: "Kickoff (the new KMenu) is perfect.  A usability study said it was perfect and that everyone loved it.  All the people who don't like it are wrong.\n\nWrong, wrong, wrong!  You're not allowed to not like the menu.\n\nDidn't you get the memo?"
    author: "T. J. Brumfield"
  - subject: "Re: Congratulations! I am looking forward to future"
    date: 2008-01-15
    body: "Why do people always have to resort to hyperbole when thy can't make a valid point?\n\nYou are perfectly entitled to not like Kickoff for whatever reasons. There are also people who dislike graphical desktop environments, word processors and other things and use CLI and vi instead. That's in their right and nobody is trying to take it away from them. But we are talking about the *default* menu here and in my opinion it's important that you use something that has been usability tested instead of listening to armchair usability experts on the internet. Someone once described an interesting phenomenon (I forgot the name unfortunately): When building a laboratory nobody really comments its design. Try the same with a barn and somehow everybody thinks he's suddenly an expert in barn construction.\n\nYou are perfectly free to chose any other menu once they are implemented and realeased (for instance, the classic menu which is part of 4.0.0). But don't try to impose your skewed notions about usability on other people."
    author: "Erunno"
  - subject: "Re: Congratulations! I am looking forward to future"
    date: 2008-01-15
    body: "I was being facetious, and you in turn argued my point for me.\n\nThanks."
    author: "T. J. Brumfield"
  - subject: "Re: Congratulations! I am looking forward to futur"
    date: 2008-01-15
    body: "I agree with T.J. Kickoff is pretty annoying in it's present state.\nIf it could be made to be full screen it would be much better.\nasiego have promised this will be implemented in 4.1.\n"
    author: "reihal"
  - subject: "Re: Congratulations! I am looking forward to future"
    date: 2008-01-16
    body: "I use the official openSuse version of Kickoff for KDE 3.5. It is just perfect, though sometimes I do not understand how to activate the tabs with/without click. The KDE4.0 version of Kickoff is a rewrite which is not nearly as smooth as the original of openSuse. I wonder why it has been rewritten for Kde4 and not just ported. Else it would have been functional. In its present state it indeed behaves like a left hand."
    author: "Sebastian"
  - subject: "Amazing!"
    date: 2008-01-13
    body: "Wow. I'm seriously impressed.\n\nI installed KDE4 right after I got home last night, and I must say: simply great. The overall responsiveness of the system is a huge improvement over KDE3, and I just love the new looks. \n\nTo everyone who contributed to KDE4; you all did a wonderful job here. \n\nHats off!"
    author: "chris"
  - subject: "Disappointed..."
    date: 2008-01-13
    body: "I'm very much wondering what all this praise is about. When I installed KDE4 (openSUSE 10.3 packages) my first thought was \"nice\", my next thought was \"Did I forget to install something?\". Really, there aren't even the most basic configuration tools available. For example, it doesn't seem to be possible to configure the application launcher, the panel, ... A quick look into some \"Emergency FAQ\" reveals that, sorry, the configuration programs \"didn't make it in time\". Oh boy...\n\nAlso, the looks are not great at all, the default color set cannot decide if it wants to be dull gray without contrast or look like a zebra with hard black and white contrast. Hit Alt-F2 to see what I mean: A light grey border, then a thick, black border, then all white boxes - it more or less exactly fails to please the eye, as Douglas Adams puts it. The \"Run Command\" has useless buttons: Why would I want to launch a task monitor tool from here? And why is the main action just one amongst many buttons. Really, there should be just two buttons, \"Run\" and \"Cancel\". Yet this is, according to surveys, the most used element on a KDE desktop. It deserved a more loving hand, for sure.\n\nThe application launcher, the second thing after the desktop itself people look at, has a very unpolished look. The margin between content and border on all elements is too small, the degree of detail between the icons varies too much, the icons for the different categories (Favorites, Applications, ...) vary in size, ...\n\nI really do hope that this is just \"the start of something\".\n"
    author: "Matthias"
  - subject: "Re: Disappointed..."
    date: 2008-01-13
    body: "I completely agree with you.\n\nThe look is what disappointed me most. I can imagine what may be possible with Plasma and use of SVG, but after all I have read about Oxygen in the last years I was really disappointed of the default window theme. The dull grey of the default color schemes reminds me of the early times of graphical desktops, but maybe a retro look was intended. The grey color is eben more annoying as in many applications there is a large amount of blank space in this color. I hope that soon there will be more appealing themes for KDE 4.\n\nThe next thing that disappointed me was usabilty. You mentioned two exellent examples: KRunner and Kickoff. There are other examples two.\n\nI don't know why \"System Settings\" should be easier to use kcontrol. The best solution IMHO would have been to keep the navigation sidebar on the left as in kcontrol and to show an overview in the main window like in system settings at the start.\n\nA simple thing I have noticed in Okular. There is an icon list at the left and I needed some time to recognize that it serves to determine the contents of the sidebar. A more obvious way to achieve this would be to have tabs at the top of the sidebar itself, such as e.g. in the sidebar of Kalcium. This would not only be more obvious, but also save space that could be used for the main window.\n\nAs a long time KDE user I have also difficulties with the switch to Dolphin as default file manager. Developpers say that I can keep using Konqueror for file management, but there are already things that don't seem to work anymore with Konqueror, e.g. what happened to media:/ and how can I manage devices with Konqueror?\n\nWhat annoys me is that I have the impression that a piece of software I use daily is evolving in the wrong direction - at least from my point of view. Perceptions of appealing looks, elegance and usability are very different. For me KDE 4 lacks appeal, elegance and usability - and remains below its possibilities with regard to the fundamental conceptual changes in the background like Plasma.\n\nThat KDE 4 still lacks many features is not my problem, but that concepts of style and usability that seem strange to me will shape ist further development.\n\n"
    author: "Stefan L"
  - subject: "Re: Disappointed..."
    date: 2008-01-14
    body: "I cannot say I disagree with the two parent posts.\n\nI really appreciate the hard work that the KDE folks are putting into this one, but the release looked... well... \"alpha\".  Mostly because of the missing configuration tools and other things that should have been in before it went beta, but also because I was able to crash KWin the first time I threw something hard at it (a full screen app under Wine).\n\nThough I'm looking forward to further updates, and 4.1 in particular, about three hours of 4.0 was all I could take before I went back to 3.5 for all my \"normal user\" type activities.\n\n-J"
    author: "3vi1"
  - subject: "Nice work!"
    date: 2008-01-13
    body: "Congrats to all, nicely done.\n\nOne question: is it possible to make the run command (ALT-F2) to work using the search entry in the kickoff menu?"
    author: "Leiteiro"
  - subject: "Isn't is a beta version?"
    date: 2008-01-13
    body: "All right... You may kill me, you may curse me, do whatever you want.\n\nI *DO* appreciate a great piece of work from KDE dev team. I really do. But the problem is that KDE has been released despite the fact it's still no more usable than a beta version. I do understand that KDE 4.0 is not a \"full\" version of KDE4... However if I lose Kickoff and can't launch it again even after reboot, if I have no possibility to find room for all icons because of great distances between icons which I can't change, if KDE hangs at random - if all this happens, this should not be called an official release. I know it's been looked forward, I know everyone was expecting it, but wouldn't it be better to wait for another month or two and release a fully usable product? Right now I theoretically have KDE 4.0 in my hands but I can't use it anyway - how's an environment working like this going to be a true alternative for Vista or Leopard? I just don't get it.\n\nOnce again: this is no offence to the work of devel team - I understand you work for free, that it's just your free will to work for KDE and all - but I don't really understand why a product with such a number of errors has been released. Anyway, I get back to using KDE 3 again and I'll wait to see what future brings. Good luck!"
    author: "mkrs"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "Hmm. I would have named it different. Maybe \"KDE 4 Phase 0 aka API Freeze\" and then release a \"KDE 4 Phase 1 aka First Steps\" at the end of summer and a \"KDE 4 Phase 2 aka Final\" in December or something like that."
    author: "panzi"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "OK, may it be, if you like it. Anyway, this should NOT be an official release. The reason is simple: people will download \"the new, polished KDE\" and see it's not working. The next BIG chance for KDE laymen to try a new product is KDE 5.0 (because transformation from 4.0 to 4.1 is never as impressive as 3.0 -> 4.0) . A great chance for a dramatic increase of KDE popularity has just been wasted and I'm very sorry to say that."
    author: "mkrs"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "\"The next BIG chance for KDE laymen to try a new product is KDE 5.0\"\n\nNo, the next big chance is the mass of KDE software pre-installed on the wildly popular EEE PC.  \n\n\"A great chance for a dramatic increase of KDE popularity has just been wasted and I'm very sorry to say that.\"\n\nDon't be so melodramatic.  "
    author: "Anon"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "\"A great chance for a dramatic increase of KDE popularity has just been wasted and I'm very sorry to say that.\"\n\n\"Don't be so melodramatic.\"\n\nIt might be melodramatic, but it's also true. One thing KDE4 managed to do in the build up to release was create a \"buzz\". People were actually excited about it.  That buzz/excitement is not going to come along for 4.1, 4.2, 4.3 or whatever. All that's happened is that 'when it's ready' has turned from a definite 4.0 into 'sometime around .2 .3 .4'. Is there even a roadmap?\n\nThe 'KDE4' buzz has been wasted on a unusable release. Yes, that's just marketing, politics, whatever, but it's a fact that non-developers who have been waiting for KDE4 want it to work. More than that, they expect it to work. If it doesn't, it fails.\n\nThe 'release early, release often' argument?: it was perfectly possible to release this as a pre-4.0 release (Release Candidate, as in REAL Release Candidate, as in it works) and carry on working on it until it was ready. When is it ready? When there are no regressions.\n\nThe argument that somehow a full release was needed to get developers to focus on tidying up the experience is the best of the bunch. That's a management problem.  Again, where's the roadmap?\n\nSo, yeh, it's politics, marketing and management. But success is not just about keeping developers happy. 'We'll get more bug reports' only works if people can use the thing.  You'd have to be a masochist to do that now.\n\n\n\n"
    author: "Anon"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: ">The argument that somehow a full release was needed to get developers to focus on tidying up >the experience is the best of the bunch.\n\nIts not a management problem, its a management solution. \n\n(its also increasing the amount of vital bug reports, and encouraging 3rd party app teams to get porting)"
    author: "Ben"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "\"Its not a management problem, its a management solution.\"\n\nIt's a solution to a management problem. But not a particularly good one.\n\n\"(its also increasing the amount of vital bug reports, and encouraging 3rd party app teams to get porting)\"\n\na. Do you have numbers to confirm that?\nb. Can you confirm that the change is because of the .0 on the end, or the fact that it finally compiles?"
    author: "Blah"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "\"b. Can you confirm that the change is because of the .0 on the end, or the fact that it finally compiles?\"\n\nI've been compiling nearly all of KDE SVN very nearly every day for a *year*, now.  These snide and uninformed jabs aren't helping your case."
    author: "Anon"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-14
    body: "The argument for releasing this as 4.0 was that doing so would increase the number of bug reports. That's an easy thing to say, but I've not seen anyone back it up with figures. Secondly, even if there *is* an increase there is a good chance that is simply because this is the first version that actually runs for some people... (unsuprisingly, including me.)\n\nFair point on the \"compiles\" bit, I take it back.\n\nI like KDE, I really do. I have even bothered to contribute in the past. I can  see the work put into this release has been clearly phenomenal. But \"KDE4\" as an entity an event has wasted a huge opportunity. You don't get this amount of buzz for new releases very often, and this is a damp squib.\n\nArguments about releasing now as 4.0 are all developer focused: getting them to bug fix, getting them focused. Of course, those things should be done for a release, but it's a pretty dumb reason to release of itself. A plan might help with this, and maybe then we'd know for definite when KDE4 is going to be  usable... is it .1 .2 .3 .4 .5?\n\nAs an aside: For all the talk about \"developers only!!\" even the KDE homepage points to this as a stable release. As a first impression for anyone of KDE4 this would suck. So we find ourselves in the situation of wishing people not to download the latest version.  That's weird whichever way you try to spin it."
    author: "Anon"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-14
    body: "\" One thing KDE4 managed to do in the build up to release was create a \"buzz\". People were actually excited about it. That buzz/excitement is not going to come along for 4.1, 4.2, 4.3 or whatever. All that's happened is that 'when it's ready' has turned from a definite 4.0 into 'sometime around .2 .3 .4'. Is there even a roadmap?\n\nThe 'KDE4' buzz has been wasted on a unusable release. Yes, that's just marketing, politics, whatever, but it's a fact that non-developers who have been waiting for KDE4 want it to work. More than that, they expect it to work. If it doesn't, it fails.\n\nThe 'release early, release often' argument?: it was perfectly possible to release this as a pre-4.0 release (Release Candidate, as in REAL Release Candidate, as in it works) and carry on working on it until it was ready. When is it ready? When there are no regressions.\"\n\n---------------------------------\n\nThat's exactly what I think. This \"buzz\" was something I've never seen before in the Open Source world (maybe I'm not a Linux dinosaur but I've been into it for about 5 years now). And now I think that it has been wasted. And that's very sad because the reason for this is not bad quality of the program itself but some unreasonable haste while trying to release it."
    author: "mkrs"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-13
    body: "I would love to have KDE 4.0 on an eeePC. =)\n\nWhen will that happen?\n\nWhat will be the first distro that will be released with KDE 4.0 out of the box? I've read Fedora, any others?"
    author: "Smiths"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-14
    body: "I read that KDE 4.1 will be included in openSuse 11 which should be released on the 19 of June."
    author: "Bobby"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-14
    body: "That's speculative, to say the least.  Most estimates of a KDE 4.1 release date are targetted at 6-9 months from now."
    author: "Anon"
  - subject: "Re: Isn't is a beta version?"
    date: 2008-01-14
    body: "Yes, it is the public Beta.  Considering that that is what it actually is, it represents a lot of good work and I look forward to seeing the actual release candidate."
    author: "YAC {yet another critic}"
  - subject: "SUSE live CD"
    date: 2008-01-13
    body: "At least SuSE's live CD is crap. There are no HDs displayed in dolphin or konqueror, even though you can mount it from a root shell. And SuSES network config tool is crap, too. It is slow and it wants to download packages from SuSEs Server, but it cant, because the network isn't configured right yet (chicken egg problem). I had do do \"sudo route add default gw 10.0.0.138\" so I could access the internet. With guidance's python tools this isn't a problem at all. Why isn't this tool installed?\n\nAnd there are some things that clearly are lacks of KDE (that hopefully will be fixed in 4.1), like I could not find a way to make that ridiculous big taskbar smaller and display only the windows from the current desktop. It is beyond me why anyone would ever like to have all windows displayed.\n\nAnd if there would have been a gcc + the kernel headers on the image (it has enough space left!) I could have installed the nvidia drivers to test KDE 4.0 with composite enabled.\n\nBut it did work with my sisters iMac! Only in stretched 800x600, though (it's a widescreen). It's the first Linux I tried that didn't just display corrupt graphics on it. Sound worked, too.\n\nI'm really looking forward to KDE 4.1, but KDE 4.0 is not the least bit usable for day to days work right now. But it looks promising."
    author: "panzi"
  - subject: "Re: SUSE live CD"
    date: 2008-01-14
    body: "\"And there are some things that clearly are lacks of KDE (that hopefully will be fixed in 4.1), like I could not find a way to make that ridiculous big taskbar smaller and display only the windows from the current desktop. It is beyond me why anyone would ever like to have all windows displayed.\"\n\nwhy do people post this over and over again? on this page alone I must have read a few complaint threads that mention this, with answers.\n\nthe answer is that while KDE 4.0 is a functioning, largely stable environment, the desktop is not feature complete. as time goes by, more and more missing features from Kicker will be added to Plasma (yes, Plasma is the new, redesigned desktop and replaces Kicker)."
    author: "yman"
  - subject: "Re: SUSE live CD"
    date: 2008-01-14
    body: "There are some issues with distros as far I know. In Kubuntu kde4.0 live cd there is no sound at all. In suse live cd i have sound but i don't know where is sound system settings to check new sounds.\n\nAnd on both live cd I couldn't use kwin effects. Maybe because I have ati 9200 se graphic card.\n\nBut unfortunatly there is no debian live cd, because I think there are no so much distro issues."
    author: "user"
  - subject: "Re: SUSE live CD"
    date: 2008-01-15
    body: "Sounds like you have an issue finding stuff in distros.  Either way, on both Kubuntu and openSUSE, look in System Settings-> Notifications-> System notifications."
    author: "Bille"
  - subject: "One big mistake"
    date: 2008-01-13
    body: "As a kde user and advocate since the original series, KDE BETA 3 i think was the first one I used, I have been here for each of the .0 releases.  They were all good polished releases with a few hiccups to be had.  The 3.0 release caused much trouble due to breaking binary compatibility but it was still a good release.\n\nHere we have a KDE 4.0 release which has much to commend it.  However, even the developers admit it is not complete.  When a new user suddenly gets an interest in the KDE desktop now he or she will go to the kde.org website, click on download \"Stable version\" and start building KDE 4.0.  They will find a desktop environment that does not actually do much apart from weird desktop effects and in all likely hood go back to whatever they used before. Imagine, if you will, that KDE 4.0 was the first time you had ever used a KDE desktop.\n\nI think it would have been prudent to keep KDE 3.5.8 as an alternative, mature, full featured stable release alongside KDE 4.0\n\nI further hope that KDE does not go along the politically correct, \"interface expert\" marketing driven road that so much software is going down these days as indicated by some of the design decisions we see in this release. These people have no idea what users actually want.  I dont want to list some of the issues I find as  it is time to thank the developers, not moan at them but it is worth mentioning that I have never met a developer who thought OSX was in any way a design you want to emulate (writing this on a macbook pro by the way as the hardware is great!)\n\nI think KDE 4.0 shows enormous promise due to the underlying technology but I think some of the design decisions need looking at. \n\nI hate bitching about KDE because I admire the people who work so tirelessly on the system but I urge them to keep away from the marketing people and to put KDE 3.5.8 back up on the website as an alternative stable version.\n\nCheers"
    author: "Wheely"
  - subject: "Re: One big mistake"
    date: 2008-01-14
    body: "\"When a new user suddenly gets an interest in the KDE desktop now he or she will go to the kde.org website, click on download \"Stable version\" and start building KDE 4.0.\"\n\nin all likelihood, new users will see KDE as presented by their distribution of choice, not build it themselves.\n\n\"I think it would have been prudent to keep KDE 3.5.8 as an alternative, mature, full featured stable release alongside KDE 4.0\"\n\nthat is exactly what is being done."
    author: "yman"
  - subject: "Re: One big mistake"
    date: 2008-01-14
    body: "I don't believe it makes a difference if they build it themselves or get it from the distro of choice.  It still is not an ideal introduction to KDE.\n\nThe kde.org website link for the stable version of KDE is only 4.0.  I believe it should mention both.  One for the current \"stable\" version and another for the old mature, full featured stable version."
    author: "Wheely"
  - subject: "Re: One big mistake"
    date: 2008-01-15
    body: "I meant that users would experience KDE as it is presented to them by the distributions, and that means they would probably not get exposed to KDE4 until their distribution thinks it is ready. and that means that users would probably get their first taste at KDE 4.1 or 4.2."
    author: "yman"
  - subject: "Read the digest?"
    date: 2008-01-14
    body: "Did you read the last digest?\n\nAaron mentioned that KDE 3.5.x will be supported for some time to come."
    author: "T. J. Brumfield"
  - subject: "Re: Read the digest?"
    date: 2008-01-14
    body: "I did actually.  I am not too worried about the people who frequent sites such as this.  We know what our options are.\n\nNew users, however, will not have a clue and from their point of view, if they download KDE4.0 and find it, shall we say \"lacking in a few areas\" there will be zero likelyhood of them also thinking \"well I didnt like that too much I think I will try one version earlier\".\n\nI think having two current stable versions would help to clarify the fact that KDE 4.0 is a new born baby, yet to grow but if you want a kick ass, full featured and now finished desktop, try its mummy, KDE 3.5.8 :)"
    author: "Wheely"
  - subject: "Re: Read the digest?"
    date: 2008-01-15
    body: "Most distros are keeping KDE 4.0 in the unstable repositories at the moment.  They aren't just pushing it out as a system critical update.\n\nIn a few months a few distros will go with KDE 4.x by default, and by that time KDE 4 will be improved.  I imagine configuration dialogs will be one of the first things fixed."
    author: "T. J. Brumfield"
  - subject: "Sound effects in KDE 4.0"
    date: 2008-01-13
    body: "I've upgraded by KDE 3.97 installation from openSUSE RPMs to 4.0 (via online update). In the final release, there is a new sound theme with keyboard chords. There was another one in the RCs which I liked much more; where can I get this one?"
    author: "Stefan"
  - subject: "Where is the rest of the compositioning effects?"
    date: 2008-01-13
    body: "Having used Compiz-fusion for a few months now. I've grown to miss the desktop cube, expose, coverflow. Where are those effects hidden under KDE 4.0?\n\nhttp://en.wikipedia.org/wiki/Compiz-Fusion\n\nI would like to be able to use all of Compiz-fusion's plugins:\nMain Plugins:\n\n    * Animation\n    * Color filter\n    * Expo\n    * Enhanced Zoom Desktop\n    * JPEG\n    * Negative\n    * Opacify\n    * Put\n    * Resize info\n    * Ring Switcher\n    * Shift Switcher\n    * Scale Addons\n    * Snapping Windows\n    * Text\n    * Window Previews\n    * Viewport Switcher\n    * Desktop Wall\n    * Window Rules\n    * Workarounds\n\nExtra Plugins:\n\n    * ADD Helper\n    * Benchmark\n    * Crash handler\n    * Cube Caps\n    * Cube Reflection\n    * Extra WM Actions\n    * Fade to Desktop\n    * Firepaint\n    * Cube Gears\n    * Group and Tab Windows\n    * Motion blur\n    * Reflection\n    * Scale Window Title Filter\n    * Show desktop\n    * Splash\n    * Trailfocus\n    * Widget Layer\n\nUnsupported Plugins:\n\n    * Fake ARGB\n    * Snow\n    * Mouse Switch\n    * Tile\n\nExperimental Plugins:\n\n    * Screensaver\n    * Compiz-Scheme\n    * Sound\n    * Visual Event\n    * 3D Windows\n    * Cube Atlantis\n\n\nSeriously... \n\nOther than that, GREAT JOB!!!! KDE 4.0 finally looks different than Windows. Seems to have some great features.\n\nStuff is missing though. Where are the great 3rd party apps? Where are the apps that integrate with Plasma? Isn't google going to help out too? We need a great userbase by the time 4.1 comes out. Otherwise interest will fade quickly and the distros will go back to gnome. :(\nPlease try to make a Killer-App for KDE 4!! Is AmaroK 2.0 going to be it? "
    author: "Smiths"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-13
    body: "KDE4 don't use compiz ;) it use KWIN4. KWIN use it's own set of plugins, not compiz plugin. Originally they planed to use compiz, but the idea was dropped soon in the development of KDE4."
    author: "Emmanuel Lepage Vallee"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-13
    body: "Yea, KDE doesn't use Compiz, but it would be cool if they would work together with the Compiz people and port all of compiz over to Kwin. No need to reinvent the wheel here.. :-)\n\nCompiz has great features, and having them in Kwin would be great. Especially if developers could write effects that work for both systems."
    author: "Max"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-13
    body: "As far as I know it's up to Compiz to port it's plugins to KDE 4 and not the other way around. I am however sure that that will happen very soon."
    author: "Bobby"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-14
    body: "No, it is up to the KDE developers (and perhaps new volunteers) to develop composite effects for Kwin.  Kwin and Compiz-fusion are two separate beasts and will likely never meet."
    author: "T. J. Brumfield"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-14
    body: "NO, it is up to those, who want to use this plugins! :P"
    author: "panzi"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-14
    body: "I agree with you. I really love the effects of compiz-fusion but I never got it to run really smoothly on KDE and it always had issues with KDE takes away my beloved system sounds. What Kwin4 does is well done, it can't be compared to compiz for now but the effects are nicely integrated in Kwin itself and that's very important. The rest will come later :)"
    author: "Bobby"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-14
    body: "I love compiz-fusion and run it daily on KDE.  I will likely continue to do so until Kwin can replicate most of the standard plugins I use all the time.  Then again, I'm not sure I'm ready to jump in the KDE 4 pool anyway.  Normally I can't wait and I run beta software, but I love everything that KDE 3.5 gives me right now, and I'm not prepared to give it up just yet.\n\nIt may be as soon as 4.0.1 or 4.0.2, but I'm hoping to see some new styles, and more configuration.  Kwin doesn't need to catch up to compiz-fusion immediately, because I can likely run compiz-fusion on top of KDE 4."
    author: "T. J. Brumfield"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-15
    body: "Yea, there is no way I'm giving up the Compiz fusion effects..\n\nHopefully Kwin will include the rest soon. At the very least \"coverflow\", and expose, and cube.\n\nYea, I think I'd be using Compiz fusion under KDE 4.0 too, but I'd rather not give up the rest of Kwin to have gorgeous eye candy...\n\nWhen is 4.0.1, or 4.0.2 coming out anyways? I haven't seen any announcement, build threads, etc..\n\n"
    author: "Max"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-15
    body: "shouldn't they all work together to create a common API, standardized by FreeDesktop.org?"
    author: "yman"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-15
    body: "http://www.tectonic.co.za/?p=2006\n\nSee compiz is actually useful and not just eye candy.\n\nNow which ones of these features are missing in the KDE 4.0 version of Compiz?\nKwin, right?\n\n"
    author: "Steve M"
  - subject: "Re: Where is the rest of the compositioning effect"
    date: 2008-01-16
    body: "Cool link.. :)\n\nIt's what I have been sayin' all along!!!!\n\n\n-M"
    author: "Max"
  - subject: "Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-13
    body: "I would like to request: \nAn Apple OS-X Leopard style dock that's compatible with KDE 4/Plasma.\n\n"
    author: "Max"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "Nearly all mac users, myself included, don't particularly like the OSX Leopard dock.  Nearly all turn off the icon zooming and many apply a hack that makes it 2D and closer to kicker.\n\nIt is representative of most flashy desktop effects i.e. they are great for impressing people who haven't seen them before but generally get in the way when you're actually trying to work."
    author: "Wheely"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "Could be it. I never owned a Mac. So there is the novelty factor.\nStill want to try it at least for a little bit. :-)\n\nI installed a Mac like dock for Win XP (moved the taskbar to the top and autohide'd it.) It's flat, doesn't have the 3d effect, but does have the effect where the icons get big when hovering over them. I like it so far, but I do miss the ability of seeing several firefox windows at once. The doc just lets me know I have firefox running, not how many windows. (so the regular taskbar is still helpful)\n\nI'd like to play with it under KDE 4, at least for a little while :-p"
    author: "Max"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "I know several Mac users (was one myself at previous job) and I've never seen any Mac user do any such thing to their dock, and all I asked like the new Leopard style (as do I).  I'd love to see Kickoff have the ability to be configured as the OS X dock (3D, reflective, bouncing icons)."
    author: "Phil"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "Try trawling the apple forums.  The majority prefer the old Tiger dock.  Personally I quite like reflective icons and the bounce is a good way to show that the application is showing an alert.  However, the icon zooming is awful because it really starts getting in the way if your application needs to get mouse events anywhere near the dock e.g for resizing.  It would be possible to do a better implementation I guess at the expense of extra mouse clicks to get the work done.  However, this would just be another candidate for the new trend of glitz over funtion.\n\nIf, it is an option however, why not."
    author: "Wheely"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "Yeah I don't like the HUGE zoom but zooming from (for example) 32 to 48 pixels is nice.\n\nThere was a kde-app called KSmoothDock which did exactly the same thing, it broke from 3.4x to 3.50 (I think, going back a couple of years now) and the author never updated it, which was a shame because it looked really good.\n\nI don't have time to trawl the Apple forums, too busy trolling KDE :-)"
    author: "Phil"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "\"I'd love to see Kickoff have the ability to be configured as the OS X dock (3D, reflective, bouncing icons)\"\n\nI would love that too. I have said many times that Kickoff is missing eye-candies apart from that I like it very much."
    author: "Bobby"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-15
    body: "I agree.. :)"
    author: "Max"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-15
    body: "Yes, because eye candy is really the most important aspect one has to consider when designing a frequently used application launcher :-/"
    author: "Erunno"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-14
    body: "http://xqde.xiaprojects.com/\n\nIt is a work in progress, but it is QT 4 based I do believe.  It is from the author of KXDocker, but I believe he said the new dock (XQDE) is supposed to be faster with far less code yet provide more features."
    author: "T. J. Brumfield"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-15
    body: "Really? That would be awesome..\n\nIt would add some color to the KDE 4.0 bar.. :D"
    author: "Max"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2008-01-15
    body: "A Plasmoid dock will be a lot better, don't you think? ;)"
    author: "Luis"
  - subject: "write the plasmoid"
    date: 2008-01-14
    body: "Someone just had to write a plasmoid to do it."
    author: "Ian Monroe"
  - subject: "Re: write the plasmoid"
    date: 2008-01-16
    body: "Did they do it already?"
    author: "Max"
  - subject: "Re: Feature request: Leopard stylle dock for KDE 4"
    date: 2009-01-14
    body: "I'd like to also add my voice to request this feature. KDE4 taskbar feels outdated compared to the rest of the desktop. "
    author: "KhaaL"
  - subject: "What happend to Sonnet?"
    date: 2008-01-14
    body: "Congrats. Great release. \n\nI've a small question: What happend to Sonnet?"
    author: "anonymous"
  - subject: "Re: What happend to Sonnet?"
    date: 2008-01-14
    body: "that's something i'd like to know, too.\nThe last bit of information is from the sonnet mailing list:\nhttp://mail.kde.org/pipermail/kde-sonnet/2007-October/000054.html\n\nI don't know, if some version of sonnet was released with KDE 4.0\nI randomly get spellchecking in Konqui, but this could be ispell/aspell, too."
    author: "Sepp"
  - subject: "Sorry for some criticism  "
    date: 2008-01-14
    body: "By all the congratulations and respects for the huge work, I have to bring some (i hope) constructive criticism. I was trying the nightly openSuSE builds for the last four months and have installed also the stable build. \n\nI'm very sorry but I think the 4.0 release is not a half as useful than the 3.5.x release is. Some of the points I found:\n\n- The task list can only contain three to four entries before it is full.\n\n- The new start menu (sorry I forgot the internal name) is just useless.\n  Specially because I have to scroll forward / backward to go through the\n  programs section. PLEASE, PLEASE add the old style menu in furter releases\n  again!!!!!!!\n\n- All the additional widgets for the main bar are that huge that its take a lot\n  of space.\n\n- Where are the option menus for all the main bar widgets such as task list,\n  pager, clock etc?\n\n- How can I move the main bar widgets? The pager is default located in the\n  middle of the main bar which lefts space for just one program in the task\n  list\n\n- When I add or remove items from the favorites list in new start menu, the\n  menu is resetted after next startup of kde 4\n\n- Where is the bluetooth integration? In konquerror I just could\n  add \"bluetooth:/\" to the addresslist to browse my bluetooth devices. \n\n- Starting up KDE 4 with a installed KDE 3.5.7 creates new kde 4 icons for\n  existing .desktop files each time it starts up. After several logouts/logins \n  the whole desktop is filled with that garbage icons\n\n- And at the end: KDE 4 crashes that often, that I cannot work for more than 5\n  to 10 minutes unless it crashes completely. Sometimes, it freezes the whole \n  system.\n\nOur local computer club has planned to make a small release party for the new kde 4 release but with that results we cannot show this to anybody because every second time a user clicks somewhere, something is going wrong.\n\nHey guys,by all respect, but there is a lot of work to become the 4.0 as useful and stable than the 3.5.8."
    author: "Roland Kaeser"
  - subject: "Re: Sorry for some criticism  "
    date: 2008-01-14
    body: "\"The new start menu (sorry I forgot the internal name) is just useless.\nSpecially because I have to scroll forward / backward to go through the\nprograms section. PLEASE, PLEASE add the old style menu in furter releases\nagain!!!!!!!\"\n\nNobody needs to complain about the new start menu (Kickoff) because one doesn't have to use it. It is a widget just like Lancelot or the traditional start menu. All you have to do is delete it from the taskbar, start your widget window/dialog (what ever it's called), search for the traditional menu and then drag and drop it in the task bar! Make sure that plasma extragear is installed though. I think that some of you are really nerving the developers because you ask for something, get it and then still keep complaining. Just take a little time out to learn KDE 4.0 like you would with a new girlfriend :)\n\nI think that you are having some problems with your installation or packages because I can't relate to a lot of the problems that you are having.\nCheck you installation brother."
    author: "Bobby"
  - subject: "Re: Sorry for some criticism  "
    date: 2008-01-14
    body: "The new menu (Kickoff) is still the default and it seems to gather a great deal of complaints.\n\nI think I installed Suse once way back in the day, and it had a wizard within KDE to configure it the first time a new user logged in.  I would love to see this sort of thing happen again.\n\nWhen a new user logs in the first time, ask them if they want to use KDE defaults, or configure the system to look and operate like Gnome, OS X, XP or Vista.  With QT having a Clearlooks engine built in, and the plethora of OS X, XP and Vista themes, it shouldn't be difficult to pull off.  In another step of said wizard, you can ask people to choose the menu they want.\n\nI think such a start-up wizard would make a great deal number of people quite happy."
    author: "T. J. Brumfield"
  - subject: "Re: Sorry for some criticism  "
    date: 2008-01-15
    body: "Someone contributed a Simple Menu that works like the old Kmenu but it wasn't ready in time for 4.0, plus Raptor is being worked on but I don't know when its due."
    author: "Skeith"
  - subject: "Re: Sorry for some criticism  "
    date: 2008-01-15
    body: "I'm most definitely looking forward to trying out Raptor."
    author: "T. J. Brumfield"
  - subject: "Re: Sorry for some criticism  "
    date: 2008-01-14
    body: "Concerning the crashes: do you have compiz-fusion running with XGL? If you are using XGL then you won't be able to run KDE 4.0 for more than 5 minutes before having a crash because it has an xgl allergy. Set Xorg as default."
    author: "Bobby"
  - subject: "SuperKaramba Widgets in Plasma?"
    date: 2008-01-14
    body: "Is it now possible to add SuperKaramba widgets in Plasma or do I have to install SuperKaramba still seperately?\n\nBest Regards,\nmarrat"
    author: "marrat"
  - subject: "Re: SuperKaramba Widgets in Plasma?"
    date: 2008-01-16
    body: "I've managed to run on plasma superkaramba's supermonitor theme with little modifications, so probably others should work also. "
    author: "Adi1981"
  - subject: "Missing"
    date: 2008-01-14
    body: "I missed two things in KDE 4.0.0:\n- bluetooth:/\n- media:/\n\nDon't know if it's a fault of kubuntu packages or those features where just removed, but without them I can't really use KDE4."
    author: "Iuri Fiedoruk"
  - subject: "Re: Missing"
    date: 2008-01-14
    body: "media:/ had so many problems that it was deliberately killed off, IIRC.\n\nI don't know about bluetooth:/"
    author: "Anon"
  - subject: "Re: Missing"
    date: 2008-01-14
    body: "Exactly, media:/ was a pile of crap. It was conceptually broken so it's good they got rid of it.\n\nFor bluetooth:/, it's a 3rd party add-on and not part of the official KDE release so it may not be ported yet. It may be waiting for a rewrite due to bluetooth handling being intergrated into Solid."
    author: "Morty"
  - subject: "Re: Missing"
    date: 2008-01-14
    body: "How do you manage devices such as USB sticks in Konqueror now?\nIn Dolphin these devices are listed in the location bar. There is nothing similar in Konqueror now.\n\nIf media:/ was too broken, then there should be something else to replace it.\n\nFor me, this is an example why it is improbable that Konqueror will remain an option as file manager in the long run. There will always be pieces of code that are too hard to maintain. For things that cannot easily be shared between Dolphin and Konqueror, but need different implementations, I doubt that they will be developed for Konqueror.\n"
    author: "Meneteqel"
  - subject: "Re: Missing"
    date: 2008-01-14
    body: "You handle devices like USB sticks the way you are supposed to, mounted in the filesystem. The way disk always has been handled on *nix. Depending on your sistribution, removable media are located under /mnt or /media.\n\nSince the functionality already existed, the media:/ ioslave did solve anything. It only created a inconsistency in the filesystemhierarchy, by inventing virtual locations. "
    author: "Morty"
  - subject: "Re: Missing"
    date: 2008-01-14
    body: "I wonder how audio-cds are now handled. media:/ ioslave was IMO quite attractive way to handle cd ripping.\nWith data-cds and devices it was really conceptually broken and confusing in many aspects so it's good that it's now gone. Even if it means that ripping cds is now done with some external app."
    author: "PJ"
  - subject: "Re: Missing"
    date: 2008-01-15
    body: "I would guess like they have been in KDE for a very long time, from before the whole media:/ mess, with audio:/. No need to use one ioslave to redirect to anotehr. And since audi CDs don't have a filesystem to be mounted, creating a virtual one in this case is the sane soulution."
    author: "Morty"
  - subject: "Re: Missing"
    date: 2008-01-15
    body: "You're right. Tahe natural way would be to handle removable devices within the *nix file system. But when I look under /media I see the USB stick but I actually in Konqueror 4.0 I have no possibilities e.g. to umount the stick via the gui while in Konqueror 3.5 or in Dolphin's location bar you can right click on the stick and umount it."
    author: "Meneteqel"
  - subject: "not missing at all, replaced with much better"
    date: 2008-01-15
    body: "media:/ is replaced in several different way. Devices are listed in all File Open dialogs on the left. The \"Recently Plugged Devices\" plasmoid is on by default and is just a great feature in my opinion. Basically if you plug or insert something in, you can use it."
    author: "Ian Monroe"
  - subject: "knetworkmanager"
    date: 2008-01-14
    body: "Any idea when a KDE4-version of knetworkmanager will be available?"
    author: "yves"
  - subject: "Re: knetworkmanager"
    date: 2008-01-14
    body: "this functionality is going to be integrated in Solid. Think it may take a while... Don't hold your breath..."
    author: "Thomas"
  - subject: "gods, it's beautiful"
    date: 2008-01-14
    body: "I haven't been this excited about any sort of computer software in years."
    author: "Panther"
  - subject: "Great work!!"
    date: 2008-01-14
    body: "I really want to thank everyone who helped developing this project!!\n\nBut - there's one program that sux a bit: KMix. There are less switches and everytime I press somewhere, the switch is going up and my speakers are getting louder and louder.\n\nOkay, maybe the problem will be solved the next days!\nThanks again, KDE-team!!\nGood job!"
    author: "Philipp Lorenz"
  - subject: "conspiracy"
    date: 2008-01-14
    body: "I just realised that the train station clock is gone again... days before release it was there as an optional plasma applet, and now it's not even more in plasma extragear (judging by kubuntu's packages).\n\nWhat's going on?"
    author: "Marc"
  - subject: "Re: conspiracy"
    date: 2008-01-14
    body: "\"conspiracy\"\n\nI hope this title was tongue-in-cheek and not yet another manifestation of the \"KDE is sub-optimal in some area - the only possible explanation is some terrible GNOME conspiracy of the devs, etc\" way of thinking that seems to have infected the Dot as of late.\n\nAnyway, the train and fuzzy clocks reside in playground: \n\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/\n\nSince playground follows trunk, neither are guaranteed to compile against the 4.0 branch, and in fact they don't: they require libplasmaclock which was moved into kdebase following the 4.0.0 tagging:\n\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/CMakeLists.txt?view=markup\n\nThey do, of course, compile and run against trunk."
    author: "Anon"
  - subject: "Re: conspiracy"
    date: 2008-01-14
    body: "thanks for the 'conspiracy unmasked'\n\nI didn't think of the gnomes or interface nazis or stuff, it was only my impuls to look after the poor kind of underdog train clock, always in danger of being quietly killed by the bad guys 'cause it's just too cool for them... so i won't rest until it's safe ;-)"
    author: "Marc"
  - subject: "Locked widgets"
    date: 2008-01-14
    body: "\nIf I lock my widgets, I find that the option to change the desktop background simply disappears from the right click menu on the desktop.  This scares the hell out of me.  Does anybody know if this is a bug or a feature?"
    author: "Wheely"
  - subject: "Re: Locked widgets"
    date: 2008-01-14
    body: "http://lists.kde.org/?l=kde-panel-devel&m=119987761904476&w=2"
    author: "Hans"
  - subject: "Re: Locked widgets"
    date: 2008-01-14
    body: "Interesting thread, thanks.  I find I agree with all the posters though less so with Aaron which is a shame.\n\nIf, as Aaron says, the KDE developers are changing the way users think about desktops, that is fine though a little help might be nice as per the suggestion to change the \"lock widgets\" option to \"lock desktop\".  Shame really because I found this option useful for getting rid of the annoying, distracting boxes that appear around icons and other widgets but unusable for that purpose because it stops you changing anything else.\n\nSecondly, the point of the original poster was that the option to change background should not go away but be greyed out or something. I personally find it a fundamentally broken idea to add or remove items from menus according to context.  When you find some option in a menu someday which you want to go back to in the future, it is unbelievably annoying if you can not find where the hell it is because at some point you changed a completely different setting.  OSX does this a lot too and is why I now run Linux (via vmware) on both my macs. \n\nFor me, important information should be easily available and stay where it is.  At least a greyed out option tells you you have reached the right place but something else has to happen before it is useable.\n\nThanks again for the link.  Where this kind of thing ends up, for me at least, will be the deciding factor for wheher I have to give up my beloved KDE or not."
    author: "Wheely"
  - subject: "Re: Locked widgets"
    date: 2008-01-17
    body: "About the menus, you're reasoning makes sense. Knowing that you can do something  in some situations but are currently forbidden to do it for some reason is interesting information. I suppose the idea was to unclutter the contextual menus, but I think we're losing important info there.\nI second the \"distracting boxes around icons\" remark also : I appreciate being able to configure these icons this way, but I would prefer this box not to appear once I have configured the icon the way I want. Problem is that in order to do so, I have to lock everything, which is not desirable : I'm not spending my time configuring the desktop but using it, so I should be locking it all the time. However, everytime I would like to change something on it, I will need to unlock it first, which is really unecessary clutter for a desktop IMO. There are obviously aspect of the desktop you change more often than others. I don't really understand the need for the casual user to lock/unlock the desktop anyway : I've worked with KDE without ever locking my desktop for years. The main issue, IMO, is if locking becomes necessary to avoid some visual or usability annoyance, like this configuration box around icons. I think the whole idea should be rethought."
    author: "Richard Van Den Boom"
  - subject: "Slackware packages?"
    date: 2008-01-14
    body: "Do anyone knows if there are some Slackware packages out there? Thanks..."
    author: "Eduardo"
  - subject: "Re: Slackware packages?"
    date: 2008-01-15
    body: "Sorry, I'm not providing Slackware packages anymore.\n\nI did it for many years and I still use Slack and KDE, but I do have no time to build packages anymore."
    author: "JC"
  - subject: "Re: Slackware packages?"
    date: 2008-01-17
    body: "You can find link to packages from rworkman (part of Slack team) here :\n\nhttp://rlworkman.net/pkgs/current/TESTING/packages/kde-4.0.0/\n\nThey cannot be installed alongside KDE3, you have to replace.\nHere are others you're supposed to be able to install alongside KDE3, but there's no guarantee.\n\nhttp://slack.bglinux.org/EXPERIMENTAL/kde-4.0.0/"
    author: "Richard Van Den Boom"
  - subject: "Re: Slackware packages?"
    date: 2008-01-17
    body: "I'm not sure that he is part of the Slack team.\n\nBut at least you have packages for Slackware"
    author: "JC"
  - subject: "Great interest on KDE"
    date: 2008-01-14
    body: "According to trends.google.com, searches for KDE more than doubled on the release day. Searches for \"KDE 4\" and \"KDE 4.0\" more than quadrupled."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "KDE, Gnome, Beryl vs Compiz"
    date: 2008-01-15
    body: "checkout KDE, Gnome, Beryl vs Compiz\n\nhttp://www.google.com/trends?q=KDE%2C+Gnome%2C+Beryl%2C+Compiz&ctab=0&geo=all&date=all&sort=1\n\nAs of now KDE is in the lead :)\n\n"
    author: "Dr No"
  - subject: "stupid"
    date: 2008-01-15
    body: "What were you guys trying to emulate?  GNOME 2.0?  Sorry, but pathetic...  \n\nCouldn't you just have ported KDE 3.5 to Qt 4?  How hard would that have been?"
    author: "KDE User"
  - subject: "Re: stupid"
    date: 2008-01-15
    body: "nobody tries to emulate gnome 2.0\nIt's actualy a compliment to say that, because the reason why kde 4.0 has less features than kde 3.5, is because everything had to be written from scratch (or nearly).\nThis means, a feature incomplete release of kde resembles a full gnome release, quite a compliment.\nBut don't worry, the kde 3.5 level will be reached somewhere around 4.1 or 4.2"
    author: "Beat Wolf"
  - subject: "Re: stupid"
    date: 2008-01-15
    body: "LOL, that was a good one."
    author: "Bobby"
  - subject: "Re: stupid"
    date: 2008-01-16
    body: "+1 insightful."
    author: "yman"
  - subject: "Re: stupid"
    date: 2008-01-15
    body: "What were you trying to emulate? TROLL 2.0? Sorry, but pathetic... \n\nSerious answer: I can understand the disappointment if you thought KDE 4.0 would be ten times better than 3.5. However, I don't approve your way to criticize.\n\nRead some of the comments here, blogs (see planet.kde.org) etc. and hopefully you'll understand. For your convenient, here's a snippet from a comment by Aaron J. Seigo:\n\n\"> Third half of the customization options from KDE3 are missing.\n\ni call bullshit on this one. if you mean \"half the customizations from kicker\", yes. if you mean \"half the customizations from kdesktop\" your probably not right (haven't counted exactly, but i'm pretty sure plasma is currently beyond its feature count). if you mean \"half the customizations of all kde together\" you're wildly wrong as there are new features all over the place.\n\nso... kicker features: coming.\n\nin the meantime, you may enjoy life more by keeping some perspective.\""
    author: "Hans"
  - subject: "which dist is best for kde4?"
    date: 2008-01-15
    body: "currently using ubuntu, but there for example are no icons in the apps menu.\njust generic ones, little bit annoying.\n\nwhat can you recommend?\ni tried suse, but I'm confused about the zillion different packet managers"
    author: "asdf"
  - subject: "Re: which dist is best for kde4?"
    date: 2008-01-15
    body: "update your ubuntu kde 4 packages. There has been a update today, and since then i got all icons"
    author: "Beat Wolf"
  - subject: "Re: which dist is best for kde4?"
    date: 2008-01-15
    body: "Good to know. Thanks.\n\nWonder when they will come out with a new Alpha that will have more of KDE 4.0 in it.\n\nWhat other distros are great for KDE 4.0 I think I'll defect from (K)ubuntu for a while, while they treat KDE as their red-headed stepchild. See what else is out there..\n\n"
    author: "Steve M"
  - subject: "kubuntu"
    date: 2008-01-16
    body: "In my experience the Kubuntu packages have been great. You don't have to use an alpha or something, just use the Gutsy backports."
    author: "Ian Monroe"
  - subject: "MacOS Style Menu"
    date: 2008-01-15
    body: "Where is the \"Menu at the top of the screen\" (\"MacOS style\") option gone ? Is it another feature to add to the (already long) list of \"Things that couldn't be implemented on time for the 4.0 release\" ?\n"
    author: "Keiran"
  - subject: "Re: MacOS Style Menu"
    date: 2008-01-15
    body: "Yes."
    author: "Anon"
  - subject: "record"
    date: 2008-01-15
    body: "wow, more than 300 comments!\nmust be the new record..."
    author: "chris"
  - subject: "Re: record"
    date: 2008-01-15
    body: "http://dot.kde.org/1172721427/\n\n494, so far."
    author: "Anon"
  - subject: "Re: record"
    date: 2008-01-18
    body: "how do you know that? I couldn't find any mentioning of the number of posts on that page.\n\nanyhow, it's now 392, and still growing every day."
    author: "yman"
  - subject: "Re: record"
    date: 2008-01-15
    body: "And it's far from over..."
    author: "Bobby"
  - subject: "KDE 4.0 in the news (well online reviews)"
    date: 2008-01-15
    body: "http://osnews.com/story/19159/KDE-4.0.0-Sweet-Follows-Sour\n\nhttp://www.linux.com/feature/124683\n\nPost other great reviews when you find them :)"
    author: "Steve M"
  - subject: "My thoughts on KDE 4.0"
    date: 2008-01-15
    body: "OK, I've used KDE 4.0 for a few days now, here's what I think about it:\n\n- general:\nKDE 4.0 starts up very fast (even without prelinking) and feels more responsive than 3.5 - great!\n\n- Konqueror:\nThat's the part of KDE 4.0 I'm really disappointed with. Not because it's not the default filemanager anymore - I quite like Dolphin. But I thought that, since Konqui is now focused on webbrowsing, it would see improvements in that area. But right now it feels that it got ported to Qt4 and nothing else was done. Maybe there were other changes under the hood, but I don't notice them. Browsing the web with Konq feels the same as in 3.1 days - it's still slow, compared to Firefox, better cookie handling is needed ( http://bugs.kde.org/show_bug.cgi?id=68582 ), it doesn't like Flash (crashes, or opens every Flash-Element in a separate window).\nWhat happened? Are the Konq-Devs pissed off, because of the endless KHTML vs WebKit discussion?\n\n- Plasma:\nOK, you can tell that the development started too late, it feels unfinished from a users point of view. I trust the devs, that the underlying technology is the groundwork for really amazing stuff, but I think it's fair to criticise the way it has been promoted. Promising the holy grail of desktop computing and presenting an ugly, unconfigurable panel and a desktop you can put SVG-clocks on (yep, for a user it's not much more than that) is rather disappointing. Let's wait for 4.1 and see what plasma holds for the users.\nApart from that, I quite like Kickoff - I don't understand, why people are bashing it so much. For me, it's a real improvement compared to the old menu.\n\n- Dolphin:\nNice! The only thing I currently miss is a decent treeview. The \"Folders\" box only shows a tree of the current directory, is it possible to change it to a root tree?\n\n- Oxygen:\nI think it looks beautiful and the artists did a very good job. I can't understand the ones, who complain that it looks like Vista - it doesn't.\n\n- KWin:\nThe best Part of KDE 4.0 IMHO. The compositing features are simply great. I've tried compiz/beryl a few times, but was never quite happy with them. They crashed a lot and didn't work well with KDE. KWin runs stable and fast over here and behaves just like you would expect from a window manager. The present plugins are very useful, I don't need much more. For my taste, KWin already surpassed Compiz. Very well done!\n\n- Sonnet:\nDoes it still exist? See http://dot.kde.org/1200050369/1200289711/\nIt would be great, if a Developer could shed some light on this.\n\n- Strigi:\nI read somewhere that metadata is now extracted with strigi. There doesn't seem to be a strigi-based search tool, though. Are there any plans for something like that?\n\nOne thing I would like to try out is Phonon, but I'm currently unable to get my soundcards to work with ALSA. Choosing the desired soundcard based on application type/message type sounds promising, though.\n\n\nAnyway, thanks to all the people involved for their hard work!"
    author: "Sepp"
  - subject: "Re: My thoughts on KDE 4.0"
    date: 2008-01-15
    body: "> - Dolphin:\n> Nice! \n\nThanks in the name of all Dolphin developers :-)\n\n> The only thing I currently miss is a decent treeview.\n> The \"Folders\" box only shows a tree of the current directory,\n> is it possible to change it to a root tree?\n\nThis is on my TODO-list for 4.1, as this has been requested by some people already and makes sense for drag & drop operations."
    author: "Peter Penz"
  - subject: "Re: My thoughts on KDE 4.0"
    date: 2008-01-15
    body: "\" This is on my TODO-list for 4.1, as this has been requested by some people already and makes sense for drag & drop operations.\"\n\nWhat about spring-loaded folders? :)"
    author: "Anon"
  - subject: "Re: My thoughts on KDE 4.0"
    date: 2008-01-16
    body: "I recommend Firefox over Konqueror for the Internet. It pretty much has the market anyways. No need to develop for Konqueror, as firefox is used by most for webbrowsing these days. Konqeror works fine for the rest, who don't like Firefox, Opera, Safari, etc.\n\nDolphin is great.. :D"
    author: "Max"
  - subject: "I disagree"
    date: 2008-01-17
    body: "Konqueror provide me with services I can't get from Firefox (KDE integration, IO/slaves allowing to deal with distant volumes just as local ones). It has had spelling check years before FF and is for me faster than FF on most sites I'm using.\nI certainly hope there will be continuing work on Konqueror at least as a browser, whether it uses KHTML or Webkit. I find it a much more productive tool than FF (and quite lighter in memory usage)."
    author: "Richard Van Den Boom"
  - subject: "Re: I disagree"
    date: 2008-01-17
    body: "Right, my original plan was to switch back to Konqueror, once 4.0 is out, because of these reasons."
    author: "Sepp"
  - subject: "Re: I disagree"
    date: 2008-01-19
    body: "On the subject of Webkit and memory usage, my brother tells me that Safari on his Macbook can consume an incredible amount of memory after a few hours.  I wonder if Konqueror with Webkit will also be very memory hungry."
    author: "Danny"
  - subject: "Worst KDE release ever"
    date: 2008-01-15
    body: "I really do not understand why people claim this release to be so radical new and better. It is not - for me its the the worst ever, and i have used KDE since 97/98. I Am pre 1,0 user.My experience is \n* Its choppy and unresponsive compared to KDE 3.5.8 (even to Gnome)- compiz works fine on this machine\n* Dialogs are overloaded  (Too much stuff). Example Press Alt-F2 and type kate - i get three (3) choices plus 4 buttons i can click on! \n*Session chooser dialog - after starting kate and pro. WTF? its just a freaking editor (Koffice opening dialogs suck as well). If i choose kwrite i do not get same dialog. Hmmmm.... where is the consistence?\n* Look and feel is horrible. If i wanted Vista i would have bought it.I use Windows (XP)at work and do not want to use Windows at home. Do a fucking study of your users.\n* Kickoff. What a joke. SUSE can do all the study's they want but for a experienced KDE user the Launch/Start-button thing is extremely bad. Its bad  bad bad bad.. if i choose \"applications\" i get a freaking scrollbar!!! It looks like Windows XP's default one  and anybody that has ever supported end users  can tell you that it is too freaking complex. Its irritates me as a experienced user .\n\nHopefully this mess will be resolved someday. Im on Kubuntu - thats probably not the best KDE distor out there but my guess is that lots Ubuntu/kubuntu KDE users will be using Gnome as desktop and KDE apps underneath. Its a shame since I never really could get used to Gnome - unfortunately KDE4,0 is extremely bad usability wise so one may get forced to it.\n\n"
    author: "Jos Andersen"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-15
    body: "Why don't you do the fucking study then, seeing as you have all the answers?\n\nIf you don't like it, go back to 3.5.8, have a coke and a smile.  Quit the negativity and talk about some good stuff too.\n\nThere's nothing wrong with Kickoff.  Play with it, get used to it and stop being such a jackass."
    author: "Phil"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-15
    body: "You're the jackass. Kickoff sucks and no talking is going to make it better. This release is an embarrassment and i will keep on using 3.5.8 and dont drink coke. So will many others - i dont see anything positive yet - maybe in 4.1 - let's see."
    author: "Jos Andersen"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-15
    body: "> Kickoff sucks and no talking is going to make it better.\n\nCrying \"it sucks, it sucks\" won't improve things, either. As I said in my above post: I'm quite happy with Kickoff. Just because you don't like it, doesn't automatically mean that everybody hates it."
    author: "Sepp"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-15
    body: "You're right - it does not help to say \"it sucks\". Others have pointed out long ago that Kickoff (and XP style menus) are a bad idea, because they introduce complexity to simple tasks. Especially Seigo has had very agressive answers to those critics. I can guarantee you, based on longtime experience with end-users, that \"they\" will not understand kickoff - they do understand the original kicoff - the XP menu - and certainly not the copy. Off course unless its intended for experienced users and they are probably satisfied with the traditional one ...... unless hte \"study\" forget to define what the users of kickoff should be."
    author: "Jos Andersen"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-15
    body: "There is no start menu presently that I like as much as Kickoff. I can't understand the complains about Kickoff in KDE 4.0. They complained and said that they wanted the old menu back so the devs gave them the choice. There are Kickoff, Lancelot and the traditional start menu to choose from. They are mere plasmoids (widgets), if one doesn't like Kickoff then delete it from the taskbar and drag and drop another start menu in it's place. It's that easy :)"
    author: "Bobby"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-15
    body: "Why don't you just switch to the traditional menu then, instead of showing of your ignorance and embarrass yourself in writing. It's right there you know, for people like you."
    author: "Morty"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "he doesn't like it, he's allowed to express that, but I am sure some of you people would prefer only posts that kiss the kde ass.  "
    author: "Richard"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "Critics? ... yeah.. it shows..\nGet the facts: There have been complaints by people not liking Kickoff and prefering the old-style menu. The devs listened to these voices and delivered KDE4.0.0 with the choice of both menus (again.. got it? KDE4.0.0 includes _both_ menus.. you can actually _choose_, which one you want to use) Now... you're free to ask how to switch to the old-style menu, but insultive bahaviour is not tolerated."
    author: "Thomas"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "Ditto"
    author: "Bobby"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "There two things :\n\n- Aspects that objectively need improvement, like bugs and faulty behaviour. \n- Personal taste, like Kickoff.\n\nMixing both is not the most efficient way to make a point, and adding insulting words and \"freaking\" all the time, because you don't want to write \"fucking\" is not only insulting but completely ridiculous. People should try not to write as they talk.\nOn top of that, making personal taste critics, while alternatives probably more compliant with the expressed taste are already available (like for Kickoff), is either dishonnest or just plain lazy, but there's no wonder they are not welcomed, especially when expressed with rudeness. It looks like the spoiled child who didn't get the toy he expected for Christmas and then make a fuss of it, instead of trying to look positively to what it got.\n\nIt has nothing to do with kissing KDE ass, but everything to do with good behaviour, politeness and taking into account people's feelings. If you don't understand the difference, there's no way I can explain it to make you do.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "To be honest, I'm just tired of constantly reading that people don't like Kickoff.  Some do, most don't.  In that case, fine - switch back to the old style.  If you hate it so much, then you go ahead and design something - even just a mockup - showing what you think an application list/chooser/panel/dock whatever should look like.  Don't start throwing \"f\" words in there just because you don't like something.\n\nAlso I'm not a KDE kiss-ass, I can see plenty of areas for improvement but at least I'll try to be objective about it and offer constructive criticism.\n\nAs for the idea of a usability study, sounds good, if you can find someone who has the time (and the inclination to do it all for free) to get 50 people together for days to provide a detailed spec to aseigo et al as to how \"the people\" want to have on the bottom corner of their desktop.  Good luck with that."
    author: "Phil"
  - subject: "I suppose you were ansering the parent..."
    date: 2008-01-16
    body: "... as my own post was to contradict the poster who dared to have the same first name as me! ;-)"
    author: "Richard Van Den Boom"
  - subject: "Re: I suppose you were ansering the parent..."
    date: 2008-01-16
    body: "Yep, Richard VDB - I was answering the guy who criticized my post about the first post and agreeing with you ;-)"
    author: "Phil"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "\"Its choppy and unresponsive compared to KDE 3.5.8 (even to Gnome)- compiz works fine on this machine\"\n\nIn what way is it choppy and unresponsive? Specific tasks?\n\n\"Session chooser dialog - after starting kate and pro. WTF? its just a freaking editor (Koffice opening dialogs suck as well). If i choose kwrite i do not get same dialog. Hmmmm.... where is the consistence?\"\n\nProbably because they're different applications that do different things?\n\n\"Look and feel is horrible. If i wanted Vista i would have bought it.\"\n\nWhat makes you think it looks like Vista? Oh, yer, right. It has a black taskbar.......\n\n\"Kickoff. What a joke. SUSE can do all the study's they want but for a experienced KDE user the Launch/Start-button thing is extremely bad.\"\n\nI have to agree really. Kickoff is shite. The sooner Raptor gets rolling and we have something different the better, because both the SLAB and Kickoff menus have been embarrassing considering they were supposed to be the result of extensive usability studies."
    author: "Segedunum"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "It's garish gradients.\n\nAesthetically, the default KDE look and feel has always been too \"in your face\" for a lot of people.  I really a prefer a classic, flat grey, win9x or motif look and feel.\n\nPeople use KDE in spite of how it looks, not because of it."
    author: "Velvet Elvis"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "Don't generalize : many people actually like KDE also because of how it looks (I do) and are actually pleased by the new looks.\nOn the other hand, most people I know find Motif apps completely old-fashion and hideous. \nI don't know on the whole what people prefer in the majority, but trend is going on all desktop OS more in the current KDE looks way than Motif's, so I don't think we should blame KDE devs for that. I find the news looks actually a lot sober than Keramik was, for instance, so you should actually be pleased by the taken direction."
    author: "Richard Van Den Boom"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "I'd blacked out the memory that was Keramik. Thanks.  I'm going to need therapy now.\n\nI'm presently using serenity with one of the Amber/Beige color schemes, Buuf icons and a Geiger background.  I like minimalistic, low contrast, earth tones and as organic a look and feel as possible.  If my UI must look like something out of a sci-fi movie, make it Cronenberg.\n"
    author: "Velvet Elvis"
  - subject: "Sorry :-)"
    date: 2008-01-16
    body: "Some deeply-buried troubles must come out at some point anyway. ;-)\nActually, I'm sure that Oxygen will be OK for you with some tuning. It is not very flashy after all. I guess Plasma will be themable in the future and you can always avoid to put plasmoid everywhere if you like minimalistic desktop.\nAnd I seem to remember a comment that the Oxygen style worked well with different colour sets (didn't try it myself yet)."
    author: "Richard Van Den Boom"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-17
    body: "yea.. I love the new look..\n\nStill haven't customized it.\n KDE 3.5.8 I changed everything the first day, because it looked so dated. That's the beauty of open source though. I CAN DO THAT!!!\n\n"
    author: "Max"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "\"In what way is it choppy and unresponsive? Specific tasks?\"\n\ni like kde 4.0, but i still want to answer that question.\nEverything that has todo with scrolling is VERY slow, to the point where i just quit kde 4.0.0 after a few minutes because my computer feels so slow. But i heard that this could be a problem with my nvidia driver, but i'm not sure, so i will wait for kubuntu 8.04 to see what the real problem is"
    author: "Beat Wolf"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "> But i heard that this could be a problem with my nvidia driver,\n\nI strongly suspect so.  Apparently the very latest driver from NVidia's website improves things somewhat."
    author: "Robert Knight"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "> But i heard that this could be a problem with my nvidia driver,\n\nI strongly suspect so.  Apparently the very latest driver from NVidia's website improves things somewhat.\n\nI get good performance from my four-year old laptop with an old Mobility Radeon."
    author: "Robert Knight"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "It is a driver problem. I had the same problem before using a few tips here and doing a few adjustments to the xorg.conf file. The effects and everything are really smooth now. \nI have only one ptovlem and that is that the taskbar flickers and sometimes become transparent when I am writing in Firefox.\n\nBtw. KDE 4 is surprisingly stable  no crashes so far :)"
    author: "Bobby"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "\"The sooner Raptor gets rolling and we have something different the better, because both the SLAB and Kickoff menus have been embarrassing\"\n\nIf you prefer the traditional menu over Kickoff, I don't think you will like Raptor. From the mockups and plans I saw it goes even further to make starting of most used apps faster and to make browsing all apps slower."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: The best KDE release ever"
    date: 2008-01-16
    body: "I would say that this is maybe best KDE release ever. It has needed so much long-sighted work and vision. It's great to see that even open-source projects like KDE can work like this.\n\nI think that most complainers are seeing only the minor problems that kde4 has and they don't see the whole picture. It's much more easy to notice problems in ui than ie. conceptually better ways to do things. And most complainers don't have the patience to test kde4 for more that few minutes and after that they think that they know everything about it.\n\nFor example, I just used the new \"New Device Notifier\" for the first time and it's just great. It is much better way to handle new devices than those popups in kde3.5. If one tests kde4 for just few minutes, he/she won't discover things like that.\n\nI myself use kde4 from svn and I just love it. It has some rough edges, but it sure feels better and better all the time when I use it and I just keep noticing great improvements over kde3.5."
    author: "Anonymous"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "Choppy : Dragging windows around, Dialogs opening. \nThere are some claims that this is due to nvidia drivers. I do in fact use a old nvidia card. Nevertheless things work fine in Gnome and before that with compiz and beryl with exactly the same card and same xorg.conf. So it must be a KDE issue.\n\nSession chooser dialog: So you think that writing kate-kde4 in konsole should give me a choice about choosing \"default session\" or buttons New Session or Open Session ot whatever? This is at least confusind and unnecessary.\n\nLike vista: Well...i think it looks like Vista. I dont want it to look like Vista or behave like Vista or whatever; I want KDE!\n\nKickoff: As someone that has done enduser support for years i find it pathetic that Seigo pulls some SUSE/Novell study that shows that kickoff is *the answer\". Get real - real people dont even know how to right-click and now they have to use a menu system with a freaking scrollbar. ITs very very sad. \n"
    author: "Jos Andersen"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "I forget the logout dialog. Why should a user confirm twice that he wants to logoff/shutdown/... ? First in Kickoff and then and a second confirmation dialog after choosing \"leave/exit\". This does not make sense and proves that someone has not been thinking but just coding.\nI hope this crap is resolved in 4.1 "
    author: "Jos Andersen"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-17
    body: "Little troll didn't have anything to do?\n\nYou have no idea what are you talking about, you may don't like Kickoff, but normal people, not you nor me, a hell lot more than the Classical Menu, which, just by it looks, make them feel in 1995.\n\nThe dialog is known issue, thanks.\n\nThe NVidia driver issue, is that, a issue in the driver, NVidia people, instead of bashing and saying: GNOME didn't say anything, it most be your problem, tried to get a solution.\n\nKate my bro, isn't made for you, there it's kwrite if you only want to do .txt"
    author: "Luis"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "I personally love the look of KDE 4.0\n\nIt looks hip and modern.\n\nI wish it were more cutting edge, and even more modern, but there were too many crybabies I guess that can't adjust to change. Hence only the small evolutionary change, instead of the great revolution KDE 4.X could have been.\n\nReasons like this made Microsoft make Microsoft Bob. Turns out people were just TOO LAZY to learn how to use a computer and adjust to change. Msft Bob proved to a failure and unnecessary. The crybabies were just a vocal minority, and were just too lazy to learn new things..\n\nFor people who don't like the 4.0 branch. Keep using 3.5.8 it will be maintained for a couple more years, so there is no need to fear.. Then when you are ready to join us in the present we will welcome you to the KDE 4.X.Y branch. By then you might have adjusted to the change.\n\nI hate it when people cry about wanting innovation, and then are just too scared to use it. They should just say \"I want more of the same\" It would sure make life easier for designers, product engineers/developers, product managers, CEO's...\n\n"
    author: "Max"
  - subject: "Re: Worst KDE release ever"
    date: 2008-01-16
    body: "Creatures of habits. The most people hate to think and even more to research :)"
    author: "Bobby"
  - subject: "Re: Worst KDE release ever"
    date: 2008-02-01
    body: "Jos, and everyone, you should read http://www.kdedevelopers.org/node/3174  Please read this before everyone just flies off the handle.  \n\nKDE 4 is intended as a technology release.  Yes, it is incomplete.  Even the developers acknowledge that.  KDE 4.0 is the base platform for new KDE development.  It is not intended to replace KDE 3.5.x until it has matured significantly.\n\nI apologize if this seems rude, but I for one wish people would actually read developer's notes before they scream.\n\nBest wishes,\nT.J."
    author: "T.J."
  - subject: "Konqueror and CSS"
    date: 2008-01-16
    body: "Maybe this is just me (openSuSE 10.3; single-click install of KDE4.0); but when i use KDE4/Konqueror to view (for example) http://www.kde.org, then apparently CSS is disabled and all <div>s are rendered flat, i.e. vertically.\n\nAm i missing something obvious?\n\nConfused now."
    author: "Robert"
  - subject: "Re: Konqueror and CSS"
    date: 2008-01-16
    body: "Weird. Someone else also reported something like this in bugzilla. Obviously not the case here. Any chance you have some odd proxy, add blocking, accessibility settings, etc., on?\n\nHmm, I guess I should do this with the bug report and not here.\n"
    author: "SadEagle"
  - subject: "Re: Konqueror and CSS"
    date: 2008-01-16
    body: "I'm using KDE4 with a test account and default settings in parallel to KDE3, whose Konqueror has CSS enabled as always. I have never used a proxy at home. Now, ad blocking and accessibility settings? Then those would be enabled by default. Are they? How would i switch these off?"
    author: "Robert"
  - subject: "Re: Konqueror and CSS"
    date: 2008-01-17
    body: "They shouldn't be, but perhaps it's some distro tweak (though seems doubtful)\nSettings->Configure Konqueror->Stylesheets should have \"Use default stylesheet\".\n\nThe \"Web Browsing\" item should have \"Automatically load images\" checked, \n\"AdBlocK filters\" should have \"use filters\" disabled...\n\n\n"
    author: "SadEagle"
  - subject: "Not impressed"
    date: 2008-01-16
    body: "I've been a huge fan of KDE since I first installed 2.1 on a PC years ago. There's even a few lines of my code here and there. However, I'm very disappointed with KDE4.\n\nI know, I know, it's a starting point. But it's not just bugs and missing applications that are bothering me, it's the whole design. For example, I can't find any way to shrink the taskbar. It's absolutely *huge*. So is the clock. The K Menu replacement makes absolutely no sense at all.\n\nIt looks like you've gone down the Gnome path of removing options everywhere, because you know what the user wants better than the user, but at least Gnomes default configuration is usable. This is just a mess. Seriously, what hardware & eyesight do the designers have? Were they sitting 6 feet away from their monitors or something?\n\nI've gone back to 3.5 and uninstalled 4 for now. Hopefully some of the insanity of this release will be fixed for 4.1."
    author: "Stephen"
  - subject: "Re: Not impressed"
    date: 2008-01-16
    body: "I have read complains about the taskbar at least 100 times in this thread and it has been explained at least 1000 times why it's the way it is. People, would you mind doing a little reading before posting the same thing over and over again?\n\nI was expecting people to complain about stability issuse but I haven seen any of those until now, only petty, cry baby things like Kickoff, the size of the taskbar and similar stuff.\nIt's hard to believe that people reduce a DE to these few functions."
    author: "Bobby"
  - subject: "Re: Not impressed"
    date: 2008-01-17
    body: "I find it hard to believe people define their DE by the composite effects.\n\nI agree that going on about the task bar size is a bit over the top considering it has been explained many times and that it will change.  I suspect that currently, plasmoids can't be resized dynamically hence the scroll bar in kickoff and the lack of a resizing task bar but I presume this will change.\n\nI do understand complaints about KickOff though as it is a design issue rather than a functional one and the more people express their views about it the easier it is for the development team to make decisions about its future."
    author: "Wheely"
  - subject: "Re: Not impressed"
    date: 2008-01-17
    body: "You find it hard to believe people reduce a DE to a few functions, like a panel/taksbar, a start menu, a control panel...? Isn't that exactly what a DE is? Those are certainly what I use a DE for, and they simply aren't up to scratch at the moment compared to KDE3.5. Sure, it has a load of bells and whistles, but those aren't core DE functionality. "
    author: "Stephen"
  - subject: "Re: Not impressed"
    date: 2008-01-19
    body: "I was referring to the missing functions/features of these components that are complained about. What you have stated is all there, maybe not as it is in KDE 3.5x but it's there and it works fine for me. i am also willing to wait on the missing features while enjoying the new and excited ones in KDE 4.0. \n\nIf you are that dissatisfied Stephen then just continue to use KDE 3.5.8 and wait until KDE4 is good enough for you. The developers have given people the same advise. KDE 3.5x is still a wonderful DE :)"
    author: "Bobby"
  - subject: "KDE 4.0 Build issues"
    date: 2008-01-16
    body: "For all previous KDE releases I have built from source.  The results have always been superior to distribution packages.  My impression, so far of KDE 4.0 has come from the Kubuntu packages so I decided to build the thing, from scratch on a Slackware 11 box that doesnt even have dbus or HAL on it.\n\nMy question:  The build instructions on kde.org appear to be for svn versions of KDE4.0 and not for the stable release tar balls.  Does anybody know if there are up to date build instructions or should I just use what is there without the svn stuff?  I really want to build what has been released, not what has been fixed since.\n\nI have a lot of the stuff built OK and am just about to build the first KDe package.  However, I notice in the README that building qt4 yourself is discouraged and that building dbus yourself is very much discouraged. I also note the bewildering number of libraries I needed to build too ( I always liked that KDE had few of them and gnome had billions of the little buggers).\n\nI get the impression that from now on, we are being encouraged to install binaries from our distribution of choice.  I hope this is not the case and it is not that us techies who have been here from the start are not the target audience any more."
    author: "Wheely"
  - subject: "Re: KDE 4.0 Build issues"
    date: 2008-01-16
    body: "I've built KDE4 from source using Slack-current without too much fuss.\nI still recommend you to start using Slack 12 (with Dbus and HAL) instead of 11. These can be a pain to build and honestly, you just hurt yourself trying.\nFrom a Slack 12, here's what to do :\n \n * Compile QT4 of course (see Slackbuilds.org http://slackbuilds.org/repository/12.0/libraries/qt4/). You should add to the package copies of the /etc/profile.d/qt.* files, renamed to qt4.sh and qt4.csh, where the Qt path in the scripts are changed to \"/usr/lib/qt4\". Otherwise, you'll have nasty surprise at first startx.\n \n * boost : http://www.boost.org. I needed to had a symlink /usr/include/boost to /usr/include/boost_1_34_1/boost, otherwise extragear-plasma would not compile because some \"feed\" header in \"syndication\" (from pimlibs) seeks a seemingly hard coded \"boost/shared_ptr\" there.\n \n * Strigi : http://strigi.sourceforge.net/\n Strigi normally uses Clucene (http://clucene.sourceforge.net/index.php/Main_Page) but it did not compile for me though I did not try very hard. It's probably useless without it, but right now, I'm essentially interested in getting KDE4 start the quickest way, even if some features are not available.\n \n * Soprano: http://soprano.sourceforge.net/\n \n * Blitz : http://sourceforge.net/projects/qimageblitz\n \n That's all I was asked as mandatory libs. Some others did miss but they provided functionnality I did not need (yet).\n To compile the Cmake-based projects, I basically replaced in my Slackbuilds the \"configure --prefix='/usr' --sysconfdir='/etc/kde'\" by \"cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -DSYSCONF_INSTALL_DIR:PATH=/etc/kde\".\n \nGood luck!"
    author: "Richard Van Den Boom"
  - subject: "Re: KDE 4.0 Build issues"
    date: 2008-01-16
    body: "Thanks very much for the detailed info.\n\nI will consider a Slack 12 install on the machine.  I have a Slacky 12 install here sitting in a vmware VM but as my Kubuntu VM chokes horribly on the whole composite scene I decided I needed to go physical.\n\nOn my 11.00 box I found dbus compiled without much fuss though I have not got to Hal yet. Thanks for the qt4 tips, I have qt4 built (twice as I didnt have dbus the fist time :)) but perhaps I will just start again from a slackware 12 install as you suggest and follow your instructions.  Two questions though,\n\n1) Did you install your 3.5.* from the Slackware packages? I tend to do a bare bones install and then spend a week building all the stuff I want afterwards.  It may be that you already have stuff I havent got.\n\n2) If I have read correctly, the correct order, once everything is in place is kdesupport, libs, base and then the rest (glad arts is no longer required).  Is that right?\n\nThanks again for the reply."
    author: "Wheely"
  - subject: "Re: KDE 4.0 Build issues"
    date: 2008-01-17
    body: "I used to build KDE packages myself but for 3.5.7 and 3.5.8, for some reason, they would not work, while Slackware's ones would, so I used them recently.\nI install some personnal packages, like Codeine, transcode or ffmpeg but nothing, I guess, that could be considered a necessity for KDE.\nrlworkman, one of the Slackware contributors, has released on his website for testing KDE4 packages and slackbuild, if you want, including all the dependencies :\n\nhttp://rlworkman.net/pkgs/current/TESTING/\n\nMaybe it's better to start from his work, he has tried to be much more extensive that I did. :-)\nAs for the order, yes that's kdesupport, libs, pimlibs, base then the rest."
    author: "Richard Van Den Boom"
  - subject: "Re: KDE 4.0 Build issues"
    date: 2008-01-17
    body: "Thanks again.  Appreciate it!"
    author: "Wheely"
  - subject: ":/"
    date: 2008-01-17
    body: "Faster, smoother, but 2/3 of features missing. A great technology demonstrator. As always, I'll switch to it around 4.0.5. Good work. "
    author: "szlam"
  - subject: "Re: :/"
    date: 2008-01-17
    body: "you are not totally right. you can say 2/3 of kickers features are missing in the plasma panel. a few features from kdesktop are missing. some features from the rest of KDE3.5.8 are missing.\n\nbut ... faster, smoother, many new features, better look ... \n\nbig thanx to all developers. can't wait for 4.1"
    author: "hias"
  - subject: "Is KDE 4.0  64 bit? All I can find is 32 bit"
    date: 2008-01-17
    body: "Is KDE 4.0 64 bit capable? All the live CD's I found so far are 32 bit.\n\nI have an AMD Athlon 64X2, and part of the reason I switched to Linux is because it genuinely supports my 64 bit processors. (Unlike Vista, which came installed as a 32 bit version) Hopefully my question is not too n00b, but I just started with Linux (Kubuntu, I was told it's popular, but I can use other distros, openSuSE? Whatever works with KDE 4.0 I'm not picky.)\n\nCould someone please help me with a 64 bit live CD? (if there is such an animal)\n\n"
    author: "John Stemos"
  - subject: "Re: Is KDE 4.0  64 bit? All I can find is 32 bit"
    date: 2008-01-17
    body: "There are amd64 binary packages for Debian.\nhttp://packages.debian.org/experimental/kdelibs5\n\nIt seems that the Debain KDE maintainers are currently preparing a new live cd for i386 + amd64:\nhttp://pkg-kde.alioth.debian.org/kde4livecd.html\n\nThey did an awesome job so far, so I guess it shouldn't take long..."
    author: "Sepp"
  - subject: "Thanks, but..."
    date: 2008-01-18
    body: "I think I may wait until 4.1 until they fix the many bugs. \n\nLets change it to \"Release early when ready, Release often when ready\". By ready I mean, no bugs, all features, live up to expectations.\n\n*Gets excited and waits another 6 months(Or whenever)*\n"
    author: "Jeremy"
  - subject: "Re: Thanks, but..."
    date: 2008-01-18
    body: "You do know that bug-free software does not exist, right? (Well, bug-free non-trivial software anyway.)"
    author: "Paul Eggleton"
  - subject: "Re: Thanks, but..."
    date: 2008-01-18
    body: "Well, i just wait until my distribution comes with a new version that includes kde 4.x.\nStable or not, complete or not, bugfree or not, a desktop experience is always at best when it is well integrated with the operating system.\nAnd just like with kde 3.0 and 2.0, that won't be the case until the next release of the distribution."
    author: "whatever noticed"
  - subject: "Re: Thanks, but..."
    date: 2008-01-18
    body: ">> By ready I mean, no bugs, all features, live up to expectations.\n\nhttp://aseigo.blogspot.com/2008/01/talking-bluntly.html\nMeme 3. But you might as well read it all when you're at it."
    author: "Hans"
  - subject: "Re: Thanks, but..."
    date: 2008-01-18
    body: "How about no regressions?\n\nThat's finite."
    author: "Anon"
  - subject: "Re: Thanks, but..."
    date: 2008-01-18
    body: "Does this not require some kind of usecase definition around usage and  functionality - both previous functionality and KDE 4.0 functionality. Unfortunately i doubt there has been much thinking about those. If this happened then maybe thinking about both software defects and software bugs could be incorporated.\nMaybe testing is the way where endusers that cannot code could make themselves important when software is released"
    author: "Jos Andersen"
  - subject: "kde 4 not bad not good :P... a must"
    date: 2008-01-18
    body: "1. kde 4 release as development platform for next 3-4 years is a must... just remember what kind of mess is going round in 3 branch because of old stuff which is deep insight eg. arts system or different storage of same data???...\n\n2. the question is what kde is like to be in near future???... j don't realy want to start next little kde/gnome war cose its useless so please remeber its my humble opinion...\n\ngnome is as simple as its possible thats ok... and kde always was as complex as possible with a lot of options... thats great to... and it still should be!!!\n\nusability in DE itself is one thing but don't forget about apps!!! e.g. kde kontact (alot smb business is switching to kde/kontact/kolab/openoffice and care less about DE )...\n\npeople use to ask why kde is not so clear in its design like e.g. mac os x is (somebody may not like it but everybody must admit that its build with a vision)? kde will never be so solid... because of nature of open source community driven projects... where developers are free to do what they like and users may interact with them... no big boss like jobs ;)...\n\nits time to realize that in mass market project (with corporate usage ambitions) like KDE \"usability\" is nothing else than providing as much features/options as possible with out breaking others... \n\nI would like to thing about kde as an industry tool for users who like to learn or small/medium it companies which provide services for smb business which gives an opportunity for deep customization...\n\nkde developer shouldn't bother so much about those who are crying that something is not simple!!! because it drives project to resource waisting!!!! eg. I don't understand need of dolphin!!! what is wrong with konqueror file management functions?\n\n\n"
    author: "genesiss"
---
<div  align="center" style="width: auto; margin-top: 5px; margin-botton: 5px;">
<img src="http://static.kdenews.org/jr/kde-4.0-banner.png" width="427" height="178" />
</div>
Several years of design, development and testing came together today for the <a href="http://www.kde.org/announcements/4.0/">release of KDE 4.0</a>. This is our most significant release in our 11 year history and marks both the end of the long and intensive development cycle leading up to KDE 4.0 and the start of the KDE 4 era.  Join us now in #kde4-release-party on Freenode to celebrate or come to the <a href="http://www.kde.org/kde-4.0-release-event/">release event</a> in person next week.  Packages are available for all the major distributions with live CDs available currently from Kubuntu and openSUSE.  Read on for details or take <a href="http://www.kde.org/announcements/4.0/guide.php">the KDE 4.0 Visual Guide</a> to find your way around.












<!--break-->
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><a href="http://www.kde.org/announcements/4.0/screenshots/desktop.jpg"><img src="http://static.kdenews.org/jr/kde-4.0-desktop.jpg" align="center" width="500" height="375"  /></a><br /><em>The KDE 4.0 desktop</em></div>
<p>
The KDE 4 <strong>Libraries</strong> have seen major improvements in almost all areas.
The Phonon multimedia framework provides platform independent multimedia support to all
KDE applications, the Solid hardware integration framework makes interacting with
(removable) devices easier and provides tools for better powermanagement.
</p>
<p>
The KDE 4 <strong>Desktop</strong> has gained some major new capabilities. The Plasma desktop shell
offers a new desktop interface, including panel, menu and widgets on the desktop
as well as a dashboard function. KWin, the KDE Window manager, now supports advanced
graphical effects to ease interaction with your windows.
</p><p>
Lots of KDE <strong>Applications</strong> have seen improvements as well. Visual updates through
vector-based artwork, changes in the underlying libraries, user interface
enhancements, new features, even new applications -- you name it, KDE 4.0 has it.
Okular, the new document viewer and Dolphin, the new filemanager are only two
applications that leverage KDE 4.0's new technologies.
</p><p>
<p><img src="http://static.kdenews.org/jr/kde-4.0-oxygen-banner.png" align="right" hspace="5"/>
The Oxygen <strong>Artwork</strong> team provides a breath of fresh air on the desktop.
Nearly all user-visible parts of the KDE desktop and applications have been given a
facelift. Beauty and consistency are two of the basic concepts behind Oxygen.
</p>

<p>Distributions known to have packages:</p>

<ul>
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
       <li>
       <strong>Debian</strong> KDE 4.0 packages are available in the experimental branch.
       The KDE Development Platform will even make it into <em>Lenny</em>. Watch for
       announcements by the <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
       </li>
       <li>
       <strong>Fedora</strong> will feature KDE 4.0 in Fedora 9, to be <a
       href="http://fedoraproject.org/wiki/Releases/9">released</a>
       in April, with Alpha releases being available from
       24th of January.  KDE 4.0 packages are in the pre-alpha <a
       href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> repository.
       </li>
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
       <li>
                <strong>Kubuntu</strong> packages are included in the upcoming "Hardy Heron"
                (8.04) and also made available as updates for the stable "Gutsy Gibbon" (7.10).
                A Live CD is available for trying out KDE 4.0.
                More details can be found in  the <a href="http://kubuntu.org/announcements/kde-4.0.php">
                announcement on kubuntu.org</a>.
        </li>
        <li>
                <strong>Mandriva</strong> will provide packages for
                <a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.0/Mandriva/">2008.0</a> and aims
                at producing a Live CD with the latest snapshot of 2008.1.
        </li>
        <li>
                <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a>
                for
                openSUSE 10.3 (
                <a href="http://download.opensuse.org/repositories/KDE:/KDE4/openSUSE_10.3/KDE4-BASIS.ymp">one-click
                install</a>),
                openSUSE Factory (
                <a href="http://download.opensuse.org/repositories/KDE:/KDE4/openSUSE_Factory/KDE4-BASIS.ymp">one-click
                install</a>)
                and openSUSE 10.2. A <a href="http://home.kde.org/~binner/kde-four-live/">KDE
                Four Live CD</a> with these packages is also available. KDE 4.0 will be part of the upcoming
                openSUSE 11.0 release.
        </li>
         <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE based KDE Four Live CD</a>
      </li>
</ul>

<p>Thanks to the coders, artists, usability experts, testers, bug triagers and many more who have made this release the start of something amazing.  Tell us what you think on this
<a href="http://wiki.kde.org/tiki-index.php?page=KDE+4.0+Feedback">4.0 feedback page</a>.</p>








