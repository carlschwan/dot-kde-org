---
title: "KDE 4.0.2 Brings New Plasma Features"
date:    2008-03-05
authors:
  - "skuegler"
slug:    kde-402-brings-new-plasma-features
comments:
  - subject: "Spread the news"
    date: 2008-03-05
    body: "Digg: http://digg.com/linux_unix/KDE_4_0_2_Released_with_New_Features_in_the_Plasma_Desktop"
    author: "Jure"
  - subject: "Re: Spread the news"
    date: 2008-03-05
    body: "Dugg.\n\nAnd btw, I wish the KDE marketing team would automatically handle submissions to Digg, Slashdot, etc, for important news. In Amarok our promotion team handles such tasks, which works nicely.\n\nThis could also help prevent horrible typos in the submission, *cough* ;)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Spread the news"
    date: 2008-03-05
    body: "+1 Great idea..\n\nDon't forget to add Facebok to the list."
    author: "Max"
  - subject: "Re: Spread the news"
    date: 2008-03-05
    body: "I thing it would be also great if major releases feature a keynote video that shows off all the cool features. <-- Videos are easier to spread virally. i.e.: Stumbleupon, Youtube, etc.\n\nSo KDE 4.1 should definitely have a cool video. (including tons of eye candy, tutorials, feature showoffs, showoffs of features that increase productivity, features that can't be found on OS-X/Windows )"
    author: "Max"
  - subject: "Re: Spread the news"
    date: 2008-03-06
    body: "Unfurtunately, video-on-linux sucks... Most of the promo ppl have issues with that. I've made a few video's once, but these days I can't start any of the recording apps without an immediate crash... It'd be great if you could help out, really. Just talk to us on kde-promo@kde.org!"
    author: "jos poortvliet"
  - subject: "Re: Spread the news"
    date: 2008-03-06
    body: "Maybe you're not the right person to promote KDE if you think Linux video sucks just because you can't make it work."
    author: "FedupUser"
  - subject: "Re: Spread the news"
    date: 2008-03-06
    body: "He's the guy who wrote all the KDE release annoucement. And BTW, that would also means that Aaron Seigo is \"not the right person to promote KDE\": http://aseigo.blogspot.com/2008/03/multimedia-its-time-to-get-serious.html"
    author: "."
  - subject: "Re: Spread the news"
    date: 2008-03-06
    body: "perhaps you can prove him otherwise by creating an astounding good video of kde4 in action.."
    author: "whatever noticed"
  - subject: "Reporting 'missing' features"
    date: 2008-03-05
    body: "Hello! First off, congratulations with the new release, just upgraded and it looks superb. It's great that features such as the ability to resize the panel is being backported to minor releases.\n\nWhat i wanted to ask about is the following: My most wanted lost feature from 3.x is the ability to autohide the panel and the ability to have multiple panels. However, I'm a bit sure if such input is useful for the devs, and in that case, how to best communicate this. Any thoughts?"
    author: "millhaven"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "> ability to autohide the panel \n\nThe worst feature ever IMO. Think about it for a minute: A small taskbar has 22 pixels height. The usual screen is at least 1024x768 today. Without autohide you go with the mouse cursor to the window, button, applet you need in your taskbar and click on it. With autohide you go down to the bottom edge of your screen. Because you dont see the taskbar yet you can only guess where your button is. You now have to wait until the taskbar appears. Look for what you wanted to click on. Go there. Wait until the taskbar disappears. Ugh. 22/768*100 equals 3%. So you do this every time you want to use your taskbar for 3% more vertical screen estate? In times where a mouse with scroll wheel is readily available. I just don't get it.\n"
    author: "Michael"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "Note that I'm not the first poster, but let me answer for him ;)\n\nMy counter argument here is that I nearly never use the panel, but it always takes 5% of my screen.\nOn the other hand, I hapen to feel I spend my life scrolling. So should this nearly useless thing that is the panel (thought it is realy usefull sometimes ) not get in my way, I'dd be more than happy.\n\nAnyway, I hope someday, there will be a 'CDE like' 'taskbar' ( wich is the whole windows ) implemented : with the dashboard 'widgets other anything' effect it could be cool.\n\nI actualy use CDE at work on an solaris box, and at my surprise, after a few configurations, I like this desktop a lot, I find it prety effiscient. Plasma could bring a new skin to it, with additional kwin (window management) bonus ^^"
    author: "kollum"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "My solution with plasma: No Panel! I've got a taskbar on the desktop accessible with ctrl-F12 if I actually need it, which I rarely do.\n\nThis matters on my 800x480 screen."
    author: "Soap"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "Ah, hooray for the new Eee PC with bigger screen :) *want*\n"
    author: "Mark Kretschmann"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "Yeah, if only I'd held out a little longer."
    author: "Soap"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "Indeed, the short batterylife sucks. I thought the new Proc&chipset would enable a fanless design (and longer battery life) but apparently not. They should've used a LED based screen, but I guess that's too expensive."
    author: "jos poortvliet"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "IIRC I've read that the new screen is LED backlit, but I might be wrong."
    author: "Mark Kretschmann"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "9 inch 1024 x 600, as it should be. me want too."
    author: "reihal"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "If I understand correctly it is already possible to completely remove the panel container. Just not through graphical settings, yet. You need to edit the Plasma configuration file for your user. Then with panel gone you can put task manager plasmoid on background and it is hidden by default and you can show it with turning on the Dashboard with pressing Ctrl+F12."
    author: "Tsiolkovsky"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: ">> ability to autohide the panel \n> The worst feature ever IMO\n\nautohiding the panel isn't about desktop realestate. It's about visual noise.\n\nI'm using autohide so I don't get distracted by all those blinking applets, buttons etc..\n"
    author: "Bjarne Wichmann Petersen"
  - subject: "In defence of autohide"
    date: 2008-03-05
    body: "Well, here's a usecase: My usual setup in kde3 was to have two panels, one at the bottom with the systray and task manager, and one at the top left corner with width set to about about 1/3 of the screen width (very important so you don't accidentially tricker it when you try to close a maximized window) containing icons for the 8-10 programs i use the most. Both panels rather high (look, pretty icons :-) ) and set to autohide with no animation. \n\nThe rationale being that i got the most out of the avaliable screen estate, that i had far easier access to the programs that i use the most and like 95% of the time the panel is really just sitting there not doing anything but distracting. \nThere is (in my experience) no time loss between this setup and not having autohiding enabled, since it very quickly becomes a habit just to throw the pointer to the bottom of the screen when you want to access a program and then find it (i also thought this would be a problem before i tried out this setup but it's not - apparently you automatically remember where the programs are relative to each other in the task manager). \n\nI agree that i never understood the idea of having animated hiding, that's just a pain."
    author: "millhaven"
  - subject: "Re: In defence of autohide"
    date: 2008-03-06
    body: "I have a quite similar setup. On the bottom I have a panel with a taskbar and few other applets I need most, and on the left side an autohiding panel with K menu, systray and some other applets I need _sometimes_. This allows to keep the always visible panel small, and still have enough space for monitoring etc. applets in the left side panel, since it can be made as big as required.\n\nI do disagree about the autohiding though, a really fast but still noticeable animation is quite nice imho."
    author: "Antti"
  - subject: "Re: In defence of autohide"
    date: 2008-05-28
    body: "I'm agree 100%.\nI think I won't switch to kde 4.1 until they implement autohide panels.\nToday I have been testing 4.1 beta 1 and I hated the panels.\nAutohide is needed."
    author: "Adrian"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "Best feature ever IMHO!  I've got a big expensive plasma TV that my computer is hooked into as its main display.  One of the problems you get with using plasma TVs as displays is burn in.  I noticed that just from a few hours of the panel sitting down the bottom of the screen I would start to get problems.  Setting the panel to auto-hide solves this nicely."
    author: "Paradox"
  - subject: "Autohide = Good"
    date: 2008-03-05
    body: "I think you make \"autohide\" sound a lot more complicated than it is. It's a feature I've been using since Windows 95. I like it because i A) frees up extra screen. B) Makes the screen look \"taller\" (that helps when having to text document pages side by side) C) gives me less distractions from what I currently work on.. \n\nWhen the taskbar is hidden I tend to focus on one task and not \"multi-task\" aka. get sidetracked. It's well known that I have the attention span of a goldfish.. ooo shiny!! \n\nWhat were we talking about again? :p \n\noh yea. When the taskbar is hidden I'm less tempted to explore the \"start menu\" aka. Kicker/Kickoff, or whatever it's called now. When it's visible I'm known to start multiple programs, games, browser windows, and don't get my actual work done as quickly.\n\nSo Autohide = Good.\n\n"
    author: "John"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "Auto-hide for the panel is the most important feature in the entire history of computing.\nI can't live without it."
    author: "reihal"
  - subject: "heh"
    date: 2008-03-06
    body: "Really, the most important feature? I would consider the role of computers in landing on the moon or the Human Genome Project to be more impressive, but that's just me. \n\n;)"
    author: "Ian Monroe"
  - subject: "Re: heh"
    date: 2008-03-08
    body: "Hello,\n\nno actually it's spell checking.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "I want to use *all* of my desktop space, and afaik on of the basic ideas of KDE is choice. If you dont want to use autohide that's fine, but I and many others want autohide, so it should be implemented."
    author: "blueget"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-08
    body: "Hello,\n\nbrings the question to my mind, what full screen view is about. When you need all the space, you go full screen view, no?\n\nBut obviously the discussion is strange in the first place. Of course is autohide useful in many setups. Think presentation, think small screen, think rare use, think second panel with specific use.\n\nPlasma has so little features in the stable release, do we really need to argue every time one is finally added now? As a matter of fact, there are still a lot of things holding me back on using Plasma now. \n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-07
    body: "My intention are not to denote any of KDE hard work, but look from the user side, just for the moment, do not try to sell something with the nice shinny package as a super new flips with a bit extra peanuts into, but taste exactly the same, or even worse.\n\nAs you know, most Linux users use Linux professionally, not for having fun or playing tetris, and we all have a big srceens, at home my 'smallest' monitor is \"17 which gives me 1280x1024, and 22pixels panel is very irritating to have, so i have 48pixs in my kde3.5.8 and i am using my monitor 90degre rotated, yeah cuz i want to have as more vertical side as i can use, hint here? And i am forced to use auto hide feature. Second mostly i am a keyboard user, i like to start everything from the keyboard, not to track my mouse somewhere and then i need to think where i should locate terminal shortcut, and than to wait about 30seconds to appear and even more. In those 30secodns i write, let say fair one line of the code. So i waste my one line of the code for using the super plasma effect, which by the way not working correctly, and you sells me that my 4% of the screen is not important, but 30 seconds i can give up? \n\nLet's be fair here and say, kde 4.0 is a bit distracted, and gone in the way win vista has been gone, to disappoint the user which are with them since qt 1.4 and the first kde release. So i am ready to give more than 13 years using kde because i am disappointed with kde 4.0 and i am not the only one, and if you wanna gain some younger people to use kde, then sell it under windows, because i will stick me on 3.5."
    author: "xddule"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-18
    body: "  Agree with KDE 4.0 being 'distracted' and going the way of Vista'. I installed KUBUNTU with KDE 4 and couldn't get past the new K environment to actually test the UBUNTU side.\n  KDE 4 is clearly not as pleasent to use as KDE 3.X, in fact I would describe the experience as incredibly frustrating, to the point where, like xddule, I am seriously considering pitching KDE to the rubbish heap. Perhaps we can fork KDE, keep the 3.X branch alive and make evololutionary changes to improve it rather than revolutionary changes which fail to deliver.\n  The lesson? If it is not ready DON'T RELEASE IT!"
    author: "Steve"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-07
    body: "I have four different panels, three vertical and one horizontal.  The horizontal one contained the taskbar and desktop switcher.  Another was the menu and launchers.  Another was just for information displays,  etc.  \n\nThe net effect was more like mouse shortcuts than anything else.  I mentally associate mouse movement to a particular region of the screen with a particular functionality.  \n\nI suspect my preferences are rooted in many years of windowmaker use. \n"
    author: "Velvet Elvis"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-08
    body: "You are assuming the machine running kde 4 has ample screen space.\n\nI never use the autohide on my desktop.\n\nOn my eeepc it is essential.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-05
    body: "Both of these well-known to the devs, so there's no need to report them :)"
    author: "Anon"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "Autohide = must have... please... rsn. In KDE3 I used a custom sized panel 108 pixels high so the alternate launcher could have 2 rows of 48px sized icons. The large panel allowed my to have a reasonable sized analog clock in the bottom right and the desktop viewer was large enough to drag windows between desktops. This huge panel would be unacceptable without immediate dissapear available. I also set my control-left|right arrows to switch to previous|nextdesktop which meant I did not need to go to the panel just to switch desktops. I really miss this functionality in KDE4."
    author: "markc"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-06
    body: "> My most wanted lost feature from 3.x is the ability to autohide the panel\n\nSame here. This and the feature loss in gwenview (gamma/brightness/contrast setting) are the main reasons why I'm not switching to KDE4."
    author: "Melchior FRANZ"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-03-10
    body: "Might I also point out that it isn't just autohide but also the setting that windows can cover the panel that is missing.  Actually, that feature was killed by a serious bug in KDE-3.5 -- what good is it to be able to cover a panel if the panel takes up desktop space that it isn't using.\n\nIn either case, using the mouse to get the panel back is much better than having to use the keyboard.  So, perhaps it would be possible to have some way for the user to get desktop widgets to come to the top by using the mouse.\n\nI also miss my separate taskbar that also autohides (vertical text only)."
    author: "JRT"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-05-15
    body: "This is premature release. For so many basic UI customization options to not make it into the release is pretty bad.\n\nI know I can run without the panel, but you know what? I *like* the panel. I use it *all the time*. And I want the thing to *hide* when it's not in use.\n\nKDE3 autohide panel without a short delay (like in Windows and Gnome) was the best thing I ever had in a GUI.\n\nThat I can't seem to customize the panel except wrt size and position - but not color or hiding - amazes me.\n\nThis should have been clearly labelled something like \"KDE 4 INCOMLETE FEATURE RELEASE\".\n\nGonna see now if Fedora 9 will let me swap KDE 4 for KDE 3."
    author: "Mr Clock"
  - subject: "Re: Reporting 'missing' features"
    date: 2008-05-15
    body: "Yikes! I can't even turn off the wallpaper?\n\nThere was a time before political correctness when this kind of software was labelled 'fascist' - it wants to tell me how I can and cannot configure things."
    author: "Mr Clock"
  - subject: "Re: KDE 4.0.2 Brings New Plasma Features"
    date: 2008-03-05
    body: ">> The KDE community hopes you enjoy this release \nI will I will. I believe in you.\n\nI know there is something called 'birth pangs' but the aftermath is always great...\n\nCongratulations to all who made KDE 4 possible."
    author: "V. P."
  - subject: "Re: KDE 4.0.2 Brings New Plasma Features"
    date: 2008-03-06
    body: "I am enjoying 4.0.2 already, besides plays games, I enjoy doing every thing in KDE 4 environment."
    author: "rockmen1"
  - subject: "panel resize?"
    date: 2008-03-05
    body: "i'm running the svn version of trunk. so i can't check it out myself. but is now possible to resize the panel like in trunk? i was surprised not to see this in the changelog."
    author: "Beat Wolf"
  - subject: "Re: panel resize?"
    date: 2008-03-05
    body: "Yeah, the panel can be resized now. \nAlso, it is in the changelog. Twice, heh. (Not in the changes marked under features though)"
    author: "Jonathan Thomas"
  - subject: "Re: panel resize?"
    date: 2008-03-05
    body: "That sounds like I soon give KDE4 another try.\nBut what about other configuration options:\n- Is it or when will it be possible to configure the size of icons and fonts in the taskbar, the menu and on the desktop again?\n- is possible to disable that ugly frame which surrounds all icons and wigdets as soon as you approach them?"
    author: "Mark"
  - subject: "Re: panel resize?"
    date: 2008-03-05
    body: "do not ask too much. It's still not possible to change the size of the font for the clock for exemple and so the clock is bigger than the panel...\n\nAnother small problem I saw is that when the panel is very small (tiny in my case) some of the widget inside are too big and appear at the top of the screen (the clock and the application launcher do have this behavior!)\n\nBut these are details! The work done between 4.0.1 and 4.0.2 is great really. And now I can think to use kde4 as my default desktop. \n\nI just hope that the plasmoid will improve for the 4.0.3 and that the ATI driver could let me use all the effetc and the OpenGL stuff."
    author: "ramses"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "> do not ask too much. It's still not possible to change the size of the font for the clock for exemple and so the clock is bigger than the panel...\n\nThe digiclock is supposed to do that itself. It does respect the \"smallest readable font\" setting from systemsettings, if you set this smaller, the clock might be able to fit in nicer.\n\nWhich settings expose those problems, by the way? Interesting would be the panel size, the font used and the smallest readable font, and of course a screenshot.\n\n> I just hope that the plasmoid will improve for the 4.0.3 and that the ATI driver could let me use all the effetc and the OpenGL stuff.\n\nI'm using the ATI fglrx driver on my notebook, X1300 and it actually works quite well, kwin composite runs smoothly so that it doesn't get in the way. YMMV, but at least it *is* possible to have kwin compositing with fglrx."
    author: "Sebastian Kuegler"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "Where can we find that 'smallest readable font' in the systemsettings\"\n\nI couldn't find it, and over here, the clock also doesn't scale with the panel size (the font size stays the same, causing the panel to only show the upper part of the digits). Also, the icons for k-menu, show desktop, konqueror and filemanager to the left of the panel don't scale. They now show the upper 3/4 of the icon on the panel.\n\nWhat i also noticed is that my Dutch desktop contains a lot of English text, which is odd considering that the statistic page of the translations gives the Dutch team a gold medal for their 100% translation of kde 4.0\n\nLat but not least: the icons on my desktop are doubled (which is an improvement, as they were tripled on my 4.01 desktop)..\n"
    author: "whatever noticed"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "Look for the \"Small\" setting in System Settings | Appearance | Fonts.\n\nPlease supply more information of your things in Plasma, as I wrote in the other post, the following is interesting:\n\n- Font sizes (Taskbar, Small)\n- Panel Size\n- Screenshots\n\nEasiest to address and track it is a bugreport, the Dot is a very poor tool for that kind of thing.\n\nThanks."
    author: "Sebastian Kuegler"
  - subject: "Re: panel resize?"
    date: 2008-05-07
    body: "I'm not the original poster, but I can comment on this as well.  If a bug report has been opened on this, give me the ID and I'll add these there -- I couldn't find one that applied to the KDE4 issue.\n\nBackground:\nKubuntu 8.04 with the latest (May-6) updates applied.  (kde4-4.0.3 I believe)\n\nWhen I right-click on the panel (kicker?) and choose the size to be \"Tiny\" there are two quickly noticeable effects (aside from the desired thin panel):\n1: The default \"Digital Clock\" on the right side is too big and only the upper half of the time is displayed.  A work-around is to enable the \"Show date\" on the clock which just shrinks the time font a bit and moves it higher.  (But you can't actually see the date so it's not a fix.)\n2: When I click on the \"K\" menu, it isn't right above the panel, rather it is shoved to the top of the screen.\n\nI'll attach a screen shot with annotations showing these errors.\n\nDan\n\n "
    author: "Dan Linder"
  - subject: "Re: panel resize?"
    date: 2008-03-07
    body: ">>What i also noticed is that my Dutch desktop contains a lot of English text,\n\nChecked this, and i noticed that a lot of languages have been commented out in the kde4 .desktop files that opensuse ships.\nDunno if this is a suse issue or a kde issue.\n\nI'll check this out..."
    author: "Rinse"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "the problem is not only with the font but with the plasmoid itself or could you explain me why if I choose to have the date in addition to the hour, the date appeared at the bottom of the screen (if the panel is at the bottom) and why the lower part of the K menu is at the top of the screen too. It's nothing due to the font in both case.\n\nI solved the problems. I put the panel at the bottom so now it's a little bit bigger than at the bottom (probably because of the icone for the K menu) and I remove the clock."
    author: "ramses"
  - subject: "Re: panel resize?"
    date: 2008-03-05
    body: "> is possible to disable that ugly frame which surrounds\n> all icons and wigdets as soon as you approach them?\n\nRight-click on the desktop -> select \"lock widgets\". Here you go, no more frames."
    author: "Moltonel"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "> - Is it or when will it be possible to configure the size of icons and fonts in the taskbar, the menu and on the desktop again?\n\nYou can change the font used in the taskbar in systemsettings | Appearance | Fonts | Taskbar\n\n> - is possible to disable that ugly frame which surrounds all icons and wigdets as soon as you approach them?\n\nJust lock the desktop via the RMB menu, they are only there when the desktop is unlocked."
    author: "Sebastian Kuegler"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "systemsettings | Appearance | Fonts | Taskbar\n\nis this logical?\nwouldn't you search for \"taskbar\" first, then for the font setting of the taskbar?\n\nalso nice to have would be: you could right-click on the taskbar and choose to open the relevant systemsettings dialog.\n\nJust an idea."
    author: "tom"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "Actually, I would look for font settings exactly where I put it.\n\nThere has been a design decision sometime, somewhere that we should try to not provide twenty different ways of setting something up because that leads to clutter in the UI and is actually harder to find in the end.\n\nTo me, the System Settings way we chose for is pretty clear and also scales well."
    author: "Sebastian Kuegler"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "Hmm, doesn't work overhere.\nChanged the font for the task bar to '4' (it was 11), logged out and in to get it activated, and while the buttons in the taskbar are now unreadable small, the clock digits are still larger than the panel\n\nI also noticed something new: the lower part of the panel apeares to be drawn on the top of the screen. I noticed some glitches over there, and when i clicked the glitch in the top richt corner, k-menu opened..\n\nPretty odd :o)"
    author: "whatever noticed"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "The date field respects the Small setting, the clock itself will take the rest of the space. Can you please be more specific, i.e. supplying the following information in a bugreport:\n\n- Taskbar, Small font settings (face, size)\n- Panel size\n- screenshot showing the problem\n\nThanks."
    author: "Sebastian Kuegler"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "I think that both http://bugs.kde.org/show_bug.cgi?id=157537 and http://bugs.kde.org/show_bug.cgi?id=158762 seem to be this issue. The latter seems to suggest it's fixed in trunk."
    author: "simon"
  - subject: "Re: panel resize?"
    date: 2008-03-05
    body: "Yes, as it says in the summary.\n\nAnything interesting in trunk?  I quit following it when they switched to qt 4.4.\n"
    author: "JackieBrown"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "I installed trunk only today again after the change to qt4.4\nI didnt have the time to realy play around. I think not very much has changed, but it seems more stable, things seemed to work the way i expeted. I dont know, nothing revolutionary from since 2 weeks (that i have found on the desktop and 1-2 apps), but the overall impression is better."
    author: "Beat Wolf"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "Oh yes, you can; right click on a free area of the panel and you will have an extra option to set the position and size of the panel.\n\nThere are issues though. The clock: on \"tiny\" and \"small\" panel the clock doesn't fit properly. I haven't found a config option to set the font size for the clock (only the font itself). The panel on the left or on the right just looks wrong due to the taskbar not rotating the different window buttons - they still appear horizontal. Sometimes the \"tiny\" size when the panel is at the bottom causes the panel to overflow the screen... towards the top!!"
    author: "NabLa"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "The clock uses the system font size like Sebastian pointed out. If you make the system font size smaller then the clock will fit (a bit inconvenient) but then your system font might be too small."
    author: "Bobby"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "perhaps (I didn't try) but what can I do for the Kmenu? The problem is a little bit more than a config of the font and other people are experiencing the same thing than me. \n\nNothing major but details which need some attention. That's it. Bu tlike I told before. This version is, for me, the first one I can think to use. \n\nI still have some issue like the logout stuff. It's complex and, at least on ubuntu gutsy, you cannot put in hibernation or sleep the computer with the options (very difficult to find) in the logout menu (At least the kpowersave stuff from kde3 is working so now I fond a workaround).\n\nBut everything is improving fast so I'm not complaining I just report bugs :)\n\n"
    author: "ramses"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "You can help by submitting bugreports, preferably with Taskbar, Small font settings (face, size), panel size and position and screenshots showing the problems. See, developers don't necessarily run into those problems, without you telling us (and doing so in a detailed way so we can keep the bug triaging overhead low) is a very good way to help improving Plasma.\n\nThanks!"
    author: "Sebastian Kuegler"
  - subject: "Re: panel resize?"
    date: 2008-03-12
    body: "What's the relationship between the system font size and the clock font size? Because my default font is 9pt and the clock font is like 20pt..."
    author: "NabLa"
  - subject: "Re: panel resize?"
    date: 2008-03-06
    body: "The panel size can be changed in hight but not in length unfortunately. Something that i would prefer btw. I always use kicker at 60% length but this isn't possible with the plasma panel as yet.\n\nThe other thing is dragging and dropping widgets directly from the desktop to the panel. I read somewhere that it was possible even in 4.0.1 but I never got that working. The widgets end up behind and not in the panel.\nThe drag and drop function only works directly from the widget dialogue box."
    author: "Bobby"
  - subject: "Changing panel length"
    date: 2008-03-06
    body: "It's scheduled for implementation Real Soon Now, a little birdie told me.  I tried it by hacking plasma-appletsrc, but the bird twittered that the code does not support it just yet."
    author: "Will Stephenson"
  - subject: "Re: panel resize?"
    date: 2008-03-08
    body: "Sorry, but panel resizing should be as in XP.\nYou can resize it comfortably as you want with the mouse.\nVery good usability.\n\nOpen a dialog for this is annoying.\n\n\nIt's strange that KDE 4 permitts mouse in several tasks.\nYou can't even drop a file on the trash can."
    author: "abc"
  - subject: "Re: panel resize?"
    date: 2008-03-08
    body: "\"Sorry, but panel resizing should be as in XP. You can resize it comfortably as you want with the mouse. Very good usability.\n\nOpen a dialog for this is annoying.\"\n\nAaron Seigo seems to be very keen on \"passive configuration\", where dialogs are not used but where the item one wishes to configure is directly manipulated by the user: the car analogy would be grabbing your seat and physically adjusting it to suit your height directly rather than typing in commands on your in-dashboard carputer ;) This being so, I would not be at all surprised to see this implemented.\n\n\"You can't even drop a file on the trash can.\"\n\nConsidering how new the trash can (and, indeed, the whole of Plasma) is, this does not surprise me in the slightest.  I imagine it will be fixed, in time."
    author: "Anon"
  - subject: "Cool"
    date: 2008-03-05
    body: "Really I impressed with the fixing rate. By the way, it seem that KHTML is in heavy fixing mode. I will try the gmail chat and digg. "
    author: "Zayed"
  - subject: "KHTML"
    date: 2008-03-06
    body: "Does anybody made the ACID3-Test?\nhttp://acid3.acidtests.org/"
    author: "Frank"
  - subject: "Re: KHTML"
    date: 2008-03-06
    body: "62 on Kubuntu."
    author: "stormwind"
  - subject: "Re: KHTML"
    date: 2008-03-06
    body: "FireFox 2.0 50/100\nFireFox 3 anyone?"
    author: "adrian"
  - subject: "Re: KHTML"
    date: 2008-03-07
    body: "Firefox/3.0b3  58"
    author: "stormwind"
  - subject: "Re: KHTML"
    date: 2008-03-07
    body: "FireFox 3 beta 4 (not released yet ) 67"
    author: "Zayed"
  - subject: "Just for comparison"
    date: 2008-03-08
    body: "IE 8 Beta (standards mode is supposed to be default) I scored 17/100 and it looks horrible."
    author: "T. J. Brumfield"
  - subject: "Great news"
    date: 2008-03-05
    body: "Awesome work.  Much kudos to the KDE devs for getting this turned around and out of the door so quickly.  You guys really show commercial developers how it should be done ;-)"
    author: "Phil"
  - subject: "Plasma panel?"
    date: 2008-03-05
    body: "Hi there,\n\nmay somebody help me? I installed the new simple-glow plasma theme from kde-look. With 4.0.2 the plasma panel background does not look right. It seems that the background texture as truncated such that the panel borders are 100% transparent. This effect appears only if I turn the KWin effects on, if not, everything works fine. I tried changing back to the default plasma theme, but got the same effect. Did anyone else experience this problem? \n\nI just want to ask,  if that may be a packaging problem on opensuse or if I messed up configuration files. \n\nBtw: When I start plasma in konsole, it says:\nX Error: BadMatch (invalid parameter attributes) 8\n  Extension:    155 (RENDER)\n  Minor opcode: 4 (RenderCreatePicture)\n  Resource id:  0x13a\nObject::connect: No such signal Plasma::Icon::doubleClicked()\nObject::connect: No such signal Plasma::Icon::doubleClicked()\nObject::connect: No such slot Tasks::launchActivated()\n\nThis message is probably related (At least this is the reason why the panel icons are not functional at all for me)\n\nregards, Sebastian"
    author: "Sebastian"
  - subject: "Re: Plasma panel?"
    date: 2008-03-05
    body: "Same over here on Kubuntu: the borders of the panel show strange effects or are even transparent. So this seems to be a regression or feature in 4.0.2."
    author: "Breco Pol"
  - subject: "Re: Plasma panel?"
    date: 2008-03-05
    body: "same here"
    author: "vicko"
  - subject: "Re: Plasma panel?"
    date: 2008-03-05
    body: "Same here.\n\nReported: http://bugs.kde.org/show_bug.cgi?id=158824"
    author: "Juan Ignacio Pumarino"
  - subject: "Re: Plasma panel?"
    date: 2008-03-10
    body: "Same here, after upgrade to 4.0.2, can't change background anymore, if I click apply, only white screen, should logout and log in again to see new background, anyone having the same issue?"
    author: "Antony"
  - subject: "Re: Plasma panel?"
    date: 2008-03-05
    body: "Definitely a nw feature - \"deskop effects\" :) I wonder what they mean by \"Wait for 4.1!\""
    author: "Sebastian"
  - subject: "Re: Plasma panel?"
    date: 2008-03-06
    body: "This is a bug in qt svg rendering.  It is fixed in 4.4 though."
    author: "Thomas"
  - subject: "Re: Plasma panel?"
    date: 2008-03-06
    body: "It didn't happen to me on 4.0.1 & 4.0.0... so something's changed that \"triggered\" this bug?"
    author: "NabLa"
  - subject: "Re: Plasma panel?"
    date: 2008-03-06
    body: "I actually have no probs with it running opensuse 10.3 "
    author: "anon"
  - subject: "Re: Plasma panel?"
    date: 2008-03-06
    body: "Confirmed, look weird to me too... running kubuntu hardy"
    author: "Redneard"
  - subject: "Plasma eveywhere"
    date: 2008-03-05
    body: "With all due respect to plasma and its devs, it does not make sense to highlight it in the title when all we got was ability to resize and reposition the panel. Am I missing something here? Wasn't that possible with kicker since a few years? Why do I get a feeling that someone is trying to push plasma in my face whenever I look at anything on the dot nowadays? Its not like its the only thing new in KDE4 but looks like its getting most hyp^H^H^Hpush from the marketing department.\n\nThanks for listening."
    author: "PlasmaLover"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-05
    body: "And some of these features are bugged also... Applets not adapting to the size of the panel or rendering bugs when the panel is on top of the screen for example...\n\nI dont want to sound too negative or rude, but i consider this as broken stuff regardless if its a new (and certainly nice) feature. Maybe this should have been included in the announcement also... I, as an example, appreciate it when things are described clearly and straightforward, and not like \"marketing stuff\"... Something like \"we are not quite there\", because not everyone knows this..."
    author: "Anon"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-05
    body: "I wonder how much effort it would have been if one would have provided plasmalib for desktop and developers and kicker for the panels as an intermediate state for 4.0.x series to make the desktop more usable. Maybe it is to some third-party-developer to port kicker to kde4/oxygen and provide it as separate rpm?"
    author: "Sebastian"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-05
    body: "Well, I don't want to discourage interested developers, but frankly: good luck! Whoever ports Kicker to KDE 4 will end up with no third-party applets at all unless they port them themselves, KDE 3 Kicker applets won't work without porting, KDE 4 Plasmoids won't work at all. And I think even porting the basics (menu, taskbar, clock) will be a significant amount of work."
    author: "Kevin Kofler"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-05
    body: "Is that really so - I mean, aren't there compatibility libs for kde3/qt3? What are they supposed to be for?"
    author: "Sebastian"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-05
    body: "An other option, since I don't expect KDE4.0.x to be the only desktop in any widely used distrib, is to use kicker from KDE3 on top of KDE4 ( or the gnome panel, or the xfce one, or ... ) That depends on KDE3, but I think most of kde4 users keep using KDE3 in KDE4 sessions.\n\nI test kde4 snapshot when I can, but I think I'll wait for kde4 to be the mandriva whatever-release _default_ desktop for me to switch."
    author: "kollum"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-06
    body: "There is no Kicker in Fedora 9."
    author: "Kevin Kofler"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-06
    body: "Not sure where this feeling is coming from. Searching through the articles reveals that Plasma has been mentioned two times in the commit-digest lately, the last time it was mentioned in an article was back in November (I can actually hardly believe this, given how visual the component and how quickly it is evolving).\n\nIn this particular case, Plasma is the only component with new features (and apparently very popular ones), that is something one should put into an announcement, if you ask me."
    author: "Sebastian Kuegler"
  - subject: "Re: Plasma eveywhere"
    date: 2008-03-06
    body: "Indeed. Normally, I'm not for boasting features which should've been there in the first place, or features which the competition had for years - but in this case, with everyone complaining about the issues with for example no resizing in the panel, it makes sense to mention it."
    author: "jos poortvliet"
  - subject: "Re: Plasma eveywhere - more developers pls"
    date: 2008-03-06
    body: "They'll ride it to death. Maybe we'll get more developers that way?"
    author: "Mike"
  - subject: "When can we expect better usability/configuration"
    date: 2008-03-05
    body: "Well panel is resizeable ... NOT\n\nCan you set it to be just 70% of the total screensize? No? Well, then it is not.\n\nWhere have other 3 panels gone? I was always using 3 panels: left, top and bottom.\n\nTop was with option to auto-hide on. Well still missing this option :(\n\nWhat about the Desktop? I used to have 4 - 6 desktops with different wallpaper on each of them. That is not just a cosmetic issue, it is usability issue. When I switch to another desktop it takes a few seconds before I actually register which one is it (yes I know we have pager for this but that is not the point). It was also possible to enable the names for each desktop. If I am not mistaken this is still not possible. If it is can someone point me into the right direction please?\n\nWhere is the TRUE multi-desktop KDE? One desktop with one panel settings, one dekstop icons and one wallpaper\n\n2nd desktop with different settings/wallpaper/set of icons?\n\nCan we expect this in the foreseeable future? This would be more than just a cosmetic fix. It would considerably improve usability. And another win for Linux desktop. \n\nIt looks like  drag and drop from the desktop to e.g. Dolphin still doesn't work :( Any progress on this? Is this doable? \n\nAnd also right click on a desktop is as usable as in Gnome before tweaking. How to get options like Create new Txt file? Dunno just missing something or it is not implemented yet?\n\nDoes anyone know how to bring wordclock as a walpaper on desktop. Really miss this one on my desktop.\n\nAnd what happened with the coolest clock ever? You still cannot set more than one timezone to it. I always used to have 4+ for NY, London, Taipei, Buenos Aires ...\n\nAnd switch user is still not working. It brings the Krunner window. Well I don't want to run anything :) ... I just want to run another session as different user \n\nAh, last but not the least. Where is the 'Showed removed tiles option' in Kmahjongg? Really useful feature. \n\nI guess will need to leave without it for a while.\n\nOtherwise guys/girls, great release and thank you!"
    author: "Damijan Bec"
  - subject: "Re: When can we expect better usability/configuration"
    date: 2008-03-05
    body: "Much of that stuff is there, but missing a GUI for configuring. Eg. multiple panels are easy to add if you edit ~/.kde4.0/share/config/plasma-appletsrc, kwinrc has number of desktops and their names right near the top. Each clock can be set with its own timezone too.\n\nWorldClock as background should be pretty easy (make a new desktop containment) Marble and WoC  can take care of that. May already be done in trunk.\n\nPersonally, I hate Icons on desktop, so I just don't care about having file creation in the context menu. And I can tell what desktop I'm on by the apps that are open."
    author: "Soap"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-05
    body: "\"~/.kde4.0/share/config/plasma-appletsrc, kwinrc has number of desktops and their names right near the top.\"\n\nThanks Soap for the panel tip :D\nWhat about displaying names instead of numbers in pager or while switching from one desktop to other?\n\n\"Each clock can be set with its own timezone too.\"\nThe only problem is that you end up with many clocks. In kde 3.5 you could set the main one and on mouse hover as many you wanted. But with 2 panels I guess I can display 2 the most used ones. \n\n\"WorldClock as background should be pretty easy (make a new desktop containment) Marble and WoC can take care of that.\"\n\nI should be more specific. The coolest thing was shadow, showing zones with daylight/night. It was dead easy to do this in 3.5 and even added your customized ones. It doesn't look so in 4.0.\n\n\"Personally, I hate Icons on desktop, so I just don't care about having file creation in the context menu.\"\n\nYes, Indeed it is a luck that we are different. :)\n\nOnce again Soap. Many thanks!\n\nCheers"
    author: "Damijan Bec"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-05
    body: "Found the workaround for Switch user issue.\n\nYou need to type SESSIONS in krunner. Dunno why it doesn't do this by default."
    author: "Damijan Bec"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-06
    body: "If you press Switch User it does show \"SESSIONS\" in krunner for a split second, but only for a split second.\n/facepalm\n\nBut thanks for the workaround! It's appreciated."
    author: "Jonathan Thomas"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-06
    body: "No probs. There is also a problem with System tray and kde 3.X applications or non native applications like amsn. Icons don't show up in sys tray but are floating instead. \n\nWorkaround. Run as different user (I did it as root) then launch the same application as logged user, close root application and your normal user application is sitting in system tray."
    author: "Damijan Bec"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-06
    body: "This is fixed on aMSN svn"
    author: "weintor"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-06
    body: "Cheers weintor.\n\nHave compiled from svn and it looks OK. However, the same issue still persist for the 3.X applications like Klineakconfig, kpowersave and Kcheckgmail."
    author: "Damijan Bec"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-06
    body: "Yes, something strange is going on there. I see this behavior (SESSIONS appears and disappears), but as soon as I restart krunner, e.g. to debug it, it starts to work. So it is very hard to see where is the problem.\nSo as a workaround type: \"killall krunner;krunner\", without quotes, in krunner and it should work. :)"
    author: "Andras Mantia"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-08
    body: "> Much of that stuff is there, but missing a GUI\n\n\"There, but missing a GUI\" is functionally equivalent to \"not there\" for the vast majority of people.\n"
    author: "Jim"
  - subject: "Re: When can we expect better usability/configurat"
    date: 2008-03-08
    body: "Consider allowing that notion for GUI. A GUI that cannot be configured via GUI is not configurable, is it?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Very good"
    date: 2008-03-05
    body: "Im here on OpenSuse System. Looks good. Thanks alot for your Work on KDE.\nFor german speaking visit http://www.kde4.de \n"
    author: "blendo"
  - subject: "Re: Very good"
    date: 2008-03-06
    body: "Thanks for the spam, I'd appreciate if you could do without next time."
    author: "Sebastian Kuegler"
  - subject: "I can understand that"
    date: 2008-03-05
    body: "I think one reason might be that many have been disappointed by the new, not-so-perfect kicker implementation. At least I have got the impression that not being able to resize the panel was one of the major complaints, and I can understand that the marketing crew wants to make clear that that is being successfully worked on."
    author: "Mo"
  - subject: "Re: I can understand that"
    date: 2008-03-05
    body: "Sorry, this was supposed to be a reply to PlasmaLover up there."
    author: "Mo"
  - subject: "Re: I can understand that"
    date: 2008-03-06
    body: "Yes, that is the number one reason. A lot of people missed this feature, so it has been addressed and also backported to branch. \nAs to pushing Plasma in someone's face, at least for this release, Plasma is the only component which has also seen new features (and with those UI changes), so why shouldn't we mention that? Given that a lot of people missed configurability of the panel, this is probably the interesting bit for most of the users. If not for you, there's a lot of other content in the changelog as well. "
    author: "Sebastian Kuegler"
  - subject: "Re: I can understand that"
    date: 2008-03-08
    body: "> Plasma is the only component which has also seen new features (and with those UI changes), so why shouldn't we mention that?\n\nYou're right, it should be mentioned.  But I think the attitude against mentioning it is that the people responsible for Plasma should be keeping their heads down out of embarrassment rather than calling attention to themselves.\n\nA lot of people were pissed off not just about how much the move to Plasma broke, but how unrepentant certain people were and how they blamed users and reviewers for their failures.  Crowing about how they managed to unbreak something just antagonises people further, even if that wasn't the intention.\n"
    author: "Jim"
  - subject: "Train Station Clock (again...)"
    date: 2008-03-05
    body: "Hey you guys, i noticed that the train station clock is still not available as plasma applet, at least not in the official 4.0.2 ... when 4.0.0 was coming out i made a tounge in cheek sort of comment about a conspiracy of a bunch of lame people about quietly killing the cool train station clock (previously promoted by Aaron's screencasts and nuno's mocks) and replacing it with a generic clunky one, instead of just beeing a cool Desktop and giving users what a majority of them really liked in the first RCs.\n\nSomeone from the devs said \"you know, it's actually in playground, just be patient\" ... but seeing it not in the release AGAIN just makes me thinking about that lame people's conspiracy all over again... if you can't bring it over you to ignore a few ignorant nay-sayers who supposedly didn't like the train station clock, at least provide it as an alternative please."
    author: "MichaelG"
  - subject: "Re: Train Station Clock (again...)"
    date: 2008-03-06
    body: "Heh, yeah. Maybe a good idea to move it from playground into extragear, so it gets released next time as well. :-)"
    author: "Sebastian Kuegler"
  - subject: "debian packages"
    date: 2008-03-05
    body: "Ahh, but will Debian package it, even in experimental, some time this year, instead of sticking to a broken, old mis-packaged version of 4.0?  There's the question! ;)\n\nNo, I'm not being completely serious.  Yes, I respect the debian dev's hard work.  Yes, I just thought I'd post this and gently nudge them into doing a little more ;)"
    author: "Lee"
  - subject: "Re: debian packages"
    date: 2008-03-06
    body: "Have you even checked the debian repositories. The packages are coming into experimental. The debian kde package team are actually doing an excellent work in packaging. 4.0.1 was available the day it was released, and so is 4.0.2\n\nThere are even plans of moving onto 4.1 trunk as it may be included in lenny (at least the development files)"
    author: "anon"
  - subject: "Re: debian packages"
    date: 2008-03-06
    body: "Hmm.  Quite right, sorry -- my mistake.  It seems -t experimental wasn't working for me.  Pinning works fine though."
    author: "Lee"
  - subject: "Re: debian packages"
    date: 2008-03-06
    body: "What are you talking about?  4.0.2 is already mostly there in experimental, and 4.0.1 was there before.  Works fine for me."
    author: "Leo S"
  - subject: "Re: debian packages"
    date: 2008-03-06
    body: "Yes, you're right.  Sorry.  My other comment below has a few more details."
    author: "Lee"
  - subject: "Re: debian packages"
    date: 2008-03-09
    body: "I need to agree that Debian packages are quite up-to-date but some packages are still missing for i386 platform:\n\n- kdebindings\n- kdewebdev\n- kdeaccessibility\n- and to a certain extent kdeartwork\n\n\nKdeartwork-theme-icon is in conflict with kdelibs-data from 3.5.X which makes this package at the moment pretty useless. Many applications were not ported yet to KDE4, namely Amarok and K3B - both relies on kdelibs-data 3.5.X\n\nI have created deb package for kdebindings (necessary to run programs which depends on Kross like Superkaramba's widgets). It is a stripped down version of debian kdebindings, free of C#/Mono garbage.\n\nYou can grab it from here\nhttp://www.megaupload.com/?d=65U7FVBH\n\nThere is no guaranty that deb package won't conflict with other packages on your system or that it will work at all. It works for me on DEBIAN/sid.\n\nEnjoy,\nD."
    author: "Damijan Bec"
  - subject: "Remember last statistics"
    date: 2008-03-05
    body: "Commits:  \t2612 by 245 developers, 6750 lines modified, 1558 new files.\nOpen Bugs: \t16124\nOpen Wishes: \t13711\nBugs Opened: \t278 in the last 7 days.\nBugs Closed: \t193 in the last 7 days.\n\nSource: http://commit-digest.org/issues/2008-02-24/\n\nThere are a lot of open bugs & more are coming as the features extend. But there is plenty of time to fix them. (!?)"
    author: "olimpiu"
  - subject: "Re: Remember last statistics"
    date: 2008-03-05
    body: "yea, no worries.\n\nThe KDE people are geniuses!!! (at least in my view)\n\nHopefully all this will be ready in 4.1. Everybody on Facebook is cheering you on!!! Huge worldwide fanbase."
    author: "Max"
  - subject: "Re: Remember last statistics"
    date: 2008-03-05
    body: "Open bugs statistics mean shit with bugzilla. It doesn't really reflect reality, at this point. Most reports are dupes or invalid and whatnot.\n\nMaintaining the bugzilla is often more time consuming than fixing actual bugs, that's the sad truth."
    author: "Mark Kretschmann"
  - subject: "Re: Remember last statistics"
    date: 2008-03-08
    body: "Then perhaps either abandon the current data and start fresh, or ask a team to do a bunch of maintenance trying to confirm/close bugs."
    author: "T. J. Brumfield"
  - subject: "Re: Remember last statistics"
    date: 2008-03-06
    body: "This is quite normal as more people start using it. The number of newly opened bugs is little more than an indicator for one or more the following:\n\n- Number of bugs (d'oh!)\n- Number of users\n- Number and Variety of new features\n- Variety of usecases\n- Intelligence of bugreporters (think dupes)\n- ...\n\nAnd then there are bugs and bugs, some are less relevant to you than others. \n\nThere are lies, damn lies, and statistics. :-)"
    author: "Sebastian Kuegler"
  - subject: "Desktop effects for 4.1?"
    date: 2008-03-05
    body: "Will we get a cool desktop transitioning effect in KDE 4.1?\n\nLike Compiz Cube - or something even much, much cooler?\n\nI can't wait to be blown away. Those effects really help visualize having virtual multiple screens. "
    author: "Max"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-06
    body: "Its on the to-do list, but it isnt high on the list.  It requires of coures 3D transformations of windows in kwin, and about a week ago there was a proof of concept (cover-flow like window switcher, see here: http://dot.kde.org/1204429306/1204452439/ ) so we know its possible.  The KDE developers want to fix bugs, make sure that plasma is essentially usuable, and focus on usuability before they add a lot of bling bling.  Be patient-- or if you know C++, you can code it yourself quite easily, just modify the source for the kwin effect I mentioned above."
    author: "Level 1"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-06
    body: "I do like blings and I miss quite a few things that Compiz-Fusion has but I would prefer to see a feature complete KDE for 4.1 (meaning migration of all or the most of KDE 3.5x programs and functionality to 4.1) and a rock solid Plasma Desktop and Panel. Feature parity and stability should be the main goals for 4.1."
    author: "Bobby"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-06
    body: "That would be nice. 3D effects are cool.. :drool:\n"
    author: "Jon"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-07
    body: "Its actually really easy to write kwin plugins, I wrote wobbly windows in 8 hours.  I'm curious what plugins people are most waiting for, any voters?\n\nI wanna do a window animation plugin, but Im not sure if the kwin developers would accept my work.  My idea is to break down all window animations into five parts: effects that move the window around the screen (like having it appear from one of the corners), effects that change the opacity of the windows (or color or whatever), effects that scale the window, effects that rotate the window, and effects that break the window into a grid and manipulate it.  By combining these 5 effects, I think all window animations could done. Can anyone think of a problem with my solution, or window animations that aren't covered by my wonderful system?\n\nI'll probably start writing my window animations plugin later tonight, maybe I'll have it done next week. I'm excited!"
    author: "Level 1"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-07
    body: "If the code is good and you are willing to maintain it, there are not really any reasons the kwin developers should accept it. Since it's just pluging it's much easier to get accepted too. Depending on it's degree of usefullness it may not be put in the main package, but end up in a addon or extragear package.\n\nDrop a message on the kwin mailinglist, they are a friendly bunch."
    author: "Morty"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-07
    body: "That would be really great!! I'm all for it.\n\nA \"magic lamp\" type effect for minimizing to taskbar would be great.\n\nAlso some cool effect to switch desktops. The compiz cube is nice, but I think there could be other effects, something that would let users visualize a \"virtual screen\" on either side of their physical monitor. Let your imagination flow.\n\nI would also like an effect that adds \"depth\" and vibrant color to the desktop. Make the \"experience of 3d\" somehow. Try to imagine the coolest desktops you've seen in sci-fi or other CSI, NCIS type shows. Think of what you would like the effects to look in 5 years from now.\n\nMaybe we should do a deviant art type competition for coolest look for effects and then see if programmers can incorporate those into reality.\n\nI - like some others - would like to use KDE 4.1 for a CarPC (low resolution 7\" touch screen in the car) I would like to see some effects that would look good in a car, but not distract from driving. (Maybe look to Polyphony digital (makers of Gran Turismo) designet auxiliary gauges for the new Nissan GTR)\n\nCan't wait to see what you come up with. :)"
    author: "Max"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-07
    body: "If you want a sci-fi desktop, what you want is not desktop effects but qt style and color scheme.  If you know C++, can you clone the oxygen or plastique styles and create a new, sci-fi style.  Maybe in the future I'll create a qt style to resemble ncurses interfaces-I think that would be cool."
    author: "Level 1"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-08
    body: "No, no... I don't mean the look, like icons..\n\nI mean animations, things that happen on the screen.\n\nI love oxygen style. I just want my desktop to surprise me and make it a joy to work with. "
    author: "Mike"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-08
    body: "I am sure that the most of us /especially those of us who can't code) would greatly appreciate your work."
    author: "Bobby"
  - subject: "Re: Desktop effects for 4.1?"
    date: 2008-03-09
    body: "Yea we do!!!\n\nThanks for coding."
    author: "Mike"
  - subject: "Shouldn't that be...."
    date: 2008-03-05
    body: "Shouldn't that be \"available in a whopping 49 languages\" and not \"available in 49 whopping languages?\"  It is indeed the number that your adjective is stressing, not the languages themselves.\n--\nBest Regards,\nGrammar Nazi KDE User"
    author: "kde-user"
  - subject: "Re: Shouldn't that be...."
    date: 2008-03-06
    body: "Give Seb a break.  He single-handedly represents the KDE e.V. Marketing group and doesn't get much sleep.  Plus English isn't his native language."
    author: "ac"
  - subject: "Re: Shouldn't that be...."
    date: 2008-03-06
    body: "Once it's posted to the Dot it's up to people like Riddell to edit it. I do not have such karma (but I do get more sleep than Riddell, I hope. :))"
    author: "Sebastian Kuegler"
  - subject: "Re: Shouldn't that be...."
    date: 2008-03-06
    body: "LOL =)"
    author: "Jon"
  - subject: "Re: Shouldn't that be...."
    date: 2008-03-06
    body: "Excuse me, what is a \"Grammar Nazi KDE User\"?? "
    author: "Sebastian"
  - subject: "Re: Shouldn't that be...."
    date: 2008-03-06
    body: "It's a grammar nazi who also happens to be a kde user"
    author: "ninjalj"
  - subject: "Re: Shouldn't that be...."
    date: 2008-03-07
    body: "In no way affiliated with actual Nazis, I can assure you.  And don't take my grammar correction too harshly;  just getting you pumped up to strive for perfection as you work on KDE4. ;)"
    author: "kdeuser"
  - subject: "Black Icon Background"
    date: 2008-03-05
    body: "Why hasn't this bug gotten fixed yet? Sorry to say, but...it looks so ugly!  :\\\n\nI have stopped using desktop icons with KDE4 just because I can't stand the black background they draw. \n\nPlease, someone fix this.\n\nOther than that, KDE4 is a great release!   <3"
    author: "fish"
  - subject: "Re: Black Icon Background"
    date: 2008-03-07
    body: "Full ACK. This is really the worst thing of KDE4 and the reason why I don't use it yet. i just see so many disadvantages and hardly any advantage. Sure, the effects and stuff are nice, but when so many basics are missing which made KDE great I still prefer to stick to KDE3 for the time being. BTW: What I really did hope for with Plasma was a way to draw labeled \"boxes\" (like Development, Graphics, Network) on your desktop and put multiple auto-arranged icons in it."
    author: "Michael"
  - subject: "Re: Black Icon Background"
    date: 2008-03-07
    body: "I think the black boxes are nice. But maybe give users an option to make the box transparent, everybody's happy ;)"
    author: "danoyout"
  - subject: "Re: Black Icon Background"
    date: 2008-03-10
    body: "I completely agree they're ugly.  I submitted a patch that got rid of them and addressed the issue of white text on white background but it was rejected.  If you want the patch let me know and I'll email it.  I use it daily and I think it makes the look MUCH nicer."
    author: "Christopher Blauvelt"
  - subject: "Proxy?"
    date: 2008-03-06
    body: "Has proxy support been fixed yet? This is the only thing that's keeping me from being able to use kde 4. It's kind of useless without internet support, and I have no direct connection at uni."
    author: "Patrick Stewart"
  - subject: "Re: Proxy?"
    date: 2008-03-06
    body: "Second that. I am using KDE4 on my office laptop and have to use firefox, as of now, since there's no proxy support in Konqueror. \n"
    author: "Ken"
  - subject: "Re: Proxy?"
    date: 2008-03-06
    body: "Will most likely be a KDE 4.1 thing, apparently they want to use Qt 4.4 for that."
    author: "jos poortvliet"
  - subject: "Like Kubuntu? Like KDE? - Vote for it."
    date: 2008-03-06
    body: "http://brainstorm.ubuntu.com/idea/478/ \n\nLike Kubuntu? Want it to get better? Want KDE to gain more exposure?\n\n-If yes, please vote at the link above. (sign up required, but it's fast)\n\nI'm reposting this for new people that haven't seen it yet.\n\nTo download KDE 4.0.2 with Kubuntu go to: www.kubuntu.org\n"
    author: "Jon"
  - subject: "Re: Like Kubuntu? Like KDE? - Vote for it."
    date: 2008-03-06
    body: "I guess I'll post my most recent stat here now. We're at 417"
    author: "Max"
  - subject: "Correction: my bad 413 Votes for KDE."
    date: 2008-03-06
    body: "Sorry.. Corrected: We're at 413 votes for KDE equal rights for Ubuntu."
    author: "Max"
  - subject: "Re: Like Kubuntu? Like KDE? - Vote for it."
    date: 2008-03-06
    body: "keep posting this, everyone who hasn't voted needs to vote, kubuntu support means kde support.\n\nwe're at 437 by the way!"
    author: "kderocks"
  - subject: "Re: Like Kubuntu? Like KDE? - Vote for it."
    date: 2008-03-07
    body: "452 - not bad."
    author: "Max"
  - subject: "452."
    date: 2008-03-07
    body: "fixed."
    author: "Max"
  - subject: "Re: Like Kubuntu? Like KDE? - Vote for it."
    date: 2008-03-08
    body: "454: Not much of an improvement."
    author: "Riddle"
  - subject: "466"
    date: 2008-03-09
    body: "466. Yea, really not much of an improvement."
    author: "Mike"
  - subject: "498"
    date: 2008-03-11
    body: "498 now, progressing slowly, very slowly."
    author: "birne"
  - subject: "and still"
    date: 2008-03-06
    body: "and still msn doesn't connect with kopete, thank god for pidgin, at least that connects, and to far much more than the very lacking kopete"
    author: "anon"
  - subject: "Re: and still"
    date: 2008-03-06
    body: "I thought that the reason they almost didn't release kopete with KDE 4 was that it wasn't fully ready for prime-time use.  People wanted it released regardless, and then they complain that it isn't fully ready.\n\nHave you tried the KDE 3 version of kopete for the time being?"
    author: "T. J. Brumfield"
  - subject: "Re: and still"
    date: 2008-03-06
    body: "On my computer, Kopete-KDE4 does work perfect well with MSN as well as with Jabber. Maybe something wrong with your ports?"
    author: "Djan"
  - subject: "Re: and still"
    date: 2008-03-06
    body: "Yes it does, and did since 4.0.0 :?"
    author: "NabLa"
  - subject: "Re: and still"
    date: 2008-03-06
    body: "It did not work for me in KDE4 so far - but i haven't checked it 4.0.2 yet. \nI just couldn't open any account. So i sticked with KDE3 Kopete so far. \n"
    author: "Kavalor"
  - subject: "Re: and still"
    date: 2008-03-06
    body: "It doesn't work here either (neither SSL in Konqueror or MSN in Kopete) - see bug http://bugs.kde.org/show_bug.cgi?id=155564"
    author: "Luke"
  - subject: "My favorite problems with KDE..."
    date: 2008-03-06
    body: "There are some problems that itch me more than others when using KDE:\n\nhttp://bugs.kde.org/show_bug.cgi?id=154774\nMakes Konqueror4 unusable for me - cannot connect to the net. Yes, it might be a weird router problem, but as no other internet program (including Konqui3 and Mozilla) does have this problem, it should get at least a workaround in KDE4, too.\n\nSpeed, speed and again speed:\nAs far as I see, with the (not so unusual) NVidia cards you will in many cases get a very sluggish desktop, even with no special effects. Again, you might say that is a driver issue, but I think for a popular piece of hardware working flawlessly and fast with all kind of non-KDE4 2D and 3D effects, this should nevertheless be addressed\n\nVisual distortions of the panel:\nOK, that's minor, but it shouldn't happen. Quite a lot of icons in the panel, and some other panel elements, too, are often only displayed distorted (or in the case of icons, simply wrong). \n\nApart from this points, I would say, that after the rushed release of KDE4.0, that KDE is now getting to the point where it starts to live up to the hyped expectations marketed before the first release. \nGo on in this direction, I'm looking forward to the next releases!"
    author: "Stefan"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "Oh, and to reply to myself: I'm still not really happy with the new \"systemsettings\" preferences center (fan of the old and crowed kcontrol).\n\nCan anybody tell me who decides which settings are to be considered \"advanced\" settings? E.g. network settings (stuff like time outs and so on - I never used that all my life) is considered to be important enough to be one of the standard modules, and important things like keyboard shortcuts are banned to the advanced options???"
    author: "Stefan"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "I agree, cut the standard / advanced crap in system settings, it confused the hell out of me. In particular as the search function on top does not show both!!! Big big usability mistake in my book. I was also searching for keyboard shortcuts.\n\nThere used to be a firm opinion within KDE that different configuration modules based on user knowledge levels are bullshit, who ever changed this opinion? "
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "What's the bug report number?"
    author: "Will Stephenson"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "we can't fix NVIDIA's drivers, and we are unwilling to work around it. Because we won't work around it, they HAVE to fix it, and KDE 4.1 will be better because of that.\n\nWe didn't rush the release, we released it early for good reasons (and you can read about those all over the web so I won't repeat them).\n\nThanks to our 'rushed' release, by summer we will have a release which will be much better than it would've been without a KDE 4.0 in Januari. If you are unable or not willing to help us get it better, just don't use KDE 4.0 but keep yourself at 3.5.x! Of course, I understand you want the latest & greatest - but you'll have to accept it's rarely perfect, esp in case of such a major release which pushes the boundaries of both KDE itself and it's underlying technologies so much."
    author: "jos poortvliet"
  - subject: "Your point is wrong!"
    date: 2008-03-06
    body: "I believe you underestimate the effort having a full functional desktop environment. \n\nEven if everything works as expected, the Plasma developers will have problems just to have finished features until summer. And I am not talking about refining these features for the average KDE user. Not to talk about the time required for optimization and bug fixing of these new features.\n\nIn every software project, optimization and bug fixing takes at least - even if you have luck - 30 percent of the whole man time. And what I can see from my own experiences with KDE4 is that there is already enough do in bug fixing and optimization even without adding new features in order to deliver something \"usable\" in summer. \n\nMoreover, I really hope your relation to KDE's userbase is not representative for KDE developers!!! \n\nIn the first point it is still the user base KDE is provided for. Off course, every developer has the freedom to decide what he/she is willing to change in KDE's codebase. But in the end the KDE quality team must define which features are ready and how certain features must be changed in order to become part of the official release.\n\nAnd really: To deliver a desktop environment which does not run on a major fraction of hardware, is not acceptable. I understand your post in a way that you ignore a huge userbase. Off course, you hope that the NVIDIA issue will be fixed, but the problem is deeper:\n(1) What if NVIDIA can not fix it in proper time. Then many users will not see a major KDE release within more than 2!!! years!\n(2) What if NIVIDA fixes it and you find out that it is not NVIDIA?\n(3) Are there at least some alternatives in case that it is NOT fixed for 4.1?\n\nIn the last years there was one constant among Linux users: Buy NVIDIA cards - ATI does not have drivers at all or they are buggy. \n\n(o) What happens right now is that many developers reply to bug reports with \"NVIDIA issue\" or \"Qt issue\". This is ignorance! And arrogance! What if not? (And after analyzing the bug one may even find out that it is not a Qt issue!) \n\nI understand that developers want to have clean and short codes without much exception handling. But as a user I want that a software provides workarounds if it knows that it may do better even with bugyg drivers. The previous poster said it: non-KDE4 apps can handle 3d effects pretty well with NVIDIA drivers!"
    author: "Sebastian"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "> In the first point it is still the user base KDE is provided for. \nThis is a common misconception. Users are nice, but someone said a few years ago: any free software can live without users, but not without developers. \nSo it's necessary to keep the developers happy, not the users. Happy developers often multiply themselves in weird magic ways and will eventually produce software that makes users happy as well. But I believe it must be the developers that come first.\n\n> To deliver a desktop environment which does not run on a major fraction of hardware, is not acceptable\nBeing a user (ie. not a developer) with that very kind of hardware, I still find it highly acceptable. Please don't generalize if you're not an official spokesman!\n\n> But as a user I want that a softw...\nYou as a user (not developer) are not very important, in my opinion. I (as an equally unimportant user) rather have a great working KDE4 in two years from now than KDE3 with a new skin. \nBecause that's what you'd get: the workarounds exist - in KDE3. Use it, be happy."
    author: "Tom Vollerthun"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "Good point :) As a real KDE3 fan (and KDE4 as well - I daily work with it at least on my private pc) one tends to overestimating his own importance.\n\nBut: Then I do not want to hear any complaints about the market share of KDE anymore... \n"
    author: "Sebastian"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "You should have bought a graphics card with Free drivers available, i.e. Intel chipsets with integrated graphics or ATI Radeon cards up to the X850 (and that includes the X1050 which is just a reissued X300/X550 with a new marketing name)."
    author: "Kevin Kofler"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "Oh, and support the Nouveau project if you want to ever get working drivers for your Nvidia card.\nhttp://nouveau.freedesktop.org\nProprietary drivers always cause problems."
    author: "Kevin Kofler"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "...and open source drivers may lack of quality and may be of alpha quality. They do not often, but the current status of KDE4 proves the possibility..."
    author: "Sebastian"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "Well, on my private box I got an old Radeon M7. Since bying the computer 3 years ago, ATI always referred me to the DRI project in order to get 3d acceleration, but DRI did not support the card. We have several recent ATI cards in our office (DELL ships them mostly by default). There, X11 hangs very often (we extensively call 3d application, mostly CAE) and many CAD structures are not rendered correctly. \n\n\nNVIDIA cards on the opposite work at least stable and correctly render the things we need. Off course I do not know how NVIDIA drivers support shaders and other gimmics used by gamers, but for us they were the only manufacturer with working drivers. I do not have experiences with Intel cards, though. I only know, that the early drivers had severe stability issues. For productive use in companies such news are not creating confidence in such hardware, so our sysadmins never tried those. \n\n"
    author: "Sebastian"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-07
    body: "The Radeon M7 (also known as Mobility 7500) should have been working for ages. My Radeon 7000 definitely did. (I'm currently using a Radeon 9200 SE though.)"
    author: "Kevin Kofler"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-07
    body: "PS: And when I say \"working\", I mean with DRI 3D acceleration, of course."
    author: "Kevin Kofler"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "\u00abI understand that developers want to have clean and short codes without much exception handling. But as a user I want that a software provides workarounds if it knows that it may do better even with bugyg drivers.\u00bb\n\nWho cares? This is a hobby project, not a business. If you want support on top of KDE, checkout Novell and Red Hat."
    author: "blacky"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "Short term thinking galore :/\n\nNext time you run into that problem, you'll realise that you really don't want those workarounds, you want fixes."
    author: "Sebastian Kuegler"
  - subject: "Re: Your point is wrong!"
    date: 2008-03-06
    body: "\"Next time you run into that problem, you'll realise that you really don't want those workarounds, you want fixes.\"\n\nWell said :)"
    author: "Anon"
  - subject: "Re: Your point is wrong! No it's not :P"
    date: 2008-03-07
    body: "> And really: To deliver a desktop environment which does not run on a major\n> fraction of hardware, is not acceptable. I understand your post in a way that\n> you ignore a huge userbase.\n\nYou have a point here, but please realize that KDE 4.0 was announced with this explicit disclaimer. 4.0 is not meant for the general public, but is a step that's very much needed to get to a production ready desktop with 4.1.\n\nLinux 2.6.0 sucked, Apache 2.0 sucked, Mac OS 10.0 sucked. Pre SP1 versions of Windows XP and Vista also had major issues. At some point you'll have to release something, or you'll be developing for the blue sky forever.\n\nI remember Linus pointing out that 2.6.0 was still not good enough, but he had to release it. Nobody dared to run the 2.5 branch. The lack of feedback slowed down development and the difference between the user base and developer community only got bigger. (people backporting the weirdest stuff to the 2.4 kernel, and you'll receive more and more patches that can't be merged anymore). Therefore I'm happy KDE took the same path.\n\nAlso see: http://aseigo.blogspot.com/2008/01/talking-bluntly.html, which explains this very well."
    author: "Diederik van der Boor"
  - subject: "Re: Your point is wrong! No it's not :P"
    date: 2008-03-08
    body: "> please realize that KDE 4.0 was announced with this explicit disclaimer. 4.0 is not meant for the general public\n\nWhere was this announced?  It wasn't in any of the official announcements I saw.  The only place I saw this expressed was in developer blogs, i.e. exactly the place the general public doesn't look and not at all \"announced\".\n\n> At some point you'll have to release something, or you'll be developing for the blue sky forever.\n\nThere's a reasonable middle ground between blue sky and the debacle of KDE 4.  No matter how many times people keep repeating that blue sky isn't acceptable, it doesn't make KDE 4.0 any better.\n"
    author: "Jim"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "I agree with you pertaining to the nVidia driver. There are also issues even when running KDE 3.5x without compositing. The desktop would just freeze after a while and the only thing that can help then is reset because the keyboard doesn't respond at all. The funny thing is that this doesn't happen when running Compiz-Fusion on KDE 3.5x.\nThe afore mentioned doesn't occur when running KDE 4.0.2 but there are a lot of performance losses and lots of issues with the nVidia driver."
    author: "Bobby"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "I completely agree with this. I do not really know what the KDE representatives are doing these days. Is there any quality management team at all, any internal testing environment? Does the quality team has any right to do decision against developers? Or are they afraid of a conflict with Aaron who probably takes most of the responsibility for plasma? It seems to me that the most KWin fixes wait for 4.1 while we receive new unfinished \"features\" in plasma introducing new bugs. And yes, I hear \"this is an NVIDIA bug\" or \"this is a Qt bug\" way too often... Really, I do not need a resizeable panel as long as any panel icon does not work or as long as I can not edit 'favourite applications' in kickoff. Particularly as the resizing feature is still incomplete and introduced new bugs (namely the panel background distortion). \n\nAs you said, just the appearance of the panel distortion bug is THE metaphore for quality of KDE nowadays. I was surprised that this found a way into the official release - "
    author: "Sebastian"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "Jeez, read your own post again. To me it just sounds like whining with total lack of constructive criticism. That could surely not have been your intention."
    author: "Amazon"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "Well, you my be right.\n\nI may summarize myself: they should concentrate on fixing bugs instead of creating new bugs by enforcing a feature plan that is not possible reach in time while doing required optimization and refining. I hoped that at least the 4.0.x branch will contain only \"usable\" backports from trunk."
    author: "Sebastian"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "And please please please stop calling every, really every critical comment as \"whining\" or \"trolling\".\n\nThis current attitude is what p**es me off most at the moment."
    author: "Stefan"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-07
    body: "You just don't understand the difference between work-for-hire and work-donated-for-free. When people are working for you for free, you need to keep the magnitude of their gift firmly in mind.\n\nIt's OK to offer criticism, but frankly the criticisms offered here have all the social grace of a pack of gradeschool brats."
    author: "T"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-08
    body: "> You just don't understand the difference between work-for-hire and work-donated-for-free.\n\nMany KDE developers are paid to work on KDE.  It ceased to be a purely hobby project years ago.\n\n> It's OK to offer criticism, but frankly the criticisms offered here have all the social grace of a pack of gradeschool brats.\n\nThe criticism was reasonable in the beginning and steadily got worse as the reasonable criticism wasn't being responded to appropriately.  It got a lot better after they announced the release was being postponed.\n"
    author: "Jim"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-08
    body: "Many? I know of only 2 people whose primary job is to work on KDE, and a \nfew more who have large KDE roles related to their employment at distributions. In contrast, the last commit digest listed 245 commiters, and not everyone commits every week!\n\nKDE is certainly not a -purely- hobby project, but it still is driven primarily by volunteers (unlike many other high-profile projects, some of which are more like corporate projects with community outreach).\n\n\n\n"
    author: "SadEagle"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-08
    body: "I probably know some more devels who get payed, but the result stays the same; KDE is a real community project driven by lot of high skilled volunteers who participate in there free time.\n\nThat's quit interesting since that so much ppl do participate in there free time says a lot about the quality of KDE (or would anyone like to work on something that is a mess in there free time?). Really, beside projects like debian I don't know of many other projects that are so much community-FOSS :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "You maybe shouldn't gauge those features by if they make it possible to break things, but by if they make it possible to have things behaving well.\n\nAnd then, again, again, again: If things break, please help by filing a bugreport. Whining on the Dot is utterly counter-productive. My motivation for hacking on those problems has gradually gone down to zero while reading all the comments to this Dot story. All this whinery on the Dot does steal people's motivation to work on the issue, reading it also costs time. \n\nGood, detailed bugreports do help fixing those issues.\nWhining about the general quality of certain components does not. If it doesn't work well enough for you, either try to provide information why and what needs fixing, or just use something else. You do everyone a service by adopting this behaviour.\n\nNow this is not only a reaction to the previous poster, it's a general thing. The fact that the comments to this story made me *not* want to work on them should be pretty alarming. \n\nYou as poster to the Dot do carry responsibility for that. It's time to use that. If you merely want to vent your frustration, it'd be better for all of us if you just stayed away, rather than making it worse."
    author: "Sebastian Kuegler"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-07
    body: "If features don't get backported (especially features which were there in KDE 3 and lacking in Plasma / KDE 4), people complain. If features do get backported, people complain too (hopefully not the same ones ;-) ). One can't win..."
    author: "Kevin Kofler"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "On my desktop, the distortion happens when I'm running Firefox or OpenOffice, I wouldn't necessarily point at KDE that's buggy here ...\n\nDid you also notice this pattern?\n\nThis is BTW the exact reason to not release later. The later you release, the longer those problems in other components are being ignored."
    author: "Sebastian Kuegler"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-07
    body: "no, panel distortion appears always. btw: bug did not appear in 4.0.1 - so this is not an arguMent against early release, but an arguMent for iMprove testing."
    author: "Kevin"
  - subject: "Re: My favorite problems with KDE..."
    date: 2008-03-06
    body: "Could the slowness be a kde4 problem, Running apps which compile against Qt 4.x e.g. smplayer are really fast! the toolbars animate beautifully, Its just apps dependent on kde which are slow (scrolling etc). This is not mentioning the window dragging slowness, resizing etc. "
    author: "Fool"
  - subject: "Wake me up when 4.1 is released"
    date: 2008-03-06
    body: "When 4.1 is out, then I'll be ecstatic."
    author: "Marc Driftmeyer"
  - subject: "Re: Wake me up when 4.1 is released"
    date: 2008-03-07
    body: "True.. But you'll probably want to give feedback/bug reports/wish list now because:\n\nThen 4.1 will have the features you want and be the way you want it.\n\nand \n\nThe projects will be included in \n\nGoogle Summer of Code (submissions are due in the next few weeks.. The more submissions we have, the better, because it means more to choose from for programmers and mentors. It would also spark more interest.)"
    author: "Max"
  - subject: "More Buggy or Is it just on my machine?"
    date: 2008-03-06
    body: "First of all thanks for this release. I installed KDE 4.0.2 once it appeared in opensuse repository(which was much before the release announcement.) On testing I could find some enhancement to the plasma desktop but at the same time I felt there were some bugs which was not found in the 4.0.1 release. One such bug was when I try to remove the Kget applet the desktop crashes. Another one was that the desktop settings are not saved on log out of the system. Another issue is once I change the wallpaper I need to log out and log in back for it to get applied. Another problem that I faced from the very KDE 4.0.0 is that even if I remove some of the icons it always come back on each login. I thought I would confirm these bugs with others and then post it to the bugs tracker. Not sure if these bugs are only on my system.\nOne enhancement I would like to see in KDE 4 is with the icons. Desktop icons is taking more screen space than KDE 3.x version. Due to the same reason I am not able to add any applets, since there is no space for additional applets. So I request KDE team to provide much better icon set up for KDE 4. I am still playing with KDE 4.0.2 and would post the bugs details if I found any. Thanks"
    author: "Swaroop Shankar"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-06
    body: "why not resize the icons to a smaller size by hand..."
    author: "jos poortvliet"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-06
    body: "\"Another problem that I faced from the very KDE 4.0.0 is that even if I remove some of the icons it always come back on each login.\"\n\nAye, this is most annoying. And in some cases, on next login some icons would appear *twice*, and removed plasmoids (can't get rid of the comic plasmoid) appear again."
    author: "NabLa"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-06
    body: "If you right click on the desktop, go to desktop settings and then remove the tick from show icons (at the bottom of the window that opens) then the icons will simple disappear. \nStill they shouldn't appear after being closed."
    author: "Bobby"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-06
    body: "That information was new to me. But this option would remove all the icons. Me (and I think NabLa too) were talking about individual icons. If we don't want any one of the icon then there is no option to remove it permanently from the desktop."
    author: "Swaroop Shankar"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-08
    body: "What you can do after you remove the tick is add the icons that you like to the desktop. You can do that from the start menu. These icons will stay there and you can remove or resize them individually. "
    author: "Bobby"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-12
    body: "Yes, I was talking about that. It's just... click close on an icon and it disappears, yes. But when you log in again, it's there again! And sometimes, it's there but twice! That has to be a bug, not a feature...\n\nCan't seem to find a bug report for this, so I'll file one.\n\nI'd actually prefer to be able to:\n* Have some file management on desktop files so I don't need to go to dolphin or a console to delete stuff from the desktop\n* Being able to drag&drop files from the desktop somewhere else (ie an application)."
    author: "NabLa"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-12
    body: "#155241 and #155620 refer to icon duplications though"
    author: "NabLa"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-08
    body: "I got exactly the same problem. I think it's not because the machine\n\nAnother bug i found is the kmix applet doesn't start when log on"
    author: "m_goku"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-09
    body: "For me KDE 4.0.2 is much more buggy than 4.0.0 and 4.0.1.\nAs this should be a buf-fix mostly release exept for some small backports from KDE 4.1 Plasma this is really bad news.\nLet's hope this is just kubuntu packages that are bad and will be rebuilded later instead of being a kde source code problem :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-09
    body: "I am using opensuse 10.3 and this is the case with me too. I too felt KDE 4.0.2 is more buggy. Too much crashes. I am now going to try the svn packages."
    author: "Swaroop Shankar"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-10
    body: "I have the same problem too, need to log out and log back in to see your new applied background, what's happening? this bug didn't occur in 4.0.0 & 4.0.1?"
    author: "Antony"
  - subject: "Re: More Buggy or Is it just on my machine?"
    date: 2008-03-11
    body: "UNSTABLE KDE 4 version from opensuse (http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/) works much much better than 4.0.2. The icons also looks much better in it and not much annoying crashes as in 4.0.2."
    author: "Swaroop Shankar"
  - subject: "SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "First congrats and thanks to the devs for another very fast release.\nThere are two little problems that I am having since I am not that demanding ;)\n1. I still can't figure out how to run Liquid Weather on Plasma. It even shows up in the add widget dialogue box after I tried to run it using SuperKaramba but when I tried to add it to the desktop I got a message saying that one or more components of the Kross scripting architecture is missing. Do I need to install one or more packages here and which?\n\n2. I set up sideshow for my wallpapers but after the first wallpaper changes the desktop just gets white - no wallpaper anymore. One can see that the wallpapers continue to change after the given interval but they don't appear on the desktop! And the white desktop + panel turn all kinds of colours when I write in Firefox (like now). It changes to black with white shadows around the Plasmoids then a mixture of green, blue, red yellow etc. with a funny pattern. This only happens to some of the Plasmoids but not the desktop and panel when I use openOffice Writer.\n\nWell, the desktop is working and seems to have improved in speed but these teething troubles are a bit frustrating since I don't know how to solve them.\nBtw I am using openSuse 10.3 with the Suse packages installed. Don't know if that could be the problem. "
    author: "Bobby"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "In fact the screen goes white simply by choosing another wallpaper, not just with slideshow.\n\nYou see the transition, but then it's all white unless you log out one time.\n\nSo i conclude that the transitioning is fucked up, it wasn't there before and so wasn't the white desktop problem.\n\nIs there a way to deactivate the wallpaper transitioning?"
    author: "MichaelG"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "The problem is that it remains white until I log out and in again. It also happens with single wallpapers, after a few minutes the desktop just gets white and you can see a white line running below the panel. It really looks messed up.\nFunny thing is that it wasn't like that in the previous versions."
    author: "Bobby"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "Eh, dude that's exactly what i just said... na, maybe my english isn't so good after all :-("
    author: "MichaelG"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "Is there a way to get you to use bugzilla to help us fix those problems? Search if it has been reported, add information about your setup (screen reso?, multihead? driver in use? exact versions? Distro? compositing or not? output on the konsole? patterns in this behaviour? None of those are in your parent post, moreover it's posted to the Dot, not to BKO where it would actually help fixing it.\n\nSigh."
    author: "Sebastian Kuegler"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-08
    body: "Sebastian, I never used Bugzilla but I would if you could give me a few instruction on how to. I would certainly love to participate a little more.\n"
    author: "Bobby"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "That's what I would like to know too. And the same applies for the 3d earth model applet. It complains that \"This object could not be created for the following reason:\nOpenGL Shaders not supported\"\n\nglxinfo | grep shader\n    GL_ARB_fragment_shader, GL_ARB_half_float_pixel, GL_ARB_imaging,\n    GL_ARB_shadow, GL_ARB_shader_objects, GL_ARB_shading_language_100,\n    GL_ARB_vertex_buffer_object, GL_ARB_vertex_program, GL_ARB_vertex_shader,\n    GL_EXT_Cg_shader, GL_EXT_depth_bounds_test, GL_EXT_draw_range_elements,\n    GL_NV_texture_shader, GL_NV_texture_shader2, GL_NV_texture_shader3,\n\nAnd I could run Compiz and compiz-fusion too. Not to mention that I have OpenGL composite enabled with Kwin4. \n\nThis is not just Opensuse specific, I am using Debian Sid with Experimental repositories. \n\nI think Bobby that KDE4 has still some hiccups :). "
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "Well, you need a videocard that has pixel and vertex shaders. Shaders aren't used in compositing. (Yet)"
    author: "Jonathan Thomas"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "Hmm. GeForce Go 6800/PCI/SSE2 Should have it. Or am I missing something? Am using Nvidia drivers v. 169.07 with X.Org X Server 1.4.0.90 (Release Date: 5 September 2007)"
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "Apparently your setup should support shaders. Maybe you should file a bug report?"
    author: "Jonathan Thomas"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "https://bugs.kde.org/show_bug.cgi?id=153508 \n\nExisting case Status INVALID. Aaron's reply was \"just because you have opengl doesn't mean you have the shader support required to run certain applets. i'm afraid that the error is accurate, and that the applet is using a feature that isn't available with your card/driver combo\"\n\nNow, I am confused ?:|"
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "Well, your title to the bug report makes it looks like all applets using OpenGL don't work. It's a tad misleading, to say the least. Hopefully the info you provided will help straighten things out."
    author: "Jonathan Thomas"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "I agree but it wasn't me who created this bug report. About info provided: I am always open to do testing on my computer. It is kinda fun :)\nIf you have any suggestions what to add to the bug report, just spit it out please. Your input is more than welcome. After all ... we all want to have a better KDE desktop :)"
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-06
    body: "re 1: install kdebindings or at least kdebindings/krosspython depending on how your distributor did split kdebindings. Then you will be able to either\n1) run SK just like before. That means, e.g. start the SuperKaramba app and load there the theme or just doubleclick on a *.skz file to have it running or\n2) from within the \"Add Widget\" dialog what runs then the theme as Plasma Widget. In 4.0.x it's needed to have 1) done before 2) is possible cause the SK themes will show up in the Plasma \"Add Widget\" dialog only after they got run at least once with the SK-app.\n\nLast but not least; in 4.1 this all got already reworked and it will be more easy to run legacy themes and they even will feel a bit more native aka like Plasma applets ;)\n"
    author: "Sebastian Sauer"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "First Sebastian thank you. \n\nThe reason why we cannot run it is because Debian haven't created kdebindings 4.X package yet. I was just about to create my own deb file while have noticed something which boiled my blood. \n\nWhat the heck is doing C# in bindings? Well done guys. Lets follow the f****** GNOME tainted Microshit path. Do not forget to include mono in core applications too. Bravo, lets encourage stupid users to run Mono apps, most of them don't care about software freedom anyway.\n\nI believe that you just D O N O T G E T IT!!! This has nothing to do with how good or bad is their technology. This is their technology. MICROSHIT HAS NEVER PLAYED A NICE GAME BEFORE. HOW CAN YOU BE SO STUPID AND NAIVE THAT THEY WILL NOT F**** US UP AS SOON TOO MUCH TAINTED CODE WILL BE INCLUDED IN KDE?!?!?!?!? And just please don't say that we can always remove the code from KDE or that MONO is open sourced (sigh). Tomboy, beagle, F-spot??? Get it?!? Programs are the key and not the libraries. Lets have 70% of the coolest programs written in C#. Who will rewrite these programs and how long will take to rewrite it? You get it now?\n\nKde should not encourage in any way the usage of C#/MONO. Providing bindings is just the first step into it and will bring us to the dead end sooner or later. \n\nRemember, Microsoft can AND WILL make sure that projects depending on their technology are halted in any possible way you can or you cannot imagine. \n\nThe main reason I am using KDE and not GNOME any more is because we somehow managed to avoid their stinky influence. It looks like this is no longer the case. \n\nAnd there is one thing more which I just don't understand and get it:\n\nWhy the heck, are you developers, spending your free time, on their technology?\nIf you need to use C# in the company you work for, then OK. But you are contributing here YOUR OWN FREE TIME. Each second spending on c# is a grave waste of your time, energy and good will and IT HELPS NO ONE BUT MICROSHIT!!! \nI dunno is just me being paranoid and stupid or are most of you blind and ignorant to actually see what Microgrind was and is doing for the last 20+ years?\n\nI do not hate Microshit per se, what I hate is that the only thing they are doing is stabbing us free users and abusing our rights to use free software (or at least they are trying to). They are lying, cheating, stealing and killing their competition - AND YOU DEAR DEVELOPERS ARE HELPING THEM :( Microdeath never stops and it goes so far to actually abuse the ISO national standard bodies to get their property standard through. If possible I simply ignore Microshit, their technology and their products. Ignorance is something which hurts the most. \n\nI have created Debian package (kdebindings_4.0.2-1_i386.deb) without C# parts. Feel free to download it from \nhttp://www.megaupload.com/?d=Q5OKY0R2\n\nP.S. If I want to use Microshit technology then I use Microshit OS. \nP.P.S. There is no guaranty that deb package won't conflict with other packages on your system or that it will work at all. It works for me on DEBIAN/sid. "
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "> I do not hate Microshit per se\n\nRight, OK..."
    author: "Paul Eggleton"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: ":) and people still have a sense of humour. Yes I  hate M. per se. The sooner they vanish the better. "
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "\"I dunno is just me being paranoid and stupid\"\n\nReading your incoherent, foaming post, I'll have to go with Choice #1 here."
    author: "Anon"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "I was just being sarcastic Anon. \nI do not hate them I rather dislike them. And try to ignore them if possible.\nHate is really unhealthy feeling ... \n"
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "I've no idea what you are getting so worked up about. We work on the C# Qyoto/Kimono bindings because it's fun."
    author: "Richard Dale"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "Still doesn't work. The same error message complaining about kross when trying to load Liquid weather 14.8.\n\n\"SuperKaramba cannot continue to run this theme.One or more of the required components of the Kross scripting architecture is not installed. Please consult this theme's documentation and install the necessary Kross components.\"\n\nWell I have all kdebindings components now except c# and still doesn't work. Any other ideas?"
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "> Still doesn't work. The same error message complaining about kross when trying to load Liquid weather 14.8.\n\nSounds like it's installed in the wrong path or with the wrong lib-name or something like this since that message will only show up if loading the lib wasn't possible.\n\nIn debian/sid, depending on your kde-install prefix which defaults to /usr iirc, it should also be installed to /usr. So, e.g. something like /usr/lib/kde4/krosspython.so\n\np.s. related to your other message above;\n\n> you just D O N O T G E T IT!!!\n> You get it now?\n\nwow, and you really expect that anybody does listen to something that is written such offensive? Man, I even guess that it's very likely that you achieve the opposite of your intention with such wordings.\n"
    author: "Sebastian Sauer"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "I would really like to hear your thoughts on this topic though...?"
    author: "MichaelG"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "On what topic? Microsoft bashing? Well, I am not that motivated to spend my time with MS-bashing on a dot-article that deals with the release of KDE 4.0.2 since somehow I guess that's not related.\n\nIf it's about Mono/Microsoft/Patents/etc. then I would suggest to read http://www.osnews.com/thread?224521 which is a nice and short answer from someone who works on it (I don't and therefore can't really provide any useful answer anyway beside that I think, that C# is much better designed then Java but still not as good as Python and Ruby ;)\n"
    author: "Sebastian Sauer"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "So i can assume that you share the thoughts of Miguel de Icaza how he expressed them on the interview? \nI just would like to get an unavoiding answer from you, to help me to make my mind about this."
    author: "MichaelG"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "\"So i can assume that you share the thoughts of Miguel de Icaza how he expressed them on the interview?\"\n\nJust because KDE developers work on Qt/KDE C#/Mono bindings, certainly doesn't mean that we agree with everything that Miguel de Icaza says. \n\nIf you use the bindings to develop Qt apps, all you are using is the C# language and the Mono runtime. I feel I'm more at risk of being abducted by aliens than being sued by Microsoft for using Mono. Encouraging C# programmers to learn Qt/KDE programming, and getting extra developers as a consequence, is much more likely to benefit the KDE project than harm it."
    author: "Richard Dale"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "> you share the thoughts of Miguel de Icaza\n\nI agree with what Richard wrote and that's why I did provide a link to his answer. Is that unavoiding enough? Well, don't expect more concrete statements from me on things I don't deal with. If you like to have a good MS-bashing then ask me about MSOOXML (but probably not on the dot on this article since it's really offtopic).\n"
    author: "Sebastian Sauer"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "\"wow, and you really expect that anybody does listen to something that is written such offensive? Man, I even guess that it's very likely that you achieve the opposite of your intention with such wordings.\"\n\nWell, I guess you are right :). But it is still frustrating that a bunch of intelligent people (read developers) just don't care and blindly lead us to Microsoft trap.\n\n\"So, e.g. something like /usr/lib/kde4/krosspython.so\"\n\n\nYou are right. Default path was /usr/local/lib/kde4 so I should put a different path in cmake_install.cmake. SET(CMAKE_INSTALL_PREFIX \"/usr/lib\"). \nI put ln -s /usr/local/lib/kde4/krosspython.so /usr/lib/kde4/ and could run Liquid without cross error message. \n\nI guess will need to recompile it again and create a deb package too.\n\n\nBut get another error message now :( Probably a broken Liquid Weather:\n\nKross: \"PythonScript::initialize() name=/home/gisovc2006/.kde4/tmp-successfull.job/runningThemes/yGFfDh/liquid_weather.py\"\n/home/gisovc2006/.kde4/tmp-successfull.job/runningThemes/yGFfDh/liquid_weather.py:3656: SyntaxWarning: import * only allowed at modulelevel\n  def checkDependencies(widget):\nKross: \"PythonInterpreter::extractException:\n  File \"/home/gisovc2006/.kde4/tmp-successfull.job/runningThemes/yGFfDh/liquid_weather.py\", line 4, in <module>\n    #                                                            #\n\n  File \"/home/gisovc2006/.kde4/tmp-successfull.job/runningThemes/yGFfDh/liquid_weather.py\", line 34, in <module>\n    bar = sys.stdin.encoding\n\"\nKross: \"Error error='cStringIO.StringO' object has no attribute 'encoding' lineno=34 trace=\n  File \"/home/gisovc2006/.kde4/tmp-successfull.job/runningThemes/yGFfDh/liquid_weather.py\", line 4, in <module>\n    #                                                            #\n\n  File \"/home/gisovc2006/.kde4/tmp-successfull.job/runningThemes/yGFfDh/liquid_weather.py\", line 34, in <module>\n    bar = sys.stdin.encoding\n\"\nAttributeError: 'cStringIO.StringO' object has no attribute 'encoding'\nKross: \"PythonScript::Destructor.\""
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "> cStringIO.StringO' object has no attribute 'encoding'\n\noh, seems that was a 'bug' (well or a missing feature ;) within krosspython. Fixed now in 4.0 and trunk. LiquidWeather starts up now, but it breaks then for me since LW still uses PyQt3 while PyQt4 is needed now :-/\n\nAnyway, thanks for the feedback/bugreport! :)\n\nre the;\n\"But it is still frustrating that a bunch of intelligent people (read developers) just don't care and blindly lead us to Microsoft trap.\"\n\nSoftware patents are a general problem and not only related to Microsoft and Mono. Probably by writting this mail I already did break a few of them without knowledge. So, we are already in traps and the only protection is to get finally right of software patents and to have meanwhile enough protection to stay in a cold war. Yeah, very sad situation for developers. Probably time to start a second carrier as lawyer :-/\n"
    author: "Sebastian Sauer"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "Thank you Sebastian for your effort and time. So we need to wait :|\n\n\"Probably by writting this mail I already did break a few of them without knowledge. So, we are already in traps and the only protection is to get finally right of software patents and to have meanwhile enough protection to stay in a cold war.\"\n\nAt least we do not have this problem in Europe, yet.\n\nP.S. I will update the deb package for Debian in the afternoon."
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "I installed the kdebindings package but it still didn't work :( I keep getting the message that this kross package is missing but it isn't in the openSuse repo.\nThanks anyway."
    author: "Bobby"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-07
    body: "Hi Bobby,\n\nit is part of the kdebindings-package which was also splitted in a python-package which both include the needed lib. See e.g. http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.2/i586/\n"
    author: "Sebastian Sauer"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-08
    body: "Hi Sebastian,\n\nI greatly appreciate your effort and  that you took time out for me. Thanks very much.\nI followed the instruction that you gave me and install both packages. What happens now is that I stop getting this message about missing packages but the Liquid Weather widget is not showing up on the desktop even though it's shown as runnning in SuperkKaramba.\nI will just wait, maybe it will show up soon ;)\n\nJedenfalls danke sch\u00f6n."
    author: "Bobby"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-03-09
    body: "For anyone interested in. Debian doesn't include kdebindings (you need it to get Kross components) in their repositories.\n\nI have created stripped down version of debian kdebindings, free of C#/Mono garbage. \n\nYou can grab it from here\nhttp://www.megaupload.com/?d=65U7FVBH\n\nThere is no guaranty that deb package won't conflict with other packages on your system or that it will work at all. It works for me on DEBIAN/sid.\n\nHave fun!"
    author: "Damijan Bec"
  - subject: "Re: SuperKaramba Widgets & Wallpapers"
    date: 2008-08-08
    body: "plz send me source code of your package for recompilig and upload to my repo\n\nwww.amarok2deb.co.cc\n\navilav [at] gmail [dot]\n\nSee ya"
    author: "Eduardo"
  - subject: "I really hate"
    date: 2008-03-06
    body: "I really hate the retarded time consuming way of logging out of KDE\n\nGo to the menu, select to shut down the computer, then more time wasted as the screen changes shade, and a box pop's up asking if you want to restart, log out, shut down.  For frigs sake, If I wanted to reboot I would of friggin picked that in the start menu, if I wanted to log out I would of bloody picked that.  To pick something from the bloody menu and to not have it do it straight away, but to get a bloody time consuming, time wasting screen come up making me select what option I want is bloody retarded."
    author: "anon"
  - subject: "Re: I really hate"
    date: 2008-03-06
    body: "I agree... i would either \n\n-remove the box popping up asking again \n-or have just the \"leave\" in kickoff with no other choices, and then the popup asking what it is that you want to do."
    author: "MichaelG"
  - subject: "Would this earn your love, then?"
    date: 2008-03-07
    body: "Valid points. \n\nThis is the ideal set of logout/shutdown etc modes, IMO:\n\n*    Launcher (kickoff)->\n**    Leave->\n***   (Menu entries:)\n***    Logout\n***    Switch user\n***    Restart\n***    Shutdown\n***    Suspend\n***    Hibernate\n****      (action is carried out immediately, as long as user has permission to do so, possibly show a cancellable splash while the session is saved on restart/shutdown/suspend/hibernate though, misclicks here are annoying, at least until we all have EFI bios and as quick boots as openSUSE)\n\n*      Desktop context menu->\n**      Leave->\n****     Leave dialog, dim desktop, choices as kickoff\n\n*      Ctrl-alt-del or ACPI power button->\n**      Leave dialog, dim desktop, choices as kickoff\n\n*      Lock/logout applet->\n**      Lock\n**      Logout (immediate)"
    author: "Will Stephenson"
  - subject: "Re: I really hate"
    date: 2008-03-06
    body: "Agreed. Put the option in one place or the other, but not in both. Extremely time consuming"
    author: "Riddle"
  - subject: "Re: I really hate"
    date: 2008-03-06
    body: "Insted of waste energy on iritating yourself on something that obviously will change as soon as somebody has time to fix it, use the bloody logout applet.\n\nYou get desktop lock in one clik, logout or reboot in two. No need to use the start menu at all."
    author: "Morty"
  - subject: "Re: I really hate"
    date: 2008-03-06
    body: "Kubuntu mapped my laptop's power button to the logout menu, so I can just hit the power button, then choose between logout, suspend, hibernate, restart, or shutdown.  The menu part is eliminated, and the choices are very useful to me, as I actually decide whether to hibernate or suspend each time.  Different use cases, different preferences.  (I don't suspend when shutting the laptop lid, in order to use the laptop as a radio.)"
    author: "Dan"
  - subject: "Re: I really hate"
    date: 2008-03-07
    body: "systemsettings->session manager->uncheck \"confirm logout\""
    author: "Dan"
  - subject: "Double click still not working on plasma icons"
    date: 2008-03-06
    body: "For what I know this bug is still present.\nIs it *THAT* hard to fix? :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Double click still not working on plasma icons"
    date: 2008-03-06
    body: "well,if it is easy to fix, please send in a patch :)"
    author: "whatever noticed"
  - subject: "Re: Double click still not working on plasma icons"
    date: 2008-03-06
    body: "Actually it is already fixed in TRUNK, the bug is even \"FIXED\". \nI should have written: \"is that hard to backport this from trunk?\" :-P\n\nSee:\nhttp://bugs.kde.org/show_bug.cgi?id=155413"
    author: "Iuri Fiedoruk"
  - subject: "Re: Double click still not working on plasma icons"
    date: 2008-03-06
    body: "\"Actually it is already fixed in TRUNK, the bug is even \"FIXED\". \n I should have written: \"is that hard to backport this from trunk?\" :-P\"\n\nOr even better, offered to backport it."
    author: "Boudewijn Rempt"
  - subject: "Re: Double click still not working on plasma icons"
    date: 2008-03-07
    body: "OK, I do! Sure, why not? The patch is there already, just a matter of applying and testing.\n\nTo who should I talk?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Double click still not working on plasma icons"
    date: 2008-03-08
    body: "Probably attach your patch to http://bugs.kde.org/show_bug.cgi?id=155413 or just write a mail to the https://mail.kde.org/mailman/listinfo/panel-devel mailinglist.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Double click still not working on plasma icons"
    date: 2008-03-09
    body: "http://bugs.kde.org/show_bug.cgi?id=155413#c12 does contain a backported patch now btw ;)\n"
    author: "Sebastian Sauer"
  - subject: "About shutting down"
    date: 2008-03-06
    body: "Just having it shut down right away might be annoying though. Let's say you accidentally press Shut Down. Then you have to start up the computer again. Just having a Quit button, and then choosing more in detail if you want to shut down, log out, or just cancel, or whatever might be more fitting."
    author: "deniseweird"
  - subject: "Screenshot please?"
    date: 2008-03-07
    body: "Could someone please put up a screenshot of what the configuration dialog for the panel looks like so far? It would be interesting to compare with the KDE3 one. Pic at http://en.wikipedia.org/wiki/Image:Kde_config_panel_arrangement.jpg"
    author: "Parminder Ramesh"
  - subject: "Re: Screenshot please?"
    date: 2008-03-07
    body: "No.\n\nThe current dialog is a really bad temporary hack."
    author: "Dan"
  - subject: "Re: Screenshot please?"
    date: 2008-03-07
    body: ":("
    author: "Parminder Ramesh"
  - subject: "Re: Screenshot please?"
    date: 2008-03-10
    body: "Meh, i can rely on ars technica to deliver: http://arstechnica.com/news.media/plasma-left.png"
    author: "Parminder Ramesh"
  - subject: "rlz"
    date: 2008-03-07
    body: "KDE rulezzzzzz forever!"
    author: "Mihail Mezyakov"
  - subject: "nice work"
    date: 2008-03-07
    body: " Congratulations people, amazing progress, i can\u00b4t wait KDE 4.1, cheers."
    author: "debianita"
  - subject: "Great progress"
    date: 2008-03-07
    body: "I've written some bitter words once but this time I have to say that a lot of great work has been done. I am really impressed. There are still some stability issues (esp. when I use System Settings) but I can really see that KDE 4.0 has become a nice and - what's even more important - usable environment. Good job! Thanks for all those who made this cute thing work."
    author: "mkrs"
  - subject: "Re: Great progress"
    date: 2008-03-14
    body: "Yes, I must agree. I just got done putting 4.0.2 on my new Slackware laptop and I must say even the build process (while long) was relatively easy compared to previous kde versions I've tried. Now to remove those clunky kde3 desktop icons..."
    author: "Tony"
  - subject: "Visual inconsistency woes"
    date: 2008-03-08
    body: "Still much to be done to achieve visual consistency, as per this screenshot: http://beranger.org/blogo12/kde4_visual_consistency.png \nBut KDE4 is looking increasingly great!"
    author: "ac"
  - subject: "Re: Visual inconsistency woes"
    date: 2008-03-08
    body: "Not true for the window decorations, they are from KDE 3 :)"
    author: "Jan"
  - subject: "Re: Visual inconsistency woes"
    date: 2008-03-08
    body: "Since the KDE 4 alpha days I've been reporting visual inconsistency and I'm not sure anyone ever listened.  I think Oxygen looks *almost* great, but I'm not sure there is a consistent theme and design to the KDE 4 desktop.  I really hope KDE 4.1 pulls everything together."
    author: "T. J. Brumfield"
  - subject: "Re: Visual inconsistency woes"
    date: 2008-03-08
    body: "There are different font settings for different widgets. \nso you should first set them all to the same size in order to look for real inconsistencies.\n\nOn my system, all fonts in the gui are the same size.\n"
    author: "Rinse"
  - subject: "Re: Visual inconsistency woes - Youre right!!"
    date: 2008-03-09
    body: "Thanks for pointing that out. You're totally right!!"
    author: "Mike"
  - subject: "I Removed KDE 3 Completely :-)"
    date: 2008-03-10
    body: "I decided KDE 4.0.2 was the time to switch my main desktop environment from KDE 3.5 to KDE 4.0. \n\nWhen you use the Debian packages, the old version is automatically overwritten by the new one (so there's no way to install both KDE version in separate folders). Anyway, I could revert to the old KDE 3.5.9 if I really wanted to but I sure don't want to ;-)."
    author: "lord_rob"
  - subject: "tech demonstrator"
    date: 2008-03-17
    body: "Still, 2/3 of features missing (well, maybe 3/5), java doesn't work in Konqueror, etc."
    author: "szlam"
---
The KDE community has <a href="http://www.kde.org/announcements/announce-4.0.2.php">released another update</a> to its cutting edge KDE 4.0 desktop. KDE 4.0.2 has, along with the bugfixes some new features in Plasma. The panel can now be configured to sit somewhere else than at the bottom and UI options for changing its size have been added. Do not let yourself be distracted by those new things, there are also plenty of bugfixes, performance improvements and translation updates in there, among which support for two new languages: Persian and Icelandic. KDE 4.0.2 is thus available in 49 whopping languages, and more are soon to come. More highlights include rendering improvements in KHTML and lots of bugfixes in Okular and Kopete. The KDE community hopes you enjoy this release which should be hitting your favourite packaging system soon. See the <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php">changelog</a> for more updates and <a href="http://www.kde.org/info/4.0.2.php">info page</a> for download options.




<!--break-->
