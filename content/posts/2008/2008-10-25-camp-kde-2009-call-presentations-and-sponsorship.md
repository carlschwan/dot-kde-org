---
title: "Camp KDE 2009: Call for Presentations and Sponsorship"
date:    2008-10-25
authors:
  - "wolson"
slug:    camp-kde-2009-call-presentations-and-sponsorship
comments:
  - subject: "Thank you"
    date: 2008-10-25
    body: "I would like to extend to you my warmest gratitude from the deep depths of my heart for not calling this \"Kamp KDE\". Good luck with the event!"
    author: "KDE fan"
  - subject: "Re: Thank you"
    date: 2008-10-26
    body: "what's wrong with kamp?"
    author: "nifgraup"
  - subject: "Re: Thank you"
    date: 2008-10-26
    body: "it's a very krave virus krawling and eKstending throukh the blood of some kde'ers that forces them to believe, that any app name or event not wearink a 'k' kannot part of kde ;)\n\nNow \"bakk on topik\", I wish I'd be able to attend this event. Amazing place and amazing people. sigh...."
    author: "uga"
  - subject: "Re: Thank kyou"
    date: 2008-10-26
    body: "KYou forkot kto prefiks kthe kwords kwithout ka k kor k."
    author: "Joe"
  - subject: "Re: Thank kyou"
    date: 2008-10-26
    body: "this!"
    author: "Dr.Schnitzel"
  - subject: "nice website"
    date: 2008-10-28
    body: "Very nice design, big congratulations to whoever did this. Maybe one could give some love for the other kde websites too."
    author: "norman foster"
  - subject: "Print?"
    date: 2008-10-28
    body: "Will the proceedings of this conference be available?"
    author: "JRT"
---
Organisation of Camp KDE 2009 is now moving at full speed and the <a href="http://camp.kde.org/about">event website</a> is now active.  The organisers are now releasing a Call For Presentations (CFP) and a Call For Sponsorship (CFS).  Camp KDE 2009 was <a href="http://dot.kde.org/1224328840/">recently announced</a> on the <a href="http://dot.kde.org/">Dot</a> and will be held in Negril, Jamaica from January 17-23, 2009.  We are excited to continue the momentum of KDE interest shown at the 2008 KDE 4.0 <a href="http://dot.kde.org/1191409937/">Release Event</a> in California.  This new conference will be held at the <a href="http://www.travellersresorts.com/">Travellers Beach Resort</a> in Negril.




<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right">
<img src="http://static.kdenews.org/jr/camp-kde-travellers-resort.jpg"  width="500" height="375" alt="resort photo" /><br />
Travellers Resort
</div>

<h2>Call For Presentations (CPF):</h2>
<p>
Presentations will be given on January 17 and 18.  The organisers have already begun to receive inquiries and offers, but we certainly need more.  Below are several potential topics for Camp KDE 2009 speeches:
</p>
<ul>
<li>KDE core libraries and framework tutorials</li>
<li>KDE Pillars</li>
<li>Qt software</li>
<li>KDE applications and their communities (Office, PIM, edutainment, media, etc)</li>
<li>New concepts such as JOLIE and QEdje</li>
<li>Usability and Accessibility</li>
<li>KDE on Mac and Windows</li>
<li>Free software topics (Linux, BSD, graphics &amp; X11, ODF, etc)</li>
<li>Free software challenges and opportunities (government, business, equality, etc)</li>
<li>Distro/vendor/corporate talks</li>
<li>Local cultural talks</li>
<li>Developer sprints topics</li>
</ul>

<h2>Call For Sponsorship (CPS):</h2>

<p>
Camp KDE 2009 is proud to announce two early sponsors:
</p>
<ul>
<li><a href="http://www.ixsystems.com/company/about-us.html">iXsystems</a>: You may know them</a> because of their desktop initiatives at electronic chains, their server offerings, or their collaboration with the BSD family (notably our friends at <a href="http://www.pcbsd.org/">PC-BSD</a>).</li>
<li><a href="http://www.google.com">Google</a>:  They were kind enough to host our Release Event in January and they are back to assist with Camp KDE 2009.</li>
</ul>
<p>
Of course, Camp KDE 2009 is still looking for more sponsors.  We are open to involvement of any kind and participation ideas are welcome.
</p>
<p>
If you are interested in either giving a presentation or conference sponsorship, do not hesitate to contact organisers at campkde-organizers&#64;<span></span>kde.org.  Help get this new KDE conference series off to the best start possible!</p>



