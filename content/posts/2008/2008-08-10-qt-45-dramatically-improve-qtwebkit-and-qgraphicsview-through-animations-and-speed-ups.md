---
title: "Qt 4.5 to Dramatically Improve QtWebKit and QGraphicsView Through Animations and Speed Ups"
date:    2008-08-10
authors:
  - "skuegler"
slug:    qt-45-dramatically-improve-qtwebkit-and-qgraphicsview-through-animations-and-speed-ups
comments:
  - subject: "Benchmarks?b"
    date: 2008-08-10
    body: "are there any benchmarks? does it partialy solve the nvidia problem? etc"
    author: "Beat Wolf"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-10
    body: "That was my first thought as well.  The nVidia part, that is.  I hope so.  I personally have only used KDE4* on Live CDs since KDE4* isn't ready for \"me\", yet (yes, I like *real* icons on the desktop, and a hideable \"kicker\").  I was going to jump to 4.1 because, well, I really want to, but the nVidia problems have kept me at bay hoping that by 4.2, nVidia will have gotten their act back together enough.\n\nIf these performance boots help make KDE4* on nVidia cards much less painless, then I can work around some of the other limitations of KDE4 until they are implemented."
    author: "Xanadu"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-10
    body: "Why should the KDE or Qt guys fix nvidia bugs ?\nnVidia recognizes that it's their fault and will release a drivers fixing these bugs, but it's not a KDE or a Qt problem when the vesa driver is faster than the nvidia proprietary driver."
    author: "Pinaraf"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-10
    body: "well, my question was actualy if the speedup makes up somewhat for the nvidia slowdown. but i forgott that the new driver will probably be out before i got a kde 4 with qt 4.5"
    author: "Beat Wolf"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-11
    body: "I wasn't suggesting that TrollTech or KDE Devs somehow become nVidia gurus and fix their busted module.  I was more asking if these tweaks that have been made make life easier for us nVidia users.\n\nI'm not nieve enough to thing that TT/KDE is somehow responsible for nVidia screw ups.  \n"
    author: "Xanadu"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-11
    body: "We hope confidently that the nvidia issues are solved *before* 4.5 ships. Qt 4.5 might make some things on nvidia faster, but it's really general optimizations, not nvidia-specific ones. All users should benefit from those optmizations, since they for example improve clipping on graphicsview."
    author: "Sebastian Kuegler"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-11
    body: "Nvidia's problem are more OpenGl oriented so the Nvidia problem can only be fixed by Nvidia. QGV doesn't use Graphics Acceleration."
    author: "M\u00e9nard Alexis"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-11
    body: "nvidias opengl driver is fine. their 2d acceleration isn't working with qt4. and graphicsview can actually use opengl for rendering."
    author: "ac"
  - subject: "Re: Benchmarks?b"
    date: 2008-08-12
    body: "NVidia's 3D drivers are among the best.\nNVidia's 2D drivers suck. All 2D is a bit slow, but it is usually not very noticeable. Qt4 amplifies the inefficiency by making heavy use of 2D acceleration.\n"
    author: "Michael \"tactless\" Howell"
  - subject: "Mmm hmm.  That's nice."
    date: 2008-08-10
    body: "Should have been here about 10 years ago.  I know some SMIL 2.0(HTML+Time 2) and these years waiting for these type of things to go mainstream have made me indifferent.  By the way, how old is Adobe's SVG viewer that has 1.0 capabilities?  And SVG 1.0 is still not fully available on the 'cool' browser today.  The third world war will probably arrive by the time developers get their acts...oh never mind...what's the point."
    author: "charlie"
  - subject: "Re: Mmm hmm.  That's nice."
    date: 2008-08-11
    body: "Adobe gave up on their SVG plugin since they bought Macromedia Flash. What else could we expect from them...\n\nhttp://www.adobe.com/svg/eol.html\n\nHopefully Firefox is going to make their svg viewer usable on IE (same with canvas and JS2.0)."
    author: "Lisz"
  - subject: "Re: Mmm hmm.  That's nice."
    date: 2008-08-11
    body: ">>Hopefully Firefox is going to make their svg viewer usable on IE (same with canvas and JS2.0).\n\nHuh? Can you please elaborate? Is there any truth on that or is just a wish?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Mmm hmm.  That's nice."
    date: 2008-08-13
    body: "Uh... What? That statement made no sense. On so many levels.\n\n1) It's not like it's a plugin or something. It's integrated into gecko's rendering engine\n\n2) Why would gecko devs care anything about IE?\n\n3) IE sh!ts it's pants whenever it is handed XML anyway. (SVG is just an XML namespace)"
    author: "a-non-a-moose"
  - subject: "Re: Mmm hmm.  That's nice."
    date: 2008-08-12
    body: "I can tell you mean well, but your statement comes off as being very trollish.\n\nTrying to explain in a simple way: IE sucks and it's in M$'s best interests NOT to add cool non-proprietary stuff to IE. There isn't as much motivation to work on SVG and friends because mainstream web can't use it. If IE wasn't the mainstream browser web page authors would be free to use those features, and developers would have more reason to work on them.\n"
    author: "Michael \"I'm sorry\" Howell"
  - subject: "Re: Mmm hmm.  That's nice."
    date: 2008-08-13
    body: "Ah, how is it a 'troll' when the reality is that the SVG implementation in Firefox is not 100% complete; may I suggest you look at the latest info on it, and you'll find that large chunks of the SVG specification haven't been implemented yet.\n\nAll very nice to rave about how great a feature is, but pointless if it isn't implemented properly."
    author: "Kaiwai"
  - subject: "typo"
    date: 2008-08-10
    body: "there's a typo where a sentence is split in two :\n\n\"After Qt 4.4 brought the goodness of widgets-on-canvas and lots of optimisations that especially speed up Plasma. More breakthroughs in the development of Qt's graphics canvas are coming up.\"\n\nshould be a single sentence, .->, and M->m"
    author: "richlv"
  - subject: "Re: typo"
    date: 2008-08-11
    body: "You are wrong."
    author: "NabLa"
  - subject: "Re: typo"
    date: 2008-08-11
    body: "I think he's right."
    author: "J. M."
  - subject: "Re: typo"
    date: 2008-08-12
    body: "He's right, but the resulting sentence after the change isn't exactly pretty either... really not pretty."
    author: "PurplePerson"
  - subject: "Re: typo"
    date: 2008-08-13
    body: "\"After Qt 4.4 brought the goodness of widgets-on-canvas and lots of optimisations that especially speed up Plasma. More breakthroughs in the development of Qt's graphics canvas are coming up.\"\n\nThis is fine, but to be succinct and proper with English Grammar:\n\n``After Qt 4.4 brought the goodness of widgets-on-canvas and lots of optimisations that especially speed up Plasma--more breakthroughs in the development of Qt's graphics canvas are coming up.''\n\nIn LaTeX terminology there should be a \\textemdash for the ``dash'' whose purpose is to emphasize interruptions in sentences by:\n\n1. to emphasize explanations, including appositives, examples, and definitions;\n2. to emphasize a contrast;\n3. to emphasize an ``aside''; or\n4. to show hesitating or broken-off speech\n\n"
    author: "Marc Driftmeyer"
  - subject: "Too bad"
    date: 2008-08-10
    body: "Too bad KDE 4.2 will not be based on Qt 4.5, that means we'll have to wait for KDE 4.3 to have a good web experience using Qt/KDE technologies (KHTML is not good enough unfortunately)."
    author: "Lisz"
  - subject: "Re: Too bad"
    date: 2008-08-10
    body: "Aren't there QT 4.5 preview packages available right now?  And don't they include a basic QT/Webkit browser?"
    author: "T. J. Brumfield"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "We're on a 6 month release cycle though, KDE 4.3 will be released pretty soon after Qt 4.5. "
    author: "Ian Monroe"
  - subject: "Re: Too bad"
    date: 2008-08-13
    body: "You mean July 2009."
    author: "Marc Driftmeyer"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "As far as I know, no decision wether to depend on Qt 4.5 has been taken, so you're \"Too bad ...\" is a bit premature."
    author: "Sebastian Kuegler"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "> you're \"Too bad ...\"\n\nit's \"your \"Too bad...\"\"\n\nand kde4.2 will be based on Qt4.5, it's been said on the ML."
    author: "Lisz"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "Ha yea, I was confused. Right, I don't see why KDE 4.3 wouldn't be based on Qt 4.5..."
    author: "Ian Monroe"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "Will the performance-enhancing improvements introduce incompatible API changes? Will it be possible to get some of the performance improvements by building KDE against Qt 4.5, even if it doesn't actually require it?"
    author: "Ben Morris"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "From reading the blog where the Troll mentioned the 40x improvement it *sounds* like it doesn't require changes to the applications, so I think (at least that change) could be gotten for free just by upgrading your copy of Qt.\n\nEven if it requires a change it probably won't be too big, and if it would have a large impact the distros could ship a patched version of KDE with a 4.5 dependency.\n\n\n(mind you, I'm just guessing based on the wording in the blog)"
    author: "Kit"
  - subject: "Re: Too bad"
    date: 2008-08-11
    body: "As far as I know Qt has strict API and ABI policies for forward compatibility so any application written with Qt 4.x should be compatible with Qt 4.y where y > x for the whole life cycle of the Qt 4 series."
    author: "Erunno"
  - subject: "Re: Too bad"
    date: 2008-08-12
    body: "Not doing so would mean shooting themselves very hard in their commercial foot...\n\nsomething they probably won't enjoy."
    author: "Mark Hannessen"
  - subject: "Re: Too bad"
    date: 2008-08-12
    body: "KDE4.0 could easily be built against 4.1, and get many of the improvements (performance stuff, bugfixes, etc). KDE4.1, if built against Qt4.5, would get the performance and WebKit improvements without any source modifications. Even if KDE4.2 isn't dependent on Qt4.5, you can always compile against it and get the improvements.\n"
    author: "Michael \"wait!\" Howell"
  - subject: "Re: Too bad"
    date: 2008-08-13
    body: "You don't need to recompile. Just install the new Qt libs."
    author: "yoda"
  - subject: "Omg! Omg!"
    date: 2008-08-10
    body: "I can't believe my eyes!!! A new animation API too !! That was what I was waiting for :D Please please, post the talks online, some timings, git references or whatever.\nI need more!! ;-)"
    author: "superfan!"
  - subject: "Use of Qt in Mozilla"
    date: 2008-08-10
    body: "Great work peeps!\n\nNow I'm sure most of you have heard that a port of Mozilla 1.9 (and Firefox 3) to Qt 4 is underway. On one of the developers' blogs (http://blog.vlad1.com/2008/05/06/well-isnt-that-qt/), he notes that while Qt is a well-designed modern toolkit, it has a few quirks:\n\n*He notes that a drawGlyphs method on QPainter \u0097 something that would take a QFont and an array of glyphs and positions - is missing;\n*QPainter can\u0092t do arbitrary path clipping with antialiasing\n*Cairo has a few optimizations (e.g. with paths) that QPainter doesn't.\n\nWill these quirks be addressed in upcoming Qt releases? It would be awesome to have a better-integrated Firefox (Konqueror is great too, but just doesn't offer the same array of extensions). Thanks!"
    author: "Parminder Ramesh"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-11
    body: "Actually, having Firefox do its rendering via Qt has little to do with integration. Integration of the webbrowser is about a bookmark system, shared proxy settings, mimetype settings, certificate integration with KIO, password integration with kwallet and of course things like file dialogs from KDE. Using Qt as rendering engine is just one step."
    author: "Sebastian Kuegler"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-11
    body: "File dialogs is the killer thing for me.  I can't stand the damned GTK file dialogs.  And yet I really love Firefox.  And I think on openSUSE which I've been using lately, I set proxy settings in KDE, and Firefox recognizes them.  There is an option in Firefox (it may be an openSUSE patch) to recognize system proxy settings.\n\nWeb browsers often have separate mime settings from the rest of the system, because you either open it as a plugin, save the file, or pass it to an app.  It is more than just saying app x is related to that file.\n\nkioslave and kwallet integration would be nice, but frankly Firefox stores passwords well enough on its own, where as I find the kwallet pop-ups to be a little annoying."
    author: "T. J. Brumfield"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-11
    body: "There is a hack around that makes a lot of GTK applications use the Qt dialogs instead. Works pretty well, and makes GTK apps play a bit nicer in your Qt environment. Of course, don't expect KIO tricks to works all of a sudden."
    author: "Andr\u00e9"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-12
    body: "How nice of you not to mention what it's called. Just that it exists... buhuuu!"
    author: "ac"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-12
    body: "http://kde-look.org/content/show.php/KGtk+(Use+KDE+Dialogs+in+Gtk+Apps)?content=36077"
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-12
    body: "Sorry, yes. I did not have time at that point to look it up. See it as an exercise to hone your Google skills ;-)"
    author: "Andr\u00e9"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-12
    body: "I can't imagine a Qt firefox using a Gtk file dialog, could you ;). Actually, I'd expect it to use the standard QFileDialog. While it's not as awful as the GTK one, it isn't KDE either. Of course you can always use KGtk or KQt, respectively.\n"
    author: "Michael \"GTK file dialogs\" Howell"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-08-12
    body: "I've used the latest build and at this moment it does use the Qt file dialog as expected.\n\nIt's total pre-alpha material and practically unusable for normal browsing, but it's still sorta cool....\n(It also has a lot of potential!)"
    author: "Jonathan Thomas"
  - subject: "Re: Use of Qt in Mozilla"
    date: 2008-11-16
    body: "Always good to hear. I may be hopelessly addicted to KDE's application-specific file dialog bookmarks and I may be doing fairly well with KGTK, but it'll definitely be a step in the right direction to have native support for Qt file dialogs."
    author: "Stephan Sokolow"
  - subject: "QtWebKit API"
    date: 2008-08-11
    body: "I could wait a few more days for the videos to be published (but whats the fun in that? :P), but did the talk cover anything about how the QtWebKit API is going to be extended? The main-snapshot documentation (on doc.trolltech.com) has already been extended to include QNetworkDiskCache but I haven't noticed anything about exposing the DOM (directly via the C++ API) or anything about NS plugin support in a while (I had heard that it was in the svn server upstream a while ago)?\n\nI'm also hoping there'll be more information about the new KJS implementation (Frostbyte, I think it was called?) and some of the other stuff going on in KHTML that I've seen blogs somewhat mentioning in the past.\n\nSo far I've really enjoyed developing against QtWebKit (the trolls have done an outstanding job with the API so far!) and loved being a Konqueror+KHTML user for quite a while, both are very nice rendering engines and I hope we get to have both actively developed for a long time to come!"
    author: "Kit"
  - subject: "Re: QtWebKit API"
    date: 2008-08-11
    body: "Frostbyte went into SVN before KDE 4.1, http://www.kdedevelopers.org/node/3476"
    author: "Morty"
  - subject: "Re: QtWebKit API"
    date: 2008-08-11
    body: "For netscape plugin support I can tell you that QtWebKit in trunk already has this support.  And as for public API to control these plugins see this:\n\nhttp://code.staikos.net/cgi-bin/gitweb.cgi?p=webkit;a=shortlog;h=adam/netscape-plugin-manager\n\nAlso, squirrelfish is the name of the new JavaScript engine in WebKit.  And yes, this will be included in the next version.\n\nI'm not sure that a public DOM API will be ready for the next version.  It is a big undertaking and could dramatically increase the size of the public API.  What's more, it is important to get it right."
    author: "manyoso"
  - subject: "Re: QtWebKit API"
    date: 2008-08-11
    body: "You won't see much information on KJS from aKademy since neither Harri nor myself are there; nor is the KHTML uber-guru Germain. ...And besides, being old-school KDE guys we're not big on PR and flashy demos anyway. The initial frostbyte version (-41.0) is indeed in 4.1.0, and I've tweaked a few things since then, though nothing major. I tend to work in a funny way where I spend a lot of time thinking over a design problem at odd times, then when I am I finally happy I go ahead and try stuff... Except even on small stuff I tend to sit on patches for a while since something is always bothering me. \n(e.g. stuff like better for ... in behavior on dense arrays and better handling of large \n object literals inspired by one of the posts by vandenoever).\n\nWrt to KHTML stuff: the biggest thing in 4.1 would be contentEditable, though the command set is currently somewhat limited.  For 4.2, an ultra-smart SoC student is working on picking up the SVG code from WebCore, and is trying to do so largely through a compatibility layer to keep things from diverging. Though, it is hard since we have a far more minimalistic design philosophy. Can't really give any big KHTML plans from myself, since I tend to just pick off areas which I think need work... Right now e.g. load events from iframes/objects are a bit wonky, though their behavior overall is a lot better after \nthe big rework in 4.0...\n"
    author: "SadEagle"
  - subject: "Re: QtWebKit API"
    date: 2008-08-12
    body: "<em>being old-school KDE guys we're not big on PR and flashy demos anyway</em>\n\nThat tastes a bit odd: showing of a new feature once in a while or posting short blogs about current development has nothing to do with PR or \"flashy\" demos. It is another way of communcation with the users.\n\nThere are several ways to communicate with users: e-mail, blogs, news, irc, bugzilla, surveys, and so on. Every project picks the set they think fits their style best.\nYou didn't pick \"web presence\", ok - but putting it like \"I'm too cool for company like PR\" is just plain wrong, both things have nothing in common."
    author: "Whatever"
  - subject: "Re: QtWebKit API"
    date: 2008-08-14
    body: "\"That tastes a bit odd\"\n\nSpeaking of odd, he never said what you imply in your post, he just said he doesn't (often/ever) show up at those big akademy get togethers that KDE has, which is not the same as saying he doesn't talk to \"users\" at all (heck, he posted here didn't he?).  Jeez, lighten up.\n"
    author: "True Grit"
  - subject: "hidden developers?"
    date: 2008-08-14
    body: "> he posted here didn't he?\n\nA very good point. ;)\n\nMost developers I've talked to say they ignore dot comments these days because of all the flames.\n\n\nIncidentally, the khtml group is very active on irc. I guess they need to put out press releases too? That seems to be the way to work these days. Although anything they say will instantly get bogged down in flames. :P\nFurther reason to hide. ;)"
    author: "blauzahl"
  - subject: "When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "For want of a better place to put this, and so it gets any attention it can, I really need the \"hideable panel\", and cant really switch back to kde without it, so I'm using gnome for now, but I always used to use kde, and I REALLY like the new 4.1, but kinda-sorta-really-need that (configurable...) \"hideable\" thing happening...?? - (Great job though, everyone!)"
    author: "r. l."
  - subject: "Don't like KDE 4.x so I must use Gnome"
    date: 2008-08-11
    body: "I don't understand this.  Why not just use KDE 3.5.x?"
    author: "T. J. Brumfield"
  - subject: "Re: Don't like KDE 4.x so I must use Gnome"
    date: 2008-11-16
    body: "Could be a[n] Ubuntu/Kubuntu user. Their packagers considered KDE 3.5 \"antiquated\" and dropped every KDE 3.5 component with a KDE 4.x equivalent as of Intrepid Ibex."
    author: "Stephan Sokolow"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "\"hidable panel\" is the most annoying feature ever, I hope it never gets implemented so I never have to use a PC that uses this lame feature. It's just so annoying to see the panel appear and disappear all the time when I just want to use it."
    author: "Lisz"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "Some users like it. As long as you can turn it off, it's better to have it than not to. If you have to use a PC with it turned on, just turn it off and turn it on again when you leave (if you don't have your own user account)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "I'm sorry to derail this fine thread, but this argumentation is beyond retarded. First of all, no distribution I know turns on hideable panels by default. It's an option strictly for the people who feel that they want to use the panel occasionaly but need the extra screen space otherwise. Nobody \"forces\" you to use it other than yourself. Plus, can anybody get more self-centered by proclaiming that your special dislike for one feature should be forced onto a whole community of users, especially if it's not even a fringe case?\n\nOr you are just trolling and I should stop feeding you. :-/"
    author: "Erunno"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "He's obviously trolling but this is somehow what the \"hiding panel complainers\" deserve, cause they're doing so much ado for a feature used by a minority of users (and in NO WAY blocker for a KDE release)."
    author: "Vide"
  - subject: "well, that's my problem with KDE4.."
    date: 2008-08-13
    body: "a lot of 'features used by a minority of users' where cut and not reintroduced, especially the many things that made KDE3 so lovely for power users (f.e. interface short cuts saving two three clicks/key presses on every file action, compared to windows, packing the panel full of features without sacrificing a pixel to white space)...\nNot only that, but the strive to GUI-wise turn KDE4 into a Vista/Leopard hybrid (i.e. hiding 'complexity', removing configurability in favor of 'defaults that please 90% of the users') totally took the fun out of using it for me. And i really tried to like it, having installed from SVN since beta phase and regularily upping. But i do see no progress on winning back the hearts of power users, just bling hype, overstatement and complacency...\n(and yes, as soon as i have the time to do it, i will contribute patches.)"
    author: "eMPee584"
  - subject: "Re: well, that's my problem with KDE4.."
    date: 2008-08-13
    body: "\"a lot of 'features used by a minority of users' where cut and not reintroduced\"\n\nYet."
    author: "Anon."
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "It depends on how large your panel is and how you use it.\nIt's the center of my universe, and it occupies about a fifth of my screen.\nNo delay, of course."
    author: "reihal"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "I'm not a fan of the panel hiding feature, but one increasingly important use case for having a panel that can be hidden is on UMPCs like the Asus eeePC and the new crop of UMPCs. These machines have screen resolutions around 1024x600 and every pixel is precious. A hideable panel gives more extra vertical space, even if it might be a bit annoying.\n\nTom-with-the-Acer-One here at akademy yesterday showed me his work-around for this problem. Remove the panel and add the k-menu and taskbar to the desktop itself at the bottom. \n\nNice machine too, I've got one ordered for the wife. 8-)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "\"I'm not a fan of the panel hiding feature, but one increasingly important use case for having a panel that can be hidden is on UMPCs like the Asus eeePC and the new crop of UMPCs. These machines have screen resolutions around 1024x600 and every pixel is precious.\"\n\nTrue. Another important feautre to have in those machines is the MacOS-type menubar. Instead of wasting space by having each app have a separate menubar, there would be just one universal menubar.\n\nIf I were asked, the MacOS-menubar would be enabled by default in KDE: At least that would eliminate most of the \"KDE is a copy of Windows!\"-whining..."
    author: "Janne"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-13
    body: "Speaking of hiding menu bars - I have for years and years wanted to \"reimplement\" the AmigaOS menubar in KDE. Unlike the MacOS menubar that is constantly there, the AmigaOS menubar only pops up when you press the right mousebutton. While you have this mouse button pressed, you can move around in the menues, click entries, set and unset checkmarks with left mousebutton - and once you release the right mousebutton, all actions you have clicked will be executed, in the order you pressed - this is what we old amiga users call \"multiselect in menues\", and that noone else seem to have implemented - ever, sadly.\n\nI miss this very much in f.ex image processing programs that tend to have long and deep menues. For every action I have to wade through the menues over and over again, instead of just go once. Instead I want to go to menu, click blur, click resize, click save, let go of menu button, see it blur, resize popup coming up, enter new size, press OK, file saved, all done by going through menues only once.\n\nAnyways, a start would be to let KDE be able to hide panels, and assign hotkeys for them to pop up - my hotkey would ofcourse be the right mousebutton.\n\nThis also brings me to another thing I miss - complete control over input modifiers, I want to define what happens when I click a file with certain modifier keys pressed, for example if I click a png I want it to open with kview (or other simple viewer), but if I click a png while holding down ALT-key, I want it to open in an editor, like f.ex gimp. To be able to define actions for click, double click and even tripple click would be most welcome - yes \"most users\" will never need them, I know. I hate \"most users\", they ruin it for the rest of us :P\n\nLikewice, I want to be able to define what drag&drop does with modifiers, f.ex I might want to drag&drop of image files while holding a key converts all files to pngs in the destination. \n\nI also miss the ability to assign certain commands to certain directories, so that if I f.ex have a directory with images (album), I can set it to open with kview instead of just opening it in the file browser (the default tool tooltype, for those who know amigaos)\n\nWhile I'm on this wishlist - it would be excellent to have the desktop and the default filebrowser (dolphin) act as one, ideally (IMO), the desktop itself should be dolphin."
    author: "Kolla"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-13
    body: "I prefer the NeXTSTEP Tear off menus that allow for saved state and auto change between applications while allowing for maximum Vertical Space.."
    author: "Marc Driftmeyer"
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-12
    body: "Thank you for the useful tip, (I am the one who started this whole conversation...!) I appreciate that. All the best!"
    author: "r. l."
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-12
    body: "Oooops! I pressed the wrong \"reply-to-this\" button, Sorry! this comment was meant for the post above!"
    author: "r. l."
  - subject: "Re: When is the \"hideable panel\" back?"
    date: 2008-08-11
    body: "this has been gone over a number of times already\n\nhttp://techbase.kde.org/Schedules/KDE4/4.2_Feature_Plan"
    author: "borker"
  - subject: "Corrections"
    date: 2008-08-11
    body: "First of all, very cool article. As I mentioned in this talk, 4.5 is going to be great. However I feel a minor correction is in place - we're researching animations, transitions, and effects, but these feature are not yet tied to any roadmap nor release target date (in the talk I believe I said \"4.5, 4.6, 4.7, we'll see once we know more\").\n\nIn any case, the future is looking very bright IMHO."
    author: "Andreas Aardal Hanssen"
  - subject: "Re: Corrections"
    date: 2008-08-11
    body: "I've clarified this in the article now.\n"
    author: "Jonathan Riddell"
  - subject: "Does it scale down?"
    date: 2008-08-11
    body: "Qt is big on the embedded scene, where the hardware isn't necessarily this week's Dell XPS. Are there performance improvements for slower CPUs and low end video cards?"
    author: "David Johnson"
---
At <a href="http://akademy2008.kde.org/">Akademy 2008</a> in Belgium, Qt developers Simon Hausmann and Andreas Aardal Hanssen announced dramatic improvements in the web browser engine in Qt and the canvas that is used by, for example, the Plasma desktop shell. Video support, animations and transitions, optimisations to speed up painting and animations, and new graphical effects open up nearly endless new possibilities for developers to present their user interfaces with. Read on for more details.






<!--break-->
<div style="float: right; padding: 10px">
<img src="http://static.kdenews.org/jr/akademy-2008-qt45.jpg" width="500" height="313" /><br />
Reflecting a video in QtWebKit
</div>

<p>Hausmann demonstrated a WebKit based Konqueror web browser displaying video content through the HTML 5 video tag. With only one line of HTML code, video content in Ogg format can be embedded in webpages. Style elements can then display reflections of this video widget. Naturally, the underlying multimedia engine used in QtWebKit for showing video content is KDE's Phonon multimedia layer. Animations will also be possible. By adding simple CSS properties to your webpage elements, you can animate, rotate and fade those elements. Writing rich and animated webpages and of course embedded web content in applications becomes a breeze with QtWebkit.</p>

<p>After Simon's demo of QtWebkit, Andreas Aardal Hanssen entered the stage to show some of the improvements that will be in Qt 4.5 which will be released later this year or in early 2009. After Qt 4.4 brought the goodness of widgets-on-canvas and lots of optimisations that especially speed up Plasma, more breakthroughs in the development of Qt's graphics canvas are coming up. Speed-ups of up to <a href="http://labs.trolltech.com/blogs/2008/08/06/in-the-flow/">40 times</a> faster in some cases go along with a set of really nice additions that are unparalleled in today's toolkits and can only be dreamt of in competing offerings. Improvements that will be available in Qt 4.5 include:</p>

<ul>
	<li>Transition animations in user interfaces</li>
	<li>Optimisations in painting operations </li>
	<li>They are also looking into extensive support for animations through a new animation API, due for an unspecified future Qt version.  Graphical effects like blur, bloom, shadow and opacity for items on the GraphicsView are being looked into as well.</li>
</ul>

<p>Hausmann's and Hanssen's future plans for those components in Qt have been met enthusiastically by the KDE developers. Soon, these new features and optimisations will be available to KDE users around the Free world.</p>



