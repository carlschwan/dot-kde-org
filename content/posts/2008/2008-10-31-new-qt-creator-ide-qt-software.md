---
title: "New Qt Creator IDE from Qt Software"
date:    2008-10-31
authors:
  - "rmoore"
slug:    new-qt-creator-ide-qt-software
comments:
  - subject: "KDevelop"
    date: 2008-10-31
    body: "Too bad Qt Software didn't decide to push KDevelop instead of reinventing the wheel. Now we have two \"beta\" IDEs instead of one \"finished\" powerful one :-(\n\nBut I hope KDevelop and Qt Creator will go on nicely and I will have a replacement for KDevelop3 pretty soon ;-)"
    author: "Birdy"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Well, as much as I'd like to see KDevelop succeed.... QtCreator to me is already much more stable, and debugging actually works!\n\nIn stead of adding new functionality KDevelop should focus on stabilizing. In our company Kdevelop didn't pass 'the test' because of issues like stability and poor debugging capabilities.\n\n"
    author: "Harry"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> \"Now we have two \"beta\" IDEs...\"\n\nNo, we have a \"first technical preview\" (so not even beta) of an IDE with a radically different approach than KDevelop) and an IDE that was almost dead at the time that Qt Creator was started, but now seems to be alive again, still with the same goals that are different from Qt Creator.\n\nI am sure that Qt Creator will lose its rough edges (would be nice if you could eloborate about them Rich, even if they are numerous) and if the KDevelop team keeps up pushing like they seemed to do in the last months, they will likely also end up with something useful.\n\nNow for which concept can win more users: We will see about that. But it's really important to see that those are not about two camps working on the same thing but about solving the same problem with two totally different approaches. Ultimately the users (and by 'users' I mean 'developers') will decide on which approach is the better one.\n"
    author: "Daniel Molkentin"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Sure, here are some of the issues I noticed:\n\n- Missing tooltips on the buttons in the sidebar, initially they don't seem to do anything but aren't greyed out.\n- Project names with spaces in seem to cause problems\n- opening a file in the editor, i seem to have an empty drop down to the right on the one listing the open files.\n- Switch between method declaration/definition is enabled even when you didn't click on a method.\n- Setting for the default editor are not inherited by the C++ editor (eg. setting it to visualise whitespace).\n- Can't right click on class name and get to the docs.\n- Can't print a selection.\n- Switching between build types has radio behaviour but displays as checkbox items in the build settings view.\n- No project editor, you just end up editing the .pro file in a text edit field in the IDE. (eg. if you want to add a module to the project).\n- The Output view is rather weak.\n\nNone of these are massive issues, but they definitetly made it feel rough to me.\n"
    author: "Richard Moore"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Please report those on the feedback mailing list, where we collect feedback and can respond to it.\n\nBtw: Nice article, Richard, although I sense a tenor of \"I really don't want to like this project, but unfortunately I cannot seem find any real major flaws with it.\" Is this just the typically KDE-project negativeness towards anything new? Or me being overprotective towards a new baby?\n\nYou are absolutely right that this is alpha code and has rough edges. The GUI itself is only halfway done. You will see heavy development on it in the coming month.\n\nJust a few quick additions/answers to the points you made:\n\n- \"Can't right click on a class name and get to the docs\"\nOh you context menu people! Simply press F1. You are right of course, there's a lot more missing from the menus currently.\n\n- \"Switching between build types has radio behaviour but displays as checkbox items in the build settings view.\"\nFunny checkable treewidget items. We all want itemviews-ng.\n\n- \"No project editor, you just end up editing the .pro file in a text edit field in the IDE. (eg. if you want to add a module to the project).\"\nThat's a feature, and a lesson learned from all of us who (still) use text editors. Real-world pro-files are too complicated to be messed up by an IDE. Without Creator, you would edit the file, too. If you integrated the current KDE build system, you would do exactely the same. I hate it when IDEs force you to let them mess with the build scripts. Convenience wizards would be nice, and maybe that's what you would like to see, but hiding the pro-file-syntax completely behind some clumsy form GUI would annoy people more than it would help. My opinion only. \n\n- \"Project names with spaces in seem to cause problems\"\nThat's a terrible qmake limitation, you will have it with every IDE that uses qmake.\n\n"
    author: "MatthiasEttrich"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Could you sort me out a posting account for the Qt Software news server? The application page at http://trolltech.com/newsapply.html still says \"The registration form for our mailing lists [sic] is currently offline, give us a day or two and it will be back in action.\""
    author: "Chris Hills"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> although I sense a tenor of \"I really don't want to like this project, but\n> unfortunately I cannot seem find any real major flaws with it.\" Is this just\n> the typically KDE-project negativeness towards anything new? Or me being\n> overprotective towards a new baby?\n\nI think you're being a bit overprotective here, but I'm not a big fan of IDEs in general. I was pleasantly surprised by this one, especially the fact the GUI was nice and simple rather than having enough buttons to run a space ship. :-) I think the fact that I was able to go from installing the app to geting a working browser without any major issues or needing to resort to the documentation speaks for itself.\n\nInteresting comment about the project editor. I can definitely see your point, as that is something I've hated in Visual Studio when I've used it in the past. I think the fact that I'm not that familiar with qmake's .pro files was the problem, so I had to look up what the module was called when I needed to go back and change the QT += line.\n\nRegarding the issue with spaces, sounds like a case for QValidator. :-)\n\n\n"
    author: "Richard Moore"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "I forgot to say that'll be reporting stuff to the preview list once I've had a chance to look at the app in more detail over the weekend. Don't forget I had yesterday evening to play with it and write the review, so I'm sure there's stuff I missed.\n"
    author: "Richard Moore"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: ">> I think the fact that I'm not that familiar with qmake's .pro files was the problem, so I had to look up what the module was called when I needed to go back and change the QT += line.\n\nSounds like the best of both worlds would be a manual pro file editor with very aggressive code completion for the qmake syntax."
    author: "Leo S"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Yes, that would certainly help me.\n"
    author: "Richard Moore"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: ">> Is this just the typically KDE-project negativeness towards anything new? Or me being overprotective towards a new baby?\n\nThat would be the latter..  Since when is the KDE project negative towards new ideas?   \n\nPersonally I think Qt Creator is a godsend, if it's half-decent.  The major problem with Qt is that none of the IDEs are any good.  I can't justify paying for Visual Studio (although I have a commercial Qt license) so the Visual Studio integration is not useful to me, KDevelop is too buggy and cluttered to use, text editor + command line is a bit too primitive, Edyuk usually either fails to compile or fails to run for me, same with Monkey Studio.  QDevelop is decent, but has some annoying bugs and seems to be largely unmaintained in the past 6 months, and Eclipse is a bloated pig that takes over everything and loves to crash when I use it with the Qt integration.\n\n>> That's a terrible qmake limitation, you will have it with every IDE that uses qmake.\n\nAny chance of that being fixed?  I've run into lots of qmake issues with spaces (like LIBS paths)"
    author: "Leo S"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> Personally I think Qt Creator is a godsend, if it's half-decent.\n> The major problem with Qt is that none of the IDEs are any good.\n> I can't justify paying for Visual Studio (although I have a\n> commercial Qt license) so the Visual Studio integration is not\n> useful to me, KDevelop is too buggy and cluttered to use, text\n> editor + command line is a bit too primitive\n\n+1 "
    author: "Stefan"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "\"Personally I think Qt Creator is a godsend\"\n\nMe too, and I think it's logo should be that \"Heroes\" sign because of this"
    author: "Paulo Cesar"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> That's a feature, and a lesson learned from all of us who (still) use text\n> editors. Real-world pro-files are too complicated to be messed up by an IDE.\n> Without Creator, you would edit the file, too. If you integrated the current\n> KDE build system, you would do exactely the same. I hate it when IDEs force\n> you to let them mess with the build scripts. Convenience wizards would be\n> nice, and maybe that's what you would like to see, but hiding the\n> pro-file-syntax completely behind some clumsy form GUI would annoy people\n> more than it would help. My opinion only. \n\nCool that I'm not the only one with this opinion :-)\n\nFor trivial things, clicking stuff together is ok, but then don't expect it to be portable (and if it's just to another Linux install).\n\nFor anything which should also build on a different machine, the developer has actually to think about what he does, not only in the programming part, but also the building part. This shouldn't be hidden from him.\n\nAbout the cmake support in the new IDE: how do you do this ? \nIn the cmake developer's opinion the right way would be to work with project files generated by cmake, not to try to parse cmake files.\nAnd if you need something from cmake (e.g. a new makefile-based project file generator) let me know or even better join on the cmake mailing list :-)\n\nAlex (cmake guy, you know)\n\n"
    author: "Alex"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Very cool, Alex, working on this together would be great. Daniel T. will probably be the one to follow up here.\n\nEffectively, all Qt Creator needs from a project plugin is some basic information on the project:\n\n- all source files that are part of the project\n- all include paths\n- the compiler (so we can ask it for the standard include paths)\n- the build environment (defines for the preprocessor)\n- the command to start the build\n- the targets (so we can run or debug it)\n\neverything else is added sugar. The best would be if we defined a standard xml format for Creator. You let cmake generate it, we read it, and everybody is happy."
    author: "Matthias Ettrich"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "I wonder how hard it would be to make a .pro generator for cmake."
    author: "Boudewijn Rempt"
  - subject: "Re: KDevelop"
    date: 2008-11-03
    body: "This is all stuff cmake can easily provide, no problem :-)\n\nAlex\n"
    author: "alex"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "Regarding project files and their edition with IDEs, I agree. Build system related files at the end of the day are nothing more than scripts written in a domain specific language and it always struck me as odd that so many programmers can't stand editing text build files despite that they spend most their time editing code.\n\nI've been using almost exclusively visual studio at work for the past ten years, yet I hate UI based build systems.\n\nSo far, I think Qt Designer has great potential. It's incredibly fast and have a clean UI, which are the two most appealing things for me in an application."
    author: "Antoine Chavasse"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "I just want to note one thing: the idea of having an IDE manage your buildsystem files is something that I think that comes from the days of automake/autoconf, etc. When I had to learn Qt/KDE development, understanding the buildsystem was even worse than learning C++. :-(\n\nI'm happy that Qt and KDE are now far away from autotools."
    author: "suy"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> an IDE with a radically different approach than KDevelop\n\nI do not see too much differences. Well - ok. The GUI is pretty different. But to be honest, the main parts of an IDE are not related to the GUI. And Qt Software could have influenced KDevelop4's GUI in a large extend.\n\nThe most importand features in my opinion is \"code navigation\". And the best basis for this is a \"model\" (AST) of the application.\nTo bad these two programs do not even share this model (like using clang?).\n\n> an IDE that was almost dead\n\nFor me KDevelop seemed to be pretty much alive the last two years.\nToo bad 2 contributors (Roberto, Harald) where \"lost\". But they are Qt Software guys...\n\nAbout the rough edges: As long as qmake is the basis, I will not likely test it (using cmake at work and home). Qt Creator really needs to be more general. Support for STL containers (as currently the Qt containers) in the debugger whould be nice for example.\nI doubt Qt Creator will ever evolve from an Qt-only tool to a general IDE. Supporting Eclipse was a better choice, but they dropped it.\n\nAnd yes, competition is always a good thing. But I'd say there already is enough competition (netbeans, eclipse, vi, emacs, visual studio, xcode, code blocks, ...).\n\nAnd KDevelop could really need some more manpower. With the help of Qt Creator's team, we would have a pretty cool IDE _today_.\n"
    author: "Birdy"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> I doubt Qt Creator will ever evolve from an Qt-only tool to a general IDE. \n> Supporting Eclipse was a better choice, but they dropped it.\n\nIf I understand the FAQ for Qt Creator right, they don't drop supporting Eclipse:\n\n\"Is Qt Creator a replacement for Eclipse? Why didn't you rather improve Eclipse/CDT?\"\n\n\"... We will certainly continue to develop and improve our Eclipse integration ...\""
    author: "Anony Maus"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "They continue to develop \"Eclipse integration\". But they stopped improving CDT itself...\n"
    author: "Birdy"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> And Qt Software could have influenced KDevelop4's GUI in a large extend.\n\nInfluencing a grass-root KDE project to a large extend? The pure thought makes me smile:) Have you ever tried that? Has it ever been tried? You can always get people to accept adding stuff, true, but to target a different user group, or to focus? Good luck. It takes more discussion time and work to get two people aligned who do not want to be aligned than it takes to replicate the coding work of those two guys.\n\n> As long as qmake is the basis, I will not likely test it\n\nYes, that makes sense. No point in trying out whether you would want to contribute the CMake project manager.\n\n> And KDevelop could really need some more manpower. With the help of Qt Creator's team, we would have a pretty cool IDE _today_.\n\nAha, that was the core of the poodle! KDevelop is so great that we at Qt Software must work with it, but has so little man power that it doesn't get where it wants to get. Don't you see the contradicion? What you are really saying is: \"write the software I want, not the software you want.\" \n\n> Supporting Eclipse was a better choice, but they dropped it.\n\nWho is \"they\"? What did I miss again? \"Better choice\" for whom?\n\nGuys, please relax, we simply announced some more free software. Nobody is going to take anybody's favorite toy away."
    author: "MatthiasEttrich"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "> Influencing a grass-root KDE project to a large extend?\n\nKDevelop4 wanted to simpilfy it's GUI. And I'd guess that the discussion about KDevelop's GUI was about at the same time when you startet Qt Creator.\nThat's why I think it would have been easier for Trolltech to influence KDevelop's GUI.\n\n> Yes, that makes sense. No point in trying out whether you would want to contribute the CMake project manager.\n\nI' currently pretty busy (besides posting here ;-). So I hoped to be able to test it with my code in just a few minutes.\nBefore contributing to Qt Creator, I'll rather start helping KDE again...\n\n> \"write the software I want, not the software you want.\" \n\nExactly. Pretty selfish - I know.\n\n> Who is \"they\"? What did I miss again? \"Better choice\" for whom?\n> Guys, please relax, we simply announced some more free software. Nobody is going to take anybody's favorite toy away.\n\nOk - my big picture is a really powerful cool full featured C++ IDE for Linux. I'd hope for cool code navigation tools, powerful refactoring tools, helpful metric analysers included, a smart debugger, fast, ...\n\nCurrently there is no such IDE for Linux (and hardly any other platform). KDevelop is the tool I think is/will come closest to my dream. Eclipse will maybe.\nQt Creator won't, as I doubt it to be a C++ IDE but will always stay a Qt IDE \"only\".\n\nSome Trolls contributed to KDevelop before. So I hoped to see more of this to see my goal reached sooner than later. That's why I may be a little dissapointed - sorry for that.\n\nBut maybe you proove me wrong and Qt Creator becomes a \"general C++ IDE\". Or maybe KDevelop and Qt Creator work together.\n\nIf I'd work in a Qt only world, Qt Creator would definitely be a cool tool.\n"
    author: "Birdy"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: ">> But I'd say there already is enough competition (netbeans, eclipse, vi, emacs, visual studio, xcode, code blocks, ...).\n\nThe problem is that all those projects suck for developing C++/Qt code.  And even after years and years of development they're not getting any better.  I think there's a lot to be said for a dedicated tool optimized for Qt.  This brings the IDE to an XCode level cleanliness and quality.  Eclipse is a good example of being so generic that it's way too stupidly complicated and massive."
    author: "Leo S"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "> And Qt Software could have influenced KDevelop4's GUI in a large extend.\n\nWell, but there's a natural potential for conflict there.  I'm a bit more cynical now that I'm running a company myself.\n\nIf you release products to support your main product at a company you usually have exactly one goal:  sell more of your main product.\n\nA well designed accessory product for Qt would do just that.  In fact, for the most part, that's true with KDevelop as well -- I don't think it's unreasonable to view KDevelop as a tool to increase development of KDE applications, with some notably different dynamics.\n\nNow, what happens when there's a conflict between something that would be better for KDE and something that would increase Qt adoption?  Well, somebody loses.  And if it's the product designer of a company, they're presumably none-too-happy.\n\nI can seriously see where the Trolls are coming from on this since in the company I founded a few months back we're going to OSS some of our stuff at some point -- but we don't right now precisely because we don't have time to deal with the community.  It'd probably slow our development down by a factor of 2 or 3.  I don't mean that as a bad thing about the OSS community -- but right now we have the luxury of not caring about bugs that don't affect our usage, making decisions with other people's goals in mind and reviewing their thoughts and patches.  (Incidentally, I've got a lot of might-someday-be-OSS-code that I've written personally that's never seen the light of day for the same reason.  Learned my lesson there.)\n\nSo if I were managing this situation at then Trolltech, I would ask:  How much effort would it take to get KDevelop with Qt 4 to the point where we need it, including factoring in a slowdown for community processes?  How far away from an ideal Qt IDE would that be?  How would that compare to the amount of time needed to develop one in-house?\n\nAnd given that it sounds like there are only a few people hacking on KDevelop, I'd very likely have made the same decision as Trolltech.\n\nA lot of times when we're looking at companies working with OSS we forget that they're companies.  They're often filled with people that are passionate about OSS, but their primary goal is naturally commercial success.\n\nIn a community that itself has produced, say, three text editors, it's hard to fault a company for developing something that competes with a KDE app if they see that as being to their advantage.\n"
    author: "Scott Wheeler"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Well, the \"goals\" seem to be exactly the same.\n\nOur plans for KDevelop4 are:\n- Excellent C++ support\n- Lightweight\n- Cross-platform\n- Only shipped with the really important, \"stable\" parts\n- Of course, good Qt support\n\nSo either the Qt Creator developers have not read the kdevelop mailing-list for a long long time, or their IDE is really just another case of NIH syndrom..\n\nI don't know how much of Qt Creator has been written from scratch, but to me this seems like a big waste of manpower, given the fact that if there were only one or two full-time developers supporting KDevelop4, we'd have a really excellent cross-platform IDE right now."
    author: "David Nolden"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "That sounds like a good plan, but after 11 years of programming, 8 of them using Qt, i realize, actually there is no good IDE at all, and it will be not. For someone who need very detail customizing level in the project KDevelop is not suitable, Eclipse too, especially with the CDT. The reason is always very same, I don't have custom make support. Everything I can swallow, but messing with makefiles without feedback is disaster. Alex, no offense, I like idea about cmake, but cmake is not always a solution, integration of the 3rd party library (for example). After so many years of experience I'm using Kate and self made make files, GNU compatible, believe or not, that gives me the best efficiency.\n\nBecause I was a fan (still in the soul) of KDevelop, I suggest to concentrate work on good project support, that's IDE for. So flexible make files, interactive editing, or at least prompt a warning, and drop support of qmake or cmake at all. The only right standard is GNU make, and yes, it is cross-platformed. Support of C++ specific is not so matter, i don't need IDE to be cross reference of my classes, i need one who can handle project, till than kate+term make the deal.\n\n"
    author: "doc_ds"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "Not wanting to go too far off topic - but how messed up does a third party library have to be before using it with CMake is difficult?\n\nAnd when using custom Makefiles, how does Eclipse CTD make that any harder than Vim or Emacs?\nIf you're having that much trouble with custom Makefiles, then it sounds like you're doing something fundamentally wrong.\n"
    author: "mabinogi"
  - subject: "Re: KDevelop"
    date: 2008-11-03
    body: "You didn't get my point. I never said Eclipse makes my work harder, Eclipse is, however, great tools, but not for CDT, not for me anyhow. I no need to use Eclipse if I need to do the work manually, I don't need IDE to be just an editor.\nAnd Eclipse doesn't have possibility to integrate new files in custom make file, doesn't call any script for that. So if you want to have your own hierarchy of the files, targets and objects you cannot use Eclipse, if you know the way than enlighten me please!\n\nI don't do anything fundamentally wrong. It is a nature of diversity.\n\nAnd yes, 3rd party library messes, because they have they own makefiles, so integration of that targets are impossible without sub-projecting, and not to mention some of them cannot be integrated as Eclipse project without serious work. \n\nWhat is point to have CMake which calls \"make -f Makefile\" as a target? I will rather than use GNU make, at least I keep syntax the same. Again, I never said CMake is bad or whatsoever in that direction. "
    author: "doc_ds"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "KDevelop has custom makefile support, always had, and always will have. So you can write the makefiles by yourself, or let them be written by whatever tool you like. If you don't want the goodies KDevelop has to offer, then that's your problem. ;)"
    author: "David Nolden"
  - subject: "Re: KDevelop"
    date: 2008-11-03
    body: "Yes it has custom make file support, but than automatism is gone, KDevelop serves just like an editor, for that I don't need KDevelop because it already uses Kate parts. There where no support for custom made scripts like:\n * Inserting new file\n * Deleting old file\n * Renaming targets\n\n For that I need to edit make files by my self, so KDevelop helps luck in this case, always did. Everything could be done, it is just scripting, user point what scripts want to execute in those cases.\n\n No need to be catchy here, I tried to be constructive, because I made the weakest point of KDevelop, and that is project support. If that targets you, sorry it's your problem, as I mentioned I don't use KDevelop any more ;) \n"
    author: "doc_ds"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "What's the difference? Either way, none of them will ever be as powerful as vim is :)."
    author: "slacker"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "kate now has a vi mode and since kdevevop uses katepart it also gets it ;)"
    author: "hias"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "emacs has a vi mode too."
    author: "debianuser"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "Nice. I lolled."
    author: "illogic-al"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Trolltech wanted a crossplatform IDE. As great as KDevelop is, I can't quite call it crossplatform, because it's such a pain to get running on Windows or Mac. But even if it weren't a pain, KDE is a huge dependency to foist onto non-KDE users."
    author: "David Johnson"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "define \"huge\""
    author: "Aaron Seigo"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "1 Gb of software that needs to be patched, compiled, patched again _before_ kdelibs gets installed. Then about 400 Mb or so for the kdelibs and other stuff itself. Then finally we have kdevelop's dependencies to get through and then _if we're lucky_ KDevelop itself. \nSo \"huge\" here comes in at about one and a half gigs of fat for non-KDE users. And we're not even talking about the \"huge\" time dependency that comes into it. And after all that's done you have an IDE. This is on OS X btw. \nFor 900 Mb on OS X, Apple gives me an IDE, debugger, multiple compilers, profiling software, proper API doc indexing (with more available for download at the mere push of a button). So with KDevelop we're getting about half a gig more for 5 times less (by my count). Now Apple's a company and can provide all that but that's beside the point.  The point is \"KDE is a huge dependency to foist onto non-KDE users.\""
    author: "illogic-al"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "I've got only a 40Gb drive on my laptop, with two OS partitions. Which means room is a bit tight. I know that terabytes are selling for dimes these days, but frugality was still a virtue last I heard. So if kdelibs/kdebase-runtime isn't needed by an app, why make it a requirement? (speaking in general here, not directed at kdevelop, which obviously leverages lots of kdelibs goodness).\n\nI see nothing at all wrong with plain vanilla Qt applications. If greenhouse can provide 90% of the KDevelop's functionality without being tied to KDE, I say more power to it! And it's still a Qt app so it will integrate nicely with KDE.\n\nI've got nothing against KDevelop, and I think it's one of KDE's shining jewels. But it's not going to be for everyone, with the large footprint is one of the reasons."
    author: "David Johnson"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Yep, KDevelop, been using it for many years (since 1.0 actually) and it is the same thing over and over again. Version 1.0, get's 80% usable - then forked between a group that wants a rewrite and a group that wants a port to the latest Qt. Same happened again from version 2 to 3, Harry from TT might still remember this...\n\nNow I would say that KDevelop-3.5.3 *is* actually very usable and way better than switching between terminal and kate/kwrite or running gdb from the command line. QMake project management has it's quirks and you do have to protect your .pro files. Code completion works, Robe (another TT guy) did a tremendeous job stabilizing all that. Debugging with gdb works, heck you could now even get idb to work with it. QT Documentation works,  you can right click on a class name and jump to docs. All of it makes me choose it over Studio 2005 for porting our app from Qt3 to Qt4... \n\nOne problem with KDevelop always was that the commercial distros have ancient version of this program, e.g. RHEL4, RHEL5 come with very old versions, and that KDevelop has gained a undeserved bad reputation through that. \n\nBut since yet another rewrite is in the making I am waiting to see whether TT can deliver. Given Matthias' track record (LyX, KDE, Qt) - how can it not!!!\n\nMuch will likely depend on whether TT will actually \"eat their own dog food\". If TT developers were to use this and had the possibility to contribute to its code base, this could quickly become a nice platform independent IDE. \n\nI hear there was lots of positive feedback at the dev days this week. \n\nRegards\nRoland\n\n"
    author: "Roland"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "The big C++ support/code-completion workover on KDevelop 3.5.3 has been done by me, before we started working on KDevelop4. And I can promise that KDevelop4 already now is far better than all that. From what I see Qt Creator looks like a slicker version of KDevelop3, it seems to have nearly exactly the same feature set. The next generation of KDevelop will be far above that, of course unless nokia pumps tons and tons more of resources into their app. Which IMO is a big duplication and waste. Bot apps have nearly the same goals from what I see."
    author: "David Nolden"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "David, thanks for the correction. I have honestly not followed who does what and lost track of who developes KDevelop. You deserve much credit, the code-completion in KDevelop3 does work great now!!! \n\nWhen will KDevelop4 be in a a comparably usable state as KDevelop3 is? \n\nRoland\n"
    author: "Roland"
  - subject: "Re: KDevelop"
    date: 2008-10-31
    body: "Lets forget the fact that the Trolls understand their Toolkit better than KDE's.\n\nThere is still a world of difference distributing a KDE app and a Qt app. Watching the Amarok developers package Amarok for OS X and Windows makes me cringe. Distributing Last.fm is hard enough, but at least we only depend on Qt.\n\nNot to mention it is not even currently technically feasible to distribute KDE applications on all platforms!"
    author: "Max Howell"
  - subject: "Re: KDevelop"
    date: 2008-11-01
    body: "the solution is not to say \"you can't use these additional (and often better than alternative solutions) libraries\", the solution is to fix packaging of kdelibs on those other platforms.\n\nand that would be a hell of a lot less work than writing an IDE, to give some sense of actual scope here (versus your Godzilla-is-coming presentation of it ;)"
    author: "Aaron Seigo"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "And who would be doing the fixing? The developers on those other platforms? Not likely.\n\nUntil KDE fixes kdelibs so that it's in a state to be packaged properly on those platforms (frameworks anyone?) packaging will suck, uptake will be next to zero, and KDE apps will have a hard time gaining. Which sucks because some of them are actually pretty awesome (when finally running on those other platforms)\n\n"
    author: "illogic-al"
  - subject: "Re: KDevelop"
    date: 2008-11-03
    body: "> frameworks anyone ?\n\nYes, there are people interested in getting frameworks for OSX. E.g. me (the buildsystem maintainer) and Ranger Rick (the OSX porter).\nWe didn't have them with KDE <= 4.1 since we required cmake 2.4.5 there, and creating frameworks is supported since cmake 2.6.0.\nThere are just two problems left:\n-limited time\n-doing it in a way compatible to what we did until now\n\nI think it won't get done for 4.2, but we will have a closer look at it for 4.3.\n\nAlex\n\n"
    author: "alex"
  - subject: "Re: KDevelop"
    date: 2008-11-02
    body: "Sorry I didn't mean to sound so extremist.\n\nHowever, it certainly is currently true that distributing a KDE application on the big three is painful. And that is much less true of distributing a Qt application.\n\nDistribution is a rubbish task. Nobody wants to do it. Because it is embarrassing to get wrong. And it is not programming.\n\nSo we all look to what is easiest as part of our decision in which toolkit to pick.\n\nThis is why nobody chooses to write cross platform apps in GTK, or wxWidgets. We all pick Qt. And, at this point, we don't pick KDElibs."
    author: "Max Howell"
  - subject: "Re: KDevelop"
    date: 2008-11-04
    body: "I must say that I'm a big fan of KDevelop and I have used it since the beta days in '99. I have used it mainly for C++, Perl and Bash stuff with good results.\n\nI really hope that KDevelop will continue to live and I hope that I can get some time to contribute to the system in a not to far future. For me, KDevelop not perfect at all, but it is IMHO the best IDE in the Open Source Community.\n\nA big thanks to the KDevelop team and the work they have done.\n\nAnd of course: A big welcome to QT Creator. You're existence will probably make KDevelop even better while being a great IDE on your own.    :-)\n\nJostein\n "
    author: "Jostein Chr. Andersen"
  - subject: "Re: KDevelop"
    date: 2008-11-20
    body: "Yes, people reinvent too much wheels.\nQDevelop, Monkey Studio, and so on. Too much IDEs, too little features!"
    author: "magisu"
  - subject: "FAQ"
    date: 2008-10-31
    body: "Unfortunately, the most important questions haven't been answered in their FAQ:\n\n- Is Qt Creator a replacement for KDevelop? Why didn't you rather improve KDevPlatform in order to reach your objectives for Qt Creator?\n\n- Will Qt Creator be managed as a true open source project inviting contributions and code exchange with external developers, or will it stay under the tight grip of Qt Software employees?\n\nYeah, the interface seems to be reasonably slick. But the duplication of effort and NIH-ilisms that are happening on the Qt side these days are worrying. They seem to come up with replacements for widely used KDE software all of the time recently, instead of joining those communities and bending the software to their needs.\n\nReally, if we've already got a code-highlighting text editor part, a powerful language framework, a base platform for creating custom IDE user interfaces, and whatnot, why do they need to whip up all of this by themselves? Just so that it's Qt-only and not depending on KDE (which is just as cross-platform as Qt itself is)?\n\nI mean, c'mon Qt people, this isn't a library that needs to be dual-licensed. There's no reason to make a Nokia-only IDE when we could be working together on a single lightweight Qt/C++/cross-platform solution with a shared code base. Guess I just don't get it."
    author: "Jakob Petsovits"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "What I want to say is not that I worry about KDevelop losing mindshare to other IDEs (if those are indeed working better, power to them). I just don't like the current trend that more and more of our software stack moves from community governance to a single controlling entity, replacing existing openly-developed components with a closed in-house development (even if the sources are published as Free Software afterwards).\n\nFor Qt as a library, that model makes a lot of sense and works very well in most cases. For applications further up in the stack, it unnecessarily prevents cooperation between similar projects where they could share libraries or even merge altogether. I hope the Qt Software department pays attention to governance issues and keeps an eye on integration with existing software rather than creating new base technology for the fun of it."
    author: "Jakob Petsovits"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "Qt Software looks into ways how to further open up the develop process to external contributors where it makes sense. I too would like to see this for Qt Creator. There's more room on the in-side, too, we do have open positions on all engineering sites. Want to hack upstream on Qt? Don't whine, come and join us.\n\nHowever, there's no black and white. What you call \"openly-developed components\" very often means \"governed by unwritten rules\", or \"not governed at all\". In some cases when people say this it simply means \"governed by me\". This happens e.g. when a few KHTML hackers argue for basing the entire KDE web experience on their work instead of the work of the greater webkit community.\n\nAll successful open source projects are governed by strong maintainers with a clear vision, group or individuals. You can't just go ahead and submit stuff upstream. Projects where you can do that are in a state of bit rot, or feature rot, or both.\n\nAbout KDevelop: we are the biggest commercial supporter of KDevelop, and we have core KDevelop contributors on our team. There's was room for two editors before, there will be room for two IDEs. Seriously, I do believe we will see code sharing on the refactoring/c++-engine side.\n\n"
    author: "Matthias Ettrich"
  - subject: "good news"
    date: 2008-10-31
    body: "\"Seriously, I do believe we will see code sharing on the refactoring/c++-engine side.\"\n\nThis is good to hear as this is all I pretty much require out of an IDE. Otherwise I'm happy to use Kate. Not knocking the integrated debugging, designer, qmake/cmake manager I'm just not interested in them. I'm happy to open up new applications and I seriously doubt that a cmake manager will ever be as easy as editing CMakeLists.txt (at least for someone who already knows cmake OK, like me), let alone be complete.\n\nThe fact that Java editors like Eclipse have absolutely awesome parsers is actually my favorite \"feature\" of Java. Would be great if the C++ parsers and refactorers approached that level of completeness. Its the main reason I look forward to KDevelop 4, since I understand they've done a lot of work with theirs.\n\n/me submits refactorers to the OED"
    author: "Ian Monroe"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "\"About KDevelop: we are the biggest commercial supporter of KDevelop, and we have core KDevelop contributors on our team.\"\n\nYou don't have active KDevelop contributors in your rows. This isn't a problem to me at all, but it is a fact.\n\n\"There was room for two editors before, there will be room for two IDEs. Seriously, I do believe we will see code sharing on the refactoring/c++-engine side.\"\nFirst useful refactoring will land in KDevelop4 very soon, and the only way to share code would be making Qt Creator a KDevPlatform based application.. which unfortunately is not going to happen(Else Qt Creator would not exist in the first place). This is a lot of very complex work, and I would _love_ to see code-sharing here."
    author: "David Nolden"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "Well - Trolltech clearly was the biggest commercial supporter of KDevelop :-) Don't forget that Trolltech sponsored two KDevelop meetings.\n\nBy the way, I agree with Matthias. I'm positive that at end of the story KDevelop and Qt Creator will share some code in the C++ engine.\n\nciao robe"
    author: "Roberto Raggi"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "Nobody is denying that Trolltech is the biggest commercial supporter of KDevelop, because they are, and we appreciate their support very much."
    author: "Matt Rogers"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "It's not practical for the C++ parser to be uncoupled from the KDevPlatform? I don't quite understand what your saying."
    author: "Ian Monroe"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "The more complicated part is not the parser itself, but what you do with the data after the parser processed it. In KDevelop we put it into a language-independent structure called DUChain, which is a part of KDevPlatform. You cannot share code that works on that structure with code that works on another structure, because there just isn't anything to share in that case."
    author: "David Nolden"
  - subject: "Re: FAQ"
    date: 2008-11-01
    body: "> this it simply means \"governed by me\". \n> This happens e.g. when a few KHTML hackers argue for\n> basing the entire KDE web experience on their work\n\nTo use your own words (cause they do match but I wouldn't have used them by myself cause they are a bit to direct for me);\n\n> Want to hack upstream on KDE [on the webkit-integration]?\n> Don't whine, come and join us.\n\nIt's in playground/libs/webkitkde :)~\n"
    author: "Sebastian Sauer"
  - subject: "Re: FAQ"
    date: 2008-10-31
    body: "Nokia is probably doing this because they want something that looks and works professional. KDevelop hasn't achieved that yet in the many years it's been around. Sadly it's still a pile of crap :( "
    author: "anonymous"
  - subject: "Re: FAQ"
    date: 2008-11-01
    body: "My comment may have been a bit whiny and unproductive, but you're right, it's always possible to take that to the next level. Congrats for achieving that."
    author: "Jakob Petsovits"
  - subject: "Re: FAQ"
    date: 2008-11-02
    body: "lmao. This is why I love the dot. \nHURRAY for unmoderation!"
    author: "illogic-al"
  - subject: "Nice IDE"
    date: 2008-10-31
    body: "Which toolkit was used for creating this IDE? GTK+"
    author: "Gnome"
  - subject: "Re: Nice IDE"
    date: 2008-10-31
    body: "FLTK actually.."
    author: "Leo S"
  - subject: "Re: Nice IDE"
    date: 2008-10-31
    body: "And why not Qt ;) ?"
    author: "Miha"
  - subject: "Re: Nice IDE"
    date: 2008-11-01
    body: "I heard it was Motif. Some people wanted to go with Interviews, but were voted down. :-)"
    author: "David Johnson"
  - subject: "Re: Nice IDE"
    date: 2008-11-01
    body: "I think Qt Creator uses Qt...\n\nAre you being sarcastic or serious?"
    author: "Michael \"Qt Creator == Qt\" Howell"
  - subject: "Re: Nice IDE"
    date: 2008-11-03
    body: "Looks like Flash to me."
    author: "Anon"
  - subject: "Qt on windows."
    date: 2008-10-31
    body: "Qt on windows is actually really easy to do these days. You can use Visual C++ Express Edition which is free, and follow the instructions I wrote here:\n\nhttp://wiki.qtcentre.org/index.php?title=Qt4_with_Visual_Studio\n\nIt's really easy. Only snag is it takes ages to compile."
    author: "Tim"
  - subject: "Re: Qt on windows."
    date: 2008-10-31
    body: "Qt with Mingw is even easier. It's prepackaged so there's no building."
    author: "David Johnson"
  - subject: "Re: Qt on windows."
    date: 2008-11-02
    body: "But VS is the best IDE and has the best debugger."
    author: "Anonymous"
  - subject: "Re: Qt on windows."
    date: 2008-11-02
    body: "But but but...\n\nTo each his own. My IDE is XEmacs."
    author: "David Johnson"
  - subject: "UH HU!"
    date: 2008-10-31
    body: "I've just started a project in Qt (a engine like Rockman - aka Megaman games) and was using kdevelop and code::blocks to develop.\nBye-bye for both, welcome official Qt IDE.\n\nThis is a happy day for me :)"
    author: "Iuri Fiedoruk"
  - subject: "Qt Designer"
    date: 2008-10-31
    body: "Wow, what an nice app!\n\nQt Designer should be absolutely merged into Qt Creator. It has to look like that. "
    author: "Miha"
  - subject: "Why not support Eclipse?"
    date: 2008-10-31
    body: "Honestly, neither KDevelop nor Qt creator cannot copete with the last Eclipse CDT 5 release. Eclipse+CDT might not have been the best choice in the past, but now with version 5.0 it beats every other Open Source IDE I know in terms of C++ support. Trolltech should invest resources into Eclipse integration of Qt, joining their community reusing all the great infrastructure already existing instead of reinventing the wheel another time.\n"
    author: "Hanno"
  - subject: "Re: Why not support Eclipse?"
    date: 2008-11-01
    body: "Now I'll be a troll in the bad meaning of the word..\n\nJava sucks"
    author: "Paulo C"
  - subject: "Re: Why not support Eclipse?"
    date: 2008-11-02
    body: "...Troll, yes, and a lame one. Why should I care that much which language the IDE I use is written in? I care about productivity. With Java 1.6, Java apps are reasonably fast. The memory footprint might not be optimal, but honestly, just buy another GB of RAM, it's dead-cheap these days. Using another editor or IDE  just because it saves you some bucks for hardware is stupid, at least when you make your money in software development. The hardware upgrade pays itself in few days or even hours."
    author: "Hanno"
  - subject: "Re: Why not support Eclipse?"
    date: 2008-11-02
    body: "There is this thing called \"marginal utility\". The effective price of Eclipse is higher because it uses Java which has a higher footprint. For most people in Eclipse's target group, that doesn't make a difference. But it does exist and will cause some people _at_the_margin_ to look for alternatives.\n\nEven with the extreme ease of opening up a case, risking a static mishap, and installing new RAM, there is still a cost to the user. Even if you came over and did it for him, there is still a cost to the user. Not a cost to use, but a cost to him. If his total cost reaches a certain threshold, then he will look for alternatives."
    author: "David Johnson"
  - subject: "Re: Why not support Eclipse?"
    date: 2008-11-04
    body: "> The memory footprint might not be optimal, but honestly, just buy another GB of RAM, it's dead-cheap these days.\n\nOkay, do you also have a price list for additional RAM slots for Dell notebooks?"
    author: "Stefan Majewsky"
  - subject: "Re: Why not support Eclipse?"
    date: 2008-11-03
    body: "There is eclipse-integration for Qt already:\nhttp://trolltech.com/developer/eclipse-integration\nPerforms well with CDT 5 :)"
    author: "Lars"
  - subject: "Where's the source? ;)"
    date: 2008-10-31
    body: "I'm just wondring aloud: where is the source code? :)\nFirst, I prefer to install binaries from Debian/Ubuntu repositories *or* build the binaries myself (not only me and not only Debian/Ubuntu, that's a common practice)\nSecond, most of us are thinking of Trolltech as on 'open source company' as soon as Qt is open.\n\nI hope that the Trolls just forgot to copypaste the link to svn/tgz on the website.\n\nWith best regards,\nSergei Beilin\nSamara, Russia"
    author: "SAABeilin"
  - subject: "Re: Where's the source? ;)"
    date: 2008-10-31
    body: "From the first paragraph of the article:\n\"The initial release is binary only, and under the terms of the Qt preview license, but the final release will be released with source code under a GPL compatible license.\"\n\nSo you can stop wondering."
    author: "Morty"
  - subject: "Nice Trolls"
    date: 2008-10-31
    body: "I've just tried it a bit now and it I already like it! I have not done any Qt/kde development since 2.x and it didn't take long for me to do a little test app thanks to the intellisense. I'm mostly using Visual Studio nowadays together with vim for repetitive tasks. \n\nIn my slot I wrote \"ui.m_lst.\" and it auto corrected it to \"ui.m_lst->\". Sweet!\n\nThere are lots of stuff that needs to be done in order to make it more usable but I do believe you are on the right track!\n\nThanks,\n\n    John"
    author: "John"
  - subject: "Re: Nice Trolls"
    date: 2008-11-01
    body: "KDevelop4 has \"intellisense\" too. And does that replacement that you mentioned too.\n"
    author: "Amilcar Lucas"
  - subject: "Re: Nice Trolls"
    date: 2008-11-01
    body: "Since _today_ right? I just saw the commit message."
    author: "Boudewijn Rempt"
  - subject: "Re: Nice Trolls"
    date: 2008-11-02
    body: "Actually that comment motivated me to add that mini-feature. With the code-representation we have, it were about 30 lines of code. :-)"
    author: "David Nolden"
  - subject: "Re: Nice Trolls"
    date: 2008-11-04
    body: "So, Qt Creator is already having a positive effect on the development of open-source IDEs :)"
    author: "Paul Eggleton"
  - subject: "A dream come true!"
    date: 2008-11-01
    body: "While writing Qt4 based software at work on an XP I used MS VC 6.0. Adequate but buggy at times.   I had tried Eclipse but it never worked well for me as a Qt4 dev tool.  KDevelop was, for me, even less useful than Eclipse.\n\nOn Linux I tried a couple Qt specific tools like QDevelop but wasn't satisfied with their approach, so I settled on using Kate and Kdbg.  Kate has pseudo code competition and no library lookup, but it made coding easy and fast.\n\nI look forward to playing with this new and much needed tool.  I can see now that it will replace MSVC on my VISTA side and become my Qt4 dev tool on my Linux side.\n\n"
    author: "GreyGeek"
  - subject: "tgz?"
    date: 2008-11-01
    body: "I do not fully like this installer thingie. Is there a tgz with binaries, since the code won't available yet, somewhere?"
    author: "Jorg"
  - subject: "Re: tgz?"
    date: 2008-11-01
    body: "It's a self installing binary. By default it installs to your home directory and creates desktop entries."
    author: "Brandybuck"
  - subject: "A small glitch with the screenshots"
    date: 2008-11-01
    body: "The third screenshot brings up the same graphic as the fourth."
    author: "Geoff"
  - subject: "Godsend"
    date: 2008-11-01
    body: "After I migrated from Gnome to KDE4 because of it awesomeness, and started programming in Qt4 and discovered it's awesomeness too, I entered a never ending quest for finding a good Qt IDE\n\nKDevelop seemed to be nice, but I actually hate kde3 interfaces because they are all so much cluttered (no offences people), and tried installing KDevelop4 several times, but never got to work\n\nNewer versions of Eclipse CDT have excelent support for c++, but it's slow, it's java, looks very ugly on my kde4 setup and it's horrible on small screens like my Eee\n\nNever got Eduiuk to work correctely, and never could pronounce it correctely too\n\nGot MonkeyStudio to work, but it has a very confusing interface, and very cluttered too, see, I'm a user interface programmer, so I just hate to use bad implemented interfaces\n\nThe last I tried was QDevelop, it's very simple, it's code-complete is very limited, but it's acceptable, because it's small, fast and runs well on my Eee\n\nSo I always had a horrible experience with Qt IDEs, the only reason I continued developing on it it's because the awesomeness of the toolkit (GraphicsView just rules for Zoomable User Interfaces), and this Greenhouse IDE it's for me, like someone sad, a godsend, it's easy to use, it has a very nice interface, a very nice debugger, a very nice and fast \"fast switch\"* and it has a plus: a beautiful logo! Fantastic! \n\n*i'm being redundant because Netbeans actually have a \"fast switch\" that's slow as hell"
    author: "Paulo Cesar"
  - subject: "Why is closed source software announced on the Dot"
    date: 2008-11-01
    body: "I wonder?\n\nAnd yes I read that once released it will be under GPL.\n\nSo we have closed source, closed development, top down development arrangments?\n\nCount me as not interested.\n\nDerek"
    author: "dkite"
  - subject: "Re: Why is closed source software announced on the Dot"
    date: 2008-11-01
    body: "Well, I see as a good thing, for me it's the first really good development tool for Qt framework, so for me it's great news for open source developers\n\nThis is just my opinion, don't get mad at me if you are a developer of one those other IDEs for Qt, I'm not saying that they are bad, I'm just saying that they didn't work for me, while Qt Creator works very well"
    author: "Paulo Cesar"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-01
    body: "Top down development arrangements? HAHAHA. You have no idea how Qt Software internally works, or any successful software company for that matter. Sad thing, you will probably also never find out.\n"
    author: "Matthias Ettrich"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-01
    body: "Chill, some people are unfortunate just just here to troll and looking for the attention they didn't get as a child.\n\nKeep rocking Mattias,\n\n   John\n"
    author: "John"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-01
    body: "Since you apparently are not aware, the top poster is Derek Kite, who did the commit digest for a large chunk of its existence, and has hence contributed a great deal of his time to keeping the user community up-to-date with what's \nhappening in development.\n\n\n\n"
    author: "SadEagle"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-01
    body: "I respect the work that both Derek and QT have done for open source greatly,\nbut find all these flames a bit worrying.\n\nI'll try to balance my personal view a bit here.\n\nI come to the dot to read about kde development, new features, programs and all the coolness around it. As for the core principles of the dot i can make an educated guess, but i don't really know what they are. So its hard to see whether this article is inside or outside its scope.\n\nAs far as my personal interest goes i can see this new QT IDE fit in nicely. I however can see the merit of a more \"conservative\" standpoint as well. We really don't want to turn the dot into a well formatted advertising page.\n\nIn the end it is probably quite hard to draw the line,\nAnd there are many arguments to defend either position,\nBut i must admit i find it hard to be offended by this particular article.\n"
    author: "Mark Hannessen"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-01
    body: "I'm profoundly uninterested in free as in beer projects.\n\nWhy? Because it is a product. There are many products out there that can be had with far less investment than free software involves. They work, are productive and cheap. \n\nI am even less interested in the business model of Qt and Nokia.\n\nI'm far more interested in the process of creating software. The process of proprietary software leads to monopolies. I remember the harm that came to users from the gradual dissolution of proprietary software choice. It is a natural monopoly business.\n\nThe process of free software has been able to break the strangleholds of monopoly power. And in a short time producing remarkable software.\n\nIt doesn't surprise me that the processes that have made free software so remarkable have been used internally in some companies. I would be surprised if they weren't. Having some nominally free interfaces to a controlled process, as I said above, is profoundly uninteresting.\n\nDerek (who has finally been called a troll. I'll mark it on my calendar)"
    author: "dkite"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-02
    body: "Derek, I never thought of you as a troll... but now it's all so clear to me. ;-) In case people don't know I'm kidding...\n\nSeriously, they say it will be released with a GPL compatible license. I'm sure you remember KDE had serious issues when it was introduced and they were put to bed. It is likely that there are internal licensing issues and code audits that must be reviewed and releases signed before it can be properly licensed. They did say this didn't start out as a product, but just looking into it. It makes sense for them to have more software showing up on all platforms.\n\nAs for me I'm in total agreement with you on the importance of free, as in speech, software. I'm also painfully aware of the paradoxes in free software. KDE is so good because there is a professional organization with a financial incentive to maintain the toolkit. My impression from my interactions leads me to believe that this is not going got change under Nokia. Almost all the same KDE people are there still. I think they deserve the benefit of the doubt until the actual release.\n\nYou're not a troll, but I should buy you a beer so you can relax. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-03
    body: "That's not what he is saying, Eric, he said he did not care if it was GPL or not, as long as something is developed by paid developers he is not interest. He's not a troll, he's an outsider who tells computer scientists like me that I should do something different for a living if I like free software. At the same time he misunderstands the mechanics of the free software community completely. Try to remove everything that was contributed by institutions and companies from the free operating system and see what you are left with. Either I'm completely misunderstanding him, or what he says is very disturbing."
    author: "Matthias Ettrich"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-03
    body: "Matthias, you are misunderstanding him. His criterium is not money, but openess. As things stand right now, for anyone outside Nokia, Qt Creator is an opaque piece of software. Maybe that is a good thing for a quick and decisive start, maybe it is the right thing to do for the interests of Qt generally, maybe things are going to change very soon or most likely a bit of all that together. You will know, because you are an insider. Outsiders, like Derek and me however perceive things very differently : closed development whose fruits currently are not free (as in spirit) software.\n\nFull disclosure : I am a professional programmer who uses Qt with a commercial license besides writing open source also based on Qt. Speaking with my commercial hat on, my team will surely evaluate Qt Creator when it has matured. But it also has to be said that an IDE is not exactly the development tool that I'd have liked Nokia to focus on. With my open source hat on, I think Derek reflects my sentiments exactly : Qt Creator is just not interesting right now...\n\n"
    author: "Bart"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-03
    body: "I meant \"outsider\" as in \"not a C++ programmer, not the target audience of a C++ IDE\". We've made it pretty clear that the code will be available under a GPL compatible license. All his points about this being uninteresting closed source are mood. A valid question would have been: why didn't you release the source code right away? My answer to that: because we didn't want to wait any longer with showing the community (commercial and open source) what we work on.\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-04
    body: "I think you were conveniently dodging the point of openess in this case, it's not just the availability of source code at some point but also the history, decision makings and design thoughts involved in the making of it (not all of which can be garnered from the pure code). At another part of this page you talked about the \"greater communities\" of webkit, which incidentally is also more of an opaque project were the publically available date (source code, some public blogs and mailing lists) don't tell the whole picture. Mozilla also had and still to some degree has this issue. At its core it's a rather philosophical issue of documentation: as soon as you have company involved the people representing it or being paid by it don't get the free bonus of trust like people contributing out of free will get. Such companies usually have to build this trust for every of its products/projects, which they tend to dodge as unnecessary parts of community-making. Then when the flamings start the consequential exchange of \"you have no idea how it works at our company\" versus \"of course not, you don't show us\" appears like on this page."
    author: "Anon"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-04
    body: "Hm, which makes me wonder if you could motivate more developer to work for Qt Software if you were (even) more open about how things works at your place. From outside there's just some typical programmers' blogs as well as a well known head who tends to be bitchy not unlike the equally bitchy creator of Linux. Must be in the genes. =P"
    author: "Anon"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-04
    body: "You miss my point.\n\nI posit that Trolltech, Qt and the profits of the sale to Nokia wouldn't exist if there wasn't a free desktop that used, promoted, generated a market for, and trained developers of Qt. A symbiotic relationship if there ever was one, and one for the history books.\n\nI don't begrudge anyone their paycheque. I enjoy mine. I fully understand the challenges of managing a software business. From that standpoint, I fully understand why you did what you did.\n\nThat is exactly my point. If I have to worry if my investment of time and energy and money in developing using a platform depends on someone's business decisions and success, then the solution is obvious; Microsoft, with their billions in cash. I've learned, developed on and lost all that time and effort on two platforms already.\n\nYou see, KDE and much of free software, especially the desktop, doesn't make sense from a business standpoint. That is why it has broken the monopoly of Microsoft. Anyone who depended on cash flow or investment to challenge Microsoft just plain starved, and disappeared. Free software has different motivations, when pooled together equal more than the sum of it's parts. \n\nAs for the goodwill among KDE developers who work for Nokia, fine. What about the ones who write their paycheques? \n\nThings have changed. Trolltech was sold to someone. It is very very common for large businesses to purchase smaller ones, run them awhile to see what benefit there can be, then drop them, subsume them, cherry pick the value and sell them. Where would that leave KDE? \n\nWhat is the symbiosis, the dependence of Nokia on KDE? The way this was released shows me that the relationship is unclear at least. Is no one insulted that a beta is released without source? Does Nokia not need KDE anymore? Or is it just a low cost apprenticeship system? \n\nI'll ignore the mood comment. Otherwise I may be convinced of ill intent.\n\nDerek"
    author: "dkite"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-04
    body: "You can't just dismiss him because he is 'not a C++ programmer', since I very much agree with him and  mot definetly am part of your target audience. Someone else argued already the releasing source is not the same as being open. Even when that is said, I find your reason about having to wait really strange. In my experience it takes far more time to make a binary release than to pur a tarball out there. So if your consideration really was time-to-audience only, you'd just have opened your source repository the very first month this project started, no?"
    author: "Bart"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-04
    body: "I could ask in what way does releasing a closed source beta contribute to free software.\n\nMy real question is how does it benefit Nokia?\n\nIt's developers who are going to be using it. Don't you think that they may be able and willing to provide bug reports with, god forbid, code?\n\nThe free software community is far more powerful and provides far more opportunity than any one company, institution or individual could create. Would IBM be as strong as they are without it? Google? Novell? Probably not.\n\nThey would be fools to fight against what has allowed them to thrive.\n\nBut fools exist. Nokia released a beta development tool without source code. Said tool designed for a library that is open source.\n\nDon't think too hard. It's not that complicated.\n\nI was hoping my stated disinterest would be seen as it is; disdain.\n\nDerek"
    author: "dkite"
  - subject: "I don't really see whats wrong"
    date: 2008-11-03
    body: "Having the sources open for anyone to contribute to isn't how all open source software works from the beginning. I think thats just fine. I'm thinking of Quassel which didn't go public for a year or two. \n\nAnyways I think your confusing style with philosophy."
    author: "Ian Monroe"
  - subject: "Re: I don't really see whats wrong"
    date: 2008-11-03
    body: "Having source open and ability to contribute doesn't always mean \nthe software is open, either.\n"
    author: "SadEagle"
  - subject: "Re: I don't really see whats wrong"
    date: 2008-11-03
    body: "True but this project is just starting, was my point. Not wanting contributions until there's some direction for the project established by the creators seems fine to me."
    author: "Ian Monroe"
  - subject: "Re: Why is closed source software announced on the Dot"
    date: 2008-11-01
    body: "\"Sad thing, you will probably also never find out.\" Sad thing Nokias shares arent going up. :D\n"
    author: "N"
  - subject: "Re: Why is closed source software announced on the"
    date: 2008-11-01
    body: "Good I am not owning any :-D But seriously, developing software in a company isn\u00b4t all that different from developing in the open source community, at least not in Qt Software. We do a bit more pair programming because we share offices, and we have more channels to actually figure out what the customers really want (and not only the vocal minority on community lists and channels). But in the end, non of us would write a single line of code if we didn\u00b4t believe this was the right thing to do. The job market for computer scientists is way too good to work for a bad company. Did I mention we have open positions? "
    author: "Matthias Ettrich"
  - subject: "Nice"
    date: 2008-11-01
    body: "I already quite like it,\nAre their any changes of this toolkit supporting jambi too?"
    author: "Mark Hannessen"
  - subject: "Re: Nice"
    date: 2008-11-01
    body: "Yes, QtJambi support would great..."
    author: "Matthias Lechner"
  - subject: "Re: Nice"
    date: 2008-11-05
    body: "And Python's PyQt :-)"
    author: "Birger"
  - subject: "Kdevelop VS Qt Creator for Qt beginner."
    date: 2008-11-01
    body: "I'm trying to learn Qt. Which IDE do you think will help me more to get a better understanding of Qt faster. I know, books are great, but I've always found that a good IDE helps me understand things faster."
    author: "Nikos"
  - subject: "Re: Kdevelop VS Qt Creator for Qt beginner."
    date: 2008-11-01
    body: "I'd say for learning it is not that good to use an IDE. In my opinion the best way to learn is from a simple text editor. This way it may be a bit harder to write code but your brain will train better this way. In a sense it is similar to maths in school. Using calculator would be a lot easier but you use paper and pencil, the hard way, and in this way you learn better."
    author: "Gal"
  - subject: "Re: Kdevelop VS Qt Creator for Qt beginner."
    date: 2008-11-01
    body: "Well, I think you are both right and wrong in different aspects. Using a simple editor is of great help while you are learning to think in a certain way. But when you have a certain mindset, abstracting the \"already acquired knowledge\" away helps you focus on a more mature understanding of things."
    author: "Nikos"
  - subject: "Re: Kdevelop VS Qt Creator for Qt beginner."
    date: 2008-11-01
    body: "Well, the problem of learning on IDEs is the autocomplete, it will make you lazy..\n\nBut, I think Qt Creator's integration with QtAssistant would be a great aid for you"
    author: "Paulo C"
  - subject: "License"
    date: 2008-11-01
    body: "I'm reading \"a GPL compatible license\". Could some one specify which one?"
    author: "Emily"
  - subject: "Re: License"
    date: 2008-11-01
    body: "I'm afraid that they haven't been any more specific yet.\n\n"
    author: "Richard Moore"
  - subject: "Re: License"
    date: 2008-11-01
    body: "You should be please it will be a \"GPL compatible one\" ;-)\n"
    author: "superfan!"
  - subject: "Re: License"
    date: 2008-11-01
    body: "Thats pretty specific already. It means GPL or a more permissive license like BSD.\n\nI'm guessing GPL since BSD would be a signal they want to make it a multi-corp project like Eclipse, I don't see that happening."
    author: "Ian Monroe"
  - subject: "Great"
    date: 2008-11-01
    body: "Would be perfect if this could be extended to allow cross platform KDE development. Please make it happen :-)"
    author: "ac"
  - subject: "Not the first attempt"
    date: 2008-11-01
    body: "Funnily this is already the fourth attempt to offer an IDE solution by the Trolls:\n\n1. There was a simple C++ editor inside Designer for Qt 2 - dropped\n2. QSA (QScript for Applications) was another IDE  - dropped because QSA was replaced by QtScript\n3. Qt plug-in for Eclipse - still in development and looking nice\n4. Qt Creator - actually looking and working great :)"
    author: "Bob"
  - subject: "Re: Not the first attempt"
    date: 2008-11-01
    body: "There was an IDE like thing for QSA I think. Can't really call it an IDE. The new QtScript debugger is its replacement, expect to see more of that with Qt 4.5."
    author: "Ian Monroe"
  - subject: "But..."
    date: 2008-11-02
    body: "Qt + moc == no real C++."
    author: "Anonymous"
  - subject: "Re: But..."
    date: 2008-11-02
    body: "What about MOC is not real C++? On my system I have a real C++ compiler. And it manages to compile each and every Qt application. So they must be composed of real C++ source code!\n"
    author: "David Johnson"
  - subject: "Re: But..."
    date: 2008-11-02
    body: "Qt apps are precompiled by moc. (Do you use make? I think it calls moc in this case.)\n\nBtw I don't think moc is a problem, if it makes the language better."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: But..."
    date: 2008-11-02
    body: "> Qt apps are precompiled by moc.\n\nNo, moc generates code based on hints the programmer put into their classes.\n\nIt is always possible to write all that code manually, just not even remotely as convenient."
    author: "Kevin Krammer"
  - subject: "Re: But..."
    date: 2008-11-03
    body: "True. Too many C++ \"purists\" misunderstand what Qt is doing. It adds three macros, not keywords. And two of those macros expand to nothing. MOC sees those macros and generates what is in essence a publish/subscribe backend. All of the magic happens because of a QObject/QMetaObject hierarchy that implements reflection. There's nothing \"illegal\" about it. "
    author: "David Johnson"
  - subject: "Re: But..."
    date: 2008-11-03
    body: "But these macros are inhibiting the use of some C++ features that, in 2008, should be considered as elemntary parts of the language. Neither nested classes nor class templates can be QObjects.\nI hope that at some point the extra features of Qt (I believe that it boils down to the signal/slot mechanism) can be implemented in a more C++-conforming way."
    author: "Elad Lahav"
  - subject: "Re: But..."
    date: 2008-11-04
    body: "The macros are not the problem as one can do without them and manually write the code they expand to or equivalent code.\n\nOr even come up with one's own code generator.\n\nIf one finds the Qt signal/slot system to be limited for ones purpose, one can still choose to use a different model, e.g. boost signals.\n\nI think for templated signal/slot parameters one can use QVariant as the transport type and add the real data type as a QMetaType"
    author: "Kevin Krammer"
  - subject: "Re: But..."
    date: 2008-11-04
    body: "> I hope that at some point the extra features of Qt (I believe that it boils down to the signal/slot mechanism) can be implemented in a more C++-conforming way.\n\n1. It does not boil down to signals/slots. Even without these, QObjects would have the nice introspection features that are for example used in Qt Designer to implement a generic property viewer, or which are absolutely necessary in QtDbus, QtScript, or the QXmlToQObjectConverter (or similar) that is coming soon.\n\n2. About signals/slots implementation, you can actually do this with templates. ClanLib does that. But you have to implement the templates separately for each parameter count (unless you would like to fiddle around with the ... operator in C++), and you cannot get all the goodies that Qt's meta objects offer (to name two, signal transfer across threads or DBus message busses, and automatic argument omission, i.e. connect signal with 3 arguments to slot with 1 argument etc.).\n\nActually, one might be able to get these features without a separate precompiler, but that would require C++ to include Design By Contract at least."
    author: "Stefan Majewsky"
  - subject: "Re: But..."
    date: 2008-11-02
    body: "Bjarne Stroustrup is happy enough to use it, I'll take his vote over yours Anonymous.\n"
    author: "Richard Moore"
  - subject: "Too late"
    date: 2008-11-02
    body: "Anjuta kicks its ass."
    author: "GNOMEr"
  - subject: "Usefull for Qt Beginners"
    date: 2008-11-03
    body: "Personally I think that Qt Creator will be usefull for Qt Beginners, Because when I brought 2 Qt Books in order to start developping and searched for an IDE, I tried Kdevelop. I must admit that it was really a pain, you had to configure a lot of things in order to get it work (Code completion, etc.), and I didn't found an easy tutorial like \"Everything you need to setup in order to start Qt Programming\". So maybe Kdevelop is only missing some good starter guides, but if I would start developping right now I would go with Qt Creator, which does not mean that after being a little more comfortable with Qt I won't change to Kdevelop.\n\n"
    author: "End User"
  - subject: "Why So Long?"
    date: 2008-11-04
    body: "What I'd like to know is, why on Earth did it take so long for the Trolls to figure out that they should have a focused IDE for Qt development, using Qt? We got Qt Designer which was sort of a halfway-house, and then people had to resort to other fuller IDEs for code development such as KDevelop, Eclipse or Visual Studio on Windows."
    author: "Segedunum"
  - subject: "\"Too bad Qt Software didn't"
    date: 2009-06-04
    body: "<i>\"Too bad Qt Software didn't decide to push KDevelop instead of reinventing the wheel. Now we have two \"beta\" IDEs instead of one \"finished\" powerful one :-(\"</i>\r\n\r\nYeah, competition sucks.  Instead of me going to the grocery store and having a choice of so many ketchups, it would be nice if everyone just worked together and contributed to a \"super\" ketchup.  \r\nLook, I know collaboration has its value as well, and I'm not saying your point is completely without merit, but I'm not going to complain when another person contributes their own version of a product, especially if it is an established, reputable software company.   I've used KDevelop for a while, but now I'm using Qt Creator all the time.  Does that mean the latter is inherently better than the former?  No, but I think the fact that myself and others use it means it has some value to some people.  "
    author: "sadastronaut"
---
News emerged recently that Qt Software (formerly Trolltech) were working on their first IDE
for Qt, code named Project Greenhouse. Today saw the release of the
first technical preview under the name <a href="http://trolltech.com/developer/qt-creator">Qt Creator</a>. The  initial
release is binary only, and under the terms of the Qt preview license,
but the final release  will be released with source code under a GPL
compatible license. The initial release is available for Linux, Mac OS
X and MS Windows.  Read on for a users review.





<!--break-->
<a href="http://trolltech.com/images/developerzone/welcome.png/image_view_fullscreen"><img src="http://static.kdenews.org/jr/qt-creator-1.png" style="border: thin grey solid; padding: 1ex; margin: 1ex; float: right" alt="screenshot" width="200" height="140" /></a>

<p>The Greenhouse project began as a research project within Trolltech.
We are told that the design is entirely plugin based, suggesting that
ultimately we will have the ability to add support for new languages,
debuggers etc. The documentation suggests a plugin for the CMake build
system used by KDE is in the works. At the moment things are a little
too bare for the benefits of this approach to be readily apparent, but
it is a sensible approach to take when developing an IDE.</p>

<p>Unusually for the dot, I have decided to look at the Windows install of
Qt Creator. The reason I have  chosen to do this is that I have tried to
get applications working with the open source Qt version for Windows
before with little success, it has always been more trouble than I have
time for. Hopefully  the combined Qt+IDE+Mingw package will make this
a pretty painless experience, which will bode well  for future work on
KDE on the windows platform.</p>

<p>The initial download for Windows is pretty huge, over 200MB, however
since this includes the  compiler and run time environment as well as
Qt and the IDE itself that is something I can live with.  The installer
is a standard windows-style setup.exe and is pretty much idiot proof
(if rather slow).</p>

<a href="http://trolltech.com/images/developerzone/integratedhelp.png/image_view_fullscreen"><img src="http://static.kdenews.org/jr/qt-creator-2.png" style="border: thin grey solid; padding: 1ex; margin: 1ex; float: left" alt="screenshot" width="200" height="129" /></a>
<p>The inital screen you see when running the IDE is very bare in
comparison to other IDEs - a basic  page with a button to get to the
getting started guide. There is a sidebar on the left with some  pretty
self-explanatory buttons and a menu bar but unusually there is no
toolbar at all, let alone  the common sight of an overwhelming
collections of icons.</p>

<p>The getting started guide itself is pretty weak right now, and is
definitely alpha quality. The  switch from a very clean initial view
to the fairly ropey tutorial with a massive index of the  entire Qt
documentation on the left is jarring. Rather than cheating by reading
the docs, let's just  dive right in and write something, how hard can
it be?</p>

<a href="http://trolltech.com/developer/qt-creator/visualizingqstringlist.png/image_view_fullscreen"><img src="http://static.kdenews.org/jr/qt-creator-3.png" style="border: thin grey solid; padding: 1ex; margin: 1ex; float: right" alt="screenshot" width="200" height="150" /></a>
<p>I will begin with the obvious - create a new project. We have a number
of choices of project type, but  let's go with a GUI application. I
note here that the dialog starts as an OK/Cancel style dialog then
seems to change to a wizard, this could do with some work. I will call
the project DotDemo. I have chosen to include the Webkit module as well
as the basic Qt modules, as these will be required for what I want
this project to do. The final screen of the wizard is for project
management, but is totally disabled and the file names are off the
edge of  the dialog - fair enough, this is alpha code.</p>

<p>Now we have got a basic project, let's see if the IDE provided shell will
build on its own. Clicking  the big 'Build&Run' button on the left
brings up the build settings rather than actually building or
running, I discovered that you need to use the 'Play' arrow button
there instead. The result is a small progress bar in the sidebar
saying building. The resulting app is pretty  unimpressive, but the
generated code has built and run successfully - a good start.</p>

<p>Double clicking the mainwindow.ui file in the file view brings up the
familiar sight of Designer  embedded in the IDE. Since my usual demo
is a browser, I have dragged a QWebView into the view and  added a
layout. This works as expected, and a click of the 'Play' button later
I have a working  minimal browser application. Not bad since I haven't
written any C++ yet!</p>

<a href="http://trolltech.com/developer/qt-creator/visualizingqstringlist.png/image_view_fullscreen"><img src="http://static.kdenews.org/jr/qt-creator-4.png" style="border: thin grey solid; padding: 1ex; margin: 1ex; float: left" alt="screenshot" width="200" height="140" /></a>
<p>At the moment, Qt Creator seems to have quite a few rough edges in the
UI department and more  missing features than I can count, but this is
looking like it is something worth watching. This is  the first time
I have got the open source version of Qt on Windows to do anything
useful despite  having previously got working code from Visual C++,
and having lots of experience developing with Qt  on Linux. Qt Creator
certainly looks like it could lower the bar for Qt development on
Windows, and  if the CMake suppport mentioned in the documentation is
added then this could be a useful tool for  KDE developers.</p>





