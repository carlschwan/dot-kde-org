---
title: "Qt Jambi 4.4 Has Been Released"
date:    2008-06-10
authors:
  - "eblomfeldt"
slug:    qt-jambi-44-has-been-released
comments:
  - subject: "Applications"
    date: 2008-06-10
    body: "Jambi is still somewhat new, but are there any slightly popular OSS projects being written with it yet?\n\nIt'd be interesting to see how well a Qt Jambi web browser compared to one of the C++ QtWebKit based browsers from both a development PoV as well as a run-time PoV."
    author: "Kitsune"
  - subject: "Re: Applications"
    date: 2008-06-10
    body: "Seems like QtJambi would really shine when it using other Java libraries. A web browser is actually a simple thing to implement when you have QtWebKit (the QtJambi demo even comes with one). "
    author: "Ian Monroe"
  - subject: "Re: Applications"
    date: 2008-06-11
    body: "Hi,\n\nyou're absolutely right. We (Salus, an OSS diabetes management system) are currently developing our application with QtJambi and it's really neat to use persistence frameworks like hibernate or an object-oriented database like Db4o in a familiar Qt-environment. Imho, developing with QtJambi in Java is much faster than it is in C++...\n\nBest regards,\nMatthias"
    author: "Matthias Lechner"
  - subject: "Java and KDE"
    date: 2008-06-11
    body: "Is it possible to program in Java KDE programms? \nAre there tutorials avaible?\n"
    author: "Dirk"
  - subject: "Re: Java and KDE"
    date: 2008-06-11
    body: "Short: no and no.\n\nLong: The tools used by Jambi to build the glue needed to make Qt usable from Java could be used for the KDE libraries, except that right now no one has stepped up to do the work. The infrastructure is in place. So the job would rather straight foreward I imagine.\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: Java and KDE"
    date: 2008-06-12
    body: "Ok thanks. "
    author: "Dirk"
---
Trolltech today <a href="http://trolltech.com/company/newsroom/announcements/press.2008-06-05.8292370812">announced the launch of Qt Jambi 4.4</a> &#8211; the latest version of its application framework for Java development.
Qt Jambi is based on the recently-launched Qt 4.4, and brings its benefits to Java developers: including the ability to develop web and multimedia enriched applications across desktop operating systems.








<!--break-->
<p>Among the new features in Qt Jambi 4.4 you can find: 
<li>Integration with <a href="http://www.webkit.org">WebKit</a>, the open source browser engine based on KDE's KHTML.</li>
<li>Integration with <a href="http://phonon.kde.org">Phonon</a>, a cross platform multimedia API from KDE which allows you to play and manipulate video and sound in your application.</li>
<li>Support for any compliant JDBC driver as a backend to Qt Jambi's SQL classes.</li>
<li>Widget can be used inside a graphics view, allowing transformations and manipulations to be combined with the regular application logic.</li>
</p>
<p>
For information about more features see the <a href="http://labs.trolltech.com/blogs/2008/06/10/qt-jambi-440-released/">Gunnar's release blog</a>.
</p>

