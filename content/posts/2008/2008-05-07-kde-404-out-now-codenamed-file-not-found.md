---
title: "KDE 4.0.4 Out Now, Codenamed File-Not-Found"
date:    2008-05-07
authors:
  - "skuegler"
slug:    kde-404-out-now-codenamed-file-not-found
comments:
  - subject: "Changelog still incomplete?"
    date: 2008-05-07
    body: "There are no changes mentioned regarding plasma nor kwin.\n\nIs the changelog really already fully updated?"
    author: "Yves"
  - subject: "Re: Changelog still incomplete?"
    date: 2008-05-07
    body: "Plasma did not recieve any attention, everything was focused on 4.1\n\nNot sure about kwin"
    author: "Dan"
  - subject: "Re: Changelog still incomplete?"
    date: 2008-05-07
    body: "KWin received several bug fixes, like for the bug that caused the password box to hide behind the screensaver when you locked the screen.\n\nThe effects seem smoother too, but that could be also due to the fact that I upgraded to Qt 4.4 too."
    author: "Jonathan Thomas"
  - subject: "Re: Changelog still incomplete?"
    date: 2008-05-07
    body: "The changelog is never really complete. The SVN changelog reveals that there are indeed a couple of fixes in both, Plasma and KWin.\n\nhttp://www.kde.org/announcements/changelogs/4_0_4/kdebase.txt"
    author: "sebas"
  - subject: "Qt backwards compatible"
    date: 2008-05-07
    body: "Hey,\n\nI was wondering since Qt is backwards compatible in the 4.* series you can also run KDE 4.0 with Qt 4.4?\n\nThanks,\nLeaves"
    author: "Leaves"
  - subject: "Re: Qt backwards compatible"
    date: 2008-05-07
    body: "Yep you could."
    author: "Ian Monroe"
  - subject: "Re: Qt backwards compatible"
    date: 2008-05-07
    body: "It works quite well actually. I'm happy with the update, the speed improvements are welcome."
    author: "Jonathan Thomas"
  - subject: "Re: Qt backwards compatible"
    date: 2008-05-07
    body: "Yes, openSUSE 11.0 has KDE 4.0.4 with Qt 4.4.  In fact, if you've tried an 11.0 live CD in the last few weeks you've already run this combination.\n"
    author: "Bille"
  - subject: "Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "Most people I know either got back to kde 3.5.9 or jumped to the 4.1 alphas. I'm curious to know how many masochists (brave?) people are still on the 4.0 series.\n\nPS: I do think releasing 4.0 was a good decision but I don't think anyone is still using it now, so maybe kde devs should forget about it and just focus on 4.1 now. I know I wouldn't care less if there is no 4.0.5 in june but a kick ass 4.1 in july :D"
    author: "kde4"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "AFAIK, the latest Kubuntu and the upcoming openSUSE 11.0 are both using the KDE 4.0 branch since 4.1 isn't out yet. This allows them to provide a stable (if somewhat lacking in some features compared with 3.5) and shiny platform for users while giving them a clear upgrade path once 4.1 is released.\n\nUp until now, the benefit has been that having the 4.0 branch in bugfixing mode has meant that whenever a bug is fixed in the branch, the unstable trunk (4.1) would benefit from it too. Otherwise, it would be quite likely that few bugs would have been fixed since January since all the devs would have been focussing on adding features for 4.1.\n\nAlso I think for users who's distros provided STABLE 4.0 packages and UNSTABLE 4.1 packages they would likely choose the stable packages, if not for the whole desktop then for things like the games, konqueror etc. without having to worry about the UNSTABLE label."
    author: "Matt Williams"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Fedora 9, which is due next week, also uses KDE 4.0, for the same reason. It will ship with KDE 4.0.3. KDE 4.0.4 will be available in updates-testing at release time and move to updates after a few days (usually 1 or 2 weeks, depending on testing feedback). The current plan (subject to change) is to push KDE 4.1.0 as an update for Fedora 9 once it's released. KDE 4.1 snapshots will hit Rawhide (our development branch) as soon as Fedora 9 is released and Rawhide starts targeting Fedora 10."
    author: "Kevin Kofler"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Since 4.0.4 is nothing but bug-fixes (no new features) shouldn't it ship with 4.0.4?"
    author: "T. J. Brumfield"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "No, Fedora 9 was already in \"critical bugfixes only\" mode when 4.0.4 was tagged. We need approximately 1 week to fix the last blockers (without breaking anything else) and 1 week to get the images mirrored, which means we can't really update all of KDE two weeks before the release, even for a bugfix release (and the Fedora release engineering team would probably have vetoed it). Moreover, sometimes some tarballs have to be respun by KDE upstream between the tagging / packager prerelease and the actual release, we wouldn't have been able to guarantee that any respun tarballs would have made it if we had rushed 4.0.4 in. (In this case, kdelibs was respun on May 5 because it reported the version number incorrectly.)\n\nPlease trust us to know what we're doing. It's not like our users will have to wait another 6 months to get the new version, it will be in updates-testing on release day and in updates shortly afterwards."
    author: "Kevin Kofler"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Oh and we wouldn't have been able to get the fix for this 4.0.4 regression in either:\nhttp://bugs.kde.org/show_bug.cgi?id=161769"
    author: "Kevin Kofler"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "I'm running 4.0  I like it more than 3.5, but I'm waiting rather impatiently for 4.1.anything binaries for Kubuntu.  I'm getting more and more tempted to switch to OpenSUSE, but I'm really not wanting to reinstall/configure my system, so I'm just hoping things will improve."
    author: "mactalla"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "just do it my friend, you will never regret switching to OpenSuse"
    author: "mimoune djouallah"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "I'm tempred to switch to OpenSUSE, too. And tried the LiveCD, unfortunately there are is a problem with the TouchPad. I filed a bug, and when/if it is fixed, I'll switch :-)"
    author: "ETN"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "The openSUSE Live-CD seems to have some problems with hardware detection which have been allegedly fixed (according to bugzilla). I'll file a couple of bug reports regarding my own hardware (macbook) once I find some time but there's a chance that the bugs are limited to the Live-CD. Nevertheless, such bugs are bad enough as I doubt that many people will install openSUSE if their hardware doesn't work in the live session."
    author: "Erunno"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "Is this with the openSUSE 11.0 beta 2 live CD? What's the bug number?"
    author: "apokryphos"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "Yes, it is. The bug number is 387793"
    author: "ETN"
  - subject: "OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "I have just updated to 4.0.4 (on 10.3) using one-click. I have the same problem that I had with 4.0.3, it starts up OK then crashes back to log-in screen.  Not complaining, just wondered if anyone had any ideas    "
    author: "Gerry"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "I guess without some more information and some own debugging from yourself nobody will be able to help you, best would be to join #opensuse-kde IRC channel."
    author: "binner"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "That's a reasonable answer, was hoping for \"oh yeah, need to tweak that\" but at risk of sounding like a moron, if e.g., konqueror crashes it generates a back trace in which case even I can work out what to do, however this one goes goes silently back to log in without a hint.  No idea where to go next.  I even checked dmesg in hope rather than expectation. \n\n\n \n\n   "
    author: "Gerry"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "Rather look at ~/.xsession-errors for a hint."
    author: "binner"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "Thank you - acquired the file, will seek help"
    author: "Gerry"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "What helped me once in a similar situation with KDE4 was to delete the KDE4 Desktopsettings.\nI logged into my KDE3 adn deleted the folder  /home/username/.kde4\n\nWhen i treid to login into KDE4 again it rebuild the settings and everything worked again. \n\nDont know if it is the same in your case. \nOf cource i neded to redo all personal configuration. \n"
    author: "Kavalor"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "Thank you for suggestion - tried it, didn't make any difference :( \nLogged it on bugs.kde.org"
    author: "Gerry"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-08
    body: "Most likely driver issues with the hardware acceleration used in KDE 4/Qt 4.4... I have the same with my laptop. It's annoying, but probably an X.org issue - good luck debugging that one."
    author: "jos poortvliet"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-07
    body: "I had the same problem after updating a month ago on opensuse. I'm don't know if that bug is still there because I'm running Kubuntu now.   "
    author: "dum"
  - subject: "Re: OpenSUSE and 4.0.4"
    date: 2008-05-08
    body: "I have often had this problem- I do a failsafe login, then start the sessions\nwith \"startkde4\".\n\n\nOpensuse often pushes out inconsistent sets of files. Typically the session\nstops with a symbol absent in an important library, or even a whole library\nmissing...\n\nThe opensuse build process/packaging seems to be a little fragile.\n\nThis is why you don't get the stacktrace, its not a crash as such."
    author: "augustm"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "openSUSE > Kubuntu"
    author: "T. J. Brumfield"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-12
    body: "I regretted just trying it, after reading about their involvement with Novel and MS."
    author: "Lee"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "I made the switch a couple weeks ago. Having the latest KDE 4.1 without building anything is just nice. (And my other computer runs Gentoo so it's not like I'm scared of building). OpenSuse is obviously for corporate environment, it's install process has lot's of control including giving the user the ability to install a pretty messed up system (which I did :P). Kubuntu's set of sane default apps makes more sense for the desktop user.\n\nBut the KDE 4.1 build service is a killer feature for me."
    author: "Ian Monroe"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "> OpenSuse is obviously for corporate environment, it's install process has lot's of control including giving the user the ability to install a pretty messed up system\n\nThis is more the result of being an early adopter of a graphical installation than of some corporate tendency. However, this has all been changed in openSUSE 11.0 too -- installation takes something like 18 minutes, requires about 6-8 clicks, and takes out a lot of questions from the user. "
    author: "apokryphos"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "This makes sense, the openSUSE 10.3 install reminded me of Linux installs circa 2001.\n\nGood to hear things have improved!"
    author: "Ian Monroe"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Yeah, definitely; just take a look at the beta2 screenshots of the installation for an example of the good looks: http://en.opensuse.org/Screenshots/openSUSE_11.0_Beta2"
    author: "apokryphos"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Opensuse 11 Beta 2 took me an amazing 10 minutes to install\n\nIt has a couple of bugs, but hey that is what a beta is for.  It's not a stable release."
    author: "anon"
  - subject: "Messed up system"
    date: 2008-05-08
    body: "I think the ideal installer will ask a user up front if they want all defaults, and then do just that (akin to *buntu installer) or ask if they want to get more specific.\n\nI'm curious how you got a messed up system from the installer.  What options did you select that gave you a messed up box?"
    author: "T. J. Brumfield"
  - subject: "Re: Messed up system"
    date: 2008-05-08
    body: "I selected the KDE desktop. But since I was wanting to install KDE 4.1, I deselected KDE and selected XFCE. \n\nBut xdm took me to FVWM not XFCE. I guess it's an insult to the FVWM folks that I consider their window manager to be so horrible as to define my system \"messed up\". :) Anyways it took a bit of wrestling but now my KDE 4.1 laptop is rockin. "
    author: "Ian Monroe"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "OpenSuse 4.0 series is better because they ported a large portion of KDE 4.1 bugfixes to 4.0."
    author: "Iuri Fiedoruk"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "I'm running 4.0.3 only at the moment and my main desktop (aka only computer) - from the Kubuntu packages. Perfectly stable, a few oddities but habit works around them.\n\nSaying that: the fact that it is so stable for me makes me agree that 4.0.5 might be unneccessary."
    author: "Martin Fitzpatrick"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Scheduling 4.0.5 is generally a good idea. It is very unlikely that there will be no changes."
    author: "Stefan Majewsky"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Meh. I'm easy... if it comes I'll quite happily use it. I was more meaning that (for me at least) it's not essential as the system in it's current state is perfectly stable and usable for everyday.\n\nWhich is a good thing ;)"
    author: "Martin Fitzpatrick"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "When I first installed 4.1 over 4.0 some apps were not working. Later i found out that it was my fault. Should've deleted the .kde4 folder before updating. After I finally fixed that plasma was broken due to the tokamak changes. KDE 4.0 is running smoothly and stable on my system. To me it seems more of a hassle to have 4.1 running atm."
    author: "Jens Uhlenbrock"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Uh, if KDE 4.1 doesn't work if you have settings from 4.0, that's not your fault, but a serious bug in KDE. New apps are supposed to handle (or migrate using kconf_update) old config files, even KDE 3 ones!"
    author: "Kevin Kofler"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "The 4.0.x has been perfectly stable. It's not a scary thing to run at all. It would be scary, however, to run KDE 4.1 alpha. Due to the tagging being in the middle of the Plasma API refactoring Plasma is super buggy and crashy.\n\nKDE 4.0.x, on the other hand, is stable and can be installed without compiling anything, unlike a more stable revision of trunk.\n\nLots of people are running KDE4; probably more than you think. It is very stable, and is the only stable release you can install without compiling crap."
    author: "Jonathan Thomas"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "I do. I started using KDE4 as a full-session from the latest beta (and used some KDE4 apps withe a KDE3 full-session before). Yes, it is sometimes buggy, but mostly sometimes ;)\n\np.s. Help->About says it's 4.0.3 while dpkg -l says it's 4.0.4 ;) Kubuntu Hardy."
    author: "SAABeilin"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "Yes you are right... did the version-number not be increased ?"
    author: "Yves"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-07
    body: "Yeap, I do too, on openSUSE, for my regular desktop. I also keep a svn tree of kde for developing, but normal use is 4.0.3 (now 4.0.4) from rpm's."
    author: "IAnjo"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "This is exactly my sentiment.\n\nOn my turn I keep waiting for KDE 3.5.10 which is still being actively developed and I'll probably try KDE 4.4 rc1 when it's out.\n\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-08
    body: "I use KDE4.0.x constantly and I haven't got much to complain about, apart from a few limitations on plasma (eg, to be able to delete files from the desktop containment, not just hide them) and a few annoying bugs that aren't stoppers I'm quite happy with it.\n\nMy major pain is not having more apps that use phonon (like amarok1, smplayer etc), as I've got two soundcards and phonon makes it so easy to manage. But that ain't a KDE fault either."
    author: "NabLa"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-09
    body: "My last install of kubuntu (7.04, which upgraded to 7.10) with kde3 started getting buggy, so I searched all over trying to look for a stable and fast kde based OS ... then I saw 4.0. \n\nI tried Arch, but they've got something broken with 64bit version and then I tried OpenSUSE, but something about the install taking 6 hours to run (downloading the net version) made me suspicious... then just trying to search for packages took over a minute to open. \n\nSo back to *buntu land because I don't have time to learn a new OS and the others had too steep of a learning curve (and if paying $59 for OpenSUSE, I expect better than 100kb/sec downloads from update servers).\n\nI installed ubuntu 8.04 alternate then used aptitude to install kde4-desktop. I'm *very* happy with what I have. I'm tolerant of plasma being new and temporarily annoying (as well as the \"hard coding\" here and there), but I am looking forward to really learning this environment and growing with it. \n\nFWIW, I learned more about aptitude with this recent exploration into KDE4 ... imho, aptitude is THE way to install packages ... apt-get and/or the update manager would allow an upgrade right now even though kdelibs5 v 4.0.4 is not in the repositories. Aptitude has something like 100 packages marked as being held back and are waiting on that dependency to run the update.\n\nNow to learn more about how the containers work, how plasma works, and sizing... I need to figure out my issue of the plasma container being laptop screen size while the full desktop is the screen size of the attached monitor. (not trying dual head mode). I'll hack/tweak around it, but this seems to be a bad bug in the interim. I would also appreciate a private e-mail with some tutorial on how to create a \"bug entry\", too!\n\nI envision creating my own \"container\" or widget that I can drop other widgets into (but without borders) as a taskbar replacement. Instead I want a task \"blob\" in a corner. I may even work out playing with the plasma container some more. I've got a lot to learn about KDE before I can decide how to go about that...\n\n:)\n\nIn short: I've used KDE3 for over a year and I'm now going to use KDE4 full time. \n\nThanks\nChris"
    author: "chris"
  - subject: "Re: Who does still use the kde 4.0 series?"
    date: 2008-05-09
    body: "I use KDE 4.0 and I like it. :)"
    author: "KDE 4.0 user"
  - subject: "Status of kio slaves"
    date: 2008-05-07
    body: "I realise this is probably the worst place to ask but I don't know where else :-)  Does anyone have a (complete) list of all the slaves which were ported/are useable under 4.x, or is there a way to query my own installation to find which ones I have?"
    author: "Wondering about features"
  - subject: "Re: Status of kio slaves"
    date: 2008-05-08
    body: "run\n\nkcmshell4 ioslaveinfo\n"
    author: "toga98"
  - subject: "Re: Status of kio slaves"
    date: 2008-05-08
    body: "Ah, I didn't know that one :)\nCool!"
    author: "ad"
  - subject: "Re: Status of kio slaves"
    date: 2008-05-09
    body: "AND they fixed the ftp issue! Previously, the ftp kio would \"shotgun\" my webhost using many connections in a series (rather than one long session) which would trip their intruder detection and get my IP denied for 10 minutes. So I was unable to use ftp:// in konqueror or for file editing in quanta, kate, etc. The first thing I did this morning was to try and \"break\" it ... and I couldn't. :D\n\nA side benefit was using sshfs to mount remote directories locally, so finding out about that is a definite \"plus\" to the former ftp \"bug\".\n\n:) Chris"
    author: "chris"
  - subject: "no proxy, no kde4"
    date: 2008-05-07
    body: "still the proxy bug not resolved, "
    author: "mimoune djouallah"
  - subject: "Re: no proxy, no kde4"
    date: 2008-05-07
    body: "The annoying thing is, if you look at the bug on bugs.kde.org (http://bugs.kde.org/show_bug.cgi?id=155707), a patch has been submitted and shown to work."
    author: "Damn that proxy"
  - subject: "Re: no proxy, no kde4"
    date: 2008-05-08
    body: "If you trust it, you can always apply it. ;)\n\nOtherwise, there's probably a good reason it wasn't applied."
    author: "Zech"
  - subject: "Re: no proxy, no kde4"
    date: 2008-05-07
    body: "Code name \"No proxy support found\" :)"
    author: "Dmitriy Kropivnitskiy"
  - subject: "Re: no proxy, no kde4"
    date: 2008-05-07
    body: "I agree, this issue is really a bad one as many campuses and workplaces require the usage of proxies."
    author: "Erunno"
  - subject: "Re: no proxy, no kde4"
    date: 2008-05-07
    body: "Talking about that, can someone please give me a good proxy for use with telnet?"
    author: "Iuri Fiedoruk"
  - subject: "For those who don't get it"
    date: 2008-05-07
    body: "http://thedailywtf.com/Articles/What_Is_Truth_0x3f_.aspx"
    author: "nonymous"
  - subject: "Re: For those who don't get it"
    date: 2008-05-08
    body: "I think it's more a reference to the famous HTTP 404 error."
    author: "Kevin Kofler"
  - subject: "Re: For those who don't get it"
    date: 2008-05-10
    body: "How did I miss that - :-s. Thanks. When the light came on it was blinding..."
    author: "Anon"
  - subject: "ArchLinux packages"
    date: 2008-05-08
    body: "I've published KDE 4.0.4 packages for ArchLinux on AUR (http://aur.archlinux.org) as binaries one with repositories for i686 and x86_64 at http://66.249.9.34/brauliobo/archlinux/kde4\nTesting is welcome."
    author: "Br\u00e1ulio Barros de Oliveira"
  - subject: "Re: ArchLinux packages"
    date: 2008-05-08
    body: "Thanks a lot brother !!\n\nI 'll give your packages a try. Though I currently have SVN packages (3 days old...)."
    author: "Another Archer"
  - subject: "Saving widgets in plasma"
    date: 2008-05-08
    body: "Seeing as there are no updated to plasma, does this mean that 4.0.4, just like 4.0.3 still doesnt save widgets on the desktop between sessions?\nIf this is the case, hasn't anyone considered backporting this fix from svn?\n\nPersonally, this is the simple bug that is keeping me from using the kde 4.0.x releases."
    author: "metellius"
  - subject: "Re: Saving widgets in plasma"
    date: 2008-05-08
    body: "There are updates to Plasma, though I don't know if your bug has been corrected. It could well be that the bug is not in plasma, but somewhere else, so it's worth checking in any event. \n\nIn the changelog, there's a link to all changes in SVN, there are a couple of things changed in plasma, just have a look at the file for kdebase."
    author: "sebas"
  - subject: "Re: Saving widgets in plasma"
    date: 2008-05-08
    body: "Plasma does save widgets in between; the problem is that on your system it's evidently crashing on log out. To find the problem, try running command from a console in a session: kquitapp plasma. You can start Plasma up again quite easily with `plasma` so don't freak out when your desktop goes all white (or whatever). You should then see the crash; and you should also get a crash dialog that will let you get to the backtrace.\n\nMost likely scenarios: you are using composite with the taskbar tooltips which is crashing somewhere in the X11 code; you have stale plasmoids sitting around somewhere and bringing your system down."
    author: "Aaron Seigo"
  - subject: "Re: Saving widgets in plasma"
    date: 2008-05-10
    body: "> Plasma does save widgets in between;\n\nThen this is the actual problem, that Plasma only saves configuration changes between sessions. This sounds like the same flaw that Kicker had, any crashes could mess up your configuration. This is specially a problem on systems where the users stays logged in for long periodes of time, basicly a very common usage pattern on *nix systems. \n\nAnd on modern computers with suspend to ram/disk logouts should not be necessary at all. And since the suspend features generally seem somewhat unrealible over the range of different hardware, this increases the problem by vanishing configurations, making it even worse.\n\nSimply making Plasma/plasmoids save it's configuration when changed, rather than waiting for logout would remove the problems. By nature the configurations will be fairly static, and any changes will only happen at one item at the time making the resource usage small.  "
    author: "Morty"
  - subject: "Re: Saving widgets in plasma"
    date: 2008-05-11
    body: "The correct place to fix this would be the KConfig backend, not plasma. Like a sqlite kconfig backend wouldn't have this issue, it's good at crash-recovery."
    author: "Ian Monroe"
  - subject: "Re: Saving widgets in plasma"
    date: 2008-05-12
    body: "No, it does not seem like the problem is about crash-recovery. A crash on logout should never make configuration changes dissappear, since any changes should have been saved long before you log out. Plasma and plasmoids should behave like regular applications. Take Konqueror, it does not lose changes made to it's settings if it crashes before you exit.\n\nIf you save when configurations are changed instead of waiting for exit, any crashes on exit or before will not loose settings. "
    author: "Morty"
  - subject: "Re: Saving widgets in plasma"
    date: 2008-07-13
    body: "I realize that this comes a couple of months after the original post; however, hopefully someone may find it useful.  Anyway, Plasma would crash every time I logged out, and none of my settings (widgets and widget settings) saved.  I saw the crash handler, and selected debugging.  In the trace, I saw that there was a crash in the Am3rok plasmoid.  So, when I logged back in, I set up my widgets and settings with the exception of Am3rok (i.e., I did not add the Am3rok widget).  When I logged out, plasma did not crash, and all of my settings did in fact save on log out (and restored when logging in).\n\nSo, if anyone else is having trouble with Plasma crashing on logout, try looking at the backtrace since it may perhaps be a specific widget that is causing Plasma to crash.  I hope this helps."
    author: "John"
  - subject: "+1"
    date: 2008-05-08
    body: "Plasma crashes on logout and looses all configurations I made.\nThis is why I came back to good and old KDE3"
    author: "Iuri Fiedoruk"
  - subject: "Re: +1"
    date: 2008-05-08
    body: "\"loses\""
    author: "ad"
  - subject: "Re: +1"
    date: 2008-05-09
    body: "I did now know dot.kde had a automatic spell checking system!!\n\nJust kidding, thanks for the fix ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: +1"
    date: 2008-05-12
    body: "Not only on logout. It crashes when one tries to close those widgets that aren't working for weeks now. All I get are black boxes instead of widgets on the desktop that crash plasma when one closes them. My KDE 4.1 desktop (4.0.71) is simple a mess. The recent alpha is even more fragile than the snapshots before :("
    author: "Bobby"
  - subject: "Love to use it, but can't"
    date: 2008-05-08
    body: "The killer feature for me is suspend, which doesn't work on kubuntu. Can't say why it won't work, just that it won't. I'm stuck with 3.X until this is fixed. I don't want to restart my computer every time I need to move."
    author: "Oscar"
  - subject: "Re: Love to use it, but can't"
    date: 2008-05-09
    body: "This bothers me too, but FYI you can work around it by using KPowersave from KDE 3 when you want to suspend to RAM / Disk."
    author: "Paul Eggleton"
  - subject: "Re: Love to use it, but can't"
    date: 2008-05-10
    body: "Suspend has been fixed in 4.0.4, see the changelog. (Than Ngo fixed it, I committed his patch upstream.)"
    author: "Kevin Kofler"
  - subject: "opensuse, and Kde4.x questions"
    date: 2008-05-08
    body: "Hello fellow kde-fans :-)\n\n1: I have opensuse10.3, and I'm using the kde4:unstable. What version is that? 4.1.x? or 4.0.x?\n\n2: Is it any EASY way possible for me to have 4.1 unstable? (one-click)\n\n3: Why do they use 4.0.x when it's old? The \"stable\" version will be 4.1, and the changes will be very different. Is it only for the apps? I think they should have weekly releases of the 4.1.x tree.."
    author: "Student"
  - subject: "Re: opensuse, and Kde4.x questions"
    date: 2008-05-09
    body: "OPensuse are using 4.0 because 4.1 will not be released for at least a month after Opensuse 11 is released.  So they are working on having 4.0 in 11, then when 4.1 is released people can upgrade to it.\n\nUnstable with Opensuse is I think 4.1."
    author: "anon"
  - subject: "Re: opensuse, and Kde4.x questions"
    date: 2008-05-09
    body: "1. A development snapshot of what will become KDE 4.1\n2. Sure, http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp\n3. It's the latest stable release? And we do have weekly updated packages of trunk (maybe are only more [often] broken than the 4.0.x packages :-)\n"
    author: "binner"
  - subject: "Re: opensuse, and Kde4.x questions"
    date: 2008-05-10
    body: "ok. Cool.\nNo I only need to fix the dependencies error, but I'm happy with updates a few times a week.\n\nThanks for the answer, and thanks to the Opensuse KDE4-people :-)"
    author: "Student"
---
Another month, another update to the KDE 4.0 series. This time, we are <a href="http://www.kde.org/announcements/announce-4.0.4.php">presenting KDE 4.0.4</a>, dubbed <i>File-Not-Found</i> to the audience. KDE 4.0.4 brings improvements to KHTML, Okular and various other components. We recommend that people who are already running KDE 4.0 releases update to 4.0.4. The emphasis of this release lies, as usual in stabilising, bugfixing, performance improvements and updated translations -- no new features. The developers have again squashed quite some bugs which you can find some of in the <a href="http://www.kde.org/announcements/changelogs/changelog4_0_3to4_0_4.php">changelog</a>. With this release, the KDE community continues to support the KDE 4.0 series that has been released for brave users earlier this year. KDE 4.1, to be released this summer (in the northern-hemisphere) will bring new features and applications. KDE 4.1 is based on the recently <a href="http://dot.kde.org/1210088950/">released</a> Qt 4.4 while KDE 4.0.4 is still based on Qt 4.3 as is the case with the whole KDE 4.0 series. So put on your update shoes and <a href="http://kde.org/info/4.0.4.php">install 4.0.4 today</a>.

<!--break-->
