---
title: "KOffice 2.0 Beta 2 Released"
date:    2008-10-23
authors:
  - "Matthias"
slug:    koffice-20-beta-2-released
comments:
  - subject: "KOffice bugs day next sunday"
    date: 2008-10-22
    body: "And also do not forget about the koffice Bugs day organised next sunday. #kde-bugs on freenode for more information."
    author: "Med"
  - subject: "Re: KOffice bugs day next sunday"
    date: 2008-10-23
    body: "Here is the official announcement on the dot: http://dot.kde.org/1224717094/"
    author: "Med"
  - subject: "please contribute high quality templates"
    date: 2008-10-22
    body: "I my opinion, one key thing about using an office suite is the availability of a mature collection of templates. Templates in my language (from the users view) for private and business use.\n It would be too simple to just say, \"use any Ooo-Template you like\". \n What we (well, at least I need) need is a good manual, how to create templates and a central, well structured soure for high quality Templates.\n \n KOffice is special, and so highly likely not so easy for everyone, who is used to other software. Good templates help to understand the priciples and increase produktivity."
    author: "Frank"
  - subject: "Re: please contribute high quality templates"
    date: 2008-10-23
    body: "Sounds like you are interested in a KGHNS Repo :) "
    author: "Roger Pixley"
  - subject: "Re: please contribute high quality templates"
    date: 2008-10-23
    body: "Well, it would be a no brainer innit."
    author: "NabLa"
  - subject: "horay!"
    date: 2008-10-22
    body: "Congrats to everyone involved! :-)"
    author: "Dread Knight"
  - subject: "I can't wait!"
    date: 2008-10-23
    body: "I look forward very much to having a FOSS office suite written in a real programming language with proper platform integration, a truly open production process, and performance that doesn't suck :)"
    author: "ethana2"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "You mean MS Office? :P"
    author: "NabLa"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "MS Office isn't FOSS.\n\n(Sometimes, in an attempt to be glib, we race to quickly to the \"Reply\" button ;)"
    author: "Aaron Seigo"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "\"(Sometimes, in an attempt to be glib, we race to quickly to the \"Reply\" button ;)\"\n\n... and press it one too many times :p"
    author: "Anon"
  - subject: "Re: I can't wait!"
    date: 2008-10-27
    body: "hahaha\n\n\npwned aseigo! ;)"
    author: "mark"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "MS Office isn't FOSS.\n\n(Sometimes, in an attempt to be glib, we race too quickly to the \"Reply\" button ;)"
    author: "Aaron Seigo"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "I think all the office suites use C++ primarily."
    author: "Ian Monroe"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "Abiword and Gnumeric don't (C) ... but then, I'm not really sure if they qualify as a \"suite\" :)"
    author: "Anon"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "Well, actually, Abiword does use c++. "
    author: "Boudewijn Rempt"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "Wow, really? That's actually quite a surprise to me."
    author: "Anon"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "iWorks doesn't use C++."
    author: "Marc Driftmeyer"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "iWorks doesn't use C++."
    author: "Marc Driftmeyer"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "What does the language matter?  Surely its the end result that counts?"
    author: "Stuart Herring"
  - subject: "Re: I can't wait!"
    date: 2008-10-23
    body: "depends if you want to write code for it I guess"
    author: "Borker"
  - subject: "Re: I can't wait!"
    date: 2008-10-24
    body: "The current theory is to throw RAM, GHz, and multiple cores at the problem of performance that sucks.  IMHO, not really the best.\n\nI don't see the issue as being the programing language but rather that Qt/KDE are really a programing language.  I don't like C++, but I have no problem using it.  I am able to use Qt by reading the Fine Manual.  The problem comes with KDE for which there really isn't a Fine Manual.\n\nYes, it would be nice if KDE and Qt were ported to FORTRAN 2008 which is the most modern commercial language.  However, performance might still suck because of the overhead which is inherit in any highlevel language.\n\nSo the issue isn't really about using a \"real programing\" language.  To get higher performance you need to use a lower level language.  C is such a language but it lacks features to the point that the compiler must optimize the code to use the features of modern microprocessors (it was designed for a PDP11 which didn't have many features).\n\nSo with programing languages we are faced with a common problem.  De Facto standardization leads to obsolete modalities continuing even though more modern and much better alternatives exist.  It is the same with MP3 and MPG formats (both of which suck when compared to more modern formats).\n\nSo, I don't really know the answer, but clearly there is an issue in what you have said.  For this reason, I designed PL/Fiv when I was in college many years ago.  It is a meta-assembler that looks like a highlevel language (NOT OO like Java) when written with the appropriate mnemonics and can be run on a VM.  The problem is that writing with such a language is much more work than using a highlevel language.  IBM suggested the method of code reuse to address this issue, but it never became popular.  Some sort of automated assistance appears to be the possible solution to the problem.\n\nSo, we are left with the problem that has not yet been solved.  A lot of code is written in C which is an obsolete and poorly designed language (when evaluated by modern standards) or in C++ which has some of the same issues but is a semi-OOL which runs with high overhead resulting in performance that sucks.  While using a more modern language might help, the real performance problem is that the toolkit is written in the OOL and that is what causes most of the performance issues.  The answer would appear to be to write the toolkit in a meta-assembler.  This would greatly improve performance since GUI programs spend over 50% of the time in the toolkit.\n\nThe OS is also part of the problem.  GUI programs run backwards with the program calling the OS for messages; this leads to blocking of input.  Using a RTOS where the program services interupts would greatly improve things -- the same throughput would look like higher performance.\n\nBut, I would suggest that you prepare for a long wait -- I wouldn't hold my breath waiting for things to improve."
    author: "JRT"
  - subject: "Re: I can't wait!"
    date: 2008-10-24
    body: "You seriously underestimate the performance of C++. C++ is now being used for most modern numerical libraries, and it's achieving better performance than C or the very well-optimized old-fashioned Fortran routines.\n\nDress something up in enough layers, however, and it will be slow. Your perception of C++ as slow presumably has more to do with how it's sometimes used than anything intrinsic to the language.\n"
    author: "T"
  - subject: "C++ better than Fortran in numerics?"
    date: 2008-10-24
    body: "Please, let me know the facts:\n\n\nBlitz++ is good, but no proper C++ and still not faster than Fortran.\nMTL is not really code compliant\nBoost/UBLAS is the currently only alternative. But: They have an overhead of 2 with small objects (it becomes smaller with larger object size) and developers suggest to use ATLAS bindings to apply good old Fortran libs to UBLAS data objects.\n\nThat said - to my knowledge!  "
    author: "Sebastian"
  - subject: "Re: C++ better than Fortran in numerics?"
    date: 2008-10-24
    body: "For Eigen we did intensive benchmarking of these libs and we found that:\nMTL and Boost::uBlas are very very slow\nATLAS is rather slow, but not as slow as the above\nBlitz++ is not at all intended for the same purpose, it handles multi-dimensional arrays (tensors) which most of the other libs don't do. But if you use Blitz++ as a matrix/vector library, it is extremely slow (slower than any other lib we've tried). It is just not designed for this use.\n\nThe benchmark is here:\nhttp://eigen.tuxfamily.org/index.php?title=Benchmark"
    author: "BJ"
  - subject: "Impressive benchmarks"
    date: 2008-10-25
    body: "I am surprised about your ATLAS results.\n\nDid you use the recommended build options and optimizations? What I have seen in the past is that Intel and Atlas fairly compete and that Atlas is even faster in some cases. There are many reasons why we have finally selected the \"slow\" ublas. And even there we have found that for example the supported axpy products are slower than other operations offered by ublas. \n\nDo you also plan to support various types of sparse objects in Eigen2? If so, I could give it a try in the future... "
    author: "Sebastian"
  - subject: "Re: Impressive benchmarks"
    date: 2008-10-25
    body: "As we say in this benchmarks page, we do our best to make the best use of each library, but if you think we got something wrong, please don't hesitate to tell us. Our mailing list information can be seen on our website eigen.tuxfamily.org.\n\nThere is already an experimental Sparse module, supporting various kinds of sparse objects, providing a few algorithms and allowing to use other libs as backends for more algorithms, and fully integrated with the rest of Eigen. It already has encouraging bench results. However it will not be part of the 2.0 release, more likely 2.1."
    author: "BJ"
  - subject: "Re: I can't wait!"
    date: 2008-10-24
    body: "> However, performance\n> might still suck because of the overhead which is inherit in any\n> highlevel language.\n\nThis is a common misconception and I would like to correct it...\n\nJust because a language is high-level doesn't mean that it incurs any overhead.\n\nMost C++ features don't incur any overhead.\n\nA few C++ features incur an overhead (example: virtual functions) but it is minimal for what it gets you, and equivalent C implementations consist in doing the same thing by hand instead of letting the C++ compiler doing it, and so aren't any faster.\n\nWhat _does_ incur a _very large_ overhead is interpreted languages (including \"bytecode compiled\") and virtual machines.\n"
    author: "BJ"
  - subject: "Re: I can't wait!"
    date: 2008-10-24
    body: "If there is no overhead, please prove that there is a C++ library doing faster linear algebra compared with BLAS/LAPACK/ATLAS/MUMPS/UMFPACK... I am very interested! "
    author: "Sebastian"
  - subject: "Re: I can't wait!"
    date: 2008-10-24
    body: "First of all, I didn't say faster, I said as fast. All non-interpreted, non-bytecode, non-VM languages have the exact same 'speed' (so this notion of 'speed' of a language doesn't make any sense). That includes ASM,C,C++,FORTRAN.\n\nI really didn't mean to bring the topic to linear algebra libs, but since you asked for it.... i'm one of the Eigen developers and it's doing far better than ATLAS, and in certain cases it's as fast or faster as Intel's and GOTO's BLAS implementations. See the benchmarks here:\nhttp://eigen.tuxfamily.org/index.php?title=Benchmark\n\nBut really, it's not even a matter of benchmarking, just looking at the C++ spec you can see that it is designed from the ground up to be zero overhead for the most part and minimal overhead for the remainder, where minimal means that you couldn't gain any more speed by using another programming language. Bjarne Stroustrup sticks to the \"you don't pay for what you don't use\" principle."
    author: "BJ"
  - subject: "Re: I can't wait!"
    date: 2008-10-25
    body: "> Just because a language is high-level doesn't mean that it incurs any \n> overhead.\n\nWhat?  All highlevel languages have overhead when compared to assembler or a meta-assembler (e.g. Intel PL/M). \n\nThe overhead in a OOL should be self evident.  Objects must be created, initialized, and destroyed, and all actions preformed on the objects must go through indirection.  Other overhead issues are going to depend on how good the compiler is, but there is no perfect compiler; compiled code will always be slower than human written assembler.  FORTRAN 2008 will probably be faster than C or C++ but this would require a good compiler (the GNU FORTRAN compiler sucks)."
    author: "JRT"
  - subject: "Re: I can't wait!"
    date: 2008-10-25
    body: "> The overhead in a OOL should be self evident.\n> Objects must be created, initialized, and destroyed\n\nThis, in itself, doesn't incur any overhead over assembly language.\n\nIf creation/destruction of your object does a dynamic memory allocation/free, then the equivalent in assembly language also does. No difference here.\n\nLikewise, if the initialization implies setting the values of some fields, then so does the equivalent in assembly.\n\nWhat I mean is that there is no inherent cost to a constructor/destructor. It's just a function and in cases when the function is trivial and you want to avoid even the cost of calling it, you just inline it like with any other function, so that cost too goes away.\n\n> all actions preformed on the objects must go through indirection\n\nat low level (once your c++ program is compiled) an object is just a pointer. Fields of that object correspond to small fixed offsets from this pointer, like \"ptr+4\". That's exactly what you would do if you wrote equivalent ASM code by hand.\n\nIf you use a d-pointer (aka \"pimpl\") in your C++ program, then indeed you add one more level of indirection. However C++ doesn't force you to do that. People (incl. the original Qt devs) discovered this technique long after C++ was designed. It is a means of allowing to isolate implementation from ABI. If you want that, you have to add a level of indirection, no way around that.\n\n> Other overhead issues are going to depend on how good the compiler is,\n> but there is no perfect compiler\n\nSure, the compiler (especially the frontend) becomes increasingly important as you use more and more OO/generic features of C++. However, recent compilers are very clever. \n\n> compiled code will always be slower than human written assembler\n\nA human is able to do a perfect job on a small piece of code, for one platform, sure... not arguing against that. However that's not what I would call \"overhead\" of compiled _languages_, that's just \"imperfection\" of _compilers_, and then even as such this problem has almost disappeared; only hard math code remains often poorly optimized by compilers and then you can help within C++ by using simd intrinsics, peeling loops etc.\n\n> FORTRAN 2008 will probably be faster than C or C++\n\non the basis of what are you saying this....\nFor a long time people didn't believe that c++ could be as fast as Fortran because of its copy semantics, but modern use of c++98 (expression templates) allows to overcome this and c++ compilers have been handling this nicely for a long time now.\n"
    author: "BJ"
  - subject: "Re: I can't wait!"
    date: 2008-10-26
    body: "Basically, what you said is simply not correct.\n\nIAC, please explain why KDE is so slow doing common things like searching for a file.\n\n>> FORTRAN 2008 will probably be faster than C or C++\n \n> on the basis of what are you saying this...\n\nI think that I have already covered this.  But, basically, it is a new language and is designed to take advantage of new hardware."
    author: "JRT"
  - subject: "Re: I can't wait!"
    date: 2008-10-26
    body: "We're wasting our time with such vague arguments.\n\n> IAC, please explain why KDE is so slow doing common things like searching for a file.\n\nThis has absolutely nothing to do with the choice of a programming language, it's a matter of which algorithm is used and how efficient the I/O operations are.\n\n"
    author: "BJ"
  - subject: "..whats up with the bar on the left side?.."
    date: 2008-10-23
    body: "\nusing kword pulled from svn couple of weeks ago ..i noticed a bar on a left side that can be moved around but can not be removed ..this bar looks a lot like the ones you see on photo editing apps ..will there be an option to hide it? or is this option in already? ..there has to be a way to turn off any menu i do not want to see and it found it to be odd that i couldnt turn this one off"
    author: "ink"
  - subject: "Re: ..whats up with the bar on the left side?.."
    date: 2008-10-23
    body: "Another \"I cannot remove my cashew\" discussion? File an enhancement report (after making sure there isn't already one), and when developers have time, they will surely look at your suggestion."
    author: "christoph"
  - subject: "Re: ..whats up with the bar on the left side?.."
    date: 2008-10-25
    body: "...and then dismiss it out of hand, because they fancy themselves superior user-interface designers who are smarter than their users, and believe the phrase \"if it ain't broke don't fix it\" doesn't apply to GUI design!"
    author: "C. M. Franks"
  - subject: "KOffice progress"
    date: 2008-10-23
    body: "For the first time ever KWord and KSpread are handling well, documents I have produced in Open Office (3.0) and vice versa. Small issue with losing cell format data to and fro, but this is trivial \n\nKOffice is looking better and better. :)"
    author: "Gerry"
  - subject: "Error with kubuntu packages"
    date: 2008-10-23
    body: "I followed the instructions but got the following error:\n\ndpkg: error processing /var/cache/apt/archives/kpresenter-kde4_1%3a1.9.98.1-0ubuntu1_i386.deb (--unpack):\n trying to overwrite `/usr/share/pixmaps/kpresenter.xpm', which is also in package kpresenter-data\n\nWhat's the recommended course of action? Can the package be updated to fix this problem?\n"
    author: "T"
  - subject: "Re: Error with kubuntu packages"
    date: 2008-10-23
    body: "Navigating to /var/cache/apt/archives and issuing\n\nsudo dpkg -i --force-overwrite kpresenter-kde4_1%3a1.9.98.1-0ubuntu1_i386.deb\n\nat least got my apt system back. It also overwrote 4 icon files.\n\n"
    author: "T"
  - subject: "ODF compatibility"
    date: 2008-10-23
    body: "I'm also looking forward to an Office suite that is natively integrated to Linux and especially to KDE. Although UI matters, by far the most important feature KOffice 2.0 should have is 100% compatibility with ODF (v.1.2). I use OpenOffice intensily for personal and academic work, and will only consider moving to KOffice when I am asured of 100% ODF compatibility."
    author: "cardlem"
  - subject: "Re: ODF compatibility"
    date: 2008-10-23
    body: "What a great comment ! (irony) \n\nSorry. I could not resist.\n"
    author: "anonimous"
  - subject: "Re: ODF compatibility"
    date: 2008-10-23
    body: "<i>Although UI matters, by far the most important feature KOffice 2.0 should have is 100% compatibility with ODF (v.1.2). I use OpenOffice intensily for personal and academic work, and will only consider moving to KOffice when I am asured of 100% ODF compatibility.</i>\n\nWell since neither OOo nor Koffice implement the spec perfectly you'd better stop using both of them.  Never mind though, I hear Microsoft are interested in ODF these days so perhaps you'll be able to use MS Office instead... :-P"
    author: "Adrian Baugh"
  - subject: "Re: ODF compatibility"
    date: 2008-10-23
    body: "Even if both would implement the spec 100%, documents would not look the same because some parts can be interpreted differently by different implementations."
    author: "ac"
  - subject: "Saving Graphics"
    date: 2008-10-23
    body: "As of beta1, I was unable to save flake objects within a document.  The text was retained, but not the graphics.  One of the developers mentioned that the code for saving was already in place and that it would be implemented in beta2.  Can anyone confirm if that is the case?  My distro hasn't released packages for beta2 yet."
    author: "cirehawk"
  - subject: "Re: Saving Graphics"
    date: 2008-10-24
    body: "Ok, OpenSUSE made beta2 available today and I was able to try it out.  Sadly, it still does not save flake shapes in the document.  It's a little disheartening to see that something so critical is still not working at this point in the beta release cycle.  Without it, it's really not possible for me to give it a thorough testing.  I continue to have high hopes for KOffice, and I hope this feature makes it into the next beta release."
    author: "cirehawk"
  - subject: "Saving Graphics"
    date: 2008-10-23
    body: "As of beta1, I was unable to save flake objects within a document.  The text was retained, but not the graphics.  One of the developers mentioned that the code for saving was already in place and that it would be implemented in beta2.  Can anyone confirm if that is the case?  My distro hasn't released packages for beta2 yet."
    author: "cirehawk"
  - subject: "Startup still is a usability issue with KWord"
    date: 2008-10-24
    body: "When compared with other word processors, the startup dialog of KWord sucks.\n\nI don't find that the demo templates help at all -- I find them confusing.\n\nIMHO, what is needed is to make it easy for the user to create a default template to be named \"Default\" so that the user can click through the start up with one click if \"Default\" is what is wanted.\n\nPerhaps it would be better if using the \"Default\" template were the default.  One issue with this is that the first time KWord was run that the \"Default\" template would need to be chosen (A4 or US letter) based on the locale."
    author: "JRT"
  - subject: "Re: Startup still is a usability issue with KWord"
    date: 2008-10-24
    body: "That's true. I installed OpenOffice3 today. I was impressed about the nice startup dialog. "
    author: "Sebastian"
  - subject: "Re: Startup still is a usability issue with KWord"
    date: 2008-10-24
    body: "Can you explain why the startup dialog is so impressive? Does it allow something like what was mentioned above?"
    author: "ac"
  - subject: "Re: Startup still is a usability issue with KWord"
    date: 2008-10-26
    body: "Experiment for yourself.\n\nOpen KWord and create a new template named Default to be used as your default at start up.\n\nThis is simply not possible with KWord 2.0 Beta using only the GUI."
    author: "JRT"
---
The KOffice Team has announced the release of KOffice version 2.0 Beta 2, the second beta version of the upcoming version 2.0.  The goal for the second beta is to show progress made since beta 1, as well as to gather feedback from both users and developers on the new UI and underlying infrastructure. This will allow the team to release a basically usable 2.0 release, demonstrating our vision for the future of the digital office to a larger audience and attract new contributions both in terms of code and ideas for improvements.  Since the last beta release a significant set of issues and speed-up fixes have been integrated for all applications and this release shows the shift of focus from new features to bug fixes until 2.0 is released.  More information on <a href="http://www.koffice.org/announcements/announce-2.0beta2.php">the full announcement</a> while <a href="http://www.koffice.org/releases/2.0beta2-release.php">the release notes</a> tell you how to get it.




<!--break-->
