---
title: "KDE to Serve 52 Million Brazilian Students"
date:    2008-04-25
authors:
  - "jriddell"
slug:    kde-serve-52-million-brazilian-students
comments:
  - subject: "Very cool!"
    date: 2008-04-25
    body: "Funny actually, now I'm twice happy :)\n\nFirst I read about the whole brazil goes linux thing at work and was happy about it.\n\nNow I'm home and they apparently chose KDE as their desktop!\n\nVery cool, this should really have an impact on the total number of KDE users, and hence, maybe, somewhen in the future, the devs.\n\n(Actually, KDE is great for kids, I always thought most of the artwork would fit a kindergarten perfectly. No offense :) )"
    author: "fhd"
  - subject: "Re: Very cool!"
    date: 2008-04-25
    body: "Brazilian army and the federal court also uses Linux with KDE."
    author: "Einzelk\u00e4mpfer"
  - subject: "Re: Very cool!"
    date: 2008-04-26
    body: "The Brazilian government did the smart thing,\nunlike others... \n\n(see: Greek government)"
    author: "nikos"
  - subject: "Re: Very cool!"
    date: 2008-04-28
    body: "or the german government\n\nthey love to throw money at huge corporations who dont need that money anyway thanks to their OEM monopoly"
    author: "she"
  - subject: "Congrats and KDE-EDU"
    date: 2008-04-25
    body: "Again, this is great news!\n\nI hope the trend continues.  As a parent myself, I'd love to see the kde-edu branch continue to grow as well.  I wonder if we could get away with open source clone/remakes of classic edutainment titles like Number Munchers, Oregon Trail, Where in the World/USA/History is Carmen Sandiego, etc.?"
    author: "T. J. Brumfield"
  - subject: "Re: Congrats and KDE-EDU"
    date: 2008-04-25
    body: "Hopefully the trend to install in Education will itself drive a wider range of titles. That said, those currently available are already pretty impressive - I found out after installing them accidentally (I'd always avoided them before: education=yuck ;) and having a look around.\n\nIt really shows the KDE project off in a good light: free *and high quality.  More the merrier I say.\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: Congrats and KDE-EDU"
    date: 2008-04-26
    body: "And new projects like Step and Avogadro are going to provide educational tools for students all the way through college. These programs totally rock!"
    author: "R. Davis"
  - subject: "Re: Congrats and KDE-EDU"
    date: 2008-04-26
    body: "I'm looking forward to a kde-edu/games front end to Micropolis (open source version of original SimCity).  Any volunteers? ;-)"
    author: "odysseus"
  - subject: "Re: Congrats and KDE-EDU"
    date: 2008-04-27
    body: "Why not lincity-ng, which is (in my opinion) a much better SimCity clone.  It is closer to SimCity 2000."
    author: "T. J. Brumfield"
  - subject: "interesting"
    date: 2008-04-26
    body: "interesting read, however it doesn't account for the large number of students who take their own lap tops too school and will not be using linux.  Talking to a friend in Brazil, I'm left realising that the figure will most likely be no where near what the article claims.  "
    author: "anon"
  - subject: "Re: interesting"
    date: 2008-04-26
    body: "Yes, there are at least 10'000 students so rich to buy a laptaop, in Brazil..."
    author: "nix"
  - subject: "Re: interesting"
    date: 2008-04-27
    body: "Only that? of Doamna! I'm pretty damn sure that there are at least 53,000 ^ x (where x is liner in troll space) students who sleep with their Windows and Air laptops. Nu Kidding."
    author: "A KDE advocate"
  - subject: "Re: interesting"
    date: 2008-04-28
    body: "Windows users can install KDE programs, too. (At least, they can easily/officially do so in three months.) When they are using mostly KDE apps, the step to Linux with its far better integration and shiny Plasma desktop is a small step. *muhahaha*"
    author: "Stefan Majewsky"
  - subject: "also"
    date: 2008-04-26
    body: "Not KDE related, but TSE (superior court of elections, contrary to some countries the election process is run by a independant court) decided they will migrate all voting machines to Linux.\nBrazil is really taking the lead on linux, and I'm very happy my taxes money isn't going to microsoft in cases it is used to run a simple program or a office suite."
    author: "Iuri Fiedoruk"
  - subject: "Re: also"
    date: 2008-04-26
    body: "Yes, it is really marvelous to see all these changes. Change to a opensource choice is more a matter of culture than everything else. The Brazilian Goverment is giving its example in the educational area (there are some areas that still use Windows, giving our maney to M$) and now the students in public schools will see the power of linux."
    author: "SVGCrazy"
  - subject: "School Administration Software"
    date: 2008-04-26
    body: "I wonder if there is something akin to Blackboard that is fully open source, and not built upon ASP.\n\nI'm sure a school could use a more traditional CMS system to provide wiki, and intranet and messaging features within a school.  However I wonder if software exists to allow a school to handle assignments, grades, even content delivery of curriculum."
    author: "T. J. Brumfield"
  - subject: "Re: School Administration Software"
    date: 2008-04-26
    body: "TJ, this came up a few years ago on Slashdot ( http://ask.slashdot.org/article.pl?sid=05/05/18/1429257 ). The suggestions then were .lrn ( http://dotlrn.org/ ) and moodle ( http://moodle.org/ ). The latter of those apparently can convert from Blackboard to it's native formats (and from there onwards if neccessary). Their stats ( http://moodle.org/stats/ ) show some nice growth as well.\n\nI think we'll see open source creeping into a lot of areas like this. Slowly perhaps, but when it comes to renew contracts its hard to compete with free. Hopefully questions of quality support etc. are addressed with the first successful installation\n\nThe first steps are the hardest I think, but once we're in, we're in for good."
    author: "Martin Fitzpatrick"
  - subject: "Chief GNUsaince at my place of work"
    date: 2008-05-01
    body: "Disney has been using GNU/Linux with the KDE desktop since at least 2004.  I remember back when they had their CG animator's lab at Walt Disney World in Orlando, FL (USA), and I saw a bunch of computers through the glass.  They were clearly KDE 3.x desktops, I believe KDE 3.0."
    author: "Terrell Prude' Jr."
  - subject: "Excelente not\u00edcia"
    date: 2008-05-06
    body: "Eu acho \u00f3timo que o governo invista em software livre.\n\nPor\u00e9m ainda, muitas escolas utilizam Windows. \u00c9 s\u00f3 uma quest\u00e3o de tempo.\n\n[]\nJean"
    author: "Prof. Jean"
---
Piacentini blogs from FISL with <a href="http://piacentini.livejournal.com/7871.html">information on Brazil's Ministry of Education ProInfo project</a>.  The project provides computers and internet connectivity to as well as open content to students in public schools.  They are using a Debian based distribution, with KDE 3.5, KDE-Edu, KDE-Games and have deployed it in 29,000 labs with plans for a total of 53,000 labs by the end of next year.


<!--break-->
