---
title: "KDE 4.1.3 Codename \"Change\" Released"
date:    2008-11-06
authors:
  - "skuegler"
slug:    kde-413-codename-change-released
comments:
  - subject: "Congratulations."
    date: 2008-11-05
    body: "I hope I can print odd pages with this released!"
    author: "Dexter"
  - subject: "Re: Congratulations."
    date: 2008-11-05
    body: "check out the release notes; if the bug you filed about your printing problem is listed in them, then you should be fine."
    author: "Borker"
  - subject: "Re: Congratulations."
    date: 2008-11-05
    body: "Not all fixes are in the release log, you need to read the detailed release notes (you can reach them with a link in the release notes)"
    author: "Beat Wolf"
  - subject: "Re: Congratulations."
    date: 2008-11-05
    body: "It seems I will continue using KDE3 'cos to buy a duplex printer is out of question :)"
    author: "Dexter"
  - subject: "Re: Congratulations."
    date: 2008-11-06
    body: "You might want to check this out\nhttp://code.google.com/p/printruler/"
    author: "Beat Wolf"
  - subject: "Spread the news"
    date: 2008-11-05
    body: "http://digg.com/linux_unix/KDE_4_1_3_Released\nhttp://www.reddit.com/r/linux/comments/7bl75/kde_413_released"
    author: "Tsiolkovsky"
  - subject: "Bug in Suse rpm's?"
    date: 2008-11-06
    body: "FIrst of all: Thanks for updating KDE. \n\nI have seen the message and had to install it. Unfortunately, I am without plasma. It hangs (100% cpu and no interaction) and the system tray is totally screwed up (all icons at the same place). Does anyone else experience this problem on openSuse or is it again a config file incompatibility between bug fix releases? Thanks for useful advice.\n\nPS: Nice kodename. (though it is true on my desktop)"
    author: "Sebastian"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-06
    body: "I just updated two opensuse 11.0, all fine for me.."
    author: "Kailed"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-06
    body: "It's all working fine for me on OpenSUSE 11.1 beta 4.  \n\nPerhaps you should ask your question in the OpenSUSE forum."
    author: "R. J."
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-06
    body: "I can confirm that Sebastian. I also did the update on two computers. KDE starts up with all the apps that I had open before and I can hear the startup sound but no Plasma. The funny thing is that Atl+F2 isn't working this time around so that i could staart Plasma manually."
    author: "Bobby"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-06
    body: "Could you file a bug at bugzilla.novell.com?  It may be due to the backports from KDE 4.2 we added and it would be good to fix this before openSUSE 11.1  Please include a backtrace obtained with gdb -p `pidof plasma`."
    author: "Will Stephenson"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-06
    body: "I checked the packages just now and realised that there are some new packages available so i did the update and things are working fine. It looks like we in Germany get the experimental packages first and later the tested ones :)"
    author: "Bobby"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-06
    body: "Same problem for me here on Opensuse 11.0. Plasma hangs and uses 100 % of cpu. If someone has posted a bug, please provide the link. I want help :=)"
    author: "Marco"
  - subject: "Bugreport"
    date: 2008-11-06
    body: "https://bugzilla.novell.com/show_bug.cgi?id=442483\n\nI updated today, since it worked for Bobby. But it didn't help me.\n\nKRunner and other things still work!\n\nNow I work with Kicker :("
    author: "Sebastian"
  - subject: "Workaround"
    date: 2008-11-06
    body: "I just found out that disabling the system tray solves the problem. "
    author: "Sebastian"
  - subject: "Re: Workaround"
    date: 2008-11-07
    body: "That's the only thing that's not working for me right now but it's a bit funny working without the system tray. I just hope that they will fix it soon."
    author: "Bobby"
  - subject: "Re: Workaround"
    date: 2008-11-07
    body: "Still, plasma is quite slow here. It takes 2 seconds before the taskbar understands that some windows disappeared. Can you agree with that?"
    author: "Sebastian"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-07
    body: "It happens to my OpenSUSE 11.0 too. I have updated to the latest packages but the system tray is still not working.\n\nIs there a bug filed to Suse yet? URL?\n\nThanks!"
    author: "Morgg"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-07
    body: "https://bugzilla.novell.com/show_bug.cgi?id=442483"
    author: "Sebastian"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-07
    body: "I read in an openSUSE Forums thread (http://forums.opensuse.org/applications/399058-kde-4-1-3-now-factory-2.html#post1892254) that it was a problem with libqt4 version.\n\nAnd when I downgraded libqt4 to the version in the KDE:Qt repository it was actually fixed.\n\nPreviously, I had a newer version (4.4.3+20081104-1.1probably unstable) from the home:anubisg1 repository (that I use for Pidgin), so check if you have this repository too. It was fixed by setting KDE4 official repositories to a lower value (higher priority).\n\nI hope it helps, it's been tough surviving without system tray :-) "
    author: "Morgg"
  - subject: "Re: Bug in Suse rpm's?"
    date: 2008-11-07
    body: "True. I used the package from the QT44 repository. That one was broken as well. The one in the factory repository works fine. \n\nSeems to me that openSuse must find a management system for quality control of the individual repositories. "
    author: "Sebastian"
  - subject: "Why can't there be bug free software?"
    date: 2008-11-06
    body: "https://bugs.kde.org/show_bug.cgi?id=160892\n\nand then KDE 3.5.10 has still the ancient scrolling bug in the file dialogue. It is these small things that you really hate."
    author: "Andre"
  - subject: "Re: Why can't there be bug free software?"
    date: 2008-11-06
    body: "There is no such thing as bug-free software.\nHowever, KDE is not bug-free because you haven't fixed your pet-bug yet.\n"
    author: "Andre"
  - subject: "The homepage's release date is wrong"
    date: 2008-11-06
    body: "On 3rd October 2008, the KDE community released KDE 4.1.2 codenamed \"Change\", KDE 4.1.3 is the third bugfix, translation and performance update to the KDE 4.1 series, featuring numerous improvements in various parts of KDE's applications and workspace. \n\nThe date is wrong&#65292;it should be \"3rd November 2008\""
    author: "shappen"
  - subject: "Re: The homepage's release date is wrong"
    date: 2008-11-06
    body: "no it is not wrong, re-read it.  it is stating that 4.1.2 was released on the 3rd of october"
    author: "R. J."
  - subject: "Re: The homepage's release date is wrong"
    date: 2008-11-06
    body: "It should be the \"4th November 2008\" and the codename \"Change we can believe in\" ;)"
    author: "Bobby"
  - subject: "Must upgrade"
    date: 2008-11-06
    body: "Fantastic! Looks like there are some Kwin fixes."
    author: "winter"
  - subject: "Panel artifacts still keeps KDE4 away :("
    date: 2008-11-06
    body: "Great news and thanks for the release. I really wish to use KDE4 and I was using it for few days. But the Panel artifacts while using wine and gtk application (http://bugs.kde.org/show_bug.cgi?id=157017) make me go back to KDE3. I know it is an nvidia driver issue but I cant help it. It seems there is no proper workaround for this issue. Even an upgrade to the latest Nvidia driver did not fix that issue. Too Bad :("
    author: "jst4fun"
  - subject: "Linux and graphic card drivers"
    date: 2008-11-06
    body: "This is one of the things that is driving a lot of users crazy in the Linux world. I recently changed my mainboard. I bought a new one with an Intel GMA 3100 because I have heard so much good things about Intel GMA, as a matter of fact I even have an older laptop, a Thinkpad R60e with an Intel GMA and it works like a charm but the newer chips seems to have some driver problems.\n\nI was fascinated at first (Intel GMA 3100) that I just installed openSuse with KDE 4 and everything just worked! Compositing was enabled and I didn't need to install an extra driver in order to get eye-candies or even worse, enter the console and change to the vesa driver just to get access to the desktop after a fresh install like it is with my nVidia card.\n\nThe problems with the Intel GMA 3100 started when I visited Youtube - Flash brought the whole system down within minutes especially on firefox. Within minutes of viewing a Youtube video all I have is a black screen and a dead keyboard. The other problem is the thumbnail preview of videos in the panel, they all appear blue - there is no video preview just a blue window. If I move Kaffeine across the screen while it's playing a video then it also shows a blue background and cause the system to crash at times. This is really frustrating.\nI was just beginning to wonder if it's a hardware problem but everything else works fine so I am really clueless. "
    author: "Bobby"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-06
    body: "mmm..I think you are using Xaa...try enabling EXA in xorg.conf"
    author: "Giovanni"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-06
    body: "I tried EXA after filing a bug report and getting this suggestion but it only made things worst. I even got some funny colours when starting KDE."
    author: "Bobby"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-06
    body: "Driver issues are driving me nuts. I have yet to hear of any driver that did not have a problem with KDE4. NVidia sucks, ATI is flaky, and Intel is hit or miss. I'm really starting to wonder what kind of cards the Plasma developers are using.\n\nA mentioned this gripe to a friend, and he said he didn't have any problems at all with KDE4 on exactly the same laptop as mine. Vainly hoping that he had a magical xorg.conf file, I started using his system. Yet the problems were even worse on his. Anything related to Plasma or the desktop renders slow and flicker, and turning on desktop effects causes weird side effects. I pointed out the problems to him and he said, \"Oh that's normal. You get used to it.\" Am I just being too picky?"
    author: "David Johnson"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-06
    body: "I am not really picky but the fact that I can't watch a 2 minutes long Youtube video without the system going down is really driving me crazy. \nIn my case it's not a KDE4/Plasma problem, Plasma is running quite smooth and stable. it's definitely a driver problem that I am having. \nI have tried it with KDE 3.5.10 (using KWin 3) and with the latest version of Gnome but I got similar results. \n\nI have installed KUbuntu 8.10 thinking that it was an openSuse problem but I encountered the same problem. I even installed openSuse 11.1 beta 4 today and tried watching flash videos but it was the same, only that the system took a little longer to crash.\nI am really frustrated and I really don't have anymore nerves to go back to nVidia.\n\nLike you said, it would be interesting to know which video cards the devs use."
    author: "Bobby"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-06
    body: "I agree. It's still not perfect - no matter what cards and driver you use and I've tried a LOT. I found the best results with ATI cards and the Open Source radeon driver and EXA enabled, AccelDFS enabled and the DisplayPriority xorg option set to \"high\" - no other options. This yields really acceptable speed and decent looks for me. Some things are still wrong though. The minimizing window effect and the \"darken\" effect are not really fluid, but OK. Especially annoying is that you sometimes see for a short second or so a checkerboard or a black box or just garbage when a new window, menu etc is about to appear - until the window is drawn. \n\nAnnonying yes - but I understand the developers as well: Until there is actually software that uses this stuff in a major way - driver developers (especially closed-source-drivers) won't see any incentive to put things right. And if some things look wrong, what are the KDE devs supposed to do? Some bugs in a driver might even be impossible to work around, others would require quite some time to work around (probably several workarounds for specific driver bugs would be required). I'd rather have the KDE devs work on improving KDE. Building new features and so on instead. We can only be patient until driver developers have mercy with us end-users and fix those problems. In the meantime: Either get used to it or - *drumroll* - switch those effects off. They weren't there in KDE3 anyway.\n\n"
    author: "Michael"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-07
    body: "I'm on openSUSE 11.0 and have GForceFX 5200 128 GB under the hood. Using the proprietary drivers kwin effects are smooooth without any artefacts or repainting problem and video also works for me without any problem (dont know if it's hw accelerated or not). But Plasma does have some repainting problems when the system is at full load (which it is on many occasions given its more than 4 year old) and it also sometimes gives a little glimpse of artefacts.\nBtw I keep my system updated with latest patches/drivers.\nAnd last (but not least) using KDE4 with desktop effects seems faster than without."
    author: "Yogesh M"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-07
    body: "I've got a similar set up, similarly patched, and get similar repainting problems but only when I'm using OOo. "
    author: "Gerry"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-07
    body: "You have a graphics card with 128GB of memory?? I'd pretty well expect that to be pretty smooth! :-D"
    author: "Adrian Baugh"
  - subject: "Re: Linux and graphic card drivers"
    date: 2008-11-08
    body: "...its internal release, I got it straight from NVIDIA ;)\n\nBTW I've now upgraded to 4.1.3 and it also shows tips in the panel and taskbar shows tips with window images, and taskbar autohiding/overlapped by windows is working good too... Seems KDE 4.1.3 (instead of 4.2), with plasma much similar to 4.2, on openSUSE 11.1 will not be bad either!"
    author: "Yogesh M"
  - subject: "Yes we can!"
    date: 2008-11-06
    body: "Making the greatest desktop ever conceived: Yes we can!"
    author: "Andre"
  - subject: "Re: Yes we can!"
    date: 2008-11-06
    body: "Can? Did. Yes you [KDE devs] did - already for years!"
    author: "Arnomane"
  - subject: "Name"
    date: 2008-11-06
    body: "why did you not name it BaracK?"
    author: "Obvious"
  - subject: "Re: Name"
    date: 2008-11-06
    body: "Because the K isn't at the front."
    author: "Bobby"
  - subject: "Re: Name"
    date: 2008-11-06
    body: "Wrong. Because it doesn't start with C."
    author: "Hans"
  - subject: "Re: Name"
    date: 2008-11-07
    body: "NO! Name should be Khanges! BTW isn't ''Changes'' motto of Obama's campaign? "
    author: "Miha"
  - subject: "Another KHTML release :)"
    date: 2008-11-06
    body: "KHTML often seem to make the bulk of the changelog. I wonder why that is, is it because the KHTML devs backport nearly all their work to the stable branch, or because they are better at remembering to update the XML changelog when they do ?\n\nI also wonder what amount of interesting changes are not reported in the user-friendly changelog, compared to the full svn changelog. Would be great if everything was !\n\nAnyhow, thanks for another release :) And special thanks to the KHTML devs, I'm still running a KDE3 desktop, but I run konqueror in its KDE4 version."
    author: "moltonel"
  - subject: "KDE4.1 rocks!"
    date: 2008-11-06
    body: "Thank you KDE team for another maintenance release!\n\nAnd please ignore the trolls bashing KDE4 with their comments on the last few dot stories.\n\nThey want to justify the fact that they can not get used to something new by claiming it does not have the same features than the product they got used to over years.\n\nI still did not see a feature request on bugs.kde.org so far, so constructive criticism is that not.\n\nWe love KDE4, continue like this. 4.2 will be mind-blowing !\n\n"
    author: "Yves"
  - subject: "Re: KDE4.1 rocks!"
    date: 2008-11-06
    body: "\"They want to justify the fact that they can not get used to something new by claiming it does not have the same features than the product they got used to over years.\"\n\nOf course users want the features they are used to! What is so horrible about that?\n\nLet's imagine you've been driving automobiles for twenty years, and then you buy a new KAR4 automobile. There are lots of little changes that you think are great, some that are mildly annoying. But the biggest change is that there's no longer a steering column! No stearing wheel, just a trackball thingly on the dashboard. The turn signals are  buttons on the armrest, and hard as you try you just can't seem to find the controls for the windshield wipers. You hope they'll automatically turn on if there's ever rain, but you're not quite sure about that.\n\nSo you complain a bit to the KAR4 auto dealer. \"Hey, is there an option for a steering wheel?\" To your surprise the salesman tells you to STFU! If you want a steering wheel, then you need to go to the used car lot across the street and get a KAR3 instead. He sneers at you and makes a snide comment that you might even be happer with a rusty KAR2. Your feature request is treated as a personal insult.\n"
    author: "David Johnson"
  - subject: "Re: KDE4.1 rocks!"
    date: 2008-11-06
    body: "p.s. I'm not bitching, just trying to point out why some people may be less than enthusiastic."
    author: "David Johnson"
  - subject: "Re: KDE4.1 rocks!"
    date: 2008-11-06
    body: "Unfortunately the salesman in this case is a whole team giving you the KAR4 for free, and definitively they aren't insulting you in any single way but explaining why you don't need the steering wheel.\n\nIf we don't need change, why do we need development? Please not everyone want to use terminals nowadays..."
    author: "xbullethammer"
  - subject: "Re: KDE4.1 rocks!"
    date: 2008-11-07
    body: "\"Please not everyone want to use terminals nowadays...\"\n\nHow ironic. KDE4 is forcing me to the terminal more and more. Not a half hour ago I needed to zip up a directory. Is there any way to do that using a GUI in KDE4? No! No action menu lets me do that in Dolphin or Konqueror. You could zip up a folder with two mouseclicks in KDE3, but I guess that was too old fashioned. So I opened up Ark, navigated to where I wanted the archive, selected add folder, navigated to where the folder was, and... got an error that adding folders was not implemented.\n\nSo I dropped down to the command line and did it by hand.\n"
    author: "David Johnson"
  - subject: "Re: KDE4.1 rocks!"
    date: 2008-11-07
    body: "Totally agreed.\n\nKDE 4 is getting cooler and, what's really the important matter, more efficient, robust and usable in each release, but that doesn't mean users can not \"complain\" in a well mannered way. Devs need to know that there are still many features which need to be implemented. I'm sure they are aware of most of them, but since in FOSS world there's no that stressful corporate competitiveness, fortunately, the pain-in-the-ass user complains are a good substitute, something needed for avoiding FOSS slow down its progress more than necessary.\n\nSaid that, there's also always a good moment to congratulate and thank KDE devs for their excellent work, dedication, and their patience with so many not very well mannered users. Thank you and keep improving and polishing this awesome KDE 4 of which I think you must feel very proud, :)."
    author: "I\u00f1aki"
  - subject: "Re: KDE4.1 (is a) rock.  Toss it in the lake."
    date: 2008-12-15
    body: "I Agree with you David Johnson, 100%.  People keep defending the KDE4 developers for removing any semblance of the features we have come to depend on having in a useful, quickly accessible place.\n\nBut my question is:  who needs this?  I don't want any \"Wow\" factor, I just want a stable computing environment that doesn't consume much processor time.  If they really wanted to make KDE4 something to speak of, and the way of the future, why not make it as customizable as the rest of Linux?  Let us use the tools we like!\n\nI've gone back to KDE3 because I like my steering wheel, and until I see a VAST improvement in KDE4 I won't be using it.  People can say whatever they want to defend their KDE4, but the fact is that I loathe it and if things keep going the way they are currently, I'll be switching to Gnome and learning their tool set because this desktop just doesn't cut it.  I have no incentive to use it, what-so-ever.\n\n"
    author: "Casper"
  - subject: "Debian PowerPC"
    date: 2008-11-06
    body: "I upgraded the KDE4 games and they run fine, I'm amazed that breakout worked smooth on 300MHz. The blue water of kbattleship has turned brown, I assume it's an endian bug in caching also seen in kpat and klines.\n\nkdebase kdebase-workspace libplasma2 were broken, some packages were not (yet?) installable. I would love to install a full kde4 desktop, yes PowerPC Debian folks your work is really used and appreciated.\n\nI'm getting used to schroot and sux but would love to see an installation script for Lenny which would set up chroot and install kde4 and koffice from experimental and create an experimental menu entry with all the kde4 apps in kde 3.5 as that would really keep Debian Stable stable and continues up-to-date.\n\nIn the mean time running konsole and typing\nsux - kde4\nschroot -p\nksudoku\nwill have to do to play a kde4 game in Debian Lenny with KDE 3.5. But setting up a second Debian install on your /home harddisk partition requires some reading of schroot in Debian documentation."
    author: "Dennis"
  - subject: "Still no hotkeys ?"
    date: 2008-11-06
    body: "I don't see this bug in the changelog :\n\nhttps://bugs.launchpad.net/kdebase/+bug/253337\n\nHope the fix will be in 4.1.3 anyway because I see this as a major issue for keyboard usability."
    author: "Fabien Meghazi"
  - subject: "Re: Still no hotkeys ?"
    date: 2008-11-06
    body: "In Kubuntu 8.04, you can launch the khotkeys of KDE3 (e.g., put it in the KDE4 Autostart). If you already configured khotkeys for KDE3, you would need to change the configuration file ~/.kde/share/config/khotkeysrc so that the keys may be associated with KDE4 applications instead of KDE3.\n\nI am wondering what would happen with Kubuntu 8.10 where there is no more ~/.kde4. Anyone knows? I will not update to 8.10 until I am sure there is a way to have khotkeys running! I would rather wait for Kubuntu 9.04.\nRelated question: if Kubuntu 8.10 has no ~/.kde4, how does it handle the configurations for the KDE3 applications (that are still available, right?)? And should the ~/.kde4 from 8.04 be just moved to ~/.kde after upgrade?"
    author: "Hobbes"
  - subject: "Re: Still no hotkeys ?"
    date: 2008-11-06
    body: "First of all, KDE devs will not be fixing bugs from launchpad generally. KDE bugs belong in bugs.kde.org. They should be reported there by downstream devs if it turns out a bug orinates in KDE itself and is not caused by distribution specific issues like backports or the like.\n\nHowever, in the bottom of your launchpad bug, I see a mention of this probably having been fixed at 22/9/2008. That would imply, I would guess, that it would have made the cut for 4.1.3. "
    author: "Andr\u00e9"
  - subject: "KDE4 has some showstoppers for me"
    date: 2008-11-06
    body: "It is good to see KDE4 moving along. Unfortunately, it has a couple of showstoppers keeping me at KDE3:\n\n* The task bar doesn't seem to be able to group similar tasks. This means it fills up almost immediately and is almost unusable from then on.\n\n* The storage media mounter no longer lets me set mount points for my USB storage devices. For various reasons, I need predictable mount points (my podcatcher syncs to /media/mp3, for instance).\n\nThat's all, but it's more than enough, unfortunately. I hope these small defects will be fixed in time for 4.2."
    author: "KDE fan"
  - subject: "Re: KDE4 has some showstoppers for me"
    date: 2008-11-06
    body: "At least for the taskbar i know it's fixed in kde 4.2, for the other one i have no idea"
    author: "Beat Wolf"
  - subject: "Re: KDE4 has some showstoppers for me"
    date: 2008-11-08
    body: "> he storage media mounter no longer lets me set mount points for my USB storage devices\n\nMaybe this could help :-?\n\nhttp://www.kde-apps.org/content/show.php/MountManager?content=76502"
    author: "Cyril"
---
The <a href="http://www.kde.org/">KDE Community</a> today announced the immediate availability of <a href="http://www.kde.org/announcements/announce-4.1.3.php"><em>"Change"</em> (also known as KDE 4.1.3)</a>, another
bugfix and maintenance update for the latest generation of the most advanced and powerful
free desktop. <em>Change</em> is a monthly update to <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>.  The <a href="http://www.kde.org/info/4.1.3.php">info page</a> points to the sources and the distros quick with their packaging: <a href="http://pkg-kde.alioth.debian.org/experimental.html">Debian</a>, <a href="http://kubuntu.org/news/kde-4.1.3">Kubuntu</a> and <a href="http://en.opensuse.org/KDE/KDE4">openSUSE</a>.






<!--break-->
<p>As a service release, the <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php">changelog</a>
contains a list of bugfixes and improvements</a>. Note that the changelog is
usually incomplete, for a complete list of changes that went into KDE 4.1.3, you
can browse the Subversion log. The most significant changes are:</p>

<ul>
    <li>Two crashes fixed in the Dolphin filemanager.</li>
	<li>A large number of bugfixes and optimizations in the KHTML HTML rendering component.</li>
	<li>Several bugfixes in the <a href="http://kopete.kde.org/">Kopete</a> multi-protocol
        Instant Messenger.</li>
</ul>

<p>KDE 4.1.3 also ships a more complete set of translations.</p>




