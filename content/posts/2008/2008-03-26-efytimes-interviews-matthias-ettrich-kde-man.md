---
title: "EFYTimes Interviews Matthias Ettrich: The KDE-Man"
date:    2008-03-26
authors:
  - "jriddell"
slug:    efytimes-interviews-matthias-ettrich-kde-man
comments:
  - subject: "interesting"
    date: 2008-03-26
    body: "An interesting interview, even if I already knew most of what he was talking about.\n\nOf course now I really want to know what the \"not Qt 4.5\" project is. O.o"
    author: "Ian Monroe"
  - subject: "Re: interesting"
    date: 2008-03-26
    body: "#2"
    author: "TeeZee"
  - subject: "Re: interesting"
    date: 2008-03-27
    body: "I really hope they are working in something like OS X's core animation.\nImagine if you could just write a small script in KPresenter to animate your slides, or similar use in a plasmoid."
    author: "Leiteiro"
  - subject: "Re: interesting"
    date: 2008-03-29
    body: "I think, (no really I hope thay do, otherwise I'm doing it myself! :-) they might be using Qt Jambi as basis for a recently introduced mobile framework .....\nWhat do you think?? :D\nUsing Qt Jambi for it could allow a fast framework implementation on top of very different hardware, and again shorten the time-to-market for mobile devices.\n"
    author: "an hope"
  - subject: "Memory usage.."
    date: 2008-03-26
    body: "Its funny how one incorrect and quickly redacted measurement can convince so many people.  KDE 4 certainly does not take less memory than KDE 3, and I don't think that myth is doing anyone any favours.\n\nAhwell.  Interesting interview."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "That's probably true,\n\nBut i must say that it does load apps a lot faster then kde3.\nI myself don't really care that much about usage, what i care about is performance. thought usage optimization is always welcome."
    author: "Mark Hannessen"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "Yeah, I still feel bad for spreading that one.  Must do better.  Anyway, it's \"retracted\", not \"redacted\"."
    author: "Bille"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Oops.  You're right.  Major brain fart on that one."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "But in Qt4 there is implicit sharing of most basic classes which makes it use less memory, so logically, it uses less memory. It is of course also dependant on how you use Qt4, but even programmers who don't care about how many copies they make of some 'QString' or whatever, will use less memory because of this sharing. \n\nSo if KDE4 uses more memory, then I think that to build the same thing with KDE3, you would use even more memory :)"
    author: "Tom"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "\"But in Qt4 there is implicit sharing of most basic classes which makes it use less memory, so logically, it uses less memory.\"\n\nNo, because you're ignoring the portions of Qt4 that use far more memory than Qt3.  The most important of these is double-buffering of *all* widgets, *all* the time.  By way of demonstration: A single Kwrite window is nearly all widgets.  On a 32-bit 1600x1200 monitor, the double-buffering for our Kwrite Window will take up a little under 4 bytes * 1600 * 1200 ~ 7.5 *megabytes*.  There's no way that Qt4's meagre savings in strings and QObjects will even come close to this, and so we are in the situation of having a text editor use many megabytes more than its Qt3/KDE3 counterpart.  Open 5 kwrite instances, and you are gobbling up a ludicrous *35MB* *just* for the double-buffering on the widgets.  \n\nQt4's so-called \"optimisations\" are one of the best examples of the phrase \"Penny wise, pound foolish\" imaginable.  "
    author: "Anon"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "Note that the buffering occurs only near the paint events. Unless I have missed something, and unless you have 5 displays: you do not have all the windows maximized and displayed/redrawn in the same time. Moreover, aren't paint events limited to rectangle regions, e.g. in KWrite: to the line where you're typing your text?\n"
    author: "jstaniek"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "No, unfortunately they are buffered continuously - even if they are on a separate desktop and you haven't used them for several hours :/ (As a corollary, it doesn't matter if only a small region of a window is updated.)\n\nIf the buffering was only used during paint events and then discarded for widgets that have not been re-painted in a while, then this would be pretty cool and efficient, but that's not the case.\n\n"
    author: "Anon"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Yes, Qt4 uses more video memory (which isn't difficult because realistically it's really difficult to be using less than 0 which we've been using up until Qt4) but it uses a lot less RAM.\nSo on systems with dedicated video memory it fallows that Qt uses a lot less memory. At least as far as you as a user of the API are concerned since, obviously, vmem is not where your application/library data is being held."
    author: "Zack"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Hmm.  Well on linux the memory used by double buffering shows up in the Xorg process.  Don't think this is stored in video memory, unless free is incorrectly counting video memory.\n\nIn one sense, double buffering is great.  It removes most UI flicker without any effort on the part of the application devs.  For my own commercial Qt development, it is a huge advantage.  But on the other hand, the cost is quite large when every app you run is completely double buffered, and it doesn't seem really necessary all the time.  For example, not all widgets suffer from flicker in Qt3.  It's really only things like a file view in a file manager or some other complex widgets.  And yet in Qt4, every single widget is double buffered, with the corresponding memory cost.\n\nAnd the problem is, turning off double buffering in Qt4 globally turns any Qt4 app into a huge flickering mess, with every single widget flickering like crazy, even widgets like menus and toolbars that don't flicker on Qt3 or GTK.  So what's the difference?  Did Qt3 manually double buffer those widgets?\n\n"
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Qt 3 had some tricks to reduce flicker without double buffering (for example, there used to be a \"bool noErase\" parameter to QWidget::repaint), which have been dropped in Qt 4 because double buffering is now used."
    author: "Kevin Kofler"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Perhaps the widget attribute Qt::WA_OpaquePaintEvent is the equivalent in Qt4. "
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "\"\"\"And the problem is, turning off double buffering in Qt4 globally turns any Qt4 app into a huge flickering mess, with every single widget flickering like crazy, even widgets like menus and toolbars that don't flicker on Qt3 or GTK. So what's the difference? Did Qt3 manually double buffer those widgets?\"\"\"\n\nNo need to enable/disable double buffering globally. From http://doc.trolltech.com/4.0/qt4-arthur.html#widget-double-buffering:\n\n\"Double-buffering is turned on by default, but can be turned off for individual widgets by setting the widget attribute Qt::WA_PaintOnScreen.\n\n    unbufferedWidget->setAttribute(Qt::WA_PaintOnScreen);\""
    author: "jstaniek"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Yep I've done this before on some special widgets, like one that displays frames from a camera.\n\nHowever I just did an experiment on an app I'm working on.  The biggest memory cost is obviously the main window widget itself.  It has a huge double buffering pixmap, and takes the most memory. So what happens if you disable double buffering on just the main widget, but leave it enabled on all others?\n\nWell, on the plus side, there is no added flicker, even if you disable double buffering on the main widget.  All the widgets that actually would flicker, like buttons, menus, checkboxes, etc. are still double buffered, so the UI is just as nice as before.  And the vast majority of the memory hit is gone.  A maximized window takes about 6mb less Xorg memory when I disable double buffering on the main window.\n\nHowever, there is one downside.  Widgets that are transparent now have a grey background instead.  So checkboxes have a grey rectangle background instead of letting the default style background show through.  This is with the oxygen style.  \nIf you choose the Plastique style instead, then everything looks much better.  Because the window background is grey anyway, the fact that checkboxes are not transparent is irrelevant.  So in this case you get the 6mb of ram back for basically free.  Not really a realistic option for most apps, but hey..\n\nI attached a comparison screenshot of the same app running with double buffering disabled on the main widget, using the oxygen and the plastique style to see the difference.\n"
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Sounds like a great idea, since lots of windows are there as a container for other widgets. In my little extra gear project, the main window is completely covered with dock windows.\nHowever with qt4.4 coming soon, there is only one native window and thus one double buffer. Just tried it with trunk and indeed the app looks completely broken with setAttribute(Qt::WA_PaintOnScreen);, docks aren't updated properly neither is the menubar and statusbar."
    author: "koos"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: ">> However with qt4.4 coming soon, there is only one native window and thus one double buffer.\n\nAre you sure?  I think each widget still has its own double buffer.  The native window issue is separate."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Your analysis is correct."
    author: "Anon"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "No, just a guess actually. Hmm, I need to figure out why the app is so broken then, maybe paint events aren't passed to child widgets ..."
    author: "koos"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "\"\"even widgets like menus and toolbars that don't flicker on Qt3 or GTK. \"\"\n\nNo idea what graphics card os settings you have but on Linux and Windows these items flicker _heavily_ and noticeable if you have switchable context like in KWord or Kexi.\n"
    author: "jstaniek"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "Hmm, I hadn't noticed.  I meant during normal usage, like mousing over a toolbar or a menu item they didn't flicker.  But Qt4 with double buffering disabled flickers on the same actions."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "\"\"even widgets like menus and toolbars that don't flicker on Qt3 or GTK. \"\"\n\nNo idea what graphics card or settings you have but on Linux/Qt3 and Windows/Qt3 these items flicker _heavily_ and noticeable if you have switchable context like in KWord or Kexi.\n"
    author: "jstaniek"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "What is the double buffering for anyway? If I look at the poor way windows and widgets are drawn at the moment (I'm referring to 4.0.2), I don't expect double buffering there. Or am I missing something?"
    author: "JeanPaul"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "The poor way windows are drawn?  Can you elaborate?  They are drawn nicely here.\n\nEvery widget is double buffered in Qt4, and by extension KDE4.  For example, dragging a window in front of a dolphin's file view does not cause any flicker as the file view refreshes, as it used to in KDE3."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-31
    body: "With \"poorly drawn\" I mean that I can see the window redrawing on a maximizing/restore/minimize in a very shaky way, and no compositing effects are smooth animations, even doing something simple like selecting file->new for example in any kde4 program makes that file menu pop up, then it disappear, then it is redrawn again, which is killing my user experience. And all that while my gfx card (a GeForce8600M) obviously can (and does) run compiz smoothly (except for fsaa, but that's another problem altogether anyway)."
    author: "JeanPaul"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "If you compare application to application, a simple one, you'll find that it does take less memory. That's what my tests indicated, by counting the marginal memory usage after the desktop libraries was already loaded (using /proc/<PID>/smaps). My testcase was kwrite.\n\nTwo things make it VERY hard to say that \"KDE 4 uses less memory than KDE 3\":\n\n1) it's not a fair comparison, since the features and services are very different. The experience with KDE 4 is much different from KDE 3. If you really want low memory usage and you're not worried about new features and bugfixes, you can keep using a very old KDE version, or even DOS...\n\n2) it's very difficult to account for memory usage for more than one application. One is easy because you know what is not shared with anyone else. With two or more, you have to ask: what is shared between those applications? And how many pages? We currently don't have the answer for that, but Andrew Morton (kernel developer) mentioned that 2.6.25 will contain a better interface to do just that.\n\nIn any case, the 40% figure came from a user that, while well-intentioned, had very little clue about what he was doing. What amazes me is that the press and the public keep referring to those numbers even after we publicly debunked them.\n\nYou'll notice that Matthias did not confirm the numbers. He's aware of memory efficiency improvements in Qt 4 and KDE 4 code, but he'll not vouch for a hard number like 40%."
    author: "Thiago Macieira"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "In an addition to the fellow mentioning the extra mem usage of double buffering.\nYou'll find this in this in the memory of the Xserver not in /proc/<PID>/smaps of the app, as this typically is done using XPixmaps."
    author: "koos"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "I'm not really complaining about memory usage.  More advanced frameworks use more resources.  That's just the way things go.  But due to double buffering, the same app on Qt4 will use more memory than using Qt3.  The process itself might take less, but the cost of double buffering easily erases that small advantage.\n\nAnd looking at the memory usage of the full desktop running typical apps, it is definitely much more than in KDE3.  Like I said, I'm not complaining, but I don't think anyone would seriously argue that it is the case."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-26
    body: "\"Its funny how one incorrect and quickly redacted measurement can convince so many people. KDE 4 certainly does not take less memory than KDE 3, and I don't think that myth is doing anyone any favours.\"\n\nHmmmmmmmmmmmm. Does that mean that KDE 4 does more than KDE 3 and uses more memory, or that when comparing KDE 4 and KDE 3 doing the same things KDE 4 uses less memory."
    author: "Segedunum"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "KDE4 uses more memory period.\n\nThe impact depends largely on the screen size.  On my EeePC, an empty KDE4 session uses about 10mb more than an empty KDE3 session.  On my work comp (1680x1050 display), the difference is much larger (~50 mb IIRC).\n\n"
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "\"On my EeePC, an empty KDE4 session uses about 10mb more than an empty KDE3 session. On my work comp (1680x1050 display), the difference is much larger (~50 mb IIRC).\"\n\nUnfortunately, measuring memory usagelike that, probably by just looking at top, is pretty meaningless."
    author: "Segedunum"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "The \"-/+ buffers/cache\" row in the output of free -m is pretty meaningful, actually - assuming no swap is being used."
    author: "Anon"
  - subject: "Re: Memory usage.."
    date: 2008-03-27
    body: "It is not meaningless at all, as long as you look at the line that subtracts the buffers/cache.\n\nIn fact, this is the most meaningful measure of memory for the user, because as a user I'm mostly interested in \"How much memory do I have left for applications\", and measuring total used memory gives that answer, without having to worry about complexities like how much of a process's used memory is shared."
    author: "Leo S"
  - subject: "Re: Memory usage.."
    date: 2008-03-29
    body: "\"It is not meaningless at all, as long as you look at the line that subtracts the buffers/cache.\n\nIn fact, this is the most meaningful measure of memory for the user\"\n\nIt doesn't means what you think it means at all. Read this:\n\nhttp://ktown.kde.org/~seli/memory/desktop_benchmark.html\n\nThe readings for top don't give you any real indication of your memory usage at all.\n\n\"In fact, this is the most meaningful measure of memory for the user, because as a user I'm mostly interested in \"How much memory do I have left for applications\"\"\n\nYou're not going to get anything accurate from top or free. This has been discussed before."
    author: "Segednum"
  - subject: "Re: Memory usage.."
    date: 2008-03-30
    body: "\"http://ktown.kde.org/~seli/memory/desktop_benchmark.html\"\n\nI wonder whether the higher memory consumption of GNOME is simply caused by GTK's double buffering and not by out-of-process applets etc...\n"
    author: "xyzzzz"
  - subject: "Re: Memory usage.."
    date: 2008-03-30
    body: "GTK doesn't double-buffer, IIRC.  Qt4 does, though, with a correspondingly large increase in memory consumption."
    author: "Anon"
  - subject: "Re: Memory usage.."
    date: 2008-03-30
    body: "\"GTK doesn't double-buffer\"\n\nI thought it does double buffering since GTK+ 2.0 (released 10 years ago)?\n"
    author: "xyzzzz"
  - subject: "Re: Memory usage.."
    date: 2008-03-30
    body: "I'm well aware of this page and have read it.  \n\nWhat you don't understand is that I'm not even remotely interested in exact memory usage.  I don't care if the value that free reports is +- 10%.  Free may not be the most accurate, but it's not random.  The values correlate with memory usage, and it's pretty clear that KDE4 uses more.  I could remeasure with exmap, but I would get the same conclusion so I really don't care."
    author: "Leo S"
  - subject: "End of interview giggle.."
    date: 2008-03-26
    body: "It made me smile that at the end of that interview, there's an error in the page ; \n\n\"Microsoft OLE DB Provider for ODBC Drivers error '80004005'\n\n[Microsoft][ODBC SQL Server Driver][SQL Server]Transaction (Process ID 74) was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction.\n\n/efytimes/fullnews.asp, line 620\"\n\nQt4.4 does however look to be a remarkable step forwards particularly in terms of how the UI is rendered. Looking forwards to KDE 4.1"
    author: "Maarte"
  - subject: "Typo"
    date: 2008-03-26
    body: "Article title says \"EYFTimes\" instead EFYTimes."
    author: "Anon"
  - subject: "Insert title here. "
    date: 2008-03-26
    body: "Its a good interview, one of the best in awhile. Though, I'm concerned on how bias he was when answering questions. \n\n\"The desktop problem has been solved many years ago. Try to compare Windows XP with KDE 3: nobody in their right mind would choose Windows over GNU/Linux based on the desktop experience alone.\"\n\nWhen I had Kubuntu installed with KDE 3.5 and a regular 15 year old friend over. He started to get frustrated while using KDE and wanted to use XP again and luckily I had a duel boot. It shows something, though hopefully he will have better reviews with KDE 4 but until alot of people like XP. There have been others, I have two other friends that hate when I have linux installed. \n\n\nAlso, can't wait for that hidden project. I hate how he said that, thats one irritating tease. "
    author: "Jeremy"
  - subject: "Re: Insert title here. "
    date: 2008-03-26
    body: "you shouldn't thrust Linux that much, i suppose. just show them that you don't have problems they have to face when using win32."
    author: "Nick Shaforostoff"
  - subject: "Re: Insert title here. "
    date: 2008-03-26
    body: "The founder of KDE is biased? Whaaa?"
    author: "Ian Monroe"
  - subject: "Re: Insert title here. "
    date: 2008-03-31
    body: "rofl :)\n\n\nNews at 11. So true :)"
    author: "Max"
  - subject: "Re: Insert title here. "
    date: 2008-03-26
    body: "> He started to get frustrated while using KDE and wanted to use XP again\n\nThat's not choosing XP based on the desktop experience alone. Your friend presumably wanted XP because he was familiar with it and wasn't with KDE. That's not 'desktop experience alone', just like e.g. hardware support or number of games available are not."
    author: "Lubos Lunak"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "The article is a bit stupid.  For example, compare XP with KDE 3.  Xp is what, 7 years old.  Of course kde 3. is better than such an old platform.  But, still with XP you can use more technology than with KDE, so if given the choice, if I wanted a computer that performed with high quality software I would go with XP, because at the moment the linux applications are still the major let down to linux."
    author: "anon"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "Is XP really 7 years old? Then can we count from KDE 3.0? Since I'm sure XP has had upgrades since it was first released. ;)"
    author: "winter"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "Xp was released Oct 25, 2001.  KDE 3.0 was released April 3, 2002.  Both have received updates since then.  So that's only just over a 5 month difference.  It's perfectly fair to compare them."
    author: "Leo S"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "But was KDE 3.0 better than XP? 3.5 is better, indeed... \nAnd it would be unfair to compare the release cycles of XP SP 1,2,... with KDE 3.1, 3.2, 3.5 etc. KDE intended to offer a large set of new features within these dot releases while XP service packs intended to update the underlying technology while offering mainly fixes in the desktop area... Even if we know that KDE4 will catch and pass by Vista within the next 12 month easily we still do not know what MS develops under the hood...\n\n"
    author: "Sebastian"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: ">> But was KDE 3.0 better than XP?\n\nAbsolutely.  XP before service pack 1 was a pretty poor operating system if you recall.  Many apps were not compatible, driver support was attrocious, performance on machines of the day was poor, security was absolutely terrible (until SP2 actually).\n\n>>  KDE intended to offer a large set of new features within these dot releases while XP service packs intended to update the underlying technology while offering mainly fixes in the desktop area\n\nNot entirely true.  Yes, the KDE Desktop got many new features, but most of those new features were in the applications, which happen to be bundled instead of distributed separately like on windows.  Similarly, Windows applications got many new features in this timeline.  The base kdelibs didn't change so drastically between 3.0 and 3.5, just like the Windows base didn't change drastically either."
    author: "Leo S"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "\"...I have two other friends that hate when I have linux installed.\"\n\nWhat? Do you uninstall it when your done?\n\nI annoys me when people use MS software. ;)"
    author: "winter"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "Naturally he got frustrated - it's different from what he's used to.  People got frustrated when using Windows 95 after getting used to Windows 3.x because it was different.  Perhaps eventually he'll get fed up with Windows' limitations and change, perhaps not.  But for someone new to computers (maybe a child getting their first computer) it's hard to make a case that they would choose Windows over Gnome, KDE or OSX on the basis of the desktop experience."
    author: "Adrian Baugh"
  - subject: "Re: Insert title here. "
    date: 2008-03-27
    body: "I made the experiment with my parents.\n\nIt was impossible for them, e.g. when suddenly applications are in English or downloading mails does not work with Kmail.\n\nI found out how overloaded the interface still is and how misleading.\n\nthink about the little problems you run into and fix on the fly and what someone else would do who cannot even deal with a remote control."
    author: "andy"
  - subject: "He is cute & good-looking man."
    date: 2008-03-26
    body: "This was the first time I saw his photo.\nHe is a hunk.\n\n:-p"
    author: "Lays Shoon"
  - subject: "Re: He is cute & good-looking man."
    date: 2008-03-27
    body: "LOL"
    author: "NabLa"
  - subject: "Re: He is cute & good-looking man."
    date: 2008-03-27
    body: "I think that is just about a requirement for working at Trolltech."
    author: "Leo S"
  - subject: "Re: He is cute & good-looking man."
    date: 2008-03-27
    body: "Yea, well, he's a german guy, and as an other german guy, let me say - without any bias of course - that those are generally total hunks. Always and every friggin one of them ;)"
    author: "MarcG"
  - subject: "Re: He is cute & good-looking man."
    date: 2008-03-27
    body: "You should have left your email address..."
    author: "Sebastian"
  - subject: "Re: He is cute & good-looking man."
    date: 2008-03-27
    body: "Did he participe to the show Beauty and the Geek ?"
    author: "JC"
  - subject: "Re: He is cute & good-looking man."
    date: 2008-03-28
    body: "That's just being sexist. If you keep objectifying men like that then soon you will chase all the men from the internet.\n\n(P.S. before anyone goes up in arms, that was said in the gentlest of humour)"
    author: "Concerned Citizen"
  - subject: "Real world scenario"
    date: 2008-03-29
    body: "The problem is not a fresh installation, the problem is always a corrupted one you have in actual use, misconfiguration etc.\n\nWindows loses strong performance and user experience when you install third party tools. It becomes a virus mess.\n\nWith KDE the main problem was sound configuration, quality issues, context menues and strange popup screens."
    author: "andy"
  - subject: "Teasing is good :)"
    date: 2008-03-31
    body: "Creates more buzz around the project.\n\nThanks for posting this."
    author: "Max"
---
EFYTimes has <a href="http://www.efytimes.com/efytimes/25412/news.htm">an interview with Matthias Ettrich</a>, one of KDE's founders.  He talks about the history of the project, what he thinks of KDE 4.0 and what he's currently working on in Qt.  "<em>The desktop problem has been solved many years ago. Try to compare Windows XP with KDE 3: nobody in their right mind would choose Windows over GNU/Linux based on the desktop experience alone.</em>"



<!--break-->
