---
title: "KDE e.V. Endorses Community Working Group, Code of Conduct"
date:    2008-08-12
authors:
  - "skuegler"
slug:    kde-ev-endorses-community-working-group-code-conduct
comments:
  - subject: "Great move! "
    date: 2008-08-12
    body: "I really really like it!\n\nNow only one thing is missing:\n\nGreat documentation!\n\nKDE should be as well documented as Qt. \nIf you look at posts like this one:\n \nhttp://www.netsplit.com/2008/08/11/development-platform/\n\nyou can clearly see that the pillars message is not coming across.\n\nAll websites should up to date and be a great marketing tools for getting new contribututors. ( not like http://plasma.kde.org/ that will be updated next month for years now or so it seems )\n\nSo why don`t you put the money in the bank to good use and pay someone to document KDE for 2 months right before each release. I think that would serve the communtiy way better than e.g. paying for a executive director that talks about open source sueing people and liking Amazons DRM and buying diapers ( sorry, I couldn't resist ).\n\nI really think KDE has the best tech out there, but documenting it is something nobody seems to like to do. So the solution IMHO is to pay someone to do it.\n\nOnly then can you truely rival Apple and MS for developer mindshare on all platforms. ( Just look at http://linux.dell.com/files/presentations/Ottawa_Linux_Symposium_2008/students.ogg if you don`t believe me. )\n\nKDE4 obviously rocks and you should try to explain that to everybody.\n"
    author: "Tom"
  - subject: "Re: Great move! "
    date: 2008-08-12
    body: "> I think that would serve the communtiy way better than e.g. paying for a\n> executive director that talks about open source sueing people and liking Amazons\n> DRM and buying diapers ( sorry, I couldn't resist ).\n\nNone of the e.V. board are paid, they are all volunteers.\n"
    author: "Richard Moore"
  - subject: "Re: Great move! "
    date: 2008-08-12
    body: "Sorry, misunderstanding. \nI meant the the executive director of some other fairly big unix desktop software project. \n\nDoes KDE even have a executive director?"
    author: "Tom"
  - subject: "Re: Great move! "
    date: 2008-10-12
    body: "Nope."
    author: "Anon"
  - subject: "Re: Great move! "
    date: 2008-08-12
    body: "Good point. M$ soft is definitely better in this aspect. It's documentation is crappy and inaccurate, but it has this subtle advantage over KDE's docs in that it EXISTS."
    author: "slacker"
  - subject: "Re: Great move! "
    date: 2008-08-12
    body: "What is wrong about techbase?\n\nAs of documentation of applications the main problem seems to be that it covers the irrelevant or obvious aspects but not the real problems you usually run into. I would like to see a link to an application specific user forum. In order to avoid a disappearence of the forum it should be a generic forward. E.g. http://kdevelop.forum-kde.org to link to the existing Kdevelop forum: http://www.kdevelop.org/phorum5/index.php and kate.kde-forum.org to link to a Kate user forum."
    author: "andy"
  - subject: "Re: Great move! "
    date: 2008-08-12
    body: "Oh, I just read this here:\nhttp://wadejolson.wordpress.com/2008/08/12/akademy-day-4-afternoon-notes/\n\n\"Danimo had a quick meeting on the state of KDE\u0092s wiki.  If you want to help out, please contact him.  Important, as TechBase is starting to get used for everything, and might eventually\u0085wait for it\u0085.turn into another bloated chaotic wiki if we aren\u0092t careful in keeping it topical.  I wasn\u0092t able to attend, but it was certainly for a Good Reason \u0099 (no comment possible now).\"\n\nI personally can recommend the concept of wiki farms to you. With great Affero GPL wiki farm software as the one used by Wikidot.com you can simplify the creation of dedicated and flexible wikis a lot and wikidot has a fantastic and easy permissions system. For your wiki farm you get a single sign-on mechanism and its easy to upgrade a user to an editor or admin (if it's a restricted wiki), theme the wiki, make the wiki private, get a new wiki hosted by the farm or even create de-facto \"static sites\".\n\nPitfall so far: it does not support login via Konqueror yet for whatever reasons."
    author: "andy"
  - subject: "Re: Great move! "
    date: 2008-08-12
    body: "Hmm, techbase is OK. But it is far from _Great documentation_ and there are lots of missing pieces.\n\nAnd don`t get me started on application documentation. \n\nTry to start hacking on konqueror and you will see that there is close to nothing that will help you. Simple things like a manifest where each source file is explained briefly, an overview of how things work etc. \n\nSimple things like that would really help new contributors. I tried and failed miserably because the solution given was: Look at the source! \n\nThat is not great documentation IMNSHO.\n\nStarting to hack on the linux kernel is easier, although it is way more complex, because there are good books and the docs in the kernel are fairly up to date and the source is better commented."
    author: "Tom"
  - subject: "Re: Great move! "
    date: 2008-08-13
    body: "How about these?\n\nhttp://api.kde.org\nhttp://lxr.kde.org"
    author: "Jucato"
  - subject: "Re: Great move! "
    date: 2008-08-13
    body: "http://api.kde.org/?"
    author: "Michael \"api.kde.org\" Howell"
  - subject: "Re: Great move! "
    date: 2008-08-13
    body: "That is what I would call \"OK documentation\", but it is still far from great.\n\nJust look around at:\n\nhttp://ldn.linuxfoundation.org/\n\nhttp://www.mozilla.org/developer/\n\nand you will see a big difference. The content there is not generated by some doxygen scripts und it is indepth. It is done by humans and that is why it is so much better.\nAnd the websites are not woefully out of date.\n\nJust yesterday I saw two articles about Akademy pointing to plasma.kde.org. \n( on heise.de and linuxmagzin.de ). \nDocumentation like that and generated stuff like api.kde.org give a very bad first impression if you ask me."
    author: "Tom"
  - subject: "Re: Great move! "
    date: 2008-08-14
    body: "Do you know about\n\nhttp://techbase.kde.org/Welcome_to_KDE_TechBase ?\n\nAlso, it is good that api.kde.org is autogenerated, because each documentation-fix in the source code automatically ends up there the next day (or week, not sure how often it is being regenerated)"
    author: "Carsten Niehaus"
  - subject: "FLA"
    date: 2008-08-12
    body: "Seems like the new header that gives the KDE e.V. the authority to pick a later version of the GPL license mostly solves the license issue. But doesn't the FLA also protect the developer from being sued to some extent? \n\nThough I (and probably most devs) have the ultimate defense from being sued: being too poor to bother suing. ;)"
    author: "Ian Monroe"
  - subject: "Re: FLA"
    date: 2008-08-23
    body: "Money is rarely a reason to sue you. They might sue to stop you from developing FOSS, or scare others away."
    author: "jospoortvliet"
  - subject: "Code of Conduct"
    date: 2008-08-13
    body: "That's a step in the right direction for FOSS. Very cool."
    author: "winter"
---
On Monday at Akademy, KDE's yearly world summit, the <a href="http://ev.kde.org">KDE e.V.</a> held its general assembly, covering a wide range hot topics, regarding licensing and community scalability. While part of the meeting is dictated by intricacies of German association law, the AGM also provides a way of effectively solving issues arising in the KDE community and deciding on ways to move forward as an organisation. This year's KDE e.V. General Assembly endorsed a Code of Conduct, the Community Working Group and a Fiduciary License Agreement for KDE contributors.








<!--break-->
<h2>Code of Conduct</h2>

<p>The KDE e.V. has ratified a proposal for a Code of Conduct as accepted shared set of values for participating in the KDE Community. This Code of Conduct presents a summary of the shared values and "common sense" thinking in our community. The basic social ingredients that hold our project together include:</p>

<ul>
    <li>Be considerate</li>
    <li>Be respectful</li>
    <li>Be collaborative</li>
    <li>Be pragmatic</li>
    <li>Support others in the Community</li>
    <li>Get support from others in the Community</li>
</ul>

<p>The full version of the <a href="http://www.kde.org/code-of-conduct/">KDE Code of Conduct</a> is online now.</p>

<h2>Community Working Group</h2>

<p>Preparations in the last months have now led to the founding of KDE's first Community Working Group. The purpose of this new body endorsed by KDE e.V. is summarised as:</p>

<em>"The Community Working Group aims to act as a central point of contact
by being available to communicate user needs and concerns to
developers, and developer intentions and plans to users."</em>

<p>The initial members of KDE's Community Working Group are:</p>

<ul>
<li>Anne Wilson</li>
<li>Juan Carlos Torres</li>
<li>Lydia Pintscher</li>
<li>Martin Fitzpatrick</li>
<li>Tom Albers</li>
</ul>

<h2>KDE e.V. Logo</h2>

<p>As KDE e.V. is a different entity from the more comprehensive KDE Community, the spirit of this organisation is now displayed by having its own logo. The three flags on top of the logo represent the three main tasks of the KDE e.V.: supporting the KDE Community, representing the KDE Community, and governing the KDE Community. The logo has been contributed by Oxygen team member David Vignoni.</p>

<img src="http://static.kdenews.org/jr/kde-ev-logo.png" width="395" height="253" />

<h2>Fiduciary License Agreement</h2>

<p>The Fiduciary License Agreement is a legal safety net to keep KDE's licensing effective in cases where a change of licensing is required (for example in the unlikely case that the GPL is void) and the contributor holding the copyright of a certain piece of code is not able to change the license of her work.</p>

<p>The FLA is in no way required for KDE developers, but a suggestion by the KDE e.V. to mitigate the risk of possible problems surrounding copyright assignment and licensing. It is left to each individual copyright holder whether or not to sign the agreement.</p>

<p>Details on all the above points will be published on the KDE e.V. website soon.</p>





