---
title: "KOffice 2.0 Alpha 7 Released"
date:    2008-05-09
authors:
  - "iwallin"
slug:    koffice-20-alpha-7-released
comments:
  - subject: "KWord targeted for 2.0"
    date: 2008-05-09
    body: "That is definately good to hear.  That should make people happy.\n\nI believe it is lower on the priority list than getting all the core apps in 2.0, and having solid ODF support, but what is the status of Mac and Windows ports?\n\nIs it safe to expect a Mac and Windows build the day that 2.0 launches?\n\nFurther down the chain, but important to many, how will MS binary format import/export compare to KOffice 1.x and OOo?"
    author: "T. J. Brumfield"
  - subject: "Mac & Windows"
    date: 2008-05-09
    body: "> Is it safe to expect a Mac and Windows build the day that 2.0 launches?\n \nThis release already makes sure that everything builds just fine on Windows, the mac version build great for at least a year already (last year a Summer of code student did his koffice2 project on the mac).\n\nSo, while there is no guarantees the chance is pretty good that indeed the day we launch 2.0 it will be cross-platform.\nThe main problem is that we hardly have anyone compiling/running koffice apps on Windows.  Just a couple of awesome kde-devs that make all of kde compile on Windows.\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: Mac & Windows"
    date: 2008-05-09
    body: "If a Windows build existed that I could download, I would be happy to test and submit bug reports!\n\nIdeally, I think it should be included with all the other KDE 4 builds downloaded through the KDE 4 Windows installer."
    author: "T. J. Brumfield"
  - subject: "Re: KWord targeted for 2.0"
    date: 2008-05-09
    body: ">Further down the chain, but important to many, how will MS binary format import/export compare to KOffice 1.x and OOo?\n\nI'm working on the KWord filter for the summer of code (porting the filter to ODF and working on graphics in libwv2). I'm not sure how much of my code will be in KOffice 2.0, but it's being worked on. So, the filter should be improving with respect to KOffice 1.x, but we've got a ways to go to catch up with OOo."
    author: "Benjamin"
  - subject: "Re: KWord targeted for 2.0"
    date: 2008-05-09
    body: "Thanks for your reply!  That is good to know!"
    author: "T. J. Brumfield"
  - subject: "Re: KWord targeted for 2.0"
    date: 2008-05-10
    body: "Actually, I think you should be working in the KOffice 2.0 codebase."
    author: "Boudewijn Rempt"
  - subject: "Re: KWord targeted for 2.0"
    date: 2008-05-10
    body: "Whoa, sorry, I guess I wasn't clear. My work is definitely in the 2.0 codebase (in the msword-odf branch); I just wasn't sure if my work would be done in time for the 2.0 release. I guess it probably will be, if 2.0 isn't released until August or later."
    author: "Benjamin"
  - subject: "Why so many??"
    date: 2008-05-09
    body: "i dont mean to offend the kde team but why do we need another office suite?? \n\n"
    author: "Darwin Reynoso"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "KOffice is like 9 years old or something."
    author: "Ian Monroe"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "So many?\n\nThere are actually relatively few Free software apps in the spaces KOffice apps exist in. Kexi and Krita are two really good examples, but even KWord, KPresenter and KSpread only have a couple Free software companions each.\n\nIt's nothing like the situation with text editors or media players.\n\nAlso keep in mind that KOffice predates the other Free software apps in this space, with the exception of the \"old school\" apps that can be used for similar tasks (e.g. latex)"
    author: "Aaron Seigo"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "I still use OpenOffice, and likely will for some time, however Krita looks really impressive and it might replace Photoshop for me."
    author: "T. J. Brumfield"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "I still use openoffice too. But im not a happy user. It does not integrate well with my desktop, takes a long time to load. I use it for the lack of better alternates. The day koffice can match i'll make the switch 100%."
    author: "Kishore"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "I totally agree with you.\n\nI also hope that the KDE guys realize that I wouldnt mind to donate money, after all such a cause helps me too. Time is really precious, better soon than late :)"
    author: "she"
  - subject: "Krita"
    date: 2008-05-09
    body: "Personally, I find that Krita is often a better tool for my graphics tasks than GIMP. For instance, I made a document management application mockup (http://syntaktisk.dk/grphcs/docuzilla-mock.png) a while ago, and I found that using Krita made it much easier. I make quite a few graphics buttons, and that is nice with Krita."
    author: "Morten Juhl-Johansen Z\u00f6lde-Fej\u00e9r"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "I think that if there's one thing missing in the free software desktop so as to be ready for the average user is a good, full-featured office suite. KDE4/QT4 technologies are just awesome, so KOffice has the possibility to become the best office suite ever!"
    author: "Lucianolev"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "OpenOffice doesn't seem capable of ever growing beyond the 'copy MS Office 97' paradigm. It's codebase is too complex and dazzling to attract new developers, hence innovation is unlikely. KOffice, on the other hand, has a strong base to build upon, and is already doing a lot of interesting things. As I'm a strong believer in innovation as the way forward for FOSS, the chances of KOffice 'saving' the office workspace for FOSS are imho much larger than for OO.o."
    author: "jospoortvliet"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "While I agree with you in theory, judging by the always existing lack of developers in KOffice, the simplicity and structure of it's code base (at least, relative to OO.o) does not really help in practice to attract new developers. Or am I off in this respect?"
    author: "Andr\u00e9"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "KOffice has far more volunteer coders than openoffice. What will happen if Sun stops funding openoffice? How many paid developers are required just to maintain it?\n\nKOffice, with a clean and lean code base could save the day in that situation. just look what happened to KHTML x Mozilla."
    author: "Amadeo"
  - subject: "Volunteers"
    date: 2008-05-09
    body: "I think OOo lacks volunteers because Sun demands to own the copyright for all submissions, but they do have people like Sun, IBM and Novell paying a decent sized staff to work on OOo."
    author: "T. J. Brumfield"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "'does not really help in practice to attract new developers. '\n\nI dont agree really.\n\nBesides the problem is really that openoffice is an artificial effort right now, marketed by Sun.\n\nKoffice is MUCH more close to the community.\n\nThe only problem is that I cant code in C++ (and I wont. I use ruby for all my work). :)\n\nThe best solution would be to be as generic as possible and allow people to write plugins etc.. in the language they use. I really have no need for java, c++ or whatever anymore, and I dont want to waste time just for coding in C++ (because I know, I wouldnt code in C++ anyway.)."
    author: "she"
  - subject: "Re: Why so many??"
    date: 2008-05-10
    body: "Well actually with the Kross binding, you can write features in Ruby, Python and JavaScript, either scripts or even dockers. I don't know what you are intersted in doing, but you can do stuff in ruby, and we would really love to have some inputs on the bindings."
    author: "Cyrille Berger"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "There are NOT many office suites.\n\nAnd especially word processors, there are like openoffice (i hate it...) abiword (the best, but has not enough features) and koffice (i think koffice will become the best word processor in the future, it has a smarter developer base. Openoffice somewhat sucks, and is so java centric - it should be more user centric.)"
    author: "she"
  - subject: "Re: Why so many??"
    date: 2008-05-09
    body: "Hopefully this won't turn into a \"but so-and-so is doing it\" and \"they'll obviously do it much better\", so \"let's give up on our stuff now\".\n\nAlong with \"we only need one free version and ours isn't good enough because we don't have an infinite number of developers on it\"."
    author: "bah"
  - subject: "Re: Why so many??"
    date: 2008-05-10
    body: "While witting a book I started off in OpenOffice, then moved to Kword, both applications left me dual booting windows simply so I could use office without the need for wine.  It's nice to build applications to try and get people away from office, but when you use those applications and then they screw up your work when given to the publisher, it is not worth the time and hassle.  I personally do not see a time when I will move back to either, and have become my next novel using Microsoft Word"
    author: "anon"
  - subject: "Re: Why so many??"
    date: 2008-05-10
    body: "On the other hand, when someone I knew had to write a book, and did it in MS Word, and noticed she had only 70 pages out of about 500 left after a save, I had to take a three hour train journey to help her out. In the end, I only got the project to the printer through Open Office.\n\nIn other words, assuming that Word won't screw you is a really sure-fire way to get screwed."
    author: "Boudewijn Rempt"
  - subject: "Re: Why so many??"
    date: 2008-05-10
    body: "Writing long and/or complicated documents with MS-Word is not really a safe thing to do.  This is why many attorneys and government agencies use WordPerfect.\n\nOpenOffice-Writer is a useful replacement for MS-Word, but it isn't nearly as good as WordPerfect-8.x (the last version ported to UNIX [this is WPDOS not WPWIN]).\n\nI do hope that the KWord developers set their sights on WordPerfect rather than MS-Word."
    author: "JRT"
  - subject: "Re: Why so many??"
    date: 2008-05-11
    body: "Have a look at Lyx (http://www.lyx.org/), that is rock-solid, LaTeX-based document formatter."
    author: "Renaud"
  - subject: "kplato?"
    date: 2008-05-09
    body: "I've been looking forward to kplato being usable for actual project planning when koffice 2.0 releases. Is this still going to happen?"
    author: "Kishore"
  - subject: "Re: kplato?"
    date: 2008-05-09
    body: "I think kplato is one of the most ready applications, especially that it's one of the application that have less needed to change to the new technologies in 2.0."
    author: "Cyrille Berger"
  - subject: "Re: kplato?"
    date: 2008-05-09
    body: "That's really nice! There needs to be more marketing then! :-)\n\nFrom the website:\n\"Important features not included:\n- Resource leveling. Resources are just allocated to the requested task even when allready booked to other task(s). The resulting conflict must be resolved manually by adding task links or constraints to avoid parallel scheduling of the tasks.\n- Network view.\n- In general: Project execution and follow up.\"\n\nIs kplato now capable of these? especially the first and the third?"
    author: "Kishore"
  - subject: "Re: kplato?"
    date: 2008-05-09
    body: "Oh, I'd love some more features in Kplato. From what I can see Kplato is alone in the project planning field. \nImprove this and add hooks to kontact and other applications and you've got yourself a \"killer app\" imho."
    author: "Oscar"
  - subject: "interesting"
    date: 2008-05-09
    body: "It's interesting to see the KOffice developers focus on some applications to get them out. FOSS development is rarely good at that: focussing on something because it's good for the project, instead of focussing on new & cool stuff. I think it's saying something good about the KOffice dev's ;-)\n\nBut of course, many of us already know what a responsible bunch they are..."
    author: "jospoortvliet"
  - subject: "KOffice Development"
    date: 2008-05-09
    body: "I think any release of the KOffice be it Alpha, Beta or Final is an astronomical achievement, also if you take the number of KOffice developers into consideration. \nAs a user I've known Koffice since KDE was in it's infancy; and believe me, with the means at it's disposal it has come a hell of a long way.\nThese developers must be working pretty hard. At the same time they are introducing new technologies into the workspace. To fulfill these visions takes nothing but a lot of guts, a lot of hard work, and above all perseverance. After the next major 2.0 release it would be fantastic to see more developers joining their development team.\nIt's funny how a lot of people always need to see some end-product before they can actually believe in it. On the one hand I believe it's good to know the status of something; but more then anything I believe projects need people who believe in a vision and can creatively see the goals ahead of them. I have a lot of respect for your work, so it's hats off from me! "
    author: "Pieter Philipse"
  - subject: "Re: KOffice Development"
    date: 2008-05-09
    body: "++ - it's also hard to believe that such a comprehensive office suite can be written in less than 1 million lines of program code (swing by ohloh.net and compare this figure to other office software)."
    author: "Anon"
  - subject: "Re: KOffice Development"
    date: 2008-05-09
    body: "But then you are not counting the lines of code of the kdelib and QT that KOffice is making use of.\nPlease remember that OpenOffice.org isn't just an office suite. It has its own crossplatform toolkit, just like QT but much more uglier and harder to deal with. When you count the lines of code of KOffice you don't put the libs it's using too.\n\nThe same stupidity has been done by the Mozilla team. Their browser ain't just a browser, it has its own motherfucking ungodly platform that they are the only one to use (and a handful of crappy, slow programs like Miro and Komodo)."
    author: "Anon"
  - subject: "Re: KOffice Development"
    date: 2008-05-12
    body: "I'd rather say it's OOo's (and Mozilla's) issue that they don't even bother to separate all the crossplatform toolkit stuff instead embedding it with every single app. So KOffice devs shouldn't be shy referring to their low SLOC."
    author: "Anon"
  - subject: "Re: KOffice Development"
    date: 2008-05-09
    body: "x2! Thanks to the Devs for their awesome work! Keep it up!"
    author: "Imlovinit"
  - subject: "Re: KOffice Development"
    date: 2008-05-09
    body: "I'll have to agree, KOffice is awesome, I am just going to school and it offers everything I need for my favourite desktop!"
    author: "d2kx"
  - subject: "Obvious question"
    date: 2008-05-09
    body: "Since nobody else has, I'll say it: Screenshots?"
    author: "Morten Juhl-Johansen Z\u00f6lde-Fej\u00e9r"
  - subject: "Re: Obvious question"
    date: 2008-05-09
    body: "We did the better thing, you don't have to wait for us to show you stills of the greatness, you can experience the greatness by installing it yourself!  [insert cool sound here]. :)"
    author: "Thomas Zander"
  - subject: "Re: Obvious question"
    date: 2008-05-09
    body: "No, I agree with him.\n\nScreenshots are vital and easy, just do 'scrot' and thats it. Or ksnapshot if you must. ;)"
    author: "she"
  - subject: "Kivio"
    date: 2008-05-09
    body: "First of all thanks a lot for your work Kde and Koffice teams!\nOne application, in my opinion that has a lot of space to grow, because there's no rival, or any good FOSS implementation is Kivio.\nThere's always commercial solutions, but if you choose omnigraffle, you need to stick with MacosX, or if you choose Visio you need to stick with Windows.\nBut in the FOSS world, the only thing you can get apart from Kivio is Dia and the Openoffice one.\n\nA lot of people need to make diagrams, from companies to students, but Kivio at this moment, seems to have only one developer. (from a IRC talk)\nI know everybody is busy this days, I included... but the facto is, if we can get a good FOSS alternative, in a deficient area, we can get a lot of success... Imagine apache web server...\n\nNote: this post is only intended to be a constructive though only. (in case somebody feels offended)"
    author: "Paulo Fidalgo"
  - subject: "Re: Kivio"
    date: 2008-05-09
    body: "\"Note: this post is only intended to be a constructive though only. (in case somebody feels offended)\"\n\nSure, I think most people would agree with you.  The difficulty is, though: what are we going to do about it? Lots of people are pointing out problems, but no one seems to be coming up with much-needed solutions."
    author: "Anon"
  - subject: "Re: Kivio"
    date: 2008-05-09
    body: "Indeed, no offense taken :) And we all agree with you, but there isn't much we can do about it. We are trying to find ideas on how to attract new developers, and the new architecture in KOffice2 will allow to bring a lot of improvement to Kivio, better graphics, better text, so the future is bright, but we indeed need more people to make that future."
    author: "Cyrille Berger"
  - subject: "Re: Kivio"
    date: 2008-05-10
    body: "No \"donate\" button on koffice site? How about letting people give a few bucks every now and then and then use that money to keep devels motivated and perhaps even attract a few new ones?\n\nLet KDE e.V. fund trips and such but let the project manager buy books/gifts/things to devels that he/she thinks should be encouraged somehow?\n\nJust a thought, feel free to ignore.\nI'd give a few bucks just for Kivio and Kplato.\n"
    author: "Oscar"
  - subject: "Re: Kivio"
    date: 2008-05-12
    body: "A nice idea, but hard to implement. Accepting credit cards is expensive. Paypal is the best option, probably, except they're untrustworthy, and you're just as likely to have them freeze your account and money as you are to get anything. (http://paypalsucks.com for tales of woe)\n\nI've seen people post their amazon wishlist or the like, and then you can buy things off there. That might be the easiest option. But not cash."
    author: "anon"
  - subject: "Re: Kivio"
    date: 2008-05-12
    body: "Seeing how KDE e.V. uses paypal perhaps koffice could too? While I understand that paypal isn't ideal by any means perhaps it could be sufficient?\n\nOr do it like you suggested, with wishlists and such. It would be nice to make sure that the project manager had some money to use for his/her developers though.\n\nIsn't there some kind of tipping jar software out there? I do think that it's easier to get 10 ppl to donate $10 than it is to get 1 person do donate $100, even if it is to buy a book of amazon.\n\nFor now my paypal money (yes, I have a small amount) goes directly into KDE e.V. and those projects that have a paypal account. "
    author: "Oscar"
  - subject: "Re: Kivio"
    date: 2008-05-13
    body: "If you paypal the KDE e.V. money and say it's a donation for KOffice it'll get spend on us :-). We are very fortunate as a project that we fall under the KDE e.V. umbrella: they handle all that kind of thing for us, and we get to spend way more money than we'd be able to spend on developer sprints, conferences and other fun stuff otherway.\n\n(Like, I've just come back from the Libre Graphics Meeting, for which the e.V. sponsored three KOffice developers travel and lodging. I'm going to write up a dot story tonight.) "
    author: "Boudewijn Rempt"
  - subject: "thumbs up!"
    date: 2008-05-09
    body: "I am very gespannt auf KOffice, it is a superb Officesuite."
    author: "KOfficeuser"
  - subject: "Re: thumbs up!"
    date: 2008-05-10
    body: "\u00bfQu\u00e9?"
    author: "Manuel"
  - subject: "Re: thumbs up!"
    date: 2008-05-10
    body: "not too difficult to work out: \"in eager anticipation of\" "
    author: "Gerry"
  - subject: "Re: thumbs up!"
    date: 2008-05-11
    body: "Just a little bit Gerrrrman (you know Rammstein?). Dont worry about it. It means somewhat like \"I cant await the final release.\" or so."
    author: "trans"
  - subject: "congratulations !"
    date: 2008-05-10
    body: "\"and especially better support for the OpenDocument format\"\n\nwonderful\n\nthis is the key to ODF success: 100% equal-fidelity in implementer applications\n\n"
    author: "orlando"
  - subject: "Great Improvements"
    date: 2008-05-18
    body: "Especially kexi seems to become an outstanding app!\n\nThanks for the new reporting tool!"
    author: "Maik Regner"
---
The <a href="http://www.kde.org/">KDE Project</a> today announced the release of <a href="http://www.koffice.org/releases/2.0alpha7-release.php">KOffice version 2.0 Alpha 7</a>, a technology preview of the upcoming version 2.0. This version adds a lot of polish, some new features in Kexi and KPresenter and especially better support for the OpenDocument format. It is clear that the release of KOffice 2.0 with all the new technologies it brings is drawing nearer.





<!--break-->
<p>This is mainly a technology preview for those that are interested in the new ideas and technologies of the KOffice 2 series.</p>

<p>The Alpha 7 release is a work in progress.  This release introduces improvements in almost all the components as well as in the common infrastructure. All the applications saw big changes, both bugfixes and new features.</p>

<h3>Ongoing Polish</h3>
 
<p>The release notes for Alpha 6 noted the addition of snap-guides that will guide the user when he or she is placing objects near other objects in any direction, Alpha 7 adds a bounding-box snapping option for even more powerful snapping options. This release has also added the option for users to configure shape-shadows in the form of a new dock widget.</p>

<h3>Report Generator for Kexi</h3>

<p>There is a whole new set of features in Kexi, the most important of which is a new report generator. This allows the user to create a document based on the data in the database with features like charting and scripting.</p>

<h3>New Features in Krita</h3>

<p>Krita has received a lot of attention e.g. general polish and bugfixing. Krita has additionally received a new plugin type which will allow both KOffice developers as well as 3rd party developers to create new pixel-generator filters, such as clouds or fractal generators.</p>

<h3>Developer Focus on KPresenter</h3>

<p>The KOffice developers decided to focus their efforts to make KPresenter an application that will be released in the 2.0 suite. This has given KPresenter a lot of page-effects in this release, but has managed to do more work on master page editing as well. Much of the work on the KOffice libraries have been specially selected because it benefits KPresenter in particular.</p>

<h3>Improved OpenDocument Support</h3>

<p>This release is the first to see some results of the OpenDocument Format testsuite being imported into KOffice.  The testsuite exists from a lot of little documents that each show one feature in ODF.  Automated testing of loading those documents will allow developers to keep on working on the code without fear of breaking the already working code. This is known as regression testing.</p>

<p>In this release already 23 tests are added into KOffice and the results are visible in much better loading of text documents in KWord. KWord is also one of the target applications for 2.0, and NLNet has sponsored a developer working on that application.</p>

<p>KOffice 2 is still under heavy development. It is not meant as something
to be used for any real work and can crash at any point.  However, here are
some of the highlights of the upcoming KOffice 2 series. Note that not all
of the new technologies will be fully implemented in the first release, 2.0.0.</p>


