---
title: "PolishLinux Interviews KPackageKit Authors"
date:    2008-10-07
authors:
  - "ppelzowski"
slug:    polishlinux-interviews-kpackagekit-authors
comments:
  - subject: "PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "Ah, finally I've submitted my first news to dot.kde.org. I hope you like it ;)\n\nIn PackageKit world many interesting things are going on:\n1) http://blogs.gnome.org/hughsie/2008/09/19/packagekit-collections/ , really useful\n2) http://blogs.gnome.org/hughsie/2008/09/09/packagekit-web-plugin/ , I personally think this is a great idea, installing applications from your distribution repository will by much easier for newcomers. Just go to the website and click. It is not adding another repository, it is installing applications from your distribution repository.\n\nPS. Now the download inside kde4.1 works only for plasmoid which are written in a scripting language. Maybe packagekit could be a solution for C++-plasmoids? As a result Plasma's AddWidgets window would list also plasmoids from your distribution repository additionally to scripts-plasmoid and both can by installed without hassle. \n\nregards \nPiotr\n"
    author: "Piotr Pe&#322;zowski"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collectns"
    date: 2008-10-07
    body: "That packagekit-collections stuff isn't really the best way to handle distribution-defined groups (at least in the eyes of me and several other people on the fedora-devel-list) and the PackageKit folks are already working on a better solution (dynamic categories), based on feedback from Fedora developers."
    author: "Kevin Kofler"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "I've been quite liking packagekit for a while on fedora - although old habits die hard and i sometimes find it faster just to fire up a console and use yum directly, particularly for big jobs because I can see exactly what is going on - but it's great for routinely keeping a system updated.\n\nAs for Kpackagekit - well as a kde user having a kde interface just makes the whole thing a bit nicer :-)\n\nOne thing I have noticed - when policykit (I think?) causes a request for the root password to perform something it is still a gtk dialog that pops up - is this/can this be part of (k)packagekit. It would just be nice to have a KDE dialog for consistency but obviously not a big issue. Or maybe this is because I still have the gnome applet installed too? I'll have a bit more of a play with it and see..."
    author: "Simon"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "There is no PolicyKit KDE integration yet. There were some plans to implement it for 4.2, I don't know what has become of those, but it's definitely not there in 4.1."
    author: "Kevin Kofler"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "Ah ok. That would make sense then, thanks for the info."
    author: "Simon"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "There is a Policykit-Kde in playground/base.i dont know if it works though.iirc Dirk was working on it."
    author: "Emil Sedgh"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "QPackageKit (i'm referring to the lib-qt) just use the \"available\" method for the authentication. That means that if there is no gtk polkit auth it'll try to use consolekit (prompt goes to the terminal), but as soon as a kde polkit becomes available it will be used since who call the auth is PolicyKit itself we just invoke it through DBUS.\nHope you like it. :)"
    author: "dantti"
  - subject: "Re: PackageKit Web Plugin and PackageKit Collections"
    date: 2008-10-07
    body: "Yep, that all makes sense - thanks.\n\nAnd Packagekit - well I really liked the idea when Rich Hughes first started blogging about it although perhaps I was a little skeptical. But the gtk front end in fedora is pretty nice and overall I'm a fan - there are some things I prefer to do in the console but I think that is just me.\n\nHaving tried Kpackagekit briefly in fedora rawhide I've got to say I like that too - I'm very impressed at how complete it seems already. I think getting the usability people to have a general look over it would be a good idea though - there is nothing really that comes to my mind as a criticism but I haven't played with it much - I think I've done one update and installed firefox on rawhide and that's it. But it certainly looks good so far."
    author: "Simon"
  - subject: "packagekit"
    date: 2008-10-07
    body: "packagekit, the idea to have one API for all packaging system is great. But its author srefuse to adapt it to debian based distro. So in its goal to unify packaging API, it is a big fail unfortunately as it ignore Debian and Ubuntu."
    author: "debianuser"
  - subject: "Re: packagekit"
    date: 2008-10-07
    body: "isn't there a apt backend??"
    author: "Beat Wolf"
  - subject: "Re: packagekit"
    date: 2008-10-07
    body: "Look PackageKit can be used for any distro, just by providing a simple backend. There is one already available working made in python, also available in Ubuntu repos, i'm a Debian user and i have to say that this backend still have some stuff that i don't like (But the good thing is you can make your own backend!).\nBut the point is that it's not packagekit authors who have to adapt it to debian, is the backend developer who has to create a compatibility backend, AND if something needs changes a good discussion on the ml, solves the issue.\nI can affirm that Richard is very flexible to ideas, and that LOTS of changes were made to make it be more useful on some distros.\n"
    author: "dantti"
  - subject: "Re: packagekit"
    date: 2008-10-09
    body: "I think the complaint here is about PackageKit not supporting bringing up a terminal (yuck!) for the broken packages which prompt for input on stdin instead of using debconf (which also sucks, package installation should be non-interactive (in Fedora, it is), but at least debconf supports a non-interactive mode, and it could also be made to ask questions in some sane GUI dialog). I don't understand why there are still some so blatantly broken packages in the Debian repositories."
    author: "Kevin Kofler"
  - subject: "Re: packagekit"
    date: 2008-10-11
    body: "ok, i agree with you in parts, but first you need to think for who packagekit was designed for. Thinking in this way you rapidly understands that a user (http://packagekit.org/pk-profiles.html) that wants cups, then he sees an ui saying that he will need to also install some packages. Ok, (next) but in this packages list is samba, which happened to have in debian a work group configuration.\nNow why do you think this user would understand that he needs to change something.. he just wants cups, so thats why Richards insists that when hit install is to install, only eulas and gpg are an exception.\nWe could have both profiles but what happens is that advanced users that need more control can use the terminal, if we add all the distro VERY specific stuff, we would end like loooooots of others programs that tried and failed..\nThink of packagekit a one way package installation, if you want rapidly install a package call KPackageKit and do it, it you need more control over the process aptitude, yum... is you friend."
    author: "dantti"
  - subject: "Re: packagekit"
    date: 2008-10-13
    body: "I'm actually completely with you on the fact that package installation should be non-interactive (and the distribution I use and contribute to, Fedora, has this as a very strictly enforced rule). I'm just explaining where the complaint about PackageKit \"not supporting Debian\" comes from."
    author: "Kevin Kofler"
  - subject: "And on a non-GNU/Linux OS?"
    date: 2008-10-08
    body: "I use FreeBSD, not GNU/Linux, so how does KPackageKit help me?  Does PackageKit have backends for FreeBSD packages and ports tree?\n\nThe same question goes for any other non-GNU/Linux OS, like OpenSolaris, for example."
    author: "jrick"
  - subject: "Re: And on a non-GNU/Linux OS?"
    date: 2008-10-08
    body: "Well as i mentioned before PackageKit is really flexible, you just need a backend, i heard Solaris team is making a backend not sure tough, but if you are a developer you can write you own. The only thing you system has to have is a DBUS implementation, iirc a DBUS port to Windows is almost done it'll be even possible to have a package manager on windows :P"
    author: "dantti"
  - subject: "Re: And on a non-GNU/Linux OS?"
    date: 2008-10-13
    body: "I've Wikipedia'ed MSI (MS Installer, Windows Installer, http://en.wikipedia.org/wiki/Windows_Installer), which is basically a script-based (read: fragile) software package manager. A program (setup.exe) updates a database with some software-specific items (what to run to uninstall the package, for example).\n\nBasically, a PackageKit back-end for MSI could be made fairly easily by treating setup.exe as a package, running msiexec /qn setup.exe to install the package, and treating Windows Update as the installation repository (Finally!! No More Of That Sucky Web Interface!!).\n"
    author: "Michael \"MSI\" Howell"
  - subject: "Re: And on a non-GNU/Linux OS?"
    date: 2008-10-14
    body: "And that won't work at all for the zillions of programs which don't use M$I. And given that the M$I SDK is proprietary and cross-compiling-unfriendly, that includes almost all Free Software (which is usually distributed through NSIS, Inno Setup or custom installers like the Cygwin Setup or the kdewin-installer)."
    author: "Kevin Kofler"
  - subject: "Re: And on a non-GNU/Linux OS?"
    date: 2008-10-19
    body: "In that case, it may be easier to (if it's not an MSI) to simply run the executable. Since PK installation is meant to be non-interactive, it's better to try not to require user interaction.\n"
    author: "Michael \"Just run it\" Howell"
  - subject: "Re: And on a non-GNU/Linux OS?"
    date: 2008-10-19
    body: "In that case, it may be easier to (if it's not an MSI) to simply run the executable. Since PK installation is meant to be non-interactive, it's better to try not to require user interaction.\n"
    author: "Michael \"Just run it\" Howell"
---
PackageKit is a system designed to make installing and updating software on your computer easier. The primary design goal is to unify all the software graphical tools used in different distributions. KPackageKit is the KDE interface for PackageKit. <a href="http://polishlinux.org/kde/kpackagekit-interview/">Polishlinux.org spoke with Adrien Bustany and Daniel Nicoletti</a> the Packagekit-Qt and KpackageKit developers, about the emerging possibilities in the process of managing software on your desktop.


<!--break-->
