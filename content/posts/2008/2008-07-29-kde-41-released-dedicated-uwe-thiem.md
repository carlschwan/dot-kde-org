---
title: "KDE 4.1 Released, Dedicated to Uwe Thiem"
date:    2008-07-29
authors:
  - "skuegler"
slug:    kde-41-released-dedicated-uwe-thiem
comments:
  - subject: "better than others(vista/osx)"
    date: 2008-07-29
    body: "let's hack on it. I miss only panel hidding from kde3X, but it is planned for 4.2\n\nLinux is VISUALY better than VISTA or OSX. "
    author: "zvonsully"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-29
    body: "That's the beauty of style, it's not limited, but it's quite clear that something must be beautiful in OS X as everyone copies the crap out of it.\n\nI'd get rid of the menu bar, but that's my NeXT preferences showing through."
    author: "Marc Driftmeyer"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "OS X looks nice, but I wouldn't say that everyone copies it."
    author: "T. J. Brumfield"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "Copy may be to big a word, but I do agree OS X sets the pace right now - and has been doing so for a while. They simply have a huge influence because they're ahead of most others, both architecturally and artistically speaking. Having some excellent ppl with a lot of freedom and a flexible architecture does that to a product ;-)\n\nThat doesn't mean MS, Gnome and KDE don't have their own unique look & feel, but saying they aren't influenced isn't right. On the other hand OS X doesn't stand entirely on its own either, seen the way they switch users? (they use the cube from Compiz for that)"
    author: "jospoortvliet"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "I'd completely disagree with you there with no offense to the software developers who have poured their heart and souls into KDE. (As a software developer myself I can appreciate the work that goes into a project this size) \n\nThe kicker/start bar/quick launch/clock widget area still needs work. The buttons on almost all the screenshots for all the apps are too big (I guess perhaps a 80px rounded button is really needed for KDE), the open/close/minimize/maximize stylings still look off perhaps due to the strange alignment. Worst of all, the fonts still don't look well chosen. They've come a long way but i'd suggest that the default font shown there still doesn't fit.\n\nNow, with all that \"bashing\", i've got to say that KDE has come a long way and i've been trying it out almost all the way. I know my complaints are almost all changeable here or there or in this .conf file but to say that the default install looks better then OS X is just crazy talk. OS X has a major advantage; it has a large crew of people that just work on its art. It's not their contribution to an open source project in their free time, it's their job. \n\nThat said, thanks to all the developers and people that made this release."
    author: "MikeTV"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "AFAIK some people did work only on KDE 4's art but (in my opinion, but not in that of many people) it did not turn out to be better than the previous defaults."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "wow, I disagree. The artwork in KDE 3.5 wasn't that bad, but Oxygen is applied much more pervasively and consistently throughout KDE 4. Whether you like the style or not, I think you must agree on that. The artists have much more influence compared to Everaldo had during 3.x - which makes sense, as there's an official team now ;-)"
    author: "jospoortvliet"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "Well to each its own, I suppose. But I too find KDE4 look depressing.\nIt may be more consistently applied, but it's consistently gray (or black... brr!). "
    author: "Luciano"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-31
    body: "I upgraded my kubuntu to KDE 4.1 yesterday and when I looked at it it was amazing.  Beautiful even.  The wonderful bright blue swirling background was the perfect highlight for Plasma and Oxygen.  I do have a slightly custom colour scheme in that the window decorations are blue instead of white, but I'm not taking credit - it simply looks absolutely incredible.\n\nWhen I get home I'll post a screenshot."
    author: "Matt"
  - subject: "True,but Oxygen style is still behind competitors"
    date: 2008-07-30
    body: "Well, to me it seems that KDE is doing a good job to implement default styles that are - technically seen - competing with the current MS Windows release. Plastik was the first KDE theme that was polished in the sense that the design of buttons fitted to lines, borders, boxes, tabs etc. But it was behind of OSX in the way that animations were limited to animated progress bars and that antialiasing of widgets not existing. Lines and 3d-borders were still RGB-(No-A) bitmaps.\n\nOxygen improves that. The philosophy of the style is different from Vista, the colors are OSX alike. The style is well thought in the sense that the Windeco, buttons, lines, boxes and tabs (and evene icons) fit well together. But still the number of animations is very limited and there are many unpolished non-antialiased widgets. \n\nTo me, the theme Skulpture is really nice. Its artistic philosophy is simple (maybe a bit old-school), but it offers at least a few smooth animations and is polished even in the sense that widgets are antialiased. \n\nJust take, for example a sunken ListView. The 3d frame is sunken and has a gray \"shadow\" on the white interior. An item of the listview is selected using blue color. In Skulpture the frame's \"shadow\" fades into blue around the selection. This is what I call \"polished\" and where I believe KDE reaches the quality of OSX/Vista. I hope the Oxygen guys can keep with Skulpture. The basic ideas of the style are awesome, though I am afraid the implementation is finished when KDE5 is already around the corner. "
    author: "Sebastian"
  - subject: "Re: True,but Oxygen style is still behind competitors"
    date: 2008-07-30
    body: "I am afraid of that too...\n\nThe problem is there aren't many developers to do so (I think there are 3 or so). Draw the widgets can be a hard task since you have to make it on C++. Nuno's mockups are incredible, and I think that is the way we all (including the developers) want KDE to look like, but \"draw\" everything in C++... :/\n\n  I would help if I know how to code, but...\n\nThere was a project named Cokoon that the main objective was to make it easier to artist draw their own theme... but I don't know what happened to that project.\n\n\n\n"
    author: "SVG Crazy"
  - subject: "Re: True,but Oxygen style is still behind competitors"
    date: 2008-07-30
    body: "Looking at some mockups at http://nuno-icons.com/images/estilo/ it seems to me that (as far as the widget style is concerned) it is nearly achieved and that's what I don't like, mainly because the lack of contrast. No problem, Plastik is fine for me (and even Plastique even though I like Plastik scrollbars better than Plastique)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: True,but Oxygen style is still behind competitors"
    date: 2008-07-30
    body: "Erm, maybe you are mixing up things here, but there are no animations in Skulpture yet. Are you talking about Bespin?\n\nRegarding shadows, Skulpture uses overlay widgets to get them, so performance suffers. I doubt KDE developers would implement them this way, but future KDE/Qt versions could offer \"native\" shadows. I think there were some ideas floating around on Trolltech Labs. The whole RGBA and shadow situation is a mess anyway. X11 and X window managers need a major redesign. Try ARGB/shaped windows with shadows... OSX and Vista are ahead in respect to that.\n\nAnyway, I think KDE developers should not put much energy into Oxygen's widget style, but instead lay a solid foundation on which theme designers can build there own styles. Creating Plasma themes is already quite easy, that's why you see so many of them popping up at KDE-Look.org, and nearly all of them of very high quality. But creating a widget style is a very tedious task.\n\nI remember someone saying the reason there are next to no third-party widget styles available for KDE 4 is that with Oxygen there is no need to find a better style. I doubt it. The real reason is that they are not easy to create. Imagine creating a widget style would be nearly as simple as creating a Plasma theme or an Emerald window decoration. KDE-Look.org would get flooded with new ideas, and encourage others to pick them up and improve upon.\n\nNuno's dream: Paint it in Inkscape and save it! He wouldn't stop drooling, if this suddenly came true..."
    author: "christoph"
  - subject: "Re: True,but Oxygen style is still behind competitors"
    date: 2008-07-30
    body: "\"Nuno's dream: Paint it in Inkscape and save it! He wouldn't stop drooling, if this suddenly came true...\"\n\nThat would be the perfect situation... We can dream, can't we?\n\nI think that was the object of Cokoon... do not know what happened... "
    author: "SVG Crazy"
  - subject: "Re: True,but Oxygen style is still behind competitors"
    date: 2008-07-31
    body: "Qt 4.x supports SVG, so theoretically you could do things in inkscape, but the problem is; you need to derive all classes and implement your own paint engine, which will do SVG rendering, and it is Hell amount of work, and it is not a job for few afternoons."
    author: "doc_ds"
  - subject: "Re: True,but Oxygen style is still behind competitors"
    date: 2008-07-31
    body: ":) yeah then the animations  :P "
    author: "nuno Pinheiro"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "That depends.  Oxygen not recognizing system colors, and presenting this overt white interface while Plasma displays an overt black interface, combined with a krunner that looked really out of place in 4.0 led me to believe there wasn't much in consistent design or application of Oxygen.  The desktop had some polish and shine here and there, but it looked rather disjointed.\n\nIt is coming more together, but thick, dark Plasma themes still don't seem to mesh with Oxygen.  Some of the thin, very transparent themes seem to compliment the white Oxygen look.\n\nThat being said, I really love Nuno Pinheiro's mockups, and the closer KDE 4 looks to his designs, the happier I will be.  Many of his early mock-ups featured quite a bit of contrast and some rather innovative takes on tabs, scrollbars, etc.  I'm not sure why the actual early Oxygen implementations were so different from the mock-ups.\n\nI'm also still hoping that someday someone will combine some of the nice effects of the Oxygen widgets with some of the wonderful elements of the KDE 3 Domino widgets.  I loved the Domino scrollbars and custom gradients especially.  Domino wasn't overtly minimal, yet it didn't scream wasted space either.  I'm glad to see Oxygen eliminate some of the excess white space, but it could still trim down a bit."
    author: "T. J. Brumfield"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-08-01
    body: "Yep, OsX is real purdy.  \nImagine how faaaaantastic it'll be as soon as it has:\n- a text editor worthy of the name\n- a usable console\n- multiple desktops\n- a file manager that lets me get some work done\n- a task bar which lets me see what I have open\n- a mouse that is usable as a 3-button x style mouse\n- stops converting my ps to pdf just so I can view them\n- has a front and back end that use the same line-ending\n- stops using hidden resource files that get lost during transfer and render the file unopenable on a mac (cool, they still work under windows!?)\netc etc\n\n\n"
    author: "A.M. Benson"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-08-01
    body: "Obvious Troll....\nNot saying OS X is perfect (jus yet...), but you're blowing issues entirely out of proportion. \n- Text Editor: Text Edit.app fully capable for most thigns, plus guess what? Vim included too..\n- Multiple Desktop? 10.5 has Spaces\n- etc\n- etc"
    author: "Johann"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-08-02
    body: "Compared to KDE, OSX is just not ready for the desktop... I mean, they are trying, but they sure have a long way to go."
    author: "hmmm"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-07-30
    body: "Hmm I thought by visually you meant it comes with sub pixel font rendering working and not stuck with everything at 96dpi, is that fixed to a usable degree? It drives me batty across my dual boot desktop and mac and pc laptops for work.\n\nas far as the visual design is there usability/eye tracking testing and what not for kde? just curious I'd love to get involved"
    author: "quik77"
  - subject: "Re: better than others(vista/osx)"
    date: 2008-08-05
    body: "Huh?\n\nPerhaps you mean that KDE is visually better than Windows Vista or the OS/X desktop (or does Apple have a better term for it?).  IAC Linux is an OS like PC-DOS and OS/370 are OSes.  Linux is not a desktop and it does not have a GUI.  IAC, you need to compare KDE to Windows -- in both cases the underlying OS is not the issue."
    author: "JRT"
  - subject: "Congrats!"
    date: 2008-07-29
    body: "Congrats and thanks to everyone who helped make this release!\n\nAlready looking forward to KDE 4.2. The improvements in 4.1 are impressive, but I think with 4.2, KDE4 will really start to shine :)"
    author: "Joergen Ramskov"
  - subject: "Re: Congrats!"
    date: 2008-07-30
    body: "Uwe has an excellent release dedicated to him!\n"
    author: "Jos"
  - subject: "Congratulations"
    date: 2008-07-29
    body: "And Thank You so much. Been using the development versions for a while now and it's already amazing. Can't wait for 4.2!"
    author: "Jens Uhlenbrock"
  - subject: "Thank you!"
    date: 2008-07-29
    body: "A big thank you to all the people involved, especially the developers!\nI use 4.1 since beta2 and really really like it. Please don't listen to all the whiners - you do a great job.\n\nKDE rocks!"
    author: "Sepp"
  - subject: "Slick Announcement page"
    date: 2008-07-29
    body: "I'd like to thank the folks who put together these announcements pages too.  This one is particulary well done I thought.\n\nCongrats all guys and gals!"
    author: "Jane"
  - subject: "Re: Slick Announcement page"
    date: 2008-07-29
    body: "Thanks Jane, encouraging words are always welcomed :)"
    author: "sebas"
  - subject: "Re: Slick Announcement page"
    date: 2008-07-30
    body: "Sebas has outdone himself once again, that's for sure :D"
    author: "jospoortvliet"
  - subject: "Re: Slick Announcement page"
    date: 2008-07-30
    body: "I'll second that.  I really appreciate that the announcement is more honest and up-front about the state of KDE and who it is suitable for.\n\nHaving explicit target release dates for the next minor and major releases is a great help too.\n\n"
    author: "Robert Knight"
  - subject: "Re: Slick Announcement page"
    date: 2008-07-31
    body: "Couldn't agree more.\n\nI really loved the screenshots with short summaries below them. It gave a nice \"feel good\" feeling. In fact, I'd suggest to mix the bullet items above more with the screenshots below.\n\nAnd the added honest notes about NVidia performance and \"is 4.1 for you\" is also a plus IMHO!"
    author: "Diederik van der Boor"
  - subject: "Congratulations :)"
    date: 2008-07-29
    body: "Many thanks for the great work, I used KDE 4.1 RC and I liked it, I am sure I will love 4.1.x even more. \n\nI have a request for KDE veterans, for new Linux/KDE in general we have little knowledge about KDE history and progress over the years. I would like to see a paper with screenshots showing the progress of KDE from version 1 beta to the amazing KDE4 platform. I think KDE project started around 10 years ago, at that time Win98 was out. So it will be great to see how things progressed."
    author: "Kais Hassan"
  - subject: "Re: Congratulations :)"
    date: 2008-07-29
    body: "http://kde.org/screenshots/\n;)"
    author: "Jonathan Thomas"
  - subject: "Re: Congratulations :)"
    date: 2008-07-29
    body: "Thanks, some of the earlier version screenshots look like they came out of an old SCI-FI movie :)"
    author: "Kais Hassan"
  - subject: "Re: Congratulations :) - history"
    date: 2008-07-29
    body: "http://kde.org/history/\nhttp://kde.org/announcements/\n\nI've also seen a \"visual changelog\" somewere.\nWith Screenschots from early 0.x -> 4.0.x \nBut at the moment I can't remember where, sorry.\nI guess somewere on the opensuse-pages related to KDE...\n\n\nPs.: from me CONGRATZ too, great release :)\nAlready using 4.x for a while ;)"
    author: "kalle"
  - subject: "Thank you !"
    date: 2008-07-29
    body: "II just want to say I love you  !!!\n"
    author: "BogDan"
  - subject: "Very nice"
    date: 2008-07-29
    body: "Been playing around with kde4.1 since beta and i must say it's one smooth experience now.\nSounds silly but KPatience really got me ;-)"
    author: "adrian"
  - subject: "Re: Very nice"
    date: 2008-07-30
    body: "Games in KDE have come a long way, but they do rock. I would like to say thank you especially to our artists who have done a great job in tailoring new suits for the KDE games."
    author: "Stefan Majewsky"
  - subject: "Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "The screenshots look wonderful. Just imagine how good KDE would be if it had the support of RedHat... It's sad that these supposedly FOSS-friendly corporation is choosing to develop an inferior product (GTK/GNOME) simply so that unscrupulous ISVs can use their LGPL'ed framework without sharing anything back. Any developer who cares about software freedom should work on KDE because it is protected from proprietary leeches, but alas few are aware of this subtle point. The KDE community should really raise awareness about this more in order to attract FOSS-conscious developers."
    author: "Bill"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "Now now, please be professional.   That sort of talk makes us look bad"
    author: "John Tapsell"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "Yeah. No need to create a flamewar. I thought Linux was all about choice.\n\nThanks for A GREAT RELEASE!  "
    author: "KIm Timothy Engh"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "Red Hat supporting GTK/Gnome is actually a good thing for KDE. Without Gnome, KDE wouldn't of had any immediate competition on the *nix platforms, as most other environments fill a different niche (more minimalist/lighter). Without the immediate competition there would have been less motivation to innovate, less opportunities to try different methods, less ability to try something big.\n\nGnome is good for KDE, just as KDE is for Gnome. Neither would be close to where they are today without the other, and theres no way to 'combine' them (the environments or the development teams) without destroying exactly what has got them this far.\n\nKDE 4.1 is already an *extremely* nice desktop environment (I've been using SuSE's snapshots for a while), but at this point I have high hopes for Gnome/GTK 3 being nearly as innovative as KDE 3 -> KDE 4 is showing to be, not because I use Gnome (or like using it) but because I know it'll help drive KDE forward."
    author: "Kit"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "On principle I'd say competition is good, yet at the same time both projects seem to be hurting for developers.  Both projects have a laundry list of features for a myriad of apps, and developers claiming that they don't have time to finish them.\n\nIs it completely necessary for developers to duplicate gedit with kwrite (or kwrite with gedit)?\n\nNeither koepete nor pidgin can finish voice and video support, yet that is one of the most highly requested features for either IM client, and has been for years and years.\n\nCompetition is great, when there is market enough for all these projects.  I remember reading that Gnome started largely because people were upset that QT \"wasn't free\" back in the day, so Gnome was created as a \"free\" desktop.  Yet today, QT is GPLed, and some might contend that Gnome is considerably less \"free\" today (integration with Mono, LGPL all over the place, etc.)\n\nOne could also contend that the moment QT became GPL, the driving need/force for Gnome vanished.\n\nOthers also insisted that Gnome is necessary because of KDE bloat and poor performance.  KDE offers vastly more features, with superior performance these days.  Gnome doesn't provide the same level of consistent design (or kpart-type usage) across it's platform.\n\nIf Gnome is largely a design philosophy of simple apps, and simple appearance, Gnome could exist today on top of QT.  Oh, and Mark Shuttleworth is saying the same things these days.\n\nA more unified underlying shared codebase of basic libs would really bring the freedesktop project together.\n\nGnome could provide a unique desktop experience that looks like Gnome (QT-Clearlooks engine) with streamlined features, and whatever design they want.  It could still be a competing DE.  Yet, it both used QT as opposed to the GTK/QT rift, we wouldn't necessarily need two of every project.  One simple editor (like kwrite or gedit) would suffice.  You wouldn't need to rush out and make a separate one for GTK or QT."
    author: "T. J. Brumfield"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "I think you forget how this stuff works. Fundamentally, people do things in open source because it's fun for them. And for me, doing KDE stuff is fun, while doing Gnome stuff won't be. Doing KHTML stuff is fun, but doing Gecko stuff won't be.... And I am sure that there are many Gnome developers who have the opposite viewpoints. Different projects have different cultures and different technical philosophies, which make them better fit for different contributors. \nChances are that if you got rid of one or the other of the projects, you'd also get rid of a huge percentage of contributors.\n\nAnd in light of this, it's also not a matter of whether it's necessary to have both gedit and kwrite (which is actually a rather mundane wrapper around katepart, or more accurately the text editor interfaces, which power quite a few things) --- it's a matter of whether there are people who want to make them and enjoy making them the best there is. And competition isn't really necessary for it, either, as if one is having fun making things, one wants to make them rock, though it can help set higher goals.\n\n\n"
    author: "SadEagle"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "Nothing to add. Thanks SadEagle :)"
    author: "Sebastian Sauer"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "I'm not saying people can't do different projects if they so desire.  That would be pretty silly to suggest.\n\nAgain look at Pidgin and Kopete.  Both are very similar apps, and neither can seem to keep up with crucial features, or finish voice and video.  Even if they still existed as two apps, but shared more libraries, and both built upon QT, I think it would make life easier for both projects.\n\nEven many Gnome/GTK devs claim they tire of how GTK themes are handled, and there seems to be discussion about making GTK themes more like QT/CSS themes.  Coupled with the Gnome/GTK devs screaming they want to abandon current APIs and start anew, I think if there was ever a time to consider unifying around QT, now would be the time.  I really do believe it would be largely beneficial to everyone."
    author: "T. J. Brumfield"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "http://derstandard.at/?url=/?id=3413801\n\nSee that as the beginning :)."
    author: "Alejandro Nova"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "I did see that interview, but thanks for the link anyway.\n\nI think his comments are pretty interesting."
    author: "T. J. Brumfield"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "> Is it completely necessary for developers to duplicate gedit with kwrite (or kwrite with gedit)?\n\nask gedit devs and they'll say yes..\nask kwrite/kate devs and they'll say.. yes.\n\n\nask mcdonalds and burgerking to merge. or ask all countries to tear down their borders and speak one language."
    author: "cies breijs"
  - subject: "Situation normal"
    date: 2008-07-30
    body: "> On principle I'd say competition is good, yet at the same time both\n> projects seem to be hurting for developers. Both projects have a\n> laundry list of features for a myriad of apps, and developers claiming\n> that they don't have time to finish them.\n\nThe more we develop, the greater our plans and ambitions become. It should not be surprising that the TODO list is alway greater than current capacity. I can't think of any piece of software except perhaps for some of the smallest utilities or libraries that don't have TODO lists which outstrip their developer capacity. This is the nature of software development.\n\nAs for KDE I don't think we are \"hurting for developers\". We have a stead and strong stream of new people joining and helping out. New SVN accounts are being made daily. I can't speak for Gnome, but I get the impression that they are in a more precarious position w.r.t. new developers.\n\nAnd concerning competition between the two major FOSS DEs. I think the competition aspect and its influence on the progress of both DEs is overrated. Most FOSS people I have met don't appear to be motivated by some drive to \"beat\" the other guy. Most people just want to create the best software they can, taking inspiration and ideas, (and help!) from many sources. Cooperation not competition, is how progress is made.\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "Actually, the LGPL requires that any changes have to be published if you redistribute your modified code. Maybe you are thinking about (one of) the BSD licence."
    author: "Erunno"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "I think he's referring to the fact the GTK+ is LGPL means that proprietry software can use that framework.\n\nHowever, kdelibs is also LGPL.  So I think that proprietry software can, in theory, purchase a Qt license and develop stuff on top of KDE.  Also, if TT/N (TrollTech: a Nokia company) ever crumbles and falls (and I sincerely hope it doesn't, simply because that would make the burden of maintaining it fall to the community), Qt will be released under a BSD license, and kdelibs+Qt will be LGPL as a whole."
    author: "Random Guy 3"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-29
    body: "Right, but that was not my point. He said that \"unscrupulous ISVs can use their LGPL'ed framework without sharing anything back\". This is only true to a certain extent as they can use LGPL code in closed source projects but can't make changes to the LGPL code without publishing the changes."
    author: "Erunno"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "Those \"sellouts\" as you're calling them are paying 2 developers (Than Ngo and Luk\u00e1&#353; Tinkl) to work on KDE packaging, who played an important part in making Fedora the first major distribution to move entirely to KDE 4 (not just as a non-default option next to KDE 3). Fedora will also be offering KDE 4.1.0 as an official update for Fedora 9, not just a backport, it is already queued for updates-testing. The Red Hat engineers are playing an important role in keeping Fedora's KDE that up to date (and this is one of the community, i.e. non-Red-Hat, Fedora KDE packagers speaking)."
    author: "Kevin Kofler"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "It's Red Hat's loss. When they start trying to create management tools for virtualisation and other things it's going to leave them short of where their competitors are, as theirs' and others' desktop efforts have been thus far."
    author: "Segedunum"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "What \"management tools for virtualization\" do you have to offer which can compare with Red Hat's virt-manager? http://virt-manager.org/"
    author: "Kevin Kofler"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-07-30
    body: "Unfortunately, they're nowhere near what VMware's and Microsoft's are (and that's being polite), and if you're running multiple operating systems those kinds of tools are things where it makes sense to be able to run them cross-platform - properly that is. I'd love to see Red Hat try and do that with the tools they're using.\n\nYou also only need to look through the graphical management tools they have with a cursory glance through RHEL, even after all these years. At best they're going to be a Unix replacement with what they have there, but they're never in a month of Sundays going to convince an administrator looking primarily for good management tools to use RHEL over Windows 2003. Who knows? Maybe they think the 'low hanging fruit' is enough? It won't be.\n\nLike I said, it's their loss and the argument still stands."
    author: "Segedunum"
  - subject: "Re: Way to Go KDE! No thanks for RedHat sellouts"
    date: 2008-08-05
    body: "Ah, the last resort of all KDE-trolls:  GNOME-bashing.  Why is it that you people just can't discuss your precious project -- even on this KDE-centric website -- without it devolving into GTK/GNOME-bashing?"
    author: "antifud"
  - subject: "User since v1.1"
    date: 2008-07-29
    body: "Amazing team making an amazing product! I have been exclusively using kde since 1.1. My appreciation goes to the entire team of software developers, graphic designers, translators etc. etc. and also the user feedback. I have seen alot of creative ideas bouncing around from every-day users, and they are sometimes impressively ingenious!\n\nCongrats to all.\n"
    author: "Karim"
  - subject: "Changelog since -RC?"
    date: 2008-07-29
    body: "I've been using 4.1-RC since its release and have read the release notes etc. but does anyone have a list of key bugfixes between -RC and -final?"
    author: "Adrian Baugh"
  - subject: "Re: Changelog since -RC?"
    date: 2008-07-29
    body: "Not really, but you can obtain a list of the changes by paging through the output of \n\nsvn log  svn+ssh://svn.kde.org/home/kde/branches/KDE/4.1"
    author: "sebas"
  - subject: "Cheers! &#9786;"
    date: 2008-07-29
    body: "congratulations!!\n\npd. don't forget the panel autohiding!"
    author: "Pocho de la Polla"
  - subject: "Well done! ( New Webpages! )"
    date: 2008-07-29
    body: "I especially like the new  \"Stable Version\" page ( Download section on the kde.org )\nThe old sucked really bad. It only mentioned KDE4 ( even as rock solid tested release .. WTF? )\nThe new explains that KDE4 is for early adoptors and that 3.5 is still around and maintained! That is the way better aproach. ( Do that again for 5.0 ;) )\n\nVery well done! Congrats&Thank you!"
    author: "Tom"
  - subject: "Re: Well done! ( New Webpages! )"
    date: 2008-07-29
    body: "That \"4.0 as rock stable\" was actually a dumb mistake (most probably by me even). While the link pointed to 3.5.9, the label said 4.0.x. So the intention was good, at least ;-)\n\nI've noticed it today while updating the webpages for 4.1.0 and fixed it immediately."
    author: "sebas"
  - subject: "Speaking of webpages..."
    date: 2008-07-31
    body: "Reading the announcement for 4.1 and the utils.kde.org announcement, it does seem a shame the 4.1 announcement talks about kscd, lokalize and krdc but none of these excellent apps seem to have online homes."
    author: "Anon"
  - subject: "Dedication"
    date: 2008-07-29
    body: "Good call on the dedication!"
    author: "Martin"
  - subject: "Thank you"
    date: 2008-07-29
    body: "for your hard work. "
    author: "kdeuser"
  - subject: "Migrate"
    date: 2008-07-29
    body: "A question:\nis there an application that helps with the migration of some data as shortcuts, bookmarks, mails, contacts,... ?"
    author: "Mig83"
  - subject: "Re: Migrate"
    date: 2008-07-29
    body: "KDE3TO4\nhttp://www.kde-apps.org/content/show.php/KDE3TO4?content=85769\n  "
    author: "anonymous"
  - subject: "Re: Migrate"
    date: 2008-07-29
    body: "Thanks a lot!"
    author: "Mig83"
  - subject: "Re: Migrate"
    date: 2008-07-30
    body: "You can try copying applications' config files from KDE3's to KDE4's .kde directory, or use KDE 3's .kde with KDE 4 (though this might cause problems). Applications, in contrast to the desktop, are mostly ports and improvements of the KDE 3 version, and not complete rewrites, so with some luck you may be able to use the old config files with them. (Application config files are normally .kde/share/config/*rc and .kde/share/apps/*/* .)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Migrate"
    date: 2008-07-30
    body: "If you use Fedora, no migration is needed, as KDE 4 uses the same .kde settings directory as KDE 3. This is also the default for vanilla KDE, but most distributions use patches or environment variable hacks to enforce use of a separate .kde4, which is why there's a need to \"migrate\" settings in the first place (actually just copy them to the new directory so KDE 4 picks them up)."
    author: "Kevin Kofler"
  - subject: "Finally comes"
    date: 2008-07-29
    body: "Congratulation KDE team!!!\nThanks you all for great work!!!"
    author: "rockmen1"
  - subject: "automoc4"
    date: 2008-07-29
    body: "Anyone able to help me with this problem?\n\n\"CMake Error: Did not find automoc4 (part of kdesupport).\"\n\nhttp://techbase.kde.org/Getting_Started/Build/KDE4 does not mention it."
    author: "tomas"
  - subject: "Re: automoc4"
    date: 2008-07-29
    body: "You need to build and install kdesupport.  I don't know why that link doesn't tell you that.  Maybe someone more knowledgeable could fix the wiki"
    author: "John Tapsell"
  - subject: "Re: automoc4"
    date: 2008-07-29
    body: "Thanks for your help but where can I find or download kdesupport?\n\nI see the mirror:\n\nkdeaccessibility-4.0.99.tar.bz2\nkdeadmin-4.0.99.tar.bz2\nkdeartwork-4.0.99.tar.bz2\nkdebase-4.0.99.tar.bz2\nkdebase-runtime-4.0.99.tar.bz2\nkdebase-workspace-4.0.99.tar.bz2\nkdebindings-4.0.99.tar.bz2\nkdeedu-4.0.99.tar.bz2\nkdegames-4.0.99.tar.bz2\nkdegraphics-4.0.99.tar.bz2\nkdelibs-4.0.99.tar.bz2\nkdemultimedia-4.0.99.tar.bz2\nkdenetwork-4.0.99.tar.bz2\nkdepim-4.0.99.tar.bz2\nkdepimlibs-4.0.99.tar.bz2\nkdeplasma-addons-4.0.99.tar.bz2\nkdesdk-4.0.99.tar.bz2\nkdetoys-4.0.99.tar.bz2\nkdeutils-4.0.99.tar.bz2\nkdewebdev-4.0.99.tar.bz2"
    author: "tomas"
  - subject: "Re: automoc4"
    date: 2008-07-29
    body: "kdesupport is not really part of KDE, it's a grouping of dependencies for KDE that are all developed in KDE svn.\n\nSo, you either need to pester your distribution to make an automoc package, or grab it from KDE svn.  I'm not sure if there's really been a release of automoc."
    author: "Random Guy 3"
  - subject: "Re: automoc4"
    date: 2008-07-29
    body: "Ah thanks.\n\nI will try to fetch it with svn.\n\nI really think that the wiki should mention this."
    author: "tomas"
  - subject: "Re: automoc4"
    date: 2008-07-29
    body: "I just realized that a guide specific to Gentoo mentions more of it\n\nhttp://techbase.kde.org/Getting_Started/Build/KDE4/Gentoo\n\nI think it should be a more distro agnostic guide because commands such as:\n\n\"The Recipe for Automoc\n\ncd \ncs\ncd kdesupport\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/kdesupport/automoc\ncd automoc\ncmakekde\"\n\nCould work on most (sourced based) distributions yet not everyone that likes source builds uses gentoo"
    author: "tomas"
  - subject: "Re: automoc4"
    date: 2008-07-29
    body: "You can also download the source from http://kde.mirrors.tds.net/pub/kde/unstable/automoc4/0.9.84/automoc4-0.9.84.tar.bz2\n\nThere is a section for building from source, but most distros have their own section because they provide some or all of their own packages."
    author: "Random Guy 3"
  - subject: "Re: automoc4"
    date: 2008-08-17
    body: "These commands are not Gentoo specific.\nThey come from a script to build kde4 from sources.\nLook at http://techbase.kde.org/Getting_Started/Increased_Productivity_in_KDE4_with_Scripts#Environment_Variables_and_other_helpful_functions\nIt 's mentioned as \"this special .bashrc\" "
    author: "Hein"
  - subject: "I have a Dream"
    date: 2008-07-29
    body: "I'm using opensuse's KDE 4.1 RC packages since last week and they are great. Looking into the future, this is what I would like to see for 4.2 (disclaimer: I know some of this items are already in the road map, but I'm not sure about all of them, maybe some developer can tell me which ones are planed?)\n\n-An Akonadi-based KDE-PIM\n\n-A Decibel based Kopete\n\n-Being able to rename a picture in gwenview by just hitting F2\n\n-Digikam with kick ass nepomuk integration\n\n-Amarok 2\n\n-Koffice 2\n\n-A kDE 4 version of Kaffeine\n\n-A completely functional folder view containment\n\n-Google widgets in plasma\n\n-Windows Vista widgets in plasma\n\n-More work in the ZUI\n\n-Some kind of animation when moving widgets in the taskbar\n\n-Autohide for the taskbar\n\n-Correction of the rendering problems in the tray icons\n\n-Something new and exciting ;)\n\nAnd that's it :) Congratulations to all KDE developers for this great release, keep up the good work."
    author: "Raul"
  - subject: "Re: I have a Dream"
    date: 2008-07-29
    body: "I'm no developer, yet I read the dot and planet regularly. Here's what I think is planned:\n\n-An Akonadi-based KDE-PIM\n Afair that is planned, though I'm not sure wether _all_ PIM applications will leverage Akonadi in KDE 4.2.\n \n -Amarok 2\n  The alphas just started to roll out. Imo a stable release could be done in half a year.\n \n -Koffice 2\n  Until 4.2 a stable Koffice 2.0 could be released. Half a year for just a few more alphas (we are already at #8 or something) then some betas + RCs. Should work. I'm confident here.\n \n -A completely functional folder view containment\n  yep, is planned.\n \n -Google widgets in plasma\n  afair it's already working in trunk, I saw screenshots on the planet.\n  \n -Some kind of animation when moving widgets in the taskbar\n If I'm not mistaken someone on the planet (i.e. one of the developers) mentioned that this bugs him as well and he wanted to fix it.\n\n -Autohide for the taskbar\n  I'm totally sure that this will be included, since it's right now one of the most mentioned complain in regard to KDE 4.1"
    author: "Milian Wolff"
  - subject: "Re: I have a Dream"
    date: 2008-07-29
    body: "> -Amarok 2\n> The alphas just started to roll out. Imo a stable release could be done in half a year.\n\nActually sooner than that, if our plan works out. Stay tuned :)"
    author: "Mark Kretschmann"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "\"Actually sooner than that, if our plan works out. Stay tuned :)\"\n\nAhhh! You're just teasing me now. I've been using the amarok alphas for the last month and this one program is equally as exciting to me as almost all of KDE4. I trust you guys to make it amazing and I don't (terribly) mind waiting for it, but statements like this make it really hard.\n\nCan't wait for the (next alpha or beta or release candidate or) final release. Thanks for making the best audio player period."
    author: "Aaron"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "i'll just comment on the plasma ones:\n\n> -A completely functional folder view containment\n\nyou'll have to be more specific for me to answer this one =)\n\n> -Google widgets in plasma\n\nalready working; will be in trunk soon\n\n> -Windows Vista widgets in plasma\n\nwould require someone stepping up to write support for them. i'm willing to help with guidance and question answering, though =)\n\n> -More work in the ZUI\n\nplanned for 4.2, yes.\n\n> -Some kind of animation when moving widgets in the taskbar\n\ndo you mean the taskbar itself or the whole panel? and right now it does follow the mouse as you move ..... what kind of animation are you thinking of in specific?\n\n> -Autohide for the taskbar\n\nwill be in 4.2, yes.\n\n \n> -Correction of the rendering problems in the tray icons\n\nlikely to not be in 4.2. to do this right requires either fixing GTK+ (whose developers have said they won't fix the crash in their toolkit when a widget is XEmbed'd into an app with a different colourmap; Qt works properly, so it is possible.. *shrug*) or changing the systray spec completely.\n\nthankfully there are other reasons to change the systray spec completely (as in \"the current one is a design that may have made sense 10 years ago but ceased making any sense a few years back\"), and that's something i've got on my hit list.\n\nhowever, i'm only thinking about getting a spec together by the time 4.2 is out, i'm not delusional in thinking i can also get an implementation of it and in wide-spread use before 4.3."
    author: "Aaron Seigo"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: ">> -A completely functional folder view containment\n> you'll have to be more specific for me to answer this one =)\n\nI cannot speak for the OP, of course; but I think what most people are missing are that\n\n1. folder view can not be used as containment\n2. folder view does AFAIK not remember the positions when you dragged items to different positions inside the applet\n\n1. is fixed in trunk as all readers of the commit digest know; and I hope that 2. will also make it to 4.2."
    author: "Stefan Majewsky"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "I'd add a wish for the folder view.....\n\nAdapt it to allow just the folder name be displayed so that when mousing over it temporarily expands to show the files or when clicking it permanently expands to show the files.  the size it expands to would be the \"previous size\".\n\n\nGreat work guys and gals.\n"
    author: "Ian"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "> 1. is fixed in trunk\n\nthat was actually fixed in trunk quite a while ago, long ago enough that it is even in 4.1.0."
    author: "Aaron Seigo"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "Regarding systray, what are the technical reasons that it works perfectly in 3.5 but somehow requires a different approach now? Does Qt4 work differently? And thanks for your work :) When autohide is in, I will rm -rf /opt/kde3"
    author: "christoph"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "Afaik we don't used alpha-channels in KDE3. Beside that thing there are multiple other technical and design limitations, see there the matching bugreports in bugs.kde.org which also provide future links and informations, why the systray-logic could really need a rework. Also it seems there is already work going on to improve the current (4.1) systray - we will see :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "a) argb visual (where the 'a' is for alpha)\n\nb) allowing widgets on the desktop, panel, dashboard\n\nc) multiple widgets of the same type allowed\n\nd) rendering directly to a canvas versus embedding lots of widgets\n\nthere is no reason a systray system needs to fail due to the above, but the current one falls flat on all of them. =/"
    author: "Aaron Seigo"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "One little annoying bug I've noticed is that some apps appear out of the systray while plasma loads, then just go to it after a while. A bit confusing for users who do not know it is a bug."
    author: "Iuri Fiedoruk"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "This is actual a bugfix to prevent some other more annoying bugs. The systray-specs and implementations using it do a lot to make sure that there are tons of problems and other weird things the systray needs to special case.\n"
    author: "Sebastian Sauer"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "Is there a way you could make the systray icons not take focus when they appear? It's really #$%* annoying, especially when you're trying to type in passwords and such. ;-)"
    author: "Jonathan Thomas"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "May I suggest to create a report for this one to prevent to have it lost within this thread? And yes, it's really #$%* annoying :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: I have a Dream"
    date: 2008-07-31
    body: "Sure.\n/me should have known better."
    author: "Jonathan Thomas"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "I want to add this on the wish-list:\n- optimizations\n\nThe reason is that KDE4 is way more slower on my eeePC 701 than KDE3. For now running KDE3 is OK, but soon I'll start missing a lot of new features, and it is sad if KDE cannot run well on this machine, because it is not a bad one in hardware terms. Software is getting eager for more and more resources, it's sad for non-rich people :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "Please be more specific which things are slow for you.\nThere is a known graphics problem. What else feels slow for you ?\n\nAlex\n"
    author: "Alex"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "I did not had enabled special graphic effects, so that's probally not the case.\nEverything seems slow, you know, opening dolhin takes a good while, after open navigating to folrders seems a bit fat... It does not seem anything in particular, but think about running vista on a duron machine with 512 ram and you'll get the picture :)\n\nSure I plan to add more RAM in the eee, but this does not mean optimizations cannot be done in KDE code already, and the 3.5 series is an extraordinary example that focusing on this can give great results :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "are you sure it's a memory consumption issue?"
    author: "Aaron Seigo"
  - subject: "Re: I have a Dream"
    date: 2008-07-31
    body: "No :)\nYou know, it is a bit hard to find out exactaly what is causing the slowness, but I'm running (when I have time) a series of tests to try improving things, so I can document it for other users on the ubuntu-eee.com page.\n\nFirst thing I did was to disable neopomuk semantic thing I hate [personal taste here, nothing to complain] :) It helped a lot.\nThen using top, noticed that all the 512 MB of RAM where being used, and little of it was desk cache, so KDE4 on ubuntu uses a lot of RAM and this is not a good thing for speed, but moved forward to next test.\n\nAs the machine is slow/small, desktop effects where disabled by default, so it's not the culprit. Again, move ahead.\n\nOpened a Dolphin, once open, memory does not matter that much, so it should behavre more or less well. No, it does not! When I simply move the mouse from a folder to other (the nice blue rectangle on the folder changes to the other). WOW, SLOW like hell! I'm talking about 3~4 seconds to change the focus from one folder to another. Maximize/restore window is slow also.\n\nAnd then it was sleep time :)\nToday I plan on using other theme than oxygen to see how things goes.\nIf any KDE developer want access to my eeePC to test, I'll gladly set up ssh and vnc for it or run more tests, just mail me (protomank at gmail), I'm here to help [we are all getting a bit more mature, aren't we?] ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: I have a Dream"
    date: 2008-07-31
    body: "I've found that KDE4 uses *massively* more X-server memory than KDE3, primarily due to the double-buffering of widgets performed by default in Qt4.  With tabbed applications like e.g. Konsole, you are quite often looking at several MB *per tab* of pixmap usage that wasn't used in KDE3 and, since only one tab can be visible, this amounts to simply wasting 10's of MBs of memory for no improvement whatsoever.  It's a pretty serious problem, IMO, and one that very few KDE devs seem to appreciate as far as I can see."
    author: "Anon"
  - subject: "Re: I have a Dream"
    date: 2008-07-31
    body: "You shouldn't generalize so easily. My eeePC 701 runs 4.1 (even trunk) quite fine. Even the desktop effects work, but due to slow scrolling in Konqueror I usually disable them. Overall memory consumption after login is ~130 MiB and rarely greater than 400 MiB (paging is disabled). I should  add that it is a Gentoo build with a heavily patched 2.6.25 kernel and an almost pure Qt4/KDE4 system (e.g. there are no Qt3 libs).         "
    author: "Ego"
  - subject: "Re: I have a Dream"
    date: 2008-07-31
    body: "In my case I'm using an ubuntu-eee with custom kernel, but that build is not *that* optimized, that's true."
    author: "Iuri Fiedoruk"
  - subject: "Re: I have a Dream"
    date: 2008-07-30
    body: "\"-Digikam with kick ass nepomuk integration\"\n\ndigiKam does not have Nepomuk integration, it had many years now own search/metadata functions and mayby in future versions we get Nepomuk too. So currently it use own kind and nepomuk does not exist."
    author: "Fri13"
  - subject: "Awesome work!"
    date: 2008-07-29
    body: "I've been using KDE4 on and off since 4.0a, and the rate at which it is improving up is nothing short of astounding.  I knew had potential back when it barely worked, and it's really showing that potential now.  Way to go, KDE devs!\nRelease day being my birthday, I'll count it as a present - thanks :D"
    author: "Jeffery"
  - subject: "Re: Awesome work!"
    date: 2008-07-29
    body: "Many happy returns of the day!"
    author: "boudewijn rempt"
  - subject: "Re: Awesome work!"
    date: 2008-07-30
    body: "Happy Birthday!!!  And KDE4.1 is an awesome birthday present.... On my birthday, Windows 95 was released in Australia (Aug 24) :(\nThankyou to all the KDE team, and to all the KDE users who have made this such a great desktop.  \n\n"
    author: "Judd Baileys"
  - subject: "KDE4.1"
    date: 2008-07-29
    body: "What a great work!!!!!!!\nCongratulations to all responsible guys !!! "
    author: "TomiF"
  - subject: "Alt+F2"
    date: 2008-07-29
    body: "When I try to run a command and press ALT+F2, the dialog appears minimized on the taskbar and there is no way I can maximize/move/resize it in order to see it. This started to happen as soon as I activated the composite option in xorg.conf.\n\nopensuse factory packages / intel graphic card."
    author: "Markus Wichser"
  - subject: "Re: Alt+F2"
    date: 2008-07-30
    body: "I had exactly the same problem. When compiled manually everything works fine. I guess the problem comes from an opensuse patch. In general the manually compiled version works (for me) incredibly better than the opensuse packages. I guess this will get better over time."
    author: "Med"
  - subject: "Re: Alt+F2"
    date: 2008-07-30
    body: "Are you sure you need to dig about in the xorg.conf file?  At least in the opensuse11 series I have not had to touch xorg as it autodetects 3d abilities and just enable wobbly windows etc through the 'configure desktop' tool.  I note there is a specific problem when I enable the blur option in kwin effects but it's just eyecandy so I don't miss it.\n\n[NB, To back out of an effect which has messed up your desktop just use ctrl-alt-F4 and login as the same user and disable it in the ~/.kde4/share/config/kwinrc file.  Then switch back to your crashed session ctrl-alt-F7 and logout of it by pressing ctrl-alt-backspace twice quickly, then log back in]"
    author: "Jean"
  - subject: "Re: Alt+F2"
    date: 2008-07-30
    body: "By \"factory\", do you mean the openSUSE Factory repos (i.e. what will become 11.1) or the KDE:KDE4:FACTORY:Desktop repos for openSUSE 11.0 or 10.3? I'm using the latter for openSUSE 11.0 and didn't notice such issues."
    author: "Stefan Majewsky"
  - subject: "Re: Alt+F2"
    date: 2008-07-30
    body: "i can confirm the krunner problem with the kde 4.1 packages from KDE:KDE4:FACTORY:Desktop repos for opensuse 11, and i am using the nvidia proprietary driver. also, the device-list does not show up when i click on the icon in the taskbar, and the calendar does not appear when i do a click on the clock. \n\nwhen switching off the kwin-3d-effects, everything works fine!"
    author: "taerde"
  - subject: "Thanks from me too..."
    date: 2008-07-29
    body: "Thanks to all the kde developers out there; many many congratulations on this release. I'm using 4.1 rc and already loving it... looking forward to 4.1 final..."
    author: "Yogesh M"
  - subject: "Very cool"
    date: 2008-07-29
    body: "KDE 4.1 really rocks.. thanks to all developers who made this possible :D"
    author: "Alex Medina"
  - subject: "Another thanks from a KDE4 user!"
    date: 2008-07-29
    body: "Thanks a lot for 4.1! Been running the dev version for a while; its a giant improvement from 4.0 and will only continue to get better!"
    author: "thothonegan"
  - subject: "A Big Thank You"
    date: 2008-07-29
    body: "I'm just here to say: THANK YOU!\nKDE 4.1 is a great release, a big thank you for every person which makes this possible.\nKeep on rocking :)"
    author: "Lucianolev"
  - subject: "TODO items on 4.1_Feature_Plan page"
    date: 2008-07-29
    body: "How come that there are still TODO and IN PROGRESS items on\nhttp://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan\nWhy don't you move them to 4.2 page or remove completely?"
    author: "vf"
  - subject: "Re: TODO items on 4.1_Feature_Plan page"
    date: 2008-07-29
    body: "Lots of these are functional in 4.1 already, just don't have the full extent of functionality.\n"
    author: "SadEagle"
  - subject: "Re: TODO items on 4.1_Feature_Plan page"
    date: 2008-07-30
    body: "The dev's probably didn't have time to update that page yet..."
    author: "jospoortvliet"
  - subject: "Congratulations... But dolphin still crash"
    date: 2008-07-29
    body: "Congratulations to all the contributers. KDE4 is really full of inovations, and a great desktop.\nI'm using 4.1 since RC1 and every day it's a pleasure.\nOnly one thing that is not fixed in stable version. In dolphin, when cursor is moved over an avi file, dolphin still crash !!! Any solution to fix the porblem ?\nThanks."
    author: "Christophe Durandeau"
  - subject: "Re: Congratulations... But dolphin still crash"
    date: 2008-07-29
    body: "The trunk version of strigi had that fixed ( maybe 2/3 weeks ago), although distros ship official strigi. My suggestion is update to a more recent one because I think they had that fixed in the last version."
    author: "Annonymous Chicken"
  - subject: "Re: Congratulations... But dolphin still crash"
    date: 2008-07-29
    body: "Forgot to add, if you don't have a new version available or it doesn't fix the problem, you can just disable the Information panel, and the crashes stop."
    author: "Annonymous Chicken"
  - subject: "Re: Congratulations... But dolphin still crash"
    date: 2008-07-30
    body: "Is 0.5.11 recent enough? Because that's the most recent release I'm aware of, and it's being pushed to Fedora 8 and 9 as an update."
    author: "Kevin Kofler"
  - subject: "Re: Congratulations... But dolphin still crash"
    date: 2008-07-30
    body: "According to bugzilla, seems that one is enough : http://bugs.kde.org/show_bug.cgi?id=164296"
    author: "Annonymous Chicken"
  - subject: "Great"
    date: 2008-07-29
    body: "Still have some problems here and there, but the pace of improvements in KDE4 series is incredible. If it keeps this way 4.2 will surpass 3.5 series and 4.3 will simply be the turn point of free desktop."
    author: "Iuri Fiedoruk"
  - subject: "Donating"
    date: 2008-07-29
    body: "As promised I'll add a little money that I can spare to KDE. Thanks for a great release (that I'll have to check out soon).\n\nI'll give 10\u0080. Anyone care to match it?"
    author: "Oscar"
  - subject: "Re: Donating"
    date: 2008-08-01
    body: "Thanks to the Devs!\n\nKeep up the good work!\n\nKUDOS!\n\n\"Matched\" and Increased with $20 USD (~12.85 EUR)\nAnother Match?"
    author: "Tim Carr"
  - subject: "Thank you!"
    date: 2008-07-29
    body: "thank you very much guys! hope this time people will appreciate the work done by the KDE team! keep it up this way!"
    author: "killer1987"
  - subject: "Astonishing progress since 4.x!"
    date: 2008-07-29
    body: "Really. A lot of rough edges have disappeared and a lot of new features have been implemented!\n\nCongratulations!! \n\nAnd \n\nThank You!!\n\n"
    author: "David"
  - subject: "Congrats"
    date: 2008-07-29
    body: "Awesome release, keep on rockin' in the free world!"
    author: "Sascha Peilcke"
  - subject: "Intel 965GM - X3xxx"
    date: 2008-07-29
    body: "What happens with this graphic card?\n\nI can't manage to make the kwin effects work although direct rendering is fine (glxgears worls perfect). Always that black screen. In the kde bug page is tagged as resolved, but, i'm know i'm not the only with this problem.\n\nThat graphic card is also tagged as \"buggy\" in the compiz page.\n\nI bought this card because the 3D drivers were open, and now this :-(\n\nWhere is the bug?\n\nAbout 4.1.\n\nFor me is nearly perfect. I miss k3b, amarok, kmldonkey and kaffeine kde4 versions, and, perhaps the most important: something to control the laptop power (powerdevil is perfect, simple and exactly what i need).\n\nBy the way, i'll spend a few minutes to write 5-6 bugs or requests, once per each program. I've realized that when a software is configured, the menubar (archive, edit...) is not needed. This way, with CTRL+M, i hide that menu. This works with dolphin, konqueror, genview, kmix, kget, konsole.. but not with others: akregator, juk, kmail... as part of KDE, i think that that option would be present.\n\nBye! "
    author: "Me"
  - subject: "Re: Intel 965GM - X3xxx"
    date: 2008-07-30
    body: "KDE4-version of KMLDonkey is available since 4.0.0 times. See also http://extragear.kde.org/apps/kmldonkey/\n"
    author: "Sebastian Sauer"
  - subject: "Re: Intel 965GM - X3xxx"
    date: 2008-07-30
    body: "I have a similar problem. Since some SVN days before the desktop effects with intel 965gm are very very slow. :((\n"
    author: "Tom"
  - subject: "Re: Intel 965GM - X3xxx"
    date: 2008-07-30
    body: "Me too :-( But it's all right with the KDE 3.x.x on OpenSuse 11."
    author: "Milan"
  - subject: "Re: Intel 965GM - X3xxx"
    date: 2008-07-30
    body: "the same here.. slow scrolling in konqueror or other apps."
    author: "Stephan"
  - subject: "Re: Intel 965GM - X3xxx"
    date: 2008-07-30
    body: "The same here with NVIDIA. Now I know what users where disappointed with: Using glxgears the frame rate dropped from 8600 to 280 frames per 5 seconds while updating from KDE 4.0.4 to 4.1.0. Wow! For the next time I need to downgrade and wait for better NVIDIA drivers. Very sad - 4.1 looks very promising aside from that. "
    author: "Sebastian"
  - subject: "Re: Intel 965GM - X3xxx"
    date: 2008-07-30
    body: "No problem here on my Dell m1330.  The graphics chip was originally blacklisted because it apparently caused compiz to crash sometimes. I removed it from the blacklist (google it or go to ubuntuforums.org) and I'm not really seeing any problems. KDE 4.1 with all the wizbang effects work great!"
    author: "J"
  - subject: "Artwork"
    date: 2008-07-29
    body: "Could anybody tell me where one can get all these promotion images used in this announcement? http://www.kde.org/announcements/4.1/"
    author: "Tobias"
  - subject: "Re: Artwork"
    date: 2008-07-29
    body: "Take a look at Wade Jolson blog: \nhttp://wadejolson.wordpress.com/\n\nor his Picasa folder:\nhttp://picasaweb.google.com/wadejolson\n\nThey are really awesome!!!"
    author: "cheko"
  - subject: "Re: Artwork"
    date: 2008-07-29
    body: "http://picasaweb.google.com/wadejolson/BeFreeKDE"
    author: "Jens Uhlenbrock"
  - subject: "Adopt KDE3 apps?"
    date: 2008-07-29
    body: "There are a LOT of KDE3 apps that are not being ported or never will.\nHere is one idea, what about if we set up a page with some usefull apps from KDE3 that are not actively developed, with a voting system, so anyone can look and see most wanted apps, and maybe adopt them to port to KDE4?\n\nI can create the site, if anyone is interested."
    author: "Iuri Fiedoruk"
  - subject: "Re: Adopt KDE3 apps?"
    date: 2008-07-29
    body: "http://techbase.kde.org/Schedules/KDE4/Application_Porting_Status"
    author: "christoph"
  - subject: "Re: Adopt KDE3 apps?"
    date: 2008-07-29
    body: "Sounds like a good idea. You should link here, of course: http://techbase.kde.org/Development/Tutorials/KDE4_Porting_Guide.\n\nWhat would be great is if you were able to build a community, that could collectively flesh out a \"for dummies\" version of the porting guide and also help people along with porting apps."
    author: "Martin"
  - subject: "Re: Adopt KDE3 apps?"
    date: 2008-07-29
    body: "And those fancy screenshots with multiple screenies next to each other, with a bit of a reflection that is being used now? I know I have seen a blog or something about that some time ago, but searching planety did not turn up anything usefull. Screenshots are just too abundant..."
    author: "Andre"
  - subject: "Re: Adopt KDE3 apps?"
    date: 2008-07-29
    body: "It's Ariya's small app \"screenie\", a small Qt application. I've hacked a bit on it to make it fit my purpose and workflow better. Find more info at:\n\nhttp://ariya.blogspot.com/2008/06/creating-fancy-screenshots-with.html"
    author: "sebas"
  - subject: "Re: Adopt KDE3 apps?"
    date: 2008-07-30
    body: "Thanks! That was what I was looking for indeed :-)"
    author: "Andr\u00e9"
  - subject: "Re: Adopt KDE3 apps?"
    date: 2008-07-30
    body: "All right, I'm starting it. Will try to be as more generic as possible, so we can use the system for anything. \nSurely it will be an open source project hosted at sourceforge ;)\n\nI was missing creating something fun like that in PHP/SQL for a while :D"
    author: "Iuri Fiedoruk"
  - subject: "Great job!!"
    date: 2008-07-29
    body: "I've been running kde4 from svn trunk for a while, and it just keeps getting better and better. This release truly marks kde's position as the leading desktop out there!\n\nA big round of applause to everybody involved and thank you for making my desktop that much better :-)"
    author: "Trond"
  - subject: "Awesome"
    date: 2008-07-29
    body: "Simple awesome!\n\nBeautiful, gorgeous!\n\nCongrats to all involved. \n\nThank you guys!"
    author: "Edney Matias"
  - subject: "KDE4 rocks!"
    date: 2008-07-29
    body: "Thanks for your hard work KDE developers, you rock our worlds =D\n\nKDE4 is better than ever, it's beautiful, functional, and it rocks... IT'S THE BEST DE EVER =D"
    author: "Diego Viola"
  - subject: "No fair"
    date: 2008-07-29
    body: "Hello,\n\nRedhat was using Gnome because initially it was not possible to distribute KDE binaries due to the license conflict of Qt. Others like Mandrake and Suse cared a lot less about the license issues, but e.g. Debian did too, purely for legal reasons.\n\nAt the time KDE was getting an acceptable license, Gnome already was sufficiently developed and indeed always has had and will have distinct differences to KDE. \n\nNot the least of it being that GTK 2.0 has binary compatibility from 2.0 release until today, with no breakage allowed ever. Only now discussion of GTK 3.0 is under way which would do that, but it's not yet accepted. That is a feature that ISVs appreciate very much so, very old binaries can still run unchanged on modern GTK.\n\nOn the other side, Qt has since 2.0 changed binary compatability several times. Due to being a C++ library, the ABI has changed even in minor releases, and has been affected when e.g. the C++ ABI in gcc was changed. That's nothing I would blaim Trolltech about, for Free Software it's OK to recompile. But these major migrations have caused the constant need to keep your applications up to date.\n\nI am willing to bet that a LGPL licensed Qt would not be more popular with Redhat or ISVs, and it would be lacking binary ABI stability. Where it's not a matter of trivial recompile (e.g. Qt3->Qt4 needs at least build changes), Qt is out of the question for some things.\n\nSo, what's good about Gnome? It's a nice ABI stable platform that Redhat, Ubuntu, etc. can use. And good about KDE is that it's a rapidly developed API stable platform. In fact, the mere existence of Gnome takes away the pain from KDE to comply with needs that Gnome fulfils.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: No fair"
    date: 2008-07-29
    body: "> Due to being a C++ library, the ABI has changed even in minor releases, and has been affected when e.g. the C++ ABI in gcc was changed.\n\nNo, this is wrong. Qt never breaks binary compatibility in minor releases and neither does KDE."
    author: "Andreas"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "Hello,\n\nwell yeah you are correct, sorry for the mistake. The point was that the ABI and even API is supported longer.\n\nThat benefit of longer support sure has a price that I personally wouldn't want each Free Software desktop to pay.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "Qt3 was released 2001 even before GTK 2.0. So, no idea where you ddi get your information from but they are probably incomplete ;)\n"
    author: "Sebastian Sauer"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "And to add to that, KDE 3.0 was released on 3 April 2002 while GTK 2.0 was released on 20 November 2002 (dates taken from Wikipedia), it's kinda surprising that the KDE 3.0 series was started so long ago."
    author: "Kit"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "Wow, six years of KDE3. Well served, I would say. At this pace, KDE5 should be out in 2014. So when they say that 4.0 was only the start, they were not joking."
    author: "Haakon Nilsen"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "No, I think they were not. It is also quite enlightening to look back at some old screenshots (still available at the KDE website) of older KDE versions from the 3 series. If you realize how the 3 series developed over the years and where the 4 series are now, you can only conclude that KDE 4 will rock the socks off the desktop as we know it."
    author: "Andr\u00e9"
  - subject: "Re: No fair"
    date: 2008-07-31
    body: "Hello,\n\noh wow, indeed. You know, I witnesses KDE 2.1 -> 2.2 -> 3.0 and it feels like yesterday. Actually I thought GTK 2.0 was much older.\n\nWell, yeah but using C++ has lost the ABI advantage big time in the past. In 2004 gcc 3.4 changed the ABI. In 2005 it did again, but only for minority platforms. And I think it did in 2003 as well. So in practice, old binary programs didn't run on the newest flavor of anything after a few years.\n\nWith the stable ABI and LSB desktop in place, it's safe to say though, that the Qt4 ABI will achieve a much higher age.\n\nI was just trying to say that Redhat was in its inital choice not primarily driven by license issues, but a better situation of the C library back then.\n\nThat's what I think is unfair.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "> I am willing to bet that a LGPL licensed Qt would not be more popular\n\nMaybe so... see also:\nhttp://www.j5live.com/2008/07/16/flames-welcome-is-a-qt-gnome-desirable/"
    author: "Rex Dieter"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "> \"Not the least of it being that GTK 2.0 has binary compatibility from 2.0 release until today\"\n\n2.x is compatible with 2.x? Stop the presses! :-)\n\n> \"Due to being a C++ library, the ABI has changed even in minor releases\"\n\nNot true, not true at all. Binaries compiled against Qt 4.0.0 run just fine with Qt 4.4.0 libraries. I think you are referring to the frequent ABI changes in GNU G++. That is a different topic, and it is a real problem. But it also affects gtkmm, sigc++, and other GNOME libraries."
    author: "David Johnson"
  - subject: "Re: No fair"
    date: 2008-07-31
    body: "Hello,\n\nyes indeed: The frequent changes are what made me think of Qt as good library for maintained software, but really bad for receiving a binary from a random guy that thinks it should run on our machines too.\n\nAnd Qt4 is by far not as old as GTK 2.0 and it would appear to become much older even. And Qt3 is unsupported now. I wasn't aware that Qt3 was intended to be a ABI stable platform, check e.g. how many people shiped statically linked binaries not so long ago. As I said before, with LSB desktop and stable C++ ABI that's a welcome change.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: No fair"
    date: 2008-07-30
    body: "\"Redhat was using Gnome because initially it was not possible to distribute KDE binaries due to the license conflict of Qt.\"\n\nNo one had any problem distributing anything.\n\n\"Not the least of it being that GTK 2.0 has binary compatibility from 2.0 release until today, with no breakage allowed ever.\"\n\nThat's funny, because Qt 3.x and KDE 3 were out before GTK 2.x and Gnome 2 and they've kept binary compatible all that time.\n\n\"That is a feature that ISVs appreciate very much so\"\n\nWhat ISVs would they be exactly? Yes, ISVs appreciate it which is why Qt has been ABI and API stable itself, but ISVs appreciate good development tools even better ;-).\n\n\"I am willing to bet that a LGPL licensed Qt would not be more popular with Redhat or ISVs\"\n\nNo it wouldn't, and it certainly wouldn't with ISVs as they wouldn't see the pace of improvement they are now.\n\n\"So, what's good about Gnome? It's a nice ABI stable platform that Redhat, Ubuntu, etc. can use.\"\n\nThat's about all it offers unfortunately.\n\n"
    author: "Segedunum"
  - subject: "Re: No fair"
    date: 2008-07-31
    body: "Hello,\n\nof course there were legal concerns:\n\nhttp://developer.kde.org/documentation/books/kde-2.0-development/ch19lev1sec2.html\n\nThere are 2 distributions that really try to stay pure of uncertainty in legal matters. Redhat and Debian did both not include KDE before these concerns were addressed.\n\nFor Redhat (and Debian) at the time the issue was clear. A non-Free vs. a Free desktop was a clear choice and their investment was geared to make the Free one match the non-Free one. \n\nOnce both were Free the issue was of course moot, but investments were done already. \n\nAlso note that nobody is (should be) claiming that the investment that Redhat puts into Gnome today doesn't benefit KDE, because:\n\na) Gnome helped produce a couple of interesting libraries that KDE uses. Some of this is under the Free Desktop umbrella. Its use benefits KDE absolutely.\nb) Gnome helped convert users away from the Windows desktop, from where it was a smaller distance, but certainly much easier to interact. If you ever got an ODF file that you open in KWord from a Gnome user, you know what I mean. It could have been a MS Word document too.\nc) Gnome tries out several ideas, project or software ideas. Some of them worked, some not. KDE certainly benefited in its own decisions from other guys that do a similar job, but have other solutions and success rates. If only to avoid a trap here or there.\n\nI personally gave up the Windows desktop in 2000 (was on Linux server before) because Gnome was supposed to be better scripted in Perl, something KDE had a much harder time with (C++ bindings were somehow rare for many years) then. I liked it, and still remember e.g. back then you would install with \"lynx -source go-gnome.org | sh\" to get a graphical installer of \"Helix Gnome\".\n\nThe only thing bad about Gnome is that those how enjoy to put other people down, use it as a target. Without contribution to anything, it's quite easy to disrespect others. Like soccer fans that could hardly hit the ball, but would talk down to other soccer club fans. Actually the analogy holds for another thing: Who do you play with, if everybody joins your club?\n\nI invite everybody to respect even those that are second best. It's still quite good place to be.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Other Platforms?"
    date: 2008-07-30
    body: "I have been compiling and recompiling the latest versions of KDE4 on my FreeBSD system the last week(s) and I must say that the desktop overall is really great.\nUnfortunately FreeBSD-support is really lacking behind.\n\nI somehow have the feeling that there is more people working on KDE for Windows than on KDE for BSD, which is a rather sad thing for a free Desktop.\n\nIn general am I looking very forward to KDE4 as a Desktop though and think that many groundbreaking work in the area of Desktop computing has been achieved."
    author: "Hannes Hauswedell"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "If the desktop is great if you just compile it, what should KDE developers support on it? KDE on Windows needs more people because it is more different from a Linux system than a BSD, and is built on an other Qt version, without X."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "How much you are paid by Microsoft?"
    author: "Anon"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "What kind of weird response is this? It simply doesn't make any sense at all. If you want to start a flame war, please be a man and at least don't post anonymously. "
    author: "Andr\u00e9"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "I don't like MS and Windows at all but I think it's good that KDE libs are ported to Windows because then developers who want to develop cross-platform applications can use the KDE libraries. Of course FreeBSD port is also important but it's probably much less a deal than the porting to Windows because a FreeBSD environment is much more similar to a Linux environment."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "I fully agree with Hannes' point. Indeed its very sad to see instead of getting the KDE working on BSDs, these developers sweat day and night to get it working on Microsoft Windows and Apple which they already have very well established, very well developed desktop environments fully supported by those mega companies.\n\nOnce the KDE is also managed to get fully run on Windows and Mac OSX, people have no reason at all to switch to BSDs or Linux, not even to know such things exists, because the Windows comes free with your hardware, all the drivers you need available for Windows, KDE is also run very well, best of all you do not need to donate money for BSD and Linux efforts anymore, Microsoft is so rich they can even give you money for using Windows.\n\nWhat Microsoft and Apple need is open source bloody fools to sweat day and night to develop software for them FREEEEEEEEEEEEEEEEEEEEEEEEEEEEE.\n\n"
    author: "Anon"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "Only KDE libs and some applications are ported to Windows, and not the desktop environment. I don't think many people would switch to Linux because of the KDE applications."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "sure, what kde developers really needed was someone to tell them what to do with their FREEEE time..."
    author: "ac"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "That's the freedom to try to take freedom from others ;)"
    author: "Sebastian Sauer"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "The reason is simple: there are more people using Window$ than people using *BSD. Also, they don't (they are actually right) think that it would be as difficult to get KDE running on *BSD, since it is fairly close to Linux.\n\nAs for the aformentioned (and sure to be more) flames about people not having any reason to switch to Linux or *BSD: name any reason why Linux or *BSD is better (I'll name some: stability, security, and lack of DRM). That is a reason, besides KDE, to switch. The familiarity gained by having used various applications far outweighs the desire to switch to a new OS for a particular application or application suite. Also, the desktop itself is not being ported to Window$ or Mac, only the framework.\n"
    author: "Michael \"trying to see both sides\" Howell"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "If there are not many BSD developers, this means there are not that many BSD users.\nWe would love to have more developers working on BSD. Basically this means, download FreeBSD (or DesktopBSD), install it on your machine (you need a primary partition for that) and build KDE from svn.\nThen find out what doesn't work and start fixing it. That's really all there is to it. You will get helping hands I guess  e.g. from Ade (and maybe from me, but I'm no BSD expert).\n\nAlex\n"
    author: "alex"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "> I somehow have the feeling that there is more people working on KDE for Windows than on KDE for BSD, which is a rather sad thing for a free Desktop.\n\nDisregarding debates on KDE for Windows/Mac, if there are more people working on KDE for those platforms, it is because more people have volunteered to work on it.\n\nKDE is a free software project composed of volunteers. Unless that developer is employed by a company to work on a particular part of the project, there is no one that will dictate what a volunteer should or should not work on. That is why Anon's comment that \"Indeed its very sad to see instead of getting the KDE working on BSDs, these developers sweat day and night to get it working on...\" shows a distorted understanding of how KDE, and most free software projects, are developed. If these developers work hard on that aspect, it is because that is what they want to work on, what they feel they need. No one is going to tell them otherwise and magically assign them to some other part of the project.\n\nIf there are very few people working on KDE on FreeBSD, then it just means that very few people have stepped up and taken responsibility to actually help in improving that.\n\nI exhort you, as well as anyone who is truly passionate about KDE on FreeBSD, to gird up and help in making it happen. That's really the only way things will change."
    author: "Jucato"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "> Disregarding debates on KDE for Windows/Mac, if there are more people working on KDE for those platforms, it is because more people have volunteered to work on it.\n\nWhile this is true in general, KDE as huge FreeSoftware Project with part-commercial backing does have some possibility of influencing development focus. \nBut that isnt really the point and if neither volunteer-devs nor commercial supporters are interested in making KDEonFBSD work, than maybe it will just take longer.\n\nAlso my post was not meant to flame against the kde-windows project, I was just trying to point out that right now there is REALLY MANY freebsd-users waiting for kde4 (which apperently has NOT led to really many freebsd-devs) while the only Windows users desperately awaiting KDE-Apps probably already use GNU/Linux as one of their main architectures.\n\n> I exhort you, as well as anyone who is truly passionate about KDE on FreeBSD, to gird up and help in making it happen. That's really the only way things will change.\n\nYeah, I know. I am on kde-freebsd and I try help where I can, but it seems all of the work really depends two or three people there.\nI hope that at least Bug-reports related to FreeBSD are taken seriously by KDE-Linux-devs..."
    author: "Hannes Hauswedell"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "\"KDE as huge FreeSoftware Project with part-commercial backing\"\n\nCommercial supporters are interested in Linux, not in BSD.\n\n\"there is REALLY MANY freebsd-users waiting for kde4\"\n\nThen FreeBSD devs should build it and package (or how it is done on BSD) it, shouldn't they? KDE devs produce the source, and don't compile neither for Linux distributions nor BSDs. It's the distributors' task to build and package it. KDE devs work on Windows and Mac OS X only because it needs some source modifications. But if I understand correctly what Hannes wrote, it already compiles and works on FreeBSD so no source modifications are needed (which is not surprising as both systems are mostly POSIX compatible and X11 and Qt for X11 are available on both)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Other Platforms?"
    date: 2008-07-31
    body: "> Then FreeBSD devs should build it and package (or how it is done on BSD) it, shouldn't they? \n\nHm... what a great idea! I suppose nobody thought of that yet! [/sarcasm]\n\n> But if I understand correctly what Hannes wrote, it already compiles and works on FreeBSD so no source modifications are needed (which is not surprising as both systems are mostly POSIX compatible and X11 and Qt for X11 are available on both).\n\nYou misunderstood. It compiles mostly, but it doesnt \"work\". At least not to the extent where its useable.\n\n> KDE devs produce the source, and don't compile neither for Linux distributions nor BSDs.\n\nHm, the produce source without ever building it? They must be better than I thought ;)\nNo, seriously. Most KDE-devs work and build on GNU/Linux, so of course KDE works better on GNU/Linux or at least gets done for GNU/Linux faster. That makes sense and is good since, since most FreeSoftware users are GNU/Linux users.\nHowever I feel, that other *free* platforms play a very small role on KDE's agenda. There is constant news of updates for this and that, bugfixing here and there, Screenshots for KDE-Apps on Windows, Screenshots for KDE-Apps on OSX. And maybe one report about OpenSolaris. \n\nAnyways I dont want to sound like a whiner. I hope KDE-devs are open to cross-platform porting issues and help porters wherever they can.\n\nOther than that I should maybe spend more time in helping porting myself than talking ;)\n\nSee you."
    author: "Hannes Hauswedell"
  - subject: "Re: Other Platforms?"
    date: 2008-07-31
    body: "> It compiles mostly, but it doesnt \"work\". At least not to the extent where its useable.\n\nThat is not right! KDE 4 works on my FreeBSD system (7-STABLE amd64) pretty well. Actually it IS usable, very fast (faster than KDE 3 ever was) and really rocks! I really love it because it works very well on FreeBSD.\n\nI do not know what goes wrong with your system but your experience is certainly NOT the general experience of all FreeBSD KDE users. I never experienced your strange problems (you mentioned on kde-freebsd@kde.org list) and it must be something particular to your individual setup / machine.\n\nWell, that does not help you. But it is true. There are some problems because of non portable code, some linuxisms etc. That should be spotted and reported to the KDE developers. That is what FreeBSD porters / committers are already working on but it needs more KDE developers attention.\n\nYou should also notice many things are done under the hood (area51 repository, private mails, IRC, IM) which are not communicated publicly (via mailing lists etc.). I personally do lots of testing but communicate issues directly and do not discuss them publicly.\n\nThere are some more testers and contributors than you may be aware of. Especially since my call for contributors, testers etc. through freebsd-ports@freebsd.org mailing list. Of course we could really have more of them. But the 'KDE 4 on FreeBSD' project is now in a much better shape than it ever was before and some folks are working very hard to further improve it.\n\nI agree with you on one point. That is that some KDE developers are actually not very interested in issues with non linux platforms. I think some of them have a very questionable attitude, especially if they tell you in private if you have problems with KDE you should put away your 'crappy' operating system and you should use the 'wonderful' linux instead, because it works for them on linux.\n\nThat kind of short sighted attitude makes me really angry and I do not understand those folks. But there are also some KDE developers who are very interested in cross platform issues and will take your issues serious.\n\nRegards"
    author: "Sticky Bit"
  - subject: "Re: Other Platforms?"
    date: 2008-07-31
    body: "Please have also a look at this blog post 'KDE4 on other platforms' from Adriaan de Groot:\n\nhttp://people.fruitsalad.org/adridg/bobulate/index.php?/archives/621-KDE4-on-other-platforms.html\n\nIt describes the situation pretty well.\n\nRegards"
    author: "Sticky Bit"
  - subject: "Re: Other Platforms?"
    date: 2008-08-01
    body: "Quoting myself here, but more in response to Hannes: there *is* no agenda. There is no steering committee saying \"we need to support FreeBSD\" (or any other OS). There are contributors who are stepping up and making a difference. On FreeBSD, that's David and Max and Martin and others. Through their efforts, KDE gets better on that platform and because they work constructively with the KDE project through some KDE committers, awareness slowly spreads. The EBN helps a little, but could certainly use more tools checking for cross-platform things. There was a little FreeBSD BoF at the KDE4 launch event; there will be some Solaris / Studio tools workshops at Akademy. Those things work to show everyone that a (sub)community is alive and working towards getting KDE4 out there in more places."
    author: "Adriaan de Groot"
  - subject: "Re: Other Platforms?"
    date: 2008-08-04
    body: "I repeat, that IN NO WAY I wanted to abate the work done by the KDE-devs in general and especially not the work done by the poeple currently porting KDE to FreeBSD. \nI have been using KDE long time and really digg the free software spirit. I know the results (and sometimes problems) of volunteer work.\n\nI really only wanted to bring FreePlatform-diversity into the discussion. Sticky Bit has mentioned some of the problems I tought about. He also seems more qualified to make these statements than me, I think, so I'll just stop complaining now."
    author: "Hannes Hauswedell"
  - subject: "Re: Other Platforms?"
    date: 2008-07-30
    body: "It's not that bsd developers not suddenly use windows and make kde work there. Those ae mostly windows developers that discovered kde for themselves and now try to get it work on their platform. So, the freebsd problem is a problem of the freebsd users/developers, they have to step forward and make kde work, not the windows developers, since they have no idea about freebsd"
    author: "Beat Wolf"
  - subject: "Re: Other Platforms?"
    date: 2008-07-31
    body: "Hannes I use KDE4 RC1+ under FreeBSD 7.0 without any problemes. Seems you have some local problems.\n\nBest ./anton"
    author: "Et"
  - subject: "downloading"
    date: 2008-07-30
    body: "Thanks, i test it the svn version some weeks ago, and it was great\ndownloading...\nanyone have slackbuilds 0_0?\n"
    author: "Lostshell"
  - subject: "More than I anticipated"
    date: 2008-07-30
    body: "Its not often in this day and age that you get what you expect from any type of consumer good, its even less often that you get more than you expect.\n\nI must say that today is one of those rare occasions. I have been eagerly anticipating the arrival of KDE 4.1 since 4.0 was released. \n\nI cannot begin to express a heartfelt thanks to each and everyone who had a hand in this latest development.  \n\nTruly a paramount effort, its all the little touches that show the true quality.\n\n"
    author: "Terry Brown"
  - subject: "Re: More than I anticipated"
    date: 2008-07-30
    body: "well said!"
    author: "same here"
  - subject: "Re: More than I anticipated"
    date: 2008-07-31
    body: "Everything that you said, I agree with. A heart-felt ++.\n"
    author: "Michael \"100% agree\" Howell"
  - subject: "ETA for OS X"
    date: 2008-07-30
    body: "Anyone have an idea how long the KDE4 for OS X will take to be released? I'm very keen to replace some of my apps with the KDE4 equivalents."
    author: "AndyC"
  - subject: "Re: ETA for OS X"
    date: 2008-07-30
    body: "mac.kde.org\n\nIt may take some day before they will be updated, so take a look often."
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: ETA for OS X"
    date: 2008-07-30
    body: "Thanks - that was the first website I checked when I found out 4.1 had been released! I didn't expect an OS X release (though I did hope), but I thought there might be an announcement.\n\nJust curious how long to expect (day/weeks/months)?\n\nAnyway, I guess I'll just have to put up with inferior apps until then ..."
    author: "AndyC"
  - subject: "thanks for the great work, just a small issue"
    date: 2008-07-30
    body: "I want to thank all the kde community for all the efforts to make this release rock!\n\nI was trying the newly introduced add panel functionality and have accidentally removed the main panel. Does anyone know how to restore it?"
    author: "Jean Pierre"
  - subject: "Re: thanks for the great work, just a small issue"
    date: 2008-07-30
    body: "cp ~/.kde4 ~/.kde4.backup\n\nrm -rf ~/.kde4/share/config/plasma\n\n*it may be .kde, it depend on distro\n\nor you can just right click on desktop, add a panel and add some widget in it."
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: thanks for the great work, just a small issue"
    date: 2008-07-30
    body: "$ ls /home/ax4/.kde4/share/config/plasma*\n~/.kde4/share/config/plasma-appletsrc  \n~/.kde4/share/config/plasmar\n\nAre the two files to wipe if adding it back doesn't work. Do make a backup first! "
    author: "blauzahl"
  - subject: "Re: thanks for the great work, just a small issue"
    date: 2008-07-31
    body: "right click on the desktop -> add panel, configure to taste."
    author: "Aaron Seigo"
  - subject: "Amazing Work!"
    date: 2008-07-30
    body: "I would also like to express my deepest gratitude towards all involved in creating such an amazing desktop experience that leads us into a new era of desktop computing!\n\nSimply amazing work!\n\nCongratulations to all of you!\n\nNassos"
    author: "Nassos Kourentas"
  - subject: "Wonderful!"
    date: 2008-07-30
    body: "Thank you very much!"
    author: "Heja AIK"
  - subject: "Windows?"
    date: 2008-07-30
    body: "I was expecting KDE4 to be stable on Windows with 4.1 - has that changed? Why is the windows.kde.org so minimalist?"
    author: "Anon"
  - subject: "Re: Windows?"
    date: 2008-07-30
    body: "If you are having problems, feel free to help them. :)\n\njoin their mailing list\nreport bugs\nhelp triage bugs\n\nNone of these require you to know how to program. If you join #kde-bugs on freenode, we are happy to teach you how to triage, and then send you back to the windows team (who live on #kde-windows, in fact)."
    author: "blauzahl"
  - subject: "Already switched to 4.1 completely"
    date: 2008-07-30
    body: "Archlinux provided 4.1 packages in their main repos a day before the official release. 4.1 is stable enough for me that i've completely switched to it already.\n\n4.1's elegance tempts me to join in as a dev. :)\n\n\nA HUGE THANKYOU to the devs, testers, artists and all kinds of contributors !!"
    author: "Junaid"
  - subject: "KDE 4.1 rocks despite some issues"
    date: 2008-07-30
    body: "First thanks for KDE team for their efforts.\nBut here I'm faced with some problems:\n- Sometimes I cannot copy/paste from a windows session open through krdc (rdp).\n- Okular won't print a PDF file to my printer\n- The telnet://host bookmark doesn't work anymore in konsole. The ssh bookmark (ssh://user@host) is working fine. Konsole keeps complaining about \"I do not know how to handle the protocol \"telnet\".\n\nAny ideas about those problems?\n\nThanks in advance"
    author: "Andy"
  - subject: "Re: KDE 4.1 rocks despite some issues"
    date: 2008-07-30
    body: "okular:\n\nthis issue should be fixed (https://bugs.kde.org/show_bug.cgi?id=162793).\n\nmaybe it takes some time until you get a new qt-version.\n\nkde 4.1 is just great.\n\nfelix"
    author: "felix"
  - subject: "Re: KDE 4.1 rocks despite some issues"
    date: 2008-07-31
    body: "On your first point, I noticed something similar when using fish to copy files from my other linux box.  A lot of times copying via \"drag n drop\" did nothing.  However, if I right click the file and select copy, I can then paste the file to the new location."
    author: "cirehawk"
  - subject: "Step in Debian?"
    date: 2008-07-30
    body: "I use the 4.1 packages from Debian experimental and I wanted to try out Step, but I can't find it.\nkdeedu is version 4.1, but the package release doesn't mention Step at all:\nhttp://packages.qa.debian.org/k/kdeedu/news/20080727T224745Z.html\n\nDoes anyone know what's up with it?"
    author: "Sepp"
  - subject: "Re: Step in Debian?"
    date: 2008-07-30
    body: "Because it does need Gmm++ 3.0  [1] (A generic C++ template library for sparse, dense\nand skyline matrices), that is not in the Debian archive.\n\n[1] http://home.gna.org/getfem/\n\n"
    author: "Ana Guerrero"
  - subject: "Re: Step in Debian?"
    date: 2008-07-31
    body: "Then why don't you package it? You can't probably get anything easier to package than this package. :-)\n\nIf that helps, here's the specfile we're using in Fedora:\nhttp://cvs.fedoraproject.org/viewcvs/rpms/gmm/devel/gmm.spec?rev=1.1&view=markup\n(but I don't think that'll give you much information other than how trivial this package is)."
    author: "Kevin Kofler"
  - subject: "Re: Step in Debian?"
    date: 2008-07-31
    body: "The Kde in debian seems very incomplete. For example I could not find any kdebindings, Superkaramba is useless without it. What happend?"
    author: "yoriq"
  - subject: "Re: Step in Debian?"
    date: 2008-07-31
    body: "OK, that explains it.\n\nAnd btw, thank you for making the great Debian packages!"
    author: "Sepp"
  - subject: "Good job!"
    date: 2008-07-30
    body: "I'll admit it ... I found the 4.0 release unusable for daily computing.  Looks like you've turned it around and really delivered a 4.1 that's ready for the world.\n\nI've only been able to use it for a couple of hours so far, but it's been stable, looks great, and no matter what some folks say, I REALLY like the folder views on the desktop!  Especially since you can filter the contents.  That's just sweet.\n\nThis one just might win me back from Gnome ... which I used to hate, but found it a better option than the 4.0 release last year."
    author: "Crazy Henaway"
  - subject: "Re: Good job!"
    date: 2008-07-31
    body: "Agreed.  4.1 is very nice.  The difference for me is I never went over to Gnome.  :)  I guess I'm more patient than most, but I wasn't bothered by the roughness of the 4.0 release.  I was more than content to keep using the great 3.5 series until the 4 series stabilized.  I have OpenSUSE with 4. on my laptop.  As soon is my distro of choice (PCLinuxOS) moves to KDE4, I will convert all my boxes.\n\nI do have a comment/question about folder views.  For the life of me I really can't grasp the benefit of it yet.  From my limited testing, it just seems like a file manager with transparency.  Seems like I can just use Dolphin (or Konqueror) to view my folders if I need to.  Obviously I have a limited perception of it since most seem to be excited about it's capabilities.  Can someone better explain to me the benefit of folder views, or better yet, give a good example of how they can be used to enhance the user experience?\n\nThanks again KDE team.  Great job!\n\nEric....."
    author: "cirehawk"
  - subject: "Re: Good job!"
    date: 2008-08-02
    body: "Normally, you are using the desktop as a sort of temp folder in which you dump stuff before using/moving it.\n\nFolder View allows you to have a series of such folders, and not just one. They are network transparent, meaning you can use them to monitor things on the network.\n\nThey support filtering, so you can refine the view to only show what you care about.\n\nunlike dolphin windows, they can be brought on top with a specific keyboard shortcut.\n\nNow if embeded file previews could be activated, I'd be really happy :)"
    author: "hmmm"
  - subject: "End User FAQ is busted"
    date: 2008-07-30
    body: "Hia.\n\nClicking on the KDE4 End User FAQ currently doesn't work. It gives an \"Account suspended\" message from the hosting supplier. I presume that it has exceeded its bandwidth allocation or something like that?\n\nAnyway, just thought I'd let you know in case you hadn't noticed yet.\n\nCheers!"
    author: "Spud"
  - subject: "this is the most amazing desktop "
    date: 2008-07-30
    body: "i have ever been using and way ahead of all the others . its shiny , beautiful , fast and custumizable the way i can lve with it . it's simple the future we see and can use now . there are some hicks here and there , but in general it's simple awewsome . i had some hard words in the past for the early releases , and i wouldn't change the words today , they were simply emotional . i am now in the state of paying back . me not being a developer  paypal will be my friend .\na big thx go out to all who made that big thing possible !!\n\ncheerz pete :-) "
    author: "pete"
  - subject: "KDE-edu is Extraordinary Stuff !!"
    date: 2008-07-30
    body: "Well I must confess that KDE-Edu offers a set of applications that is simply UNMATCHABLE for any project !!\n\nIn my opinion, these applications alone warrant KDE's adoption in schools. Great stuff guys !!"
    author: "Junaid"
  - subject: "Nvidia performance fixed soon?"
    date: 2008-07-30
    body: "See this post by AaronP from Nvidia:\nhttp://www.nvnews.net/vbulletin/showpost.php?p=1728071&postcount=7\n\nQuote:\n\"I posted about it somewhere, but I can't find the post now. Anyway, a number of improvements to the RENDER acceleration on GeForce 8 and higher GPUs are being developed for the 177.* and 180.* driver series. These include support for the various repeat modes, transforms, and convolution filters, as well as acceleration for trapezoid rendering. In addition, resizing windows in KDE 4 hits a number of particularly slow fallbacks that we're working to avoid. The official release notes will mention these improvements when they're ready for prime-time.\"\n\nSounds great!"
    author: "Robin"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-30
    body: "Oh yeah!\n\nPlease post it on the dot as soon as new Betas are available. I neeeeeeeed to try them!"
    author: "Sebastian"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "Congratulation for KDE 4.1, btw, today i see the NVidia lounch new update driver http://www.nvidia.com/object/linux_display_ia32_173.14.12.html\nI don't know this one already fix the issue or not."
    author: "Teddy Widhi"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "This version does not. But it really looks like the KDE 4.1 release has brought so much attention to the \"problem\", that (a) the NVidia/Linux issue made it to the digg hall of fame, and (b) kicked NVidida in the ass so hard that they made the first public statement about their intention to fix it. Perfect."
    author: "christoph"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "Sorry, i dont understand this...\n\nThe nvidia guys want to force me to buy an new 8xxx+ card just to have proper 2D???\n\nI doubt that my current 7600GT cannot do the same...\n\nWhat a shame!"
    author: "Jan"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "older cards don't have those performance issues..."
    author: "Robin"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "Of course they have...\n\nJust check my posting here and try it:\nhttp://www.nvnews.net/vbulletin/showthread.php?p=1725445&posted=1#post1725445\n\nAnd i have already answered in the thread the original poster mentioned here...\n"
    author: "Jan"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "interesting... maybe a (slightly?) different issue or only a few 7xxx are affected.\nI read many reports of people using 7xxx without performance issues."
    author: "Robin"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-08-01
    body: "I'm using a 7300GS and yes, things are really crappy, mostly with compositing enabled though, as often reported before, performance is ok with compiz."
    author: "fredde"
  - subject: "Re: Nvidia performance fixed soon?"
    date: 2008-07-31
    body: "i have got a nvidia fx 5200, and 2d performance (resizing windows,... ) in kde 4 ist much worse than in in kde 3.5, no matter whetzer desktop effects are enabled or not. so this definitely also affects older cards. "
    author: "taerde"
  - subject: "My faith in the devs is confirmed"
    date: 2008-07-31
    body: "There's nothing like a clean, visionary rewrite of the foundation to keep you busy and out of sight for a while, to make an initial release people don't get unless they understand what's happening under the hood, and then to dazzle them with all the payoff at the end.  Go devs!  Your attention to infrastructure for KDE4 is getting its due at last."
    author: "Daniel"
  - subject: "Mac style menubar"
    date: 2008-07-31
    body: "I am currently running KDE 3 and have been looking forward to a more fully featured KDE 4. The one feature I absolutely must have is the ability to use a mac style menubar on top (outside the app window). There was an option to do this in KDE 3, is there any way to do this in KDE 4.1?"
    author: "Srikanth Agaram"
  - subject: "Re: Mac style menubar"
    date: 2008-07-31
    body: "Not yet. The devs love to bring this feature back to KDE 4 as well, but there wasn't much time for it now. AFAIK it will reappear in KDE 4.2."
    author: "Diederik van der Boor"
  - subject: "stiffy for kde dragon"
    date: 2008-07-31
    body: "i'm sexually attracted to the KDE dragon"
    author: "kde dragon makes me stiff"
  - subject: "Re: stiffy for kde dragon"
    date: 2008-08-02
    body: "I'm sorry to tell you, but he's taken:\n\nhttp://www.kde.org/stuff/clipart/katie-221x223.jpg"
    author: "AC"
  - subject: "NVIDIA vs. XGL"
    date: 2008-07-31
    body: "There is one thing that KDE's techbase misses in order to support better NVIDIA effects: XGL\n\nI read it months ago on the dot, but using XGL on OpenSuse 10.3 with KDE 4.0.x I could not enable desktop effects, though most 2d rendering problems vanished. \n\nRight now I tried XGL and it works much better (though not optimal):\n\nMeasured Frames per 5 seconds: \nXorg, effects activated:  280\nXorg, effects deactivated: 21000\nXgl, effects activated: 12000 (Though the keyboard is a little choppy)\n\nUsing OpenSuse 11.0, NVIDIA 8400 GT, latest NVIDIA driver. The hibnts on techbase did not work at all. \n"
    author: "Sebastian"
  - subject: "Wrong!"
    date: 2008-08-01
    body: "I am sorry. I was wrong. Before yesterday I tried the hints on techbase, but they did not work. Yesterday I used yast2 to start xgl instead of Xorg, restarted X and the wonder happened. After rebooting I found out, that Xgl still is not working, but the benefits were still active using Xorg with the modified settings (somehow they were not enabled before yesterday?). Does anyone experience problems with the keyboard and with firefox? I got them with the modified settings."
    author: "Sebastian"
  - subject: "Still not ready, for me"
    date: 2008-08-03
    body: "Yes, 4.1 has come on a long way from 4.0. But there's still too many features missing that were in 3.x, too many things that aren't obvious. Just a few examples:\n\nHow do I move widgets in a panel? If it's possible, it's not at all obvious.\n\nHow do I change the colour of the panels? That black just draws the eye too much from the windows where I'm actually working.\n\nWhy can't I change the layout in knode? I could in 3.x.\n\nPlus, fonts seem fundamentally broken - I change the size so that one application looks right, and another has giant or tiny fonts. And I was using it for about 10 minutes when the system tray disappeared - no amount of removing & re-adding it would make it come back.\n\nAh well, back to 3.5 for now..."
    author: "Anonymous"
  - subject: "Re: Still not ready, for me"
    date: 2008-08-03
    body: "Of course, as soon as I post it, I manage to find something explaining how to move widgets. I have to say though, that even knowing how to do it, it's not obvious."
    author: "Anonymous"
  - subject: "Re: Still not ready, for me"
    date: 2008-08-06
    body: "I too had problems figuring this out.\nIn general configuring the panel is very hard to figure out. I.e. resizing the panel; it is hard to see exactly how big it is and how the end result will become.\nFirst I was extremely irritated, not only about this problem but many other configuration issues in KDE 4.1\nI have been using KDE since version 1 and has always been very happy with how easily it was to configure and how many options there were.\nBut as time has gone by and as I've used KDE 4.1 since it's release, I have gotten to like a lot of them and its becoming harder and harder to return to KDE 3 :-)\nHave to agree on the panel thing though :-)\nThanks KDE team!"
    author: "Jarl E. Gjessing"
  - subject: "It still sucks dead hamsters through a garden hose"
    date: 2008-08-15
    body: "KDE 4.1 SUCKS!!! It still sucks dead hamsters through a garden hose! \n\nWhy? Because you, Sir, have stopped listening to your community and its needs and just code along what you think is cool, because you stopped giving us the opportunity to actually configure many things! \n\nWhat sucks? \n\nFor example this:\n\n- folderview or the plasmoids at all. Yes, might be a NVIDIA driver problem with that lousy performance, but: it was known before, why haven't you implemented it another way? Why did you take our old desktop away??? GIVE US OUR OLD DESKTOP BACK!!!\n\n- knode. This thing is completely broken in 4.1.0. Something, that shouldn't have slipped through beta testing. All postings are now without any charset at all, it doesn't honor this anymore, all postings are made without any word wrap at all - what in Jesus H. Fucking christs name has gone wrong there???\n\n- the panel. Yes, finally we can move it (ooooh), and also make new plasmoids on it (ahhh), but: why can I still not change its color? Where are all the little helpers gone we had in 3.5.9, like quick folders and such? In few words: it STINKS! Give us our old, beloved kicker back!!!\n\n- The kickoff style menue. What in Jesus Fucking Christs name have you been thinking while implementing this stinker? It's size is fixed, it doesn't show us all entries at all, moving between the levels takes time, because animated, in one word: it is rotten, it stinks, it's rubbish! And you keep us telling this stinker gives us better usability? Holy shit! I cannot believe it! At least we can finally turn on the old menu style back now again!!!\n\n- Akregator. It has serious bugs in positioning the tool bars, it isn't taking archiving options anymore, meaning it's somewhat broken, too! \n\n- Mounting of NTFS-Volumes through ntfs-3g in Dolphin/Konqueror is still not working. WHY NOT? This is something that even GNOME is able to do for quite some time now without any problems AT ALL! \n\n- Responsiveness. KDE 4.1 still feels slow and sluggish on many occasions. Why? \n\nMight be, that KDE 4.1 has all the nice shiney technolgy, buzz and such under the hood, but what is it worth to us when the car build upon this engine stutters and is still making errors regularly? \n\nFor short: start listening to your community again and act upon it wisely, otherwise KDE is doomed to further run down the hill!!! At the moment it JUST SUCKS!!! It is what Vista is for Windows, all bling and looking nicely, but no one really wants to use it, because it SUCKS, because it's limiting its users and its broken. \n\nAt the moment I am going to stick with XFCE, because even development might be a little bit slower than with KDE, this project doesn't nearly stink as much and the main developer cares deeply about his community!!!"
    author: "Herbert Eisenbei\u00df"
  - subject: "Re: It still sucks dead hamsters through a garden hose"
    date: 2008-08-15
    body: "And I forgot:\n\n- resizing of plasmoids just SUCKS, too! What have you been thinking - again - while implementing this stinker??? When I resize a window, I only want to resize it in the direction I am moving my mouse, NOT resizing it the lousy way it's implemented now, because this makes resizing unprecise, when it's mirrored on the other side! It sucks, sucks, sucks!!!"
    author: "Herbert Eisenbei\u00df"
  - subject: "Re: It still sucks dead hamsters through a garden hose"
    date: 2008-08-15
    body: "You need:\n\n- a thesaurus; and \n- some basic lessons in human-human iteraction."
    author: "Anon"
  - subject: "Re: It still sucks dead hamsters through a garden hose"
    date: 2008-08-15
    body: "Yo, homie, yo got da shit, yo! \n\nAnd seriously: I am disappointed with KDE 4.1, especially being a long time user, using it since KDE 2.x days. "
    author: "Herbert Eisenbei\u00df"
  - subject: "Re: It still sucks dead hamsters through a garden hose"
    date: 2008-08-15
    body: ">Why? Because you, Sir, have stopped listening to your community and its needs\n>and just code along what you think is cool, because you stopped giving us the\n>opportunity to actually configure many things! \nBecause KDE is coded by one person, amirite?\n\n> - folderview or the plasmoids at all. Yes, might be a NVIDIA driver problem\n>with that lousy performance, but: it was known before, why haven't you\n>implemented it another way? Why did you take our old desktop away??? GIVE US\n>OUR OLD DESKTOP BACK!!!\nWhat can't you do with your \"new\" desktop that you couldn't do with your old desktop? You still have icons.\n \n>the panel. Yes, finally we can move it (ooooh), and also make new plasmoids on\n>it (ahhh), but: why can I still not change its color? Where are all the little\nYou can get new themes for it easily. Right click the desktop and go to desktop settings, and select \"Get new themes\" in the desktop config dialog.\n>helpers gone we had in 3.5.9, like quick folders and such? In few words: it >STINKS! Give us our old, beloved kicker back!!!\nhttp://kde-look.org/content/show.php/QuickAccess?content=84128\nYou can still use Kicker if you want to that badly.\n \n>The kickoff style menue. What in Jesus Fucking Christs name have you been >thinking while implementing this stinker? It's size is fixed,\nWrong. It's resizable like any other window.\n>it doesn't show us all entries at all, moving between the levels takes time, >because animated, in one word: it is rotten, it stinks, it's rubbish! And you\n>keep us telling this stinker gives us better usability? Holy shit! I cannot\n>believe it! At least we can finally turn on the old menu style back now >again!!!\nYou could do this since KDE 4.0.0. It's not \"finally\"\n \n>Mounting of NTFS-Volumes through ntfs-3g in Dolphin/Konqueror is still not\n>working. WHY NOT? This is something that even GNOME is able to do for quite\n>some time now without any problems AT ALL! \nBlame your distro. ntfs-3g works wonderfully with Kubuntu.\n \n>Responsiveness. KDE 4.1 still feels slow and sluggish on many occasions. Why? \nCould be your drivers if you're using nvidia.\n \n Might be, that KDE 4.1 has all the nice shiney technolgy, buzz and such under the hood, but what is it worth to us when the car build upon this engine stutters and is still making errors regularly? \n \n>For short: start listening to your community again and act upon it wisely,\n>otherwise KDE is doomed to further run down the hill!!! At the moment it JUST\n>SUCKS!!! It is what Vista is for Windows, all bling and looking nicely, but no\n>one really wants to use it, because it SUCKS, because it's limiting its users\n>and its broken.\nKDE has been very receptive to the needs of the community. This is all just wrong.\n \n\"Whaa, a few features I want aren't implemented\" in no way means \"KDE sucks dead hamsters through a garden hose\". While KDE4 doesn't have/duplicate every last feature KDE3 has, in general KDE4 applications are much better bug-wise and feature wise than their KDE3 counterparts. (See: Dolphin, Okular, etc)"
    author: "Jonathan Thomas"
  - subject: "Re: It still sucks dead hamsters through a garden hose"
    date: 2008-08-15
    body: "I applaud your effort, but honestly, that post simply wasn't worth responding to."
    author: "Anon"
  - subject: "Re: It still sucks dead hamsters through a garden hose"
    date: 2008-08-16
    body: "> What can't you do with your \"new\" desktop that you couldn't do with your old\n> desktop? You still have icons.\n\nYup, I've got icons. But either alone or in a plasmoid. I cannot for example sort them nicely in folderview (yet), there's no device showing up anymore at the desktop per default, the older thing just had more options and freedom at all. \n\nAlso scaling of folder view, make this plasmoids at all, sucks. When scaling a window, I want it only to scale in the direction I am moving my mouse, but not having that mirrored all over the window, which makes it really hard to scale it nicely. Wonder what the developers have been thinking about that - nothing much at all, I guess. \n\n> You can get new themes for it easily. Right click the desktop and go to desktop\n> settings, and select \"Get new themes\" in the desktop config dialog.\n\nI don't want new themes. That's not the point. I want to be able to right click at the panel again and choose the transparency and color of it in a configuration dialogue, just like I could do it with kicker in 3.5.x. With the new panel - impossible.\n\nI also want to change the size of the new panel, meaning the vertical space it occupies. Also impossible - yet, without editing some configuration files with a text editor. What's the point in having a GUI when quite fundamental settings can only be changed with a text editor at all? \n\nThere's no way for the new panel to autohide - yet. \n\nThere are no quick-hiding buttons for it yet, like kicker had. \n\nThere's no way for a quick listing of the contents of a folder, like the old kicker had. There I could make a quick lister and it showed up presto. Yes, I saw the plasmoid you pointed at, good work - but this should ship with the whole package at all. \n\nThe truth is: the new panel lacks many options old kicker had and still has a long way to go before it's really usable again. \n\n> Blame your distro. ntfs-3g works wonderfully with Kubuntu.\n\nLet me put it that way: GNOME has found a better way than HAL to deal with it, that works out of the box better than KDE. KDE normally needs quite some fine tuning for it, IF it is even able go work afterwards. \n\n> Could be your drivers if you're using nvidia.\n\nYes, I'm using Nvidia, also tried the workarounds. But: it's not only Nvidia, but also ATI. No one else had those problems yet, so why didn't they implement it another, better way, that works better with the graphics stuff? \n\n> KDE has been very receptive to the needs of the community. This is all just wrong.\n\nI doubt that one really dearly. The internet is full of \"KDE 4.x sucks rants\" and that's not without reason.\n\nMight be, that in general KDE4 applications are better bug wise now, but: why did Knode slip through the QA process of the beta? \n\nThis thing is really unusable with KDE 4.1.0 now! It shows word wraps in the editor, but when posting they are all gone. There is no charset anymore in the headers of a post at all, you can try to set, what you want - it doesn't honor this. And it is always trying to set a Followup-To: to the newsgroup you're posting at, even if it's only one at all. So for short: nice, to have it back, but in the state KDE 4.1.0 has it - it's rubbish. Period. \n\nSome things are looking nice, like Dolphin or Okular, right. But then again, kprinter has gone with its filters, there's no option for a print preview anymore nor a module in the system settings to add printers. Why is there no GUI for installing printers anymore??? Arg! "
    author: "Herbert Eisenbei\u00df"
---
6 months after the release of KDE 4.0, the KDE community today <a href="http://www.kde.org/announcements/4.1/">announced</a> the released of the second feature release in the KDE 4 era. Lots of changes have gone into this release and the KDE community hopes to be able to make most early-adopting users happy with this release. Lots of feedback from people trying out KDE 4.0 has gone into KDE 4.1, filling most of the gaps people experienced with the 4.0 releases. Highlights of KDE 4.1 are the KDE PIM suite, which has returned in its KDE 4 incarnation, a more mature Plasma desktop and many, many new features and applications. Make sure to take some time to read through the <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">high-level changelog</a> or even the more detailed <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">feature plan</a> on Techbase. Before you try KDE 4.1, please read the <a href="http://techbase.kde.org/User:Jucato">KDE4 End User FAQ</a> and make an educated guess whether <a href="http://techbase.kde.org/Schedules/Is_KDE_4.1_for_you%3F">KDE 4.1 is for you</a>.



<!--break-->
<p>
<a href="http://www.kde.org/announcements/4.1/"><img src="http://www.kde.org/img/kde41.png" border="0"/></a>
</p>

<p>The release is dedicated to KDE's contact in Africa, Uwe Thiem. Uwe <a href="http://dot.kde.org/1216068830/">passed away</a> after a kidney failure two weeks ago. Africa's new press contact for KDE is AJ Venter, a friend of Uwe's who stepped up on short notice to help with filling the gap Uwe's death leaves in the KDE community.</p>

<p>
Meanwhile, KDE's Release Team <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Schedule">scheduled</a> a number of bugfix and translation updates. KDE 4.1.1 will be made available on September 3rd, 4.1.2 will be out on October 1st, and 4.1.3 will be there on November 5th. KDE 4.2.0 will in 6 months, the release date is set to January 27th 2009.
</p>


