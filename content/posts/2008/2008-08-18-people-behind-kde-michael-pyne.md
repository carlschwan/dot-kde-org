---
title: "People Behind KDE: Michael Pyne"
date:    2008-08-18
authors:
  - "aspehr"
slug:    people-behind-kde-michael-pyne
comments:
  - subject: "Nice Interview"
    date: 2008-08-17
    body: "what an interesting interview!"
    author: "Karl Leckerli"
  - subject: "Re: Nice Interview"
    date: 2008-08-18
    body: "Yes, I like it too. Especially since it reads far more like a dialog than previous installments did. Those read like a standard list of questions being answered and then send back. In this one, the interviewer responds to what is being answered, asks more questions. This makes for a far more interesting read. Good job!"
    author: "Andr\u00e9"
  - subject: "Wallpaper"
    date: 2008-08-17
    body: "Nice interview indeed.\n\nI was going to ask where he found that wallpaper, but some googling led to this:\n\nhttp://customize.org/wallpapers/42771"
    author: "AIK"
  - subject: "Good employer"
    date: 2008-08-17
    body: "Michael's got a good employer - the Navy employed Grace Hopper, the discoverer of the computer bug - for most of her career as a programmer. So he's following in some great footsteps.\n"
    author: "John"
  - subject: "Re: Good employer"
    date: 2008-08-18
    body: "Yeah, it's been pretty good times in the sub fleet.  I realize I kind of sounded down on the Navy but like I said overall it's been a good deal.  Not sure that I will make it a career but it's hard to complain even after some of those days that seem like they will never end.  The work is interesting and the people are awesome so although it would not have been my first choice if I could go back in time, I have no regrets.\n\nI will mention however that my work for the Navy is completely unrelated to the work done by ADM Hopper, I was accepted into the Submarine community in spite of my Computer Science degree, not because of it."
    author: "Michael Pyne"
  - subject: "Re: Good employer"
    date: 2008-08-19
    body: "Hello Michael,\n\nimagine that: You could hack the bombs your submarine carries to not explode and exterminate life on earth. Just please, make no mistake.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Good employer"
    date: 2008-08-19
    body: "Yeah, let's strip away our ability to defend ourselves. Good idea. Now we can all die together \"peacefully\"."
    author: "yman"
  - subject: "Re: Good employer"
    date: 2008-08-20
    body: "Debian User:\n\nI'll talk about disarming our bombs when you've disarmed everyone else's.  Sound fair? :P\n\nAlso I'm not really a big fan of getting beat up or stabbed, so I think we should also go and ban all clubs and knives."
    author: "Michael Pyne"
  - subject: "Re: Good employer"
    date: 2008-08-20
    body: "Hey Michael, I'm ex-Navy myself. I was an airdale way back when the Enterprise was the only nuclear carrier. For this reason it was the choice instrument of sabre rattling and offered typical 8 month deployments. No senior people wanted to be there and thus you can imagine what kind of command it was. Sea dogs voted us most likely to sink back in 1976. It was around this time I was told Navy stands for \"Never Again Volunteer Yourself\". \n\nI am quite proud to have served. When I got out I had received one meritorious promotion and was the only Avionics Tech in my squadron fully qualified on all four versions of the A6 aircraft. I had zero downing gripes in 6 months as a flight deck troubleshooter. I loved those 100 hour work weeks of run don't walk in full gear and equitorial heat around 50 turning jets in 1100 feet (33.5 meters). I think it was partly that excellence was not rewarded without seniority that frustrated me. By the same token it was the first place I proved to myself I could achieve excellent performance and the discipline I learned impacted the rest of my life. I have no regrets but one nice thing about being old is that I can't wake up re-enlisted. ;-)\n\nAnyway it's always fun to see others from the US in KDE and even cooler to see people in the Navy. You have my appreciation both for what you do for KDE and for standing ready to protect my freedom. I salute you.\n\nOh... and check out http://kittyhooch.com to see what I do now."
    author: "Eric Laffoon"
  - subject: "Re: Good employer"
    date: 2008-08-18
    body: "She didn't discover the computer bug:\n\nhttp://en.wikipedia.org/wiki/Software_bug"
    author: "anonymous coward"
  - subject: "Re: Good employer"
    date: 2008-08-18
    body: "Yeah, and everyone knows that wikipedia is so totally, 100% accurate at all times :-)"
    author: "Hmmmm"
  - subject: "Re: Good employer"
    date: 2008-08-18
    body: "If you don't like Wikipedia itself, take a look at the sources it cites relevant to that quotation.  I just somehow doubt they'd make up a quote by Thomas Edison that uses the word \"bug\" in the modern sense, and cite it as well.\n\nBut I know everyone likes to hate on wikipedia even though I and many others rarely find any gross or even minor errors on it (maybe I'm just not looking at the right articles?)."
    author: "anonymous coward"
  - subject: "Re: Good employer"
    date: 2008-08-18
    body: "Ah shucks. It was such a good story!"
    author: "Ian Monroe"
  - subject: "\"Football\""
    date: 2008-08-18
    body: "\"Football football or soccer football?\" should obviously read \"Football football or American football?\" :-P"
    author: "Tim"
  - subject: "Re: \"Football\""
    date: 2008-08-18
    body: "Noted for the future! \n(I don't actually really follow either one. And I am American, so have poor perspective on that. ;)"
    author: "blauzahl"
  - subject: "Re: \"Football\""
    date: 2008-08-18
    body: "Isn't American Football  just a modified version of Rugby?"
    author: "yman"
  - subject: "Re: \"Football\""
    date: 2008-08-18
    body: "Yeah, the \"wuzzy\" kind. \n;-)\n\nIt's like Rugby only with breaks every 15:th second. And with big helmets and other protective gear. And let's have another two teams standing by if someone from the first team gets tired from running for 10sec straight.\n\nJust kidding. I enjoy the sport, although I do think it should have another name. Football is played with the feet, hence the name. Call it Am-ball or something. ;-)\n"
    author: "Bmin"
  - subject: "Re: \"Football\""
    date: 2008-08-19
    body: "actually, football is played on foot (as opposed to on horses) hence the name.  You don't have to redefine the word \"football\" just to win an argument :)\n\nPersonally, I prefer the Australian way: just call the sport by it's name.  We have Soccer, Gridiron, Rugby (Union), (Rugby) League and Aussie Rules."
    author: "m"
  - subject: "Re: \"Football\""
    date: 2008-08-19
    body: "Great explanation, never heard it before. It's good to know where the name came from. To be fair though most ball sports are played on foot aren't they? Never heard of the sport \"horse ball\" but I'd give it a chance if it was on the tube. Maybe."
    author: "Bmin"
  - subject: "Re: \"Football\""
    date: 2008-08-19
    body: "\"horse ball\" is called polo, I think."
    author: "Boudewijn Rempt"
  - subject: "Re: \"Football\""
    date: 2008-08-18
    body: "You can throw the ball forward. That seems like a big difference. :)"
    author: "blauzahl"
  - subject: "Re: \"Football\""
    date: 2008-08-18
    body: "Do you mean football with the feet or football with the hands ? :-)"
    author: "anonymous cow"
  - subject: "cool"
    date: 2008-08-18
    body: "Hehe, that was a nice interview for sure!"
    author: "jospoortvliet"
  - subject: "Badly missing?"
    date: 2008-08-18
    body: "Something like this http://www.conduit-project.org/ is badly missing in KDE imho."
    author: "Bmin"
  - subject: "Re: Badly missing?"
    date: 2008-08-18
    body: "+1"
    author: "yman"
  - subject: "Re: Badly missing?"
    date: 2008-08-18
    body: "Isn't that like what Akonadi is for?\n\nI'm on gnome now and haven't even heard of conduit before, but I'm pretty sure KDE 4.x has that covered.."
    author: "ethana2"
  - subject: "Re: Badly missing?"
    date: 2008-08-18
    body: "From the Akonandi webpage:\n# Syncing mail, calendar, addressbooks to remote servers\n# Syncing with mobile devices\n\nSo it does seem that you are right. But it does seem that conduit does a bit more than that. Again, I'm just judging it from the screenshots so I could be very wrong. "
    author: "Oscar"
  - subject: "So this is why he prefers submarines"
    date: 2008-08-18
    body: "\"There are no windows.\"\n"
    author: "Jos"
  - subject: "Re: So this is why he prefers submarines"
    date: 2008-08-18
    body: "hehehehe\nI missed that. That was funny.\nSad thing is that the USN is moving to digital periscopes. That is the mast has totally outside of the ship, and that only wires go in an out. It might be useful for the USN to consider mounting other cameras on tower to watch around. I mean, in peace time, why not use the ship for a bit of exploration.  It might also come in handy during littoral operations. My understanding is both seas of china are starting to clear up, though the water in the middle east remains pretty bad."
    author: "a.c."
  - subject: "Re: So this is why he prefers submarines"
    date: 2008-08-19
    body: "> Sad thing is that the USN is moving to digital periscopes. That is the\n> mast has totally outside of the ship, and that only wires go in an out.\n\nOnly someone who has never stood hours of periscope operator on the midwatch could say that the new photonics masts are a \"sad thing\". :P\n\nI've never used a photonics mast underway so I haven't been able to compare to see whether the resolution is adequate.  But I hope to never ever see a normal periscope again. :)"
    author: "Michael Pyne"
  - subject: "Re: So this is why he prefers submarines"
    date: 2008-08-19
    body: "I did not say that the new masts are a \"a sad thing\". I said that not putting more cameras out there and doing research at the same time is a sad thing. At any one time, we have a number of subs in the oceans in locations that research vessels will not see for the next 50 years. These could be shooting pix. In particular, there is no reason to not use various wavelength lights during peacetime (obvoiusly we do not want them shining during none peacetime)."
    author: "a.c."
  - subject: "Re: So this is why he prefers submarines"
    date: 2008-08-20
    body: "Wow. Just read my original post. I did say exactly that. I did not mean it that way. I am sorry. What bugs me is that the USN is in areas that researchers will not be in for 50 years. It would be useful to have them check interesting areas and record them while there."
    author: "a.c."
  - subject: "So now I'll start with Qt (hopefully)"
    date: 2008-08-19
    body: "As a result of this interview I ordered \"C++ GUI programming with Qt 4\" by Jasmin Blanchette and Mark Summerfield from my local library. I sure hope it's good. Maybe I'll even implement that video manager/player idea sometime this side of college graduation."
    author: "yman"
  - subject: "Thanks"
    date: 2008-08-20
    body: "Thanks for People Behind KDE. I like the project. It's a good idea to introduce the people who work on KDE"
    author: "Beeema"
  - subject: "behindkde.org seems down.."
    date: 2008-08-29
    body: "This Account Has Been Suspended\nPlease contact the billing/support department as soon as possible. "
    author: "mxttie"
---
In the next <a href="http://behindkde.org/">People Behind KDE</a> interview, we stay in the United States of America (but leave in an underwater craft!) to meet a KDE developer who could be a JuKebox in another life, someone who helps you build development versions of KDE (staying on the bleeding edge without the pain!) - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/mpyne/">Michael Pyne</a>.

<!--break-->
