---
title: "KDE Commit-Digest for 17th February 2008"
date:    2008-02-24
authors:
  - "dallen"
slug:    kde-commit-digest-17th-february-2008
comments:
  - subject: "Frist Thnaks!"
    date: 2008-02-23
    body: "Many thanks, Mr Allen :)"
    author: "Anon"
  - subject: "Re: Frist Thnaks!"
    date: 2008-02-23
    body: "I'll just drop in and thank you as well. Always a good read and the work is very appreciated."
    author: "Erunno"
  - subject: "Re: First Thnaks!"
    date: 2008-02-24
    body: "I have been meaning to donate since the new laptop post, and only just got around to it. \n\nThe commit digest is more up-to-date than any magazine that goes through a media company before going to print. I donated approximately what I spend on magazines in a year :P.\n\nKeep up the good work."
    author: "alsuren"
  - subject: "Re: Frist Thnaks!"
    date: 2008-02-24
    body: "\"Frist Thnaks\" form me as wlel ;)\n\nSCNR, no spelling flame intended ... "
    author: "furanku"
  - subject: "Question"
    date: 2008-02-23
    body: "Apologies if this is a stupid question, but I couldn't find an answer on the commit-digest site (or anywhere else). But when this claims to be a digest of things that have been committed, does it mean in the casual/conventional sense (ex: \"I commit to backport some of the Plasma changes for 4.0.2 sometime next week.\") or in the version-control sense (ex: \"I committed the Plasma changes through svn this morning.\")?"
    author: "The Troy"
  - subject: "Re: Question"
    date: 2008-02-23
    body: "The latter.  If you read the digest, you'll see that underneath each change, there's a link to the diff."
    author: "Jim"
  - subject: "Re: Question"
    date: 2008-02-23
    body: "Always the version control sense :)"
    author: "John Tapsell"
  - subject: "time to get a new digest writer"
    date: 2008-02-23
    body: "personally, I like to read the commit before I upgrade, if there is nothing there worth upgrading for I wait.  This whole crappy attitude of posting it nearly a week after it is just lazy.  It's time Danny, you hand the reins over to someone who can get a digest out timely.\n\nMany home users will be thinking it doesn't matter, but for businesses it does matter to know what is in the updates before committing to them.  \n\nYou work has been appreciated, but if digest continue to be a week late, I can only see us moving to another desktop environment\n\n"
    author: "anon"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "Don't drink and write, honey. Please tell me, where are the ads on this side? Do you even have a slightest idea that the digest is brought to you by a brave man who spends nothing but his free time on this? Instead of having a walk in the sunshine? Or surfing the sea?\nOh, was this your application to take over the digest writing? Sorry, then. Not."
    author: "Digest Reader"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-25
    body: "Hey, that's not a bad idea.\nWe should have ads on this site. All the proceeds go towards the KDE project. Might be a good income source to help offset some of the costs associated with the project. Or to help the programmers out financially that need it.\n\nWe could either have banner ads, or google adwords, or something similar. Could the project developers in charge please vote on this?"
    author: "M"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-25
    body: "We discussed things like that, but there are many issues involved. Legal (we can't earn money) and social - do we want to let MS advertise here? Do we like google enough to let em make money on us? How does it affect our image? Etc.\n\nWe decided to use advertising as little as possible."
    author: "jos poortvliet"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "Sorry, i dislike your comment.\n\nI don't know, how much work this could be, but i think that, if i am in a business, than i don't take the last snapshot ...\n\nThere are \"normal\" releases for those(?) i'm waiting (next Fedora for me), but i never ever take snapshot for more than just playing with the new integrated things like i've done with the daily-release from <sorry>. 4.0.2 or 4.1.0, see the release-plan.\n\n"
    author: "Hardy"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "What annoys me on the dot is not the casual general anesthesia posting bullshit, but others starting to debate it.\n\nGuys, nothing to see here, move along!"
    author: "anonymous coward"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "whats annoys me even more are people like you who tell others who and if they should respond to anyone\nthat includes trolls"
    author: "she"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "You anonymous dorks. 1st off, if you are serious about using code from SVN like you say you are, then read all the logs and the code. 2nd, shut your pie whole. You're a whining wanker. Go buy software and get shafted. 3rd, I will repremand you just to show that I appreciate Danny and anyone's ones contribution to the community.\n\nWhining wankers... yawn..."
    author: "winter"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "\"if digest continue to be a week late, I can only see us moving to another desktop environment\"\n\nWhich other desktop environment has a weekly commit digest?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "good point! :)"
    author: "Koko"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "Windows.  Except there, the commits are to your own machine, by botnets ;)"
    author: "Lee"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "Danny is writing this on a volunteer basis! Calling him lazy for taking time out of his schedule to do this is an insult to any volunteer who has ever helped out an open source project. \n\nFurthermore, if he 'lets go of the reins' as you suggest, it's very likely that no one else will pick them up, and we'll be left with absolutely no commit digest at all. Is that what you want?\n\nIf you want instantaneous info about commits, just watch the kde-commits archive: http://lists.kde.org/?l=kde-commits&r=1&w=2"
    author: "Tray"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "Don't feed the obvious troll, people."
    author: "Jim"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "2/10.\n\nYou get four for getting so many responses, but lose one for posting the same comment last week and another for using the name \"anon\""
    author: "Dan"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "You and the other \"us\" you refer to have fun in another desktop.  We don't need your kind around theses parts.  :)"
    author: "cirehawk"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "You know, I sort of agree with this mentality. We (internally) have talked before about poisonous people in the KDE community, but this could just as well extend to users. We don't need poisonous users, and they can go elsewhere while the rest enjoy the project.\n\nI will have to think about this some more, and try to develop some sort of position statement... hrmmm..."
    author: "Troy Unrau"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "How about a trojan in Konqueror that detects troll posting to the Dot and uninstalls KDE the next time the user gains superuser privileges? Oh, so a post suggesting to trojan KDE could itself be considered trolling? Alright, one moment. I'll just install this package and then come up with something better...... Hey!? what is happ"
    author: "Martin"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "LOL ;-) (although he is probably not using Konqueror)"
    author: "Erik"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "And to be honest, I wasn't saying it in a mean spirited way.  I just hate when people try to make demands of someone who is providing them a free service.  It would be as if I were giving someone $1,000 dollars out of the kindness of my heart, but they get upset that I don't give it them at the exact moment they want it.  I'd rather they say thank you or nothing at all.  FOSS is all about choice.  If a person wants to move on to something else (no matter how silly the reason), then by all means do it."
    author: "cirehawk"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-25
    body: "At the 2007 LGM Bryce Harrington gave a presentation on this topic that was very insightful. His presentation is at http://www.bryceharrington.org/lgm07/, especially http://www.bryceharrington.org/lgm07/03_4_users_vs_contributors.svg. His recent blog entry http://bryceharrington.org/drupal/foss-win-paradox develops the same theme: users can have a negative effect on a project, and the trick is to make users stop considering themselves as entitled consumers and turn them into worthwhile contributors."
    author: "Boudewijn Rempt"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-23
    body: "This is not a reply to the parent comment, but a bit of information to help those they may have confused:\n\nThe digest is a summary of the last week's worth of commits to the KDE source repository. It's not about what's in a given release (releases happen periodically, pulled from the source repo) and it's not published to let people see the changes (aka \"commits\") before they happen but as a report on what (of significance) has changed.\n\nThe parent post is either just an outright troll or highly confused (I'm guessing the former).\n\nAnd Danny: rock on =)"
    author: "Aaron Seigo"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "I know, I shouldn't feed the troll, but I want to say that I appreciate the hard work from Danny. So please don't get discouraged by such idiots and keep on doing the digest. And if it helps to get rid of such people, take another week for the digest and let those people move to whichever desktop environment they want. It's a win for KDE if they troll somewhere else."
    author: "hias"
  - subject: "Do you even understand what the commit DIGEST is?"
    date: 2008-02-24
    body: "It's a SUMMARISED list of major changes in the KDE SVN: the DEVELOPERS source repository. It's not a packaged release detail. It's not meant for general consumption. It's not meant to advise/otherwise on upgrading. It's not meant for home users, companies or anyone not involved in development: It can't be \"LIVE\" or it wouldn't be a \"DIGEST\".\n\nIf you're running your business IT based on this digest your company is in trouble: but not in the way you think it is. If you're not, you have no business posting about what does/doesn't matter for businesses.\n\nSo... You're going to switch DE because of a delayed commit digest? Do you change web browsers when you order something online and it turns up late? Did you stumble onto the wrong site? I'm not sure you know how a computer works. \n\nIn summary: You, sir, are a moron.\n\n\n\nP.S. Danny: Keep up the good work...\nI've donated before but will send some more as compensation for putting up with this guy. Evolution owes us all an apology."
    author: "Anon"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "Ok, I'll tell you, you can get even better from the weekly commit digest, you can subscribe to kde-commits mailing list and see every single person committing to the KDE source SVN :p Much better right? No need to wait for one week"
    author: "fred"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "Duh!\n\nI don't see the Digest as being at all relevant for the purpose that you wish to use it for.  I am not saying that the information that you need shouldn't be available, only that you aren't going to find it in the Digest.\n\nIt is simply a digest of what was added to SVN.  This tends to emphasize what has been added to TRUNK while the next release is going to be from a BRANCH.  Making it a useful source of information, but not for the purpose you want to use if for.\n\nI suggest that you consult the Change Log for the release to obtain the information which you want. "
    author: "JRT"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: ">You work has been appreciated, but if digest continue to be a week late, I can \n>only see us moving to another desktop environment\nI must have fallen in a time shift and suddenly, it's the 1st of april.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "Danny, could you see how important is your work on KDE? This guy is on KDE because of Commit-Digest!\nThanks, for all the time you spend on KDE and Commit-Digest, you rock.\n\n@anon: 'us'? talk as yourself not 'us', please."
    author: "Emil Sedgh"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "Not defending Danny here but hey! You can switch to Gnome, XFCE or E17, they all have a better Commit Digest. Apart from that I think it would be good if Danny could really pass the baton to you seeing that you know it better ;) "
    author: "Bobby"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "The digest lists bleeding-edge commits.  If you use svn kde in a production environment you're insane.  This isn't the place to find changelogs for stable kde versions.  And if you really need to know what's being committed to svn in a desperate hurry, read the svn logs!\n\nThanks Danny, the digest does an excellent job of keeping informed those of us who like to follow kde development without actually ploughing through svn in detail."
    author: "Adrian Baugh"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "Welcome to the project then !! As I see you have some free time, you will be able to read what's going on at SVN and write down all the most important things. Oh wait... you are not that kind of person. Yeah, I hit the WRONG person. Those kind of people that prefer to say \"this sucks\" better than getting hands to work and help for some purpose.\n\nDanny is rocking, he does what nobody elses do, and is a hard work. Never forget as Aaron and others said that commit digest is not what you try it to be. It is just a place where you can see how things are going, but is not a release notice with a list of bugfixes or new features.\n\nSo, it is nice to before posting, you do some things:\n\n1) Inform yourself.\n2) Think.\n3) Think again.\n4) Write.\n\nMaybe 2 and 3 are sometimes hard, but hey, you can do it. Maybe you aren't of those too... in that case I completely got the dirt on the WRONG person.\n\nThanks Danny for your incredible work."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-24
    body: "I'm inclined to believe you to be little more than a spoiled, self-indulgent troll with an over-inflated ego. That you didn't even have the decency to give your name shows how much of a coward you are. You are far from any part of the community, and you fail at life. Spread your poisonous ego elsewhere. "
    author: "Maarte"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-25
    body: "Danny, you rock! Don't get insulted by such trolls! I am looking forward to the next reader's, err.. sorry, commit digest!\n\nPS: Given the number of people who donated money towards your laptop (me included) you probably know how much your effort is appreciated :)"
    author: "Mate"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-25
    body: "Don't you have anything more productive to do than light crtizising?\nAt least give your real name!\nYou suck.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: time to get a new digest writer"
    date: 2008-02-25
    body: "Casse toi, pauvre con! *\n\n... but actually Aaron might be right, and you are simply confusing the digest with some changelogs of some kde release. Very unlikely of course, as noted by Aaron himself.\nAnd the link from Boudewijn is definitively of striking actuality.\n\n* this slightly aggressive ;-) sentence was used a few days ago by the actual and very respectable french president for a much minor offence towards his own person."
    author: "nicolas"
  - subject: "Re: time to get a new digest writer"
    date: 2008-04-23
    body: "Men this kind of coments will achive only one thing.... no more digest...\n"
    author: "nuno pinheiro"
  - subject: "Kooka Development"
    date: 2008-02-23
    body: "Will Kooka be supporting the google supported open source OCR project \"Tesseract OCR\" ? http://sourceforge.net/projects/tesseract-ocr \n\nPlease!!!! We need a better OCR program for Kooka!! It'll benefit everyone, gocr is sooo bad.\n\nBelow: gocr output, from a human readable scan!\n\n\n_le Work ol J8mes S__g                    Ifwe s\u00b5e8X 0f 1he class_c_al _n Sr_1__ug _c _s no( ro _mply\nni 0Y k_nd ofrevival_st tendency, bu1 I9ther 1u su_mr that\nRn_I1 M9xwc,II                         I_e _was _IIle__esl__ _n thc idca u( a .m7TI ut gcncric\nC_9x_c_sm, oE _he k_nd rl>8t_IIn Rowe cl8imed La firld iu\nthe vrnrk ofk Corb usIfx . fndfed , _1 j kkely rhn r Rowe 's\nRe_fwing the c8r91nguf nI n renoc exh_b_rion oE _e   __>1erprelnt_on of the cl8scic81 ns 8 r_urrig in\u00e7IreInen1 to\nwurk oE _9rl Fr_edr_eh Sch_nkel a1 1I>c V_ctur_a 9nd    m9nnerisIn, was 8don1ed byS_irI_n6 ns ni lJ_u(lmr 1_iv_Idi;\nAIhe,rt Mutcum, a thn_v which mvered nll nsmcM nt   tvhilc I>e never het_itnted _n _clc0m__ modem'sm. 8nd"
    author: "Anon"
  - subject: "Re: Kooka Development"
    date: 2008-02-24
    body: "Oh dear.  That's bad :)\n\nIt's always nice to know that the FOSS world has world-class products, and so tesseract's engine would be great to have for actual OCRing.  I think we'd really need a layer on top of that which supports page layout analysis, though.  These days, any decent OCR program should be able to output a fairly close copy of the original, but translated into html (images, text, tables, etc.) and/or ODF."
    author: "Lee"
  - subject: "Re: Kooka Development"
    date: 2008-02-24
    body: "vote for the wish to add tesseract ocr to kooka.\nhttps://bugs.kde.org/show_bug.cgi?id=135253"
    author: "Anon"
  - subject: "Re: Kooka Development"
    date: 2008-02-25
    body: "Don't spit on gocr, there are testcases where it performs a lot better than Tesseract. But yes, support for Tesseract would be nice to have."
    author: "Kevin Kofler"
  - subject: "Re: Kooka Development"
    date: 2008-02-25
    body: "Maybe, but I have a lot of problems with it... but I know it is a *very* difficult task. Also, there is a lot of money to be earned if you know this kind of stuff (OCR is very hot for multiple reasons), so asking it to be done free will not yield the best results.\n\nI like gocr, but I would love to have it improved! :)"
    author: "Mate"
  - subject: "Okular"
    date: 2008-02-23
    body: "I visited the irc://irc.kde.org/#okular and asked if there was the same 4 sheets to 1 physical print out limitation as in kpdf. I was corrected that it is a kdeprint limitation which is apparently dead for kde4. instead there's a new limitation and okular can only print 1 page per sheet of paper, oh boy... can someone please fix this?!\n\n(I would if i could)"
    author: "Anon"
  - subject: "Re: Okular"
    date: 2008-02-24
    body: "Wishlist items on the the dot and IRC are likely to be missed or forgotten or misunderstood (sorry, I don't understand your issue, but you are just Anon...).\n\nPlease, please, file wishlist bug reports if you want changes. Then we can don't forget, don't miss it, and can ask you questions if we aren't sure what you want."
    author: "Brad Hards"
  - subject: "Re: Okular"
    date: 2008-02-24
    body: "for example. you have a 12 page pdf document. I want the entire document printed on 2 pages of paper. Okular would have to support printing 6 pages (of the pdf) onto 1 page of paper.\n\nfyi https://bugs.kde.org/show_bug.cgi?id=158314\n\n:) thanks for reminding me though!"
    author: "Anon"
  - subject: "Re: Okular"
    date: 2008-02-24
    body: "kdeprint isn't dead, it was replaced with QPrinter for KDE 4.0 and they are working on fixing the feature regressions for 4.1. And maybe add some more features. ;)"
    author: "Ian Monroe"
  - subject: "Re: Okular"
    date: 2008-02-25
    body: "Yes, this is an issue with the print system and not Okular.  \n\nUnder KDE3 we had KDEPrint which supported n-up printing using postscript pre-filtering.  With the switch to fully Qt based printing this was a feature we lost as it is not currently supported by Qt 4.3.  \n\nFor 4.1 I plan to re-introduce this on *nix platforms by using Cups based n-up, if Qt doesn't end up supporting it directly in Qt4.4.  Sorry lpr users, but it really is time to join the 21st century :-)\n\nUnder Mac and Windows, the native print driver and dialog are used so it depends on if the manufacturer supports it.\n\nI'll try post on the planet tonight about the printing changes in Qt 4.4."
    author: "Odysseus"
  - subject: "Re: Okular"
    date: 2008-02-25
    body: "Great - thanks for all your efforts (from a user with no printers whatsoever ;)) and for keeping us up to date!"
    author: "Anon"
  - subject: "Re: Okular"
    date: 2008-02-26
    body: "Thank you so much for your hard work!\n\nIt is already a massive improvement over qt 4.3.\n\nJust a minor thing, will there be pictures in the dialog later on? Sorry if it seems a little trivial, but I loved the fact that the KDE3 dialog had pretty pictures for collated pages and icons on all the buttons :)\n\nP.S. I tried to post the above in your blog, but the captcha doesn't work at all."
    author: "Moobuntu"
  - subject: "Re: Okular"
    date: 2008-02-26
    body: "Not my hard work, I've mostly just been patiently waiting for 4.4 to get finalised to see what the trolls are providing, while poking around the old code to see what can be salvaged.  If you want to thank someone, drop the trolls a note :-)\n\nAs for the cute pictures, I'm not sure what qt will provide in their stuff, but anything I add on I'll try get good graphcs for from the artists."
    author: "Odysseus"
  - subject: "Upload plugin is for Quanta only"
    date: 2008-02-24
    body: "Just a small correction, the upload plugin is inside Quanta's source tree and is not part of the KDevelop Platform. Its something specific for web development."
    author: "Andreas Pakulat"
  - subject: "KIO-GIObridge"
    date: 2008-02-24
    body: "\"KIO-GIObridge is an optional adapter for KIO to use the new GIO/GVFS (the successor of GNOME-VFS) to handle the protocols mentioned above.\"\n\nI think this is really cool. There is no reason why KDE and GNOME should use different implementations of the same remote protocols.\n\nI always wanted to make it possible for KDE and GNOME applications to share the same file dialogs, for consistency's sake. Qt's support for GLib makes it almost possible - but it probably wouldn't work when opening/saving non-local files. But the KIO-GIObridge should really help here.\n"
    author: "Dima"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-24
    body: "Check out gtk-engines-qt which allows gtk apps to mimic qt styles and widgets.  I shudder at the thought of using Firefox with the GTK file dialog."
    author: "T. J. Brumfield"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-24
    body: "Ehm? I don't think gtk-engines-qt changes the file dialog at all. It still the same old horrible Gnome dialog (just with a KDE look-and-feel)! If you know of a way to really change it to the KDE dialog, please let me know because it's the only major gripe I have with Firefox. (And no, Konqui just doesn't do it for me, sorry ;) )"
    author: "Quintesse"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-25
    body: "There's KGTK, but it doesn't work with all apps, IIRC it doesn't work with Firefox."
    author: "Kevin Kofler"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-25
    body: "I've looked at its kde-apps.org page: the author says it works with Firefox 1.5, but not 1.0. I don't know if it works with Firefox 2 or 3."
    author: "Kevin Kofler"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-25
    body: "For me, it works fine with FF 2."
    author: "blueget"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-24
    body: "Yes, gtk would definatly benefit from a file dialog that didn't look like utter shit."
    author: "Dan"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-24
    body: "Mostly I don't have a problem with the look of the gtk file dialog, just it's pookiness. \n\nLike in firefox, when downloading a file and wanting to open a file in a certain application. From reading a flamewar that erupted when the breadcrub navigation was added, I'm aware that you can hit ctrl-l to get an editable location bar. When I do that to be able to type \"/usr/bin/kpdf\" or whatever, I get to \"/us\" before the app attempts to lookup what I am trying to type and freezes. By the time it comes back I've already typed the remaining r/ so I end up with \"/us/r/\" where I then have to delete it. This continues until I get to /usr/bin/ where the app freezes for about 40 seconds while it attempts to enumerate all the possible applications. Once I hit \"k\" for the kpdf, the process starts over. Once I have gotten to the point of being able to type \"kpd\" it trys to auto fill in the rest of the name while I am trying. So I end up with \"/usr/bin/kpdff\" I then have to go back in and edit the name once more while the app freezes trying to look up more stuff in /usr/bin. Basically it is pretty frustrating and not a pleasant experience.\n\nThis ended up sounding like quite a rant, but my point was intended to say thanks KDE devs! I like the existing file browser. It does the right thing and lets me save or open things without getting in the way."
    author: "xian"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-24
    body: "That behaviour is pretty annoying. I've not noticed it recently but when it used to get me I discovered you could pretty much get past it by simply typing the first letters of the dirs. For example /usr/bin/kpdf needs to be entered as /u/b/k...\n\nFrustrating to say the least :)"
    author: "Anon"
  - subject: "Re: KIO-GIObridge"
    date: 2008-02-24
    body: "Just use KGTK: \n\nKGtk (Use KDE Dialogs in Gtk Apps)  \nhttp://www.kde-apps.org/content/show.php?content=36077"
    author: "Ljubomir"
  - subject: "Calm in the KDE4 camp ? :D"
    date: 2008-02-24
    body: "I was wondering, things have been somewhat quiet for the last week in the planetkde and also it seems the number of commits is not what it has been lately. Is there something happening or is it just random ? This is my curiosity as a kde4 enthusiast, not me trying to start a quarrel :)"
    author: "nikolas"
  - subject: "Re: Calm in the KDE4 camp ? :D"
    date: 2008-02-24
    body: "Actually, my previous post is a bit misleading about the number of commits now I read it again. I don't know where I saw the numbers I saw before. I guess I'm still sleepy, stupid me ... :D But it's nice to wake up and find the digest is there :D"
    author: "nikolas"
  - subject: "Napoleon's Tomb"
    date: 2008-02-24
    body: "> Stephan Kulow committed changes in /trunk/KDE/kdegames: \n>   removing calculation and napoleon's tomb - both are too much games of luck\n>   with too complex rules\n\nI was wondering why Napoleon's Tomb had vanished for the last few opensuse releases.\n\nDoes that mean Tomb is gone for good? I love that game.\n\n"
    author: "Flitcraft"
  - subject: "NOATUN"
    date: 2008-02-24
    body: "Each 2nd. KDE-update breaks the file associations with Kaffeine and sets Noatun as the default player. It's something as trivial as annoying! (in opensuse).\n\n\n\n "
    author: "joe"
  - subject: "Re: NOATUN"
    date: 2008-02-25
    body: "That's a configuration issue, where newer versions of the default file overwrite the customized users' file. Imho wrong. I hope someone can fix this the right way\n\n(eg it should work based on individual entries, not whole files - so if you did configure a certain setting, it keeps your setting, and uses the default from the system settings for all other settings. So user config files only contain the changed settings, not all settings)"
    author: "jos poortvliet"
  - subject: "Thanks guys!! - I'm really impressed. :)"
    date: 2008-02-25
    body: "I just read this: http://polishlinux.org/kde/kde-41-visual-changelog-rev-777000/\n\n\"KDE 4.1 will be what everyone expected 4.0 to be \u0097 a fully functional revolutionary Linux desktop. I took a look at the revision 777000 of this desktop environment and what you get is a visual changelog describing the current progress in terms of look and feel and the features.\"\n\nNvm the above paragraph, I just included it for completeness sake.\n\nI'm really impressed with KDE 4.1 development so far.\nIt seems that KDE is really on track to deliver everything Aaron Seigo promised it would in the keynote.\n\nThank you to everyone involved with the project. You guys are doing a great job.\n\n"
    author: "Max"
  - subject: "Re: Thanks guys!! - I'm really impressed. :)"
    date: 2008-02-25
    body: "lets keep a low profile on kde 4.1 and not hype it to death."
    author: "louis"
  - subject: "Re: Thanks guys!! - I'm really impressed. :)"
    date: 2008-02-26
    body: "I see your point.\n\nYet I think hype is what attracts new developers.\nEspecially for the next few weeks, when submissions for Google's Summer of Code are due. Let's hope we'll get many volunteers. :)\n\n"
    author: "Max"
  - subject: "Re: Thanks guys!! - I'm really impressed. :)"
    date: 2008-02-25
    body: "I'm still not impressed.\nSome basic features as double click for desktop icons, moving plasmoids in panel, hide inactive icons in dock and much more are missing.\nAnd for the plasmoids, I've plaiyed a little, and so far did not saw anything that KDE3 could not do.\n\nBUT the hope is the last one to die ;)"
    author: "Iuri"
  - subject: "One package manager to help them all?"
    date: 2008-02-25
    body: "As distros start rolling out KDE4, I think it would be great if there was a uniform graphical package manager that worked across the divide of packaging systems, a la KPackage. It would be great to have a manager that matched the features and clean design of Synaptic (not to detract from the competence that is found in Adept) and the only differences across distros would be the backend used. Would it be possible to have a system like Phonon is for sound engines, where each distro writes a backend (Ubuntu & Debian's deb, Fedora, opensuse, Mandriva, PCLinuxOS & a billion others' rpm, all the other exotic ones) that links up to the same interface? Or is this what PackageKit already intends to accomplish?\n\nIt would be wonderful if there was co-operation between the various KDE distros on one Oxygen-designed and HIG-compliant manager that would make app installation even easier than now. This would reduce duplication of effort and free up individual distro developer time to polish up some other features. Of course, this is all idealistic stuff that would probably hit roadblocks in the real world, but the free software movement is all about dreaming and working towards better stuff right? :)"
    author: "Parminder Ramesh"
  - subject: "Re: One package manager to help them all?"
    date: 2008-02-25
    body: "there's already such a project aiming to reach this goal.. packagekit... the only thing that's missing is a KDE4 GUI. Atm there exists a QT and Gtk+ GUI. Some major distros like Fedora are already thinking about including packagekit in their next major releases."
    author: "Bernhard"
  - subject: "Re: One package manager to help them all?"
    date: 2008-02-26
    body: "Thanks for the info. I've heard of packagekit, but it still seems gnome-centric - none of the screenshots on their site show a qt or kde interface. But I guess all it needs is time, so fingers crossed!"
    author: "Parminder Ramesh"
  - subject: "Re: One package manager to help them all?"
    date: 2008-02-26
    body: "http://lists.freedesktop.org/archives/packagekit/2007-October/000750.html"
    author: "Koko"
  - subject: "Okular rocks!"
    date: 2008-02-25
    body: ">Optionally, read the \"button\" form fields from poppler.\n>Enable the support for them only if the poppler version is recent enough (atm, git master of around 9 hours ago).\n\nCool, Okular clearly is pretty up-to-date ;-)"
    author: "jos poortvliet"
  - subject: "Is KDE going to be at this years Google Summer of "
    date: 2008-02-26
    body: "Is KDE going to be at this years Google Summer of Code again?\n\nhttp://developers.slashdot.org/developers/08/02/26/0134235.shtml\n\n\nI'm very curious. Looking forward to the results.. :)"
    author: "Max"
  - subject: "Re: Is KDE going to be at this years Google Summer of "
    date: 2008-02-28
    body: "*bump*"
    author: "Max"
  - subject: "Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "Who needs that? What comes next? Why not switch over to GTK and other Gnome technologies alltogether?! This would make us so unbelievably compatible and attractive for 3rd party applications, as they wouldn't have to decide between to frameworks! Just take Gnome, put a few new icons and wallpaper on top: thats the new KDE 5! Giving up identity? But we are compatible! Hell yea!\n\n(I know, this is carried to extremes)"
    author: "Stefan"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "I do understand what you're on about. DCOP was a much superior IPC system, but we changed to DBUS to have compatibility with The Foot. Now many KDE distros have to support glib which is slower than qtcore, and our awesome KIO technology will ultimately be pushed aside for someone else's copy. Isn't it silly for them to recreate our technologies and then us having to adopt their version? As non-native stuff, performance is likely to suffer. It's like NIH syndrome in reverse. The worst thing is that it's fine for us to have gtk+ technology thrust upon us, but when opensuse 11 plans to use qt for the installer, some gnomes are up in arms. How sad."
    author: "Konqi's love child"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "> DCOP was a much superior IPC system, but we changed to DBUS to have compatibility with The Foot\n\nWhile it would have been possible to continue using DCOP and probably enhancing it, the change made sense on its own since we also needed it for communication with system services.\n\nSince D-Bus replaces DCOP as the technology behind KDE's inter-application communications, I am quite sure that KDE uses it way more pervasively than any other desktop or set of applications.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "It's carried to such a ridiculous extreme that it has destroyed whatever point you were trying to make - assuming you were indeed trying to make one."
    author: "Anon"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "Well, maybe... Indeed I was trying to make a point here. I'm just concerned about the direction all this is heading. What comes next? They develop a successor for bonobo and we throw out KParts? Are we adopting Gnome base technologies one by one for the sake of the holy compatibility and sacred 3rd parties? Where will that lead us in the long run? It is indeed to fear that Gnome centric distros like Novell or RedHat will activly use these things, making the KDE Desktop as such redundant. If this would be a mutual process, like say the adopt Phonon or Solid, however it seems more a oneway street ..."
    author: "Stefan"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "> Are we adopting Gnome base technologies one by one for the sake of the holy compatibility and sacred 3rd parties?\n\nKDE does not adopt technologies for such reasons.\n\nHowever, when there is a need for a certain functionality, any candidate which matches our requirements in quality, stability and portability will be considered before starting from scratch.\n\nIf a replacement candidate for a technology we are currently maintaining ourselves emerges, it still has to fullfill our requirements to be even considered for adoption.\nIn some cases considerations such as compatibility with the current solution will also have an impact on the descision regarding adoption, e.g. the KDE3 software stack is using D-Bus for external communication and DCOP for established interaction patterns.\n\nAs KDE users it is probably not as visible to you when others adopt our achievements since from your point of view there is no change to your environment.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-27
    body: "Please don't forget that people need applications too. Locking out 50% of the apps by insisting on desktop-individuality in areas like the file-system-layer seems a bit foolish. I would question the undertaking as a whole, if thats the only point of having multiple desktops. Reminds me of the different rail-gauges in Russia and Western Europe - just a few centimeters apart, but a total disaster in \"compatibility\". \n\n\n\n\n"
    author: "nf2"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "I'm not quite sure what this is about, but I'm all for compatibility.\n Either way, you guys are doing great.\n\nAll the rants mean - including mine. That KDE is gaining more interest among the pulic. The higher the adoption rate, the higher the controversy. ]:-)\n\nControversy sparks interest!\n\nKeep up the good work guys. Every time I get frustrated at some of the Windows XP machines that I have to maintain, I see a glimmer of hope in KDE 4. I can't wait for the light at the end of the tunnel in KDE 4.1. Very soon I'll be able to take Windows XP off some of these computers and will be able to send users on their merry way with a distro based on KDE 4.1 :)\n\nOther Linux based systems I have to convince people to use. With KDE 4 people are practically beating down my door to put it on their systems. It's hard to have to tell them, that they'll have to wait for 4.1. (I'm actually waiting for the next gen distros to support it out-of-the-box for my sake. I like simple deployment ]:-) but psst, it's a secret.) The compiz fusion style effects sure help spark interest as well. (most users don't understand the difference between compiz fusion and Kwin. I don't want to bother to explain it to them, with KDE 4 I don't have to, as 4.1 will have cool Kwin effects. Yay! )\n \n\n"
    author: "Max"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "Be careful about setting up 4.1 as The Second Coming.  It should have plenty of improvements over 4.0, certainly (as 4.2 will in turn have over 4.1), but is such a large amount of work that it will doubtless introduce its own share of bugs, and many features may end up being dropped due to lack of manpower.\n"
    author: "Anon"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-26
    body: "Please do keep in mind that this is nf2's pet project, and there is no reason to believe it will ever be part of KDE proper.\n"
    author: "SadEagle"
  - subject: "Re: Rant on KIO-GIObridge"
    date: 2008-02-27
    body: "If you can write an app that uses KIO, and can then run on top of GIO as well without modification, or you can write an app that uses GIO and nothing else, wouldn't you prefer to write it for KIO?\n\nI think that is also part of the rational behind porting KDE4 to Windows, that then developers would rather develop for KDE4 and have their apps run on multiple systems without modification, than write them only for Windows."
    author: "yman"
---
In <a href="http://commit-digest.org/issues/2008-02-17/">this week's KDE Commit-Digest</a>: Configuration and layout work in <a href="http://plasma.kde.org/">Plasma</a>. A whole load of Plasma backports from trunk to the KDE 4.0 branch (for KDE 4.0.2). Plasma applets begin to be ported to use WebKit from Qt 4.4. Color blindness simulation for KMag. Work on support for button form fields, and support for encrypted ODF documents in <a href="http://okular.org/">Okular</a>. More developments in the porting and maintanence of Kooka. Remote KABC resource and an Akonadi to KCal bridge in <a href="http://pim.kde.org/akonadi/">Akonadi</a>. UPnp integration in <a href="http://kopete.kde.org/">Kopete</a>. A rewritten upload plugin for KDevPlatform (used in <a href="http://quanta.kdewebdev.org/">Quanta</a> and <a href="http://www.kdevelop.org/">KDevelop</a>). Continued work on a new projection framework in <a href="http://edu.kde.org/marble/">Marble</a>. Undo/Redo work using a "piece table" in Okteta. Optimisations in <a href="http://edu.kde.org/kalzium/">Kalzium</a>, <a href="http://amarok.kde.org/">Amarok</a>, and <a href="http://kget.sourceforge.net/">KGet</a>. A KControl module for configuring imaplib resources in <a href="http://www.mailody.net/">Mailody</a>, and a module for managing emoticon themes in KDE. Start of work on Puck, a tool to convert the Plasma XML user interface format into C++ code. Experiments with a KDE 4 version of <a href="http://kommander.kdewebdev.org/">Kommander</a>. A branch of KDEPrint to experiment with refactoring and porting to Qt 4.4 (for KDE 4.1). <a href="http://decibel.kde.org/">Decibel</a> and the Plasma "Luna" and "Trash" applets move to kdereview. KSystemLog moves into kdeadmin. Import of Smoke and Ruby Plasma bindings. <a href="http://dot.kde.org/1203436147/">KDE 3.5.9</a> and <a href="http://koffice.org/">KOffice</a> 1.9.95.3 (<a href="http://dot.kde.org/1203507166/">KOffice 2 Alpha 6</a>) are tagged for release. <a href="http://commit-digest.org/issues/2008-02-17/">Read the rest of the Digest here</a>.

<!--break-->
