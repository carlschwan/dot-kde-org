---
title: "KDE at FrOSCon this Weekend"
date:    2008-08-21
authors:
  - "aleisse"
slug:    kde-froscon-weekend
comments:
  - subject: "Stream"
    date: 2008-08-21
    body: "Hi,\nthe german Linux Magazin will offer a livestream from the main track. If you want to see Sebas talk on Saturday, look here:\nhttp://streaming.linux-magazin.de/programm_froscon08.htm\n\nBTW, here \nhttp://www.linux-magazin.de/news/akademy_2008_entwickler_gruesse_aus_belgien http://www.linux-magazin.de/news/video_geoclue_holt_geodaten_auf_den_linux_desktop\nhttp://www.linux-magazin.de/news/video_social_desktop_holt_community_auf_den_bildschirm (german)\nyou find videos from akademy."
    author: "Isabell"
  - subject: "Re: Stream"
    date: 2008-08-21
    body: "Thank you very much!\n\nI can't wait for the official vids now :)"
    author: "Tom"
  - subject: "Location"
    date: 2008-08-21
    body: "For thos who don't know where this is exactly happening:\nThe University of Applied Sciences St. Augustin is near Bonn in Germany"
    author: "Philipp"
  - subject: "Re: Location"
    date: 2008-08-21
    body: "Hell, I might go over there. 5 EUR are not exactly the amount of money for which I'll have to cancel my next holiday, and the university is quite near to where I live. And Tannenbaum is having a speech too!\n\nAnd since KDE 4.1 is wonderful, I really want to have a little more insight on features and development now :)\n"
    author: "fhd"
  - subject: "Looking forward"
    date: 2008-08-21
    body: "Last year's Froscon was a blast, so I'm really looking forward to this year's too, especially the social event :D\n\n\nMeet me there!\n\n\nPS: Here's my report from Froscon 2007:\nhttp://amarok.kde.org/blog/archives/480-I-survived-Froscon-2007!-...barely.html\n"
    author: "Mark Kretschmann"
  - subject: "KDE 4.1 rocks!"
    date: 2008-08-21
    body: "Free hugs for KDE and its developers!"
    author: "velum"
  - subject: "Any NL'ers driving going there for the Saturday?"
    date: 2008-08-21
    body: "...with a car? (that works and isn't burnt out.)\n\n(just kidding Jos)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Any NL'ers driving going there for the Saturday?"
    date: 2008-08-21
    body: "tsss, making fun of poor me... ;-)"
    author: "jospoortvliet"
  - subject: "Re: Any NL'ers driving going there for the Saturday?"
    date: 2008-08-21
    body: "tsss, making fun of poor me... ;-)"
    author: "jospoortvliet"
  - subject: "Were was the KDE room?"
    date: 2008-08-23
    body: "Just back from FroSCOn, but I couldn't find the KDE room there, and I'm on the go to the next appointment, so I couldn't attend the lectures :(\n\nThe only thing I saw was a kubuntu booth, said hi to them instead :)"
    author: "fhd"
  - subject: "Re: Were was the KDE room?"
    date: 2008-08-23
    body: "it was on the second floor, well hidden in the far bof corridor."
    author: "kajak"
---
KDE will be present at this years <a href="http://froscon.de/">FrOSCon.</a> The Free and Open Source Conference will take place this weekend on 23rd and 24th of August at the University of Applied Sciences St. Augustin.
Besides the usual booth where one can meet active members of the community, there will be a developers room where we will give talks on certain KDE related topics. On Saturday at 15:15 Sebastian Kügler will appear in the main track talking about the new concepts and techniques in KDE 4.  Talks programme is below.





<!--break-->
<h3>Schedule for the developers room</h3>

<p><b>Saturday:</b><br />
11:15 KDE Edu, Frederik Gladhorn<br />
16:30 KDE Community - How to get involved, Alexandra Leisse and Lydia Pintscher</p>

<p><b>Sunday:</b><br />
11:15 Amarok 2, Sven Krohlas and Lydia Pintscher<br />
15:15 Kubuntu - a KDE distribution, Markus Czeslinski<br />
16:30 KDE Grill - Ask questions about KDE you always wanted us to answer</p>



