---
title: "KDE 4.1 Beta 1 Released"
date:    2008-05-27
authors:
  - "wstephenson"
slug:    kde-41-beta-1-released
comments:
  - subject: "Rough guide to plasma"
    date: 2008-05-27
    body: "For those wondering how this Plasma thing works, I've written down directions how to use its various new features on my blog at http://vizZzion.org/?blogentry=817"
    author: "sebas"
  - subject: "Re: Rough guide to plasma"
    date: 2008-05-27
    body: "Very nice. Sebas, would you mind giving me permission to reproduce/reuse those for the Plasma FAQ (which should move from techbase one day or another... too little visibility there)?"
    author: "Luca Beltrame"
  - subject: "Re: Rough guide to plasma"
    date: 2008-05-28
    body: "Yes, please use it."
    author: "sebas"
  - subject: "Ars Review on KDE 4.1 Beta"
    date: 2008-05-30
    body: "I just read a brief review of KDE 4.1 Beta 1 on Ars: http://www.osnews.com/story/19803/Ars:_Plasma_Continues_to_Advance_in_KDE_4.1_Beta_1\n\nThe writer stated that the height of the plasma panel can't be adjusted, which is misleading. If one moves the mouse to the top of the new panel settings dialogue (resize/remove dialogue) then the cursor becomes a double arrow like with any other window, one can then adjust the height by pulling to the top or bottom.\nCould you please mention this in your guide sebas?"
    author: "Bobby"
  - subject: "News posted to Digg and Reddit"
    date: 2008-05-27
    body: "http://digg.com/linux_unix/KDE_Project_Ships_First_Beta_of_KDE_4_1\nhttp://www.reddit.com/info/6ky82/comments/"
    author: "JLP"
  - subject: "Woohoo!"
    date: 2008-05-27
    body: "Both the OpenSource AMD Radeon and the KDE team have presented huge milestones today. What is going on? I love this day :D Great work to all the developers involved! Looking forward to using KDE 4.1 as my primary desktop environment, replacing my KDE 3.5.9."
    author: "d2kx"
  - subject: "Re: Woohoo!"
    date: 2008-05-27
    body: "Yeah, lots of good news lately. Also MPX has been merged into xorg master : http://lists.freedesktop.org/archives/xorg/2008-May/035641.html"
    author: "JLP"
  - subject: "Re: Woohoo!"
    date: 2008-05-27
    body: "When will Qt support source events from multiple mice properly, in the sense of: I can actually use it with the usual code-less-create-more paradigm? I would love to include rotation of pieces in Palapeli based on two fingers turning around on a tablet."
    author: "Stefan Majewsky"
  - subject: "Re: Woohoo!"
    date: 2008-05-27
    body: "What are you asking? I'm not sure you've actually included a meaningful question, please explain.\n"
    author: "Richard Moore"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "Yes, he has as something meaningful. The question was:\n\"When will Qt support source events from multiple mice properly.\" I'm not much of a coder, but I understand this. Actually, I'd love to see it happen too. It'll change the way you work."
    author: "winter"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "I think he wants to know when will Qt be able to distinguish from which pointer/keyboard device the imput events are comming from and treat them separately. As far as I know Qt currently can't do that and just thinks it is all comming from one input device."
    author: "JLP"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "I thought this was X's job..."
    author: "NabLa"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "Yes, but Qt is an abstraction layer and hides platform dependent code away. It also hides such special features away, unless support for them is explicitly implemented."
    author: "panzi"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "Wrong: http://doc.trolltech.com/4.4/qtabletevent.html#uniqueId\n\nWhat would need to be done to make it nicer, would be to generalize this support to QInputEvent, now."
    author: "S."
  - subject: "Re: Woohoo!"
    date: 2008-05-27
    body: "Hopefully the OpenSource radeon driver now has hardware XRender. The proprietary FireGL driver does not, which results in abysmal \"widgets on canvas\" performance, needed by Plasma. This is on my laptop, so I don't have the option of buying new hardware (which I shouldn't have to do anyway). Proprietary drivers suck."
    author: "David Johnson"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "Excuse me, what did the oss radeond river guys do today? I cannot find anything on the web about it?!"
    author: "anonymous coward"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "http://www.phoronix.com/scan.php?page=article&item=amd_stream_released&num=1"
    author: "Jo \u00d8iongen"
  - subject: "Re: Woohoo!"
    date: 2008-05-28
    body: "That's not it, it's some proprietary SDK. :-/\n\nThe really interesting news, which is what was referred to in this thread, is this: http://airlied.livejournal.com/60180.html"
    author: "Kevin Kofler"
  - subject: "Fedora packages"
    date: 2008-05-27
    body: "Since fedora ships KDE 4 by default, it will ne nice to have packages to test this beta...\nAnybody want to step in, and make some rpm's?\n\nPS: I cannot switch to other distro, but I would like to test and report bugs, while I'm at work."
    author: "Paulo Fidalgo"
  - subject: "Re: Fedora packages"
    date: 2008-05-27
    body: "Patience grasshopper, coming soon. :)"
    author: "Rex Dieter"
  - subject: "Re: Fedora packages"
    date: 2008-05-28
    body: "It's in Rawhide now. But be warned, Fedora 10 isn't even alpha yet, so Rawhide is a bit bumpy right now."
    author: "Kevin Kofler"
  - subject: "Re: Fedora packages"
    date: 2008-05-29
    body: "Thanks for the info (and the packages). Any plans for 4.1 betas/rcs to appear in F9 repositories (testing perhaps?) or just the final release - if I've understood correctly then 4.1 should appear as an update for F9 after release like the 3.5.x updates used to in earlier Fedoras?\n\nHopefully is clear above but this is intended as a 'what are your plans'/'how do you see this working' kind of question rather than a 'give me 4.1 in Fedora 9 now!' demand :-)\n\nCheers,\nSi"
    author: "Simon"
  - subject: "Re: Fedora packages"
    date: 2008-05-29
    body: "We're planning to push it to updates-testing once it's officially released, then to the stable updates when/if testing went well. We don't want to push it to testing right now because we still need updates-testing to test 4.0.x bugfixes."
    author: "Kevin Kofler"
  - subject: "Re: Fedora packages"
    date: 2008-05-29
    body: "Makes sense. Thanks for the reply. I'm liking KDE 4.0.x on Fedora btw, seemed like a bold move making it default in Fedora 9 but it is working for me"
    author: "Simon"
  - subject: "Re: Fedora packages"
    date: 2008-05-29
    body: "All you need to do is the following on an up-to-date Fedora 9 system:\n\nyum update 'kde*' soprano 'extragear*' --enablerepo=rawhide\n\nOnly the new kde 4.1 betas will be pulled in. I have been using this for a day and have to say that I am yet to find bugs in anything but plasma and possibly, kwin. KMail works very well with disconnected IMAP.\n"
    author: "Ravi"
  - subject: "Re: Fedora packages"
    date: 2008-05-29
    body: "Good point, cheers. I think I'll discipline myself to hold off for a few days though until my main system is up and running again (from hardware breakage) then I can afford to try this (or even full rawhide) on my old laptop, which to my surprise runs kde4 nicely, even the composite stuff"
    author: "Simon"
  - subject: "Mistake on site"
    date: 2008-05-27
    body: "\"On 29th May 20008\"... Seems a bit TOO behind schedule for my liking."
    author: "Paul"
  - subject: "Re: Mistake on site"
    date: 2008-05-27
    body: "Fixed, ta."
    author: "Will Stephenson"
  - subject: "KDE PIM"
    date: 2008-05-27
    body: "How stable is KDE PIM for KDE 4? \nWould you guys recommend it over the KDE 3 version?"
    author: "ad"
  - subject: "Re: KDE PIM"
    date: 2008-05-27
    body: "I have a pretty recent checkout of KDE PIM and I've had very few problems, everything works as expected, it hasn't eaten any of my emails. The only problems I encountered were in trying to add new feeds to akregator manually, but once I exported my feeds from kde3 and into kde4 everything works great."
    author: "Aaron"
  - subject: "Re: KDE PIM"
    date: 2008-05-27
    body: "Great :) Are you using IMAP by the way?"
    author: "ad"
  - subject: "Re: KDE PIM"
    date: 2008-05-27
    body: "I briefly (~5 min) tested disconnected IMAP on KMail from trunk and seems to work as expected. I'll try to test more during next weekend."
    author: "Luca Beltrame"
  - subject: "Re: KDE PIM"
    date: 2008-05-29
    body: "I have been using it for my mailing lists for about a month now and it's been mostly fine. Minor issues but never has it chewed on my mail! :-)"
    author: "Kishore"
  - subject: "Re: KDE PIM"
    date: 2008-05-29
    body: "I have been using it for my mailing lists for about a month now and it's been mostly fine. Minor issues but never has it chewed on my mail! :-)"
    author: "Kishore"
  - subject: "Re: KDE PIM"
    date: 2008-05-28
    body: "It is quite stable but not absolutely. For example, every time I try to open an attached image in on of my emails... CRASH!\n\nHowever, his is the only problem I've found so far."
    author: "Damnshock"
  - subject: "Re: KDE PIM"
    date: 2008-05-28
    body: "Keep your eyes out for a kde pim krush day, in which we will look for bugs."
    author: "A. L. Spehr"
  - subject: "krunner question"
    date: 2008-05-27
    body: "does the new krunner have the possibility to run a program as another user? i could not find this when i tried the beta.\n\nthanks to all the developers, beta 1 looks great!"
    author: "taerde"
  - subject: "C#"
    date: 2008-05-27
    body: "we need a plasma binding for c# :-)"
    author: "Darwin Reynoso"
  - subject: "Re: C#"
    date: 2008-05-27
    body: "That's perfectly possible. However, the current ScriptEngine api doesn't allow you to do very much, and only C++ is supposed to use the full Plasma api. C# isn't really what I would call a 'scripting language' anyway, and really people will expect to just be able to use the C++ api. So technically it is possible, but I don't think it would fit in very well with Aaron and the Plasma team's idea about how non-C++ languages should work in plasma.\n"
    author: "Richard Dale"
  - subject: "Haskell"
    date: 2008-05-27
    body: "Yeap C# is not scripting at all. I'd much rather get Haskell bindings. \n\nOf the all the scripting languages with plasma bindings available, is any of them statically and strongly typed? "
    author: "ad"
  - subject: "Re: C#"
    date: 2008-05-27
    body: "Are there any examples of ScriptEngine usage with some languages? I'd like to get my feet wet with Plasma using Python."
    author: "Luca Beltrame"
  - subject: "Re: C#"
    date: 2008-05-27
    body: "+1"
    author: "Jonathan Thomas"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "Seconded. Thanks. :)"
    author: "S."
  - subject: "Re: C#"
    date: 2008-05-28
    body: "Well, SuperKaramba [1] integrates as ScriptEngine into Plasma and allows to use Python, Ruby and JavaScript.\n\n[1] http://techbase.kde.org/Development/Tutorials/SuperKaramba\n"
    author: "Sebastian Sauer"
  - subject: "Re: C#"
    date: 2008-05-27
    body: "We also need more developers working on C# bindings. (Just in case we have enough and I'm not aware of that, we need even more developers. Remember Steve Ballmer's well-known speech.)"
    author: "Stefan Majewsky"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "Yes, more bindings developers are always very welcome. At the moment Arno Rehn and myself are mainly doing the C# development. You don't have to actually work on the bindings themselves, there are plenty of things to do, like translating the Qt C++ examples to Qyoto/C#, or adding docs to the TechBase wiki.\n\nThe C# bindings use the same 'Smoke' library as the Ruby and PHP bindings, and recently Arno has made a major improvement to the way Smoke works, which will allow us to wrap a whole pile of KDE4 apis. Arno wouldn't have starting working on bindings if the C# bindings didn't exist, and now he has made a great contribution to improving both the PHP and Ruby bindings too. So one reason I wish people wouldn't keep being rude about Mono/C# is that this is an example of how the C# bindings development has helped the KDE4 bindings 'eco system'. If you don't like C#, please use other languages like Python or Ruby (we really don't mind), but as a language geek I think it has some nice features and a 'personality' of its own when used for Qt/KDE programming."
    author: "Richard Dale"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "It is great you have large amounts of code sharing between the languages and have someone hacking on Smoke. QtRuby rocks so much. :) It's so much fun to code in."
    author: "Ian Monroe"
  - subject: "Re: C#"
    date: 2008-05-27
    body: "Just keep it out of core.  Just because Gnome swallowed the Trojan Horse doesn't mean that KDE has to.  Keep that filth off my hard drive.\n\nLua bindings on the other hand . . .\n"
    author: "Velvet Elvis"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "+1, agreed. Better languages FTW."
    author: "Satin Elvis :P"
  - subject: "Re: C#"
    date: 2008-05-29
    body: "+1, I don't want Mono on my harddrive."
    author: "noorker"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "Why do we need a binding for that M$ language? What's wrong with C++?"
    author: "Kevin Kofler"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "Are you claiming that C++ is as easy to write stuff in as C#?"
    author: "Anon"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "C# is not what a linux or macOSX user may want to have in his\nsystem, and there ia plenty of cause for this.\n\nYou're however always free to do yourself bindings for C#, cobol,\nfortran and logo languages, nobody will stop you."
    author: "Anon"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "i hate c++ and i'm pretty sure there's others like me. anyway just because c#\nwas conceived by microsuk it doesn't mean that we linuxers can't use it and just for the records i hate windoze so much that  i even removed all the windows from my house all i have it's wall:-)"
    author: "Darwin Reynoso"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "Um, read the reply Richard Dale already provided. The C# bindings help out the QtRuby bindings, so they are good in my book.\n\nAlso insert generic \"people can spend their free time doing what they want, who the heck are you to say it's a waste of resources which aren't yours\"."
    author: "Ian Monroe"
  - subject: "Re: C#"
    date: 2008-06-01
    body: "it's not just wasting personal resources, it's actively helping out a company that stands for everything we as a community are trying to avoid. in general I don't like people complaining about open source either, but in this case, they are actually standing up for it."
    author: "mark dufour"
  - subject: "Re: C#"
    date: 2008-06-02
    body: "I'm sorry, I strongly disagree that I'm 'actively helping Microsoft'. And I couldn't care less about 'open source', as I write Free Software, which isn't the same thing.\n\nWhat we're actually doing is allowing existing C# programmers to write code using the Qt and KDE apis. I would say that we are helping them to transition away from the Microsoft 'eco system' of IDEs and APIs such as Visual Studio and Win32, Winforms etc, and move to tools and frameworks based on Free Software.\n\nIt takes much more effort to learn a large framework such as Qt, than it does to switch to another language, such as going from C# to Java. Hence, once these former Microsoft C# programmers have learned how to write Qt/KDE code in C#, they can pick up C++, Python or whatever fairly easily if they want to."
    author: "Richard Dale"
  - subject: "Re: C#"
    date: 2008-05-28
    body: "The Qt guys could give you a huge list, they made quite some effort to improve on it. For some reason Qt is not really C++ but requires the code to be pre-processed. \n\nBut come on, can't you think of anything? Type (un)safety? Really crappy polymorphism support?  What other languages do you know?"
    author: "ad"
  - subject: "Konqueror and WebKit"
    date: 2008-05-27
    body: "In the announcement it says that \"KHTML get a speed boost from anticipatory resource loading, while WebKit... is added to Plasma...\"\n\nSo is Konqueror going to have WebKit or KHTML?"
    author: "Zubin Parihar"
  - subject: "Re: Konqueror and WebKit"
    date: 2008-05-27
    body: "There is a webkitpart available which you can select at \"File Associations\" in systemsettings to be the default renderer for embedded HTML contents (such as the web pages in HTML).\n\nPlasma makes use of Webkit to render Mac OS X Dashboard Widgets (as the quoted text states...). That has nothing to do with Konqueror."
    author: "Stefan Majewsky"
  - subject: "Konqueror ---> khtml"
    date: 2008-05-28
    body: "Konqueror uses KHTML, Plasma uses WebKit. \n\n(Ok, you can do bizarre things and use webkit under Konqueror, but you'll lose lots of speed optimizations and other stuff.)"
    author: "anon"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-28
    body: "Come on guys, just choose one renderer and let it be webkit. I have a lot more compat issues with konqueror than with safari. Webdev's don't give a damn about khtml compatibility. They do about webkit. Qt choose webkit. It's only logical to follow. Performance? nice, but compatibility is more important!\n\nJust my 2 ct."
    author: "Fred"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-28
    body: "\"Come on guys, just choose one renderer\"\n\nWhy?"
    author: "Anon"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-28
    body: "> Webdev's don't give a damn about khtml compatibility. They do about webkit.\n\nThey don't. They care about Safari. Incidentally having the same engine does not solve the problem. If at all it makes it easier to \"lie\" about your real browser identity."
    author: "Kevin Krammer"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "\"They don't. They care about Safari. Incidentally having the same engine does not solve the problem.\"\n\nYou're describing something which doesn't get us any further forward in order to try and tell us that the status quo is OK. It most decidedly isn't. The fact is that bug-for-bug compatibility with Safari does count for quite a bit for exactly the reason that the parent has stated - developers care about market share. Do some web developers use user agent strings? Yes they do, but as people use libraries for HTML and JavaScript and write less of it, and owing to the complexity of AJAX, they're being used less and less. The problems now are all about bugs and quirks between different engines. KHTML's bug list is down to user agent strings. Using WebKit gives KDE a far wider pool of web sites that will render well without having to jump on every mole hill in the field, and for those that don't, it gives web developers a far easier avenue to fix their sites and have it render well in any WebKit browser without having to think about it too much.\n\nContinuing to use KHTML isn't going to get us any further forward on that front, and the past few years of Konqueror's limited usage as a web browser should have taught us all that."
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "The problem is that you don't get bug for bug compatibility with Safari at all just by using WebKit. WebKit is just the rendering engine of Safari, everything else (e.g. client <-> server communication) can (and already does) behave differently between different browsers using WebKit."
    author: "Anon"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "\"The problem is that you don't get bug for bug compatibility with Safari at all just by using WebKit.\"\n\nThe point is that you have a pretty good shot at it. The situation now is that an undermanned group of people (that are sometimes pretty unresponsive, it has to be said) are having to run around fixing bugs in a rendering engine that few people use, and corner cases are always......just around the corner. There can be no end game to KHTML in terms of it becoming a widely used engine where it will render the vast majority of web pages for people in a confident fashion and where you'll get a KDE web browser that, quite frankly, will be of any use to people.\n\n\"WebKit is just the rendering engine of Safari, everything else (e.g. client <-> server communication) can (and already does) behave differently\"\n\nI don't see that being a problem in all honesty. The corner cases are in the engine itself. You can work yourself to a set of known issues, and the advantage is that Trolltech are taking the workload on for this."
    author: "Segedunum"
  - subject: "no magic pony"
    date: 2008-05-29
    body: "> The point is that you have a pretty good shot at it.\n\nNo you don't. You don't magically get a pony by using QtWebKit. It's a port. It is NOT bug-for-bug compatible with Safari Webkit or feature-for-feature compatible with Safari Webkit. \n\n> The situation now is that an undermanned group of people\n\nIf that's going to be your excuse for being unhappy, switch back to MS Windows and just don't use KDE at all. I think only plasma and amorak (which isn't even part of kde proper!) don't fall into the \"undermanned\" category. Most of the big apps, and the core libs, not just khtml fall into the \"need more developers\" category. If you want tons of developers and professional marketing, don't use free software.\n\n> \"WebKit is just the rendering engine of Safari, everything else (e.g. client > <-> server communication) can (and already does) behave differently\"\n>  \n> I don't see that being a problem in all honesty.\n\nThis person is trying to point out /why/ it isn't bug-for-bug compatible. And what all is going to be different from Safari Webkit. And that it is a problem. QtWebkit is a port of safari webkit, and would have to be ported to KDE. Doing so will generate all sorts of your \"corner cases\". And you already claim there aren't enough khtml developers to deal with what is out there.\n\nSo go back to Windows, and be sure to FUD khtml and plasma on your way out. As everyone else in these dot comments seems to be happy to do so.  :P\n\n"
    author: "anony"
  - subject: "Re: no magic pony"
    date: 2008-05-30
    body: "\"No you don't. You don't magically get a pony by using QtWebKit. It's a port.\"\n\n*Yes you do*. Repeating the above is not going to magically make it all go away. Yes, it's a port (from the same codebase no less) and that's exactly the reason why you have a shot at it rather than in some totally different and rarely used 'fork' in KHTML that produces corner cases right, left and centre that aren't going to be solved and have no chance of being so.\n\n\"If that's going to be your excuse for being unhappy, switch back to MS Windows and just don't use KDE at all.\"\n\nBoo, hoo, hoo. I don't have to. The fact is that you're never, never have and aren't, going to make KHTML a well supported rendering engine that will be of use to the vast majority of users and web developers out there. The engine that will be used most prevalently within KDE, and in any KDE browser, is the one that has the most support in terms of web pages that people can view well, and the past few years has taught us that that isn't be KHTML. Trying to attack QtWebKit won't change that problem for KDE's users, or KDE's developers trying to get a rendering engine that won't cause them problems for them and their users.\n\nKHTML is a very small island, that hasn't changed and isn't going to.\n\n\"I think only plasma and amorak (which isn't even part of kde proper!)\"\n\nRrrrrrrrrrrrright.\n\n\"QtWebkit is a port of safari webkit, and would have to be ported to KDE. Doing so will generate all sorts of your \"corner cases\".\"\n\nWhat are you talking about? QtWebKit exists now, will get developed much further at a pace that KHTML cannot match via upstream code from Trollech, and further upstream from WebKit, and will simply be reused as every other part of Qt is in KDE these days. You make it sound as if there is some insurmountable mountain to get over in terms of porting a Qt component to KDE, which is just plain daft.\n\n\"Doing so will generate all sorts of your \"corner cases\".\"\n\nNo it won't, and any issues you might have will be far, far, far, far, far smaller than trying to jump on every mole hill in the field and investigate every bug report about a particular site that isn't rendering properly in KHTML. They are exceptionally difficult to debug. You're not going to get any help from any web developers or anyone else in the form of upstream code on that score.\n\n\"So go back to Windows, and be sure to FUD khtml and plasma on your way out.\"\n\nFeel free to ignore the issues as KDE's users and developers leave KHTML behind in favour of something that works for users, and more importantly, has a far, far better chance of continuing to work in the future. The robust 'defence' of KHTML is just a case of 'our ego is bigger than yours' rather than having the best interests of KDE's users and wider developer community at heart."
    author: "Segedunum"
  - subject: "Re: no magic pony"
    date: 2008-05-30
    body: "you know the problem is if QWebKit was one tenth as good as you sad trolls touted it for monthes, it would stand on its own, just like KHTML does without any of this silly propaganda.\n\nYou wouldn't need 30+ lines of strange and insane drivel that seem to obey to some kind of personal obsession (sorry).\n\nThe truth is current QWebKit suck. It's slow, buggy, and incomplete.\nThis state of affair might change. It might not.\n\nIt's not like any Trolltech employee as a significant role on the WebKit project or any bearing on its future.\n \nThey are all just stub-fillers for features made by Apple employees.\n\nHardly exciting, and certainly not a guarantee of a perrenial and fully functional \"product\".\n"
    author: "D."
  - subject: "Re: no magic pony"
    date: 2008-05-30
    body: "\"you know the problem is if QWebKit was one tenth as good as you sad trolls touted it for monthes, it would stand on its own, just like KHTML does without any of this silly propaganda.\"\n\nVery, very, very few people use KHTML at the moment, and it's fair to say that most of KDE's users use Firefox. That's for a reason. Additionally, KDE developers get something that works with QtGraphicsView.\n\nDo the maths. You're right about one thing. KHTML does stand on its own. Very alone, and that's the problem.\n\n\"You wouldn't need 30+ lines of strange and insane drivel that seem to obey to some kind of personal obsession (sorry).\"\n\n*Shrugs shoulders*. If you're willing to debate it I'm all ears. I'll condense it into something that might make it through your head better - the market decides on the basis of what works best for them, and based on the evidence of the past few years they aren't going to pick KHTML.\n\n\"The truth is current QWebKit suck. It's slow, buggy, and incomplete.\"\n\nIf you have something to back that up, I'm all ears. There's a lot of upstream code going into it - most if it from the very people who started KHTML in the first place!!\n\n\"It's not like any Trolltech employee as a significant role on the WebKit project or any bearing on its future.\"\n\nPfffffffffff. Yer, you're right. Trolltech, Nokia, Apple and all these other people who contribute code to it are just wasting their time when you compare it to the powerhouse of development that is KHTML.\n\n\"They are all just stub-fillers for features made by Apple employees.\"\n\nThat's the usual sad line trotted out by Harri Porten and a few others, that WebKit is entirely dependant on the empty stubs that Apple have left for their own features which Steve Jobs has asked for by next week. Well, as I said the market decides and the proof will be in the eating. I don't see QtWebKit having any issues thus far."
    author: "Segedunum"
  - subject: "Re: no magic pony"
    date: 2008-06-10
    body: "See, but virtually nobody uses QtWebKit.  Plasma is about the only application based on it.  You really don't seem to understand that a port is not an equivelency, and a library is not an application.  \"Using QtWebKit\" is not going to suddenly have the contents of a Konqueror browser look the same as a Safari browser (which does *not* use QtWebKit... it uses WebKit)... it will never do so.  Once again, it isn't a magic pony.  It's a kit to build your own pony, and the results due to things like different font rendering, network interface and base widgets will mean the two will never look or act the same.  Safari doesn't use KIO\n\nQtWebKit is being worked on, and it may replace KHTML at some point, but don't ever think that it will make a bug for bug Safari clone (nor will Epiphany, Android, S60 or any other ported WebKit project).  It is already different than WebKit, and attaching it to a different system with different IO system and rendering system will result in a different application no matter how close you want it to be.\n\nOtherwise, if it were this lego like simplicity you seem to think it is... why would it take any effort and new code in the ongoing effort to port it?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: no magic pony"
    date: 2008-05-30
    body: "Come on guys, why you all are fighthing each other? The fact is: QtWebKit is there, it is not (yet) as good as Safari with WebKit, it might be distributed as KPart for KDE 4.1, but seems the WebKit KPart will only be ready for daily usage for 4.2 onwards.\n\nMeanwhile, KDE will still ship KHTML as the default for Konqueror, providing the best for the user. And even after 4.2, users will be given a choice between that two, I don't know which one will be the default. Those who still want the uber performance of KHTML or those who want compatibility with each website out there.\n\nJust my personal opinion: QtWebKit still needs to mature first, for compatibility, I really should rely on Firefox/Gecko (I try QtWebKit from time to time)"
    author: "fred"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "\"The situation now is that an undermanned group of people (that are sometimes pretty unresponsive, it has to be said)\"\n\nI just hope that the KHTML devs can successfully sync directory structure of KHTML and WebKit (and also helped by a GSoC to port WebKit's SVG to KDE) so that both projects can synchronize each other easily (but I guess WebKit won't bother to sync with KHTML...). I guess it would be the best path for KHTML.\n\nAnd also, having KHTML in our tree IMO won't hurt, WebKit is good, but KHTML  is also progressing very nicely. I also hope with the availability of KHTML on Windows can significantly grow KHTML market share.\n\nAnd also, don't underestimate a small team. I'm so grateful that even though WebKit is there, the KHTML devs still maintain and develop their own baby. As a comparison, just imagine if KOffice devs had dropped KOffice in favor of OOo long time ago, we would not have a hope that someday we can have a viable alternative to a slow and resource intensive office suite.\n\n"
    author: "fred"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "\"I just hope that the KHTML devs can successfully sync directory structure of KHTML and WebKit\"\n\nThey can't. KHTML is now a fork of WebKit, and as Zack Rusin puts it, not a terribly good one at that. "
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "Yish, people can actually buy THAT line?\n\nSad.\n"
    author: "SadEagle"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "\"Yish, people can actually buy THAT line?\"\n\nYes, because the proof as they say is in the eating. Take a look at the last few years. KHTML has just not emerged into an engine of sufficient reliability and quality for people, and developers, to use confidently enough, and there is no reason at all to believe that situation will change.\n\nI'm not entirely sure how often that needs to be repeated."
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "Repeating it often doesn't make it any less of nonsense. It's completely and utterly absurd.\n\n"
    author: "SadEagle"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-31
    body: "\"Repeating it often doesn't make it any less of nonsense.\"\n\nUh huh. Repeating it isn't going to make it go away.\n\n\"It's completely and utterly absurd.\"\n\nWhy? I don't use KHTML because it has a lot of widely recorded issues with browsing a lot of web sites on a day-to-day basis, and KDE developers outside the world of KHTML are reluctant to use it because of that. There's no reason at all that you can point to as to why that situation is going to change.\n\nThat is *why* it's not absurd. Did that not become clear at any point?"
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "\"They can't. KHTML is now a fork of WebKit, and as Zack Rusin puts it, not a terribly good one at that.\"\n\nNothing is impossible ;) Harri Porten is doing that job (you can see from KDE 4.1 Feature Plan in techbase), I don't know whether it will be ready for 4.1. Also we get one SoC project to port WebKit SVG to KDE/KHTML, seems that it will tremendously help ;)."
    author: "Anon"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "Don't forget that the status quo is the reason why we are able to choose between different browsers since else everybody would be on IE which is still the leading browser.\n\nTo if-elif-else Browsers at webpages is not the solution but the problem since that list will always exclude some users and may it only those that depend on the IBM blindreader or another kinder of useragent.\n\nIf you look a bit more close at things like ACID1+2+3+n, IE8 quirks vs standard and so on, you may also note, that the general goal is to be independend of browser-implementations and this is the only correct way to go in the long run to prevent incompatibility, to easier the job of webdesigners, to prevent to exclude smaller user-groups and to have real interoperability.\n"
    author: "other anon"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "\"Don't forget that the status quo is the reason why we are able to choose between different browsers since else everybody would be on IE which is still the leading browser.\"\n\nThere is a balance to be struck between having a choice of rendering engines that open up other platforms for people, and having something that, uhm, works. The status quo just doesn't work for KDE users currently."
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-28
    body: "For compatibility, I'd rather go for Firefox/Gecko (or even IE, I bet 99% websites out there is compatible with IE :D). I still see some compatibility issues with Safari/WebKit. Also, there is a recent news that a guy from Mozilla is working for Qt/Gecko.\n\nFor performance, KHTML is still the winner (especially the latest KHTML from trunk). I still use Konqueror/KHTML as my main browser since it serves me well for my 99% browsing need. And I just fire Firefox when I encounter site which is not compatible with KHTML.\n"
    author: "Another fred"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "Same for me. The only place where I need Firefox is the Wordpress post editor, but afaik KHTML now has WYSIWYG editor support, so this might be solved already."
    author: "Stefan Majewsky"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-06-01
    body: "I'm also almost exclusively using Konqueror / KHTML to browse the web, it's simple the fastest I could find and has the nicest rendering in my eyes, leading the pack by a large margin.\n\nUnfortunately ECMAScript / JavaScript support with KDE 3.5's KJS is a bit flaky, but as far as I understand that's not directly KHTML's fould.\n\nI'm looking forward to KDE 4.1 and I'm curious to see how Konqueror has improved. :-)\n\nIn case of compatibility problems with web sites I resort to Firefox, but it's just too slow for daily work and many pages get rendered just plain ugly... Maybe Firefox 3 will improve something on that front, I've not tested its betas yet."
    author: "Gunter Ohrner"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "Last I heard, qt's webkit was using the same user agent that konqueror was.\nChange yours and then you can stop complaining, that's all the site wants.\nOf course, then it will look like you're using something that isn't konqueror, so you might as well just go ahead and install internet explorer. \n\nWeb developers seem to care very little about standards. And Firefox has written its own. Picking which rendering standard to implement is non-trivial. Deal with that for a while then come back and complain about compatibility.\n\np.s. qt's webkit != safari's webkit\n\n"
    author: "Anon"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-29
    body: "\"Last I heard, qt's webkit was using the same user agent that konqueror was.\nChange yours and then you can stop complaining, that's all the site wants.......p.s. qt's webkit != safari's webkit\"\n\nI'm afraid that this is the myth that lots of people want to perpetuate, but it simply isn't true I'm afraid. Using the same WebKit engine and code allows for bug-for-bug compatibility with Safari, and with greater market share that counts for a lot, and it's far easier to drive market share up together for the engine. That's why Nokia and lots of others are using it.\n\nPeruse through KHTL's bug list and you'll see that the problems arise from differences and quirks between how KHTML handles certain elements and other engines. Very few, if any at all, of the real problems are down to user agent strings as some people want to pretend."
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "\"Using the same WebKit engine and code allows for bug-for-bug compatibility with Safari\" \n\nI'm pretty sure this isn't and will never be true.  http://zecke.blogspot.com/2008/01/joys-of-debugging-webkit.html \nThat doesn't paint a pretty picture."
    author: "MamiyaOtaru"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-05-30
    body: "HTML and JavaScript engines never are pretty. Debugging issues with any engine is damn hard, whether it be WebKit or KHTML. In the end' it's a question of what ends up being easiest to work with in the long run, and gives better results.\n\nWhat happened there was they GIT merged a new branch into QtWebKit, got pointed to a known bug by the Apple people (which affected nightly builds of WebKit and Safari as well) and ended up being OK. In reality, they didn't need to resolve and patch this themselves although they did need to find out what was happening in their port. Try doing this with your own engine that no one else works on."
    author: "Segedunum"
  - subject: "Re: Konqueror ---> webkit"
    date: 2008-08-01
    body: "Unfortunately, this doesn't prove your point at all.\n\nI have been developing web browser components for 9 years. Yes, all the way back when HTML4 was brand new and AJAX wasn't even AJAX (it was all \"DHTML\" :)\n\nSimply put, having Konqueror use WebKit means that Konqueror and other QtWebKit embedding applications will be running the same rendering engine. This means, if a site fails to load in one, it will fail in the other. The current state of affairs is that Konqueror renders it one way using KHTML (note, badly most of the time) and QtWebKit from a very recent WebKit build (~SquirrelFish checkin time) will render it very nicely. And 3x faster.\n\nTo say that it will be bug-for-bug compatibility, is wrong, that is for sure - the abstraction of QtWebKit is different to Safari, so you may see bugs in Qt classes, but then you will also see these in Konqueror, perhaps hidden behind different, confused codepaths. But, WebKit is maintained by Nokia (alone as well as through Trolltech for QtWebKit), Apple and a large team of developers, and the bugfixes and better abstractions will be committed in, and there will be more concerned developers working on WebKit than there are on KHTML.\n\nIn fact the blog URL you posted just goes to prove THAT :)\n\nThe major benefits will be that Konqueror will develop new features in lockstep with WebKit, and not be a second-rate cousin implementing it's own development path. Rendering, DOM, Javascript and bugfixes can be consolidated, and resource usage reduced (why load two whole rendering engines when you can use one?). Konqueror could concentrate on being the best wrapper around a browser it can be, instead of trying to knock against the status quo. There is no benefit in writing The Best HTML Renderer That Nobody But Konqueror Uses.\n\nI think KDE developers should at least produce some affirmation of their stance on this, and correct the hundreds of not thousands of articles which proclaim that Konqueror is \"based on WebKit\", when in fact it is based on KHTML which is where WebKit came from. If there is no plan to support WebKit Actual as Konqueror's renderer, then say so. Tell us all, so we can start porting the surrounding Konqueror components into a WebKit-based browser :)"
    author: "Matt Sealey, Genesi USA"
  - subject: "Re: Konqueror ---> khtml"
    date: 2008-05-28
    body: "Hmm is flash supported yet? Support for swfdec and/or gnash would be cool...could u puhleeeez cute em? :)"
    author: "hihi"
  - subject: "flash is written to work on firefox"
    date: 2008-05-29
    body: "and not really anywhere else\nat least with newer versions\nalso, they don't support 64bit systems\n\ncomplain to them\n\nsorry folks, firefox is THE monopoly today\n\n(but yes, it does work, depending on what version you use)"
    author: "anon"
  - subject: "Re: flash is written to work on firefox"
    date: 2008-05-29
    body: "Read again. He was asking for Qt ports/adaptions of swfdec and gnash, which aren't the binary trash Adobe tosses out."
    author: "Anon"
  - subject: "Re: Konqueror and WebKit"
    date: 2008-05-28
    body: "Konqueror can use both KHTML and WebKit, I believe, though WebKit plugin support is still experimental. The added support of WebKit for Plasma is probably for better compatibility with OS X Dashboard widgets."
    author: "Michel S."
  - subject: "Re: Konqueror and WebKit"
    date: 2008-05-28
    body: "Konqueror has always been technically renderer agnostic. Which is good, since essentially the \"free market\" (in the form of distros and users) will decide what ends up being the primary renderer of Konqueror. It would've been ugly if what renderer Konqueror used had to be a top-down decision... \n\nJudging from what I've seen blogged about WebKit in Qt 4.5, it sounds like WebKit won't really be ready until then (Qt 4.5 will be probably be late 2008)."
    author: "Ian Monroe"
  - subject: "Thanks KDE for all your hard work & perseverance!"
    date: 2008-05-27
    body: "That is all :)"
    author: "Martin Fitzpatrick"
  - subject: "Re: Thanks KDE for all your hard work & perseverance!"
    date: 2008-05-28
    body: "+1"
    author: "Bram"
  - subject: "Re: Thanks KDE for all your hard work & perseverance!"
    date: 2008-05-29
    body: "+999999\n\n(I learned)"
    author: "Riddle"
  - subject: "Missing Bits"
    date: 2008-05-27
    body: "First of all thank you all who are involved in this wonderful work and for another important beta release.\n\nThere are a few things that I am personally missing, they are not vital but nonetheless missing.\nFlickr - could somebody tell me where Flickr disappeared to?\nThumbnail preview (composite) doesn't seems to be working although it's turned on and the KWin compositing effects has brought my system to a crawl once again although it was quite fast with previous builds. Anyone here has similar problems?\n"
    author: "Bobby"
  - subject: "no proxy support :'("
    date: 2008-05-27
    body: "Well... I still don't see any word on proxy support in the release notes. More is a pity, but I cannot even try KDE 4.x in a reasonable fashion (well, that is try KDE and KDE programs) without fixed proxy support. I could always use firefox, pidgin, liferea, thunderbird  and other non-KDE programs to access the net, but in that case I don't see the point of using KDE. I hope they will fix this soon."
    author: "Dmitriy Kropivnitskiy"
  - subject: "Re: no proxy support :'("
    date: 2008-05-27
    body: "+1, please don't release another version of kde4 without proxy support, I'd really like to use kde4, but no internet access from kde apps is a deal breaker for me.\nApart from this, I'm really looking forward to trying it. Thanks to all the KDE devs."
    author: "Patrick Stewart"
  - subject: "Re: no proxy support :'("
    date: 2008-05-27
    body: "+1"
    author: "proxy"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "+1"
    author: "yeah"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "+1"
    author: "same here"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "I do not use proxy.. but: +1\nI do not understand how a system like KDE can ship without such important feature."
    author: "Iuri Fiedoruk"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "+1 (same for me)"
    author: "Stefan Majewsky"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "+1"
    author: "Mark"
  - subject: "Re: no proxy support :'("
    date: 2008-05-29
    body: "+1: I don't use proxy, but it does need to be implemented.\n"
    author: "Riddle"
  - subject: "Re: no proxy support :'("
    date: 2008-06-13
    body: "+1"
    author: "tomek"
  - subject: "Re: no proxy support :'("
    date: 2008-07-02
    body: "+1 need proxy support."
    author: "BiSScuiTT"
  - subject: "Re: no proxy support :'("
    date: 2008-07-07
    body: "+1 !\n\nIs there any trick to make it works ?"
    author: "JC"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "This patch works fine:\n\nhttp://bugs.kde.org/attachment.cgi?id=24758&action=view\n\nHave fun :)"
    author: "UlliBulli"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "[09:44] <thiago> the patch is wrong\n[09:45] <thiago> really really wrong\n[09:45] <thiago> it makes support for SOCKS5 work, but it breaks proper HTTP proxying\n[09:45] <thiago> well, it would work for HTTPS, but HTTP would be ... stupid\n[09:45] <thiago> instead of sending GET and POST commands to the proxy, we'd be sending CONNECT\n\nWhich bug is the patch attached to?"
    author: "Will Stephenson"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "http://bugs.kde.org/show_bug.cgi?id=155707 I believe"
    author: "NabLa"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "Yes, you are right, its in comment #46"
    author: "UlliBulli"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "FYI, this patch is included in Fedora's current KDE packages (the 4.0.4 update for Fedora 9 and the Rawhide 4.0.80 build)."
    author: "Kevin Kofler"
  - subject: "Re: no proxy support :'("
    date: 2008-05-28
    body: "Last I knew, proxy support was included in someone's summer of code project.\n\nHopefully that was one that made it in. :P"
    author: "anon67"
  - subject: "Re: no proxy support :'("
    date: 2008-06-15
    body: "+1"
    author: "davide"
  - subject: "proxies"
    date: 2008-05-27
    body: "Any news about proxy support?"
    author: "anon"
  - subject: "No - but they have added nice GUI effects."
    date: 2008-05-28
    body: "Proxy is still not working"
    author: "Max"
  - subject: "Re: No - but they have added nice GUI effects."
    date: 2008-05-28
    body: "The skills needed to \"add nice GUI effects\" are completely different to those needed to add proxy support.  Don't try to spin this as a \"KDE guys only care about eye-candy hurrrrr\"."
    author: "Anon"
  - subject: "Re: No - but they have added nice GUI effects."
    date: 2008-05-28
    body: "Well, but I guess it's much more complicated to implement GUI effects (the math!) then proxy support."
    author: "panzi"
  - subject: "Re: No - but they have added nice GUI effects."
    date: 2008-05-28
    body: "Not as long as it's just dragging and dropping GUI elements around in KDevelop ;-)\n\nfor proxy, you have to know quite a bit about network internals - and apparently, there simply is nobody who does. If you know someone..."
    author: "jos poortvliet"
  - subject: "Re: No - but they have added nice GUI effects."
    date: 2008-05-28
    body: "Not at all - not to detract from the achievements of the guys writing the kwin_composite effects, but most of the effects are just a few hundred lines long and mainly use OpenGL to do most of the 3D \"math\" stuff.  The \"core\" stuff by Seli, Rivo et al was probably pretty hard-core, though.\n\nProxy support requires an intricate knowledge of networking stuff and, even if it isn't \"hard\" as writing kwin effects (I think it's harder, but there we go), it's certainly more \"niche\" than this kind of graphics stuff and less people are likely able to write it."
    author: "Anon"
  - subject: "Re: No - but they have added nice GUI effects."
    date: 2008-05-31
    body: "Proxy support does not require the same amount of prior knowledge to learn than graphical stuff. The data structures simply are not going to be as complicated, unless you write highly scalable apps that has to communicate with hundreds of servers or databases but I don't think that's the point of proxy support in your *desktop*. \n\nThe amount of algorithms and data structures you have to know to write a 3d engine is sickening. I always wished I had the time to learn the stuff but it is just too big and time consuming and unrelated to what I do nowadays. "
    author: "Anon"
  - subject: "Re: No - but they have added nice GUI effects."
    date: 2008-05-28
    body: "Hell, at least using proxychains kopete works .... techincally all it does is replace some libc's calls (probably connect/read/write or such) with versions that do the same but using a proxy (http(s)/socks) connection.\nI suppose that this method and probably the code too can be used by kdelibs ... Or hopping that some poor soul at Qt will add that support there which is probably even more \"right way\", in some non graphical library, the networking one ...\nUnfortunately i dont have much knowledge of internals of KDE (KIO part? ) to put my hands in there and then one has to count for the time it takes to compile all .... \nWhatever, i digress. Hope that the proxychains or Qt ideas got the attention of some right person though ;)\n"
    author: "anon"
  - subject: "any chance for stable plasma?"
    date: 2008-05-27
    body: "I was wondering why plasma widgets can crash plasma with simple actions like remove widget.. ?? After the first crash plasma starts again, but with the second one, kde doesn't start again...only remove .kde* dirs helps.. :(("
    author: "Tom"
  - subject: "Re: any chance for stable plasma?"
    date: 2008-05-28
    body: "Please, do not remove the complete configuration directory. Try to delete Plasma's configuration first and see if it works. And file a bug report, the developers might know better where to look and can help you on that problem."
    author: "Stefan Majewsky"
  - subject: "Re: any chance for stable plasma?"
    date: 2008-05-28
    body: "There are already some bugreports about this:\n\nhttp://bugs.kde.org/show_bug.cgi?id=158052\nhttp://bugs.kde.org/show_bug.cgi?id=152845\n\n\nI hope they will be resolved soon."
    author: "David"
  - subject: "Re: any chance for stable plasma?"
    date: 2008-05-29
    body: "http://bugs.kde.org/show_bug.cgi?id=158052 is solved with 4.0.2."
    author: "jam"
  - subject: "Fast"
    date: 2008-05-27
    body: "I don't know what changed in the last few days, but my last build from yesterday was downright snappy. Konqueror renders quickly, kmail loads suddenly.\n\nVery nice.\n\nDerek"
    author: "D Kite"
  - subject: "Probably KHTML's frostbyte."
    date: 2008-05-28
    body: "Probably frostbyte:\n\nhttp://www.kdedevelopers.org/node/3476\n\nGive your khtml devs some love. "
    author: "anon67"
  - subject: "No More Desktop Icons in 4.1"
    date: 2008-05-27
    body: "No More Desktop Icons in 4.1 = No longer a KDE user."
    author: "T. J. Brumfield"
  - subject: "The link would be good..."
    date: 2008-05-27
    body: "http://aseigo.blogspot.com/2008/05/no-more-desktop-icons-in-41.html"
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-27
    body: "Reading the article and comments rather than just the title would be even better."
    author: "Anon"
  - subject: "Re: The link would be good..."
    date: 2008-05-27
    body: "And you should know you *can* have icons on the desktop... but that in fact, Plasma will not display *the contents of ~/Desktop* over the wallpaper by default. For that you can use the folderview widget, or drag items to the desktop from kickoff or dolphin.\nThe main difference is that now it won't be a display of a specific folder on your filesystem."
    author: "Luca Beltrame"
  - subject: "Re: The link would be good..."
    date: 2008-05-27
    body: "Warning!\nMindless \"BAWWWWW\"-ing by mindless anons which won't read the above message ahead!"
    author: "Jonathan Thomas"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "The folderview widget shows icons in a containment.  Breaking ~/Desktop is a deal breaker for me.  Aaron has talked about doing this for a while.  He has said that he hates the concept of ~/Desktop, and repeatedly I've asked what is wrong with the concept, and how it can be improved.  I've never seen a good explanation.  So the preferences of a few are now the mandate of everyone.  Why remove flexibility and choice for no good reason?  If some people want to use folderview widgets, then let them, but why destroy ~/Desktop at the same time? This is not the KDE I knew and loved.\n\nThis isn't the first time such a decision has been made to remove functionality and choice, and it won't be the last.  If I wanted minimal choice, I'd run Gnome."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "Please read Aaron's post _fully_ before complaining..."
    author: "mxttie"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "I did read his post fully, and commented several times.  And I've asked Aaron about this every time I've seen him bring it up here, or on his blog.  I doubt you fully read my posts or comments."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "Your comments were answered several times in that discussion. In short: this change enables better functionality in the future and you can still have your precious icons. This argument is about nothing, basically."
    author: "Janne"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "No Janne, they weren't.\n\nNot once has anyone explained why ~/Desktop is broken and must be abandoned.  You repeat over and over again that this someday will be better.  You explain that you can see ~/Desktop in a folderview, but you continue to ignore my questions.\n\n1 - What is so broken that we have to throw it out?\n2 - Why throw out the old before the new is ready?\n3 - Why can't we have a folderview applet, and still allow people to have icons natively on the desktop like before?  Can't they exist side-by-side?\n4 - Why make such a change right before the 4.1 freeze?\n\nYou also insist no functionality has been removed, which just isn't factual.  I can't place files directly on my desktop.  I either have an applet window cover my desktop (in which the icons also act differently now) or I replace the desktop containment with folderview, and have no wallpaper (and the icons still behave differently).\n\nI had been championing for Linux installs on our new desktop rollout at work.  I am no longer doing so.  I do a slew of desktop work on the side for all my friends and family.  I can no longer advocate KDE desktops to people.  I certainly won't use it anymore myself.\n\nJust a few days ago I was in an airport showing off my KDE 3.5.9 desktop and watching people's jaws drop (technically it was my wife's KDE install on her laptop).  Now I'm supposed to show people a desktop without a wallpaper, with huge ugly black borders around icons, a whole icon fiasco, no proxy support, etc.?\n\nThis is the release we were supposed to wait for to switch for our everyday desktop?\n\nMaking this kind of change right before the 4.1 release was horrific decision making at best.  These kinds of changes should be developed in an individual developer's git tree (KDE should seriously consider moving to git) and fully fleshed out into a mature, functional concept before integration at all, and should then be integrated as an option.\n\nIn the much the same way with Kickoff, something new and unpopular was made the only choice (not just a default, but the only functional menu) until enough uproar was raised to half-ass an implementation of a classic menu.\n\nIf the future of KDE is sacrificing choice, flexibility, features and stability for slower, less productive, buggy, restrictive interfaces just so something supposedly looks nicer (and I get widgets!) I might as well adopt Vista.\n\nI feel a bit foolish for buying in so wholely into the hype of KDE 4 the past few years."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-29
    body: "1. In the Plasma view of things, file icon plasmoids on the desktop containment would make absolutely no sense, since the Plasma desktop is not a file icon dumping ground, but rather a container for much more featureful items. (Which could include a widget that displays files in a folder!)\n\n2. The old implementation was ultra-confusing, since files were represented as icons plasmoids. With folderview Mr. Upgraded-from-KDE3 has his files right on the desktop, and they work as expected, with the only difference being that they are set aside into a box. No confusion with the new implementation, plenty with the old.\n\n3. Confusion, see above. Not having icons directly on top of the desktop isn't a big deal anyway, and anybody should be satisfied with the current implementation until the folderview containment has an option for wallpaper, thus being suitable to be used as a main desktop containment.\n\n4. To save 4.1 from the confusion of the current implementation. I know many, many people would be dismayed to find out that they would have to live with the current way ~/Desktop is displayed. Right before the feature freeze would be as good a time as any.\n\nKDE 4.1 will be viable for every day use. ~/Desktop icons are displayed in a plasmoid by default and work just as expected. Nothing prohibiting daily use here.\n\n>In the much the same way with Kickoff, something new and unpopular was made the only choice (not just a default, but the only functional menu) until enough uproar was raised to half-ass an implementation of a classic menu.\n\nI should remind you that this was done before the release. It's not like KDE 4.0 was released without a choice. If people want to improve on this alternative choice, then let them do so! (I personally would like to see the Traditional Menu get more features, but I will have to wait until somebody improves it)\n\n>If the future of KDE is sacrificing choice, flexibility, features and stability for slower, less productive, buggy, restrictive interfaces just so something supposedly looks nicer (and I get widgets!) I might as well adopt Vista.\n\nGood thing it isn't. The infrastructure is all in place for numberless different, and innovative Plasmoids and containments so that your desktop can become a completely customizable and more productive environment. That is the future of KDE. With Vista, you'd have extreme luck if you'd ever get any new features to your desktop. You'd be restricted to one type of start menu, and you would be unhappy."
    author: "Jonathan Thomas"
  - subject: "Re: The link would be good..."
    date: 2008-05-29
    body: "For what it is worth, thanks for your response.\n\nI still can't imagine why this change is necessary, especially right before this release.  More and more distros have focused on Gnome, and if KDE 4 isn't ready to be an every-day desktop now, I fear KDE will lose ground even more.\n\nGiven that the desktop in KDE 4.x is a containment, I'm not sure why icons had to be removed, only to implement a completely new containment rather than code the desired improvements into the existing system.\n\nThere are some concepts out there that appeal to me, such as automatic filtering of displayed icons based on tasks, but again this seems like a lot of work that could be simplified into expanding already existing multiple-workspace concepts.\n\nIcons are certainly in a weird state in KDE 4, and I don't see how this improves the state of icon behavior.  It seems like staining the drapes so you don't notice the stain on the rug.  Why spend developer resources on something new, when that time could have been spent working directly on how to better interact with icons?\n\nIcons are now plasmoids as I understand it, which is part of the reason they have that odd border now.  Icons are displayed in an odd manner, and handled in an odd manner.  They advantage seems to be that I can rotate them, and resize them so my desktop looks silly.  I'm not sure how much of an advantage that is.  It certainly doesn't boost productivity, and it makes my desktop look less \"clean\".\n\nAnd the odd icon-around-icon implementation just doesn't seem intuitive, but perhaps that is because we think of an icon as a symbol, not a widget itself.  When people want to interact with an icon, they either click on it (default action, often open) or they right click for more detailed options.  If you want to switch to a more graphical interface here, you could use a radial menu which would address a few concerns instead of hovering producing these small icons that are hard to deal with.\n\nHaving a right click produce a clear, decent-sized radial menu would solve one problem with many context menus.  To select one item in a typical context menu, I have to hover past many other options, which involves then expanding the menu outwards over every option I mouse over, sometimes that involves some computing to figure out which sub-options are available.  In a radial menu, you can more directly move to the option you want.\n\nEven better, when you right click, you get a radial menu which shows these new icons as common choices around the icon, and the option directly to the right would open a traditional, full context menu."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-30
    body: "How does a radial menu replace the overlay icon? The overlays are there so you don't need a right mouse button (like with touchscreens). A radial menu could be combined with an overlay icon - the icon is a way to access the menu.\n\nAnyway, the change in icon behavior you are angry about is done because it would be relatively hard to make icons work like they used to - and yet it would, in a sense, be a waste of time. After all, plasma is about trying new things, trying to innovate. Innovation is difficult, often doesn't work and takes time. \n\nNow you probably don't want us to do anything new and original, but we think we must. So we're gonna ignore those who are holding us, and The Free Desktop, back. Sorry about that, but we aspire more than just appealing to a few geeks. We want to overtake Apple and MS - and for that, we need to be clearly better.\n\nAnd to be clearly better you need to to things different. Try new things. Innovate. So that's what we are trying.\n\nIf you can be constructive, and tell us better, new things to solve old problems - we will listen. If you can only scream 'I want it to work EXACTLY like KDE 3.5.x - then just go to KDE 3.5.x and never look back at 4.x because it WILL NOT HAPPEN. We sure want the same features, but NEVER the same THING.\n\nWe're going for the future, and we're leaving the old behind. If you can't keep up, stay with 3.5, nothing else I can say."
    author: "jos poortvliet"
  - subject: "Re: The link would be good..."
    date: 2008-05-30
    body: "Why do people continually insist I am against anything new, especially when you are directly responding to a post where I suggested something new?\n\nWhen Win 95 launched, people had the option of loading Program Manager if they wanted.  When Windows XP had complex theming, they gave the option of going back to a classic look, and again with Vista.\n\nAgain, I've only said about ten times that I'm not opposed to choice and new ideas.  I'm opposed to the philosophy of forcing people into a new way of doing things against their will."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-29
    body: "OK, I'll try to be as specific as possible here....\n\nWas the old method with desktop-folder horribly broken? Well, from user point of view, not really. I mean, it worked right? The problem with it is that it's inflexible and stsatic. So you have a folder, and the contents of that folder are displayed on the desktop. And that's the only way that feature works. If you want it to work some other way, tough! Not gonna happen!\n\nWhat this change does is that it gives you, the user a lot more flexibility on this matter. You just want to display a bunch of files and folders on your desktop? Fine, you still have that. No-one has taken that feature away from you. So take a deep breath and repeat that fact: no-one has taken your precious icons away from you.\n\nWith this new system you can do things that you couldn't do before. You can display icons from several folders if you want to. And those folders can be remote (hell, they could be on the opposite side of the world!)  or local. You could have filters that only show certain kind of files. In the future you could have automation that changes what files and folders are displayed, based on what you are doing with your computer. That was not possible with the old system.\n\n\"3 - Why can't we have a folderview applet, and still allow people to have icons natively on the desktop like before? Can't they exist side-by-side?\"\n\nPray-tell: what would you accomplish by that? Could you please tell me what functionality \"icons on desktop\" gives you that this new system does not give you? You seem to be focusing on the applet, while you ignore the fact that you can use it as a containment as well.\n\n\"You also insist no functionality has been removed, which just isn't factual. I can't place files directly on my desktop.\"\n\nyes you can.\n\n\"I had been championing for Linux installs on our new desktop rollout at work. I am no longer doing so\"\n\nWhy? because you incorrectly believe that one of your favourite features has been removed, and you therefore feel that everyone else will hate Linux as well? And last time I checked, there are other GUI's besides KDE available for Linux.\n\n\"ust a few days ago I was in an airport showing off my KDE 3.5.9 desktop and watching people's jaws drop (technically it was my wife's KDE install on her laptop). Now I'm supposed to show people a desktop without a wallpaper, with huge ugly black borders around icons, a whole icon fiasco, no proxy support, etc.?\"\n\nThose black borders are determined by the theme you use. Don't like them? Then use some other theme. And aren't those black borders part of the theme for the plasmoids? Do you still have those borders if you are using the folderview as a containment instead of plasmoid?\n\n\"Making this kind of change right before the 4.1 release was horrific decision making at best.\"\n\nNo it wasn't. It was part of the development-cycle and it was done before the feature-freeze.\n\n\"If the future of KDE is sacrificing choice, flexibility, features and stability for slower, less productive, buggy, restrictive interfaces just so something supposedly looks nicer (and I get widgets!) I might as well adopt Vista.\"\n\nThe old desktop & icons was exactly what you describe. It was inflexible, featureless and offered the user no choice. Either you used it in the only way it worked, or you didn't use it at all. Now that users are presented with a system with more features and flexibility, you whine?\n\n\"I feel a bit foolish for buying in so wholely into the hype of KDE 4 the past few years.\"\n\nNo, you are being foolish, because you incorrectly believe something to be true, and when dozen or so people line up to tell you that you are wrong, you cover your ears and shout \"lalalalalala, I can't hear you! KDE 4.1 does not support icons on the desktop! I can't hear you!\"."
    author: "Janne"
  - subject: "Re: The link would be good..."
    date: 2008-05-31
    body: "\"Not once has anyone explained why ~/Desktop is broken and must be abandoned. You repeat over and over again that this someday will be better. You explain that you can see ~/Desktop in a folderview, but you continue to ignore my questions.\"\n\nIt's stupid to have hardcoded \"Desktop\" on your home, what you cant change to other name. On KDE this is possible, but on GNOME, it's not. (Yes, it is GNOME problem). You are always stuck to the english name of Desktop, if you change it to \"desktop\" or \"desk\" on KDE and you login to GNOME, it greates new one named \"Desktop\". If you have other localisation, you have all folders on your own language but then you have \"Desktop\" there as ghost what you cant ripp off.\n\nWhy you want to use desktop as place to store files? It's just a one folder. This function adds you A POSSIBILITY(!) to have TWO desktop in one desktop on multiple plasmoid desktop what are on multiple virtualdesktops. You get more control what you are doing on your computer, you are not forced to have your desktop files/icons on ~/Desktop. You can change those containers so you can have keep multiple project files on desktop, all pointing to correct folders on your home directory. Currently you need to add shortcuts to do that.\n\nAnd this is first step. When Nepomuk comes, you could set smart-folder to your home, then set that as container to your desktop. No more needing to search stuff, place them to correct folders, when you always find what you need, in one container.\n\nWhy to force desktop to be a ~/Desktop? Why it cant be flexible and powerfull tool what allows more control to your files, icons and information?\nWhy it should be just a folder what has plasmoids/widgets/gadgets top of it? Why to stop there? Why not doing desktop as plasmoid/widget/gadget?\n\nThis is just a first step, dont blame it if you cant see what future can bring to you. Many said when they heard from iPhone that it's silly or stupid idea and Apple does not sell those so many. What happend... Very simple technology what brought new way to use the phone. \n\nThis is samekind technology, it brings new way to use your desktop, to use your whole desktop enviroment, whole computer! But it does not replace the old one. If you dont like it, dont use it. You can still keep the icons on desktop, you can still keep it as ~/Desktop.\n\nIt's up to you what you want! I tested that and I liked it much more than KDE3 style, I can see great potential for it and flaws of old style. I wouldn't suprise if Microsoft, Apple or GNOME would copy this idea.\n\nWhen you use this new thing a week, you notice the difference. It's like you would be getting virtual desktops to Windows desktop!!"
    author: "Thomas.S"
  - subject: "Re: The link would be good..."
    date: 2008-06-01
    body: "\"Why to force desktop to be a ~/Desktop?\"\n\nHere is the kicker.  If you don't mind browsing your files in an applet instead of directly on the desktop, then have a folderview applet as you please.  See, we both win.  I'm not against a folder with filtering and such.  I'm opposed to not being able to store things directly on my Desktop.\n\n~/Desktop has a few advantages.\n\n1 - It is simplistic to understand.  As a career IT guy, I've had to support tons and tons of end-users who aren't that computer savvy.  I've always told people that Linux has this undeserved reputation of being hard to use.  It is far more flexible and powerful, but that shouldn't intimidate new users.  New users should be able to get their feet wet without being scared off.  Given that the file structure of Linux is considerably different from Windows (a disseration for another day, but do we really need to stick to Unix roots here and have binaries in 20 different folders?) people need to know where they can find things.  Desktop passes the Grandma test.\n2 - Desktop is fast.  Many apps default to saving to the Desktop.  I don't let things linger there, but I throw things there when I'm in a hurry to deal with later.\n3 - Desktop is a reminder.  When those files stare directly at me, I know what I have to deal with.  I could drop things in /home/foo instead, but then I have to pull up a window to look at those things, as opposed to having a built-in reminder right in front of me.\n4 - No one is forcing you to store things there if you don't want.  Again, you can have any folder you want in a folderview applet, or you can have a completely blank Desktop.  Do as you will here.  But don't take my choice away from me.\n\nNow, if the change were transparent (the new containment was fullscreen, transparent, had a wallpaper) and in practice offer the exact same functionality and more (as it perhaps might in 5.2) then no one would raise a stink.  Expecting me to set the current containment as a desktop with no wallpaper, or have an ugly applet cover my Desktop is not the same thing."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-06-02
    body: "So you are complaining that a feature that the developers plan to implement in the future hasn't already been implemented? Sheesh, and I just went ahead and wasted my time explaining stuff."
    author: "yman"
  - subject: "Re: The link would be good..."
    date: 2008-06-02
    body: "Aaron's article made no promise that would be the future.  A few people here said it might likely be the future, or distros might start doing it.\n\nWhat I'm upset about is 4.1 being released in what I consider an unusable state.  Making this kind of change right before the release we were instructed to wait for just doesn't make sense.\n\nFurthermore, there is no proof right now that 4.2 will implement the change in a more transparent manner.  That is merely a suggestion I've seen bandied about here."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-06-02
    body: "from the actual blog post:\n\"In 4.2 we'll be separating context menus and background rendering from containments so that those functionalities can be easily shared; so while in 4.1 you can set the folder view as your desktop containment ... it's not going to be as pretty as it will be in 4.2. In 4.2 it will be completely seamless, whereas right now you'll get a \"nice\" wallpaper based on the applet background svg. Call it the Model T wallpaper: any colour you want, as long as it's the applet background ;)\"\n\nDass said...\n\n    mmh\n    CRAZY IDEA:\n    1)transparent background +\n    2)possibility to put icons not only in grid but also free in any position of the plasmoid +\n    3)plasmoid big as the desktop +\n    ________________________________\n    =\n\n    i can put icons in any place of the desktop and they are usable as in dolphin\n\n    so we have (the possibility, if we want) the same functions of the kde3 desktop but with plasmoids...\n\n==========================\n\nAaron J. Seigo said...\n\n    @Dass: \"mmh\n    CRAZY IDEA:\"\n\n    not crazy at all. that's *exactly* how folderview-as-your-containment works.\n\n    neat, huh? =)\n\n==========================\n\nburpnrun said...\n\n    Yadda, yadda. KDE3 \"just works\", I happen to like having program icons on my desktop, and therefore the question is the same as another commenter asked: Will KDE4.x cater to this?\n\n    Apart from that, I think the KDE4 \"look and feel\" is all glitz and sizzle. No change in the amount of steak provided. YMMV.\n\n==========================\n\nAaron J. Seigo said...\n\n    @burpnrun: 'Yadda, yadda. KDE3 \"just works\"'\n\n    for you, perhaps.\n\n    \"I happen to like having program icons on my desktop, and therefore the question is the same as another commenter asked: Will KDE4.x cater to this?\"\n\n    which i already answered, but i'll patiently answer it again: shortcuts (which is what program launchers are) are not affected by this change.\n\n==========================\n==========================\n\nI also remember him saying that what was removed was the automatic loading of the contents of ~/Desktop. Aside from that, it supposedly has the same functionality so you can have shortcuts on your desktop as before (because that seems to be what they essentially were in 4.0 as well). However, in 4.1 they added icons on the desktop by adding that Folder View Plasmoid / Containment. And as you can see from the quote in the blog post, they definitely intend to allow you to use wallpaper as the background of Folder View in 4.2."
    author: "yman"
  - subject: "Re: The link would be good..."
    date: 2008-06-02
    body: "\"1 - What is so broken that we have to throw it out?\"\n\nConcept 1: The desktop is where you store files, folders, and launchers.\nConcept 2: The desktop is where you have all tools and data pertaining to a specific activity.\nConcept 2 includes Concept 1 and expands it.\n\n\"2 - Why throw out the old before the new is ready?\"\n\nFrom the sound of it, icons as Plasmoids meant a lot of code where it doesn't belong (\"dirty hack\"?), and produced broken functionality. Folder View is practically fully functional but looks ugly with certain themes because it is only partially implemented. Between bad and worse, I think Folder View sounds like the better option.\n\n\"3 - Why can't we have a folderview applet, and still allow people to have icons natively on the desktop like before? Can't they exist side-by-side?\"\n\nBecause icons as Plasmoids is broken in concept and implementation and involves dirty hacks that seriously mess up the design, while Folder View provides better functionality?\n\n\"4 - Why make such a change right before the 4.1 freeze?\"\n\nOne would think the freeze is the deadline for new features, not the deadline for totally completed, rock stable, fully functional features. Thus, as long as a feature makes the deadline, what's the problem?"
    author: "yman"
  - subject: "Re: The link would be good..."
    date: 2008-06-02
    body: "I think I just spread false information in the above post. Oops."
    author: "yman"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "You can still show all icons from ~/Desktop on your desktop. Simple as that. With Folder View the mechanism for that is even extended. So now you can show content of any folder you like on the desktop. You can add as many Folder Views to your desktop and show the contents of different folders. You can even filter what kind of files are shown (think about filtering by file type or by tags or by score aor by any combination). And I'm sure this will be made even more powerfull once people get even more clever and innovative ideas."
    author: "JLP"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "You can show them in a separate containment, which is different from your desktop."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "And what exactly is the problem then? This only makes this mechanism more versatile and extendable. The desktop is in fact also just a containmanet. So now you have a Folder View containment layered over Desktop containment, while before you had icons layered on top of desktop background."
    author: "JLP"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "And the plasmoid is floating on your desktop. Seigo shows a screenshot."
    author: "Riddle"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "I'm familiar with the screenshot.\n\nNow imagine going to your desktop, but your files aren't there.  You have to configure and pull up this containment folderview instead.  However, you're juggling multiple windows (if not multiple desktops, etc) and you need to find that containment on your taskbar, except it isn't a window so it isn't on the taskbar.\n\nIf someone really wanted a plasma applet to view files (which seems odd when we have file managers for this) then let them have it.\n\nI've yet to see one single good reason however to remove displaying ~/Desktop on the Desktop for those who want it.  If you want a clean desktop, then don't put anything there.  Everyone wins.\n\nNot a soul can seem to explain why we should remove choice from the users to force everyone into this new mandate.  Repeated moves like this demonstrate quite clearly the new direction of KDE.  KDE used to represent freedom and choice."
    author: "T. J. Brumfield"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: ">Now imagine going to your desktop, but your files aren't there. You have to configure and pull up this containment folderview instead. However, you're juggling multiple windows (if not multiple desktops, etc) and you need to find that containment on your taskbar, except it isn't a window so it isn't on the taskbar.\n\nDrop Folderview applet on desktop.\nHave ~/Desktop icons on desktop inside Folderview. Simple. Files on desktop, and new user upgrading from KDE 3.5.9 is happy because the stuff on his desktop is there and he can do all that he could do before + new stuff.\n\nEven if you set Folderview as a desktop containment, its not as complicated as you uninformatively make it out to be. Currently, all desktops show the same containment. No juggling around multiple desktops. Since Folderview is a containment, it can house other containments such as panels and even applets. You could use the folderview containment in the exact same way as a regular containment with you panels and widgets and such, and you wouldn't even need to use the normal desktop containment. No juggling between virtual desktops, no juggling between containments, no problem. All you will have to do is set the Folderview as a containment and you will be good to go!\n\n>I've yet to see one single good reason however to remove displaying ~/Desktop on the Desktop for those who want it. If you want a clean desktop, then don't put anything there. Everyone wins.\n\nMost people would be satisfied with an applet displaying the icons on the desktop like folderview does. For those who can't bear to give away the icons being directly on the desktop, set Folderview as a containment. Configure a bit of Panel mojo and bam! A completely customized desktop with icons on the background. Everybody wins and nobody loses. \n\nNothing is being taken away, as you can still display your precious icons directly on the friggin desktop, not that most people wouldn't be satisfied with the little box with their files nicely grouped. Even the most diehard file icon lovin' configurations I've seen have icons separated in some manner. If you don't like the default opaque background, find a theme that has a transparent background or wait until Folderview can have a set background."
    author: "Jonathan Thomas"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "I was prepared to launch into my usual flamebait about Havoc Pennington infiltrating KDE, but it really looks like this is a change in implementation with no feature loss.\n\nI'm still waiting on the liveCD torrent so I can't say for certain.\n\nAt this point I still plan to stick with 3.5.x until there's not a single distro left supporting it.\n"
    author: "Velvet Elvis"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "\"I was prepared to launch into my usual flamebait about Havoc Pennington infiltrating KDE, but it really looks like this is a change in implementation with no feature loss.\"\n\nI'm glad you spared us what would no doubt have been a rant high on emotion and low on facts and logic.\n\n\"No feature loss\" does this a huge disservice: \"eventually large feature gains\" is more accurate.\n"
    author: "Anon"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "> Now imagine going to your desktop, but your files aren't there. You have to configure and pull up this containment folderview instead.\n\nThe article says that a folderview applet for ~/Desktop will appear by default (i.e. when you first start Plasma, or when you delete your plasma-appletsrc configuration)."
    author: "Stefan Majewsky"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "And it does, tested with a svn trunk compile from a few days ago with a different user."
    author: "Luca Beltrame"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "And it adds that ugly black background to your icons? What do I have a nice wallpaper for when I can't see it? Do I understand something wrong?"
    author: "panzi"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "You can theme it and use e.g.\n\nhttp://www.kde-look.org/content/show.php/Glassified?content=81388"
    author: "Anon"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "nice theme!\n"
    author: "silent bob"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "Short answer: No. Hiding it behind your windows? Yes.\n\nLonger answer: There is a workaround in trunk that makes it possible to use a transparent background. In the future, the Folderview applet is going to be a containment. This means, if I've understood it correctly, that you can set a background for Folderview and make it \"fullscreen\" = traditional desktop."
    author: "Hans"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "> Not a soul can seem to explain why we should remove choice from the users to force everyone into this new mandate. Repeated moves like this demonstrate quite clearly the new direction of KDE. KDE used to represent freedom and choice.\n\nIs your brain switched off? KDE is quite obviously still representing freedom and choice - more than before in fact, because now, not only do you get the contents of ~/Desktop on your desktop by default, you can also show other folders, remove them, move them around - all sorts of things you didn't have the freedom or choice to do before. Just because KDE now mandates that you have more freedom and choice doesn't make it a bad thing."
    author: "Johnny Awkward"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "From the article:\n---\nIn the future we'll have a little label in the folderview telling you which folder you are looking at, it will turn into an icon with a menu listing in horizontally constrained containments (e.g. panels), it will be collapsible on the desktop with a single click (it's already resizable, rotatable and removable) and you will be able to use it as a containment itself.\n\nThat last bit is important: it means that you can have an Old Skool(tm) desktop with an icon mess if that's what you really, really want. So don't bother with that flame, nobody has anything to complain about. ;)\n---\n\nOk, he does say \"in the future\" :)\nBut the future is just a measurable amount of time away!"
    author: "fhd"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "If you've read the article and comments as you've claimed, you would know that your \"Desktop\" *is* a containment, and one that will eventually (already?) be replaceable by any other containment.  So why did you make this obviously incorrect post, if not to troll?"
    author: "Anon"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "\"So why did you make this obviously incorrect post, if not to troll?\"\nbecouse T.J.B is 13 years old boy without friends? "
    author: "question"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "Ad hominem attacks are unnecessary and unproductive. Please be more constructive.\n "
    author: "Oscar"
  - subject: "Re: The link would be good..."
    date: 2008-05-28
    body: "Actually, it shows them inside an applet.\nThe Folder View applet can be used as a containment like the desktop, but in a default KDE installation you will have a Folder View applet inside your Desktop containment which will display your ~/Desktop folder.\n\nNothing is taken away or destroyed with the addition of this applet and the removal of ~/Desktop files being displayed as icon applets on the desktop. If anything, the \"display a bunch of files on your desktop\" paradigm has been advanced with this, allowing you to do much more with any folder you wish. Flexibility has increased, which is the KDE way!"
    author: "Jonathan Thomas"
  - subject: "Re: The link would be good..."
    date: 2008-05-29
    body: "I wonder if this whole thread just points to that when KDE developers talk about the future and current plans, that they might need to dumb it down, especially considering that a lot of people are shifting to linux from windows.\n\nBut alas, having used KDE 4.  I love the new desktop, very easy, and very good."
    author: "anon"
  - subject: "Re: The link would be good..."
    date: 2008-05-29
    body: "It's simply impossible to dumb it down enough to get through the thick skulls of some of the densest Dot commentators - I'm sure you could literally say something like this:\n\n\"The sky is blue\"\n\nand you'd get a flurry of posts saying \n\n\"Oh god why have you made the sky green I'm switching to GNOME where at least the sky is blue!!1\""
    author: "Anon"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "I've just been dragging and dropping files and foldres to my desktop, what is it that you are missing? \n\nWhat I'm mising is a folderview where the background and borders are invisible until hovered. BUt knowing the KDE developers, more choice will be available in future releases ;)"
    author: "Jo \u00d8iongen"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "Ok, you always looked like a wannabe troll here on the dot, now you've just confirmed it.\nPLONK."
    author: "Vide"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "No one needs desktop icons!\n\nI'm always like OMG when I see how my wife uses her computer.\n\nFirefox saves all downloaded files to the Desktop folder per default. She is a rather messy person with her computer, and she is an artist who keeps downloading reference pictures and such 24/7. So letting her sit at ANY pc, \nthere will be >300 files on the desktop after approximately one hour.\n\nSome day she found out how to turn off showing files on the desktop in Windows XP and this tells me all about the use current desktop I need to know.\n\nI like to keep some app launchers around, but almost no files. If at all, just temporary - I'd love to use a nice file area instead. This would be just ONE temp directory actually used by the user, not his programs. It's a very useful thing.\n\nHaving ~/Desktop files automatically integrated with plasma icons is just a PITA. You mix up actual widgets and actual files, and you are confused because you cannot more/rename/delete \"files\" from the desktop. It was a good decision.\n\nI'm pretty sure that you will be able to drag-and-drop all the app launchers and file/folder shortcuts from anywhere to your desktop to suit your needs later.\nBut keeping files (literally: files) on your desktop is IMO not necessary.\n\n\nBut, to all those who can't life without there I-have-them-since-8-years-desktop-icons:\nit's REINVENTING THE DESKTOP right here! (not just telling to do so as MANY do)\nWhat do you expect, a Windows XP clone with EXACTLY the same drop shadow on desktop icon fonts?"
    author: "fhd"
  - subject: "DESKTOP: ~/Desktop -- what's in the name?"
    date: 2008-05-28
    body: "if the ~/Desktop is no longer reflected on the users 'wallpaper' than what's in the name?\n\ni mean ~/Desktop is a bit of a standard for desktop linux right?\n\ni truly hope a transperant-folderview-plasma-thingy will be covering the wallpaper of default KDE installations...\n\njust my .02 (MYR at the moment) "
    author: "cies"
  - subject: "Re: DESKTOP: ~/Desktop -- what's in the name?"
    date: 2008-05-28
    body: "If you read Seigo's blog, that's exactly what happened. He stated that his screenshot was (mostly) a default Plasma.\n"
    author: "Riddle"
  - subject: "Re: DESKTOP: ~/Desktop -- what's in the name?"
    date: 2008-05-28
    body: "Although people seem to think that a transparent background over the folderview widget would solve all issues, it has been tried very recently and actually shown to have some issues that will require more thought."
    author: "Luca Beltrame"
  - subject: "Re: DESKTOP: ~/Desktop -- what's in the name?"
    date: 2008-05-28
    body: "1. what issues?\n2. what people?\n"
    author: "???"
  - subject: "Re: DESKTOP: ~/Desktop -- what's in the name?"
    date: 2008-05-28
    body: "THere's a a thread on the panel-devel ML about it (I'll see if I can dig up the link) and discussion was pretty strong."
    author: "Luca Beltrame"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "\"I've yet to see one single good reason however to remove displaying ~/Desktop on the Desktop for those who want it. If you want a clean desktop, then don't put anything there. Everyone wins.\"\n\nThis is the most sane argument about this topic I ever saw.\nI do not know why Aseigo hates the current desktop with icons and no-fancy widgets so much :-P\nBut overall I do not see a problem in placing the desktop icons in a container, but isn't the ~/Desktop a freedesktop standard? Can we simply change things that where made as a consensus with other DE guys (like gnome)?"
    author: "Iuri Fiedoruk"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "Actually, it isn't. The functionality isn't being removed, just approached at a slightly different angle. (Sorry for ripping of your phrase, Chani ;P)\n\nThe standard isn't being abandoned, just implemented slightly different."
    author: "Jonathan Thomas"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "I would even say extended rather then different since everything works like before if someone likes to but can also be extended the way users like to have it. That's why I love KDE so much. They care about there users!\n"
    author: "pineapple"
  - subject: "Re: No More Desktop Icons in 4.1"
    date: 2008-05-28
    body: "It seems to me that the distributors could just make the necessary widget installed by default, thus eliminating any discomfort people may experience."
    author: "blueshirt"
  - subject: "README"
    date: 2008-05-28
    body: "Desktop Icons Have Not Been Abandoned: http://aseigo.blogspot.com/2008/05/no-more-desktop-icons-in-41.html and http://plasma.kde.org/media/plasma_4_1_beta.png\n\nIn KDE4.0, the desktop containment automatically added an Icon Plasmoid for each file in ~/Desktop. This had the serious disadvantage on disassociating the icon on the desktop from the actual file it points to. If you delete an icon from the desktop, it isn't deleted from ~/Desktop and may reappear.\n\nIn KDE4.1, a Folder View Plasmoid (basicly a mini-Dolphin) is placed on the desktop, thus completely associating the icon with the file. It appears by default, pointing to ~/Desktop, thus keeping with FD.o standards. If you delete an item from the desktop, it stays gone and is actually deleted from ~/Desktop.\n\nIf you dislike keeping the icons contained, it is still possible to add an Icon plasmoid by d'n'd to the desktop.\n\nNOTHING IS BROKEN. NO FLEXIBILITY IS LOST. Plasma just changed the way it used ~/Desktop.\n\nYes, Seigo made a poor choice of a title for his blog entry.\n"
    author: "Riddle"
  - subject: "Re: README"
    date: 2008-05-28
    body: "I wouldn't say it was poor, just unfortunate that there are so many people who misunderstood it AND skipped the body of his post."
    author: "yman"
  - subject: "Re: README"
    date: 2008-05-28
    body: "I'd guess it was all on purpose. I'm quite sure he got addicted to those flamewars."
    author: "fhd"
  - subject: "Re: README"
    date: 2008-05-28
    body: "No, you missed the body of my post.\n\nI clearly read the body of his post, and saw the screen shot, and that's what I'm upset about."
    author: "T. J. Brumfield"
  - subject: "Re: README"
    date: 2008-05-28
    body: "After a bit of polishing (making the folder view transparent etc. which is likely to happen) you won't experience any difference from the traditional icons-on-the-desktop system. (Probably a folder view with ~/Desktop will be default, if not in KDE, then in the distributions.) Just the advantages as stated above."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: README"
    date: 2008-05-29
    body: "If so, then say straight out: what is the problem.\n"
    author: "Riddle"
  - subject: "sigh"
    date: 2008-05-27
    body: "wish i had my computer to try it on, but alas, HP technicians are friggin hopeless and damn slow when repairing a hardware fault under warranty.   Can't wait to get it back and try beta 1"
    author: "anon"
  - subject: "Re: sigh"
    date: 2008-05-29
    body: "Take VirtualBox."
    author: "andy"
  - subject: "Not there yet"
    date: 2008-05-28
    body: "I've tried it in a debian machine, but I can't live without autohide panels.\nDoes anyone know if this feature will be implemented in 4.1? If it's not in the beta1 I think it won't be a new feature."
    author: "Adrian"
  - subject: "Most requested features"
    date: 2008-05-28
    body: "I'd contend the most requested features of KDE 4.x that I see on the Dot are:\n\n* Panel configuration\n* Removal of black border from icons\n* Proxy support\n* Oxygen recognizing color settings\n\nEveryone keeps saying to wait for KDE 4.1, so what happens when none of these are addressed in KDE 4.1?"
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "The Panel is configurable.\nThe icons thing is more of an aesthetics issue, and not a deal breaker imo.\nProxy support is being worked on, though it still is a quite major problem. (http://bugs.kde.org/show_bug.cgi?id=155707)\nOxygen is no longer the default KWin window decoration style. Ozone is, which is a direct clone of Oxygen that respects the color settings. No problem here, and it hasn't been for a while.\n\nIn short, please stop drama-llamaing. Half of these have already been addressed, and one is just an aesthetics issue. You dislike KDE4, we get it, but please stop spreading FUD or at least take the chance to research so you can make a list of things that actually need to be addressed..."
    author: "Jonathan Thomas"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "Since when is Ozone default?  I have 4.0.4 installed on Sabayon, and it still has Oxygen default.  The entire reason for the Ozone fork was that the color setting patch wasn't accepted in Oxygen.\n\nAnd the panel still doesn't match KDE 3's configuration."
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "I'm talking about trunk/KDE 4.1 beta1, which is where you are bawwwwing about features not getting implemented before release.\n\nThe 4.0.x branch is just a bugfix branch, you shouldn't be looking to that as what KDE 4.1 will be like. Only the most vital Plasma features were backported to that branch."
    author: "Jonathan Thomas"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "I didn't realize Ozone was default now.  So 3 out of the 4 most commonly requested features will miss the cut.\n\nProxy support is a deal-breaker for many, including me.  I wanted to use KDE on my new work laptop but can't."
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "You can't be sure. Proxy support still could be fixed since its a bug, and the icon background issue is an issue of personal taste.\n\nThe panel is also more configurable, as promised. I don't recall anyone ever promising that the panel would have feature parity with Kicker for 4.1. If you drew that conclusion, then it sucks to be you.\n\nSo, 1, possibly 2 out of this list 4 issues won't be addressed. Most of these besides the proxy one aren't important anyway or can be fixed with another theme."
    author: "Jonathan Thomas"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "\"Proxy support is a deal-breaker for many, including me. \"\n\nI thought \"no desktop-icons\" was the dealbreaker for you? Besides, your list of complaints is mostly invalid. You claim that three of the four complaits go unaddressed. But that's not true. Complaint about Oxygen is fixed. then you complain that \"panels are not configurable\". Um, yes they are. What about the black-border thingy? Well, that's a question of aesthetics, and you can't please everyone. So we are basically left with the proxy-issue, which is (apparently) dealbreaker for you.\n\nWhy do I get the feeling that your \"dealbreakers\" change on a whim? Earlier it was the \"no icons on the desktop\". Now that you have been educated on the matter, the dealbreaker suddenly changed to \"no proxy\". I get the feeling that no matter the KDE-devels do, there will always be some \"dealbreaker\" that is going to prevent you from using KDE. Helkl, maybe you should just switch to Windows, I bet there are no \"dealbreakers\" there..."
    author: "Janne"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "You just need to go through TJ's posts to see that he hates the project, and will happily seek out (or make up) reasons to moan about it.  Just ignore him.  I can't imagine why he still posts here."
    author: "Anon"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "I have a great deal of respect for people who misspell my name (as amazingly complicated as it is), post anonymously, make personal attacks, and don't research their opinions.\n\nGoogle my name and you'll see for years I've been a huge KDE advocate and fan."
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "Well, that's kinda funny. No matter how many features you implement, as long as there are 3 features left unimplemented, there will be \"three most commonly requested features that miss the cut\".\n\nI agree that these features should be implemented, but that will be the case in a few weeks/months."
    author: "Jens Uhlenbrock"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "'Well, that's kinda funny. No matter how many features you implement, as long as there are 3 features left unimplemented, there will be \"three most commonly requested features that miss the cut\".'\n\nThanks for that, it really cheered me up (and it's a very good point)."
    author: "Martin Fitzpatrick"
  - subject: "Re: Most requested features"
    date: 2008-05-29
    body: "My point is the four most commonly requested features were ignored, where as the changelog from 4.0 to 4.1 is huge.  Surely there will always be a to-do list for future versions, but the things requested most when 4 was still a beta, and when 4.0 was released, still haven't been addressed, or so it seemed when I made the initial posting.\n\nIf Ozone is the default now, then I stand corrected on that one point."
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "You seem to he in a whining mood today...."
    author: "Janne"
  - subject: "Re: Most requested features"
    date: 2008-05-28
    body: "You seem to fall back on personal attacks rather than logic.  Grow up."
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-29
    body: "Calling whining \"whining\" is not a \"personal attack\"."
    author: "Janne"
  - subject: "Re: Most requested features"
    date: 2008-05-30
    body: "+1\n\nSorry, TJ, you sometimes make sense, but imho you went way out of line on this one. Just because in your opinion the default look is horrible (the black Plasmoid area behind the icons) you create a huge flame. Not a good one, sorry.\n\n(the whole 'it's a different containment so they are not on my desktop' is really sad, btw - for all sense and purpose, the icons ARE on the desktop, separated by a black border or not)"
    author: "jos poortvliet"
  - subject: "Re: Most requested features"
    date: 2008-05-31
    body: "The icons aren't on the desktop though.  I have an applet cover my desktop, or I get the containment with no wallpaper as my desktop.\n\nIt comes across as archaic while presenting no benefit right now, and I'm not even sold on future benefit at this point.\n\nA flame is a personal attack.  I didn't create a flame.  I didn't go after Aaron, or anything like that.  For months I've politely, humbly asked someone to please explain what is so broken about ~/Desktop.  Instead, I get attacked.\n\nI'm disgusted and disappointed by the philosophy that KDE is demonstrating.  Few people seem to understand the principle here.  Generally, I don't care what the defaults are.  I just want the freedom to have my desktop operate the way I want it, and KDE used to be all about that level of freedom.  I've never argued against people having choice.\n\nFor instance, I can't stand the Mac-like menu being at the top at all times.  It saves real-estate, but I just don't like it.  I've never suggested developers shouldn't spend time offering that option to those who want it.\n\nI've seen repeated developments and decisions that suggest the future of KDE is forcing people into mandates, whether they like it or not."
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-05-31
    body: "But they *are* on the desktop. It barely makes a difference whether they are in a translucent rectangle. This is hardly archaic. If anything, this makes things more organized. What this argument boils down to is \"this implementation looks different\". \n\nThis was not a calculated move to \"force\" anybody into a \"mandate\". Functionality such as having a wallpaper for folderview as a containment may not be coming with 4.1, but that doesn't mean that come KDE 4.2 you' won't be able to use Folderview as a containment with wallpaper functionality! But for now, due to time constraints, this is a very livable compromise. Would you have rather stayed with the confusing and rather limited way ~/Desktop icons were represented in 4.0.x? No, of course you wouldn't. Nobody would. It's current implementation is confusing and frustrating for new users. Certainly it would be better to live with a translucent box between your icons and the wallpaper (Which is not the desktop, by the way) than to leave it in the broken state it was in 4.0.x while developing Folderview in a separate git directory until it's super-duper-perfect.\n\nYou get attacked because you generally bash KDE and its developers with an ignorance of the facts (Issues that have already been addressed or bugs that could still be fixed or patched by the distros have no chance of being addressed during the time appropriated for bug fixing!!1!1!)  and blowing other things out of proportion to express your ill-placed \"disgust\". (Time constraints are a concerted effort to take away our freedom!1!)"
    author: "Jonathan Thomas"
  - subject: "Re: Most requested features"
    date: 2008-05-31
    body: "Insisting that two things are identical when they are not isn't very productive.  The new implementation, and the long-standing implementation are different.  Please stop suggesting the icons are on the desktop.  They simply aren't.\n\nYou are forcing people to operate their desktop in a new manner.  If the state of icons in KDE 4 was unacceptable by your own standards, then either perhaps a greater developer emphasis should be have been placed there, or the release should have been pushed back.\n\nMaking a half-hearted, half-implemented change the new standard does not bode well for what is supposed to be a major release, especially when I keep hearing the mantra of \"everything will be fine by 4.1\".\n\nFurthermore, I think it is pretty silly to suggest that a proper KDE release is one with a major component broken (or several), and then expect distros to fix it for you.  KDE, like most FOSS software, is community driven, and contributions come from multiple sources.  That's great.  Expecting others to clean up your mess isn't.\n\nIt however mimics all the responses regarding Flash not working on Konqueror.  Nevermind that other browers can handle the plugin fine.  The plugin operates within the standards of nsplugins, but if Konqueror can't implement it, that isn't an issue for Konqueror, but rather for Adobe.\n\nFor years I've praised KDE.  For years I've also spread the hype I swallowed regarding KDE 4.  I'm not universally critical.  I'm critical of things that I am strongly opposed to, and quite frankly I wouldn't feel nearly so strongly on these issues if I hadn't enjoyed using KDE so much in the past.\n\nYou insist everything will be great down the road, except I keep hearing that.  I've been told that since the KDE 4 alpha releases.  4.0 will be usable.  4.1 will be usable.  Now 4.2 might be usable.  I'm patient.  I'm understanding.  However, if you're not going to represent feature parity and stability until next year or whenever, then say so.  Don't make claims about your next release if you're not going to uphold those claims.\n\nAs for ignorance of facts, I largely operate off of what I'm told.  For months I repeatedly politely requested why Aaron felt that ~/Desktop was broken, and repeatedly he said he intended to get rid of it.  Aaron never did explain (that I've seen from reading here, his blog, and all his interviews that I can find) what exactly is wrong with ~/Desktop, but he did repeatedly say he wants to be rid of it.\n\nSo now you claim that we'll likely have the same thing in the future, except I find that hard to believe.  "
    author: "T. J. Brumfield"
  - subject: "Re: Most requested features"
    date: 2008-06-02
    body: "True. For whatever reason he hasn't replied to that question."
    author: "Kapri"
  - subject: "Re: Most requested features (add Mac OS menu bar)"
    date: 2008-05-29
    body: "I would like to add a Mac OS style menu bar at the top of the screen as a requested feature. As far as I see this is still quite broken. Lubos Lunak offered to take a look at this sometime \"post-4.0\", which I think would be now."
    author: "furanku"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "Yep, the missing autohide sure is a pity when your on a laptop and vertical size matters :-(. It was on the featureplan but it seems  like there was not enough time.. \nI think the main reason was that it would depend on some (relative new) changes to the containment, and those changes was ready so late it didnt get implemented. Perhaps some devs can comment? Is there any chance it will get backported for 4.1.n like with 4.0.2?"
    author: "Steve"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "No autohide, eh?\n\nI will not use KDE 4 until the panel autohides."
    author: "reihal"
  - subject: "Re: Not there yet"
    date: 2008-05-29
    body: "+1"
    author: "noorker"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "\"It was on the featureplan but it seems like there was not enough time..\"\n\nIt's still on the 4.1 feature plan:\n\nhttp://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan"
    author: "Anon"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "Don't worry yet.  This isn't actually a Beta.  In the great KDE4 naming tradition, it is incorrectly named.  \n\nIt is really just a TRUNK snapshot that is called a Beta.\n\nYes, I miss autohide as well as several other desktop features.\n\nLike many others, I don't want a radical new desktop.  I like what comes with KDE3.  It had some bugs and a few design issues under the hood, but the interface was fine.\n\nWe have made the Vista mistake!  If Plasma isn't going to be able to provide the same features as Kicker and KDesktop, we should seriously consider offering both desktops in KDE4."
    author: "JRT"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "It's quite ready. It's stable and is now quite configurable. Granted, it is not feature-complete with the 7 years worth of features accumulated by KDesktop/Kicker, but that's to be expected.\n\nPlus, porting KDesktop/Kicker over to KDE4 and maintaining it would be a ton of work. It would probably take at least 1 release cycle, during which time no work could be done on Plasma since all of the desktop-shell devs are hard at work porting these 2 applications into a usable state. This would lead to Plasma stagnating and remaining virtually in the same state for another 6 months at the least. Not a good idea, and this will never happen.\n\nYou can always use the KDE3 KDesktop/Kicker with KDE4 if you don't like Plasma anyway."
    author: "Jonathan Thomas"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "> You can always use the KDE3 KDesktop/Kicker with KDE4 if you don't like \n> Plasma anyway.\n\nDuh?  That won't fix the bugs in KDesktop/Kicker will it?\n\nBut, the main point that you missed is what was stated:\n\nIf ... , Then ... .\n\nEmphasis should be in the IF, and the subjunctive context in which it was stated.  \n\nInstead, we are treated to yet another straw man. People that don't reply to the main clause in a sentence usually have their own agendas.\n \nPlasma needs to provide all of the featrues of KDesktop/Kicker.  Saying that this issue isn't important because there is no alternative is useless.  If Plasma can't do this, then it would be better to port the KDE3 desktop and get back to Plasma in 6 months.  I hope that this isn't necessary."
    author: "YAC"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "My last statement which you are attacking so vehemently is but a small aside I placed at the end of my comment. I am most certainly not putting up a straw man. I was merely suggesting that you use the existing kicker/kdesktop in the meantime while Plasma gains these features you so desire.\n\nGoing through my post I:\n-Refuted your first paragraph/point. KDE 4.1 beta is aptly named a beta.\n\n-Then, in a separate paragraph, you state that you miss panel autohiding along with some other features. There is no point or anything here to refute, as, unless you are lying, you truly do miss these features. That's your personal opinion. Nothing I can do here.\n\n-Your next paragraph follows the same pattern as you last paragraph, you are just stating your personal opinion. There's nothing to debate here either unless I can prove that you actually hate KDE3.\n\n-Now we get to the last paragraph, my response to which you accuse as being a response to a straw man argument. In this paragraph, you state that KDE4 should offer KDesktop/Kicker alongside Plasma. I refute this by saying that KDE shouldn't (and won't) since it would be a monumental amount of work which would probably also delay work on Plasma for another 6 months. This was the main point of my post.\n\nNow as an aside to my main argument, I suggested that KDesktop/Kicker was still a viable alternative if you can't live without feature X in Plasma.\n\nBut you, you turn around and accuse me of not addressing/partially addressing all of you claims in an attempt to weaken your argument. I have refuted all refutable claims that you have made. You, on the other hand, hone in on my aside and try to attack it as a straw man argument. It's kinda ironic how in fact *you* were the perpetrator of straw man logic in the end.\n\nPlease, don't insult my intelligence and/or my logic."
    author: "Jonathan Thomas"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "I think he's talking about this: \"If Plasma isn't going to be able to provide the same features as Kicker and KDesktop...\"\nTo which the response is, I guess, that it will all come in due time and you can't expect a brand-new desktop shell to have all the features of a fully mature desktop right at birth. Right?"
    author: "yman"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "There will always be whiners, no matter what the devs do to make KDE even better. Some people haven't seen the wonderful improvements even over KDE 3.5x because they are too busy finding faults.\n\nPlasma has issues yes, but it has also come a long way and has improved a lot since the 4.0 release. At this pace I am sure that KDesktop's and Kicker's features will be surpassed in the not so distance future.\nI would also like to have a panel as feature rich and flexible as the good old champion Kicker but the Plasma panel is also evolving, slowly but surely. We are all eager to see our favourite KDE 3.5x feature being ported to 4 but we have to simple exercise patience and help the devs in whatever way we can. That's the fastest and best way to get there."
    author: "Bobby"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "Hi JRT,\n\nPlease stop using your sock-puppet \"Yet Another Critic\" identity to make it seem like more people agree with you.  Thanks!"
    author: "Anon"
  - subject: "Re: Not there yet"
    date: 2008-05-31
    body: "\"Granted, it is not feature-complete with the 7 years worth of features accumulated by KDesktop/Kicker, but that's to be expected.\"\n\nHey genius,\nautohide isn't just one of the \"7 years worth of features\", it's the kind of thing you find in any panel/taskbar project worth mentioning. \nEven Gnome 2.0, when they ported Gnome to GTK2, which was a massive change from GTK1, as big as the change from QT3 to QT4, they got features like this one right.\nFor fuck sake, autohide is a feature that was present in windows *95*.\n\nAnd you wonder why everyone says KDE4 isn't even considerable alpha as it lacks so many *basic* desktop features ? all those things traded off for.. plasmoids ? all I see are retarded desktop widgets like Konfabulator and OS X dashboard. "
    author: "Anon"
  - subject: "Re: Not there yet"
    date: 2008-05-31
    body: ">Even Gnome 2.0, when they ported Gnome to GTK2, which was a massive change from GTK1, as big as the change from QT3 to QT4, they got features like this one right.\n\nThey probably didn't do a complete from-scratch rewrite of their desktop shell. It's just a port from GTK1 to GTK2. If KDesktop and Kicker were ported from Qt3 to 4, we wouldn't have this problem either. But instead KDE opted to rewrite the shell into something greatly more flexible.\n\nYou also can't compare that to a non-free software project, which usually gets all its features at once.\n\nAnd, for your information \"genius\" hardly anybody calls KDE4 alpha. As for 4.1 it has a respectable set of features. It has gained maturity, though it still has a little ways to go. If you can't see past the plasmoids we have to day, then that's you fault."
    author: "Jonathan Thomas"
  - subject: "Re: Not there yet"
    date: 2008-06-01
    body: "\"And, for your information \"genius\" hardly anybody calls KDE4 alpha.\"\n\nDon't tell me you're taken back by a personal comment, since those don't exist on the dot.  However, to be fair, a whole slew of bloggers and reviewers have critized KDE 4 for lacking basic desktop features you'd expect.\n\nAbout a month back or so, I was concerned that Plasma was getting a serious rewrite because of QT 4.4, and there were seperate changes taking place at the same time.  I suggested then, that it would be nice to have multiple repositories (via git perhaps) when considering making large changes to important functions such as Plasma.  I was concerned about making major changes to Plasma, when everything was supposed to be stabilizing towards a KDE 4.1 release.\n\nI was told that there wouldn't be any more changes to Plasma before 4.1, except the digest suggests new features have been added, and now this infamous Desktop change as well.\n\nAside from my hatred for the desktop change itself, I see a bigger problem.  When rewriting the basic desktop shell from scratch, I'd consider it very important to lock in core libraries first (which you guys did early, except you were dependent on a feature you couldn't use yet until QT 4.4, which meant readdressing things, which might give people pause enough to hold off the 4.0 release until you could use 4.4 and do things the right way from day 1), plan for major feature sets, implement core important features first, and then simultaneously try to stabilize those core features while integrating smaller, new features.\n\nHowever, what I've seen (largely from reading the digests) is a bunch of small features constantly being added to Plasma before important basic features like panels, desktop, icons, etc. are addressed.\n\nKDE 4 isn't meant to look and operate exactly like KDE 3, but it should provide most of the same basic features you could to expect from a modern GUI, let alone any GUI."
    author: "T. J. Brumfield"
  - subject: "Re: Not there yet"
    date: 2008-06-01
    body: "\"And, for your information \"genius\" hardly anybody calls KDE4 alpha\"\n\nYou don't want to see the boatload of KDE 3 users complaining but they are there no matter how you like it. Closing your eyes doesn't make reality disappear. No matter how good the underlying foundations are (true, the KDE4 libs, except for plasma, are all in good shape) if the desktop has less user features and GUI configuration (as opposed to go in a hunt for config files) than something you would have expected in 1995 it's alpha quality, not production release. Yet distributions start shipping KDE4 as is, following the \"KDE4 is good enough\" meme. Fedora 9 only ships KDE4, openSUSE 11 will ship KDE4 by default with an optional KDE3. They will probably all ship KDE4 as the default when KDE4.1 will be out.\n\nPlasma is dragging down all the other efforts that went into making KDE4 great. Qt4 is a great toolkit and Phonon, Solid and other KDE additions to it are all great ideas too. But plasma is making the users flee in terror and turns KDE4 future into uncertainty.\n\nWell, it would be unfair to say that plasma is the only one, there is a second offender : Dolphin. Everyone cops out by saying \"if you don't like dolphin, use Konqueror, which is still there\" but they don't seem to understand that the dolphin kpart made a lot of old konqueror features disappear. Even if you use Konqueror you can't drag and drop via Spring Loaded Folders like in KDE3. The right click context menu is the same and lacks a lot of basic functionality. Like a menu to extract an archive without having to open it.\nKonqueror has become useless because it's just another name for Dolphin."
    author: "Anon"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "Not a beta?\nDo you mean autohide could still be implemented?"
    author: "Adrian"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "He just doesn't understand what beta means. It's just a point in time in the KDE release cycle when we start to focus on stabilizing, and will only add bugfixes."
    author: "jos poortvliet"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "Ok, that's what I thought. Bad news :-("
    author: "Adrian"
  - subject: "Re: Not there yet"
    date: 2008-05-28
    body: "Well, it IS possible someone MOSTLY implemented the feature, and the fact it doesn't work can be considered a bug ;-)\n\nBesides, the beta was tagged a few days early. So there IS a chance it will be implemented. Finally, as everything in Plasma is a Plasmoid, I can imagine someone can come up with a pannel applet which DOES have auto hiding after the release. Code it in Python, and it can be automatically downloaded using Get New Applets :D\n\nNot sure how feasible that is, technically, but you can still have hopes ;-)"
    author: "jos poortvliet"
  - subject: "Re: Not there yet"
    date: 2008-05-30
    body: "Let me give you the same I said to TJ.\n\nHere you go:\n\nAnyway, the change in icon behavior you are angry about is done because it would be relatively hard to make icons work like they used to - and yet it would, in a sense, be a waste of time. After all, plasma is about trying new things, trying to innovate. Innovation is difficult, often doesn't work and takes time.\n\nNow you probably don't want us to do anything new and original, but we think we must. So we're gonna ignore those who are holding us, and The Free Desktop, back. Sorry about that, but we aspire more than just appealing to a few geeks. We want to overtake Apple and MS - and for that, we need to be clearly better.\n\nAnd to be clearly better you need to to things different. Try new things. Innovate. So that's what we are trying.\n\nIf you can be constructive, and tell us better, new things to solve old problems - we will listen. If you can only scream 'I want it to work EXACTLY like KDE 3.5.x - then just go to KDE 3.5.x and never look back at 4.x because it WILL NOT HAPPEN. We sure want the same features, but NEVER the same THING.\n\nWe're going for the future, and we're leaving the old behind. If you can't keep up, stay with 3.5, nothing else I can say."
    author: "jos poortvliet"
  - subject: "Re: Not there yet"
    date: 2008-05-30
    body: "It almost sounds like you want to make changes because you have a pie-in-the-sky idea that KDE4 will be revolutionary. KDE 3.5 is a fairly good desktop environment that does a lot of things well so it seems reasonable to leave many things as they were before and only make changes that actually are improvements.\n\nAnd then when people point out that something isn't done well in KDE4 and was better in KDE 3.5 you insult them by saying that they can't keep up. "
    author: "jeff strehlow"
  - subject: "Re: Not there yet"
    date: 2008-05-31
    body: "Change isn't always inherently good.  Where is the benefit here?\n\nI'm not against change wholesale.  I'm against the removal of choice.\n\nYou suggest that you want the same features of KDE 3.5, except we're losing a feature here.\n\nThe old and new can exist side-by-side.  Instead you opt to force everyone (willing or otherwise) into a new paradigm."
    author: "T. J. Brumfield"
  - subject: "Re: Not there yet  <- I don't agree"
    date: 2008-06-02
    body: "I'm not forced to anything by KDE4 T.J.B.! I like those changes, and I want them. Don't assume that your opinion equals everyone's opinion.  "
    author: "Dog"
  - subject: "Re: Not there yet  <- I don't agree"
    date: 2008-06-02
    body: "I never said everyone agrees with me.  In fact, my point is that people disagree.  If you like operating out of an applet as opposed to working directly on your desktop, then do so.  But I want the freedom to work as I please."
    author: "T. J. Brumfield"
  - subject: "Waiting to test"
    date: 2008-05-28
    body: "I was hoping ubuntu packages would be ready for this night... but seems like I'll have to wait a bit more for the fun.\n\nAny previews from people who are running it, comparing to 4.0 and telloing how stability is (remembering 4.0 is still a kind of beta in terms of stability).\n\n:)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Waiting to test"
    date: 2008-05-28
    body: "Stability in what? Different apps are very different, imo."
    author: "anon"
  - subject: "Re: Waiting to test"
    date: 2008-05-28
    body: "Applications seem quite stable to me, while Plasma has some bugs and feels a bit quirky in general. Because of the feature freeze, I'm confident that these get fixed."
    author: "Stefan Majewsky"
  - subject: "Re: Waiting to test"
    date: 2008-05-28
    body: "Overall maybe?\nBut if you want to list each app and give individual ratinging on all of them, I will not complain ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Waiting to test"
    date: 2008-05-28
    body: "I am working on the KDE 4.1 Beta packages right now for Hardy. Once they are all completed, I will more than likely blog about it, or you will see some news about it at http://www.kubuntu.org."
    author: "nixternal"
  - subject: "4.1 packages"
    date: 2008-05-28
    body: "hardy ??? i am already using Intrepid"
    author: "pvandewyngaerde"
  - subject: "kedit"
    date: 2008-05-28
    body: "where is kedit?? is used to be in kdeutils (at lest in version 3.5.9), i just compiled and is not there :\\\n"
    author: "Raul"
  - subject: "Re: kedit"
    date: 2008-05-28
    body: "Do you mean kedit the text editor? I think It has been removed from KDE and replaced by kwrite. IIRC the only reason kedit was kept in KDE 3.5.x was because of bidirectional language support in kedit."
    author: "fred"
  - subject: "Re: kedit"
    date: 2008-05-28
    body: "And kwrite now has bidi support, so kedit was retired."
    author: "Will Stephenson"
  - subject: "Re: kedit"
    date: 2008-05-28
    body: "It's gone. It was unmaintained for quite some time already, and kwrite is a good simple editor that should cover pretty much all usecases."
    author: "sebas"
  - subject: "Re: kedit"
    date: 2008-05-28
    body: "It's gone. It was unmaintained for quite some time already, and kwrite is a good simple editor that should cover pretty much all usecases. Kwrite has all the features that kedit had (and kwrite didn't) in kde3, and it's quite a bit faster."
    author: "sebas"
  - subject: "Re: kedit"
    date: 2008-05-28
    body: "Thanks, i always throught kwrite was more heavy, i will start using it"
    author: "Raul"
  - subject: "printing support ?"
    date: 2008-05-28
    body: "And what abou priting support in KDE 4.1 ? because in KDE4.0 is not..."
    author: "levin"
  - subject: "Re: printing support ?"
    date: 2008-05-28
    body: "KDE 4.0 supported printing just fine. Lot of the advanced printing functinality of KDE 3 was not there, but printing is supported and works.\n\nAs for your question, if you read the anoncument you see \"A new printer applet provides unparalleled printing power and flexibility on the Free Software Desktop.\". \n\nSo my guess is thet they have done some improvment and extended the printing support a bit from what it was in 4.0. And of course improvments and new cababilities in Qt 4.4s printer support also helps."
    author: "Morty"
  - subject: "Re: printing support ?"
    date: 2008-05-28
    body: "Printing support has improved, look for an article in the Digest soonish (if I ever escape form work for long enough to write it).  Basically, QT4.4 gives us a vastly improved Print Dialog on Unix, which I've further enhanced with a few extra Cups features.  Printer management is still an issue, Jonathon Riddell has imported a new python based tool which I haven't had time to look at yet so can't comment on.\n\nJohn."
    author: "Odysseus"
  - subject: "WONDERFUL!"
    date: 2008-05-28
    body: "Many people bitched about KDE 4.0, including me (though only in my head).\n\nAfter some clarifying words in aseigo's (and others) blog(s), I was assured that KDE 4.1 will kick ass, and have been at war with KDE 4.0 bitchers from then on.\n\nBut I've tried to use KDE 4.0 all the time. In fact, KDE 4 is the only reason I'm using KDE, I never liked KDE 3 a lot. Approximately one and half a year ago, I discovered first mockups of KDE 4 and became certain that it will just rock. To become a KDE 4 user, I got rid of Gnome (have been a gnomie for years) and got myself KDE 3. I never managed to get a \"beautiful\" desktop with it, because I never wanted to put much effort into it. So it looked like kindergarten but was a very powerful tool to me.\nI've been following KDE 4 development very actively all the time, and now the day has come I've been waiting for - the 4.1 beta!\n\nWhen I get home from work today, I will do nothing before the beta is not installed on my system.\n\nJudging from the screenshots and changelogs so far: GREAT WORK!\nThis looks like a total usable release, I'm ESPECIALLY looking forward to finally using the new Kontact, Kontact is IMO the best out of all KDE programs. I hope my kitchensync setup continues to work nicely.\n\n\nSo to sum this up, thank you for KDE!!!"
    author: "fhd"
  - subject: "Re: WONDERFUL!"
    date: 2008-05-28
    body: "Man I like KDE a lot, but shouldn't you wait until you try it before making this kind of post? :D"
    author: "ad"
  - subject: "Re: WONDERFUL!"
    date: 2008-06-02
    body: "Yeah I should, but I was at work and it is all windozes here :)\n\nHowever, I can underline it now, 4.1 ROCKS!\n\nI even switched from Kubuntu to openSUSE to try it (Kubuntu's KDE support is just sick lately compared to the GNOME Flavour)"
    author: "fhd"
  - subject: "Re: WONDERFUL!"
    date: 2008-05-29
    body: "I love to see people converting from Gnome :_)\n"
    author: "Riddle"
  - subject: "Re: WONDERFUL!"
    date: 2008-05-29
    body: "I AGREE, i have using kde 3(i love it)for years, but is realy nice what are you doing whit kde 4, "
    author: "debianita"
  - subject: "Conclusions after using it a few hours"
    date: 2008-05-28
    body: "First of all I want to say I know:\n- This is the first beta, not ready for production\n- It's a big project and developers had too much work to do\n- I'm using packages from the experimental branch of debian, and this is not good at all.\n\nafter all I must say I really like KDE, I think it's the greatest project made by the open source community, but... imho, KDE 4.1 isn't what I was waiting for, at least at this point (beta1).\n\nI found some problems just starting, some of have already been posted here.\n1.- No autohide panels, I really need them\n2.- The widgets can't be moved in the panel (I mean change the position in the panel)\n3.- It's not possible to configure the widget to be on the TOP of all the other windows, this would be even better than 1 & 2, since we could have a menu on the top left corner. 1 & 3 together would be the typical stuff that makes KDE the best desktop ever. Example: One widget on the top-left corner always over all the windows, and an auto hiding panel in the bottom with the task bar, paginator...\n4.-Plasma was using 50-70% of my CPU. I don't know why. My system was very slow.\n5.-I don't like the theme, but this is not important, I just wanted to say it :-)\n6.- I can do the same things in KDE 3.5.9 much faster than in 4.1. This is what is killing me, when you upgrade the software, you must:\n1.- Increase productivity of the users\n2.- Make it more attractive\nIf you change priorities you'll get a copy of Windows.\nThere are a few things you can do in 4.1 and you can't do in 3.5 to make your life easier, and a lot of things in 3.5 you can't do in 4.1.\n\nIn my case, I can't give up productivity in favour of eye-candy.\n\nI want to make this clear: This is just an opinion.\nI understand KDE developers, and I'll support them, but this is the second time I don't like a KDE release, and I feel like if I say it, it might help. I just want to be constructive.\nWhat do you think about this?"
    author: "Adrian Ribao"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "\"I just want to be constructive. What do you think about this?\"\n\nThere's some decent information in there, and you've civilly made some concrete suggestions that devs can actually work on, so I think this counts as contructive :)\n\n1) They are still on the 4.1 Feature Plan - hopefully there will be time to add them.\n\n2) This is known and planned, but unfortunately almost certainly won't make in to 4.1 due to the freeze :/ \n\n3) Again, 4.2 - it's on the feature list ( http://techbase.kde.org/index.php?title=Schedules/KDE4/4.2_Feature_Plan ) as \"Top-level windows plasmoids\".\n\n4) Sounds like a bug - report it, making a list of what widgets you had running at the time.\n\n5) Heh :) Try this one:\n\nhttp://www.kde-look.org/content/show.php/Glassified?content=81388\n\n6) Ok, this is the weak part of your post: you don't give any examples at all.  Also, comparisons to Windows are unfounded and *extremely* irritating to developers.  \n\nAlso, the \"KDE4 is just eye-candy\" is very annoying and untrue.\n\nAnyway, hope this helps! :)"
    author: "Anon"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "You are right, kde 4 is not only eye-candy, I know it, but it's one of the most important parts, and I shouldn't have wrote about windows, I'm sorry about this.\n\nI had a few plasmoids running, I don't remember the exact names: calc, kate sessions, terminals, notes, and perhaps one or two more, but when I took a look at the system CPU usage, I only had the picture frame plasmoid running. and the two panels.\n\nI'll post some examples, but now I must go for a while.\n\nI think it would be great if we don't have to wait 6 months until we can use all the features we miss, maybe they can be implemented in each 4.1.x release."
    author: "Adrian Ribao"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "It would be helpful if you posted more information about your system, such as graphic card and driver, distro etc."
    author: "Hans"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "2) Moving plasmoids inside the panel works (almost, with a little bug) fine even in 4.0.3. If it does not work in trunk, it is a bug that can and needs to be fixed, isn't it?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "Currently, moving around applets that have already been added to the panel is impossible without removing and re-adding them entirely. This goes for both trunk and 4.0.x."
    author: "Jonathan Thomas"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "I miss kicker :("
    author: "MamiyaOtaru"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "The 4.0.3 binaries you're using are patched. That feature is a patch Stefan Binner added to the plasma-4.0-openSUSE branch, and we're also shipping it in Fedora."
    author: "Kevin Kofler"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "can we please vote againt panel hiding if less than 50 percent of the users need it ?\n\n"
    author: "yyyyyyyyyyyyyy"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "What do you mean \"vote against it\"?"
    author: "Anon"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "That's quite and arrogant attitude. You do realize that if all features with less than 50% of the userbase was removed, there would be almost nothing left? For some of us its quite a dealbreaker (i'm still using 3.5.9 exactly because of it) and for those who do not use it its just a configuration option that's not checked..."
    author: "th"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "I think/hope it was irony."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "I support some of the points, especially those pertaining to the panel. The panel is the thing that I am most unhappy about. After 4.0 release I thought that since the panel is a plasmoid then it wouldn't take that long to at least bring it to match Kicker. I am sure that it will surpass Kicker in time but TIME is the key word here because a lot of us are running out of patience.\nThe panel is just not there yet but I hope that it will be by the time 4.1 is released.\n\nBTW, aren't there any one here who can write a panel plasmoid? Is it that difficult?\nWhat about the XQDE project? Anyone knows if it's still alive?"
    author: "Bobby"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "\"BTW, aren't there any one here who can write a panel plasmoid? Is it that difficult?\"\n\nHaven't you tried?"
    author: "Boudewijn Rempt"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "Lol, I have two left hands when it comes to programming ;)"
    author: "Bobby"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "1 and 2 are definite annoyances.  Hopefully they will be fixed before 4.1.  \n3 will likely be somewhat difficult to achieve, given how plasma is implemented.\n\n4 is some sort of bug.  I'm using the same packages from debian and am not seeing this.  What video card and drivers do you have?\n\n5 is preference.\n\n6 needs elaboration.  I think 4.1 is much nicer than 3,x ever was.  Please give concrete example of exactly what is faster in 3.5.9.  THat will help people actually fix things."
    author: "Leo S"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-28
    body: "Kickoff is slower to navigate than Kmenu to the point that after trying it, I grew so frustrated, I only use Alt+F2 any more."
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "It's 2 clicks to switch to the traditional menu (try right click on the menu button)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "Thanks for the info.  I am aware a classic menu was implemented (for which I am grateful) but Alt+F2 is now just a habit.  I find it faster than either menu."
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "So why complain about Kickoff T.J? I guess you enjoy griping about KDE 4."
    author: "Bobby"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "He asked specifically for an example of how something in KDE 4 slows you down productivity wise.  I provided a specific example."
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "Hey, how do you like the new Alt-F2 launcher, I mean the one just recently added before the beta? IMHO it really rocks..."
    author: "jos poortvliet"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "I have only seen screenshots.  I have no intention of using KDE 4.1 ever.\n\nThat being said, the screenshots do look nice for the new Krunner dialog."
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "\"I have no intention of using KDE 4.1 ever\".  So..\n\nWhy do you come here\nwhy do you hang around\nI'm so sorry\nI'm so sorry\nWhy do you come here\nwhen you know it makes\nthings hard for me\nwhen you know, oh\nwhy do you come?\nWhy do you telephone?\nand why send me silly notes?\nI'm so sorry"
    author: "morrissey"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "I guess you haven't noticed the search box in Kickoff. Apart from that you can add all your most often used programmes to favourites. I love Kickoff because of these features. I don't navigate, that was yesterday. I just click once on the start button and then my favourite programme or i use the search box. I bet that you can never beat me using the traditional menu :)"
    author: "Bobby"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "I don't need a search box if I can navigate in less than two seconds with a traditional menu, or just type it in via Alt+F2.\n\nopenSUSE had search in the traditional menu before developing Kickoff.  I'm not against search.  I'm against Kickoff's scrolling within a menu, and several slow layers.\n\nVista's menu also comes under fire for scrolling within the menu.\n"
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "Relax dude :D We already have two menus in KDE 4, Kickoff and the traditional KMenu. And more menus are coming, Raptor Menu and Lancelot ;).\n\nPersonally, I always use Alt+F2 since it is the fastest way to launch a (GUI) program."
    author: "fred"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "You miss my point.  Initially Kickoff was going to be the only option, and we were told to live with it.  It wasn't until there were enough complaints that an alternative was presented.\n\nRemoving choice is not what KDE is supposed to be about."
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "I don't know since when the traditional KMenu existed, but IIRC it already existed early this year (maybe 4.0? or even earlier), maybe you were misled by some people saying Kickoff the only option? :D\n\nIf Kickoff was the only option, and then they added the traditional KMenu later, that means the developers listen to users, no? :p"
    author: "Anon"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-31
    body: "It was added before the 4.0 official launch.  But during the Alpha and Beta launch, it was the only menu."
    author: "T. J. Brumfield"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-06-01
    body: "do you ever not complain in dot, seems that is all you ever do, and since you have no intention of upgrading to 4.1, why not move to gnome then they can put up with you"
    author: "anon"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "I also experience that I use Alt+F2 extremely more often than in 3.5.x\n\nBut the reasons are different:\n\no I changed my own customs and learned then names of executables of my favourite applications.\n\no KRunner has been improved over Alt+F2 in 3.5.x\n\no Kick-off is mainly for beginners. I use it not that often, but I do use it whenever I need to search for an application I do not know the name of. The search function indeed is very good.\n\nIndeed, I believe the combination of KRunner and Kickoff is a real improvement over the classic K-Menu. \n\n"
    author: "Sebastian"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "I found many problems with the panels and plasmoids. KDE crashed a few times.\nMoving some of the widgets in the desktop was a pain, very slow and buggy.\nI decided to disable all the desktop effects, but the system was still very slow.\nCTRL+F12 was very very slow also, between 3 and 5 seconds to show up.\n\nThe widget used to show the content of a folder is very useful, but not 100% usable yet. It has no scroll.\nWhen I added a system tray widget to the new panel, it didn't work, neither in the desktop.\nResize some widgets was weird, for example, I couldn't resize the widget that shows the devices to get all the devices shown at the same time. Resize in one direction only worked in some widgets.\n\nI think my computer is no too old and should run KDE4 smoothly. Besides, I have a GeForce FX 5600.\n\nI also had problems with konqueror, but I think this is because of Debian. When I was using it as a web browser, the status bar was empty, the icon of the microformat plugin was not shown. The search field that usually is on the right of the url bar, wasn't there, and the zoom of the web pages is very slow also.\n\nWhen I wrote about all the things we can do in 3.5 and not in 4.1 it was basically because of the panel, but I'm sure there is more things I haven't tried yet.\n\nI thought that maybe it would be good to assign shortcuts to the widgets, I mean, if you press \"c\" the clock shows over the windows, and also if you move the mouse to one specific place like top-left corner, the Menu widget is opened. This is just an idea for 4.2\n\nI just used 4.1 for a few hours, so I can't say much more about it, I had to get back to work and I decided that 3.5.9 was better. Anyway, I'll probably install it again in the near future, I need to keep testing."
    author: "Adrian Ribao"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "KDE 3.5x is definitely snappier than 4.0x independent of hardware but 4.0x seems to handle my nvidia card better than 3.5x. In 3.5x, if I have composite turned on without using Compiz then the desktop would simple freeze after not using it for a while and the only thing that would help is pressing the restart button. That doesn't happen in 4.0x but you have to use the nvidia hack to get a little speed if the composite effects are turned on.\n\nThe panel and plasmoids have gotten better with the Beta 1 release but there are still quite a few glitches and missing features."
    author: "Bobby"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "Don't forget to report bugs.\n\nRemember that because developers are so close the design, they often unintentionally adjust their usage patterns to what works.  So if something doesn't work for you, no matter how obvious, report your bugs.\n\nThis is Open Source."
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "> 4.-Plasma was using 50-70% of my CPU. I don't know why. My system was very slow.\n\nsame issue in my system!\njust remove the \"showdesktop\" plasmaoid from the panel and you can use KDE4 again! :)\ni don't know if \"a showdesktop-application\" is so difficult, but it is buggy since a long time, and if it works, you can go and get a coffee while minimizing windows with this plasmoid. ;)\n\nanyway, kde4.x kicks everything, i love it, and, everything is getting better compared to kde3! you just have to be patient :)"
    author: "KDE4 daily on openSuSE10.3"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-30
    body: "Hmmm.  I just opened a bug report with openSUSE because of this same problem (having never experienced it with previous 4.0/4.1 builds), though I'm running on 11.0 factory.\n\nAfter reading your message, though, I got rid of the showdesktop icon from the panel, and I can confirm that the spike is gone and my system isn't running on all cylinders any more. \n\nBizarre, but then, it is still a beta... ;)\n\nCheers."
    author: "elsewhere"
  - subject: "Re: Conclusions after using it a few hours"
    date: 2008-05-29
    body: "> 6.- I can do the same things in KDE 3.5.9 much faster than in 4.1. This is what\n> is killing me, when you upgrade the software, you must:\n> 1.- Increase productivity of the users\n> 2.- Make it more attractive\n\n That's intentional. Now that it takes more time to do something, you can longer         \nenjoy in eyecandy."
    author: "vf"
  - subject: "No character encoding selector in KWrite's menu?"
    date: 2008-05-29
    body: "Now user has to be determine character encoding by oneself when opening a external txt file.If he wants to change encoding after erroneous judgement,he must reopen it,and re-determine the file encoding in drop listbox until everything is OK.\n\nIMHO,it's less convenience than before.More dialog,more click,more waiting."
    author: "Lie_Ex"
  - subject: "Re: No character encoding selector in KWrite's men"
    date: 2008-05-29
    body: "You can add \"encoding\" to the toolbar - thats the fastest way to change it anyway...\n\nBut you're right - i don't understand it either why it was removed."
    author: "ecki"
  - subject: "Re: No character encoding selector in KWrite's menu?"
    date: 2008-05-29
    body: "Changing the encoding after opening a file used to reload said file anyway. What I rather like to see is KWrite not corrupting text files with wrong encoding applied. Autodetection of encoding would also be nice to have (isn't a framework doing that was being worked on, also for use in conjunction with the new spell checking framework?)."
    author: "Anon"
  - subject: "Re: No character encoding selector in KWrite's menu?"
    date: 2008-05-30
    body: "I would love auto-detection of encoding.\nUbuntu, for example, uses by default utf-8, but the last time I've upgraded it I forgot to change to iso 8859-1. So, now some files are utf-8 and some 8859-1, I have to often change the encoding in kate when opening a file :-("
    author: "Iuri Fiedoruk"
  - subject: "Where are the packages for FreeBSD?"
    date: 2008-05-29
    body: "KDE 4.1 Beta packages are available for even MS Windows and Mac OSX!, for those who do not need KDE. Where are the packages for FreeBSD, the ones who really need it?\n\nDoes this mean KDE developers do not use FreeBSD or any other BSD?"
    author: "Unga"
  - subject: "Re: Where are the packages for FreeBSD?"
    date: 2008-05-30
    body: "Or maybe KDE developers only create OS-specific packages, rather than distribution-specific packages?"
    author: "yman"
  - subject: "Submenu delay?"
    date: 2008-05-29
    body: "I think this is a QT4 thing, but is the long delay in displaying submenus going to be fixed? It makes using apps seem heavy compared to KDE3 where submenus are displayed nearly instantly."
    author: "samwyse"
  - subject: "Re: Submenu delay?"
    date: 2008-05-30
    body: "This is a Qt bug, yes. What is worse isn't the delay per se, but the fact that you have to really STOP moving the pointer for the configured amount of time to make the submenu appear. This is annoying if you use a pen, because it is artistic to hold it still. The odd is that this bug is there since Qt 4.0.0, but nobody seems to care."
    author: "christoph"
  - subject: "Daily SVN Packages For ArchLinux"
    date: 2008-05-30
    body: "It may be of interest to some users to know that ArchLinux has had daily binary packages from SVN available every day for the last 2 months and will continue to do so for at least the rest of this year, and maybe always. Even if you don't primarily run ArchLinux it is easy to install into a separate 5Gb to 10Gb partition and dual boot, or run in a VM. More details here...\n\nhttp://bbs.archlinux.org/viewtopic.php?id=44507&action=new"
    author: "markc"
  - subject: "Some problems"
    date: 2008-05-30
    body: "I am periodically testing KDE4 SVN under Gentoo. I have never seen flash working in Konqueror so far :-( Some users told me that it is working but all of them are using nspluginwrapper. I have 32bit system so I don't have it and flash is not working. \n\nNext is session management. In KDE3.5 I want some application to be started automatically (e.g. kopete, kmail). I'm using \"restore session saved by user\" (or how is it called in english) option in KDE3.5. This option is also present in KDE4, but I haven't found how to save the session. What is working fine is \"restore previous session\" option. What it the trick there?\n\nI have read somewhere that tasklist in panel should support more then one row of applications. It should decide when to use more then 1 row and when not. I opened about 15 windows and still I had just one row of very small application \"buttons\". They are in fact bigger in height then in width and it was not nice. Is there any option to enable more rows? \n\nAnd the last point is that Phonon doesn't see my sound card in list of devices. I can se there just \"arts\" and sound is not working. My sound cars is SB Live 1024, quite old but well supported by ALSA.\n\nBut nevertheless KDE4 is a huge step forward and I hope that in the end it will be better than 3.5.X.\n"
    author: "KejPi"
  - subject: "this might be a stupid question, but...."
    date: 2008-05-31
    body: "...How do I move widgets inside the panel? I tried the 4.1-beta LiveCD, and couldn't figure this one out. \n\nSuppose that I want to move the taskbar or the clock around to a new location in the taskbar, how do I do it? Dragging didn't work. IS this merely a bug, or am I missing something?"
    author: "Janne"
  - subject: "Re: this might be a stupid question, but...."
    date: 2008-06-01
    body: "Good question. The ability to move widgets in the panel was in 4.0.4 (at least, and earlier ones I think) but I can't find it at all in the 4.1 beta"
    author: "Simon"
  - subject: "HTML and Javascript support for Plasma?"
    date: 2008-06-02
    body: "Does anyone know if Plasma supports plasmoids written in HTML and JavaScript yet? From what I've read, this is a feature scheduled for 4.1, but I can't find any documentation on Techbase nor any examples in SVN. :("
    author: "Anonymous Coward"
  - subject: "Dashboard widgets"
    date: 2008-06-03
    body: "Hello,\n\nis it now possible to use Dashboard Widgets on Plasma?\n\nIf yes, is there an example on how to download and install them?"
    author: "Yves"
  - subject: "NetworkManager Plasmoid"
    date: 2008-06-03
    body: "Will it be included in KDE 4.1?\n\nIt's the last piece of KDE which I currently need"
    author: "Yves"
  - subject: "Re: NetworkManager Plasmoid"
    date: 2008-06-03
    body: "s/KDE/KDE3"
    author: "Yves"
  - subject: "Date with KDE 4.1"
    date: 2008-06-09
    body: "I tried using KDE 4.1 for one day at my work. I have summarized my impressions at http://kashi.webhop.net/blog/Technology/index.php/archives/29"
    author: "Umesh Sirsiwal"
---
The KDE Project is happy to set <a href="http://www.kde.org/announcements/announce-4.1-beta1.php">the first beta of KDE 4.1</a>, codenamed Caramel, free today.  KDE 4.1 is intended to meet the needs of a broad range of users and we therefore respectfully request you to <strong>get testing Beta 1</strong>.  Beta 1 is not ready for production use but is in wide use by KDE developers and is suitable for testing by Linux enthusiasts and KDE fans.





<!--break-->
<p>Highlights of 4.1 are a much more mature Plasma desktop shell that returns much of the configurability that was missing in KDE 4.0, many more applets and look and feel improvements, the return of Kontact and the rest of the KDE PIM applications, and many improvements and newly ported applications.  The feature set is now frozen, so the developers look forward to using June and July to metamorphosing your bug reports into rock solid code, completing documentation and translating everything into your language.  A series of Bug Days where users can contribute quality assurance to the release will continue towards 4.1's final release on the 29th of July, so watch the Dot for details.</p>

<p>For more details, see the <a href="http://www.kde.org/announcements/announce-4.1-beta1.php">release announcement</a> and <a href="http://www.kde.org/info/4.0.80.php">info page</a> or if you are at LinuxTag, see <a href="http://www.linuxtag.org/2008/de/conf/events/vp-freitag.html">KDE 4.1 being presented in Berlin this Friday</a>.</p>





