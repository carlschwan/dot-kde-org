---
title: "KDE PIM Team Meets to Talk Akonadi and KDE 4.1"
date:    2008-02-22
authors:
  - "cschumacher"
slug:    kde-pim-team-meets-talk-akonadi-and-kde-41
comments:
  - subject: "Looking forward to it!"
    date: 2008-02-22
    body: "I've been using jpilot for a couple years now, but remember how much nicer KOrganizer was -- when it worked with KPilot. So I have high hopes for a robust system underneath! :)\n"
    author: "Joseph Reagle"
  - subject: "let's hope"
    date: 2008-02-22
    body: "let's hope they can bring kmail into the future instead of the anal past.  kmail can't really compare with email clients any more, it is too stale.  You just have to look at windows live mail, and apple mail to see the direction people want their email clients to go.  While kmail remains anal about html etc, linux will suffer.  People coming from windows do not want to be back in early 90's with a text based email client. "
    author: "anon"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I'm sorry, but please leave me out of your \"future\". I'm a happy KMail user since years and I really can live without fancy-shmancy HTML emails. I rather hope that they stabilize the IMAP support. I still have to remove KMail's local indexes occasionally because it just screwed up the order or got a hickup if a new mail comes in at the same time I delete a large mail.\n"
    author: "kk"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I believe one of the major reasons behind the creation of Akonadi was the IMAP situation in KMail."
    author: "Ian Monroe"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "*You* can live without it... but do remember that KDE-PIM Enterprise means that it could be used for business. And it's sad to say, but it's nowhere near a professional soft. I find it too messy (what is that window showing the parts of the e-mail ? KMail is the only one doing that, I find it useless, it should be optional instead of default).\n\nAs long as geeks make soft for geeks, there will be usability issues."
    author: "Cypher"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Funny, I frequently encounter usability issues in Microsoft Outlook 2007, and yet I'm sure it is used by millions of businesses around the world."
    author: "Paul Eggleton"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I didn't mean that the other products around were perfect either ;) There are usability issues in every product. Let's then try to make Kontact (and its sub-components) better than the others, which is not the case for the moment."
    author: "Cypher"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I second that. While I personally loathe HTML emails, most people want to be able to at least do some HTML formatting.\n\nAnd I agree with a previous post. That pane with all the different components on the email... what's that all about? Lots of wasted space IMO, and very messy."
    author: "NabLa"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "\"Lots of wasted space IMO, and very messy.\"\n\nShrink it down to zero size."
    author: "Anon"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Why? Why not come up with something better for displaying attachments?"
    author: "Segedunum"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Update to KDE 3.5.9 instead of complaining.\n\nFrom http://techbase.kde.org/Projects/PIM/Features_3.5.9:\nAggregated attachment view in the mail header area of the reader window"
    author: "mahoutsukai"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I find it to be a key feature, actually. I simply love this view of my messages. It allows me to quickly get out attachments, for instance. It also allows me to to see a message in plain text view by default, but switch to HTML view in a snap. That other programs don't have it is no argument. That is called innovation."
    author: "Andr\u00e9"
  - subject: "Re: let's hope"
    date: 2008-02-23
    body: "+1\n\nAs a programmer, I'm a friend of detailed analyses in general, and of this message components view in particular."
    author: "Stefan Majewsky"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Last time I checked, Kmail can read HTML quite nicely. \nThere's a little button at the top of the message that asks you to click on it if you wish to view the html message. \n\nIf you don't like the window showing the tree of files in the email, then you can turn it off by dragging the resize bar of that window to the very bottom of the app. \n\nYou shouldn't judge an application based on 4 minutes of use.\nKmail is *significantly* more configurable and user-friendly than MS Outlook and Apple Mail. \n\nAnd if you like those apps instead of Kmail, then go use Windows or Mac. \n\nHowever, if you are willing to learn a new (and IMHO better) way of doing things, then stick around and let the good times roll. "
    author: "nzivkovic"
  - subject: "Re: let's hope"
    date: 2008-02-26
    body: "> what is that window showing the parts of the e-mail ?\n> KMail is the only one doing that, I find it useless, it should be optional instead of default\n\nAre you kidding?\nThat is one of the greatest features i've seen in a MUA and I use it frequently. Don't you get any multi-part message? It gets really great when you have a mail with attached files embeded in another mail.\n\nAnd if you don't like, juste disable it in the config."
    author: "Renaud"
  - subject: "Re: let's hope"
    date: 2008-02-26
    body: "\"And if you don't like, juste disable it in the config.And if you don't like, juste disable it in the config.\"\n\nAppearance -> Layout -> Message Structure Viewer, BTW, for anyone who is curious."
    author: "Anon"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Yeah, I'm all set with your vision of the future, thanks. Been using Kmail for years and love it. You sound very well-versed...so much so that you should start your own mail client and leave ours alone."
    author: "Joe"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Oh come *on*. Don't bash people who complain about KMail's mediocre HTML support. I'm using KMail happily at home for years and I appreciate the work of all contributors a lot, but I would still very much like to see correctly rendered HTML mails (which I don't for almost 100% of them)."
    author: "Planetzappa"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "If you find that HTML emails are not displayed correctly, then it's a bug in the rendering engine that you should consider reporting. The few HTML emails I receive are usually rendered correctly."
    author: "ac"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I absolutely agree. Most people today do not even know about HTML etc. They don't need to. HTML should be default, with pure text optional for those who like it. Today every mail client can cope with HTML mails and the formatting info does not matter for today's bandwidth (not even mentioning spam!).\n\nIn addition, some nice things from Outlook should be integrated, e.g. a right mouse click option of \"Find related emails\" (simple but useful).\n\nThe most important thing of AKonadi will be non-blocking spam checking. This is what makes kmail a pain in the ass to use. I have been waiting for this for more than five years!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Yes, I agree that non blocking operations (getting data from the server, filtering, whatever else you want to do with it) is key. I am the original reporter of KDE's most hated bug back in 2002(!): http://bugs.kde.org/show_bug.cgi?id=41514\n\nPledging money to get it solved did not work, but I hope Akonadi will finally kill this bug."
    author: "Andr\u00e9"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "\"The most important thing of AKonadi will be non-blocking spam checking. This is what makes kmail a pain in the ass to use.\"\n\nThat the worst thing about Kmail, something that make me want to hate this programme sometimes. I just hope that this problem will be solved soon. Kmail has the potential of becoming a great e-mail client but there are still a few things to be ironed out before it gets there."
    author: "Bobby"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Even when compared to Evolution or Thunderbird it lags behind IMO. I am still using Kmail, not because it's such a good e-mail client but because Evolution failed me in openSuse 10.1 (it had some issues with storing my password) and i couldn't bother any more. I wasn't impressed by Kmail, especially it's complicated setup and speed but I continued to use it with the hope that it will improve."
    author: "Bobby"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Why exactly are we reinventing Evolution here anyways? Wouldn't it be easier just to port Evolution to KDE 4/Qt? Evolution is so much better, works great with MS-Outlook environments and does everything I need it to.\n\nWhy develop a separate program? Why not just merge it with Evolution? I though KDE and gnome had an interoperability/shared resources agreement a few years ago? Wasn't that the idea behind it?\n"
    author: "Mike"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "\"Why exactly are we reinventing Evolution here anyways?\"\n\nWho is \"we\"? I'm not even sure that Evolution predates KMail.  Anyway, I don't think that building a modular and well-designed PIM suite integrated with KDE counts as \"reinventing Evolution\", anyway.\n\n\"Wouldn't it be easier just to port Evolution to KDE 4/Qt\"\n\nI'm sure the GNOME guys would love it if someone ported their official mail client to a completely different toolkit!\n\nAnd no - \"porting Evolution to KDE 4/Qt\" is basically the same statement as \"starting from scratch, write a KDE 4/ Qt mail client that is a clone of Evolution\".\n\n\"Why not just merge it with Evolution?\"\n\nIt would be great if you had the slightest grasp of software engineering before you started second-guessing the KDE developers.\n\n"
    author: "Anon"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "Evolution has tons of bugs, it's probably the GNOME component which gets most complaints about bugginess on the Fedora mailing lists. And it would be infeasible to port it to KDE anyway, it's already hard enough for them to move it from the obsolete GNOME technologies it's using to the current GNOME ones."
    author: "Kevin Kofler"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "> Why exactly are we reinventing Evolution here anyways?\n\nIn case you are referring to EDS (Evolution Data Server): Akonadi is more a next generation implementation of the same principle, including a wider range of use cases (e.g. also handle e-mails).\n\n> Wouldn't it be easier just to port Evolution to KDE 4/Qt?\n\nMost likely not, but it would certainly be possible to port Evolution to Akonadi.\nIn December I started working on an EDS adapter for Akonadi based on the D-Bus enabled version of EDS, but then migration D-Bus got removed from the GNOME roadmap. Though I might have a look at a later time, i.e. once we have Akonadi released and our application stack upon it working properly.\n\n> I though KDE and gnome had an interoperability/shared resources agreement a few years ago?\n\nNo formal agreement, since this wouldn't be possible (both projects are driven by their contributor communities and do not have some kind of official body which can do binding agreements regarding development).\n\nThere is some consensus that service oriented strategies allow us to share certain infrastructure easier than bringing foreign components into the others software stack, but infrastructure can not be replaced from one day to the next because there are all sorts of compatibilty and migration issues that need to be taken care of"
    author: "Kevin Krammer"
  - subject: "Re: let's hope"
    date: 2008-02-23
    body: "\"I wasn't impressed by Kmail, especially it's complicated setup and speed but I continued to use it with the hope that it will improve.\"\n\n\nI definitely agree about the complicated set-up!   I consider myself an intermediate level Linux user but I could not configure Kmail (on Fedora 8) to access my POP accounts without referring to the help. Whilst this was generaly well-written, it was out of date as some of the screens had been renamed. Dejargonizing and making the configuration of POP accounts much more user friendly will certainly help Kmail adoption.\n\nI am not advocating a \"dumbing down\" of the program (which I know is anathema to many KDE devs) but simply removing UNNECESSARY complexity. Both Thunderbird and Evolution have this one aspect right, so surely Kmail can be improved to surpass them."
    author: "Kerr Avon"
  - subject: "Re: let's hope"
    date: 2008-02-22
    body: "I use KMail because KMail has by far the best interface for working with a few hundred mail folders.  My only problem with KMail is that it is very fragile when connections timeout - not a problem on DSL or better but tiresome on dialup."
    author: "Mike5000"
  - subject: "KDE Brainstorm Concepts"
    date: 2008-02-22
    body: "I believe there were some brainstorm concepts about really further linking the concepts of contacts across apps.\n\nSomeone pops on on Kopete, and you can link to them in Kontact, mail them in Kmail, share files with them, and search for all things related to them.\n\nHow close are we to apps sharing relational information like this through NEPOMUK and Akonadi?"
    author: "T. J. Brumfield"
  - subject: "Re: KDE Brainstorm Concepts"
    date: 2008-02-22
    body: "Hmmm, what you describe is already there - has been since KDE 3.3. You can create contacts in Kontact and associate them with your Kopete contacts. Only the searching isn't there yet.\n\nBut Nepomuk and Akonadi will make these things better, easier etc, yes. And searching will be there. But much of it will sadly most likely be KDE 4.2 material :("
    author: "jospoortvliet"
  - subject: "Syncing"
    date: 2008-02-22
    body: "For me as a long time happy user of Kontact and its parts most important is the syncing situation even without a groupware server.\n\nI hope that Akonadi will solve inter app, multi desktop, different devices syncing.\n\nFancy html-mails are less important then stability and functionality.\n\nAnyway, i like what i seen from the KDE4 previews!\nGood Job and keep going,\nRalph."
    author: "Ralph M\u00fcller-Welt"
  - subject: "Migration from KDE 3"
    date: 2008-02-22
    body: "Hi guys,\n\nWill there be a way to import emails, configurations, contacts, etc., from KDE 3? Right now I can't even find how to have Kopete 4 load my accounts and contacts from Kopete 3... :/"
    author: "S."
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-22
    body: "If you use a distribution like Fedora which keeps .kde as the configuration directory for KDE 4, migration should just work. If it doesn't, please file bugs. If your distribution uses the .kde4 hack, complain to your distribution... But manually copying .kde to .kde4 should work around that mess."
    author: "Kevin Kofler"
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-22
    body: "or create a symlink between ~/.kde and ~/.kde4 to ensure kde4 uses the previous configuration directories of kde..\n"
    author: "whatever noticed"
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-22
    body: "It's okay to make a symlink if you are definitely migrating to KDE 4 and staying there. But if you are just trying out KDE 4 software while keeping KDE 3 as your main system, keep ~/.kde and ~/.kde4 separate. Otherwise you'll find that the KDE 4 version of an application will probably mess up the configuration for the KDE 3 version. You can move smoothly from KDE 3 to 4, but not back again using the same configuration directory."
    author: "crawman"
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-23
    body: "\"It's okay to make a symlink if you are definitely migrating to KDE 4 and staying there.\"\n\nIF definitely migrating, huh?  But I don't see how it will help, if those people start symlinking from ~/.kde4 to /win/WINNT ;)"
    author: "Lee"
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-22
    body: "Hi Kevin! Thank you for your answer.\n\nAs others have explained below, sharing the whole KDE 3 configuration directory with KDE 4 works if you don't intend to switch back, but may not be such a good idea otherwise. Since KDE 4 is really not there yet as far as my personal workflow is concerned, I am simply not switching for good yet, although I'd really like to give more KDE 4 apps a go, in order to get used to the changes, but also so I can file bug reports (and hopefully provide patches where I'm able).\n\nThank you for the answer anyway. I'll manage something."
    author: "S."
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-23
    body: "Right. Copying .kde (or at least the config files you care about) to .kde4 should be safe though."
    author: "Kevin Kofler"
  - subject: "Re: Migration from KDE 3"
    date: 2008-02-25
    body: "Hi Kevin,\n\nWell, that's my problem -- while I know which files I 'care about' (whichever contain my Kopete accounts and contact lists), I don't know which ones those are! Apparently Kopete delegates account info storage to some external module, so copying over kopeterc and share/apps/kopete/ didn't suffice.\n\nI'll probably copy the whole .kde/ and see how that goes. *g*\n\nThanks for the advice!"
    author: "S."
  - subject: "Web 2.0 and mobile"
    date: 2008-02-22
    body: "You know, I was a big user of kmail and korganizer.\n\nNow I run Gmail with Firefox3 (sorry konqueror, but I'm still waiting for webkit) and my agenda is now running on my Sony Ericsson phone.\nWhat does both have in common? I can access them in any place, anytime. I can even access Gmail from my phone! I don't miss kmail and korganizer.\n\nSo, what the hell I'm talking about?\nSimply put: PIM on desktop should be much better, and have much more features than it's web and mobile counterparts, great integration and run fast. Without those, PIM on desktop is not that atractive.\nSO you guys have all my support to take the time needed to turn all needed changes in KDE PIM into reality. IMHO KDE PIM needs some drastic changes, much like the kdesktop was replaced by Plasma in KDE4. :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-22
    body: "i use kmail on my desktop for e-mails and gmail when i'm traveling :)\n"
    author: "whatever noticed"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-22
    body: "Look, I'm without connection at home since a couple of months (Thanks to my wonderful ISP... fuck it!) and so I could only use Gmail (webmail) at work and what can I say? when you have lots of messages, especially in mailing lists, I find it quite uncomfortable, traditional MUA are ways better in my case/opinion."
    author: "Vide"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-22
    body: "Same here. I use Gmail, Google calendar, docs.google, etc. all the time now.\nWith Firefox of course.\n\nCan't we just write a plasma based widget that let's me look at Gmail, Google calendar when I'm at my desk? But automatically sync it back to Google so I can access the same information when I'm on the go again?\n\nI still don't see a decent google integration? *google, are you reading this? Are you listening?* We Linux people need you!!!!\n\n"
    author: "Max"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-22
    body: "so you except million lines of codes go away because you like to use google services?\nplease, PLEASE think before posting such things! youre not the only person in the world and people have differrent prefrences.\nbtw i 'think' this will be possible with Akonadi,to get the data from google stuff and play with them in dekstop applications.but im not sure if devs like to do such thing.\nalso please do not speak as linux people.speak as yourself, as 'Max'.\n\nGO KDE-PIM, GO!"
    author: "Emil Sedgh"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-22
    body: "s/except/expect :P"
    author: "Emil Sedgh"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-22
    body: "Hi\n\nSame here.. years I use KDE PIM, now I use google apps. I think, if calendar, doc etc can sync with google storage, then it is very good. I like to use KDE PIM, but i can add stuff to calendar :( todo and so on. Often occur that internet connection have but not my KDE PIM :(\n\nAnd I can say that first time I hate gmail style email \"chat\" windows.. now i like it :)\n\nKristo"
    author: "Kristo"
  - subject: "Re: Web 2.0 and mobile"
    date: 2008-02-28
    body: "er, it already has more features. I moved back to kmail because gmail's web interface is simply too limited for me. I need powerful filtering, I like the integration, and having offline mail is important on a laptop.\n\nI'm sure the kde4 release of kdepim will be much better than the kde3 stuff - but the kde3 version is already pretty damn good, at least for my needs."
    author: "Chani"
  - subject: "Damn!"
    date: 2008-02-22
    body: "Gah. So much complaining here! Strangely, some of people complaining this time around pretty much say they wish KDE PIM did not even exist, because they are so much in love with Apple, Microsoft, GNOME, Google or whatever... Please file a bug then to this effect!\n\nI just hope the developers can ignore the complaints. The KDE PIM apps are wonderful, and I am sure they will become even more solid when they are based on Akonadi!"
    author: "Martin"
  - subject: "Re: Damn!"
    date: 2008-02-27
    body: "I don't see so much complaint. But it seems that it is indeed true that some people do not need KDE PIM.\n\nDo not take me wrong here. The KDE PIM team makes an awesome job only voluntarily and we can only thank them for that. I used to be a happy user of KMail. I must admit however that since I started with Gmail, Kmail has dissapeared from my computer. Actually I don't think I use any application related to KDE PIM. From what I read, I am not the only one in this situation.\n\nIf it was for me to choose, KDE PIM would be pretty much at the very end of my list of priorities for development for KDE 4.1. Some other people may find it more important. Of course it is not for me to choose. And people are free to program whatever they feel like. Especially if they are not being paid. But I think there is some value is finding out in these posts what the priorities are for the everage users."
    author: "Luis"
---
The <a href="http://pim.kde.org/">KDE PIM</a> crew met again at Osnabrück for three days of hacking, discussing and community building. The big topics were Akonadi and KDE 4.1. The team settled on the plan to release KDE PIM with KDE 4.1 based on the traditional backends and include the first platform release of Akonadi as the future base for PIM applications in and around KDE. The meeting was kindly hosted by <a href="http://www.intevation.de/index.en.html">Intevation</a> and supported by the <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.kdab.net">KDAB</a>. Read on for a report or see the <a href="http://pim.kde.org/development/meetings/osnabrueck6/">notes on the website</a>.















<!--break-->
<div style="float: right; width: 300px; border: thin solid grey; padding: 1ex; margin: 1ex;">
<a href="http://pim.kde.org/development/meetings/osnabrueck6/group.php"><img src="http://static.kdenews.org/jr/kdepim-osnabrueck.jpg" width="300" border="0" height="226" /></a><br />
Mike, David, Frank, Sebastian, Jaroslaw, Ingo, Tom, Thorsten, Thomas, Kevin, Tobias, Volker, Cornelius.
<a href="http://www.flickr.com/photos/mikearthur/sets/72157603859596691/">More photos from the event</a>.
</div>

<p>Much time was spent on discussing and working on <a href="http://pim.kde.org/akonadi">Akonadi</a>, the future PIM data storage backend which provides a platform for efficient storage and retrieval of PIM data. The modular architecture makes it easy to extend the platform by agents and resources which provide access to various sources of data or encapsulate complex functionality like mail threading for convenient use in applications. Tom Albers is working on an Akonadi based KDE 4 version of the mail client <a href="http://www.mailody.net/">Mailody</a> as a first full application making use of the new platform. Kevin Krammer spent some time on providing a migration path by implementing a bridge between the traditional KResources and the new Akonadi agent world.</p>

<p>The meeting also provided the opportunity to better integrate some new people. Ingo Klöcker made use of this by passing on maintainership of KMail to Thomas McGuire. Sebastian Trueg who joined the meeting for the first time worked together with Tobias König on integration of <a href="http://nepomuk.kde.org">Nepomuk</a> with KDE PIM, which will bring the joys of tagging, semantic search and virtual folders to KMail and all the other KDE PIM applications.</p>

<p>Next step is to have a focused Akonadi hackfest to solidify the platform and make it ready for the first release with KDE 4.1 in July 2008.</p>














