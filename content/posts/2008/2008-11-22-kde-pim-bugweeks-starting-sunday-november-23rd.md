---
title: "KDE PIM Bugweeks Starting this Sunday November 23rd"
date:    2008-11-22
authors:
  - "mleupold"
slug:    kde-pim-bugweeks-starting-sunday-november-23rd
comments:
  - subject: "Great news!"
    date: 2008-11-22
    body: "I use KDE-PIM almost only at work, at least with all its more advanced features, so this time I can help :)"
    author: "Vide"
  - subject: "Please excuse my naiive question:"
    date: 2008-11-23
    body: "Will it support Google plugins. (synchronize with google calendar, ToDo lists, Gmail)? <-- Google might be helpful with this, as it's their bread and butter to be ubiquitous.\n\nWill it support my Blackberry Pearl? (yes, I know I'm slowly getting to be a crackberry addict.)\n\nI would love to have all my PIM \"under 1 roof\". This looks to be it.\n\nEspecially if I can use the same program under Linux and KDE on Windows.\n\n"
    author: "Mike"
  - subject: "Re: Please excuse my naiive question:"
    date: 2008-11-23
    body: "For 4.2, no.Thats gonna happen using Akonadi project in the future, not even 4.3 probably.Actually you can find more by searching about Akonadi.There is a migration plan for applications to use it in future.\nPIM applications are huge and this is a complicated task, takes time.\nIm sure akonadi and pim devels can explain more."
    author: "Emil Sedgh"
  - subject: "Re: Please excuse my naiive question:"
    date: 2008-11-24
    body: "http://bugs.kde.org/show_bug.cgi?id=133614\n\nAn important lost feature, for me."
    author: "Fran"
---
Bugsquad will be revisiting PIM bugs next Sunday, concentrating on KMail and KOrganizer.
Recently the number of people hanging out and doing bugs in our IRC channel regulary has increased considerably. As there is almost always someone hanging around (and because some people specifically requested it), we decided we could extend our events. So, if you are one of those people who just can not spare time on Sundays, rejoice for Bugweeks.


<!--break-->
<p>I hereby announce our first KDE PIM Bugweeks. It will start this Sunday the 23rd and end December the 6th.</p>

<p>As always, all you need to join in is a recent version of KDE (trunk, 4.1.3 or 4.2beta1 if that is out by then) and some time to spend with us nice folks. If you are triaging for the first time, do not despair. There will be plenty of people to help you get started. No development skills needed, though we could accomodate for some bug-annihilating developers as well.</p>

<p>The <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KDEPimWeek1">Techbase page</a> with further information about the event will be up starting tomorrow.</p>


