---
title: "Beta 1 of Amarok 2 \"Nerrivik\" Released"
date:    2008-08-22
authors:
  - "lpintscher"
slug:    beta-1-amarok-2-nerrivik-released
comments:
  - subject: "Fantastic!"
    date: 2008-08-22
    body: "This is just fantastic!!!\n\nThe progress of this beast is just amazing!! I'm going to Amarok this night for sure :)\n"
    author: "David"
  - subject: "Import m3u playlist"
    date: 2008-08-22
    body: "There's probably a very easy way to do so, but I have not been able to import several m3u playlists I have in a SVN version from a few days ago (my first try on a KDE4 amarok).\nDoes anyone have a hint? "
    author: "Richard Van Den Boom"
  - subject: "Amarok 2 - 2 rule the world!"
    date: 2008-08-22
    body: "I Have already been using the Alpha ( 1.86 ? ) of Amarok on gentoo for some time now.\n\nThe User interface is very simple and nice looking,\n\nInternet services are really well integrated.\nUsing magnatune was never so much fun!\n\nFunctionality is still a bit basic, ( no lyrics or wikipedia integration for example )\nBut i suppose that will change soon now that the script interface is ready.\n\nBut all in all Amarok seems to be shaping up pretty well.\n"
    author: "Mark Hannessen"
  - subject: "Re: Amarok 2 - 2 rule the world!"
    date: 2008-08-22
    body: "lol,\n\nJust when i said this the 1.86 ebuild vanished from the overlay..\nlet's hope there will be a new ebuild soon :p\n\nA well..\nThat's what you get for living on the edge."
    author: "Mark Hannessen"
  - subject: "Re: Amarok 2 - 2 rule the world!"
    date: 2008-08-22
    body: "1.90 ebuild :D\n\nLooks like i'll be running the latest build today..."
    author: "Mark Hannessen"
  - subject: "Re: Amarok 2 - 2 rule the world!"
    date: 2008-08-23
    body: "Hi,\n\nwhich overlay are you using ?"
    author: "Benjamin"
  - subject: "Re: Amarok 2 - 2 rule the world!"
    date: 2008-08-24
    body: "im using kdesvn-portage\n\njd"
    author: "jaked"
  - subject: "Re: Amarok 2 - 2 rule the world!"
    date: 2008-08-24
    body: "Thank you for the info ;)"
    author: "Benjamin"
  - subject: "Great artwork, greatness in general"
    date: 2008-08-22
    body: "I love the artwork. The playlist and plasmoids look awesome in an Oxygen-y awesome way.\n\nI do have a few thoughts about the new main toolbar artwork though...\nRight now the progress bar and volume icon/bar look very... monochrome. It'd be nice if they could get some color.\nAlso, I think the previous/play/stop/next buttons would look better unscrunched.\nI can also see the faint outline of some letters in the main toolbar background, behind the progress bar.\n\nAnd there's a bug easily noticable from the screenshots. :P\nWhen music is playing, the play button doesn't change into a pause button. :P\n\nBest wishes for Amarok2! It's the best music player on Earth even in it's beta state."
    author: "Jonathan Thomas"
  - subject: "Re: Great artwork, greatness in general"
    date: 2008-08-22
    body: "> Also, I think the previous/play/stop/next buttons would look better unscrunched.\n\nI second that. The first time I've seen the overlapping buttons in a screenshot, I thought it was a rendering error."
    author: "Kevin Kofler"
  - subject: "Re: Great artwork, greatness in general"
    date: 2008-08-23
    body: "++"
    author: "yman"
  - subject: "thanks "
    date: 2008-08-22
    body: "amarok 2 beta1 on my birthday, what a lovely surprise;) \nif you want to try it on windows \nhttp://techbase.kde.org/Projects/KDE_on_Windows/Installation#KDE_Installer_for_Windows\n\nsincerely yours  \n"
    author: "mimoune djouallah"
  - subject: "Re: thanks "
    date: 2008-08-22
    body: "I've been anxiously awaiting a working Windows build.  I can't wait to download this!"
    author: "T. J. Brumfield"
  - subject: "Re: thanks "
    date: 2008-08-25
    body: "You're not the only one. A bunch of students at my university are as well.\nI'm sure a Windows build would increase exposure as well. \nMaybe even draw some artistic talent.\n\n(I'm not saying the current look is \"bad\", I just think there is much room for improvement. Tons of potential out there..)\n"
    author: "Max"
  - subject: "Re: thanks "
    date: 2008-08-23
    body: "Happy birthday!"
    author: "You should be celebrating"
  - subject: "Re: thanks "
    date: 2008-08-27
    body: "Has someone got it to play mp3s on Windows without hanging and/or crashing? I'm sure it's a mostly a Phonon issue: how can I trace that? (asking before I file a bug, to make sure it's not my setup)"
    author: "Luca Beltrame"
  - subject: "Love the new look"
    date: 2008-08-22
    body: "While I would mind seeing just a touch of color back in the the progress and volume sliders, I LOVE the new look and the direction you guys are going!  Yes I love those overlapping buttons.  \n\nLooking forward to context plasmoids that mimic the old \"Suggested Tracks\", \"Favorites by this Artist\" and \"Other tracks on this album\" functionality from context view of 1.x.  Then all that space in the context view starts to become really useful again.\n\nLove it, love it, love it!!!"
    author: "J"
  - subject: "Re: Love the new look"
    date: 2008-08-23
    body: "Thanks the idea ist to make them look unike and not like everything else out there... there is still room for improvemt dough..."
    author: "nuno Pinheiro"
  - subject: "Re: Love the new look"
    date: 2008-08-24
    body: "Pitty. I *like* having an application look like other applications on the system. Now it seems Amarok is slowly moving back in the direction that other media players on other popular platforms have been for ages: applications that do not fit the rest of the system at all, work differently, look differently, all in an attempt to be \"cool\". Amarok used to be pretty well integrated with the rest of the KDE UI, but it seems it is sliding down the slope called \"cool-but-unusable\" more and more."
    author: "Andre"
  - subject: "Re: Love the new look"
    date: 2008-08-25
    body: "Although it is a nice idea to give an application an unique look, Amarok should stick with some more defaults.\nOn windows systems media applications are a mess.\nThey work differently, look differently and behave differently from other applications and make them often hard to use compared to other applications.\nPlease don't do this to Amarok!\nHaving a common style and behaviour across all applications is the biggest advantage KDE has over other Desktop environments or operating systems.\n"
    author: "Christian Nitschkowski"
  - subject: "Re: Love the new look"
    date: 2008-08-25
    body: "I think is good that you are trying to make it unique, but the overlapping buttons make me feel uncomfortable. They look as a pyramid of domino tiles and I  have the feeling that if I press the fast forward button they will all fall. I know is silly but that's the impression that I get"
    author: "Raul"
  - subject: "Re: Love the new look"
    date: 2008-08-25
    body: "I think is good that you are trying to make it unique, but the overlapping buttons make me feel uncomfortable. They look as a pyramid of domino tiles and I  have the feeling that if I press the fast forward button they will all fall. I know is silly but that's the impression that I get"
    author: "Raul"
  - subject: "Where are to OpenSuSe builts"
    date: 2008-08-22
    body: "Looking forward to Amarok 2, great piece of software and soooooo beautiful :-)"
    author: "Gerhard"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-22
    body: "http://software.opensuse.org/search?baseproject=openSUSE%3A11.0&p=1&q=kde4-amarok "
    author: "T. J. Brumfield"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-23
    body: "I'm using the current OpenSuSE package and the collection doesn't seem to be working properly.  It appears to have scanned my music folders but while the albums appear, they contain no songs.  I haven't built from source and I'm not experienced at submitting proper bug reports but if this is a new bug and someone can tell me what to do I'm happy to provide any useful information."
    author: "Adrian Baugh"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-24
    body: "It took me a while to work out why I was having problems with it, then I found out that I had to add phonon, and then everything worked perfectly, including adding the appears that wouldn't show as being there, make sure you have phonon loaded inncluding the phonon-xine"
    author: "R. J."
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-24
    body: "i'm having the same problem on kubuntu, and i have phonon installed buti  don't see the option for phonon-xine, any ideas?"
    author: "Kevin"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-24
    body: "I have phonon installed (including phonon-xine).  Actually I've been using the alpha builds for a while and this seems to be a regression with the beta package as the alphas could build my collection fine."
    author: "Adrian Baugh"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-25
    body: "The collection schema changed between the alphas and the beta.  You need to delete your collection.db (probably ~/.kde4/share/apps/amarok/collection.db and rebuild it."
    author: "Dan"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-26
    body: "Ah, will do. Is there any plan to optimize collection-building for version 2? I know it's better than it used to be, but I have a middle-sized collection (15GB) and it is *S*L*O*W* to build the collection in Amarok.  Building the exact same collection into mythtv's database takes a few seconds. I guess Amarok sets up a few more fields for ratings, cover art etc. but I'm talking orders of magnitude..."
    author: "Adrian Baugh"
  - subject: "Re: Where are to OpenSuSe builts"
    date: 2008-08-27
    body: "I've been told to use PostgreSQL over sqlite.  I'm planning on wiping my collection file and trying it myself."
    author: "T. J. Brumfield"
  - subject: "Zune-type sharing???"
    date: 2008-08-22
    body: "Last.fm, Pandora and the like may all be disappearing.  In fact, many of the \"web radio\" services are all going to go away with the new financial model that is pricing them out of existence.  I'd like a means to discover new music legally.\n\nI can just swap music with people, but it isn't legal, nor is there a good, intuitive way to do this.\n\nI'd love to see Amarok tie into Akondi for my contacts, and ultimately maybe even suggest music to me based on music genome code, or based on what they are listening to most often.  Perhaps the interface could also allow me to \"tag\" a song with Nepomuk as a recommendation, which my contacts can check.  Perhaps I could export an RSS feed of XML of my recommendations to keep on my blog.\n\nI'm glad to see the core re-write is coming along.  Now how do you push the concept of \"discovering your music\" into the future?"
    author: "T. J. Brumfield"
  - subject: "Re: Zune-type sharing???"
    date: 2008-08-22
    body: "http://mail.kde.org/pipermail/amarok/2008-August/006467.html\nThis will not be in 2.0, but later (I think 2.1). It will allow for recommending of similar music. Ionno about Akondi/Nepomuk usage though."
    author: "a thing"
  - subject: "Re: Zune-type sharing???"
    date: 2008-08-23
    body: "Last.fm isn't based in the usa so those idiotic tariffs don't apply...Regarding the social aspects,it seems a bit pointless to replicate the friends functionality already built into last fm...this is already possible...you can listen friends' radio stations and on last.fm, you can check their listened songs (i think you can even export rss from there). We shouldn't be trying to replicate functions already existing in the web, but we should plug into them more.\n\nThe thing about having contacts integrated with music sharing means that everyone would have to be using amarok/kde."
    author: "txf"
  - subject: "Re: Zune-type sharing???"
    date: 2008-08-23
    body: "\"We shouldn't be trying to replicate functions already existing in the web, but we should plug into them more.\"\n\nWe shouldn't be relying on magical software in the sky that we cannot control."
    author: "a thing"
  - subject: "Re: Zune-type sharing???"
    date: 2008-08-26
    body: "> We shouldn't be relying on magical software in the sky that we cannot control.\n\nAmarok web services are hot pluggable, so if worse came to worst, we could always set up our own web service and integrate into Amarok, so we don't really rely on them per say.\n"
    author: "Michael \"okay\" Howell"
  - subject: "Re: Zune-type sharing???"
    date: 2008-08-23
    body: "I'm so sleep deprived that I forgot one of the primary reasons I was posting under the title of \"Zune-type sharing\".\n\nCould we extend music recommendations one step further, and stream a song to a friend/buddy/contact?  Swapping the .mp3/.ogg/.etc to the other person would be illegal, but what about a stream so we can recommend/share music with others?"
    author: "T. J. Brumfield"
  - subject: "Re: Zune-type sharing???"
    date: 2008-08-24
    body: "Sharing an audio file is not nessecairily illegal, nor will it be in all places of the world. I think the possibility for a feature like this to be misused for illegal purposes should not stop developers from implementing it anyway. "
    author: "Andre"
  - subject: "Using neon again now..."
    date: 2008-08-22
    body: "Keep up the incredibly awesome work, guys!"
    author: "ethana2"
  - subject: "Beta? Still alpha, imho, but coming along nicely"
    date: 2008-08-23
    body: "I've tried using the Amarok SVN after my weekly KDE SVN compiles for a while and it simply is not close to being stable enough for daily usage. I'll try it, use it for a short while, see it crash and switch back to mpd/gmpc.\n\nAmarok 2 looks great, and it will probably become a great product. But it isn't close to good enough to be called beta at this point, and I wouldn't recommend it to anyone unless they just want to take a look at the GUI because they are curious."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Beta? Still alpha, imho, but coming along nicely"
    date: 2008-08-23
    body: "Well, I can use it daily without any major problems, maybe your builds are broken\n\nFor me the only problem is the change they made to the main toolbar, it looks pretty bad. I had to change the default-theme.svg myself so I could use it.."
    author: "Paulo Cesar"
  - subject: "Sounds fantastic - now let's work on visuals"
    date: 2008-08-25
    body: "Technically Amarok sounds and seems fantastic.\n\nNow could we get some designers to work on the visual styles and themes? (maybe even have a competition on deviant art, kde-look, and others?)\n\nWhile we all want and need functional software, please remember the new linux motto: <B>Pretty is a feature!!!!!</B>\n\nI would love for Amarok to be the best looking media player out there, not just the most functional and extendable. Right now it has a bit of a '90's look. :( IMHO.\n\nIf I had any artistic ability I'd contribute myself. I'll try my best at spreading the word though. \n\nMaybe post a request to digg,stumbleupon, slashdot, etc. I'm sure someone will want to take over that project. Amarok is the most talked about KDE application out there at the moment. Everyone on my college campus is looking forward to the \"iTunes killer\" (their words, not mine.)\n\n"
    author: "Max"
  - subject: "Re: Sounds fantastic - now let's work on visuals"
    date: 2008-08-26
    body: "\"Pretty is a feature!!!!!\"\n\nIs that why Gnome looks so basic? ;-)"
    author: "Svempa"
  - subject: "Re: Sounds fantastic - now let's work on visuals"
    date: 2008-08-29
    body: "Probably. ;-p\n\n"
    author: "Max"
  - subject: "russian tags?"
    date: 2008-08-26
    body: "will amarok 2 support russian tags in playlist?"
    author: "lurker"
---
The Amarok team is proud to announce the first beta version of Amarok 2, codenamed Nerrivik. It contains a considerable amount of improvements over the previous alpha versions, bringing Amarok one step closer to the 2.0 release.  The highlights of this first beta version are the scripting interface, file tracking (so you keep your statistics if you move your files around), gorgeous new artwork as well as lots of bugfixes.  Read more about it in the <a href="http://amarok.kde.org/en/releases/2.0/beta/1">release announcement</a>.



<!--break-->
<img src="http://static.kdenews.org/jr/amarok2-beta1-wee.png" width="500" height="208" />


