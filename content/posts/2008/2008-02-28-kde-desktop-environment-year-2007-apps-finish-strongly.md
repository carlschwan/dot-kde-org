---
title: "KDE Desktop Environment of the Year 2007, Apps Finish Strongly"
date:    2008-02-28
authors:
  - "skuegler"
slug:    kde-desktop-environment-year-2007-apps-finish-strongly
comments:
  - subject: "Kongratz!"
    date: 2008-02-28
    body: "Congratulations to KDE, Amarok, K3b, Konqueror, and all other KDE applications who win the 2nd place! (And of course to all involved in those projects!) Anyway, is this the 4th times KDE wins the award? or 5th? (Clearly KDE always wins the best DE award from Linuxquestions.org :D)\n\nUnfortunately, they've decided to merge best IDE with best Web Development software, which resulted in Quanta only in 2n position after Eclipse. Otherwise, Quanta would have won 4th consecutive award!"
    author: "fred"
  - subject: "Re: Kongratz!"
    date: 2008-02-28
    body: "Nevermind, I've found the answer: KDE won the award in: 2001, 2002, 2003, 2004, 2005, 2006 and 2007."
    author: "fred"
  - subject: "Re: Kongratz!"
    date: 2008-02-28
    body: "I am curious to see if KDE will win next years edition as well with KDE 4. "
    author: "Andr\u00e9"
  - subject: "Re: Kongratz!"
    date: 2008-02-28
    body: "Ah well, if not, KDE 4 might be second after KDE 3.5.9 ;-) gnegne."
    author: "Terracotta"
  - subject: "Re: Kongratz!"
    date: 2008-02-28
    body: "Unless Gnome 3 is released LOL. KMail surprised me though, did it beat Evolution to it?\nCongrats guys and keep up the good work. KDE is truly no.1."
    author: "Bobby"
  - subject: "Re: Kongratz!"
    date: 2008-02-29
    body: "Last time I used Evolution it was not able to display CSS formated HTML mails. That's why I dropped it for KMail back then. Sometimes it is such as simple."
    author: "Thomas"
  - subject: "Re: Kongratz!"
    date: 2008-02-29
    body: "The only ting surprising regarding Evolution was that it had gained some since last year, when KMail beat it with a greater margin. From last year it seemed Evolution lost most of it's users to Mozilla Thunderbird. I guess this years increase is a response to Evolution being more actively developed again. "
    author: "Morty"
  - subject: "Re: Kongratz!"
    date: 2008-02-29
    body: "> Unfortunately, they've decided to merge best IDE with best Web Development software, which resulted in Quanta only in 2n position after Eclipse. Otherwise, Quanta would have won 4th consecutive award!\n\nNot that I was taking it for granted, but I was a bit surprised when I didn't get notified of a win, until I looked closer. Interestingly Quanta and KDevelop together did take more votes than the winner, which has financial and community backing that utterly dwarfs ours. In KDE4 Quanta and KDevelop are working off a common platform, so I will petition Jeremy if he wants to do things this way to make it a joint entry. I'm happy to share the award with the KDevelop guys, My was was getting crowded anyway. ;-)\n\nOn a serious note, looking at comments there in past years it was pretty clear that there was no competition for Quanta. That makes a boring contest. The irony is that due to us being bound through the 3x series to desktop release schedules we were never able to do the larger things I wanted to do. By this time next year I hope to have made a huge jump. I don't feel any need to rest in first place just to \"play nice\". Our objectives are set against our own measure.\n\nThanks to all our supporters and don't forget to drop by our site and read the news if you are interested in our project. http://kdewebdev.org"
    author: "Eric Laffoon"
  - subject: "Re: Kongratz!"
    date: 2008-02-29
    body: "I second that:\n\nKongratz!!!\n\n\nKDE is making the world a better place. (at least for us geeks :p )\n\n\nCan't wait to share it with the rest of the uninitiated world. (let's start them slowly with KDE on Windows, and slowly have them come over to our side..)\n\n\n\n-Max"
    author: "Max"
  - subject: "Re: Kongratz!"
    date: 2008-03-02
    body: "Congratulations on this historic event.\n\nNow I'm going to vote for Kubuntu. Others please join me."
    author: "Steve"
  - subject: "cool"
    date: 2008-02-28
    body: "Cool to see how over 2 year old KDE tech beats the latest & greatest the competition can throw at it ;-)\n\nWait until KDE 4.x gets a bit more complete, and we can beat our proprietary competition hands down."
    author: "jospoortvliet"
  - subject: "Re: cool"
    date: 2008-02-28
    body: "\"Wait until KDE 4.x gets a bit more complete, and we can beat our proprietary competition hands down.\"\n\nThe axe is already at the tree. Let's start with hasta la Vista and the MACies better watch out ;) "
    author: "Bobby"
  - subject: "Re: cool"
    date: 2008-02-29
    body: "Personally, I wouldn't mind if even were just Mac OS-X and KDE/Linux.\n\nThe Mac'ies are almost cousins. They're BSD'ies, sort-of. Besides, if we work together with #2, we can get closer to beating #1 Vista. :)\n\n"
    author: "Max"
  - subject: "Re: cool"
    date: 2008-03-01
    body: "Wait a minute\nthere isnt much competition available\n\ngnome\nand xfce\n\nxfce will forever remain light weight so it will never be a legit competition.\n\nthat leaves gnome. So its only kde vs gnome actually. (ede is way too tiny)\n\nAnd gnome has so many problems, i dont even know where to start:\n\n- The components are harder to compile AND run. When i started kde, it just worked. When i started gnome, i had nautilus problems, gconf problems, libbonobo(and libbonoboui) problems... googling to resolve these errors didnt help. So many errors that i gave up.\n- Gnome tries to make the user an idiot. Maybe KDE options are bloated, but at least I GET to choose what i dont want. I think the ideal DE would be one that is super modular, allowing a user to choose how she wants something (and choose to not be able to choose, i.e. like gnome)\n\nBut the way gnome forces its users to accept its default choices is an idiot-developer attitude. I hate this attitude. \n\nGnome devs need to fix these two problems if they ever want me to \"convert\" to gnome. Kde is a lot better in this regard, big props to the kde devs (and I know not _every_ gnome dev has this attitude, just some gnome devs...)\n"
    author: "she"
  - subject: "Re: cool"
    date: 2008-03-02
    body: "I would love for KDE 4 to beat proprietary software. At any rate it will offer consumers more choice."
    author: "Steve"
  - subject: "Hooray Kate"
    date: 2008-02-28
    body: "I've always found the vi vs. emacs debate very tiring. It's like an 80's historical re-enactment. \n\nSo huzzah for Kate's second place finish. Kate vs. Vi is a debate I can live with. ;)"
    author: "Ian Monroe"
  - subject: "Re: Hooray Kate"
    date: 2008-02-28
    body: "When was the last time you actually saw any of that debate? I read a lot, but I can't seem to recall anything of that for the last 5 years at least. I think the debate died a long time ago, just nobody noticed.\n\nAnd, Kate is my favorite editor only as long as I don't need scripting. When will I be able to script Kate in Python, can Kate 4.0 do that? \n\nCause recently I read that Emacs can now be scripted in Python, and it made me consider to go back.\n\nCurrently I am a vi user on the terminal, and Kate (or in its KPart form at least) for graphical editing, depending on what I do.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Hooray Kate"
    date: 2008-02-28
    body: "I saw it in #ruby-lang about a week ago."
    author: "Ian Monroe"
  - subject: "In the dust?"
    date: 2008-02-28
    body: "KDE: 52%\nGTK+ based systems: 50%\n"
    author: "Ross"
  - subject: "Re: In the dust?"
    date: 2008-02-28
    body: "So you say it's rigged, because it totals to more than 100%? ;-)\n\nNo, honestly, GTK+ based vs. Qt based, doesn't make a lot of sense. While it's interesting to note that there is no alternative Qt desktop, what does it give when entirely different desktops use the same or a different underlying toolkit, it matters how the desktop present the interface.\n\nSo well, you ought to compare the numbers for every 2 desktops. Forming groups on technical implementation details won't make sense.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: In the dust?"
    date: 2008-02-29
    body: "> While it's interesting to note that there is no alternative Qt desktop [...]\n\nThere is! Well, sorta: XPDE http://www.xpde.com/\nBut I think it's unmaintained."
    author: "panzi"
  - subject: "Re: In the dust?"
    date: 2008-02-29
    body: "unmaintained and...\n\"I use Kylix 3 Enterprise, I have not tested with Kylix 3 Open Edition,\""
    author: "Debian"
  - subject: "Re: In the dust?"
    date: 2008-02-29
    body: "It's close.. But we're getting there.. :)"
    author: "Max"
  - subject: "Summer of Code?"
    date: 2008-02-28
    body: "Is KDE going to be at this years Google Summer of Code again?\n\nhttp://developers.slashdot.org/developers/08/02/26/0134235.shtml\n\n\nI'm very curious. Looking forward to the results.. :)"
    author: "Max"
  - subject: "Re: Summer of Code?"
    date: 2008-02-28
    body: "Yep, we will.\n\nThink of some good ideas, and get all your friends to apply as well. :)\n\nIan"
    author: "Ian Monroe"
  - subject: "Re: Summer of Code?"
    date: 2008-02-29
    body: "Already ahead of you. :)"
    author: "Max"
  - subject: "KDE System tools lacking"
    date: 2008-02-28
    body: "KDE may have great specialty apps, but when it comes to integration with system tools like ConsolieKit, Apt, PackageKit, NetworkManager it's seriously behind the competition.\n\nThis is largely thanks to Red Hat and Canonical, who are throwing money behind the competition because it chose a licensing model that sucks up to closed source proprietary ISVs. The same ISVs that want to use Linux without giving anything back.\n\nWe see the same troubling attitude from the competition when it's leaders in the form of the GNOME Foundation decided to stab the open standards community in the back and give legitimacy to M$'s patent-poisoned OOXML.\n\nKDE, on the other hand, it fully protected by the best copyleft Free Software licenses: GPLv2 and recently GPLv3.\n\nThe KDE community has got to do better in spreading the word that if developers want to promote Free and Open Source Software, they should help out KDE and shun the competition sellouts."
    author: "Tray"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "> KDE may have great specialty apps, but when it comes to integration with system > tools like ConsolieKit, Apt, PackageKit, NetworkManager it's seriously behind \n> the competition.\n\nFunny you should say that, I'm sat here at midnight working on NetworkManager 0.7 support as I have been doing all week in my day job at Novell.  We had knetworkmanager as soon as NM was in openSUSE.  This afternoon I spoke on IRC with one of my counterparts on the KDE Team at RedHat on how we could complete Admin mode support in KDE 4 using PolicyKit.  And at the weekend I and several other openSUSE guys had a discussion with the PackageKit guy at FOSDEM.  \n\nApt, I don't care about, libzypp in openSUSE 11.0 is going to make it look like your grandad's package tool \n\nMy point?  KDE (and its distro backers) are actively supporting these technologies, as they mature."
    author: "Will Stephenson"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "I'm impressed how the KDE team at Novell/OpenSUSE has indeed remained supportive of KDE despite detracting forces like Migues de Icaza and the Ximian people. Keep up the good work!\n\nAs for Red Hat, at best they're all talk and no action when it comes to KDE, and at worst they're actively badmouthing and crippling it to make it look bad compared to GNOME. How long has it been since a Red Hat employee has committed something to KDE's SVN? Months if not years. This isn't necessarily a reflection of individual developers' desires, but rather of Red Hat's business agenda for the past six years:\n\nhttp://www.theregister.co.uk/2002/09/26/kde_red_hat_spat_escalates/"
    author: "Tray"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "to be honest, Novell has taken a lot of ignorant statements because of their deal with microsoft, but if it wasn't for all the hard work that Novell has done, the linux community would be years behind where they are now.  It is the hard work of the novell teams that are bringing some amazing features to KDE and to the linux world.  I take my hat off to you Novell/opensuse teams, you have truly benefited the linux community more than some people would like to acknowledge.\n\nI've recently decided to try a few other distro's again, all the alpha's and without a doubt, opensuse is still years in front of the competition. "
    author: "anon"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "The Linux community would be far more advanced if not for uncompetetive behaviour from the competition. \n\nThis competition is IBM, HP, Sun. The last of them actually vowed recently to support Linux against Patent attackers with its own Patents. \n\nAnd that competition is Microsoft who is in standby mode of attacking Linux and doing everything to forbid competition with its products.\n\nThe truth of fact is that in make its deal with Microsoft, the only thing Novell did achieve was to weaken the general pressure to open Microsoft information to everybody. The longer that takes, the more beneficial to Microsoft, and Novell let Microsoft buy out of that for, granted, a lot of money for its share holders.\n\nI am never going to thank Novell for the buy out. Not for the FUD that it helped spread, and for the idea that patent protection for unspecified patents is needed and worth some money. I am not going to thank them for helping to force smaller Linux companies into deals with Microsoft that hurt their bottom line.\n\nAnd while your at it. Try out Kubuntu or Fedora with KDE. Those guys did not see a need to help Microsoft out of its anti trust problems with EU, because those are well deserved.\n\nThat said, thanks for the good things people at Novell do. But I am not going to put up good things against bad things. If an entity does evil or helps evil, it can no longer pay through other deeds and get to heaven. Not even in Church anymore. Even there you have to regret.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "You \"forgot\" to mention Mandriva.\n\nLook at the commits, Brazil + France = Mandriva.\n\n"
    author: "reihal"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "Thanks for the addition, you are right. They too were clear about not conspiring against the users.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "> http://www.theregister.co.uk/2002/09/26/kde_red_hat_spat_escalates/\n\nYawn, that 2002 news about RHL 8.0 again! :-( I really don't see how that's relevant to the current state of Red Hat, Fedora, RHEL or anything else. A lot has changed since then.\n\n> How long has it been since a Red Hat employee has committed something to\n> KDE's SVN?\n\nLess than one day: http://websvn.kde.org/?view=rev&revision=780193\n\n        Kevin Kofler"
    author: "Kevin Kofler"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "> Less than one day: http://websvn.kde.org/?view=rev&revision=780193\n\nThat's a translation commit, no code.\n\nSure translations are very important, but it doesn't bring new or improved technology to KDE.\n"
    author: "coward"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "That's a particularily dumb and dividing comment. It implies many things, none of which you probably can confirm.\n\nThe strength of KDE does in particular come from the fact that is is localized so well. Look on the commit map and ponder how few people that commit are native English speakers. The whole KDE project was started by Germans. \n\nAnd in order to give KDE to their dads, friends, etc. the developers need it to be localized here in Europe. That is why KDE not only has the most localizations and best technology to achieve that. That is why e.g. in KDE 4 we are going to see spellcheckers with automatic language detection per paragraph.\n\nThen of course, if Redhat helps Gnome, what's wrong with that? How is that not helping KDE? Last I listened to e.g. the KDE developers, people said that Gnome was not as good as KDE, but still Free Software and respectable and welcome allies in the battle for Free Desktop Standards. \n\nOut of historic reasons - at the time you had _no_ license to distribute KDE binaries, Redhat had to focus on Gnome initially. And once liberated by Trolltech, the decision was already done. With Redhat for the longest time not being interested in the desktop at all, why should they \n\nYou can hardly ask them to care about investing in 2 desktops. A decision that Novell announced as well, just didn't get away with.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: KDE System tools lacking"
    date: 2008-02-29
    body: "There is an issue with system tools, which is that different distributions do things differently. KDE developers debated this and decided as a whole to leave it to the distributions as no \"one size fits all\" solution would work. It's a wise choice.\n\nHaving said that, look at qtparted, which I don't think there is another graphical tool quite like. Along with which I've seen some others incliding info-systema (sp?) which is based on Kommander. I did an etc-update graphical tool for Gentoo with Kommander.\n\nHere is either a wonderful solution or a \"what are you still don't warming the couch\" idea... Most system configuration is just text files. If you have a clue how to set them you can point and click an interface together in a few hours with Kommander and distribute it. Since it doesn't need to be compiled every user who has it on their system can run it right away and typical small configuration dialogs tend to run under 100KB. In fact most are under 40 KB and can be posted on our user list to ask questions.\n\nGiven that you have a Function Browser to point and click your way to finished scripts and you can also add any other scripting language you like, and you can draw your interface... Seems to me anything missing could be made by the average guy in a few hours. In fact many distributed programs are done by people who never wrote a line of code in their lives. "
    author: "Eric Laffoon"
  - subject: "Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "It's great to hear that my favorite desktop environment is winning in the polls.  Congrats to all the hard working KDE devs out there, and a big thanks!  I wanted to also let everyone know about Ubuntu Brainstorm at http://brainstorm.ubuntu.com.  Right now \"improving KDE support\" only has 18 votes.  Please vote that up so that Kubuntu can be more of a first class distro (I love Kubuntu, and all of the work put into it, and my hats off to Jon Riddell and all his fantastic work, but I'd love to see at least one more full time developer, and other stuff like a KDE interface to TimeVault coming out at the same time as the Gnome version).  Maybe if enough of us vote this up, more support will be thrown Kubuntu's way.  Thanks again to all the KDE folks for the best desktop hands down.  Take care."
    author: "scanady"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "voted.\ndirect link http://brainstorm.ubuntu.com/idea/247/"
    author: "Br\u00e1ulio"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "I voted too!\n\nhttp://brainstorm.ubuntu.com/idea/478/\n\nUnfortunately some people (who do not like KDE?!) voted against the proposal. \nIt takes a minute to register, but it's worth it to show that there are is a big number of people who feel a bit neglected. Vote now!"
    author: "HVB"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "I voted, too.\n\nI didn't know about this brainstorm thing, before. It might be a good idea to put these badges on a few websites. Maybe even at kubuntu.org...\n\nhttp://brainstorm.ubuntu.com/idea/478/promote"
    author: "Sepp"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-03-02
    body: "Totally agree!!!"
    author: "Max"
  - subject: "Re:  (Kubuntu) - PLEASE VOTE"
    date: 2008-02-29
    body: "No kidding..\n\nWe need to vote on that.\n\nMaybe Kubuntu won't be the red-headed-stepchild of Ubuntu anymore, and more people with flock to KDE.. :)"
    author: "Max"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "I wonder if it isn't there on purpose on Kubuntu.org? This way less people show interest in Kubuntu. *conspiracy theory*\n\nIn other news.\nI still haven't received my login password. I've been waiting 5 minutes for it now. Anybody else experience the same problem?"
    author: "Max"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "\"I still haven't received my login password. I've been waiting 5 minutes for it now. Anybody else experience the same problem?\"\n\nTook me 15 minutes to get mine."
    author: "Anon"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-02-29
    body: "I voted even though I'm an Ubuntu user.\n\nWhat's up with their web site? why doesn't the Kubuntu one look like the nice Ubuntu one (with Kubuntu colors, of course)?"
    author: "yman"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-03-02
    body: "My guess is because almost no staff is dedicated to the Kubuntu project.\nI've heard rumors that only one full time person is working on the Kubuntu project. Hopefully they are unfounded.\n\n"
    author: "Max"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-03-02
    body: "I think it's because they cite that there is \"no demand\" for KDE on Ubuntu/Kubuntu.\n\nPart of it is because how well hidden it is. i.e.: The link is on Ubuntu, and NOT the Kubuntu website."
    author: "Max"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-03-01
    body: "Yeah, rocK on Kubuntu! Though, I really don't understand why some people say it is not pollished. Perhaps I am just a grateful user from the the RedHat 3 days. ;) It'll take a lot to get me to use an RPM based distro again. I don't care about all the YUM hype and what else they've got to make RPM functional. At least, it's competition.\n\nThanks to everyone who has contributed to KDE. KDE is a pleasure to use."
    author: "winter"
  - subject: "Re:  (Kubuntu)"
    date: 2008-03-02
    body: "I'm going on that website, as soon as I finish reading the dot.\n\nHope a lot of people here voted already. "
    author: "Steve"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-03-03
    body: "I've voted too: now a total of 35 votes.\nStill doesn't show very good support.. but better!"
    author: "riddle"
  - subject: "Re: Ubuntu Brainstorm and KDE (Kubuntu)"
    date: 2008-03-12
    body: "I love KDE as I love Gnome too. I'm sorry I can't say longer cause my english was poor.\nI love Kubuntu and I used it, but I also love Ubuntu (Gnome)\nThanks"
    author: "AP Widodo"
  - subject: "Amarok good, but"
    date: 2008-02-29
    body: "Amarok undoubtedly has the best functionality, but isn't a finished product IMHO.\n\ncrashes when (starting) playing, crashes when drag/dropping to collection, sometimes skips tracks because it cannot open the sound device, etc.\n\nhope amarok for kde4 will be better polished.."
    author: "Wil Palen"
  - subject: "Re: Amarok good, but"
    date: 2008-02-29
    body: "Sounds like a problem with your choice of base system, or a packaging bug to me.\n\nI've been running Amarok for a while, and haven't had any of the described issues since 1.2 at least. Perhaps there's a problem with the sound engine you're using?"
    author: "Tim McCormick"
  - subject: "Re: Amarok good, but"
    date: 2008-02-29
    body: "I don't develop Amarok, I develop Quanta and Kommander. Your comment looks familiar though. I had someone say Quanta was slow once when it is very fast... turns out he had lots of very long if/else statements in his PHP which identified a parser bug. Someone else complained about behavior I wasn't experiencing and it was choices Fedora had made packaging kdelibs. It took a day and a half for our lead developer to find that. Statistically at one point we were tracing over half our bugs to packaging issues... in user terms, things that happened in the chain of bringing the software to you after it left our hands.\n\nWhenever you state your experience with a piece of software, never forget you may have some difference in your hardware, configuration or libraries that make your experience utterly foreign to the vast majority of users. Here's a clue to your subjective analysis. Programs don't win user awards and accolades when they don't work right and don't seem finished. \n\nYou wouldn't complain to an auto manufacturer that you were stuck in a traffic jam after you bought their car. Instead of accepting you have a problem you should check with your distribution to see if they have a solution."
    author: "Eric Laffoon"
  - subject: "Re: Amarok good, but"
    date: 2008-02-29
    body: "Programs do win awards if it's the best FOSS has to offer, regardless of their shortcomings. And I'm not that clueless that you should slap me with your car/traffic jam analogy.\n\nI manage multiple systems with the same distribution, using the same backend, etc. On my main system Amarok works flawless. On two other systems I see intermittent crashes and track-skips. Might be the backend, might be the hardware, but apparently it's not the packaging.\n\nAlso, when dragging/dropping multiple tracks to the Collection view, many times only 1 of those tracks ends up in the local Collection folder, without any feedback to the user that not all has been copied.\n\nIt's often the error conditions that are badly handled, as is the case with Amarok.\n\n"
    author: "Wil Palen"
  - subject: "Re: Amarok good, but"
    date: 2008-02-29
    body: "Amarok 2.0 isn't finished yet.\n\nMost everybody knows that.\nI just hope it will be in time for KDE 4.1"
    author: "Max"
  - subject: "Re: Amarok good, but"
    date: 2008-03-03
    body: "\"I just hope it will be in time for KDE 4.1\"\n\nWhy?\nAmarok has its own release schedule, it isn't part of the KDE Core Packages.\nSo it could get released before or after the release of 4.1.\n"
    author: "whatever noticed"
  - subject: "Re: Amarok good, but"
    date: 2008-03-02
    body: "Will a new alpha of Amarok be released anytime soon? \nMaybe with KDE 4.0.3? A wider audience should get to test it."
    author: "Steve"
  - subject: "Re: Amarok good, but"
    date: 2008-03-02
    body: "Yep, we're shaping up for another alpha. Stay tuned."
    author: "Mark Kretschmann"
  - subject: "Re: Amarok good, but"
    date: 2008-03-02
    body: "Awesome. I'm already on the edge of my seat!!!\n\n-Max"
    author: "Max"
  - subject: "what does KDE developer uses?"
    date: 2008-02-29
    body: "I'd like to see the same poll answered by KDE developer only.\n\nwhat distribution do you prefer to compile KDE4 on ?\nwhat distribution has best and always fresh KDE4 packages?\neditors, method of compiles, etc.\n\nfavorite language/bindings,environment?\n\nqtruby/kommander/C++\n\nall this could be updated frequently, or all member of http://www.kdedevelopers.org/ could simply update their information at anytime and the stats would be always dynamic.\n\nthat would be great.\n\n"
    author: "Mathieu Jobin"
  - subject: "Re: what does KDE developer uses?"
    date: 2008-03-02
    body: "Wouldn't this cause controversy?"
    author: "Steve"
  - subject: "Re: what does KDE developer uses?"
    date: 2008-03-02
    body: "I'm a KDE developer (maintainer of Kompare), but as I'm also a Fedora KDE packager, I don't think I can give you an unbiased answer to your second question. :-) You can guess my answer to the first one. ;-)"
    author: "Kevin Kofler"
  - subject: "That was for KDE 3.X...."
    date: 2008-03-18
    body: "  I suspect this is all going to go in the opposite direction with KDE 4.X."
    author: "Steve"
  - subject: "kde is ok and also"
    date: 2008-03-19
    body: "it's not the only desktop environment!\npersonally I prefer jvwm-crystal with rox desktop but still use most of the kde apps. "
    author: "knifemonkey"
---
The 2007 LinuxQuestions.org Members Choice Award winners have been announced. KDE <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/desktop-environment-of-the-year-610190/">leads the popularity list</a> in the category Desktop Environment with a rocking 52% percent of the votes leaving competing contenders in its dust. Among the users on Linuxquestions.org, KDE is being praised for its high level of integration, for the number of applications and of course for Konqui being the cute mascot it is. But also KDE applications have been very popular among the voters on Linuxquestions.org. Read on for more details.



<!--break-->
<p />
KDE applications also score very high in users' popularity. KDE's CD and DVD burning application K3b <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/multimedia-utility-of-the-year-610224/">has won</a> in the category of multimedia utilities with nearly 2/3 of the votes: 63%. Amarok continues to amaze its users. 57% of the sample in the Linuxquestions survey prefer Amarok above other tools, making it the <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/audio-media-player-application-of-the-year-610226/">clear winner</a> in the category Audio Media Player. Konqueror wins as <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/file-manager-of-the-year-610231/">most famous</a> filemanager with 38% of the votes.
The discussion whether to use Vi or Emacs is history now. Kate <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/text-editor-of-the-year-610219/">wins a second place</a> behind Vi and scores much better than Emacs. KDE's email client KMail <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/mail-client-of-the-year-610222/">follows up</a> Mozilla Thunderbird. In the category of IDEs, Quanta <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/ideweb-development-editor-of-the-year-610217/">finishes second</a> behind Eclipse. The category messaging application lists Kopete as 2nd most popular, following up Pidgin.
<p />
The KDE community is really happy about all the awards and the confidence users have in KDE4 living up to everyone's expectations. Congratulations to everyone involved in those projects!
