---
title: "KDE Commit-Digest for 30th December 2007"
date:    2008-01-03
authors:
  - "dallen"
slug:    kde-commit-digest-30th-december-2007
comments:
  - subject: "Nice Progress"
    date: 2008-01-03
    body: "A very Happy New Year to you all in the KDE team and thanks very much for your tremendous accomplishment with this project. \nI did a KDE4 Suse update last night (German time) and all I can say is that the critics will be getting more silent :) It's obvious that you guys haven't been sleeping much over the Christmas season. I am really happy with the progress.\n\nLast but not least, thank you very much for keeping us up-to-date Danny."
    author: "Bobby"
  - subject: "Re: Nice Progress"
    date: 2008-01-04
    body: "Agree!!!\n\nYou guys have been busy. Please keep up the good work. Only a few days left. Please don't skimp now. Only a few more days, then we all can sleep again.\n\nYou guys can sleep, cause the product is released and working well.\nand we can sleep, because we're not waiting up all night in exitement and anticipation. :p\n\nHappy New Year 2008 to the KDE team and the open source community!!!!\n\n\n"
    author: "Max"
  - subject: "Re: Nice Progress"
    date: 2008-01-04
    body: "this post made my smile :)"
    author: "Dolphin-fanatic :)"
  - subject: "Re: Nice Progress"
    date: 2008-01-04
    body: "It is unfortunate that you don't seem to actually understand the meaning of the word 'critic'.  Since it is basically the same word in German [Kritiker], I presume that it isn't a language issue.\n\nTrue critics are not mere gripers, they (we) raise issues because we want them to be addressed -- we want the problems fixed.  So, our interests are in having KDE succeed.  One wonders if the critics are more interested in the success of the project than those that seek to silence critics by attacking the messenger rather than addressing the message.\n\nI am happy to see progress, but I doubt that it will be sufficient to have it ready by the release date."
    author: "YAC {yet another critic}"
  - subject: "Re: Nice Progress"
    date: 2008-01-04
    body: "I didn't imply that critics are all negative in my statement. What I said is that they will be more silent.\nI have to disappoint you by saying that I do understand what the word means both in English and German. German is actually my second language and I was born and grew up in an English speaking country. I have to admit that my English is not as good as it once was but the basics are still there :)\n\nIt's a bit sad that the most people don't see criticism in a positive way, that they can't use it for improvements but that's the way it is unfortunately. However at this point I think that the developers could do with a little less bashing. \nThere is a thread below about \"stinking consistency\" which Aaron replied to. He said it's not what the guy said that was a problem but how he said it. And that's the point. The most people who criticise do it aggressively without realising that they are doing more damage than good, making even personal attacks which kills the whole purpose of the criticism and demotivates the one criticized.\n\nI have faith in the KDE team that they will deliver a very stable (though not perfect) and usable desktop by next Friday. They have told us what to expect and  I am sure that they will fulfil what they promised.\n"
    author: "Bobby"
  - subject: "World Wide Commits"
    date: 2008-01-03
    body: "I am very impressed by the increasing number of commits from the US of A.\n(Still nothing from Ireland)"
    author: "reihal"
  - subject: "Re: World Wide Commits"
    date: 2008-01-03
    body: "It's \"U, S and A\" !"
    author: "Borat Saghdeyev"
  - subject: "Re: World Wide Commits"
    date: 2008-01-03
    body: "No, its not."
    author: "she"
  - subject: "Re: World Wide Commits"
    date: 2008-01-03
    body: "according to Borat it is ;-)"
    author: "debian"
  - subject: "Re: World Wide Commits"
    date: 2008-01-03
    body: "All your base are belong to us."
    author: "NabLa"
  - subject: "Re: World Wide Commits"
    date: 2008-01-03
    body: "My only comment on the map would be to use a colour other than red - it's hardly a \"bad thing\" that commits are coming from those countries (all work gratefully received etc), and especially with some large-area countries like Russia and Canada it looks like it's a map of Really Bad Things Everywhere :-)"
    author: "Andy Allan"
  - subject: "Re: World Wide Commits"
    date: 2008-01-04
    body: "The \"Rainbow\" map theme is more about the \"temperature\" of development - where red means \"red hot\"! I don't see that as a bad thing. ;)\n\nYou can also use the \"default\" colour theme!\n\nDanny"
    author: "Danny Allen"
  - subject: "networkmanager"
    date: 2008-01-03
    body: "I just compiled from SVN... looks good so far. I know, there has been some work on integrating networkmanager into solid... How do I use it? is there already a GUI frontend available? or do I need to install KDE3 libs and use knetworkmanager for the time being?"
    author: "Thomas"
  - subject: "Re: networkmanager"
    date: 2008-01-03
    body: "The Solid/NM stuff is not usable yet.  Use KDE3 knetworkmanager for now."
    author: "Will Stephenson"
  - subject: "Something I really liked"
    date: 2008-01-03
    body: "Thanks for a simple scanning application!"
    author: "Diederik van der Boor"
  - subject: "Re: Something I really liked"
    date: 2008-01-03
    body: "does it support scanning to pdf?\nor just pixel images?"
    author: "asdf"
  - subject: "Re: Something I really liked"
    date: 2008-01-04
    body: "I\u00b4m assuming you will do that through Print to PDF.  Planned for KDE4.1, a proper `Export to PDF\u00b4 standard action rather than expecting users to know to use Print instead.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Something I really liked"
    date: 2008-01-05
    body: "proper `Export to PDF\u00b4?\ncreating PDFs with OCRed text behind the image, that would be *proper* IMHO."
    author: "testerus"
  - subject: "RSS Broken"
    date: 2008-01-03
    body: "I don't know if you have noticed, but the RSS feed is somewhat broken.\nEach item have a body of \"undefined\" instead of the real message text. It has been like this for a while."
    author: "Magnus Bergmark"
  - subject: "Re: RSS Broken"
    date: 2008-01-03
    body: "The Digest RSS feed works fine for me here with Akregator...\n\nCan you explain more (preferably telling me what needs to be changed code-wise in the feed?)\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: RSS Broken"
    date: 2008-01-03
    body: "I think he means there is no content in the rdf feed. Below is an extract from the feed:\n\n<item>\n<title>KDE Commit-Digest for 30th December 2007</title>\n<link>http://dot.kde.org/1199338440/</link>\n</item>\n<item>\n\nYou're right that it works in Akregator (does that fetch the content from the URL?), but in other places e.g. Google /ig homepage the feed shows up as only links with no content. That means the only way to read the summary is to come to the dot. No biggie, but unusual."
    author: "Martin Fitzpatrick"
  - subject: "Re: RSS Broken"
    date: 2008-01-03
    body: "My mistake, Akregator is using a different feed. It is the dotkdeorg.rdf which is contentless, while the updates.rdf feed contains the content. Hope that helps.\n\nhttp://commit-digest.org/updates.rdf\nhttp://www.kde.org/dotkdeorg.rdf"
    author: "Martin Fitzpatrick"
  - subject: "Re: RSS Broken"
    date: 2008-01-03
    body: "I believe he is speaking of the Dot's RDF feed.  That feed is contentless while the Commit Digest's RSS feed actually shows the main paragraph as content."
    author: "Matt"
  - subject: "Re: RSS Broken"
    date: 2008-01-04
    body: "Anybody knows btw why the Dot feed is contentless?"
    author: "Fred"
  - subject: "Good job, KDE guys and gals!"
    date: 2008-01-03
    body: "Although the 4.0 release will likely be rough, it seems there are some people around who are happy to concentrate on all the many, many good things we're seeing, even at this very early stage of the KDE4 cycle:\n\nhttp://digg.com/linux_unix/10_Things_I_Love_about_KDE_4_RC2\n\nKeep up the good work! And thanks to the people who worked so hard over the Christmas time to get everything banged into shape - a surprisingly high number of commits, considering the season."
    author: "Anon"
  - subject: "Re: Good job, KDE guys and gals!"
    date: 2008-01-03
    body: "He did 10 things I hate about KDE4 RC2 last week. It was very interesting but some people weren't happy about it. Anyway the most of those things have now disappeared. I am going to read what he wrote this time.\n"
    author: "Bobby"
  - subject: "Re: Good job, KDE guys and gals!"
    date: 2008-01-03
    body: "There is also a long discussion whether to release or not at http://digg.com/linux_unix/KDE_4_0_Reviewer_Reminders "
    author: "Anonymous"
  - subject: "Re: Good job, KDE guys and gals!"
    date: 2008-01-03
    body: "The developers know what's best, after all it's not their first desktop release.\nWhat really sick my stomach are people who just look at a few screen shots or read a few blogs about KDE4 and then pass their judgements. There are people who have handed down the most critical judgements without ever using KDE! This is really crazy. And there are those who compare KDE 4 to Vista calling it a beta product on release. There is a world of difference between the both: one is not forced to take a beta product on purchasing a new PC/Laptop like it is in the case of Vista and paying hundreds of dollars in the process. Another thing is that one is not forced to live with that beta product for 5 years as in the case of XP but the same people who are complaining about a 4.0 that's not released yet  stop complaining after buying their expensive beta M$ software. Isn't it a crazy world?"
    author: "Bobby"
  - subject: "Re: Good job, KDE guys and gals!"
    date: 2008-01-05
    body: "Indeed!"
    author: "jospoortvliet"
  - subject: "Re: Good job, KDE guys and gals!"
    date: 2008-01-03
    body: "Shouldn't that be a piece of dot.kde.org news in itself?"
    author: "Axel"
  - subject: "New name for Glimpse"
    date: 2008-01-03
    body: "Thanks Danny for another great Digest and KDE developers for your frenetic work at this turning point in KDE history.\n\nSince the developer mentioned they wanted a \"less generic\" name for the scanning program than \"Glimpse\", I propose \"KangaScan\" since Kooka was an abbreviation of kookaburra, another Australian animal. Kanga is from kangaroo, an indigenous Australian word. Or what about \"KoalaScan\"? An oxygen icon for either name could look pretty exciting.\n\nI was going to propose DrakScan as an alternative, so the person who proposed Drak for Dragon Player can still see their name make an appearance, :) but after googling it, I see that it's the name of a Mandriva program so it's unavailable :(\n\nAre there other ideas? I guess some people will be wanting a non-English name to reduce the \"Anglo-imperialism\" in KDE's application names. Kangaroo is not English but an Aboriginal word originally meaning \"I don't know\" (this is what the Sydney Aborigines said when an Englishman asked them what the animal was called), but is now the name of the cute furry marsupials that are Australia's animal emblem. Still, it would be cool to hear other people's suggestions. Propose a name now, or forever hold your peace! :)"
    author: "Darryl Wheatley"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "I like \"KangaScan\" very much, sounds nice and the oxygen icon could be really cool!"
    author: "Marc"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "\"KangaScan\", that sounds familiar. Am I the only one who thinks of Kangaskhan? Probably. But it's still quite funny: http://en.wikipedia.org/wiki/Kangaskhan\n\nHeh."
    author: "Hans"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "It really sounds like Kangaskhan but I like the name, especially that it has a K at the beginning keeping the KDE tradition :) "
    author: "Bobby"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "What about \"Genghi Skan ?\""
    author: "anonymous"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "Now *that* would have an interesting icon!"
    author: "Matt"
  - subject: "Re: New name for Glimpse - Kangaroo name origins"
    date: 2008-01-03
    body: "Actually the Kangaroo was named in Cooktown (Queensland) when Captain Cook's crew were repairing their ship there.  The name was derived from the local Aboriginal name for the grey kangaroo.\n\n(As an completely off topic aside: It is technically Cooktown and not Syndey that was the first recorded site of European settlement in Australia- since Captain Cook and his men lived there seven or so weeks in 1770.)"
    author: "Tom Chandler"
  - subject: "Re: New name for Glimpse - Kangaroo name origins"
    date: 2008-01-03
    body: "Oops, I don't want to verse you in a trivia contest :D Shame on me for not verifying my facts (and the \"I don't know\" meaning is apparently a myth, so my charming frontier tale is falling apart). But thanks for the correct info."
    author: "Darryl Wheatley"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "What about a simple \"Skan\" or \"sKan\"?"
    author: "blueget"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "What about Klimpse? "
    author: "Mihas"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-03
    body: "I was actually happy to see yet another name not being k-ified in it's name. Hell, even one starting with a G ;) I do like Glimpse as a name, and that it is a word used often should not stop you from using it. As long as there is no other (comparable) software with the name, you're fine I would say."
    author: "Pieter Bootsma"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-04
    body: "in modern Hebrew a scanner is a sorek."
    author: "yman"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-04
    body: "Keep Glimpse. Along with Phonon, Plasma and other non-K names this one works."
    author: "Marc Driftmeyer"
  - subject: "Re: New name for Glimpse"
    date: 2008-01-04
    body: "Cornea is much better.\nNo need for a k."
    author: "reihal"
  - subject: "Wallpapers?"
    date: 2008-01-03
    body: "The new wallpapers are still not commited. Any plans to commit them so we will have a new nice default? I think tagging ist tomorrow so we are running out of time."
    author: "KDErocker"
  - subject: "Re: Wallpapers?"
    date: 2008-01-04
    body: "http://websvn.kde.org/branches/KDE/4.0/kdebase/workspace/wallpapers/"
    author: "Mihas"
  - subject: "New Oxygen logout icon"
    date: 2008-01-03
    body: "Hmm, the previous logout icon was much better imho, the new one looks so XP :-/\n\nhttp://commit-digest.org/issues/2007-12-30/moreinfo/752456/#visual\n\nhttp://danakil.free.fr/linux/logout.png  (XP and KDE)"
    author: "DanaKil"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-03
    body: "The colour distinction does make a difference. Before logout, restart and shutdown were all black. One had to read the discription but now it's easier with different colours although I think that logout should have a different colour than shutdown.\nI disagree that they look like XP's, they are far more beautiful."
    author: "Bobby"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-03
    body: "I you want to complain about the new logout icon design, the only reason could be because it looks like the one in ubuntu.\n\nI think it looks great and is totally different than the XP logout icon except the color.\n\nBTW is there any change to get another folder icon as option, not default. I'm referring to the one replaced by the current version because people complained it doesn't look like a folder. I use it since months and am addicted to it, in the end its a matter of habit. It would be cool to be able to change it easily because it's part of oxygen, instead of keeping a copy around all the time of KDE4."
    author: "hias"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-04
    body: "> BTW is there any chance to get another folder icon as option, not default.\n\nI'd say that's not very probable, it would likely require a mechanism to override any theme icons, and that's not particularly easy to do and requires a bit intrusive code changes for a very rare edge case wish.\n\nThe better way would be to create a derivative theme that just replaces the \"folder\" icon and otherwise inherits from Oxygen... but I'm not motivated enough to find links on how this works - I'd say take a good icon theme, look at its index.desktop file and adapt that, throw out most of the icons and insert your favorite folder icon there.\n\nWith a little luck, you can have your own personal derivative theme without much effort, plus you don't need to wait for some developer with too much time to implement in-theme icon switching."
    author: "Jakob Petsovits"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-03
    body: "I have to agree with DanaKil. The colors are far to bright; they don't fit in with the other Oxygen icons or the dark Plasma theme at all. I think the artists should ditch the idea that log out, shutdown, restart, etc. should all have the same shape. Or if they don't, at least keep the cool round shape and add the color to the symbols on the buttons, rather than make the whole button colored.\n\nHere's an example of what I mean: http://wmhilton.googlepages.com/my-KDE4-system-log-out2.png\n\nI'm not an artist, so it doesn't look very good, but that's the idea I'd like to see used instead of the square buttons. If this can't be done before KDE 4.0 is tagged today, then maybe for 4.1."
    author: "kwilliam"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-05
    body: "Hmmm, I think I kind'a agree with you. I found the logout buttons to be very in-your-face as well... Probably a 4.1 thing, indeed."
    author: "jospoortvliet"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-04
    body: "The sad part is that I have to agree.. :(\n\nC'mon people. Oxygen looks so gorgeous, why stop now at the logout icon?\n\nRemember KDE is supposed to look towards the future, not the past..\n\n-M"
    author: "Max"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-04
    body: "I have to disagree. I like the new logout icon better. The Oxygen guys are doing a great job.\n\nBUT... It is impossible to please everyone, and taste is very personal"
    author: "Askrates"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-05
    body: "Well that was the group of icons people complained most about, this ones are less controversila and still fit the style, in fat they fit beter, one day I will make an all black theme ;) "
    author: "nuno pinheiro"
  - subject: "Re: New Oxygen logout icon"
    date: 2008-01-14
    body: "Hourra for an all-black theme :)\n\nCheers"
    author: "DanaKil"
  - subject: "We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "I recently tried a KDE4Live. The good news is that the Live-CD sucks considerably less than it did earlier, the bad news is that for some unknown reason KDE seems incapable of bringing some consistency to the desktop even if it would have reduced the devs workload without any meaningful drawbacks.\n\nPoint in case: Sidebars.\n\n-Konqueror got the old one. Tiny tabs and icons, the selected tab is expanded and shows icon+vertical text, so the tabs constantly jump up and down if you switch between them.\n\n-Amarok got its modification of that one. All tabs have icon+vertical text. (Imho that's the best solution but that's not really my point)\n\n-Ocular has an Opera-style bar. But unlike Opera's its icon bar with horizontal text isn't collapsible and (like Opera) you can't adjust its width. (It's a collossal waste of space, and a POS -- but that is also beside the point)\n\n-Dolphin has normal tabs along the bottom and if you place more than 2 items in the sidebar (places, folders, meta-info) you most likely have to scroll through the tabs. You can tile them but that makes the directory tree hard to navigate, or put the meta-info on the right but then you get only a tiny sliver with the actual files and dirs in the middle.\n\n-I'm not sure if there are still apps that use kpdf's old solution, or a simple drop-down menu but I wouldn't be surprised\n\nDo we really need four different solutions? Wasn't the goal to become more usable? Four ways of doing one thing is stupid. It's not even as if they were all specialized for their specific use case."
    author: "ac"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "Well you say which one is best isn't the point but it kind of is. We Amarok devs like ours the best, but others have a phobia of all that vertical text."
    author: "Ian Monroe"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: ">> -Konqueror got the old one. Tiny tabs and icons, the selected tab is expanded and shows icon+vertical text, so the tabs constantly jump up and down if you switch between them.\n\nYeah the icons are too small to distinguish and hit\n\n>> Amarok got its modification of that one. All tabs have icon+vertical text. (Imho that's the best solution but that's not really my point)\n \nExcept it doesn't work at all when you have more tabs than fit vertically.  Why do you think konqueror went to icons.\n\n>> Ocular has an Opera-style bar. But unlike Opera's its icon bar with horizontal text isn't collapsible and (like Opera) you can't adjust its width. (It's a collossal waste of space, and a POS -- but that is also beside the point)\n\nWorks for me.  When I'm viewing a PDF I have a lot of unused horizontal space anyway, and the buttons are easy to click and easy to read.\n\n>> Do we really need four different solutions?\n\nProbably not.\n\n>> It's not even as if they were all specialized for their specific use case.\n\nPerhaps not, but they all have unique advantages and disadvantages, and none of them are clearly superior and suitable for a default."
    author: "Leo S"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "Can't really say that Konqueror went to icons, Konqueror's sidebar predates Amarok's.\n\nBut yea, your last sentence pretty much sums up the issue."
    author: "Ian Monroe"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: ">> Except it doesn't work at all when you have more tabs than fit vertically.\n\nFade out part of the text, group some tabs together, have a \"list all tabs\" button for those that don't fit, that's not a deal breaker.\nAnd Konqueror is probably the most extreme case when talking about sidebars.\n\n>> Works for me. When I'm viewing a PDF I have a lot of unused horizontal space anyway, and the buttons are easy to click and easy to read.\n\nIf there's one of the tabs expanded it gets awfully wide, especially if the document is landscape.\n\n>> Perhaps not, but they all have unique advantages and disadvantages, and none of them are clearly superior and suitable for a default.\n\nExactly. As I said I don't really like the Okular solution, but having everything use Okular's sidebar (preferrably collapsible like Opera) would still be superior to doing it in four different ways."
    author: "ac"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "You know... you have a point. That sucks.\n\nI wonder what we could do about it? Konqueror, Amarok, and Ocular could conceivably just use the Amarok style sidebar tabs.\n\nThe difference with Dolphin's is that it's using Qt panels, which can be placed so more than one shows at a time (unlike a sidebar) and that \"tabbing\" feature is built in. Dolphin's panels are really shooting for a different paradigm than a \"sidebar\" because you can put them on either side or even the bottom. Kpdf used an entirely different widget - it's supposed to be more of a \"toolbox\" widget - and I must agree it was a... interesting choice. Other than Qt designer, which uses the widget in a way that makes much more sense, I can't think of any applications that use it. (Kpdf was deprecated by Ocular for KDE 4.0.)\n\nSo, yes, we should see if we can get the KDE Human Interface Guidlines (HIG) group to pick a standard \"sidebar\" widget, or suggest the specs for a new sidebar widget that would combine the advantages of Okular's, Konqueror's, and Amarok's sidebars. I'm not sure who to contact about that (nor do I see this as a pressing matter), but this would be an excellent example for HIG to work on."
    author: "kwilliam"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "Ah, I just saw the comments regarding the disadvantages of Amarok-style tabs. Well, maybe some kind of sidebar that does icons & text if there is enough room, and automatically switches to just icons if it senses theres not enough room? And you can drag the left edge to make it wider, at which point it becomes Okular style. That would solve all the usability problems, no? (It could be wide by default in Okular, but anoyed users could make it Amarok/Konqueror style by trying to resize it intuitively, by grabbing the edge and dragging.)"
    author: "kwilliam"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "\"Other than Qt designer, which uses the widget in a way that makes much more sense, I can't think of any applications that use [Kpdf-style sidebar].\"\n\nAh, I take that back. Marble uses it. The plot thickens.... ;-)"
    author: "kwilliam"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "\"even if it would have reduced the devs workload\"\n\nwell, that's unfortunately not particularly true. nor is the fact that there are these different sidebars a given that therefore usability is significantly weakened. i'd like to see the sidebars be consistent (and btw, a lot of things have been made consistent in kde, so your general snipe here is off the mark), the question is \"how\".\n\ni don't think we'll get away with One True Sidebar Style for fairly obvious reasons once you sit down and look at the needs and use cases of each. i do think we can probably end up collapsing the current sidebar options down to just 2 or 3 though: the amarok/konq style, the tool docks (what dolphin uses) and perhaps a toolbox style ala kpdf/marble. but you see, that gets us pretty much right to where we are, though each style could be shared and at the very least made consistent between those use cases (e.g. konq should either look more like amarok, or should go in one of the other directions0. unfortunately, one style does not fit the needs or ergonomics of each app.\n\nso while i agree that similar sidebar styles should be harmonized, asking for One True Sidebar style is probably not even a desirable end goal. trying to make the case that that would be best for usability is also far fetched.\n\nand since you've given us some feedback, i'd like to give you some as well: you ought to learn how to communicate in a way that encourages thought without resorting to invective and insult. you had some interesting points, but they were completely drowned out by your communication style, or lack thereof."
    author: "Aaron J. Seigo"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "What about allowing more than one GUI per application without resorting to a complete fork?\n\nI made a small suggestion about this some time ago in kde-look:\nhttp://www.kde-look.org/content/show.php/Scriptable+Application+UI?content=71313\n\nIn the future with more fancier GUIs (3d effects on everything, etc) it's inevitable to annoy some users, so this could allow someone to try something different without alienating old users."
    author: "Leiteiro"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "Use kde from Suse 6.0\nFollowed the new releases always\n\n\nNever seen so much resistance to the changes \n\nAs i stated elsewhere  there are no specs for the kde developpers\neach and every one can come up with \"whe do it this way\"\nThere is no organisation in the development.\n\nSo if this dus not change for the better i think kde 3.5.8 31.1 is the \nlast real kde release sofar and all the talk off kde4 results after many month\nin what we now see.  A very unsuefull version off.......   but sure no KDE.\n"
    author: "susegebr"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-03
    body: "I think you miss the point, no matter how long you use KDE or not, even though it is nice to see an old time user.\n\n\nWhere do you see resistance?\n\nImagine I'd say to a chef in a restaurant that his food sucks he should change that. First you pissed him off, second you gave no feedback on what did not taste you (!) well and third the chances things will get changed the way you want are pretty small because of first.\nMaybe devs are pissed of such kind of \"feedback\", I hardly beleive they are not open to a discussion but well they, you and me are human beings so respect that. \n\nIt boils down to \"How To Ask Questions The Smart Way\" [0] or simply how to treat the people you are talking to with respect.\n\nThat will make everything easier for you and them. A win-win situation. Really!\n\n\nOn \"specs\":\nYou have the old guidelines for KDE 3 and the new ones are a work in process. To quote: \"New interface guidelines are being developed in parallel with KDE4.\" [1]\nYou are free to post your ideas on the HIG mailgroup. There you could also see the process there may be instead of assuming there is none.\nAdditonally you could visit techbase [2] as a starting point on developement or the API documentation [3] if you want to see a proof of people not doing everything their way.\n\nMaybe, just maybe, you see or feel to see only a small part of the whole picture and assume the rest looks like that small part. So what you see probably is not the whole picture and not even the base of that picture.\nThe base could consist of changed colours, canvas ... -- basically a changed underlying technology -- that could result in a better outcome, like \"new\" colours that were not available/unaffordable a long time for painting really did.\nAnd for seeing the difference in the underlying technology you have to start painting, i.e. creating programs in this context. In fact you have to \"know\" the new colors first, where the documentation comes into play.\n\nI hope my reply is not too confusing -- that happens sometimes.\n\n[0] http://www.catb.org/~esr/faqs/smart-questions.html\n[1] http://usability.kde.org/hig/\n[2] http://techbase.kde.org/\n[3] http://api.kde.org/"
    author: "mat69"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: "\"You are free to post your ideas on the HIG mailgroup. There you could also see the process there may be instead of assuming there is none.\"\n\nThere have been no posts to that mailing list in well over a *year*:\n\nhttp://lists.kde.org/?l=kde-guidelines&r=1&w=2\n\nkde-usability is hardly a hotbed of discussion and productivity, either.  KDE has simply failed, over its 11-year lifespan, to develop a large/ active usability/ HIG-team, which is a huge shame as it has all the technical underpinnings down pat. "
    author: "Anon"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: "Well you are right, I had no time to check that myself yesterday -- did not wanted to write such a long answer in the first place -- that is why I wrote \"may\".\n\nThat's a pity. I hope that KDE will have more progress in that area someday."
    author: "mat69"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: "point taken but i take nothing back\n\nsee http://www.kde-forum.org/artikel/17965/KDE4---the-great-disappointment.html\n\nand further on  and you hopefully see what i mean.\n\n\nWe want a normale kikcker menu. 3.5.8  style and for me KDE menu style.\n\nif the underlaying software is Kwin or plasma  it should work the Same\n\nI have a Soundblaster live 5.1  see KMIX in kde 3.5.8  en see KMIX  in kde4 it misses nearly half the possible sliders.\nXine has delayed sound playing a dvd no arts no normal sound\n\nand the list go's on    read the forums !!!!!!!!!!!!\n\nWhere are the design specifications,  in a normal project you have them first\nbefore programming starts. and design specs have to be followed to the letter.\n\nYou cant build a car and  come up with the idea 3 4 or 6 wheels when the body is nearly ready.\n\n\n"
    author: "susegebr"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: "You can have a \"normal\" kicker-like menu. Go on read the dev's blogs!!!! ;)\n\nNo design specs don't have to be followed to the letter. That's unrealistic as sometimes you'll come up with problems those specs hardly can \"solve\", meaning the initial design specs have to be modified to a degree.\nAlso new technology like a newer Qt version could result in a modified spec.\n\nOn kcontrol that was mentioned in that forum-thread: If there is so much (dev) interest in it won't disappear as Aaron said in his latest interview, but so far there is not much interest by people capable of maintaining it. In contrast to the old menu that found a maintainer.\n\nAlso config options or lack thereof of taskbar etc. were mentioned. As Aaron said some time ago he plans to add them. Adding config options is like polishing, you do that rather at the end than at the beginning before you actually have something that could be configured. Yet I have to admit that I like the Gnome system settings more than the KDE one. Now hurt me. :D\n\nKicker button can be activated by moving the mouse to the lower left now.\n\nOn why they don't use compiz fusion but rather KWin also listen to the latest interview or read some blogs. In short it's easier that way.\n\nIIRC the weather applet has been discussed in a recent blog post. In fact does the author plan to add icons he simply ran out of time.\n\nBtw. a lot was \"broken\" in the old code base what I read so far, meaning it would have been hard to extend a lot of it. So hard to some degree that it would be faster and easier to start from scratch.\n\nSo some of the points raised in that thread are wrong and even I without being a dev was able to show that to a degree.\n"
    author: "mat69"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: "\"On why they don't use compiz fusion but rather KWin also listen to the latest interview or read some blogs. In short it's easier that way.\"\n\nHere's a comment giving hard numbers and stats:\n\nhttp://dot.kde.org/1180541665/1180560211/1180560581/1180578526/1180594617/\n\nIn a nutshell - writing a real, production-ready Window Manager is hard.  Writing code to move textures around is easy. "
    author: "Anon"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "\nOk   Bottom line  kde4 is not finished.\n\n\nGoogle for kde3 + compiz  vs  kde4  gives 33.300 hits   \nsort it out and you have more or less 12.000 pure kde3 vs kde4 hits.\n\nI should say time to start a site only stating  at what state kde4 and the kde4 programs + applets etc etc are  so we can see in one page if it is usefull to switch to kde4 from a good running system opensuse 10.3 retail +kde3 + compiz\n\nBtw I have been a dev for 25 years on large mainframes, the last 10 years in project management.\n\n"
    author: "susegebr"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "Well none said KDE 4 is finished.\nKDE 4 won't be finished for a long time.\n\nThe only thing that is \"finished\" is KDE 4.0 an initial release that even devs dub as not intended for most people out there as a lot improvements as well as old features are missing that are going to be add in later versions.\n\nMaybe what you are asking for will be mentioned in the release notes.\n\nBtw. it should be clear that neither alphas, betas nor RCs are intended for the general public per se. The same goes for a release.\n\nThe 4.0 release is for early adopters, people that want to develop apps for KDE and the people that like to bitch about unfinished products.\n\nImo you should not switch from a good running system to KDE 4.0, that could result in a disappointment. Wait for reviews of KDE 4.X versions and if you think it actually offers value for you then switch. Or you could try a Live CD.\nThe same rule goes for other software as well, like the Kernel. 2.6.0 was not that useable at all, while the latest version is.\nYou have to start somewhere.\n\nIf you found the time you could probably help a lot with your experience.\n"
    author: "mat69"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-07
    body: "\nTell me how to help  and give a idea off the time involved\n\nanwser me on email\n"
    author: "susegebr"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "I want to what Mat said above - there will be aq KDE 3.5.9 release soon (as soon as we've recovered from the stress caused by KDE 4.0). And yes, that release will have new features. We won't abandon KDE 3.5.x anytime soon - it will probably be supported for years still. Part of that is because KDE 4.0 simply can't replace 3.5 fully - yet. And I think it'll take to 4.2 before we have practically every feature from 3.5 back, so we need to actively support it for at least a year - and then some, as many users won't switch for another 2 or 3 years."
    author: "jospoortvliet"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: ">> well, that's unfortunately not particularly true.\n\nSo what you're saying is that Okular doing there own sidebar wasn't any more work than just using Amaroks? Sorry, I don't believe you, I think that's marketing-Aaron talking :)\n\n>> nor is the fact that there are these different sidebars a given that therefore usability is significantly weakened.\n\nProbably not significantly. But the one thing I hate the most about Vista usability is that stuff's all over the place. And I'm not the only one who thinks that. It won't make or break a GUI but I don't think there's a compelling reason to have all these different solutions because the needs of the different apps are mostly the same.\n\n>> the amarok/konq style, the tool docks (what dolphin uses) and perhaps a toolbox style ala kpdf/marble\n\nI think one of the advantages of Opera's sidebar would be that it could easily replace the first and the third.\n\nHow about icon-only, but when you hover over the side-sidebar (i.e. the selector) labels for all icons fade-in. ( http://img233.imageshack.us/my.php?image=sidebardn9.jpg -- Frankenstein's version. Use your imagination for a cool oxygenized look with bells and whistles)\nThat would be\na) slim\nb) provide icons+text\nc) no vertical text\nd) lots of space for many entries\n\nDisadvantages and possible solutions:\na) the labels can't be clickable otherwise actually using the sidebar (as opposed to the sidesidebar) becomes a chore\nb) even this way you'd probably want a slight delay and relatively slow fade-in to prevent a constant flicker of the labels appearing and disappearing again\nc) small target area for the mouse, although Fitt's law helps in some cases; it'd probably be a good idea to use a size somewhere between Opera's and Konqueror's icons\nd) you'd have to use graphical effects to make clear to even the most imbecile of users that the labels are just labels for the icons on the left and not clickable buttons\n\n\n\nDolphin's tool dock. Well, at least Dolphin doesn't really need it. The only use case I can think of is a combined Places/Meta sidebar (because the dirtree is pointless if you have to scroll all the time you generally don't want it to share vertical space with one of the others. Vista did it and it's just unusable) but is that really that important?\n\n\n>> so while i agree that similar sidebar styles should be harmonized, asking for One True Sidebar style is probably not even a desirable end goal. trying to make the case that that would be best for usability is also far fetched.\n\nMaybe not 1, but 2 should be enough. What's the big difference in usage between your first and third? I can see how the second one is optimized for a different task, although I can't think of any existing app where I'd actually need/want it (I think I'd prefer a Konqueror style sidebar in Dolphin. In things like Krita where stacking toolboxes vertically could be useful, the awkward tabs limit its usefulness, imho, because it's not really optimized for more than perhaps three boxes down one side)\n\n\n>> and since you've given us some feedback, i'd like to give you some as well: you ought to learn how to communicate in a way that encourages thought without resorting to invective and insult. you had some interesting points, but they were completely drowned out by your communication style, or lack thereof.\n\nIt's the first rule of Linux online tech-support but it also applies to feedback in my experience:\nPost a friendly request for help, state your problem clearly and concisely and list all information that could be useful in solving your issue and you'll be lucky if someone hurls a \"RTFM!\" in your general direction.\nBitch and complain about how Linux is unusable and <competing DE> is much better  and the POS doesn't work because <your problem here> and you're gonna be insulted and derided but they'll prove your inferiority by posting 4 different perfectly working solutions to your problem.\n\nIn this case:\n>> you ought to learn how to communicate in a way that encourages thought \n\nIt apparently did work.\n\n>> without resorting to invective and insult.\n\nThe worst part of my post was that I called the current state of things \"stupid\". I didn't even call the people that did it stupid and the second one doesn't follow from the first (the way to hell is paved with good intentions).\nMy tone was confrontational but not insulting.\n\n>> but they were completely drowned out by your communication style, or lack thereof.\n\nIt didn't keep you from posting a detailed reply, so I challenge your assertion"
    author: "ac"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-06
    body: "\"Invective\". Great word, AS."
    author: "Joe"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-04
    body: "I have filed a wishlist item in May about similar issues with MDI and also sidebar system: https://bugs.kde.org/show_bug.cgi?id=145275 :\n\nCurrently handling multiple documents/tabs and sidebars work differently in Kate, Kile, Quanta+, KDevelop, KOffice (which actually can't handle more than one documents in one window) and Konqueror. \n \n  - Konqueror has tabbing. It is acceptable, although some improvements could be made (e. g. dragging a tab without pressing Ctrl should move it instead of copying it). It features a New tab and a Close tab button. The tabs also have a popup menu. \n  - Kile also has tabbing but somewhat differently: there is no New tab button (no problem, there is New document instead) and tabs can be closed with clicking on the icon after a delay, as there is no Close tab button on the right. Tabs do not have a popup menu. \n  - Kate also handles multiple documents at the same time, but in a quite useless way. It has a Documents sidebar instead of tabs, in which the documents are listed in a column, so it occupies much more place on the screen. (Especially if few documents are open.) It also has a Tab bar extention, but it can only do switching between documents - no popup menu, no Close button etc. and if you have more documents open than how many fits on the screen then it requires two clicks. \n  - KDevelop has a quite well configurable UI, and Quanta as well, but not the same system. \n  - KOffice components do not feature MDI at all, they use top level windows. \n \n This means that if you configure the MDI/tabbing in one application, and get used to it, it may be completely different in an other app. And you have to suffer from useless MDI in some applications (e. g. Kate), while it is already implemented much better in other apps. \n   \n  The same applies to sidebars: just try to open a Konsole inside Konqueror, Kate, Kile and Quanta and see the differencies. \n \n I think all these applications should use a common MDI and/or sidebar system and it should be configurable in the Control Center."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "I agree, this is one of the things we really need to work on in KDE... Esp Kate is imho a thing needing improvement, I don't use Kile but I guess the same goes for that one. KOffice has MDI in the KOffice Workspace, so you don't need it in the separate apps.\n\nKonqi can move tabs, btw, with the middle-mouse. Much better than the normal mouse - as that would make dragging & dropping harder. Let's not dumb it down, please."
    author: "jospoortvliet"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "I don't know, but I think that one of the reasons to why GNOME is so popular is that they have a guideline for how an application have to look and work.\nIt make it more consistent and make a better user-feeling (is that even a word? ;) )\n\nI think that it would be great if the KDE-developers could make a guideline too, a desktop environment as large as KDE needs a consistent look. And I think that it was a goal for the KDE4 development, right? But lucky for us, is that the KDE4-development cyclus isn't over yet... Actually it's first started :)\n\nLet's hope we get a more consistent look over time!"
    author: "Kris"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "Why don't you take a look at Techbase (techbase.kde.org) ?\nSpecifically this: http://techbase.kde.org/Development/Guidelines"
    author: "csanchisb"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "These guidelines are very old and or incomplete.\nI think it is true KDE needs something like the Gnome HIG.\n\nIn fact a lot of the information in the Gnome HIG could be dubbed as \"logical\" and unnecessary but on the other hand a lot of questions that might arise are answered that way.\nThe Gnome HIG has the advantage that everything is concentrated in one place and still very clear. "
    author: "mat69"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-03-11
    body: "We need no guidelines that are implemented (more or less) by the developers of every application, but shared code that is used by every application. That is the KDE way."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: We don't need no stinkin' consistency"
    date: 2008-01-05
    body: "I think you shouldn't really include konqueror here, it still has the old style sidebar - but I think that's mostly simply because there wasn't much work put into konqi's UI."
    author: "jospoortvliet"
  - subject: "\"regexpeditor\""
    date: 2008-01-03
    body: "is one of the best KDE apps... sad :(\n"
    author: "anonymous"
  - subject: "Re: \"regexpeditor\""
    date: 2008-01-03
    body: "Just because it's currently unmaintained doesn't mean it's dead forever, dude :)"
    author: "Anon"
  - subject: "Ocular"
    date: 2008-01-04
    body: "Two people misspelled Okular as Ocular already. And you know what?, it looks better.\nHow about another name change Okular->Ocular? For 4.1? :-D"
    author: "tobami"
  - subject: "Re: Ocular"
    date: 2008-01-04
    body: "For me as Austrian (don't know of all the other languages, but Okular exists as a German word at least) this reads with a completely different intonation.\n\nWhile I read \"Okular\" as \"Oh-coo-lar\", \"Ocular\" sounds more like \"Oh-kyu-lar\", and that changes the character of the name completely. Don't know, personally I like it the way it is."
    author: "Jakob Petsovits"
  - subject: "Re: Ocular"
    date: 2008-01-04
    body: "Do you Austrians read Binocular as \"Bye-nah-kew-lar\" or \"Bin-ah-kew-ler\"?\n\nOcular is better."
    author: "Marc Driftmeyer"
  - subject: "Re: Ocular"
    date: 2008-01-04
    body: "They both read the same in my language. But i would prefer Okular since it kontains letter \"K\" doh..."
    author: "Dread Knight"
  - subject: "Re: Ocular"
    date: 2008-01-04
    body: "They both read the same in my language. But i would prefer Okular since it kontains letter \"K\" doh..."
    author: "Dread Knight"
  - subject: "Re: Ocular"
    date: 2008-01-05
    body: "Here too..."
    author: "jospoortvliet"
  - subject: "Re: Ocular"
    date: 2008-01-04
    body: "Cs suck. Ks rule. Okular ftw :)"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Ocular"
    date: 2008-01-08
    body: "> Two people misspelled Okular as Ocular already. And you know what?, it looks better.\n\nNo it doesn't."
    author: "Erik"
  - subject: "KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "Please make sure that KDE 4.0 integrates with, or works well with Compiz fusion.\n\nCompiz has a long history, and many modules. I'd hate to have to give these up for KDE 4.0 :(\n\nIt has to be simple to install (or activated out of the box (depending on hardware)) otherwise it will turn many people away. Users have to be impressed with KDE right out of the box. (include things like: cube rotate, Expose, magic lamp, \"cover flow\", reflections, etc.)\n\nKDE is already a minority. (distro's are dropping it. :( )\nIt has to be great to regain it's former glory. Don't skimp on eye-candy.\n\nThey way I see it: Eye candy is what makes a person first notice a mate and ask him/her out. Only then the person will discover the personality, intelligence, and stability to stick around and get to know the person (or KDE) more.\n\nCompiz fusion was the reason I finally migrated to KDE + Linux and took 5 clients with me along the way. There just wasn't anything remotely as beautiful on OS-X, or 'gasp: Vista. Now we fell in love and don't want to leave.. :D\n\n "
    author: "Max"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "KDE4 has it's own compositing manager built in.. \nhttp://francis.giannaros.org/blog/2007/12/03/kde4-desktop-effects-kwin-composite-video-tour/"
    author: "somairotevoli"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "I know that.. :)  I'm happy about it.\n\nIt just doesn't have anywhere near the functionality, or the hype as compiz fusion.. :(\n\n\n"
    author: "Max"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "I think it's up to compiz fusion develepers to make sure kde4 \"integrates with, or works well with Compiz fusion\", not the other way around.\n\nI been playing around w/ kwin's compositing , and even w/ a nvidia tnt2, it's looking good. Have you been able to check it out yet?"
    author: "somairotevoli"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "\"I think it's up to compiz fusion develepers to make sure kde4 \"integrates with, or works well with Compiz fusion\", not the other way around.\"\nThat's what the KDE developers said, which actually makes sense but will they (the compiz guys) go the extra mile? I mean they have Gnome and compiz-fusion is actually a Gnome application so why bother with KDE?\n\nCompiz-Fusion is far ahead of Kwin4 when it comes to eye-candies but the effects in Kwin4 are much better integrated. An example is running Kaffeine with Compiz-Fusion turned on. You will get a funny looking green line at the top and the pictures delay when playing some video formats. This doesn't happen when using Kwin4 and one gets the KDE system sounds! Which you don't get when Compiz-Fusion is turned on.\n\nI would love Kwin4 to get a bit more of Compiz-Fusion's eye-candies with it's very good integration instead of using Compiz on KDE4 but I think it will come over time. Like they say: patience is the name of the game :)"
    author: "Bobby"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-05
    body: "Well, what you want is much easier too - Compiz effects are just a few hundred lines of code each, pretty easy to port too. On the other hand, all the cool stuff in KWin and all it's power would be almost impossible to port to Compiz, so the KDE dev's clearly choose the smart route."
    author: "jospoortvliet"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "compiz has more eye candy effects, but KWin has lots of practical functionality that's missing from compiz. Adding practical functionality to compiz is harder than adding more eye candy to KWin."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "Which distro has dropped KDE?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE 4.0 + Compiz fusion?"
    date: 2008-01-04
    body: "Maybe he has confused Ubuntu's KDE LTS with them dropping it which is not the case. Canonical is simple being careful because KDE4 is still in early stage, which one can understand. \nI think that they will do the same when Gnome finally sees the light and decides to do a clean up and go 3.0, which by then KDE4 will be more than mature. Distros would be stupid to drop KDE because it's the more beautiful and advanced of the two popular open source desktops and I don't see that changing in the future."
    author: "Bobby"
  - subject: "Impressed, no matter what other say!"
    date: 2008-01-04
    body: "Personally i'm deeply impressed by the state of KDE4 allready. It's simply amazing what a group of dedicated developers, translators, and other way involved people can do, mostly in there free time!\n\nYesterday, I installed Ubuntu 7.10 (the gnome edition) on some free space on my laptop. This, because this release is said to be the most polished Gnome experience at the moment. I fiddled around a bit, using f-spot, some multimedia apps, and evolution. I was confused allmost all of the time. KDE 3.5.8 is, for me, by far the better desktop. KDE4, in it's current shape is, to me, also better. Sure. there are some things left to be desired, but overall, the look and feel is imho so much better than current Gnome. And this is just the start! Remember, this is v1.0 of the KDE4 series!\n\nWhen KDE 4.0.0 is released, it will receive critics, i'm sure. People tend to focus on what's wrong, not what's better. I really, really, really hope this will not discourage the people involved to leave in bitter disappointment. Just be prepared for it and keep informing people of the reasons why KDE 4.0.0 was released. I see it as a \"developers release\", It gives the dev's of third party apps a solid ground to work on to port their apps. I really hope that Gnome users, eager to try KDE4, will not back down when using KDE 4.0.0. \n\nI would like to give all you guys a very big thumbs up for all you've done so far and all you will do in the future. There come's a moment I will get involved. I'm playing with Python/Qt4 now and I like it. Speaking of the future, I can't finish this without at least giving my personal top3 wishlist for 4.1:\n\n1. PIM (kmail needs to support HTML sigs and nice background and that kind of stuff, just to make my boss and my wife happy :-). Make it \"cool\" again!\n2. Konqi. maybe webkit isn't such a bad idea. Give us a choice of render kit (khtm, webkit, etc). I'm working with ampache 3.4-beta right now, and it won't play nice with konqi. I filed a bugreport, but the dev just plainly refused to fix it, because he states konqi is \"seriously broken\".\n3. KNotes should get tomboy like functionality (without mono please :-)\n\nAgain. Thanks for all and keep it up!!!\n"
    author: "Fred"
  - subject: "Re: Impressed, no matter what other say!"
    date: 2008-01-04
    body: "Nice post. I hope that the developers will read it and let it sit in their heads. People will bomb them with criticism on the release of KDE 4.0, some of the critics will even be coming from people who never used KDE but the devs should keep their goal in mind, the goal to make KDE 4.0 the most beautiful and powerful desktop ever! \nThey are already on the right track, all that they have to do is keep it up.\n"
    author: "Bobby"
  - subject: "Re: Impressed, no matter what other say!"
    date: 2008-01-04
    body: "I can't imagine KDE 4.0 winning over critics necessarily, but I think it might win over developers.  KDE 4.x will win over the critics in time."
    author: "T. J. Brumfield"
  - subject: "seconded :)"
    date: 2008-01-05
    body: "Agreed guys (and girls, of course); great job!  Now that 4.0 is coming out and stabilising in distros, I'll be able to target that platform with my meagre code, and I appreciate that too :)\n\nEnjoy the release party -- you've more than earned it :)\n"
    author: "Lee"
  - subject: "Re: Impressed, no matter what other say!"
    date: 2008-01-08
    body: "One thing everyone can do, be they developers or not, is to monitor the tech review sites and make sure to be active in the comments sections to correct errors of both fact and perception. \n\nIf we all make an effort to be polite and provide links where possible it will surely help up the signal side of the usually pretty poor signal/noise ratio a lot of these sites have."
    author: "Borker"
  - subject: "supporting KDE 4.0's devs"
    date: 2008-01-04
    body: "Support is here... who should we carry? :)"
    author: "Tom"
  - subject: "Re: supporting KDE 4.0's devs"
    date: 2008-01-05
    body: "Start with Aaron (or should I say aaron?) - with all the sh*t he got over himself during the last months, he can use some support."
    author: "jospoortvliet"
  - subject: "KWin4 on KDE3"
    date: 2008-01-04
    body: "My best computing experience is KDE 3.5 along with KWin 4 to enable compositing (I have tried Beryl earlier which refused to work even with the right X config). Does anyone have experiences regarding this combination? All I know is that locking the session does not work (unless I replace KWin 4 with KWin 3 before locking)."
    author: "Stefan"
  - subject: "Re: KWin4 on KDE3"
    date: 2008-01-04
    body: "I've used KDE 4's kwin in KDE 3 for a number of months, and in general it works fine, including kdesktop's screen locking."
    author: "Eike Hein"
  - subject: "Re: KWin4 on KDE3"
    date: 2008-01-04
    body: "I just use compiz-fusion over KDE 3.5.x and I'm very happy with that.  Better performance, and more effects than KWin 4 at this point.\n\nCompiz-fusion is much more mature than Beryl."
    author: "T. J. Brumfield"
  - subject: "Upps, less desktop settings."
    date: 2008-01-04
    body: "Hi everybody,\n\nI'm using kde4daily and I noticed that the desktop is unlikely settings. It seems impossible to maintain a similar appearance to the present and, for example, no way to add applets to the Deskbar or create another (like Mac)\n\nAre there plans to improve this aspect?\n\nThanks a lot."
    author: "Jesus R. Acosta"
  - subject: "Re: Upps, less desktop settings."
    date: 2008-01-04
    body: "you can add widgets to the panel by dragging them from the widget chooser to the panel. works well for some, not so much for others. "
    author: "somairotevoli"
  - subject: "Re: Upps, less desktop settings."
    date: 2008-01-04
    body: "It works fine"
    author: "Bobby"
  - subject: "Re: Upps, less desktop settings."
    date: 2008-01-04
    body: "works well for some widgets I meant, some don't scale correctly."
    author: "somairotevoli"
  - subject: "Re: Upps, less desktop settings."
    date: 2008-01-05
    body: "Oh, yes. It Works.\n\nSorry and thanks. 8 years using KDE and I feel like a novice :)"
    author: "Jesus R. Acosta"
  - subject: "Re: Upps, less desktop settings."
    date: 2008-01-05
    body: "Don't be sorry.\nThe discoverability of the feature is extremely low."
    author: "S."
  - subject: "Glimpse"
    date: 2008-01-04
    body: "Hmm, interesting to read aboubt Glimpse and libksane, it was on my todo list for this year to look at something similar, I think Alex Merry had simlar thoughts too (I even wrote a very basic library implementation last year).  So a couple of quick questions for the author (forgive me for not e-mailing, I\u00b4m on holidy in a net cafe).\n\n1) OCR.  Kooka has probably the best OCR interface in FOSS, any plans to include these in Glimpse, or is that too advanced for the Glimpse concept?\n\n2) The use case I most want to see is the following.  I can fit 3 or more photos on my scanner at once.  XSane allows me to batch queue the scans, but the various parameter levels (brightness, contract, aut-levels, etc) are not recorded separately for each selected scan area, instead the same settings are applied to all queued jobs.  It would be great to have Glimpse remember and apply the settings separately.\n\n3) The other use case discussed in the past is a more basic Copier utility, a simple GUI mode that has 3 simple buttons for Save, Copy (i.e. Print), and Fax.\n\nDo you envision libksane replacing libkscan in kdegraphics eventually?  I\u00b4m glad to see it supports more of the SANE features than libkscan.  I\u00b4ll have a look sometime soon and see if I have any other suggestions.\n\nCheers!\n\nJohn.\n\n"
    author: "Odysseus"
  - subject: "Re: Glimpse"
    date: 2008-01-05
    body: "Apparently you do need to email the author ;-)"
    author: "jospoortvliet"
  - subject: "Oxygen again"
    date: 2008-01-05
    body: "Hey, last week I said oxygen theme was geting nice, and one week later, I don't see any remaining glitch from the past. Good work.\n\nUnfortunately, I can't find how to use oxygen in my to many unported yet KDE3 apps, they look uterly alien.\nI just hope next update will bring as much improvements in KDE that it did in oxygen, only litle is missing for me to switch, hope you had time to achieve  it ^^\n\nKDE4.nearly0.0 looks so beatyfull and polished, I can't wait for the very anoying bugs to be squashed so I can reliabely use it"
    author: "kollum"
  - subject: "Re: Oxygen again"
    date: 2008-01-05
    body: "on bealf of the oxygen theme team I say thanks. Its was alot of efort. Alot more polishing will be going on in the next releases. But I must say we are very happy on its current look. \"not perfect but as good as we could get it to be\" "
    author: "nuno pinheiro"
  - subject: "Re: Oxygen again"
    date: 2008-01-07
    body: "For what it is worth, I don't think the existing Oxygen work reflects the true brilliance of your initial mock-ups.  But great work either way!"
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen again"
    date: 2008-01-05
    body: "Yeah, I think it's very awsome. And I think that we have a very good theme here - much better than Plastik from KDE 3.5.\nI really hope that the Oxygen team would spend more time and make it the best theme ever - already now it's pretty awsome :)"
    author: "Kris"
---
In <a href="http://commit-digest.org/issues/2007-12-30/">this week's KDE Commit-Digest</a>: Furious last-minute application of polish across the board in preparation for the tagging of KDE 4.0 Final next week. Work towards threading GDB operations support in <a href="http://www.kdevelop.org/">KDevelop</a>. Support for media players employing the <a href="http://wiki.xmms2.xmms.se/index.php/Media_Player_Interfaces">MPRIS</a> standard in the <a href="http://plasma.kde.org/">Plasma</a> "Now Playing" data engine, with the import of a Flickr Plasmoid. A style manager, support for <a href="http://koffice.kde.org/karbon/">Karbon</a> gradients and lots of colourspace work in <a href="http://www.koffice.org/krita/">Krita</a>. Various improvements in the <a href="http://eigen.tuxfamily.org/">Eigen2</a> math vector library. Continued progress in the KBugBuster rewrite. Revived support for .tar, .tar.gz, and .tar.bz2 files in Ark. More work on KCabinet, a library to support the MS Cabinet format. A printing framework in Okteta. System Settings moves from a custom view to <a href="http://enzosworld.gmxhome.de/">Dolphin</a>'s KCategorizedView. Finishing touches in the <a href="http://oxygen-icons.org/">Oxygen</a> widget style and colour schemes. Work from the "newssl" branch is moved back into kdelibs. Various unfinished features hidden in Konsole for KDE 4.0. The Trolltech <a href="http://phonon.kde.org/">Phonon</a> backends are moved from kdebase to kdereview for KDE 4.0. The unmaintained "regexpeditor" moves from kdeutils to playground/utils. <a href="http://commit-digest.org/issues/2007-12-30/">Read the rest of the Digest here</a>.

<!--break-->
