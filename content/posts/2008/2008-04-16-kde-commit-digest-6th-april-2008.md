---
title: "KDE Commit-Digest for 6th April 2008"
date:    2008-04-16
authors:
  - "dallen"
slug:    kde-commit-digest-6th-april-2008
comments:
  - subject: "1 weeks and 3 days late"
    date: 2008-04-16
    body: "wtf danny? thanks anyway."
    author: "me"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "What have _you_ done to help?"
    author: "Benoit Jacob"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "So he is not allowed to ask for the reason? I'm interested in it, too."
    author: "Erik"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "He is, but there are certainly better manners than starting with \"WTF?\", no?\n"
    author: "comeon"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "\"from the yeah-busy-busy dept.\"\n\nDanny Is Busy And That Is The Fscking Reason.\n\nThanks Danny, nothing else."
    author: "Emil Sedgh"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "And you're the same person? No he's not allowed to ask for the reason, because he's not the freaking CEO of KDE (God frobid such a thing will ever exist) and Danny is *volunteering* some of *his* time and *his* effort to do such awesome  work unmatched in major Free software projects. "
    author: "A KDE advocate"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "> And you're the same person?\nNo.\n\n> No he's not allowed to ask for the reason\nDid Danny allow you to speak for him?\n\n> because he's not the freaking CEO of KDE\nWorst bullshit I ever read.\n\n> Danny is *volunteering* some of *his* time and *his* effort\nSo asking for the reason of the delay is like flaming him? I'm asking again: Did Danny allow you to speak for him?\n\n> to do such awesome work unmatched in major Free software projects\nI also do, and if anyone asks me, I'm going to answer. If someone's about to be blaming me, I'll ignore. That's how communication works."
    author: "Erik"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "Nice to meet you Mr. Unmatched \"LOL\" Erik!"
    author: "A KDE advocate"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "Why do you continue with this destructive BS?  Are you not ashamed of yourself Erik?\n\nIt's obvious to anyone that Danny is busy and the commit digest is a boatload of work, and that sometimes causes delays.  Asking stupid hostile questions is not helping anyone.  Stop being a dumbass and enjoy it when it comes."
    author: "Leo S"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-17
    body: "commit needs to be handed down to someone who has the time to get it out on a more timely manner.  How about since danny can't get it out more timely due to being busy, others put their hands up to do it."
    author: "anon"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-17
    body: "I don't see anybody else volunteering, do you? I only see people complaining about the lateness. (Plus a bunch of people who are grateful despite the lateness)"
    author: "Jonathan Thomas"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-18
    body: "No it fucking doesn't.  KDE doesn't *need* a commit digest -- it's not an essential service, like an SVN server -- it is done out of Danny's spare time -- if he didn't do it, there would very likely be no commit digest.  I am so glad he spends the time working on it even though he's in the middle of exam block.\n\nWe certainly don't do it to please users like you.  Now go use Windows and leave us alone.\n\n/me thinks the dot comment moderation system can't come soon enough."
    author: "Troy Unrau"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-19
    body: "Please. If you want a regular digest, send Danny a big check. Probably around $2000 per week would be reasonable.\n\nOtherwise, enjoy the gift you are given.\n\nDerek"
    author: "dkite"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-17
    body: "> Why do you continue with this destructive BS?\nAsking questions is destructive?\n\n> It's obvious to anyone that Danny is busy and the commit\n> digest is a boatload of work,\nBut he managed that work faster, some time ago. So the question is allowed. Nobody wants to blame him, just ask.\n\n> Stop being a dumbass and enjoy it when it comes.\nAsshole."
    author: "Erik"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-18
    body: "> Asking questions is destructive?\nIs that a rhetorical question?\n\n> But he managed that work faster, some time ago.\nHe must be more busy now than before.\n\n> > Stop being a dumbass and enjoy it when it comes.\n> Asshole.\nPlease no name-calling from either side of any issue.\n"
    author: "Riddle"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-18
    body: "> Is that a rhetorical question?\nYes. Asking questions doesn't imply any motives, so I wonder why it should be destructive.\n\n> He must be more busy now than before.\nYes, maybe. At least speaking for me I've been really interested in the reason (maybe he's got a new job, gets more involved in the KDE project elsewhere, is about to write his diploma, ...), that's why I've been asking for it. I don't imply any laziness and don't want to blame Danny and I don't want to be blamed for asking questions, too. Again, I don't think that the previous poster wrote in Danny's name, so stating the ordinary \"do it yourself/get involved and help/stop being a dumbass\" is a real mess.\n\n> Please no name-calling from either side of any issue.\nBlaming people for asking questions is arrogant and asshole behavior."
    author: "Erik"
  - subject: "Mandatory login pleas"
    date: 2008-04-16
    body: "Pleas add mandatory login on dot comments."
    author: "Anon1234567"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "Don't you guys get tired? \n\nFor dot maintainers, can you please start thinking of replacing the python-based dot application with a PHP- or at least a Ruby-based based one? I think that would open the door for more people to contribute features to bury these trolls down, and count my as one of those potential devs. "
    author: "A KDE advocate"
  - subject: "Re: 1 weeks and 3 days late"
    date: 2008-04-16
    body: "Gaaaah it's late!\n\nIt does make you realize how addictive you're to the digests, doesn't it?\nGreat work as usual Danny, thanks alot."
    author: "Hans"
  - subject: "ARE WE GOING TO GO THROUGH THIS EVERY WEEK?"
    date: 2008-04-19
    body: "Are we going to go through this every week?\n\nYes, it might take Danny some extra time to compile the digest, but stop getting on his case about that.\nBe happy he's doing it.\n\nWhy does it need to be \"up-to date\" anyways? As long as there is a digest every 2 weeks, who cares if it is a bit behind. It's always more recent than last weeks. :p\n\n"
    author: "AD"
  - subject: "Thank you, Danny!"
    date: 2008-04-16
    body: "Hey Danny, I would like to thank you for the excellent commit-digest. I simply love reading it since you have taken over. In the beginning the digest was all I read, now I am reading the planet daily, too. The great thing: the digest is nonetheless very informative and fun to read. You put a lot of work in it and I know many who highly appreciate this.\n\nThank you so much!"
    author: "KDE user"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "+1 "
    author: "mimoune djouallah"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "+1"
    author: "Matt Williams"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "+1"
    author: "Mariano"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "yeah commit digest is pretty nice."
    author: "Stefan Monov"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "+1\nThank you!"
    author: "Vide"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "+1"
    author: "nuno pinheiro"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-16
    body: "Count me in too. Thanks Danny :D"
    author: "Damijan Bec"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-17
    body: "Yet another +1."
    author: "Riddle"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-17
    body: "+1 \nThe dot ist rally cool!"
    author: "Dirk"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-21
    body: "+1"
    author: "Christian Loose"
  - subject: "Thank you"
    date: 2008-04-16
    body: "Great job once again, Danny."
    author: "Fran"
  - subject: "Some KOffice2 questions"
    date: 2008-04-16
    body: "I'm studying computer graphics design in college, and the tools we are learning are quite obviously Photoshop and Illustrator. I would like to know whether or not Krita and Karbon will support these formats (PSD and AI).\n\nI'd also like to know if rotating objects will be possible in Kivio."
    author: "yman"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "Karbon allready has some support of AI in 1.6. Support for PSD is planned for some day. And rotating objects will be possible in Kivio (but Kivio won't be released in 2.0)"
    author: "Cyrille Berger"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "The big problem with .psd and .ai is that Adobe since version 6.0 of Photoshop has closed their SDK. The SDK contains complete specs for their file formats. I have mailed with someone at Adobe who really did his best to get me the most recent Photoshop and Illustrator SDKs -- but ran afoul of Adobe's legal department.\n\nTheir have been some attempts to either reverse engineer the .psd file format or to write a GPL'ed library based on a leaked SDK, though, but not within the KOffice project."
    author: "Boudewijn Rempt"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "Missing .PSD support in open source apps is quite unfortunate, because I often get layouts from external sources where I can't decide which format I'd like to receive. So I can't use Krita or GIMP for editing PSD files because PSD support is quite bad when opening current - say CS2 - files. But I understand why a closed-source binary format is very difficult to support.\n\nWhat I don't understand why I still (April 2008) cannot open simple GIF files in Krita (using Kubuntu here). I've sometimes read that it's a Ubuntu problem but I wouldnt want to change the distro of my PC because of such a simple issue. Another idea I found was to recompile Krita. When there is an update I have to recompile again. This cannot be a solution to simply open a GIF file like the one you find at the top of this page. GIMP can do this as well. I know that PNG is a much better format, but sometimes I have the feeling that there is no understanding at all that you can't always choose which files you want to open.\n"
    author: "Michael"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "We really cannot fix the kubuntu problems for you, I'm afraid. Since I'm not using Kubuntu myself I cannot even check what the problem is, though I suspect a lack of GraphicsMagick. I could replace the GraphicsMagick-based gif filter with filter based on Qt's gif support -- but there again, there's no guarantee that Qt will be compiled with gif support on all distributions. Writing a gif filter ourselves is a third option, but frankly, I've got enough to do already, so we'd need a volunteer for that."
    author: "Boudewijn Rempt"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "\"I suspect a lack of GraphicsMagick.\"\n\nCorrect :) Thanks for the tip."
    author: "Martin Fitzpatrick"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "I'll just repeat the normal; \"don't report bugs on the dot, it will get lost and you will end up frustrated\" line.\nThis is really a known bug and has little to do with KDE. Its Kubuntus issue.\nsee https://bugs.launchpad.net/ubuntu/+source/koffice/+bug/71711"
    author: "Thomas Zander"
  - subject: "Re: Some KOffice2 questions"
    date: 2008-04-16
    body: "Boudewijn,\n\nAbout Exiv2, PSD support (Read only) is implemented in trunk. You can now extract EXIF/IPTC/XMP metadata as well.\n\nThis is a first stage to support PSD in Krita.\n\nI have planed to implement a PSD image loader into digiKam core to be able to support 16 bits color depth PSD image in editor. Of course, this will not include layer support. We will talking about at LGM2008...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "kde app in gnome !"
    date: 2008-04-16
    body: "hi\n\nsuppose i am a ubuntu (gnome) user and i want to install amarok or dragon player, so in order to switch phonon backen i have to install systemsettings thus all the kde-worspace package (plasma and kwin)which obviously nonsense.\n\nis this a packaging problem. or i am missing the obvious.\n\nfriendly   "
    author: "mimoune djouallah"
  - subject: "Re: kde app in gnome !"
    date: 2008-04-16
    body: "the phonon kcm comes with the library in kdelibs, kcmshell is in kdebase-runtime. runtime is required by any kde application to behave well (help browser, essential tools and kcms...). so there is no requirement to install workspace.\n\npersonally i'd like to see integration with the \"native\" control centers on other platforms for our kcms. this seems to be possible on windows, not sure how this will work on macos or gnome (i simply haven't looked =)."
    author: "Aaron Seigo"
  - subject: "Re: kde app in gnome !"
    date: 2008-04-16
    body: "thanks really for the clarification, yeah gnome control center showing phonon kcm is neat, perhaps a freedesktop specification is needed here, \n\n\n\n "
    author: "mimoune djouallah"
  - subject: "Re: kde app in gnome !"
    date: 2008-04-16
    body: "ok i have to say, i did not get it at all, under windows it is as simple as  kcmsell4 \"icons proxy etc\" i never felt as embarrassed as now.\n\n"
    author: "mimoune djouallah"
  - subject: "contentEditable/designMode"
    date: 2008-04-16
    body: "I want to thank KHTML developers for their great work. I have checked KHTML from trunk and I get 73 out 100 in the Acid3 test. Well done. I have question about contentEditable, Is there any progress ? It is in Feature Plan for 4.1. "
    author: "Zayed"
  - subject: "Re: contentEditable/designMode"
    date: 2008-04-16
    body: "I also want to thank them - I was overjoyed when I held down CTRL and moved the scrollwheel intended to just increase the font size, and the whole page zoomed (including images!).  Really neat feature! Virtually every problematic site I've gone to has rendered better in KHTML 4 than in KHTML 3.  Three cheers for the KHTML dudes! Hip hip ..."
    author: "Anon"
  - subject: "Re: contentEditable/designMode"
    date: 2008-04-16
    body: "Wow, I did not know they have implemented this until I read your post. I just tested it on Konqueror/KHTML 4.0.68 and it is really nice. Kudos to the KHTML developers!"
    author: "fred"
  - subject: "Re: contentEditable/designMode"
    date: 2008-04-16
    body: "Wow...! khtml devs ... you're great! can't wait to test it out ;-)"
    author: "Thomas"
  - subject: "Re: contentEditable/designMode"
    date: 2008-04-16
    body: "Ha, me neither! Cool."
    author: "Stefan Monov"
  - subject: "PCMIO?"
    date: 2008-04-16
    body: "What is PCMIO?"
    author: "Richard Van Den Boom"
  - subject: "Re: PCMIO?"
    date: 2008-04-28
    body: "Yeah, that's not clear from the digest.  Presumably though, it's a way to supply a buffer of sound to read/write to the soundcard, instead of just asking it to play a file etc.  Slightly more low-level, or possibly very low-level.  I remember the Phonon guys saying that it WOULDN'T support the kind of very low-level stuff that, say, pro audio apps need, though."
    author: "Lee"
  - subject: "Beginnings of a Windows/WMI backend for Solid. "
    date: 2008-04-16
    body: "\nplease any further explanations ?"
    author: "mimoune djouallah"
  - subject: "Re: Beginnings of a Windows/WMI backend for Solid. "
    date: 2008-04-16
    body: "This could help:\n\nhttp://en.wikipedia.org/wiki/Windows_Management_Instrumentation"
    author: "Stefan Majewsky"
  - subject: "woo!"
    date: 2008-04-16
    body: "\"Integration of Marble into Digikam for geolocation\" - awesome!  I've been waiting for this :-)\n\nHuge thanks for the digest Danny; I don't usually post here but I am pleased to see it every time."
    author: "Mark Williamson"
  - subject: "Thanks"
    date: 2008-04-16
    body: "Impressive, like usual !!! Thanks a lot, Danny. It's always a pleasure to read the digest"
    author: "saphir"
  - subject: "Pleased with QtBrowser"
    date: 2008-04-16
    body: "In the last days, I've been using the Qt 4.4 demo browser, which is WebKit based, and I really liked it.\nIn Debian it is very easy to install, all you have to do is install the qt4-demos packages and execute /usr/lib/qt4/demos/browser/browser\nThis browser has a really fast startup and also it's HTML engine is fast also.\nUnfortunatelly for me Konqueror 4.0.x and 4.0.68 never worked properlly in my setup, so I can't compare directlly, but QtBrowser interface it's really simple and very easy to use, although it's very Safari inspired.\nThis browser has it's limitations, it doesn't support plugins, so no Flash, etc., but all the others things that it's supposed on a modern browser are there, except password management.\nGmail and Google apps are working really fine, the only site which doesn't work for me it's netvibes.com.\nI'm starting to like better the combination of Dolphin and QtBrowser+KDE integration (plugins, kwallet, kget) than Konqueror in it's actual state.\nDon't get me wrong I'm always have been a huge Konqueror fan, but it's user interface hasn't really touched in years and in some ways it feels too complex.\nBut let's see what hapens, Konqueror has a faithful userbase and I can see it still appearing in future releases, even with it's actual incarnation.\nAlthough that I can understand that for some users Dolphin can be a regression, my perception it's that it is really a leap forward and I hope that a new web browser or a renewed Konqueror can do the same in the KDE web browser arena."
    author: "Josep"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-16
    body: "Agreed 100%.\nSorry khtml devs, but webkit is the future. I don't know why khtml developers see this as bad, webkit is basically the enterprise version of khtml and plataform independent.\nAlso, I like dolphin more than konqueror as file manager, it is just plain, simple and good."
    author: "Iuri Fiedoruk"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-16
    body: "I was referreing more about on it's user interface than on it's HTML engine.\nAlthough combining file management and web browsing in Konqueror has some valuable advantages, in it's current form it creates more confusion and complexity which are reflected in it's user interface."
    author: "Josep"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-16
    body: "Disclaimer: I'm not a KHTML developer\n\nI think KHTML devs never see Webkit in KDE as bad, but they still want to develop their very own rendering engine, what's wrong with that? Having alternatives is always good, moreover you can't force people what to do in their free time.\n\nI support Webkit in KDE, but disposing KHTML now is just a plain stupid idea comes from people who does not know how open source works. I'm sorry, but seems you do not really understand how open source works. Saying: dump this because we have better alternative maybe will work in proprietary world. (I also hate when people say that KOffice is a waste of effort because we already have OpenOffice.org)\n\nAs long as KHTML still have developers behind them and still have users, it will *not* die. If finally KDE project has decided to ditch KHTML but the developers still exist, KHTML can be continued outside KDE."
    author: "eds"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-16
    body: "Well, yes and no.\nYou see, a lot of users did not wanted the dying of kicker, but it happened for better (at least we hope so). :)\nBut yes, I'm not saying khtml must die now, but webkit is the future, soon or later it will be one of the dominant engines.\nLook at maemo, qt, safari, iPhone, android and you see a bright future for it. "
    author: "Iuri Fiedoruk"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "\"You see, a lot of users did not wanted the dying of kicker, but it happened for better (at least we hope so). :)\"\n\nI'm sure you're well intentioned and aren't doing this in bad faith, but why, *every* time you post about Plasma, you take shots at it?"
    author: "Luca Beltrame"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "He is a well known strong supporter of old good kicker and Webkit. He doesn't really like plasma (KDE 4.0.x version of plasma to be more precise) and really likes to discredit KHTML ;)"
    author: "eds"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "Yes and no :)\nPlasma 4.0 = trash, crash, poor, less than alfa-quality.\nPlasma 4.1+ = gold, awesome, impressive, shiny.\n\nI was not being sarcastic, I've already asked forgiveness to Aaron Seigo (and can ask how many time it is needed, he can ask me to do so as many times he wish, I don't mind), because I think his *vision* of plasma as substitute to the old desktop is right and plasmoids development will be more easy than creating a cake.\n\nThe problem is that plasma is far from ready now, and I won't even talk about way back when 4.0 was released.\nThe main idea of plasma was having a stable base where developers could create widgets without changes in the code each new kde version and that plasma updates could be distributed indepentent of KDE release cycles. I've asked Aaron, and he said this will happen on 4.1 and afterwards.\n\nSo, long history short: After KDE 4.1 plasma is the king, if they had released kde 4.0 as kde4-developer-edition I would have nothing to complain.\nI just have strong opinions, as Aaron does.\n\nOur last struggle was about removing those crappy mini-icons on plasmoids. He did not liked my (and other opinions) and we kind of crashed, but that does not mean I don't respect and admire him, I just won't help get his ego bigger because a big ego leads to bad decisions ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "Well, kicker died because all the developers have agreed that it must die, and porting all those kicker applets would be just waste of time. In the case of KHTML, none of KDE developers say KHTML shall die ;)\n\nWell, we just see, whether KHTML will die as what you wish or it will still strive as an alternative rendering engine. With the introduction of KHTML in Windows, I'm sure KHTML will get more market share.\n\n(And safari in Windows is not as good as Safari on Mac, I prefer Firefox over Safari in Windows, and if Konqueror has been stabilize on Windows, surely I'll use Konqueror)\n\nAlternative is good, do you know even there are more smaller open source rendering engines which have smaller market share - even maybe less than 0.001% market share? Like Netsurf for RiscOS, which is quite capable despite it is still very young and ultra-lightweightness.\n\n"
    author: "eds"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "> I support Webkit in KDE, but disposing KHTML now is just a plain stupid idea comes from people who does not know how open source works. I'm sorry, but seems you do not really understand how open source works. Saying: dump this because we have better alternative maybe will work in proprietary world. (I also hate when people say that KOffice is a waste of effort because we already have OpenOffice.org)\n\nThis is complete nonsense.  There's nothing wrong or anti-open-source about dropping a branch when a fork is a better option.  It's exactly what happened with GCC/EGCS.  Do you think that the GCC developers don't know how open source works?\n"
    author: "Jim"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "Nonsense, they are not the same. For the case of EGCS, the FSF themselves who officially announced that they officially terminated old branch of GCC and made EGCS the new GCC. For KHTML vs WebKit case, the (current) KHTML devs still do not want to drop KHTML until several issues have been addressed. Note that they are not opposed in this idea, they just want some issues to be addressed first. So in this case, there is no agreement yet.\n\nAnd even in the case of KDE really wants to kick KHTML from official KDE (KDE 5 maybe?), everybody is free to maintain KHTML outside KDE.\n\nReally, nothing wrong with dropping a branch or a fork, but you cannot force your opinion to some people who are willing to maintain it. And why the heck we need to drop a stable and actively developed rendering engine?"
    author: "eds"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "> For the case of EGCS, the FSF themselves who officially announced that they officially terminated old branch of GCC and made EGCS the new GCC. For KHTML vs WebKit case, the (current) KHTML devs still do not want to drop KHTML until several issues have been addressed. Note that they are not opposed in this idea, they just want some issues to be addressed first.\n\nSo why are you accusing luri of not understanding how open source works when he says that \"webkit is the future\"?  He didn't demand that they drop it immediately or anything like that.\n\n> And even in the case of KDE really wants to kick KHTML from official KDE (KDE 5 maybe?), everybody is free to maintain KHTML outside KDE.\n\nYes, just like anybody is free to maintain the old pre-EGCS GCC.  What's your point?\n\n> Really, nothing wrong with dropping a branch or a fork, but you cannot force your opinion to some people who are willing to maintain it.\n\nWho is trying to force anything?\n\nAnd if there's nothing wrong with dropping a branch, then why are you saying that doing so is anti-open source?\n\n> And why the heck we need to drop a stable and actively developed rendering engine?\n\nJosep already posted some good reasons why.\n\nBy definition, you can't drop something that isn't actively developed, so using the fact that it is actively developed as a reason to not drop something is circular logic.\n"
    author: "Jim"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "> He didn't demand that they drop it immediately or anything like that.\n\nHe sure implies they might as well.\n\n> Josep already posted some good reasons why.\n\nHe didn't post good reasons. He posted poor reasons, and he didn't even bother trying the most recent version of Konqueror to compare. Instead he implies it is so buggy he couldn't even get it to work. It works just fine for me, so if he's having problems, he should be filing bugs. \n\n> By definition, you can't drop something that isn't actively developed, so using the fact that it is actively developed as a reason to not drop something is circular logic.\n\nI'd like to point out that khtml is still under active development. Look at the changelogs for the 4.0.x releases. It's also pretty stable: in all the time I've played with 4, I've only crashed Konq once, and once I reported it, it was fixed within the day.\n"
    author: "blauzahl"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "> > He didn't demand that they drop it immediately or anything like that.\n\n> He sure implies they might as well.\n\nNo, he didn't.  Saying something \"is the future\" is *explicitly* making a statement not about the present, but the future.\n\n> > Josep already posted some good reasons why.\n\n> He didn't post good reasons.\n\nFast startup, fast rendering, really simple, very easy to use... they sound like decent reasons to me.\n\n> he didn't even bother trying the most recent version of Konqueror to compare.\n\nWould that be the version of Konqueror tied to the version of KDE that end-users have been told *time and time again* to stay away from because it isn't finished yet?  Are you surprised that people are actually taking this advice?\n\n> if he's having problems, he should be filing bugs.\n\nNo, if he's having problems, then that justifies his preference for another browser.  You can't blame users for choosing a more stable competitor instead of helping you track down bugs.\n\n> I'd like to point out that khtml is still under active development.\n\nI wasn't saying otherwise.  I was pointing out that isn't a reason not to drop it.\n"
    author: "Jim"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-16
    body: "disagree 100%\n\nI remember a few years ago the exact same comments being made about khtml with regard to gecko. If the khtml developers had just said 'gecko is the future' would there even be a webkit today? \n\nso I for one am glad developers continue to provide alternatives and don't take the easy way out and just say 'why bother there is already program XYZ that does that?'\n\ndr.  "
    author: "dr"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "khtml developers didn't go for gecko because it was too messy to make it work with KDE although they tried to integrate it so they could get rid of KHTML. Now that webkit is there, most KHTML original developers have switched to it because it is more supported than khtml and easy to integrate in KDE."
    author: "Patcito"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-17
    body: "They switched because they are paid to work on webkit."
    author: "anon"
  - subject: "khtml"
    date: 2008-04-17
    body: "You're assuming that khtml developers are actually opposed to to webcore. It ends up being a lot more complicated than that, and it doesn't help that they don't tend to say much, preferring instead to code.\n\nBut I think you'd want a proper solution where we don't lose KDE features, such as the kwallet integration, spell checking in form widgets, and our public DOM api (without which applications such as Kopete cannot function).\n\nDevelopers want to be able to make new releases, with features and bugfixes at the same time as the rest of KDE, and not wait for Apple to make a release first, then wait 6 - 9 months for Trolltech to release a new version of QtWebKit based on Apple's latest version (and then you wait longer for a KDE release that uses it).\n\nWhile we're at it, QtWebkit is a port of Apple's WebKit. It isn't bug-for-bug compatible. Webkit doesn't come with any graphics or networking or rendering code, all of that is written in the port. Currently QtWebKit implements only a subset of Safari's features. So you lose various features that currently exist in khtml. \n\nIf you change the user agent, you too can make konqueror work with gmail. That is what QtBrowser is currently doing; it lies and says it is Safari. If we did this for all websites by default, then nobody would know that anyone was using Konqueror. It wouldn't show up in their webserver statistics. Which is better? To work with sites that assume there is no browser other than Firefox or perhaps Explorer? Or to prove that we exist? \n\nDisclaimer: I'm not a khtml developer, but I'm also not telling lies. And if I am, I'm sure the actual developers will correct me. "
    author: "blauzahl"
  - subject: "Re: khtml"
    date: 2008-04-17
    body: "+1.\n... no, make it +100000000000000\n"
    author: "Richard Van Den Boom"
  - subject: "Re: khtml"
    date: 2008-04-18
    body: "Thank you for this post.  I am so tired of hearing that webkit is a panacea.  It's controlled by an outside entity, and to get to KDE has to go through another outside entity.  There will be lead time for each new version.  \n\nThere will be issues integrating it into KDE.  Anyone who thinks webkit can just be dropped into Qt, much less KDE should read this: http://zecke.blogspot.com/2008/01/joys-of-debugging-webkit.html  \n\nI don't link to that to somehow try to prove that webkit in KDE won't work, it's just to show that there are issues getting webkit to work on any platform where it isn't native (pretty much anything but OSX).  It's a lot of work getting it working in Qt, never mind KDE.\n\nAnd yeah, in so many cases it seems like people want the browser string.  Thing is, those people don't need a new rendering engine to get the browser string they want.  I wish they'd just change it in their copy of Konqueror and stop agitating for webkit.  If webkit proves to be superior (including how easy it is to get working and current in KDE) then it will happen, with or without boosterism on the dot."
    author: "MamiyaOtaru"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-18
    body: "\"like Dolphin better\"\n\nThat's fine, but I don't appreciate calls to remove Konqueror's file browsing functionality as long as Dolphin's dirtree sidepanel isn't up to snuff.  Running the latest Dolphin now I see I can now copy and paste and stuff using the dirtree (yay) but it still doesn't show the root directory, and there's no way to change the dirtree's root.  Konqueror's much maligned vertical tabs provide a way to switch between / as dirtree root and ~ as dirtree root.  \n\nI can live without that of course, but not showing the current root at all still irks me greatly.  Combine that with edit mode (instead of navigate) in the address bar and lack of an 'up' button (which people wanted removed from konqueror for years for some reason) and there's no easy way to get to the dirtree's root.  \n\nIt's coming along nicely though, a lot of stuff I had to complain about regarding Dolphin's dirtree implementation are no longer applicable, as I saw when I installed it from debian experimental.  So that's nice.  \n\nAt any rate, I quite like Nautilus' dirtree.  Showing multiple roots (/, ~, /mnt/X, /mnt/Y, etc) at once is quite nice.  Something like that would be grand for Dolphin.  \n\nAt any rate, the changes I'm seeing are making me feel a lot better about KDE4."
    author: "MamiyaOtaru"
  - subject: "Re: Pleased with QtBrowser"
    date: 2008-04-18
    body: "Aaand I see I can add the up button back.  yay.  Would still love to see the root in the dirtree, but hey.  That's what I get for using edit mode instead of the breadcrumb :-/"
    author: "MamiyaOtaru"
  - subject: "KDE3+KDE4"
    date: 2008-04-16
    body: "I was using KDE4 (4.0.3) on Ubuntu, but recently came back to KDE3 because Plasma was sooooooooo instable I could not live with it. \nSimply put, as a desktop 4.0 is not there yet (by far) but I have big hopes for 4.1. On the other side, KDE4 apps in general are really great, so in the end I'm running a mix of KDE3 and KDE4, here goes my impressions:\n- kopete: really bad bugs (can't use google talk with transports without crashing), but some neat improvements\n- krdc: this app in  KDE3 was really not good, in 4 it went into a overhalt and now it's almost better than windows remote desktop client (it still wins some points because of unit drivers and clipboard integration)\n- dolphin: the one in kde4 have more options, seems faster and more responsive than the kde3 version\n- gwenview: too unstable yet, crashes more often than the kde3 version, both need some bug-hunting, I'll try to help where I can next time it crashes on me\n- general-style: even that I use the same style in both kde 3 and 4, the last one seems to be nicer and uses less spacing. But if you have a small screen/resolution avoid oxygen. I recommend Polyester.\n- fonts: in both cases to get the look I like, it's better to download microsoft core fonts and user verdana-9 everywhere.\n- kcontrol/systemsettings: systerm settings is really great, but most of kcmshell modules where designed to fit kde3 overall look (like the left-side tabs/buttons). I think they could be changed in on extra level in the systemsettings, but overall a great improvement over kcontrol.\n- konqueror: I still like webkit most (not only because I think is technaly better, but because it's more used and have big-players support), but khtml improvements are really nice and visible. But nspluginviewer crashes all times, so youtube is a no-go for me.\n\nSo if you are avoiding KDE4 because it's unstable, my suggestion is to run it's apps inside kde3 or gnome to have a glimpse of the improvements that where made into out favorite apps."
    author: "Iuri Fiedoruk"
  - subject: "KDE forums need attention!"
    date: 2008-04-16
    body: "This is off-topic, but could a site admin. please delete the massive amount of spam in the KDE forums?  The forums have been literally drowning in spam for several weeks now!  It's not the impression KDE should give to new users or those seeking assistance.\n\nThanks."
    author: "Spam killer"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-17
    body: "agree, the forums really are not worth using at present, but then again, they were never that active anyway, so perhaps they are just letting the forums die.  It's a shame because as the KDE user base grows with a release to windows, there will be many people looking for support that they can't get at a distro forum"
    author: "anon"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-17
    body: "Is it bad that help requests for spam deleting were the first I've heard of a KDE board? :S"
    author: "Jonathan Thomas"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-17
    body: "Haha yeah me too.  I had no idea there were any KDE forums, and I read all the KDE related news pretty much every day."
    author: "Leo S"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-17
    body: "i already posted in the last commit about the invasion of spammers but no result, even i sent a message to Seb, ' i think he thought my email is a spam "
    author: "mimoune djouallah"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-17
    body: "It's a mystery why the Forum isn't linked directly from the Dot.\nIt's linked from http://www.kde.org/"
    author: "reihal"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-18
    body: "There was a dot story on it way back when the forums re-launched, but they never got much attention."
    author: "Adriaan de Groot"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-18
    body: "The forums never grew very large -- they're built and run outside the \"normal\" KDE community and developer attention to forums is very low. I'm one of the forum admins, which means I look at it every six months or so -- there's just not much incentive for me.\n\nWhat the forum could really use is an injection of (a) energetic forum moderators (b) some clueful people to help out in all those threads."
    author: "Adriaan de Groot"
  - subject: "Re: KDE forums need attention!"
    date: 2008-04-18
    body: "Not sure if it was you, but a big thanks to whoever cleaned the forums!\n\nI agree the forums are slow and underutilized at present.  Part of the problem is the partial redundancy in function between posts on the dot and the KDE forums.  The whole point of the forum is to share and comment on KDE ideas or news.  The dot subverts this purpose to a certain extent by allowing unregistered people to post directly to the story.  This is not to say the dot causes the slow forum, but it does contribute to it.  \n\nWe (KDE) could mitigate this problem by changing the way users post to the dot.  Imagine if you wanted to comment on a dot story that you'd have to go to the forums and use your registered account.  Each dot article could easily have a link with something like \"discuss this article in the KDE forums.\"  A lot of devs, users, and potential users follow and comment on the dot and there is no reason to think they'd stop posting just because they'd have to go to a forum.  That alone would increase traffic and it might encourage devs/users to wander around while they're in the forum.  It'd also cut down on the morons who post unsupported, inflammatory comments about the KDE project."
    author: "Spam Killer"
---
In <a href="http://commit-digest.org/issues/2008-04-06/">this week's KDE Commit-Digest</a>: General improvements in <a href="http://en.opensuse.org/Kickoff">Kickoff</a>, KRunner, and assorted <a href="http://plasma.kde.org/">Plasma</a> applets. Integration of <a href="http://edu.kde.org/marble/">Marble</a> into <a href="http://www.digikam.org/">Digikam</a> for geolocation of photos. Configuration of fullscreen mode in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a> fully passes <a href="http://disruptive-innovations.com/zoo/css3tests/selectorTest.html#target">"selector" test</a>. An automation GUI for KLinkStatus. A database connection plugin for the <a href="http://kommander.kdewebdev.org/">Kommander</a> scripting framework. Tutorials and examples added to <a href="http://stepcore.sourceforge.net/">Step</a>, which moves from kdereview to kdeedu. More maps for <a href="http://edu.kde.org/kgeography/">KGeography</a>. Various enhancements in the new KBlocks game, which moves to from playground/games to kdereview. Get Hot New Stuff becomes functional in the now-feature-complete KDiamond game. Various work in <a href="http://kate-editor.org/">Kate</a>. Improved file tagging mechanics in <a href="http://enzosworld.gmxhome.de/">Dolphin</a>. Improvements in context menu sharing between <a href="http://www.konqueror.org/">Konqueror</a> and Dolphin. Beginnings of a Windows/WMI backend for <a href="http://solid.kde.org/">Solid</a>. Work to integrate PcmIO into <a href="http://phonon.kde.org/">Phonon</a>. Improved hyperlink creation support in composition across <a href="http://www.kontact.org/">KDE-PIM</a>. Work on user data/statistics migration issues between <a href="http://amarok.kde.org/">Amarok</a> 1.x and the upcoming version 2.0. Group policies in <a href="http://ktorrent.org/">KTorrent</a>. The Glimpse scanning application is renamed "Skanlite". Redesigns and refactoring in <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. Okteta moves to kdereview. <a href="http://commit-digest.org/issues/2008-04-06/">Read the rest of the Digest here</a>.

<!--break-->
