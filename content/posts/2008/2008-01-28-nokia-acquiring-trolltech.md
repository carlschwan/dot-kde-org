---
title: "Nokia Acquiring Trolltech"
date:    2008-01-28
authors:
  - "aseigo"
slug:    nokia-acquiring-trolltech
comments:
  - subject: "Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "Some question if FreeQt can work depending on what Nokia does.\nThis guy suggests (http://insanecoding.blogspot.com/2008/01/say-goodbye-to-former-qt-and-hello-to.html) that FreeQt won't do anything if the open source version becomes  Windows only. \n\nPersonally, I think Nokia bought Trolltech to prevent other companies from using QTopia on their phones, and really don't care about advancing Qt at all. They will probably also inevitably end up doing something to hurt us."
    author: "Nach"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "well, yes, that blog is what you get when a developer comments on legal issues and does so before having all the facts in hand. it's a completely bogus analysis.\n\nas for motivation, it may make more sense to consider the benefits to Nokia rather than the detriments to others. why? because that's what usually motivates such companies.\n\nit's business basics."
    author: "Aaron Seigo"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "Which facts was he missing?\n\nWell yeah, perhaps Nokia may want to use QTopia on their phones too, although they're currently not doing that. But in the process, it's probably good for their business to be the only one on the block with a stellar UI.\n\nAnd heck, now that KDE Games have gone SVG, they'd look nice on their new phones :)"
    author: "Nach"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "There's the fact Google's Android is a threat to Nokia, and so it makes sense for Nokia to embrace a similarly open source platform.  Besides, phones are quickly becoming OS's, and OS/software vendors like Microsoft are quickly becoming phone developers.  There are plenty of reasons for them to want to pickup things like Qt/KDE (with all their accomplishments, cross-platform support, nicer-(and, importantly, LIGHTER)-than-vista UI, etc.\n\nBelieve me, I've seen companies shoot themselves and their products in the foot enough to consider it a crime against the advancement of humanity, but it could be a good thing too.  Time -- and only time -- will tell."
    author: "Lee"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "Personal question, if I may: will you and e.g. David keep your jobs?"
    author: "Anon"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "Of course they will. The open letter to the community that Lars points to in his blog explains the situation.\nIn short Trolltech commitment to KDE stays completely unchanged. Look at it from the very simple perspective: the amount of resources which Trolltech has at their disposal increases a lot due to the merger and that's something to be excited about."
    author: "Zack Rusin"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "...for the time being.\n\nWe don't know what Nokia will feel like in a few years.\n\nI'm not happy about this. It's one thing to cooperate with a small company composed of engineers, programmers, hackers and software lovers, and a completely different thing to have the very heart of your desktop environment controlled by a multinational behemoth involved with software, telecommunications and a million other things, which started off as a cable manufacturer.\n\nI had a far easier time trusting Trolltech than I do trusting Nokia. While things will probably stay pretty much the same in the short term (or even improve due to new funds), in the long run I'm not sure Nokia will care enough about Free Software or KDE to stop them from hurting the community if they deem it profitable.\n\nI'm not doing this to spread the FUD, I'm simply uneasy about this."
    author: "KOffice fan"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "well, I might add that small companies \"composed of engineers, programmers, hackers and software lovers\" usually aren't around for long. Those companies tend to go out of business quickly or become acquired by a \"multinational behemoth\". If they're lucky.\n\nBut still, I think you're mostly correct about the implications for KDE. We're no longer close to the Qt business model. KDE had a good leverage versus Trolltech because they knew well to thrive on KDEs success on the desktop. \"Look how fancy things are done with our product\". I wonder if Nokia cares so much...\n\nAlso I'm wondering whether the possibility exists of Nokia BSD'ing (or LGPL'ing) Qt, and whether this was good or bad. Clearly they don't need the turnover Trolltech achieves through Qt license sales.\n"
    author: "matze"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-02-01
    body: "Well, companies have the obligation to bring revenues to their share holders. Both big and small companies. So I don't understand this romantic view of small company composed of engineers and SW lovers... if you don't want a company to control your desktop (I don't see how Nokia could do so), use a Desktop that is not based on any technology owned by any company (I don't know if that is the case with Gnome).\n\nNokia started by doing paper and rubber boots. I don't think it is easy to find a company that reinvent itself as Nokia does. I just think this is positive, specially when thinking about the resources and the market penetration nokia has."
    author: "Jos\u00e9 Alarc\u00f3n"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "so let's consider the facts:\n\n1.) Nokia hat a net profit of 2.8 billion euros in q4 2007; TT isn't profitable at this point (tho the turnover increases).\n\n2.) Trolltech was a device-independent embedded software vendor iirc selling to Motorola as well.\n\n(1) was clearly not the reason for the acquisition, (2) probably much more so. what will probably happen is that TT gets aligned with the Nokia business operations and then everything which is not up to the efficiency threshold (sales people might be affected) will get cut away.\n\nthis way, Nokia has an in-hour alternative to Symbian, keeps Motorola from benefitting from TT (or acquiring the company themselves) and a nice staff of engineers who can be assigned to whatever software development they do inhouse. and that's prolly going to be quite a bit in the future (since the profit margins with hardware are much much smaller than with software).\n\n16NOK per share is pretty much (i'd need to look it up) the issue price btw, it's not like you're gonna make big money with that."
    author: "anony-mouse"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\nThis is a good observation.  Most companies do not like to use a product developed by a competitor.  This means that unless Nokia changes the license (to permit free commercial development by its competitors), Qt is quite unlikely to be used by other phone manufacturers."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\"Qt is quite unlikely to be used by other phone manufacturers\"\n\nI can't think of any instance where getting your competitors to use your platform is not a good thing."
    author: "Segedunum"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "And what about using your competitors platform?"
    author: "John Tapsell"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "I see plenty of Microsoft's competitors using Windows."
    author: "Segedunum"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\nYou miss the point - it's not in the interests of other manufacturers to use Nokia's toolkit.  Simple example:  what if Ericsson needs a change to Qt to work on its new device?  They have to go rely on their competitor to make the change to help them, which of course Nokia has every motivation not to do - and, on top of that, risk pre-notifying their competitor of their new device's feature in order to get the change, giving up a major competitive advantage (first to market).  Smart management just does not put themselves in that position."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "One word: Android. "
    author: "A KDE advocate"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\"They have to go rely on their competitor to make the change to help them\"\n\nWhy?"
    author: "Anon"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\nIn the hypothetical:  assuming Ericsson does not want to open source its drivers and all its software, it has to use the commercial version of Qt / Qtopia, which means it can't make its own changes, which means it has to ask Nokia to make the changes for it."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "> it has to use the commercial version of Qt / Qtopia, which means it can't make its own changes\n\nThis is a wrong implication. A Trolltech customer, i.e. someone who has bought a commercial licence, still gets the Qt code and is allowed to change it and distribute alongside their applications.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\nSince I don't have (and Trolltech for some reason does not publish) their commercial license I cannot confirm your summary or whether it would be adequate.  One inadequacy, e.g., is that this license might change, including that the license fees might go up dramatically or this right you refer to to make changes goes away.  And as I have noted before, there is no secondary market in Qt licenses (e.g., buying an off-the-shelf kit at Best Buy), which enables Nokia to price discriminate (charge some customers more than others, or plain refuse to license to someone).  All very unattractive features when deciding on the platform for a hardware device.\n\nCan you post your Qt Commercial License somewhere and provide a link?"
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-29
    body: "Section 6 of my QTopia license states: \n\n(a) modity the licensed Software as limited by section 10 below.    \n\nSection 10 seems to deal only with trolltech's rights to audit out process to ensure we have the correct licenses, but it does have the following statement: Licensee does not implicitly grant Trolltech any form of license agreement.\n\nI won't post the full agreement, it is likely protected by copyright laws.   I'm pretty sure my company doesn't intend to modify QT, so I assume the above is generic terms they grant to everyone.\n\nI can't tell you how/if Nokia will change those terms."
    author: "Hank Miller"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\"You miss the point - it's not in the interests of other manufacturers to use Nokia's toolkit. Simple example: what if Ericsson needs a change to Qt to work on its new device? They have to go rely on their competitor to make the change to help them\"\n\nFor starters, Qt is an open source platform, and secondly, I see few of Microsoft's competitors having a problem with Windows - and that's even worse."
    author: "Segedunum"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\nQt may be open source but we are speaking about the commercial license.\n\nAs to Microsoft, at this point there is little choice for most people, but it's not at all analogous.  A better analogy:  Intel relying on AMD to write its microcode for its processors."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-29
    body: "\"Qt may be open source but we are speaking about the commercial license.\"\n\nSo what? What is in Qt is what is in Qt. The open source version has the enterprise source code, and if you're a customer then you get the Qt source code as well.\n\n\"As to Microsoft, at this point there is little choice for most people, but it's not at all analogous.\"\n\nYes it is - certainly from what you're implying. These days Microsoft's competitors have no choice but to write software for Microsoft's platform and with their development tools, which means that Microsoft is first to market with everything. With Qt, that just isn't the case. If someone is really worried then they can use Qt for some things and not for others.\n\n\"A better analogy: Intel relying on AMD to write its microcode for its processors.\"\n\nAs long as they can get control over that microcode and write their own then it's not a problem and a pretty poor example."
    author: "Segedunum"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-02-01
    body: "What?\n\nLG Electronics\n  \t\t\u0093We are anticipating the unique features and functionalities that the S60 platform will bring to our product portfolio and look forward to working in the S60 community. Our target is to build ubiquitous mobile devices that will best serve our customers. S60 on top of the Symbian OS will provide us with a compatible platform for our smartphones throughout our CDMA, GSM/GPRS, and WCDMA portfolio.\u0094 \u0097 Yong E. Shin, Vice President, UMTS Handset Laboratory, LG Electronics\n\nRead more about LG at www.s60.com\nLenovo\n\t\t\u0093The S60 platform, as an open and industry interoperable platform, best supports our innovations and product differentiation. It will provide Lenovo with one of the most optimal software platforms in the very competitive Chinese handset market.\u0094\n\u0097 George He, Senior Vice President and Chief Technology Officer, Lenovo\n\nRead more about Lenovo at www.s60.com\nLenovo developer Web site\nSamsung\n  \t\t\u0093S60 is an ideal software platform for the advanced smartphones.\u0094\n\u0097 ByungDuck Cho, Senior Vice President, Mobile Communications R & D Team, Samsung Electronics\n\nRead more about Samsung at www.s60.com"
    author: "Jos\u00e9 Alarc\u00f3n"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "You're right: at IPO the price was fixed at NOK16/share. Which means that Nokia gets to acquire them at a bargain price now. And still its a generous offer, the shares were publicly traded at around NOK8 before Nokias stepped up."
    author: "matze"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: ">>  TT isn't profitable at this point (tho the turnover increases).\n\nWhat?  TT is making a tidy profit. Not 2.8billion worth, but a decent amount."
    author: "Leo S"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "i've been following TT since their IPO and they haven't had a single financial report which stated that they were profitable. their turnover increased well (which is usually what counts) but still.\n\nyou can see it yourself in the q3 2007 report at \n\nhttp://dist.trolltech.com/pdf/Q3finalreport2007.pdf\n\n- 'The loss before tax was NOK 4.6 million in the third quarter 2007'\n- 'For the first nine months of 2007, the loss before tax was NOK 26.1 million and the net loss after tax NOK 31.0 million'\n\ni think it's crystal clear that this is a purely strategic acquisition and whoever says that TT can just keep going on as they always did except that they have a lot more resources plainly doesn't understand how businesses work at this scale (thinking about the comment of 'zack rusin').\n\nTT is interesting because of a nice engineering division but after the deal it's entirely up to Nokia what those people are working on. if Nokia decides that the manpower is more useful to them when working on embedded stuff instead of KDE stuff, then that's the last word.\n\nsome people never understand when it's time to drop the romantic hacker mentality and realize what it means to be working for (and adjusting to the preferences of) a highly profitable and very tough global enterprise like Nokia."
    author: "anony-mouse"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "I might be mistaken, but I vaguely recall TT being profitable pre-IPO. It certainly wouldn't be surprising to see a company that went public push for expansion and growth over immediate profits, given the infusion of capital the  IPO produces.."
    author: "SadEagle"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "yes, acquisitions, restructurings and hiring is a bitch to the bottom line when using GAAP methods."
    author: "Aaron Seigo"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-29
    body: "Huh.  You're right.  What the heck.  I wonder why I thought that they were making a profit.  That's... disturbing.  When they were privately owned they must have made money, and lately they seem to be spending more and more, so I assumed they were in decent shape."
    author: "Leo S"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-29
    body: "The stock exchange Q3 report also says: Positive EBITDA[1] of NOK 0.3 million. Sales has increased in the period, the report says. This shows that Trolltech is heading for profitability after a period of heavy investment. \n\n1. http://en.wikipedia.org/wiki/EBITDA\n\nWhen it comes to your \"romantic hacker mentality\" theory, science says something else. For years now researchers has studied the phenomena of free, libre and open source software. One such study is the Free/Libre and Open Source Software: Survey and Study. The study reports: \n\nLearn and develop new skills, and share knowledge and skills are major motivations for free software developers. The study[2] shows that the free software community is a rather young and predominantly male community with a strong professional background in the IT sector and a high educational level. \n\n2. http://www.infonomics.nl/FLOSS/report/Final4.htm\n\nTrolltech's relation to KDE are well known. At the KDE 4 launch Haavard Nord presented how the relationship was from Trolltech's part: \n\n3. http://video.google.com/videoplay?docid=2866967248993401461\n\nFinally, Nokia embraces open source technology and will take further the open source development culture found in Trolltech.  FreeQt foundation is being maintained to guarantee the continued freedom of the toolkit KDE depends upon. Chief Trolls and Nokia VP are now asking for ideas and comments on improving their relationship with the open source community. "
    author: "Knut Yrvin"
  - subject: "Smoke and Mirrors?"
    date: 2008-01-29
    body: "\"Finally, Nokia embraces open source technology\"\n\nNews to Maemo developers, I'm sure.\n\n\"Chief Trolls and Nokia VP are now asking for ideas and comments on improving their relationship with the open source community.\"\n\nLet's take a look at the first item on the agenda:\n\nhttp://www.google.com/search?q=Nokia+software+patents"
    author: "The Badger"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\"Insane Coder\" wrote:  \"So Nokia could continue to develop Qt for their cellphones, and for customers such as Adobe, and even release Windows only, or perhaps even Windows and Mac OS X builds of Qt both proprietary and under the GPL, but none for X11.\"  This is the foundation of the critique in the blog post.\n\nHowever there is a fundamental error here.  The Agreement defines Qt as being Qt on the \"KDE Window System\" (a defined term), which is designated as the X Window System (X).  Note that the agreement does not concern itself at all with Qt on Windows, Mac or the Linux frame buffer (except indirectly:  a GPL Qt on X can be ported to these other platforms).  So when you read \"Qt\" in the agreement, read it as \"Qt on the X Window System\" (the agreement is not just for Linux, it's for all U*xes).\n\nSo if Trolltech (or its successor) does not release an \"Important Release\" of Qt for X every year, or releases a non-free version of Qt on X without an equivalent free version within a year, the Foundation will have the right to release the latest release under BSD or other open source licenses."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "In this case, then, they could still for whatever reason stop releasing Qt for Mac OS X as open source, or at all, and nothing FreeQt can do, right?\nSo what happens to the new Mac port of KDE?\n\nIs it perhaps time to update that agreement to cover all the systems that KDE now runs on?"
    author: "Nach"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\nYou forget it's open source.  It's already been ported to Mac OS X - so the great bulk of the work has been done, and the free software community could maintain the Mac OS (or Windows, or framebuffer) ports."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "..and you forget that they can change the license to whatever they want\nfor future versions and if you think the FREE SOFTWARE community would\nmaintain something for a proprietary platform you have to be delusional.\n\nIf you want to maintain it yourself just go ahead."
    author: "alzheimer"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "There was a GPL Qt3 port to Windows. Basically the Qt framework is obviously modular to allow it to be ported to other platforms. So if you have Qt/X11, it's not too much work to make Qt/Mac or Qt/Windows.\n\nIn reality Nokia is a hardware company not a software company, so I see them becoming even more free about handing out licenses to Qt."
    author: "Ian Monroe"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "> So if you have Qt/X11, it's not too much work to make Qt/Mac or Qt/Windows.\nOh it's much work alright. A lot work actually\n\n> ...so I see them becoming even more free about handing out licenses to Qt.\nOkay... I don't."
    author: "alzheimer"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "I personally tried out the Qt3 port to Windows from X11, it was pretty bad, had all kinds of obscure bugs all over the place.\n\nFor example, launch another application with QProcess, and watch not being able to control the child process properly on Windows, and having memory leak all over the place."
    author: "Nach"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "\n\"you forget that they can change the license to whatever they want\"\n\nActually, what I remembered is the following: (i) Qt for X is GPL and, due to the FreeQt agreement, has to remain that way; (ii) ports to Mac, Windows and FrameBuffer have been released under GPL; and (iii) even if these ports are no longer released under GPL in the future - i.e. the license on the other platforms changes - the past ports can be maintained (so you don't have to start from scratch)."
    author: "Sage"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "The thing is that you/we/KDE have/has to maintain it which is a huge effort and will _at least_ slow development down immensely. It's not like the guys at TT will quit their jobs to work on the free versions of Qt.\n\nLet's wait and see. It's probably not going to happen today (if at all)."
    author: "alzheimer"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "Nokia did this move to have an answer to the Google phone I think. Therefore they need Qt and also Qtopia as a free and open alternative. Maybe they need to open it up even more, like under LGPL or BSD license.\n\nWhat I personally may not even like, since the GPL is the best license to get further parties to contribute ...."
    author: "Adrian"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: ">What I personally may not even like, since the GPL is the best license to get further parties to contribute ....\n\nNo. Even the kdelibs are lgpl and KDE is not lagging attraction.\nLibs -> LGPL\nApps -> GPL"
    author: "Philipp"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "however, KDE isn't trying to make money off its libraries."
    author: "Chani"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "You're missing a part of the picture here. Having Qt under the GPL is part of the business model which drives Qt development. Putting it under the LGPL would actually make things worse.\n\nConsider the following:\n1. release a beta version of a GPL toolkit\n2. receive tons of feedback from KDE developers\n3. process feedback to produce an enterprise-quality toolkit\n4. release the final product, and commercial version\n5. profit!\n6. use the income to fund development and KDE developers\n7. goto 1.\n\nThe model above drives the development of Qt, and has some interesting side effects:\n- it's an upward spiral!\n- Trolltech does lot of the heavy lifting, since Qt is their primary income. \n  They also do boring stuff developers wouldn't like to do unless they get paid.\n- KDE gets an enterprise-quality toolkit in return.\n- Trolltech is able to reach more customers and fund some KDE developers.\n\nAlso note the CEO of Trolltech explained recently how Trolltech became succesful because of KDE, not despite KDE. Customers are confident Qt is a good product since it's used to build a complete desktop environment, and often hear about Qt from KDE.\n\nPutting Qt under the LGPL would break this model. It would hurt KDE too. Customers have fewer reasons to buy the commercial version, and there will be less income to fund development."
    author: "Diederik van der Boor"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "Releasing Qt under under BSD or at least LGPL license would lead to massive spread of Qt which in turn would lead to improving Qt."
    author: "Budzes"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: "I'm not sure I'd agree with that. who exactly are you expecting to do the improving?\n\nit would certainly lead to less money going to trolltech, which increases the risk of some talented developers finding themselves out of a job and having to go work on something other than qt/kde to pay the bills."
    author: "Chani"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-29
    body: "Motorola, IBM, and KDE develops.   This depends on where Nokia wants to go with QT.  If they just want to kill Google's (and Apple's) entry into the cell phone market, but don't want to get into monopoly trouble they may want to make QT open source in a BSD type license.  In this case they may want to leave just enough Motorola around that regulators won't come looking at them.   IF they go this route they may get rid of most of their qt developers (there will never be a qt5), keeping just those who work on parts they need for internal projects.\n\nI can come up with lots of alternatives that they CAN do.   I can't tell you what they will do.  So long as I can use qt4 in my comercial developments I don't care."
    author: "Hank Miller"
  - subject: "Re: Will the FreeQt foundation actually work?"
    date: 2008-01-28
    body: ">Having Qt under the GPL is part of the business model which drives Qt development.\n\nSomehow I don't think TT business model is terribly important to Nokia.\n\nCheers,\nCC"
    author: "Carlos Cesar"
  - subject: "so, the million dollar question.."
    date: 2008-01-28
    body: "If this was done to..\na) to get a better product than gtk+/maemo and to actively develop it, or\nb) to wipe out competition?\n\nHmm, Nokia to unify Linux userspace?"
    author: "jmk"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-28
    body: "\"a) to get a better product than gtk+/maemo\"\n\nNokia's development is Symbian. GTK/Maemo is irrelevant really."
    author: "Segedunum"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-28
    body: "Perhaps they're planning to port Qt to Symbian? Symbian (IIRC) has several different UIs available to it already, and perhaps Nokia thinks that Qt is much nicer than any of the others."
    author: "Apollo Creed"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-28
    body: "They would have been absolutely free to do this without the huge legal, administrative and social headache that comes from acquiring another company.  \n\nI'm really not seeing a single advantage to Nokia here unless they want to stifle development of Qt.  The only thing I can think of is that they do indeed want to use Qt (remember that they expressed dissatisfaction with the development of GTK? - http://www.osnews.com/story/18444/Nokia-Pushes-for-GTK+-3.0/) and will license Qt for development of apps intended to run on *Nokia* devices to commercial parties *for free*, or maybe a heavily discounted rate, and thus obtain a top-notch toolkit that has the number one strike against it (the need for closed-source software writers to pay a fairly hefty fee) removed, or at least mitigated.  Money would continue to flow into Trolltech from the Windows app-writers side.  \n\nI'm sure though that even if this is what they are planning, there would have been easier ways to accomplish this than outright purchasing Trolltech - brokering some kind of special deal with TT, perhaps - so again, I really question what they have to gain, here.  \n\nOn the plus side, TT are neither stupid/ naive nor desperate for cash, so there is presumably a good case for embarking on this path.\n\nWe'll have to wait and see, I guess :/"
    author: "Anon"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-28
    body: "> unless they want to stifle development of Qt.\n\nthat has not been my understanding of the strategy.\n\n> The only thing I can think of is that they do indeed want to use Qt\n\nthis is closer to it."
    author: "Aaron Seigo"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-29
    body: "\"I'm really not seeing a single advantage to Nokia here unless they want to stifle development of Qt\"\n\nI don't see how that would be an advantage. TT is doing nothing to hurt Nokia right now."
    author: "Segedunum"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-29
    body: "Greenphone.  I know it is a non-issue today.  However if Nokia sees derivitives as a future problem (compitition) they may want to buy TT now, just to make sure the greenphone never is a problem.   \n\nAs I've said elsewhere, I can come up with many different explinations.   I can't tell you which are correct.   I wouldn't be surprised if all are wrong."
    author: "Hank Miller"
  - subject: "Re: so, the million dollar question.."
    date: 2008-01-28
    body: "http://www.kdedevelopers.org/node/3233 -- Lookie this one.\n\nAnd yes, Symbian has a couple UIs already, UIQ (Sony-Ericsson and recently also Motorola-owned) and the S whatevers (S60 right now) of Nokia.\n\nDeveloping on Symbian seems like a total pain in the bum (at least from what I&#8217;ve seen so far), so having Qtopia/Qt/whatever on Symbian would be unspeakable awesomeness."
    author: "Henrik Pauli"
  - subject: "Highly unlikely"
    date: 2008-01-28
    body: "How could they get a better product than gtk+/maemo but purchasing QT? It makes no sense as QT is worse option. Not technically but in the big picture. Technically QT is respectable. It just attracts people who do not have the slightest clue about GUI designing - KDE folks and alike :-(\n"
    author: "Drol"
  - subject: "Re: Highly unlikely"
    date: 2008-01-28
    body: "\" It just attracts people who do not have the slightest clue about GUI designing - KDE folks and alike :-(\"\n\nDon't be ridiculous.  What possible property of Qt could make it unappealing to companies with a usability team?"
    author: "Anon"
  - subject: "Re: Highly unlikely"
    date: 2008-01-28
    body: "Your nick says it all - drol = shit in dutch... Makes sense, as you talk like that."
    author: "jospoortvliet"
  - subject: "Re: Highly unlikely"
    date: 2008-01-28
    body: "I&#8217;ve seen quite a few terrible UIs written in&#8230; well, every toolkit, so whatever.  Yes, Gnome has problems too, believe it or not."
    author: "Henrik Pauli"
  - subject: "KDE and Nokia"
    date: 2008-01-28
    body: "I hope it won't end like Novell and OpenExchange."
    author: "peter"
  - subject: "Re: KDE and Nokia"
    date: 2008-01-28
    body: "To me it looks like a big company seeing the benefits of Qt, and taking measures to make sure it continues to be technically excellent in the future. Nokia has a nice (if not totally clean) track-record in the Free Software world, and as e.V. dude, I'd be happy to welcome them to support KDE more actively (which is what they plan to do).\n\nDespite all the bitching, this is an excellent opportunity for KDE to gain more traction also in the mobile space - an area where there's quite some room for improvement for KDE currently. Seeing another big player in the industry backing (and using Qt) is probably a very good thing.\n\nAnd if anything fails, having Qt under a BSD license (which is what the FreeQt foundation guarantees in case TT doesn't release a new version with real new features every year) should be enough of a safety net.\n\nOtherwise, Trolltech has been an excellent partner to work with in the past, and I do not get the impression that this will change."
    author: "sebas"
  - subject: "Re: KDE and Nokia"
    date: 2008-01-28
    body: "Not completely clean:\n\n- heavy support of software patents in Europe\n- known for pressing w3c to not include ogg in html 5 standards"
    author: "Jorge"
  - subject: "Re: KDE and Nokia"
    date: 2008-01-28
    body: "\"- heavy support of software patents in Europe\n- known for pressing w3c to not include ogg in html 5 standards\"\n\nIf Nokia are to get anywhere, that has to change."
    author: "Segedunum"
  - subject: "Re: KDE and Nokia"
    date: 2008-01-29
    body: "\u00ab- known for pressing w3c to not include ogg in html 5 standards\u00bb\n\nThat's misleading. Nokia voted against OGG as a web standard; that is they want HTML to remain multimedia-neutral."
    author: "blacky"
  - subject: "Re: KDE and Nokia"
    date: 2008-01-29
    body: "I wonder how many people complaining about this use gmail or similar sites?\n"
    author: "SadEagle"
  - subject: "Re: KDE and Nokia"
    date: 2008-01-29
    body: "Their arguments about this was amazingly flawed though. They even called Ogg for proprietary technology - I'm sorry, but WTF?!"
    author: "Joergen Ramskov"
  - subject: "April 1 is early this year"
    date: 2008-01-28
    body: "That was my first thought..."
    author: "Fred"
  - subject: "WTF"
    date: 2008-01-28
    body: "1) Nokia is pushing for software patentability.\n2) Nokia opposed OGG as a web standard\n3) Nokia shut down the Bochum factory because they don't like the idea of well-paid workers\n4) Nokia is a GTK/GNOME house and dont be naive, this is not changing\n5) Nokia is obviously doing this in order to harm its competitors (Motorola...) who use Qtopia. Hence, don't expect good news for Qtopia.\n6) Again, what is Trolltech gaining from this? Even if an acquirer was needed, Nokia would not be a good candidate.\n\nThere are many companies out there who would be happy to buy Trolltech and could be good partners. Nokia just isn't one of them.\n"
    author: "A KDE developer"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "well, (some) gtk guys are right now worried, that nokia will abandon gtk for qt. :-)"
    author: "bobo"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Which would be sad. There is a need for competition. A killed competitor makes you win for short, but you loose the force to be better. And it should be rather the closed source competition which should be the \"looser\", no?"
    author: "Erich"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Lucky for us, a switch by Nokia from GTK to Qt would not really harm GTK all that much, I'd say. Still, I have no illusions here. Novell bought Suse and it is not like all of a sudden they became great KDE supporters either."
    author: "Andr\u00e9"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "but in the end they did, and they do more work on KDE than other distro's, so Novell buying Suse turned out to benefit suse and at the same time benefit gnome."
    author: "RJ"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "I agree. The other positive thing is that Gnome and KDE run side by side on openSuse better than on any other distro.\n\nIt's too early to conclude whether Nokia acquiring Trolltech and with it QT is negative or positive for KDE. I would say, let's wait and see ;)"
    author: "Bobby"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\"1) Nokia is pushing for software patentability.\n2) Nokia opposed OGG as a web standard\"\n\nIf Nokia are to get anywhere, this has to change. This is step 1.\n\n\"3) Nokia shut down the Bochum factory because they don't like the idea of well-paid workers\"\n\nBochum produced little, and Nokia would have had to factor in the cost of laying off German workers. That isn't inexpensive. It happens.\n\n\"4) Nokia is a GTK/GNOME house and dont be naive, this is not changing\"\n\nDon't be naive. Nokia is not a GTK/Gnome house, they are a Symbian house. Many people like to think the GTK/Gnome stuff is significant. It isn't.\n\n\"5) Nokia is obviously doing this in order to harm its competitors (Motorola...) who use Qtopia.\"\n\nI don't see why. They'll be making money out of them now."
    author: "Segedunum"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: ">3) Nokia shut down the Bochum factory because they don't like the idea of well-paid workers\nNokia was the last producing in Germany.\nSo ALL mobile phone companies \"don't like the idea of well-paid workers\"."
    author: "Philipp"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Your worries are less substantial than my yearly donations to \"Crackheads for Kids\" foundation (which are $0 if I recall correctly).\n\nYou look at it from the wrong perspective. The bottom line for KDE (also known as Lars at this point) points out that nothing, nada, zero changes for Trolltech engineers. Meaning that Trolltech team works as it always had. What changes is that they have more resources which is somewhere between \"awesome\" and \"holly crap\" on the good-scale.\n\nAlso what changes is that, and you can trust me on that, there's virtually no way of developing a patent free vector graphics framework that that good text handling. Most of the patents involved /can/ be disputed but the fact stays that there is tons of them. Your options pretty much are a) do nothing because you have no resources, b) get a patent sugar-daddy super company who can protect you. We used to do a. Option b is way better. Now Trolltech has b. \n\nIt all means that Trolltech is safer than it ever was and is free to do what it was always doing with more resources at its disposal. Wicked :)"
    author: "Zack Rusin"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\n\"nothing, nada, zero changes for Trolltech engineers\".\n\nI haven't seen the merger agreement, maybe Lars has, but as a general rule, when someone acquires a company, one fundamental thing changes:  the new owner gets control.  And that means everything is changed.  All the talk about \"everything will stay the same\" is standard fare chatter in connection with acquisitions, but those words have \"nothing, nada, nada, zero\" to do with predicting the actual future of the target company.\n\nNow show me agreements in the purchase agreement in which Nokia agrees not to exercise any management control over Trolltech (i.e., does not get to appoint, terminate or reprimand the board of directors or officers), and there might actually be some substance to it, but I would eat my proverbial hat . . . ."
    author: "Sage"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "As important as someone who uses just a nickname on a public forum is, I'm not sure whether you are in a position to make demands (maybe if you changed it to Madonna, Prince or Bono it would work).\n\nIt's people who make the company, not the company that makes people. Trolltech is what it is because it has one of the best engineering teams in the world. All you need to know from the agreement is: what happened to the Trolltech engineering team. Answer of \"nothing\" is the best possible scenario (the case here).\n\nLook at the managers who lead engineering teams at Trolltech for indication of what's going to happen. If Lars, Matthias and Warwick are there, any discussions about Trolltech's dedication to KDE, Open Source and Qt are silly at best. If there's anything you can be certain in this world is that this guys will never let their vision to be trashed."
    author: "Zack Rusin"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Zack, I have a lot of respect for you (from reading your blogs), but you don't honestly believe what you write here, do you? At least I'm very concerned about all this. When the Nokia accountants decided that e.g. supporting KDE developers is not efficent for reaching there business goals how much say will \"Lars, Matthias and Warwick\" have? None, probably. And what interest can a company like Nokia have to support an open source desktop? None, probably. This is best illustrated by the Bochum case: Nokia make a profit of over 7 billion Euro, an increase of 67% and they close the factory because of - inefficiency: wtf?!"
    author: "psychotron"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "I think Nokia has all the reasons to continue working with KDE for the exact same reason it was interesting for TrollTech to do so : a toolkit like Qt is a rather complex thing to create and to have a complete desktop built on it and providing enormous feedback on almost every usage you can imagine, with quite a lot of users, and at the same time not being directly usable by the competition, is really an enormous advantage. I don't see any reason why Nokia would phase this out as long as they are interested in Qt technology.\nI don't think there is any reason to panic here : there's very little chance that Qt could be lost to the open source cause, and if everything goes wrong (and there's no reason to think it will, open source lives well within Sun, for instance), I'm pretty sure many TT guys will leave and make take over the BSD Qt to create another company with the same goals TT had.\nSo since there should be nothing to be afraid of as far as KDE is concerned, why just not wish good luck to TT and hope they'll be able to add even more goodness in it, using the input of ressources Nokia will provide?"
    author: "Richard Van Den Boom"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "If TT goes evil and closes the QT source somehow, that'll be a massive pity. That said, as long as QT is GPL it can (and will if the need arises) be forked.\n\nIt has happened before."
    author: "NabLa"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Well, the difference I see here is that Trolltech developed a general purpose framework to be used for all kinds of applications. And for this having KDE as a widely spread testing platform covering all kinds of usage cases is surely a good thing for the company. BUT Nokia has probably no interest in that. I can't imagine they now want to enter the business of selling widget toolkits. IMO they bought Trolltech primaily to develop their own restricted set of applications and for this I think they don't need such a lot of testing as provided by KDE. Of course they will take it as long as they get it for free. But will they support KDE developers? Will they react on development wishes by the KDE community? Maybe they do it now, in order to avoid bad press. But mayby they will silently cease the support for KDE ... I just find it hard to believe that Nokia activly supports free software, as TT did.\n\n(Note: I'm not talking about the licensing issues here, this should be fairly save due to GPL and Free-Qt-Foundation. I'm more concerned about the other support KDE got from TT)"
    author: "psychotron"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "You mean that Nokia is now in the desktop business?\n\nSeriously?\n\nNokia is phones with alot of traction. Phones are getting bigger and more capable. Trolltech is desktops, ie. a library for building apps with some traction, KDE being an obvious benefit to them. They moved into portables and phones with little traction.\n\nHow does KDE as desktop tie into Nokia as phones?\n\nHow the phone market works here is that you run what you get from the store. No changes, no changing to version 4.1 to see how it works. If KDE has any place here it's as free labour for the phone manufacturers and network owners.\n\nIn other words, Trolltech/Nokia will probably continue to provide a seriously good library, but what KDE needs will get back to how it always was, KDE writes. I have had serious doubts about the viability of the Trolltech takeover of major maintenance intensive sections of the KDE desktop that we have seen in the last while (QPrinter, Webkit, #'s of lead developers on payroll, to name a few).\n\nDerek"
    author: "D Kite"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\"When the Nokia accountants decided that e.g. supporting KDE developers is not efficent for reaching there business goals\"\n\nPolitics inevitably comes into it, it alwas does, but the fact is that Nokia wouldn't have bothered with this if they weren't going to make Trolltech their development centre, effectively. It would be a bad idea to chop that. Additionally, it is self-sustaining.\n\n\"This is best illustrated by the Bochum case: Nokia make a profit of over 7 billion Euro, an increase of 67% and they close the factory because of - inefficiency: wtf?!\"\n\nBochum was a mistake, produced little and shows what can happen when subsidies are involved."
    author: "Segedunum"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\"This is best illustrated by the Bochum case: Nokia make a profit of over 7 billion Euro, an increase of 67% and they close the factory because of - inefficiency: wtf?!\"\n\nAre you of the school of thought who thinks that \"if company makes even one euro in profit, they must NOT try to make their operations more efficient in any shape or form\"?.\n\nFact of the matter is that comparing expenses to production, Bochum was an albatross around Nokia's neck. IIRC, Bochum represented about 24% of Nokia's expenses, while representing only 6% of production. Bochumians should be grateful for the fact that Nokia pumped millions of euros to the local economy for years. That's more than German cell-phone-manufacturers have done (which moved out of Germany long ago).\n\nFew years ago Fujitsu-Siemens shut down a proditable computer-plant in Finland and moved production to Germany. For some reason Germans never opposed that move..."
    author: "Janne"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "It's not _that_ they did it, it's _how_ they did it. Sure, the can make as much money as they like, I couldn't care less. And the numbers you name a company surely can't accept forever. But they didn't seem to even try to make them better by talking to the employees and maybe find a compromise. If that didn't work out, they still could move the production. However the employees got the news from the press! Additionally they not only pumped millions of euros to the local economy, but also got subsidies from the government... With the 7 billion I just wanted to note that Nokia can very well afford to keep certain ethical standards while still making _a lot_ of profit, but they just didn't care.\n\nAnd hey, this is not a Finland vs. Germany thing, at least from my side. No need to feel attacked ;) It wouldn't be any better if that were a german company!"
    author: "psychotron"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "\"It's not _that_ they did it, it's _how_ they did it.\"\n\nWell, how did they do it?\n\n\"But they didn't seem to even try to make them better by talking to the employees and maybe find a compromise\"\n\nReality-check: Bochum is an old television-factory. years ago televisions became unprofitable and Nokia exited the business. They found a compromise back then: they started building phones in the factory. Isn't THAT a \"compromise\"? Nokia gave the factory YEARS of extra life, while pumping millions to the local economy. \n\n\"Additionally they not only pumped millions of euros to the local economy, but also got subsidies from the government.\"\n\nAnd I bet they paid a lot more back than what they received from the government. And why are those subsidies an issue? They gave those subsisies on the condition that Nokia will operate the factory for X years. X years has now passed, and they are closing the factory. And the problem is.... what?\n\n\"With the 7 billion I just wanted to note that Nokia can very well afford to keep certain ethical standards while still making _a lot_ of profit, but they just didn't care.\"\n\nWhat \"ethical standards\"? Is is \"more ethical\" to operate a factory in Germany than it is to operate one in Romania?\n\n\"It wouldn't be any better if that were a german company!\"\n\nIt just happens that when German companies pulled the plug on cell-phone manufacturing in Germany, no-one in Germany said a word. What should we do if/when Porsche decides to pull the plug on Porsche-manufacturing in Finland, and move that production to Germany... Will there be demonstrations in Germany then? But hey, I guess that would be \"ethical\"?"
    author: "Janne"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "I rather believe that Trolltech got the biggest chunk of its high profile because of its licensing. I don't say anything against the developer team - they may be the best of the word but that's not decisive. The licensing is! We all know that KDE would not be based on Qt without the free lecense being there.\n\nMost likely Nokia, even being in control now, cannot change anything about the licensing of the existing versions - but they can do it for future versions! So, it's possible that really nothing changes for the developing team of Trolltech. A little licensing policy change is all that's needed. I cannot imagine a reason for Nokia to spend money on something like this UNLESS it provides them with a way to handicap competitors.\n\nI'm seriously worried!"
    author: "dirks"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "If licensing was an issue, I'm not sure Nokia would have left TT switch to GPL3 one week before the buyout annoucement.\nYou're aware, of course, that this type of buyout takes months of discussion, and it's usually not a good idea to cross your buyer?"
    author: "Richard Van Den Boom"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Why the would they care about the license? They could change it to whatever they want with the next version."
    author: "doesn't matter"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Yeah, they can change but once Qt is GPLv3 it can not be revoked and that version  can be forked by others."
    author: "Mithrandir"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Even ignoreing the poison pill, this verson is GPLv3 and that can't be revoked"
    author: "Ben"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Because by terms of the agreement with the Free-Qt-Foundation, the latter can basically throw the whole codebase on the street for everybody to use as they see fit under a BSD-style licence. You'd risk throwing away a rather large portion of Trolltech's capital in the form of intellectual property."
    author: "Andr\u00e9"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\"intellectual property\"\n\ngo to RMS"
    author: "Nick Shaforostoff"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\n\"I'm not sure whether you are in a position to make demands.\"\n\nI'm not demanding anything.  You made what I believe to be a very incorrect statement, namely, that nothing has changed.  My point was that Nokia has control and can fire the engineers you are speaking about if it chooses.  Maybe Nokia won't but my point is the entire business model has changed.\n\nIf we are lucky, Nokia will treat Qt as a services business and make revenues through support and charging for modifications, rather than on license fees, and keep all the KDE developers.  But that is Nokia's choice and, as has been noted, they are a global company focused on the bottom line.\n\nConsider for example:  did SuSE undergo changes after being acquired by Novell (recall we where told, no significant changes would be made)?  Would SuSE have entered into a patent agreement with Microsoft?  Relegated KDE to the backburner?  Open-sourced YaST?"
    author: "Sage"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "\"My point was that Nokia has control and can fire the engineers you are speaking about if it chooses. Maybe Nokia won't but my point is the entire business model has changed.\"\n\nWhy the heck would they do that? Trolltech are the experts in Qt technology. Nokia wants to help grow that technology. To fire the engineers would mean a huge brain drain. They would have to spend millions to train other engineers."
    author: "lpotter"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "Well, it happened to me before.\n\nSmall \"linux embbeded\" company get bought by a bigger company.\n\nManagement decides: \"Hey lets make a new product, but to make it really nice,\nlet's make it a Windows CE box, so that users can plug-in USB modules like GPS and webcam and stuff out of the box and do Pocket Word and Pocket Excel\".\n\nManagement thinks: \"But we are a Linux company, so hmmm let's have \na hardware layer, a Linux layer and a Windows CE GUI layer in C#\",\nsince we can do the Linux layer easily (they used framebuffer before).\n\nThe system ends up really slow and too much battery consuming due to 2 MIPS processor. 80% of Linux engineers quits because they are fed up and don't want to do any C# on Windows CE. Junior MSCE programmers are hired to do the C#,\nand they create an inefficient piece of crapt.\nThey also get a MSDN Universal / Microsoft Partnership deals so it cost less.\n\nSomeone at the head quarters figure out this mess, \nwonders why this Linux thing is used at all.\nAMD sends us a prototype board and using a x86 instead for Windows CE makes thing much faster (problem solved).\nThe Linux layer is scrapt (useless).\n\nThe project is cancelled and moved to head quarters\nusing Windows CE / Hardware only on x86, \nso that they can more tightly control the project. \n\nEveryone at our office gets laid off just before Christmas. \nHappy new year everyone!\n\nOverall, since the head quarter bought us and another rival, \nboth shops are now closed and both R&D products were shut down.\nWhat a waste of time and energy.\n\n\n"
    author: "merged"
  - subject: "Re: WTF"
    date: 2008-01-29
    body: "That's exactly what gets me worried...\n\nThat and the big lie at the keynote!!!\n\nThey should have addressed the buyout."
    author: "Richard Lionhard"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "> 3) Nokia shut down the Bochum factory because they don't like the idea of well-paid workers\n\n(Un?)fortunately this is how it works in this globalized world. What is bad for workers in Germany is good for workers in Romania, where they open a new factory. But don't worry, they will close this factory as well if they find a place where they can produce with less costs (wages, transportation, logistics, whatever costs in total). And they aren't the only one, e.g  IBM did the same with a hard disk factory some years ago in Hungary: they opened it, when Hungary become too expensive, just closed and moved further (east).\n This is called capitalism, business and global market. There is the government who should make laws to either keep businesses in country and/or protect the workers in such cases when a company shuts down its facilities."
    author: "Andras Mantia"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "I'm not sure what to think of it - Nokia obviously doesn't have a totally clean track record, but they do have a decent track record when it comes down to contributing to OSS projects they're using/developing (e.g. Linux kernel patches, developing their GTK fork in the public, ...)\n\nWith companies the size of Nokia or IBM, it's hard to put out a general statement like XXX is \"evil\" or XXX is \"good\" -- there are too many departments that don't know what the others are doing, and some departments are leaning towards \"good\", some towards \"evil\".\n\nThe patents department of Nokia is leaning towards \"evil\" (and I'll count the ogg stuff in there, because this is quite obviously about making a profit from patented alternatives), while their engineering team is leaning towards \"good\" (contributing patches etc). Their management is in between, trying to play both sides, which, from a pure business standpoint (which is really all they care about) makes sense -- patents are a revenue stream, working with the open source community to make products better is another, completely separate, revenue stream. No, I don't like it either, but that's the way the system works.\n\nNokia has shown some pretty strong dissatisfaction with GTK/Hildon, and for now I'm giving them the benefit of the doubt - they realized Qt is better, they don't want to depend on TT, so they make it an inhouse development. Qt also allows them to consolidate their phone development and desktop application development (e.g. the phone and the desktop sync tools using the same code -- Qtopia and Qt/{X11,Windoze,Mac} are fully compatible), which just makes sense.\n\n3. is evil, but it's the evil any sufficiently large company does - and chances are it won't affect TT, because R&D work is usually kept in expensive countries - manual labor is always the one that goes to low-pay countries first.\n\nRe 5, I don't think this is about blocking the competition, they'd be stupid if they didn't take in the revenue of licensing their toolkit to their competitors (and they already license Symbian to competitors). It may actually be good news for Qtopia if only because they'll start using it in some products - Qtopia based devices are still quite rare, even one huge deployment would be good news there.\n\nRe 6, Nokia is maybe not an ideal candidate, but is far from the worst (One of many worse alternatives: Microsoft - for them, buying TT would make sense to provide a better replacement for MFC in the next Windoze release, and while at it they could slow down development of the best Linux toolkit).\n"
    author: "bero"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "A very good analysis there Bero, beats some of teh more panicked reactions.  I'm hoping this is the intent of the deal.\n\nJohn."
    author: "odysseus"
  - subject: "Re: WTF"
    date: 2008-01-28
    body: "\"1) Nokia is pushing for software patentability.\"\n\nAnd other parts of Nokia are deeply involved in Free Software. Nokia is a big corporation with different activities.\n\n\"3) Nokia shut down the Bochum factory because they don't like the idea of well-paid workers\"\n\nWorkers in Romania get good salaries by local standards. Yes, their salaries are lower than in Germany, but so are their living-expenses, so the resulting standard of living is propably more or less similar (maybe less BMW's, but still). \n\nSince when are our (that in, rich westerners) salaries the universal yardstick upon which all salaries around the world should be compared to? Isn't it a bit arrogant to assume that WE are some kind of universal standard? If you think that \"workers in Romania are underpaid since their salaries are lower than our salaries\", couldn't we just as well say \"we in the West are overpaid, since workers in Eastern-Europe and Asia are willing to do the same job for less money\"?\n\nAnd, a honest question: which is more ethical: Offering jobs to rich Germans, or offering jobs to poor Romanians?\n\n\"4) Nokia is a GTK/GNOME house and dont be naive, this is not changing\"\n\nUm, by and large, Nokia is a Symbian-house. N800 is an exception to the rule."
    author: "Janne"
  - subject: "a company is a company is a company"
    date: 2008-01-28
    body: "I think about Bochum. "
    author: "Tagesthemengucker"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Closing a PRODUCTION site in Germany - well, welcome to globalism. Stop buying 98% of the products in the market if you're afraid of that.\n\nDon't forget that Nokia has huge R&D sites in Ulm and Duesseldorf, plus dozens of local smaller offices. It's a pity for the unqualified workers, though."
    author: "harryF"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "People here seem to love Nokia and globalization.\nRather strange for users of a free software project."
    author: "Psst"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Well, I see free software as a positive example of globalization. Developers around the world make a product to the end user at a cost that is as small as possible, ie. free."
    author: "Pnt"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Well, globalization played a big role in the spreading of FOSS, the internet is the pinnacle of the global economy - it creates a fair playingfield. And Nokia, though not without its flaws, has contributed a lot to FOSS already, and they apparently have seen the light now. I welcome them to KDE/Qt development ;-)"
    author: "jospoortvliet"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "\";-)\" indeed. I hope you're kidding.\nThere is no fair playing field in global economy. It doesn't work. Globalization is merely a buzzword to justify the most insane actions most corporations take to increase profits.\n\n> And Nokia, though not without its flaws, has contributed a lot to FOSS already, and they apparently have seen the light now.\n\n\"not without it flaws\"? Haha. Not even the Finns like Nokia.\nAlso you can't be pro FLOSS AND pro software patents. It just doesn't work that way.\n\nSo I _don't_ welcome our new KDE/Qt owning, software patenting overlords.\n\n...but I still HOPE that everything turns out fine. :-/"
    author: "Psst"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "\"There is no fair playing field in global economy. It doesn't work. Globalization is merely a buzzword to justify the most insane actions most corporations take to increase profits.\"\n\nPurpose of corporation is to make profit. Why should they manufacture products more expensively than is required? Do you voluntarily pay more than needed for goods and services? No you do not. So you are out to maximise your \"profit\" as well."
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "I take the \"Made in USA\" over the \"Made in China\" ANY day. I gladly pay double.. screw that. I pay five times the price.\n\n> Purpose of corporation is to make profit. Why should they manufacture products more expensively than is required?\n\nWhy not let little children put together your shoes. Why not take slaves.\nIt's depends on HOW they do their business. Most corporation just go too far with their greed."
    author: "Psst"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "\"I take the \"Made in USA\" over the \"Made in China\" ANY day.\"\n\nSo would I, assuming it's not outrageously more expensive. So what's your point?\n\n\"Why not let little children put together your shoes. Why not take slaves.\nIt's depends on HOW they do their business. Most corporation just go too far with their greed.\"\n\nIf a corporation mistreats their employees, they sure as hell should be bitch-slapped for it. But is Nokia (for example) doing that? They are paying competetive salaries in the countries their factories operate in. Is that wrong? Why? Because that salary is less than we in the west are getting paid? What makes you think that our salaries are the universal standard that should be used when determining salaries in some other countries? If you think that those workers are underpaid, couldn't it also be said that we in the west are overpaid?"
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "> So would I, assuming it's not outrageously more expensive. So what's your point?\n\nI answered your question.\n\n\n> If a corporation mistreats their employees, they sure as hell should be bitch-slapped for it.[snippy]\n\nSo they mistreated over 2000 workers in Germany. Will they be \"bitch-slapped\" for it? No.\n\n\n> What makes you think that our salaries are the universal standard that should be used when determining salaries in some other countries? If you think that those workers are underpaid, couldn't it also be said that we in the west are overpaid?\n\nYes. \"There is no fair playing field in global economy. It doesn't work.\"\nCertain people don't want it to work so it will never work. \"Globalization\" will never be fair or even things out. It's a business scheme to increase small term profits."
    author: "Psst"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "\"So they mistreated over 2000 workers in Germany. Will they be \"bitch-slapped\" for it? No.\"\n\nHow exactly are those employees in Germany being \"mistreated\"? They are being laid off, they are not being whipped or humiliated or tortured. No, they got jobs from Nokia for several years.\n\nAll this makes me feel that Nokia should have closes that factory down for good in the mid-nineties, then everything would be A-OK, since Nokia wouldn't have any factories in Germany to close down, and everyone would be happy, right? But no, Nokia kept that factory running for about 10 years, providing thousands of jobs and millions in income.\n\nOr do you believe that if some company sets up a factory, they are REQUIRED to operate that factory until the end of time? If they close that factory, they are \"mistreating\" those employees?\n\n\"Certain people don't want it to work so it will never work. \"Globalization\" will never be fair or even things out. It's a business scheme to increase small term profits.\"\n\nIt's already evening things out. Living-standards in those poorer countries are increasing at a rapid pace. That is an undeniable FACT. And since salaries and expenses in China for example have been going up, China in return has been starting to outsource some thing to Egypt for example. So things HAVE been evening out.\n\nI'm really sorry, but your arguments make exactly zero sense."
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "You're a pathetic word twisting internet troll who confuses wishful thinking with facts and misinterprets inane statistics for retarded arguments.\n\n:-D"
    author: "Psst"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-31
    body: "And I can't figure out why you are so intent on ignoring reality.  What was bad for a couple thousand German workers was good for a similar amount of Romanian workers.  Why is it you don't seem to care for their improved lot at all?  How can you possibly construe giving them jobs as a bad thing?  They are not slaves or children.  They are paid good wages for their area and living expenses.  If you want to suggest that some entity should mandate that their wages are the same as wages in Germany, that would lead to them not having those jobs as they then might as well stay in Germany.\n\nThe lower wages they are paid are the reason they are paid them in the first place.  If enough companies move production there, the wages will increase and some of the jobs will go somewhere cheaper again, thus improving those places, which otherwise would not have those jobs.  At the same time, if jobs leave Germany wages will go down, bringing it in line with Romania.  We don't need to have fairness mandated, it happens by capitalistic processes.  It just doesn't always happen immediately.\n\nWhat you are advocating is communism (same wages for everyone) and it failed.  You seem to be extremely bitter that capitalism is the engine that has driven improvement in areas like China and India.  And it's not all about foreign companies reaping profits by gaming variable wages.  See China's homegrown and owned industries.  "
    author: "AS"
  - subject: "Re: a company is a company is a company"
    date: 2008-02-06
    body: "Hi AS,\n\n\nthe problem here is the very assumption that this special kind of global capitalism works.\n\nThat the intention of a few CEOs to maximize their profits actually leads to a long-term improvement for a society, or any society.\n\nI doubt that.\n\nWhat can be said generally is, for the CEOs to \"earn\" their exorbitant pay, they need other people to work for them, the cheaper the better.\n\n\nThe question here is, what makes the people that run a company actually and assuredly at the same time care about their employee's wellbeing.\n\nThe answer is a rather simple aspect of human psychology: You care about the people next to you. \n\n\nThus, on the big scale with altruistic perspectives, it simply doesn't work when some CEOs let random people anywhere in the world work for them, at the lowest wages. \nIt doesn't work, because they (are even forced by business law to) care about money, shareholder value, and not about people.\nIt doesn't work, because on can't rely on CEOs to act according to moral guidelines.\n\n\nBut it _does_ work, if there is a personal connection, again requiring a local connection, to the employees.\n\nThis kind of globalism that Nokia made the best example of essentially shows that the company considers their employees as exchangeable, expendable goods.\n\nIt shows, that this is really 100% capitalism in action.\n\nThe 2000 new jobs in Romania are just a coincidence, not a result of mutual human dependence, or human relations.\n\nAnd now you come and prove to me that this actually benefits a society. (Not necessarily a country)"
    author: "Ulrich"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "hippies are back"
    author: "Nick Shaforostoff"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Yes.. ex-soviet talking about hippies. Gotta love it.\nBut hey.. East-Europe.. you might be one of those \"beneficiaries\" of globalization. I guess I have to thank you for my cheap T-shirt. (this was much more evil before editing)\n\n</evil>"
    author: "Psst"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Nope, sorry, you need to thank the Chinese for your cheap T-shirt, just like me ;) We, East-Europeans, are allready too expensive for that."
    author: "Andrei"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "you'd better thank for the brains we export :)"
    author: "Nick Shaforostoff"
  - subject: "What IS globalization?"
    date: 2008-01-28
    body: "Is it the fact that production-plants are moving to cheaper countries? Is that a bad thing? And before you say \"YES!\", consider few facts:\n\na) Fact is that due to globalization, the countries that have been targets of investments have made HUGE strides in eliminating poverty. Offering poor people jobs is the single best way to improve their lot in life.\n\nb) Fact of the matter is that thanks to globalization, our purchasing-power in the west has been going up, since products have gotten cheaper.\n\nIsn't it smart to manufacture good where it's the cheapest and most efficient?\n\nIt seems to me that all those people who complain when company moves production to Asia etc. forget the fact that the people in Asia are people too. Why do we deserve jobs, while they do not? Are we somehow \"better\"?"
    author: "Janne"
  - subject: "Re: What IS globalization?"
    date: 2008-01-28
    body: "> Fact is that due to globalization, the countries that have been targets of investments have made HUGE strides in eliminating poverty.\n\nErrrm. No.\n\n\n> Offering poor people jobs is the single best way to improve their lot in life.\n\nIf you think that ANY corporation that moves to \"cheaper countries\" has those novel intentions to improve anyone's life you have to be extremely ignorant.\n\n\n> Fact of the matter is that thanks to globalization, our purchasing-power in the west has been going up, since products have gotten cheaper.\n\nWhere are you from? Over the last 15 years (at least) every single thing has been increasingly less affordable each year where I'm from. Wages are dropping and everything is getting more expensive...\n\n\n> Isn't it smart to manufacture good where it's the cheapest and most efficient?\n\nFor money most corporations would even work together with the nazis (IBM). It's not a matter of being smart or not. It's a matter of how to increase profits.\n\n\n> It seems to me that all those people who complain when company moves production to Asia etc. forget the fact that the people in Asia are people too. Why do we deserve jobs, while they do not? Are we somehow \"better\"?\n\nAre Asian people better? Just take a look at the Nokia Bochum \"incident\". Are people in Romania better than people in Germany? This argument is stupid. It's NOT about people. It's about money. NOKIA DOESN'T CARE ABOUT PEOPLE (OR FREEDOM OR FREE SOFTWARE OR KDE... just to keep on topic, hehe)."
    author: "Psst"
  - subject: "Re: What IS globalization?"
    date: 2008-01-28
    body: "\"Errrm. No.\"\n\nErrrrrrm. Yes. Don't believe me? Go read some statistics. Seriously. Here's one from Wikipedia:\n\nhttp://en.wikipedia.org/wiki/Image:Percentage_living_on_less_than_%241_per_day_1981-2001.png\n\nThere are lots more besides that.\n\n\"If you think that ANY corporation that moves to \"cheaper countries\" has those novel intentions to improve anyone's life you have to be extremely ignorant.\"\n\nWhere exactly did I claim that they move to poorer countries out of the goodness of their hearts? What  did say is that the fact that they do so, reduces poverty in those countries, due to the fact of having more jobs, and due to the fact that those jobs pay better than jobs offered b local companies do.\n\n\"Where are you from? Over the last 15 years (at least) every single thing has been increasingly less affordable each year where I'm from. \"\n\nThere are two things to consider here. One of them is called \"inflation\". Yes, prices do go up over time. But that does not mean that they necessarily get more expensive. Take a look at computers. Or television, or any piece of consumer-electronics that is made in Asia. Prices have come crashing down.\n\n\"Are Asian people better? Just take a look at the Nokia Bochum \"incident\"\"\n\nWhat about it?\n\n\"Are people in Romania better than people in Germany? This argument is stupid. It's NOT about people. It's about money.\"\n\nSo, what about money? Should Nokia keep pumping more and more money in to rich Germany, while not pumping one cent to poor Romania? Where exactly are the ethics of giving millions of euros to people who are already richer than about 90% of the people on this planet, as opposed to giving that money to poorer people? Please explain the rationale and ethics of THAT!\n\n\"This argument is stupid.\"\n\nYou only say that because you realize that you have no way out of this discussion. In one hand we have the option of pumping millions of Euros in to one of the richest countries on the planet, and on the other hand we have the option of pumping millions of euros in to one of the poorest cointries in Europe. And here you are basically making the claim that this money should be given to the rich, as opposed to giving to the poor."
    author: "Janne"
  - subject: "Re: What IS globalization?"
    date: 2008-01-28
    body: "Hehe. You gotta be kidding.\nNot gonna bother anymore. Think of that what you will. :-D\n\nIs Nokia already paying astroturfers? If so, where are they from? India? Haha..."
    author: "Psst"
  - subject: "Re: What IS globalization?"
    date: 2008-01-29
    body: "Ironic, considering who you are."
    author: "KDE User"
  - subject: "Re: What IS globalization?"
    date: 2008-01-29
    body: "I ripped your \"arguments\" to shreds with pure facts. And when you notice that you have been utterly defeated, what do you do? \"I'm not gonna play anymore, I'm taking my ball and going home!\". I take that as an admission of defeat. Thanks for playing!\n\nSeriously: I gave you facts. I gave you statistics. I gave you numbers. Your reply: \"You gotta be kidding\". Why is it that some people refuse to believe even when they are being spoon-fed facts and statistics?"
    author: "Janne"
  - subject: "Re: What IS globalization?"
    date: 2008-01-30
    body: "I suggest you take a peek at some of J. Stiglitz's books, like \"Globalization and Its Discontents\". Globalization is like the Force : a light and a dark side. :-)\n\n"
    author: "Guillaume Laurent"
  - subject: "Re: What IS globalization?"
    date: 2008-01-31
    body: "Utterly OWNED and Psst is too stubborn to see it.  Psst, if you are wondering why someone doesn't take you seriously, look at your behaviour here.  Your petulant assertions got demolished and you have no reply other than more disbelief.  You're not worth discussing things with."
    author: "AS"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Well, globalization is also about freedom: freedom to produce where you want, sell where you want, do what you want. :) Independent of the fact that I like or dislike Nokia. Yes, I'm somewhat worried, but I don't think the end of the world has arrived. This whole thing can mean good as well. The problem will be if Nokia won't make profit in the following years and they will have to cut costs, close R&D sites and so. And, unfortunately, nobody could guarantee the existence of Trolltech in 5 years, with or without Nokia. "
    author: "Andras Mantia"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "I am not afraid. KDE, Gnome, Linux itself would not exist if there were no globalization.\n\nI just wanted to say that what Nokia does in Bochum was just not \"noble\". \nAnd I think that most people in Germany don't care about Nokias behaviour because people want affordable cellphones and stuff with a lot of eyecandy. That's it. \n\n\n\n"
    author: "tagesthemengucker"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "\"I just wanted to say that what Nokia does in Bochum was just not \"noble\". \"\n\nWhy is employing bunch of rich people in a rich western country \"noble\", whereas offering jobs to poor people in Romania is not \"noble\"? Should those Romanians simply remain poor? What's so \"noble\" in that?"
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "Nokia closed that factory though it made profit. On the other hand employing people in Romania is a good thing. Because when become rich, they buy \"german\" technology...\nFor the fired worker in Bochum the sitaution is bad, the same situation is a good one for the people in Romania. "
    author: "tagesthemengucker"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "\"Nokia closed that factory though it made profit. \"\n\nUm, so? It does not matter that did the factory make profit or not. What matters is that could they do the same production somewhere else more effectively and profitable. Why should Nokia manufacture their phones in a more expensive way than is required? \n\nWe all do the exact same thing. We shop around to find the best deal on goods and services. When I buy something I look around to find where I could buy the items the cheapest. Even though I might think that paying 100e (for example) for some gizmo is a reasonable price, I would still buy it for 90e if I had the option, instead of 100e. And so would you. Also, if I found the gizmo's value for me to be around 100e, and it was sold for 60e, I would not pay 100e for it even though I wasn't asked to. And neither would you. What we would do is to maximise our profit, and look for the most economical source.\n\nNokia is doing the same thing. They noticed that instead of paying 20e (a made-up number, but still) in labor-cost for every phone built in Bochum, they could manufacture the exact same product in Romania, while paying only 3e."
    author: "Janne"
  - subject: "Corporate Apologists"
    date: 2008-01-28
    body: "\"Nokia is doing the same thing. They noticed that instead of paying 20e (a made-up number, but still) in labor-cost for every phone built in Bochum, they could manufacture the exact same product in Romania, while paying only 3e.\"\n\nAnd then you discover that it retails for 500e. Oh, and forget Romania, you can manufacture it in Turkey for 2e. No, wait! How about China for 1e? And let's not think about the working conditions, pollution, social factors - it's all so distant, so vague. Besides, the Chinese economy is booming - they can't get enough BMWs!\n\nSometimes you have to stop, look up, and see where the hunt for bargains has taken you: it's probably not a very nice neighbourhood."
    author: "The Badger"
  - subject: "Selfish hoarder"
    date: 2008-01-29
    body: "\"And then you discover that it retails for 500e.\"\n\nUm, so? It also might retail for 100e.\n\n\"Oh, and forget Romania, you can manufacture it in Turkey for 2e. No, wait! How about China for 1e?\"\n\nWhat exactly is your point? That they should manufacture it in the location where it's most efficient? And do you also feel that those people do not deserve jobs, only we in the rich west deserve jobs, rest of the world can just stay poor?\n\n\"And let's not think about the working conditions, pollution, social factors - it's all so distant, so vague.\"\n\nWhat makes you think that those are worse in Romania (for example) than in Germany?\n\n\"Sometimes you have to stop, look up, and see where the hunt for bargains has taken you: it's probably not a very nice neighbourhood\"\n\nSometimes you have to stop, look up, and see where your greed and selfishness has taken you. It has taken you to a world where HUGE amounts of people live in poverty. It has resulted in a world where a minority lives in luxury and extravaganza, while majority lives in poverty. And when we have a MASSIVE transfer of wealth from uber-rich west to the poor countries in form of jobs, you whine.\n\nSeriously: when you boil away all the useless rhetoric from arguments made by Attac and other activists, what you have is this: \"We feel that people in the rich western countries should have jobs, instead of the poorer people in Asia and other countries\". That's it."
    author: "Janne"
  - subject: "Re: Selfish hoarder"
    date: 2008-01-29
    body: "Janne, you're now being a freetrade troll. \n\nDoesn't bother you a bit that this \"massive transfer of wealth\" only touches the middle and lower classes, while the top 5% gets richer and richer? That communities are being ruthlessly destroyed, both in Europe/US and in developing countries? That the environment simply cannot substain a worldwide expansion of the current production model?\n\nI live in England, I saw what Thatcherism produced. When you put economic interest above everything else, the result is not a nice sight. Globalization has lots of problems that need to be addressed, and pushing hard for 100% free trade doesn't do anything to solve them."
    author: "YourAverageJoe"
  - subject: "Re: Selfish hoarder"
    date: 2008-01-29
    body: "\"Janne, you're now being a freetrade troll.\"\n\nSo, let me get this straight: I don't buy in to this \"only people in the rich countries deserve jobs, screw the poor!\" BS, and that makes me a \"freetrade troll\"?\n\n\"Doesn't bother you a bit that this \"massive transfer of wealth\" only touches the middle and lower classes, while the top 5% gets richer and richer?\"\n\nUm, last time I checked, the people working in those factories could be considered \"middle-class\". Or are you saying that the Romanian elite ends up working in the Nokia-factory?\n\nFact remains that countries that have received these investments have made MASSIVE progress in reducing poverty. That is a FACT. And that reduction in poverty has NOT been done by making the top 1% of those poor countries earn more money, it has been achieved by employing lots and lots on low- and middle-class people, and giving them good salaries (IIRC, studies have shown that the western companies pay salaries that are triple of what local companies pay).\n\n\"That the environment simply cannot substain a worldwide expansion of the current production model?\"\n\nyou are right that environment couldn't really sustain 6 billion people with lifestyles similar to that in North-America and Western Europe. But do you think that that means that 5 billion people should live in poverty, while Americans and Europeans live in luxury?\n\nBut this isn't really about \"production-model\". It's due to the fact that we in the west are living beyond our means. Only reason we can live our lives filled with luxury is because billions are living in poverty. So this boils down to two things:\n\na) either we in the west have too high living-standard, since not everyone on this planet can reach our living-standard, since the ecosphere can't support it\n\nor\n\nb) There are simply too many people on this planet. The planet can't sustain 6 billion people with western lifestyle. That is a fact.\n\nSo the options are threefold:\n\na) We maintain status quo. People in the \"west\" remain rich, elsewhere people remain poor\n\nb) We try to reach an equilibrium in living-standards. And that means that people in the west need to make sacrifices, while poorer people improve their living-standards\n\nor\n\nc) We try to reduce the number of people on the planet, so we can have more people with high standards of living\n\nObviously, C is a long-term task. Which would you choose?\n\n\"Globalization has lots of problems that need to be addressed, and pushing hard for 100% free trade doesn't do anything to solve them.\"\n\nOf course globalization has it's issues. But so does socialist protectionism."
    author: "Janne"
  - subject: "Re: Selfish hoarder"
    date: 2008-01-29
    body: "\"So, let me get this straight: I don't buy in to this \"only people in the rich countries deserve jobs, screw the poor!\" BS, and that makes me a \"freetrade troll\"?\"\n\nYes, because you've just distorted the position of the person you responded to.\n\n\"Um, last time I checked, the people working in those factories could be considered \"middle-class\".\"\n\nMaybe in Romania, although I have my doubts, but in your advocacy for the lowest cost of production maybe you should read up on factory conditions further down the globalisation chain.\n\n\"But do you think that that means that 5 billion people should live in poverty, while Americans and Europeans live in luxury?\"\n\nI doubt that this was the suggestion, but again you're happy to project that viewpoint onto anyone who disagrees, aren't you?\n\n\"It's due to the fact that we in the west are living beyond our means. Only reason we can live our lives filled with luxury is because billions are living in poverty.\"\n\nAn interesting perspective: the developed world is living beyond its means (no argument there), so we'll get the developing world to make all our gadgets. Oh, and because they're all getting rich, they'll be a huge new market for our products too, or rather the products that big companies of western origin will actually be making under licence in those countries, because people in western countries obviously won't be making them. Yes, we'll spend our way out of this! Or rather, they'll spend their way out of it; we'll tax their hard work (patents anyone?) and then help them out with all that spending!\n\n\"Of course globalization has it's issues.\"\n\nSo far you've preferred to make up imaginary positions for anyone who has had anything bad to say about globalisation, so I'd obviously like to hear what you think the issues are. Instruction booklets with Finnish relegated to page 87, perhaps?"
    author: "The Badger"
  - subject: "Re: Selfish hoarder"
    date: 2008-01-29
    body: "\"Yes, because you've just distorted the position of the person you responded to.\"\n\nNope, I did not distort anything. I just boiled away all the BS, and exposed the true hidden meaning.\n\n\"Maybe in Romania, although I have my doubts, but in your advocacy for the lowest cost of production maybe you should read up on factory conditions further down the globalisation chain.\"\n\nSure, there are factories out there where conditions are not that good when compared to our standards. But even then we need to ask ourselves: what is their (the employees that is) alternative? Prostitution? Working for even crappier local companies for 1/3 the pay?\n\nNote: I don't buy any products from Nike.d\n\n\"I doubt that this was the suggestion, but again you're happy to project that viewpoint onto anyone who disagrees, aren't you?\"\n\nNo, that was not the suggestion, but that would be the end-result if we heeded the suggestion.\n\n\"An interesting perspective: the developed world is living beyond its means (no argument there), so we'll get the developing world to make all our gadgets.\"\n\nNot only us, but for them as well. \n\n\"So far you've preferred to make up imaginary positions for anyone who has had anything bad to say about globalisation\"\n\nlet's just say I'm fed up with BS surrounding this issue. I'm fed up when people who are in no shape or form affected by Nokias actions in Bochum start to whine about Nokia. Because when they do, they are in effect saying \"these jobs should be in Germany, and not in Romania\". And when I call them out on their opinion, they get their panties in a bunch.\n\nI can understand if Germans who lose their jobs get annoyed over Nokia. But when unrelated third-parties start whining, then the argument changes entirely."
    author: "Janne"
  - subject: "Re: Selfish hoarder"
    date: 2008-01-29
    body: "\"Nope, I did not distort anything. I just boiled away all the BS, and exposed the true hidden meaning.\"\n\nSo, let's look at what they wrote...\n\n\"Doesn't bother you a bit that this \"massive transfer of wealth\" only touches the middle and lower classes, while the top 5% gets richer and richer?\"\n\nIn other words, that the people just made redundant are, well, the middle and lower classes. Meanwhile, the owners and shareholders (the top 5%) reap the rewards. Sure, as I've said already, the middle classes might also be employed in the newly opened plants, although they're more likely to be working class who admittedly do need the work. However, your boiling process managed to cause some kind of chemical change:\n\n\"Or are you saying that the Romanian elite ends up working in the Nokia-factory?\"\n\nI guess they were referring to the people in the rich nation getting richer, but then I suppose everyone in the rich nation is moving swiftly on to becoming a \"knowledge economy\" with patents on fresh air and running water, right?\n\nMeanwhile...\n\n\"Sure, there are factories out there where conditions are not that good when compared to our standards. But even then we need to ask ourselves: what is their (the employees that is) alternative? Prostitution? Working for even crappier local companies for 1/3 the pay?\"\n\nThe alternative as I've said all along is that multinationals should not just take the cheapest deal with the exploitative working conditions and then justify it by saying, \"Well they'd all be much worse off it weren't for us, so they should take what they get offered!\" Read up about some of the working practices in some of these \"favoured nations\" and then wonder what the average corporation is doing about it when people aren't kicking up a stink.\n\n\"let's just say I'm fed up with BS surrounding this issue. I'm fed up when people who are in no shape or form affected by Nokias actions in Bochum start to whine about Nokia.\"\n\nI personally have no opinion about Nokia's situation in Germany. As the person whose opinions you managed to distort said, this kind of thing is familiar to anyone who lived in Britain during the Thatcher era, and anyone familiar with manufacturing industry knows that the work moves around, politicians make sweetener deals, and it never really works out. It's a bad thing when people lose their jobs, but it isn't just the fault of the company involved - the government should be doing their best to make opportunities in other sectors without paying businesses off.\n\n\"Because when they do, they are in effect saying \"these jobs should be in Germany, and not in Romania\". And when I call them out on their opinion, they get their panties in a bunch.\"\n\nI could whine about Nokia on a number of levels. However, on the topic of globalisation, what I do have an opinion about is this blissful notion that shopping around for the lowest cost of manufacturing - no matter what - is a good thing, which is pretty much your position if I've understood correctly. You can frame this as a Germany vs. Romania thing if you want, but you're on record as saying this...\n\n\"Why should Nokia manufacture their phones in a more expensive way than is required?\"\n\n\"What we would do is to maximise our profit, and look for the most economical source.\"\n\nAll this depends on what Nokia deems to be \"required\". If looking for \"the most economical source\" means not even bothering about how the work gets done, then it's a reprehensible stance to take in the name of \"free trade\" and \"globalisation\", and totally understandable why these terms, particularly the latter, have become dirty words for many people.\n\n\"I can understand if Germans who lose their jobs get annoyed over Nokia. But when unrelated third-parties start whining, then the argument changes entirely.\"\n\nI'm only pointing out the details that take the shine off this globalisation adventure you seem to be so eager to tell everyone about, but yes, I'm an unrelated third party. If you're really from Finland, on the other hand, I'd expect some kind of partiality in this matter given the \"elephant in the corner\" position of certain corporations in that country."
    author: "The Badger"
  - subject: "Convenient Strawman"
    date: 2008-01-29
    body: "\"What exactly is your point? That they should manufacture it in the location where it's most efficient? And do you also feel that those people do not deserve jobs, only we in the rich west deserve jobs, rest of the world can just stay poor?\"\n\nThat's not what I said: stop projecting your own prejudices onto what I wrote! I noted that companies can enjoy huge margins on products, and perhaps that gives them some potential for doing good socially, whether it is in Germany, Romania, Turkey, China or anywhere else, and not shaving the last few cents from costs.\n\nIf chasing down the lowest cost leads you into places where people have to work 16 hour days in appalling conditions, where their children don't get an education because they've been pulled in from the countryside and aren't \"entitled\" to it, and where - most importantly - the people commissioning the work can't be bothered to insist on decent social standards for the workers, then while there's money entering that particular country and the workers even see some of it, I think you'll find that a very small number of people are seeing most of that cash.\n\n\"What makes you think that those are worse in Romania (for example) than in Germany?\"\n\nOh sure, giving Romania revenue will hoist up living standards - I never denied that. And at least Romania is subject to a certain amount of oversight with respect to social protection and corruption, but that doesn't make a compelling argument for just finding the cheapest bidder and sending them the work.\n\n\"Sometimes you have to stop, look up, and see where your greed and selfishness has taken you.\"\n\nDon't patronise me! I know how lucky I am, but that doesn't mean that I have to buy into the argument that \"hey, when I can buy a $10 iPod that will be so cool because I'm really helping people in the developing world\". It's like all those people who claim that by playing the lottery they're being big charity donors.\n\n\"It has taken you to a world where HUGE amounts of people live in poverty. It has resulted in a world where a minority lives in luxury and extravaganza, while majority lives in poverty. And when we have a MASSIVE transfer of wealth from uber-rich west to the poor countries in form of jobs, you whine.\"\n\nAgain, you just take what I wrote and turn it into something that wasn't said. There's no problem in giving work to poorer countries, but there is a problem when this takes place in the form of working conditions that you or I certainly wouldn't put up with. I suppose you'll claim now that workplace regulation is an unnecessary obstacle to free trade and that the people now doing all the unpleasant jobs actually want to work insane hours.\n\n\"Seriously: when you boil away all the useless rhetoric from arguments made by Attac and other activists, what you have is this: \"We feel that people in the rich western countries should have jobs, instead of the poorer people in Asia and other countries\". That's it.\"\n\nYeah, people like Amnesty International, too - that must be it. I mean: what do they care about human rights?"
    author: "The Badger"
  - subject: "Re: Convenient Strawman"
    date: 2008-01-29
    body: "\"That's not what I said:\"\n\nyes it is. You might not know it, but it is. Everyone who gets their panties in a bunch when some factory moves from Germany, France etc. to poorer country is saying EXACTLY that. Of course they are masquerading their argument in to something more digestible. But when the BS ends, that is the core of their argument. That people in the west deserve these jobs, people in the poor countries do not.\n\n\"If chasing down the lowest cost leads you into places where people have to work 16 hour days in appalling conditions....\"\n\nAre there any indications that Nokia is doing any of that?\n\n\"I know how lucky I am, but that doesn't mean that I have to buy into the argument that \"hey, when I can buy a $10 iPod that will be so cool because I'm really helping people in the developing world\".\"\n\nWell, in a way, you ARE.\n\n\"Again, you just take what I wrote and turn it into something that wasn't said.\"\n\nBut that is what you are saying. All those anti-globalization-folks are saying exactly that. Sure, they are not saying it out loud, since it wouldn't sound nice if they told the press that \"we think that these people should not have any jobs\". But when you look at the true meaning of their arguments, that IS what they are saying.\n\n\"There's no problem in giving work to poorer countries, but there is a problem when this takes place in the form of working conditions that you or I certainly wouldn't put up with.\"\n\nIn many ways I would consider working-conditions in Japan to be something I would not put up with. Where do we draw the line? And what makes you think that by default working-conditions outside \"west\" are inferior? I HAVE seen Nokia's factories in China. They are not that different from their factories in Finland."
    author: "Janne"
  - subject: "Re: Convenient Strawman"
    date: 2008-01-29
    body: "\"Everyone who gets their panties in a bunch when some factory moves from Germany, France etc. to poorer country is saying EXACTLY that.\"\n\nI am *not* saying that. This is just your ridiculous way of framing what I have written in order to go over the same scapegoating exercise.\n\n\"Of course they are masquerading their argument in to something more digestible. But when the BS ends, that is the core of their argument. That people in the west deserve these jobs, people in the poor countries do not.\"\n\nPeople in poor countries *do* deserve jobs and they *do* deserve *genuine* free trade. But they also deserve decent standards in their workplace. I mean, you could argue that it's just great that electronic and industrial waste gets shipped out to India, for example, because people can make a living recycling it, but would you have a clean conscience knowing that they're extracting toxic materials without elementary safety equipment?\n\n[16 hour days in appalling conditions...]\n\n\"Are there any indications that Nokia is doing any of that?\"\n\nHas anyone bothered to check? I'm not accusing Nokia specifically of anything, but then I was addressing your general \"shop for the lowest cost producer\" argument which inevitably ends up with conditions like those I've described. I mean, I've read about this kind of stuff in the finance press of all places, so I hardly think we are in imaginary territory. Besides, I believe there was a fuss about a certain well-known, fashionable brand of computer and consumer electronics equipment and their production facilities in China - you should know who I mean, and a search for their name plus \"China\" and \"factory\" will yield the details. Did the famous CEO know or care?\n\n\"All those anti-globalization-folks are saying exactly that.\"\n\nRight: so when Amnesty International says that working conditions in China are inhumane they're just part of some anti-globalisation brigade of protectionists?\n\n\"And what makes you think that by default working-conditions outside \"west\" are inferior?\"\n\nPerhaps working conditions are really great in China by default and only the really bad places get dredged up in the press, but then you have to ask who is doing business with these really bad places. Hint: there's usually a western interest in this kind of story because, sadly, it wouldn't be newsworthy otherwise.\n\n\"I HAVE seen Nokia's factories in China. They are not that different from their factories in Finland.\"\n\nWell, if we can take your word for it, then it's reassuring to know that Nokia maintains some ethical discipline in this regard. But my general point about the \"lowest cost\" doctrine still stands, because some corporations and their shareholders have no incentive to consider the wider costs. It was exactly that I objected to, regardless of how you endeavour to rewrite my position."
    author: "The Badger"
  - subject: "Re: Convenient Strawman"
    date: 2008-01-30
    body: "Do I have a clear conscience when my electronic waste is shipped to India for recycling purposes? Yes, I do. Because the alternative of having an unhealthy job for the casteless Indian is living on subsistence farming. And time and time again, subsistence farmers find their live so miserable that any escape from it, they take, even if that means moving to the most hazardous slums..."
    author: "Bart"
  - subject: "Re: Convenient Strawman"
    date: 2008-01-30
    body: "\"Do I have a clear conscience when my electronic waste is shipped to India for recycling purposes? Yes, I do.\"\n\nIs that the \"globalisation is so *generously* sharing the wealth\" argument sinking below the surface? Hint: are the subsistence farmers arranging the shipping themselves or do the pixies magic the waste over to India?\n\n\"Because the alternative of having an unhealthy job for the casteless Indian is living on subsistence farming. And time and time again, subsistence farmers find their live so miserable that any escape from it, they take, even if that means moving to the most hazardous slums...\"\n\nI don't deny it. But the argument is, as always, that the scraps from the developed world's table are more than good enough for the developing world. Perhaps they're better than nothing, although to look at it as \"waste vs. subsistence farming\" ignores plenty of other factors, but it's so convenient to spin the whole issue as \"they're getting steadily richer thanks to us\" (and poisoning themselves and others, incidentally) and for those making the sales pitch to avoid thinking about what little they've actually had to give up themselves."
    author: "The Badger"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "And you think poor people in Romania will get rich from salaries from Nokia ? They go there because they can pay almost nothing for the same work. Moron."
    author: "Sebhelyesfarku"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "\"And you think poor people in Romania will get rich from salaries from Nokia ? They go there because they can pay almost nothing for the same work.\"\n\nHere's a news-flash for you: living-expenses are different in different parts of the world. I have an average salary by Finnish standards. In Estonia I would probably be in top 10%. While my salary gives me a nice home 40 kilometers from Helsinki and average car (Year 1994 VW Passat Wagon if you must know) in Finland, in Estonia that exact same salary would probably give me a penthouse in Tallinns old town and a brand-new BMW.\n\nSee the difference?\n\nYes, by OUR standards, the salaries in Romania are poor. By THEIR standards, the salaries Nokia will pay are pretty good. And while that salary wouldn't get them far in Germany or Finland, it will get them VERY far in Romania!\n\nWhat exactly makes you think that OUR salaries are the global standard? What exactly makes you think that if salaries in some other country are lower, it means that those salaries are bad? What you need to do is to compare those salaries to the living-expenses in that particular country, instead of comparing them to some completely unrelated country!\n\nHell, we had similar situation in Finland few years ago. There are bunch of passenger-ferries that go between Helsinki and Finland. Quite a few of those are under Estonian flag and they have Estonian crews. Then the Finnish labour-union representing Finnish seamen said that those Estonians should receive similar salaries as Finnish seamen do. Estonians politely pointed out that if they did that, then those seamen would have higher salaries than Estonias prime-minister has....\n\n\"Moron.\"\n\nWell, I haven't resorted to personal attacks here. But considering that you don't understand even the most basic things about global economy and living-expenses, it begs the question: who exactly is the \"moron\" here?"
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "\"Yes, by OUR standards, the salaries in Romania are poor. By THEIR standards, the salaries Nokia will pay are pretty good. And while that salary wouldn't get them far in Germany or Finland, it will get them VERY far in Romania!\"\n\nWill it? Probably not. If they get too rich, they'll stop working or look for better jobs and  Nokia will lose the economic advantages; so it's in Nokia's interest to keep those wages low, keep them just-above-starvation, ready to be exploited and tapped at will (better if to put pressure on other countries' salaries). \n\nI'm not saying that globalization is always bad, i'm just trying to tone down your ideology of freetrade a little bit. "
    author: "YourAverageJoe"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "\"Will it? Probably not\"\n\nProbably yes. All I need to do is to look at Estonia. Their salaries are a fraction of Finnish salaries, yet they seem to be driving better cars than we are.\n\n\"If they get too rich, they'll stop working or look for better jobs and Nokia will lose the economic advantages\"\n\nSo let me get this straight: you are arguing that if Nokia pays the employees in Romania good salaries, those employees will leave Nokia and look for some other job instead?\n\nO.... K.... \n\nHell, maybe you are right. I too try to find the job with worst salaries. If my employer gives me too much money (those bastards!), I will probably start looking for another job...\n\n\"so it's in Nokia's interest to keep those wages low, keep them just-above-starvation\"\n\nSo, you are basically making the claim that the Nokia-employees in Romania are barely earning enough money to keep them from starving to death? \n\nO.... K....\n\n\"i'm just trying to tone down your ideology of freetrade a little bit.\"\n\nAnd I'm trying to beat some sense in the heads of those \"only jobs for westerners!\"-folks....\n\nSeriously: reading some of these comments makes me feel like I have entered the bizarro-world, where keeping people poor is the right thing to do."
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-31
    body: "\"If they get too rich, they'll stop working or look for better jobs and Nokia will lose the economic advantages\"\n\n\"So let me get this straight: you are arguing that if Nokia pays the employees in Romania good salaries, those employees will leave Nokia and look for some other job instead?\n\nO.... K.... \"\n\nReply of the week.  How can people say that stuff with a straight face?  "
    author: "AS"
  - subject: "Re: a company is a company is a company"
    date: 2008-02-04
    body: "Please do not mind 'Sebhelyesfarku' (aka Scabs-on-his-Dick), I Googled him -- he is just another kakistocratic, coprolalic wanderer of cyberspace, still lives in a tiny room of his mother's dank apartment in Budapest. (see Tarr B\u00e9la: \u00d6szi Almanach - The Almanac of Fall)"
    author: "Azazello"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "I wonder whether there's a company in Germany that's \"noble\" - pointing at Nokia while hundreds of companies are cashing in and leave is a bit over the top, especially given that Nokia still invests heavily in Germany (BenQ anyone?)."
    author: "harryF"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-28
    body: "I'm thinking about those 24.000 piss-poor people in Romania who are receiving jobs thanks to Nokia, and who receive a possibility to pull themselves from poverty. Maybe you should think about them as well?\n\nOh, wait, I forgot: Only workers in rich western countries matter. They are the ones we see in television. People in those distant poor countries do not matter, since they don't stick their faces in front on TV-cameras.\n\nSeriously: People in Romania are people as well. Why do you think that Germans deserve these jobs from Nokia, and not Romanians?"
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "I think that some people here (such as the poster you are replying to) are thinking only of the examples where companies have outsourced to countries with cheap labour and ended up running sweat-shops. Just because a company is outsourcing somewhere where labour is cheaper, doesn't necessarily mean the workers will be treated badly. Until someone can point to actual examples of where Romanian workers are being mistreated by Nokia in this situation, the whole argument is just a little bit misguided."
    author: "Paul Eggleton"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "I think few people got a problem with Nokia building up a factory in Romania. But why does building up a factory in Romania exclude continuing a profitable factory in Bochum? Companies should take social responsibility for their workers. Fact is that Nokia had insane profit margins this year, so they don't need to close factories. They can expand, and it's nice to see that they do it in Romania, but closing down other factories where the workers get higher loans, only to _raise_ their __increase__ in profit is what is subject to criticism (which is justifiable i think)."
    author: "blubb__"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "\"But why does building up a factory in Romania exclude continuing a profitable factory in Bochum?\"\n\nBecause then Nokia would have excess supply. And because while Bochum might be profitable, it's still not efficient. Figures released by Nokia state that Bochum represented over 20% of their expenses while only representing 6% of their production. Numbers like that are not healthy.\n\n\"Companies should take social responsibility for their workers.\"\n\nAnd Nokia has done that: They offered employees of unprofitable television-factory good jobs for years as cell-phone builders. If you look at Bochum and Nokias employees there, you would notice that they are several millions of euros in profit, thanks to Nokia.\n\n\"Fact is that Nokia had insane profit margins this year, so they don't need to close factories.\"\n\nSo you believe that if company makes even one cent in profit, they should not close any factories?\n\nSure, Nokia has no dire NEED to close Bochum. But fact remains that they could do the EXACT same work somewhere else while paying fraction of what they are paying right now.\n\nTo compare to your situation: You are probably doing OK in your life, right? Since you are doing OK, then surely you could pay five times as much for your monthly haircut? I mean, it would not bankrupt you, and it would surely benefit the barber, right? Would you? I bet that you would not. You only pay what you need to pay. But even if you were willing to pay extra, then as you spread that mentality to other areas as well, you would suddenly notice that you are not doing so well anymore, since you are wasting money needlessly.\n\n\"They can expand, and it's nice to see that they do it in Romania, but closing down other factories where the workers get higher loans, only to _raise_ their __increase__ in profit is what is subject to criticism (which is justifiable i think).\"\n\nIt's not justifiable. Like it or not, we are not entitled to jobs. Not from Nokia or from anyone else for that matter. When company sets up a factory, they are not required to operate that factory until the end of time.\n\nAnd like it or not, a purpose of a corporation is to earn profit, period. Corporations are not welfare-organisations. If you want \"social responsibility\", corporations are the wrong place to turn to. It's the job of the government to look after it's citizens, and even then some people could say that it's the responsibility of each individual to look after themselves. But in any case, calling for care and comfort from the corporations is the absolutely wrong thing to do. If the government want to, they could give the unemployed some kind of \"safety-jobs\", but demanding that corporations do it instead, is totally wrong."
    author: "Janne"
  - subject: "Re: a company is a company is a company"
    date: 2008-01-29
    body: "\"Because then Nokia would have excess supply.\"\n\nHow can you know that? And even if so, Nokia could build a factory with a smaller volume in Romania at first, and optionally extend it later on. \n\n\"So you believe that if company makes even one cent in profit, they should not close any factories?\"\n\nNo, I don't, and neither did I say so. I believe that a company which has _such_ a massive profit margin should not close one factory just to build up another one at a different location.\n\n\"To compare to your situation: You are probably doing OK in your life, right? Since you are doing OK, then surely you could pay five times as much for your monthly haircut?\"\n\nThis comparison has a major flaw, because your talking about service supply here, and voluntarily changing from something cheaper to something more expensive. Even in that scenario, the question is wrong. The right question would be: Would you change your barber if anyone else makes you an offer for a cheaper haircut, knowing well that he will have to close down his barbershop? And the answer would be 'No, I wouldn't!\". And in this case I'm not even an employer, I'm only accepting a service. As an employer you have more responsibilities to take. "
    author: "blubb__"
  - subject: "That was bound to happen, wasn't it?"
    date: 2008-01-28
    body: "Hello,\n\nreally no surprise here, except for the buyer. I may say, I love that the Trolltech management prepared this buyout so well and everybody at Trolltech can be very proud about them. The GPLv3 release, the Webkit moves, etc. it all pays out.\n\nI think it's very good for us that it's Nokia and not e.g. Novell. That's a company that will indeed care most for the embedded market and leave the desktop simply alone. And those who say that they may stop to develop X11, do not understand that Qt enormously benefits from the free testing and publicity they get through KDE.\n\nNokia will not hide Qt, it will continue share it with its concurrents. That's simply not a useful thing to do, as they would then miss out on the impact of community contributions, which save a lot. Mind you, that's a hardware company, who would simply like to have better software to sell its hardware and little more. They won't mind if I can run 3 office suites on the phone, if it makes me buy their phone.\n\nWell, who knows, probably it means more free phones for developers. Like the one Aaron got, but for every KDE developer. :-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: That was bound to happen, wasn't it?"
    date: 2008-01-28
    body: "Gotta love the blind optimism, hehe."
    author: "moo"
  - subject: "Re: That was bound to happen, wasn't it?"
    date: 2008-01-28
    body: "Yes, all the knee-jerk, sky-is-falling pessimism is much cooler."
    author: "dolio"
  - subject: "FreeQT and new features"
    date: 2008-01-28
    body: "What is unclear for me is what \"new features\" means. What if Nokia will start to release a version per year with some totally unrelated and useless new features just to prohobit opening it under BSD ? So Qt will slowly get outdated and will die ? I hope I'm wrong !"
    author: "Anonymous"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "People, please note that it's Nokia we are talking about here, not Microsoft!"
    author: "A KDE advocate"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "The Bochum case proves that Nokia is not a Mark Shuttleworth company. They try to get money where they can, they happily took the subsidy offered for founding the factory at bochum and now they're moving to next sponsored place. I don't damn them because of that, in a highly competizive market you cannot afford to be an angel. It just prooves that Nokia cannot be expected to do a step like this without \"planning something\" to get some worth out of it."
    author: "dirks"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: ">The Bochum case proves that Nokia is not a Mark Shuttleworth company.\n\nThe fact that Canonical is registered in the a tax-haven like the Isle of Man would imply that \"Mark Shuttleworth companies\" are as concerned with the ultimate bottom line as any other for-profit concern, not that the point is really relevant in the first place."
    author: "elsewhere"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "Good news is that you are completely wrong!\nAnd even better news is that there is no bad news (since we already established that you being wrong is good)\n\nNokia engineers don't release Qt, Trolltech engineers do. Trolltech engineering teams stays completely unchanged. As long as they're there you can be absolutely certain that nothing in no way will change and Qt will continue rocking (more than ever).\n\nAnd what if all the dedicated Trolltech engineers like Lars, Simon, Paul, Andreas, Brad, Warwick, Matthias or Espen to name just a few quit you ask? Well in that case you will have more important things to worry about e.g. flying pigs, frogs raining from the sky, death of the firstborn and the like.\n"
    author: "Zack Rusin"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "> Nokia engineers don't release Qt, Trolltech engineers do. Trolltech engineering teams stays completely unchanged.\n\nBut can Nokia tell that engineers what to do or not do ? Set other not qt-related task or just stop paying them to work on Qt ?\n\n"
    author: "Anonymous"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "This week.   I've been through this type of thing before.  Two years from now Nokia's CEO quits, and the new CEO doesn't share the qt vision, so the trolltech offices are closed.   QT becomes BSD, but to Nokia it is just a write off.\n\nI'm not say it will happen that way.   The future is really hard to understand.  However wouldn't be the first time if it did."
    author: "Hank Miller"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "i bet you are. the same devs are going to develop qt even on the future, just under Nokia's umbrella."
    author: "kedadi"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "Perhaps they reduce commercial license prices ? ;)"
    author: "Hen"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "Commercial licences are actually cheap. You are so much productive with Qt that you will forget about the price. And renewals are about one third, IIRC, which is dead cheap.\n"
    author: "Pau Garcia i Quiles"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "HAHA. The lamest sales pitch I've ever heard."
    author: "money-hater"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-31
    body: "Yeah and your standard sales pitch is to pay expensive developer salaries for devs to spend longer creating projects with inferior toolkits.  That's at least as lame as his."
    author: "AS"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "Haha! Qt is one of the most expensive toolkits out there, which hinders its spread badly. I hope the BSD license will come into effect soon to get rid of the commercial license."
    author: "Budzes"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "if you wanna make money off something that uses qt, you have to give a bit back to trolltech... that seems pretty fair to me. those guys need to feed their families too."
    author: "Chani"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "But Nokia doesn't need to make money that way.  To tell the truth, based on the modest price they got QT for, it was never a very lucrative business model in the first place.  It was all Trolltech had, but Nokia makes their money selling phones, not software.\n\nSo, if they're serious about QT, and they want to encourage 3rd party developers, they can afford to lower the price significantly.  Better yet, LGPL it and start a revolution."
    author: "littlenoodles"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "Expensive but worth it.  Without Qt I wouldn't have a business because I can't develop software part time and still be competitive with any other toolkit.  So for me, it is pure win.  Qt license prices are fine the way they are (of course, I wouldn't say no to a decrease, but Trolltech has to remain profitable so they can continue to develop Qt)"
    author: "Leo S"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "I hope Qt doesn't become BSD - imho the GPL is far superior in terms of promoting freedom. If you want to allow leeching off of FOSS developers, you gotta love the Lesser GPL or the BSD licenses, but I prefer to have companies developing proprietary software contributing money to GPL software development. I hope you realize all the commercial applications being written with Qt pay for the salaries of people like Aaron and meetings like Akademy, right?"
    author: "jospoortvliet"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-29
    body: "You mean \"NOT DEVELOPPING\" products using Qt because it's GPL. Right?!\nCuz, apart some few exceptions, no company will risk it.\nIf you don't get it talk to the Linksys router folks."
    author: "merged"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-31
    body: "Precisely the point.  They prefer paying for the commercial license, thus \"pay(ing) for the salaries of people like Aaron and meetings like Akademy\".  "
    author: "AS"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "do you really think TT developers would put up with that?\nthe fact that they're confident about this news, and not running off to resign, makes me a lot more optimistic about it. :)"
    author: "Chani"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-28
    body: "\"those guys need to feed their families too\""
    author: "Psst"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-29
    body: "Yes, but that dosen't mean they'd be happy about bad news, they could feed their families before this after all."
    author: "Ben"
  - subject: "Re: FreeQT and new features"
    date: 2008-01-29
    body: "\"Chambe-Eng added that Nokia's decision to maintain Trolltech's existing dual-licensing model and continue feeding into the open-source community had been \"very key\" to Trolltech's agreement to the acquisition. According to Chambe-Eng, Trolltech has a \"very special status in both the Linux and Unix community at large [and now benefits from being] part of an organisation with much more muscle than [Trolltech] had so far\".\"\nhttp://news.zdnet.co.uk/software/0,1000000121,39292448,00.htm?r=1\nno worries! :D "
    author: "Koko"
  - subject: "Congratulations"
    date: 2008-01-28
    body: "Congrats to Nokians and the trolls! I think time is the only answer to the plethora of questions springing into the Free Software community about the acquisition. I'm just concerned that it might create some hard feelings between the GTk+/GNOME people and us, the Qt/KDE community. I hope everybody will try to be open-minded rather than engaging in spiral toolkit flame wars. \n\nNokia: Now it's your turn to show a true commitment to Free Software and open standards or else you'll be risking a wave of Novell-like infamy.\n\nQt: But never turn a blind eye to what might your freedom just because it's being done by your parent company.\n\nKDE: On to more ubiquity and achievements!"
    author: "A KDE advocate"
  - subject: "Re: Congratulations"
    date: 2008-01-28
    body: "* to what might hurt your freedom"
    author: "A KDE advocate"
  - subject: "Re: Congratulations"
    date: 2008-01-29
    body: "I think is a good for kde and linux. If every thing will go ok kde and qt will became more, and more popular(if Nokia LGPL QT would be perfect), if qt will die we will have only one gui toolkit....\n"
    author: "alonso"
  - subject: "what if !"
    date: 2008-01-28
    body: "what if nokia decide that qt will be LGPL ( just to make it more attractive to developers, think android here :-)\ni hope they are smart enough to do it !\n\nps: please don't flame me here, just a user wondering to be happy by the move or not"
    author: "djouallah mimoune"
  - subject: "I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "When Nokia really buys Trolltech, I will Switch to GNOME. KDE may be better, but having to do anything with Nokia is simply not acceptable for me."
    author: "blueget"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "Psst - Nokia are a major proponent of GNOME and GTK."
    author: "Anon"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "Do they own GTK? No..."
    author: "Psst"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "\"Do they own GTK?\"\n\nNo. Red Hat does, and they are the primary bunch of people who decide what happens and what code goes into it. Same difference."
    author: "Segedunum"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "> \"Do they own GTK?\"\n>\n> No. Red Hat does...\n\nDon't think so. Lot's of the source files don't have the redhat (c)."
    author: "wtf123"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "\"Don't think so. Lot's of the source files don't have the redhat \"\n\nWho puts the code in the repository decides ultimately. You're free to fork GTK, as you are Qt, but that isn't an easy thing to do."
    author: "Segedunum"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-29
    body: "\"You're free to fork GTK,\n as you are Qt, ...\"\n\nA GPLed library with a single copyright holder compared to a LGPLed library with lots of different copyright holders is quite another cup of tea. The second seems a lot more immune to take overs like this, and therefore - i think - more in the spirit of FOSS. Quite paradox.\n\n"
    author: "Wtf123"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-29
    body: "You didn't read the comment properly.\n\n\"A GPLed library with a single copyright holder compared to a LGPLed library with lots of different copyright holders is quite another cup of tea.\"\n\nWhen you think about how this works, there isn't really much difference at all. Those who write the code and are in control of the project decide, and in the case of GTK that is Red Hat ultimately. If a Red Hat maintainer decides not to accept your patches, as has happened, then you're pretty much stuck. You are free to fork it, as you are with Qt, but you have to replicate that infrastructure and manpower whatever.\n\n\"The second seems a lot more immune to take overs like this, and therefore - i think - more in the spirit of FOSS.\"\n\nIn what way? If you're not happy then you will still have to fork the project, and that is the barrier."
    author: "Segedunum"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-30
    body: ">> \"A GPLed library with a single copyright holder compared to a LGPLed library \n>> with lots of different copyright holders is quite another cup of tea.\"\n\n> Those who write the code and are in control of the project decide, and in the \n> case of GTK that is Red Hat ultimately. If a Red Hat maintainer decides not to \n> accept your patches, as has happened, then you're pretty much stuck. You are \n> free to fork it, as you are with Qt,..\n\nOf course Redhat has some control, but only as long as everybody is happy with their work. If Redhat was being taken over, moving in a totally wrong direction, it wouldn't matter for GTK+ that much. Others - even companies - would continue the work with no degradation in rights, as the only license in the game is LGPL (Which more or less fulfills everyones needs). \n\nAnything Redhat or anyone else does for GTK+ is instantly and only creating public value. There is no additional private value for the copyright owner. The situation with Qt is quite different. For instance the XFree86 fork wouldn't have worked that well with a license model like Qt.\n\n>> \"The second seems a lot more immune to take overs like this, and therefore - \n>> i think - more in the spirit of FOSS.\"\n> In what way?\n\nBecause i think depending on the fate of a single company is quite problematic for a FOSS project like KDE. People wouldn't worry about this Nokia deal that much if Qt were LGPLed.\n\n\n\n\n"
    author: "Wtf123"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-30
    body: "I don't really see what the difference here is. Whats there to worry about exactly?\n\nSuppose nokia does decide to render Qt useless for the next version in some way.\n\n1) Close the source\n\nWell except there's the poison pill, the last release of Qt then becomes BSD. I can only hope nokia would be so ignorant of what would happen.\n\n2) Anything else...\n\nWell, the current version is available under gpl2/gpl3, there's absolutely no reason a fork could not be made and call NotNokiaQt or whatever.\n\nSo I really don't see the problem. The only potential annoyance here would be the lack of fulltime devs working solely on Qt. A real loss for sure, but still recoverable with a community such as KDE I'd hope. As someone else stated, replacing the manpower is the real problem should Bad Things (tm) happen.\n\n"
    author: "Random Commenter"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-30
    body: "3) Nokia plays nicely and embraces KDE, invests money in improving KDE, writes closed source apps for KDE (it's privileged to do so), ports Qt and KDE to its selected platforms... Will competitors use KDE? Will the community work force continue with the same enthusiasm? \n\nMy point is that with the current Qt licensing model - extra value for the owner of trolltech - things will always be delicate...\n"
    author: "Wtf123"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-02-01
    body: "\"Will competitors use KDE? Will the community work force continue with the same enthusiasm?\"\n\nI don't see why not. The source code is there, people are free to use KDE and Qt for things they want, and not to use KDE and Qt for things they won't want.\n\n\"My point is that with the current Qt licensing model - extra value for the owner of trolltech - things will always be delicate...\"\n\nI really, really wish everybody had top notch development tools and libraries for free, and that everything was magically and invisibly moved forwards. Alas, the world doesn't work like that."
    author: "Segedunum"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-02-01
    body: "\"Of course Redhat has some control, but only as long as everybody is happy with their work.\"\n\nThey don't really have a choice, unless they want to fork it.\n\n\"If Redhat was being taken over, moving in a totally wrong direction, it wouldn't matter for GTK+ that much.\"\n\nIf Red Hat was taken over and they didn't like some of the additions people were proposing to GTK, or any other project they had influence over, you can bet that patches wouldn't be accepted. You can see some examples where Red Hat's people have categorically stated that if Gnome were to start including Mono as its base, Red Hat would for Gnome. Presumably, they wouldn't accept patches for that purpose into the main GTK repository either.\n\nThere's really no difference at all.\n\n\"Anything Redhat or anyone else does for GTK+ is instantly and only creating public value. There is no additional private value for the copyright owner.\"\n\nYou don't understand this. Trust me. If people were proposing patches to GTK or any other heavily Red Hat influences project, and those patches threatened Red Hat's commercial value somewhere, they would simply not be accepted. People have levelled this accusation at Nokia with Qt, but the same applies, and has applied, to Maemo.\n\nYou don't understand that you don't necessarily have to be the copyright holder to have major influence over an open source software project at all.\n\n\"Because i think depending on the fate of a single company is quite problematic for a FOSS project like KDE.\"\n\nIt's funny that a lot of people claim that for other projects, corporate backing of companies is a good thing.\n\n\"People wouldn't worry about this Nokia deal that much if Qt were LGPLed.\"\n\nSigh........... I wish people would drop this LGPL crap as if it is the answer to everyone's prayers. The license model of Qt means that an organisation has the motivation and will to drive the software continually forwards. This is very important for a development library. When you compare it to GTK and other more 'liberally' licensed development libraries, you can't see an awful lot happening with them.\n\nWe go back to the reasons why KDE used Qt. No, not everyone gets everything they want, but creating a desktop with all the applications development tools and infrastructure needed is damn hard. Everything depends on everything else. Do you create your own development libraries, taking on all that entails, or do you lean on a receptive company or organisation where you all get something in return?\n\nGiven the fact that GTK and various other liberally licensed development libraries are a long, long, long way behind where we need to be if we want a good looking, functional desktop that fulfils all the requirements the KDE developers want of it, you'd have to say that KDE has made the right choice."
    author: "Segedunum"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-30
    body: "I'd like to see RMS comment on this Nokia/Trolltech (Qt) situation."
    author: "asdx"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "Psst. Nokia != Qt != KDE. There are minds and eyes at each of them. Who are you? KDE doesn't need you. Kthxbai. "
    author: "A KDE advocate"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "KDE and QT were on a really good way, but Nokia won't do anything good to us all. Nokia has now control over Qt, but they can't really get control about GTK."
    author: "blueget"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "I do believe that Nokia needs to do more to show true support for Free Software and open standards (and yes I'm aware of Nokia stance on the HTML5/Ogg issue and patents) but Nokia *is* developing Free Software *now* Acquiring Trolltech means they will develop *more* free software.\n\nAs far as \"control\" concerned, I think you're referring to Free Software vendor and community relationship management. Trolltech has been a prominent example of how a company truly supports Software Freedom: GPL licensed (not 3!), working in the open (now with a recent public issue and a development blog) and maintaining a vibrant, active and very friendly community. In the recent conference at Googleplex, Trolltech stated that much of their publicity and half of their customers were routed from KDE. So do you think they are going to give this all up in a blink of an eye?\n\n"
    author: "A KDE advocate"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "* now 3!\n\n(I hate confusing those typos!)"
    author: "A KDE advocate"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "> KDE doesn't need you.\n...but KDE needs Qt. Haha.\nKDE without Qt would be just random c++ gibberish. \"Qt != KDE\" == LOL\n\nIt's gonna be a fun ride.\n\"The fall of KDE. It started in early 2008 when...\""
    author: "Psst"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "Ever hear of the FreeQT Foundation?  If Nokia screws up, Qt gets the BSD license across the board.  No downfall there, the KDE devs will just pick it up (or someone else interested)."
    author: "Chmeee"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "Yes. Everything will be awesome. This buyout is good after all.\nWait a second..."
    author: "Psst"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "I'm talking people-wise not code-wise. And it seems like the old KDE-4-is-just-hype kind of trolls are back around the block. Well screw you, KDE is as strong as it never has been and people who develop it care about it way much more than you can dream of. "
    author: "A KDE advocate"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "..taking that for granted still makes me wonder why a lot of grave bugs still haven't been wiped from trunk (except from translation commits, kdelibs SVN is rather quiet).. if I just knew how to debug problems lying somewhere in communication between processes, hell I would try.."
    author: "eMPee584"
  - subject: "Re: I'll Switch to GNOME if this is really true"
    date: 2008-01-28
    body: "Qt is under GPL which means that KDE can fork it if Nokia does not develop it so that it is good for KDE. And, as it is under GPL, I do not think that the copyright owner really matters."
    author: "Grosy Daniel"
  - subject: "Just curious"
    date: 2008-01-28
    body: "Hey,\n\nI was wondering, does anyone know how much does this cost Nokia? I don't know much about big money, but is this just a small investment for Nokia or is this something really big for Nokia. Ie. how much bigger is Nokia then Trolltech?\n\nThx,\nLeaves"
    author: "Leaves"
  - subject: "Re: Just curious"
    date: 2008-01-28
    body: "Well think something, in the last numbers comming in, Nokia has 40% of the cell phone market in the world, that means it's the single largest cell phone maker in the world..."
    author: "Lars G"
  - subject: "Re: Just curious"
    date: 2008-01-28
    body: "Nokia profits Q4 2007 - 2.8 billion $\nEstimated TrollTech value with 16NOK per share price - 150 millions $"
    author: "m."
  - subject: "Re: Just curious"
    date: 2008-01-29
    body: "Which is peanuts. Everything under $1billion is peanuts for companies the size of Nokia."
    author: "YourAverageJoe"
  - subject: "Striking back at Google"
    date: 2008-01-28
    body: "This guy has another theory on why Nokia went for Trolltech, and I think if you understand what Nokia feels about Google's Android platform, you'll likely agree with him. Read his opinion at http://openandroids.com/2008/01/28/nokia-to-acquire-trolltech/"
    author: "planner"
  - subject: "Re: Striking back at Google"
    date: 2008-01-28
    body: "I would fork Qt in Google's position, this would would make a continued free (desktop) development more realistic. Then Google could merge Nokia's code from time to time (if they will release something useful).\nI think Nokia will concentrate on embedded/mobile devices if anything and won't open the key technologies for their competitors. "
    author: "Pessimist"
  - subject: "Re: Striking back at Google"
    date: 2008-01-28
    body: "Finally someone understands the real reason.\n\nThere is only couple of company's Nokia is worried about. Those are Google and Microsoft."
    author: "anonymous"
  - subject: "My reaction"
    date: 2008-01-28
    body: "I've read all messages here so far, I've stoped to think... and by all means my initial perspective was not changed. Simply put: \"Oh crap, this is bad!\""
    author: "Iuri Fiedoruk"
  - subject: "Re: My reaction"
    date: 2008-01-28
    body: "Bad and sad. What's also sad are all the responses from the KDE people.\nI didn't know that KDE was lead by a bunch of Miguel de Icaza's. Speaking of... I remember a lot of people complaining about the inclusion of .NET into GNOME and that many GNOME users would switch to KDE as a result. What now? Now the foundation of KDE is controlled by yet another greedy evil multi-billion corporation which doesn't even has an interest in KDE on the PC desktop.\nSo GNOME is crap because of MS tech in it, GTK is crap because it's crap and now KDE is virtually controlled by a massive evil entity.\n\nSad times for the free software desktop."
    author: "thruth"
  - subject: "Re: My reaction"
    date: 2008-01-28
    body: "I hope you can back your statements about KDE developers endorsing vendor-lock and patent-encumbered solutions because otherwise your just spreading lies and FUD. IBM is one example of a \"massive entity\" yet it does develop proprietary and Free Software platforms, side by side. "
    author: "A KDE advocate"
  - subject: "Re: My reaction"
    date: 2008-01-28
    body: "> I hope you can back your statements about KDE developers endorsing vendor-lock and patent-encumbered solutions because otherwise your just spreading lies and FUD.\n\nOkay. It's known that Nokia advocates software patents. The buyout of Qt screams \"anti-competitive\" and vendor lock-in is probably next on their list. Now the fact that none of the KDE guys worry one single bit about this travesty that's going on here leads to much concern. I think a comparison with de Icaza is not too far off here.\n\n> IBM is one example of a \"massive entity\" yet it does develop proprietary and Free Software platforms, side by side. \n\nIBM is a patent troll and one of the worst corporations ever:\n\nhttp://slashdot.org/article.pl?sid=08/01/22/0258213\nhttp://it.slashdot.org/article.pl?sid=08/01/24/049246\nhttp://yro.slashdot.org/article.pl?sid=08/01/22/1828245\nhttp://en.wikipedia.org/wiki/IBM_and_the_Holocaust\n\nThey don't really give a crap about free software and its philosophy. It's just another way to make profits.\nNot a good example."
    author: "thruth"
  - subject: "Re: My reaction"
    date: 2008-02-02
    body: "A company should always try to maximize profits! - Otherwise they are inefficient!\nIBM is motivated by profits! - And why is that bad?\n\nImprovements on free software is in public interest, controlled by a \"massive entity\", or not.\n\nIn this Nokia-case though, a fork should be made, and maintained aside Qt."
    author: "Daniel Skov Klejnstrup"
  - subject: "Re: My reaction"
    date: 2008-01-28
    body: "Yeah, it's really sad. I wouldn't have a problem if it wasn't Nokia who now owns TT/Qt. A well known GTK/GNOME Company, enemy of free software standards (OGG, for example) and supporter of software patents now controlls the best toolkit on the free software market."
    author: "blueget"
  - subject: "Re: My reaction"
    date: 2008-01-28
    body: "Nokia is not a GTK company.  They use GTK, but they have also publicly complained that GTK has no roadmap and is lacking in several areas.  Much more likely is that they will now shift their focus to Qt for their products.  Much cheaper than trying to get GTK up to snuff."
    author: "Leo S"
  - subject: "Motivation"
    date: 2008-01-28
    body: "Why should Nokia actually buy Trolltech?\n\nWhy not fund a project or cooperate?\n\nWhat is the actual motivation here for Nokia, what do they actually get out of owning Trolltech?\n\nCompanies the size of Nokia get run by accountants and lawyers.\n\nI hope it all works to the good."
    author: "Martin "
  - subject: "Re: Motivation"
    date: 2008-01-28
    body: "Nokia is a corporation, they don't want to cooperate, they want to control."
    author: "carlo"
  - subject: "Re: Motivation"
    date: 2008-01-28
    body: "shouldn't it be called controlation or so then..?!"
    author: "eMPee584"
  - subject: "Re: Motivation"
    date: 2008-01-28
    body: "There motivation might be, as suggested above, a fear of Google's android platform. They, despite their packaged letter, do not want necessarily to help us(KDE) unless if there is an apparent benefit to their business. It is all game theory xD "
    author: "danborne"
  - subject: "Re: Motivation"
    date: 2008-01-28
    body: "Nokia buys Trolltech because they are expanding to software market. It's hard for Nokia to get more then 40% of world's mobile market so they have to get more ways to make money. Trolltech gives them one good way to compete with Microsoft and Google.\n\nStock owners want more money. That's the only meaning for publicly traded company."
    author: "anonymous"
  - subject: "Answer: FORK!"
    date: 2008-01-28
    body: "At the slightest sign of QT becoming a piece of shit like Symbian, we should fork QT and the hell with Nokia et all.\n\n"
    author: "karl"
  - subject: "Re: Answer: FORK!"
    date: 2008-01-28
    body: "Slightest sign: http://dot.kde.org/1201517986/\n\nYou maintain it."
    author: "you just got nokia'd"
  - subject: "Re: Answer: FORK!"
    date: 2008-01-28
    body: "As a commercial developer using Qt, I am not happy with the Nokia deal.  For free software, forking may not be a bad idea if Nokia drives Qt down a bad path... but for companies that sell products based on Qt this will not work.  My company will not be able to sell our product developed with the GLP version.  Since I do not use QTopia, this will hopefully not effect me... but we have used 3rd party products that have later been bought by a competitor.  I could write pages about how bad this is.  Bottom line, I feel bad for those using QTopia... I just hope this doesn't affect regular developers and the KDE community."
    author: "Brandon P."
  - subject: "Re: Answer: FORK!"
    date: 2008-01-29
    body: "Thanks for posting.\nThanks for the first hand experience too. I was wondering what commercial developers think of the deal."
    author: "Richard Lionhard"
  - subject: "Your average user won't care"
    date: 2008-01-28
    body: "I personally believe the tidings of woe are overblown.  You're average desktop user does not particularly care about underlying technologies and who owns what - just ask the millions of Windows users out there.  People just don't care, and why should they. The vast majority of the target market out there looks for useful solutions to day-to-day productivity needs - issues of ownership and licensing isn't really part of the equation."
    author: "Antonie Fourie"
  - subject: "Re: Your average user won't care"
    date: 2008-01-28
    body: "Bla bla bla average joe.\n\nAverage joe doesn't care, it's true, but people are worried (me included) that Qt development stalls or stops worrying about the desktop. Qt has been advancing in big steps on the latest releases, and I guarantee that although \"average joe\" doesn't care, some cool technologies that would make it to qt never do, and as such never make it to KDE, and that \"average joe\" can see.\n\nWithout Qt, kde wouldn't be where it is today (also the reverse is true), and would you say that isn't visible?\n\n(I might add that after seeing so many important kde dev's and trolltech members speaking for this acquisition, my fears are starting to settle. Just tired of average joe trolling.)"
    author: "AC"
  - subject: "No surprise here"
    date: 2008-01-28
    body: "The first thought that came to mind was - about time too. Let's see what Nokia gets here - a common SDK for their desktop and mobile/PDA software, a mature and proven app/telephony platform to replace the badly outmoded Symbian OS, control over the feature and portability roadmap of this platform (which they don't have with GTK+), and instant access to a third-party developer base (Qt, KDE and Qtopia developers). The price they are paying for all this is basically lunch money for a company like Nokia.\n\nI don't see why they would mess with Trolltech's relationship with KDE, since KDE put Qt and Trolltech on the map in the first place and nobody at TT is going to forget that. It's the best advertising for their platform possible. In the worst case, there is the FreeQt agreement, and KDE devs can fork the GPL version of Qt anytime."
    author: "taj"
  - subject: "Re: No surprise here"
    date: 2008-01-28
    body: "As we can fork Suse, right? After Novell bought Suse it got polluted with Gnome/GTK technology and ideological decisions against QT and KDE took place.\n\nWill Nokia be another Novell?"
    author: "gerd"
  - subject: "Re: No surprise here"
    date: 2008-01-29
    body: "That is always a danger when we are depending on a for-profit company to maintain a critical piece of software, whether Nokia is involved or not. If the \"community\" thought it was important enough to fork SuSE, it would. That it did not is a sign that all the folks who think it is important are also the folks who are unwilling to put in the work. Too bad.\n\nThis same issue applies with Qt. If the KDE \"community\" fails to fork and maintain Qt in the case of hostile behaviour from Nokia, then it is the community's problem and not that of Nokia."
    author: "taj"
  - subject: "Wow... People need to relax a bit and think"
    date: 2008-01-28
    body: "Everyone here needs to take a breath.\n\nNokia has good reasons for wanting Qt and good business motivation to use it and develop it.\n\nNokia develops: Phones, Smartphones, PDAs, UMPCs, Embedded devices, Set-Top Boxes and a raft of other devices of various sizes and configurations. This will get worse with WiMAX devices no longer even being recognisable phones at times.\n\nPhone makers used to managed their own single OS stack across their entire range, and they had some control. Now they are facing disperate platforms, with differing OSs, because the form factors of their products vary so much. That's a killer for:\n-Tech Support\n-Programmer team requirements for each platform\n-Cross-platform services and apps\n\nThe current situation is costing them way too much, they can't support it all, they can't provide a distinctive 'Nokia look and feel' anymore, and they can't add their own services and apps that are unique to their phones and allow them to distinguish themselves from the competition and add value to their devices.\n\nIf you sell someone a phone with a special web search application that is form-factor suitable and charge 1p a search, you get their initial money and an ongoing fee, essentially. Nokia would like that, but it needs a client application to make it work on all their platforms.\n\nQt makes that happen.\n\nNokia wants desktop Qt to continue as it is, because sooner or later, UMPCs and all the rest make phones look like desktop machines with different form factors... Code will just work across all scales of devices. This is the ongoing power of Linux and Qt, taking the same apps from phone to PDA to Laptop to Desktop to Server to Mainframe. \n\nThis is the future, and Nokia just ensured that they are part of that future.\n\nBut you should definitely expect some more Qt/Embedded development in the near future :)"
    author: "Luke Chatburn"
  - subject: "Re: Wow... People need to relax a bit and think"
    date: 2008-01-29
    body: "The first sensible comment I\u0092ve read. :D"
    author: "Gareth F "
  - subject: "The End"
    date: 2008-01-28
    body: "This is the end\nBeautiful friend\nThis is the end\nMy only friend, the end\n\nOf our elaborate plans, the end\nOf everything that stands, the end\nNo safety or surprise, the end\nI'll never look into your eyes...again\n\nCan you picture what will be\nSo limitless and free\nDesperately in need...of some...stranger's hand\nIn a...desperate land\n\n..."
    author: "Jim"
  - subject: "Congrats to the closed source world!"
    date: 2008-01-28
    body: "Now, a bad case in open source community. Congratulations! \nActually, I think KDE are the better desktop solution on open source world. I use KDE the last 6 years, and not only on Linux. \nKDE can compete with Windows XP/Vista and MacOSX. That's really good! \nThis comment aren't just about \"open source defense\". Are about serious open source applications against closed source applications. \nWith this move, let's me say, IMHO, that's will be:\n1) Some developers will move away from Qt based applications if Nokia don't do a good job with Qt (like TT was done).\n2) Users go away because, with less developers, applications will be obsolete (bugs, lack of features, etc).\n3) More developers go away because don't have users interested and so on....\nSilently, with the time, this applications can be die. \nWho will concerns about a Qt BSD Licensed without a lot of applications using them? I think no ones. \nOf course, I really think that's Qt will continue to be distributed under GPL. And with some improvements, but oriented to Nokia business. \nTo me are simple: Qt and software development are the core-business of Trolltech. Mobile are the core business of Nokia. Improvements are made oriented by the core business.\nI can't think in a worst company to acquire TT... Oh, yes, may be M$?\nWith this, my previous congratulations to the closed source world: will be free of more good competitors! \n"
    author: "Anon"
  - subject: "Re: Congrats to the closed source world!"
    date: 2008-01-28
    body: "I knew Lolcats don't like this merge neither!\n\n"
    author: "carlo"
  - subject: "Re: Congrats to the closed source world!"
    date: 2008-01-28
    body: "This is a sad day for open source agreed. +1"
    author: "Richard Lionhard"
  - subject: "be optimistic!!"
    date: 2008-01-28
    body: "Well, Let us an opportunity to Nokia. Let's see what happens, but do not worry, KDE is open source and there are ways to continue its development independently.\n\nPessimism does not lead us to anything.\n\nLet us be optimistic!!!"
    author: "Jesus R. Acosta"
  - subject: "Proposal"
    date: 2008-01-28
    body: "Nokia is a great concern because of their agressive software patent promotion and lobbying in Europe through inhouse patent professionals as Tim Frain. I would prefer a patent pledge from Nokia which clearly indemnifies open source development from patent threats and an open patent reform platform, and a more sane corporate policy towards patent policy in Europe that endorses both a harmonization of substantive patent law in the EU through legislative means, full support of the community patent and a rejection of the costly EU-EPLA proposal from the Commission and the the EPLA proposal from the EPO."
    author: "Andre"
  - subject: "..."
    date: 2008-01-28
    body: "That was a major WTF news to me. Ages ago I read the majority of Trolltech shares were owned by current and former employees, so I'm surprised at the reported easiness Nokia was able to get the majority of Trolltech's shares. Nokia has a long history of heavy pro software patent lobbying. The Foundation for a Free Information Infrastructure (FFII) lists several worrying points related to Nokia at http://www.swpat.ffii.org/gasnu/kamni/index.en.html\n\n-Software Patents in Finnland: Between 1998 and 2003 the Finnish Patent Office (FiPO/FiPRH) did not follow the European Patent Office's (EPO) decisions to grant literal claims to information objects such as \"computer program product, characterised by ...\". In 2003 the FiPO suddenly rushed to grant such claims, although both the European Commission and the European Parliament had proposed not to allow them and the existing laws clearly forbid them. (...) Nokia owns about 70-80% of the finnish software patents at the EPO and is said to wield overwhelming influence on Finnland's politics. Nokia's patent department has been intensively lobbying for software patentability in Helsinki, Brussels and Strasburg.\n\n-Nokia und Software-Patente: Tim Frain, head of Nokia's patent department, is a \"permanent resident\" of the European parliament and has used every opportunity to ask politicians in Brussels and in Finland to support the European Commission's software patentability directive. He is present at conferences everywhere. He argues that small companies badly need software patents because otherwise their ideas might be stolen by large companies.\n\n-International Chamber of Commerce (ICC) and Software Patents: ICC's \"Intellectual Property Committee\", consisting of 240 corporate \"IP professionals\" from around the world, headed by Urho Ilmonen, Vice-President Legal of Nokia Mobile Phones Ltd, has vigorously defended the interests of the patent community in Europe. Their letters and statements are characterised by \"strong belief\" in the beneficiality of patents and disregard for the opinions not only of most ICC member companies but also of national member organisations such as the German Chamber of Commerce, which has pronounced itself against software patents and against the directive proposal.\n\nTrolltech putting all version of Qt under GPL v3 is a good sign, but I sure hope they are aware of Nokia's activity in the patent area and put in their merger contract that such activity no longer happens at Nokia (likely wishful thinking, especially considering Trolltech doesn't bother to mention the issue of software patent once in their numerous merger related articles/letters/FAQs linked)."
    author: "Datschge"
  - subject: "It's over"
    date: 2008-01-28
    body: "KDE already in the shit and now QT going there. It was nice while it lasted, but now it's gone.\n\nRIP KDE\nRIP QT"
    author: "Me"
  - subject: "Re: It's over"
    date: 2008-01-28
    body: "So what is your plan? Are you going back to Windows? GNOME? Sell your computer?\n"
    author: "christoph"
  - subject: "Re: It's over"
    date: 2008-01-28
    body: "hihiihihi, good point"
    author: "djouallah mimoune"
  - subject: "Re: It's over"
    date: 2008-01-30
    body: "I don't know about him, but forking Qt, removing all not-used features and building a very light desktop over it (think about not having kdelibs, just a bunch of qt programs) seems like a nice idea to me :)\n\nI would call it QuanTum 95 and be happy ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: It's over"
    date: 2008-01-31
    body: "One and a half of the above."
    author: "AS"
  - subject: "Bad taste in the mouth"
    date: 2008-01-28
    body: "This leaves a bad taste in the mouth. Particularly for people who are not paid for working on KDE. Who controlls the future of their toolkit and who benefits from testing, reporting bugs, writing applications...? That's quite important. KDE adds a lot of value to Qt. The fact that a single company is privileged in \"chanalizing\" this value might not be motivating. \n\nBSD'ing and LGPL'ing would help."
    author: "wtf123"
  - subject: "Message from Eirik Chambe-Eng and Haavard Nord"
    date: 2008-01-28
    body: "Meesage from Eirik Chambe-Eng and Haavard Nord to the KDE Community;\n\nSo long, and thanks for all the fish.\n\n"
    author: "Stephen"
  - subject: "sucks"
    date: 2008-01-28
    body: "This news really sucks.\n\nI can no longer depend on nokia for supporting the next gen linux desktop.\n\nIn few months all lead developers of QT will leave Nokia..."
    author: "ano"
  - subject: "The good news"
    date: 2008-01-28
    body: "now we are certainly going to have native DRM support for KDE 4.2!!!"
    author: "Stephen"
  - subject: "Google wasn't available??!!??"
    date: 2008-01-28
    body: "Google wasn't available?\nThey would at least let it be open source.\n\nYou guys sold out!!!\n\nHope your conscience eats you. Seriously! The whole open source world is watching and we will ridicule you once Nokia starts wrestling power and jobs away from you, one at a time..\n\nLet's see if development still continues at this great pace, or will slow down. Wonder if this will even cost you programmers, that don't approve of this merger.\n\n"
    author: "Richard Lionhard"
  - subject: "Re: Google wasn't available??!!??"
    date: 2008-01-28
    body: "Of course it is 'open source' - it is Free Software available under both the GPL2 and GPL3 licenses. If the Trolltech engineers didn't approve of the change I doubt very much if the takeover/merger would have gone ahead. In fact I'm certain it wouldn't have.\n\nAnyway, what gives you the right to think you speak for 'the whole open source world' with this silly scare mongering?"
    author: "Richard Dale"
  - subject: "Re: Google wasn't available??!!??"
    date: 2008-01-28
    body: "I'm not worried about Nokia taking any already developed software/ip away. That won't happen because of GPL.\n\nI'm worried about\n\na) Nokia firing a big chunk of Qt developers (you know open source does not work for free either..)\nb) Nokia essentially freezing development on the corporate side of Qt.\nc) if you're wondering look to Novell and what SuSE turned into. Novell fired tons of employees from SuSE.\n\nThis will cause:\na) development to come to a screeching halt/slowing\nb) Manpower being reduced which will make development come to a screeching halt.\nc) newly developed technologies that aren't released yet be closed source\nd) the open source Qt to fork and just become some hobbyist toy. \n\nJust cause open source software is free, doesn't mean there aren't programmers working on these project full time. Those are usually employed by commercial enterprises, and they have to eat too. You cannot make existing code proprietory but you can effectively stop active development. In the computer world, code that isn't maintained/actively developed/improved, dies off. Remember the computer world is chasing a moving target!!\n\n\nI hope in the end Nokia will be a good thing though...\nLet's just hope."
    author: "Richard Lionhard"
  - subject: "Re: Google wasn't available??!!??"
    date: 2008-01-29
    body: "\"a) Nokia firing a big chunk of Qt developers (you know open source does not work for free either..)\"\n\nTrolltech employ large numbers of the very best C++ programmers in the world and there is no way Nokia will just fire some of them as that would be just throwing money away. \n\n\"b) Nokia essentially freezing development on the corporate side of Qt.\"\n\nNokia doesn't just develop mobile phone software. The desktop side of Qt helps the development of the mobile version anyway as they are the same source. So it would make no sense to freeze development.\n\n\"c) if you're wondering look to Novell and what SuSE turned into. Novell fired tons of employees from SuSE.\"\n\nI don't agree this is a comparable situation. It won't happen because Trolltech's engineers are very valuable for further development of Qt, and for possibly building a complete dual licensed mobile phone stack based on Qt.\n"
    author: "Richard Dale"
  - subject: "Re: Google wasn't available??!!??"
    date: 2008-01-31
    body: "I agree with you.\n\nI just hope we're both right. \n\nHopefully Nokia will pour more money into the KDE project. (E$ 10,000 for Patronage is not a whole lot of money to a big company. Heck it's a tax write off. I hope Nokia donates more money than that. THey should also pay for travel expenses and webcast keynote presentations by Aaron Seigo.)\n\n "
    author: "Max"
  - subject: "Congrats!"
    date: 2008-01-28
    body: "Congrats to Trolltech for making a product that was worthy of such a large purchase!\n\nCongrats to the KDE community for testing (and, in some cases, fixing) such a great toolkit!\n\nAnd, of course, congrats to the future -- only Good Things will come of this."
    author: "Chris H"
  - subject: "Re: Congrats!"
    date: 2008-01-28
    body: "\"...large purchase!\"\n\nTrollTech bought for 155 Millons\nMySQL bought for 1 Billion\n\n\n"
    author: "Max"
  - subject: "Be realistic"
    date: 2008-01-28
    body: "Hello,\n\nAfter all that bitterness maybe something realistic. Announcement like that was inevitable. TT was just a small fish. Some surprise is buyer. But all that badmouthing of Nokia is an overreaction. Not that they don't deserve it but ALL corporations are behaving like that. For all big names in IT world you could tell ugly stories.\n\nAbout future: someone already wrote that. Nokia really needs crossplatform tool like Qt. With UMPCs, top boxes, more and more complicated smartphones demands of desktop and mobile world aren't so different.  *If* Nokia will not put special effort to crippling desktop version of Qt + laying off several hackers working on KDE nothing should really change. Maybe Aaron will quit PR role and spend more time on coding :)"
    author: "m."
  - subject: "Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "Nokia will push Qtopia (if they're going to use it on some of their mobile devices), but at the same time stop any further development of Qt (to harm Google or others which started to built their apps on top of Qt). Trolltechs manpower will be directed towards Qtopia or other software projects inside Nokia and shifted away from Qt. Some of the core Qt developers will eventually start to feel sad about this direction at some point and leave the company. \n\nYeah, Nokia is great.. big company... big money.. and unbeatable instinct how to ruin peoples motivation and failing all over with forward-looking projects. Selling the usual devices and being aggressive at making contracts with providers. That's their job, that's what these guys are used to... and that's it. \n"
    author: "Thomas"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "\"Nokia will push Qtopia (if they're going to use it on some of their mobile devices), but at the same time stop any further development of Qt (to harm Google or others which started to built their apps on top of Qt).\"\n\nThis is some of the lamest speculation so far.  Google has only a couple of apps using Qt and, quite frankly, they could probably stick with the already excellent Qt4.3 and not be fazed in the slightest.  The damages your suggested move would cause to \"others\" (are you seriously suggesting that Nokia would deliberately piss off all Qt users indiscriminately, including the ones that are industry partners?) are so trivial they would probably be dwarfed by the meagre license fees they'd get from continuing to deliver a top-notch, cross-platform toolkit.  They'd also annoy all the customers who don't want to have to re-write their apps for every platform they deploy on.  Also, QTopia and stock Qt4 share a gigantic amount of code, so stopping development on Qt4 while developing QTopia would be quite a balancing act.\n\nThis kind of thoughtless pessimism and frankly bizarre dystopian imaginings are even more annoying than the Pollyanna-ish blind optimism."
    author: "Anon"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "Your point of view is based on sound reasons. If there's one thing, Nokias management is _not_ capable of, than it's basing decisions on reason."
    author: "Thomas"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-29
    body: "<irony>Yeah ... sure ... because acting solely without reason is what makes a small producer of rubber boots a big tech company with Gigadollars of profit.</irony> "
    author: "Carsten"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-29
    body: "no, don't take nowadays management for the past one some 50 years ago. The poeple being in the leader position now have good knowledge how to optimize sales and squeeze the maximal out of existing markets.\n\nIf they would have been the decision makers 50 years ago, Nokia would still be selling rubber boots, but probably with 40% market share... Still they would complain about prices for rubber boots falling by 35% in the last decade whereas production costs have gone up. \n"
    author: "Thomas"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "you're setting a strawman here..."
    author: "Martin F."
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "I don't think they will intentionally hurt Qt desktop versions - it doesn't make any sense (first of all, they develop desktop applications - second, they'll want to push Qt as a standard, and you don't do that by crippling it) - but it is true that they will probably shift a lot of focus to Qtopia.\n\nThat may be a bad thing, but it could also turn out to be the best thing to happen to Linux for years.\n\nX11, at its core, is 20 years old and it shows -- the Xlib API sucks pretty badly, and the attempts to overcome that (xcb) are just not that much better, they're still a pile of fairly ugly plain C code.\n\nQtopia on the other hand is relatively clean code and was designed with a great API in mind from the beginning. The only thing that is holding up Qtopia Core+KDE on the desktop is the lack of decent framebuffer drivers.\nIf they fix that, and chances are they will at least for a few devices, Qtopia can replace X11 and it will be a major step forward.\n\nCompatibility with non-Qt applications could be solved the way OSX supports X11 applications -- launching an X server on top of Qtopia that handles legacy applications and displays their graphics inside Qtopia would work.\n"
    author: "bero"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "Is Qtopia client-server like X11 is? because say what you like about 20 year old code, that's a very nice feature to have."
    author: "Ben"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "Yes, but I haven't tried out to what extent this works so far (not sure if network transparency is there yet for example, but I've run 2 qtopia sessions on 1 machine -- the mechanism is similar to X11 there -- export QWS_DISPLAY=whatever).\n\nEither way the basic infrastructure is there and if anything is missing it can be added."
    author: "bero"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-28
    body: "You do know that Qtopia is built on top of Qt and actually depends on Qt?"
    author: "anon"
  - subject: "Re: Qtopia vs. traditional Qt"
    date: 2008-01-29
    body: "Funny thing is that afaik since Qt 4, Qtopia and Qt form one codebase... So they would actively have to strip stuff from Qtopia to make Qt any worse. Big companies are harder to keep decent, but they're still people, they still have smaller departments doing their thing. I don't believe this HAS to lead to trouble for Qt and KDE."
    author: "jospoortvliet"
  - subject: "BSD'ing"
    date: 2008-01-28
    body: "This notion that going BSD license will help is just plain rubbish.\n\nRubbish, rubbish, and more rubbish.  The dual license model allows TT to have some income, and to pay for development.  never underestimate that.  The BSD license would mean that rivals can steal code and offer nothing back, and on top of that, the BSD license would prevent including GPL code.\n\nWhile it might seem that BSD is less restrictive, from a certain sense, going BSD would pigeon-hole QT development.\n\nI must say I'm wary of the Nokia deal, but going BSD is not the solution."
    author: "T. J. Brumfield"
  - subject: "Re: BSD'ing"
    date: 2008-01-28
    body: "If the poison pill is triggered, it wouldn't automatically go to BSD. They could license as LGPL instead."
    author: "Ian Monroe"
  - subject: "Re: BSD'ing"
    date: 2008-01-29
    body: "\nIn the event of a \"release condition\" (i.e., when the foundation gets to re-license Qt), the FreeQt Foundation Board can select the BSD License *AND* one or more other open source licenses."
    author: "Sage"
  - subject: "OSS commitment of Nokia"
    date: 2008-01-28
    body: "I'm really undecided if I should cheer or cry.\nNokias current involvement in OSS (as in Maemo) shows, that they are *NOT* committed to Open- and Free-Software.\nThey basically release only things they *have* to release due to the GPL/LGPL, but nearly all things they can keep closed source, are kept this way.\nBest example: The OS2008 (Maemo) tray-applets (volume-, brightness-slider etc.) are kept CLOSED even if they bear no real intellectual property that's worth to protect. This alone shows their commitment to the FLOSS world (i.e. just pure opportunism and \"old-economy values\").\n\nI *really* hope not only TT shareholders will profit from this. \nIt's a serious thing that happened and given the possible influence of development priorities we all have to look very closely!\n\nOn the positive side:\nIf we will see Qt(opia) on more Nokia embedded devices and if they really support this way, chances are good they choose Linux instead of doing all as Qt on Symbian and we will have a nice future of Linux based embedded devices (Android and Qtopia) right in front of us.\n\nAnyway, let's hope the best because otherwise we all have to cry ;-)\n"
    author: "Little fish"
  - subject: "gobble gobble!"
    date: 2008-01-28
    body: "munch munch and gobble gobble,\nthe closed source hounds gulp and swallow,\nwhen will microsoft acquire the nix,\npasty white coders looking for their money fix"
    author: "ninja"
  - subject: "I can't understand you..."
    date: 2008-01-28
    body: "Why are you bashing on Nokia? Don't you think Nokia will read all these posts and perhapse decide on something like that, if they will continue development of Qt? Nokia is the most important partner of KDE, now. You cannot know what Nokia's aim is. But if you start bashing it will be much easier for them not to support Qt any longer and to focus on Qtopia. Be friendly to the companies you rely on!\n\nWhat is the worst that could happen? Qt to be released under an BSD licence because of FreeQt agreement? KDE has to fork from current development of Qt 4.4? Well if it happens KDE has to do more work. That would be bad. But it would also offer other possibilities for KDE. I could imagine that Google would start helping KDE to develop Qt, so that they can use it for their (few) apps. Or Opera, or Skype. There are so many companies who need Qt and would probably support a BSD licenced Qt hosted at KDE. So don't worry.\n\nPersonally I think Nokia bought TT for two reasons. First of all they want Qtopia. I don't think they want to hurt Motorola. Motorola is a partner of Android. That's a threat. Nokia needs Qtopia to compete with Android.\n\nThe second thing is Qt and KDE. KDE 4 is so powerful. I could imagine that Nokia wants to have Plasma on their smartphones. And that would be the best that could happen to the KDE community. Perhaps Nokia would start to develop apps in an Open Source model for KDE?\n\nGive them a chance. Now Nokia has the possibility to prove that they really support Open Source.\n\nPersonally I was not very happy with Nokia during the last week because of Bochum. But today my feelings towards Nokia became better again. This can be a great chance for KDE. If Nokia becomes a Patron and supports the development of KDE there will only be winners. So please stop bashing."
    author: "Martin"
  - subject: "Re: I can't understand you..."
    date: 2008-01-28
    body: "after doing what they just did in Bochum without any second thoughts whatsoever, I'd say they'll support our community as long as they can extract something from us. \n\nThe same guys lobbying for software patentability will now decide over QT. In a couple of years we will have a QT library where most of the features are only available for Windows, or some other problems like that.\n\nIt's 1997 all over again :(\n\n"
    author: "George"
  - subject: "Seconded"
    date: 2008-01-28
    body: "This seems a very rational position to me\u00a0! I think that only time will tell. As for now, just keep up the good job\u00a0!"
    author: "OdyX"
  - subject: "Re: I can't understand you..."
    date: 2008-01-28
    body: "Pfff... Patron of KDE just doesn't mean anything. Think of Mark Shuttleworth and his great dedication to KDE - 9 Ubuntu/Gnome dev's vs 1 Kubuntu/KDE dev."
    author: "blueget"
  - subject: "Re: I can't understand you..."
    date: 2008-01-28
    body: "Please do not mix. Mark Shuttleworth is Patron - not Canonical. For most this seems not to be a difference. But I think it is. Canonical probably thinks that Gnome is better for business. Mark thinks that KDE is the better desktop (at least he uses it). This probably explains some differences. And do you know how much the 9 developers work on Gnome? Or do they work on Ubuntu? For me Kubuntu is Ubuntu with KDE and Ubuntu is Ubuntu with Gnome. Only the desktop environment is different. Most of it is simply the same. So you could say that probably most of the work done by the 9 Ubuntu developers is also for Kubuntu as it is work on the basics: Linux, xorg, apache, etc. etc."
    author: "Martin"
  - subject: "Re: I can't understand you..."
    date: 2008-01-28
    body: "I know what Tim Frain did in the name of Nokia against European independent software developers"
    author: "gerd"
  - subject: "Re: I can't understand you..."
    date: 2008-01-29
    body: "> Be friendly to the companies you rely on!\n1) Asking for motivation or showing your feelings is not being unfriendly\n2) As a software developer, Qt's future influences the future of your products, when you are using it.\n3) One's personal market share as a customer of Trolltech/Nokia products gives him/her the right to talk about it and to be heard.\n\n\nRegards,\nErik"
    author: "Erik"
  - subject: "Think Positive? But can't help thinking negative"
    date: 2008-01-28
    body: "Lets think positive:\n1. Nokia wants to further integrate Qt/Qtopia into their mobile phones on top of symbian or maybe Linux? (and maybe integrating Qt/Qtopia into the internet tables - since Nokia often expresses their disappointments with GTK due to lack of roadmap) \n2. By using Qt, they can get tons of high quality open source software to be deployed on their internet tables for free, such as office (KOffice), pim (Kontact + friends).\n3. This acquisition might boost Trolltech and KDE/Qt as well since they're now backed by the largest mobile phone vendor on earth.\n\nBut I can't stop thinking negative:\n1. For how long Nokia will release Qt/Qtopia under GPL 2/3? Sure KDE can always fork it and release under BSD, but it will really hurt as the current development model is both parties are benefited from Qt. Nokia can close the development of Qt in the future and make it only commercial software.\n2. What will happen to Qt/Desktop? Sure Qt/Desktop and Qtopia share the same core, but they might be only interested to the core and neglect the Qt/Desktop? Why on earth Nokia have to buy the whole company if they want a cross-platform synchronization tool? For sure, selling a crossplatform toolkit might be less interesting for them, despite of Google, Adobe, skype all using Qt.\n3. What about the fate of the competitors who are using Qtopia?\n4. Will Nokia still sponsor the KDE developers to work on KDE? Yes they say support for open source will be the same, but we've been promised that for million times, when Apple forked KHTML, when Novell purchased SuSE and yet we've seen disappointment all over the place.\n\nOnly time will tell, I can only pray for good things for Qt, KDE, Trolltech and Nokia. Good luck for us all!!"
    author: "fred"
  - subject: "Re: Think Positive? But can't help thinking negative"
    date: 2008-01-30
    body: "Errrh... Novell *made* SuSE Open Source. The renamed it to openSUSE and GPL-ed all source code (especially YaST).\n\nhttp://www.linux.com/articles/46912"
    author: "whatever"
  - subject: "a link worth to read"
    date: 2008-01-28
    body: "i just find this article in zdnet, i think it make the situation more clear. \nhttp://news.zdnet.co.uk/software/0,1000000121,39292448,00.htm\n"
    author: "djouallah mimoune"
  - subject: "Re: a link worth to read"
    date: 2008-01-28
    body: "Doesn't make sense.  Why would they spend that much for TT and continue to use GNOME as their DeskTop?"
    author: "JRT"
  - subject: "Re: a link worth to read"
    date: 2008-02-07
    body: "Agreed. If your going to spend so much on a company, and then not go all the way, you spit yourself in half. The people from TT are not going to be very familiar with GTK/GNOME."
    author: "Riddle"
  - subject: "re-Frain"
    date: 2008-01-28
    body: "I am just saying \"Tim Frain\", Nokia's anti-software tool for ruthless software patent promotion at the expense of European creators. Tim Frain, the radical proponent behind so much Intellect.UK and EICTA policy proposals in Europe that put SMEs and independent software development at risk. \nhttp://www.timfrain.co.uk/index.html\n\nSure he also had his say on the acquisition. Which makes me think about the possible real forces behind the deal. Novell taking Suse made MS-Novell possible. What safeguards does Nokia provide?"
    author: "andy"
  - subject: "IWFM"
    date: 2008-01-28
    body: "This seems like a logical business move.  Nokia needs a GUI for its products and TT currently develops a GUI toolkit and is not currently a profitable company.\n\nThere is also the webbrowsing issue.  IIUC, Nokia currently currently uses WebKit and TT is going to adopt WebKit as part of Qt.\n\nI hope that Nokia is also doing this because they intend to use KDE for their products (as Sun uses GNOME).  If that is the case, it looks like all of this will lead to a beneficial synergy and it will insure that TT will continue in business since they will no longer need to be profitable to survive.\n\nPossible benefit for KDE is that Nokia will have their software developers working on KDE.  They will probably be more interested in fixing the bugs which KDE really needs."
    author: "JRT"
  - subject: "Re: IWFM"
    date: 2008-01-29
    body: "I don't see any logic in Nokia using KDE.\nThey most probaly will use qt/qtopia and leave KDE alone."
    author: "Iuri Fiedoruk"
  - subject: "Re: IWFM"
    date: 2008-02-07
    body: "Actually, I see plenty of reasons Nokia would want to use KDE. Picture KDE as an extension to Qt, with technology such as libPlasma, KParts, etc. KParts may prove particularly useful if Nokia wants to view a bunch of different file formats..."
    author: "Riddle"
  - subject: "I'm less worried about a sinister plan"
    date: 2008-01-28
    body: "The open source-ness of qt has been ensured by both Trolltech and Nokia. I'm just worried about Nokia not getting the open source thing like Apple did. If all the current practices for qt development are kept then this is a great thing that will benefit everybody."
    author: "Skeith"
  - subject: "This is a problem"
    date: 2008-01-29
    body: "This is a problem in the lunux community.  Everyone runs around looking for the bad, rather than just sitting back and watching what unfolds.  For allyou know this could be the best thing to happen, so stop the panic and worry and let it unfold.  "
    author: "RJ"
  - subject: "I'm not the only one who sees Doom & Gloom "
    date: 2008-01-29
    body: "http://mobile.slashdot.org/mobile/08/01/28/136204.shtml\n\nSlashdot readers agree with me wholeheardetly.\n\nNokia, thanks for destroying something that promised to be so great. Qt, will become useful only for mobile phone (smartphone) related stuff, and that will be it sadly.\n\n150 million? Trolltech could have sold their soul for much more money. MySQL got 1 Billion dollars. *Dr. Evil pose*\n\nHopefully the trolltech guys have some time looking for other jobs, as I doubt they have the job security they had in the past.\n\nWell hopefully Nokia will embrace KDE and the Linux desktop community as well. Maybe they will even make a new Linux based set-top box similar to Apple TV.\n\nI'm trying to stay positive but it's hard. Where is the reassuring news from Nokia? Nokia needs to post something positive, concrete, and believeable VERY SOON. That reasuring open letter, while a nice gesture, means nothing.\n\nWe will be watching the level of commitment, the number of weekly commits, and any other measure of progress very closely.\n\nWell at least this part of the Linux world stayed in Europe and didn't sell to a U.S. company. Here in Europe the legal system at least protects the open source world somewhat. Maybe Nokia will rethink software patents too now.\n"
    author: "Richard Lionhard"
  - subject: "Re: I'm not the only one who sees Doom & Gloom "
    date: 2008-01-29
    body: "WTF? Why all this negativity?\n\nApparently Nokia needs a new good cross platform UI, and they decided to buy one rather than develop one by themselves. Good for Nokia. Good for the Nokia developers getting a better platform than Avkon, and good for the Qt developers getting a big company backing them up.\n\nAlso, Nokia is a huge development house with (tens of?) thousands of software developers working with a variety of platforms, including PC's. Nokia has loads of internal custom dev tools for PC, this is a great opportunity for Nokia to build it's internal tools to be X-platform with Qt.\n\nAnd BTW, I believe Nokia has used Qt in some of it's commercial PC sw for some years already. So I see no reason why would they stop developing Qt for desktops.\n\nPlease, stop being so f***ing lame people!"
    author: "Angry Dad"
  - subject: "Re: I'm not the only one who sees Doom & Gloom "
    date: 2008-01-29
    body: "\"Please, stop being so f***ing lame people!\"\n\nI fear you are wasting your time; over the last year or so, The Dot has become a haven of Chicken Littles and Drama Queens who squeal and sob hysterically over any news that might possibly (with a whole lot of effort and an unhealthy dollop of pointless and unrealistic cynicism) be contorted into having some kind of negative side associated with it[1].  It's an embarrassment.\n\n\n[1] See e.g. http://dot.kde.org/1172721427/"
    author: "Anon"
  - subject: "Re: I'm not the only one who sees Doom & Gloom "
    date: 2008-01-31
    body: "ah the memories.  One of my posts there is exactly what you are talking about.  It did take me a little longer to follow through on my drama queen antics about switching to Gnome, but I actually finally did it this week, the last straw being the Nokia thing.  \n\nI'm still hanging around the dot occasionally because I used and loved KDE for many years.  Maybe some time in Gnome will bring me to my senses, just in time for a KDE 4.1 release that will be ready to earn my confidence again.  Maybe by then Nokia's position and direction regarding TT will be clarified.  If that happens I'll be sure to be back, and I'll strongly repudiate my earlier objections to KDE's direction.\n\nIf I was a total drama queen then, I apologize.  I was responding to things that seemed to be leading to me not enjoying KDE anymore.  At least I'll be giving an honest attempt to put my money where my mouth is by following through for a while at least with the overwrought musings about leaving.\n\nI think the lesson for me has been not to get involved with software: just treating it as something I use.  There's no way I'll be filing bugs for Gnome and hanging around whatever they have instead of the dot.  If I end up back with KDE I'll have to do the same and just roll with the changes.  It'll save me some heartburn, but is rather disappointing to lose my pipedream of a DE that is just right for me and over whose direction I exercise a modicum of control.  The whole KDE4 and Nokia sequence killed a dream, but left me (I hope) more grounded."
    author: "AS"
  - subject: "Re: I'm not the only one who sees Doom & Gloom "
    date: 2008-01-31
    body: "I agree. We should watch the level of commitment and the pace of development.\nIf it slows down at all (# of commits, publicity, # of new features) we're in trouble and have to decide on a course of action.\n\nMaybe we're lucky and it will speed up. If Nokia engineers and programmers will contribute to the project now, that number should go up significantly.\n\nNow that we have Sun on board, all we need is google!!!\n\nI also agree that it's good that at least Nokia is an European company. Much better than if it were an American (lobbyist *err*) company.\n"
    author: "Max"
  - subject: "Re: I'm not the only one who sees Doom & Gloom "
    date: 2008-02-01
    body: "\"I also agree that it's good that at least Nokia is an European company.\"\n\nThat's European Commission thinking, that is: \"must promote our regional champions at the cost of other businesses\" (although it doesn't come out like that, obviously). If anything it's worse when a European company is, amongst other things, lobbying for software patentability because the different EU representatives won't be tempted to just brush them off as predatory foreign interests looking to transfer their home advantage to other markets."
    author: "The Badger"
  - subject: "Re: I'm not the only one who sees Doom & Gloom "
    date: 2008-02-01
    body: "That's true, never thought about it this way.\n\nHopefully (dreaming) Nokia will change their thinking. I used to like the company A LOT!! about 10+ years ago.\n\nMy second cell phone was a Nokia. (first was \"hagenuk <-- what ever happened to them?) I probably had at least 5-6 Nokia cell phones in my lifetime. We have a Nokia TV, and a Nokia satellite tuner. They make outstanding products.\n\nIf Nokia would make something nearly as good as my Blackberry Pearl, I'd probably still have Nokia products. Maybe with KDE/Qt that will again be the case. Especially if I can sync it with my Linux and Windows desktops with no issues whatsoever.\n\nNokia just needs to get back to it's roots and adopt google's ~\"Don't be evil!\"~ philosophy. If Nokia will do more for open source and against American concepts like \"software patents\" *grr* we will give them a second chance and welcome them with open arms.\n"
    author: "Max"
  - subject: "Uh oh..."
    date: 2008-01-29
    body: "Does this mean the next major version of KDE won't have a \"Qt 5 dance\"?"
    author: "AC"
  - subject: "Re: Uh oh..."
    date: 2008-01-29
    body: "NO, it means that the Qt 5 Dance will be a ringtone."
    author: "lpotter"
  - subject: "Re: Uh oh..."
    date: 2008-01-31
    body: "Guess what it will sound like?"
    author: "Max"
  - subject: "2.1%"
    date: 2008-01-29
    body: "Take: Price of TT getting bought (150M), divide by Nokia's Net Income 2007 (7205M according to wikipedia), and you get that result (math done using kcalc ;-) ).\n\nWhat does that result mean?\n1. a whole company with a roadmap for a GUI\n2. bunch of programmers who could be used somewhere else within Nokia\n3. A massive damage to the rivals like Motorola when Nokia should close Qtopia or even Qt (close as in everything from raising license costs to stoping any (public) development)\n4. Proof that the EU needs software patents because that poor company who did Open Source could not live from it due to no patents possible.\n\nNow imagine you would only have to pay 2.1 from your income to reach all these points with 1 buy. I call it a cheap shot (pun intended?).\n\nSorry to sound that pessimistic, but right now, I feel very worried about the future of No Software Patents, Open Source and KDE."
    author: "anonymous coward"
  - subject: "HUMMM"
    date: 2008-01-29
    body: "Good for QT and Trolltech and bad for KDE.\n\nNokia has zero interest in KDE."
    author: "fun"
  - subject: "Re: HUMMM"
    date: 2008-01-29
    body: "I'm not so sure about that. Think plasmoids running on the PC desktop as well as other handheld devices. The same desktop everywhere...\n\nMight fit right into Nokias strategy."
    author: "Axl"
  - subject: "Its Obvious..."
    date: 2008-01-29
    body: "Lets think of it from a different perspective; you have 40% of the phone market share. Nokia, as with all other phone/mobile electronics companies, always discuss ways to improve; not always in features but in money also. \n\nNokia, has quite the compition this year, with Google's Android, and Windows Mobile coming this following year. They are probably looking for a newer, much better platform for there mobile phones, one with a much better roadmap. They have been using Qt for quite awhile, in a few of there phones and in there desktop mobile suite of applications.\n\n<B>Few things Nokia could want from Trolltech and Qt/Qtopia:</B>\nCross Platform Framework - For there Desktop application suites. \nNewer, much better platform for mobile phones; with better roadmap. \nBetter communication with the open source community.\nIntegration with KDE, think of Nokia plasmoids on your desktop. \n\n<B>Few bad things Nokia could want from Trolltech and Qt/Qtopia:</B> \nInert the development of Qtopia with Motorola.  \nClose future versions of Qt/Qtopia and shove there legal patients up everyones ass. \n"
    author: "Jeremy"
  - subject: "Re: Its Obvious..."
    date: 2008-01-29
    body: "> Integration with KDE, think of Nokia plasmoids on your desktop. \n\nAnd this might actually become a real Microsoft-killer. Nokia has a global 35% market share on handsets."
    author: "Dane"
  - subject: "Windows killer"
    date: 2008-01-29
    body: "> Integration with KDE, think of Nokia plasmoids on your desktop. \n\nAnd this might actually become a real Windows-killer. Nokia has a global 35% market share on handsets."
    author: "Dane"
  - subject: "Ouch!"
    date: 2008-01-29
    body: "This must have really hurt. :|\nI hope this won't end badly. But those usually tend to end badly.\n\nI hope Nokia will start utilizing Qt on their cell phones but if possile don't try to pull of anything dirty since Microsoft has something to do with this bought no matter how I look at it. No matter the reasons they are behind this in some way.\n\nLook, Nokia supports software patents, Nokia doesn't want OGG to HTML5, they are Gnome company on their FOSS side of things, They support and USE DRM in their products, Why is their Music Store WMA/WMV store instead of some other format?, ...\n\nLots of reasons why this means war and why it is really bad news for many kde users out there. Plus that personally I cannot stand now the GTK and Gnome people starting their own commentatory about things with \"It's just like we said 10 years ago what would happen\" or \"hahha, we knew this was going to happen and now kde is going to fall down.\"\n\nLonglive kde..."
    author: "AnXa"
  - subject: "Re: Ouch!"
    date: 2008-01-29
    body: "\"ut if possile don't try to pull of anything dirty since Microsoft has something to do with this bought no matter how I look at it. No matter the reasons they are behind this in some way.\"\n\nlol"
    author: "Anon"
  - subject: "Re: Ouch!"
    date: 2008-01-29
    body: "microsoft have nothing to do with it, get your facts straight before you start running around like a chook with its head chopped off\n"
    author: "RJ"
  - subject: "Re: Ouch!"
    date: 2008-01-29
    body: ">>Look, Nokia supports software patents,\n\nSo do IBM, HP, Sun and many other large OSS sponsors/supporters.\n\n>>Nokia doesn't want OGG to HTML5\n\nThey didn't want any specific media format tied to the format\n\n>>they are Gnome company on their FOSS side of things\n\nThey utilize GTK and have been very vocal about it's shortcomings and lack of roadmap.\n\n>>Gnome people starting their own commentatory about things with \"It's just like we said 10 years ago what would happen\" or \"hahha, we knew this was going to happen and now kde is going to fall down.\"\n\nIs this your first day on the internet? ;)\n\nThey say that about every point release or project announcement that appears on the dot. Life goes on. "
    author: "elsewhere"
  - subject: "Excuses, excuses"
    date: 2008-01-30
    body: "\"Look, Nokia supports software patents,\"\n\n\"So do IBM, HP, Sun and many other large OSS sponsors/supporters.\"\n\nThis is the \"everyone else is doing it\" excuse. However, the most genuine supporters of Free Software have been companies like Trolltech, MySQL and others who were quite happy to go on the record and say that software patents are wrong.\n\nAt least one can say that some of these big companies are moderately serious about not suing people into the ground over patents by, for example, participating in patent non-aggression pacts such as OIN (http://www.openinventionnetwork.com/) - I mean, even Philips is a member, and in the whole software patent lobbying affair, they didn't come out much cleaner than Nokia. Meanwhile, Sun's stance on patents has probably shifted somewhat after getting sued by Kodak over some techniques used in the standard Java libraries: a $92 million lesson for Jonathan Schwartz.\n\n\"Nokia doesn't want OGG to HTML5\"\n\n\"They didn't want any specific media format tied to the format\"\n\nReading the position paper from Nokia, it says firstly that \"the active endorsement of proprietary technology such as Ogg, ..., by W3C, is, in our opinion, not helpful\" (probably trying to set records for comma usage along the way). I think the response by the Ogg developers says more than I ever could about how this \"proprietary\" labelling of genuinely open technologies is, at the very least, dishonest:\n\nhttp://xiph.org/press/2007/w3c/\n\nBut what do you expect from a company whose business is based on an industry driven by patent licensing cartels? I'm sure they can peruse their portfolio and find a few things that vaguely apply to (and thus encumber) their favoured solutions."
    author: "The Badger"
  - subject: "god speed to kde"
    date: 2008-01-29
    body: "hope kde does what is best and takes the necessary action that will benifit the free software community in the best possible way.\n\nthis will be really hard for awesome kde/qt developers.\n\nmy best wishes are with kde.\n\noften the truth is hardest to swallow, the earlier you do it the better it will be. hopefully kde wont repeat the same mistakes again.\n\nit will be also better for nokia to spill out what is their future roadmap to kde. which i think they wont. it will be worst to make huge promises to qt/kde and then piss upon them after few months. this is highly anticipated from nokia so it shouldn't be a surprise when it happens.\n\ni am moving away to gtk/python, although not very good compared to kde/qt but i dont wont to deal with licensing issues and evil takeover. i can do my work in peace and enjoy it. this entire fiasco was a learning lesson for me. \n\nkde team has put a lot of hardwork since many years and kudos to them. it will be really sad to see them run over by some mega multi billion corporation with their own money making agendas and quaterly results."
    author: "goodluck"
  - subject: "Re: god speed to kde"
    date: 2008-01-29
    body: "The kde 4.0 case (release with bugs but release so people will forget us) also left a bad taste in my mouth.\nThe bad part is that I don't see gtk/gnome as good alternatives.\n\nMaybe it's time for someone just create a lighweight fork of both qt and kde and start a new and small DE?"
    author: "Iuri Fiedoruk"
  - subject: "Fork dat hoe!"
    date: 2008-01-29
    body: "at the first sign of trouble from Nokia, fork it for KDE and never look back!"
    author: "serenity"
  - subject: "almost crying"
    date: 2008-01-29
    body: "I feel so betrayed!!!!\n\nI thought that between freedom and commercial, we don't have to choose. I still think that. But I forgot you can buy most of mens with money. Trolltech's chiefs were ones of them. \n\nI didn't see that.... foolish I was!\n\n\nps: To Nokia, freedom is not just buying a free company or giving money to kde, it's a true commitment which means putting freedom first of ALL. Do this commitment and you will be welcome, even if you don't become a patron of kde. We do not need your money but you do need our trust. Can we trust you???"
    author: "disapointed"
  - subject: "Cautiously optimistic"
    date: 2008-01-30
    body: "You are absolutely right about the commitment issue. This issue is essential for \"Qt everywhere\" vision to become reality with Nokia owning Trolltech. I assume Nokia is really planning to use Qt/Qtopia as a toolkit in their crossplatform strategy.\nAt the same time I doubt that suits in Nokia management even realize how much they can benefit (or lose) from this Trolltech buyout.\nIf Nokia embraces freedom and gains our trust, they would have huge army of devolopers/contributors with samurai mentality working for them 24/7 for free.\nOn the other hand if Nokia screws this up, they will end up having aging toolkit and watch how e.g. Android sails by with former Qt/Qtopia contributors onboard.\nThose two scenarios above are of course just simplifications and exaggerations, but you get the picture. Personally I see tremendous opportunities here and I'm cautiously optimistic as story unfolds...\n\nKDE and TT, keep up the good work :-)\n"
    author: "Anonymous"
  - subject: "Nokia will most probably"
    date: 2008-01-30
    body: "turn this into a mobile only toolkit and forget about the rest. It's definitely the end of commercial development of Qt IMHO. Don't know if this is a bad thing though. Qt is GPL. Look at what happend to X11. Anybody still using that? A fork is about that happen soon."
    author: "Michael"
  - subject: "Re: Nokia will most probably"
    date: 2008-01-30
    body: "Complete nonsense. With Google's Android and the Asus EeePC the lines between mobile phones and small PCs are soon to vanish.\nIt's pretty apparent that Nokia desperately needed a strategy that covers both PCs, UMPCs as well as mobile phones. Qt + Qtopia is the perfect answer to that.\n"
    author: "Anonymous"
  - subject: "Oh how i sob for your indignant fury"
    date: 2008-01-30
    body: "It is quite frankly pathetic.\n\nThis is business, TT is a company, the acquisition is not necessarily a bad thing.\n\nNokia acquired TT after they adopted the GPL3.\n\nNokia have applied to be a KDeV sponsor thus showing goodwill.\n\nQT is in a very healthy state (4.4) for KDE build its near mid-term future if the worst were to happen.\n\nAnd yet all i see is a bunch of drama queens claiming to have been betrayed by TT and cast into the rapacious maw of Nokia. \n\nTosh!\n\ncongrats TT, lets hope it works to KDE's benefit.\n\nRegards"
    author: "Laughing at the Doom-mongers"
  - subject: "Re: Oh how i sob for your indignant fury"
    date: 2008-01-30
    body: "> And yet all i see is a bunch of drama queens claiming to have been betrayed by TT\n\nAnd those are mostly anonymous cowards who in reality are maybe mislead GTK guys in disguise anyways ... "
    author: "Anonymous"
  - subject: "Re: Oh how i sob for your indignant fury"
    date: 2008-01-30
    body: "> And yet all i see is a bunch of drama queens claiming to have been betrayed by TT\n\nAnd those are mostly anonymous cowards who in reality are maybe mislead GTK guys in disguise anyways ... "
    author: "Anonymous"
  - subject: "Nokia is pure evil!!"
    date: 2008-01-30
    body: "Nokia is no better than M$ atm. They closed down the Bochum ALTHO it made LOTS of profits and told people otherwise ( New information leaked to Capital magazine. http://www.heise.de/newsticker/meldung/102705 the factory made 132\u0080 profit.)\n\nJust a bunch of lying corporate dumb fucks if you ask me. \n\nMark my words: KDE IS DOOMED!"
    author: "Bochumworker666"
  - subject: "Re: Nokia is pure evil!!"
    date: 2008-01-31
    body: "It's pretty apparent you deserved a job with Nokia more than some Untermensch in Romania.  Keep fighting the good fight."
    author: "AS"
  - subject: "Re: Nokia is pure evil!!"
    date: 2008-01-31
    body: "Not true and I did not say that. \n\nMy points are:\n\n- Nokia will fuck everybody over if they think there is more money to be made somewhere else.. no matter how good of a job you do. Very Walmartesque if you ask me.\n- Nokia is not telling the truth. So how can we trust them??\n\nAny objections?"
    author: "Bochumworker666"
  - subject: "Re: Nokia is pure evil!!"
    date: 2008-02-01
    body: "It's not really about Nokia, the problem is called CAPITALISM.\nLook at Google for example, how much has it given back in comparison to what it took?\nName me one single holy company; Deutsche Bahn? Deutsche Post? Siemens maybe?\nIt's all about making money and being ahead of the competitors and the most or all companies would stop at nothing to achieve that :( "
    author: "Bobby"
  - subject: "Re: Nokia is pure evil!!"
    date: 2008-02-01
    body: "\"Siemens maybe?\"\n\nHow unfair to bring Siemens up just as they were learning about \"lubricating the wheels of business\" in various sectors where brown envelopes seem to be the norm! ;-)\n\nIt's interesting to see that they're mentioned in the small print of the Trolltech acquisition, though. Life in the telecoms cartel goes on, it would seem."
    author: "The Badger"
  - subject: "Re: Nokia is pure evil!!"
    date: 2008-02-01
    body: "Yes, Deutsche Telecom... how could I forget those very nice people whose goal is to squash any and every other company that has the word \"Tele\" or \"com\" in it's name?\nLike they say, when you point one finger 4 point back at you. Die Deutschen kochen auch nur mit Wasser ;)"
    author: "Bobby"
  - subject: "Re: Nokia is pure evil!!"
    date: 2008-02-01
    body: "Weren't those Capital numbers proven wrong? Some estimated averages that had nothing to do with the said manufacturing facility or its real performance.\n\nHowever, as we all know, Nokia is evil, unlike, say Motorola and others that have moved away from Germany long before this. Nokia should have left like others years ago. Would have caused less fuss then. And a foreign company is always a cute target, especially in Germany, where people are aware of their own superiority. Have always been. "
    author: "EineKleine"
  - subject: "Awesome :)"
    date: 2008-01-30
    body: "I'd love to have a good Nokia phone with Qtopia on it :) Last time I used Qtopia was on a Sharp Zaurus PDA and I liked it a lot... it would be wonderful to have Linux and a similar interface on a phone...:)"
    author: "dreamscape"
  - subject: "improving their relationship with the open source."
    date: 2008-01-30
    body: "\nHere is a good way to improve their relationship with the open source community.\nWell, they can start by retracting their objection to HTML5 proposed use of .ogg and theora format for the next web standard. \n\nhttp://yro.slashdot.org/article.pl?sid=07/12/11/1339251\n\n"
    author: "Giovanni "
  - subject: "KDE5"
    date: 2008-02-01
    body: "Now it's time to start planning KDE5 which is bound to be a QT-free release"
    author: "no-one"
  - subject: "Re: KDE5"
    date: 2008-02-01
    body: "Maybe Gnome3 will be built on QT.\nThe future is not ours to see :)"
    author: "Bobby"
  - subject: "Re: KDE5"
    date: 2008-02-01
    body: "Er ... why not (In the absolute worst possible case) continue with KDE4 using the last released Qt4 version?"
    author: "Anon"
  - subject: "Re: KDE5"
    date: 2008-02-01
    body: "\"I offer a toast. The 'undiscovered country.' (Beat) The future.\" \n-Star Trek VI\n\n"
    author: "Max"
  - subject: "Re: KDE5"
    date: 2008-02-01
    body: ">  KDE5 which is bound to be a QT-free release\n\na QuickTime-free release? I wouldn't bet on that.\n\nApple want it to be the next Flash and aggressively pushes it in its 'video' tag, hijacking the W3C as needed, with help from Nokia who would love to see such DRM-ridden proprietary crap become pervasive.\nAwww wait, you meant Qt..."
    author: "seb"
  - subject: "Re: KDE5"
    date: 2008-02-02
    body: "LOL....\n\nGreat post. Totally agree with you.\n"
    author: "Max"
---
Today, Nokia and Trolltech <a href="http://trolltech.com/28012008/28012008">announced that Nokia will be purchasing Trolltech</a>.  Nokia will continue with Qt's dual license model, which was <a href="http://arstechnica.com/journals/linux.ars/2008/01/18/trolltech-to-adopt-gpl-3-for-qt">updated to GPL 3 only last week</a>. In <a href="http://trolltech.com/28012008/28012008-letter">an open letter to KDE</a>, the chief Trolls and Nokia VP asked for ideas and comments on improving their relationship with the open source community.  Nokia will be applying to become <a href="http://ev.kde.org/supporting-members.php">a patron of KDE e.V.</a> and the <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">FreeQt</a> foundation is being maintained to guarantee the continued freedom of the toolkit KDE depends upon.  This change should help ensure both the continued longevity of Qt and KDE as well as give the platform a boost in industry, particularly in the consumer electronics industry. 

<!--break-->
