---
title: "KDE-Bindings / Kross Meeting"
date:    2008-07-21
authors:
  - "tmoenicke"
slug:    kde-bindings-kross-meeting
comments:
  - subject: "Great, just great!"
    date: 2008-07-21
    body: "Thanks to all that work on bidings.\nUsing script languages is, on my opinion, the future of desktop. It brings more flexibility, speed of development and is much easier to modify. For example, I do not see a future of plasmoids beings written in C++ becuase it's hard to create packages for all systems and plataforms, while writing a plasmoid with a script, you just have to install the script interpretator and voil\u00e1! :-D\n\nAlso, adding Lua is great news, I've worked a bit with it in freecraft/stratagus/stargus and it is a lot of fun and flexibile."
    author: "Iuri Fiedoruk"
  - subject: "Re: Great, just great!"
    date: 2008-07-21
    body: "re plasmoids;\n* href=\"http://techbase.kde.org/Development/Tutorials/Plasma/RubyApplet (QtRuby tutorial)\n* http://www.kdedevelopers.org/node/3560 (QtRuby and C#)\n* http://techbase.kde.org/Development/Tutorials/SuperKaramba#Plasma (you can even use Plasma dataengines)\n* http://developer.apple.com/macosx/dashboard.html (they are supported by Plasma too)\n* more to come :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Great, just great!"
    date: 2008-07-21
    body: "> It brings more flexibility, speed of development and is much easier to modify.\n\nThere should be additional code implemented in the language on top of the C++ API, in order to get more out of the language characteristics and simplify the usage. But the developer should still be able to dig deeper if he wants to learn more.\n\n"
    author: "Thomas Moenicke"
  - subject: "Superkaramba?"
    date: 2008-07-21
    body: "Does all of this mean Superkramba will soon work in KDE 4.0 without complaining about missing Kross support? I have yet to get Superkaramba to work in 4.0"
    author: "Jim"
  - subject: "Re: Superkaramba?"
    date: 2008-07-21
    body: "It works and worked already in 4.0. Depending on the exact error-message, which you don't shared with us, it may the case, that you just need to install kdebindings (or to be more exact either ruby-supported located in kdebindings/ruby/krossruby or python-support located in kdebindings/ruby/krosspython).\n"
    author: "Sebastian Sauer"
  - subject: "Re: Superkaramba?"
    date: 2008-07-25
    body: "Hello,\n\nisn't it so that Plasma is supposed to contain the Superkaramba in 4.0?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Regarding Plasma and Python..."
    date: 2008-07-21
    body: "Greetings all,\n\nWith my apologies if this is an FAQ -- I haven't been able to find an answer to it yet, at any rate -- but do the Plasma bindings for Python work? My understanding is that they didn't as of not very long ago, but that was to be fixed at the meeting.\n\nRelated question: has anyone produced working example code for Python Plasma widgets, dataengines and runners, that we could put in the Wiki? (Emphasis on 'working' -- I know there are bits of non-working code in SVN.)\n\nThanks. :)"
    author: "S."
  - subject: "Re: Regarding Plasma and Python..."
    date: 2008-07-21
    body: "Someone did some initial work on getting Python working with Plasma. But they don't have enough time to continue with it so they've passed it on to me. Python support should be in KDE 4.2, working compilable code should be in SVN much before 4.2 though.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Regarding Plasma and Python..."
    date: 2008-07-21
    body: "Thanks Simon - appreciate you taking this on."
    author: "Anon"
  - subject: "Re: Regarding Plasma and Python..."
    date: 2008-07-22
    body: "Fantastic! Thank you lots, Simon. :) I did look into contributing, although I'm sorry to say I was left confused and generally at a loss on how to proceed by what I found in SVN back then (that was a couple months back). There was stuff that looked like it /should/ have been working, and yet didn't, and I couldn't tell whether it was broken, or I was doin' it wrong.\n\nAnyway -- how may a wee little user like yours truly help with things?"
    author: "S."
  - subject: "My hobby"
    date: 2008-07-21
    body: "Writing QtRuby scripts that run using the Kross QtScript extension running in a  QtRuby application.\n\n:) :)"
    author: "Ian Monroe"
  - subject: "Scripting Documentation"
    date: 2008-07-21
    body: "One thing I'd like to see on Techbase would be to extend the Kross documentation to include more about how you should design your API with various Dos and Don'ts. I'd imagine theres lots of research and papers out there on designing a nice high-level API, even just collecting links to them on a Techbase page would probably be quite useful.\n\nOn a slightly side note, does the Smoke-to-Kross bridge restrict sharing objects to situations where both are in the same language? (i.e. could you use QtRuby bindings with the Kross-Python backend and any other combination?) Also, in the future would you (theoretically) also be able to mix-and-match C#, PHP, and Lua as well?"
    author: "Kit"
  - subject: "Re: Scripting Documentation"
    date: 2008-07-22
    body: "> One thing I'd like to see on Techbase would be to extend the Kross\n> documentation to include more about how you should design your API\n> with various Dos and Don'ts.\n\nyes, we - scripting in general - need there really more documentation. That doesn't mean to only write new tutorials but to also connect already existing information together and to provide specialized code-snippets, best-practice samples, links to already existing solutions that does solve a specific problem already, modules that can be used, etc.\n\n> does the Smoke-to-Kross bridge restrict sharing objects to\n> situations where both are in the same language? \n\nsmoke is language-independent and so is Kross. So, it's not restricted to a specific language though there may tweaks needed for each language.\n\n> i.e. could you use QtRuby bindings with the Kross-Python backend\n\nShould be np as soon as kdelibs/kross/core/* is accessible in QtRuby, someone could use it to exec e.g. Python-code from within Ruby. Same with all other backends.\n"
    author: "Sebastian Sauer"
  - subject: "ObjC Bindings?"
    date: 2008-07-21
    body: "Are you looking into this option once Qt has ObjC support and Cocoa?"
    author: "Marc Driftmeyer"
  - subject: "Re: ObjC Bindings?"
    date: 2008-07-22
    body: "The very first language binding I did was an Objective-C Qt one in 2000. But there was virtually no interest in it, as everyone who likes Objective-C was more interested in GNUStep than Qt or KDE.\n\nAlthough Qt will be based on Cocoa, I don't think it will have any sort of public Objective-C api, and I would be surprised if anyone at Trolltech was to produce a language binding for Objective-C\n\nThe old Qt Objective-C binding was implemented by using a C binding to the Qt api and calling the C functions from Objective-C. Objc++ wasn't available for Gnu Objective-C on Linux then, and so you couldn't easily mix C++ and Objective-C. \n\nToday I wouldn't implement a binding like that at all. For Ruby, PHP and C# we use an auto-generated dynamic runtime library called 'Smoke', which allows you to look up methods according to their type signatures. In Ruby there is a method called 'method_missing' which corresponds to [NSObject :forwardInvocation], and the Ruby bindings use that method to trap method calls and forward them to the C++ methods in the Smoke library. So it should be possible to do that in Objective-C. But as well as the types of the method arguments we would need to add their names to the Smoke tables, to allow lookups by name as the Objective-C method signatures would be derived from the arg names."
    author: "Richard Dale"
  - subject: "Re: ObjC Bindings?"
    date: 2008-07-23
    body: "Thank you for the very professional and thorough background on the matter. Having Debian and OS X with Qt on both platforms [made the jump to KDE4.1RC1 after I thought they were mature enough to say adios to 3.5.9 (I was vocal in the past but enjoy the start of what I'm seeing--the Widgets customization or drag n' drop positioning for the Panel Widget would be an improvement; ordering of how widgets currently have to go onto the panel stack I take is something that will later be relocatable to suit the user's needs--but glad it's almost there)] I also include GNUStep and the Etoile-Project to see how they are maturing. I've noticed a lot of Smalltalk additions to the Etoile-Project hooking in with ObjC so I hope once the Etoile-Project is capable of more that there would be a way to have more cross-pollination between KDE, GNUstep, Qt and OS X.\n\nI realize it's a large effort, but I think it will be much easier if GNUstep does indeed get their port of WebKit onto GNUstep.\n\nI'm glad to see all the other additions like Ruby and Lua. I've personally zero interest in C# but then I can't stomach Microsoft [it's that ex-NeXT employee/ex-Apple employee in me].\n\n- Marc"
    author: "Marc Driftmeyer"
  - subject: "What about Java-KDE binding?"
    date: 2008-07-23
    body: "OpenJDK is near to be finished and included in all Linux flavours.\n\nWhy not a Java-KDE binding?\n\nThere is QtJambi (Qt-Java binding) supported by Trolltech :\n\nhttp://trolltech.com/products/qt/features/language-support/java"
    author: "Noa"
  - subject: "Re: What about Java-KDE binding?"
    date: 2008-07-26
    body: "It's not a question of why it is a question of who. Most of the hard work is already done. It is now a matter of someone stepping up to do (and maintain!) a KdeJambi or whatever you want to call it.\n\n--\nSimon"
    author: "Simon Edwards"
---
Last weekend we hosted the <a href="http://techbase.kde.org/Development/Languages">KDE-Bindings</a> and <a href="http://techbase.kde.org/Development/Languages/Kross">Kross</a> meeting here at the <a href="http://www.kdab.net">KDAB</a> Office in Berlin/Kreuzberg with the goal of organising, community building and of course hacking. It was the first meeting of its type for a bindings crew, with eight people representing 
<a href="http://techbase.kde.org/Development/Languages/Ruby">Ruby</a>,
<a href="http://techbase.kde.org/Development/Languages/Python">Python</a>,
<a href="http://techbase.kde.org/Development/Languages/QtSharp">C#</a>,
<a href="http://techbase.kde.org/Development/Languages/Lqt">Lua</a> and 
<a href="http://techbase.kde.org/Development/Languages/PHP-Qt">PHP</a>. The projects do not all share code bases, and so it was an opportunity to present and review the details of how the implementations worked.
<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px; float: right;">
<a href="http://static.kdenews.org/jr/kde_bindings_kross_meeting_kdab.jpg"><img src="http://static.kdenews.org/jr/kde_bindings_kross_meeting_kdab_small.jpg" width="400" height="300" border="0" /></a><br />
Mauro Iazzi, Thomas Moenicke, Arno Rehn, Cyrille Berger, Aleix Pol i Gonzalez<br />
Richard Dale, Sebastian Sauer, Simon Edwards</div>

<p><a href="http://techbase.kde.org/Development/Languages/Python">Python</a> and <a href="http://techbase.kde.org/Development/Languages/Ruby">Ruby</a> are in a very good shape and stable for being used in application development using Qt and KDE facilities, while <a href="http://techbase.kde.org/Development/Languages/QtSharp">Qyoto</a>, the C# binding is well on its way to  providing the same level of completeness and stability. <a href="http://techbase.kde.org/Development/Languages/PHP-Qt">PHP</a> will try to fill the gap between Desktop and Web Applications using the powerful technologies in the Qt toolkit, such as QtWebKit for instance. An interesting new star on the bindings horizon is <a href="http://techbase.kde.org/Development/Languages/Lqt">Lua</a>, which is widely used for scripting some well known computer games and other applications.</p>

<p>As a result of the meeting, we were able to get the Lua bindings running the cannon game tutorial t7, which is an important milestone in the progress of a Qt language binding, as it uses custom signals and slots. Furthermore, Lua bindings have been moved into KDE playground this week. Another technology we were working on was a Smoke to Kross bridge that allows one to share objects between
e.g. Ruby bindings and Krossruby. Using it, a developer can show GUI elements using QtRuby and the underlying QObjects and QWidgets can be picked up on the Kross side. The issues discussed included how to organise modules and documentation, improving the design of the template based QList and QMap marshallers for the Smoke lib, and which modules for the new KDE APIs we should target.</p>

<p>An amusing demonstration of the power of some of the KDE bindings technology happened when Sebastian asked Richard for an estimate about the effort of make QtRuby applications scriptable with QtScript. He just came out with a Smoke2 module only 10 minutes later, and after an additional 10 minutes he wrote a corresponding extension for QtRuby, and we were quite amazed. More work was also done on the Kross plugins for Krita and KDevelop. On balance, it was a great meeting with great people and a nice ambiance at the <a href="http://www.kdab.net">KDAB</a> office. </p>

<p>Now we are looking forward to <a href="http://akademy.kde.org/">Akademy</a> and more bindings discussions there. If you are interested in developing support for programming languages in KDE or writing documentation, just hop on Freenode in the #kde-bindings or #kross channel, or write an email to the <a href="https://mail.kde.org/mailman/listinfo/kde-bindings">kde-bindings</a> mailing list, and we will be very happy to help get you going.</p>

<p>Thanks to <a href="http://www.kdab.net">KDAB</a> for sponsoring and hosting, and thanks to the <a href="http://ev.kde.org/">KDE e.V.</a> for supporting the event.</p>