---
title: "KDE Commit-Digest for 20th April 2008"
date:    2008-05-02
authors:
  - "dallen"
slug:    kde-commit-digest-20th-april-2008
comments:
  - subject: "Thank you!!"
    date: 2008-05-01
    body: "Good job, as always."
    author: "Fran"
  - subject: "Re: Thank you!!"
    date: 2008-05-01
    body: "Danny already knows that he's my hero, but I'll respond here anyway just so that the rest of the world knows that he's my hero :)\n\nGood digest, as always, Mr. Allen."
    author: "Troy Unrau"
  - subject: "Re: Thank you!!"
    date: 2008-05-01
    body: "Thanks a lot for another great Digest!"
    author: "Poldark"
  - subject: "Re: Thank you!!"
    date: 2008-05-01
    body: "Thanks Dany!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Thank you!!"
    date: 2008-05-02
    body: "Thanks for this really big one diggest. Even if I currently follow the trunk, it's nice to read what's actually new :)"
    author: "Phobeus"
  - subject: "Re: Thank you!!"
    date: 2008-05-02
    body: "Thanks, Danny. Right in time for the weekend. :-)"
    author: "Erunno"
  - subject: "Re: Thank you!!"
    date: 2008-05-02
    body: "Made my friday! The week feels incomplete without those..."
    author: "fhd"
  - subject: "Re: Thank you!!"
    date: 2008-05-02
    body: "and thanks to Danny too :)"
    author: "Sebastian Sauer"
  - subject: "Thank You ++"
    date: 2008-05-01
    body: "Good job danny..."
    author: "Fanito"
  - subject: "openwengo"
    date: 2008-05-01
    body: "openwengo *is* multi-protocol though it is now known as Qutecom."
    author: "yo"
  - subject: "kopete"
    date: 2008-05-01
    body: "once we see 4.1 released will there be any new chat thingies added to kopete, sorry, don't know the word.  I was using gaim or what ever they call it when kopete wouldn't work on KDE 4, and they have a large amount that kopete doesn't have, like myspace, xfire, etc.  And also will we get yahoo msn compatibility? \n\nThanks :) I can't wait for the final release as someone who came from windows and has a lot of friends on it, I think 4.1 will be the tool I can use to convince more people over to linux"
    author: "R. J."
  - subject: "Re: kopete"
    date: 2008-05-02
    body: "Chat protocols is the term you are looking for, I believe. :)"
    author: "Jonathan Thomas"
  - subject: "Re: kopete"
    date: 2008-05-02
    body: "Instant messaging protocols."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Jabber"
    date: 2008-05-02
    body: "Didn't AOL/AIM go Jabber?\n\nI would really, really love if all the major players switched to one format.  Back in the day, email was a series of separate networks without one standard.\n\nI don't care of MSN, Yahoo, etc. want to keep having their own clients with additional features for 5D chibi-avatars, but I should be able to find anyone on any network via their email or username, and converse with them on one standard for sending basic messages. "
    author: "T. J. Brumfield"
  - subject: "Re: kopete"
    date: 2008-05-02
    body: "I believe that is exactly what Telepathy is meant for - sharing protocol support between clients like Kopete and Pidgin."
    author: "Random Guy 3"
  - subject: "Re: kopete"
    date: 2008-07-14
    body: "I have to agree that Kopete could definitely benefit from supporting more protocols. Unfortunately, I'm stuck using Pidgin right now only because it supports MySpaceIM. "
    author: "Al"
  - subject: "KNetworkManager"
    date: 2008-05-01
    body: "Awesome to hear about the KDE4 port of KNetworkManager.  It's pretty much the last KDE3 app that I depend on all the time.  Konversation is the other..\n\nThanks for the digest Danny!"
    author: "Leo S"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "About Knetworkmanager\n\ni have seen in the feature list that somebody work on a networkmanager plasmoid\nan other people on a network-manager backend for solid.\n\nWhat is the difference with this 3 apps.\n\nIs the idea to have an api provided by solid, usable by knetworkmanager or either the plasmoid (does not will make a lot of \"bricks\") ?\n\nOr something else ?\n\nThanks !!\n\n\n "
    author: "okparanoid"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "Hi\nyeah, i think its like:\n\n                             KNetworkManager\n                           /\n                          /\nNetworkManager <---> Solid\n                          \\\n                           \\\n                             Network Manager Plasmoid"
    author: "Emil Sedgh"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "oh! it ignored all whitespaces...\nsorry for messing up."
    author: "Emil Sedgh"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "Solid is the hardware abstraction layer for KDE and is meant to ease the development of cross-plattform KDE application (together with other KDE frameworks like Phonon and, of course, Qt). \nIt is planned that Solid will get a NetworkManager Backend which will handle all networking functions transparently so developers won't have to work with NetworkManager directly anymore but can rely on Solid to do it via different backends on different platforms.\n\nMy uneducated guess about KNetworkMangager:\n\nIt will work directly on top of NetworkManager. KNetworkManager is developed by openSUSE guys and since they'll release openSUSE 11.0 with a heavily modified KDE 4.0 they aren't able to use (and develop against) Solid as long as the backend is not in place.\n\nAbout the plasmoid:\n\nThe plasmoid *might* make use of Solid as both backend and plasmoid will probably be released with KDE 4.1 but I truely don't. One of the developers would have to comment on this."
    author: "Erunno"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "Don't want to flame here but I really don't understand Novell and Opensuse folk here... why Danny Kukawa (knetworkmanager maintainer, hal developer) simply ignores Solid? KNM, as a KDE4 app, should only rely on Solid, and if something is missing in Solid to make this possible, development should be driven towards this goal. As far as I can tell reading Danny's blog, KDE news etc etc, this is not happening right now. Am I wrong? (I hope I am)"
    author: "Vide"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "Maybe it's just an issue of time... They might get this into solid for KDE 4.1 or 4.2?"
    author: "jos poortvliet"
  - subject: "Re: KNetworkManager"
    date: 2008-05-02
    body: "\"why Danny Kukawa (knetworkmanager maintainer, hal developer) simply ignores Solid?\"\n\nI don't think he is. It's probably just a case of Solid being in a working state where KNM can use that instead of NM directly."
    author: "Segedunum"
  - subject: "Re: KNetworkManager"
    date: 2008-05-05
    body: "that has been answered.  Solid is not at a point where OpenSUSE can use it for their upcoming release"
    author: "anon"
  - subject: "Re: KNetworkManager"
    date: 2008-05-06
    body: "So why OpenSuse doesn't put some of its employees working on Solid? (well, they could have started months ago, actually). IMO, Solid is something distro should just fall in love with, cause it can make system management integration much simpler."
    author: "Vide"
  - subject: "Re: KNetworkManager"
    date: 2008-05-08
    body: "I agree, but seriously since Novell took over there was even more coding support for Novell-specific stuff. Fragmenting a community even more.\n\nWith KDE this would be so much better, because KDE is distro-agnostic - if it works on KDE, it will work on distros (with a little bit of tweaking sometimes)"
    author: "she"
  - subject: "latest awards.."
    date: 2008-05-02
    body: "comments?\n\nhttp://www.linuxjournal.com/article/10065"
    author: "Iam Wright"
  - subject: "Re: latest awards.."
    date: 2008-05-02
    body: "I'm glad Amarok won of course, and by a lot. :) \n\nThough I'm not sure why we were in competition against Audacity..."
    author: "Ian Monroe"
  - subject: "Re: latest awards.."
    date: 2008-05-02
    body: "I think it's surprising that even given that Ubuntu is the most popular, and widely more advertised than Kubuntu, KDE still got just a couple less votes than Gnome.  Clearly a lot of people are making the concious choice to use KDE.\n\nGood for digikam to be highly used.  It's a great app.  Surprising that picasa got so many votes.  Perhaps that included the web site as well as the app.\n\nThe fact that vi won the text editor category really shows that the users filling this survey out have nothing in common with the average non-technical user.  Same with tar as a backup utility.  Seriously??  I'm a developer and pretty geeky, but I can't be bothered to learn those tools when much easier GUI tools exist. \n\nIn the package management category, things like rpm are mixed with synaptic even though they're not even remotely the same thing.\n\n"
    author: "Leo S"
  - subject: "Re: latest awards.."
    date: 2008-05-02
    body: "I have seen references around to Canonical own numbers, indicating that over 30% of Ubuntu users run KDE."
    author: "Morty"
  - subject: "Re: latest awards.."
    date: 2008-05-02
    body: "Hi\ni think Gnome's usage is increased because KDE3 is not developed actively and meanwhile Gnome just got new features and polishes.\ngive some time to KDE 4, maybe about 2-3 years and you see how KDE will get more and more attention and users (just on Linux, not Win/OS X).\n\nbtw it looks like that (recently) Gnome is not getting new features and stuff in releases.look at the changelogs...very minor stuff are in announcement and there is nothing exciting added in these 6-months periods, except bugfixes."
    author: "Emil Sedgh"
  - subject: "Re: latest awards.."
    date: 2008-05-04
    body: "vi blows, greasy newbies love to tell me how they use it and it somehow increases their productivity... yadda yadda.."
    author: "anon"
  - subject: "Re: latest awards.."
    date: 2008-05-02
    body: "means nothing.  All it is, is their readers voting.  I don't know anyone that reads that website or even knew it existed."
    author: "anon"
  - subject: "Re: latest awards.."
    date: 2008-05-02
    body: "It's more of a magazine then a website. :)"
    author: "Ian Monroe"
  - subject: "Big thanks to KHTML developers."
    date: 2008-05-02
    body: "It is nice to see constant improvements to KHTML: this one is especially good - \n\"Preloading of network resources via a side-tokenizer.\"\n\nKonq very often \"hangs\" on big pages waiting for some element to load. Thanks to that is should be much quicker. I hope this is recursive and will skip not only first but second, third, ... to get them later. On most sites when those scripts are just icing on the cake and can get all necessary info when it still tries download missing bites (that is, browsing on Windows with IE or FF)."
    author: "m."
  - subject: "Re: Big thanks to KHTML developers."
    date: 2008-05-02
    body: "Indeed.  Page loading does feel somewhat faster in recent builds which is much appreciated.  Thanks KHTML hackers :)"
    author: "Robert Knight"
  - subject: "Re: Big thanks to KHTML developers."
    date: 2008-05-02
    body: "I also notice that. Thanks KHTML devs!"
    author: "fred"
  - subject: "Re: Big thanks to KHTML developers."
    date: 2008-05-05
    body: "And much more stable, except for the flash plugin.\nI'm happy to come back to Konqueror from QtBrowser."
    author: "Josep"
  - subject: "Re: Big thanks to KHTML developers."
    date: 2008-05-06
    body: "(is it worth bothering to post this?)\n\nUsing an older version of flash may help. Complaining to Adobe may also help (as to why you're complaining, see Seli's blog, apparently their newest linux version works only in gecko, can you say \"browser lock-in\"?). With a slightly older version you should be fine unless you're on trunk. Flash 9.0.119 works for me on 4.0 svn branch. So I suspect it would work fine in 4.0.1-4.0.3. \n\nTyping about:plugins in your urlbar will tell you what version you have.\n\nFor reporting bugs, please specify if it's repeatable (those are the interesting ones, I'm told), and be detailed in what you did to cause it. There's a lot of duplicate flash bug reports. Feel free to join bugsquad and test various pathological flash websites if you are bored. Be patient, developers know about it.\n\nAnd remember, you can always kill nspluginviewer.\n\n"
    author: "Blauzahl"
  - subject: "So... this is 4.2?"
    date: 2008-05-02
    body: "4.1 is now in feature freeze, right?  So does that mean it's been branched, and new stuff going in (such as KNetworkManager) are for 4.2?"
    author: "Lee"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-02
    body: "It's in soft feature freeze. Only features that were announced before can be added now. No, it won't be branched before the release, because then too few would work on polishing the code."
    author: "Hans"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-02
    body: "And standalone stuff like KNetworkManager can easily be released independant of KDE releases, thats what extragear are for afterall."
    author: "Morty"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-02
    body: "IIUC, then we use a broken development method because if we used a correct method the developers wouldn't follow it.\n\nCorrect method: 4.1 should have been branched pre-Alpha.  Then it should be determined which features would be included in the release.  Any features (and other changes) not ready for the release -- that wouldn't be completed before the release -- would be removed from the branch then 4.1 Alpha 1 would be released.\n\nNote that with a correct development model that there are never any feature freezes on TRUNK (there is no need for them); instead of a feature freeze, a new branch is tagged.  As the branch is developed towards release there will be freezes on the branch, but not a feature freeze because features would not be added after the first Alpha is released.\n\nI have to say that I don't offer any solution if developers will only work on TRUNK.  All I can say is the obvious: that this simply will not work.  Developers must work on the branch.  If they won't do this, then it is hopeless."
    author: "JRT"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-03
    body: "And why do you think your method (or probably some textbook's method) is more correct than the KDE developers do now? After all, they've been working like this for many years and it never has posed any serious problems that I know of."
    author: "Arend jr."
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-03
    body: "The problem is that KDE releases never reach a high level of stability because new stuff is added that does not reach a high level of stability before the release.\n\nIIUC, this standard method would avoid that problem, which is probably why it is a text book method.\n\nThere are other methods that also achieve this such as having a stable branch and an unstable branch.  New stuff is always added to the unstable branch and then copied to the stable branch when it is sufficiently stable.\n\nAny method that avoids including new and unstable code in what is supposed to be a stable release will work.  Experience has shown that the current KDE method doesn't avoid that.  Releases include code that isn't ready for prime time yet and the result has been that KDE does not become more stable with each release."
    author: "YAC"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-03
    body: "you realy have to stop spreading fud....\n\nthere is no standard \"textbook\" way of doing softwaredevelopment. and in the days of dvcses your proposal is long deprecated. if kde wants to use more branching, every new feature should get its own branch, which are merged back to trunk when the new code is stable.\n\nthough svn isn't meant to be used like that, so heavily using branches in the main repository is actually no option for kde today.\n\nhaving more than one main-branch like you propose is a maintenence nightmare for big projects...\n\n\nbut thats not the point. kde already does what you want. its called the feature freeze, and like its name suggests, it doesn't allow new features in the repository when a new release is stabilizing."
    author: "ac"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-06
    body: "It is not my intent to spread FUD.  I am asking for higher quality.  Or, stating that higher stability in releases is needed.\n\nYou are correct that there is NOT one standard textbook method of doing software development (one dead Straw Man) -- there are many.  However, there objectives which they must meet.\n\nI do not see that the idea is long deprecated since other OSS projects use other models which I believe are better.\n\nNext Straw Man.  I made no suggestion about using many multiple branches.  That is something which you brought up.\n\nI am suggesting EITHER earlier branching for release branches or (not my first choice) having a stable and an unstable release branch.  Such an extra branch might actually make thing easier.  Do you have a citation that would support your unsupported statement (is this just your opinion) that having another branch would be \"a maintenence [sic] nightmare for big projects\".  Quite the opposite is, in general, true.  \n\nSmall projects can use most any development method.  It is large projects that need something more structured.  The KDE project has grown and it now needs more structured methods.  The proof of this is what I stated: lack of stability and regressions along with new features that were in releases before they were ready for prime time.\n\nYou seem to have missed what I said about a \"feature freeze\".  I said that pre-alpha branching can replace a feature freeze.  Obviously, I don't think that the feature freeze accomplishes what I think is necessary or I wouldn't have suggested that change was needed.  Some of the new features added to KDE 3.x.y didn't stabilize enough before they were released. \n\nSo, your argument reduces to 'no you are wrong'.  Do you have any suggestions?  Or, are you just going to use rhetoric and logical fallacies to defend the status quo?"
    author: "JRT"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-03
    body: "How do we know whether something is stable or not. It is tested. Why is it tested? It is released. The challenge is to devise some means of broad testing of software. So far the best answer is to release it.\n\nAnd pray tell, who is going to merge the unstable to the stable branches? That hurdle alone dooms the idea.\n\nFree software isn't a product. It's a process. Methods that work (some of the time) in well funded turnkey solutions usually end up harming free projects. \n\nWhy do I say that? Write 10 rules, enforce them vigorously, and see how many developers you have after a year.\n\nDerek"
    author: "dkite"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-06
    body: "It is axiomatic that stable releases should not contain unstable code.\n\nI believe that I have made two suggestions about how software can be released for testing without releasing a 'stable' release that isn't.\n\nSo, the Linux Kernel project is doomed?  I think not.  Who merges new features?  Obviously, the person that wrote them and wants them included in the release.\n\nFree software may be a process, but ultimately, it must deliver a product.  If it doesn't ever deliver a stable product, then it is only entertainment for the developers.\n\nNO!  I would never try to rigidly impose rules.  Commercial software companies have already found out that this is a bad idea.  Having a good process and maintaining standards are different.\n\nWe have rules in the KDE project.  Some of them are even written.  I was told that I couldn't fix KView to conform to the KDE GUI standards because it would violate the rules to do so.  But, most of our rules are unwritten.  They seem to be enforced vigorously and we do loose developers because of them.  Then we have the unwritten OSS rules that don't seem to be enforced and need to be."
    author: "JRT"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-03
    body: "Hi\nits not the current methods fault.There was a Features Plan from months ago.if KNetworkManager4's developer wanted his program to be in 4.1, its his fault.\nall the thing he had to do was to add KNetWorkManager to feature plan, but he didnt.thats it.\n"
    author: "Emil Sedgh"
  - subject: "Re: So... this is 4.2?"
    date: 2008-05-06
    body: "Perhaps a good point.\n\nBut, how do planned features that turn out not to be ready for release get dropped from the release.  Currently, this doesn't happen."
    author: "JRT"
  - subject: "KFloppy"
    date: 2008-05-02
    body: "Wasn't Solids goal to replace all hardware interfacing code why is KFloppy still alive. "
    author: "Origins"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "Kfloppy is a GUI interface to the Linux utilities which format floppy disks.  There is no KDE hardware interfacing involved."
    author: "JRT"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "But shouldn't it be integrated more discretely to appear only when there makes sense  instead of dedicated front-end which in my opinion clusters space in  appropriate sub-menu of K menu.    "
    author: "Origins"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "> But shouldn't it be integrated more discretely to appear\n> only when there makes sense\n\nYes but as I hope you can appreciate, sorting out floppy-disk formatting is somewhat low on the list of priorities right now - not least because I suspect that the majority of KDE developers do not even have floppy-disk drives in their laptops or workstations.  Consequently Kubuntu for example doesn't ship the utility out of the box and I'm sure that is true of other distributions as well - that is the simplest way to stop it from cluttering up the K-Menu. \n\nIf this is something of a bugbear for you then I encourage you to get involved yourself - even if that means just asking your distribution not to include it out of the box.\n\nOne last thing to point out - \"Solid\",\"Phonon\",\"Nepomuk\" etc. are all libraries of code there for use by developers.  They are not programs which the user can interact with directly.  If you are not developing KDE software then they are not directly of interest to you.\n\n\n"
    author: "Robert Knight"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "And why is there a tool specifically for floppies, and featuring only 3 hard-coded filesystems when many computers don't even have floppy drives? It should be independent of disk type and file system and it indeed shouldn't be a stand-alone app."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "True - it's just that noone has gotten round to doing it."
    author: "John Tapsell"
  - subject: "Re: KFloppy"
    date: 2008-05-05
    body: "That guy is everywhere. \nhttp://www.encyclopediadramatica.com/Noone"
    author: "MamiyaOtaru"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "Sounds like a plan. Seriously, try to do it yourself, coding isn't that hard as soon as you made the first steps :-)"
    author: "Beat Wolf"
  - subject: "Re: KFloppy"
    date: 2008-05-03
    body: "KFloppy has been around for a while.\n\nI think that it clearly needs some work -- needs to be replaced with a utility that can format other types of removable media as well.  And needs a new name.\n\nI have floppies because I have a DIY computer, I have had the floppies for some time. :-)  But, when I upgrade, I don't remove them since I still have some data and software on floppies.\n\nYes, they are becoming obsolete."
    author: "JRT"
  - subject: "Re: KFloppy"
    date: 2008-05-04
    body: "Some of people here didn't understand me I am not saying that floppy drive isn't\nimportant in fact I keep it around in case I need it.I am just saying that it doesn't need a separate front-end for its purpose end that the same thing can be done in a more conformable way for example option to format floppy can appear in  front of user when hi puts unformated floppy in drive and rest of the time be an entry in properties.         "
    author: "Origins"
  - subject: "Re: KFloppy"
    date: 2008-05-04
    body: "> option to format floppy can appear in front of user \n> when hi puts unformated floppy in drive\n\nUnfortunately I don't think this is possible without constantly polling the floppy drive, which is probably not a good idea. Still, you could have a \"Format\" context menu action or similar."
    author: "Paul Eggleton"
  - subject: "Re: KFloppy"
    date: 2008-05-05
    body: "My thinking is that it appears when he try to access the unformatted floppy not before because it is reasonable to suspect that he first needs to format it to use it.    "
    author: "Origins"
  - subject: "Re: KFloppy"
    date: 2008-05-04
    body: "Doesn't QParted already do floppies? If that's true, then KFloppy is already obsolete.\n"
    author: "Riddle"
  - subject: "Re: KFloppy"
    date: 2008-05-04
    body: "Except qparted is really buggy and not at all end user friendly."
    author: "Leo S"
  - subject: "A dream comes true"
    date: 2008-05-05
    body: "Several years ago, I made a wish report about such a feature, thanks Lydia Pintscher and Daniel Jones (and Google of course) to want to make it real.\n\nTitle: A Plan for Automatic Playlists in Amarok\nStudent: Daniel Jones\nMentor: Lydia Pintscher\nAbstract:\n\nThis proposal deals with adding unique feature called \"biased playlists\" to Amarok. Biased playlists are a natural generalization of random or shuffle mode which is standard to media players. They operate similarly, but instead of being purely random, any number of biases may be specified.\n\nThe biases use information and statistics that are already stored by Amarok, (e.g. related artists, score, playcount, last time played) along with a weight factor, specifying proportion of the playlist should satisfy a condition. For example, \"artist: Miles Davis\" with a weight of 0.25, would insist that a fourth of the playlist would be made up of Miles Davis tracks. Any number of these biases can be added to create very unique ways to explore a large music collection."
    author: "Capit. Igloo"
  - subject: "Commit Digest disappearing?"
    date: 2008-05-13
    body: "I'm afraid the great commit digest is slowly disappearing, not only it has\na quite big backlog, but the releases are more and more rare. It unfortunately\nreminds me a pattern from the past or kernel traffic and such. Understandable,\nbut still sad.\n"
    author: "Michal"
---
In <a href="http://commit-digest.org/issues/2008-04-20/">this week's KDE Commit-Digest</a>: The <a href="http://dot.kde.org/1208993986/">start of the Google Summer of Code with 47 KDE projects</a>. Initial version of a kxsldbg plugin for <a href="http://quanta.kdewebdev.org/">Quanta</a>. <a href="http://kross.dipe.org/">Kross</a>-based scripting in KDevelop. Tabs return to the kdevplatform (<a href="http://www.kdevelop.org/">KDevelop</a>, etc) interface framework. A database plugin for <a href="http://kommander.kdewebdev.org/">Kommander</a>, with Kommander widgets becoming accessible within Designer. Support for file attachment and sound annotations in <a href="http://okular.org/">Okular</a>. Work on support for JavaScript runners, and an enhanced visual appearance for KRunner in <a href="http://plasma.kde.org/">Plasma</a>. Desktop search returns to KRunner. An improved implementation of "Send Input to All" in <a href="http://konsole.kde.org/">Konsole</a>. "Close buttons on the right side of tabs" in kdelibs. A search KIOSlave for virtual search folders across KDE. Get Hot New Stuff support for KDE splash themes and chat window styles in <a href="http://kopete.kde.org/">Kopete</a>. A "wobbly windows" effect and non-linear timelines in KWin. The start of a WMI (<a href="http://en.wikipedia.org/wiki/Windows_Management_Instrumentation">Windows Management Instrumentation</a>) backend for <a href="http://solid.kde.org/">Solid</a>. Rewrite of connection management in <a href="http://konversation.kde.org/">Konversation</a>. Work on playlist modes and tooltips in <a href="http://amarok.kde.org/">Amarok</a> 2. A media player plugin to play audio and video files in <a href="http://ktorrent.org/">KTorrent</a>. Initial work on charting/graphing and spreadsheets for <a href="http://www.kexi-project.org/">Kexi</a> reports. Work starts on a Kexi Web Forms Daemon. Initial imports of KLesson, SuperPong, and a KDE 4 version of KNetworkManager. KBreakout and KSirk move from playground/games to kdereview. KSanePlugin moves from playground/graphics to kdereview. printer-applet moves from kdereview to kdebase. Okteta moves from kdereview to kdeutils. <a href="http://commit-digest.org/issues/2008-04-20/">Read the rest of the Digest here</a>.

<!--break-->
