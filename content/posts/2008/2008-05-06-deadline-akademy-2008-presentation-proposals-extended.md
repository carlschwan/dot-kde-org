---
title: "Deadline for Akademy 2008 Presentation Proposals Extended"
date:    2008-05-06
authors:
  - "cschumacher"
slug:    deadline-akademy-2008-presentation-proposals-extended
---
The programme committee of the Akademy 2008 KDE contributor's conference would 
like to thank everybody who already has submitted a proposal for a 
presentation at Akademy 2008. The conference programme is beggining to gain shape. Due to popular request the program committee would like to solicit additional proposals and has decided to extend the deadline for submission of 
proposals to Monday, May 12th. Tell the world about your contribution to KDE. 
Tell the community what cool things you have done with KDE. Submit your 
proposal for a presentation at Akademy 2008 no later than Monday, May 12th 
2008, 23:59 UTC, to akade<span>my-tal</span>ks-2008@kde.org.


<!--break-->
