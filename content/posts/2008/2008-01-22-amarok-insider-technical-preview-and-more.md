---
title: "Amarok Insider on Technical Preview and More"
date:    2008-01-22
authors:
  - "Ljubomir"
slug:    amarok-insider-technical-preview-and-more
comments:
  - subject: "Coverflow?"
    date: 2008-01-22
    body: "I noticed the coverflow like collection browser in one of the screenshots in this article. I've got the technology preview of Amarok installed on my system, but can't find how to enable this feature. Does anybody here now how to do this, since it looks really nice, and I'd like to test it.\n\nOh, and Amarok2 is starting to look really promising. I really like the new playlist! Thanks for all the work, amarok people!"
    author: "pinda"
  - subject: "Re: Coverflow?"
    date: 2008-01-22
    body: "I installed the Kubuntu packages. I can enable CoverBling in Settings/Configure Amarok/Appearance/Use CoverBling.\n\nIt doesn't appear to do much for the time being, anyway."
    author: "Juan Ignacio Pumarino"
  - subject: "Re: Coverflow?"
    date: 2008-01-22
    body: "d'oh, I looked over it... thanks...\n\nIt looks nice btw... I hope it gets functional soon."
    author: "pinda"
  - subject: "Re: Coverflow?"
    date: 2008-01-22
    body: "CoverBling is, as the name suggests, bling and nothing else. Concepts exist exploring the idea of expanding the concept from Apple's coverflow uselessness into a much more useful thing, but as for code, none currently exists bar this proof of concept. Sorry :)\n\nIf you want to help, of course... ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Playing Streams"
    date: 2008-01-23
    body: "I can't even get the included streams (ShoutCast for eg.) to play, as a matter of fact nothing is working like it was with KDE 4 RC1 an RC2. I just don't know why. I even tried to play mp3 files but the program crashes constantly."
    author: "Bobby"
  - subject: "no pics"
    date: 2008-01-22
    body: "Most of the images don't appear on the article page :/"
    author: "Patcito"
  - subject: "Re: no pics"
    date: 2008-01-22
    body: "Fixed, try now."
    author: "Ljubomir"
  - subject: "Re: no pics"
    date: 2008-01-22
    body: "thanx, works great now."
    author: "Patcito"
  - subject: "Great software"
    date: 2008-01-22
    body: "Great software, I love Amarok.\n\nCould you guys include a way that Amarok calls up a KDE video player, Codeine/Kaffeine/whatever so I can play my music videos from within Amarok. (or at least click on them within Amarok?\n\nAlso I second that, it would be great if there were some sort of Wizard, or tutorial mode, that shows off all the features of Amarok in an easy to learn fashion for novices.\n(including: playlists, importing cover art, advanced features, how to edit mp3's, how to get Ipods to work with it, etc..)\n\nAlso could you do some sort of plasmoid that would show what song is currently playing with some simple information that shows as a widget on KDE 4.x? I'd like to see what's playing without having to call up the whole program from being minimized.\n\n"
    author: "Max"
  - subject: "Re: Great software"
    date: 2008-01-22
    body: "In Amarok 1.4, for podcasts you can right click on them and play them with an external app (eg a movies player). For Amarok 2.0 we'll likely integrate a simple video player."
    author: "Ian Monroe"
  - subject: "Re: Great software"
    date: 2008-01-23
    body: "Yeah, I miss the ability to play music videos with Amarok as well. I have several hundreds of clips, but not really a good player to play them all and have some queue management as well. So that option would be good (I already took a sneak peak into Amarok 2 code myself to see if I could shift in some code of my own to accomplish this on a dirty-hack manner).\n\nAnyway: great work, keep it up! And I definitely own you guys some beers might I ever run into you!"
    author: "Bert"
  - subject: "Re: Great software"
    date: 2008-01-23
    body: "I *love* amarok. I spend about 8 hours a day on the computer and I know amarok better than my girlfriend ;) THANK YOU for this great piece of software!\n\nYou obviously are free to integrate whatever you desire, I just want to express my dearest hopes it'll stay a great music player that doesn't get too much into the users face.\n\nI think one should be careful about implementing wishes from minorities (of users). One day, amarok might be able to read my mail, but I'll be probably be using something else by then. I cannot imagine that a significant amount of amarok users want to watch videos in playlists. I might be wrong, though."
    author: "anonymous coward"
  - subject: "Re: Great software"
    date: 2008-01-23
    body: "Well with something like playing videos, its pretty easy to keep it out of the way for users who don't want the feature. I mean if there's no video in the playing track, you don't show a video. And we won't bend our user interface around to make it about playing videos, like (at least old versions of) WMP does, if that's what you were worried about. It will be just simple video playing support, showing up in the context view.\n\nI read the first sentence of your post to my girlfriend, she feels your girlfriends pain. ;) Thanks for your support."
    author: "Ian Monroe"
  - subject: "Re: Great software"
    date: 2008-01-23
    body: "i thought the idea still was to put the video stream in place of the analyse? (as in, when there's audio visualisation included, replace out generated one with the included...) ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Along with a video player"
    date: 2008-01-23
    body: "Along with integrating the simple video player, could you also make it so visualizations could be played on said video player?"
    author: "NuclearPeon"
  - subject: "Re: Great software"
    date: 2008-01-23
    body: "\"Also I second that, it would be great if there were some sort of Wizard, or tutorial mode, that shows off all the features of Amarok in an easy to learn fashion for novices.\"\n\ni suppose it makes most sense to use documentation for this. compiling walkthroughs in wiki probably is the most sensible thing, at least for noe :)\n\n\"Also could you do some sort of plasmoid that would show what song is currently playing with some simple information that shows as a widget on KDE 4.x?\"\n\ngiven the plasma-centric everything, it is possible that plasmoids from amarok could be placed anywhere.\n\nthere's also kirocker - now, if only somebody picked it up and ported to plasma... :)"
    author: "richlv"
  - subject: "BUUUUT... it does not work as expected :("
    date: 2008-01-22
    body: "Well, didn't you promise that it is supposed to crash?\nIt builds my collection and plays my songs - what else should I try?\nI think you should release more buggy code before you start to scare the hell outta me :("
    author: "katakombi"
  - subject: "Re: BUUUUT... it does not work as expected :("
    date: 2008-01-22
    body: "Sorry to disappoint. :("
    author: "Ian Monroe"
  - subject: "Re: BUUUUT... it does not work as expected :("
    date: 2008-01-23
    body: "im not disappointed by the announcement, amarok crashes often if you click often... \nbut, amarok 1.4 is really, really the best music player out there and i am looking fast forward to a even greater 2.0 release :)\n\nkeep up and thanx for the great work !\n"
    author: "Max Merkel"
  - subject: "Re: BUUUUT... it does not work as expected :("
    date: 2008-01-23
    body: "You could always try to play around with some of the services. If you use the filter a lot in Jamendo or Magnatune I am sure we can find a crash for you! :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: BUUUUT... it does not work as expected :("
    date: 2008-01-23
    body: "I am using 1.4 since it's released and I mean intensively and haven't had a crash upto now. \nIt is presently the best out there for sure. I can only hope that Amarok 2.0 will be better."
    author: "Bobby"
  - subject: "I hope the worm doesn't turn"
    date: 2008-01-23
    body: "I've seen many developers of Linux projects begin work on a Windows version only to see the developers become fat with money flowing in from the Windows side and give up on Linux development from being hired away by big companies and told to sit in a corner to discourage them from returning to Linux development.\n\nI hope the focus remains on Linux and the values of FOSS."
    author: "Anonymous"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-23
    body: "With the underlying KDE libraries ported to Windows and OS X there won't be a Windows or OS X version of Amarok per say. It'll all be the same Amarok codebase. Same with any other application."
    author: "Skeith"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-23
    body: "And how many developers like that have you *really* seen? Two or three examples would suffice my curiousity :)"
    author: "Alexandre Prokoudine"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-23
    body: "Skype is the best example that comes to mind at the moment.\n\nFirefox devs are working hard to integrate Firefox into Vista, while they've largely ignored Linux users for some time."
    author: "T. J. Brumfield"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-23
    body: "I disagree based on my impressions of a recent Firefox 3 nightly.  There is better integration with the Linux desktop in a number of areas, most visibly there is improved styling of form controls using Gtk.\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-23
    body: "I wouldn't worry too much about Firefox.\n\nI think the biggest reason FOSS and Linux is getting so big at the moment is BECAUSE OF FIREFOX. It makes sure that websites look the same and are easily decoded across plattforms. I've been running Firefox 3 beta for about a week now, and I don't see any problems with it. (well yahoo mail beta doesn't work right, but that's about it) \n\nI wonder if anybody will integrate Firefox into Plasma (or the RSS part of it)?\n\n\n\n"
    author: "Max"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-24
    body: "Hello,\n\nI think you are giving Firefox too much honor. \n\nThere have been Apache: With Apache, IBM first took note that Open Source may be a good idea.\n\nThen there was of course Linux the kernel, which has turned into a power horse, starting with 2.4, and absolutely there with 2.6 series. The list of companies working on it is like the who is who. From Nokia to Samsung to the more obviously Linux associated ones.\n\nThen there was MySQL, which made Linux together with MySQL the absolute best choice as for fast web applications. It wasn't as capable as Postgresql, but it was damn fast.\n\nGnome/KDE and a lot more successful projects, that really made inroads in companies, and the toolkits behind, Qt and GTK, and not to forget wxWindows are very relevant to a large developer population.\n\nI am a bit hesitant to mention OpenOffice, but to some Windows people an Office that needs no crack, was confusing and some real news and indication, how nice a world computers could be. I am thinking of OpenOffice as a largely failed project technically, but it does the trick for a while.\n\nAnd to answer your question: Of course, KDE doesn't have any need or desire to integrate Firefox, as it has a nice web browser that I absolutely prefer over Firefox (only use it once in a week for that buggy website), and that is Konqueror with its KHTML, which was turned into Webkit and delivered as Safari by Apple (who hired Firefox developers to do that, btw) and is part of Qt 4.4, so it's the natural choice. I think the 4.1 Konqueror is going to surpass Firefox on the web browsing aspect.\n\nBTW: I am currently using a Gutsy/Hardy mix with Konqueror 4 as the default web browser, and I am very impressed by its speed of rendering. Firefox is very slow, even compared to Konqueror 3. I am very much looking forward to the first near finished KDE 4 release.\n\nAnd the RSS part has its counterpart in akregator. I use it every day, it embeds Konqueror nicely, and works perfect. No idea what Firefox does with RSS, but I couldn't stand any not KDE integrated interface unless I really had to.\n\nWell, as you guessed by now, I only have faint memories of Windows. I think the last time I used it for anything else but gaming was like 2002, and only until I got my new employer convinced. I was only the Linux desktop since 2000 and on Linux as a server since 1995.\n\nI think Firefox has helped a great many people to stay on Windows, which otherwise would have been impossible due to the extremely vulnerable Internet Explorer. It gives Microsoft very much needed breathing time, to restart the browser development. Back then, I knew people who went to Linux, because it was better for web browsing. I think it still is, but there is no more pain for the aware Windows user. So, honestly, their contribution to FOSS adoption is under doubt. \n\nIt may still be good, but I think their crappy Netscape code better had remained closed, then more healthy code like KHTML would have prevailed an gone further than it did so far. I am so glad, they managed to get past that now, but years were lost. Mind you, I used Mozilla when the releases were still named like M16/M18, and that was incredibly long ago. The same applies to OpenOffice.org btw.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-25
    body: "Wtf is your point again? We've been trying to analyze your text for the last 10 minutes. How about putting this in one understandable sentence?\n\nOh, and it's off topic."
    author: "Mark Kretschmann"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-24
    body: "That's correct. I tried the latest Beta and I was surprised that it used my KDE icon-theme. \nI also tried the Vista version on Vista but it wasn't there yet, it didn't adopt to Vista's looks.\n\nBtw, has anyone tried to run the laest Firefox beta on KDE 4?"
    author: "Bobby"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-23
    body: "Skype isn't open source, and I'm pretty sure was a Windows app first."
    author: "Ian Monroe"
  - subject: "Re: I hope the worm doesn't turn"
    date: 2008-01-24
    body: "These are the initial releases:\n\n29.08.2003 Skype for Windows Beta 0.90.0.5\n21.06.2004 Skype for Linux Beta version 0.90.0.3\n31.08.2004 Skype for Mac OS X Beta 0.8.0.2\n\nSo ordered by time, it was Windows, Linux and then MacOS.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Thank You!!"
    date: 2008-01-23
    body: "Thank you again for working so hard on Amarok!!\n\nThis will be the greatest music player EVARRR!!! <--- Sorry, had to. :)\n\nCould you please integrate some way to play Pandora streams within Amarok (hopefully that's even possible..)\n\nI agree, focus on making it a great audio player, we really don't need a fancier feature, than just one that let's us play music videos buried within song playlists. (I have a few songs, that are only part of a video)\n\nThanks for the plasma integration too.\n\nAre you going to include some sort of \"upsampling\" that makes the sound quality of music better than the actual file?\n\nDoes Amarok support (or will it) support a feature that creates playlists on similar style of music (like it analyzes the beat patterns, or something an groups like songs together?)\n\nAlso, please add some sort of first time wizard, or video tutorial on Youtube, nothing to fancy. But I want to get to know all of Amarok's features, as well as cool \"shortcuts\".\n\n"
    author: "Max"
  - subject: "Re: Thank You!!"
    date: 2008-01-23
    body: "Hey, thanks for the praise! Feedback like this keeps us motivated :)"
    author: "Mark Kretschmann"
  - subject: "Re: Thank You!!"
    date: 2008-01-23
    body: "Why not some kind of pluggable playlist import?  This could allow separate programs or webservices to generate the \"similar\" music playlists?"
    author: "joe l"
  - subject: "\"10 foot\" interface / \"Car PC\" interface"
    date: 2008-01-23
    body: "I know you already have enough on your plate, but could you also design the option/theme in Amarok for a \"10 foot\" interface, that can be read from far away, and a \"CAR PC\" interface that can be used on a tiny 7\" touch screen?\n\nBtw, it would be great if KDE itself designs some plasmoids/theme/front end, that work well on a 7\" touch screen in a \"Car PC\". \nKDE 4.0 (I know it's not ready yet and mostly a developer release..) wastes so much screen real estate, and on small screens, or low resolution TV screens, it's quite annoying. I hope that's fixed by the time 4.1 comes around. =)\n\n"
    author: "Steve M"
  - subject: "Re: \"10 foot\" interface / \"Car PC\" interface"
    date: 2008-01-23
    body: "Well we do hope to improve our scripting interface. Making such an interface would be pretty easy then."
    author: "Ian Monroe"
  - subject: "What are the features of amarok 2.0?"
    date: 2008-01-24
    body: "Where can I find the list of amarok 2.0 features?"
    author: "Sagara"
  - subject: "Re: What are the features of amarok 2.0?"
    date: 2008-01-25
    body: "We haven't even gotten to the point of bothering to enumerate them."
    author: "Ian Monroe"
  - subject: "Any news?"
    date: 2008-01-28
    body: "Any news on Amarok?\n\nI've been interested in Amarok ever since I saw the KDE Keynote. Has there been any cool new features added? Or betas?\n\n"
    author: "Richard Lionhard"
---
<a href="http://amarok.kde.org/Insider/">Amarok Insider</a> is the newsletter previously known as Amarok Weekly News (AWN), and is now hosted on the <a href="http://amarok.kde.org">official Amarok website</a>. The <a href="http://amarok.kde.org/Insider/Issue_11">new issue</a> covers the freshly released <a href="http://amarok.kde.org/en/Amarok_Kutie_TP1">Amarok 2.0 Technical Preview</a>, Amarok's Media Device architecture, the Context View, Playlist, Service Framework, the MS Windows version, recent happenings inside the Amarok team, and much more.


<!--break-->
