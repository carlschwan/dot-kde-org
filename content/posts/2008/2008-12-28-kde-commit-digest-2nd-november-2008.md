---
title: "KDE Commit-Digest for 2nd November 2008 "
date:    2008-12-28
authors:
  - "dallen"
slug:    kde-commit-digest-2nd-november-2008
comments:
  - subject: "Just to say"
    date: 2008-12-28
    body: "Go Danny go!"
    author: "Dimitri Povlesky"
  - subject: "Re: Just to say"
    date: 2008-12-28
    body: "Thanks! Pump 'em out! Thank you!!"
    author: "Eric"
  - subject: "Re: Just to say"
    date: 2008-12-28
    body: "<i>Deleted. I know that Danny probably would have mildly smiled about this one, but I won't let anonymous cowards poison the atmosphere any further. --danimo</i>\n"
    author: "Anon"
  - subject: "Re: Just to say"
    date: 2008-12-28
    body: "that was obviously just a flamebaiting troll"
    author: "Dimitri Povlesky"
  - subject: "Re: Just to say"
    date: 2008-12-28
    body: "Slower Danny, slower !! I can't read them all !!! ;-)\n\nThank you for this work, really appreciated !"
    author: "DanaKil"
  - subject: "Trends"
    date: 2008-12-28
    body: "3% by female committers, that's about double the percentage of the previous few weeks, and something like four times as much as the usual percentage I remember. That's nice to see - I wonder what caused this spike, and whether it's a real trend (which would be terrific) or rather a short-term effort of a single woman."
    author: "Jakob Petsovits"
  - subject: "Re: Trends"
    date: 2008-12-28
    body: "that was short after the release of kmylittlepony "
    author: "Petar Popara"
  - subject: "Re: Trends"
    date: 2008-12-29
    body: "lol"
    author: "mark"
  - subject: "Re: Trends"
    date: 2008-12-29
    body: "not-lol\n\nReally, some people need to get a life."
    author: "Odysseus"
  - subject: "Re: Trends"
    date: 2008-12-29
    body: "I would like to see at least 30% females committers, that would make KDE even more beautiful :)"
    author: "Bobby"
  - subject: "GO GO GO!!!"
    date: 2008-12-28
    body: "This tempo we're seeing should shut up everyone whining about skipping digests up to current. EXCELLENT WORK! Keep it up! :)"
    author: "slacker"
  - subject: "wrong link"
    date: 2008-12-28
    body: "Hey Danny,\n\nif you click on the marble map link (the one that should lead you to http://commons.wikimedia.org/wiki/Image:World_Map_1689.JPG, it instead redirects you to http://commit-digest.org/issues/2008-11-02/files/Image:World_Map_1689.JPG).\n\nAnyway, thanks for yet another great digest!"
    author: "me"
  - subject: "Re: wrong link"
    date: 2008-12-28
    body: "The link pointing to the Digest server is actually not a mistake: it is so that years and years from now when these external files have moved or the sites no longer exist, a copy still exists and the link is still alive.\n\nOf course, readers can still copy the address and visit it manually to see the source.\n\nHowever, this file didn't actually make it to the server correctly (it's 19mb!) - i've fixed that now.\n\nThanks,\nDanny"
    author: "Danny Allen"
---
In <a href="http://commit-digest.org/issues/2008-11-02/">this week's KDE Commit-Digest</a>: Added support for images in the "Pastebin" <a href="http://plasma.kde.org/">Plasma</a> applet, drag and drop support added to the "Quick Launch" Plasmoid. All comic providers for the "Comic" Plasmoid moved to kde-files.org to be solely accessible through the GetHotNewStuff mechanism. Security improvements in KWallet. Support for Mono-based KIO-slaves. Addition of a historic map (from 1689) display to <a href="http://edu.kde.org/marble/">Marble</a>. Improvements in scripting in <a href="http://ktorrent.org/">KTorrent</a>. Experiments to allow acquiring images using the TWAIN interface under Windows in kipi-plugins (<a href="http://www.digikam.org/">Digikam</a>, etc). A small Konqueror <a href="http://nepomuk.kde.org/">NEPOMUK</a> annotation plugin, enabling annotation of web pages. A new NEPOMUK Search KRunner in Plasma, added straight to kdebase/workspace. "Partition Manager" moved from playground/sysadmin to kdereview. Kolor Manager configuration panel imported into playground/graphics. Following work on statistics, support for importing cached album art from <a href="http://amarok.kde.org/">Amarok</a> 1.4 into Amarok 2, with new and improved library-based last.fm interface code imported. Amarok 2.0 Beta 3 (1.94), <a href="http://www.kdevelop.org/">KDevelop</a> 4 Alpha 3, and KDE 4.1.3 tagged for release.<a href="http://commit-digest.org/issues/2008-11-02/">Read the rest of the Digest here</a>.

<!--break-->
