---
title: "KDE Commit-Digest for 27th January 2008"
date:    2008-02-02
authors:
  - "dallen"
slug:    kde-commit-digest-27th-january-2008
comments:
  - subject: "Nice new effect Martin Graesslin"
    date: 2008-02-02
    body: "While you're at it, you could add the cube too, I think it uses 3D too :)"
    author: "Patcito"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-02
    body: "yeah.. having a cube like in compiz would be amazing.. I'm really missing it... but before this can be added I think that the overall performance of kwin-composite must be improved... at least on my ATI X700 with the latest fglrx it's unbearable slow :-(\nbut nonetheless: great work kde guys!"
    author: "Bernhard"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-02
    body: "I've been toying for a while with the idea of having a KWin Effect Competition with a small cash award as the prize and the Oxygen guys as judges.  It might prove burdensome for Seli, Rivo and the Oxygen fellows, though :("
    author: "Anon"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-02
    body: "I think that's a great idea - I would definitely donate a little cash towards it.  I think there should be a competition for plasmoids as well though, it would help the kde 4 desktop reach feature parity with 3.5 a lot quicker and also help encourage much of the innovation we have been hearing a lot about."
    author: "Paradox"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-03
    body: "+1"
    author: "Steve M"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-02
    body: "good idea, lets do a fundraiser first and then the competition."
    author: "TeeZee"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-03
    body: "Yes!! Please port the rest of compiz over to Kwin.\n\nI REALLY miss the effects.\n\nCube, expose, coverflow, among others."
    author: "Steve M"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-03
    body: "kwin has an expose-like effect already, no?"
    author: "Anonymous"
  - subject: "Re: Nice new effect Martin Graesslin"
    date: 2008-02-03
    body: "Please: \n\nAt the very least make parity to the effects and speed compiz fusion has. At the best add speed and add original effects.\n\nPeople love the effects. I hope in the future they will be turned on by default if the system is capable of handling them. (distro's are you listening?)"
    author: "Richard"
  - subject: "\\o/"
    date: 2008-02-02
    body: "Thank you Danny :)"
    author: "shamaz"
  - subject: "Plasma in Dolphin / Icon overlays"
    date: 2008-02-02
    body: "First off, the KDE4 K3B port looks amazing!\n\nI was wondering about the icon overlays that were mentioned in the \"KDE 4.1 Feature Plan\": http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan\n\nThis means *ALL* icons will have the overlay not just the ones on the desktop? Does this mean Plasmoids are actually going to be inside Dolphin?\n"
    author: "josh"
  - subject: "Re: Plasma in Dolphin / Icon overlays"
    date: 2008-02-02
    body: "The overlay for desktop icons will have features that the Dolphin overlay wouldn't need, like resizing and rotating. Chances are they'll be separate overlays anyway, since it would be silly to have a Plasma dependency in Dolphin.\n\nAlso nice to see that Raptor is confirmed (to be aiming) for 4.1, I'm excited to see it action with the plans they have for it."
    author: "Skeith"
  - subject: "Re: Plasma in Dolphin / Icon overlays"
    date: 2008-02-02
    body: "I don't see why a feature like rotation is necessary in one place, but not in others.  Besides, it generally shows better design when features are orthogonal to where they're used."
    author: "Lee"
  - subject: "Re: Plasma in Dolphin / Icon overlays"
    date: 2008-02-02
    body: "> This means *ALL* icons will have the overlay not\n> just the ones on the desktop?\n\nNo, currently we just try finding a solution for Dolphin/Konqueror to select files in the single-click mode in an easy way. It has not been decided yet about adding other overlays too (from the current point of view I'd say that there is no need for this). BTW: this feature can also be turned off...\n\n> Does this mean Plasmoids are actually going to be inside Dolphin?\n\nTechnically no, but for sure we'll try to stay consistent from the look and feel where it makes sense. Plasma raises the bar for applications making things visually handsome :-)"
    author: "Peter Penz"
  - subject: "Re: Plasma in Dolphin / Icon overlays"
    date: 2008-02-03
    body: "I think this is a solution to a non-problem.  I mean, what use is selecting something if you don't then do something with it?  (drag, copy, delete etc).  All the possible actions one could want to take are available in the right click menu.  Including (potentially) right click -> select, if you really do just want to select a file and then do nothing with it.\n\nI'd prefer to leave the whole icon available for opening the file, and if one wants to take a specific other action, right click.\n\nAnd besides, one can already easily select (and then do nothing with) an icon in single click mode.  If in detailed list view, click anywhere on its row except for its name (it in blank space next to name, on the date etc).  There was a justified bug report made when things were changes to a single click anywhere on the row opened the file and it was changed back and works now as well as before.  If in icon view, lasso a single icon.  \n\nThe old ways are a bit clunky, but are they broken enough to cut the target size of an icon to a third when the right click menu does everything one would need?  I mean seriously, hover over an icon then select \"actions\"?  I doubt many KDE users are going to be using old one button apple mice."
    author: "MamiyaOtaru"
  - subject: "Re: Plasma in Dolphin / Icon overlays"
    date: 2008-02-03
    body: "I strongly disagree. I find it a lot easier to press the Del key than aim at the delete option in the context menu. Muscle memory.\n\nThat said, the icon-overlay solution sounds too clunky and I'm pretty sure using it wouldn't be fun. Actually, my ideal selection method would be middleclicking."
    author: "logixoul"
  - subject: "Kooka"
    date: 2008-02-02
    body: "\"The Kooka scanning application finds a new maintainer, with various initial improvements.\"\n\nWhat? I read it over and over again. There is a new maintainer? To bad that there is no information on the Kooka homepage. However, I'm really glad to hear this!\n\nBTW, any news about Decibel?"
    author: "jimB"
  - subject: "Re: Kooka"
    date: 2008-02-02
    body: "Having recently acquired a wonderful new scanner and then finding that it didn't work with Kooka, I've volunteered to step in and maintain/update that application.  It won't reappear in kdegraphics until it has been ported to KDE4 and gone through the review process, but if you are interested you can compile from SVN (branches/work/kooka-kde3) and try it out.  Any feedback will be appreciated!\n\nI'm concentrating on the code so far rather than the web pages, but that will get updated sometime...\n"
    author: "Jonathan Marten"
  - subject: "Re: Kooka"
    date: 2008-02-02
    body: "Hi Jonathan,\n\nthanks for the kind reply. Now there is a name. I'm glad that your scanner was not working with Kooka. :)\nRight know I'm using Kooka the way it is since this tool is working quite well for me. Thus I don't know if I'll find the time to give you some feedback (sorry) unless I stumble in a serious issue with the current version. But I certainly will dig through SVN some time (maybe today ;) ).\nAbout the web page. Of course there is no need to hurry or so. You will certainly publish some information when Kooka is in the appropriate shape.\n\nKind regards & keep up the good work.\njimB"
    author: "jimB"
  - subject: "Re: Kooka"
    date: 2008-02-04
    body: "Well done for jumping in as maintainer!\n\nI've dabbled a bit in libkscan and kooka. Reported some memory problems a while ago and recently added a photocopy button on one of the drop down menus, even though I'm a bit of a newbie with qt.\n\nCan I be of any help?\n\nRegards\n\nAlex"
    author: "AlexKempshall"
  - subject: "Re: Kooka"
    date: 2008-05-16
    body: "Thank you for keeping this project alive!!\n\nI know there is probably a lot of other basic stuff that needs to be updated first, but is there any chance we will see the Tesseract and OCRopus projects from Google integrated for better OCR capabilities?\n\nhttp://code.google.com/p/ocropus/\nhttp://code.google.com/p/tesseract-ocr/"
    author: "Travis"
  - subject: "Kwin effect"
    date: 2008-02-02
    body: "Personally I don't like the Vista effect, but I'm sure it'll make some happy. Great work!\n\nWhile reading that part of the digest, I thought of a new window-switching effect. Just a crazy idea that popped up, I haven't given it enough thought yet; but hey, let's share it and see what you think.\n\nI think one word is enough to describe the effect, as it has become quite known: Coverflow. Or, should I say \"Windowflow\"? ;)"
    author: "Hans"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "Do'h. Just did some searching, and it seems that effect already exists in Compiz:\nhttp://artipc10.vub.ac.be/serendipity/uploads/screenshots/mandriva2008/kde-compiz-coverflow.jpg\nhttp://www.youtube.com/watch?v=xBRRe2PP4CY\n\nBut still, wouldn't it be a nice effect for KWin? =)"
    author: "Hans"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "plz dude, no more effects, stop dillusioning, on this or that supercalifantastickool effect,    there are way other stuff to be done first to have all the *bling* working at full throttle , i.e, projector Hotplugging, blank sync, rendering, namely.\n\nKDE devs: Don't listen to the trolls !!, after all kde will win in the long run, there is nothing more appealing than plus+ functional desktop.\nI'm really tired of the meme \"just works\".\nFor instance compiz is super fast and nice, but it's cramped of glitches.\n\nSure from most users standpoint (and maybe some programmers), it's perfect, gives all the bling it promises, it's state-of-the-art-code. But what about when you browse a flash web and the cpu spikes % 100 usage ?? hehe, not so \"state-of-the-art\",I guess... maybe compiz category should be degraded to : \"maybe works\", or works best web pages flash where the it's colour is apricot. \nAnd don't get me wrong I like compiz a lot. \nppl will fall out over the time.\n\nKwin really does lot more, it has less bugs and does lot more for day to day usability.\n\nWe need stuff that works great, and usefull also, (err shouldn't that be named! ;) \"program\")...that's the word\n\nNOT a demo-ware!!\n\n"
    author: "joekey1"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "Before Cube and all this things are implemented, i would very much like the idea of nice and smooth blend-in and blend-out, just like in KDE3 but faster and with less bugs."
    author: "blueget"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: ">> KDE devs: Don't listen to the trolls !!,\n\nUh? Was the troll part directed at me? I don't see how I'm trolling - if my post really offended you, please tell me how and, if it makes sense, I'll try to avoid it in the future. To me, giving a suggestion != trolling.\n\n>> plz dude, no more effects, stop dillusioning, on this or that supercalifantastickool effect, there are way other stuff to be done first to have all the *bling* working at full throttle , i.e, projector Hotplugging, blank sync, rendering, namely.\n\nPlz dude, I don't think you're the one to tell what the developers should be working on. Sure, some things are more important to users, but AFAIK the developers are still free to work on what they want.\n\nIn this case, we have someone who knows OpenGL and has implanted the first(?) 3D KWin effect. If that's what he like to do, should we stop him and rattle off things that are \"more important\" than \"*bling*\"?\n\nThat's what I would call trolling.\n\nI never said that I _must_ have that effect, nor did I say that it was fantastic. I just gave a mere suggestion, and if you don't like it, why not just ignore it or at give some constructive critic."
    author: "Hans"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: ">>Uh? Was the troll part directed at me? \n\nnah, it was not specifically directed to you, it's just the general climate on the bling o-factor ;-)\n please of course... come on everyone is free to do whatever till heart content == open source!, \n\nYou know everyone rambles on the same subject ...\"I want widgets to do xxx, I want windows to fly that xx way, etc ,etc.\"..  \n\nIMHO _boring_!\n \nDidn't not intented trolling, you really twisted what I said.\n\n>>Plz dude, I don't think you're the one to tell what the developers should be working on\n\nhmm, sorry, you are right and I am wrong, I am an ugly, stinky and stupid troll.\n\n\nAnyways as a matter of fact I do have a constructive critic. \nThe compiz guys already started their decorator port of Kwin/Kde4, \nhttp://gitweb.compiz-fusion.org/?p=compiz;a=commit;h=176a81107c9b77439850c2c1f7d9ecb138b7e2cd\nhttp://gitweb.compiz-fusion.org/?p=compiz;a=commit;h=d29094df5fcc0c0e1eb13a02cdb7662085cef9c6\n\n\nwhy have  a duplicate list of excentric complex features requests, it's just too much, why don't just leave the hard ones to them ?\n\n"
    author: "joekey1"
  - subject: "Re: Kwin effect"
    date: 2008-02-04
    body: ">> Plz dude, I don't think you're the one to tell what the developers should be \n>> working on\n \n>  hmm, sorry, you are right and I am wrong, I am an ugly, stinky and stupid \n>  troll\n\nWhat am I missing here?\n\nUsers have every right to tell developers what they think about the project.  I think that it is called FREE SPEECH!  Nobody said that the developers have to listen to the users (only that it would be a good idea if they did so).  Developers can be as arrogant as they want, but the question is what the intention of the KDE project is.  Do we intend to make a usable DeskTop to be used by the public, or is it just to amuse the developers?  I really doubt that, in the long run, a project that continues to be partially done, and with serious quality issues, will be successful with anyone other than *NIX Geeks."
    author: "A Critic"
  - subject: "Re: Kwin effect"
    date: 2008-02-04
    body: "Maybe I was unclear, and I apologize if it offended anyone. However, that is my point of view:\n\n\"Users have every right to tell developers what they think about the project.\" Yeah, I completely agree. But that's not what I meant; you can't _force_ developers to do anything. That's what it all boils down to.\n\nI gave a suggestion of what I think could be a neat effect. And joekey1 responded with, what sounded to me like, \"shut up troll, the developers should work on making KDE more stable\". Could be because of my (lack of) English skill, but that's how I interpreted his post.\n\nWhile I agree that stability is more important than bling, I still want to stand by my point: users are free to give suggestions, constructive critic etc, but shouldn't decide what the developers have to work on. If someone likes to implant KWin effects, should we force him/her to fix bugs in KDElibs instead? \n\nIf you find something unstable, you're free to report bugs or get the devs' attention in other ways; but you can't force anyone to fix that bug, can you?"
    author: "Hans"
  - subject: "Re: Kwin effect"
    date: 2008-02-04
    body: "> ... you can't force anyone to fix that bug, can you?\n\nNo, users can't.  However, the developers need to take steps to see that more bugs are fixed and fixed quicker.  I leave it to the developers to decide how to organize a quality management group.  All that I can say is that it needs to be done, and soon."
    author: "JRT"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "What trolling are you talking about?\n\nPeople like eye-candy.  And while it may not help productivity for certain effects, people still like them."
    author: "T. J. Brumfield"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "\"People like eye-candy. And while it may not help productivity for certain effects, people still like them.\"\n\nSOME people like eye candy. SOME people don't like eye candy and probably a lot of people don't care. I don't like it because:\n\n1. KDE4 is far from being finished and there are things much more important for the developers to focus on like getting the configuration tools finished and working properly.\n\n2. I'm concerned about bloat. The RAM usage on my machine using KDE4 is more than twice what it is using KDE3. My machine can handle it (2 GB RAM) but my wife's machine (512 MB) will start to hit limits and the VM will start running which will slow her machine down. Some people say just buy more RAM because RAM is cheap. DDR2 RAM is cheap now but the older type of RAM that my wifes machine needs isn't. I showed KDE4 to my wife and she likes it but there's no way she'll want me to put it on her machine if it means we have to buy a new machine for her or spend a lot of money upgrading the RAM. If I see the memory footprint drop later then I'll probably put KDE4 on her machine otherwise I won't. There must be a lot of other people in that same situation that have machines about 7 years old and are now using KDE3 but won't use KDE4 because it's too bloated.\n\nThe only special effect I like is the magnify feature that sometimes makes it easier to read or see objects. I hope the developers don't add more eye candy to KDE4. Please make it reliable, implement the configuration features we need and reduce the memory footprint if you can.\n   "
    author: "Jeff Strehlow"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "You worry about bloat when every KWin animation is a separate plugin, which is only loaded if you've enabled it?\n\n...\n\n:-p"
    author: "Diederik van der Boor"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: "I don't know how the animations are implemented but one thing for sure KDE4 does take a lot more memory than KDE3. I'd rather see the developers working on finishing KDE4 and making it work right and reducing the footprint than working on unimportant things. "
    author: "Jeff Strehlow"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: "Most of the extra memory usage comes from Qt4's double-buffering of widgets, which virtually eliminates flickering.  It's a design choice, so if you disagree with it, take it up with TT - it's largely out of KDE's hands."
    author: "Anon"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: "And correct me if I'm wrong, but won't the next QT release (4.4?) fix this?"
    author: "T. J. Brumfield"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: "I don't know, to be honest: I'd *imagine* \"Alien\" would reduce the need for double-buffering when resizing and such, but I'm not sure if it would be so effective that TT would turn double-buffering off.  My guess is \"no\" :/"
    author: "Anon"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: "Yes. I second that.\n\nEye candy was the whole reason I dumped Vista and went over to linux.\n\nIf it weren't for compiz fusion's effects I wouldn't have bothered.\nThere just isn't the same amount of eye candy (and useful eye candy) on ANY other operating sytem at the moment.\n\nNow I love linux.\n\n"
    author: "Steve M"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: "You are right about the functionality and getting KWin to work more perfectly before introducing more blings but I think that it's unfaif to describe peoply as trolls just because they request more blings.\nT am a very loyal KDE user and I can wait but to tell you the truth, I also miss some of Compiz-Fusion's blings. Some are overkill but some are just breathtaking beautiful and also useful.\nI too hope to see some of these useful effects/functions implemented in KWIN 4 as an option for those who love them :)"
    author: "Bobby"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "I allready have a little bit of this Coverflow (but called it ringswitch). It is not yet committed as there are still some things to do :-) Give me some more weeks then I think it will be ready, but actual sources can be find in KWin mailing list."
    author: "Martin"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "Martin, you rock! :)\n\nI tried the FlipSwitch, and it's very smooth. However, when using it I found some small issues such as:\n\n- With only two windows, the animation was a little bit weird. I hope you notice what's wrong, as I don't know how to explain it. \"Back and forth\" is the best description I can think of.\n\n- Hold down <Alt> and <Tab> (given that you have keyboard repeat enabled). Just hold for some seconds and then release the keys. The animation doesn't stop, it'll continue for a while without any apparent method to stop it.\n\n- And finally: better support for multiple monitors. Right now it's between my two screens, which is a little bit annoying (because of the \"gap\").\n\nOtherwise it works very good, great job! Do you want me to file these issues bugs at bugs.kde.org?"
    author: "Hans"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: "Thank's for the feedback :-D\n\n>>  With only two windows, the animation was a little bit weird. I hope you notice what's wrong, as I don't know how to explain it. \"Back and forth\" is the best description I can think of.\n\nI know, but I liked it that way, so I did not try anything to fix this issue ;-) Perhaps I'll do something about it. At the moment: \"It's not a bug - it's a feature\" :-D\n\n>> Hold down <Alt> and <Tab> (given that you have keyboard repeat enabled). Just hold for some seconds and then release the keys. The animation doesn't stop, it'll continue for a while without any apparent method to stop it.\n\nActually there is a method for it. The animation will continue until the correct window is selected. Ok, I have never tried with holding both keys at the same time. I don't know if this is a real usecase ;-) If you press in a  moderate way the animation will continue correctly. By the way - just tested: boxswitch does not work correctly with this use case, too.\n\n>> And finally: better support for multiple monitors. Right now it's between my two screens, which is a little bit annoying (because of the \"gap\").\n\nThat's bad. I only have one screen and of course not tested with two. What are you using? Xinerma or something like that? But I think I know the problem. I'll test it with my laptop and try to fix it :-)"
    author: "Martin"
  - subject: "Re: Kwin effect"
    date: 2008-02-02
    body: ">> Hold down <Alt> and <Tab> (given that you have keyboard repeat enabled). Just hold for some seconds and then release the keys. The animation doesn't stop, it'll continue for a while without any apparent method to stop it.\n\nJust fixed this bug with revision 770090. Thank's for the hint ;-)"
    author: "Martin"
  - subject: "Re: Kwin effect"
    date: 2008-02-03
    body: ">> \"It's not a bug - it's a feature\" :-D\n\nHaha, fully acceptable.\n\n>> Just fixed this bug with revision 770090. Thank's for the hint ;-)\n\nDid I mention that you rock?\n\n>> That's bad. I only have one screen and of course not tested with two. What are you using? Xinerma or something like that? But I think I know the problem. I'll test it with my laptop and try to fix it :-)\n\nI use Nvidia's twinview, a snippet from my xorg.conf:\n\nSection \"Device\"\n    Identifier     \"Device0\"\n    Driver         \"nvidia\"\n    Option         \"TwinView\" \"True\"\n    Option         \"TwinViewOrientation\" \"RightOf\"\n    Option         \"UseEdidFreqs\" \"True\"\n    Option         \"MetaModes\" \"1280x1024, 1280x1024; NULL, 1152x864; NULL, 1024\nx768; NULL, 800x600; NULL, 640x480\" \n    Option         \"UseDisplayDevice\" \"CRT, DFP\"\n    Option         \"TwinViewXineramaInfoOrder\" \"DFP-0\"\n\n    Option         \"AddARGBGLXVisuals\" \"true\"\n    Option         \"DisableGLXRootClipping\" \"true\"\n    Option         \"RenderAccel\" \"true\"\n    Option         \"AllowGLXWithComposite\" \"true\"\n    Option         \"TripleBuffer\" \"true\"\nEndSection"
    author: "Hans"
  - subject: "k4b"
    date: 2008-02-02
    body: "will k3b now be renamed to k4b (kde 4 burning)?\nor what does k3b actually stand for?\nI never got it..."
    author: "burner"
  - subject: "Re: k4b"
    date: 2008-02-02
    body: "http://dot.kde.org/1145245411/1145247602/"
    author: "Anon"
  - subject: "Re: k4b"
    date: 2008-02-02
    body: "ah, thanks"
    author: "burner"
  - subject: "Re: k4b"
    date: 2008-02-02
    body: "I would like if they put the old progress icon back instead of the new big progress-bar..."
    author: "George"
  - subject: "Don't get me wrong..."
    date: 2008-02-02
    body: "Don't get me wrong, but isn't fixing A LOT of bugs more important than implementing new functionality to apps? I (and many, many users) will never be able to use those new versions of apps as long as the core of KDE is not stable enough. And that's not good at all."
    author: "mkrs"
  - subject: "Re: Don't get me wrong..."
    date: 2008-02-02
    body: "\"Don't get me wrong, but isn't fixing A LOT of bugs more important than implementing new functionality to apps?\"\n\nThis may blow your mind, but many devs can do both of these things at once! Also, KDE is a volunteer-driven project, so people will work on what they want: it's all very well to say \"people should be fixing bugs in KDE before adding features to apps\", but how are you going to make it happen? Also also, the people working on apps probably aren't the same people working on KDE core, and may have none of the competencies that the libs hackers require - lib and app work is often very different and require different skillsets."
    author: "Anon"
  - subject: "Re: Don't get me wrong..."
    date: 2008-02-02
    body: "> Don't get me wrong, but isn't fixing A LOT of bugs more important\n> than implementing new functionality to apps?\n\nA lot of KDE developers have spend months before KDE 4.0.0 only with fixing bugs. This is important but also can get quite frustrating and is really far from being fun. Starting implementing some features for 4.1 is important for the motivation of developers and does not mean that no bugs are fixed in parallel. E. g. currently I spend around 30 % of my time for new features/improvements and the rest for bug fixing in Dolphin. The feature freeze for KDE 4.1 is at the end of March, so it's important getting the rough design for features fixed until this milestone."
    author: "Peter Penz"
  - subject: "Re: Don't get me wrong..."
    date: 2008-02-02
    body: "Alright, but don't you have a feeling that a new version will be the one in which you remove only some of the old bugs and add a lot of new ones?\n\nYou see, the problem is that even the fanciest app is frustrating when it's buggy. Please don't forget that the most important thing about any computer program is usability. Without it, a program is worth nothing more than a wallpaper - maybe it looks great, but you get no benefit from using it if it hangs every 20 minutes, just like KDE 4.0.0 does on my computer (which is quite new and the computer itself is not a source of this problem). It's your free will to do what you want as devs, I do appreciate your hard work and I do love the idea KDE - but I am just very sad to say that I (and many other users) will never have a chance to check out all these new fancy functionalities as long as I can't run KDE and keep it stable!\n\nThanks for great work anyway!"
    author: "mkrs"
  - subject: "Re: Don't get me wrong..."
    date: 2008-02-02
    body: "> Alright, but don't you have a feeling that a new version\n> will be the one in which you remove only some of the old\n> bugs and add a lot of new ones?\n\nJust some current numbers: since KDE 4.0 we've fixed around 50 Dolphin related bugs and have added 3 minor features/optimizations. Assuming that each of those 3 features will generate 5 new bugs (and that guessed number is quite high), then still we have a rate of 50 fixed bugs vs. introducing 15 new bugs...\n\n> but you get no benefit from using it if it hangs every 20 minutes,\n> just like KDE 4.0.0 does on my computer (which is quite new and\n> the computer itself is not a source of this problem)\n\nI cannot confirm such an instability with KDE 4.0. I'm working with KDE 4.0 each day and had 2 crashes since the release. There have been a lot of distribution specific issues with KDE 4.0 (especially with Kubuntu) - did you report your crashes to bugs.kde.org so that this can be verified and fixed by us developers? Thanks!"
    author: "Peter Penz"
  - subject: "Re: Don't get me wrong..."
    date: 2008-02-03
    body: "As I said, I meant no offence. I'm not here to complain and criticize you - please take it rather as a piece of advice or request from a regular user. I'm very happy that you are working hard and I promise I will try KDE 4.0.1. \n\nAnd yes, it's true - I do use Kubuntu. Maybe that's why there are so many issues concerning the new KDE. \n\nCheers!"
    author: "mkrs"
  - subject: "Re: Don't get me wrong..."
    date: 2008-02-02
    body: "Lots of people are fixing lots of bugs. Just because you may not see all of the bugfixes in the commit digest doesn't mean they are not there.\n\n"
    author: "SadEagle"
  - subject: "K3b plugins?"
    date: 2008-02-02
    body: "I'm a bit confused about the K3b plugins. Why does it use separate plugins and not Phonon for audio encoding and decoding?\n\n\nPS: Thanks Danny for the Didgest"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: K3b plugins?"
    date: 2008-02-02
    body: "Because Phonon is for playing, I don't think it can help with audio encoding and decoding... But I might be wrong, Phonon might do those things in the future."
    author: "jospoortvliet"
  - subject: "Re: K3b plugins?"
    date: 2008-02-02
    body: "I saw at one of presentation videos of phonon (by trolltech) that they intend to bring encoder/decoder tasks to phonon too."
    author: "SVG Crazy"
  - subject: "Re: K3b plugins?"
    date: 2008-02-02
    body: "What's the difference between decoding and playing??"
    author: "Santa Claus"
  - subject: "Re: K3b plugins?"
    date: 2008-02-04
    body: "decoding can support playing but not the other way around (and phonon only does playing atm)."
    author: "attendant"
  - subject: "K3b plugins?"
    date: 2008-02-02
    body: "I'm a bit confused about the K3b plugins. Why does it use separate plugins and not Phonon for audio encoding and decoding?\n\n\nPS: Thanks Danny for the Didgest"
    author: "yxxcvsdfbnfgnds"
  - subject: "Buzz"
    date: 2008-02-02
    body: "How is the Buzz of apps calculated? :)"
    author: "dh"
  - subject: "Re: Buzz"
    date: 2008-02-02
    body: "http://commit-digest.org/poppy/?view=buzz-explain\n\nBut I must find Tobias Hunger's secret ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Buzz"
    date: 2008-02-02
    body: "He has hired a team of BEOs (Buzz Engine Optimizers)"
    author: "Leo S"
  - subject: "Why KDE 4.0 have two \"Zion\" Color schemes ?"
    date: 2008-02-02
    body: "in KDE 4.0 systemsettings - > appearance -> Colors\n\nI see there are two color schemes with the name \"Zion\" - the other is reversed zion - so why this exact name is chosen ?\n\ndoes this reflect the KDE support for Israel ?\n\nif no , can we have a less provoking name please and keeping kde neutral ?\n\nthanks ."
    author: "KDE user since 2.1"
  - subject: "Re: Why KDE 4.0 have two \"Zion\" Color schemes ?"
    date: 2008-02-02
    body: "\"does this reflect the KDE support for Israel ?\"\n\nNo, it reflects the KDE support for lions and tigers:\n\nhttp://en.wikipedia.org/wiki/Zion_Wildlife_Gardens\n\nI swear, the Dot KDE peanut-gallery parade's nitpicking becomes more and more ludicrous as time goes by."
    author: "Anon"
  - subject: "Re: Why KDE 4.0 have two \"Zion\" Color schemes ?"
    date: 2008-02-03
    body: "I thought it was just named by a Matrix fan..."
    author: "odysseus"
  - subject: "konqueror restore from crashed session"
    date: 2008-02-02
    body: "I like the KDE4 Konqueror very much. The only thing that prevents me from daily use is that it doesn't restore the session after a crash like firefox.\nI don't mind if it crashes, no software is perfect, but I use the tab feature quite extensive and it is annoying if all the tabs are gone after a crash.\n\nAnyway, Konqueror is really great and I like the new restore closed tab feature very much."
    author: "hias"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-02
    body: "There is the crashes plugin, which remembers what tabs you had open after a crash, but indeed - it forgets any info you entered in webforms etc. I really hope these things will come in time, and I do remember vaguely some dev talking about supporting that stuff when doing session restore - maybe that same architecture can be used for crash prevention as well."
    author: "jospoortvliet"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-02
    body: "Nice :)\n\nDo I have to enable it somewhere? If not, it seems to be a kubuntu bug."
    author: "hias"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-02
    body: "Not sure about the kde4 packages, but for kde3 it's packaged as \"konq-plugins\" (and I don't see a konq-plugins-kde4, so you might have to dig or perhaps it's not packaged).  After installing it, select Settings->Configure Extensions->Tools and select Crashes Monitor."
    author: "mactalla"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-02
    body: "It's already available in KDE3? Wow, I didn't know that. That's another hidden KDE feature ;)\n\nIt seems konq-plugins-kde4 is not packaged for kubuntu. Sad, because the search bar belongs also to the plugins. I hope it get soon packaged."
    author: "hias"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-02
    body: "Stay tunned! I plan to add the restore session feature for KDE 4.1 =)\n\nBTW I'm glad you like the restore closed tab feature, I developed it!. I like open source, there's no way I could have done it for the official version of IE for example."
    author: "Eduardo Robles Elvira"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-02
    body: "Sounds great. Does that mean that it is on by default? I'm sure there are more user like me who don't know that KDE3 Konqueror can restore the tabs after a crash. It would be nice if there's a dialog which asks for restoring or a new session.\n\nRestore closed tabs and restore after crash were the only reasons for me to stick with firefox. It looks like it's time to switch ;)\n\n"
    author: "hias"
  - subject: "Re: konqueror restore from crashed session"
    date: 2008-02-03
    body: ">>Stay tunned! I plan to add the restore session feature for KDE 4.1 =)\n>>I'm glad you like the restore closed tab feature, I developed it!\n\nYou are a champion - how awesome that KDE has people like you! :)"
    author: "Parminder Ramesh"
  - subject: "K3b and solid"
    date: 2008-02-02
    body: "I know it is just the beginning and first of all I want to congratulate all KDE developers for the excellent work so far.\n\nReading the commit news about k3b a question came to mind:\n\nWouln't it be interesting to have the k3b device detection code in solid? (since it is optimized for optical devices and gathers much more information than Solid does). \n\nI think it would be great because other apps would have the ability to use it and  giving the device media detection tasks to solid would make easier to have k3b running on windows and macosx. \n\nSorry for my english, still learning."
    author: "SVG Crazy"
  - subject: "Re: K3b and solid"
    date: 2008-02-02
    body: "Your english is OK. And I think what you suggest is the plan already, just needs some time etc..."
    author: "jospoortvliet"
  - subject: "Re: K3b and solid"
    date: 2008-02-02
    body: "I would like to see some sort of burning pillar of kde. Something like solid oder phonon just for burning. Is this already easily possible with k3b?"
    author: "TeeZee"
  - subject: "Re: K3b and solid"
    date: 2008-02-02
    body: "what about a linux equivalent of EAC (exact audio copy).\n\nIt's a shame that I need to keep a windows install just for EAC.\n\n\n"
    author: "asdf"
  - subject: "Re: K3b and solid"
    date: 2008-02-03
    body: "um, cdparanoia works great."
    author: "Ian Monroe"
  - subject: "Re: K3b and solid"
    date: 2008-02-03
    body: "no comparison to EAC.\nand, no further development in recent years."
    author: "asdf"
  - subject: "Re: K3b and solid"
    date: 2008-02-02
    body: "Thats the Solid's job.Burning a disc is some kind of interacting with hardware.\nI hope to see K3B using Solid soon.this would be better for everyone."
    author: "Emil Sedgh"
  - subject: "use of solid"
    date: 2008-02-03
    body: "Solid's job is really to just make it easy for application developers to know what kind of hardware they have on the system. So the use in k3b would be to get a list of CD burners, know what kind of disc was inserted etc. The actually burning would be done the current set of cd burning apps, like what happens now."
    author: "Ian Monroe"
  - subject: "Dragon Player"
    date: 2008-02-02
    body: "I'd love to have a minimal playlist support (integred like the volume or video settings panels) in Dragon Player\nI think it's a basic feature in a video player... well, at least as basic as Audio CD support in a video player ;)\n\nI'd love to be able to control the volume with the mouse wheel over the volume button and to delete history too.\n"
    author: "DanaKil"
  - subject: "Re: Dragon Player"
    date: 2008-02-02
    body: "+1 for Playlist support.\n\n"
    author: "js"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "Playlist support is the opposite of minimalist IMO. That's what Kaffeine is for."
    author: "Ian Monroe"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "Using that rational, adjusting brightness/contrast(which Dragon does) is superfluous. Why is that included? Playlist support is a basic necessity of a video player, far more important than changing video display settings.\n\nWere not talking about a feature which only a minority of users need, playlist support is used by the vast, *VAST* majority of end users. It makes no sense that it isn't included. I know developers are trying to avoid a recurrence of KDE3, where useless features cluttered all the apps but this is insane.\n\nThere has to be a balance between Dragon(crippled, unusable for most users) and Kaffeine(so many features, it hurts my eyes looking at the UI). "
    author: "js"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "I have some videos where brightness/contrast controls makes the difference between the video being viewable or not. And they are pretty much always needed on a laptop outside if there's any hope to see it at all. \n\nBut really I've heard very little demand for playlists. Why is it so necessary? I certainly contest the idea that a vast majority of users expect it in their video player, but I wonder why you think this."
    author: "Ian Monroe"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: ">>I certainly contest the idea that a vast majority of users expect it in their video player\n\nI believe users expect a playlist because all popular GUI media players have it (WMP/QT/VLC/Totem/Xine-UI/I really can't name a single video player that doesn't)\n\n>>but I wonder why you think this.\n\nI think of a GUI video player as a convenient method of playing video as opposed to a tedious CLI. Take the following scenario where I, say watch sequential episodes of a TV program:\n\n-show finishes, credits roll\n-exit fullscreen mode\n-locate next video in Dolphin\n-double click to open\n-click fullscreen mode\n\nas opposed  to:\n\n-show finishes, credits roll\n-left arrow\n\nIf a GUI video player isn't convenient then whats the point of having one? \n\nWhy not try running a poll on your blog to see what the users think?"
    author: "js"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "Whoops, I meant \"right arrow\"."
    author: "js"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "+1, maybe it would be possible to recycle that cute little view bar from Gwenview (although obviously the \"5.0 seconds\" thing is not needed for a video collection)."
    author: "Parminder Ramesh"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "I don't really like the UI's of any of the video players you mention, I don't think this is a coincidence. Why would you locate the next video in dolphin as opposed to just using the open file dialog? In this case I suspect you haven't used Dragon Player...\n\nWe are planning on adding features to Dragon Player to make it easy to tell what videos you have already watched. But a playlist really is outside of the scope of Dragon Player."
    author: "Ian Monroe"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "I'm confused as to why File -> Open makes it any more convenient. The revised scenario would be:\n\n-show finishes, credits roll\n-exit fullscreen mode\n-File -> Open video\n-click fullscreen mode\n\nas opposed to:\n\n-show finishes, credits roll\n-right arrow\n\nIts still more work for a common task not to mention the lack of continuity.  \n\n>>In this case I suspect you haven't used Dragon Player...\n\nYou'd be right, I haven't had the opportunity yet. I'm sorry if my criticisms aren't accurate but from what you've said, they seem to be on target.\n\n>>We are planning on adding features to Dragon Player to make it easy to tell what videos you have already watched.\n\nAgain I'm confused, why would I want to know what I've already finished watching?\n\n>>But a playlist really is outside of the scope of Dragon Player.\n\nI can't really argue there,it is your project."
    author: "js"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "I'm not really a big fan of any playlist players either.  If I'm watching episodes of a Tv show I don't really care about having to click on the next one every hour or half hour.  For the once in a blue moon that I really need a playlist I can fire up VLC.  For the other 99% of the time I'd rather have a very lightweight player with a minimal interface."
    author: "Leo S"
  - subject: "Completely disagree"
    date: 2008-02-03
    body: "For me, a minimal player has to allow me to view a single video quickly with the best possible viewing experience. So being able to change birghtness/contrast is definitely part of it.\nHaving playlist involves organizing what you want to see, and this is IMO definitely out of the scope of a simple video player. It's more for something like an Amarok-for-videos, which I also believe Kaffeine could become but has not really reached this point yet.\nActually, I know more people who organize their movies on their hard disk in directories and watch them by double-clicking on them than I know people using playlists for their videos. And Dragon player (as was Codeine), loading quickly and providing all needed settings to view a single video, is just doing this job perfectly.\nNow, I agree that an app like Amarok for videos could be great also. But that's another issue."
    author: "Richard Van Den Boom"
  - subject: "Re: Completely disagree"
    date: 2008-02-04
    body: "+1 for \"an app like Amarok for videos\"."
    author: "yman"
  - subject: "Re: Completely disagree"
    date: 2008-02-05
    body: "++! from me also!"
    author: "Kerr Avon"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "Exactly, please keep Dragon Player simple and fit for it's important use case. The use case of simply playing one media file. As Ian say, if you want playlist, use a player thats optimized for that kind of use case. \n\nAnd playlist are not necessary as simple as it sound, take for instance the 3 major audio players for KDE3. Juk, Noatun and Amarok they all have different kinds of playlist. Which is the best and the one to use."
    author: "Morty"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "Thanks for taking a strong stance. I'd hate Dragon to turn into the shuttle cockpit that is Kaffeine."
    author: "logixoul"
  - subject: "Re: Dragon Player"
    date: 2008-02-03
    body: "well, when I spoke about a playlist, it was nothing fancy like in Amarok... just a list view in a sidebar with a \"+\" and \"-\" button (no tabs like in kaffeine !) which I can show or hide easily.\n\nWhen I drag-n-drop or select several files, they enqueue in this list and I can go to the next video with a \"next\" button. All of this can be invisible to the user if he don't want it (no button on the toolbar by default...)\n\nBut like someone said, it's your project and if you think you're right, no problem, really :) It's just that, as an official KDE application, Dragon Player will be exposed to many people so be sure to do the good choices."
    author: "DanaKil"
  - subject: "Amarok 2.0?"
    date: 2008-02-03
    body: "Hi,\n\nI haven't seen any news on Amarok 2.0 for KDE. Could someone please update me on the progress of that?\n"
    author: "Max"
  - subject: "Re: Amarok 2.0?"
    date: 2008-02-03
    body: "http://amarok.kde.org/blog/"
    author: "Hans"
  - subject: "Great job guys"
    date: 2008-02-03
    body: "Great job guys!!\n\nDon't let the nitpicking discourage you. See it as a way of people showing interest and exitement about the project.\n\nKeep up the awesome work!!!\n\n"
    author: "Max"
  - subject: "K3B and QProcess"
    date: 2008-02-03
    body: "> since K3b directly links the stdout and stdin file descriptors of\n> processes such as mkisofs and cdrecord to gain maximum performance\n> when piping data during on-the-fly burning.\nDoesn't http://doc.trolltech.com/4.3/qprocess.html#setStandardOutputProcess handle what is needed to directly link those mkisofs and cdrecord descriptors? \n\n> since there is also no way to use QProcess synchronously in a\n> multi-threaded application\nWell, that's the way Qt works - asynchronously by design. This might look quite annoying at the beginning, but it really pays off.\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: K3B and QProcess"
    date: 2008-02-03
    body: "btw... what about libburnia? Does it work in \"real world\" situations? Would it be an option to get rid of these QProcess calls and program output parsing?\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Who many media players?"
    date: 2008-02-03
    body: "Hello,\nhow many media players do we have in KDE 4 now? From KDE 3 I remember kaboodle and noatun. Have they been removed or do we have 3 players (kaboodla, noatun, dragon player) now? I think this is two too many. (I'm talking about the apps included in kde multimedia etc.)"
    author: "FL"
  - subject: "Re: Who many media players?"
    date: 2008-02-03
    body: "Kaboodle and Dragon Player are different kind of players with a different use case than Noatun, Juk, Amorak, Kaffeine etc. Since they are optimized for a different, but very common use case they should definitely be included too.\n\nAs I under stand it, Kaboodle is already removed. But Dragon Player should handle audio files nicely handling Kaboodles tasks too."
    author: "Morty"
  - subject: "Re: Who many media players?"
    date: 2008-02-03
    body: "The only apps in kdemultimedia are JuK, kscd, kmix and now Dragon Player as well."
    author: "Ian Monroe"
  - subject: "KDE-PIM"
    date: 2008-02-04
    body: "Amazing work Till! In shadow of KDE 4 eye candy there are great improvements to Kontact usability and feature wise.\n\nThanks to whole KDE-PIM team."
    author: "m."
  - subject: "APIs"
    date: 2008-02-04
    body: "I wonder if there are any plans for APIs with pluggable backends for the following things:\n\n1. extracting text from images (text recognition?)\n2. voice recognition.\n3. automatic translation of text from one language to another.\n4. screen reading.\n\nuse cases:\nautomatically generate subtitles for movies.\nautomatically generate transcripts of interviews.\n\ncompile multiple narrative nicely into a single sheet of paper for history class.\ncreate E-Books out of the hard copies you bought.\ntranslate documents you receive into a language you can actually read.\nbrows ALL the web in your language, regardless of what language it was written in (including images!)."
    author: "yman"
  - subject: "Re: APIs"
    date: 2008-02-05
    body: "I don't know that KDE should have a library to extract text from images.  I'm assuming you're referring to something like OCR, which should be more of a generic library that all desktops should be able to access.\n\nSonnet was working on language recognition as well as translation, and spelling.  Too bad the original developer disappeared.  I really hope it gets some love.  It is so practical in how it could improve a great number of applications.  I'd love to see it become a pillar of KDE 4 as much as Solid."
    author: "T. J. Brumfield"
  - subject: "Re: APIs"
    date: 2008-02-05
    body: "but I'd like people (maybe myself one day?) to be able to write applications for KDE that work on any platform, with any backend. what if I don't want OCR on my system and instead use something else? I wouldn't want to maintain different versions for each possible backend.\n\nI really like the idea of Sonnet, and consider it as something that should be a pillar of KDE4. I'd also like KDE to switch as much as possible to pluggable backends, including spellcheckers. that means that I think Sonnet should be a backend for a language library. that way, if something better comes along, I could switch to it without any code modifications to the app that uses it. it would also give Sonnet time to be developed while using something else as a placeholder."
    author: "yman"
---
In <a href="http://commit-digest.org/issues/2008-01-27/">this week's KDE Commit-Digest</a>: Heavy refactoring and work on merging translation branches in Lokalize (which is renamed from "Kaider", and moved from playground to kdesdk). Work on a question editor in <a href="http://edu.kde.org/keduca/">KEduca</a>. Work on real-time cloud imagery in <a href="http://edu.kde.org/marble/">Marble</a>. An initial implementation of a new undo stack in <a href="http://edu.kde.org/kwordquiz/">KWordQuiz</a>. The start of a <a href="http://edu.kde.org/kalgebra/">KAlgebra</a>, Rot13, KWorldClock, and Pastebin <a href="http://plasma.kde.org/">Plasma</a> applet, with the inclusion of more functionality from KDE 3.5 (such as the multi-row taskbar panel) in Plasma. Progress in scripting support and functionality in Plasma. The "Now Playing" data engine and applet, and the fuzzy-clock Plasma applet move into kdereview. Viewports support declared "complete" on the KDE desktop. "FlipSwitch" window-switching effect in KWin. The start of a KIO slave for handling arbitrary <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> resources. Draft implementation of a KABC resource based on <a href="http://pim.kde.org/akonadi/">Akonadi</a>. Wholesale merges from the enterprise branch of <a href="http://pim.kde.org/">KDE-PIM</a> back into the main KDE branch. Move to complete support for the MPRIS media player interaction standard, and support for Video CD's and Audio CD's in <a href="http://dragonplayer.org/">Dragon Player</a>. Dragon Player moves from kdereview into kdemultimedia for KDE 4.1. Last.fm streaming radio now works in <a href="http://amarok.kde.org/">Amarok</a> 2. Work on gradient editing in <a href="http://koffice.kde.org/karbon/">Karbon</a>. The <a href="http://www.kde.org/apps/kooka/">Kooka</a> scanning application finds a new maintainer, with various initial improvements. KSystemLog moves from playground into kdereview. Krone, a simple expense manager for KDE 4, is added to KDE SVN. <a href="http://commit-digest.org/issues/2008-01-27/">Read the rest of the Digest here</a>.

<!--break-->
