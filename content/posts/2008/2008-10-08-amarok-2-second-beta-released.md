---
title: "Amarok 2 Second Beta Released"
date:    2008-10-08
authors:
  - "mkretschmann"
slug:    amarok-2-second-beta-released
comments:
  - subject: "Where is Amarok2 heading?"
    date: 2008-10-07
    body: "Is there any paper that describes what Amarok2 should look like once its ready? I still don't understand the User Interface of the new release."
    author: "Andr\u00e9"
  - subject: "Re: Where is Amarok2 heading?"
    date: 2008-10-07
    body: "What part do you not understand? We'll be happy to explain."
    author: "Mark Kretschmann"
  - subject: "Re: Where is Amarok2 heading?"
    date: 2008-10-08
    body: "I for one can't figure how to do edit the IDv3 tags of multiple files. (Album and Artits tags). Nor how to go to the next song in the play list when editing only one file.\n\nApparently there are missing features.\n\n(Don't mean to sound like a whiner! I love Amarok and like the Fuzzy Playlists idea) Keep on amaRoking!"
    author: "PuercoPop"
  - subject: "Re: Where is Amarok2 heading?"
    date: 2008-10-09
    body: "\nI am trying to find out \"Stop playing after this track\" feature. What is very nice when you are leaving somewhere or going to sleep and you can just mark wanted song to be last in playlist, without dragging it as last one or anyother way reorganising or tweaking playlists."
    author: "Fri13"
  - subject: "Re: Where is Amarok2 heading?"
    date: 2008-10-10
    body: "This feature has not been forgotten. Please see this post for more information regarding missing features:\nhttp://amarok.kde.org/blog/archives/809-Missing-features-in-Amarok-2.html"
    author: "Casey Link"
  - subject: "Score in OpenSuse"
    date: 2008-10-08
    body: "I have been following Amarok 2 using OpenSuse builds, and have not been able to work out how to get songs to be automatically scored.  They all just sit a zero no matter how many times I play them.  The play-count works, as does the rating (if it rate the files), but the score does not change, unless I change it.\n\nIs this something I am missing, or something not yet done?\n\nAmarok 2 is looking great - I am looking forward to the dynamic playlist being usable, as I think it looks better structured then the old one, but without scores, it is less useful.\n\nThanks...\nAndrew"
    author: "Andrew Lowe"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-08
    body: "Would you be so kind and tell me where to find suse builds of amarok2."
    author: "toto"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-08
    body: "This page lists the KDE repositories for Open Suse (straight to factory - 4.1):\nhttp://en.opensuse.org/KDE/Repositories#KDE_4.1_Factory_Development\n\nBut the one you want is probably this:\n\nhttp://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Extra-Apps/openSUSE_11.0/\n\nIt actually has amarok builds, pretty much daily.  I am pretty sure you need to install the factory (4.1) builds of KDE for core, qt and perhaps community?\n\nHope this helps... "
    author: "Andrew Lowe"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-08
    body: "Thanks :)\n"
    author: "toto"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-08
    body: "There's a config option somewhere to use ratings, scores, both or neither if I remember correctly, maybe yours is off?"
    author: "NabLa"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-10
    body: "I cannot find it in the gui... and had a look at the config files, but nothing giving any hints?\n\nAnyone know the option or where I can find the options?\n\nThanks"
    author: "Andrew Lowe"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-10
    body: "I'm pretty sure scoring has not been re-re-re-implemented after the script stuff changed.  I have a feeling it slipped through the cracks... File a bug to remind us :)"
    author: "Dan"
  - subject: "Re: Score in OpenSuse"
    date: 2008-10-11
    body: "I've just implemented it in my local Git repo."
    author: "Mark Kretschmann"
  - subject: "Postgres"
    date: 2008-10-08
    body: "Please add support to postgres :D"
    author: "Postgres"
  - subject: "Re: Postgres"
    date: 2008-10-08
    body: "why?"
    author: "sfdg"
  - subject: "Re: Postgres"
    date: 2008-10-08
    body: "Why not? Why should anyone install MySQL if he already have PostgreSQL installed and running? Just playing music is not an argument for running another one resources-consuming app. I'll better use mpd or whatever."
    author: "GArik"
  - subject: "Re: Postgres"
    date: 2008-10-08
    body: "Yes, whatever is the right tool for the tool."
    author: "Mark Kretschmann"
  - subject: "Re: Postgres"
    date: 2008-10-09
    body: "lmao"
    author: "illogic-al"
  - subject: "Re: Postgres"
    date: 2008-10-08
    body: "\"Why should anyone install MySQL if he already have PostgreSQL installed and running? \"\nbecause even if someone has postgreSQL already installed AmarokTeam has to provide sane default configuration which just works. That is why providing embeded mysql is good idea, it will work out of box and still later you may switch to postgreSQL if you really want. \nWhen we finally end this I'm-pro-user-I-want-insane-deault-configuration nightmare?  Lets end it and start I'm-pro-user-,sane-default-configuration-is-v.good-and-I'm-happy-that-I-still-can-configure/tweak-it-the-way-I-want"
    author: "Koko"
  - subject: "Re: Postgres"
    date: 2008-10-10
    body: "For EXACTLY the same reason you guys could continue using Sqlite. The claim that the performance difference is too big is a joke.\n\nPersonally I dont mind because I agree that one should use the right tool for a given job and amarok developers decide what this is best for, but I dislike the argument that MySQL _needed_ to be chosen, as if it were significantly better than any other database.\n\nAnd frankly I want flat files instead of databases ... ;-)"
    author: "markus"
  - subject: "Re: Postgres"
    date: 2008-10-15
    body: "I want flat files too!"
    author: "gentoo"
  - subject: "migrating from a1 > a2"
    date: 2008-10-08
    body: "Hello Rockers,\n\ntoday a very vital question came to my mind. How can I migrate from Amarok 1 to Amarok 2 once it's been released (a question that's actually a problem with all kde applications from qt 3 to 4). My problem is not so much the playlists and the library that can be done quite fast by hand my problem are my statistics which can't be moved so easy (afaik). Lately I was also asked if there was a plan/possibility to move ones iTunes statistics to Amarok 2 (once it's been released on win and mac too).\n\nSonnenschein"
    author: "Sonnenschein"
  - subject: "Re: migrating from a1 > a2"
    date: 2008-10-08
    body: "I believe someone is working on a script to import statistics from the 1 series to the 2 series. A quick search has turned up this gem.... so it looks like the devs think it is important\nhttp://amarok.kde.org/blog/archives/708-Amarok-2-Artwork-is-Back.html\n\nI also found this topic in regards to importing iTunes stats - but it seems to be for 1.4 series, but you could make it work...\nhttp://amarok.kde.org/forum/index.php/topic,11902.0.html\n\nHope this helps..."
    author: "Andrew Lowe"
  - subject: "Re: migrating from a1 > a2"
    date: 2008-10-08
    body: "Correct, I have been working on a migration tool to carry over statistics from Amarok 1.4 to Amarok 2. I hope to include it in the next beta.\n\nan iTunes library also shouldn't be difficult to import."
    author: "Seb Ruiz"
  - subject: "How to migrate your statistics"
    date: 2008-10-10
    body: "I came up with the following to migrate the statistics from the old SQLite collection.db to the new mysqle database.\n\nFirst: You have to start with a clean collection in Amarok 2.  No statistics can have been recorded.\n1. rm -r ~/.kde4/share/apps/amarok/mysqle\n2. Start Amarok 2 and let it build the collection again, but do NOT play anything!!\n3. Once the collection is fully built, quit Amarok completely.\n\nYou will need the MySQL server installed and configured.  You probably already have it installed since Amarok 2 depends on it, so go ahead and give it an 'emerge --config' if you haven't already set it up.  Remember the root password that you choose.\n\n4. Now start the MySQL daemon if it's not already running:\n# eselect rc start mysql\n\n5. Move the Amarok database to the MySQL server's data dir:\n# mv ~youruser/.kde4/share/apps/amarok/mysqle/amarok /var/lib/mysql/\n\n6. Change ownership so MySQL can access it:\n# chown -R mysql:mysql /var/lib/mysql/amarok\n\n7. Now the magic foo:\n# ( echo \"SET sql_mode='ANSI_QUOTES';\"\n  sqlite3 ~youruser/.kde3.5/share/apps/amarok/collection.db .dump |\n  grep '\\(CREATE TABLE \\|INSERT INTO \"\\)statistics' |\n  sed -e 's/\\(CREATE TABLE \\|INSERT INTO \"\\)statistics/\\1old_stats/;s/,PRIMARY KEY([^)]*)//'\n  echo 'INSERT INTO statistics (url, createdate, accessdate, score, rating, playcount, deleted) SELECT id, createdate, accessdate, percentage, rating, playcounter, deleted FROM old_stats INNER JOIN urls ON (urls.rpath = old_stats.url);'\n  echo 'DROP TABLE old_stats;'\n  ) |\n  mysql -u root -p amarok\n\nIt could take a minute for all the statistics to be migrated.  Don't panic if it doesn't complete immediately.\n\n8. Stop the MySQL server to be sure it has flushed everything to disk:\n# eselect rc stop mysql\n\n9. Change ownership back to your user and group:\n# chown -R youruser:yourgroup /var/lib/mysql/amarok\n\n10. Move the database back to Amarok's dir:\n# mv /var/lib/mysql/amarok ~youruser/.kde4/share/apps/amarok/mysqle/\n\n11. Start Amarok and appreciate that all your statistics are saved!"
    author: "Matt Whitlock"
  - subject: "Re: How to migrate your statistics"
    date: 2008-10-10
    body: "I should have mentioned that these instructions are for Gentoo.  Not much should change for other distros -- just how to install, configure, and start/stop MySQL, and possibly the location of the MySQL data dir."
    author: "Matt Whitlock"
  - subject: "contextview"
    date: 2008-10-08
    body: "Amarok2 is really doing awesome, and i enjoy the new placement of the various parts!\n\nthe central pane contextview looks really promising, but is still a little clunky to use in comparison to the wikipedia/lyrics tabs of a1.  But, nonetheless, I'm really looking forward to seeing where it will go :) like kde4 has done successfully, I'm sure amarok2 will successfully polish and complete it's new concepts post 2.0 release as well :)"
    author: "Vladislav"
  - subject: "Re: contextview"
    date: 2008-10-10
    body: "Thank you :)\n\nContinuing to improve Amarok 2 after the 2.0 release is definitely part of the plan. "
    author: "Casey Link"
  - subject: "Congratulations for switching to commcercial MySQL"
    date: 2008-10-08
    body: "Hurray...excellent... Congratulations for switching from Public Domain SQlite to commercial MySQL."
    author: "Anon"
  - subject: "Re: Congratulations for switching to commcercial MySQL"
    date: 2008-10-08
    body: "You do realize that MySQL is GPL? (Which is imho better than public domain because it enforces sharing of changes!) "
    author: "tkjacobsen"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-08
    body: "So is the SQlite also supported in addition to MySQL if I want link against instead of commercial MySQL? "
    author: "Anon"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-08
    body: "No, just like GTK+ is not suported if you want to link against it instead of commercial Qt..."
    author: "Jonno"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-09
    body: "hahah. nice."
    author: "illogic-al"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-10
    body: "This is not correct. You can continue to keep it closed source IF you do not release it to the (outside) public. And as far as I know MySQL is not \"GPL\", MySQL is dual-licensed. And whenever something is dual-licensed it is incorrect to attribute it solely to GPL, because one would think it _IS_ only GPL licensed when it fact it has TWO licenses (like Qt has).\n"
    author: "markus"
  - subject: "Re: Congratulations for switching to commcercial MySQL"
    date: 2008-10-08
    body: "Congratulations for the FUD"
    author: "Vide"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-11
    body: "In other news, KDE decided to drop Qt because developers finally found out that it is also offered under a commercial license..."
    author: "gousiosg"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-12
    body: "\"I mean do we even know how many music files it takes for the overhead of MySQL (in terms of memory) to be worthwhile? \" - Collection by yeah\n\nFinally, nobody answered the above question. So, how many files does it take MySQL to show a difference over public domain SQLite? Ha ha ha ...take the challenge and show the FACTS.\n\nHow many files does it take to **ditch** a public effort to make an embedded database (SQLite), which strive to second to none, and embrace the commercial MySQL-embedded which Sun Microsystems anytime can close the source? \n\nShow us why do you want to impose **ONLY** MySQL on us? I feel rather **suspicious** over this move.\n\nYou want to collect **public** donations to impose commercial products on us?\n\nWhy don't you ask a big donation from Sun Microsystems, the present owner of MySQL? For Sun to standardise MySQL-embedded, for people to not try the public domain SQLite, its a peanut marketing expense.\n\nWe are trying to escape from using commercial products specially as the parts of the structure or foundation of public open source products. If amarok is developed by a company for profit motive, then I don't have any argument at all. \n\nPeople questioned the Qt is also a commercial product, released under GPL too, so what's the big deal? the big deal is the Qt **CANNOT** be taken away from us. Read this if you have no idea of what I'm talking about: http://www.kde.org/whatiskde/kdefreeqtfoundation.php\n\nDoes the dual-licensed MySQL-embedded also protected like that? If no, do you think Sun Microsystems will ever agree for a such a legal agreement?\n\nIf there is no hidden agenda behind this move, ie, switching from public domain SQLite to commercial MySQL-embedded, then what do we call this? This is utter stupidity. This is called foolishness. That is, no F idea what you are doing. No F idea **why** you are developing an open source product in first place. Wake up guys. \n\n"
    author: "Anon"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-12
    body: "<<Finally, nobody answered the above question. So, how many files does it take MySQL to show a difference over public domain SQLite? Ha ha ha ...take the challenge and show the FACTS.>>\n\nWhy dont'you take the challenge yourself? Do you really think you are entitled to ask for numbers? If you want numbers to prove something you really need to stop asking everybody for generating numbers for you. You wouldn't trust them anyway, would you?\n\n<<You want to collect **public** donations to impose commercial products on us?>>\nUhoh, are you living under a rock? I guess your PC is one of those hated commercial products too, an the table and the chair you are sitting at and the floor etc.. I could go on for hours... And by the way: Your are not <<the public>> so stop acting like it.\n\n<<People questioned the Qt is also a commercial product, released under GPL too, so what's the big deal? the big deal is the Qt **CANNOT** be taken away from us. Read this if you have no idea of what I'm talking about: http://www.kde.org/whatiskde/kdefreeqtfoundation.php>>\n\nNo, really, Sherlock? \n \n"
    author: "Grow up!"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-14
    body: ">>Finally, nobody answered the above question. So, how many files does it \n>>take MySQL to show a difference over public domain SQLite? Ha ha ha ...take \n>>the challenge and show the FACTS.\n\n>Why dont'you take the challenge yourself? Do you really think you are entitled \n>to ask for numbers? If you want numbers to prove something you really need to\n>stop asking everybody for generating numbers for you. You wouldn't trust them \n>anyway, would you?\n\nPlease understand, its not me who ditched the public domain SQLite and switched to commercial MySQL-embedded. The amarok developers should justify their switch. They should have numbers, I'm saying again, if there is no under the table deal with Sun Microsystems, they have no reason not to publish the numbers.\n\nIn fact, this is not a question of numbers or performance, this is a question of principals. We are discussing this on KDE website, not on Microsoft or Apple.\n\nIf performance is the issue, you are at the wrong place. You are on the wrong OS. You should use most probably Apple's Mac OSX and forget about open source, embrace with your whole heart the closed source.\n\nYou are on Linux, FreeBSD, etc. etc. because you have principals. You are not a fool or you are not a blind. You don't like the traps set by billion-dollar companies. You don't like software patents, you prefer to made advances already made by your peers, You are not a puppet billion-dollar companies can manipulate, you are a soldier with a cause.\n\nThe question here is, those who portray as the soldiers of freedom denied your freedom to use a public software product if you really want to use, which can never be taken away from you, (SQLite in this case) and imposed on your head a commercial product (MySQL-embedded in this case). You were not given a choice which easily could have given, did not even ask from you for the change, it seems do not even want to justify for the switch.\n\nWhat all these explain:\n1. People who develop amarok have no principals OR\n\n2. There is a under the table deal with Sun OR\n\n3. amarok is becoming a commercial product like their parent MySQL sold public sweat for more than a billion dollars.\n\nWhat's the public can do:\n1. Write to amarok developers and show your displeasure and ask them to support SQLite also.\n\n2. Develop a patch to strip down MySQL-embedded code and use SQLite only or both. (This sure works)\n\n3. Ask the amarok developers to accept the patch (if you support both MySQL-embedded and SQLite) and ask them to show they no hidden agenda.\n\nWake up guys, good luck.\n\n\n\n\n"
    author: "Anon"
  - subject: "Re: Congratulations for switching to commcercial M"
    date: 2008-10-14
    body: "Sorry, guys, there is a spelling mistake.\n\nAbove \"principals\" should be corrected as \"principles\""
    author: "Anon"
  - subject: "easter egg"
    date: 2008-10-08
    body: "Is the picture a hint that the new release contains an easter-egg?"
    author: "maninalift"
  - subject: "Re: easter egg"
    date: 2008-10-08
    body: "Maybe ;)\n\nThe easter egg is well hidden this time. Good luck!\n"
    author: "Mark Kretschmann"
  - subject: "Is the equalizer coming back?"
    date: 2008-10-08
    body: "Amarok 1.4 had a rather nice equalizer. Is it going to appear in Amarok 2 as well?\n\nApart from missing that, I like what I see. :)"
    author: "Godji"
  - subject: "Re: Is the equalizer coming back?"
    date: 2008-10-08
    body: "It will probably come back at a later point. Currently it's not supported by Phonon."
    author: "Mark Kretschmann"
  - subject: "Collection"
    date: 2008-10-08
    body: "Is it possible to turn off MySQL when you don't have a large collection and would rather avoid the bloat? I mean do we even know how many music files it takes for the overhead of MySQL (in terms of memory) to be worthwhile?"
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-08
    body: "No, it's not possible. Amarok is very much centered around the database; it's the heart of the program.\n\nIf you want a simple player without database I would rather suggest to use something like XMMS or its many clones.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Collection"
    date: 2008-10-08
    body: "Or perhaps even JuK, as it should already be default in your KDE install. Or at least easily avalibel in your package manager."
    author: "Morty"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "Yeah I guess I'll use JuK then.\nPity the devs weren't able to come up with a design that scales down though, that's not gonna help KDE's reputation of inefficiency"
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "Amarok != KDE"
    author: "Anon"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "Okay but you didn't answer. How many music files does it take before using a full relational engine makes a significant difference?\n\nI mean it's not I don't want to trust you, but I find it hard to because say Dolphin has no problem sorting views with tens of thousands of files very quickly, lots more than how many musics people have. So perhaps if you published stats which compares MySQL to a simple but well designed algorithm for 100, 1000, 5000 and 10000 music files it would be easier to trust you. Because otherwise it's hard to believe you I'm afraid, with my apologies."
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "I suspect there is quite a difference between showing a bunch of files & parsing all meta data while making it searchable. You wouldn't want to wait 10 sec for a search in 1000 mp3's to finish, right?"
    author: "jospoortvliet"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "Yah but with a relational engine you would still have to parse all the metadata before inserting it right?\n\nBesides, no, I don't want to wait 10 seconds and I never said I would but you keep ignoring the question I'm trying to ask, which is how many files it takes for MySQL to make a difference as opposed with a good algorithm.\n\nI mean MySQL doesn't do magic, it just implements good searching and retrieval algorithm in a fast yet generic way, and I am confused why we can't implement good searching and retrieval algorithms in the much simpler case where we don't have to have the genericity of an RDBMS. Plus it'd be even faster because of no SQL overhead.\n\nSorry if I'm being bothersome with this, as a whole I think KDE is great but this issue in particular smells a lot like people trying to rationalize bad design after the fact and that's be a pity for such a project as KDE. I mean back in the day it was GNOME that made the poor choices (such as CORBA for interprocess communication) rather than implement the correct solution themselves (DCOP, in the case of this comparison), right?"
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "So if MySQL implements good searching and retrieval algorithms in a fest way, and provides them as a library to embed in your application, why not use it, but implement the same thing again? A lot of work went into mysql to make it fast and powerful, plus you don't need to maintain it. "
    author: "Andre"
  - subject: "Re: Collection"
    date: 2008-10-11
    body: "Oh because MySQL implements a lot of additional stuff such as SQL parser and network layer and the whole database engine independence system and query planner (and besides the database engine independence is the reason why the MySQL query planner is not as good as other databases because it has to remain generic across them all, and that's one more reason why too much genericity leads to more overhead). Kinda like using CORBA for communication between local processes indeed, it works and that's not the problem, the problem is the overhead. You are giving me the not reinventing the wheel argument which does make sense, except when you're trying to fit truck wheels on your sports car because that's all you have at hand."
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-11
    body: "They are using MySQL embedded, which removes much of the multi-user/network stuff (which is why it's called embedded).\n\nThis kind of metadata management is the RIGHT application for SQL. Manually recoding every possible kind of query that Amarok needs, when MySQL already does this, would be utterly silly. Especially since it _does_ have things like the query planner built in, so that it can take into account dynamic things like indexes, table sizes, join path optimization etc, which _could_ be reimplemented manually but with little noticeable performance benefit, and huge opportunity cost (I'd rather have the Amarok developers spend their limited time on Amarok than duplicating RDBMS features - the MySQL devs do that full time already). Not to mention that db versioning between releases of Amarok would be much easier with an SQL backend than some ad-hoc db implementation.\n\nThe query planner overhead you are talking about is irrelevant btw - for simple queries (one or two tables) it will be negligible, and if it is a complex query, it will often do a better job than someone who isn't a DBA coding it by hand."
    author: "taj"
  - subject: "Re: Collection"
    date: 2008-10-13
    body: "Hi taj\n\nThank you for actually addressing my questions it is good of you to do so.\n\nI can see how MySQL embedded would be way better than the server version, yes, so that's less of a concern, thank you for this detail.\n\nAbout the query planner and complex queries though, this is a music player, what kind of complex queries do you think would be running? It may be there is something I'm missing there mind you so this is a honest question.\n\nThanks."
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "Why is it that non-programmers are determined to argue about internal design against the actual developers of the system?"
    author: "Leo S"
  - subject: "Re: Collection"
    date: 2008-10-11
    body: "By now if you haven't realized that I am indeed a programmer I don't know what I can say that will convince you!"
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-11
    body: "I am pleased that you said \"why we can't implement...\" because I am interested in reading about your proposed alternative DB design for Amarok. Amarok requires dozens of different storage, searching, sorting and filtering queries across many different data types, so I'd be interested in seeing how you solve this problem while still allowing plugins and future versions of Amarok to perform additional queries without causing re-programming of the db algorithms, and taking into account data size and distribution like the MySQL query engine does.\n\nAnd I am genuinely interested btw, because this is something that has been kicking around in my head since JuK implemented its own custom database for all of this. I am (passively) for more SQL use within KDE, so a good analysis of this case would be very useful fodder for debate.\n\nSome possible avenues for exploration comparing JuK's db to Amarok's - is there a large delta in performance across all query types? Particular query types? At what point is this affected by database size? How about ad-hoc queries - are both able to handle future query requirements? How about versioning and updates? On-disk size? Reliability and fault-tolerance (ACID)?"
    author: "taj"
  - subject: "Re: Collection"
    date: 2008-10-13
    body: "Hi again taj,\n\nThank you for bringing up actual implementation requirements and not just generalities such as speed, because it helps make the issue clearer.\n\nThe way I was seeing the problem domain it was essentially about sorting and filtering a medium-size set of data because I don't think many users have more than say a few thousands songs at most? Is this correct? I'm going to work based on that from here down so please keep this in mind, thanks.\n\nBecause if so a simple tabular binary representation of data would work fine, that's actually how RDBMS engines work under the hood. By this I mean binary files with fixed-sized records (so you can scan sequentially with a fixed increment which is very fast).\n\nActually a RDBMS adds a lot of layers around such as indexes, which are depending on the implementation b-trees or hashes pointing to offsets in the tabular data, and of course JOINs and the query planner for non trivial queries, and in some cases collation (well for MySQL anyway because the support for collations in Postgres is kinda weak).\n\nWhat I was thinking is that in a number of cases the query planner ends up deciding to not use indexes at all and scans the tabular data instead, it depends on how expensive index lookup is as opposed to scanning sequentially (which itself depends on the cost of loading a page of tabular data). I refer you to the Postgres documentation for EXPLAIN ANALYZE which does a good job of speaking about this.\n\nSo in the case of a list of song files (if there are at most a few thousands entries for instance) it's really not obvious that a good query planner would decide to use the index, or that the index if used would make a huge difference in speed. Plus in the case of text search (by fragments of title for instance) there will be no index used of course.\n\nSo a simple tabular representation might work very well for this problem domain.\n\nThere is the problem of collations (which we will probably want to use for searching and sorting, for instance G\u00f6del should appear alongside Goedel), which I think is what the QString::localeAwareCompare method exists to solve, although there might be too much overhead for this. In such a case I've at times used an algorithm where I store a normalized version of strings (meaning all lowercase, spaces simplified and accents removed) to search and order based on that, which works well but has the drawback to double the storage space required for a given string.\n\nThen there's JOIN and I didn't think there would be a need for those in a playlist because the data model of artist / album / song can be designed as not following the usually correct normal form (because a song encompasses both album and artists, i.e. there are never more albums than songs for instance), but of course I may be wrong because I don't know what kind of queries the playlist would need to do anyway. Besides there is the issue of artists collaborating on an album or on a song which may require the use of some normalization at least if that's an issue for you.\n\nOf course JOINs can be implemented all the same if needed, it is logically speaking only a matter of scans across different tables using the JOIN ON clause as the search key. This is actually where a query planner makes a difference by ordering the JOINs if there are several of them, you want to start scanning the smallest table first but that particular optimization is not hard to implement especially as in our case we'll nearly always have more songs than artists and albums and more albums than songs so we can work based on that assumption.\n\nThere is also the issue of data and index buffers caching, which for small datasets (and a few thousands entries is still a small dataset I think) can probably be handled better by the OS-level cache while a RDBMS is generally designed to work best when it has the whole machine to itself and so manages the buffers itself for more efficiency, although of course that may not be true of MySQL embedded.\n\nSo for the problem domain as described thus far, a tabular data implementation with fixed-size records would probably work very well but, but but but of course I'm working based my assumptions of how a playlist works and that may not fit at all the data model Amarok really uses, in particular it seems you mean that plugins should be able to create data columns, right?\n\nFor instance it would be harder to implement flexible data model description, so columns could be added and removed and changed dynamically, and if that's a very very important part of the way you see playlists then of course a simple tabular model would no longer fit the problem domain as such, we should at least consider an array of such tabular models and that's already more complicated.\n\nSo it would help if you could quote typical complex queries such as you would like to use, and I do mean typical and not too far fetched because the \"what if we ever need to do...\" kind of scenarios is recipe for overengineering I think, because typical use cases is what you need first and foremost to design such a system, otherwise it's all assumptions and assumptions are the mother of all fuckups. (Which is a way to say that the whole above text might be full of fuckups because I still don't know exactly what you truly need to do with your playlist data.)\n\nWell! I'm sorry this reply was so long but I didn't have time to make it shorter. :)\n\nThank you for reading taj. I hope at least it's clear I'm really trying not to talk out of my ass and to give this thorough thought."
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-20
    body: "Hi taj\n\nYou didn't respond so I assume you aren't interested anymore which I can understand because the amarok developers said they'd still use MySQL as a datadump for various things even with other collection engines, so that makes the whole idea of a better collection engine to avoid the overhead of MySQL kinda moot.\n\nIt's a pity because I did some preliminary testing and a very very simple implementation of mmap'ed tabular records shows excellent performance I mean we're talking almost one million records per second for a case insensitive search (think like 'ILIKE') and that's without any indexing whatsoever of course."
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-20
    body: "Sorry this is wrong, that number is for case-sensitive search (the equivalent of 'LIKE'), case-insensitive search is more like 500,000 records per second.\n\nMind you that's without the collating algorithm I told you about, with that algorithm it's back to 700,000 to 800,000 records/s at the cost of doubling the record size.\n\nAlso note this is a very simple proof-of-concept implementation so of course it means little about how the final implementation would perform, the sole idea is to show that a very simple and quick to implement algorithm using tabular records can be very fast.\n\nPerhaps I'll keep working on this for its own sake it's kinda fun, I like algorithmic problems."
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-10
    body: "Didn't someone on the planet comment the other day that several KDE apps are using very different approaches for storage, and wonder if perhaps they should come together and discuss what is the best way for them to store data, and perhaps unify?\n\nA database backend for data is likely the way to remain, but what about Strigi indexing that data, and Nepomuk keeping ratings info and tags?"
    author: "T. J. Brumfield"
  - subject: "Re: Collection"
    date: 2008-10-10
    body: "There is a nepomuk collection in the works for amarok that when finished will likely be an alternative to using mysql.\n\nThe current problem with it?  It's way too slow for anyone to use."
    author: "Dan"
  - subject: "Re: Collection"
    date: 2008-10-09
    body: "Here comes the famous idiot keyword: bloat.\n\nAhhhh, they never know what they are talking about, but they use it so expansively."
    author: "Joe"
  - subject: "Re: Collection"
    date: 2008-10-11
    body: "Hi there care to elaborate on why you think I may not know what I am talking about?"
    author: "yeah"
  - subject: "Re: Collection"
    date: 2008-10-12
    body: "You never even made a claim to have any facts to backup your assertion..."
    author: "Ian Monroe"
  - subject: "Re: Collection"
    date: 2008-10-13
    body: "Hi Ian\n\nWell actually facts are what I'm asking about, because all I've seen thus far is claims that the current method is too slow but for all I know it takes many more song files for it to become slow than most users have and if so we'd be making the RDBMS a mandatory dependency for the benefit of but a few, that's why my initial query was about making it non-mandatory.\n\nAs for facts to back my concern a default MySQL installation consumes several tens of megs of memory which is not that much for a RDBMS but all the same pretty crazy in the face of the problem domain as it appears to be to me, that being a playlist.\n\nGranted as someone else pointed out politely the embedded version would have less overhead, I overlooked that.\n\nMind you I understand why developpers would act defensive these days after the mean spirited comments there have been about Plasma and KDE 4 as a whole so I am not blaming you or anyone, but it worries me a little that the actual questions I've been asking are only now being answered after this thread has already grown more than it probably should have.\n\nThank you for reading."
    author: "yeah"
  - subject: "Quick search"
    date: 2008-10-08
    body: "I am really missing the old quick search of Amarok 1.4. This is the feature I used most of the time and it was easily accessible using just the keyboard. But now search results are burried within the tree and I need to perform 2 mouse clicks to expand artist and album on every single search result to see the results. This makes the result totally inaccessible using the keyboard.\n\nWould be great, if search results would be expanded automatically and if they would become accessible by the keyboard like in the old days.\n\nHere is my wish: http://bugs.kde.org/show_bug.cgi?id=172379"
    author: "Carsten Schlipf"
  - subject: "Re: Quick search"
    date: 2008-10-10
    body: "Yes, I agree. IMHO the 1.4 search bar was very cool. Please add that bar to the amarok2\n\nThanks"
    author: "Alex Medina"
  - subject: "Internet services"
    date: 2008-10-08
    body: "I really like the new internet services. Since I installed beta2, I've listened to a number of short stories through the librivox-service, and harassed my friends using their last.fm personal radios :)\n\nMy only gripe so far is concerning the phonon-xine engine, I can't seem to make it respect ~/.xine/config, and it insists on resampling everything to 48Khz. Not the biggest issue in the world, and not really all that connected to Amarok.\n\nThanks for your work, I'm looking forward to 2.0 final!"
    author: "Anonymous"
  - subject: "Impact of MySQL on memory and other resource usage"
    date: 2008-10-08
    body: "My impression of mysql is that it is bigger and needs more resources than sqlite (it's also more performing, given enough memory, and suports more features).\n\nDoes anyone know what the memory and disk footprint of mysql is vs sqlite? I just hope I can still fit it into my ram-challenged laptop :)"
    author: "IAnjo"
  - subject: "Re: Impact of MySQL on memory and other resource usage"
    date: 2008-10-11
    body: "For what is worth, MySQL on my machine is using 24 megs of RAM after a couple of days uptime working with amarok.\n\nMy music collection contains 11000+ files."
    author: "NabLa"
  - subject: "Re: Impact of MySQL on memory and other resource usage"
    date: 2008-10-11
    body: "Okay that doesn't seem to unreasonable, thank you for being the first to actually provides figures because that's what I was asking."
    author: "yeah"
  - subject: "Re: Impact of MySQL on memory and other resource usage"
    date: 2008-10-12
    body: "He must be using Amarok 1.4 though. Amarok 2 doesn't use a separate process."
    author: "Ian Monroe"
  - subject: "Re: Impact of MySQL on memory and other resource usage"
    date: 2008-10-11
    body: "Yeah, thanks for an honest answer. That seems rather reasonable."
    author: "IAnjo"
  - subject: "Group compilations"
    date: 2008-10-08
    body: "Does it support grouping together compilation albums now?\nMy collection is very centered around albums/releases and this is a showstopper for me."
    author: "Tuxie"
  - subject: "Re: Group compilations"
    date: 2008-10-08
    body: "Yep, works. Compilations can be marked as such, and will then be listed under \"Various Artists.\"\n"
    author: "Mark Kretschmann"
  - subject: "Re: Group compilations"
    date: 2008-10-10
    body: "Sometimes, Amarok 2 is still acting quite oddly with some methods of distinguishing compilations.  There are plans for a much more complete method of handling the issue in the 2.1 timeframe."
    author: "Dan"
  - subject: "Amarok playlist settings/information"
    date: 2008-10-08
    body: "I can't seem to find how to change the amount of info the playlist shows. The most important one being the actual rating of the song. I find useless the ability to rate a song if you can't have a look at the songs ratings unless you are playing the song. I mean that you can't see the ratings on the collection nor even on the playlist.\n\nAm I missing something?\n\nThanks,\n\nDamnshock"
    author: "Damnshock"
  - subject: "Re: Amarok playlist settings/information"
    date: 2008-10-08
    body: "See here:\n\nhttp://amarok.kde.org/blog/archives/810-The-Old-style-Playlist-Is-Dead,-Long-Live-The-Old-style-Playlist.html\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok playlist settings/information"
    date: 2008-10-08
    body: "Thanks Mark, I already read it though ( a couple of minutes after posting my question :S)\n\n"
    author: "Damnshock"
  - subject: "Importing collection from 1.4"
    date: 2008-10-08
    body: "You can import stats from 1.4 what about collection from MySQL database?\n\nFrom release notes:\n\n\"Furthermore, this will allow us to expose using a standalone MySQL server in the future, if so desired, though this is not possible yet.\"\n\nI guess I will need to wait with migration to Amarok 2 ... :)\n\nOtherwise, big thank you for the excellent work.\n\nCheers\n\n"
    author: "Damijan "
  - subject: "Re: Importing collection from 1.4"
    date: 2008-10-09
    body: "I'm not quite sure what exactly you mean, but with Amarok 2.0 it will be possible to import your old MySQL database from 1.4. We are working on a script that does this.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Importing collection from 1.4"
    date: 2008-10-09
    body: "Hi,\n\nThanks. That is exactly what I meant and it is really good news :). So it looks like the only drawback will be that you need to migrate from standalone SQL to embedded MySQL database. \n\nI know that is stupid to ask for the roadmap, so my question is not when but: \n\nIs anyone currently working on MySql (standalone) database as a collection backend? \n\nMany thanks.\n\nD."
    author: "Damijan "
  - subject: "Re: Importing collection from 1.4"
    date: 2008-10-09
    body: "No!\n\nThe simple reason for this is that there is no need to. One of the reasons we choose MySql-embedded is that pointing it at a standalone external service is just a config option away and is completely transparent to the rest of the application. So all we really need is a config dialog for handling this ( and potentially a way to transfer an existing db back and forth between the embedded and external server )\n\nLikely this will come in one of the very first post 2.0. releases.\n\n- Nikolaj"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Importing collection from 1.4"
    date: 2008-10-09
    body: "Cheers Nikolaj"
    author: "Damijan "
  - subject: "Thanks"
    date: 2008-10-09
    body: "Thanks for another fine release, and for all the work you all put into Amarok."
    author: "R. J."
  - subject: "Deletion"
    date: 2008-10-09
    body: "This might seem a stupid question but when I have my Ipod show up in the collection tab, how do I delete a song from it? I see a 'copy to' and 'move to', but no 'delete from'."
    author: "Damiga"
  - subject: "Screen Space"
    date: 2008-10-09
    body: "I am using a resolution of 1400x1050 and amarok doesnt really fit into it... \n\nIn my opinion the main window is too crowded horizontally. I know that it is still in heavy development, so what about this idea: Move the plasma \"workspace\" in the mainwindow into its own tab on the left, and make it cover the whole window in this new tab?\n\nCombining this with effects (maybe under the plasmoid layer) would be awesome imho :)\n\nI can do some mockups if someone likes this idea :)\n\n"
    author: "Jan"
  - subject: "Re: Screen Space"
    date: 2008-10-10
    body: "No.\n\nThe logic of moving it to the center was to emphasize its importance in the Amarok world view.  Appropriate contextual information should always be availible.  Unfortunatly the world of plasma makes creating displays for this appropriate contextual information a whole lot less enjoyable than it could be, and therefore a lot of this appropriate contextual information does not exist at this point.\n\nI do not understand your claim that Amarok doesn't fit into 1400 pixels wide.... I have no problem with it here."
    author: "Dan"
  - subject: "Re: Screen Space"
    date: 2008-10-11
    body: "I second this idea.\n\nI also wish for there to be a resolution that lends itself to CarPC's.\n\nLow resolution 7\" screens.\n"
    author: "Max"
  - subject: "Re: Screen Space"
    date: 2008-10-19
    body: "We are actively working towards an alternative player, tentatively named the Amarok Mobile Companion, which is being based on the groundwork in Amarok 2's collection and service frameworks, incorporating a new UI designed specifically for touch-screen small form factor devices... So... watch this space :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Scripting change?"
    date: 2008-10-12
    body: "I seem to recall reading somewhere that Amarok 2 no longer will allow me to use Ruby to do scripting; is this true?\n\nIf so, is it possible to call external programs using the new Amarok scripting?  Here's to hoping I can hack my Ruby into Amarok some way or another."
    author: "KDE User"
  - subject: "Re: Scripting change?"
    date: 2008-10-12
    body: "Uhm, sure. I dont see any reason why it should not be possible to write a thin QtScript <-> Ruby wrapper.\n\nBUT, this would very much be against the entire point of why we switched to QtScript only. The reason is to avoid users running into dependency hell when installing or running scripts, a problem that is magnified several times when we go truly cross platform. Now, you can argue that ruby itself is not that bad, but many scripts today use other external dependencies ( Ruby-Qt bindings, xml parsing modules, ... ) that the user also has to install. With QtScript, as you have access to the entire Qt framework, there is really no need to install additional dependencies, and we can ensure that all scripts will work \"out of the box\" for all users as the dependencies are met simply by having Amarok installed.\n\nSo while you might be able to get around this, it will severely limit the audience of your scripts as we will only allow pure QtScript scripts to be distributed via the \"get hot new stuff\" service.\n\nBesides, although javascript has a very bad reputation, most of this (  deservedly ) comes from it being used as a web scripting language. As a popper scripting language, backed by the entire Qt and Amarok API, its really not that bad!"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Scripting change?"
    date: 2008-10-17
    body: "Thanks for the reply!\n\nMy scripts are only for me, so portability is not an issue at all.  I have no problem with Javascript per se, I just don't want to learn yet another language.  I certainly understand your reasoning, and who knows, I might wind up learning it anyway!"
    author: "KDE User"
  - subject: "any \"improvements\" on playlist management?"
    date: 2008-10-15
    body: "\ni am still using amarok 1.4 and i have running all the time because i use it to manage by podcasts.\n\nin amarok 1.4, trying to open an audio file from a konqueror will automatically add it to a playlist without start playing it, the only way(as far as i know) to have amarok play a file from double clicking it in konqueror is to first make sure the playlist is empty and this is not very practical and there is no way to change this ..\n\nhow do you guys plan to handle this in amarok 2? ..can i just double click a file in konqueror and have amarok start playing it? ..just adding it to the playlist and have me go to amarok to click it again(the way i have to do in amarok 1.4) to start playing it is not very convenient.\n\ncurrently, i have xmms player set as a default player in order to be able to just double click an audio file and simply have it start playing ..i think amarok should be able to do this"
    author: "ink"
  - subject: "Re: any \"improvements\" on playlist management?"
    date: 2008-10-19
    body: "Well, instant-play single-shot type stuff is what Dragon Player (or Codeine in KDE 3) was designed to do, so... you could always give that one a shot :)\n\nBut to answer your more immediate question, it's a case of default actions - try right-clicking on a sound file, there should be actions to append to playlist, load and play, and append and play (depending on the version of amarok you're running there may be less options, but at least the first and last option should be there). So... you can set up the default action to be what you want it to do on double-click (and since you're already set it to double-click in stead of single-click, which is the default in KDE, you should be able to change that default action as well with not too much trouble :) )."
    author: "Dan Leinir Turthra Jensen"
---
The Amarok Squad is proud to announce the second beta release of Amarok 2.0. This release includes a lot of bug fixes and improvements, like the switch from SQlite to MySQL-embedded. The LibriVox service is back, as is lyrics support. Please read the <a href="http://amarok.kde.org/en/node/554">release announcement</a> for a detailed list of changes since the release of Beta 1 and more.


<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: left">
<img src="http://static.kdenews.org/jr/amarok2-beta2.jpg" width="480" height="320" />
</div>

<p>
The most significant change in this release is the switch from SQLite to MySQL-embedded as the database backend. MySQL-embedded allows us to use the performance increase of the popular MySQL database, while avoiding non-trivial configuration that comes with a standalone server. Most noticeably, you will see much improved performance of collection scanning and searching, especially with very large music collections.
</p>

<p>
More generally we have fixed a huge amount of bugs, bringing Amarok 2 one step closer to the stability you would expect for your daily music needs. While not quite stable quality, we encourage our users to test this release and continue submitting bug reports. Already this beta version is good enough for daily use in many cases.  
</p>

<p>
We hope you enjoy this release which we have worked so hard to produce. Stay tuned for upcoming releases, and please do not forget to donate for <a href="http://amarok.kde.org/en/node/548">Roktober</a>!
</p>


