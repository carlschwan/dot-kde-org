---
title: "Google Summer of Code Projects Announced"
date:    2008-04-24
authors:
  - "jriddell"
slug:    google-summer-code-projects-announced
comments:
  - subject: "So no bitching about Gnome favoritism?"
    date: 2008-04-24
    body: "I saw the list on Google's site the other day and I almost couldn't believe it.\n\nLast year people were screaming Gnome favoritism.  I wonder if this is Google trying to be bipartisan, or perhaps there were just more good KDE ideas this year."
    author: "T. J. Brumfield"
  - subject: "GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "Unforunately, Google prefers to throw money at the GNOME office suite, which got 19 spots, compared to KOffice's 7: gnumeric 3, abisource 6, gimp 5, inkscape 5\n\nOverall, GNOME still got more projects (58): Gnome Office 19, Gnome 30, Cairo 3, Pidgin 6\n\nGoogle should allow KOffice and Kopete to register separately in order to make it fair."
    author: "Tray"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "Last year KOffice completed just 5[1], so 7 is certainly up. It should also be noted that KOffice has gotten less developers, the code that last years SoC devs wrote still is not released and some has been obsoleted even.\nI think its not that useful to talk about numbers here, lets focus on getting the most out of it. Like long lasting new developers and good code contributions.\n\nWith 7 I think KOffice can be very happy. I know I am :)\n\n1) http://dot.kde.org/1188249220/"
    author: "Thomas Zander"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "NOT. gimp isn't gnome, inkscape isn't gnome. also pidgin.\nThose are all GTK+ apps (as we can have Qt apps), not so bound\nto the DE as you seem to believe.\n\nThem are not part of any office suite, and you would be surprised\nif you knew how much kde users also use gimp or inkscape\n(with no offense for anyone but them are still fairly above the quality\nlevel/number of users of Krita and Karbon)."
    author: "Nae"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "\"gimp isn't gnome, inkscape isn't gnome. also pidgin.\"\n\nThen don't count KOffice and Kopete as KDE.\n\n\"them are still fairly above the quality level/number of users of Krita and Karbon\"\n\nKrita has some features that GIMP doesn't and it is likely that it will be better than GIMP short time after 2.0."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "Gimp isn't developed by the core Gnome developers, nor Inkscape, etc.\n\nHeck, last time I checked, those were multiplatform apps, where as Gnome is *nix only.\n\nKopete is shipped as a core KDE app these days, isn't it?  Koffice is in the KDE SVN repository, yet has its own release schedule, but it is closely tied to KDE."
    author: "T. J. Brumfield"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-25
    body: "So? Gimp is in the Gnome svn repository, uses the Gnome bugzilla and has extra support for gnome technologies it doesn't offer on other desktops."
    author: "Boudewijn Rempt"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-25
    body: "Gimp and Inkscape are not Gnome applications. They are simple GTK ones.\nKOffice and Kopete are KDE applications. \n\nIt's not a matter of 'opinion'. It's a fact. \nIt has to do with which libraries they use. \n\n"
    author: "ad"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-25
    body: "I know. But the comparison does not show anything if we compare the accepted projects for KDE containing an office suite and an instant messenger with the accepted projects of GNOME not containing those. The fair comparison is to compare GNOME with KDE-KOffice-Kopete because GNOME does not contain them, so KDE with them is a bigger project and, as such, could have received more projects."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-05-03
    body: "What the hell are you talking about? Does Krita run without KDE installed? Does Gimp run without GNOME installed?\n\nEnd of story."
    author: "Marc Driftmeyer"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "IMO this is not because Google prefers GNOME Office or GNOME to KDE but more because KDE is one big umbrella organization, for KOffice, Amarok, pim, network, education, etc. It would be nice to get separate organization for example Amarok, KOffice for next year, but for now lets be grateful for what we've had.\n\n(And also, some projects are not GNOME project directly, but GTK+ projects (e.g: pidgin) - so a bit hard to compare directly).\n\nP.S: Is there really a GNOME Office? As far as I know it is only few separated applications branded as \"Office suite\"."
    author: "fred"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-25
    body: "GNOME likes (liked?) to pretend there is http://live.gnome.org/GnomeOffice\nBut in a world where even OOo and Firefox are often seen as/claimed to be GTK+/GNOME apps this doesn't mean much indeed."
    author: "Anon"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "I find it sad that you create such a negative post with your commented that unfortunately they prefer gnome.  Sour grapes in your post.\n\nI sometimes get bored of the linux community.  I get tired of the whole attitude some have between gnome vs kde.  And the attitude between distro vs distro.  \n\nPerhaps your sour grapes can be dropped and you can congratulate Gnome on their accomplishments, after all, it is all about free and open software.  And google is being fair, it's just your spoilt brat attitude towards Gnome needs to change."
    author: "anon"
  - subject: "congrats to all"
    date: 2008-04-24
    body: "true, congrats to all."
    author: "dave null"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-24
    body: "There is no such thing as Gnome Office. Also Pidgin, Gimp and Inkscape are not Gnome applications, and I'm not sure about Gnumeric and Abiword."
    author: "Narishma"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-25
    body: "Just nitpicking here, but apparently there is such a thing as \"GNOME Office\". It's just not as integrated.\n\nIt consists of Abiword, Gnumeric, and GNOME-DB.\n\nAt least that's according to Gnumeric's website: http://www.gnome.org/projects/gnumeric/ and wikipedia.\n\nThe abiword website barely even mentions GNOME or even gtk (but it's there). GNOME-DB of course mentions GNOME, but I didn't look hard enough to find a mention of GNOME Office.\n\nOh, and Gnumeric is an official gnome project.\n\nAbiword and Gnumeric are both very good apps. I actually tend to use them over the KOffice counterparts, and I'm a KDE fan boy. KSpread didn't have XY-scatter, and its calculation dependencies were innefficient (to the point of crashing), so I had to use gnumeric. However, to print, I had to open the spreadsheets up in KSpread for good PDFs.\n\nFrom what I've read, the 2 problems I had with KSpread are fixed in 2.0, so I've got my fingers crossed."
    author: "Soap"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-04-28
    body: "This point is moot. Abisource runs without most gnome libraries.\n\nCairo too.\n\nWhat are you talking about? You cant bundle individual components together.\n\n"
    author: "she"
  - subject: "Re: GNOME still got more projects (58) :-("
    date: 2008-05-03
    body: "GNOME Office?\n\nLast time I checked, it wastn't a suite, just a bunch of useful apps for office work."
    author: "Axl"
  - subject: "Xbox 360 Streaming? "
    date: 2008-04-24
    body: "After UPnP is done being integrated into Amarok. Does this mean that I can stream videos, and Music to my xbox 360? \n\nSurprisingly enough, Microsoft used a standard for streaming inside the Xbox. This is my most missed feature and I would really appreciate it. :D\n\n"
    author: "Jeremy"
  - subject: "Re: Xbox 360 Streaming? "
    date: 2008-04-24
    body: "The priority of the project is on using Amarok as a client to UPnP servers, since actually you can stream from Linux to your XBox 360 already I think."
    author: "Ian Monroe"
  - subject: "Re: Xbox 360 Streaming? "
    date: 2008-04-24
    body: "There are various streaming programs that will allow you to do this, but it would be nice if Amarok could do this as well."
    author: "T. J. Brumfield"
  - subject: "Re: Xbox 360 Streaming? "
    date: 2008-04-24
    body: "with the vlc backend it should be easy to do"
    author: "Patcito"
  - subject: "Re: Xbox 360 Streaming? "
    date: 2008-04-24
    body: "wait... what?\n*insert lolcat image*"
    author: "Ian Monroe"
  - subject: "Re: Xbox 360 Streaming? "
    date: 2008-04-24
    body: "Isn't there a vlc backend for phonon?"
    author: "me"
  - subject: "Re: Xbox 360 Streaming? "
    date: 2008-04-27
    body: "Phonon doesn't have anything to do with UPnP."
    author: "Ian Monroe"
  - subject: "NX"
    date: 2008-04-24
    body: "I see that NX is in SoC. But I wonder: I saw those impressive demos few years ago, and even tried it myself. And the software is very impressive. But it seems that it hasn't gotten anywhere, as far as KDE is concerned. Few years ago I wished for transparent, bulletproof NX-support in KDE, and it still seems to be in the \"to-do\"-list. Yes, it propably works, but it's a hassle to set up.\n\nback then, I dreamed of a aystem that would be 100% transparent no matter if the desktop was running on the local machine, or on a machine far away. We could then have low-powered net-enabled devices, that the user could use to connect to a powerful machine somewhere else. We are already getting devices just like that with the HP Mininote and Asus EEE.\n\nSince KDE already has server-related projects (Kolab etc.), how about NX-focused server that is integrated with KDE?"
    author: "Janne"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "> Since KDE already has server-related projects (Kolab etc.), how about NX-focused server that is integrated with KDE?\n\n\nSounds good!  When will you be finished? :-)"
    author: "John Tapsell"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "Why is it that whenever someone makes a suggestion, the answer is something like \"Sounds great, when can you start coding?\". Does it ever occur to any of you that not everyone is a coder?"
    author: "Janne"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "\"Yea, they probably say so becoz they want to do what interests them, and not what some random tells them to do\""
    author: "Book of Konqui"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "I'm not telling anyone to \"implement this! NOW!\". I'm throwing a suggestion out there. If someone picks it up, great! if not, well, too bad. I think something like this would be very useful and cool. But I can't write it, since I'm not a coder."
    author: "Janne"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "and if you complain about some software they'll wiggle in their chair with pride after they type \"then return it\" or \"get your money back\". making their comment will secure their position in the linux l33t or some bull shit like that! Oh yes, for years after people will say \"anonymous fanboy was so hard core linux\" and visions in this coders head of girls noticing him for his linux wit, or perhaps the amount of ram in his system.. blah blah blah...\n\ni think those who jump on others even in the name of linux are just as bad"
    author: "anon"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "was that English?\n\n:D"
    author: "avid reader"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "It is because all the active coders have a lot of ideas themselves, most of which they will not even be able do anytime in their life. \n\nSo it occurs pretty lame to just throw in \"See my great idea, am I cool, please do it for me.\". Ideas are easy to produce, they are created all the time. If we all shouted out our new ideas anytime there would be a hell of a noise.\n\nCode is the hard work. It takes time. And sweat.\n\nStill everyone can learn to code, the current coders did, you can, too. To do so won't be lame, really ;)"
    author: "Koder"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "\"Ideas are easy to produce, they are created all the time.\"\n\nYes, but good ideas are rare and coders don't have a monopoly on producing them.\n\nAssuming that developers are able to tell a good idea from a bad one, the greater the number of good ideas available (from users and developers alike) the more likely that time will be spent on them. I've wasted time developing random crap because I can't come up with anything better, I'm sure I'm not the only one.\n\nThere is a huge difference between suggesting and demanding. Not everyone can code or wants to. Get over it. If you want \"better ideas\" encourage people with ideas to flesh them out, deliver specs and use cases. That's far more productive than saying \"learn to code\" to someone who has no experience in doing so: Think how long it has taken you to learn to code (including school, college, university, and self educating). If you had to start coding now, could you retro-fit that around your 9-5 day day job, family and social commitments? I doubt it: I couldn't. Not everyone has that much free time.\n\nFor what it's worth I would also like to see what the OP proposed become a reality. But, don't worry, I won't mention it again ;)"
    author: "Martin Fitzpatrick"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "[If you want \"better ideas\" encourage people with ideas to flesh them out]\n\nExactly. Put emphasize on the \"want\". If one wants, sure. But there are too many just rushing into this virtual room and shouting without being asked to \"Look, you must do it, it is so clever.\" This \"must\" is what hurts. It collides with the liberty feeling.\n\n[Not everyone has that much free time.]\n\nThen he should take his money and pay someone to scratch his itch. That simple. Or learn to pray. But not go out and act like nagging people to do something for him for free.\n"
    author: "Koder"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "what on earth is this discussion about? I see comments about how \"making demands is not cool\" when in fact I explicitly said that I'm NOT demanding anything. And it seems to me that we are approaching a point where coding-skills are required even if you just suggest something or float an idea that might be useful. and what is this about \"nagging\"? Was my comment \"nagging\"? \n\nWell, I apologize. I'll try not to make any feature-suggestions, ideas for improvement or anything of the sort in the future. I guess those are reserved to coders and coders alone. Please, just disregard my comment, it was just a stream of thought from a lowly user. I know better now, and I'll just stfu. "
    author: "janne"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "Apoligize taken.\n\nThe problem is not your intention, but the way it arrives. It is like unwanted help or hints. Too many of them make someone angry. It is received as nagging, by sensible ones at least ;)\n\nYou are welcome to add your ideas to bugs.kde.org, so they won't be lost. Other than in random forum posts, where they are. Yes, that needs some work. Still it is the way for you to participate, they way you are asked for. Just \"throwing in wild ideas\" at the first best place without being asked for, is too easy and received as nagging, really."
    author: "Koder"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "No the problem is that many posters are morons who overreact to perfectly sensible posts like Janne's. Many of them are not even part of the project themselves but they get high on these pathetic power trips. How about letting the people directly related to these projects criticize these kind of posts if they feel like it. They are usually much more sensible. "
    author: "ad"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "\"Apoligize taken.\"\n\nUm, I was being sarcastic.\n\n\"The problem is not your intention, but the way it arrives.\"\n\nAnd what would that be? \"nagging? It would be nagging if I constantly demanded that this particular feature gets implemented. How exactly can one singular comment that is few sentences long be considered \"nagging\"?\n\n\"You are welcome to add your ideas to bugs.kde.org\"\n\nNo, I won't be doing that. I mean, I don't want to offer any \"unwanted help\" or \"nag\" to the developers... Besides, those coders have interests of their own, what does it matter what I want done, right? After all, I'm not a coder.\n\n\"Just \"throwing in wild ideas\" at the first best place without being asked for, is too easy and received as nagging, really.\"\n\nI talked about NX, which was directly related to the subject of this article. Are you saying that comments that contains something that could be seen as a \"suggestion\" do not belong to dot.kde.org? \n\nDon't like the suggestion or the way it was presented? Fine, ignore it. Just don't start nagging about it."
    author: "Janne"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "[It would be nagging...]\n\nWell, you are just not the only one. Try to see if from a active person's POV: There may be only one suggestion per user, but by the masses of users it makes up an enormous flood of suggestions.\n\n[How exactly can one singular comment ... \"nagging\"?]\n\nWell, you also only got one singular comment \"Where is your code?\" and already started to be pissed of. As you also see a masses of developers who each only one time say something like that ;)\n\n[No, I won't do that.]\n\nSo, you are not really interested? Too sad. Pretty sensible, you are. And see, bugs.kde.org is set up exactly for this, that is where your help is really, really wanted. But obviously now your ego is in the way, oh dear.\n\n[Fine, ignore it.]\n\nHoney, just be a good example and ignore comments like \"Where is your code?\" then :P"
    author: "Koder"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "\"There may be only one suggestion per user, but by the masses of users it makes up an enormous flood of suggestions.\"\n\nYes, heaven forbid when users make suggestions on how to make the software better! How dare they! they should just use the software and STFU! Or better yet: they shouldn't even use the software either, then the coders would REALLY be left in peace, right?\n\n\"Well, you also only got one singular comment \"Where is your code?\" and already started to be pissed of.\"\n\nBecause the mentality that only coders are worth something, is getting pretty old, pretty fast. If I wanted to use software where dialog with the developers is not wanted, I might as well use Windows.\n\nIt's not constructive to suggest something, and receive a reply that basically says \"do it yourself\". What WOULD be constructive is a discussion on the pros and cons of the idea, and further ideas how to improve it. \n\n\"So, you are not really interested?\"\n\nYes I am. In fact, I have filed bug-reports. I have discussed things in mailinglists, developer-blogs and in various forums. But I wont be doing that anymore, since you made me see the light. Feedback, suggestions, wishes and the like not wanted. Thanks for clearing that up for me.\n\n\"Honey, just be a good example and ignore comments like \"Where is your code?\" then :P\"\n\nIn the end, suggestions lilke that are not constructive. It just reinforces the \"by coders, for coders\"-mentality. Fact of the matter is that most people are not coders. And still, those people might have valuable feedback to provide. But here you are basically saying that if they can't code, their feedback is worthless.\n\nLike I said: thanks for helping me see the light. I know now that feedback and suggestions are not wanted.\n\nHave fun coding."
    author: "Janne"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "\"... And see, bugs.kde.org is set up exactly for this, that is where your help is really, really wanted.\"\n\n[I know now that feedback and suggestions are not wanted.]\n\nI see. What stops you from learning to write code is that you have already problems to read... :) At least if it doesn't fit your own view.\n\nBut you have a good excuse now to stop contributing even your ideas, those (all of them?) lame developers are just too arrogant to listen to you. I gratulate. You are free."
    author: "Koder"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "\"Koder\":You're an idiot.\n\nJanne: For what it's worth, I am a developer (not of KDE but other things) and I value contributions like yours. AKA polite and interesting. Keep contributing, filing bugs and having discussions: if only to drown out the sound coming from this guy. Have fun.\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: NX"
    date: 2008-04-25
    body: "Oh, thanks, other than you obviously. Hope you don't mind that I still value your elaborated argumentation techniques.\n\nPlease now continue your fruitful discussions about nx dreams, sorry that I disrupted.\n\nHave fun. But learn to code."
    author: "Koder"
  - subject: "Re: NX"
    date: 2008-04-26
    body: "<blockquote>Have fun. But learn to code.</blockquote>\n\nAlready can. Learn to read.\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: NX"
    date: 2008-04-28
    body: "Koder: could you please quote the part in dot.kde.org that says \"feedback and suggestions are not wanted here\"? Also, could you please quote the part in kde.org that says \"feedback and suggestions are only wanted in bugs-kde.org, and nowhere else\"? I'm waiting.... If you can't find text that says that, then what exactly makes you think that dot is not kosher as far as suggestions and feedback is concerned?\n\nI have talked about KDE in my blog, what are you going to do about it? Punch me in the face? I have also talked about KDE in developer-blogs? What are you going to do about that? Start whining that I should only talk about kDE in bugs.kde.org?\n\nAlso, are you part of the KDE-project? If you are not, then what makes you think that you can talk for KDE? Only person you can talk for, is yourself.\n\n\"At least if it doesn't fit your own view.\"\n\nLook who's talking. You start whining if someone does something that does not fit your narrow world-view on things."
    author: "Janne"
  - subject: "Re: NX"
    date: 2008-04-28
    body: "Koder: are you a KDE developer?"
    author: "Vide"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "But not everyone learns how to code well. I would know,  I suck. \nI get by but mostly only because of my good looks and rapier wit. \nThe last thing we need is an influx of crappy coders into the project. \nOh wait, gSOCers. nm.\n\nRemember now kiddies, don't feed the troll. \nEspecially if he only assymptotically approaches a valid point..."
    author: "illogic-al"
  - subject: "Re: NX"
    date: 2008-04-24
    body: "I've spun this thought a bit further: \nhttp://ich.war-es-wirklich.net/cms/KDE-home-system-pipe-dream\n\nIf think this is as much a question of packaging and creating an installer as it is of writing KDE-specific code.  I wonder if there is a distro that helps with these things (easy setup of more than one machine).\n\n"
    author: "cm"
  - subject: "OT: flatforty broken"
    date: 2008-04-24
    body: "Am I the only one to notice that the flatforty functionality of the dot is broken since some time... :-)\n\nhttp://dot.kde.org/static/flatforty\n"
    author: "Yves"
  - subject: "Re: OT: flatforty broken"
    date: 2008-04-25
    body: "I know, and I will look into it this weekend."
    author: "Daniel Molkentin"
  - subject: "Re: OT: flatforty broken"
    date: 2008-04-25
    body: "Seems to be working now - thanks for helping to lick the Dot into shape :)"
    author: "Anon"
  - subject: "Amarok!?"
    date: 2008-04-24
    body: "Maybe Amarok should have applied seperately though.\nI recognize Amarok is one of the most famous KDE-Apps, but still I think too many projects were assigned to it (after all amarok is not part of the default kde-desktop - thank god ;) ).\nAnyways, congradulations for getting that many assigned students.\n\nI am especially looking forward to \"Jingle video and voice chat in Kopete\"!"
    author: "Hannes Hauswedell"
  - subject: "Re: Amarok!?"
    date: 2008-04-24
    body: "We hate you too.\n\n-signed \nillogic-al \ninsanity"
    author: "illogic-al"
  - subject: "Congratulations to all"
    date: 2008-04-24
    body: "To all the students who were accepted: Congrats!  I hope you enjoy it and I look forward to the results.\n\nI am, however, disappointed that my single most anticipated project didn't make the list ( http://lists.kde.org/?l=kde-multimedia&m=120640149026582&w=2 ).  I do hope Konrad is still able to find time to work on it without GSoC.  Subtitles could really benefit from his work, and I think many many people (myself included) would appreciate it."
    author: "mactalla"
  - subject: "webkit"
    date: 2008-04-24
    body: "I'm glad a project for webkit-kpart was approved.\nNow I can drop my plans for a webkit browser, hehheheheh."
    author: "Iuri Fiedoruk"
  - subject: "Re: webkit"
    date: 2008-04-24
    body: "Also noticed a project to add libjinble into kopete for audio & video: OH YEAH!\nI miss it really a lot."
    author: "Iuri Fiedoruk"
  - subject: "Re: webkit"
    date: 2008-04-25
    body: "Why drop it? You could also just join the current work on it to be sure the solution still comes with everything your own solution would have.\n\np.s. http://websvn.kde.org/trunk/playground/bindings/phpqt/ ;)\n"
    author: "Sebastian Sauer"
  - subject: "Re: webkit"
    date: 2008-05-05
    body: "A standalone QtWebKit-based browser is already existing, and in need of contributors: http://code.google.com/p/arora/"
    author: "Yves"
  - subject: "OMG!!! kate vi mode!!!"
    date: 2008-04-24
    body: "There you go, so it's official, it will come!\n\nI'm so depending on this one, I am sure that all living things will never feel any positive feeling ever again if kate doesn't get a decent vi mode!11!!eleven!\n\nGO FOR IT ERLEND HAMBERG, YOU'RE A HERO!\n\nPS:\nLet's have a church of kate!\n\nI already entered the church of emacs in my years of study, I hope these religions are compatible!\nI would, if I could, actually leave the church of emacs. Never used it anyways. But RMS is just too arrogant to provide us with a respective sentence...\n\n\n\n\n\n\nWell honestly, loads of cool projects going on, and KDE 4.0 surely needs a lot of manpower these days. Having invested so much in fast and easy application development, it should really work out like a charm."
    author: "fhd"
  - subject: "Re: OMG!!! kate vi mode!!!"
    date: 2008-04-25
    body: "Finally! I have crazy fan boys! :-)\n\nI hope you will be happy :-)"
    author: "Erlend"
  - subject: "Get the others"
    date: 2008-04-25
    body: "is there anything done to get the students on board that did not get accepted?\nI think a little special event where all students not accepted are invited (do you have their email adresses?) would be nice, if only 10% would try to complete their project even without SoC it would be a huge win for kde"
    author: "Beat Wolf"
  - subject: "Re: Get the others"
    date: 2008-04-25
    body: "Last year KDE did something like the GSC don't know if there was a reward though."
    author: "Terracotta"
  - subject: "Re: Get the others"
    date: 2008-04-28
    body: "I think it would be nice with an unofficial SoC for those who didn't get picked. Small brainstorm:\n\n- The mentors pick projects they think would benefit the application etc.\n\n- There would still be lots of proposals left. Now it's time to decide the final ones.\nAll projects accepted by the mentors will be put on a website, and everyone can vote for their favorite. The special thing here is that you \"vote\" by donating money - the more you donate, the more it counts. You may pay for more than one proposal.\n\n- Let's say five projects get accepted. The money from the voting gets distributed evenly between these projects. It'll be used to pay the student and mentor after the work's finished.\n\nSomething like that. What do you say? There sure are many problems to be addressed, but what do you think about the basic concept? Is it possible, is it be fair?\n\nPersonally I would happily donate at least 10 Euro to see my favorite feature in KDE, for example extenders in plasma. And I'm a poor student without much money left in the end of every month. Surly there are more people who are willing to donate to motivate students to finish the project?"
    author: "Hans"
  - subject: "Re: Get the others"
    date: 2008-04-29
    body: "We did already have a special GSoC metting on #kdegames IRC."
    author: "Stefan Majewsky"
  - subject: "Plasma extenders?"
    date: 2008-04-25
    body: "Sounds like an interesting concept. But I am not sure if I correctly understood what is intended. Are there any mock-ups, videos or may somebody explain it any further?"
    author: "Sebastian"
  - subject: "Re: Plasma extenders?"
    date: 2008-04-25
    body: "http://plasma.kde.org/cms/1069\nDoes an animated gif along with an explanation count? :P"
    author: "Jonathan Thomas"
  - subject: "Re: Plasma extenders?"
    date: 2008-04-25
    body: "Ah, that one. Thanks."
    author: "Sebastian"
  - subject: "Re: Plasma extenders?"
    date: 2008-04-25
    body: "You can also take a look at my blog where I try to explain it a bit further:\n\nhttp://pindablog.wordpress.com/2008/04/23/gsoc-extenders-project/"
    author: "Rob Scheepmaker"
  - subject: "SVG"
    date: 2008-04-29
    body: "I see two projects:\n\nIntegration of WebKit SVG library with KHTML\n\nWebKit SVG Filters Design and Implementation\n\nThere seems to be some inconsistency in the project descriptions regarding which features are available for SVG in WebKit.  Perhaps the two projects could work together on this.\n\nIAC, there is an architecture question here.  KHTML currently uses a KPart from KDEGraphics to render SVG.  This KPart is based in QSVG.  It appears to me that if this is to be changed (and work is needed since QSVG doesn't support filters) that work should be done on the bottom of the stack -- on KDEGraphics rather than changing the library that KHTML uses to render SVGs.\n\nThen it appears that WebKit currently doesn't support much (if any) more of the SVG features than QSVG does.  I would like to point out again that Apache Batik has a very good SVG renderer which is written in Java.  Rather than reinventing the wheel, could this existing code be used?  Either by porting the Java to C++, using JNI to use the Java code, or compiling the Java code into C++ classes with GCJ. "
    author: "JRT"
  - subject: "Re: SVG"
    date: 2008-05-05
    body: "> Then it appears that WebKit currently doesn't support much (if any) more of the SVG features than QSVG does. I would like to point out again that Apache Batik has a very good SVG renderer which is written in Java. Rather than reinventing the wheel, could this existing code be used? Either by porting the Java to C++, using JNI to use the Java code, or compiling the Java code into C++ classes with GCJ.\n\nQtSVG only supports SVG Tiny, whereas WebKit's SVG is intended to support full SVG.\n\nUhmm... porting Batik? I guess it will introduce a lot more problems than porting WebKit SVG into KHTML."
    author: "fred"
---
Google have announced the <a href="http://code.google.com/soc/2008/kde/about.html">projects and students for this years Summer of Code</a>.  We received the biggest number of students allocated to a project with 47 taking part.  Applications which will be worked on include Amarok, KOffice, Marble and entirely new features such as a collaborative text editor.



<!--break-->
