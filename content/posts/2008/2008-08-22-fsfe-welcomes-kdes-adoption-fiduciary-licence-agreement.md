---
title: "FSFE Welcomes KDE's Adoption of the Fiduciary Licence Agreement"
date:    2008-08-22
authors:
  - "skuegler"
slug:    fsfe-welcomes-kdes-adoption-fiduciary-licence-agreement
comments:
  - subject: "Great news, will help KDE in the long run"
    date: 2008-08-23
    body: "This seems to be a formal and legally-sound version of the table in\n\nhttp://techbase.kde.org/Projects/KDE_Relicensing\n\nwhich lists contributors that have agreed to re-license code. I can't wait until all of KDE is 100% compatible with the GPLv3."
    author: "Bill"
  - subject: "Sebas starts talking ( right NOW )"
    date: 2008-08-23
    body: "At Froscon\n\nhttp://streaming.linux-magazin.de/programm_froscon08.htm\n\nin english. \n\nMight interest somebody.\n\nCheers"
    author: "Tom"
  - subject: "Re: Sebas starts talking ( right NOW )"
    date: 2008-08-23
    body: "cool; thanks for the link. i'm watching now; a great way to start my saturday morning =) i've seen this presentation of Sebastian's a few times, but it's still a fun one to watch and its cool to see it get slicker each time he does it. =)"
    author: "Aaron Seigo"
  - subject: "Re: Sebas starts talking ( right NOW )"
    date: 2008-08-23
    body: "You are welcome.\n\nIt is a very cool service from linux magazin. I also watched your talk at Linuxtag. Only thing to improve would be giving out mics for Q&A.\n\nAnd you are right it was pretty slick. \n\nAlthough I think most Akademy talks were as slick. If only I could download them today ;)  (Sorry .. I had to push that agenda, cause I am dying to see some of the talks. Is there a new timeframe on when they can be downloaded?)"
    author: "Tom"
  - subject: "Re: Sebas starts talking ( right NOW )"
    date: 2008-08-23
    body: "is it also possible to download the talk? Sadly i missed the live stream..."
    author: "pinky"
  - subject: "Re: Sebas starts talking ( right NOW )"
    date: 2008-08-24
    body: "Just check back in a week (maybe more), there will be an archive page for Froscon, like there is for Linuxtag. ( http://streaming.linux-magazin.de/ )"
    author: "Tom"
  - subject: "FSFE"
    date: 2008-08-25
    body: "There were some misleading variants of the news spread, e.g. that KDE contributors would assign their copyright to the FSFE."
    author: "andy"
  - subject: "Re: FSFE"
    date: 2008-08-25
    body: "if this was published anywhere (e.g. not just passing irc chatter), please let us know so that we can offer a correction."
    author: "Aaron Seigo"
  - subject: "Re: FSFE"
    date: 2008-08-26
    body: "Very big german news site heise.de got it wrong:\n\nhttp://www.heise.de/newsticker/KDE-fuehrt-neue-treuhaendische-Lizenzvereinbarung-ein-Update--/meldung/114692\n\n"
    author: "Tom"
  - subject: "Quick note: KDE website ouf of date"
    date: 2008-08-25
    body: "I just noticed this while browsing Kubuntu links.\n\nthe http://discover.kde.org/ site is out of date. \n\nI think it should reflect the fact that KDE is on version 4.1 now and maybe include a quick list of updates and new features.\nIt would look more professional to the KDE community and more importantly new users considering switching to a KDE environment.\n\n(maybe even include KDE on Windows on that page.)\n\n\nHave an awesome day!\n\n"
    author: "Max"
  - subject: "Re: Quick note: KDE website ouf of date"
    date: 2008-08-25
    body: "The site never was up to date, it's not even finished yet. It currently is in some sort of transient state. It'll be ready somewhen, and up to date then. Until then, please don't tell anybody to look over there. Thanks."
    author: "sebas"
---
Free Software Foundation Europe welcomes the adoption of the Fiduciary Licence Agreement by the KDE project. The FLA is a copyright assignment that allows Free Software projects to assign their copyright to a single organisation or person. This enables projects to ensure their legal maintainability, including important issues such as preserving the ability to re-license and certainty to have sufficient rights to enforce licences in court. Read on for more details.
<!--break-->
<p>"<em>We see the adoption of the FLA by KDE as a positive and important milestone in the maturity of the Free Software community,</em>" says Georg Greve, president of Free Software Foundation Europe. "<em>The FLA was designed to help projects increase the legal maintainability of their software to ensure long-term protection and reliability. KDE is among the most important Free Software initiatives and it is playing a central role in bringing freedom to the desktop. This decision of the KDE project underlines its dedication to think about how to make that freedom last.</em>"</p>

<p>Adriaan de Groot, Vice President of KDE e.V., the organisation behind the KDE project, said "<em>KDE e.V. has endorsed the use of a particular FLA based directly on the FSFE's sample FLA as the preferred way to assign copyright to the association.  We recognise that assignment is an option that individuals may wish to exercise; it is in no way pushed upon KDE contributors. There are also other avenues of copyright assignment available besides the FLA, but we believe this is the easiest way to get it done, with little fuss. Enthusiasm for the FLA was immediate - people were asking for printed versions of the form at Akademy before the week was out so that they could fill one in.</em>" </p>

<p>"<em>The FLA is a versatile document designed to work across different countries with different perceptions of copyright and authorship,</em>" says Shane Coughlan, Freedom Task Force coordinator.  "<em>As a truly international project, KDE provides a great example of how the FLA can provide legal coherency in the mid-to-long term.  It's been a pleasure to help with the adoption process and FSFE's Freedom Task Force is ready to continuing supporting KDE in the future.</em>"

<p>KDE's adoption of the FLA is the result of cooperation between KDE e.V. and FSFE's Freedom Task Force over the last year and a half, part of the deepening collaboration between the two associate organisations.</p>

<p>The full press release can be found on <a href="http://ev.kde.org/announcements/2008-08-22-fsfe-welcomes-fla.php">KDE e.V.'s website</a>.</p>



