---
title: "Akademy 2008 was Amazing"
date:    2008-09-09
authors:
  - "jpoortvliet"
slug:    akademy-2008-was-amazing
comments:
  - subject: "Ogg Theora"
    date: 2008-09-10
    body: "Thanks for offering all the media in free formats. Sadly this is rare even among free software projects.\n\nThumbs up!"
    author: "Hannes Hauswedell"
  - subject: "Re: Ogg Theora"
    date: 2008-09-11
    body: "Yeah, except that Ogg Theora extension is now .ogv other audio format are .oga. ogg can be used for vorbis for backward compatibility."
    author: "Lisz"
  - subject: "OGG BROGGCASTERS in 2009"
    date: 2008-11-05
    body: "A super-ammount of OGG BROGGCASTING, TV stations will start in 2009, to challenge big coporate/big media outlets (with streaming video plans). The challenge will come from grass-roots media, with professional news vans, and TV studios, (like YouTubeLive will have). Mobile to HD BROGGCASTS, will place OGG, as the winner of a online media, streaming content war."
    author: "OGGTV"
  - subject: "picture link..."
    date: 2008-09-10
    body: "Nice overview, thank you Jos.\n\nPlease fix this picture link:\n\nhttp://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-discussing-plasma.jpg\n\nIt has an additional \".jpg\" at the end.\n\nCheers\n"
    author: "Hans"
  - subject: "Re: picture link..."
    date: 2008-09-10
    body: "Fixed."
    author: "Danny Allen"
  - subject: "[OT] Good enough for LHC, good enough for me ;)"
    date: 2008-09-10
    body: "Few nice screenshots from todays great physics experiment:\n\nhttp://img388.imageshack.us/my.php?image=ohptoftimemeasured13sepjy2.png\nhttp://www.spiegel.de/fotostrecke/fotostrecke-35141-3.html\nhttp://www.spiegel.de/fotostrecke/fotostrecke-35141-5.html\nhttp://www.bbc.co.uk/radio4/bigbang/gallery.shtml?select=13\n\nIs this KST with Keramik?"
    author: "m."
  - subject: "Re: [OT] Good enough for LHC, good enough for me ;)"
    date: 2008-09-11
    body: "> Is this KST with Keramik?\n\nWell, clearly it's Keramik, but the title basrs say \"Atlantis\".  A quick Googeling turns up some GTK app installer named that.  I don't know what that is they are using.  It could very well be some in-house thing.\n\nM.\n"
    author: "Xanadu"
  - subject: "Re: [OT] Good enough for LHC, good enough for me ;)"
    date: 2008-09-11
    body: "as this seems to be some application to monitor some ATLAS experiments, it should be an inhouse-program and the first 2 letters from ATLANTIS are the same like in ATLAS..."
    author: "Thomas"
  - subject: "Re: [OT] Good enough for LHC, good enough for me ;)"
    date: 2008-09-11
    body: "You mean the first 4... :)"
    author: "4 letter writer"
  - subject: "Re: [OT] Good enough for LHC, good enough for me ;)"
    date: 2008-09-11
    body: "http://www.hep.ucl.ac.uk/atlas/atlantis/\n\nThis is what I could dig up after a quick google search. It's actually a java app, and fully open source source as well."
    author: "FVA"
---
It has been a couple of weeks since <a href="http://akademy2008.kde.org/">Akademy 2008</a> finished.  KDE's contributors are now back home, more enthusiastic than ever about our future.  If you missed the talks <a href="http://akademy2008.kde.org/conference/program.php">videos are now online</a>. This article covers what happened during the week and outlines some of the results. Read on for more.




















<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-location.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-location.jpg" width="400" height="267" /></a><br />
This is where Akademy was held.
</div>

<h2>The Beginning</h2>

<p>On Friday August 15th 2008, hundreds of KDE contributors came to the city of Mechelen to register for the event many had been looking forward to for almost a year: <a href="http://akademy2008.kde.org">Akademy 2008</a>. We played. We worked hard. We drank beer and we ate food. We even <a href="http://www.nielsvm.org/2008/08/13/french-fries-size-comparison/">discussed eating food</a>. We listened to talks. We brainstormed. We discussed. We designed. And we wrote code. But after a long and busy week, it was time to go home. Most of us have regained our strength after this exhausting, yet energising week, and we are looking back at one of the best meetings we ever had. Of course, one can never really capture all that happened. Despite the impact of the keynotes and BoF's, much happened in the corridors. Much has not been recorded anywhere but in the memories of those participating. The following report therefore focuses on the big events and the announcements.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-enthousiastic.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-enthousiastic.jpg" width="300" height="450" /></a><br />
Modelling the latest sleek design.
</div>

<p>The <a href="http://dot.kde.org/1218353987/">first day</a> of Akademy brought us two keynotes, 16 other presentations, and various lightning talks about Plasma, moderated by Aaron Seigo. The first keynote was given by Frank Karlitschek. He spoke about increasing community involvement by giving "power to the people", and the <a href="http://www.open-collaboration-services.org/">refreshing ideas in his talk</a> represented Akademy 2008 in a nutshell: innovation and community. This topic was further explored in talks following the keynote. Some of these ideas are described in the article about <a href="http://dot.kde.org/1218645101">integration at Akademy</a>. Related was the talk about a <a href="http://dot.kde.org/1219926799/">future development model of KDE</a>. This talk and the BoF session later on have resulted in many discussions within the community. Time will tell if the ideas discussed will really shape the future of KDE development and the Free Desktop at large. The second keynote was about Nokia, who discussed their involvement in Qt and KDE. On Tuesday, Nokia gave away over 100 N810 internet devices to KDE developers to prove their point, and we also reported their support for <a href="http://dot.kde.org/1218543988/">the Firefox port to Qt</a> in cooperation with <a href="http://www.mozilla.org">Mozilla</a>. Suffice to say, the first day at Akademy was a great success.</p>


<p>More news came in during <a href="http://dot.kde.org/1218497374">the second day</a>, most notable the <a href="http://dot.kde.org/1218387228/">the many improvements in Qt 4.5</a>, <a href="http://dot.kde.org/1218388855/">work by the KDE-PIM hackers</a> and <a href="http://dot.kde.org/1220789755/">JOLIE bringing service-oriented computing to KDE</a>. Later on, a casual meeting of Frank Karlitschek and Fabrizio resulted in plans for <a href="http://blog.karlitschek.de/2008/08/akademy-rocks.html">co-operation</a> <a href="http://fmontesi.blogspot.com/2008/08/open-collaboration-services-have-been.html">between</a> the <a href="http://www.open-collaboration-services.org/">Open Collaboration project</a> and JOLIE. A great and certainly not unique example of how Akademy brings people with brilliant ideas together!</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-whos-weird.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-whos-weird.jpg" width="300" height="450" /></a><br />
We can always depend on Seb to find practical solutions to difficult problems (this cup was meant to hold the voting cards up...)
</div>

<p>The day ended by <a href="http://dot.kde.org/1218497374">handing out the Akademy Awards</a>. Mark Kretschmann and the Amarok team, Nuno Pinheiro and the Oxygen team, and Aaron Seigo and the Plasma developers were awarded with the official metal gear and praise and recognition from the community. Of course, we gave a standing ovation to the <a href="http://commonideas.blogspot.com/2008/08/akademy-2008-team.html">organisers of this year's Akademy</a> as well.

<h1>Moving on...</h1>
<p>Monday was set aside for the famous <a href="http://ev.kde.org">KDE e.V.</a> meeting. 7 hours of talking and (re)counting votes, who could say no to such an experience? Not many - we welcomed several new members to the e.V. and during the meeting, the previous <a href="http://dot.kde.org/1218451963/">quarterly report</a> was released. Further, it was decided to <a href="http://dot.kde.org/1218525921/">endorse the new Community Working Group, and a Code of Conduct</a>. We also voted on and <a href="http://dot.kde.org/1219405212/">accepted the Fiduciary License Agreement (FLA)</a>, which has been worked on for the last year in co-operation with the <a href="http://fsfe.org/">Free Software Foundation Europe</a>.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-out-for-beer.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-out-for-beer.jpg" width="400" height="267" /></a><br />
Having food and a beer in Mechelen.
</div>
<p>The exciting atmosphere and the Nokia N810 devices we received fuelled much of the discussion during the Emsys-sponsored <a href="http://akademy.kde.org/events/emmobile.php#maemo">Embedded and Mobile day</a>. Nokia clearly played a vital role, demonstrating their long-term commitment to Qt and KDE. Soon <a href="http://www.fredemmott.co.uk/blog_154">blog posts</a> <a href="http://blog.forwardbias.in/2008/08/n810-is-awesome.html">about</a> <a href="http://www.kdedevelopers.org/node/3628">the</a> <a href="http://www.nseries.com/products/n810/">Nokia N810</a> showed up with KDE developers talking about the potential of this device. <a href="http://www.kdedevelopers.org/node/3624">KDE packages</a> for the N810 are already available, and much work is going into porting several key KDE infrastructures like <a href="http://www.notmart.org/index.php/Software/Misc_plasmoids_on_n810">Plasma</a> or <a href="http://www.kdedevelopers.org/node/3623">Ruby</a> bindings support to it. Expect more, especially since Nokia <a href="http://akademy2008.kde.org/events/social_event.php">provided lots of free food and beer at the social event</a> on Saturday night!</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-discussing-plasma.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-discussing-plasma.jpg" width="400" height="267" /></a><br />
Careful, hot Plasma design going on!
</div>
<h2>Development and BOF Meetings</h2>
<p>On Thuesday, the <a href="http://akademy2008.kde.org/events/bof.php">BoF sessions</a> (done <a href="http://en.wikipedia.org/wiki/Unconference">"unconference"</a> style) started. The purpose of the <a href="http://en.wikipedia.org/wiki/Birds_of_a_Feather_(computing)">BoF</a> sessions is to bring developers interested in a certain subject together to talk about it informally. During the BoF sessions, several rooms centring around a certain sub-project were available: the Amarok Den, the Plasma Hackers Containment, and the Office and PIM productivity room. In each of these rooms you could find 20-odd developers working on their respective applications, using the whiteboards to develop new interface concepts or discussing the weather (bad). Furthermore, two days were reserved for a more in-depth exploration of important topics: <a href="http://akademy2008.kde.org/events/bof.php#welcome">the HCI usability day</a> and the <a href="http://akademy2008.kde.org/events/bof.php#solarisplatform">Sun tutorials day</a>.</p>


<p>We can not detail everything that happened in these rooms, but here is a quick impression of some of the results.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-good-weather.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-good-weather.jpg" width="400" height="267" /></a><br />
Outside enjoying the weather.
</div>
<p>One interesting discussion in the Plasma Containment room was about small form-factors. Aaron Seigo noted how they figured out how to solve the issue with the system tray taking up too much space - combining it with the notification widget. A big target for the Plasma developers is to ensure Plasma can just as easily be controlled with multiple fingers or thumbs as with the mouse. For this, work on a full-screen application launcher and better controls is being undertaken. Another interesting development is going on around a Qt port of Edje. Edje allows a separation between the application logic and the user interface, which is described in an easy-to-use language. Integrating this technology in Plasma seems a high priority, and the <a href="http://labs.morpheuz.eng.br/blog/21/08/2008/plasmoid-with-qedje/">first experimental Plasmoids</a> using QEdje have appeared already. It will make it easier to write Plasma interfaces, allowing people with UI design skills (but little programming knowledge) to contribute.</p>


<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-usability.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-usability.jpg" width="400" height="267" /></a><br />
Usability team having a drink.
</div>

<p>The BoF about Solaris had a demo of <a href="http://opensolaris.org/os/community/dtrace/">DTrace</a>, which not only <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/630-Some-Solaris-Notes.html">led the developers to a bug</a>, but also prompted the <a href="http://utils.kde.org/projects/okteta/">Okteta</a> developer to <a href="http://frinring.wordpress.com/2008/08/18/oktetaakademy-2008/">have a look</a> at Okteta running on other platforms like Windows and Mac OS X.</p>

<p>The focus of the session was really more about the developer tools available on the Solaris platform (and also on other platforms, because DTrace can be used on FreeBSD and Mac OSX as well) than the platform itself; some words were said about KDE 4 on Solaris, "it'll be there soon" as well.</p>

<p>Seb Ruiz, one of the Amarokers wrote <a href="http://www.sebruiz.net/343">in his blog</a> how the major work in the Amarok Den was critiquing and improving the major components in their GUI. Lydia Pintscher, the Amarok Community Manager, noted "The most important thing about Akademy in my opinion was meeting our Summer of Code students. It really helped to get to know them and make them feel they are part of the team. I hope it helped to convince them to stay with Amarok after SoC. Oh, and we really enjoyed the Akademy Awards Ceremony, obviously."</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-good-beer.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-good-beer.jpg" width="300" height="450" /></a><br />
We enjoyed good beer at the social event.
</div>

<p>
The <a href="http://akademy2008.kde.org/events/bof.php#hci">Human Computing Interface workshop</a> by <a href="http://ellen.reitmayr.net/index.php/blog">Ellen</a> and <a href="http://weblog.obso1337.org/">Celeste</a> focussed on giving developers the tools to make their applications easier to use. Ellen explained: During the Hacking Marathon, we organised a Human Computer Interaction day including various workshops to educate the KDE developers with regards to usability and design practices. This included an introduction to <a href="http://techbase.kde.org/Projects/Usability/Project_User_Research_Template">the KDE user research profiles</a> that will help developers define their project goals and focus their work on the users' needs. In a second workshop, <a href="http://weblog.obso1337.org/2008/6-research-and-design-methods-for-developers/">six usability and design methods</a> were explained to developers which they can can apply to improve the usability of their software.
<p>

<p>
Furthermore, in the scope of the <a href="http://season.openusability.org">Season of Usability</a>, we offered a student project to further develop the Human Interface Guidelines and identify common design patterns in KDE 4. Thomas Pfeiffer, one of our student interns, also attended Akademy and together, we documented several design patterns that will soon be available on techbase.</p>

<p>
Finally, we had a discussion about dialogs. Dialog alignment has been an issue in KDE for about 2 years now. During this year's Akademy, we worked together with several developers to come up with some final guidelines for dialog alignment. They will soon be documented on techbase, including some Qt Designer tips and tricks.
<p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-boat-trip.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-boat-trip.jpg" width="400" height="267" /></a><br />
Boat trip, see the videos at <a href="http://radio.kde.org/">KDE://Radio</a>.
</div>

<p>
Work and play have to go together. So we had a Nokia-sponsored <a href="http://akademy2008.kde.org/events/social_event.php">social event</a>, were we had good food and Belgian beer. And Thursday we went to Mechelen, and had a great tour over the river, paid for by our own KDE e.V. Many pictures were shot during Akademy, be sure to have a look at those made by <a href="http://www.kdedevelopers.org/node/3606">Bart Coppens</a>, <a href="http://www.kdedevelopers.org/node/3604">Jonathan Riddell</a> or in <a href="http://vizzzion.org/?id=gallery&amp;gcat=Akademy2008">Sebas' gallery (who donated the pics in this article)</a>.
</p>

<h1>Wrapping up</h1>
<p>
That wraps up the overview of Akademy. On Friday most people left, though some stayed <a href="http://www.nielsvm.org/2008/08/15/akademy-2008-public-kisses-and-flowers/">until Saturday</a>, still working. It took everybody a while to get back home (<a href="http://nowwhatthe.blogspot.com/2008/08/car-accident.html">not everybody</a> <a href="http://wadejolson.wordpress.com/2008/08/16/living-the-glamourous-life-in-a-one-star-no-tell-motel/">having a good trip</a>), and a while to adjust. Alexander Neundorf even speaks of a <a href="http://www.kdedevelopers.org/node/3622">"Post Akademy blues"</a>. Once adjusted, normal life continues. Wade <a href="http://wadejolson.wordpress.com/2008/08/26/be-careful-of-what-you-wish-for/">continues to be funny</a>. And we are still writing code.
<p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-final-article/akademy-2008-ev-members.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-final-article/wee-akademy-2008-ev-members.jpg" width="400" height="267" /></a><br />
Some new members of the KDE e.V. joined at Akademy. So can you!
</div>
<p>
But, despite the importance of <a href="http://hemswell.lincoln.ac.uk/~padams/index.php?entry=entry080831-102422">coding</a> and design going on at Akademy, it is not all what our yearly KDE meeting is all about. Talking to enthusiastic fellow KDE developers <em>is</em>. The KDE community offers a diversity of bright, interesting and simply amazing people. Meeting those, talking, having dinner or a drink - it is what makes Akademy one of the best things the year brings. There is so much more fun to be had, things to be learnt and work to be done, from art to be drawn to code to be written, you know you want to <a href="http://kde.org/getinvolved/">join us</a>!
<p>













