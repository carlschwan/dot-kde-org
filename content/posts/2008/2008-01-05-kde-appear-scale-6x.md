---
title: "KDE to Appear at SCALE 6x"
date:    2008-01-05
authors:
  - "ajohnson"
slug:    kde-appear-scale-6x
comments:
  - subject: "Curious mail account"
    date: 2008-01-05
    body: "If we have an aaronforjesus here, do the gnome people then have a samuelforsorath? ;)"
    author: "He who wonders himself..."
  - subject: "Re: Curious mail account"
    date: 2008-01-07
    body: "Only if he hath Sorath's scriptures in eight languages on his cell phone. If not, he could always substitute Ahriman's."
    author: "Aaron Johnson"
  - subject: "Re: Curious mail account"
    date: 2008-01-08
    body: "It may be a little cheesy, but I think it's quite apt \u0097 a Christian's life is defined by Jesus after all, and an e-mail address represents a person.  It's nice to see Christians in the OS community that aren't embarrassed to wave a slightly cheesy e-mail address around to leave noone in any doubt :)  Go Aaron!"
    author: "Paul Dann"
---
When most folks think of February they muse about hearts and roses. But in Southern California, free software enthusiasts look forward to the Southern California Linux Exposition. KDE will once again be represented at <a href="http://www.socallinuxexpo.org/">SCALE 6x</a> (February 8-10, 2008) showing off the newly released KDE 4.0. KDE's usability guru, <a href="http://behindkde.org/people/celeste/">Celeste Lyn Paul</a>, will also be there, giving a <a href="http://www.socallinuxexpo.org/scale6x/conference-info/speakers/Celeste-Lyn-Paul/">talk on user centred design in open source</a> on the Saturday. For those KDE experts in the Southern California area who would like to participate, there are a couple of openings for booth duty. Prospects should contact Aaron Johnson &lt;aaronforjesus<span>@gm</span>ail.com&gt; for more information.





<!--break-->
