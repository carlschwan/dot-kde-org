---
title: "Gran Canaria Desktop Summit 2009 to be Held July 3-11, 2009"
date:    2008-11-27
authors:
  - "sk\u00fcgler"
slug:    gran-canaria-desktop-summit-2009-be-held-july-3-11-2009
comments:
  - subject: "A pity for me"
    date: 2008-11-27
    body: "It's a pity for me. Those days it's the second call exams in my university, and it's very likely that I will fail at least one subject. Hopefully I could attend at least the first days."
    author: "Anon"
  - subject: "Cool!"
    date: 2008-11-27
    body: "I've jut checked and by that date I'll have done all my final exams so as it's been suggested, I'll buy the tickets these days because after Christmas people start to plan their holidays and airplane prices go higher. See you all there then =)"
    author: "Eduardo Robles Elvira"
  - subject: "Too much money, sadly"
    date: 2008-11-28
    body: "I checked some flights. $4-8K.  Ouch.  Jamaica is only $600 from here, so I'll at least be able to attend one KDE conference this year.\n\nGood luck to those in the joint summit - it's been a long time coming.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Too much money, sadly"
    date: 2008-11-28
    body: "Oh dear that sounds an awful lot. Is that because the flights are direct? If so, maybe it would be cheaper via London or Madrid. Certainly London to Gran Canaria return should be something like 300-500 USD, and you can add to that Canada/Europe return (how much is that, 500-1000 USD I would have thought?)."
    author: "Richard Dale"
  - subject: "Re: Too much money, sadly"
    date: 2008-11-28
    body: "That's with three transfers, via Northwest/KLM being the cheapest.  Other airlines were approaching 7-8K.  That's more than double my university tuition for the whole year :P"
    author: "Troy Unrau"
  - subject: "Re: Too much money, sadly"
    date: 2008-11-28
    body: "I can get Barcelona - Gran Canaria now from about 150&#8364; and Barcelona - New York about 500&#8364; so it's quite surprising you get asked 4000$"
    author: "Albert Astals Cid"
  - subject: "Re: Too much money, sadly"
    date: 2008-11-28
    body: "Someone should tech the dot the euro sign :/"
    author: "Albert Astals Cid"
  - subject: "Re: Too much money, sadly"
    date: 2008-11-28
    body: "Air Canada has flights to Tenerife for $1335 right now; which is essentially what I'd expect for a summertime ticket for Canada to Europe + 1 more hop. With taxes it's ~1643.\n\nThat's less than what I paid to get to Brussels for this year's Akademy. (Which is why we should never, ever have Akademy in August again!)\n\nYou should be able to get that cheaper directly via Air Canada, too, but since they don't fly there try via a travel agent to see if you can get a lower combined cost."
    author: "Aaron Seigo"
  - subject: "Re: Too much money, sadly"
    date: 2008-11-29
    body: "Consider to take two flight. Flights to Tenerife from Europe are cheap."
    author: "Plumplum"
---
The inaugural Desktop Summit, uniting the flagship conferences of the
GNOME and KDE communities, GUADEC and Akademy, will be held in Gran
Canaria, Canary Islands, Spain the week of July 3-11, 2009.  The conference will be hosted by Cabildo, the local government of Gran
Canaria. The GNOME and KDE communities will use this co-located event to
intensify momentum and increase collaboration between the projects. It
gives a unique opportunity for key figures to collaborate and improve
the free and open source desktop for all.

Please visit the <a href="http://www.grancanariadesktopsummit.org">official website</a> for further information.

<!--break-->
