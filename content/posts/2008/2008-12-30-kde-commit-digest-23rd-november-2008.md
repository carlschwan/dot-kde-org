---
title: "KDE Commit-Digest for 23rd November 2008"
date:    2008-12-30
authors:
  - "dallen"
slug:    kde-commit-digest-23rd-november-2008
comments:
  - subject: "little things matter a lot"
    date: 2008-12-30
    body: "* \"Items can now specify their position in the Plasma \"System Tray\". \"\n\n* \"Support for drag-and-drop re-ordering of the favourites list (and potentially other lists) in the Kickoff menu. \"\n\n* PartitionManager moves from kdereview to extragear/sysadmin\n\nAll those, and many others are small improvements that matter a lot. Thank you for this :) \n\n\nI hope that main kde apps will use new system tray features in good way ;]\n"
    author: "Koko"
  - subject: "What a pace!!"
    date: 2008-12-30
    body: "Thank you Danny for another great commit-digest! Always a pleasure to read!\nIf you keep up this pace, you'll be publishing the digest for february 8, 2009 by tomorrow... ;)"
    author: "Neil"
  - subject: "impressive"
    date: 2008-12-30
    body: "900+ bugs fixed, 277 distinct contributers. that could be the highest numbers ever in those cats. does anyone have comparitive statistics on that?"
    author: "jay"
  - subject: "Question"
    date: 2008-12-30
    body: "\"add proper support for virtual desktops, which really requires more than just the View being aware of it...\"\n\nThis to plasma. Any explanation?\n\nDerek"
    author: "dkite"
  - subject: "more little things..."
    date: 2008-12-30
    body: "\"hack sound notifications to delay calls to close by 1 minute.\nThis \"fixes\" the startup sound which got cut off by the hardcoded timeout of 6 seconds - now it's hardcoded to 1:06.\"\n\nIt was just a small glitch, but now as it is fixed, it makes my working experience quite better, definately.\n\nThanks alot!"
    author: "Burke"
  - subject: "Re: more little things..."
    date: 2008-12-31
    body: "Further, while out prosletysing, it undermined the fantastic visuals and you never get a second chance to make a first impression. \n\nExcellent"
    author: "Gerry"
---
In <a href="http://commit-digest.org/issues/2008-11-23/">this week's KDE Commit-Digest</a>: Addition of a DNS prefetch mechanism (as seen in Google Chrome) to speed up page loads in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. First base work on kolf-ng, a rewrite which may become Kolf 2.0 in the future. A crude configuration dialog is added to the "NetworkManager" <a href="http://plasma.kde.org/">Plasma</a> applet, preliminary support for horizontal and vertical form factors in the "FolderView" Plasmoid. kuiserver is now fully merged into the Plasma "System Tray". Items can now specify their position in the Plasma "System Tray". Support for drag-and-drop re-ordering of the favourites list (and potentially other lists) in the <a href="http://en.opensuse.org/Kickoff">Kickoff</a> menu. Implementation of basic text extraction from file tags in some TIFF images in <a href="http://okular.org/">Okular</a>. Start of a batch queue manager in <a href="http://www.digikam.org/">Digikam</a>. Initial work for a "set-specific preview" for the "Colours" KControl module. Configuration support for the Seeqpod script, and an initial import of a "Danish Radio Stations" service in <a href="http://amarok.kde.org/">Amarok</a> 2. New "IceWorld" theme in KBreakOut. Support for enabling/disabling "Rich Text" in the <a href="http://kopete.kde.org/">Kopete</a> chat window. Rudimentary integration with ConsoleKit in KDM. Eigen 1 has now been replaced by <a href="http://eigen.tuxfamily.org/">Eigen</a> 2 across KDE SVN. Guidance removes own implementations of utilities now covered by KDE 4.2 tools. PartitionManager moves from kdereview to extragear/sysadmin. The Falcon-language <a href="http://kross.dipe.org/">Kross</a> backend moves to kdebindings in time for KDE 4.2. "Bomber" game moved from kdereview to kdegames for the KDE 4.2 release. The game "KGo" is renamed "Kigo". <a href="http://edu.kde.org/kpercentage/">KPercentage</a> retired as it is unmaintained, with some exercises incorporated into <a href="http://edu.kde.org/kbruch/">KBruch</a>. Amarok 2 Release Candidate 1 (1.98) is tagged for release.<a href="http://commit-digest.org/issues/2008-11-23/">Read the rest of the Digest here</a>.
<!--break-->
