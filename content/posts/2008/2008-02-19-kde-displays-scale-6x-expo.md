---
title: "KDE Displays at SCALE 6x Expo"
date:    2008-02-19
authors:
  - "ajohnson"
slug:    kde-displays-scale-6x-expo
comments:
  - subject: "appreciation"
    date: 2008-02-19
    body: "thank you for promoting KDE at SCALE. the more users the more contributors. and we all win :-)"
    author: "rustahm"
  - subject: "I'm the first poster!"
    date: 2008-02-19
    body: "Why not show the latest build from SVN as well?"
    author: "yman"
  - subject: "Re: I'm the first poster!"
    date: 2008-02-19
    body: "Nuts..."
    author: "yman"
  - subject: "Computers"
    date: 2008-02-19
    body: "Thank you all guys for your work.\nRegarding the computer... why such an old machine to display KDE 4? It's kind of weird that there was no chance of getting a better one..."
    author: "Santa Claus"
  - subject: "Re: Computers"
    date: 2008-02-19
    body: ">> Regarding the computer... why such an old machine to display KDE 4? It's kind of weird that there was no chance of getting a better one...\n\nOTOH, I think it speaks well to the fact that KDE can run on such a limited machine. When you consider that the consumer trend seems to be moving towards ultra low-cost laptops and devices where resources are at more of a premium, maybe it will help cut down on the \"KDE is too bloated!!! Use Xfce or Flux if you have less than 1GB RAM\" troll posts that appear every now and then on various forums."
    author: "elsewhere"
  - subject: "Re: Computers"
    date: 2008-02-20
    body: "I think that it certainly does show KDE in good light to be able to run on low spec hardware like this.\nOut of interest (as I have an older laptop, of a similar spec) what distro was being used on that machine?\n"
    author: "bonybrown"
  - subject: "Re: Computers"
    date: 2008-02-20
    body: "Debian Unstable with X.org from Testing and KDE 4.0 from Experimental."
    author: "Aaron Johnson"
---
To Southern Californians February means several things. Winter storms bring snow to the San Gabriel Mountains. Most college students will have returned to school for another semester. Early flowering plants have started to bud and bloom, attracting bees. Most important of all however, is the coming of SCALE, the annual Southern California Linux Expo.





<!--break-->
<div style="width: 250px; border: thin solid grey; margin: 1ex; padding: 1ex; float: right">
<a href="http://static.kdenews.org/jr/scale-6x-aaron-celeste-mike.jpg"><img src="http://static.kdenews.org/jr/scale-6x-aaron-celeste-mike-wee.jpg" alt="scale photo" width="250" height="220" /></a><br />
Aaron Johnson, Celeste Lyn Paul, and Mike Elliott representing KDE at SCALE 6x
</div>

<p>From February 8th to the 10th, Linux enthusiasts from the greater Los Angeles area (and beyond that!) converged at the Westin Hotel near Los Angeles Airport to celebrate Linux and Free Software. KDE was once again there showing attendees the best Free Software desktop. Starting things off, one of KDE's usability helpers Celeste Paul gave a talk on <a href="http://www.socallinuxexpo.org/scale6x/conference-info/speakers/Celeste-Lyn-Paul/">A Quick and Dirty Intro to User ­Centred Design in Open Source Development</a>. </p>

<p>In the mean while, two KDE desktops were demonstrated at the KDE booth. The fully featured, reliable and well supported KDE 3.5 was shown to many of those stopping by the stall.  What most folk were interested in, though, was the newly released KDE 4.0. Despite being run on a Via C3 laptop with only 256 MB of RAM (minus 32MB for the onboard video chip), so no KWin effects, many people were impressed. After being impressed by the features of KDE 4, many said that they would try it out after the release of KDE 4.1 (which we recommended as the one for average users, rather than early adopters). Others were helped in getting CDs to try out the current KDE 3.5. Just like the past several years, everyone had a great time, and plans are already being made for KDE to return next year.</p>




