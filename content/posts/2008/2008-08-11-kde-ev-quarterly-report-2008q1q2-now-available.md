---
title: "KDE e.V. Quarterly Report 2008Q1/Q2 Now Available"
date:    2008-08-11
authors:
  - "dallen"
slug:    kde-ev-quarterly-report-2008q1q2-now-available
comments:
  - subject: "IS KDE moving to Berlin?"
    date: 2008-08-11
    body: "I've read that the german wikipedia is going to move from Frankfurt to Berlin. Is KDE also going to Berlin? I just ask, because KDE and the german wikipedia are sharing an office in Frankfurt."
    author: "Isabell"
  - subject: "Re: IS KDE moving to Berlin?"
    date: 2008-08-11
    body: "We (KDE e.V. board) are aware of this and are discussing the issue."
    author: "Adriaan de Groot"
  - subject: "Wrong pdf title?"
    date: 2008-08-11
    body: "Looks like the title of the linked PDF is totally wrong - especially annoying as Okular only shows this title."
    author: "Eckhart"
---
The <a href="http://ev.kde.org/">KDE e.V.</a> Quarterly Report is <a href="http://ev.kde.org/reports/ev-quarterly-2008Q1-Q2.pdf">now available for Q1 and Q2 2008</a>, covering January to March, and April to August 2008. This document includes reports of the board and the working groups about the KDE e.V. activities of the first two quarters of 2008, as well as event summaries and future plans. All long-term KDE contributors are welcome to <a href="http://ev.kde.org/getinvolved/members.php">join the KDE e.V.</a>
<!--break-->
