---
title: "KDE 4.0.3 Released With Extragear Applications"
date:    2008-04-02
authors:
  - "skuegler"
slug:    kde-403-released-extragear-applications
comments:
  - subject: "Let's spread the great news"
    date: 2008-04-02
    body: "Reddit: http://reddit.com/info/6ebus/comments/\nDigg: http://digg.com/linux_unix/KDE_4_0_3_Released_Brings_Lots_of_Improvements"
    author: "Tsiolkovsky"
  - subject: "Re: Let's spread the great news"
    date: 2008-04-02
    body: "Free Software Daily: http://www.fsdaily.com/EndUser/KDE_4_0_3_Released"
    author: "Matt Williams"
  - subject: "only bug fixes for plasma ?"
    date: 2008-04-02
    body: "Now it makes sense why opensuse will release their \"special\" kde 4.0.x branch,\n\nby the way, the last kde 4.0.67 for windows rocks, happy to read pdf by okular. Thanks pino ;-)"
    author: "mimoune djouallah"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "AFAIK, there are also some new features, but as they didn't make it into the changelog, they didn't make it into the announcement. That's why I wrote \"not complete\" about it. Trying to get everybody to fill the changelog is quite a challenge. *hint*"
    author: "Sebastian Kuegler"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "As in my experience the openSUSE 4.0.x releases had at least as much bugs as the supposedely \"unstable\" 4.1pre Packages, I happily switched to the more feature-rich 4.1 and never looked back to 4.0.x. If you get bugs as well with 4.0 as 4.1, you might as well learn to cope with 4.1's bugs ;).\n\nWhile this is basically a good sign for the upcoming KDE4.1, it nevertheless leaves one with a dull taste concerning the release policy which lead to 4.0. Here something went wrong IMHO."
    author: "Anon"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "Well, actually my experience has been quite different. While the 4.0.x releases are stable I've already had lots of problems with the 4.1 svn version - both using opensuse 10.3. \n\nPlasma crashing on every startup, lots of apps not even loading - I'm glad I haven't seen that in the 4.0.x versions. :) Anyway, I agree that there are some nifty features waiting for us in current svn. Looking forward to it!"
    author: "WPosche"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "\"Well, actually my experience has been quite different. While the 4.0.x releases are stable I've already had lots of problems with the 4.1 svn version - both using opensuse 10.3\"\n\nperhaps you are mixing the two repositories stable and unstable, remove all kde 4.0.x and delete .kde4 , and just install the last kde 4.0.67, you will be surprised. \n\n\nfriendly"
    author: "mimoune djouallah"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "I used to have the stable packages on that machine, then I upgraded to the unstable packages and had these problems. Just deleted .kde4 and checked that there were no more old packages installed. Still some apps don't work (games, amarok). Still nice to see the default settings again in KDE4."
    author: "WPosche"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "Hi\nProbably there were something wrong from your side because games were/are in a very nice shape.they were never broken."
    author: "Emil Sedgh"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "> Plasma crashing on every startup,\n\nwith BIC changes to libplasma happening for 4.1, you have to be really careful to keep your plugins in step with libplasma. that's been the only known source of start up crashes to date. so this is an installation issue, not something in our code."
    author: "Aaron Seigo"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "Absolutely, please can you explain to me, why a distribution can't ship kde 4.1 alpha if it is quite stable, because right now  (imho) kde trunk is more stable and has more feature (of course) then branch.\n\nStrange days, the unstable branch is more stable then the stable branch. \n\n"
    author: "mimoune djouallah"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: ">Strange days, the unstable branch is more stable then the stable branch. \n\nThis one made my day, and the funny part is that it's probably true :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "openSUSE does. You can choose between stable and development version of KDE4.\n\nHowever, I had a problem with the dev version: it needs Qt 4.4 beta and LyX didn't work with that."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "I guess you don't know what \"Alpha\" state means in software development."
    author: "Anonymous"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "I agree. It feels like 4.0.x is a waste of time both for developer and users.\n\nThey should keep it as close as possible to KDE 4.1, so stuff get tested and we get a quality release in summer. There is no point in developers wasting time to maintain 4.0.x and backport stuff to it. Nobody uses 4.0 in a production environment and it's not a real stable release. As you say, if users can cope with 4.0, they'll cope with 4.1 alpha and beta."
    author: "Anon"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "The first time I read your post, it seemed kinda harsh.\n\nBut the more I think about it, the more truth I see: Nobody wanting to use a stable desktop will use KDE 4.0.x - in this case KDE 3.5.9 is the way to go. KDE 4.0.x is a beta version (there's no flaming here, as that's more or less the official line, can be read in various blogs of developpers). \n\nSo basically, we're talking about a work-in-progress, and maintaining a KDE 4.0.x besides developping KDE4 seems as sensible as maintaining an early beta version of a program."
    author: "Anon2"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "Oops, something lost here:\nMaintaining KDE 4.0.x besides developping KDE 4.1 seems as sensible as maintaining an early beta version parallel to a newer beta version."
    author: "Anon2"
  - subject: "How many ?"
    date: 2008-04-02
    body: "Anons do we have? We have Anon (original afaik) then we have anon and now Anon2. What's next? Anon reloaded?"
    author: "Bobby"
  - subject: "Re: How many ?"
    date: 2008-04-02
    body: "We are Legion."
    author: "Anon Reloaded"
  - subject: "Re: How many ?"
    date: 2008-04-02
    body: "Just want to say: LOL"
    author: "fred"
  - subject: "Re: How many ?"
    date: 2008-04-02
    body: "How did you know?"
    author: "Anon reloaded"
  - subject: "Re: How many ?"
    date: 2008-04-03
    body: "We always know everything since we control this Matrix."
    author: "Anon Revolutions"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "Well, your mileage may vary... I've been using KDE4 nonstop since 4.0.1 (4.0.0 was too unstable for me). Apart from the obvious defects, 4.0.2 is quite stable. My biggest annoyances are krunner (after executing successfully a command it complains it couldn't do it) and kwin (way too slow in composite mode)."
    author: "NabLa"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "I see on 4.0.3's changelog that some effort has been made to optimise kwin on nvidia, let's see how it goes :)"
    author: "NabLa"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "Seems smoother here on my GeForce 4 MX 440. (It wasn't half bad before either)"
    author: "Jonathan Thomas"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "Not smooth at all on my Geforce 6600GT. Drives me nuts, have to use compiz."
    author: "MarcG"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "Since the MX 440 is much older, slower and have less memory,\nit's another driver you have to use."
    author: "reihal"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "Still slow for me, no improvement :? I have a similar card with latest nvidia drivers... "
    author: "NabLa"
  - subject: "NVIDIA fixes?"
    date: 2008-04-03
    body: "I read your post and decided to install 4.0.3\n\nSurprising result when measuring frame rate on 8400GT using glxgears:\n\nkde 4.0.2: 8300 frames in 5.0 seconds\nkde 4.0.3: 4617 frames in 5.0 seconds\n\nhalf the performance.\n\nI have to file a bug report on that..."
    author: "Sebastian"
  - subject: "Re: NVIDIA fixes?"
    date: 2008-04-03
    body: "No, even less. The first reduction appeared after installing 4.0.3, killing kwin4.0.2  and starting kwin 4.0.3.\n\nAfter restarting the session I am at 1500 frames per 5.0 seconds... \n\nwow! this is as fast as my old ATI wothout 3d support! May somebody explain this? Shall I see this as a sign (I will exeggerate) that kwin is making the gpu busy or is it a good sign (in terms that kwin now uses the gpu at all)?"
    author: "Sebastian"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "> krunner (after executing successfully a command it complains it couldn't do it)\n\nfor the record, this bug is not in krunner but elsewhere =)"
    author: "Aaron Seigo"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-04
    body: "The debian KDE packagers agree with you.  They are skipping 4.0.X>2 entirely and packaging KDE trunk builds until 4.1 is released.\n\nhttp://www.mail-archive.com/debian-chinese-gb@lists.debian.org/msg16211.html"
    author: "Leo S"
  - subject: "debian is snobing kde 4.0.x"
    date: 2008-04-04
    body: "i think (imho) as a user, it is the right decision, i am really concerned about the reaction of users after using kde 4.0.x specially plasma (btw the most important piece in kde), let's wait and see their reaction to fedora 9, ubuntu 8.04-kde4 and in less extend opensuse 11 ( i really hope they postponed their release date and ship kde 4.1 bata ) \n\n\nit is always hard to forget the first impression ;)\n\n"
    author: "mimoune djouallah"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-06
    body: "I tought the contrary.\nBut as most bugfixes are NOT being ported to 4.0 series, and plasma just crashes everyday for me (on logout and sometimes on login), I just want 4.1 stable and with more features. Having to maintain 4.0 series does not help to improve 4.1, so please, kde tem, do not work on 4.0.4-beta, we want 4.1-stable!"
    author: "Iuri Fiedoruk"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-06
    body: "> please, kde tem, do not work on 4.0.4-beta\n\nI disagree, please do backport bugfixes! I really don't want to have to backport dozens of bugfixes from 4.1 all alone for Fedora 9's KDE 4.0.4 update which is going to come a few days after the release. Please keep users of the stable branch in mind and backport bugfixes to the stable branch whenever possible. With that logic, i.e. if all the bugfixes go to the unstable trunk only, we'll never have a stable release.\n\nThis:\n> most bugfixes are NOT being ported to 4.0 series\nif it's true (I don't have any metrics to decide either way), is the real problem and should really not happen. But dropping 4.0 entirely is not the solution, actually backporting bugfixes is!"
    author: "Kevin Kofler"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "The question is how trunk get more stable ?\nBy all users testing and using what is kde 4.0.\nSo the state of kde 4.1 is so good because some use kde 4.0."
    author: "phD student that should stop reading the dot"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "and the answer is: i think developers are users too :)\n"
    author: "Emil Sedgh"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "it's a numbers thing: both the number of users as well as the number of different ways the system is used. developers involved with a system tend to use the software in repeated, predictable patterns. introduce new users (even if they are also developers, but of some other software) and issues start to arise pretty naturally / obviously.\n\nthat said, i'm happy to see the 4.0.x releases rolling. it's a good practice and the improvements are welcome by many.\n\ncounterballancing that, yes, i'm very much of the \"concentrate on 4.1 as much as possible (but no more)\" opinion as well."
    author: "Aaron Seigo"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-03
    body: "That is because 4.0 is in essence a Beta release. And as a Beta release it is, it doesn't make that much sense for it to have bugfix releases, the best way to make it stable is to continue development towards a Release Candidate state, which is being done in the 4.1 branch. \n\nIf you look at Plasma in 4.0 for, which is central to the end user experience, it is far from done, that's why it is allowed to introduce new features in supposedly bugfix releases. Libplasma won't even remain binary compatible until 4.1. All this are characteristics of Beta releases. \n\nOf course 4.1 is more prone to break than 4.0 but, since so many improvements have been done over that old Beta state, it's natural that overall it is better quality. \n\nI know this is beating a dead horse. In any case it is the natural answer for these kind of comments. From the moment that you keep in mind that 4.0 is Beta, lots of strange things suddenly make sense. "
    author: "ad"
  - subject: "Re: only bug fixes for plasma ?"
    date: 2008-04-02
    body: "I noticed that now there is a divider line on the right click menus between the containment configuration and applet configuration buttons."
    author: "Jonathan Thomas"
  - subject: "Impressive Changelog"
    date: 2008-04-02
    body: "Very impressive changelog. Seeing forward to using 4.0.3 in a couple of days!\n\nBTW, anybody in the know whether the annoying https problem (\"SSL negotiation failed\") is fixed?\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "That bug is Qt's fault, not KDE's.\n\nSee these for the gory details:\nhttp://bugs.kde.org/show_bug.cgi?id=155564\nhttps://bugzilla.redhat.com/show_bug.cgi?id=432271\nhttp://cvs.fedoraproject.org/viewcvs/rpms/qt/devel/qt-x11-opensource-src-4.3.4-openssl.patch?rev=1.1&view=markup\nAlso be warned that SHLIB_VERSION_NUMBER as used in this patch doesn't necessarily work on your distribution. (It's broken at least in Fedora 7 and 8, we got that fixed in Fedora 9.)"
    author: "Kevin Kofler"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "Uh well, so I have to wait for 4.1 (with Qt 4.4) and use Firefox for https sites meanwhile. Annoying but certainly manageable. ;-)\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "Well, if the bug report is correct, you can add a symlink to get openssl found...\n"
    author: "SadEagle"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "Or install the openssl-devel package or whatever your distro calls it, which contains that symlink."
    author: "Kevin Kofler"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "Well, if you're building from source, you can patch your Qt, if not, try getting your distro to fix it.\nI hope Trolltech will fix it for 4.4, I'm not sure whether they know about it yet though (we just figured out what was wrong a few hours ago, it was filed as KDE bugs in Konqueror and Kopete and not recognized as a Qt bug)."
    author: "Kevin Kofler"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "Already fixed about a month and a half ago."
    author: "Thiago Macieira"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-03
    body: "To be fair, a few sites only work with a specific version of the SSL protocol and it was KDE that needed to be fixed. The fix is to try SSL versions in turn (this means 3.1 and then 3.0). I dont't know if the fix has made it into 4.0.x but it is in trunk which will become 4.1.x."
    author: "Andreas"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "Sad to say no it has not been fixed."
    author: "taurnil"
  - subject: "Re: Impressive Changelog"
    date: 2008-04-02
    body: "\"Seeing forward to using 4.0.3 in a couple of days!\"\n\nI think you mean 'Looking forward...' :) Made me smile anyway... I was once asked to write an 10,000 word essay on the difference between 'looking and seeing'. If I can spend that long discussing it I think I'll allow you the confusion... ;)\n\nI not entirely sure but I think in this context 'looking' is active but 'seeing' would be considered passive."
    author: "Martin Fitzpatrick"
  - subject: "Gopher"
    date: 2008-04-02
    body: "Does anyone use Gopher or is kio_gopher more like some kind of nerdfun? ;-)"
    author: "question"
  - subject: "Re: Gopher"
    date: 2008-04-02
    body: "There are a few gopher enthusiasts still out there that painfully refuse to let the protocol die; but the same could be said about any old protocols... anyone remember FIDO-net?  It's still (barely) alive these days too! :P\n\nEarly in its history, KDE had gopher support, but even then the protocol was mostly dead.  The protocol is simple though, and easy for open source hackers to have some fun writing kio plugins :) Open source is half motivated by having fun anyway :)"
    author: "Troy Unrau"
  - subject: "Nice"
    date: 2008-04-02
    body: "I was actually expecting more bugfixes (bugs missing is what NOT happening), but I'm really happy the blank desktop and double click where fixed. This together with the quick-launch plasmoid from kde-look makes KDE4 usable to me (but still far from kde3.x).\nNow if I could get a stable port of k3b, amarok and kmplayer..."
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice"
    date: 2008-04-02
    body: "As said in the article the changelog is not complete."
    author: "Narishma"
  - subject: "Re: Nice"
    date: 2008-04-02
    body: "The full automatic changelogs are linked from the manual crafted one btw..."
    author: "Anonymous"
  - subject: "Re: Nice"
    date: 2008-04-03
    body: "No, that's not a full, automatic changelog. It's done by hand, and as that, it's not complete."
    author: "sebas"
  - subject: "Webkit?"
    date: 2008-04-02
    body: "I thought KDE was going to switch to Webkit for Konq? Or was that only for plasmoids? "
    author: "Jeremy"
  - subject: "Re: Webkit?"
    date: 2008-04-02
    body: "Right now, the KPart that uses webkit for Konqueror is not ready for consumption. It's somewhere in playground/. Until that one is ready, KHTML will stay Konqueror's default engine for sure. We'll see what happens afterwards, in the end the user will be able to decide.\n\nWebkit is used in Plasma, though, for example for OS X Dashboard widget support."
    author: "sebas"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "And just to clarify sebas's comment, KDE won't have WebKit at all until KDE 4.1, when it starts to depend on the (still unreleased) Qt 4.4."
    author: "Ian Monroe"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: ">in the end the user will be able to decide\n\nOnce I have a webkit part, bye-bye firefox (yeah, because right now khtml does match my needs)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "You were misled\n"
    author: "SadEagle"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "You know, I really admire you KHTML guys to keep chugging on despite the ignorance and negativity.\n\nAnd for giving us such a wonderful HTML engine cum working complete kpart."
    author: "hmmm"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "Yeap, thanks for your work and your patience! :)"
    author: "ad"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "\"I thought KDE was going to switch to Webkit for Konq? Or was that only for plasmoids?\"\n\nThat all depends on what engine the vast majority of developers want to use, and what applications that vast majority of users want to use. That's the direction things will naturally head in, and the bottom line is if WebKit provides better day-to-day web browsing than KHTML, with bug-for-bug compatibility with Safari being useful, then that's what people will use. Alas, web developers are not going to change and are not going to test with KHTML - just as they have never done."
    author: "Segedunum"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: ">  bug-for-bug compatibility with Safari\n\nQtWebKit is not WebKit and does not provide Safari compatibility. Saying such a thing is not even a joke, it is a fraud.\n\nIn order to build a webkit backend, developers have to fill hundreds of undocumented stubs with a non-trivial meaning, especially for people who are very new to the internals.\nEach one of this stub is an occasion for a new bug, or for a missing feature when left unimplemented.\n\nSo on the surface, you have a working engine, as regression tests allow you to check that you match the very basics - but at every corner things will fail, because of poorly understood limit conditions and undocumented requirements, or because of vastly different under-pinnings, different network layer exhibiting undiscovered race-conditions in CSS loading, whatever.\n\nHell, I can already single out QtWebKit with a goddam *CSS* statement because of such bugs. That's just ridiculous.\n\nThe only people able to build a fully working backend with WebKit, are the Apple developers, that have full knowledge of the code base - it will always be so, and that's exactly how Apple likes it:\nFree advertisement, mind share, and competitors under tight dependance.\n\nBut anyway, you just have to try the demo browser for 5 minutes to realize that : a web browser is much, much more than just a rendering engine.\n\n\n"
    author: "S."
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "\"QtWebKit is not WebKit and does not provide Safari compatibility. Saying such a thing is not even a joke, it is a fraud.\"\n\nI know certain people like to go around perpetuating this impression for their own ends, but this is false. WebKit, and henceforth QtWebKit, opens the door very much for bug-for-bug Safari compatibility and between other WebKit browsers to create a bigger target for web developers. This counts for a lot, because no web developer is going to change their ways and start testing with KHTML this side of the next ice age. As you say elsewhere, there are corner cases to every web rendering engine, and that's why KDE is still devoid of a native browser people can count on.\n\n\"In order to build a webkit backend, developers have to fill hundreds of undocumented stubs with a non-trivial meaning, especially for people who are very new to the internals.\nEach one of this stub is an occasion for a new bug.......\"\n\nBlah, blah, blah, blah, it's too difficult. We get the picture. Taking the above, why do you think most KDE users use Firefox rather than Konqueror?\n\n\"The only people able to build a fully working backend with WebKit, are the Apple developers, that have full knowledge of the code base - it will always be so, and that's exactly how Apple likes it\"\n\nThe source code for WebKit is there. It's taken a while to get there, but it is there, Trolltech are using it in Qt and devoting lots of resources to it, as are Nokia, Apple of course and lots of others. Economies of scale are always better.\n\nRepeating the above is not going to make this true, but my main point still stands - if a QtWebKit based browser renders the vast majority of web sites people visit better then KHTML, people will use the WebKit based browser. That's the way it is."
    author: "Segedunum"
  - subject: "Re: Webkit?"
    date: 2008-04-03
    body: "KHTML or WebKit does it really matter? Most web developers seem to test for browser not engine. And that would still be Konqueror.\n\nFor the record, in my browsing I have minimal need for other browsers. The few sites I wistit where Konqueror/KHTML is not up to the task, usually works when changing browser identification. Not that I have any strong feelings either way when it comes to WebKit, but I doubt it will revolutionize my browsing experience. "
    author: "Morty"
  - subject: "Re: Webkit?"
    date: 2008-04-04
    body: "\"KHTML or WebKit does it really matter?\"\n\nIt does because if we use QtWebKit, (even if it is not absolutely compatible with the Safari WebKit) the websites tested in Safary will be likely to work in Konqueror as well, and Safari has much bigger market share than Konqueror.\n\nI think Konqueror should be rewritten (in the same powerful but more flexible way) because developers often say that Konqueror code is such a mess that adding even small features is painful. If KHTML improves (as it continually does) and a decent web browser based on it is written that runs on Windows and Mac OS X as well, it could enter the browser war and gain enough market share to be noticed by website developers."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Webkit?"
    date: 2008-04-05
    body: "\"the websites tested in Safary will be likely to work in Konqueror as well\"\n\nThat may be, but the waste majority of sites I see that Konqueror has serious problems works when changing user agent. It's not the testing by web developers and the rendering of KHTML that is the issue, so changing to WebKit will not change this. "
    author: "Morty"
  - subject: "still no proxy support"
    date: 2008-04-03
    body: "nothing to see here, moving along"
    author: "Dmitriy Kropivnitskiy"
  - subject: "Re: still no proxy support"
    date: 2008-04-03
    body: "yeah that's very bad, a lot of people still use proxy to access the net, in my country nearly all cybercaf\u00e9 and companies network use proxy to share net acces, unfortunately many valuable kde applications can't be used due to this limitation.\n\n"
    author: "mimoune djouallah"
  - subject: "Re: still no proxy support"
    date: 2008-04-04
    body: "Odd, I thought that was what setting proxy settings under systemsettings -> Network Settings were for.\n\n"
    author: "m_abs"
  - subject: "Re: still no proxy support"
    date: 2008-04-04
    body: "\"systemsettings -> Network Settings were for\" eh sorry, i forget to mention  i was speaking about kde apps under windows,  "
    author: "mimoune djouallah"
  - subject: "Using KDE4.0.3 Now..."
    date: 2008-04-03
    body: "Just upgraded using the Kubuntu packages and I must say I'm quite impressed. There are rough edges here and there (obviously) like some bizarre funkiness when logging out and then back in (all KDE3 apps create windows instead of tray icons, and Kwallet, IMAP, etc. processes are \"not found\") but to be honest it's completely useable.\n\nSo... question is... is it worth filing bug reports for 4.0.3? Or is this stuff getting fixed elsewhere (i.e. should I be running from the latest branch to help with bugfixing?)\n\nAnyway, ta very much like. Good job folks."
    author: "Martin Fitzpatrick"
  - subject: "Re: Using KDE4.0.3 Now..."
    date: 2008-04-03
    body: "yes, you can and should report bugs. After all, next month we'll release the 4.0.4 bugfix release, and the more bugs are fixed, the better it'll be ;-)"
    author: "jospoortvliet"
  - subject: "Re: Using KDE4.0.3 Now..."
    date: 2008-04-03
    body: "Thanks :) Just wanted to check... Things are moving along so quickly I was concerned any bugs would be behind the current status. \n\nI'll get on with it then.... :)"
    author: "Martin Fitzpatrick"
  - subject: "Re: Using KDE4.0.3 Now..."
    date: 2008-04-03
    body: "It would be useful if upstream developers at least read/answer the bugs. I've opened since 4.0.1 a bug for Kopete not working correctly with my Jabber server (while Kopete KDE3 works flawlessy) and I've still got no answer. I don't want the bug fixed, but at least someone telling me \"we care 'bout your bug reports\"."
    author: "Vide"
  - subject: "New Compositioning effects yet?"
    date: 2008-04-03
    body: "I'm still downloading, but very impatient...\n\nWere any new compiz effects added since 4.0.2?\n\n\n(yes, yes, I know. They're not essential, but I do enjoy looking forward to new eye candy with each release. It makes computing more fun :D )\n"
    author: "Max"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-03
    body: "i wouldn't think so.  4.0.x releases are bugfix releases only.  new features likely introduce new bugs.  plasma is the only kde 'project' that i'm aware that was allowed to introduce new features with a bugfix release."
    author: "tooth"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-03
    body: "Makes sense.\n\nStill. :(\n\nI guess I'll have to wait a few more months until KDE 4.1.\nHopefully there will be lots of effects by then.\n\nMaybe \"feature parity\" with compiz fusion. *dreaming* I know..."
    author: "Max"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-03
    body: "You can install KDE 4 from trunk, that gets you some new and enhanced effects, such as the CoverSwitch [alt][tab] switcher. Instructions how you do that are to be found on techbase."
    author: "sebas"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-03
    body: "I see a lot of this \"feature parity\" with compiz fusion about, but nobody ever gives any indication of wich particular feture they are missing in KWin. You can not expect the KWin devlopers or others to hunt trough Compiz fetures to find things to implement. \n\nAnd Kwin even have original effects, and they may even be better solutions than the effects Compiz uses. In that case perhaps one should drream of the day Compiz fusion reaches \"feature parity\" with KWin."
    author: "Morty"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-03
    body: "Probably the most visible and common feature that still seems to be missing is the rotate cube functionality for switching desktops.  Its flashy and provides clear visual feedback for desktop switching.  And it always surprises shoulder surfers who want to know what cool software you're using on your laptop."
    author: "topcat"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-03
    body: "To be honnest I can't see what all the fuzz is about, do people actually use that? Or is it just a show-off toy, turned off when real work should be done? And it's not soo cool either, afterall I have seen much the same effect as browser plugin when switching tabs. When your browser does it, it kind of looses some cool :-)\n\nIMHO, KWin's Desktop Grid gives a clearer visual feedback for desktop switching. That you see all desktops at once gives a better overview, and the ability to move applications from one desktop to another are all parts that gives it much better usability than the rotating cube.\n\nBut it's true KWin does not seem to have the cube yet, but that's only one feature. If that's the only thing missing, the \"feature parity\" should not be such a big issue. "
    author: "Morty"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-04
    body: "It was only after playing with compiz that I realised what a good window manager kwin is, while compiz has the bling bling looks after a while its poor window management got on my nerves. \n\nI do miss the wobbly windows though!"
    author: "dr"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-04
    body: "Wobbly windows is beeing worked on, but I'm not sure of the current status regarding release.\n\nhttp://lists.kde.org/?l=kwin&m=120560666505903&w=2"
    author: "Morty"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-06
    body: "Poor window management, and the fact that most of its effects are pretty much useless. Well, useless as in not being of any practical use that is. Great for showing of or playing with...\n\nI much prefer the more sensible effects available in KWin (well, okay. The explode effect may not be all that sensible...). \n\nStill, there are a few Compiz things I would love to have in KWin:\n\n*The wobbly windows. Don't know why I like them...\n*A better minimize-effect. IIRC, Compiz calls it the magic lamp or something like that (or was it MacOS X that called it that? They use the same effect for the dock at least). It provides a much better visualization when minimizing windows.\n*A win-deco that can import and use Emerald themes.\n\nOkay, kinda silly things to miss maybe..."
    author: "Jonas"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-07
    body: "\"Feature parity\" means that both have \"the same\" features.\nIt does not include a meaning of which one is better or which one had the features first.\n\nJust wanted to clarify that.\n\nI also think the original poster meant that to be a noble goal, not a strict requirement."
    author: "Mike"
  - subject: "Re: New Compositioning effects yet?"
    date: 2008-04-07
    body: "Here are some of the effects I'm missing:\n(this list includes effects currently under development, or already done.)\n\nDesktop Cube (both normal and inverse) Win Fish/KDE gears \"swimming\" in it. \n(please add \"wet floor\" and fish swimming outside of the cube. As if the Cube were a fishtank, but strangely the fish swim on the outside. Maybe make some of the fish 3d Penguin \"Tux's\". I think that would be cool.)\n\nA Compiz effect screensaver/\"relaxation effect\" for when people are \"brainstorming\" and would love their desktop cube interact with virtual environments. (i.e.: Snowy hill -rolling down, or stuck on the side, floating in the ocean - like a message in a bittle, in a tropical setting - small lonely island with a lonely palmtree. Desktop is perched against the tree.)\n\nA \"paper plane\"/origami effect for minimizing applications or desktops.\n\nAn effect similar to \"magic lamp\" on minimizing applications.\n\nA \"wet floor\" effect - Have fun with it, surprise us.\n\nExpose - better than desktop cube, it allows all desktops to be seen side by side on a nice glossy surface. (aka.: \"wet floor\")\n\nEffects such as \"Fire\" for when programmers/writers are frustrated and need to get rid of some frustrations. \n\nMaybe another effect like the above where I can throw Toasters, red swingline staplers, Mice, calculators, notepads, sharpies, and lots of other typical \"office equipment\" at the desktop, which then has dents in it. -- That effect needs to clear easily and quickly, should the muse strike again and I'm ready to do work again.\n\nA \"shuffle\" effect, where windows bump each other out of the way, when moved violently or brought to foreground.\n\nA compositioning effect that reacts to the music that's playing using KDE's Amarok/Phonom engine. (could also be a screensaver) Make it do something cool with the desktop.\n\nEffects that entertain the user - when he asks for them to be loaded. (DON'T BE \"CLIPPY\" and show up without asking)\n\nA random effects generator. - for people who can't decide.\n\nA way for emerald window decorations to work - or a similar easy to use engine for getting additional window decorations, mouse effects, etc.\n\n\nI realize many of these effects are less useful and more \"fun\", but that's what linux and desktop computing should be: FUN!!!!!!\nLinux/KDE computing should be more fun than the gray on gray computing world that we have to deal with Windows.\n\nOthers please add effects you would like to see. Describe them as best you can, so creative 3d animators/3d effects programmers can incorporate them.\n\nAnd yes.. the \"over the shoulder look\" when using a Compiz enabled laptop is VERY, VERY necessary and important. I've recruited at least 5 people in the last three months to the world of Linux and some to KDE because of \"DESKTOP - EYECANDY\".\n\nPeople like to be visually stimulated. Why do you think french food is prepared in such a nice way. The eye enjoys the meal too, so to speak.\nDon't be wasteful with screen real estate either. Notice how big plates are when serving french food, and how little space is used by the actual food, leaving tons of room for \"whitespace\"?"
    author: "Max . "
  - subject: "Plasma Updates"
    date: 2008-04-03
    body: "Correct me if I'm wrong, but the idea of Plasma wasn't having a (stable) API where developers could work into, and that would make possible to release independent updates of plasma desktop and plasmoids, as those whould be only script not needing compilation?\n\nI remember Aaron talking about something like that (or I am terrible mistake and sorry) and was curious if Plasma (not the lib part). It would be much batter for users indeed, but nowdays I only see C++ plasmoids.\nIs there a plan to make plasmoids stop using C++ (and stoping crashing my whole desktop) and plasma API stop receiving changes so that KDE 4.1 plasmoids work on KDE 4.2 and vice-versa?\n\nTHAT would be the true nirvana for plasma and users. A nice goal to plasma project, in my opinion, making plasmoids more than simple widgets in the desktop and panel."
    author: "Iuri Fiedoruk"
  - subject: "Re: Plasma Updates"
    date: 2008-04-03
    body: "I'm pretty sure python-plasma exists now."
    author: "Lee"
  - subject: "Re: Plasma Updates"
    date: 2008-04-03
    body: "What about pony-plasma? That's been promised for aaaaaages."
    author: "Humph"
  - subject: "Re: Plasma Updates"
    date: 2008-04-03
    body: "> those whould be only script not needing compilation\n\ncorrect; and right now we have support for ecma script (updated), python and html/css all coming together. hopefully they'll all be ready for 4.1 (they all currently work, it's just a matter of details).\n\n> Is there a plan to make plasmoids stop using C++\n\nfor technical reasons, certain widgets will remain C++.\n\n> and plasma API stop receiving changes so that KDE 4.1 plasmoids work on KDE 4.2 \n> and vice-versa?\n\nas i said long before 4.0 came out: libplasma will not be binary compatible between 4.0 and 4.1, the goal is to move to binary compatibility once 4.1 is out."
    author: "Aaron Seigo"
  - subject: "Re: Plasma Updates"
    date: 2008-04-03
    body: ">libplasma will not be binary compatible between 4.0 and 4.1, the goal is to move to binary compatibility once 4.1 is out.\n\nThat was what I thought, and really will be very-very good thing indeed sir! :D\n\nI just hope there is a kind of incentive to people avoid C++ in the non-official plasmoids. I know they can be even more stable than the ones from the KDE team, but I do not like the idea of getting a new plasmoid that crashes all my desktop and on restart I loose a lot of plasmd configuration (in 4.0 series it seems like plasmoids geometric are saved only in logout, a bad thing because plasma crashes on logout for me, so basically I never have new applets saved) ;)"
    author: "Iuri Fiedoruk"
  - subject: "Konqueror not using Webkit?"
    date: 2008-04-04
    body: "I thought that Konq was moving to webkit and they were dropping the KHTML support, and am I incorrect? Or did plans change?"
    author: "KDE User"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-04
    body: "This never was the plan, is not the plan and will not be the plan.\nKHTML is maintained and developed so fast, there is no reason to drop it.\nthere might be some Webkit support, so you could choose you want KHTML or Webkit, but KHTML is never going to be dropped.\nabout the webkit, there is currently noone working on intergration with Konqueror right now, so i think Konqueror + Webkit is a post-4.1 thing."
    author: "Emil Sedgh"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-04
    body: ">KHTML is maintained and developed so fast, there is no reason to drop it.\n\nSorry to tell you, but what about the users?\nI do not want to be bad at KHTML developers, but those are the facts:\n- webkit is today, much (much, much, much in case you do not see it) better than khtml (a thousand times sorry, but.. it's true)\n- webkit is becoming a standard with support from industry key players like apple, nokia and google. \n- If KHTML development is fast, webkit development is twice faster :-P\n\nC'mon guys! I know it's hard to drop your ego because letting khtml goes means admiting the the fork is better, but look at xfree86 and xorg! Sometimes evolving means forking and letting the old die alone in peace. :D\n\nWell, if KDE 4.1 do not come with a webkit kpart, I'll start a browser from zero using webkit/qt/kdelibs!!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-04
    body: "> webkit is today, much (much, much, much in case you do not see it) better than khtml\n\nit's much harder to spread such lies and vapored hype when people can actually try the demo browser in Qt 4.4 RC and see what it is up to.\n\nKHTML majorly kicks WekitQt's ass in about every domains,\nand is ridiculously faster.\n\nI'm not talking about beliefs here, you just have to *use* the software to see it.\n\n> I know it's hard to drop your ego\n\nif KHTML developers had any inflated ego problem, they would have dropped\nthe project long ago - it takes quite a bit of selflessness to endure the nauseating corporate-loving drivel from people who have forgotten their history or are just plain malevolent.\n\n"
    author: "w."
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-05
    body: "Did I said webkit or qt/webkit?\nLet me se.. no, webkit, there it is!! webkit, no qt mentioned :D\n\nTryied ipod touch, safari for windows and safari for mac with webkit nighties.\nAll of them kick khtml ass, and I am using khtml from kde4 that is way better than kde3 was ;)\n\nThe only advantage khtml have is that kde developers are more used to the code and integration is easier, I give you/them that."
    author: "Iuri Fiedoruk"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-05
    body: "> Did I said webkit or qt/webkit?\n> Let me se.. no, webkit, there it is!! webkit, no qt mentioned :D\n\nYou should have, because if konqueror is to use webkit it will be qtwebkit."
    author: "Renaud"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-12
    body: "iPod Touch? LOL, iPod Touch sucks at rendering websites, far behind the destop versions of Safari, and behind Konqueror on Linux by far too. Oh, and you did mention Google Android and Nokia S60 Browser right? Maybe you yourself should go download the Android SDK and S60 SDK (or buy a N95) to try them out, they are also far behind KHTML, and they are dead meat compared to Opera Mobile on the mobile platform. And Safari on Windows is worse than Konqueror on Linux. The only area that WebKit truly excels is OS X, I'll give you that, but on any other platform, it's worse than KHTML on Linux. Granted the Windows version of Konqueror in KDE4 is also not so shiny, that's why on Linux we stay with KHTML, and on OS X we stay with WebKit. WebKit on Linux simply sucks, so there's no point for Konqueror to use WebKit."
    author: "lol you are so funny"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-04
    body: "\"webkit is today, much (much, much, much in case you do not see it) better than khtml\"\n\nWebKit is essentially better only in displaying defective web sites.\n\nA lot of code went into both WebKit and KHTML since the fork and now AFAIK KHTML devs are merging more WebKit code into KHTML than WebKit devs into WebKit. So to not lose developements, WebKit advantages should be merged into KHTML. If KHTML devs do this and/or try to catch up with WebKit with their own development, there is nothing bad with it.\n\n\"I'll start a browser from zero using webkit/qt/kdelibs\"\n\nNothing stops you from doing it. But I think there is a QtWebKit kpart already woth some flaws. Even if its development is stalled (I don't know what is about it), you'd better fix that code."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-05
    body: ">WebKit is essentially better only in displaying defective web sites.\n\nOK, right.\nThat means only 95% of thwe web ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-12
    body: "maybe 95% of \"thwe web\" that you made up in your imagination :D\n\nI'm using Konqueror 3.59 and there's not a problem with 99% of the website I visit, and when there's some fucked up website that Konqueror can't handle, Safari can't handle it neither.\n\nIf you want Konqueror to handle those fucked up website, you should argue for Konqueror to use Gecko, not WebKit, both KHTML and WebKit are miles behind Gecko in handling fucked up websites, which should not be handled anyway :rolleyes:"
    author: "lol you are so funny"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-05-28
    body: "this is a general problem, but industry giants begin to enroll engines for ajax which are webkit compatible. like gmail or others.\n\nso, yes, khtml does fall behind webkit here. but i would rather say, only kjs vs. webkit's js is sometimes the real issue.\n\nwe can be as fanatic as we want to be, khtml is great and so on and so on, but, there would be no harm in making webkit available in konqui, which is a user-friendly choice of the developers towards the user.\n\n"
    author: "g4b"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-05
    body: "About using the kpart...\nLook at dolphin case.\nIt simply is rocking konqueror (as file manager) away. There are a lot of users used to konquy that hate it, but in my experience, most new users or more desktop oriented ones just love it.\n\nI think it's better to just create something not-bloated sometimes. or we will become microsoft.\n\nAaron was right about droping kicker (ok, I hate myself now) and i MUST ask him for forgive or punishment for complaining about it :-)\nIt was a bit late in development for 4.0 (actually I think it will only really catch in 4.2), but it simply rocks the development of desktop from now and forward. He had the guts the face our fury and he will cleary be the winner (exept for the very incomplete 4.0 part, but that is already water over the bridge... at least I hope). Why not do the same for konqueror/khtml/webkit?\n\nI'm only have to get a qt 4.4 package for ubuntu now and start doing my minimal browser. I think I'll call if Doninha!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-05
    body: "The more the computer evolves into a widely spread appliance like a TV the more users are naturally those using their computer mostly for browsing, mail and a few office documents. Those, lets call them 'simple-users', probably will be comfortable with dolphin. Thats ok. \n\nBut on the other side there are also a lot of users in need of a file manager. It may be unimaginable for you - but those are actually depending on that features only Konqueror (KDE 3 for now) has. \n\nSo, a statement like \"It simply is rocking konqueror (as file manager) away.\" is as absurd and ridiculous as it ever could be.\nThere wouldn't be much of a problem. But why those 'simple-users' are so eager to see the advanced programs dropped - i wont ever get this. \nThese programs had made the success of KDE, and without them the whole KDEnvironment will lose. Cause - honestly - who cares a lot more than a * about the 10ths finder/nautilus or a widget-engine?"
    author: "Thorstein"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-07
    body: "\"It simply is rocking konqueror (as file manager) away.\"\n\n- It doesn't - there are many features in Konqueror that are not and are not planned to be in Dolphin. For some users those features are much more important than the extra features of Dolphin.\n- It has those extra features only because developers decided to write them for Dolphin. Which of them couldn't have been written for Konqueror?\n\n\"I think it's better to just create something not-bloated sometimes. or we will become microsoft.\"\n\nPlease make it more specific about what problems of being bloated are similar in KDE and Windows. And that what do you mean by the statement that Konqueror is bloated. It can not do a lot of things: it can load files using kio and kparts to display them. Kparts are only loaded if you use them: if you only browse html pages, only KHTML will be loaded and Konqueror together with KHTML is nothing different from a web browser. It just has more possibilities, which you do or do not use."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-05
    body: "A couple of years ago, most of your arguments (let alone here how subjective those are) had applied to Gecko too. (even to a greater extent in some cases (de facto standard, spreading))\n\nBut do you really think abandon KHTML in favor of Gecko would have been \nthe right decision? The tide of events clearly exposed that this had been a substantial mistake. (funnily enough, than probably there wouldn't be a WebKit at all) \n\n> If KHTML development is fast, webkit development is twice faster :-P\nlike Gecko's development? Even that may true for a while - in the long term it is a big advantage for (but not only) KDE to have a rendering engine/browser of it's own. \n\n> better than khtm (a thousand times sorry, but.. it's true)\nbtw. what led you to generalize your opinions? That in fact seems like an ego related problem \n \n\n"
    author: "Thorstein"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-06
    body: "> Well, if KDE 4.1 do not come with a webkit kpart, I'll start a browser from zero using webkit/qt/kdelibs!!\n\nYou could as well help to improve the so-far excellent demobrowser Qt comes with:\nhttp://benjamin-meyer.blogspot.com/2008/04/demo-browser-update.html\n"
    author: "Yves"
  - subject: "Re: Konqueror not using Webkit?"
    date: 2008-04-04
    body: "Copying and pasting that stupid rumor over and over won't make it any truer. The KHTML and Konqueror developers have denied this rumor many times, why do you still believe it?"
    author: "Kevin Kofler"
  - subject: "Spell checker not works (however good job!)"
    date: 2008-04-04
    body: "I use KDE 4.0.3 on my desktop machine. I'm pleased with this version. I'm impressed with the speed of the KDE4 development processes.\n\nOne missing thing for me is spell checking. It's simply not works. Just try it in kate for example.\n\nOk, there are other things too, however I know this is the beginning of the KDE4 journey. \n\nI hope I can switch to KDE4.1 on my notebook when will be finished. Meanwhile I will stick with KDE3.5.9. Keep up the good work!"
    author: "totya"
  - subject: "scrolling tabs"
    date: 2008-04-05
    body: "4.0.3 certainly has less bugs. Kudos to all.\n\nOne thing still annoys me tho. This is the ability to change to a new tab on just scrolling on it (or the LACK of it). This is really convenient and i can only compare it to just using the scroll wheel to scroll down a web page without having to hunt down the scroll bars. Ironically, gnome has this for AGES. The strange thing tho is that some KDE apps HAVE this feature. For example, konsole can change between its tabs. Also, Amarok 2 (in the edit track details box), does this beautifully. Is this a plasma thing or what? Could the code for this tab changing on scrolling be extracted from amarok2 (thanks guys), and taken to something like kdelibs so that all KDE apps can enjoy this. Again, good work."
    author: "hacker"
  - subject: "Re: scrolling tabs"
    date: 2008-04-07
    body: "Which application are you speaking about?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: scrolling tabs"
    date: 2008-04-08
    body: "All KDE apps. Just try the help-about dialog present in every (most) KDE apps. Try to scroll to a different tab (e.g. to authors or translators). This doesnt work and the sad thind is that this happens in ALL tabs regardless where they are, in dialog boxes eg. Is there something like qtabpanel or sth that would encompass all this changing tabs logic. The strange thig that amarok (partly) does it (in the edit track info dialog). Does amarok use a custom set of widgets not present in kde or qt? Is there any communication that goes on between the amarok devs and those of kde/qt?\n\nfinally, is there an issue tracker I can log this feature (should be a bug)?"
    author: "hacker"
  - subject: "Re: scrolling tabs"
    date: 2008-04-08
    body: "You want http://bugs.kde.org for reporting that feature request."
    author: "m_abs"
---
The <a href="http://kde.org/announcements/announce-4.0.3.php">third bugfix release of the KDE 4.0 series</a> is available. KDE 4.0 is mainly targeted at users who live on the bleeding edge. As a dot-oh release it might have its rough edges. The KDE Community releases a service update for this series once a month to make those bleeding edge users' lives easier. The <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php">changelog for KDE 4.0.3</a> is, although not complete, quite impressive. Especially KHTML and with it the Konqueror webbrowser have seen great improvements in both, stability and performance.




<!--break-->
<p>Since KDE 4.0, some Extragear applications form part of the release. Extragear applications can choose to follow their own release cycle, which is what for example Amarok and K3b do. There is also the option to have extragear applications released with KDE. Applications such as <a href="http://www.rsibreak.org">RSIBreak</a> (a program to save your health), <a href="http://www.kde-apps.org/content/show.php/KGrab?content=74086">KGrab</a> (an advanced screenshot-taking tool), <a href="http://www.kpovmodeler.org/">KPovModeler</a> (a 3D modelling application). Tom Albers, responsible for the release of the Extragear package says, <em>"The new item in this release is kio_gopher. It's really great that this protocol is still not dead and we now have support for it in KDE4. New technology meets ancient protocol ;-)"</em>.</p>

<p>The KDE Team also uses this release to highlight some gems of the KDE package. This time, a closer look is being taken at the <a href="http://edu.kde.org">KDE Educational</a> software. The KDE Edu project has also recently done some work on their website, which is surely worth a visit. We hope you enjoy this release and notice our commitment to making progress in stability and usability of the KDE 4.0 series while <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">development of KDE 4.1</a> is going on at more than full speed.</p>



