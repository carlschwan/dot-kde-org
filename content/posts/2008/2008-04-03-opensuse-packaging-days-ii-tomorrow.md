---
title: "openSUSE Packaging Days II Tomorrow"
date:    2008-04-03
authors:
  - "jbrockmeier"
slug:    opensuse-packaging-days-ii-tomorrow
comments:
  - subject: "Debian"
    date: 2008-04-03
    body: "why aren't just all distributions debian based. This would make life much easier...\n\nWhere is the Desktop LSB gone?"
    author: "andy"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "And why aren't these debain distros not using gentoo instead? By your rationale it would make life much easier for me."
    author: "anon"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "LSB 3.2 was released in January. See:\n  http://www.linux-foundation.org/en/Specifications"
    author: "Thiago Macieira"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "Why aren't just all distributions RPM based? This would make life much easier ..."
    author: "Josefine Wiesend"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "The is where sometimes choice can be a bad thing. I like RPM distributions more then Debian, though, I wish there was only one binary package. There just isn't a need for 10 of them. "
    author: "Jeremy"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "In fact it had been better if it had been the other way, more binary packages. There should have been one for each distribution, RPM, MPM, SPM, FPM etc(RedHat, Mandriva, Suse and Fedora respectively). \n\nThat RPMs are installable across different distributions are one of the main causes of RPMs bad reputation(Late in getting dependency resolvers are the other). If the packages are not carefully created to be distribution independent, installing RPMs from a different distribution has always been a efficent way to mess up your installation. \n\nInstalling RedHat RPMs on Mandriva(Mandrake) was always unwise, and trying to do it on on Suse was worse. Unlike Mandriva, Suse was never even based on RedHat making the difference even greater.\n\nNaturally, this has never been a problem for Debian. And any problems caused by mixing stable and unstable repositories has always been firmly placed as the users own fault. And there are reports on the net of funny thing happening when mixing Ubuntu and Debian repositories, so despite having a historic good reputation .deb packages can give you much the same problems as RPMs. "
    author: "Morty"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "Why aren't they all .exe and .msi based? That's really simple!"
    author: "Sarah"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "Ok Sarah, everybody was having fun in this thread and you had to come up and cross the line. Shame on you. "
    author: "ad"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "Why aren't all distributions Pacman based? Then everyone would have fast package management :)"
    author: "miggols99"
  - subject: "Re: Debian"
    date: 2008-04-03
    body: "Rpms are more powerful and flexible than debs, but they are so slow and seemingly all distros that use them suck at package management in comparison to debian based distros. \n\nThat may be because their tools suck, but seeing as they all use different tools the only element of commonality is the fact that they use rpms. This leads to the conclusion that either rpm should be chucked, or that the tools should be improved a lot seeing the way they are now.\n\ndebian by way of ubuntu already has masses of users using and loving debs"
    author: "txf"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "> seemingly all distros that use them suck at package management in comparison to debian based distros. \n\nZYpp (package management stack for openSUSE) in the upcoming version -- 11.0 -- is actually faster than APT, and has even more useful features, like installation of local/remote RPMs. It also features a much cleverer and flexible solver. \n\nOf course it also supports 1-click-install, which is much more \"what the masses want\"."
    author: "apokryphos"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "RPMs are gzipped cpio archives and DEBs are gzipped ar archives. Not a big difference when you install a single package with the rpm or dpkg tools.\n\nApt-get *seems* to be faster than many other tools because it don't automatically refresh it's package cache. Running apt-get update && apt-get install xxxx takes a longer time.\n"
    author: "LP"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "> RPMs are gzipped cpio archives and DEBs are gzipped ar archives.\n\nIn fact, this is another new change in openSUSE Factory. We're switching to LZMA which actually offers better compression (so smaller downloads), and faster decompressing (for faster installation).\n\nSee coolo's blog for some figures: http://www.kdedevelopers.org/node/3326"
    author: "apokryphos"
  - subject: "Re: Debian"
    date: 2008-04-06
    body: "Out of interest, whats the deal with the two versions of RPM? I haven't used RPM based distros for ~8 years so I was kind of suprised to find out that the codebase split?\n\nGood/bad thing? Which does SUSE use?"
    author: "Martin Fitzpatrick"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "Why aren't all distributions based on recipes (and packages)?\n\n(GoboLinux 014.01 is just out)"
    author: "reihal"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "You shout for LSB which declares RPM format the standard? :-)"
    author: "Anonymous"
  - subject: "Re: Debian"
    date: 2008-04-04
    body: "'How you package it' is now basically became the first priority of people then having a solid desktop/application. There may be a single developer, but another 100+ is busy in packaging it!!! Sheer waste of brain/time. And at the end of it what do u gain? Every distro (should kill this word) behaves like a monolithic application. "
    author: "window washer"
  - subject: "Re: Debian"
    date: 2008-04-06
    body: "Why aren't all distributions KDE 4.1 based?\n\nThat would make life so much easier for me. :)"
    author: "Max"
  - subject: "Unity"
    date: 2008-04-03
    body: ".deb ++\n\n...How unpopular would it be to say that SuSE and Fedora should merge?\n..or that Novell should perhaps support Ubuntu and spin SuSE off?\n\nGoogle trends basically says that Ubuntu is now the face of desktop linux...\n\nIf .deb isn't good enough for everyone now, we need something that is. ..and it's not rpm."
    author: "ethana2"
  - subject: "Re: Unity"
    date: 2008-04-03
    body: "And what are you going to do when Ubuntu is not more the most popular desktop distibution?\n\nPlease try to remember: that what is good for you is not good for everyone or every purpose.\n"
    author: "LP"
  - subject: "Re: Unity"
    date: 2008-04-06
    body: "No, Google Trends says that Ubuntu is so bad that everybody uses Google a lot to try to fix his problems.\nOther distro users are just using their distro, not using Google to fix it.\n"
    author: "ME"
  - subject: "good!"
    date: 2008-04-03
    body: "thank you for this initiative :)"
    author: "mxttie"
  - subject: "life before and after opensuse"
    date: 2008-04-03
    body: "i used to be a sad linux user, after enduring all the wick laughs and sometimes compassion remarks from my Windows  friends, when they saw me going back to the cybercaf\u00e9 in the cold nights, walking alone, stones in my pocket in case I\u0092ll get a surprising meeting with all those fierce dogs,  because a fu..... dependencies is missing, I still remember when I tried to download gnome planner, after three times of unsuccessful install due to weird dependencies problems, i ended up in a act of high hopeless to download the Wondows install.   \n\nWhen a day, a good man in the dot, told me there is Opensuse who has rpm repositories, just get the dvd install, then no more pain, no more harm, search the package you want, download the files to flash disk and go home proudly.\n\nNow, i am a proud linux user, i keep downloading and installing the latest and the greatest linux software, without fears of dependencies and incompatibilities. \n\nthanks, opensuse, thanks build service, GOD bless you.\n"
    author: "mimoune djouallah"
  - subject: "Re: life before and after opensuse"
    date: 2008-04-04
    body: "well said, and I also had the same experinces.  Opensuse, changed my linux experince and made me happy."
    author: "R. J."
  - subject: "Re: life before and after opensuse"
    date: 2008-04-04
    body: "Well, I wouldn't go as far as referring to higher beings of any persuasion ;-), but true indeed, openSUSE made my linux life definitely easier. (Am I happier due to openSUSE? Dunno.)"
    author: "Planetzappa"
  - subject: "Re: life before and after opensuse"
    date: 2008-04-04
    body: "I've been on SUSE since 7.3. Tried some other distributions along the way but always came back. Since Novell took over, despite all the fears, it only got better. Much better!\n\nThey actively work on KDE and provide a very polished KDE desktop with Qt and KDE system tools. The development process is open to the community. There is an impressive amount of official and community repositories. The distribution supports easy inclusion of both official and community based repositories. \n\nFind it very difficult to imagine myself moving to any other distribution any time soon. \n\n\n"
    author: "ad"
  - subject: "Re: life before and after opensuse"
    date: 2008-04-04
    body: "\"(Am I happier due to openSUSE? Dunno.)\" at least you are happy :-)"
    author: "mimoune djouallah"
  - subject: "Idea: KDE-apps.org Build service"
    date: 2008-04-04
    body: "I feel the pain of that *cool app* which isn't available in my favourite repository or repository format. Can't KDE-apps.org set up a build service for the most favourite distro's (just let the distro maintainers manage the scripts)? This way everybody can easily test-drive these apps (especially new users) and apps can get popular more easily. \n\nI know that the actual implementation of such a build service is quite difficult and might take a while. I can imagine while uploading a tar file that the developer has to click some check-boxes with library prerequisites and dependencies. The build service can build the right file with right dependencies for each distro."
    author: "Bert"
  - subject: "Re: Idea: KDE-apps.org Build service"
    date: 2008-04-04
    body: "Why not just use the openSUSE Build Service? It supports lots of distributions..."
    author: "binner"
  - subject: "Re: Idea: KDE-apps.org Build service"
    date: 2008-04-04
    body: "Actually, that's exactly what the openSuse build service actually does, it's not just for openSuse,  You have to take your hat off to them for doing this, how many software vendors do you know who would go to the effort and expense to support their competitors products?  The build farm doesn't come cheap :-)\n\nHere's the list of currently supported distros:\n\n          o openSUSE 10.3\n          o openSUSE 10.2\n          o SUSE Linux 10.1\n          o SUSE Linux Enterprise 10\n          o SUSE Linux Enterprise 9\n          o openSUSE Factory \n\n          o Debian Etch \n\n          o Fedora 8\n          o Fedora 7\n          o Fedora 6 + Extras\n          o Red Hat Enterprise Linux 5\n          o CentOS 5 \n\n          o Mandriva 2008\n          o Mandriva 2007\n          o Mandriva 2006 \n\n          o xUbuntu 6.06\n          o xUbuntu 7.04\n          o xUbuntu 7.10 \n"
    author: "Odysseus"
  - subject: "Re: Idea: KDE-apps.org Build service"
    date: 2008-04-04
    body: "Oh, and I should add that the Build Service code is obviously Open Source, so you could run your own if you had the build farm for it :-)"
    author: "Odysseus"
  - subject: "Re: Idea: KDE-apps.org Build service"
    date: 2008-04-28
    body: "Bin von windows zu mandriva gewechselt.Wo bekomme ich linuxtreiber f\u00fcr meinen comp her.Habe amd atlon dualcorex2 3000+,motherboard asrock.Es muss doch auch ohne windows gehen.\n\nMfG A.H."
    author: "A.H.Kirchert"
  - subject: "Re: Idea: KDE-apps.org Build service"
    date: 2008-04-05
    body: "Actually, apps on KDE-apps.org are provided in openSUSE repositories quite quickly. When I released KDiamond at first, I got a mail from an openSUSE packager about one hour later. Every update was handled as quick as this."
    author: "Stefan Majewsky"
---
Ever run into the issue that you saw a cool new app on <a href="http://www.kde-apps.org">KDE-Apps.org</a> and could not find a binary package for your favourite KDE version on your favourite KDE distro? The <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a> allows creation of binary packages quite easily, so you can do the work yourself and help other KDE users who run into the same problem. If you are interested in learning how, the openSUSE community are organising <a href="http://en.opensuse.org/Packaging/Packaging_Day">Packaging Days II</a>, which starts tomorrow.




<!--break-->
<p>Members of the openSUSE team will be available for any help with packaging from Australian morning on Friday, April 4th to American evening on Saturday, April 5th in the #opensuse-buildservice IRC channel on Freenode.</p>

<p>We will help new packagers to create packages for software that is not currently available for openSUSE. We invite people to roll up their sleeves and get involved. If you have this fancy application or tool that is not packaged yet, and you are not afraid to learn how to do it yourself, then this is for you.  The openSUSE Build Service has KDE 3.5.x, 4.0.x, and 4.1.x packages updated weekly, so it is easy to package software that requires the most recent KDE. </p>

<p>This will be a good opportunity for people who have hitherto done little or no packaging to learn how to do it, with helpful people available to answer all the  questions you might have related to packaging.</p>



