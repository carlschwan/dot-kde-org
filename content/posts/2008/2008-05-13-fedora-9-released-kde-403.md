---
title: "Fedora 9 Released with KDE 4.0.3"
date:    2008-05-13
authors:
  - "kkofler"
slug:    fedora-9-released-kde-403
comments:
  - subject: "x86_64 Live now fits on a CD"
    date: 2008-05-13
    body: "For 64-bit users, there's another good news: the KDE Live image for x86_64 no longer needs a DVD, a standard 700 MB CD is now sufficient."
    author: "Kevin Kofler"
  - subject: "Re: x86_64 Live now fits on a CD"
    date: 2008-05-13
    body: "Are you sure?  According to http://fedoraproject.org/en/get-fedora , it still requires a DVD."
    author: "Brendan"
  - subject: "Re: x86_64 Live now fits on a CD"
    date: 2008-05-13
    body: "The information there is out of date, check the file sizes. :-)"
    author: "Kevin Kofler"
  - subject: "Re: x86_64 Live now fits on a CD"
    date: 2008-05-14
    body: "This has been fixed now, thanks to our webmasters. :-)"
    author: "Kevin Kofler"
  - subject: "cool stuff"
    date: 2008-05-13
    body: "It's cool to see they will provide updates to 4.1 when it becomes available - like Suse plans to do. I decided to put Suse on a laptop when I get a new one (if it proves to be fast enough, that is), but Fedora only lost due to it's gnominess (most tools are gnome apps, and I personally don't like mixing very much when on low-end hardware, too slow)."
    author: "jos poortvliet"
  - subject: "Re: cool stuff"
    date: 2008-05-13
    body: "hi jos\n\n\" Fedora 9 will include KDE 4.1 when it is released in July of this year\" that's really a very good news, but for opensuse it is still not decided http://en.opensuse.org/KDE/KDE_on_openSUSE11.0 , of course we can always install kde4.1 when it will be out, but an automatic update is even better\n\nfriendly\n\nkonqueror 4.0.73 ; ms windows xp, net access with proxy ;)"
    author: "mimoune djouallah"
  - subject: "Re: cool stuff"
    date: 2008-05-13
    body: "ops,  for the proxy part, not sure if it is fixed or not because it seems we have a direct access for the internet now. "
    author: "mimoune djouallah "
  - subject: "Re: cool stuff"
    date: 2008-05-13
    body: "Well, the .kde4 settings in 4.0.3 didn't work with the 4.1 svn. For my system they screwed everything up and had to be deleted. After that, everything just worked fine. But that makes me wonder if it would make any sense at all to provide an automatic update to 4.1 once it's out and delete all the settings. Some users might prefer to keep their old ones."
    author: "Jens Uhlenbrock"
  - subject: "Re: cool stuff"
    date: 2008-05-13
    body: "If 4.1 chokes on 4.0 settings, surely that's a bug which has to be fixed?\n\nFolks, if you run into problems, it would be helpful if you didn't delete .kde (or .kde4 if your distro uses that) outright, but tried removing individual files out of the way so we can figure out what _exact_ files are the problem, that way we (developers and/or packagers) should be able to fix it (either by fixing the code or by writing a kconf_update migration script). Automatically deleting all the settings is obviously not an option."
    author: "Kevin Kofler"
  - subject: "RSS Feed"
    date: 2008-05-13
    body: "Is there a RSS feed for the dot?\nI've looked for it several times and haven't found any.\nLove the dot, Pascal."
    author: "Pascal"
  - subject: "Re: RSS Feed"
    date: 2008-05-13
    body: "Yes, there is: http://www.kde.org/dotkdeorg.rdf enjoy :)"
    author: "Thomas"
  - subject: "Re: RSS Feed"
    date: 2008-05-13
    body: "Super nice!\n\\me thinks it should be visually available on the dot as well as main kde site\n"
    author: "Pascal"
  - subject: "What's about KNetworkManager?"
    date: 2008-05-13
    body: "I'm using Fedora 8 and there is nm-applet (GNOMEs network manager systray app) integrated (in the KDE spin). However, there has been announced that there would be an update for KNetworkManager in Fedora 8, but that somehow never happened. Is there a new version of KNetworkManager integrated? It seems to me that development has stalled for this app... Any news?"
    author: "Thomas"
  - subject: "Re: What's about KNetworkManager?"
    date: 2008-05-13
    body: "Sadly, last we tried, knetworkmanager still wasn't ready for nm-0.7 (work is still undergoing).  Understandable, since nm-0.7 isn't (officially) released either.  Rest assured, when able and working, knetworkmanager (or some other kde nm interface) will make a re-appearance in fedora."
    author: "Rex Dieter"
  - subject: "Re: What's about KNetworkManager?"
    date: 2008-05-13
    body: "Thanks for the information!"
    author: "Thomas"
  - subject: "Re: What's about KNetworkManager?"
    date: 2008-05-13
    body: "Helmut Schaa and myself are working on a new version of KNetworkManager for NetworkManager 0.7, for KDE 3.\n\nIn addition, I just merged the changes to Solid that add support for NM 0.7 in KDE 4 to kdebase trunk, and Chris Blauvelt is working on a plasmoid (in kdereview)"
    author: "Will Stephenson"
  - subject: "Re: What's about KNetworkManager?"
    date: 2008-05-13
    body: "Mmm, mmm sweet. Thanks Will."
    author: "Rex Dieter"
  - subject: "openSUSE"
    date: 2008-05-13
    body: "On the trail of distributions with KDE 4: I'm running openSUSE 11.0 Beta 3 with KDE 4.0.4 since a week or two. It works just without a flaw, and looks just great. Stephan Binner has posted a screenshot of the default KDE 4 desktop on his blog on kdedevelopers.org some weeks ago."
    author: "Stefan Majewsky"
  - subject: "Re: openSUSE"
    date: 2008-05-14
    body: "Where did you get Beta 3 from?"
    author: "Bobby"
  - subject: "Re: openSUSE"
    date: 2008-05-14
    body: "Must be a time traveller."
    author: "binner"
  - subject: "Re: openSUSE"
    date: 2008-05-15
    body: "beta 3 is meant to be released tomorrow.\n\ndoes anyone know if you have to download it to be running it, or is using the factory repositories automatically update you to what beta 3 would be?"
    author: "anon"
  - subject: "Re: openSUSE"
    date: 2008-05-15
    body: "Sorry for replying this late, but: `sudo zypper dup` (= dist-upgrade) from openSUSE 11.0 Beta 2\n\nIt then was a Beta 3 according to /etc/SuSE-release"
    author: "Stefan Majewsky"
  - subject: "why gtk applications look so ugly?"
    date: 2008-05-14
    body: "Dear all, \n\n   I've just install Fedora 9 because I was courios to enjoy KDE, but all gnome applications and in particular I mean Firefox and the network manager applet look so ugly compared to what they look with gnome. \n\nI managed to partially fix it using gtk-qt-engine, but just once. With the next reboot I can't even log in in KDE.\n\nPlease help me!\n\n\ncheers,\nhilbert"
    author: "hilbert"
  - subject: "Re: why gtk applications look so ugly?"
    date: 2008-05-14
    body: "That's a bug which will be fixed in the next update of KDE (the KDE 4.0.4 update which is coming soon).\n\nhttps://bugzilla.redhat.com/show_bug.cgi?id=443309\nhttp://bugs.kde.org/show_bug.cgi?id=146779\n\nThe fix will also be in upstream 4.0.5 and 4.0.74."
    author: "Kevin Kofler"
  - subject: "Re: why gtk applications look so ugly?"
    date: 2008-05-14
    body: "thanks a lot! "
    author: "hilbert"
  - subject: "Congratulations but"
    date: 2008-05-14
    body: "Congratulations for the great work!\n\nJust recommended a friend who is new to linux to go to http://fedoraproject.org/get-fedora to download and give it a try.\n\nHe screamed at me when he noticed that his desktop is different from mine. That poor chap download the \"Fedora Desktop Live Media\"  because he didn't think that \"Fedora KDE Live Media\" is for the desktop.\n\nThis is a good joke !!  or maybe not if you want new users to try KDE \n\n :(\n\n\n"
    author: "Mich"
  - subject: "Re: Congratulations but"
    date: 2008-05-14
    body: "Agreed. It's a bit confusing that the GNOME version is called \"Desktop\", as if it was the only DE available. Despite the magnificent work of the KDE SIG, I see that Fedora is still biased towards Gnome/Gtk users. Hopefully this will continue to change to a more equal footing for all major DEs (yes, even Xfce)."
    author: "Humph"
  - subject: "Re: Congratulations but"
    date: 2008-05-14
    body: "If they wrote GNOME on it, nobody would download it. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: Congratulations but"
    date: 2008-05-15
    body: "Haha that's naughty but true (for me at least) :D"
    author: "Humph"
  - subject: "Re: Congratulations but"
    date: 2008-05-15
    body: "that does suck.\n\nwhat confussed me when I came from windows to linux was the whole x64 x86.  I had no idea what any of that meant back then.  And it took me ages to work it out :(  might sound silly, but as a windows user, plug it in turn it on and that is all that I cared about.  Oh wait that is linux, windows is plug it in, turn it on, run a antivirus, run a spyware scan, update anti virus, update spyware, argh shit blue screen of death.   ahhhhhhhh, linux........"
    author: "anon"
  - subject: "Staying with Fedora 8"
    date: 2008-05-14
    body: "Since KDE3 is not in Fedora 9 and I feel like it is impossible to leave KDE 3.5.9 from Fedora 8 intact, I'm not gonna upgrade to Fedora 9 in the nearest future.\n\nAlas, KDE 4.0.x is not a replacement for KDE 3.5.9."
    author: "Artem S. Tashkinov"
  - subject: "Re: Staying with Fedora 8"
    date: 2008-05-14
    body: "Fedora 8 is going to get updates for about another 6 or 7 months (until 1 month after the Fedora 10 release), so nobody is forcing you to upgrade to Fedora 9 right now.\n\nMaybe you will reconsider when KDE 4.1 will be available? :-)"
    author: "Kevin Kofler"
  - subject: "Re: Staying with Fedora 8"
    date: 2008-05-15
    body: "> Maybe you will reconsider when KDE 4.1 will be available? :-)\n\nDefinitely. :-)"
    author: "Artem S. Tashkinov"
  - subject: "Re: Staying with Fedora 8"
    date: 2008-05-15
    body: "+1\n\nthe sad thing is that I am supposed to be on the bleeding edge, as I participate in the development ... maybe a time to kick up some virtual machine"
    author: "kavol"
  - subject: "Ugliest ever release"
    date: 2008-05-16
    body: "This is most ugliest and buggy release of KDE ever seen for last 10 years. Kopete is crashing constantly and can't connect to google, program starter is worse even comparing to Vista, panel is ugly and not configurable, and most annoying thing is icon behavior on desktop. They live their own life populating all desktop over with strange shadows and unneded controls.\n\nThe best thing -just the brest! My laptop can not go to hybernation with kde4. It eats memory like crazy colorado bug. \n\nBut why gnome just works? \n \nI was just forced to switch to Gnome which I really do not like, but I must work so I have no choice.\n\nI do not understand KDE team. How you guys can call \"release\" this not even alpha-grade work? Such kind or \"artwork\" has only one consequence. You lose your users.\n "
    author: "Alex Lukin"
  - subject: "Re: Ugliest ever... [here it is: KDE4 on Fedora 9]"
    date: 2008-05-16
    body: "Definetely true! Placement of the Icons on the desktop seems to be random, there is no \"Align to Grid\". Right-Click on the desktop is useless, no \"Create new Folder\" or whatever. No Quickstart-Panel in the taskbar, useless like it is. I am very disappointed, having installed Fedora 9 and there is only KDE4 :-(\n\nLike this, KDE4 is just unusable and ugly, sorry guys."
    author: "Fritz Pinguin"
  - subject: "Re: Ugliest ever... [here it is: KDE4 on Fedora 9]"
    date: 2008-05-18
    body: "KDE team should make something about that icon thing on desktop. Now I need to open dolphin to manage files on desktop because I cant copy/paste etc from it. I would like that transparent background would be same size on all icons and not different. Placement could be set easily and those would stay there. Altought those icon managements should be more like filemanagement on konqueror or dolphin. ANd not as plasmoids."
    author: "Fri13"
  - subject: "Re: Ugliest ever... [here it is: KDE4 on Fedora 9]"
    date: 2008-05-19
    body: "The desktop was never intended to be a place to put files on, only shortcuts (being able to store files on it was only a side-effect of how it was implemented), and even for those shortcuts, several of the Plasma developers appear to believe that this is an obsolete practice. Files belong in ~/Documents according to the xdg-user-dirs spec, not ~/Desktop."
    author: "Kevin Kofler"
  - subject: "Re: Ugliest ever... [KDE4.074 on oS 11]"
    date: 2008-05-19
    body: "That's of course fine & dandy, except now we have something which owns 90% of desktop real estate sitting as empty as the plains.  If not shortcuts (to applications or otherwise) then what will it be used for?\nI assume there is a master plan but I can't find any document/blog to hand.  \n\nAlso from a usability standpoint, why does the 'cashew' on the panel need to permanently be shown/take-up-space when that feature will only be used for a few minutes maybe once on account setup or infrequently at best?  Surely it would be just as discoverable through a 'Design your Desktop' button/cashew prominently displayed in the main-menu.  That could then turn the whole DE into 'design-mode'  where any and all aspects of the desktop could be moved, guided, justified, poked and prodded to ones hearts content.  This separates all that qdesigner type bells and whistles out of the way and ready for messing about with during lunchbreaks.  That way no cashews or extraneous context-menus are needed, this satisfies both the touchscreeners and interface minimalists."
    author: "KISS"
  - subject: "Re: Ugliest ever... [here it is: KDE4 on Fedora 9]"
    date: 2008-10-01
    body: "Having worked in computing/technology 30+ years, I find the lack of \"user\" use-case input to this featureless aspect of KDE4 to be very impressive, but not positively so.\nIf designers wish their products to be used, they would be well advised to pay very strict attention to what the user wants. The Ivory Tower committees rarely get it \"right\"."
    author: "BobC"
  - subject: "Re: Ugliest ever release"
    date: 2008-05-28
    body: "yep, i do agr33.no quick start, no desktop simplicity.I think plasma adoption on desktop ruined evrythin.kD3 team should consider these things n better NOT to put these damn things again. kde is fine upto  < f8 .NO w(m)idget plz next time\n \n"
    author: "buggy"
  - subject: "4.0.4 now in updates"
    date: 2008-05-23
    body: "FYI, KDE 4.0.4 is now in the Fedora 9 (stable) updates."
    author: "Kevin Kofler"
---
The <a href="http://fedoraproject.org/">Fedora Project</a> has <a href="https://www.redhat.com/archives/fedora-announce-list/2008-May/msg00007.html">announced the release of Fedora 9</a>, codenamed "Sulphur". As your <a href="http://edu.kde.org/kalzium/">periodic table</a> will tell you, Sulphur is the element below <a href="http://www.oxygen-icons.org/">Oxygen</a>, a fitting release name for the third major distribution to ship KDE 4.0 (congrats to Mandriva and Kubuntu for getting there first) and the first to make it the only version of the desktop. Fedora 9 includes <a href="http://www.kde.org/announcements/announce-4.0.3.php">KDE 4.0.3</a>. Unfortunately, <a href="http://www.kde.org/announcements/announce-4.0.4.php">KDE 4.0.4</a> was released too late to make it in, but there is no need to despair, it is already available in updates-testing and is expected to become a stable, tested update in a few days. To support your existing KDE 3 applications such as Kontact, Amarok and K3b, Fedora 9 includes <a href="http://docs.fedoraproject.org/release-notes/f9/en_US/sn-BackwardsCompatibility.html#sn-KDE3-Development-Platform-Librates">compatibility libraries</a> from <a href="http://www.kde.org/announcements/announce-3.5.9.php">KDE 3.5.9</a>. As always, the KDE Live CD is installable. New in Fedora 9, the live image can also be <a href="http://www.redhatmagazine.com/2008/04/08/interview-jeremy-katz-on-fedora-live-cds/">converted</a> to a <a href="http://fedoraproject.org/wiki/Features/LivePersistence">persistent USB key</a>. The <a href="http://docs.fedoraproject.org/release-notes/f9/en_US/">release notes</a> have <a href="http://docs.fedoraproject.org/release-notes/f9/en_US/sn-Desktop.html#sn-KDE">a section dedicated to KDE 4</a>.




<!--break-->
<p>In addition to the <a href="http://fedoraproject.org/wiki/Releases/FeatureKDE4">inclusion of KDE 4 as the default KDE</a>, Fedora 9 also comes with other <a href="http://fedoraproject.org/wiki/Releases/9/FeatureList">major new features</a>, such as the <a href="http://fedoraproject.org/wiki/Features/Upstart">switch</a> to <a href="http://upstart.ubuntu.com/">Upstart</a> to handle system startup, an <a href="http://fedoraproject.org/wiki/Releases/FeatureMoreNetworkManager">improved NetworkManager</a> including support for <a href="http://fedoraproject.org/wiki/Features/NetworkManager-MobileBroadband">mobile broadband</a> and systemwide configuration, a <a href="http://fedoraproject.org/wiki/Features/XserverOnePointFive">new</a>, <a href="http://fedoraproject.org/wiki/Features/OneSecondX">fast</a> version of X.Org X11, <a href="http://fedoraproject.org/wiki/Releases/FeatureTexLive">TexLive replacing tetex</a>, <a href="http://fedoraproject.org/wiki/Releases/FeatureDictionary">unified spellchecking dictionaries</a> and much more.<p>

<p>If that was not enough to convince you, you can have a look at some <a href="http://www.linux-noob.com/forums/index.php?showtopic=3455">screenshots</a> showing KDE 4 on Fedora 9. (The first few screenshots are of the installer, so scroll down to see the KDE ones.)</p>



