---
title: "KDE Commit-Digest for 12th October 2008"
date:    2008-11-18
authors:
  - "dallen"
slug:    kde-commit-digest-12th-october-2008
comments:
  - subject: "octubre"
    date: 2008-11-18
    body: "I can't understand why the date is so far away. If you show the digest today the date should of 18 Nov 2008 not of a month ago. Even if some of the news are that late in time.. But don't get me wrong, I love the digest and is great to know from the insides of future kde4."
    author: "jorge"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "I guess Danny can't write digests for a living so it's where it belongs on his priority list...\n\nThx Danny!"
    author: "Thanker"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "Because the Digests are processed and created in weekly blocks, which isn't going to change.\n\nAnd as the next person says, i'm not paid for doing this, and i'm very very busy right now (and will be into the new year).\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "Your work is very much appreciated. Thanks very much for investing so much time and energy in the Digests. Whether they are late or not we welcome them and are happy to read them.\nOnce again than you very much Danny :)"
    author: "Bobby"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "I really love your work, but because your are very busy, \n\nwhy not do it every 15 days?\nCan anybody help Danny?\n\nIt's really important to have a newsletter from KDE, for user, for marketing... \nI believe that KDE should help Danny in to deliver a KDE journal/magazine...\n\nThanks for your hard work Danny! (hope you get paid ;-)\n\n"
    author: "jorge"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "Jorge, you can be KDE, too :) You can help, too. There is nothing that separates you from those who are active for KDE. Yes, you can."
    author: "Hans"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "With is poor English of mine... I help KDE sometimes, but I agreed I could do more.. "
    author: "jorge"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "Danny, I totally consent with your opinion. I don't care if it is \"outdated\" for some people. Fact is that feature xy was developed in the time frame you aggregate and it is still of value for me to see what happened in the \"past\". People who only complain about such issues are just ignorant and dull. As long as those whiners just state meaningless word and do nothing to improve your work just don't listen to them.\n\nThanks for you professional work. I cannot complain about things you do. You provide an easy to read text, enriched with links and so on.\n\nThanks.\n\nRespectfully\n\nJames."
    author: "James B."
  - subject: "Re: octubre"
    date: 2008-11-19
    body: "Please read al my posts before you slam all your fanboyhate on me. I was saying:\n\nIf the commit velocity is bigger than Danny's velocity the time lag will be larger and larger, so Danny needs help \n\nnever meant to offend/bash Danny, as I am an avid digest reader. What I was talking about was a KDE Newspaper... Something like the planet by readable for the common user! \n\nMy mistake comes of the great work made from Danny that extents the natural limits of a commit digest to a almost news report from kde...\n\n"
    author: "jorge"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "You know, I have to agree. I enjoy these digests but it has been lagging for months. Why not just amputate and skip stuff up to today? The most important stuff from the past month could be put in one digest, and then it'd be current again. Reading 1-month old digests is starting to get old..\n\nOther than that, good work as always!"
    author: "tsb"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "Shame on you. Sorry. "
    author: "DigestLover"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "Shame on you ! Sorry. \nSuch destructive comments have been recurring for months now on the dot. \nAt first I thought people were joking. But I feel like you and others beeing very serious. This is so sad. \nIt is clear that digest-delays are due to lack of man power. So try harder to understand, why you should be ashamed and why digest might come late. \nAnd no, skipping some weeks is not an option. Start skipping a week, and then you will start skipping a month, ... \nAnd I guess summarizing some changes during past weeks to bring the digest up to date is a no go, as this would require lot of work, and this is the lack of man power who makes danny not release as often as _his_  own wishes.\nSo please help Danny since you seem to be able to write good english or let him live in peace."
    author: "DigestLover"
  - subject: "Re: octubre"
    date: 2008-11-18
    body: "learn english and/or be usefull, friend.\n\nbtw tanks Danny."
    author: "Draconiak"
  - subject: "Re: octubre"
    date: 2008-11-19
    body: "Tanks? Make love not war!\n\nThanks Danny, despite the (in the grande scheme very small) delays it's great to see your steadily continued commitment. =)"
    author: "Anon"
  - subject: "Re: octubre"
    date: 2008-11-19
    body: "I think some people have missunderstood your question.\n\nThe date of the digest is for the week of commits that took place for KDE.\n\nSo a digest dated 12 october, means that the digest is for the changes committed to KDE durring that week, it's not connected to the current date."
    author: "R. J."
  - subject: "Re: octubre"
    date: 2008-11-19
    body: "That's right, I thought that the digest was something like \"what's happening today\" but is \"what was committed in the week XY. \n\nBut if the commit velocity is bigger than Danny's velocity the time lag will be larger and larger, so Danny needs help \n\nnever meant to offend/bash Danny, as I am an avid digest reader. Maybe what I was talking about was a KDE Newspaper... Something like the planet by readable for the common user! \n\nMy mistake comes of the great work made from Danny that extents the natural limits of a commit digest to a almost news report from kde... "
    author: "jorge"
  - subject: "Re: octubre"
    date: 2008-11-19
    body: "Thanks for the digest Danny, your contribution will be of great value some day, when somebody wants to sketch some sort-of history for our the KDE codebase. You're following so closely that it's very easy to understand why decision A lead to development B which in turn led to C... This is actually very helpful (besides the fact that it's a joy to read and to be consistently astonished about the (weekly!) changes and improvements the KDE devs come up with."
    author: "Thomas"
  - subject: "kdenlive"
    date: 2008-11-18
    body: "The information about kdenlive looks really promising---I look forward to trying it!"
    author: "T"
  - subject: "Re: kdenlive"
    date: 2008-11-19
    body: "Yes, it's a great program and somewhat useable - unlike KDE...\n\nAt least KDE's framework is great. Maybe there will be KDEX. It can be made light, simple, and useable.\n\n(Talk to the hand)"
    author: "winter"
  - subject: "Re: kdenlive"
    date: 2008-11-22
    body: "As http://www.joelonsoftware.com/articles/fog0000000020.html says, \"light and simple\" never works because, though 80% of the users use 20% of the features, each user uses a different 20%.\n\nIf that weren't true, you'd be using GNOME and happy with it."
    author: "Stephan Sokolow"
  - subject: "Re: kdenlive"
    date: 2008-11-24
    body: "Hmmm... never works... hmmm... Let's see some apps I'm using right now: speedcrunch, putty, konsole, kate, konqueror, notepad, dtach, bash, less, cat... what more can I say? Oh yes, 80% of users are probably computer illiterate - maybe even plain illiterate.\n\nThankfully some people come up with great designs - even without the help of a college degree and HIG and whatnot. ;)"
    author: "winter"
  - subject: "Thanks, Danny!"
    date: 2008-11-18
    body: "The prospering of KDE is so nicely presented by the digest. Good to have!"
    author: "Hans"
  - subject: "Only Danny?"
    date: 2008-11-18
    body: "Is this Danny only as a single person doing all this incredibble work?\nI hope you get some helping hands some time."
    author: "Birdy"
  - subject: "You need help"
    date: 2008-11-18
    body: "You are doing this all alone, Danny. It seems like you could use some help.\n"
    author: "Michael \"You need help\" Howell"
  - subject: "Re: You need help"
    date: 2008-11-19
    body: "The folks at phoronix.com could probably be of assistance.."
    author: "ethana2"
  - subject: "Suggestion"
    date: 2008-11-18
    body: "The digest is great, it's always a pleasure to see it appearing. In the last couple of digests, there was also the presentation of new applications, or very interesting new technologies, illustrated with pictures. I think these articles could just as well be placed on the dot itself, as a story. In this digest for example, the presentation of kdenlive is really nice. Perhaps it could be good for marketing purposes to have it more publically on the front page? I also remember the writeup of the semantic note taking application. It would probably reach more/other people than in the commit digest, and its good publicity."
    author: "Andre"
  - subject: "Re: Suggestion"
    date: 2008-11-18
    body: "Good idea, I think. As the dot is said to have some people working on a relaunch, it could be an idea to have an Updates from Development section. Which could then integrate both the detailed weekly digest as an own article series and the program updates as other, independent ones, as kind of reviewed blog entries."
    author: "Hans"
  - subject: "Aaron's svn comment... :-)"
    date: 2008-11-18
    body: "\"one day i will knock on the door of the house that contains the people who write subversion and wrap my hands around their pretty little necks.\"\n\n\nI'd pay to see that!!!"
    author: "Phil"
  - subject: "Re: Aaron's svn comment... :-)"
    date: 2008-11-20
    body: "Wouldn't it be more useful to tell them in detail exactly what you think is wrong with their program?"
    author: "JRT"
  - subject: "Re: Aaron's svn comment... :-)"
    date: 2008-11-20
    body: "That has been done so much times. Sometimes, you have to rethink the whole concept."
    author: "Stefan Majewsky"
  - subject: "kdenlive"
    date: 2008-11-18
    body: "first of all, thanks danny for your work :)\n\ni'm using kdenlive for a while now (ok, just 1 week :) ), and it's a great work. i'd love to use it to take screencasts, but i miss the possibility to select an area on the screen (i mean visually, like ksnapshot or other software, without calculate everytime the exact pixels :( ). I don't find neither the possibility to crop a video only on a given area (eg only a 640x480 on a 800x640 video), but this is probably that there is already..somewhere... :)"
    author: "xdmx"
  - subject: "Re: kdenlive"
    date: 2008-11-19
    body: "Re the screengrab: On the capture monitor, click the little tool icon, you should be able to configure capture. Note that Kdenlive SVN have changed to use \"recordmydesktop\" to do screen grabs (one of the reasons is that the mouse is now included in the grabs).\n\nRe the crop: You can try to use the composite transition to crop with. Should work, although it may not be exactly what you are looking for. \n\nFeel free to use the forums at kdenlive.org if you have more questions."
    author: "Mads Bondo Dydensborg"
  - subject: "Re: kdenlive"
    date: 2008-11-19
    body: "> On the capture monitor, click the little tool icon, you should be able to configure capture.\n\nYep, it's possible to configure it, but you always have to calculate the pixels of the area you want record. It's would be better something visual (eg you can look at this gui for recordmydesktop: http://linux.wellassa.org/wp-content/uploads/2008/04/gtk-recordmydesktop.jpg it's simple to select only a region, and you don't have to know where is the start and end pixels :) )\n\nabout the second one i'll try ;)"
    author: "xdmx"
  - subject: "Re: kdenlive"
    date: 2008-11-20
    body: "Ok, now I understand. In 0.7.0 it was possibly to select the area on-screen. In recent SVN, this feature is gone (probably with the ffmpeg support).\n\nPlease file a feature request for this on kdenlive.org/mantis \n\nRegards\n\nMads"
    author: "Mads Bondo Dydensborg"
  - subject: "Thanks and suggestions"
    date: 2008-11-18
    body: "First, a big thank you Danny.\n\nNow on to the suggestions :\nThere is one thing that bugs me at every commit digest wich comes late, it is the mismatch between the title ( for 12th october ) and the first words ( in this week ). Would it be possible to avoid it please ?\n\nThe other thing I wonder is \"how to help you ?\". would you need someone to keep track of their pet module svn commit message and to send you the most interesting ones at the end of the week ? Would you rather need someone to write the summary ? Or some bit of infrastructure to get collaborative commit digest editing ? Or all of the above :)\n\nWell, don't hesitate to give a hint, you might get a few diggesters by your side."
    author: "kollum"
  - subject: "Re: Thanks and suggestions"
    date: 2008-11-19
    body: "> There is one thing that bugs me at every commit digest wich comes late, it is the mismatch between the title ( for 12th october ) and the first words ( in this week ).\n\nThat mismatch only exists in your head. The \"this\" you point to never referred to the \"current\" week, it always referred to the week ending with the date in the title. Being a digest it could never hope to look in the future to cover a \"current\" week even if that were technically possible."
    author: "Anon"
  - subject: "Backport of the new Systray to 4.1.x?"
    date: 2008-11-18
    body: "Hi,\n\ncould it be possible to backport the new systray to kde 4.1.x? Perhaps 4.1.4? The current one is a real pain and is rendered terribly buggy and causes several other plasma bugs.\n\nOther than that kdenlive looks really promising and KDE 4.2 is really taking shape.\n\nCheers"
    author: "knue"
  - subject: "Re: Backport of the new Systray to 4.1.x?"
    date: 2008-11-18
    body: "i think backporting in KDE itself will only happen in extreme cases.  only at the very begining of 4.0 with plasma being so new.  i doubt you'll see any kde backporting for the remainder of 4.\n\ni think you'd have better luck getting this from your distro.  but, its really only two months away :)"
    author: "tooth"
  - subject: "Re: Backport of the new Systray to 4.1.x?"
    date: 2008-11-19
    body: "I am certain that distros like openSuse will do so..."
    author: "Sebastian"
  - subject: "Wiki!"
    date: 2008-11-18
    body: "How about publishing the digest in a wiki? That way, everyone could contribute!"
    author: "Wiki!"
  - subject: "Re: Wiki!"
    date: 2008-11-18
    body: "or, there could be special commit keyword, opposite to SVN_SILENT - SVN_DIGEST"
    author: "Nick Shaforostoff"
  - subject: "Re: Wiki!"
    date: 2008-11-18
    body: "I second this... that would cut down on sorting them out.\n"
    author: "Sam Weber"
  - subject: "Re: Wiki!"
    date: 2008-11-18
    body: "Set one up, get contributors. Readers are almost automatic.\n\nThere is a huge amount that Danny doesn't cover, and I'm quite sure he would enjoy someone else's writing about KDE.\n\nDerek (beware though, readers like things on a reasonably regular schedule. This project may eat you alive :)"
    author: "dkite"
  - subject: "Nice touch from the Gnomes..."
    date: 2008-11-18
    body: "...that they paid homage in the title, and acknowledged the source of the idea in their Gnome commit digest.\n\n"
    author: "Gerry"
  - subject: "Re: Nice touch from the Gnomes..."
    date: 2008-11-19
    body: "And they use a very nice and minimalistic web design... "
    author: "Sebastian"
  - subject: "Thanks for the heads-up"
    date: 2008-11-18
    body: "Hi danny, thanks for the heads-up on kdenlive. I almost forgot about it and from what it seems, it just seems mature enough to start editing my holiday DV's into a nice clip.\n\n(and report bugs on the occasional beta-crash :) )"
    author: "David"
  - subject: "Thanks"
    date: 2008-11-19
    body: "I check kde.org every day to see when these come out and get so excited when they do :)"
    author: "Andrew"
  - subject: "Re: Thanks"
    date: 2008-11-19
    body: "I used to check it out daily.\n\nNow I don't have the time, but I still check it out weekly.\n(I feel that the buzz around KDE 4.X died down a bit. :( Hope this is just temporary. We should really show off our support to KDE and the world, before Windows 7 makes inroads. )\n"
    author: "Mik"
  - subject: "Leave Danny alone"
    date: 2008-11-19
    body: "I rather have him and others code, than write digests.\n\nThere is only about a month left, or two before KDE 4.2, and I expect as much publicity, if not more from it when it comes out.\n\nFor that to happen we need:\n\nTons of new features added\nEye candy added (sells well in blogs)\nWindows and Mac versions functional\nAmarok 2 working (with a Genius like feature and full Ipod support)\n\nA keynote address on video like last year. Would google be able to host again?\nKeynotes are what sells Apple, I see no reason why keynotes wouldn't be able to \"sell\" KDE.\n\nLook up how many times last years keynote has been watched.\n\n____\nLet us know if we can help.\nDo you need a donation drive to pay for renting camera/webcasting equipment?\nOffice location?\nWho else is interested? Who else can help?\n\n"
    author: "Mik"
  - subject: "Danny - request for more Amarok coverage in Digest"
    date: 2008-11-19
    body: "Danny, (no rush) when you get around to making another digest, could you emphasize AMAROK 2 some more? You're doing a great job, but I would love to hear more about it.\n\nI feel that Amarok is the \"KILLER APP\" for Linux/KDE and is what will help spread open source. (kind of how the Ipod saved Apple.) Especially when it's cross platform in the near future. If we can get people hooked on Amarok - KDE on Windows/Mac, maybe we can slowly move them over to KDE on Linux.. ^_^\n\n"
    author: "Mik"
  - subject: "Re: Danny - request for more Amarok coverage in Digest"
    date: 2008-11-19
    body: "We recently released issue 13 of the \"Amarok Insider\". This should hopefully satisfy your crawing for Amarok 2 news for now :-)\n\nhttp://amarok.kde.org/en/node/565"
    author: "Nikolaj Hald Nielsen"
  - subject: "kdenlive in extragear"
    date: 2008-11-19
    body: "Does the kdenlive team consider moving to extragear ? It would make sense for KDE to have an official video editor and for them to benefit from the kde infrastructure (translations, QA, etc.)"
    author: "cmiramon"
  - subject: "Number of bugs closed"
    date: 2008-11-22
    body: "May I make a small suggestion that instead of reporting the number of bugs closed that you report the number of bugs fixed.\n\nThe reason I am suggesting this is that it has been my experience that developers are a bit trigger happy when it comes to closing bugs without fixing them."
    author: "JRT"
  - subject: "kdetv port started!!??"
    date: 2008-11-25
    body: "whooaaaa!! :) i'm quite interested to learn more about that :)"
    author: "mxttie"
---
In <a href="http://commit-digest.org/issues/2008-10-12/">this week's KDE Commit-Digest</a>: More improvements in <a href="http://edu.kde.org/kbruch/">KBruch</a> as part of a Brazilian student projects initiative. Ability to search by "HD Catalog Number" in <a href="http://edu.kde.org/kstars/">KStars</a>. Initial <a href="http://kross.dipe.org/">Kross</a> support in the Rocs educational tool. Multiple projection support in the <a href="http://edu.kde.org/marble/">Marble</a> Plasmoid. Preliminary support for editors in Klotz (formerly KLDraw). Ability to change the alignment of the window title in the <a href="http://oxygen-icons.org/">Oxygen</a> window decoration. Animation is added to the "boxswitch" KWin-Composite effect. More new features in <a href="http://amarok.kde.org/">Amarok</a> 2.0. A configurable zoom slider is added to the <a href="http://dolphin.kde.org/">Dolphin</a> status bar. A simple new table-based "annotation display widget" in <a href="http://nepomuk.kde.org/">NEPOMUK</a>. The "reset search bar when changing feeds" feature returns to <a href="http://akregator.sourceforge.net/">Akregator</a>. More progress on the KJots rewrite effort (<a href="http://pim.kde.org/akonadi/">Akonadi</a> integration and Plasmoid). Improved keyboard navigation usability in <a href="http://koffice.kde.org/kplato/">KPlato</a>. UI effects configuration options added to the "TweakKDE" utility. Various <a href="http://plasma.kde.org/">Plasma</a> applets, including "Previewer", "Web Browser", and the new "System Tray" move to kdereview in time for KDE 4.2. Killbots moves to kdereview/games. The <a href="http://commit-digest.org/issues/2008-10-05/#2">rewritten webarchiver tool</a> for <a href="http://www.konqueror.org/">Konqueror</a> is imported into KDE SVN. KDETV starts to be ported to KDE 4. <a href="http://koffice.org/">KOffice</a> 2.0 <a href="http://dot.kde.org/1224696725/">Beta 2 is tagged for release</a>. <a href="http://commit-digest.org/issues/2008-10-12/">Read the rest of the Digest here</a>.

<!--break-->
