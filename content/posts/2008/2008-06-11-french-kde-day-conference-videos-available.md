---
title: "French KDE Day Conference Videos Available"
date:    2008-06-11
authors:
  - "tpetazzoni"
slug:    french-kde-day-conference-videos-available
comments:
  - subject: "Direct link to Aaron's video"
    date: 2008-06-11
    body: "http://streaming.linux-magazin.de:8080/linuxtag08/aseigo.ogg"
    author: "yman"
  - subject: "Re: Direct link to Aaron's video"
    date: 2008-06-11
    body: "Thanks - I was hoping someone would post something like this!"
    author: "Anon"
  - subject: "Re: Direct link to Aaron's video"
    date: 2008-06-13
    body: "Thanks for posting a link to the ogg as I didn't want to set up Java for this.\n\nIf you are like me, you would also want the images of the slides. I found out that they have are URL like:\nhttp://streaming.linux-magazin.de/events/linuxtag08/archive/aseigo/slides/1211974694.jpg\nAnd a list of ids:\nhttp://streaming.linux-magazin.de/events/linuxtag08/archive/aseigo/timecodes.json\n\nA small python script that gets all the images:\nhttp://pastebin.com/f14038bf4"
    author: "Daniel"
  - subject: "French ;("
    date: 2008-06-12
    body: "The world would such a simple place if the french would put more effort in learning english :-P"
    author: "Letroll"
  - subject: "URL link error"
    date: 2008-06-12
    body: "Hi, there's an error in the first URL (that of K\u00e9vin Ottens). It lacks a /pub/ following the hostname. Thus the correct one is http://www.toulibre.org/pub/2008-01-25-soiree-kde/video/kevin-ottens-kde4-un-nouvel-environnement-pour-votre-ordinateur.ogg"
    author: "Orestes Mas"
---
To celebrate the release of KDE 4, the KDE French contributors and the <a href="http://www.toulibre.org">Toulibre</a> LUG organised a two-day event on January 25th and 26th 2008 in Toulouse, France. On the 25th, Kévin Ottens made a general presentation of KDE 4, and on the 26th there was a day of technical conferences featuring speakers such as David Faure, Laurent Montel, Alexis Ménard, Kévin Ottens, Aurélien Gâteau and Anne-Marie Mahfouf. The videos of all these talks, in French, are now available for download.







<!--break-->
<p>Here is the list of available videos, with their original french titles:</p>

<ul><li><i>Présentation de KDE 4</i> by Kévin Ottens, <a href="http://www.toulibre.org/2008-01-25-soiree-kde/video/kevin-ottens-kde4-un-nouvel-environnement-pour-votre-ordinateur.ogg">vidéo Ogg Theora (207 Mb, 58 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-25-soiree-kde/keynote-toulouse.pdf">slides</a>. We also have <a href="http://www.toulibre.org/pub/2008-01-25-soiree-kde/video/aaron-seigo-kde4-keynote-at-google.ogg">Aaron Seigo's keynote</a> using the <a href="http://www.toulibre.org/pub/2008-01-25-soiree-kde/video/aaron-seigo-kde4-keynote-at-google.srt">french subtitles</a>, a <a href="http://www.toulibre.org/pub/2008-01-25-soiree-kde/video/kde-commercial-1920.avi">commercial for KDE 4</a> and a <a href="http://www.toulibre.org/pub/2008-01-25-soiree-kde/video/kde4-kwincomposite.avi">demonstration</a>.</li><li><i>Contribuer à KDE, Bienvenue à tous !</i> by Alexis Ménard, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/alexis-menard-contribuer-a-kde-bienvenue-a-tous.ogg">vidéo Ogg Theora (97 Mb, 26 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-contribution-kde.pdf">slides</a></li><li><i>Présentation de Qt</i> by David Faure, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/david-faure-presentation-de-qt.ogg">vidéo Ogg Theora (210 Mb, 43 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-qt.pdf">slides</a></li><li><i>Tour d'horizon de CMake</i> by Laurent Montel, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/laurent-montel-presentation-de-cmake.ogg">vidéo Ogg Theora (140 Mo, 36 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-cmake.pdf">slides</a></li><li><i>Solid: intégration avec le matériel sans utiliser d'aspirine</i> by Kévin Ottens, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/kevin-ottens-solid-integration-avec-le-materiel-sans-utiliser-d-aspirine.ogg">vidéo Ogg Theora (302 Mb, 1h and 3 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-solid.pdf">slides</a></li><li><i>Plasma: une nouvelle approche du gestionnaire de bureau</i> by Alexis Ménard, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/alexis-menard-plasma-une-nouvelle-approche-du-gestionnaire-de-bureau.ogg">vidéo Ogg Theora (112 Mb, 28 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-plasma.pdf">slides</a></li><li><i>Les feuilles de style de Qt</i> by Aurélien Gâteau, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/aurelien-gateau-les-feuilles-de-style-de-qt.ogg">vidéo Ogg Theora (201 Mb, 49 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-qt-css.pdf">slides</a></li><li><i>Phonon: multimédia facile pour vos applications</i> by Kévin Ottens, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/kevin-ottens-phonon-multimedia-facile-pour-vos-applications.ogg">vidéo Ogg Theora (157 Mb, 39 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-phonon.pdf">slides</a></li><li><i>Guide de contribution à la traduction de KDE</i> by Anne-Marie Mahfouf, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/anne-marie-mahfouf-guide-de-contribution-a-la-traduction-de-kde.ogg">vidéo Ogg Theora (87 Mb, 31 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-traduction.pdf">slides</a></li><li><i>Présentation KIO</i> by David Faure, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/david-faure-presentation-kio.ogg">vidéo Ogg Theora (271 Mb, 42 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-kio.pdf">slides</a></li><li><i>Des logiciels éducatifs dans KDE</i> by Anne-Marie Mahfouf, <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/video/anne-marie-mahfouf-des-logiciels-educatifs-dans-kde.ogg">vidéo Ogg Theora (91 Mb, 28 minutes)</a> and <a href="http://www.toulibre.org/pub/2008-01-26-journee-kde/presentation-kde-edu.pdf">slides</a></li></ul>

<p>Meanwhile for English speakers Aaron Seigo's talk from LinuxTag <a href="http://streaming.linux-magazin.de/archiv_linuxtag08.htm">KDE 4: Desktop interfaces in a mobile Web 2.0 world</a> is available in video form.  And coming up this Sunday is <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a> with IRC talks on Usability, Plasma with Python and more.</p>





