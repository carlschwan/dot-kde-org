---
title: "KDE Commit-Digest for 24th August 2008"
date:    2008-09-01
authors:
  - "dallen"
slug:    kde-commit-digest-24th-august-2008
comments:
  - subject: "Thanks Danny "
    date: 2008-09-01
    body: "Always pleased to both read the digest and continue a tradition"
    author: "Gerry"
  - subject: "Re: Thanks Danny "
    date: 2008-09-01
    body: "+1\n\nBTW: Diffs seems to be broken here and there:\n\n http://websvn.kde.org/trunk/KDE/kdebase/workspace/plasma/plasma/panelview.cpp?r1=850955&r2=850956\n\nOther diffs works fine:\n\nhttp://websvn.kde.org/trunk/KDE/kdegames/ksirk/ksirk/GameLogic/gameautomaton.cpp?r1=848522&r2=848523"
    author: "Claus Rasmussen"
  - subject: "Re: Thanks Danny "
    date: 2008-09-01
    body: "Holy crap Danny! You're amazing here!\nThanks again!"
    author: "Steve"
  - subject: "Raptor Menu"
    date: 2008-09-01
    body: "I wonder when will i can start to use Raptor Menu. I can't find any build, and there are no official release yet\n\nI've tried Lancelot to replace kickoff and find out that it's just like kickoff without click, so i think Raptor is my last hope  "
    author: "m_goku"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: "That's not true. Lancelot has many more differences than Kickoff. While they both occupy a single window without submenus extending across the screen, Lancelot splits the view so you can navigate to sub-menus from sub-sub-menus just as quickly as you could with the old KMenu. The lack of this was my main beef with Kickoff."
    author: "Jonathan Thomas"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: ">>Lancelot has many more differences than Kickoff\nIndeed..., and personally i think Lancelot look prettier than kickoff\n\nHowever its still look and feel like Kickoff. I think both of them are a good Application launcher, but right now i am looking for something radically different\n"
    author: "m_goku"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: "I must agree that I am eagerly awaiting Raptor. But for now, Lancelot is different enough in a good way compared to the classical menu for my needs. For me, the differences between the classical menu and Kickoff didn't improve the experience for me but rather impended it. Lancelot solves the problems I feel Kickoff had plus has some neat extra functionality. (The Contacts pane and the device pane for example)"
    author: "Jonathan Thomas"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: "I think that both could learn a few things from each other. Kickoff could learn to handle submenus like Lancelot and Lancelot could learn to... well I am not quite sure ;) Anyway Lancelot is coming on quite fine."
    author: "Bobby"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: "If you are just looking for something radically different without specific desires, it might turn out that the radically different thing is not at all better for you than the current ones."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: "On the other hand, AFAIK Raptor is not planned to make navigating in submenus like in the traditional K-Menu easy."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Raptor Menu"
    date: 2008-09-01
    body: "The last time I looked at Lancelot it was actually better than Kickoff even though I don't personally have a problem with Kickoff. Lancelot's usability is really better. I am curious about Raptor though."
    author: "Bobby"
  - subject: "Catching Up"
    date: 2008-09-01
    body: "Congratulations on finally catching up, Danny!\n\nI'm sure it was disheartening having fallen so far behind earlier this year, but you've powered through them and you've now caught up to almost real time. I'm thoroughly impressed.\n\nKeep up the great work."
    author: "Parker"
  - subject: "Re: Catching Up"
    date: 2008-09-01
    body: "Well, at this rate Danny will have the digests for September-December done by Oct. 1. Covering the past is just too \"easy\" (hah!), soon I think we're going to see just how well Danny can predict the future...someone needs to start KFortuneTeller to help him out!\n"
    author: "T"
  - subject: "Re: Catching Up"
    date: 2008-09-01
    body: "Then the developers will have to catch up with the features described by Danny. :)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Catching Up"
    date: 2008-09-01
    body: "The developers just follow the builtin commit links and apply the future patches."
    author: "Erik"
  - subject: "Re: Catching Up"
    date: 2008-09-01
    body: "> someone needs to start KFortuneTeller to help him out.\nWith the newer naming scheme wouldn't it be called something like Alakazam.\n"
    author: "Michael \"Wow!\" Howell"
  - subject: "Re: Catching Up"
    date: 2008-09-01
    body: "From me too congrats. He did a really fine job :)"
    author: "Bobby"
  - subject: "Hope for Designers!"
    date: 2008-09-01
    body: ">>>We also plan to support other declarative languages, something like CSS for example. This would make the designer's life even easier and we could support QWidget's styles also.\n\n\nThat would be awesome. An easy way to create KDE styles with all the fantastic and beautiful animations EFL is able to bring us (Just take a look at the enlightenment default theme and you'll see). No need to code C++ and stuff... the designer's dream ...\n\nPlease make this dream come true, guys!\n\nKeep up the excellent work. You rock!\n\n"
    author: "SVG Crazy"
  - subject: "Re: Hope for Designers!"
    date: 2008-09-01
    body: "I had opened a bug/wish for qss but was told it was not feasible.\nGood to see there is a hope for that. Theming qt apps with qss is one of the great innovations in qt4. It would help a lot if I could, for example, just set the button padding in the qss file and then kde would use much less space in my limited eeePC screen :)"
    author: "Iuri Fiedoruk"
  - subject: "Great work Danny!!! and..."
    date: 2008-09-01
    body: "very soon KDE developers will work very hard to catch up with commit digests :-) :-)"
    author: "OC"
  - subject: "Konsolator... NO, PLEASE!"
    date: 2008-09-01
    body: "Please, change that name ASAP.\n\nYou are going to get a load of laughs of your Spanish-speaking mates, because you named a very useful Plasmoid like... a dildo (\"consolador\" is the Spanish word for \"dildo\")"
    author: "Alejandro Nova"
  - subject: "Re: Konsolator... NO, PLEASE!"
    date: 2008-09-01
    body: "Well, what about the KOffice's flake concept, then? See here: http://www.urbandictionary.com/define.php?term=flake\n\nI'm sure there are loads of other examples."
    author: "Planetzappa"
  - subject: "Re: Konsolator... NO, PLEASE!"
    date: 2008-09-01
    body: "c'mon \"flake\" as in \"cornflakes\" means something in some slang is not the same as meaning dildo in \"the world's second most-spoken language by native speakers\" (wikipedia), spanish.\n\ni like consolator over dildo actually as it hints what it is used for..\n"
    author: "cies"
  - subject: "Re: Konsolator... NO, PLEASE!"
    date: 2008-09-01
    body: "Also in portuguese (at least brazilian) it can be used \"consolo\", and also can mean the same thing :)\nEven without this meaning the name is bizarre, better thing out alternatives."
    author: "Iuri Fiedoruk"
  - subject: "Re: Konsolator... NO, PLEASE!"
    date: 2008-09-01
    body: "Yes, I was already laughing! It is indeed a dildo\n"
    author: "Jose"
  - subject: "Re: Konsolator... NO, PLEASE!"
    date: 2008-09-01
    body: "Yes please!!! Looks like a very useful plasmoid, but with this name I can't even speak about it with anothers linuxers :-)."
    author: "Christian"
  - subject: "Re: Konsolator... NO, PLEASE!"
    date: 2008-09-08
    body: "Dude, I had a chuckle when I saw about \"Konsolator\".\n\nKonsolator, NO WAY!"
    author: "NabLa"
  - subject: "Its KDevelop 4 not KDevelop 2"
    date: 2008-09-01
    body: "Hey Danny. Small mistake in the KDevelop related sentence. KDevPlatform is the basis for KDevelop _4_ not _2_ :)"
    author: "Andreas Pakulat"
  - subject: "Re: Its KDevelop 4 not KDevelop 2"
    date: 2008-09-03
    body: "That depends on how you version. Konqueror, for example, never existed in KDE1, so Konqueror KDE4 is Konqueror3. KDE3 used Konqueror2 and KDE2 had Konqueror1. KDevelop, I guess, did not exist in KDE2, so KDE3 had KDevelop1 and KDE4 KDevelop2.\n"
    author: "Michael \"version\" Howell"
  - subject: "The funniest digests"
    date: 2008-09-01
    body: "are the ones the weeks following a release :) They give you an image of what you're waiting for..."
    author: "Linus"
  - subject: "Konsolator question"
    date: 2008-09-01
    body: "Can it be used as a containment? Could be useful. :-)"
    author: "Svempa"
  - subject: "limiting size"
    date: 2008-09-04
    body: "Limiting the size of the trashcan is nice.  Is there something similar for limiting the size of the thumbnails directory?  That thing can get pretty large."
    author: "MamiyaOtaru"
---
In <a href="http://commit-digest.org/issues/2008-08-24/">this week's KDE Commit-Digest</a>: First steps towards autohide and windows-cover-panel in <a href="http://plasma.kde.org/">Plasma</a>, and support for auto creation of Plasmoids based on the type of file dropped on the desktop. "Konsolator", a new <a href="http://konsole.kde.org/">Konsole</a> Plasma applet, and "Unit Converter" and "QEdje" Plasmoids. A runner for searching in the "Recent Documents" history. Initial attempt at previews-in-tooltip for <a href="http://www.konqueror.org/">Konqueror</a> and <a href="http://dolphin.kde.org/">Dolphin</a>. Configurable support for size limits in the Trashcan (and trash:// KIO slave). More bug fixes for Kicker on KDE 3.5. More work on version control interfaces in kdevplatform (the basis for <a href="http://www.kdevelop.org/">KDevelop</a> 4). Ability to save as a PNG image in <a href="http://edu.kde.org/kturtle/">KTurtle</a>. Jigsaw patterns in Palapeli, start of a new skin editor in <a href="http://home.gna.org/ksirk/">KSirK</a>. Sound effects (using <a href="http://phonon.kde.org/">Phonon</a>) and a new theme in Kapman. A new default theme in KBounce. Various work in Darkroom, including access to different export codecs. Configuration work in KWin-Composite, especially for cylinder and sphere effects. Option for Compiz-like "mouse dragging in cube" effects. KDED module for Phonon for handling audio devices. Support for ejecting devices in the <a href="http://ivan.fomentgroup.org/blog/2007/10/27/lancelot-revealed/">Lancelot</a> alternative menu. Work on indexing web sites in <a href="http://nepomuk.kde.org/">NEPOMUK</a>. Start of integration with NEPOMUK in <a href="http://www.mailody.net/">Mailody</a>, with a move from KHTMLPart to <a href="http://webkit.org/">Webkit</a> for displaying HTML emails. Work on inline track info editing, and the ability to play a track directly off of an MTP device in <a href="http://amarok.kde.org/">Amarok</a> 2. NX resume sessions support, and improved scaling behaviour in KRDC. A 7zip plugin for Ark. Various improvements (and a move to kdereview) for PowerDevil. Beginnings of a "download order script" in <a href="http://ktorrent.org/">KTorrent</a>. Configuration dialog for selecting presentation slides in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. Porting to Eigen2 (from Eigen1) across <a href="http://koffice.org/">KOffice</a> 2. Scriptable GUI plugins, and an RSS reader script plugin, in the Shaman package manager. Import of "Twine", a tool for generating and updating Python bindings from C++ headers, into KDE SVN. Import of a KDE 4 port of Guidedog, a tool for setting up connection sharing, basic routing, and Network Address Translation (NAT), into KDE SVN. Import of an "Asciiquarium" screensaver into playground/artwork. system-config-printer-kde is added to kdereview. <a href="http://dot.kde.org/1219751598/">KDE 3.5.10 is tagged for release</a>. <a href="http://commit-digest.org/issues/2008-08-24/">Read the rest of the Digest here</a>.


<!--break-->
