---
title: "KDE Commit-Digest for 29th June 2008"
date:    2008-08-01
authors:
  - "dallen"
slug:    kde-commit-digest-29th-june-2008
comments:
  - subject: "Go, Danny, Go!"
    date: 2008-07-31
    body: "just ~22hours since the last one :)"
    author: "Robin"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-07-31
    body: "He's a machine!"
    author: "Jonathan Thomas"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-01
    body: "Thank you Danny :)"
    author: "DanaKil"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-01
    body: "Just another one of your loyal readers chiming in :-)\n3 digests in one week? That's some serious work, well done!\n\nThanks for taking on this project; I'm a regular reader of PlanetKDE and I still manage to find about all sorts of cool stuff going on in the digests.\n\nHere's to hoping one day I'll have the time to give something back myself!"
    author: "S\u00e9rgio Gomes"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-01
    body: "Thanks a lot Danny. You're doing everybody an awesome service."
    author: "Matt Boehm"
  - subject: "Give back to KDE?"
    date: 2008-08-04
    body: "You can always join BugSquad. ;)"
    author: "blauzahl"
  - subject: "Give back to KDE?"
    date: 2008-08-06
    body: "You can always join BugSquad. ;)"
    author: "blauzahl"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-01
    body: "Thanks Danny :D"
    author: "spawn57"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-01
    body: "Wow Danny- that's amazing!\nThanks so much for doing this!"
    author: "Steve"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-01
    body: "Impressive ! Your efforts are greatly appreciated. \nThank you."
    author: "shamaz"
  - subject: "Re: Go, Danny, Go!"
    date: 2008-08-02
    body: "Danny, You will catch up. You will catch up. You will catch up. You will catch up. You will catch up. You will catch up. You will catch up. You will catch up.\n\nNo, I didn't cut-and-paste.\n"
    author: "Michael \"Impressed\" Howell"
  - subject: "webkitkde google soc status?"
    date: 2008-08-01
    body: "It would be good to have some news report about the webkitkde google soc to build a webkitpart compatible with KHTML. The svn repo seems very active but its author doesn't seem to blog too much about it."
    author: "Brad"
  - subject: "Re: webkitkde google soc status?"
    date: 2008-08-05
    body: "Right now, they've got a QWebView embedded into Konqueror, using KIO, and right now most of the work is on integration (back-and-forward, KWallet, etc).\n"
    author: "Michael \"I know\" Howell"
  - subject: "Basket & git"
    date: 2008-08-01
    body: "First, it is nice to see people stepping in and help porting applications that would be sad to lose. With Basket porting started, and Tellico appearently also, this means two excellent applications that even my grandma could use are going to be available for KDE 4. This is excellent news! I was thinking about starting a KXMLEditor port, since I use it sometimes, but I don't know if it is worth the efford just for me alone.\n\nKnowing that the switch from cvs to subversion was a huge and tedious task, I feel a bit afraid to ask in what time frame we could expect a switch to git. As far as I understand it, git offers enormous advantages, but does not support all features needed to replace subversion for KDE. Could someone in the know shed some light? Appearently, the new Basket maintainer would see it as a major drawback to switch from git to subversion.\n"
    author: "christoph"
  - subject: "Re: Basket & git"
    date: 2008-08-01
    body: "Whoot Basket is coming back! One of my now very short list of apps that I need in KDE4 \n\nThey have a IRC chan on freenode at #basket-devel if anyone wants to help out or mull over the brand new fancy possibiltes that can occur when you can tag your notes with contact information from akonadi! :)\n\nGit Rocks by the way. Is anyone looking at hosting a github.kde.org for all the projects that have git repos?"
    author: "DaSkreech"
  - subject: "Re: Basket & git"
    date: 2008-08-01
    body: "I think a good XML editor would be nice too, though I am not sure if maybe Quanta could be used for that as well. "
    author: "Andr\u00e9"
  - subject: "Re: Basket & git"
    date: 2008-08-02
    body: "Yeah I'm really happy with the porting of Basket as well. It's an amazing application, and it's really productive for me to track my todo's (in Getting Things Done (TM) style).\n\nIf I may post two feature requests:\n - printing support (I currently have to export to HTML and let Konqueror print it).\n - Sharing / collaboration, to have your TODO's available everywhere (e.g. sync to a \"basket-online\" webapp, or a PDA).\n\nI think those two would improve the productivity a lot more. Other then that, it does everything I need already :)"
    author: "Diederik van der Boor"
  - subject: "Serbia & Montenegro"
    date: 2008-08-01
    body: "On the map, Montenegro has a grey shade indicating that it has 2-10% of commits while Serbia has a grey shade indicating it has 0% of commits. Yet, in the list, &#8220;Serbia and Montenegro&#8221; have 4.28%. So, are all KDE contributors from Montenegro or is there a bug?\n\nOtherwise, congratulations for this stunning digest release frequency those past few days. :)"
    author: "Med"
  - subject: "Re: Serbia & Montenegro"
    date: 2008-08-01
    body: "Oh it seems the dot does not like non latin-1 characters. :("
    author: "Med"
  - subject: "Konsole Bell and Digest"
    date: 2008-08-01
    body: ">The irritating bell sound is disabled by default in Konsole.\n\nRobert Knight is now my hero :P Although I am kinda embarrassed I never even thought to look for a configuration option!\n\n\nAlso, thanks for the great work Danny, it's always exciting to read the Digest and find out all the interesting things happening in KDE-land!"
    author: "Kit"
  - subject: "Re: Konsole Bell and Digest"
    date: 2008-08-01
    body: "In the KDE3 era this was my first step after a clean install. One less thing to worry about :-)"
    author: "Daneel"
  - subject: "KWord 2.0 without Tables !!!"
    date: 2008-08-01
    body: "Will KWord 2.0 be without tables ? It's really sad."
    author: "Zayed"
  - subject: "Re: KWord 2.0 without Tables !!!"
    date: 2008-08-01
    body: "It'll have tables, but they'll be pretty basic."
    author: "jospoortvliet"
  - subject: "Thanks!"
    date: 2008-08-01
    body: "You rock, man!"
    author: "Heja AIK"
  - subject: "Re: Thanks!"
    date: 2008-08-02
    body: "Heja LHC, menar du v\u00e4l?\n\nIf you come to Akademy, we can discuss the finer points of ice hockey. :-)"
    author: "Inge Wallin"
  - subject: "Kexi"
    date: 2008-08-01
    body: "Yay, xBase support, I have a use for that..."
    author: "odysseus"
  - subject: "Thank you!"
    date: 2008-08-01
    body: "When I was first wetting my feet in the waters of KDE development, and started taking an active interest in the community around the desktop, I had two main sources of information: aseigo's blog (until I found out about planet.kde.org), and the commit digest.\n\nAfter lapsing in reading the digest, I saw [ade] mention it, and went back to reading the backlog.  Doing that, I realised how much I missed on what was happening elsewhere in KDE (it's a big project) when I stopped reading, even though I kept up with the planet.\n\nI really appreciate the fact that Danny is catching up properly, rather than just skipping, since otherwise I'd miss out on, say, the progress of ODF support in KWord.  And, I have to say, he's doing an impressive job.  Keep up the good work!"
    author: "Random Guy 3"
  - subject: "Restart after crashing"
    date: 2008-08-02
    body: "Wow that just made my day, great improvement!"
    author: "Diederik van der Boor"
  - subject: "3 cheers for improved ODF support"
    date: 2008-08-03
    body: "I was really happy to see the progress in ODF support. That kind of \"solidifying\" tends to breed more progress: when things are just too broken, it's scary to wade into the morass, but when most things work it makes it easier to tackle the remaining issues.\n"
    author: "T"
---
In <a href="http://commit-digest.org/issues/2008-06-29/">this week's KDE Commit-Digest</a>: Some new wallpapers and an <a href="http://oxygen-icons.org/">Oxygen</a> mouse cursor theme are imported into KDE SVN for the KDE 4.1 release. The KDM login manager gets an Oxygen facelift. Preliminary version of a basic web browser Plasmoid, and a new "ScriptedImage" <a href="http://plasma.kde.org/">Plasma</a> applet. Support for storing <a href="http://amarok.kde.org/">Amarok</a> 2.0 statistics in <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>, more work on the new scripting interface, preliminary support for iPod's, and a partially-working "random mode" restored to Amarok 2.0. The "PopupDropper" interface element of Amarok 2.0 is imported into KDE SVN for future development. Expansion of the D-Bus interface and stats collection functionality of <a href="http://ktorrent.org/">KTorrent</a>. Alignment support in KJots. The irritating bell sound is disabled by default in <a href="http://konsole.kde.org/">Konsole</a>. <a href="http://www.kexi-project.org/">Kexi</a> driver for the xBase family of databases is almost complete. <a href="http://edu.kde.org/kstars/">KStars</a> receives a INDI Astrophysics driver, and tweaks to its zooming user interface. Work on the "Leitner box" mode in <a href="http://edu.kde.org/parley/">Parley</a>. The start of a "SnaKe" mode in the newly-revitalised KTron. Work on a KIO-based network implementation in the experimental <a href="http://webkit.opendarwin.org/">WebKit</a> integration into KDE. An early implementation of storing <a href="http://decibel.kde.org/">Decibel</a> contact data in <a href="http://pim.kde.org/akonadi/">Akonadi</a>. Many KDE applications will now automatically restart after a crash. <a href="http://commit-digest.org/issues/2008-06-29/">Read the rest of the Digest here</a>.

<!--break-->
