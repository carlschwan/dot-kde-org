---
title: "KOffice 2 Alpha 8 Reviewed"
date:    2008-07-04
authors:
  - "tzander"
slug:    koffice-2-alpha-8-reviewed
comments:
  - subject: " Congrats guys!"
    date: 2008-07-04
    body: "Some clarification from my side on the article - http://blog.forwardbias.in/2008/07/koffice-alpha-8.html\n"
    author: "Girish"
  - subject: "sshots..."
    date: 2008-07-04
    body: "Guys, we're looking for volunteers to make screenshots for 'visual changelogs' of each release. \n\nMore info at http://www.kdedevelopers.org/node/3545"
    author: "js"
  - subject: "Screenshots"
    date: 2008-07-04
    body: "\nThis sounds great! We need screenshots for us who dont have space/time/whatever to download and install KDE4daily/SVN etc. I'm so waiting that kword has space around sheet, so it would look more like OO.o or MS Office. I just cant stand how the sheets are together, and I want to get just verification about this usability thing! :-)"
    author: "Thomas.S"
  - subject: "Re: Screenshots"
    date: 2008-07-04
    body: "> I'm so waiting that kword has space around sheet\n\nHmm? KWord has had that for about 3 years now..."
    author: "Thomas Zander"
  - subject: "Re: Screenshots"
    date: 2008-07-04
    body: "It has not have space _around_ sheets, only space on it's sides but not between sheets, what is important. Or then I have not found option to get space between sheets. :-/"
    author: "Thomas.S"
  - subject: "Re: Screenshots"
    date: 2008-07-04
    body: "Ah, got it. Thanks for the clarification :)\n\nKWord can do that too in the 2.0 series. See http://www.koffice.org/kword/pics/200606-KWord-PageSpread.png"
    author: "Thomas Zander"
  - subject: "Re: Screenshots"
    date: 2008-07-05
    body: "Ah... good to know. I hope that space can be adjusted to be bigger than just a few millimeters, like between 1-15mm what can be set from options. Then it would give better usability to sheet view, so user can see more easily the difference of two sheet and how it would look on printed."
    author: "Thomas.S"
  - subject: "styles"
    date: 2008-07-05
    body: "I like the new interface in KWord 2. I think that it could help encourage using semantic markup (head 1, body text, ...) rather than low level visual formatting. Some suggestions to further encourage users to do the right thing:\n\n1) Put the semantic formatting options in more prominent positions, on top etc. For instance, the \"paragraph\" tool tab (which contains semantic formatting) could be the first one, rather than \"style\", which is low-level.\n\n2) Include semantic character styles, like \"emphasized\", \"quote\". Maybe put them on top of the style tool tab, then put the low-level formatting (bold, italic) below a separator line?\n\n3) Include a \"body text\" paragraph style in the standard template. This should include formatting to separate paragraphs (indentation or vertical white space depending on the template). Set this style to be the \"next\" style for all the paragraph styles, except \"standard\". Maybe rename the \"standard\" paragraph style to \"unformatted\" or something that does not encourage its use as the standard style.\n\n4) Include a style browser that lets users browse, and customize, various templates for the standard formatting styles, with a live preview.\n\nI have some more suggestions, but I'll stop here for now.\n\nThanks for some great applications!"
    author: "Martin"
  - subject: "Re: styles"
    date: 2008-07-14
    body: "Those are some good ideas, right there."
    author: "Micah"
  - subject: "Unique"
    date: 2008-07-05
    body: "You do understand what that word means right?"
    author: "fffadas"
  - subject: "Re: Unique"
    date: 2008-07-05
    body: "I'm pretty sure he knows what unique means and that he knows what he is talking about. http://www.merriam-webster.com/dictionary/unique:\n\n1: being the only one \n2 a: being without a like or equal\n2 b: distinctively characteristic\n3: unusual\n\nHTH.\n\nOh I know you want to argue that KOffice is not the only office suite that can be compiled natively from one code base on all available platforms. So please show us one known office suite that has the same feature (hint: It can't be OpenOffice - you cannot compile it as a native app e.g. on Mac from the normal code base).\n"
    author: "Arnomane"
  - subject: "Re: Unique"
    date: 2008-07-05
    body: "> hint: It can't be OpenOffice - you cannot compile it as a native app e.g. on Mac from the normal code base\n\nI think this will be the case for the next major release, or am I mistaken? And KOffice 2.0-final isn't released yet, either."
    author: "panzi"
  - subject: "Reviewed?"
    date: 2008-07-05
    body: "A few screenshots and a single notion that KOffice works slowly.\n\nIs it a review?"
    author: "Artem S. Tashkinov"
  - subject: "Qt Filters"
    date: 2008-07-05
    body: "I'd like to see Trolltech branch out and produce some filters for ODF and Microsoft Office within Qt that all developers and applications can access in a usable fashion. Not only would it be a big step forward for Qt and KDE applications, I'm pretty sure a lot of Qt customers would be happy as well."
    author: "Segedunum"
  - subject: "Re: Qt Filters"
    date: 2008-07-05
    body: "Couldn't KDE just make some as part of KDELibs. A QDoc (or something like that) would be cool (and make some Qt customers happy), but I think KDoc in KDELibs would work just as well."
    author: "Riddle"
  - subject: "Re: Qt Filters"
    date: 2008-07-05
    body: "Perhaps something like a OpenDocument library?\nTake a look here: http://dot.kde.org/1177773146/\n\n"
    author: "Morty"
  - subject: "ODF and MS libraries"
    date: 2008-07-06
    body: "I thought and ODF library was happening.  I have seen it discussed quite a bit.\n\nWhat I'm curious about is the MS support.  Some people claim it isn't important because people SHOULD use open standards.  I'm all for using ODF for my files.  But what about the litany of files I need to open at work in a Microsoft shop?  What about converting existing users' documents, even if they going to use ODF in the future?\n\nHonestly, until you can reasonably import and export the MS binary formats, you can't expect most users to switch.  I use OpenOffice when I can, and people brag up their excellent MS import/export capability, which sadly isn't all that great.\n\nI've read why the MS formats are as complicated as they are.  I understand reverse-engineering a closed, binary format with no help isn't easy.\n\nWhat I don't necessary understand is why any work on deciphering MS formats isn't put into a shared library that everyone can use.  I've seen it asked if KOffice can borrow the superior implementation in this regard from the OOo sources, and each time I've seen a resounding no answered, but I'm not sure why that is.\n\nCan someone please explain why that is?"
    author: "T. J. Brumfield"
  - subject: "Re: ODF and MS libraries"
    date: 2008-07-06
    body: "There is quit a lot of work done to get import-filters for MSOffice done and there is even a gsoc that deals with that topic. That export has lesser priority is imho even logical taken into account that MSOffice 2007 will come with an ODF-import filter. Reusing the OO.org import-/exportfilter is out of scope, just look at the code and you will see what I mean.\n"
    author: "Sebastian Sauer"
  - subject: "Re: ODF and MS libraries"
    date: 2008-07-06
    body: "It's difficult because MS Word files are basically dumps of Word's internal data structures, and OOffice (which inherited their import filters from StarOffice) basically maps them directly to OOfice internal data structures. Trying to adapt this to be useful for KOffice is possibly even more difficult than writing a new filter from scratch (and this is missing manpower).\n\nThere have been several attempts to create a shared library for importing MS stuff, there was one called wvware, which was apparently shared by Abiword, Gnumeric, and the like (i'm pulling this from memory, forgive me if I'm mistaken here). \n\nGetting the basic formatting and fonts right is easy. Getting complex OLE structures and macros out is very very difficult."
    author: "KOffice fan"
  - subject: "Re: ODF and MS libraries"
    date: 2008-07-06
    body: ">there was one called wvware, which was apparently shared by Abiword\n\nAnd also KWord. KWord still uses the descendant, wvware2."
    author: "Morty"
  - subject: "Re: ODF and MS libraries"
    date: 2008-07-06
    body: "If I remember correctly, work on wvware2 was even spearheaded by KOffice developer Werner Trobin."
    author: "Kevin Krammer"
  - subject: "Re: ODF and MS libraries"
    date: 2008-07-06
    body: "> I thought and ODF library was happening. I have seen it discussed quite a bit.\n\nMy guess is that the current priorities are on finishing and stabilizing the current implementation and have such a library as a goal post KOffice 2.0 release.\n\nIt is important to understand that moving a project-internal library to a general library module (e.g. kdelibs) is a lot of work, since one has to remove all the project specific code, adapt the project's code to cope with these changes and put lot of work in API clean up because of the more strict change restrictions of library modules.\n\nI am very confident that everyone involved understands the importance of widespread availability of ODF capabilities, so it can be used as an almost universal format for import and export not just for files but also for clipboard or drag&drop operations.\n\n> What I don't necessary understand is why any work on deciphering MS formats isn't put into a shared library that everyone can use.\n\nAs someone else already wrote, the Free Software office developers (e.g. KOffice, Abiword) have been working on such libraries for quite some time.\nIt is a bit unfortunate that the formerly proprietary code inherited by the OpenOffice.org project wasn't available in such a form and thus couldn't be contributed to a common effort.\n\nAt some point it might be possible for the OOo project to leverage the existance of the shared format ODF to create a stand-alone converter which could then be used by other Free Software projects to tranform legacy formats into ODF and read this.\n\n"
    author: "Kevin Krammer"
  - subject: "Now larger audience has no reason to switch to "
    date: 2008-07-06
    body: "\"The latest KOffice alpha release has attracted extra attention since it ships Windows and Macintosh binaries. This naturally means a much larger audience will be able to try it out and kick the tires.\""
    author: "Unga"
  - subject: "Now larger audience has no reason to switch"
    date: 2008-07-06
    body: "\"The latest KOffice alpha release has attracted extra attention since it ships Windows and Macintosh binaries. This naturally means a much larger audience will be able to try it out and kick the tires.\"\n\nIts good when Koffice runs successfully on Windows and Mac, the so called \"larger audience\" has no reason to switch to more secure, fundamentally correct Unix, Linux, FreeBSD, etc. No wonder Microsoft offer full support for Blender developers to get the Blender run successfully on Windows.\n"
    author: "Unga"
  - subject: "Re: Now larger audience has no reason to switch"
    date: 2008-07-06
    body: "You just said why they would switch to *nix: because it is more secure and fundamentally correct."
    author: "Michael \"Unga is a troll\" Howell"
  - subject: "Re: Now larger audience has no reason to switch"
    date: 2008-07-07
    body: "Don't forget, once they're using cross platform software it becomes easier and easier to switch. You may not dump your current OS, but in the future your next system could be one running all free software (few people manually install windows themselves, expecting them to install a Linux distro is IMO not realistic).\n\n*Nix based systems are slowly becoming more common and popular (as options from manufacturers) on non-server systems, so it's becoming more and more realistic to use this strategy. Does anyone really expect people (especially non-techies) to manually install a different *operating system* just so they can use an application they've never used or possibly even seen or heard of before, giving up most all software they're used to using?"
    author: "Kitsune"
  - subject: "Why is this project necessary"
    date: 2008-07-06
    body: "Hello Guys,\nWhile I admire the energy/effort being placed in this product I would think that with other good/existing open source office suits (OpenOffice comes to mind) that it would be beneficial for the KDE team to focus their energy on putting together a spectacular desktop and use \"all their members/developers\" to ensure that KDE4.1 is built as quickly (and stable) as possible.\n\nI doubt that Koffice will change the way people use office suites and doubt even more that it will have as big an impact/adoption as openoffice 3.0.\n\nWhile I admire the tenacity (i really thought the project would lose steam) I think someone should suggest that a moratorium be imposed on extraneous elements of KDE until 4.1 is complete and stable (enough).\nMy question is simply this, does anyone believe Koffice really is necessary (now) and whether it would not be better that some (smaller) effort is spent to integrate/extend OpenOffice (if even a custom version of OO)?\n\nI like what i see with Kword,Kspread and Kpresenter and I am sure it holds up well with regards to memory use and hence would be practical on small (cheap) laptops. However, in an era of Google docs, 280slides.com [http://280slides.com/Editor/ ] and ever more powerful RIAs (using AIR and JavaFX) I see most of the current desktop apps being made irrelevant in the next few years (especially when ODF and OOXML take root).So the question is how much return on investment (time and energy) will be obtained from devoting so much time to this suite (possibly a silly statement for an open source project). \nTherefore, I would think that a well designed and scalable desktop with properly defined interfaces would be the better place to put focus.\n\n-Kurt [BP]\nP.s. \nAlso there is a cool graphics app written for the AIR runtime and funny enough the performance is already quite good and with Adobe launching Acrobat.com [http://www.adobe.com/acom/] I think we will soon see internet delivered applications comparable to full desktop suites very soon.\n"
    author: "Anon"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-06
    body: "This is assuming three things:\n1) That all of the developers care about the desktop: some developers are on Windows and can't/don't use the KDE desktop. It is unreasonable to ask them to develop something they can't test!\n2) That all developers want to and can develop the desktop: some developers are not familiar with and don't want to become familiar with the desktop code base. Others simply are better at developing office suites than developing desktops. Again, it would be unreasonable to ask them to develop something they don't want to develop.\n3) That the office suite does nothing for the desktop: at the very least, KOffice helps test/refine the platform as used in different situations other than the desktop. At the very most, the desktop may make use of platforms developed as part of KOffice: for example, Kross started as being developed in KOffice. Other platforms, such as Flake and Pigment, may eventually be moved into the KDE libraries as well.\n"
    author: "Michael \"Replying to an honest question\" Howell"
  - subject: "Why?"
    date: 2008-07-06
    body: "1) Free software developers code for what they want to because they want to. Only a small percentage are paid for it. What this means is that you can't magically reassign a team working on one project to another project. They're working in their spare time, usually from 10-20/hrs/week *on top* of whatever else they're going (ie full-time job + family + life). \n\nIf you think you're going to tell one group of people to go code on whatever KMagicPony of the week there is, you're crazy. \n\n2) I think that KOffice actually existed before OpenOffice. So maybe all the OpenOffice devs should join us? ;)\n\n3) People seem to like saying that choice is good, so I might as well repeat that. Although I'd say that KOffice is probably good for KDE because of its tight integration, etc. If we really wanted to, we could probably outsource most of the desktop to elsewhere and other projects, but then we cease to be KDE. "
    author: "blauzahl"
  - subject: "Re: Why?"
    date: 2008-07-07
    body: "Thank you, I wholeheartedly agree with this sentiment.\n\nGood on the KOffice team, I for one, will use it.\n\n(I know people support Open Office due to their support of legacy formats, and the \"only game in town\" mentality, but viewed in its own merits I feel it no amount of \"push\" is gonna make a nimble nymph out of it)\n\nThe original question really grates my carrot, when people like this attain positions of power, we as open source users/devs, are porked"
    author: "Donald Carr"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-06
    body: "> Adobe\n\nThose Adobe that fails since years to compile a 64bit Flash? :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-06
    body: "AIR is an interesting example. it uses WebKit. perhaps you would've said the same thing about khtml 5 years ago as you just did about koffice. perhaps you even did. but had khtml not been around there also wouldn't be a WebKit and maybe AIR wouldn't've even found its feet as a result.\n\nso why koffice?\n\nbecause it was there first (before any other open source office suite), because the developers want to, because there's nothing like some of the apps in the foss world (kexi and krita in particular) and because it runs well on very modest hardware (when's the last time you saw OOo on a handheld device?).\n\nwho knows what koffice will become in the future.\n\nand to be honest, unless you or i are working on it, it's none of our business to question it either because it's not our investment of time, energy or money.\n\nfinally, the \"put more people on it and it will go faster\" thing is a complete myth, just as the \"if you make people not work on Y then they'll naturally work on X\" idea is a myth.\n"
    author: "Aaron Seigo"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-06
    body: "Right on Aaron! And let's not forget the demise of rationality in zero sum economics. People assume a developer working on project B takes a developer away from project A. Why should Linus Torvolds have continued working on his kernel after he met RMS instead of going to work on the herd kernel? In fact the more developers work on different projects, the more different projects developers start. Maybe some ought to see if there is one in line with their thinking before starting, but it is absurd to think developers could be shuffled off to other projects by some centralized big brother. Free software is about freedom and choice. In fact the idea of terminating a project that a number of users love because it has failed to achieve a majority of users is really rather fascist. What standard or timeline should be used? Do we go for a monolithic program and eliminte choice? I thought that is why people like me were using GNU/Linux... \n\nFinally, users refer to developers as if they were born developers, when in fact most developers learned to become developers to write code on projects they were interested in. That's how and why I learned C++. I was a user swept into an infant project when there were many alternative programs to Quanta out there. I wanted a program more to my liking and as a result my program became the most popular in it's class... like other KDE programs.\n\nA better question than why someone is doing something they are contributing to the community is what are you doing and contributing? I view the contribution of any program that any user downloads and enjoys at a substantially higher value than any opinion that does not produce a real benefit to users, thus to the community. It is in our nature as humans to mistake intent for action and think we are doing something when we merely want to have some impact. To be clear, without people giving of themselves to make the software happen the community disolves. So I think the best rule is to never imply someone should not contribute in a given place, because there is no zero sum distribution of programming and they probably won't jump ship from their program. I wouldn't.\n\nWhat is necessary is development on projects. Suggesting otherwise is ludicrous."
    author: "Eric Laffoon"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-08
    body: "and right Eric!\n\nGuess myself would just have left linux and went on to another system if KDE wasn't there in the first place and caught my attention :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-06
    body: "I'd rather have KDE 3.5 with a excellent KOffice. KOffice is the best office suite. OO is so slow and arcane. It's like Cakewalk. You've got layers of old code. KOffice is very fast. I like fast simple software.\n\nBesides, KDE is not communist. People do what they want. ;)"
    author: "winter"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-07
    body: "I agree.\nKOffice is fast, it is simple (from a user's point of view) and it is just doing it's job. What more can one want? \n\nAnd for the really nasty things there is Latex ;-)"
    author: "captain obvious"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-09
    body: "Does that make KDE anarchist then? lol"
    author: "NuclearPeon"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-07
    body: "Seeing apps like Krita there is no doubt the answer is: YES."
    author: "Iuri Fiedoruk"
  - subject: "It is not the same!"
    date: 2008-07-09
    body: "Because one size or fashion does not fit all.\n\nWhy do peaople use MS Works when there is MS Office?\nWhy do people buy Mac when there is DELL?\nWhy do people use Gmail when there is Hotmail?\n\nEtc...\n\nKOffice is not an exact copy of OpenOffice. They seem quite different."
    author: "Axel"
  - subject: "Re: Why is this project necessary"
    date: 2008-07-09
    body: "First of all, KDE applications are meant to function in a KDE environment seamlessly, something that couldn't be achieved in Open Office or other Office Suites. \n\nI personally do not like open office compared to KOffice because the startup times are longer, although this issue is trivial. That aside, KOffice just 'feels' better, less out of place, plus it has that famous KDE functionality going in its favour. \n\nI really dislike Microsoft Office, so having KOffice look different is also a plus. I would prefer that KOffice act more like Kile in regards to formatting. (Kile is a Latex editor, so formatting doesn't show up until rendered)\n\nFor my last point, competition is good for the market and consumers. Who said that the only contenders could be Microsoft Office and OpenOffice? That's a rather narrow way of thinking if you ask me.\n\nPlus I think the Open Office windows port looks horribly ugly. But that's not relevant here.\n\nP.s.\nI work with adobe almost every day, and I'm really unimpressed with the GUI layouts especially in reader and acrobat."
    author: "NuclearPeon"
  - subject: "my unique selling point"
    date: 2008-07-06
    body: "to me the unique selling points of KOffice are a quality FOSS database GUI (kexi), a really cool imaging app (kexi), that it's small and fast and integrates nicely with the rest of my dekstop.\n\nbut that's just me.. =) congrats to the koffice team on spreading your wings further."
    author: "Aaron Seigo"
  - subject: "Re: my unique selling point"
    date: 2008-07-06
    body: "Personally I love Krita. It deserves more attention, especially in light that it's much better than gimp :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: my unique selling point"
    date: 2008-07-07
    body: "Seconded. I'm eagerly waiting for the next iteration of Krita, since I liked the first one quite a lot. (Plus, it's much better integrated into KDE than the GIMP)"
    author: "Luca Beltrame"
  - subject: "Re: my unique selling point"
    date: 2008-07-07
    body: "At the Linux Foundation Summit, an Access-replacement was given as one of the reasons one of the companies didn't switch some of their desktops to Linux. So further Kexi development and promotion is quite important.\n\nNot that Kexi is any use to me personally really. ;) But Krita is a fun app for playing with my small drawing tablet. I'll probably try out Krita 2.0 once it hits beta or so. I look forward to the more natural brushes. "
    author: "Ian Monroe"
  - subject: "Well done"
    date: 2008-07-06
    body: "Great to see that KOffice progresses well (especially on Windows/Mac OS X). With its great technology (clean codebase, well integrated/flake) and its completeness (traditional office apps + krita + karbon +...) it has the possibility to become a major office suite. \n\nThis will for sure require some more helping hands. I am very optimistic that this is achievable due to the attractiveness of the KOffice platform (well designed and inter-platform). A bit more promotion/hype (e.g. something like Troys' \"the road to KDE 4\") could surely help to spread the word, too.\n\nWell done KOffice team and keep up the fabulous work!"
    author: "MK"
  - subject: "keep an eye on kplato"
    date: 2008-07-06
    body: "just to say thank you for your work on kplato, and a personal thank for Danders on his work on EV analysis, "
    author: "mimoune djouallah"
  - subject: "Re: keep an eye on kplato"
    date: 2008-07-07
    body: "Couldn't agree more on this. KPlato is an excellent project planner that you can't find in OpenOffice.org and I also have to say thank you very much to the authors of KPlato."
    author: "Jure Repinc"
  - subject: "How can you not like this"
    date: 2008-07-07
    body: "A guy wants to draw, so he takes over Krita, builds a team. We get a math library out of it. They come up with stuff no one else has, like mixing colors.\n\nWouldn't it be neat to do database stuff? Someone starts writing Kexi. Years, yes years later, we have something pretty cool.\n\nAnd so on. Every app is a story, a reflection of someone, or of many.\n\nThe hubris is awe inspiring. Their persistence is humbling.\n\nI would use these apps for that reason alone.\n\nDerek"
    author: "dkite"
  - subject: "Re: How can you not like this"
    date: 2008-07-07
    body: "Amen."
    author: "taj"
  - subject: "Re: How can you not like this"
    date: 2008-07-08
    body: "Nice post!"
    author: "Borker"
  - subject: "Re: How can you not like this"
    date: 2008-07-09
    body: "+1"
    author: "Michael \"agree\" Howell"
  - subject: "tables "
    date: 2008-07-09
    body: "How far have table support improved in kword.  Last time I checked it was broken.  I can't think of a word processor that does not make good tables.  Any idea ?"
    author: "Ask"
  - subject: "Re: tables "
    date: 2008-07-09
    body: "Table support was removed and will very likely not be reintroduced for 2.0. This time we want to do it right."
    author: "Boudewijn Rempt"
  - subject: "Sugestions"
    date: 2008-07-09
    body: "I've installed and tested KDE for windows at my job (love having kate at hand).\nIt is still a bit chunky and have lots of bugs (acess to drive A:, for example), but shows a lot of potential.\nI just have some wishes I would like to see happen one day:\n- konsole for DOS - yeah right, it would be great, even that I do not think it's plausible\n- organize packages better. Some things like find files could be moved to an utilities package\n- systemsettings: this is an old one, I can't the mouse to use double click without having to mess with config files. Can't we just read windows option and use the same behavior?\n- man, where is Quanta+? :(\n\nOverall looks good. Thanks to all that are making this port become reality."
    author: "Iuri Fiedoruk"
  - subject: "Re: Sugestions"
    date: 2008-07-11
    body: "\"Thanks to all that are making this port become reality.\"\n\nYour're welcome. You'll also say \"thank you\" in a practical way, if you report some of the issues and describe wishes on the bugs.kde.org, and/or discuss them on the kde-windows mailing list (http://www.kde.org/mailinglists/). You'll also discover that most of your points are already on the TODO list of developers, or they are just aware of them.\n\nRegarding konsole, we're depending on the native shell, so all you can have is http://techbase.kde.org/Projects/KDE_on_Windows/Tools#Console_2\n\nthx\n"
    author: "js"
---
KOffice, like most KDE applications, has the unique selling point that the same codebase can be compiled and run on various platforms. The latest KOffice alpha release has attracted extra attention since it ships Windows and Macintosh binaries. This naturally means a much larger audience will be able to try it out and kick the tires. <a href="http://www.techworld.com.au/article/224689/first_look_koffice_2_0_alpha_8">Techworld has an article</a> up where they give a good overview and plenty of screenshots. Have you already tried out a KOffice alpha release?

<!--break-->
