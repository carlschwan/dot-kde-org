---
title: "KDE and Wikimedia Collaborate"
date:    2008-04-04
authors:
  - "skuegler"
slug:    kde-and-wikimedia-collaborate
comments:
  - subject: "wikipedia isn't that hot these days"
    date: 2008-04-04
    body: "Wikipedia has some great content, and I'm all for great free content.  However...\n\nI think this should be in the form of generic plugins for mediawiki, moinmoin, etc., and perhaps some kind of community database of sites and what kind of data they contain, rather than wikipedia-only stuff.  \n\nPersonally, I find wikipedia very elitist now -- you basically can't write anything without it being picked apart by those who obsess on the site and use it regularly.  It's not about making sure information is useful anymore -- it's about making sure information fits with the regular users' preferred information.\n\nThis goes under the guise of helpful \"editing\", but in fact is a lot more like a control freak culture.\n\nAlso, KDE plugins that allow using free content from sites should be able to contribute content to the sites.  I suspect wikipedia would very much dislike that, unless your desktop also became a notification tool for complaints from other wikipedia users."
    author: "Lee"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-04
    body: "I second that"
    author: "Yeap"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-04
    body: "Wikipedia is not to be trusted! \nA wikipedian bureaucracy has been created, \nunder the blessing of the owners (of course). \nI would rather use \"bad\" language to describe wikipedia, \nbut I don't, only out of respect to this site."
    author: "nikos"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-05
    body: "I emphatically agree."
    author: "Kerr Avon"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-04
    body: "Hello,\n\nmy experience and understanding is different: First of all, review and critical review at that, is a good thing. If you can't stand it, you better don't consider yourself capable of public writing. You see, that's somehow like Free Software vs. Closed Software. You can't get everything you want into the Linux kernel. It must have a minimum quality, although not always. And you must have the trust of the people, although not always. In a Closed software, you simply don't have the issue, you can't contribute anything.\n\nWere Wikipedia a failure, a free fork would be on its way already. The same thing is true of Linux kernel, KDE, etc. if there was something wrong, there would be forks. \n\nThat doesn't mean that there isn't a whole lot of people going around who tried to contribute, failed to pass review, and then starting whining.\n\nYou may be one of them. Your prediction on the KDE Wikipedia editor is baseless at least.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-05
    body: "I dont agree with the posters before you, but I also dont agree with you, and your response has prompted me to correct some things:\n\n\n\"Were Wikipedia a failure, a free fork would be on its way already. The same thing is true of Linux kernel, KDE, etc. if there was something wrong, there would be forks.\"\n\nNo. A fork requires time (money, indirectly too), and a LOT OF EFFORT.\nThe bigger a project, the harder a fork would be. Take modular xorg.\nThe fork took a lot of time, and they moved a very difficult and long way. \nHow many \"hobby\" programmers are really working on it compared to people \nhired by companies to do work?\nThe linux kernel is a much better example anyway because they have Linus and \nmore hobby programmers helping than any other project, but I dont think a \nfork would be as easy as you predict here.\n\n\"That doesn't mean that there isn't a whole lot of people going around who tried to contribute, failed to pass review, and then starting whining.\"\n\nWait a second. There is no whining involved here. My comments have been often reverted, so often that I only add stuff to the discussion page now.\nI am not whining. It is your attitude here that is at fault.\n\nPeople expressing VALID CONCERNS may never be said to be \"whining\".\nAnd a lot of the claims are true. I know admins that are nice, i know some that are asses (and this is not so much a personal opinion but rather one where I notice that different admins do things differently on wikipedia.)\n\n\"You may be one of them.\"\n\nStop your ad hominem attacks against him. \n\n\n\"Your prediction on the KDE Wikipedia editor is baseless at least.\"\nThat may be, but your suggestive remark is even more baseless."
    author: "she"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-06
    body: "technically, the linux kernel is being forked everyday by using Git :p"
    author: "Patcito"
  - subject: "Re: wikipedia isn't that hot these days"
    date: 2008-04-05
    body: "I actually don't know anyone that allows their kids to use wikipedia, it is just not a reliable source for information.\n\nmy prediction, KDE and wikimedia will part forces under bad terms in a year or so."
    author: "R. J."
  - subject: "Spread the word"
    date: 2008-04-04
    body: "dugg:\n\nhttp://digg.com/software/KDE_and_Wikimedia_Collaborate"
    author: "kiwiki4e"
  - subject: "Re: Spread the word"
    date: 2008-04-05
    body: "FSD'd: http://www.fsdaily.com/Community/KDE_and_Wikimedia_Collaborate"
    author: "Matt Williams"
  - subject: "More details"
    date: 2008-04-04
    body: "I've also blogged about it at http://vizzzion.org/?blogentry=809\n "
    author: "Sebastian Kuegler"
  - subject: "Willkommen im Verein"
    date: 2008-04-04
    body: "Herzlich Willkommen und hoffentlich neben viel Spa\u00df und Arbeit."
    author: "Einzelk\u00e4mpfer"
  - subject: "Not sure if that's offtopic "
    date: 2008-04-04
    body: "CAUTION: NOT SAFE FOR WORK!\n\nCould anyone explain WTF (in the truest meaning ...) what purpose the notebook with the KDE Logo in this illustration serves:\n\nhttp://de.wikipedia.org/wiki/Bild:Amazon-variant-sex-position.jpg\n\n?!\n\nIs that a contribution to \"KDE everywhere\"?\n\n"
    author: "furanku"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: "LOL"
    author: "qwq"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: ":-D\n\nKDE-Logo \u00fcberall.\n\n:-D"
    author: "Einzelk\u00e4mpfer"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: ":D"
    author: "ad"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: "I think it means that KDE is just sexy. Or maybe it means \"KDE everywhere - everytime.\""
    author: "reader"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: "Ahaha shit, that is awesome!!!!"
    author: "LJ"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: "Someone reacted, if I see it right: Arne Klempert himself (the founder of Wikimedia, germany) changed the illustration used before in the article \"Reiterstellung\" in the german wikipedia to a vectorized SVG version without the silly notebook.\n"
    author: "furanku"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-04
    body: "obviously we're collaborating on many fronts. ;)"
    author: "Aaron Seigo"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-06
    body: "GREAT idea for a KDE-edu application. Why only use KDE to teach about letters, words, math and languages, and not about that one universal language: love making? :-)\n"
    author: "Andre"
  - subject: "Re: Not sure if that's offtopic "
    date: 2008-04-06
    body: "and the name is obvious: seks-ed"
    author: "Aaron Seigo"
  - subject: "I love Wikipedia and KDE, but .."
    date: 2008-04-04
    body: "I just dont love the german wikipedia. It really stinks bigtime. They have a very elite attidude .. like only stuff that should belong in a hardware Encyclopidia should be in Wikipedia and so they deleted a lot of articles about Linux distributions or about TV shows etc .. those people totally suck balls IMHO.\nThe german wikipedia could have MUCH more content .. but we just have too many powertrippin Delete-Freaks .. \nIMHO they are a symbol for intolerant behavior .. probably a german problem .. we had too many stupid beurocratic idiots in our history and some of those sadly reproduced. (Blockwarte)\n\nJust my .02c\n\n( P.S.: I do not contribute to the german wikipedia .. i sometimes correct english  tech articles. The german one is a lost cause )"
    author: "Tom"
  - subject: "Re: I love Wikipedia and KDE, but .."
    date: 2008-04-04
    body: "Maybe it makes me an elitist attidude, but one of the things I find stinks about english wikipedia is the amount of non-knowledge on there.   People are free to spend their time as they like, but IMO an encyclopaedia should stop where the fictional loses contact with the real and starts referring back to itself.  Kudos to the german wikipedians for keeping fantasy and reality separate.\n"
    author: "Bille"
  - subject: "Re: I love Wikipedia and KDE, but .."
    date: 2008-04-04
    body: "I was not talking about fantasy. The information must be facts.\nBut .. well ..  different people have opinions what knowledge is I guess. For me a well maintained article about a niche distro is knowledge. \n\nI think wikipedia is more than a traditional encyclopedia .. i think it was intended to be a wiki to collect information. All sorts of real proven facts etc. If you have people maintaining it should be fine. I really dont see the need/reason for deletion. It is not that diskspace would be a problem or something..\n\n"
    author: "Tom"
  - subject: "Re: I love Wikipedia and KDE, but .."
    date: 2008-04-05
    body: "Why?  How does it harm you if there is an article on the characters in Buffy, say?"
    author: "John Tapsell"
  - subject: "Re: I love Wikipedia and KDE, but .."
    date: 2008-04-05
    body: "My gut instinct is that it isn't, as long as fan gushing is kept in check.  I think deleting is bad form: if there is something wrong with an article factually (or style) it should be fixed: deleting peoples work only creates resentment. Even people who know nothing about a subject can identify problems with it or help to rewrite but it's fans you need to maintain pages and remove vandalism.\n\nWikipedia could do with a \"new editors\" programme to bring in fresh talent. Of course, everyone can edit but the rules upon rules and groupthink can get in the way. Every organisation needs fresh ideas to keep it innovating.\n\n\n...Back on topic. There's something neat about seeing a KDE office... but it did make me wonder, what have you been doing up to now? That it has grown to the size it has with volunteer only administration is impressive in itself."
    author: "Martin Fitzpatrick"
  - subject: "Re: I love Wikipedia and KDE, but .."
    date: 2008-04-05
    body: "Full ACK. I don't use the German Wikipedia at all exactly for this reason. The English Wikipedia has LOTS more of additional information on nearly every topic you can think of than the German Wikipedia. This is of course not only due to \"delete freaks\" but also due to the much more limited amount of people speaking German. The delete freaks add insult to injury though and keep deleting stuff unnnecessarily from a site which already has reduced content in comparison to its \"parent\".\n\nIMHO the idea to have your local language instead of English for every site is a lost cause in general. It causes unnecessary barriers, making the Internet more and more fragmented and is in stark contrast to the \"Inter\" in \"Internet\". German is great for everyday talk between colleagues, friends, family and I like it for its increased flexibility to express something and the many ways you can have fun with words, combine and rearrange them - but do I really want to have \u00e4\u00f6\u00fc-characters in my e-mail-address? Certainly not. Do I really want to find a much more unrelated page in Google? I do not use German Google at all anymore. Just try and search for \"gimp\" at the German Google and look at the first entry. They can't be serious! The list goes and on and it even gets worse with other countries. Imagine a Chinese friend sending you his new e-mail-address. Ugh.\n\nLots of great ideas are possibly floating around the internet. Perhaps someone has already posted the solution to a problem you are desperately looking for on the Internet but it's in Japanese or German or Turkish or whatever so it's completely useless - even more - it's invisible to everyone else. I don't think it's a great thing at all.\n\n"
    author: "Michael"
  - subject: "Re: I love Wikipedia and KDE, but .."
    date: 2008-04-06
    body: "Well replace \"German Wikipedia\" by \"I18n\" and your message doesn't change. You belong to the group of people that think I am stupid cause I like my Computer talking in German to me and not in English. People that think translations of Computer topics are intrinsically evil cause there exist so many bad translations.\n\nBy the way did you know that this \"software science thing\" which is known in German by the very short and descriptive word \"Informatik\" (~ computer sciences/information technology...) has no real English equivalent? Strange, hum?\n\nWhy can't you be so relaxed on the Internet in case you see a \"somewhat funny\" German translation? In daily live you also most certainly don't use high standard German but your very local dialect with a very relaxed grammar.\n\nFurthermore there are many people -and certainly not stupid people- that can express themselves much better in their own language. Do you want to silence these people for the sake of your \"unified English Internet\"? This would be a poor and small Internet. Luckily this is not the case.\n"
    author: "Arnomane"
  - subject: "Welcome Claudia"
    date: 2008-04-04
    body: "Hope you enjoy being part of our community :) I guess you've already been introduced to our various other media (IRC, mailing lists, planetkde.org)"
    author: "Benoit Jacob"
  - subject: "Re: Welcome Claudia"
    date: 2008-04-04
    body: "Err I rather mean, I guess you've already been _told_ about them. My English's rather bad."
    author: "Benoit Jacob"
  - subject: "Re: Welcome Claudia"
    date: 2008-04-04
    body: "\"Introduced\" works fine. You're just too paranoid about your English being bad. ;)"
    author: "Ian Monroe"
  - subject: "Re: Welcome Claudia"
    date: 2008-04-04
    body: "Actually you were spot on the first time - just a little mixing of formalities.\n\nFormal: \"I trust you've been introduced to...\"\nInformal: \"I guess you've been told about...\"\n\nAh, what a wonderful language... /sarcasm.\n\n"
    author: "Martin Fitzpatrick"
  - subject: "Welcome"
    date: 2008-04-04
    body: "Cool to have a KDE E.v. office in my home town!\n\nEarlier at work I saw the picture of Claudia, the new employee, and then, a few hours later, I walked to a restaurant with my girlfriend and on the \"Eiserner Steg\" I saw someone who looked exactly like the person on the Foto, taking a Skyline Picture.\n\nWas that you? Would be a very funny coincidence :)"
    author: "David"
  - subject: "Re: Welcome"
    date: 2008-04-05
    body: "you could just ask"
    author: "Nick Shaforostoff"
  - subject: "wikipedia may be inaccurate, KDE is not."
    date: 2008-04-05
    body: "Wikipedia is information done by men, and thus is naturally biased.\nThis leads to inaccuracy , eg: speaking about polithics or about sports.\nFor anything else wikipedia can be 99% trusted but for those arguments\nbiasing is too high (just think at two wikipedia sites in two opposing\ncountries, which give 2 different versions of the same identical history,\nor in two countries with different religion, or in one capitalist and in one\ncommunist country....).\n\nI don't think it would be a gret move for KDE to be associated with something\nthat could be interpreted as lying, malicious and inappropriate,\nso I'm against any deeper involvments, actual plugins and apps are more than\nenough."
    author: "nae"
  - subject: "Re: wikipedia may be inaccurate, KDE is not."
    date: 2008-04-05
    body: "> just think at two wikipedia sites in two opposing\n> countries, which give 2 different versions of the same identical history,\n> or in two countries with different religion, or in one capitalist and in one\n> communist country....\n\nSeriously, get a life dude. \nThe fact that wikipedia DOES actually provide different articles in two countries IS a good sign. After all only in totalarian and fascistic system \"the true one opinion\" does exist. In reality there is no \"truely objective\" way to look at history. Hence having two different \"interpretations\" of history gives the reader the opportunity to compare both articles and draw his own conclusions.\n "
    author: "Torsten Rahn"
  - subject: "Re: wikipedia may be inaccurate, KDE is not."
    date: 2008-04-05
    body: "Agreed, I'm actually impressed a lot of the time on Wikipedia articles where they attempt to give equal balance to two sides of history of a contentious issue. The talk pages are always interesting - almost as useful a reference as the pages themselves.\n\nNobody is every going to agree on everything. If we can get the arguments out there in public, and present both sides fairly and equally, that's a good thing.\n\nIt's not perfect, but it's probably the best we've had."
    author: "Martin Fitzpatrick"
  - subject: "Re: wikipedia may be inaccurate, KDE is not."
    date: 2008-04-06
    body: "Grow up kid. Tien'An'Men and the actual Tibetan question, Holocaust and Holomodor. Reality IS one, interpretation may be multiple but reality is just\na single one. Wikipedia is too often offering just one point of view, and\nmisplacing interpretations as reality, depending on who is in charge in that\nparticular country/ wikipedia branch.\nSo it's untrustable for more than 90% of the politician questions, while being \ntotally reliable only when talking about sciences and math (typo and other\nerrors excluded, obviously)."
    author: "nae"
  - subject: "Re: wikipedia may be inaccurate, KDE is not."
    date: 2008-04-06
    body: "> may be multiple but reality is just a single one.\n\nWhat is \"reality\" without a (human or whatever) being to observe it? Nothing. \"Reality\" is only a fuzzy and idealized thing that becomes more \"verifiable\" and only gets a meaning in human terms with (more) \"observer reports\" of the actual action or outcomes of the action. \n\nA body such as Wikipedia can only attempt to give a summary of those different observer reports. And of course this requires simplification and of course also might appear \"one-sided\" if some opinions are either in a minority below significance or if there are cultural / political pressures involved which affect \"common sense\" of people involved with compiling the articles  (which is rather unfortunate but I can't see how Wikipedia could \"fix\" such a human problem as long as articles get written by human beings).  \n\n> depending on who is in charge in that particular country/ \n> wikipedia branch.\n\nAgain as I've stated in my last posting that's ok by me: That's ok because as long as there ARE still many different Wikipedia branches (and discussion sections) that have different \"interpretations\" people are still able to compare and can perform a \"reality-check\".\n\n> while being totally reliable only when talking about \n> sciences and math (typo and other errors excluded, obviously).\n\nHaving studied physics myself I'd say that there is no such thing as \"totally reliable\" in science. Yes, Wikipedia is probably pretty accurate in terms of natural sciences - but certainly not \"totally reliable\". People always need to read critically - and that in a reasonable way of course which doesn't make things easier ... . \n\n> Grow up kid.\n\nWell, posting insults as an anonymous coward isn't exactly something that would prove your own matureness - rather the opposite. \n\n\n\n\n\n "
    author: "Torsten Rahn"
  - subject: "Re: wikipedia may be inaccurate, KDE is not."
    date: 2008-04-12
    body: ">Well, posting insults as an anonymous coward isn't exactly something that would prove your own matureness - rather the opposite. \n\nYou're saying there's no reality, that for example if german wikipedia would deny the olocaust they wouldn't be lying as \" \"Reality\" is only a fuzzy and idealized thing that becomes more \"verifiable\" and only gets a meaning in human terms with (more) \"observer reports\" of the actual action or outcomes of the action.\". \n\nSo even the fact that \"grow up\" was an insult is questionable, \nfor example in your case, for a lot of people, that could be a compliment,\ndon't you agree???? It's your own way of .... thinking. Maybe wikipedia in my country will tell that Torsten Rahn would likely need to grow, being just a point of view you should not get mad about it."
    author: "nae"
  - subject: "Re: wikipedia may be inaccurate, KDE is not."
    date: 2008-04-06
    body: "You (and others) are unfortunately totally missing the point. Whether Wikipedia is accurate in all places is irrelevant here.\n\nThink about this: I would've been deeply surprised hearing from the Wikipedia community that they shouldn't work together with KDE because KDE contains bugs. Hell, yes we do. KDE has lots of bugs. There is no such thing as \"perfect\". Well, maybe there is for software (though I strongly doubt that), there surely is not for \"truth\", \"history\" and \"knowledge\".\n\nNow take a step back and realise that it's not about products (Wikipedia / KDE vs. Wikimedia e.V. / KDE e.V.). Wikimedia' e.V.'s mission is to promote the creation of Free knowledge knowledge. KDE e.V.'s mission is to support the creation of Free Desktop software.\n\nSeriously, sometimes I wonder if they had been right rejecting our collaboration based on the stupidity and lack of vision of some people commenting on the Dot.\n"
    author: "sebas"
  - subject: "Did I read the article right?"
    date: 2008-04-05
    body: "Did Sebas really quote himself in the article?  Isn't that just a little silly..."
    author: "Dan"
  - subject: "Re: Did I read the article right?"
    date: 2008-04-05
    body: "\"Well ... it depends, if it's from another official press release it should be marked as a quote, even if you're quoting youself. Sure, it sounds a bit silly, but it helps to keep things clear who said what when and in wich context it was originally\" commented furanku (see here). ;) "
    author: "furanku"
---
<a href="http://ev.kde.org">KDE e.V</a> and  <a href="http://www.wikimedia.de">Wikimedia Deutschland</a> have <a href="http://kde.org/announcements/kde-and-wikimedia-collaborate.php">opened a shared 
office</a> in Frankfurt, Germany and have hired a joint employee for administration.
As two charitable organisations that share similar cultural goals and organisational challenges, they 
hope that working out of the same space will strengthen and expand their links to 
the Free Culture community, as well as allowing them to share resources, experience 
and infrastructure.









<!--break-->
<p>
"<em>We believe that the combination of Free Software and Free Content is not only 
beneficial,</em>" remarked Sebastian K&uuml;gler, a KDE e.V. board member, "<em>but the 
next logical step towards a mature, organised Free Culture community.</em>" K&uuml;gler 
explains the idea behind opening the shared office, "<em>Being able to tap into the 
expertise of an organisation in a different field, but with very similar goals 
and principles, provides us with an opportunity to grow and gain experience that 
I hope to see more often, both within our projects and those of our peers.</em>"
</p>

<p>
K&uuml;gler added that the shared office space is not the only organisational 
improvement under way for the KDE e.V.  The founding of the office in Frankfurt 
coincides with the hiring of an administrative assistant, the e.V.'s first employee.  
"<em>This will help the KDE e.V. become more efficient in our work of supporting 
the KDE community.  With eight developer meetings and presence at many events 
already planned for 2008, a dedicated administrative assistant enables an even 
faster growth of the K Desktop Environment.</em>"
</p>

<p>
Wikimedia Deutschland (the German Wikimedia chapter), which established its office 
originally in October 2006, is also happy to be able to expand its activities 
together with KDE. "<em>Back then, we were the first national section of the 
Wikimedia Foundation to open its own office. Before that, we were a purely 
voluntary-driven organisation. Opening an office was new and unchartered territory 
for us</em>", says Arne Klempert, Wikimedia Deutschland's executive secretary. 
"<em>We're now glad to share the experiences we've made during that first period 
with KDE e.V. That said, due to the collaboration with KDE it is also much easier 
for us to extend our office and activities. Both organisations can share a lot of 
resources this way, resources that both would have had to carry individually if 
we weren't collaborating.</em>"
</p>

<p>The new address is <a href="http://ev.kde.org/contact.php">on the KDE e.V. website</a>.  A big <a href="http://blog.wikimedia.de/2008/03/04/verstarkung-fur-die-geschaftsstelle/">welcome to Claudia Rauch</a>, our new administrator.</p>






