---
title: "Quickies: Nepomuk, Raptor, LProf, FOSDEM, Supporting Member"
date:    2008-02-21
authors:
  - "skuegler"
slug:    quickies-nepomuk-raptor-lprof-fosdem-supporting-member
comments:
  - subject: "Screw everything else..."
    date: 2008-02-21
    body: "There are many things I've suggested keeping me from using KDE 4.x every day, but Raptor looks good enough to single-handedly change my mind.\n\nKeep up the fantastic work!  I can't wait to use it!"
    author: "T. J. Brumfield"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "I agree! Raptor is gorgeous!\n\nTake that apple. Just kidding, it's not a competition. At any rate, Windows, including Vista has been left dead in the water for usability and looks anyways."
    author: "Mike"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "I agree, I can't wait to ditch the opensuse menu. I don't care what people say, I will never get used to it and I have gave it a shot. "
    author: "Jeremy"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "The opensuse menu is very Windows Vista like. Better than Vista, but not necessarily user friendly/faster. The classic menu as easier for seldom used tasks.\n\nRaptor looks promising. A bit Mac-like, and that's a good thing these days."
    author: "Max"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "are you serious?\n\nThe opensuse menu looks nothing like windows vista, have you ever seen vista?  I dual boot between opensuse and vista, and the menu's are so far apart, so completely don't look like each other that you must be joking"
    author: "anon"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "I find the Vista Menu even prettier than Kickoff but Kickoff is more usable for me personally.\nRaptor looks really nice, if it's usability is as good as it's looks then it will surly be a hit."
    author: "Bobby"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "Then don't use it, and just use the classical menu insted. No reason to spam this forum with usless comments."
    author: "Morty"
  - subject: "Re: Screw everything else..."
    date: 2008-02-21
    body: "I am fine with Kickoff but I just can't wait to test Raptor :)"
    author: "Bobby"
  - subject: "Just to clarify..."
    date: 2008-02-26
    body: "The original (!) Opensuse Kickoff menu for KDE 3.5 is superb, it is the KDE4.0 implementation that is not ready (in terms of missing some important features). I like Kickoff3.5 though I use the classical menu in 4.0."
    author: "Sebastian"
  - subject: "Great Job guys!"
    date: 2008-02-21
    body: "GO LINUX! GO KDE 4.1! GO AmaroK!\n\nThere are huge fanclubs of AmaroK, and KDE on facebook. Somebody should post links to the \".\" there.\n\nGreat to see progress. KDE and Qt are going to be great. I might finally be able to give up my Windows box. I could have my own independend \"cloud\" (see my other post. I could do my work completely platform independent on all devices I own. All running Linux + KDE. )\n"
    author: "Mike"
  - subject: "Re: Great Job guys!"
    date: 2008-02-21
    body: "Thanks for praising Amarok.\n\nJust one small correction: It's been called \"Amarok\", not \"amaroK/AmaroK\" since June 2006 ;)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Great Job guys!"
    date: 2008-02-21
    body: "K. duly noted. :)\n\n\n"
    author: "Mike"
  - subject: "\".\" = dot"
    date: 2008-02-21
    body: "\".\" = dot.kde\n\n"
    author: "Mike"
  - subject: "Looks great, but PLEASE SAVE DESKTOP REAL ESTATE."
    date: 2008-02-21
    body: "Raptor looks great.\n\nI hope that's just a demo/screenshot. This wastes so much Screen real estate!!\n\nDoes everybody here have huge monitors? Or huge resolutions? Or is totally blind? (no offense to disabled people, I totally understand in their case) Normal people though should, either use glasses, or sit closer to the monitor.\n\nI am typing this from a Sony laptop with a 10.4\" screen, using just 1024x768. This  taskbar + Raptor would use 1/3 of my total screen. (whole bottom 1/3 of my useful screen gone *grr*) Am I really the only one who is bothered by this?\n\nAnd no - I don't want to put these on \"autohide\" that defeats the purpose of having a task bar in the first place. Then I could just use shortcut keys.\n"
    author: "Max"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "First off as I understand it, Raptor is another k-menu. So why not use all the real estate it needs. It's like saying Konqueror uses too much real estate since you're supposed to use the whole screen.\n\nAnd even if you didn't misunderstand it, some folks do have dual monitors and plenty of screen real estate. We have needs too. :)"
    author: "Ian Monroe"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "One of my machines is a dual 22\" wide-screen setup. That doesn't mean I have to waste all that gained screen real estate. \n\n\\rant\n\nI don't see a need for Konqueror to use the whole screen. As a matter of fact, I like it as small as possible. My FILES should use the screen real estate, not he program/window that houses them. I'm all for small icons, thin borders, minimalistic window decoration. Anybody notice how with Windows Vista everything got bigger? Borders, title bars, buttons, etc.? What was the need? This is not the car industry for crying out loud, where each new model is larger than the previous year. Even the damn MINI Cooper got larger with the newest version. \n\nI got bigger screens to gain some EXTRA screen real estate, not for the programs to \"bloat\" it all away again. In the future I'll have a 60\" screen, and still won't be able to display more information on it at a time than I do now. Why does it have to be this way?\n\nI'm not even going to mention my 10.4\" laptop screen, or an Asus EEEpc, or *gasp* my 7\" CarPc screen, that runs at 800x600. <-- I wouldn't even be able to see ANYTHING BESIDES THE PANEL and a menu. \n\nAm I really the only one who feels this way?\n\nWindows 3.1 ran comfortably under 640x480 VGA resolution, Vista recommends as a MINIMUM 1280x1024. Does KDE/Linux have to head this way too? I thought it's all about choice? Please give users the CHOICE OVER HOW MUCH SCREEN REAL ESTATE HE/SHE WANTS TO ASSIGN (waste)!!!\n\n/rant"
    author: "Max"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "You've got the choice, by not using it, there are other menus out there. At least it's a choice extra. Besides, it's a menu, something that takes screen estate from the moment you're not looking at anything else but the menu, so it doesn't really matter how much is covered by the menu when opened.\n\nShould the people with big resolution be ignored by not using the options available, just because users with other panels can't use the option? They still have the older options (in my point of view the new \"old menu\" is one of the best menus out there, though this raptor thingie does look nice/way better than that opensuse mistake.\n"
    author: "Terracotta"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "That's the part I'm confused about. The screenshots make it look like it's \"always there\" kind of like OS-X's quicklaunch at the bottom. Hence my confusion."
    author: "Max"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-23
    body: "It's quite hard to show-of a menu when it's not opened ;-). But it's just like the kmenu now, just visible when you press the button."
    author: "Terracotta"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-23
    body: "That should not be the choice.  KDE4 is supposed to be vector-based, fluid, and fully scalable.  Besides, he's not saying he dislikes the app or would prefer effort wasted on a slighty different version of the same thing --- he's just saying he'd like it a bit smaller."
    author: "Lee"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-25
    body: "Thank you! That's exactly what I was saying.\n\nI just want the ability to resize, that's all."
    author: "Max"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "they'll probably have different interfaces for different form factors, so that generally size would fit available space. at least that is what I assume."
    author: "yman"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "First of, Max, I totally agree with you in so many ways. And let's face it, all the current menu implementations use a lot of space screen (except possibly kmenu, but it's not the nicest).\n\nI've got a fairly new eeePC, and it's a testiment to efficient use of screen real estate. It's 800x480, and I can do most of my tasks comfortably on it. Tomorrow, I plan to finally install kde4 on it, because I can remove the panel, and free up more space. Also, I hope to reimplement some of the really nice stuff that I'll miss, and plasma should be great for it. Also, krunner is awesome!\n\nNow, plasma's panel has gui for customizing size (I think it's getting into 4.0.2), and most of the widgets/plasmoids/applets are fully resizeable, and hearing Aaron talk, you really get the sense that he understands there are different form factors. So, I hope someone will also make the launchers, and whatever else resizeable."
    author: "Soap"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "While I agree with your general plea not to waste screen realestate with useless decor, I don't think this applies to something like a start menu. If you are using the menu, your (only) task is to find and start the application you're after. I would actually prefer a FULL SCREEN start menu that nicely blends over my open desktop, so there is plenty of space to make a decent and logical presentation of my applications that allows me to find what I'm after quicky. No more menus with sub menus that are impossible to navigate with a mouse, let alone - heaven forbid! - scrollbars in the menu. \nWhy not use the full screen for that? It's not like you're doing something else while starting a new application, are you? And as soon as you found what you are looking for, the whole thing will disapear again anyway. \n\nAgain: I am totally with you if you're talking about normal applications that you have running side by side, but the start menu is not a normal application."
    author: "Andr\u00e9"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "Agreed on the full screen start menu. I really don't understand the \"I want to save the screen real estate, even while I'm not doing anything in it\". The same sort of logic that says \"the OS is using up all the memory!!!\" like it should keep some sitting doing nothing just to make you feel comfortable.\n\nWeird...\n\nAnyway, I'd love to see a full-screen menu system that does something smart. Searching for things we can assimilate a lot of information in our peripheral view, hiding the things we're looking for in hierarchies doesn't make a whole lot of sense. (note: I say /hiding/ not /putting/ although taggings better)\n\n"
    author: "Anon"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "Cool down man. May I recommend some fukitol http://www.flickr.com/photos/woeful/390701224/sizes/o/"
    author: "Ian Monroe"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "ooo That's great. :)\n\nRobin Williams fan too?"
    author: "Max"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "I so agree... especially i couldn't quite understand who on earth defined the default size of the KDE-panel (assuming to that time it wouldn't be resizable until 4.1) to a freaking huge retarded nightmare of wasted screen estate!\n\nIt ONLY looks normal with resolutions 1600x1200 an higher ... wtF?!!? So many people i heard saying \"well, KDE4 looks nice, but that insanely big panel...\" - that much for the good first impression KDE4 was aiming for.\n\nThese are the little things where you ask yourself \"What on earth where they thinking\"?"
    author: "Marc"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "Exactly!!\n\nOnly reason I could see at the time was: \"Looks great in screen-shots\". Then I was really disappointed after i installed to see that it's the FINAL version. :("
    author: "Max"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTATE."
    date: 2008-02-21
    body: "\"I am typing this from a Sony laptop with a 10.4\" screen, using just 1024x768. This taskbar + Raptor would use 1/3 of my total screen. (whole bottom 1/3 of my useful screen gone *grr*) Am I really the only one who is bothered by this?\"\n\nHey, its a 'menu', not a toolbar. It comes up only when you want to start application and that time you dont need to control your applications on screen. It's like with any menu, they always hides something but they are just a button and those are not on your way when you use your other applications ;-)\n\nCurrently menu-plasmoid is too big, even for 1280x800 screen but it's fixed on SVN version (4.0.2) so you get it smaller (atleast two rows for apps).\nBut it will get better when KDE 4.1 hits the road ;-)\n\nI like that Rapotor menu, it's good to get menu what use space if it gives great usability. KickOff just 'sucks', it's great when user just use 'favorites' part but when user goes to \"applications\" or other tabs, old kmenu comes much faster and easier to use (and most new PC users even only need two click to open applications, first to open menu, then go menutree, no matter how deep, and second click to start application) \nThat's why i like old kmenu with 10-15 favorite-list on top of it so i get KickOff benefit but still have a faster applications menu. Only thing what i miss is search bar what makes Kickoff great because it is easy to find apps what you dont know. But if i relly wanna start applications fast, i forget all menus and i use alt+F2 what has drop-list and autocomplete so i can start all apps/address/etc very fast.\n\nBut i dont blame those who likes kickoff, kmenu or any other menu, most important thing is that there will come to be choises for everyone so we are not forced to use one menu as on MacOSX or Vista (yak).\nThat's why KDE is so great, it's all about choises!"
    author: "Fri13"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "I actually like GNOME's arrangement for panels, and that's how my KDE3 desktop is configured: on the top a panel with the menu, quicklaunch, a few applets and a systray. At the bottom, on a single line, the taskbar on another panel. And both panels are quite thin, as I run 1280x800. Can't wait for KDE4.0.2 so I can resize the panel. Actually, I can't wait for Kubuntu to fix KDE4 which has been broken and unable to boot since an update 3 days ago lol\n\nQuestions:\n* Is it possible to have more than one panel in KDE4?\n* Is there any quicklaunch plasmoid going on?\n* Is there any panel plasmoid that has the effect of showing the dashboard, or perhaps any way to put a button there that does CRTL+F12?"
    author: "NabLa"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "\"* Is it possible to have more than one panel in KDE4?\"\n\nIt was possible in 4.0.0, IIRC - it's just that no GUI config had been written for it.\n\n\"* Is there any quicklaunch plasmoid going on?\"\n\nWhat is \"quicklaunch\", precisely?\n\n\"* Is there any panel plasmoid that has the effect of showing the dashboard, or perhaps any way to put a button there that does CRTL+F12?\"\n\nThe Show Desktop Plasmoid does something similar, but may not be 100% the same."
    author: "Anon"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-21
    body: "*What is \"quicklaunch\" precisely*\nPerhaps a \"panel\" in the panel with icons to launch a program.\n\nI have been thinking about such a thing to incorporate it with a \"favorites\" kind of thingie (and some other stuff as well), but the idea is still growing, gonna dig into plasma api's and hopefully the raptor guys make their favorites document accessible by other applets, would be nice."
    author: "Terracotta"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-22
    body: "You can just put icons in the panel by dragging them from Kickoff IIRC. You can't move them, though (not yet at least)."
    author: "Luca Beltrame"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-22
    body: "Correcting myself: by right-clicking and selecting \"Add to Panel\". Unsure if it's a distro-specific tweak (I use Kubuntu) or not."
    author: "Luca Beltrame"
  - subject: "Re: Looks great, but PLEASE SAVE DESKTOP REAL ESTA"
    date: 2008-02-28
    body: "If you're talking about KDE3, no: it's in stock KDE3 (I use Gentoo, so no distribution specific tweaks), but if you're talking about KDE4, then it's Kubuntu specific (I never saw it and I've seen Kubuntu KDE4 before)."
    author: "Riddle"
  - subject: "Raptor - roadmap?"
    date: 2008-02-21
    body: "Raptor looks quite interesting - how far along are the development? Is the plan to have something ready for KDE 4.1? Sooner? Later?\n\n"
    author: "Joergen Ramskov"
  - subject: "Re: Raptor - roadmap?"
    date: 2008-02-21
    body: "Last I heard they were aiming for KDE 4.1 but the website had download and build instructions to use it today, so it is likely semi-usable currently."
    author: "T. J. Brumfield"
  - subject: "Re: Raptor - roadmap?"
    date: 2008-02-21
    body: "cool.\n\nHopefully someone will come sort of guide. Or guide video. Newbies will be royally confused. What are we at 3 application launchers now?\n\nClassic\nKickoff - opensuse style\nRaptor\n\nI like the choices, but we'll really need a tutorial/guide of some sort for newbies to even KNOW these features exist.\n\nI smell another Aaron Keynote!! :)"
    author: "Max"
  - subject: "Re: Raptor - roadmap?"
    date: 2008-02-22
    body: "Don't forget Lancelot.\n\nhttp://ivan.fomentgroup.org/blog/2008/02/06/lancelot-applet-close-to-finish/"
    author: "The Troy"
  - subject: "Raptor - roadmap? - Guide video? please?"
    date: 2008-02-21
    body: "cool.\n\nHopefully someone will come sort of guide. Or guide video. Newbies will be royally confused. What are we at 3 application launchers now?\n\nClassic\nKickoff - opensuse style\nRaptor\n\nI like the choices, but we'll really need a tutorial/guide of some sort for newbies to even KNOW these features exist.\n\nI smell another Aaron Keynote!! :)"
    author: "Max"
  - subject: "Kicker"
    date: 2008-02-21
    body: "Am I the only one who still use Kicker in KDE4 ? :)\n\nAniway, Raptor looks nice, that's sure !"
    author: "Heller"
  - subject: "Saturday more beer"
    date: 2008-02-21
    body: "On saturday the Belgium company imatix (remember xitami?) will give another beer party for Fosdem participants and Brussels geeks.\n\nhttp://www.imatix.com/fosdem-2008\n\nWhen?\n\nBrussels, Saturday 23 February from 9pm (21h).\nWhat, exactly?\n\nA party held at the iMatix HotSpot, a warehouse on the wrong side of town that's been the scene of many heavy parties over the years. We invite all FOSDEM attendees to come and enjoy. The program includes live West African music and a great lineup of DJs. Belgian beer, snacks, and other drinks, compliments of iMatix. We love free software, and we love free beer!\nWhere is it?\n\nRue des Ateliers 13-15, 1080 Brussels, near metros Yzer and Comte de Flandres."
    author: "Andre"
  - subject: "NEPOMUK"
    date: 2008-02-21
    body: "Nepomuk is beeing advertised somehow as revolutionary. Can somebody explain to me in which way is it better than the years old Mac-Spotlight?"
    author: "noob"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "\"Nepomuk is beeing advertised somehow as revolutionary.\"\n\nMost KDE things have been, of late.  I think for years the KDE guys were too silent about their achievements and content to stay out of the spotlight (no pun intended ;)) but nowadays they've swung too far in the other direction and hype everything to death, which inevitably leads to disappointment and subsequent backlashes.  Hopefully they'll find a happy medium at some point in the future."
    author: "Anon"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "> they've swung too far in the other direction and hype everything to death\n\nI think setting up a website to promote a *menu* falls under this description.  Guys, the enthusiasm is great, but Raptor doesn't need its own website.\n"
    author: "Jim"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "wtf, I'm finishing right now with the website for the clock applet/plasmoid!"
    author: "adrian"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "Cool!! =)\n\nCan we have a separate newsletter for it? :)"
    author: "Max"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "couldn't they all be links of ONE (1) major KDE site?\n\nWhy do we have to hunt and peck all over the internet to read about all the features?\n\nNothing wrong with overhyping. JUST PLEASE, PLEASE, DON'T UNDERDELIVER!!!"
    author: "Max"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "spotlight is like strigi, it just does full-text indexing. Nepomuk does more than that - it extracts and indexes RELATIONSHIPS between files. Spotlight doesn't tell you where a file came from - Nepomuk can."
    author: "jospoortvliet"
  - subject: "Re: NEPOMUK"
    date: 2008-02-29
    body: "AHHH!\n\nsuch a lame example!!! and one you've no doubt read in the million places i have.\n\nthis is the same functionality my email client has had for years.\n\nnepomuk has to be so much more than just that."
    author: "mike"
  - subject: "Re: NEPOMUK"
    date: 2008-02-21
    body: "Nepomuk allows you to do stuff like:\n- find that picture of a giraffe I got in an e-mail from Joe lately."
    author: "Diederik van der Boor"
  - subject: "Re: NEPOMUK"
    date: 2008-02-24
    body: "You know, I have read this description many times but it has just hit home with me.\n\nThink of Gmail's tags but implimented on a desktop scale.\n\nFolders will become almost meaningless.\n\nAt work, I have documents on returns and lost shipments.  A lot of times those doc apply to both and when looking them up I am usually searching both folders.\n\nNow, just one folder.  The stuff that applies to returns and lost shipments are tagged as both and the stuff that apply to only one tag with the correct one.\n\nI have been using KDE 4 for months now, but this is the first time I see it as more than a \"code rewrite\" or eye candy thing.\n\nI am excited about the future of KDE!!"
    author: "JackieBrown"
  - subject: "Re: NEPOMUK"
    date: 2008-02-28
    body: "Sounds like WinFS (which was yet another Vista dropped feature). Cool to see KDE delivering where Vista did not."
    author: "Riddle"
  - subject: "raptor"
    date: 2008-02-21
    body: "All Kudos to raptor devs. Looks are that this will not only kick some serious Gnome/Windows butt, but Apples as well. And that's a lot folks."
    author: "foo"
  - subject: "Re: raptor"
    date: 2008-02-21
    body: "What are you basing this on, exactly? Have you used it, or are you just falling for (and amplifying) the hype?"
    author: "Anon"
  - subject: "Re: raptor"
    date: 2008-02-21
    body: "Hype, Hype, Hype!!!\n\nMaybe some additional developers will get off their butts and start coding.\n\nThere really haven't been any major changes in over 2 months. How will KDE 4.1 bring any interesting changes then? Is everybody waiting until the last minute?"
    author: "Max"
  - subject: "Re: raptor"
    date: 2008-02-22
    body: "raptor's developers are busy working on its website. have patience, first things first.\n\n"
    author: "joe"
  - subject: "Re: raptor"
    date: 2008-02-23
    body: ":) we had some serius problems in the frist phase of raptor and was clear that we wouldnt make it in time for 4.0.\nIm a fairly visual person and making an web site for me is easy specily if you had to that equation my grat friend Luke Parry. So that was an easy thing to do. Dont think that it was a random event, making the web ste without having alot of code to show! Tha fact is that this project is a bit difrent from other OSS projects, in this projevt the way its gona work and the visuals were made before the code was ready, and alot of difrent options were visualy tested. The result was a bit of documentation we gathered on the subject and that we thought would be very interesting to the project to keep and folow as guidlines. \nSo the site was created as a, \n1) repository of the documentation.\n2) Front page of the project.\n3) Call for more developers. "
    author: "nuno pinheiro"
  - subject: "Re: raptor"
    date: 2008-02-21
    body: "Kicking other desktop environments'/operative systems' butts, is that what the menus are supposed to do nowadays?\nSeems like I haven't followed the recent developments, I just want it to browse/launch my applications..."
    author: "Hans"
  - subject: "How about some Compositioning news?"
    date: 2008-02-21
    body: "There hasn't been ANY NEWS WHATSOEVER about NEW compositioning effects.\n\nCould somebody please remedy that?\n\nKwin doesn't even have 1/2 the features of compiz fusion yet. An even they are years old now. When are you going to release new (useful) effects?\n\n"
    author: "Max"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "\"When are you going to release new (useful) effects?\"\n\nWhen someone writes them, of course.  This isn't rocket-science, people!"
    author: "Anon"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "Are you trying to tell me I'm the only person on this blue/green planet that wants more desktop effects?\n\nI always thought it's: \"demand it - and they will build it?\" No? =)"
    author: "Max"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "\"I always thought it's: 'demand it - and they will build it?' No? =)\"\n\nI know that this was a tongue-in-cheek comment, but the answer is \"no\" - with volunteer-driven software, if no one steps up to do a task, then the task doesn't get done.  Full stop."
    author: "Anon"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-28
    body: "I guess the main news is optimization :). It's SLOW on my system (and Compiz is fast). Also, first-things-first: Plasma, KDEPIM, etc needs work."
    author: "Riddle"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "Perhaps when the glitches are out of the compositing stuff, first a stable and fast a compositing base, is more important than those extra effects no one uses for more than a day.\n\nCompiz is nice and fast and has great effects, but it falls short on the window management part, sadly, otherwise I'd use it over kwin anyday."
    author: "Terracotta"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "Agree, glitches and speed are awful. Especially on older machines.\n\nI wish it were just possible to merge the best parts of Compiz and Kwin together.\n\nCompiz knows how to do compositioning, Kwin is a great window manager; Together there are Compiz-win, the greatest superhero desktop fighter in the galaxy! *cue cheesy superhero movie music*\n\nWhy is development on Kwin so slow anyways? Wouldn't we need a good window/compsitioning manager, before we need an office suite, and basic games?\n\nGood compositioning effects by themselves could amuse me for hours.. :drool: =)\nI'm easily entertained though.\n\n(besides, my boss wouldn't know they're games.. hehe *sneaky* )"
    author: "Max"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-24
    body: "<i>Why is development on Kwin so slow anyways?</i>\n\nMaybe because people are not sending patches instead of asking such questions?\n"
    author: "Lubos Lunak"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "Cover switch effect: http://de.youtube.com/watch?v=dv1Nu4425g8"
    author: "Anonymous"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-22
    body: "Cool!\n\nooo, aaah,...\n\n*cue, astounded aliens from Disney's Toy story*"
    author: "Max"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-24
    body: "Me ooo, aaah too!\nMay even be useful."
    author: "reihal"
  - subject: "Re: How about some Compositioning news?"
    date: 2008-02-23
    body: "Coooool! So now we have Flip-Switch and Cover-Switch - Yeeehaw!"
    author: "blueget"
---
The KDE e.V. <a href="http://ev.kde.org/news.php">welcomes</a> a new Supporting Member, <a href="http://verwandt.de">OSN Online Social Network GmbH</a>, a company based in Düsseldorf in Germany. Supporting Members help the community with financial support, their contribution is used for example for sponsoring developer meetings you often read about on the Dot. ***  The Nepomuk KDE project that is creating the social semantic desktop on top of KDE has <a href="http://nepomuk.kde.org">launched its new website</a>. Go there for numerous tutorials integrating Nepomuk features like "<em>who sent me this file?</em>". *** The German Kubuntu team has <a href="http://www.kubuntu-de.org/english/interview-harald-sitter-about-amarok-and-kde-4">an interview with Amarok release dude, Harald Sitter</a>. *** The team developing the <a href="http://raptor-menu.org">Raptor-menu has just launched their website</a>. Raptor aims to deliver a new launch menu for KDE. *** We were sent an <a href="http://prokoudine.info/blog/?p=68">interview with Hal Engel of LProf</a> the only open source ICC profiler, made in Qt. *** <a href="http://fosdem.org/2008/">FOSDEM</a> is Europe's biggest Free Software conference and will be held this weekend in Brussels.  See the <a href="http://www.fosdem.org/2008/schedule/devroom/kde">KDE</a> and <a href="http://www.fosdem.org/2008/schedule/devroom/crossdesktop">Cross Desktop</a> rooms for exciting talks and say hi to us at Friday's <a href="http://www.fosdem.org/2008/beerevent">beer event</a>.









<!--break-->
