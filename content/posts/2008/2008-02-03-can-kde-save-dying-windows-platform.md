---
title: "Can KDE Save a Dying Windows Platform?"
date:    2008-02-03
authors:
  - "MrCopilot"
slug:    can-kde-save-dying-windows-platform
comments:
  - subject: "See"
    date: 2008-02-03
    body: "Where Mr. Ballmar accuses us to do medicide to his Windows thingy, we actually do livesaving... gotta love that arrogant approach to achieve world domination..."
    author: "Thomas"
  - subject: "dying windows platform !!"
    date: 2008-02-03
    body: "you are not serieus ;) "
    author: "djouallah mimoune"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "Hint: the Windows platform market share is not increasing."
    author: "Anonymous"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "duh, it's pretty much as big as it gets."
    author: "jospoortvliet"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "That is true however there is no real alternative available.\n\nImagine if KDE was feature rich on Windows. People would try it out.\n\nAnd now imagine if KDE-on-Windows actually gave LEGIT advantages over \nwindows out of the box!"
    author: "she"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "Some people would. They would tinker with it, and a lot of poeple would switch back.\n\nWhy?\n\nBecause KDE on Windows is not the standard. The strength of Windows is that it's everywhere. People can take place behind a computer and instantly know how to use it - because it's familiar. Moreover, it's commercially supported, which is very important for large companies. Besides, for a lot of people, Windows is \"good enough\". They don't necessarily need the power of KDE.\n\nCompare it with existing Windows shell replacements. None of them ever achieved a significant installbase."
    author: "starbase218"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "KDE on windows is not a shell replacement, it is simply a suite of apps and libs."
    author: "Troy Unrau"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "Yes and one day Dell will ship computers with windows with kde apps preinstalled."
    author: "BabaLi"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "Most likely it will not be KDE, but the Dell desktop (of some other name their marketing comes up with).   It will be KDE (at least KDE games, kdepin, kdeedu, and koffice, some other the other great parts may not make it), but with KDE replaced with Dell in all the documentation.   They will likely to their own theme, so it will LOOK very different.   The code will not have significant changes - Dell is a marketing company and marketing just wants to look different, they don't care about the code so long as it is good enough.  (This is a good thing)\n\nThey will ship the source code right on the harddrive.   I wouldn't be surprised if they sent patches back to KDE for any changes they make.  (Come to think of it, they may not do this for the first release, but by the 3rd update they will just because re-patching all their work in is such a hasstle)\n\nMaybe it won't be dell who does this first, but the parts of KDE I listed are great, and nothing like that comes with windows.   They can have a large marketing advantage by shipping those parts of kde.   They could get the same result by shipping other programs, but they would have to increase the price they have to charge which marketing doesn't like (Microsoft Office is obvious).   Add in the fact that they can apply a Dell theme to kde, and they have a marketing winner that they can't get now for a reasonable price."
    author: "Hank Miller"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "It's inevitable that someone will make a shell extension with KDE 4."
    author: "reihal"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "It is true that (for now) Windows still holds the majority of computers, especially in government departments, hostage. But, almost every week I read an article in the news about government departments switching their computers from Windows to Linux. Around the world, people are starting to tear themselves away from the Windows beast. I, personally, have had several devout Windows users ask me why I like Linux so much. I sent them a few Linux 'live' distros to play with, and told them that if they had any questions, I would be happy to help them find the answers. Every one of them, within a week of receiving the distros, has, at the very least, dual-booted their machines. A few of them made a complete  and total switch to Linux. Despite the propaganda coming out of Redmond, Windows IS dying."
    author: "linuxaddict"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "Most people don't care, period.  You see, when car guys get all excited and start talking about why this car is better than the other one and what sort of parts it has in it and why it's more reliable and all that ... I still don't care.  It's just a car.  For most people, it's just a computer.  Any significant changes that we want to see in the technology landscape need to be filtered through that.\n\nWindows isn't dying, it's lost a small bit of its complete and total hegemony.  But you can keep pretending if you like.\n\nMe, I don't really care if Windows dies.  I just want it to have to compete on value and I feel like that's what, in some markets, it is being forced to do.  If Linux could even get and hold 5% of the global desktop market that's enough to make MS change the way that it does business."
    author: "sw"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "Exactly. Most people want to get things done. They expect things to work, and don't want to tinker. Most people in the community are different (myself included), but if KDE really wants to achieve world domination :), it is important to acknowledge this.\n\nAs an example, the hardware support situation on Linux is still far from perfect. Now, the community has always stated that that is not their fault. Of course it isn't. No-one said it is. But for most people, it is a fact. Some people might be able to learn how to write device drivers and spend a lot of time on that. Or they can do what they originally wanted to on Windows, and then move on. because they use their computers to actually do functional stuff, not to have to learn C or whatever \"scary\" programming language."
    author: "starbase218"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-06
    body: "You may be reading of OS migrations throughout industry or government sectors but keep in mind that a lot of what you're reading is speculation based on promise (in both directions). \n\nI can assure you that in many cases, these migration news bits are not fully followed up on. News of migration to another OS (platform) can leak out when a sector (public, private or governmental) starts just the investigation into cost analysis with a new vendor (speculation based on promise). I've read that some migrations went sour and the company involved lost a ton of money in the effort and lost more during the move back. \n\nIt's been said that the numbers on a chart can be made to show any desired outcome. Keep that in mind while you're reading the news. "
    author: "Jeff (OS Switcher)"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "Well firefox was not the standard but people are certainly using it everywhere. I have a lot of hope in linux. "
    author: "Ryan"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "Why do you think MS is now putting more focus then ever on IE? Firefox got a lot of publicity and marketing. That helps a lot. Firefox became more than software, just like Heineken does not sell beer, but the ability to have a good time.\n\nTechnically-oriented people often say this is nonsense, and they are right - from their point of view. So use whatever you like, but don't go telling people Linux is better on all accounts. It's just not that simple."
    author: "starbase218"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-13
    body: "You're right about windows being everywhere,the question here is,why has Windows been the only operating system available for the last twenty five years??.I started working on computers when the only OS was Windows 3.1, and let me tell you, i don't remember going to the store and being to big on choices in that area.That's what we call \"Monopoly\".They are the best right now because there wasn't anybody fighting against Windows(Bill Gates).They owned the markets for so long is not even funny if you think about.I like open source more than anything else and all nine pc's i have run some version of Linux.Fuck bill gates,i don't see my self paying that fucking asshole any money for licenses any time soon.....ever.Open source code rules....and it works just great."
    author: "Ivan Speranza"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-20
    body: "Dying, not likely. i can see it on the server side maybe because linux has that almost. but not desktop ever. linux is just far to unwieldly for the regular user. i have a resident linux expert in the house who is a huge supporter of linux and is always bashing windows. But he still uses windows more. sure he uses linuz for doing his programming and what not. but for everyday tasks he still uses windows. you can argue like he does and says it's because the support for linux isn't there yet. well hes been saying this to me for 10 years now. to me it's just an excuse. sure he could get most of his everyday stuff working in linux, seen him do it. but where it's usually a 5minute install for me, it's about a half hour for him. time is money ppl and the more you waste the less you get. I've tried linux, and found that for me, the average user, even the user friendly versions involve you knowing a bit about the language linux uses. kinda like you need to know the language for windows a bit, back in the dos days. the trick was windows got rid of the need for users to know any of that, while linux is still trying to do that. you can say it's superior if you want, but remember it's only superior in some ways, and only for some ppl. for the rest of us windows is fine."
    author: "Nolan"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-03
    body: "Sure there is: Linux and MacOS (which is iirc up to 10% market share in US meanwhile)."
    author: "Anonymous"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-04
    body: "You cannot compare KDE to Windows, just to the desktop environment of Windows. KDE as a desktop environment is by far more feature rich than the desktop environment of Windows. However, KDE as a desktop environment will not be ported to Windows, just applications, most of which are not found in Windows by default."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Don't believe the hype"
    date: 2008-02-16
    body: "Please don't believe the hype you hear within your own community about your own community.  Wherever you are, it's the same: local radio will tell you your area has great attractions, world-class events, etc.  To find out the truth, you have to go elsewhere, see other attractions/events, listen to people from other places, etc.  It's the same in software communities or any other subgroup that isn't formed around a negative theme -- I remember being part of the Amiga community over a decade ago, believing the hype in it's press, its fanbase, about its great future, great technology, etc.  The fact is, it *started* with world-leading technology, was not marketed or developed properly, and went on for years purely due to its fanbase.  However, things like the PC moved on, and the Amiga stagnated.  The IT world would be MUCH better now if the Amiga's OS had replaced Windows or even Mac.  To believe the hype, it was inevitable -- Amiga had already beaten Atari STs, and PCs were just the next logical step.  But in reality, Atari STs were just as dead, but just as popular in their own communities.\n\nDon't make the mistake I did, because it feels sucky when you finally wake up.  KDE is great.  It's a joy to use.  It might be one of a thousand factors that pushes MS to the brink.  Will MS ever die?  I highly doubt it.  Will KDE ever get the recognition it so richly deserves?  I highly doubt that too.\n\nEnjoy it for what it is.  Be part of the community, and part of the secret.  Don't imagine the world will suddenly understand someday.  They won't."
    author: "Lee"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-11
    body: "Either way, the market is shifting..\n\nand Microsoft knows that. Why do you think there is this big debacle with Yahoo at the moment. Microsoft is looking for alternatives to diversify.\n\nWe're slowly moving to \"operating system as a service\".\n\nI have great hopes that KDE will be at the forefront of this movement and will, together with Apple, take over more of the desktop market. I like choices.\n\n"
    author: "Max"
  - subject: "Re: dying windows platform !!"
    date: 2008-02-18
    body: "Gee.  So the idea is to throw a layer of basic KDE4 functionality atop the Overpriced Redmond Product with the Dangerously Enforceable License, and make things all right at last for WinBox users in that manner?  Well, OK.  I'll have a look at the demo and maybe adopt it once she goes stable, if I like it at all.\n\nBut I would not bet on Vista's hospitality, really.  I think the Redmond krue is just plain insular, judging by the responses that well-respected folk such as Mr. Steve Gibson ( HTTP://www.grc.com ) get from the MS home office when wide-open vulns become just too obvious to ethically ignore.  \n\nUm, y'think making a printer work on Linux is hard?  Hmph.  I'm looking at a fine near-new HP 5610 all-in-one, sitting by my left foot right now, that Vista apparently just would not touch.  Its former owner (newly upgraded to Vista) finally bought a Lexmark unit of similar capability, THEN hacked his way (again) through all the necessary anti-just-everything cyber-condom$ and thus got his retail business running again, after a mere week of foolishness, head-banging and lost trade...\n\nMaybe he really needed a different printer, maybe not.  Not my department.  But I think Redmond just might have started to compete with CUPS, at least.  I reckon I'll wait no more than half a year, myself, afore someone in our community plops the right homemade HP driver into Foomatic.  (Being semi-retired, I confess I am half-inclined to look into what is required for easing that aspect's lead-time myself - a printer-driver *generator* utility such as the Amiga world once enjoyed is the notion I have in mind.)\n\nSo I work on the Other End of the Corporate Cybergear Food Chain.  We salvage moribund Windows boxes both from the street and over-the-counter as a community service (now headed toward becoming one viable non-profit inner-city public cyber-recyling depot).  At ComPDQ, we Linux the best - and scrap out (\"upgrade\") the rest back into their component materials.  No extra charge for never ever putting any part of our materials into your air, lunch or drinking water.  Haven't yet found a boggy-to-crashed CPU yet that did not have Windows on its' deck, of course.\n\nBut when it comes to revitalizing any no-longer-wanted, gone-boggy Windows box:  Up to now  a DoD diskwipe followed by a fast shot of Freespire (for \"just-wanna-USE-it!\" folk) or Mandriva/KDE (for the more adventurous or demanding) has stood for a few years, now, as our sovereign agent for resurrecting and nimbling-up those moribund '586 boxes that find their way to us for ethical disposal.  (Not to mention the RIPLinux distro that confers basic Linux desktop functionality to just about any insufferably munged laptop unit, all in one swell foop, and does NOT wear out the CD-ROM drive like, say, a Mandriva One disk'll do to ya'.)  \n\nGummints ain't the only ones learning to like Linux, turns out.  Po' folk like it just fine, too.  Different reasons, but still the same love-on-contact.  MUCH simpler demos and testing, of course, compared to an entire State government's requirements. Easy to keep the warranty honored appropriately, too.\n\nSo:  Until/unless a customer up and explicitly asks me to KDE their Windows for them but insists to \"SAVE THE WINDOWS PLEASE!\", I think I'll stick to fresh, full-scale installs and a shop-standard initial packages/config formulation that consumer-minded low-end people do tend to value highly.  As long as it works when they get it home, though, y'all's exactly right - they DO NOT CARE what the actual code under the hood really is, who wrote it, or why it works at all.  Some might just rip DVDs, many surely do cruise the Net for oh whatever, and some just LOVE the Magnatunes service - a little sumpin' for free for Just Everyone sure helps draw grassroots folks over to the Linux Side.\n\nEven when the desktop does *not* look so very much like Mr. Gates' brainchild.  Muscular gorm at ones' fingertips is one thing; corpy-standardized eye-candy is another.\n\nThis manner of revitalization and recycling has proved to be a *great* way to make new friends and keep old ones very happy indeed, in my own humble experience over the last five years or so.  It keeps 'em out of the landfill and in the hands of poor-but-inquiring fellow humans.  This happy situation can only improve again and yet again over time."
    author: "Walking Turtle of ComPDQ"
  - subject: "If all goes..."
    date: 2008-02-03
    body: "If all goes according to the plan, then I'm a teacher in 7 years or so ;) I hope I can enjoy KDE-EDU on the school I have to work on - I hope it could be on a Linux platform, elso KDE4 on Win is okay too ;)"
    author: "Kristho"
  - subject: "Re: If all goes..."
    date: 2008-02-03
    body: "I really hope in 7 years or so you get to use modern up-to-date computers and OS's (and GUI also.)"
    author: "..."
  - subject: "Re: If all goes..."
    date: 2008-02-04
    body: "And I really hope taht I will be using higher version of KDE after 7 years. At least KDE 5..."
    author: "Andrius"
  - subject: "Why?"
    date: 2008-02-03
    body: "Honestly, i can't find a reason why the windows platform should be saved."
    author: "foobar"
  - subject: "Re: Why?"
    date: 2008-02-03
    body: "me too, but it's an incredibly funny title :D"
    author: "jospoortvliet"
  - subject: "Sooo true foobar!"
    date: 2008-02-03
    body: "Embrace, extend and extinguish. That's the microsoft motto!\nThey may soon be begining to taste what it is like to be extinguished!"
    author: "mr_x1035"
  - subject: "Re: Why?"
    date: 2008-02-03
    body: "Forget windows, just think about open sourced win32 operating system \"Reactos\" running kde4 in future would be great combination. Right now both in alpha stage are incompatible (i have not tried out but reactos developers are eager to do so in near future). "
    author: "Dr. Asfak Motiwala"
  - subject: "Oblivion"
    date: 2008-02-12
    body: "Oblivion and other few notable games are still hardly playable under Linux. Few things are better than sex and a new Elder Scrolls game is, so I need to have Windows somewhere :( ."
    author: "Wiseman"
  - subject: "It might sound"
    date: 2008-02-03
    body: "It might sound strange but Windows is like television. So many of my friends including myself don't miss a TV set anymore. Unthinkable 12 years ago.\n\nApple is gaining market share, Vista does not really sell... Even Windows applications start to work out of the box with wine.\n\nWindows is a large plattform with many developers and many users. KDE on Windows will be another step to get the choice of your OS irrelevant for most of the users. Most applications I used on Windows were free: firefox, thunderbird, azureus, freemind... They run out of the box on Linux as well.\n\n"
    author: "Ben"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "What do you mean? They run out of the box on Linux as well. Are you for real?"
    author: "Raphael Emportu"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "He means all those apps he listed run fine on Linux."
    author: "Ian Monroe"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "I miss the days of excessive packaging for software.\n\nSo when I install anything, I first make installation CDs, 3.5\" floppies, and 5-1/4\" floppies, then I print out all the documentation in a non-standard form factor, and spiral bind it (or put it in custom made 3-ring binders). Then I create some simple box art, print it on a piece of paper that wraps around a cardboard box with corrugated inserts for strengthening. The installation media goes in the cardboard box with the documentation, and all that goes on the shelf to collect dust."
    author: "Soap"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "Lol, that made my day. I think my favorite thing about switching to Linux was realizing that software could be installed by simply checking checkboxes in Synaptic... and if I didn't like it, I could UNinstall it by just unchecking those checkboxes! ('Course, now I do that with apt-get in Yakuake.) Trying new software and uninstalling it in less than 5 minutes - it was unreal! Installing software on Windows requires going through a bunch of \"Next\" buttons and giant splash screens that take up time, and uninstalling software from Windows was a dark art.  (Sometimes programs left registry entries that caused trouble, or even left their Start Menu entries resulting in broken links.) Installing Software... now there's something where Linux really beats the Windows experience.\n\nAssuming the program you want is in your repository... the playing field begins to even when you have to compile stuff yourself. :-/\n\n"
    author: "kwilliam"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "\"...the playing field begins to even when you have to compile stuff yourself. :-/\"\n\nI'm a Gentoo user, you insensitive clod! ;)"
    author: "Wyatt"
  - subject: "Re: It might sound"
    date: 2008-02-04
    body: "IMHO compiling yourself is overrated:\n\n1) Debian based distros (Ubuntu) have a far larger repository than i.e SuSE\n2) Quite often ./configure && make will already compile it. For KDE apps ./configure --prefix=`kde-config --prefix` && make\n3) You can use \"checkinstall\" instead of \"make install\" to generate a package.\n4) Now, uninstalling works like with any other package.\n"
    author: "Michael"
  - subject: "Re: It might sound"
    date: 2008-02-05
    body: "First of all you mean e.g. not i.e., and SUSE as SuSE is the old way :)\n\nYeah Debian's is bigger, but don't underestimate SUSE's repository it's been growing a lot since things became much more open after Novell took over. It's pretty amazing. Check these out: http://en.opensuse.org/Additional_YaST_Package_Repositories\n\nThe new 1 click install from SUSE is also pretty neat for things like \"Codec Packs\" or \"KDE4\". \n\nYou make compiling sound easy. But even if the compilation process is easy you need the appropriate development tools for it to compile (sometimes that's plenty) so you have to get and install them, and you also need to track and install all the dependencies for the compilation to be successful. Same if you want to use checkinstall. "
    author: "Santa Claus"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "Microsoft evidently doesn't think much of Vista, either. I read an article yesterday that said Microsoft is trying to fast-track Vista's replacement, which is not due out until the end of 2009!"
    author: "linuxaddict"
  - subject: "Re: It might sound"
    date: 2008-02-03
    body: "They 'fast tracked' 2000's desktop replacement, and then XP's replacement as well, did MS have no faith in those products either? Remember, Vista was originally a minor upgrade to XP planned for 2003. Oh my, that timeline seems familiar. 2001->2003 = 2007->2009."
    author: "SMB"
  - subject: "Portable KDE 4.0"
    date: 2008-02-03
    body: "For when I am forced to use a computer without Linux, I love \"portable\" apps, it would be awesome if KDE could work as portable app."
    author: "Vexorian"
  - subject: "Re: Portable KDE 4.0"
    date: 2008-02-04
    body: "go to pendrivelinux.com its main app is Gnome based, but it has links to portable linux versions that use KDE"
    author: "Anon"
  - subject: "Re: Portable KDE 4.0"
    date: 2008-02-06
    body: "SLAX works perfectly from a small USB pendrive, and it comes with KDE.\nwww.slax.org"
    author: "Paul Philippov"
  - subject: "Re: Portable KDE 4.0"
    date: 2008-02-11
    body: "Yes, and that would be awesome if it wasn't so usual that you have to wait ages to boot (I am talking of any live-cd distro) and that dchp is not too common around here so configuring internet is always a pain, or the fact, that for most computers I frequent booting another OS would get me banned from those places. But hey, if it wasn't for those things, I would really like to use Slax...\n\n"
    author: "Vexorian"
  - subject: "Nice Headline"
    date: 2008-02-03
    body: "Nice headline, but tell me something:  What color is the sky in your world?\n"
    author: "3vi1"
  - subject: "Re: Nice Headline"
    date: 2008-02-03
    body: "KDE blue"
    author: "Anonymous"
  - subject: "Re: Nice Headline"
    date: 2008-02-03
    body: "<i>Nice headline, but tell me something: What color is the sky in your world?</i><p>\n\nWhat is this Sky you speak of?<p>\n\nXp is a dying platform, like it or not. Microsoft EOL is coming. In that sense it is dying. The article though looks at a specific XP machine that was withering away from non usage.<p>\n\nMy daughter has spent the last 2 days playing all the new KDE Games on the test laptop. She had never even touched it before, preferring her Kubuntu machine. Now she is asking me to install KDE4 on her box.\n\nMrCopilot\n"
    author: "MrCopilot"
  - subject: "NO!"
    date: 2008-02-04
    body: "The reason is that KDE will not fix the security problems in MS-Windows.  The internet security issues are the main thing that is killing MS-Windows.  There are other problems with Vista, but the main issue is that the promissed security improvements simply aren't there."
    author: "JRT"
  - subject: "kde for windows?"
    date: 2008-02-04
    body: "no matter how hard it tries, windows just is not linux. and when linux tries to be windows, well, it just uses WINE. The funny thing is that ultimately one of these operating systems is superior and yet people and developers still waste their time and our security and enjoyment by supporting windows. I would like to say 'there can only be one' but that is not true, there will always be underdogs, like beos, windows however, I believe, with a little effort, will be wiped from the face of the earth and looked back apon only as a bad memory (0x05637e) from the time when micro$oft ruled our right to software freedom. KDE for windows?! Cool! Let people know how sweet linux managers are before giving them the real thing. and if this is not there thing show them bblean instead :) just my educated point of view i aint no flamer or prophet...\n\np.s. do something good for the environment."
    author: "knifemonkey"
  - subject: "Re: kde for windows?"
    date: 2008-02-04
    body: "You have to remember that Windows is not an OS, it includes an OS which was called NT and is based on DIGITAL VMS.  Microsoft sells \"Windows Services for UNIX\" which allows UNIX applications (NOT GUI) to run on an NT based Windows system.  So it isn't really correct to say that the problem is that Windows isn't Linux.\n\nThe problem comes with running GUI applications in a windowed environment.  AFAWK, MS-Windows doesn't use X11 for its windowed environment.  MS-Windows is all bolted together so we really don't know how modular it is, but I doubt that it is.  IAC, we don't know how to interface with it.  So, the only possibility appears to be that which WINE uses. which is to convert Windows calls into X11 calls.  This doesn't seem to be the ideal design, but I don't know if building a parallel windowed environment that could run concurrently with X11 would even be possible -- are MS-Windows and X11 too different to ever coexist?"
    author: "JRT"
  - subject: "odd"
    date: 2008-02-04
    body: "for some reason, each time I read the title, I see \"Can CDE Save a Dying Windows Platform?\"\n\nI have no idea why I get this weird effect."
    author: "yman"
  - subject: "Nice..."
    date: 2008-02-04
    body: "Kde is getting quite alot of these reviews/interviews. I like them; gives me something to read once in a while. Keep up the good work, KDE; keep the press coming. ;)"
    author: "Jeremy"
  - subject: "Irony"
    date: 2008-02-04
    body: "Please note, the title contains irony.\n"
    author: "Jonathan Riddell"
  - subject: "What !!!???"
    date: 2008-02-04
    body: "What the heck is a dying Windows Platform? Is it my XP which is the only OS that performs as it should and is stable. Linux is intrinsically unstable and Vista appeared slow to me (so I've dumped it). I do use Linux whenever I want to waste some time - it is not entirely good and not entirely bad, but it simply won't beat XP because XP just runs and Linux might run if tweaked sufficiently. Before you guys are getting a heart attck - I am talking about laptops here. Except for Lenovos and a few other models you won't be able to deny that Linux is having a hard time to cope with my favorite travel companions.\n\nNow to KDE: I have mainly used KDE, Gnome and XFCE - and there is no obvious reason why I should prefer either of them. Same applies to Windows - if you slow down my XP I could even live with Vista.\n\nAs a result: no, I do not think that KDE can save a dying Windows platform (whatever this might be). If those guys joined forces and created something like a KnomeFCE (combined advantages of course), you could try to ask me again.\n"
    author: "Bob"
  - subject: "Re: What !!!???"
    date: 2008-02-04
    body: "I've often wondered what it would be like if we had one Linux DE instead of at least three. But apart from the technical differences between KDE/Qt and GNOME/XFCE/Gtk, I think that people in the community do what they do because no-one forces them in such a direction. It's mostly a spontaneous development, led by peoples' individual choices. And that's both a strength and a weakness. About five years ago, I thought that eventually one DE would come out on top. Today I tend to think it doesn't work that way, and I don't think there will be a standard free desktop environment on Linux for at least the next five years.\n\nBTW: Am I entering a danger zone here? Let's post entirely on-topic:\n\nDon't get me wrong, I am pro-free software, pro-Linux and pro-KDE. But I do realize that in the end, it's not about the tools, it's about getting things done. I don't know if KDE on Windows is a good or a bad thing. But well, if it makes developers happy, I'm all for it. Happy developers are more creative, and I and many others get to enjoy the fruits of that :)."
    author: "starbase218"
  - subject: "Re: What !!!???"
    date: 2008-02-04
    body: "> intrinsically unstable\n\nNo, it isn't.\n\n> I am talking about laptops here\n\nFWIW, I just spent a week running Linux with KDE 3 (Kubuntu 7.10) on my laptop with no trouble, including many, many suspend/resume cycles, wifi usage, etc. For the record, my laptop is a Toshiba. Just because you have had issues doesn't mean the majority of others do as well."
    author: "Paul Eggleton"
  - subject: "Re: What !!!???"
    date: 2008-02-05
    body: "Good for you if K7.10 is working on your hardware. Unfortunately, most forums prove that you belong to an exclusive minority. I know that there are some Toshibas which work but others don't. I had the chance to check out certain models from Sony, Acer, HP, FSC and Compaq. All of them had (Linux) issues: mostly suspend/resume, sometimes wireless. I vaguely remember warnings about a certain HP desktop series which would not allow Linux to be installed. HP customer service reportedly had no clue.\n\nBut we all know or heard about these problems already, just some of us won't admit that too easily  ;-)\n\n"
    author: "Bob"
  - subject: "Re: What !!!???"
    date: 2008-02-05
    body: "I never said there weren't problems. I do however object to broad generalisations such as the ones you made since I do not believe them to be generally correct."
    author: "Paul Eggleton"
  - subject: "Re: What !!!???"
    date: 2008-02-05
    body: "In my experience, you don't really appreciate how good Windows really is and how hard it is to make a global consumer OS until you try one of these so called \"easy\" desktop-oriented Linux distributions.\n\nLinux is an OK system for servers. On desktops, it plain sucks. Yes, it is unstable, it's slower than Windows, it lacks quality apps, and hardware support is a pain. \n\nBut I guess Windows is a dying platform... in an alternate reality."
    author: "John C."
  - subject: "Re: What !!!???"
    date: 2008-02-05
    body: "The statement that Windows is a \"dying platform\" is pretty absurd, I agree. \n\nOther than that, would you care to provide some basis for your assertions?"
    author: "Paul Eggleton"
  - subject: "Re: What !!!???"
    date: 2008-02-05
    body: "I don't think I have to provide hard proof about lack of hardware and ISV's support. Also, the few third party drivers and applications that exist, are ridicuously hard to install. As for the rest, as I said, that is just my experience with both systems. Since XP was out, I've never experienced a Windows crash and Vista is equally rock-solid. On the other hand, I've had quite a few with Linux. As for the desktop experience, I've never seen a Linux desktop quite as responsive as Windows is (and Vista on good hardware is no exception, by the way). \n\nLinux has the upper hand when it comes to desktop features though: window management and built-in virtual desktops are far better in KDE. But that's about it."
    author: "John C."
  - subject: "Re: What !!!???"
    date: 2008-02-06
    body: "I would disagree with that. I use Linux and Windows daily. I prefer Linux but to do my work in development I have to use XP Pro on my laptop. No matter what I do, after a few hours of uptime on my laptop the process \"System\" begins to use 100% CPU, heats the laptop to death and causes it to almost lock up.\n\nOn the other hand, running large BT downloads can be done on Linux for days, even weeks at a time with no need for a reboot. The system is just as fast and responsive after all that time. It just seems for me, that with the poor memory management of Windows in comparison with Linux (why windows needs to use 250mb of swap when theres over 200mb RAM free I don't know) the desktop is generally more responsive than Windows.\n\nI can't dump Windows yet, and I will admit that on my hardware battery life is better with XP, but if I had a choice I would work soley in Linux."
    author: "Amara Emerson"
  - subject: "Re: What !!!???"
    date: 2008-02-06
    body: "I personally found the article arrogant.  Windows platform dieing, ah, no it's not.  So now we don't just have kde users bashing gnome, they now want to bash windows.  Boring and retard author of the article needs to get his facts straight before he day drams and shows the lack of his intelligence."
    author: "Richard"
  - subject: "Re: What !!!???"
    date: 2008-02-06
    body: "Did you read the whole article?\n\n-He wasn't bashing on Windows. The headline was just an attention grabber."
    author: "Max"
  - subject: "Re: What !!!???"
    date: 2008-02-10
    body: "> Linux is intrinsically unstable\nMay be you are referring to the bugs in the desktop environments or bugs introduced unwittingly by your distribution. Mind you, Linux (Linux kernel) is far more stable than windows (Windows kernel). If it appears unstable to you, then you must realize that it's because of applications which you believe to be part of Linux."
    author: "Anonymous Coward"
  - subject: "Fantasyland"
    date: 2008-02-04
    body: "You're obviously new to Linux and its' many Distro's.\n\nI've installed Linux over 300 times and NOT ONE of those computers have had ANY instability problems once the unstable code (Micro$oft products) was removed.\n\nOver 50 of those systems were laptops, Toshiba included....\n\nSorry guy, I think that you're stuck in noob fantasyland....\n\n-UncleMonkey"
    author: "UncleMonkey777"
  - subject: "Re: Fantasyland"
    date: 2008-02-05
    body: "My experience is quite opposite to yours? So?"
    author: "Caemyr"
  - subject: "Re: Fantasyland"
    date: 2008-02-07
    body: "I think that, if you experience an unusual amount of instabilities/crashes with Linux, you either have a bleeding-edge distro - in which case you could ask yourself if that is a wise choice - or you have screwed something up yourself. \n\nWhich does not mean that you are a \"dumb\" user. Linux sometimes forces you to go deep into the system, editing config files etc. If you aren't comfortable with that, you might be better of using Windows. IMO this is not fantasyland, it's fact."
    author: "starbase218"
  - subject: "Re: Fantasyland"
    date: 2008-02-07
    body: "BTW, it seems a bit strange to me that you would be aware of every instability issue of over 300 systems, especially if over 50 of those are portable systems.\n\nOr are you talking on a professional level, e.g. are you a Linux sysadmin? In that case, I wonder what it is you do all day long ;)."
    author: "starbase218"
  - subject: "Since when Windows is dying?"
    date: 2008-02-05
    body: "Cmon... since when Windows platform is dying? Just because Vista hasn`t hit the spot (it`ll take its time), while XP is right now a rock-solid Ms columnt, supported by Win2k and 2k3 (soon 2k8)....\n\nAnd what is supposed to \"save\" the \"dying platform\"? KDE and a bunch of 40 or so apps? Could you please cut out the FUD?\n\nNow, dont take me wrong. I`m not anti or pro anything. I use both Linux and Windows, having BOTH of those systems working rock solid, 24/7, 3x days a month etc. No, i dont reboot Windows more often than once a month (Windows Update), neither I reinstall it once a half a year. It works fine, no bogging down.\n\nI`m not against porting KDE to Win, why should i be? But KDE as a Windows savior? Who is gonna use it? Those who doubleboot Linux and Windows, maybe... It is not gonna replace Explorer on Windows. Most of Windows folk will just ignore it.\n\nJust be realistic, am i asking for too much?"
    author: "Caemyr"
  - subject: "Re: Since when Windows is dying?"
    date: 2008-02-05
    body: "Now Microsoft will became a KDE Platinum Sponsor, just like Nokia."
    author: "George"
  - subject: "Re: Since when Windows is dying?"
    date: 2008-02-05
    body: "He is talking about his own PC, on which he finds he uses Windows less and less. He is not talking about Windows in general.\n\nit's the difference between \"Can KDE Save a Dying Windows Platform?\", which speaks about one system, and \"Can KDE Save the Dying Windows Platform?\", which talks about all Windows systems."
    author: "yman"
  - subject: "Re: Since when Windows is dying?"
    date: 2008-02-05
    body: "Windows isn't dying. By a long-shot.\n\nThis is called great copy-writing. A title like \"windows is dying\" will catch more eye-balls, than \"KDE is offering programs for Windows\"-yawn. Every journalism major knows that. A good article needs an attention grabbing headline!! Then the actual article can condition the initial statement.\n\nAs it is. Great news about KDE on Windows. Great news for operating system agnostic cross-plattform users. "
    author: "Richaard"
  - subject: "Once again, the elitism of the Linux crowd"
    date: 2008-02-05
    body: "As with a couple of the other not-anti-Windows crowd above, I use both Windows and Linux. A few years ago, I was one of the \"Linux is obviously superior to Windows\" crowd. That phase of my life ended when I started experimenting with Linux (FC3.)\n\nI'm running OpenSuse 10.3 on this computer, and dual boot that with Windows XP on my laptop.  Stability wise, I can't remember the last time Windows crashed on me since SP2 was installed on my old desktop a few years ago; my (new) desktop hasn't had issues with stability with Linux, but I have had some with my laptop.\n\nSecurity? I'll start accepting comparisons when Linux has more than 1% of the client market penetration that Windows has. Seriously, who even tries writing viruses for Linux? You might knock out a server here and there, but the vast majority of the world wouldn't blink an eye.\n\nThe big difference, though, is that Windows Just Works. Linux doesn't. Part of it is the fact that I'm still fairly green with Linux, but if you want to get rid of Windows, then Linux better be easy enough to use that someone far less capable as an admin than myself can use it out of the box. On my laptop, I can't count the number of hours trying to get ndiswrapper to work properly (still haven't gotten it.) I built the desktop with an ATI card; no luck with fglrx to load properly. Motherboard died, decided to upgrade to a SLI system; no luck with nVidia's driver working properly either. CUPS I got working, but it took some digging (it was easier on my old desktop with Xubuntu than OpenSuse.) Yes, a lot of it is that drivers are first and foremost written for Windows. Nevertheless, I have gained an appreciation for the fact that, despite being a massive OS encumbered with a not-so-great historical code base, Windows has a phenomenal ability to cope with a massive range of apps and hardware and not bat an eye.\n\nIf all you're doing is basic office and internet stuff, Linux is fine.  A step beyond, though, and you're headed into command line interfaces, dealing with hardware compatibility issues, and in general a bunch of stuff 95% of users are absolutely unwilling to deal with. I like Linux; I really want this computer to never see Windows (mainly to save the $110 on Vista Home Premium.) But it really isn't ready for the mainstream user yet, and it's annoying that half the websites I go to for Linux advice, news, etc. have people that are extreme OS elitists."
    author: "Bob"
  - subject: "Not the same Bob"
    date: 2008-02-05
    body: "Didn't notice that someone above me had already used my standard pseudonym. Not to be confused with the first Bob."
    author: "Bob2"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-05
    body: "problems with hardwaresupport won't go away as long as the market share of linux is relatively small.\n\nI general it means that when you buy hardware, you should be aware that you need to buy stuff that is linux compatible.\nIf you do so, linux will perform like a breeze, withouth the hassle of installing drivers, etc. \n\nYou see the same with Vista at the moment: most of its problems are related to the fact not all hardware has drivers ready for it.\nDoes that make Vista unfit for mainstream users?\nMain difference with linux is that Vista is forced upon new users (finding new pc's with XP is getting more and more difficult), so it will gain its market share automagicly and become more mainstream suitable as well.\n\n\n"
    author: "whatever noticed"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-05
    body: "Agreed.  Both systems have their good points.\n\nAs to the stability issue, the crashes I had before SP2 were mostly due to shoddy firewall drivers like those belonging to Agnitum Outpost or Kerio - the latter of which gave me a BSOD every 12 seconds like clockwork until I used system restore.  When my last graphics card fried (an ATI Radeon) Windows found a problem with agp440.sys and wouldn't even get me into safe mode.  \n\nLinux, however, booted just fine.\n\nI use Ubuntu 7.10 and I find Envy to help a LOT with loading graphics card drivers.\n"
    author: "Ryan"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-05
    body: "That's the reason why I'm looking forward to KDE on Windows.\n\nI really want to use Plasma, widgets, compositioning effects under Windows too.\n\nWhile I use Linux a lot, there are a lot of production environments that I can't use Linux, or am not allowed to.\n\nNot to mention my gaming rig. It needs DirectX 10, and as everybody knows, dual SLI configurations are at best iffy under Linux. There is no way I'd even expect cedega to support this. To make a long story short: I NEED WINDOWS FOR GAMING. I WOULD ABSOLUTELY LOVE TO USE KDE as a window manager though and all the apps I'm used to from the Linux world.\n\nSo please guys, stop with the Holier than thou!! attitude and listen. We're all on the same side. Just cause we need KDE to run on Windows/OS-X does not mean we're any less loyal to the Linux version, or would *gasp* abandon it.\n\nHaving KDE on Windows isn't just nice to have - It's a necessity.\n\nBesides it will be great when I can synchronize my next Nokia smartphone under Windows/Linux/Solaris/whatever else I can dream up without any problems. I would love to use the same desktop Plasma apps on my Cell phone as well as on the desktop.\nThis would be an iPhone on steroids!!! Running full blown KOffice on the mobile device (and CarPC) as well as on the desktop. INCLUDING WINDOWS!!!"
    author: "Richaard"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "\"I really want to use Plasma, widgets, compositioning effects under Windows too.\"\n\nSo you need to port them yourself - these are not part of KDE/Windows.\n\n\"LOVE TO USE KDE as a window manager\"\n\nKDE is not a window manager, KWin is. And KWin is not going to be ported, because Windows already has native one. On of important rules here is not to break existing functionality by introducing KDE."
    author: "j"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "Aww...\n\nShucks. I was hoping that the rest of KDE will be ported too. I remember in the Windows 3.11 days, on could replace Microsoft's window manager (\"Program manager\" at the time) with one by a company called \"PC Tools\". It was great, and I loved it.\n\nWhy can't we do that for KDE too? \n\nAt the very least, port Plasma. This way people could write the same widgets and be platform independent. That would be swell."
    author: "Max"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "Can't remember the last time Windows crashed since you installed SP2?  What planet are you from?  Did they release a different version of SucksPee there to the one they did on earth?  \n\nWindblows SucksPee basically sucks so badly it practically inhales itself.  I came off Windblows after my XP installation (with SP2) crashed 633 times in 2 weeks.  \n\nSince I went onto linux, I have forgotten what a Blue screen looks like (other than the one which pops up when I boot my computer, telling me I have a Hewlett Packard!).\n\nGet a grip.  I went from Windblows to Linux in just under 3 days, including working out how to change permissions, limit access, list directories and set up my printer.\n\nOut of the box, the best one is Mandriva by far.  Practically sets itself up.\n\nTrust me.  If I can use linux, you should have no grief at all."
    author: "Thor Malmjursson"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "Hey, I happen to like XP Media center edition. (don't use the actual media center functionality though)\n\nIt's the best version of Windows Microsoft released. (other than Windows 95 :) )\nIt's easy to use and works with everything. It's what my Game machine is running.\n\nI love linux as much as the next guy, but please give them credit, where credit is due.\n\nSome pragmatism please. Linux, KDE, is an operating system/desktop environment - NOT A RELIGION.\n\n"
    author: "Max"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "Didn't need to go too far to prove Bob's point, I guess.\n\nLinux can really do without the people that live to bash Windows.  It makes the community look stupid and immature.  \n\nGM doesn't run an advertising campaign bashing Ford.\n\n"
    author: "Ryan"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "(although God knows that after the rear-exploding tank thing they could have)"
    author: "Ryan"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-07
    body: "There are realy two planets here.\n\nSince the XP launch I experienced *much* more crashes in linux than in windows, given that the time I used linux was only an fraction of the time using windows. By the way, my last crash in Windows was one year ago triggered by Mplayer, and it's been like 4 years since my last reinstalation of Windows, while I re-instaled all linuxes frequently.\n\nI share bob's opinion, Windos Just Works for me, while linux requires days and many search and how-tos to set up just the basic functionality. \n\nAnd I too like lots of things in linux, and wanted to use only it if possible and pratical. That is not the case for me yet... but YMMV."
    author: "Manabu"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-06
    body: "> The big difference, though, is that Windows Just Works. Linux doesn't.\n\nI'm using Linux for about 10 years now, KDE since the 1.x releases. Working in a software company I can afford to use Linux all over the day for my development tasks. We're developing inhouse applications for one of the world's largest mail order companies. Do you know what? Until about three years ago, there was mainly Windows (except Unix on the servers). Today, there is Linux wherever possible. Workstations which are set up to run only inhouse applications are Linux-based. Nearly all servers are Linux-based meanwhile.\n\nThe only lock into MS are Office (because some people like nonsense macros as copying the Word document title into the file attributes) and Exchange. Ha, have you ever seen such messages sent to mailing lists where a sender wants to recall a message?\n\nWell, Exchange competitors are on the way, OpenDocument is on track. Vista is clearly a no-go for larger installations. Give Windows another five years, but then...\nReally, big companies change their IT behind the scenes. It's not as public as some governments do. Lots of people were surprised to see inside those companies."
    author: "Andreas"
  - subject: "Re: Once... - Great Writeup above!"
    date: 2008-02-11
    body: "Yea, that's true.\n\nGreat writeup! It's very true. Most business applications are platformindependent web applications. At that point nobody will care what operating system runs underneath.\n\nMy Masters program teaches all about \"Analysis of Web-based Systems and Design\". That's where the future is.\n\nHence Microsoft wanting a slice of the pie. Vista isn't making any money. (Force-feeding it on preinstall doesn't make much money, as OEM's get volume licenses.) MS-Office days (well years) are numbered. We'll see that when they release a odf-compatibility bugfix/servicepack. The large customers will demand it. (in Europe at least) The U.S. is pussy-whipped too much by Microsoft to have a spine.\n"
    author: "Max"
  - subject: "Re: Once again, the elitism of the Linux crowd"
    date: 2008-02-07
    body: "You appear to looking at Linux through a MS-Windows frame.  Since *NIX (this applies to all UNIX like OSes) is not Windows, this tends to distort the issues.  The virus issue on *NIX is fairly simple.  A virus modifies an executable file.  On a properly configured *NIX system, only root can modify an executable file so a virus needs to obtain root permission to propagate itself.  I think that you will find that this is considerably more difficult on a *NIX system (including OS/X) than it is on Windows.  Yes, some of this is configuration, but on *NIX a secure configuration is the default.\n\nA serious issue is hardware support.  Windows types call this drivers, but *NIX doesn't use \"drivers\" like Windows does.  Actually, a great deal of the hardware support issues are due to the Windows driver syndrome.  You might call your printer issues a driver problem, but really it is an issue with the RIP and/or spooler software that converts from file you are printing to the format of the printer and sends it to the Kernel to be printed.  The actual driver is the Kernel module that drives whatever I/O port that you have the printer connected to.  I do not recommend CUPS because it is overkill for a stand alone system, but LPR is no longer supported.  KDE needs to step in and fill the void.\n\nThe driver syndrome is the fact that the Windows architecture with plug-in drivers means that a piece of hardware doesn't need to follow any standards as long as the manufacturer writes a driver for it.  This is not a good thing.  Good luck getting it to work on future versions of MS-Windows after that piece of hardare is discontinued.\n\nI hope that there is a special place in hell reserved for those that have caused the current graphics card mess.  Unlike other hardware, this is partially a driver issue since the Kernel needs to have a DRM driver for your graphics card, but it is mostly a software issue since what is needed is to get X11 and OpenGL to work with your graphics card.  This is  done by using X11 (X11 does have graphics card specific modules commonly called drivers) and GL libraries that are specific to the graphics card being used.  This is not the ideal solution, but it did work OK till ATI and nVidia decided that they would no longer provide documentation for their new products.  In the past, there were published industry standards for graphics cards.  The last two were the VGA standard which IBM fully documented and the VESA extensions to it.  The current situation with proprietary graphics cards is untenable.  We can only hope that Intel and AMD remedy the problem completely.\n\nTo make this clear.  Proprietary hardware that doesn't conform to published standards is EVIL -- published industry standards are needed.  This is not something wrong with Linux except that Linux has a harder time dealing with the problem than MS-Windows does.  But there are still issues with Windows that are in someways worse than Linux.  With Linux, if there is support for hardware then you can probably get it to work on any version.  For example my old Epson LQ1000 still works on the current version of Linux because it doesn't require a \"driver\" -- just the RIP software (it also helps that the printer comes with full documentation).  With MS-Windows, the situation is that you need a driver for that piece of hardware and that version of MS-Windows -- if you can't find a driver, you are dead."
    author: "JRT"
  - subject: "Sounds good, but..."
    date: 2008-02-17
    body: "KDE is built into Linux at the moment, and rightly so too. Why put onto the unstable Windows OS when Linux looks as good has the same apps (with cheesier names). If you want an easy to use OS like window I would highly recommend PC Linux OS [ http://www,pclinuxos.com ] it is on both KDE and Gnome. It is very fast nand esier to use than Windows XP or Vista (wizard galore!)\nIn conclusion moving KDE to windows is daft. Particularly because Window's kernel32 is so unstable on less powerful machines it will crash continuously!\n\nAgree? Disagree?  Post a comment"
    author: "Gray"
  - subject: "Re: Sounds good, but..."
    date: 2008-03-15
    body: "You haven't used Windows since Windows ME, isn't it?\n\nThe Win2K kernel -and the XP one, too- is very stable. In 6 years of intensive daily use, I have seen only 2 hangs. Since I switched to Ubuntu (6 months ago), I've seen at least 4 hangs on Linux; complete freeze, not even Ctrl+Alt+Backspace could save it.\n\nWhich one is the unstable one?\n\nI've no doubt that other Linux distros can be a stability marvel; Gutsy has a lot of broken corners, and feels more like a beta than anything. I only hope Hardy it's much better at it, because (hangs or no hangs) I'm not gonna go back to MS; I value my new freedom too much."
    author: "It's me"
---
As a longtime KDE user forced to use Windows, is the recent announcement and availability of a port of <a href="http://windows.kde.org/">KDE for Windows</a> a dream come true?  "<em>KDE 4.0.0 was released and there again was much joy. More importantly an actual honest to goodness Windows port is released.</em>"  Blogger MrCopilot gives us <a href=http://mrcopilot.blogspot.com/2008/01/can-kde-save-dying-windows-platform.html>a hands on review</a> with 50+ screenshots of KDE in action on that other operating system and tries to answer that question. KDE on Windows is not yet ready for the masses but hopes to be declared stable for KDE 4.1.



<!--break-->

