---
title: "KDE Commit-Digest for 19th October 2008"
date:    2008-12-27
authors:
  - "dallen"
slug:    kde-commit-digest-19th-october-2008
comments:
  - subject: "Thanks Danny!!!"
    date: 2008-12-27
    body: "Thanks a lot for continuing the Commit-Digest series.  I really appreciate all the effort you put into it!"
    author: "Steve"
  - subject: "Re: Thanks Danny!!!"
    date: 2008-12-27
    body: "Go Danny, thanks for all the work :)"
    author: "ianjo"
  - subject: "Skip to 2009?"
    date: 2008-12-27
    body: "Hey Danny,\nGood to know that you're still working on the digests. Can I suggest skipping all the digests between October and now, and simply start fresh? I like seeing what's happening currently with KDE trunk. Or simply do a recap of October -> December's work and then starting weekly updates again from January?\n\nCheers,\nSeb"
    author: "Seb Ruiz"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "For the love of Seigo, I fully agree!\n\nDanny, please realize that commit-digest are best served _fresh_. Would you want to read a newspaper from two months ago?\n\n\nThanks,\nMark.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-29
    body: "If danny-boy decides to skip digests to catch up, I'm gonna complain about it - just to screw with your mind :D\n\n\n(and I think he should do whatever he likes. I'll enjoy the digests no matter what)\n"
    author: "jospoortvliet"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "I also would like to recommend you to skip to 2009."
    author: "szotsaki"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "Sigh. Not again."
    author: "Sander"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "I do not agree with that. Sure it is nicer to get last week's digest but having archives of all weeks is also nice in the long term and is better to expose the work of the developers who committed cool things during that period. I guess that one person is not enough to handle such digests and Danny could use some help (no, i do not volunteer for the task)? With real life coming across i guess this sort of delay can happen again. If i remember correctly Danny finally published all late issues a few months ago, without skipping any."
    author: "Med"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "This comes up a lot.\n\nPerhaps it is time to hold a poll on whether people want to read commits that are months old, or if they do want them skipped to the more recent.\n\nSo how about asking the readers of dot through a poll what they want.  Because unless Danny catches up, very unlikely, this will come up each time a post is made.  So ask the readers what they want."
    author: "R. J."
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "If you read the Digest, Danny has already stated his plans.  A poll would be pointless: it's his series of articles, and his decision to make."
    author: "Anon"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "and of course if you read the digest, you would realise that there is no time line that Danny has set out to do that.  So a poll would not be pointless, and would show what the readers of dot would prefer."
    author: "R. J."
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "A poll would NOT be pointless.\n\nI am interested in digests. I like them. But I want them fresh.\n\nI am fine with a short summary. It does not have to be heavily detailed.\n\nBut if I am in December, and I get to read digests from October, I really am less interested in that - unless nothing would have changed in two months :-)"
    author: "mark"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-27
    body: "Fortunately, democracy (AKA mob rule) did not make it to the dot. Being in majority doesn't make you right, and you don't pay Danny to do this work, either. So he will do what he thinks is the Right Thing - and that's perfectly fine. After all, doing what you want to do (and sharing it with everyone) is the quintessence of the Free Software philosophy, isn't it?\n\nBig thx to Danny! Merry Christmas! (I know those are a bit late by now :P)"
    author: "slacker"
  - subject: "Re: Skip to 2009?"
    date: 2008-12-29
    body: "Please don't. If the \"old news\" aren't interesting to you then don't read it. \nI enjoy reading it all and I don't care if it's a bit late. Even months. The tech described isn't exactly mainstream so any exponation it gets is great."
    author: "Oscar"
  - subject: "You really need some help..."
    date: 2008-12-27
    body: "... to keep up.\n"
    author: "Michael \"Help\" Howell"
  - subject: "Re: You really need some help..."
    date: 2008-12-27
    body: "Indeedy.  Danny - what would be the best way for people to help you out?"
    author: "SSJ"
  - subject: "Re: You really need some help..."
    date: 2008-12-28
    body: "I did the digest for around 3 years. Danny around the same. The commitment of time and effort is relentless.\n\nI don't speak for Danny, not at all. Let that be clear.\n\nStart reading the commit logs. Subscribe to the email stream. Familiarize yourself with what is happening within. Get a feeling for the flow, get to know the participants. Familiarize yourself with the svn tree (not a trivial task). Read the lists.kde.org and get to know the participants and streams of thought. Start writing bits on what is happening. Discipline yourself to do it every week. Become obsessed.\n\nThen offer some help to Danny. \n\nProducing something weekly as a team requires almost as much work in coordination as doing it yourself.\n\nDerek (Who doesn't speak for Danny) "
    author: "dkite"
  - subject: "Thanks"
    date: 2008-12-27
    body: "Thanks Danny. I'm looking forward to future installations on whatever schedule your time permits. And I'd rather see them come out without skips, as you have planned."
    author: "T"
  - subject: "Thank"
    date: 2008-12-27
    body: "I don't mean to sound ungrateful, I'm not. I really like your digests. But could we perhaps automate the statistics part, and turn the text into a wiki form. The commit-digests are nice, but it's really losing its relevancy. "
    author: "Eric"
  - subject: "KnewsTicker"
    date: 2008-12-28
    body: "I am really sad that knewsticker has gone. I have to say that it is still not superceded by the current plasma-based RSS applets, no one of them fit well in a panel like Knewsticker did.\nFor example the fonts are really tiny and unreadable in a panel with the rssnow plasmoid."
    author: "Josep"
---
In <a href="http://commit-digest.org/issues/2008-10-19/">this week's KDE Commit-Digest</a>: Support for MTP devices, a script-based integration of the "Free Music Charts", and beginnings of a "first run tutorial" added to <a href="http://amarok.kde.org/">Amarok</a> 2. More steps towards supporting other planets than Earth in <a href="http://edu.kde.org/marble/">Marble</a>. GetHotNewStuff support in the "Comic" applet, a configurable auto refresh interval in the "Web Browser" applet, and a first version of a configurable "Pastebin" applet in <a href="http://plasma.kde.org/">Plasma</a>. KsCD is connected to the "Now Playing" applet using D-Bus. Support for Python-based Plasmoids. QEdje's wallpaper plugin reaches an almost-working stage. Shade and unshade buttons added to the <a href="http://oxygen-icons.org/">Oxygen</a> window decoration, with a new scrollbar design in the Oxygen widget style. RESTful web service access possible in Lokarest framework. Improvements in the Details view of <a href="http://dolphin.kde.org/">Dolphin</a>. Source Browser usability improvements, and less interface lockups in <a href="http://www.kdevelop.org/">KDevelop</a>. Work on a GeoNames annotation plugin for <a href="http://nepomuk.kde.org/">NEPOMUK</a>. Support for refreshing diffs in <a href="http://www.caffeinated.me.uk/kompare/">Kompare</a>. New syndication plugin (superceding the RSS plugin) added to <a href="http://ktorrent.org/">KTorrent</a>. Spellchecking returns to Lokalise. Initial commits to KPackageKit. Kapman moves from kdereview to kdegames. "System-monitor" Plasmoid moves to kdereview. Continued porting of KDETV to KDE4. Import of a first version of the Skype text protocol for <a href="http://kopete.kde.org/">Kopete</a>, ported to KDE4. KNewsTicker removed as superceded by Plasma-based RSS applets. <a href="http://commit-digest.org/issues/2008-10-19/">Read the rest of the Digest here</a>.

<!--break-->
