---
title: "KDE Commit-Digest for 16th November 2008 "
date:    2008-12-30
authors:
  - "dallen"
slug:    kde-commit-digest-16th-november-2008
comments:
  - subject: "Thanks Danny :)"
    date: 2008-12-30
    body: "I was about to send you some money as a (belated) Christmas present but the donate link on the digest is broken. Fix it and I'll make it worth your while ;)\n\nHope you had a good break & thanks again for all your efforts this year."
    author: "Martin Fitzpatrick"
  - subject: "Re: Thanks Danny :)"
    date: 2008-12-30
    body: "That might be a problem... PayPal suspended my account (about 8 months ago!):\n\"PayPal requires accounts within the financial services category to provide us with some additional information regarding their organisation. Upon a recent review, your account was identified as falling within this classification. Until we can collect this information, your access to sensitive account features will be limited.\"\n\nI tried to explain that I shouldn't be in the \"financial services\" category and that i'm not an organisation (or am I?!? *evil laugh*), but to no resolution. I guess i'll try again...\n\nWhy, PayPal... Why?!?\n\n:)\nDanny\n"
    author: "Danny Allen"
  - subject: "Re: Thanks Danny :)"
    date: 2008-12-30
    body: "Dear Danny,\n\nThis is odd, indeed.\nYou may publish your IBAN and BIC data in your digests, this would make it possible for donors to transfer money easily. If your account is in &#8364;, the cost for money transfer is even neglegible.\n\nWith regards,\nHendric\n\nPS: Thank you for your ongoing efforts and all the best for the new year!"
    author: "Hendric"
  - subject: "Re: Thanks Danny :)"
    date: 2008-12-31
    body: "That doesn't surprise me. I've heard many stories of people getting screwed by PayPal. Enough for me to steer clear from it."
    author: "slacker"
  - subject: "off topic"
    date: 2008-12-30
    body: "This will be a rather lengthy rant, so if you are a KDE fanboy: Don't read it.\n\nBreaking with tradition, summary first: In its current state of development KDE4 - I refer to the 4.2beta2 and trunk - are unuseable, at least for me. I'm a longtime KDE user\nsince 2.0 back then and I am excited and amazed about the new ideas and concepts and improvements to existing ones the developers made possible. But still it somehow ended up\nas an almost non-useable KDE version. And I don't mean stability, which is surprisingly good for a pre-rc version and most trouble probably stems from faulty graphics\ndrivers.\n\nLet's start with the most obvious and visual thing: Oxygen. To be honest, I like it. Although I love the rich KDE3 Baghira style, Oxygen definitely has something. It is more\nsimple and clean and has not much visual clutter. And I use the Oxygen icons even with KDE3, they are not as friendly and inviting as the cartoonish Crystal ones, but are\nmore distinct and clear. However, the Oxygen window decoration is a failure. Not from an artists but a users view, since it is missing a vital option: No window\nborders. Having window shadows borders are useless and always get in the way if you want to use a scrollbar on the edge on the window, even with maximed windows. Without\nwindow borders you just have to move your mouse to the screen edge and you are right, you don't even have to look. Of course, there are other window decoration styles, but as\nthere are way more for KDE3s kwin so there is no need to use the KDE4 one.\n\nOxygen toolbars are an improvement too, the possibility to lock them and remove the visual clutter from the drag zone is very useful and I would suggest to change KDE4 to\nhave all toolbars locked by default. However, the loss of the MacOS-like menubar at the top of the screen is quite severe. Not only does every window get its own menubar,\nwasting precious vertical space (in a 3200x800 twinview setup vertical space is crammed), it also makes it slower to actually use the menubar. Having it consistently at the\ntop of the screen you just have to move the mouse there, which is easy, but now you have to target way more precisely and spend more time on it, which is a no-go.\n\nThese two things exclude the KDE4 desktop from usage. There's one other thing: Plasma. Although I have to admit it looks stunningly nice and clean and can be very useful (I\nhave seen setups from some of my coworkers who use many plasmoids and a dedicated screen just to monitor system stats of several hosts at once) I never found any use having\nsomething on my desktop other than a wallpaper. This leaves the containers (aka panels). They are not really useable in a vertical setup yet (as written before I don't have\nmuch vertical space) but this will hopefully improve. However, they are mostly a wasted chance. Although there are important improvements to the systray plasmoid recently the\nwhole concept needs a complete overhaul. If I have an app starter plasmoid, let's say for Amarok, I click on it and it will start, continuing playing the song from when I\nlast quit it. Everything should be fine, right? Nope. In order to control it and not waste precious global shortcuts (and my keyboard had no \"multimedia\"-keys) there is a\nsmall plasmoid for the systray which gives access to the most important functions and the ability to hide the main window. But why? If I have a nice big Amarok symbol in the\npanel, why waste space for a second, although smaller, one? Why not use the bigger and easier to target one? I know, due to X specs and restrictions this would only work for\nKDE programs but that's what an integrated desktop is all about, right?  So when I click on the Kontact starter and the app is running the starter could take over all\nfunctionality currently crammed into the tiny systray area. An overlay number can show the amount of unread mail and it can spawn a passive popup when a meeting is\napproaching. This would free up space without reducing any functionality and could be applied to quite some programs like ktorrent (a larger icon could show the up/down\nnumbers) or konsole (for example, having a small indicator in the icon if on of the shells received a bell).\n\nAs the desktop as whole is already ruled out let's have a look at some of the KDE4 apps, since they can still be used while running KDE3. Ktorrent, Kget and Konqueror have\nalready replaced the KDE3 versions for me. The same goes for the Kontact suite and Kate. Dolphin &#8212; I never figured out why people like it so much. Maybe because I never used\nKonqueror for file management, either. Krusader, I use the KDE4 version, is much easier and more useful, at least for me. Only two things are very nice: The one-click file\nselection and the Nepomuk integration in Dolphin. One-click file selection could be enabled in many more KDE apps and all KDE open file dialogs which allow multiple file\nselections. It could even be extended to allow selecting files from different directories. The other part, Nepomuk integration, is probably even more important. Gwenview, one\nof the most used KDE apps for me, has picked up both functionalities, but Amarok and Digikam still uses their own tagging system.\n\nSpeaking of Digikam, which I quite like for my personal photo collection but it is in fact useless for my work, due to a simple fact: It decides if files are photos or not\nbased on their names and not their content, unlike all other KDE apps. This inconsistency makes it almost impossible to use it without heavy (and stupid) symlinking\nfrenzy. Give a file named \"foo\": Krusader, Gwenview and Krita and even Konqueror or Dolphin correctly indentify it as a png file and all is well, but Digikam stubbornly\nrefused to do so. I tried to create a patch myself, but my skill are not that good. What's the central filetype settings good for if it's not used (btw, this goes for both the KDE3 and the KDE4 version)?"
    author: "Stefan Waidheim"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Arrgh, these gnome disciples are everywhere these days. Don't feed!"
    author: "JJ"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "I will feed, because I find your cheerleader-esque dismissal of valid points highly offensive.\n\nI don't think the poster here is a \"gnome disciple\" if the things he values are configurability and flexibility to apply GUI conventions that improve his workflow. KDE, as a project, gained a huge following and spectacular reputation for accommodating such useful configurations.\n\nFor you to criticize or name-call probably does more damage to the reputation of the KDE project than the complaints in the original top-level comment.\n\nThink about how you respond to criticism before you type, next time. You might find you are being part of the problem."
    author: "Anon"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "http://i40.tinypic.com/211z5v.png"
    author: "ethana2"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Let's make a deal.\n\nThe instant KDE gives me the configurability and flexibility to apply GUI conventions that improve my workflow, I switch.\n\nWhat say you?"
    author: "ethana2"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "What is the reward of getting you to switch? \nWill you send free candy? \nA donation?\n\nI actually like that layout, however the way you worded your request makes you sound arrogant like if it were an honour to have you switch... this is FOSS, devs do what they like and if you want something you can either pay someone to do it or just do it yourself. If by any chance what they did in past actually pleases you, it does not mean they are forced to do it again just because you want it.\n\nA besides, have you looked around kde-look?\nhttp://www.kde-look.org/content/show.php/Netbook+Plasma+Theme?content=92433\n"
    author: "AC"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "I file many bugs on and provide much peer support for the software that I use."
    author: "ethana2"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Name 5 such features available in other GUI interfaces and not in KDE."
    author: "jospoortvliet"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: ">What say you?\n\nwhat do i care? use what ever suits you best and works. if thats kde, fine. if it ain't, look for something else or improve the things that bug you."
    author: "henk"
  - subject: "Re: off topic"
    date: 2008-12-31
    body: "> What say you?\nMove ZIG!"
    author: "slacker"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Gnome disciples? WTF? If you read the post, it's obvious he's a KDE user. It wasn't a superficial rant based on some screenshots, but valid  usability concerns by someone who has spent quite a bit of time with KDE 4.2 beta. While I may dispute the seriousness of many of his complaints, he is hardly a troll. "
    author: "David Johnson"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "> Not from an artists but a users view, since it is missing a vital option: No window borders.\n\nwow. one would think that this keeps the smallest minority of kde users ever up at night. can u elaborate on what makes no window borders so important to u?"
    author: "jay"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "I think he has explained it: Scrolling via mouse (not scroll wheel) is a bit easier for him. However in fullscreen mode Oxygen has no window borders on KDE 4.2 Beta 2 and thus you can scroll via mouse by just pushing it to the right border and then pressing the mouse button and move up/down.\n\nFor me smaller window borders save screen space (I have a Samsung NC10 with 1024 x 600, so every pixel counts) and remove a small visual bug on the left button at the window title bar with enabled stripes in background. I have a custom title bar with: left \"close button\" - center \"window title\" - right \"minimize button\" \"maximize/restore button\" (IMHO the most useful setup, as none needs the program icon button and the close button should be separated anyways: however Windows users will get confused :p).\n\nSo an option for very small window borders would be really nice. Maybe we don't need normal/big Window borders at all, as there is the glow effect around Windows, which in itself has the effect of a border.\n\nBTW Konqueror on KDE 4.2 Beta 2 has no scroll focus by default on new tabs which is odd. You need to click inside the Window until you can use your scroll wheel.\n"
    author: "Arnomane"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Right click on your window decoration, Configure Window Behaviour, Go to \"Moving\" and enable \"Allow moving and resizing of maximized windows\" gives me Borders to my windows. (in 4.1.3) :)\n\nI don't know if that still works in 4.2 Beta 2 but perhaps it might be an acceptable compromise to our complainer :p\n\n"
    author: "Mark Hannessen"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "It actually work on 4.2 trunk (future RC1). So I think it solves the problem."
    author: "SVG Crazy"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Almost all of this post is a rant because someone is used to some things that were part of KDE 3.x and are easily solvable by a quick \"use-KDE3\" command. But I find the Digikam one to be a serious complaint. That really introduces an inconsistency that shouldn't be there. Can anyone of the digikam list have a fix for that? Add me in the \"thanks\" list ;)."
    author: "Alejandro Nova"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "I'm glad I waded through the poor formatting to understand what this person was saying, because now your reply is more useful to me.\n\nSee, these are ALL \"serious complaints\" to someone who considers feature parity important. I guess we can now know for sure that you don't consider feature parity important.\n\nI'm thankful for what has been done too. However, I am not so crass or inconsiderate to think that those who seek feature parity, configurability, flexibility, and good GUI design should be brushed off dismissively or otherwise ignored."
    author: "Anon"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "It baffles me why people still post stuff they label \"off topic\".  If it is \"off topic\", then post it in its proper forum."
    author: "Juan Miguel"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "How can features she admittingly doesn't even use be called failures? It's just a bunch of nonsense. Nobody forces her to use KDE4 at all.\n\nDamn, i took the bait.\n\n\nbtw, that Digikam-trouble is easy to solve: just switch the 16bit support on in preferences."
    author: "Sebalin"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Here's what I find offensive about your post:\n\nYou rant quite accurately about the system tray. And then claim it's a reason \"KDE4 fails\". Well, quite succinctly: screw you.\n\nI started Plasma to build a base upon which we could fix these kinds of problems such as the system tray *mess*.\n\nNOBODY has had the balls or foresight to do this to date. I complained for a couple years about it then just rolled up my sleeves. I've sat down a few times with Seli of KWin to talk about these issues and we came up with some solid ideas.\n\nIn 4.2 we finally have a system tray widget with the backend to accomodate these kinds of changes. Of course, in the meantime, we *had* to support the existing system, so we did that. That, in your mind, is the failure.\n\nBut now we can actually address this set of issues due to the last year of *hard* work we've done on Plasma. \n\nNow, where were you 3 years ago when I was writing about how crappy these things were? Where is your \"KDE3 sucks!\" rant? Nope .. non existent.\n\nInstead, you decide to blame KDE4, the only saviour in *sight* for this problem. I've blogged about the impending demise of the existing system tray and why the current system is wrong.\n\nBut honestly, what is in that system tray right now in svn is better than anything in KDE2 and on par with what's in KDE3 and, unlike in KDE3, has the ability to go beyond it (including our intentions to do so).\n\nThe rest of your rant is similarly misguided.\n\nYou complain about advanced new features or one app here or there that misses some detail. If you've been around KDE as long as you say you have, you know that details amiss is nothing new, but you ought to at least recognize KDE 4.2 for what it is. And it's not \"unusable\" as you state. I do hope you stay away from it, though, as I really don't need to deal with users of your ilk."
    author: "Aaron Seigo"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Dear Aaron,\n\nas much as I appreciate your coding work, this kind of replies to user's comments really makes me wonder about your ability to stand and to handle criticsm. To be sure, I, too, don't know why missing window borders are showstoppers. And I also don't share many of the other criticsms the OP has.\n\nHowever, I would not dismiss this post as \"trolling\". That is the  comfortable attitude that seems to be prevailing among KDE devs nowadays if somebody dares to criticse their work. The OP pointed out stuff he likes and stuff he  dislikes. He gives his opinion why he doesn't use KDE4. He doesn't say \"KDE sucks\", though in some points he might have formulated his opinion more carefully, this I admit. Thus IMHO posts like this could be as well interpreted as constructive criticism from a KDE user - but taking users' criticism into  account seems to be a no-go currently.\n\nFor the record, I'm usind KDE4.2pre and I like it. I think KDE4 is with this release finally going to be what 4.0 ony promised to be. And I dislike the way many people are flaming KDE4 and you developpers. \n\nHowever, I also would like you devs to be a bit more aware of user's feedback. Too often in the KDE4 release cycle I had to witness that criticsm I share  has been, without further consideration, brushed away as \"trolling\". Giving me sometimes the bitter taste that user feedback  is not really appreciated  if  it  doesn't fit in the developpers' philosophy."
    author: "Noname"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "\"as much as I appreciate your coding work, this kind of replies to user's comments really makes me wonder about your ability to stand and to handle criticsm.\"\n\nPeople can only take so much before they snap - having followed Aaron's posts and read the comments he receives on blogs, mailing lists, forums etc, I think he shows admirable restraint.  And if you read his reply, he focuses only on one specific criticism, which is one that I'd argue was both unjust and rather ignorant: railing at someone who is actively trying very hard to solve the difficult and longstanding problem you're complaining about is really bad form and basically the exact opposite of constructive criticism because it discourages further work on the problem.\n\n\"However, I also would like you devs to be a bit more aware of user's feedback. Too often in the KDE4 release cycle I had to witness that criticsm I share has been, without further consideration, brushed away as \"trolling\".\"\n\nI tend to judge KDE devs attitudes by the way they respond to actual bug reports, rather than random posts on blogs and forums, and from this they seem, at worst, no worse than any other volunteer-driven project."
    author: "Anon"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "An interesting problem which has arisen recently is users thinking they are clients/consumers.\n\nPeople who have the habit of being served, in a shop, in a restaurant, when calling for support. There, the customer is always right, in that it is the right for the customer to have long/ignorant rants and be answered with a smile.\n\nBecause in the commercial world, there is a layer between the customers and those who do the stuff. It is in fact there not only to provide a punching ball for the customer, it is also there to shield the engineers.\n\nBut this is free software. It used to be that people spouting nonsense would be greeted by flames and RTFM. And it is good that it is no more the case. However, the fundamental reason for this state of being was that ignorance was not the norm. Because free software becomes successful, it has become the norm.\n\nSo now flames go the other way, from the ignoramuses to the knowing. Who are without a shield.\n\nBut the basic fact remains that highly intelligent and technically competent people enjoy crafting software for other technically minded people who appreciate what they do. This is what makes free software tick. Harsh criticism is accepted in the free software world. Being polite is not even expected.\n\nBut being logical, consistent and technically correct is still expected from those who count (the devs) from their users. And those flames are taken badly because they are _wrong_. They increase the noise to signal ratio, and this hurts the community."
    author: "hmmm"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "The customer may be wrong, may be ignorant, and may even be an idiot, but the customer is always deserving of respect. In the Free Software community, the user is the customer. By making your software Free you are inviting everyone to be a user, even those you don't like. Respect those that use your software."
    author: "David Johnson"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "http://www.kde.org/code-of-conduct/"
    author: "David Johnson"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "BS.\n\nWe have a saying in our business. The PAYING customer is always right.\n\nWe don't sell widgets, we sell our time and expertise. If someone doesn't want to pay, they get nothing.\n\nIt is no different with free software.\n\nWe should be thankful that we can use what we can for free. For the full benefit we have to pay, in time, effort, development effort, building relationships, whatever.\n\nOtherwise we deserve nothing.\n\nDerek"
    author: "dkite"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "As always Aaron is right. So let's stop the ranting for a moment and start looking at each point:\n\n\n>And I don't mean stability, which is surprisingly good for a pre-rc version and most trouble probably stems from faulty graphics\ndrivers.\n\nHow true. With KDE3 vendors could slack off and still mostly got away with it but Qt4/KDE4 started exercising their drivers and most of them were not up to it. Lately there have been a lot of improvements, after all who wants a product with a slowliness sticker on it, but a lot of bugs still remain.\n\n\n>However, the Oxygen window decoration is a failure. Not from an artists but a users view, since it is missing a vital option: No window\nborders.\n\nAs you wrote yourself, there are other styles so this point is invalid.\n\n\n> [] I would suggest to change KDE4 to have all toolbars locked by default.\n\nRather not. Distributors could do this, but upstream should never. Maybe include a TotD about it, but don't make it default.\n\n\n> However, the loss of the MacOS-like menubar at the top of the screen is quite severe.\n\nThere's a plasmoid which offers this. Create a slim panels at the top and put it in there. Since KDE4 runs native on MacOS X this option had to be moved.\n\n\n> [] They are not really useable in a vertical setup yet []\n\nThis has changed recently. A lot actually.\n\n\n> However, they are mostly a wasted chance.\n\nExactly the opposite, they give us the possibility to _use_ this chance. When building a house you start with the foundation, not the furniture. But some of what you describe has already found its way into KDE4 in the last weeks. Not all will be in 4.2 but distributors will surely backport some of it. After all, KDE4 is not even one year old. How long did KDE3 to cook?\n\n\n> One-click file selection could be enabled in many more KDE apps []\n\nThis is already being done. And yes, I consider it very useful, too ;-) Dolphin is a general-purpose filemanager, while Krusader is an old-school type one. They serve different purposes and attract different userbases (I like Dolphin a lot more), so it's unfair to scold Dolphin for being different from Krusader. That's its purpose.\n\n\n> The other part, Nepomuk integration, is probably even more important.\n\nNepomuk is a long-term goal. Its more the workflow and usage concepts that's missing than technology, which is already there. So in this point you can actually help by giving examples of your daily workflow and ideas how Nepomuk could improve or speed it up.\n\n\n> Speaking of Digikam []\n\nI don't understand this one. Please give a bug number or a more detailed description. It sounds important."
    author: "Karl"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "Dear Aaron.\nI appreciate your coding work and I appreciate your way of dealing with critizism. You can't please everybody do no need to even try. You always manage to be civil to those who repetedly critizise your work and that's great.\n\nThanks for everything. Wish I could use KDE4 more than I do. I wish I could use it at work."
    author: "Oscar"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: ">No window borders.\n\nHave you ever even used kde? If click on the window borders like you did in kde3 there is an advanced menu...select \"no borders\" . Problem solved.\n\n> Small plasmoid for the systray which gives access to the most important functions and the ability to hide the main window.\n\nIt isn't a plasmoid, it is a systray icon for those people that are not running kde. You can disable it in the amarok options (there aren't many so it won't be too difficult to find)."
    author: "txf"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "> Have you ever even used kde? If click on the window borders like you did in kde3 there is an advanced menu...select \"no borders\" . Problem solved.\n\nHum well this option does a bit too much... Where is my window title bar gone and how do I restore it? ;-) Anyways have a look some posts where up I replied to somebody else on the window border thing and (hopefully understood the OP correctly).\n"
    author: "Arnomane"
  - subject: "Re: off topic"
    date: 2008-12-30
    body: "hmm... yeah, I have only used to once or twice in the past, but I can't remember the hotkeys that put it back to normal. Fullscreen has the same effect, but you can actually reset it by unticking when right clicking on the entry in the taskbar.\n\n"
    author: "txf"
  - subject: "Re: off topic"
    date: 2009-01-02
    body: "ALT-F3 brings back the window menu so you can change the setting. KWin tells you about that if you disable the borders (unless you told it not to bother telling you...)"
    author: "jospoortvliet"
  - subject: "Re: off topic (I don't get it, people)"
    date: 2008-12-30
    body: "Honestly, I find this to be a breath of fresh air. If the OP had explained first and concluded later (instead of \"However, the Oxygen window decoration is a failure. Not from an artists but a users view, since it is missing a vital option: No window borders\", how about \"However, the Oxygen window decoration is missing a vital option: No window borders. For me, this makes it a failure. Not from an artists but a users view.\".\n"
    author: "Michael \"Ding, Dong!\" Howell"
  - subject: "Go Go Danny !"
    date: 2008-12-30
    body: "Many thanks Danny for your excellent job. And a very happy new year for 2009!"
    author: "Christophe T"
---
In <a href="http://commit-digest.org/issues/2008-11-16/">this week's KDE Commit-Digest</a>: KUIServer applet merged into <a href="http://plasma.kde.org/">Plasma</a> "System Tray". Wallpaper configuration added to the Plasma screensaver. Support for drag and drop of images onto the new "Pastebin" Plasmoid. Support for unloading/reloading Plasmoids from the Python interpreter. More work on PolicyKit integration in KDE. Hidden panel glow hints when using Plasma with a compositing window manager. Improvements to SQL playlists (incompatible with previous saved playlists), and an importer for iTunes added to <a href="http://amarok.kde.org/">Amarok</a> 2. Unfinished Amarok Plasma applets moved to playground in preparation for the release of Amarok 2.0. Sonnet-based spell checking in <a href="http://www.mailody.net/">Mailody</a>. Basic functionality to support collections on network shares, with better interface support for smaller screens in <a href="http://www.digikam.org/">Digikam</a>. Zeroconf support for sending and receiving notes in KNotes. Initial import of MobiPocket format support, and SVG images for PDF stamp annotations added in <a href="http://okular.org/">Okular</a>. Further progress on the reimplementation of the Windows Live Messenger protocol in <a href="http://kopete.kde.org/">Kopete</a>, including a merge into trunk. Further student project work on KBugBuster and Akonadi/Maemo. Start of sound support in KBlocks. Meaningful optimisations in <a href="http://strigi.sourceforge.net/">Strigi</a>. RAW thumbnail loader for <a href="http://dolphin.kde.org/">Dolphin</a> moved from playground to kdereview. Kephal screen management library moved from kdereview to kdebase.<a href="http://commit-digest.org/issues/2008-11-16/">Read the rest of the Digest here</a>.
<!--break-->
