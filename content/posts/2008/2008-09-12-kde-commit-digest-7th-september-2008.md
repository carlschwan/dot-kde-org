---
title: "KDE Commit-Digest for 7th September 2008"
date:    2008-09-12
authors:
  - "dallen"
slug:    kde-commit-digest-7th-september-2008
comments:
  - subject: "noscript fot konq"
    date: 2008-09-12
    body: "Is there plans for a firefox's noscript addon for konqueror?\n\nI know that konq have the option for create a \"white list\", but i think is no easy. For example, youtube.com. With noscript i knew that ytimg.com is necessary to watch videos. And only with one click. But with konq, you can't know that althought you take a look the html code or let the whole page load the scripts.\n\nBye\n\nPD: thanks for the digest"
    author: "Metoo"
  - subject: "Re: noscript fot konq"
    date: 2008-09-12
    body: "Thanks Danny, a joyfull read as usual :P"
    author: "Shuss"
  - subject: "folderview activity"
    date: 2008-09-12
    body: "\ni am using kde4.1.64 and for the desktop i use the folderview activity, i have nothing against desktop activity except the fact i can't rename icons, and i don't like ( visually) putting the folderview applet on my desktop.\nmy questions is there a way to change the folder of the foldervoew activity( by default it is /home/myhome ?\nany plan to add arrange icons vertically or horizontally ?\nor better any plan to make applet icons smarter, ( rename, copy etc ...)\n\nthanks for your work "
    author: "mimoune djouallah"
  - subject: "Re: folderview activity"
    date: 2008-09-12
    body: "> is there a way to change the folder of the foldervoew activity\n\njust the other day i added a generic Configure Activity to the cashew, so now you can get to the desktop configuration dialog. from there we'll be adding a way to configure the plugin itself.\n\n> plan to add arrange icons vertically or horizontally\n\nyou mean grid, etc. alignments? yes, it's on the TODO.\n\n> any plan to make applet icons smarter, ( rename, copy etc ...)\n\nalso on the TODO, not sure how much time i'll personally have for this before 4.2 though. i'm already pretty full up."
    author: "Aaron Seigo"
  - subject: "Re: folderview activity"
    date: 2008-09-12
    body: "> except the fact i can't rename icons, \n\nI can't either (using trunk), but since the \"Rename\" menu item already is there, it's probably only a matter of time till it gets implanted.\n\n> and i don't like ( visually) putting the folderview applet on my desktop.\n\nThen use folderview as your desktop (containment). Coming in 4.2 if it isn't already available.\n\n> my questions is there a way to change the folder of the foldervoew activity( by default it is /home/myhome ?\n\nIf it's the folderview applet, you configure it the same way as the other applets: press on the wrench in the hover menu.\nUnfortunately I don't know how to enter the configuration dialog for the containment.\n\n> any plan to add arrange icons vertically or horizontally ?\n\nI think the plan is to save the icon positions. Not sure about it, though.\n\n> or better any plan to make applet icons smarter, ( rename, copy etc ...)\n\nAlready in trunk (although, as I mentioned, rename doesn't work correctly for me at the moment)."
    author: "Hans"
  - subject: "Re: folderview activity"
    date: 2008-09-18
    body: "\"Then use folderview as your desktop (containment). Coming in 4.2 if it isn't already available.\"\n\nIt's availale in 4.2 Devel. I actually tried it but had a very unpleasant surprise after trying to switch back to the normal desktop. \nThis option was nowhere to be found. I had to go to the .kde4 folder and changed the containment plugin from folderview to desktop. I am sure that this will change though since KDE 4.2 isn't even alpha yet. Until then Newbies should avoid using this option ;)"
    author: "Bobby"
  - subject: "update : just use kde4.1.67"
    date: 2008-09-20
    body: "thanks really just one week and my wishes are here, thanks plasma dev"
    author: "mimoune djouallah"
  - subject: "Mac OS-like menu plasmoid"
    date: 2008-09-12
    body: "Is the Mac OS like-menu plasmoid going to be ready for 4.2?"
    author: "LP"
  - subject: "Re: Mac OS-like menu plasmoid"
    date: 2008-09-12
    body: "Yeah, id like to see this much missed KDE3 feature come back in KDE4(.2?)"
    author: "Ljubomir"
  - subject: "Re: Mac OS-like menu plasmoid"
    date: 2008-09-12
    body: "It will only if anyone fixes the issues with it. I have talked to Friedrich Kossebau who started it (in playground/base) and he meant that there are serious problems which he is not capable of fixing (and the other developers he asked seem to not be capable, either)."
    author: "Stefan Majewsky"
  - subject: "Re: Mac OS-like menu plasmoid"
    date: 2008-09-13
    body: "Capable as in time to care ;) In theory it should be easily portable, using the old trick from KDE3 with making the KMenuBar toplevel and then embedding the window in the plasmoid. I once had it half working, KMenuBar and friends even have all the code still active.\n\nProblem: You will have a clash of the normal Qt style with the Plasma one, once the menu is embedded. I heard someone is working on a solution which will take the menu data, freedesktop-styled also from GTK apps, and make a menu itself, such being able to use the Plasma style. No idea how far this is.\n\n"
    author: "Frinring"
  - subject: "Re: Mac OS-like menu plasmoid"
    date: 2008-09-13
    body: "What about the X-Topbar (or whatever it is called) from the Bespin style? It seems to work to work quite well (of course not for Non-kde-apps), it just could do with a few configuration options.\nI don't know what plans Thomas has got for it, maybe it could be made into a plasmoid independently from Bespin (which is a really cool style, just seems not to be as stable as Oxygen is).\n\nAlex"
    author: "alex"
  - subject: "Re: Mac OS-like menu plasmoid"
    date: 2008-09-13
    body: "Yes, it was Thomas, now I remember. For those curious about Bespin, see here: http://cloudcity.sourceforge.net/"
    author: "Frinring"
  - subject: "Re: Mac OS-like menu plasmoid"
    date: 2008-09-13
    body: "It's not been touched in 4 and a half months, so I wouldn't bet on it:\n\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/menubar/\n\nUnfortunately, Friedrich has his hands full with Okteta.  Any developers who are waiting in the wings: if you want this feature, it looks like you'll have to step up and help out, as the KDE guys are short-staffed :/"
    author: "Anon"
  - subject: "Extenders style."
    date: 2008-09-12
    body: "Those extenders look pretty cool. One thing I think that is slightly wrong with them, and would make them look even better is the fact that they are detached from the panel.\n\nInstead of rendering them as a separate window like this:\n\n                ,----------.\n                |          |\n                |          |\n                |          |\n,---------------|          |--------.\n|               `----------'        |\n|                                   |\n\nIt should be rendered attached to the panel like this:\n\n                ,----------.\n                |          |\n                |          |\n                |          |\n,---------------'          `--------.\n|                                   |\n|                                   |\n\nThat will probably require new functions in the theme engine and some code to snap the window to the top of the panel, but I think it would be worth the effort.\n\nHmmm HTML is disabled for some reason so no shiny ASCII art :-("
    author: "Tim"
  - subject: "Re: Extenders style."
    date: 2008-09-12
    body: "Use non break spaces :\n\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0,----------.\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n,---------------|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|--------.\n|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0`----------'\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0,----------.\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n,---------------'\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0`--------.\n|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|\n|\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0|"
    author: "Shift"
  - subject: "Re: Extenders style."
    date: 2008-09-12
    body: "Ah cool. Still didn't quite work with my font though. How do you input nbsps without &nbsp; or copy/paste lots? Is there an Alt-Gr shortcut?\n\nSpeaking of which, the alt-gr shortcuts for things like &#8592;&#8595;&#8594;&#8593;, \u00d7\u00f7\u00b9\u00b2\u00b3\u0080\u00bd\u00be and so on are one of the things I miss massively on windows. Does anyone make keyboards/stickers that have all the symbols on them?"
    author: "Tim"
  - subject: "Re: Extenders style."
    date: 2008-09-14
    body: "I used the compose key (Windows key) : Compose, Space, Space\n\nThe funny thing is that few days ago I have updated the wikipedia page http://en.wikipedia.org/wiki/Non-breaking_space#Keyboard_entry_methods to add this shorcut :)\n\nI used this shorcut one time. After, I copy/paste the non breaking space.\n\n"
    author: "Shift"
  - subject: "Re: Extenders style."
    date: 2008-09-13
    body: "i sent an email about a month ago or so mentioning we'll want to eventually extend Plasma::PanelSvg to paint \"join areas\" ... just need to actually write the code for it. so much to write; at least i know what i'll be doing for years to come ;)"
    author: "Aaron Seigo"
  - subject: "Re: Extenders style."
    date: 2008-09-15
    body: "Awesome. Have fun! :-)"
    author: "Tim"
  - subject: "My experience with kde 4.1 "
    date: 2008-09-12
    body: "I\u0092m using kde 4.1 (kubuntu packaging )  in daily basis. Overall it is pleasant to work in kde 4.1. But there are some annoyances which I hope they will be gone in the next releases. I will start with big ones:\n1- There are no a real web browser in the KDE!!\nReally, I can not use Konqueror  to visit my lovely websites (google (docs, gmail ) , my own website witch has tinyMCE editor , blogspot etc \u0085 ) . Therefore, I use firefox which means I\u0092m forced to use gnome (dialogs and applications).  I do not care about KHTML or webkitPart because both of them are in miserably statue in kde 4.1. \n2- Ark \nI want ask the kde developers this question: Do you really use ark in daily basis? Sometimes, I doubt that kde developers use gnome file roller!!  Anyway, I hear somebody working on Ark and that good thing!\n3- When I download new Plasmoids from internet, they do not appear in plasmoid list. I think this is a bug in kubuntu itself. \n3- Spelling check while writing in kwrite does not work !! But when I search in the kde bugzilla , I find it fixed. I want ask, do I miss something? \n4- I know this is a crazy idea but do we really need menu bar in all the applications ( specially in Konsole , Kwrite , dolpin ) Frankly, this idea popup in my mind when I see  Google Chrome and Microsoft office 2007.  \n\nSorry for my poor English.  \n"
    author: "Zayed"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "1. You can use https://addons.mozilla.org/de/firefox/addon/7962 or wait for a good webkit integration into konqueror.\n\n2. Ark is right now under heavy development and I think it will be shiny in 4.2.\n\n3a. The download works only for plasmoid which are written in a scripting language. There will hopefully a solution for this in 4.2.\n\n3b. Works for me with 4.1.1\n\n4. Konsole -> View -> Show Menu Bar, you can do the same in many other KDE apps."
    author: "cheko"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "1. Or wait for this: http://dot.kde.org/1218543988/"
    author: "cheko"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "Regarding number 4, If I disable menu bar how can I reach the settings option ? I think (correct me if I'm wrong ) most the application settings reach only menu bar. My idea is to element the menu bar and redesign the GUI without menu bar. I (and I think most end users) do not use the menu bar that much unless if we want to tweak something. The tool bar is more than enough most of the user.  Just imagine kde without Menu Bar ! "
    author: "Zayed"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "Quoting: (not literally)\n> My idea is to eliminate the menu bar and redesign the GUI without menu bar.\n\nTry to design an interface for a program (preferably one with many switches and options) without a menu, e.g. any of the KOffice programs. I don't even know how Chrome handles advanced internet settings (proxy, cache config, or privacy settings *g*), but I'm pretty sure that, to get what people tend to call a \"simple interface\", you will end up removing options from the user. (There is a possible example in the FLOSS world, but I do not want to cause a flame war.)\n\nMy point is: A menu bar is very good for the usability. It is good for organizing a large number of actions (at least if you do not overdose it, 70 actions is a good border from my intuitive feeling). Combined with a toolbar, you can hide most of the possible actions from the novice user, providing him with a standard set of common actions while still enabling the poweruser to use the full functionality of the program. (It should now also be clear why accelerator key sequences are such a natural extension to the menubar/toolbar paradigm: Novice users can ignore them, and they are very useful to powerusers.)\n\nOf course, your point is that the menubar is just unused space for most of us. Yet, I do not see a problem in hiding the menubar as you can always get it back through Ctrl-M. I use this myself in Okular: I need much space to view many text at once, so I have hidden everything (literally, from menubar through toolbar, to navigation pane; I have configured some keys for size changes). In Konqueror, I do only have the address bar visible (which, for me, also contains the navigation actions).\n\nOne thing I expect to be promising are round context menus which popup around your mouse and allow you to select common (probably context-sensitive) actions just by remembering how to move the mouse (this is called \"motion knowledge\" or similar, and is known to be very effective). I always wanted to implement that, but never got to it. Hackathon, anyone? ^^"
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "Well, I always remove Kopete menu, and add a button for preferences on the icon bar.\nGoogle chrome is a very good example how a interface can be very well designed to work without a menu, it have two buttons for all options, and that is it.\n\nSeriously, menu in apps such as konsole is really strange, and on my eeePC I always use my browser in full screen mode (opera because it's the most fast and that uses less resources I've found)."
    author: "Iuri Fiedoruk"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-14
    body: "We should remove the menu someway. At least to bring the Ctrl+M to kdelibs so all applications would follow it. I know that many KDE developers are against it because it \"confuse\" users if they accidentaly hide the menu. But the fix for that is to make it not to be a activated by default. So when user press Ctrl+M it does not get hided. The \"Settings\" menu could not include the option for hiding the menu. So for user who wants to have menu hided, they could just simple go first to system settings shortcuts and enable the ctrl+M option so it then would work for all applications.\n\nI love when I can save the screen space and make the GUI more intuitive by hiding the menu on Kopete, Konsole, Dolphin, Konqueror, Okular, Gwenview, System Settings (Default) etc.\n\nBut then I hate using Kontact (Kmail) because I cant just simply hide the menu, because I dont need it at all (same for my old aged clients who does not need it at all!). Same goes Amarok, we have three menu options what we dont use on daily basics... we should have possibilities to hide the menu and get clean simple and intuitive user interface if we want. \n\nReason to force us to have menu (not possible to hide it) is stupid because always there is referred to situations where few users has hided it someway and has not got idea how to get it back. And reasons too that you have important settings in menu, what you should always get change to set.\n\nA) If the setting is important, do not place it to menu, place it to toolbar.\nB) If the setting is not used daily or basics situations, do not place it to toolbar but place it to menu.\nC) If the user interface must be a intuitive and clean, allow users to edit the user interface (change buttons to toolbars etc).\nD) If you allow user to edit the UI, allow them to hide the menu if wanted.\n\nI think we should have new kind menu buttons. More like \"menu actions\". Idea is that we can add buttons to menu, what functions like anykind drop-down list, but we can edit what is on that drop-down list. \nAnd this \"menu action\" would act like any other button on menu. It follows the setting of text position and icon/text settings. \n\nThen when we would have the action menu, we could hide the menu bar, and add to these action menus the settings what we want to use from menu. But getting a more intuitive UI if we want. \n\nOR we could make it so, that when the menu is hided, it get added as \"action menu\" to toolbar as last one. So the menu would never actually be gone. It would be samekind as menu on GIMP when you press the small arrow on the top-left corner. Or addons what you get to Thunderbird and Firefox, to get menu hided.\n\nI use a lot the sub-notebooks (those small 8,9\" laptops) and you really need to fight to save every pixel on the 1024x600 monitor. KDE4 is just too big for such monitor. Firefox and Thunderbird are great applications for it because I can hide the menubar and add it as button for toolbar.\n\nThats why I love those applications what I mentioned earlier, and I dont like the amarok, dragonplayer etc applications, what are unintuitive because they dont allow hiding the menu and configuring the application so well. \nI am waiting the Kaffeine ported for KDE4, because it allows me to have player without toolbars just by pressing M.\n"
    author: "Fri13"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "Discussing KDE 4.1 is off-topic here.\n\nDiscussing current developments:\n\n1- KHTML is progressing very fast these days, and in other news there is a Qt port of firefox underway.\n\n2- most developers probably use tar in the command-line...\n\n3- what's the point of mentioning kubuntu bugs here?\n\n3- probably fixed in trunk (i.e. 4.2)\n\n4- apparently you didn't know that you can enable/disable the menubar in each application. In konqi/dolphin it's Ctrl+M. In Konsole it's in the View menu.\n"
    author: "ac"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "Number 3 is actually a kde-look bug, but the point still stands. It's off-topic."
    author: "Jonathan Thomas"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "s/bug/problem\nThe lack of content moderation isn't exactly a bug."
    author: "Jonathan Thomas"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "ok as a person who moved from windows to linux I don't understand a lot of what I read\n\ncan someone tell me what a qt firefox will do different from the current one, which look in Opensuse is built on XUL what ever that is"
    author: "Anon"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "\"can someone tell me what a qt firefox will do different from the current one\"\n\nIt will use KDE's native toolkit, meaning Firefox will look and feel like a regular KDE application."
    author: "Anon2"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "> It will use KDE's native toolkit.\n\nDoes that mean that Firefox will not take ages to load?"
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "\"Does that mean that Firefox will not take ages to load?\"\n\nIt should load faster and use less memory."
    author: "Anon2"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "\"Does that mean that Firefox will not take ages to load?\"\n\nFirefox takes me 2 seconds to load up on OpenSUSE.  Actually I am rather impressed by how quick things load compared to what I used to use, Vista."
    author: "Anon"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "I'm also on openSUSE. Firefox loads 8 seconds (cold start) and 3 seconds (warm start)."
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-14
    body: "Forgot to mention that I do not have any addons installed."
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-16
    body: "Dude, a Commodore 64 with twin cassette decks would be quicker than Vista :-)"
    author: "Phil"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "\"It will use KDE's native toolkit, meaning Firefox will look and feel like a regular KDE application.\"\n\nNo, it will look and feel like a regular *Qt* applications.  As far as I'm concerned, Qt applications stick out only barely less than GTK apps.  The lack of KDE's awesome file dialog (Qt's one is nowhere near as functional) is just one major difference."
    author: "Anon"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "There was some rumour on k-c-d that Qt 4.5 will allow application developers (kdelibs developers in our case) to replace several standard things, including the file dialog, in Qt applications. Hopefully, Oxygen can become the default style for all Qt applications through this (not that I dislike Cleanlooks, but I want it to be consistent)."
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-14
    body: "actually not even that. firefox has its own toolkit. it will only use qt for rendering. so the buttons might look like any qt program, but the feel will still be exactly the same.\n\nfirefox will never integrate on linux. it never was integrated in gnome. the only thing we may get is a different open/save-dialog.\n\nkdes future is either an even more advanced khtml, or webkit. don't hold your breath for a useable firefox..."
    author: "ac"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "Is there a way to disable all menubars for all applications with just one move?"
    author: "Alex"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "You could recompile kdelibs after applying a patch which sets the menubar height to 0 pixels. *SCNR*\n\nBack to reality: It is perfectly possible to add a global configuration option which would apply to all menu bars. (Of course, applications who handle their menubar manually, will have to be fixed, but these will be very few.)\n\nBe sure to file a bug for this."
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "I'm not aware of a completely universal option, however every XmlGUI KDE application (99.9% of them) can have it removed from it's respective RC file. It's usually $KDEDIR/share/kde4/apps/<name>/<name>ui.rc; the only one I know of that isn't is Konqueror, which has a proper configuration option. If you want update safety, copy it to $KDEHOME/share/apps/<name>/<name>ui.rc and tweak it there.\n\nKDEDIR?=/usr/\nKDEHOME?=~/.kde or ~/.kde4\n"
    author: "Michael \"XmlGui\" Howell"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-14
    body: "That will not work if apps manipulate the menu bar manually. If they attempt to manipulate the menu bar, one will automatically be created."
    author: "Stefan Majewsky"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "i use ark fairly regularly, actually. it was rather broken in earlier kde 4 release, but it seems to be getting progressively less broken and it looks a lot nicer. when it does occassionally let me down, i revert to the command line. i hope that won't be necessary ever in 4.2 =)"
    author: "Aaron Seigo"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "for number 4, vote it :-)\n\nhttp://bugs.kde.org/show_bug.cgi?id=168070\n\nbye"
    author: "dienadel"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-12
    body: "Is there a way to disable all menu bars for all applications?"
    author: "Alex"
  - subject: "Re: My experience with kde 4.1 "
    date: 2008-09-13
    body: "ad 1: As webkit is now backed by three major vendors we can expect it to progress quite fastly and get large scale user testing. <i>Therefore, I use firefox which means I\u0092m forced to use gnome (dialogs and applications).</i> Contrary to popular belief firefox is no gnome app.\n\nad 2: http://techbase.kde.org/Projects/Utils/Ark The pieces for the KDE 4.x platform slowly fall into their place. Developers use command line tools."
    author: "Malte"
  - subject: "Small sound playing app?"
    date: 2008-09-12
    body: "Thanks for the digest, Danny! Always a pleasure to read.\nIs there a plan for a simple app to play sound files, just as Dragon Player for videos? Juk or Amarok are a bit too slow to start when listening to a file you double-click on in Dolphin (this looks like very awkward english but I'm too tired to rephrase it)."
    author: "Richard Van Den Boom"
  - subject: "Re: Small sound playing app?"
    date: 2008-09-13
    body: "Dragon works for sound files as well."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Small sound playing app?"
    date: 2008-09-14
    body: "Yep and we plan on improving the audio-playing experience in future releases.\n\nThere are also longterm plans to make Amarok 2 work well as a one-off player."
    author: "Ian Monroe"
  - subject: "KDE compile time"
    date: 2008-09-12
    body: "does anybody know the duration on a full kde(from svn) compilation? Please mention the RAM size, CPU power, cores .etc.. i want to compile KDE but im scared of the duration(a couple of years ago i compiled KDevelop... it took 8 hours(it think it compiled every it needed))"
    author: "zvon"
  - subject: "Re: KDE compile time"
    date: 2008-09-12
    body: "If you have Intel Core Duo and use \"make -j3\" (three jobs at once), and have 1 GB of RAM, and are using your distros packages for the basic stuff (including Qt 4.4) then you can get a basic KDE trunk (kdesupport+kdelibs+kdepimlibs+kdebase) up&running in less than 2 hours. Additional modules (kdegraphics, kdesdk, kdeedu...) don't take very long to compile."
    author: "ac"
  - subject: "Re: KDE compile time"
    date: 2008-09-13
    body: "Worth mentioning as well is that the two hours is for the initial install.\n\nKeeping it updated is usually 5 or 10 minutes and if you use kdesvn-build it can be completely unmanned."
    author: "JackieBrown"
  - subject: "Re: KDE compile time"
    date: 2008-09-14
    body: "I often had big problems with just doing an update \"svn up; cmakekde\". Compilation or linking did fail. I had to sweep the whole build-directory and do a clean initial making to get it run again.\n\nAnd also if it works it takes here minimum 30 minutes I think."
    author: "gttt"
  - subject: "Re: KDE compile time"
    date: 2008-09-13
    body: "On a Core 1 Duo with 2x1.8 GHz, 2 GB memory, I remember that compilation of the 4.0.2 tag (with all default modules) took about 3 to 3.5 hours.\n\nI do not have numbers for newer versions as I'm using kdesvn-build, and am also compiling qt-copy and some playground modules (which you probably do not want to do)."
    author: "Stefan Majewsky"
  - subject: "Re: KDE compile time"
    date: 2008-09-13
    body: "I do not know the exact time, only that it really takes looooong here (I guess 6-8 hours or so) to compile KDE4 with the following packages:\n\nkdesupport \\\nKDE/kdelibs \\\nKDE/kdepimlibs \\\nKDE/kdebase \\\nKDE/kdeadmin \\\nKDE/kdeartwork \\\nKDE/kdemultimedia \\\nKDE/kdepim \\\nKDE/kdesdk \\\nKDE/kdegames \\\nKDE/kdegraphics \\\nKDE/kdenetwork \\\nKDE/kdeedu \\\nKDE/kdeutils \\\n\nMy system:\nsingle core AMD Athlon(tm) XP 2200+ (1800 MHz) with 786MB RAM, but a nicely fast harddisk (what is fast for my motherboard).\n"
    author: "gttt"
  - subject: "Re: KDE compile time"
    date: 2008-09-14
    body: "The point is that compilation stresses not only the hard disk, but also the CPU and memory. KDE requires Qt and a big bunch of other libs, and loading these structures into memory takes quite some time."
    author: "Stefan Majewsky"
  - subject: "will there be a \"player window\" in amarok 2?"
    date: 2008-09-13
    body: "\namarok 1.x has two modes, \"playlist mode\" and \"player window mode\" ..i have been following amarok 2 from svn and i honestly have no idea where you guys are going with it ..but i guess i will have an idea when its more matured ..\n\nthere is one thing i always wonder though ..will there be \"player windows\" ..i can only see a playlist window .."
    author: "ink"
  - subject: "Re: will there be a \"player window\" in amarok 2?"
    date: 2008-09-13
    body: "No, there will be no \"player window\". That would be very XMMS'ish and old fashioned. Yikes :)\n\nHowever, such a widget could be easily implemented as a Plasmoid. I fully expect someone to whip up such a Plasmoid sooner or later.\n"
    author: "Mark Kretschmann"
  - subject: "What about printing?"
    date: 2008-09-13
    body: "Just a quick question: is any work being done on the KDE 4.x printing infrastructure?  I mean, before we had KDEPrint since KDE 2.2, but this was dropped in the 3.x ---> 4.x transition.  Any hope for a 4.x version of KDEPrint, eventually?\n\nI'm asking because I do have to print from time to time (ok, more often than that), and a good, high-level printing infrastructure that does not force me to be a \u00fcber-geek to get what I want on paper is, well, important to me.\n\nYes, I'm old enough to remember the days of lpr being *the* printing infrastructure of *nix (that and getting my OS on a 125MB tape... or having to string up a DEC-Writer II to the PDP-11 so that we could work... (sorry, having a flashback, here)) and I don't have that sort of patience anymore.\n\nCheers.\n\nAC"
    author: "Anonymous Coward"
  - subject: "Re: What about printing?"
    date: 2008-09-13
    body: "i remember lpr as well, though not quite as far back as you do =) and yeah .. it was the definition of annoying. well, sendmail.cf was the definition of annoying, lpr just liked to compete ;) anyways ...\n\nkprinter ..\n\nthere are 3 parts to printing: the UI for printing from an application, managing print jobs once they are in the print system and configuring printers.\n\nprinting from an app UI is now handled mostly by Qt and seems to be working pretty well for me these days. there was also a SoC project to bring a new level of usability to printing in FOSS and they implemented both a Qt and a Gtk+ version of their design; they presented on it at Akademy. hopefully we'll see it in Qt/KDE someday soon.\n\nwe have a new printer configuration tool courtesy of Jonathan Riddel. it's written in python and pretty straightforward.\n\nfor managing print jobs ... i'm honestly not sure what we have there right now. i probably should, though, shouldn't i? =P"
    author: "Aaron Seigo"
  - subject: "Re: What about printing?"
    date: 2008-09-14
    body: "(Maybe I should call myself 'aging coward', considering I'm old enough to remember the days punch cards and batch processing? Ah, dealing with the finer points of IEBPTPCH... Hmm...(flashback in progress))\n\nThanks for the reply A-man :-)\n\nSo if I understand you correctly, we lost KDEPrint in the KDE3 to KDE4 migration, but some of its functionality more or less came back, the only missing bit being print job management?\n\nWhat about adding printers (and their associated drivers)?  Is it as apparently clean & usable in KDE4 as it was with KDEPrint (KDE3)?\n\nNot quite off-topic, but if these rumors of HP building its own Linux distro are true, I'd suggest they work instead at adding a polished, full-featured and usable by non-geek printing infrastructure to KDE4. Since printing (and other \"little details\" of the sorts) appears to be too boring for hackers to work on, maybe a company with a paid staff, working on a project dictated by marketing (can't think of a better way of saying it) might be the only way to go in regards to printing.\n\nAC"
    author: "Anonymous Coward"
  - subject: "Re: What about printing?"
    date: 2008-09-14
    body: "I would hate to rely on hardware manufaturers for producing printing guis. We would then have the horror of completely divergent interfaces for different printers, hideous uis and branding everywhere...and... the horror of half a dozen systray icons just to manage a printer just like windows users get to enjoy."
    author: "txf"
  - subject: "Re: What about printing?"
    date: 2008-09-15
    body: "I am not talking about various manufacturers writing their own GUIs for their own printers only.  I am talking about HP (in this case) writing the KDE4 version of KDEPrint, about them writing a generic end-user-centric printing infrastructure.\n\nThis instead of writing their own Linux distro, which I fear would add nothing to the mix and be just another Linspire/Xandros/whatever.\n\nAC"
    author: "Anonymous Coward"
  - subject: "Re: What about printing?"
    date: 2008-09-18
    body: "http://techbase.kde.org/Schedules/KDE4/4.2_Feature_Plan#kdelibs\n\nSeems like John Layt <john@layt.net> is working on \"Reintroducing KDEPrint in some form, depending on what Qt offers\". Obviously making use of Qt when we can is a good idea...\n\nAaron already mentioned current progress (we've got everything basically licked except a Job Manager)."
    author: "Michael \"Techbase\" Howell"
  - subject: "Big Thanks"
    date: 2008-09-13
    body: "Big thanks for the constant effort by Danny to make it a joy to read and to stay informed. I love those digest reads!\n\nGnome folks should learn from it :) this is why i prefer KDE. NOT because KDE is better or worse but because the guys BEHIND KDE seem to be very very much in touch with the users.\n\nOf course thanks to the KDE dev guys. I think the KDE 4 move was rather painful actually - at least for me - but finally it seems to be spearheading in a good direction with a proper pace."
    author: "markus"
  - subject: "extender mockup"
    date: 2008-09-13
    body: "The extender shown in the video was pretty difficult to read (not only because of the bad font), so I tryed to do it better in a mockup.\n\nI also \"fixed\" it's frame. The current plasmaoid style has a to strange shininess look to be the default. Please, keep things easy to recognize for the brain!\n"
    author: "anon"
  - subject: "Re: extender mockup"
    date: 2008-09-13
    body: "Hmm, no attachment, here's a link: http://img137.imageshack.us/my.php?image=extendermockuphu2.png\n"
    author: "anon"
  - subject: "Re: extender mockup"
    date: 2008-09-13
    body: "fwiw, i agree =)\n\nthis is all done with svg's so should be easy to get something much nicer in. just requires some artist help.\n\nyou should get in touch with Rob Scheepmaker and give him a hand with the styling; he can do the C++ parts, and maybe you can do the svg parts =)\n\nyou can find all of us on plasma-devel at kde dot org."
    author: "Aaron Seigo"
  - subject: "Re: extender mockup"
    date: 2008-09-14
    body: "I agree with you about the text presentation: the way your mockup presents is information is much clearer and I'll be working on that. I also like your progress bar, which is a great improvement over the disk usage plasmoid's meter svg, which I just used because I needed something to use, and looks was not my number one priority. And indeed, I could add some more margins to make it a lot clearer.\n\nI don't agree about the draghandle though. The current one fits quite good in the default oxygen plasma theme (actually, it's the same element that's being used as background for the plasma tabbar). It looks more consistent then the one from your mock, imho. What's also a point is that often, not the whole area can be used as draghandle, simply because the widget that's there accepts mouse events itself. The calender for clocks for example, clicking there interacts with the calender, not the extender item. In this kuiserver case I was thinking about making the directory names clickable to open the containing directories.\n\nBut of course, we're quite some time away from 4.2, so things can still be improved. You obviously have some feeling for design, so why not join us on IRC or the mailing list and help out? :) And: you've got that progress bar as svg? Because I would be really interested in that."
    author: "Rob Scheepmaker"
  - subject: "Re: extender mockup"
    date: 2008-09-15
    body: "Mmmh, I agree with \"anon\", the default style it'stoo shiny and glossy, and your progress bar is plain ugly :) The mockup is just gorgeus IMHO, much more clear and easy to understand at first sight. \"anon\", please join the plasma artist team starting with the extender implementation :)\nAnd Rob, anyway great work!"
    author: "Vide"
  - subject: "Opensync & Akonadi"
    date: 2008-09-13
    body: "It took a very very long time until someone started to develop this plugin. Ok, now the development has started, but i hope that we can see some success in near future. I think that this plugin won't be that big, though development should take only a few days :D\nSee you in the gentoo portage tree ;)"
    author: "benneque"
  - subject: "What happened to these kind of comments?"
    date: 2008-09-14
    body: "Thanks Danny!"
    author: "Michael \"Thankful\" Howell"
  - subject: "\"return to source\""
    date: 2008-09-14
    body: "\nHi Rob Scheepmaker \n\nAbout the icon \"return to source\", I think it should just be a \"back\" icon(like a webpage), since most users will understand this.\n\n//ac"
    author: "ac"
  - subject: "Re: \"return to source\""
    date: 2008-09-14
    body: "Thanks for you suggestion, but Pinheiro has already created a nice icon recently which I will be using. It fits better with the plasma theme then the default back icon. The idea is that hovering this icon shows a small bar inside the applet, where the detached item will go to, so it is very discoverable."
    author: "Rob Scheepmaker"
  - subject: "Update broke KDE4.1..."
    date: 2008-09-16
    body: "The last update for KDE 4.1 (opensuse 11, factory) broke my KDE badly. I see the splash screen when I log in, but after that only the customized cursor and a background similar to a chess board. Since everything is blocked, I need to switch runlevels through SSH.... I hope someone fixes this soon!"
    author: "Klaus Gekackt"
  - subject: "Re: Update broke KDE4.1..."
    date: 2008-09-16
    body: "Maybe you want to install your display driver again?"
    author: "wanzigunzi"
  - subject: "Re: Update broke KDE4.1..."
    date: 2008-09-17
    body: "Add this repo:\nhttp://download.opensuse.org/repositories/KDE:Qt/openSUSE_11.0"
    author: "Dondalski"
  - subject: "Re: Update broke KDE4.1..."
    date: 2008-09-18
    body: "http://lists.opensuse.org/opensuse-kde/2008-09/msg00052.html"
    author: "Anonymous"
---
In <a href="http://commit-digest.org/issues/2008-09-07/">this week's KDE Commit-Digest</a>: A <a href="http://kphotoalbum.org/">KPhotoAlbum</a> developer sprint leads to various developments, including a new viewer and support for image "stacks". Initial lyrics support and a new "Albums" applet in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Support for export to OpenDocument text and HTML formats for certain file types in <a href="http://okular.org/">Okular</a>. More functionality in the <a href="http://plasma.kde.org/">Plasma</a> "Engine Explorer", an application for data engine development. More work on the "grouping taskbar" and "Weather" applet for Plasma, and new features in the wallpaper configuration dialog. A new Plasma wallpaper plugin, "Mandelbrot fractal viewer" based on <a href="http://eigen.tuxfamily.org/">Eigen</a>. Lots of new settings across KWin-Composite effects. Start of code for a "Plasma loader" in <a href="http://raptor-menu.org/">Raptor</a>. Experiments with using Jabber to propose/find network games in KSirK. Support for subprojects with CMake, and a generic "Source Formatter" plugin (with multiple backends) in <a href="http://www.kdevelop.org/">KDevelop</a> 4. Start of an <a href="http://www.opensync.org/">OpenSync</a> plugin for <a href="http://pim.kde.org/akonadi/">Akonadi</a>. An Akonadi "server configuration" KControl module, intended for use in KDE System Settings. Support for adding files through command-line arguments in <a href="http://en.wikipedia.org/wiki/Ark_(computing)">Ark</a>. "Instant search" is implemented in KCharSelect. More work on a new IRC implementation, and improved Kiosk support in <a href="http://kopete.kde.org/">Kopete</a>. <a href="http://nepomuk.kde.org/">NEPOMUK</a> query service, and kosdwidget move to kdereview. Import of <a href="http://techbase.kde.org/Projects/LokaRest">"LokaRest"</a>, an experimental framework to access RESTful web services. A new application, kReMail, is added to playground/pim. Import of "deKorator" KWin window decoration engine to playground/artwork, and a KDE4 port of Kvkbd into playground/utils. KColorEdit 2.0 is released. <a href="http://commit-digest.org/issues/2008-09-07/">Read the rest of the Digest here</a>.

<!--break-->
