---
title: "Meet the KDevelop Crowd"
date:    2008-02-09
authors:
  - "hfernengel"
slug:    meet-kdevelop-crowd
comments:
  - subject: "This is bad"
    date: 2008-02-08
    body: "I don't like this.  You are making it look as if developing other parts of KDE than KOffice can be enjoyable and lead to travel opportunities. :-)"
    author: "Inge Wallin"
  - subject: "Re: This is bad"
    date: 2008-02-08
    body: "Yeah, good point. Sure, [A][a][K][k]ademy is fun and all, but that's mostly because of the KOffice developers, of course. And Sebas. And beer. Hmmm, now I think about it, maybe it's just the beer.\n\nNa, I think it's a combination of all of those. Good food (say, pizza and beer but I have no problem with all the Indian food you guys love so much), good conversations. Sometimes good code, yes. And some writing for those unable to do coding :D\n\n\nMeetings are big fun, I'm looking forward to FOSDEM!"
    author: "jospoortvliet"
  - subject: "This is great."
    date: 2008-02-09
    body: "Glad you guys are having fun.\n\nCoding should be a fun experience. Great software tends to come from a fun environment. According to the \"ballmer peak\" (look up the cartoon) slight intoxication should help as well.\n\nCan't wait to see what will come of it a few days afterwards. I'm already planning to see lots of cool added features, bugfixes, and general improvements.. =)"
    author: "Max"
  - subject: "Quanta"
    date: 2008-02-09
    body: "Sounds Wonderful! Does this include any work on quanta?  Last thing I heard (I may be wrong) is that quanta was going to be integrated into kdevelop4."
    author: "Vladislav"
  - subject: "Re: Quanta"
    date: 2008-02-09
    body: "As Andras will be there too, I hope so.\n\nQuanta4 used the new kdevplatform - which kdevelop4 uses too."
    author: "Niko"
  - subject: "Website"
    date: 2008-02-12
    body: "Are there any plans to revamp the KDevelop website?  I've always found it pretty frustrating to use.  For example, I was on it a while back trying to find out if it has any Python support.  Here's what I had to do:\n\n* Go to the front page.\n* Find the \"Features\" link on the left-hand side.\n* Realise that clicking on \"Features\" does nothing at all.\n* Figure out that I have to click the \"HEAD\" link to the right that is insanely small (it's \"x-small\" in the CSS).\n* That page tells me absolutely nothing of use and directs me to the KDevelop 3.3 features page.\n* I go to that page, which only lists the features new in 3.3.\n* I realise that it missed KDevelop 3.4, so I check that.  Another partial list of features new in 3.4.\n* I check 3.2.  Another partial list of features new in 3.2.\n* I check 3.1.  Another partial list of features new in 3.1.\n* I check 3.0.  Another partial list of features new in 3.0.  But this one actually mentions the feature I am interested in.  Success!\n\nI don't think it's unreasonable to expect a website dedicated to a single application to give you a straightforward list of its features, but there doesn't seem to be anything of the sort available for KDevelop; a changelog spread across multiple pages is hardly the same thing.\n"
    author: "Jim"
---
It is the time of the yearto gather and spend some time on our favourite  <a href="http://www.kdevelop.org/">IDE</a>. Continuing the tradition to meet in cities famous for alcohol-based beverages and oversized servings of <a href="http://www.flickr.com/photos/ob8/72018306/">meat</a>, Munich was the obvious pick. Pretending to be a civilised crowd, we managed to convince the boss of the <a href="http://www.trolltech.com/">Trolltech's</a> Munich office to generously provide us with a room, a 4MBit SDSL line and lots of coffee.  Read on for the agenda and how any interested KDevelop helpers can join.





<!--break-->
<p>Here is the agenda:

<ul>
<li>12.04.2008 - 13.04.2008: Free for all KDevelop meeting, including mingling, 
tutorials, hacking and <a href="http://en.wikipedia.org/wiki/Pretzel">Brezn</a></li>
<li>14.04.2008 - 18.04.2008: Intense hacking, debugging, polishing and occasional 
blogging</li>
</ul>

<p>If you ever wanted to meet the KDevelop crowd, or if you are a KDE or free 
software enthusiast, or just curious, feel free to visit us during the 
weekend. If you are serious about contributing, consider spending some days during the 
week with us. Since the hacking-only room has a limited capacity, please contact us (harry<span>@kd</span>evelop.org) if you would like to join. Any other contributions (food, bribes, funny little gadgets) are also highly welcome.</p>

<p>Details about how to get there and all the other useful stuff can be found at 
the <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_Developer_Meeting_2008">KDevelop wiki</a>.  Many thanks to <a href="http://ev.kde.org/">KDE e.V.</a> who are covering the travel costs.</p>





