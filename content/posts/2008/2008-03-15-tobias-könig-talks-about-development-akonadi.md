---
title: "Tobias K\u00f6nig Talks About the Development of Akonadi"
date:    2008-03-15
authors:
  - "jriddell"
slug:    tobias-könig-talks-about-development-akonadi
comments:
  - subject: "Clarifications"
    date: 2008-03-15
    body: "It was nice to see some clarifications on what Akonadi is and isn't."
    author: "T. J. Brumfield"
  - subject: "KDE 4"
    date: 2008-03-15
    body: "Tobias K\u00f6nig says that KDE4 is stable. This is almost true. However, despite some nasty bugs in basic functionality we see on several levels that KDE4 is not complete yet, missing icons and the like. These create a bad impression. I find KDE4 absolutely promising but it will be very important that more people join Tobias and use the new platform to let it mature more quickly."
    author: "Andre"
  - subject: "Feature Complete vs Stable"
    date: 2008-03-15
    body: "Is KDE 4 crashing all the time?\n\nStability is different from matching (and surpassing) all the features of KDE 3."
    author: "T. J. Brumfield"
  - subject: "Re: Feature Complete vs Stable"
    date: 2008-03-17
    body: "There are many places where the UI is not polished (visual artefacts, even on main components, poor performance at some commands). These \"bugs\" do not agree with \"instability\" - nothing is crashing. But still they prevent many early adopters (like me) from using 4.0. There are probably different definitions of something being \"unstable\" in terms of developers' and average users' view. I do not want to go into detail. I just want to clarify this fact. Though I want to point out that exactly the kind of reply made by you is what makes people upset. \nYes, you are right: KDE is not crashing all the time (if at all). And yes: Not all features from KDE3 are there. But this is not the point. There are many other things in between which are lacking. By replying in such a way you just ignore the feelings of many users (as it was posted many times on the dot). If you would have said: Yes, there are issues we are aware of, but at least the DE is not crashing, but has only polishing and feature issues which will be addressed in 4.1, it would have been fine probably."
    author: "Sebastian"
  - subject: "Re: Feature Complete vs Stable"
    date: 2008-03-18
    body: "> Stability is different from matching (and surpassing) all the features of KDE 3.\n\nThis stance isn't wrong, but right either. There is a certain set of base functionality a user can expect to be in a comparable state, when switching from one stable called major release to another and in case of KDE this includes kdepim. Calling KDE 4.0 stable is just annoying for end users, even excluding lack of polishing here and there or questions of stability and other bugs."
    author: "Carlo"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "We know ;-)"
    author: "jospoortvliet"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "Stability and completeness are very personal things. For people used to something minimalist such as fluxbox, the KDE desktop has been usable for quite some time. For those that are used to KDE 3.5 and which expect all the features to be as mature as 3.5, they'll have to wait for some time. I'd guess that 4.1 will be bearable even for those.\n\nThis is by the way what we have been communicating from the beginning, long time before even the first Alphas hit the streets. \n\nNow we know that people do an awesome job at ignoring that, especially when they can blow steam off by complaining about superficial things, but it still leaves people that are actually solving those remaining issues with a bad taste, and less motivation to fix things.\n\nIt would be very nice if more people helped us to get the message out: We never promised full stability with 4.0, we stated clearly that 4.0 is mainly for developers and bleeding edge users that can live with glitches. KDE 4.0 is a stable platform, it has excellent apps, but it is not yet mainstream quality.\n\nThat said, those that are following the development more closely see that we're going at full steam at the quality level KDE 3.5 provided, and that we have surpassed it in some areas already. But not in all.\n\nBeing an Open Source project is nice, but it also puts some burden on evaluation skills of people who pretend to know things about it. They actually need to listen once in a while. The comments under the 4.0.2 announcement shows that there's a lack of those people, which is a pity since it does harm to motivation of developers and in some way is a self-fulfilling prophecy. \n\nMaybe we should just keep quiet, not even release bugfix updates until everything is perfect (that's never!), that saves us the useless and demotivated bitching.\n\nI wonder when will people understand it."
    author: "Sebastian Kuegler"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "Quite a few of us do.  I'd hazard the majority who are just using KDE as an underpinning for their actual work (as opposed to just playing with desktop software) understand it.\n\nAlas, the patient ones who appreciate the work are quieter than complainers.  Probably part of being patient and busy actually working, doing whatever it is they do, assisted by KDE."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "/this is way off topic /\n\"...complaining about superficial things\" \n\nLike the wasted space in menus/windows, usability frell-ups like \"now you can do it in 3 clicks, before you needed one\" and so on? \n\nOr that KDE4 is in danger of starting to look like a half-breed between the _worst_ parts of OS X GUI and crappy Vista GUI?\n\nSorry for harsh words but I like KDE and I use it every day for fun and for work (and no dual boot bull shi* here!).\n"
    author: "Bling-Bl\u00e4ng"
  - subject: "Re: KDE 4"
    date: 2008-03-16
    body: "i thought this article was about akonadi? =)\n\n\"Like the wasted space in menus/windows,\"\n\nother widget styles may give you the compactness you desire. though you may be operating under an incorrect assumption here that compact == better for people other than yourself.\n\n(and yes, before others start chiming in with \"i like it more compact\", please keep in mind that getting all N% of a group to speak up doesn't increase the number N =)\n\n\"usability frell-ups like \"now you can do it in 3 clicks, before you needed one\" and so on?\"\n\nlet's assume that whatever you're thinking about is a mistake. with a fell swoop you've dismissed all the other wins that were made in the process. expecting every change in a revision to be perfect is unrealistic, ignoring other improvements due to what you think are mistakes is just daft.\n\nyou may also be wrong in your assessment of the \"usability frell-up\". that does happen.\n\n\"Or that KDE4 is in danger of starting to look like a half-breed between the _worst_ parts of OS X GUI and crappy Vista GUI?\"\n\nthat, chicken little, is not the sky that is falling.\n\n\"Sorry for harsh words but I like KDE and I use it every day for fun and for work (and no dual boot bull shi* here!).\"\n\ni'm glad you're passionate about it."
    author: "Aaron Seigo"
  - subject: "Re: KDE 4"
    date: 2008-03-16
    body: "Well handled. Good to see kind and patient developers."
    author: "Impressed"
  - subject: "Re: KDE 4 by Sebastian Kuegler"
    date: 2008-03-15
    body: "The complaint of the general public can never be accounted for when developing anything, be it open source or not. Personal satisfaction of achieving a goal in the development of a piece of software is what motivates most programmers to do their very best when dealing with any programming language, and beta or alpha testing done by a community of knowledgeable people is always a requirement. As you have stated, there is no such thing as bug free code. Most people in the world that use computers will never understand that. (incidentally, the bugs that are the hardest to find in any program, are those that are generated by use of a program outside of it's design , i.e. someone doing something with the program that the programmer didn't anticipate.)\n\nIn general, the public should realize that anything offered that is in constant development is always a rule that it has problems.\n\nHat's off to the development team of KDE, and let us all pray that the developers of KDE manage to come up with a nice thick steel door in their minds and hearts when they come across deterrent and ignorant comments about their software.\n\nanswer to your question of when will people understand it...... the day that the human race becomes one devoid of idiots.(one can hope can't he?) "
    author: "Johnathon"
  - subject: "Re: KDE 4"
    date: 2008-03-16
    body: "I am puzzled by the statement that the limitations of the early KDE4 releases were communicated clearly.  I was struck that the announcement http://kde.org/announcements/4.0/ did <em>not</em> communicate such limitations, when it was clear to me from discussions here that 4.0 was not ready for prime time.\n\nI certainly agree that communicating the limitations is important, but it seems to me it could be done more clearly.  Although there is a natural tendency to want to accentuate the best features, one of the charms of free software is that there are fewer financial pressures (and fewer marketers) leading to that bias.\n\nMaybe a statement that KDE 4 is currently suitable for early adopters would convey the necessary flavor.  It would also be useful to set expectations for users of KDE3; aside from general reliability/speed issues and the loss of kprinter, I'm not sure if there's any loss of functionality.  I think you can run KDE 3 apps if necessary.  I have not looked in the release notes; my point here is that the general publicity should include some hint about such matters.\n"
    author: "Ross Boylan"
  - subject: "Re: KDE 4"
    date: 2008-03-16
    body: "> it could be done more clearly\n\nyep, you're absolutely right that it got missed in the 4.0 press release. =/ it was all over the community print around 4.0, though.\n\n> general publicity should include some hint about such matters.\n\nagreed. interestingly, it's not the general public that is giving us grief.\n\n> and the loss of kprinter,\n\nthings are much better in kde 4.1 w/regard to printing.."
    author: "Aaron Seigo"
  - subject: "Re: KDE 4"
    date: 2008-03-16
    body: ">Maybe a statement that KDE 4 is currently suitable for early adopters would convey the necessary flavor.\n\nThis is kind of redundant, since KDE 4 still are only avaliable in forms used by early adopters. As source or in distributions experimental or extra repositories, all very unlikely to be used by non early adopeters. Until distributions starts to carry KDE 4 in their default installations, it stays in the realm of early adopters.\n\nIf I'm not mistaken Fedora is going to be the first, and that's a distribution catering to early adopters by trying to stay on the bleeding edge of new software."
    author: "Morty"
  - subject: "Re: KDE 4"
    date: 2008-03-16
    body: "I understand your disappointment with the people reactions. In fact, I've the feeling that the most part of them think KDE 4.0 as a commercial release and not as the real technical design improvement inside the code. But end-users don't read mailing-list...\n \nMoreover the confusion is coming from the major distributions that already propose KDE 4.0 by default. However I consider that's a good thing for the long term even if some people seems unhappy at the moment. \n\nI just want to tell to all the kde developpers to keep their motivation because the facts wont make so long to talk. Thank you very much for this great piece of work.\n\nP.S. I'm not involded in the KDE development and I have not enough time to test and reports things. That's why I wait for a future 4.1 ou 4.2 for my daily use.\n\nP.S.2 Sorry for my bad english"
    author: "Julien J."
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "I am running here a self-compiled KDE 3.5.9 without a problem.\n\nStill up today, I was unable to compile 4.x\n\nI had i810 problems... and noone was able to help me so far.\n\nI am sorry to say, but even though 4.x will definitely kick ass, \nright now it simply is not there.\n\nThe move to cmake wasnt the best, simply because it made \nme more problems and googling for the error message also \nwasnt helpful ... Cant there be a special report page\nto the KDE project, where users who fail to compile something\ncan send their stuff in?\n\n"
    author: "she"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "Only the move to CMake made several things possible. It is utter nonsense to say it is wrong just because you have one issue with it. Why don't you post a link to pastebin.com with your output?"
    author: "FooBar"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "\"Why don't you post a link to pastebin.com with your output?\"\n\nBecause someone might solve the issue, and what would they complain about, then? ;)"
    author: "Anon"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "What is your exact CMake error message?\n\nFrom what I can say the move to CMake was overdue. The autotools are so anachronistic while CMake is quite easy to use for me as programmer. For the user, it is not harder than autoconf.\n\nAs we're talking about build errors, I have some with the kdesvn-build script. Everything except for kdesupport, kdepimlibs, and extragear/libs. From the logs for kdelibs:\n\nLinking CXX executable testkateregression\n/home/kde4/kdesvn/build/qt-copy/lib/libQtSql.so.4: undefined reference to `QFactoryLoader::QFactoryLoader(char const*, QStringList const&, QString const&, Qt::CaseSensitivity)'\n\nAnd for kdebase:\n\nLinking CXX executable khc_indexbuilder\n/home/kde4/kdesvn/build/qt-copy/lib/libQtSql.so.4: undefined reference to `QFactoryLoader::QFactoryLoader(char const*, QStringList const&, QString const&, Qt::CaseSensitivity)'\n\nI'm using Qt 4.4.0-snapshot-20080310, installed from openSUSE's KDE:KDE4:Unstable:Desktop repository. Is this regression known?"
    author: "Stefan Majewsky"
  - subject: "Re: KDE 4"
    date: 2008-03-15
    body: "What are i810 problems ?\n\nAnd yes, there are at least two places where you can post build problems: the mailing list kde-buildsystem@kde.org and the bug tracker, there is a component \"buildsystem\" -> \"cmake (KDE4).\n\nNow while we're at it, just post it here.\n\nAlex\n"
    author: "Alex"
  - subject: "MS Exchange"
    date: 2008-03-15
    body: "The most important remark made by Tobias for me is an interface for MS Exchange. IMHO this is *THE* most important feature for KDEPIM to be competitive with other PIM solutions. \n\nI work at a company where slowly but steadily a move is made from the Windows desktop to a KDE desktop (openSUSE). Unfortunately we use Evolution as our mail client just for the simple fact that Evolution supports Exchange rather good.\n\nNow this isn't the only issue for corporate PIM'ming: a simple test with KMail also showed me that the whole dealing of KMail with HTML is rather... technical. Yes I know, 'HTML mail is bad', but for a non-technical user KMail is way to complex. \n\nIt would be great if KMail would support some kind of profiles. Where the default profile is 'average-non-technical-user-used-to-outlook'.\n\nJust my 2 euro cents.\n\nBut as every other KDE technology Akonadi impresses me a lot!"
    author: "Spockfish"
  - subject: "thanks"
    date: 2008-03-17
    body: "interesting read, thanks\n\nbtw, anyone hear the news that novell has released mono?\n\nThings just keep getting better\n\nhttp://blogs.zdnet.com/open-source/?p=2128\n\n"
    author: "R.J."
---
<a href="http://www.kubuntu-de.org/">kubuntu-de.org</a> has <a href="http://www.kubuntu-de.org/english/interview-tobias-k-nig-about-development-akonadi">interviewed Tobias König</a> about his work on <a href="http://pim.kde.org/akonadi/">Akonadi</a>, the personal information storage server.  Tobias shares his impressions about the integration into the KDE desktop environment and Akonadi's development and features.  He also talks a bit about himself.  Available in the original <a href="http://www.kubuntu-de.org/nachrichten/sonstiges/interview-tobias-k-nig-ber-die-entwicklung-von-akonadi">German</a> and <a href="http://www.kde-it.org/news.php?extend.183">Italian</a>.

<!--break-->
