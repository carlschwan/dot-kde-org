---
title: "Mobile and Embedded Day at Akademy"
date:    2008-08-15
authors:
  - "jriddell"
slug:    mobile-and-embedded-day-akademy
comments:
  - subject: "Why Wt?"
    date: 2008-08-15
    body: "Wt sounds strange. Why not just use Qt/Embedded? I would think that would have a smaller footprint than httpd+browser+wt, especially since Qt/Embedded can be stripped down very small. Will this talk be available for non-attendees?"
    author: "David Johnson"
  - subject: "Re: Why Wt?"
    date: 2008-08-15
    body: "all talks will be available in coming days."
    author: "Lisz"
  - subject: "Re: Why Wt?"
    date: 2008-08-15
    body: "Qt/Embedded is not an option. Wt is for devices _without_ display.\nSo it's httpd+Wt on the device. The browser is on a different device."
    author: "michael"
  - subject: "Re: Why Wt?"
    date: 2008-08-15
    body: "In fact you do not need httpd at all. With Wt, you have two options: embed a tiny HTTP(S) server in the application, or build the webapp as a FastCGI module and use it from any webserver with FastCGI support (most of them, including IIS).\n\nWt is not only for embedded devices, by the way. Its performance is really good, therefore you may want to use it for high load websites. And given that you have access to the zillions of C/C++ libraries out there, without worrying about bindings, I'd say it is a very good option if you need, for instance, Active Directory authentication (hint: just use libwbclient from Samba in your application)."
    author: "Pau Garcia i Quiles"
  - subject: "Re: Why Wt?"
    date: 2008-08-16
    body: "I see. I'm assuming the browser need a Wt plugin?\n\np.s. Qt/Embedded does VNC, and I have used it on headless devices. A lot of bandwidth, to be sure, but it works quite well."
    author: "David Johnson"
  - subject: "Re: Why Wt?"
    date: 2008-08-16
    body: "No plugin needed, it outputs HTML.\n"
    author: "Jonathan Riddell"
  - subject: "Riding two horses"
    date: 2008-08-15
    body: "One thing I don't get is how Nokia plans to support two IDE's - GTK and Qt.  The message on the Open Moko device is already confusing. They paid 100 million for Trolltech why not drink their own koolaid and say Qt is our platform period.\n\nIf your going to have two you might as well have three, why not include Java, or have they already.  "
    author: "David Boosalis"
  - subject: "Re: Riding two horses"
    date: 2008-08-15
    body: "Why throw away all the existing GTK apps or turn off the existing GTK developers working on the platform?"
    author: "T. J. Brumfield"
  - subject: "Wt pronunciacion"
    date: 2008-08-15
    body: "Does anyone know how to pronounce Wt? is it wit?"
    author: "yman"
  - subject: "Re: Wt pronunciacion"
    date: 2008-08-16
    body: "It's pronounced \"witty\". They seem to be copying Qt (pronounced \"cute\")."
    author: "Michael \"witty\" Howell"
  - subject: "Re: Wt pronunciacion"
    date: 2008-08-16
    body: "I think it should be woot :-)"
    author: "Simon"
  - subject: "License and other manufacturers"
    date: 2008-08-16
    body: "Maybe the license topic was discussed very often. But I think if Qtopia/Qt will be GPL/QPL in the near future not many other mobile companies won't use it for their devices. It will be interesting to see if Nokia will release it for example under LGPL. That would be great because in my point of view other companies would contribute to it and it would get more widely used.\n\nNokia is also member of LiMo foundation which uses Gtk+... So lets see what future brings."
    author: "FelixR"
  - subject: "Re: License and other manufacturers"
    date: 2008-08-16
    body: "They are dual licensed GPL and commercial. Probably phone companies will be able to afford the cost of the commercial Qtopia (a Qtopia+Linux based firmware is still cheaper than developing a custom firmware) and it gives funding for the development of Qt/Qtopia."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: License and other manufacturers"
    date: 2008-08-17
    body: "Hi FelixR, \n\nNokia will continue the dual business modell for Qt and Qtopia. Many commercial customers are not confortable with GPL, LGPL or any other free software licence for that matther. Supporting new and existing customers, and the open source activeties, was two of several main goals when Nokia acquired Trolltech.\n\nIt's mostly commercial customers which pays for further development of both  proprietary and free versions of Qt and Qtopia. It's the same products with multiple licenses (with some tiny differences where DRM is omitted from the free software version). Discontinuing the proprietary version of Qt and Qtopia could force customer to choose other platforms. It's unwise to exclude customers which pays for further development of Qt and Qtopia. Commercially it will also hurt Nokia's reputation not supporting new and existing customers. \n\nBest regards\n\nKnut Yrvin\nCommunity Manager for Qt and Qtopia"
    author: "Knut Yrvin"
  - subject: "Re: License and other manufacturers"
    date: 2008-08-18
    body: "I know it is asked in several forums, but nowhere there is an official statement:\n\nWhat is the status and future of the Qt Green Phone edition?\n(not the mobile, the software edition)"
    author: "Philipp"
---
This year Akademy held a dedicated day for mobile and embedded talks.  With Trolltech being owned by Nokia, mobile is suddenly a hot topic for KDE and several variants of Qt and KDE on mobiles were in progress at Akademy.  Read on for an overview of the talks.














<!--break-->
<div style="border: thin solid grey; margin: 10px; float: right; padding: 10px">
<a href="http://static.kdenews.org/jr/akademy-2008-mobile-opencitymap.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-mobile-opencitymap-wee.jpg" width="400" height="267" /></a><br />
Open Citymap on Qtopia
</div>

<p>The day opened with Kate Alhola from Nokia showing off the Nokia N810.  As already reported, <a href="http://dot.kde.org/1218580876">the talk was followed by handing out the devices</a> to the audience.  <a href="http://blogs.forum.nokia.com/blog/kate-alholas-forum-nokia-blog">Her blog says</a> she hopes to see lots of Qt and KDE applications in <a href="https://garage.maemo.org/">Maemo Garage</a> soon.</p>

<p>Koen Deforche from EmWeb showed off Wt, a Qt like toolkit for webpages.  It is intended for use in embedded devices without their own display but which can run a small web server to provide their user interface.  Wt itself is a C++ library which closely follows the Qt API and compiles into a programme featuring a built in web server.  Wt is GPL and uses a similar dual licence to Qt for proprietary uses.  If you like Qt but you need to write an embeded UI with web technologies, Wt seems like a perfect fit.</p>

<div style="border: thin solid grey; margin: 10px; float: left; padding: 10px">
<a href="http://static.kdenews.org/jr/akademy-2008-openmoko.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-openmoko-wee.jpg" width="400" height="267" /></a><br />
Taking apart an OpenMoko phone.
</div>

<p>Ole Tange said he had a dream.  He wanted a mobile device which was open to hacking, used free software and worked as a phone and PDA.  He found his dream with the OpenMoko, a mobile phone made by FIC, a Taiwanese company who were fed up of making hardware which was then rebranded by US companies.  OpenMoko has recently changed to using Qtopia, the mobile platform from Trolltech.  As a free device we are able to work on ideas that do not interest companies making closed platforms, one suggestion was to programme the phone so when a salesperson calls you can have it say "press 1 if you are selling something".  Another suggestion was to use the motion sensor to test if you are cycling, in which case you do not want a call.  One more was to turn off the phone when in a cinema but turn it back on automatically after the film.  After the talk he took out his screwdriver and showed how to take apart the device.</p>

<p>Bling was on show next with Leonardo Sobral Cunha and Artur Duque de Souza from OpenBOSSA showing off QEdje, a port of the Enlightenment Edje library to Qt.  They make applications for Nokia N810 and other devices and showed off the slick user interfaces they have made.  </p>

<p>Making KDE technology available to embedded devices was the theme of Eva Brucherseifer's talk.  She used Decibel as an example of a KDE library that would be interesting on mobile devices.  She hopes there will be more code shared between KDE and mobile platforms in future.</p>

<p>Student projects were the topic of the next two talks.  Firstly Mickey Leroy showed off his university project which he did at the De Nayer Institute, home of Akademy (Akademy organiser Bart Cerneels from the EmSys research group was the mentor for the OpenCityMap project).  <a href="http://code.google.com/p/opencitymap/">Open Citymap</a> is an application to show and edit Open Street Map from within a Qtopia device.  It made maximum use of the small screen space available on a mobile device to create a usable interface.  It also uses QtScript to allow plugins with custom functionality.</p>

<p>Knut Yrvin from Trolltech spoke about his
experience of mentoring student projects in Norwegian University. Many
students tended towards choosing simple projects over ambitious ones
which would have results in the real world. It also takes up his time
of course and as a frequent traveller he is not always able to respond
quickly. Given the good results from OpenCityMap though we can hope he
and Bart will continue mentoring student projects. It was also pointed
out the good results we have been getting from Google Summer of Code.

<h2>Panel Discussion</h2>

<div style="border: thin solid grey; margin: 10px; float: right; padding: 10px">
<a href="http://static.kdenews.org/jr/akademy-2008-mobile-panel.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-mobile-panel-wee.jpg" width="400" height="267" /></a><br />
The Panel
</div>

<p>The day closed with a panel discussion on the topic of Driving innovation with Open Desktop Technologies.  Shane Martin Coughlin a lawyer with FSFE opened by saying he wants to make more technology available to more people and that needs free software and genuinely open standards.  Aaron Seigo said free software opens doors that can span between different companies and their devices and we will see lots of crossover between different devices.  Knut from Nokia said free software lets you get on without just debugging and fixing existing software.</p>

<p>It was asked what is it about free software that allows crossover.  Aaron emphasised the social aspect.  Innovation does not happen in a vacuum, with free software people can experiment more and work together.  He is a desktop guy but today he is on a mobile panel, that sort of crossover does not happen outside of free software.  Shane said free software gives a grant that lets people take changes and do what they want with it.  The biggest challenge going with open desktop is it is more complex than any one person or company can go with.  There will be technical and cultural problems crossing borders, problem is how we move forward with global developer and user base where there are different cultures.  We will have to innovate with licencing and platforms.</p>

<p>Knut pointed out that in Africa people have mobile phones as their first computer.  Only a small fraction of the world use PCs, increasingly the rest use phones first.  He told us that the Trolltech CEO said the challenge to the mobile industry is it needs to change its business model from proprietary software since software is becoming commoditised, it can be installed at no cost and that will change business models in the same way as happened with the internet.  The next step is letting applications flood into devices.</p>

<p>Aaron said that the majority of software is written on high powered computers.  Not many people rushed to write apps for XO, while plenty did for the iPhone because it is a more interesting device.  You can not try and bring the people writing free software now to do mobile stuff if it's not interesting and sexy for them.  Instead we need to make user interfaces span from one side to the other.</p>

<p>Sven, a programmer from Nokia said they introduced standard libraries as a lowest common denominator.  It will take time for people to learn how to do everything on small and limited devices, but we will get there.  If you are able to do it you will then get much more performance from desktop devices as a result.  Cross platform is important too, people using second hand machines do not have a choice about platform so it is important to be able to work on as many devices as possible.</p>

<p>It was asked if the desktop is still relevant.  Aaron said it is massively important, we are deploying 50 million desktops in Brazil, MS and Apple have plenty more of course.  It used to be desktop was the only game and embedded was maybe done by some people over there.  Now desktop is on a continuum which includes embedded. As desktop developers we can question ourselves in how we build our user interfaces so they are more componentised and work on a range of devices.  He challenges everyone in the audience to write software which is more aware of its environment.</p>





