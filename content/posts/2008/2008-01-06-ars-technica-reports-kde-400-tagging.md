---
title: "Ars Technica Reports on KDE 4.0.0 Tagging"
date:    2008-01-06
authors:
  - "rjohnson"
slug:    ars-technica-reports-kde-400-tagging
comments:
  - subject: "Ars Rocks :D"
    date: 2008-01-06
    body: "I feel Ars (and Ryan Paul) is doing a great job writing about KDE, in an objective and decent manner, and I must say I really look forward to meeting (some of) them at the Release Event..."
    author: "jospoortvliet"
  - subject: "Re: Ars Rocks :D"
    date: 2008-01-06
    body: "Of course, I should add to that they are also doing a great job at spreading tech info in general - I always enjoy their articles... Many of them are a must-read for tech enthusiasts."
    author: "jospoortvliet"
  - subject: "KDE4"
    date: 2008-01-06
    body: "Well. Uhm. I'm still a bit confused about what to think now of what is soon to be released as KDE 4.0. Are they really serious about those black boxes everywhere? The taskbar for instance looks really ugly in my opinion. Will there be no way to change its color back to sth. more acceptable like a dark blue? Likewise what about the other applets on the desktop like the clock. Will they all be in black? Ugh. In this case I'd rather stick with KDE 3.5 for the time being. Don't take this wrong please. I know there's lots of new technology underneath the surface and this is just the beginning, but from an end-users' point-of-view KDE 4.0 means right now: Less features, less customizability. 3-D functions, effects etc also work relatively sluggish right now. For instance I would expect that I can seamlessly resize and rotate the anolog clock applet without any jumps. Not at all even though I own a fairly decent PC. Perhaps this all works out but right now I wouldnt recommend KDE 4.0 to any end-user."
    author: "Michael"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "The desktop is themable, but you'll need also more than one theme to be able to change it.\n\nAnd sure, Plasma at this point is less customisable than KDE 3.5's kdesktop. It's completely new and will gain more features over time.\n\nAs to the speed of resizing applets, that's not a big deal right now, as it's fast enough -- it doesn't need to be completely smooth to make the desktop workable, it just has to work. You don't resize applets all the time."
    author: "sebas"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "I'm excited that Plasma will make it easier in the future to customize the desktop even further.  However, because it less customizable today, I'm sticking with 3.5 today, and I imagine many will.  That isn't a knock on the development of KDE 4, just a reflection of the current state."
    author: "T. J. Brumfield"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "yeah, i fully expect some people will do this. you can, of course, still run other kde4 apps in your 3.5 so you don't have to give up everything. kde is more than a desktop environment and things are welded together, after all =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "this is probably a trivial question, but :\ni expect running kde4 apps on a kde3 desktop to increase memory usage, right (for kde4 libs not present before) ?\n\nif so, what would be an approximate estimate, how much additional ram would require first launched kde4 application on a kde3 desktop compared to kde4 desktop ?\n\n(note that i explicitly noted first app ;) )"
    author: "Richlv"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "well, I can't give a precise approximation, but it won't be very much. I'd say 20 mb, 30 mb... something like that. Maybe even less."
    author: "jos poortvliet"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "I'm using kde4 applications with kde 3.5 on a 1.5 ghz 256 mb ram laptop and don't notice any slow down of my computer experience because of that.\n"
    author: "whatever noticed"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "If I understood it correctly then the present taskbar won't be as it is now in RC2 on release. Aaron at one point said that it's just provisional and that much time wasn't invested in the present taskbar. He said that that would change by release.\nIf you are expecting the functionality of KDE 3.5x then I would advise you to have it install alongside 4.0. I personally haven't decided to make a total shift until all my favourite apps have been ported to 4.0 and working at least as good as they are on KDE 3.5x but I will certainly be using KDE 4.0 on a daily basis."
    author: "Bobby"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "Well, for starters this is will be the first major KDE release that I have not compiled from source. Suse has spoilt me (-: I love all that black. I wish I could have more. The only two things I miss now are a weather applet/plasmoid and the applications' real name on the KDE 3 style menu. I hate the generic naming of apps. I spent a lot of time learning what the different applications were to now have to switch to generic names like 'document viewer' and 'image viewer'. Call them by their given names - Okular! Gwenview! Be proud! You deserve it. Suffice to I now use KDE4 exclusively on Suse. It is just a matter of time and space till I uninstall KDE3. Thanks KDE devs! /rant"
    author: "ne..."
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "Why should a beginner know what Okular or Gwenview is if there is no description in the menu?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "One word: explore!"
    author: "ne..."
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "Why would a beginner open those apps?\n\nSurely it would just, for example, click on a PDF file in their file manager and, thanks to sensible default actions, Okular would magically appear?"
    author: "Ben Morris"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "it :-)"
    author: "Martin"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "beginners always open apps to see what they are and learn.  Seems to me the new approach is to dumb the system down."
    author: "Richard"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "Just imagine a beginner looking for Firefox. Why would anyone click on something like \"Webbrowser\" when they're looking for Firefox? ;) After all, who would expect just generic names? Nobody who has not used KDE before.\n\nEven beginners expect to find certain programs and look for them by name."
    author: "emu"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "IMO default should be description and app name. For example: Firefox (Webbrowser) or Amarok (Music Player). Of course option should exist to change it to show only app name."
    author: "Syzar"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "I second that"
    author: "Richard Van Den Boom"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "I second that too!"
    author: "rorgarf"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "I can't think of a better way to introduce new users, which is one of the most important things KDE has to do to grow further."
    author: "Glen Anthony Kirkup"
  - subject: "Re: KDE4"
    date: 2008-01-09
    body: "http://bugs.kde.org/show_bug.cgi?id=155362\n\nEven more feedback is welcome :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "\"description and app name\"\n\nGoes without saying, why should they do it any other way?\nUsability? I spit on \"usability\"!"
    author: "reihal"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "\"usability\" or \"decide how others should work\" was clearly not the reason for the decision to go with the generic aka describing name. It was more the question what may work well and if users will accept it. There where multiple points I/we did look at;\n1) What needs to be really configurable and what doesn't. Goal here is/was to don't provide a checkbox for every single combination of solutions by picking a default and look if that's enough. Related to the name vs. description vs. description (name) vs. name (description) case; seems there is just no easy solution for this :-/\n2) The traditional KDE3 style menu does even contain already a hack to e.g. display \"OpenOffice.org Presentation\" (the name) rather then \"Presentation\" (the description). One of the problems here was, that the generic name is used different between e.g. KDE and Gnome. While we define only the application name, so e.g. \"kspread\" as name and \"Spreadsheet\" as description, others prefer do use the generic name as appname+description field, e.g. \"Gnumeric Spreadsheet\" as generic name and \"Spreadsheet\" as description. That makes it rather difficult to just choose e.g. \"description (name)\" without looking somehow broken :-/\n\nSo, all in all: something that can be made better with an idea how to make it better :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: KDE4"
    date: 2008-01-09
    body: "Eh, please, this is KDE not gnome.\nYou are obviously entangled in some heated discussion behind the scenes \nwhich I don't want to know about.\nDescription;name or name;description is the only variations that makes sense."
    author: "reihal"
  - subject: "Re: KDE4"
    date: 2008-01-09
    body: "> You are obviously entangled in some heated discussion behind the scenes which I don't want to know about.\n\nOh, you should know about them cause one of the goals of an open project is also to be transparent on such things what I tried to achieve by explaining the way that was leading to the result.\nBut beside transparency participation is another great thing an open project is able to provide. E.g. with your reply you already did participate since that's feedback and without such kind of feedback it would be rather difficult to know what ppl expect and how things should be solved.\n\n> Description;name or name;description is the only variations that makes sense.\n\nand that's now a decision you made behind the scenes cause there may ppl who may not agree there. Guess that's the \"problem\" about beeing human with an own idea how things should be. What helps most here is btw earlier participation and talks.\nIn the menu-case such kind of feedback was just missing and that's why it is how it is now. But since we saw within that thread already very good feedback, it's now easier to take them into account.\n"
    author: "Sebastian Sauer"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "KDE 4 Kickoff does list app names also."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "There is, in fact, a weather plasmoid.  The data engine is installed by default, and the widget part is at http://websvn.kde.org/trunk/playground/base/plasma/applets/weather/ (you may need to run cmake in the playground/base/plasma directory for the build to work)."
    author: "randomguy3"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "Thanks, I was hoping Suse would spoil me rotten and have a rpm for me. I might head back to compiling soon tho."
    author: "ne..."
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "SUSE has a playground-base.rpm actually."
    author: "Anonymous"
  - subject: "Re: KDE4"
    date: 2008-01-06
    body: "There is a playground package and it includes the weather applet."
    author: "Sutoka"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "Yeah, I couldn't get the weather plasmoid in time for extragear. It's in the playground for the time being."
    author: "Shawn Starr"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "> I love all that black. I wish I could have more.\nhint: POWER button. Hit it."
    author: "eMPee584"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "the jumps are due to the compositing. this happens with compiz, too. it bugs the HECK out of me as well. i'm not sure what the solution will end up being, but apparently composition managers do not deal well with canvasy apps like plasma.\n\nwhen i run a non-compositing window manager things are silky smooth.\n\nblarg.\n\nyou have no idea (or.. maybe you do =) how frustrating it is after compiz and what not having been out there for the last few years how much composite, argb, etc support in x.org and its drivers just flat out *sucks*.\n\nit's The Way for the future (non-composited displays just won't be around in years to come, i'm sure of it; there are too many advantages) so we shouldn't deviate .... but i wish x.org was a bit more ... robust in the area.\n\nnvidia's new driver they released at the end of december works properly with argb, though! hooray for the small wins =) and that's exactly how it's like to go; a lot of my near future will consist of trying to find ways to stack up more and more of these little wins by exposing the weaknesses and working with those responsible for those pieces of software to address the issues. not exactly my idea of \"fun\", but .. yeah. needs to get done."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE4"
    date: 2008-01-07
    body: "Just as KDE apps were needed to help insure that the KDElibs are really \"ready,\" so too I guess KDE is going to help make x.org \"ready.\" While it surely will be a challenging/annoying road, we'll be grateful in the end!\n"
    author: "T"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "\"it's The Way for the future (non-composited displays just won't be around in years to come, i'm sure of it; there are too many advantages) so we shouldn't deviate\"\n\nBut doesn't composting slow things down and take up a lot of memory? To me those are huge disadvantages. I sure hope it will be possible to turn it off or better yet put it in a separate package that can be uninstalled if we don't want it; that way there wont be code sitting in memory that isn't used for anything.  I'm sure there are a lot of people out there that don't have the latest computers (like my wife) and won't use KDE4 if it makes their computer run much slower."
    author: "Jeff Strehlow"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "Of course you can turn it off."
    author: "Paul Eggleton"
  - subject: "Re: KDE4"
    date: 2008-01-08
    body: "well, compositing does those things now, but theoretically, it should actually speed up things. \"Just\" needs a proper X.org and driver architecture, which we simply don't have right now..."
    author: "jos poortvliet"
  - subject: "Re: KDE4"
    date: 2008-01-09
    body: "And we never will, unless KDE and/or Trolltech takes over it.\nThe division of Linux, X and KDE is the way that MS can \"conquer by division\"."
    author: "reihal"
  - subject: "Reduction of features"
    date: 2008-01-06
    body: "Now that KDE 4.0 is here I can ask why you are removing features form it? I know that plasma (is there an option to remove desktop toolbox from the top right corner...) and kwin effects (...and put present windows trigger there?) are not complete and ready yet, but what about other stuff which is present in KDE3?\n\nFor example, in file dialog there are no more owner, group and permissions columns or \"folder first\", \"separate folders\" and \"case sensitive\" options, WHY?\nApps are not exceptions. Look at gwenview, it's all nice, but it has only 30% features of the older version which I use currently.\n...\n\nRegards\n\n\n"
    author: "Vedran Fura&#269;"
  - subject: "Re: Reduction of features"
    date: 2008-01-06
    body: "Because KDE4 and KDE3 are different, not progressions of the same software.  To run a KDE3 app, you need the KDE3 libraries and they run fine.  That applies if you're running them under KDE3, Gnome or KDE4.  Or anything else other than KDE3.  If you like KDE3, stay with it: as a distinct environment with many users, it isn't going anywhere.  That said, quite a bit of developers will move on to KDE4.\n\nThis is open source, and software packages are maintained by their level of use.  Old Linux kernels are actively patched by their developer and userbase.  Yes, the majority of developers and users tend to run the lastest thing, but KDE3 is not going to go away.  If you prefer it, stick with it... many other people -- both users and maintenance developers -- will as well.  It's okay.   This is not a commercial venture where last year's model suddenly becomes unavailable.   You (and some others) labor under a misconception that because KDE4 exists, KDE3 is going to go away.\n\nYou aren't the only one: aseigo even addressed the same thing in his blog recently ( http://aseigo.blogspot.com/2008/01/talking-bluntly.html ).  Open source just works differently than the cycle you seem to expect and that exists in the commercial world."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Reduction of features"
    date: 2008-01-06
    body: "Remember that you are using a release candidate and not a final version. Wait until KDE 4.0 is launched then you can complain. Concerning plasma, it's said many times that plasma is still under heavy development and that KDE 4.0 is only the first of a series. Still I am sure that more polisching will be done before the release."
    author: "Bobby"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "> Wait until KDE 4.0 is launched then you can complain.\n\nwell, isn't that a bit late if he wants to change something in KDE 4.0? :-)"
    author: "kavol"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "Since it has been tagged, it is precisely too late.\n\nThere will be zero changes to KDE 4.0 at this point, though I'm sure there will be plenty of changes to KDE 4.x"
    author: "T. J. Brumfield"
  - subject: "Re: Reduction of features"
    date: 2008-01-08
    body: "Haha I love that.  Someone who complains about something in a beta is often told to wait for the final release.  If he does so though, it's too late to do anything about it.  Brilliant!"
    author: "MamiyaOtaru"
  - subject: "Re: Reduction of features"
    date: 2008-01-06
    body: ">I can ask why you are removing features form it? \n>Apps are not exceptions. Look at gwenview\n\nIn most cases the answer is simple, the features are not ported yet. Some features may even requre a revirite, depending on the changes done to the application. Its simply a time ting.\n\nAs for applications, look at Okular. It has more features than its KDE3 predecessor, KPdf. The same is true for several other applications too, so there are lot of exceptions. "
    author: "Morty"
  - subject: "Re: Reduction of features"
    date: 2008-01-08
    body: "Both gwennview and Konsole have been rewritten, that's why features are missing. their authors know and want to add those back, but that'll just take a while."
    author: "jos poortvliet"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "each of the examples you give are all for the same reason: they are new bits of code. the file dialog views are new (based on the m/v stuff in qt4, shared with konq & dolphin to boot); this new view will certainly increase in feature capacity over time.\n\ngwenview was also very much a rewrite. the kde3 ui was tossed out to take a new run at how to present the same kinds of functionality but with a much nicer approach.\n\nthe down side to doing this is that you lose some features with the old code.\n\nyou'll find that apps/components that didn't need to do this usually have *more* features than they did in kde3. okular is a good example of that (it was kpdf; renamed because it's more than pdf's now). then there are the completely new apps, like marble.\n\nso we're not removing features at all. a lot of features have actually been added, and where there are feature regressions due to rewritten or refactored code those will catch up (and likely surpass) their kde3 counterparts with time.\n\nit would have been awesome if we could have magically kept every feature of every app and component, but these were prices we paid for being able to go further in the future.\n\noh, as for talking about kwin effects not being there yet, those are also brand new in kde. see, more features! ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "So. I just did a LOT of reading. I read your posts, your blog, etc etc. It all now becomes finally(!) much clearer to me how to make sense of this all. Thanks for patiently explaining all this to me and all the KDE end-users. Not very common really in open source software to get such detailed answers. \nThe conclusions I've drawn for KDE end-users right now:\n\n1) If you happily use KDE 3.5 right now as an end-user for day-to-day work and wouldnt like to miss out on features and customizability, stay right now with KDE 3.5. It isnt even a \"bad\" thing to do, but expected by the developers and perfectly OK. It's not a commercial product, where everyone expects you to drop the old stuff no matter what and shell out all your money for the new and shiny version.\n\n2) If you are interested in taking a glimpse at the future of the KDE desktop, install KDE 4.0.0 in parallel or use KDE 3.5 with new apps or KDE 4.0 with old apps.\n\n3) KDE 4.0.0 has lots of under-the-hood changes right now that make it much easier for developers to implement new features. So we can expect a gradual shift and each user can decide individually when KDE 4.x.y is good enough to switch. Again, this is not due to KDE 4.0.0 somehow not being ready right now, but the expected \"behavior\" of an Open Source product in contrast to a commercial product.\n"
    author: "Michael"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "exactly. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "Nice. That is actually, in my opinion, a very clear and concise summary of how the end user should look at KDE 4.0.0.\n\nThis is the foundation, usable for some, lacking for others. Migration will happen slowly, as each user or distribution decides that it has reached sufficient maturity. :-)\n\n"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "Put this in the release notes : )\n\nI've been in two minds about this release being 'premature' from first trying the RCs, but this just sold it to me: Releasing is totally the right thing to do.\n\nThanks Michael"
    author: "Martin Fitzpatrick"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "> For example, in file dialog there are no more owner, group and\n> permissions columns or \"folder first\", \"separate folders\" and\n> \"case sensitive\" options, WHY?\n\nI ported the file-dialog from KDE 3 to KDE 4 and the reasons for having this features not available in KDE 4.0 are just because we've been running out of time.  As Aaron explained already especially the file-view related parts in KDE 4 had to be rewritten nearly from scratch due to Interview from Qt4. We'll add such kind of missing features again in the 4.x releases :-)\n"
    author: "Peter Penz"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "This is the point where I get frustrated with KDE's monolithic releases ;)  I'd love to see such stuff come in without necessarily having to wait for 4.1 (and without having to run SVN).  The monolithic thing just seems to slow things down sometimes.  I wasn't happy at all when Kopete went into KDE, as that meant new features would only come out with KDE releases.  \n\nObviously it works well (for all but the illogically impatient perhaps) so I'm not advocating change.  I guess it's my way of saying I'm glad to hear changes are coming and I look forward to them!"
    author: "MamiyaOtaru"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "Everytime I build KDE from source, I'm so glad it has monolithic releases. :-)\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "Set yourself up a build environment where you can use the latest from svn code. Set yourself up a proper and robust backup method for your data.\n\nThen run the latest and greatest. During the kde 3 development cycle I found it very satisfying to experience the maturation as it happened. And I found it surprisingly stable.\n\nSome inside baseball: don't update during the two weeks preceeding a code freeze. It will break. \n\nDerek"
    author: "D Kite"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "This approach usually does not work. If your preferred app resides in a KDE mayor package, you can't cherry-pick it but have to pull the hole package, which maybe only builds against the unstable trunk kdelibs, but not against any stable version. That means that you have only two reasonable options:\n\n(1) Live on the bleeding edge and use the trunk for all your KDE apps.     \n\n(2) Wait until the next release.\n\nA more modular approach (i.e. git repositories for each app) would allow an app to proceed at its own pace and state its own preconditions.             "
    author: "anonymous coward"
  - subject: "Re: Reduction of features"
    date: 2008-01-07
    body: "This approach does usually work, even if you have to get the whole major package you don't have to compile more than the application you want. Much the same way distributions split it in to individual packages for each application. The KDE packages are very modular.\n\nAs stated the only problem you can have are when the application require functionality not present in the current stable kdelibs, but the ability to only pick the application you want makes this less likely. In the past this has even been done officially by application maintainers, for instance Kopete has been released both as part of the kde and as separate releases.\n"
    author: "Morty"
  - subject: "Where is Plasma headed?"
    date: 2008-01-07
    body: "Hi, the work that's gone into the KDE4 series is impressive. I was looking at the Plasma website (which I know is out of date) and I saw a whole thing on \"extenders\" (scroll halfway down http://plasma.kde.org/cms/1069). So I now wonder if extenders are still part of the developers' plans for Plasma (obviously in 4.1 or later) or if they are going to use something else for the same functionality? \n\nAlso, there was a really exciting article a while ago by Troy Unrau about a job progress manager (http://dot.kde.org/1169588301/). And it was also cool to hear from Aaron that Plasma will be better integrated later on for use with Linux MCE. There probably won't be updates on that for a while, but it's definitely something I'll look forward to as KDE4 matures.\n\nSo how do the developers find all their ideas going into implementation phase? Have you had to remove or change some of the features you intend to have for Plasma over the next few years, or is everything going according to a pre-arranged plan? KDE is a really exciting project, and I look forward to hearing more about its future direction soon!"
    author: "Parminder Ramesh"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "everything you note is still on the rails to happen. just not all in 4.0. i wish i was a magician and could make it all happen yesterday, but at least we've been able to put down all the tracks necessary to drive the train to the destination. media center (relies on mce project; they'll be at the release event), extenders (relies in part on WOC in Qt 4.4) and job manager (just didn't have time for it, just as with completing the welcome applet) will come too.\n\ni should have a lot more time for plasmoid devel rather than only plasma framework devel, meaning that i'll be able to help bring the plasma features to the forefront with the other plasma devs this time around =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "Thanks for the reply Aaron. I'm happy to wait patiently, because I know that you and the KDE developers can be relied on to deliver. Even if you're only human, the results so far look pretty promising, so good job!"
    author: "Parminder Ramesh"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "Human? Aaron? no shit!\n\n:-)"
    author: "Fred"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "Wow, Aaron, you did a great job! I really hated the Plasma before, but I just checked out the kde4live, and it rocks hard! Very stable, does exactly what I thought it would (i.e. user-friendly), and, as I heard, it is (along with the rest of the KDE4 technologies) a great foundation. So, you took a lot of heat (even from me...) and you still did a great job. Well done, to you and to everyone else in the KDE team!"
    author: "Phd Student"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "I'm quite impressed about how you've been able to take a mix of different posts, discard the trolling ones, take the constructive critics and comments, and for the nice work. The newest live release was clearly better and I'm sure it'll improve further. It takes guts to withstand so much cr@p from people. Other developers have quit different project because of that. Thanks for holding on, and for the good work.\nJulian"
    author: "Julian"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "That's what I wanted to say too. Thanks."
    author: "Glen Anthony Kirkup"
  - subject: "Re: Where is Plasma headed?"
    date: 2008-01-07
    body: "Thanks for all the hard work, and for taking the time to answer questions & take feedback. I know it might not seem it sometimes, but you've got a very appreciative community out here who seriously impressed with what you've managed to turn around in such a short timeframe. We're also looking forward to seeing what you can pull together for 4.1! Well done, seriously.\n\n(P.S. Remember to get some rest after it's out the door, you've earnt it.)"
    author: "Martin Fitzpatrick"
  - subject: "Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "The plasma website makes all these bold claims about a vision, and this is what gets me.\n\nI love KDE, and I mean I really love it.  I'm excited to see it continue to grow.  However, the Plasma website promises a lot, where is it seems like so little is delivered at the moment.\n\nI'm a patient chap, and if you tell me, we have this vision, and this is what it is going to be, but it will take 18 months, then so be it.\n\nI'm just curious to see what exactly the vision is to reinvent the desktop.  How can we rethink the space, make it work better, etc?\n\nFor starters, I'd wouldn't mind hearing you fully explain the rationale behind your opinion on the Desktop folder.  You say the concept is broken, and I'm curious why you think that, and how we can do it better."
    author: "T. J. Brumfield"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "I would like to see a little containment applet to hold and sort all the icons for the files in ~/Desktop/. As is, the icons get spread out all over the place. A containment applet could provide quick access to files and apps, keep them sorted, and keep them from sprawling all over the desktop. "
    author: "Joe Kowalski"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "Yep - I've been wanting this for years, and I especially would like to be able to reserve a separate area for removable devices.  Thankfully, this should all be relatively easy within the Plasma framework!"
    author: "Anon"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "thing is you wont be able to place new items on desktop so easy....\n\nyou need to go to your home folder... click Desktop... and then make a new file or folder.\n\ndont like this 'feature' personally."
    author: "ptr"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "I'm happy icons are in Plasma at all. From what I can consider, Plasma is the first desktop to allow one to rotate the icons. If only my KWin would work (bug 154637)..."
    author: "Stefan"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-08
    body: "The Answer for this is actually simple.  When QT 4.4 is out, load the dolphin file view kpart (the same that is now being used in konquerors file view) into the containment as a widget on canvas. Now the file icons with this won't be plasma icons, but they will be fully manageable as files."
    author: "Joe  Kowalski"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "It would be nice if the folders on the desktops are these containments. Imagine, you drag&drop a folder to your desktop and what you see is the folder icon. So far nothing new, but when you resize the icon it turns into the containment and shows all the items inside the folder."
    author: "hias"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-08
    body: "Would be easily possible with plasma ;-)"
    author: "jos poortvliet"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-09
    body: "> I would like to see a little containment applet to hold and sort all the\n> icons for the files in ~/Desktop/\n\nthat's already on my list for 4.1 =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-07
    body: "Read Aaron Siego's blog at http://aseigo.blogspot.com/  It will explain you everything. Aaron is very talkative :)"
    author: "Phd student"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-08
    body: "I do, and I've seen him say several times that icons in a Desktop folder is a broken concept, though I'm not sure why.  He writes about hating the old Kicker and desktop, and how Plasma is meant to allow great flexibility, but I haven't read how we're going to reinvent the desktop.\n\n\"This, then, is the goal and mandate of Plasma: to take the desktop as we know it and make it relevant again. Breathtaking beauty, workflow driven design and fresh ideas are key ingredients and this web site is your portal onto its birth.\"\n\nWhere is the workflow driven design and fresh ideas?\n\nAt the moment, we just have an easier means to write widgets.  If widgets are the end-all-be-all of Plasma, I'll count myself extremely disappointed.  We've had widgets since Windows 95, and they've never revolutionized anything."
    author: "T. J. Brumfield"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-08
    body: "Well, the point is that these widgets can do pretty much anything. Very unlike traditional widgets for the desktop. Now we just need to figure out what out of 'everything' it is we need ;-)"
    author: "jos poortvliet"
  - subject: "Re: Dramatic vision of reinventing the desktop"
    date: 2008-01-09
    body: "before i could write the new workflow components, i needed a framework capable of supporting what i needed.\n\nsince i planned on replacing the desktop/panels, i needed a rewritten desktop/panels app on top of that framework.\n\nwhen we replaced the desktop/panels, people wanted every feature they are used to from traditional desktops.\n\nmeanwhile we ran in bugs in x, bugs in drivers in x, bugs in Qt, missing or (for plasma) unformed features in Qt and kwin .. hell, even issues in gtk+.\n\nall this took time to cook. not to mention everything else i end up doing during the day/night. so 4.0 arrived.\n\nin the meantime, many of the the ideas that i talked about publicly a lot have appeared in other environments since. a little depressing ;) i don't think that people listen to me and then run off to implement features in macos, windows, etc.. or anything (i'm not quite that megalomaniacal ;). but i do think that when one person in an industry starts thinking openly about things, then other people near them do as well, and then the next person and the next person and ... ideas ripple and pop up here and there like particles tunnelling about via quantum mechanics. =)\n\nthe ideas i haven't really discussed much haven't appeared anywhere mainstream that i've noticed (yet). so just to avoid jinxing myself even more, i'm keeping a bit more quiet about things until the code is there.\n\ni've also spent more time trying to get deserved attention for the fine work of others in kde4. it's not just all about plasma, you know ;)\n\nand i will admit, another motivation for being quiet is that many times when i i would talk about some sort of feature people would shout \"vapour! vapour! you and your vapour suck!\" which is a bit annoying to deal with while your writing the code to implement those features. even i have limits to how much negative feedback i can receive in one time span. so .. i grow quiet.\n\nanyways.. i'm feeling good and energetic, 4.0 is out, we can barrel forward with 4.1 ... fun times.\n\nas for why ~/Desktop is a bad, stupid and limiting idea ... i'll blog about that."
    author: "Aaron J. Seigo"
  - subject: "BumpTop..?!"
    date: 2008-01-08
    body: "I'm sure some of you have seen Anand Agarawala/Ravin Balakrishnan's prototype of a new Desktop (if not, be sure to check it out at http://bumptop.com/, it's really great work).. I think the usefulness of this does not scale to any kind of computing one would like to do, but it is to be noted as a very refreshing and exciting INNOVATION in the field of graphical user interfaces. I don't know if plasmoids scale to 3D yet, but it would sure be kool to have that possibility, also in the next few years we will see the arrival of 'real' 3D screens as consumer products, so if we talk about 'visions' and computing NG(tm) then certainly we at some point need to give some thoughts about the third dimension ... ;)"
    author: "eMPee584"
  - subject: "Re: BumpTop..?!"
    date: 2008-01-08
    body: "Anand Agarawala/Ravin Balakrishnan's prototype of a new Desktop is kinda messy, but since it's just a prototype I think it is very important to share these ideas to make things better. I don't really know if plasma is going to be in a few years (since it will be able to be pretty much everything ;)) but I think these kind of ideas of making the desktop and apps windows real 3D will pretty much be possible with the addiction of widgets-on-canvas on qt 4.4.\n\nThe preview of this technology can be seen here:\n\nhttp://labs.trolltech.com/blogs/2007/11/22/widgets-on-the-canvas-integrated/\n\nAaron already mentioned about moving plasma to widgets-on-canvas and webkit for applet rendering:\n\nhttp://aseigo.blogspot.com/2008/01/pencils-down.html\n\nWhat will happen then? Only the future will say... And I'd say it's a near and very exciting future...\n\nSorry for my English, still learning..."
    author: "SVG Crazy"
  - subject: "Ktip message regarding panels"
    date: 2008-01-08
    body: "Hi, today I runned KDE4 on a new user from a saturday's build on kde 4.0 branch and I found Ktip first tip was on a way for adding new panels that I'm not sure if it still valid on Plasma, because it remainds to me the kicker way to do it, even if the Ktip message doesn't mention kicker. I attached the Ktip message.\n\nBTW, it is possible in Plasma to have another panel? I ask this because my brother can't live without a top panel with knewsticker. I haven't seen any GUI method to do it, but I'm interested if it is possible editing a Plasma configuration file."
    author: "Josep"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-08
    body: "KTip will probably be killed off relatively soon.\n\nI'm not sure if Plasma in its current state supports multiple panels, but it is almost guaranteed to come 4.1."
    author: "Anon"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-08
    body: "To get a second panel edit .kde4/share/config/plasma-appletsrc and add something like this at the bottom:\n\n[Containments][3]\nformfactor=2\ngeometry=0,829,1440,71\nlocation=3\nlocked=false\nplugin=panel\nscreen=0\ntransform=1,0,0,0,1,0,0,-906,1\n\nthen you will need to restart plasma (killall plasma & plasma is a quick way to do this) and you will be able to drop widgets in your new top panel"
    author: "Charles Fryett"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-08
    body: "If someone can explain the rationale for the different parameters I'll add it in my in-progress Plasma FAQ. THis is surely a feature that's going to be requested and if there's a workaround like this, better."
    author: "Luca Beltrame"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-09
    body: "I have a 1440x900 LCD so with some of the parameters are easy (I'll guess the other poster below has a 1280x1024.\nlocation=3 is the top edge\nlocation=4 is the bottom edge\nlocation=1 & 2 are the sides I would guess (but don't work atm)\n\nAnd for the rest I guess someone will ahve to look at the code ;)\n\n"
    author: "Charles Fryett"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-05-08
    body: "Hi Charles, thanks for your very informative post. I have been using kubuntu for a year and just switched to KDE4 (with a fresh install of hardy). \n\nI have a feeling I'm about to start REALLY tweaking KDE settings... I almost started writing a php tool to rewrite the plasma-appletsrc file... maybe instead I need to research how to use kdialog for it?? \n\nAnyhow, I'm replying because I'm having an interesting problem and hope that you could reply with your thoughts on my situation with where to look, post, search terms to use, etc... I have googled like mad (which finally came up with this dotKDE thread) and there is SO much content that searches come up with little real info (like yours).\n\nMy situation:\n\nHardware:\nI have a laptop with 1440x900 attached LCD... at work, I drop it onto a docking station and use a 24\" monitor to run 1920x1200 ... at home, I plug in a 19\" LCD at 1280x1024. \n\nKDE4 behaviour:\nThere is a \"transparency\" the size of 1440x900 with the taskbar on it. This is 1440x900 regardless of which monitor is present. The plasma \"button\" thing is in the top right corner of the 1920x1200 desktop (again, doesn't matter the size of the viewing area of current monitor).\n\nWith the taskbar at the \"bottom\", it is at the bottom edge of the 1440x900 area and on the external monitors (with more vertical pixels than laptop) windows can be moved down but the taskbar stays on top of them. So I move the taskbar to the top, no problem. Buuut, on the 19\" monitor, there is no plasma button and the taskbar's tray is off the right of the screen. (mouse still goes out there and can click on things, too!)\n\nMy next workaround may be to change its length via various different plasma-appletsrc files and a bash script to execute the change. (I already do this to alter my /etc/hosts and .ssh/known-hosts files so things work when moved from home to work, etc)\n\nI'm trying to learn more about xorg, but prior to KDE4, the desktop and ubuntu worked seamlessly in the above 3 cases. I suspect this is a temporary situation caused by hard coding (and will be fixed soon) so I'm hoping for a quick fix or something \"simple\" that I'm not aware of.\n\nRight now, I'm going to try a fresh install using the kubuntu+kde4 mix iso... Maybe there are some small changes to it that are better than my current brew of unbuntu + adding packages??\n\n:) Chris\n"
    author: "chris"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-08
    body: "It is possible in plasma to have another panel today. What we don't have is the UI to make it yet. I think it's because this feature isn't completely done yet and need some minor adjustments(as well as the knewsticker applet). I'd say that it will be ready and shiny in kde 4.1.\n\nIf you want to test this feature anyway, all you have to do is edit the plasma-appletsrc file. On Opensuse, it is located at:\n(don't forget to backup the file first)\n\n~/.kde4/share/config\n\nAll you have to do is put a third Containment, plugin panel. (Since the first containment is the desktop and the second is the panel). Remember to change the location to 3 (location 3 is top). That means a panel at the top of the screen. \n\nExample:\n\n[Containments][3]\nformfactor=2\ngeometry=0,5,1280,10\nlocation=3\nlocked=false\nplugin=panel\nscreen=0\ntransform=1,0,0,0,1,0,0,-1030,1\n\nRestart plasma.\nYou can easly put and widget to the panel after that (the way we usually do).\n\nIt is very fun to play with this file and make some tests...\n\n\nAs I say before, I think this feature is not completely done, but don't worry, Kde developers will make it work perfectly :D\n\n\nHere's a screenshot to prove it"
    author: "SVGCrazy"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-08
    body: "Thank you!!!\n\nI've got another question, it will be possible to make the top panel a bit smaller?"
    author: "Josep"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-01-08
    body: "I tried to change the size of the panels by changing the geometry parameter but it didn't work for me. I don't know if it's either a plasma bug or an opensuse's package problem. I think it wasn't implemented yet (maybe that's why we don't have a UI to change panel sizes)."
    author: "SVG Crazy"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-02-24
    body: "I noticed the same issue. I tried to halve the default panel's size to 24 (+4 for separator), and set all the coordinates correctly, and nothing changed. I made sure to kill off plasma before making the changes. Maybe it needs a restart."
    author: "fenerli"
  - subject: "Re: Ktip message regarding panels"
    date: 2008-02-24
    body: "There's a lot of stuff in Plasma that was hard-coded in the frenzied rush for the release date - I'd wait until you use packages where re-sizing has been officially added by the devs, if I were you :)"
    author: "Anon"
---
<a href="http://www.arstechnica.com">Ars Technica</a> is reporting on the tagging of KDE 4.0.0 in an article titled, &quot;<a href="http://arstechnica.com/journals/linux.ars/2008/01/04/kde-4-0-0-tagged-in-preparation-for-release">KDE 4.0.0 tagged in preparation for release</a>.&quot; In this report, Ars Technica briefly explains the importance of the tagging process as well as what one can expect with the upcoming KDE 4.0.0 release. &quot;<em>Although the 4.0 release has many rough edges, it also showcases a tremendous number of innovative new features and technologies.</em>&quot;  4.0 is scheduled for release at the end of next week.




<!--break-->
