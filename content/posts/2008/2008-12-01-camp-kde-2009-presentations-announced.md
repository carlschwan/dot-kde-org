---
title: "Camp KDE 2009 Presentations Announced"
date:    2008-12-01
authors:
  - "wolson"
slug:    camp-kde-2009-presentations-announced
comments:
  - subject: "Good selection"
    date: 2008-11-30
    body: "This looks like a pretty good list, Wade. It's nice to see so many people from different areas of KDE's broad ecosystem on board for this event. :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Good selection"
    date: 2008-12-01
    body: "Yes, the thanks goes to all of those that replied to the CPF.  We'll be posting the schedule for the presentations this week.\n\nAnd more importantly, one of the presenters is the self-proclaimed Duke of Poland.  I'm glad that person let us know ahead of time, allowing for additional security precautions."
    author: "Wade"
  - subject: "\"Bug\""
    date: 2008-12-01
    body: "Sorry guys, but there's a small bug in this announcement. It should be \"Culture\" not \"Culuture\".\n\nOther than that, great!"
    author: "Paul Jimenez"
  - subject: "Re: \"Bug\""
    date: 2008-12-02
    body: "Fixed, thanks.\n\nDanny"
    author: "Danny Allen"
---
At the start of the month the organisation committee for <a href="http://camp.kde.org/">Camp KDE</a> asked for <a href="http://dot.kde.org/1225821886/">talk proposals</a> for our conference in the Caribbean. We have reviewed the excellent submissions and come to an agreement on selected presentations. Read on for the talks.



<!--break-->
<p>We wish to thank all those who entered submissions. In no particular order, the following presentations will be given at Camp KDE 2009 on January 17-18, 2009:</p>

<ul>
<li>Pradeepto Bhattacharya: KDE and Global Culture</li>
<li>Kenneth Christiansen: Declarative user interfaces, using Qt and QEdje</li>
<li>Marcus Hanwell and Gökmen Göksel: KDE and distros</li> 
<li>Eugene Trounev: KDE Games and graphics</li> 
<li>Holger Schroeder: KDE and Windows </li> 
<li>Guillermo Antonio Amaral Bastidas: KDE and Business Software</li> 
<li>Jeff Mitchell: Why Enterprises Should Hire Open-Source
Programmers</li> 
<li>Soren Howard: Amarok 2 and automatic playlist generation</li> 
<li>Sebastian Kugler: Plasma and small formfactor devices </li> 
<li>Till Adam: Akonadi</li> 
<li>Orville Bennett: KDE and Mac </li>  
<li>Bill Hoffman: CMake</li> 
<li>Zack Rusin: Accelerating Graphics</li> 
<li>Leo Franchi: Amarok 2 and use of libplasma in applications</li> 
</ul>

<p>These presentations will be scheduled on the <a href="http://camp.kde.org/schedule.xhtml">Camp KDE 2009 website</a>. We feel that these presentations provide a strong range of topics and interests. In addition, we hope to have many Bird-of-a-feather (BoF) meetings on January 19-20, 2009 on topics from Qt development to <a href="http://techbase.kde.org/Contribute/Bugsquad">BugSquad</a> and many more in-between.</p>