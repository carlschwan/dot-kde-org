---
title: "Sunday 8th June is KDE PIM Krush Day"
date:    2008-06-07
authors:
  - "ggoldberg"
slug:    sunday-8th-june-kde-pim-krush-day
comments:
  - subject: "Damnit."
    date: 2008-06-07
    body: "I'd love to be in on a kdepim bugsquashing day. But this sunday I'll be somewhere without a network connection (I think)."
    author: "Oscar"
  - subject: "Re: Damnit."
    date: 2008-06-10
    body: "Drop into #kde-bugs anytime, and we can train you and you can start working on whatever we're currently doing. We just announce the BugDays, but we actually keep doing stuff continually. *g*"
    author: "blauzahl"
  - subject: "KDE4DAILY on qemu & virtualbox"
    date: 2008-06-08
    body: "I ran KDE4DAILY on virtualbox on Ubuntu 7.10 prior to the 4.0 release, and I think it worked at acceptable speed. I tried running the current KDE4DAILY in qemu on Ubuntu 8.10 and it crawled so painfully I gave up. Unfortunately I have no idea how to make virtualbox work on Ubuntu 8.10, so I can't compare. Any help and suggestions will be appreciated. Machine model and make: Acer Aspire T180."
    author: "yman"
---
This Sunday (8th June), the <a href="http://techbase.kde.org/Contribute/Bugsquad">KDE Bugsquad</a> will host a KDE PIM Krush day. The aim of the day will be to find and document as many of the bugs in the PIM applications (including KMail, Kontact, Akregator and many more) of the upcoming KDE 4.1 release as possible. The day will begin at 0:00 UTC on Sunday and continue until the day is over throughout the world. Krush Days are an excellent opportunity for KDE users keen to make their first contribution to the KDE project. You don't need to set aside a huge amount of time - as little as ten minutes will be enough to do some testing - nor do you need to have any programming experience. All you need is a recent version of KDE. <a href="http://kde.org/announcements/announce-4.1-beta1.php">KDE 4.1 Beta 1</a> will do, or a newer version from SVN. If you don't want to install KDE 4.1 on your system, then the Virtual Machine Image <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html">kde4daily</a> is ideal. Just come along to <a href="irc://irc.kde.org/#kde-bugs">#kde-bugs</a> where the Bugsquad will help you get started. More information can be found on the <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KDEPIMKrush1">Krush Day Techbase page</a>.
<!--break-->
