---
title: "Interview: MarkMail Indexes KDE Mailinglist Archives"
date:    2008-08-06
authors:
  - "jpoortvliet"
slug:    interview-markmail-indexes-kde-mailinglist-archives
comments:
  - subject: "Great! :)"
    date: 2008-08-06
    body: "I've played a few minutes with the service and it rocks! \nGreat job really, well presented, fast and, most important, accurate results.\nThanks for this useful tool :)"
    author: "Lucianolev"
  - subject: "archive"
    date: 2008-08-06
    body: "Are there also services like these for non-public mailing lists or means to stop at least google indexing? \n\nThe reason I detest public archives is that it gets indexed by google and may also pose legal liabilities under German law (Abmahnungen) you can hardly manage if enforced in a ruthless manner. It further invades privacy. \n\nI always had a problem with attachments to mailing list archives under mailman.\n\n"
    author: "velocifer"
  - subject: "Privacy issue"
    date: 2008-08-06
    body: "This is a nice idea, but I think there is a big privacy issue. First a lot of bug reports get automaticly forwarded to mailinglists, exposing the email address of the reporter to the public and search engines. I know, this has happend with the old archives, but not it is even easier. I guess most of the bug reporters are not aware oft this fact. A similar problem arises through some support-email addresses of some KDE-programs, which are in fact mailing lists. Here also the emailaddresses of people asking for support are exposed into the public through the archives being scanned by search engines. It would be great, if there was a solution for this."
    author: "Mark"
  - subject: "Re: Privacy issue"
    date: 2008-08-06
    body: "Yes, this is why I always refrain from sending the crash reports, e.g. of Amarok.\n\nHowever I don't really understand why its necessary that your email gets revealed."
    author: "velocifer"
  - subject: "Re: Privacy issue"
    date: 2008-08-06
    body: "Geez, being concerned about exposing your email address is so 90s. \n\nMy address is all over the web (you can google it in 10 seconds), and still Gmail deals fine with the spam. Absolutely manageable.\n\n\nkretschmann(replace_tinfoil_hat_with_@)kde.org\n"
    author: "Mark Kretschmann"
  - subject: "awsome!"
    date: 2008-08-06
    body: "Can I get free advertising for my proprietary privacy invading web20 web site just cause I'm barely helping a FOSS project so I can get free advertising for my proprietary privacy invading web20 web site?"
    author: "BiddyBoo"
  - subject: "Re: awsome!"
    date: 2008-08-06
    body: "Have you ever heard about lists.kde.org ? It already have everything to invade your privacy, this site adds nothing new with it. There is also gmane.org - here you even have search interface (for example: http://dir.gmane.org/gmane.comp.kde.devel.core ). The problem with privacy is more general - all data is available regardless of MarkMail.\n\nWhat MarkMail adds is really good interface for browsing the archieves (which is really important for people who have not subscribed for that archieves for ages and thus don't have local copy of the history). For example one can use it to check whether given topic was already discussed before asking it themself. Yes, of course its not free, and it sucks. But hey, the google is also not free, but you don't blame it when it sponsors FOSS projects, do you ?\n\nAnd about free advertising - well, its actually not free. The company does something for project and as reward gets advertasement. It's quite common in free software world, and it's good for both the company and the project and is very natural way of working together."
    author: "Anonymous"
  - subject: "Re: awsome!"
    date: 2008-08-06
    body: "There are plenty of websites who offer nice interfaces for searching mailing list, source code, coders etc... Are we going to advertise each and every one of them that helps FOSS a little more than other especially if they are not FOSS themself? (not to mention this one is full of flash nasty stuff).\n\n> But hey, the google is also not free, but you don't blame it when it sponsors FOSS projects, do you ?\n\nWhat does it have to do with it? I've never seen an announcement on the dot about how google products should be used to help KDE developers. The only google announcements here are about Google SoC which are not about how people should use google products. And for what I know, markmails is not sending any checks to young kde hackers."
    author: "BiddyBoo"
  - subject: "Re: awsome!"
    date: 2008-08-06
    body: "\"Are we going to advertise each and every one of them that helps FOSS a little more than other especially if they are not FOSS themself?\"\n\nProbably not. And why should they?\n\n\"not to mention this one is full of flash nasty stuff).\"\n\nThen don't use it. Sheesh. \"we should not talk about this web-service because it uses Flash!\"...\n\nAnd if you consider this service to be \"invasion of privacy\".... Well, how on earth can you expect one shred of privacy on a PUBLIC mailinglist? All the data they display and use is 100% public by default."
    author: "Janne"
  - subject: "Re: awsome!"
    date: 2008-08-06
    body: "Well, the search is for the \"better tool\". In the recent years the concern for privacy is getting higher as no data is lost anymore and governments apply anti-terror madness. People are not sensitive enough and reveal everything about themselves they can over myspace and the like.\n\nOn the other hand mail encryption is still immature and poses an usability burden. Who uses gmail makes the decision to go public. Fine.\n\nParanoid mode: when I reveal what software version I am using I consider this a personal security problem for a targeted attack.\n\nRead this:\nhttp://ec.europa.eu/public_opinion/flash/fl_225_en.pdf\nhttp://ec.europa.eu/public_opinion/flash/fl_226_en.pdf\n\nTwo-thirds of respondents throughout the EU (65%) indicated that their \ncompany transferred personal data via the Internet. One in three respondents (32%) admitted that their company did not take any security measures when transferring personal data over the Internet.\n\nAmong companies that transferred personal data to non-EU countries, \nalmost half of respondents (46%) indicated that this data mostly concerned clients\u0092 or consumers\u0092 data for commercial purposes, and 27% said it was human resources data for HR purposes.\n"
    author: "velocifer"
  - subject: "Re: awsome!"
    date: 2008-08-07
    body: "\"Paranoid mode: when I reveal what software version I am using I consider this a personal security problem for a targeted attack.\" \n\nSo you are all for security by obscurity?"
    author: "Vide"
  - subject: "Re: awsome!"
    date: 2008-08-08
    body: "You mix a matter of principle, it is a matter of nondisclosure. \n\nYou don't document your infrastructure for an attacker. \n\n\"Security by obscurity\" relates to non-transparency of the tools as such.\n\nIf someone has the ability to find out that I am using a service x he can target me specifically. E.g. I get a lot of phising messages for financial institutions I am not a client with. But if an attacker has access to my customer data or knows that I contacted the bank and who the person at the bank is he might succeed as he can customize the attack in a more intelligent way. Don't reveal what you don't have to reveal.\n\nEven the fact that you use Linux may be an issue you have no desire and no reason to disclose to the public at large. It is as private as which books you have in your room. I don't want \"my room\" to communicate to the rest of the world which books I store.\n\nFor submitting a defect report an association with my real name is factually irrelevant. "
    author: "velocifer"
  - subject: "Re: awsome!"
    date: 2008-08-06
    body: "Oh, yes, if you help us, we help you.\n\nThat's normal in FOSS - if someone does something for the project, we try to thank them. MarkMail didn't ask for this interview, you know... WE asked them to index our stuff, they did, we decided it would be cool to let the world know about it."
    author: "jospoortvliet"
  - subject: "There's a CAPTCHA to protect email addresses"
    date: 2008-08-06
    body: "All email addresses shown by MarkMail are obfuscated.  Only by clicking on an address and solving a CAPTCHA will you be able to see what the true address is.  This is true for any emails mentioned in the body of a message also.\n\nThe goal is to make the site useless to address harvesters but useful to people who want to communicate with each other.  We think this strikes a nice balance.\n"
    author: "Jason Hunter"
  - subject: "Re: There's a CAPTCHA to protect email addresses"
    date: 2008-08-07
    body: "Hi Jason,\n\nindeed! I like that feature very much. Thanks for that.\n\nMarcel"
    author: "Marcel"
  - subject: "Threaded browsing"
    date: 2008-08-06
    body: "Thanks for the great work. It would be also very cool to have threaded browsing available."
    author: "Anonymous"
  - subject: "Re: Threaded browsing"
    date: 2008-08-06
    body: "Just click on an email and it opens up the thread."
    author: "Ian Monroe"
  - subject: "Re: Threaded browsing"
    date: 2008-08-07
    body: "No, if I click on a message, I get a *list* of messages in a conversation, not a tree-like view that gives me an idea of which message is a reply to which other message (the different threads in the conversation). Or is there something I am overlooking here?"
    author: "Andr\u00e9"
  - subject: "Wrong URL linked"
    date: 2008-08-06
    body: "It's www.markmail.org not www.markmail.com..."
    author: "axel"
  - subject: "Only works with JavaScript GLOBALLY enabled"
    date: 2008-08-06
    body: "I have globally disabled JavaScript, and only enable it using per-site rules in Konqueror. However, markmail search site does not display any results this way. Please fix.\n"
    author: "christoph"
  - subject: "Re: Only works with JavaScript GLOBALLY enabled"
    date: 2008-08-06
    body: "Your internets are broken."
    author: "Ian Monroe"
  - subject: "Re: Only works with JavaScript GLOBALLY enabled"
    date: 2008-08-07
    body: "I guess your rules are broken then. Using per-site rules with Firefox (sorry, I am at windows at this machine) just works."
    author: "Andr\u00e9"
  - subject: "Re: Only works with JavaScript GLOBALLY enabled"
    date: 2008-08-08
    body: "I needed to whitelist both markmail.org and kde.markmail.org, sorry for the bogus alert, maybe a Konqueror bug."
    author: "christoph"
  - subject: "Jason fan"
    date: 2008-08-06
    body: "I was a big fan of JDOM back when I was a Java programmer, it really did make XML processing in Java tolerable. Nice to see Jason's name pop up again, this time in relation to KDE!"
    author: "taj"
  - subject: "fun"
    date: 2008-08-06
    body: "I like the kind of statistics you can easily get out of it. (Like with any search engine, the first thing I do is search myself :D). And the UI is really well done. Good stuff."
    author: "Ian Monroe"
  - subject: "Well"
    date: 2008-08-06
    body: "Hello,\n\nit's good times if people provide free services like this one. \n\nI personally don't like that it wants to use flash for the graph. The layout (results in a scollbar container) seems broken, but that could be my outdated KHTLM (3.5.9), overall, when searching for myself, I found a pretty nice set of results, and good way to view it.\n\nI would say this site is very useful. Only a pity that its software is not Free, is it?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "What I'm missing"
    date: 2008-08-07
    body: "markmail is excellent if you are searching for specific topics or just want some statistics, but it is very bad for just browsing the mailing lists.\n\ni.e. select a specific list and get an overview of alle threads\n\nlists.kde.org does a better job here.. would be nice to see an improvement there"
    author: "Robin"
  - subject: "Re: What I'm missing"
    date: 2008-08-07
    body: "oh, just found the browsing mode:\nhttp://kde.markmail.org/docs/faq.xqy#browse\n\nhowever, it's very basic..."
    author: "Robin"
  - subject: "Re: What I'm missing"
    date: 2008-08-10
    body: "sounds like something for a feature request ;-)"
    author: "jospoortvliet"
  - subject: "Re: What I'm missing"
    date: 2008-08-10
    body: "I really wish markmail provided an nntp interface. I far prefer using a good nntp client with threading to any online forum/list archive. I find discussions a whole lot easier to follow. I also prefer it to mailing lists since i don't have to worry about my mail account filling up with lots and lots of messages."
    author: "Chaz6"
---
Several weeks ago <a href="http://www.markmail.org">MarkMail</a>, a project sponsored and run by <a href="http://www.marklogic.com">Mark Logic</a>, started indexing the KDE mailinglist archives. After about a week of hard work, the KDE archives are now <a href="http://kde.markmail.org">directly searchable</a> from MarkMail. Besides interesting analytics, this brings some powerful search capabilities to the table. Read on for a short interview with Jason Hunter who was responsible for engineering on the project.






<!--break-->
<p><b>Hi Jason! Could you give a little introduction of yourself and <a href="http://www.marklogic.com">Mark Logic</a>?</b></p>

<p>Hi, KDE! I'm a Silicon Valley hacker. I've been working at <a href="http://www.marklogic.com">Mark Logic</a> for about 5 years now, since the days it was an early startup. We sell <a href="http://www.marklogic.com/product/marklogic-server.html">MarkLogic Server</a>, a special-purpose database built for content (where "content" is the stuff that's textual, hierarchical, irregular, and not often regularly repeating - like books, articles, and presentations). We use XML as our native data type instead of tables, and pride ourselves on performing very well at high scale.</p>

<p>Until about a year ago I worked with our customers to help them write content apps. I had the idea that we could use the core server to build a public email archive repository, using some of the product features to push the envelope of what people had done before with email archives. That's where <a href="http://www.markmail.org">MarkMail</a> came from. We started with 4,000,000 emails from the <a href="http://www.apache.org/">Apache Software Foundation</a> mailing lists.</p>

<p>I've been involved with open source for a long time, leading JDOM and participating as a member of the Apache Software Foundation, so it felt natural to put MarkMail to work initially on the problem of getting more value from open source mailing lists.</p>

<div align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;"><a href="http://static.kdenews.org/dannya/MarkMail.png" title="Click to enlarge"><img src="http://static.kdenews.org/dannya/MarkMail_thumb.png" align="center" height="425" /></a><br /><em>Konqueror showing MarkMail's search results</em></div>

<p><b>Why did you decide to grab the KDE mailinglists?</b></p>

<p><a href="http://behindkde.org/people/cornelius/">Cornelius Schumacher</a> started the ball rolling when he asked if we could load the KDE lists. OK, that's not quite true. We have a long list of communities whose lists we hope to load, and KDE was actually on that list since the very beginning. It's just that one day in April we heard from Cornelius, and the next day received a separate request from <a href="http://behindkde.org/people/ade/">Adriaan de Groot</a>. That popped KDE to the top of the priority list.</p>

<p><b>The KDE mailinglists aren't the largest you have at MarkMail, but they sure aren't small. Did that pose any problems?</b></p>

<p>Yes, KDE is <i>Big</i>. At current count there's 2.7 million KDE emails. Hosting those emails isn't an issue (we're designed to scale to hundreds of millions) but we had to work hard to gather clean historical archives. We have one person on the MarkMail team dedicated only to this (we like to call him an email archaeologist. I'm not sure he's happy about that nickname).</p>

<p>Why the challenge? Well the most authoritative archives for KDE were the web-based <a href="http://mail.gnu.org/pipermail/">Pipermail</a> archives (I'm using past tense because I'd like to think that today the most authoritative archives are in MarkMail). Pipermail exposes a set of "mbox" files for each archived list. Very handy. The mbox file format is a classic storage format for email and a format from which we can readily load. But as we found out, the mbox files aren't really mbox and there was a lot of post-processing we had to do. Some examples:</p>

<ul>
<li>Pipermail "scrubs" attachments from its mbox files. Instead of placing the attachment content into the message as normal, it gets placed at an external URL with a marker in the message dictating where you can find it. We had to recognize the scrubbed references, fetch the attachments, and then inline the contents. Sounds simple, doesn't it? It probably would be if the external links were always accurate. Sometimes we could guess and fix things and sometimes we couldn't - bonus points go to anyone who finds an email in MarkMail mentioning an attachment that doesn't really exist. Extra bonus points if you know our search syntax well enough to write a query that directly lists those emails.</li>
<li>Then there's the problem with character encodings in old emails. If you look at an mbox file it seems like ASCII, but in fact it's a binary file. That's because each message may have a different character encoding for its body (or even portion of the body). The Pipermail list archiver didn't always realise this, and fixing that was non-trivial and imperfect.</li>
</ul>

<p>There are more examples, but I don't need to bore you. I should make clear it's nothing special with KDE or even with Pipermail. Turns out if you load a couple million emails you'll see at least one example of almost every problem that's ever existed. It's the same for every community, just with different challenges.</p>

<div align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;"><a href="http://static.kdenews.org/dannya/MarkMail_search.png" title="Click to enlarge"><img src="http://static.kdenews.org/dannya/MarkMail_search_thumb.png" align="center" height="435" /></a><br /><em>Graphically drilling down to a specific date</em></div>

<p><b>You mentioned pushing the envelope. Can you give an example of that?</b></p>

<p>Sure, here's a good example: When you do a search, besides getting the top 10 most relevant emails, you see lots of analytics. You see a histogram chart showing the number of messages matching your query each month across time. With it you can watch trends for lists, people, ideas, or any combination. Every query also shows the top senders, lists, attachment types, and message types for the messages matching the query. You can learn who's an expert on a topic, on what lists something is being discussed, which people are most involved on lists, and so on. By dragging across bars on the graph you can limit the view to just a particular time period. You can also click on any person's name or list name to limit the search. It's convenient to start with a simple query and refine interactively.</p>

<p>We've also strived to make the site easy to navigate. You can hit "n" and "p" to go to the next and previous search results. To move up and down the thread view you hit "j" and "k" (a homage to vim users). If you find an attachment (search for ext:pdf) you can <a href="http://kde.markmail.org/search/?q=ext%3Apdf">view it inline in your browser</a>.</p>

<p>Oh, and here's a little-known tip. If your screen is sufficiently wide, we give you all three panes (analytics, results, messages) at once. If not, you get the "slide".</p>

<p><b>Do you have any tips for the KDE community to take advantage of the available capabilities in MarkMail?</b></p>

<p>The first thing to remember is that you can limit your view to KDE-related mails by going to <a href="http://kde.markmail.org">kde.markmail.org</a>. The use of a subdomain adds an implicit constraint to all your queries.</p>

<p>Another is that you can do negations. For example, KDE has a huge number of automated emails generated by bug reports and code check-ins. You can search without those by adding -type:bugs -type:checkins. For example: <a href="http://kde.markmail.org/search/?q=-type%3Abugs+-type%3Acheckins">http://kde.markmail.org/search/?q=-type%3Abugs+-type%3Acheckins</a></p>

<p>Lastly, if there's any other lists people want to see, let us know at <a href="http://markmail.org/docs/feedback.xqy">our feedback page</a>. You can track what we're up to at <a href="http://markmail.blogspot.com">our blog</a>.</p>

<b>Thank you very much, Jason, both for the work <a href="http://www.marklogic.com">Mark Logic</a> and you've been doing, and of course for granting this interview!</b>

