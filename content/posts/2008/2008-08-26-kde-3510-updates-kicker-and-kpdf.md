---
title: "KDE 3.5.10 Updates Kicker and KPDF"
date:    2008-08-26
authors:
  - "skuegler"
slug:    kde-3510-updates-kicker-and-kpdf
comments:
  - subject: "Nice"
    date: 2008-08-26
    body: "I love 3.5. Nice to see it's still getting updated. :)"
    author: "tsb"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "Didn't we promise we would? :D"
    author: "jospoortvliet"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "Yeah, well .. Canonical said you wouldn't. Someone was wrong."
    author: "Tom"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "I don't remember saying any such thing.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "Hello,\n\nI think Kubuntu 8.04 is not LTS because of the assumption that KDE 3.5 will not be maintained until 2011.\n\nI wouldn't bet my first born on security fixes for KDE 3.5 in 2010. Could very well be that its up to distributors themselves to do that. \n\nAnd that could well pose a problem to anybody without deep pockets. They would have to do it for free, as opposed to the RHEL/SLES people. As Ubuntu must remain free of charge (they promised to never charge for Ubuntu), it's a tough demand to do what the community may not want to do.\n\nIn 2010 we are talking about 4-5 more releases of KDE 4, like KDE 4.6, only really old fashioned people will even consider KDE 3.5 at that time. Who is going to spend his free time to support KDE 3.5 then?\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "2010 is only two years off. I know that seems like forever in internet years, but it really isn't. I don't understand the attitude of some developers that they won't support security fixes for a mere two years."
    author: "David Johnson"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "This may seem strange, but it is open source, and people work on what they want to work on.  "
    author: "Troy Unrau"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "OR what they're paid to work on.\nFOSS supports and encompasses pretty much all development models out there."
    author: "ethana2"
  - subject: "Re: Nice"
    date: 2008-08-29
    body: "> FOSS supports and encompasses pretty much all development models out there.\n\nThat's quite a bold statement and the not uncommon attitude Troy mentioned contradicts it. And no, I don't think there're enough developers paid for pure maintenance, to compensate for the low interest in long term maintenance of unpaid open source developers."
    author: "Carlo"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "Yes of course. There's no guarantee that developers will maintain their software. That doesn't make the attitude a good one, however. It's not fun and it's not sexy, but maintenance *is* a part of the software lifecycle."
    author: "David Johnson"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "Hello,\n\nsee David, that's not attitude, that is free will, and circumstances. \n\nLike your favorite distribution doesn't offer from 3.5 anything else but kdelibs anymore. And if you were to compile it yourself, you probably would find out that:\n\na) The library versions you can have on your distribution (easily) are too new and have had interface changes. Tough game. \n\nTry to compile KDE 2.x once in a while on a brand new distribution, to get an idea.\n\nb) The C++ compiler of your distribution will no longer accept the invalid C++ that in one form or the other is in KDE 3 right now. \n\nI recall trying to compile a g++ 3.2 on a system that had 4.x series only. I had to go backward in steps to get the gcc compile the gcc. \n\nI am almost 100% sure, KDE 2.x won't be accepted by latest gcc release. Prove me wrong.\n\nc) You don't even know a KDE 3.x user. These guys certainly don't install new distributions (which won't come with it anymore).\n\nAnd if they use free distributions, it must be Debian and they will do it themselves anyway for the Stable release, but everybody will be using Testing, which will be on KDE 4 already. All others are going to be unsupported by the time, so would users actually see your fixes?\n\nIf they are commercial users, it's not supported by KDE, so you would barely notice and why should you care (they paid other guys already to do it).\n\nd) By then KDE 4 has had 5-6 releases by the time. It is so far better than what KDE 3.5 achieved, you can't convince yourself to invest time into it. That's like sending your ex-wife flowers although you found the perfect replacement - you just don't do it.\n\nOverall, how I see it: Right now, KDE is maintaining 3.5 out of a self-declared unability to satisfy (all of) its users with the new releases. And out of the self-obligation to not let users in the dark.\n\nBut as KDE 4 progresses and all distributions do releases based on it, that points become moot and 3.5 will no longer be supported by KDE. \n\nMy guess would be that with 4.3 at latest, we could have the point where KDE community decides to discontinue its support. But be 100% assured, only when these criterias are met, will it happen. And as we know, they will be met, sooner or later :-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "Kay, I agree you have a point but if you start exaggerating you'll have a harder time convincing people. \n\n\"Try to compile KDE 2.x once in a while on a brand new distribution, to get an idea.\"\n\nThe last KDE 3.0 was released over 6 years ago! Also there wasn't much of a reason to stick with KDE 2.2, it was not that mature anyway (only 3 minor releases) and the transition to KDE 3 was pretty smooth and far from radical, more of an evolution. \n\nNow it's a whole different game. We're talking about compiling KDE 3.5 in two years time (not six). KDE 3.5 is incredibly stable and mature (11 minor releases!) and KDE 4 is a much more radical change, so distros will have much more interest in maintaining 3.5 then they had in 2.2. \n\n"
    author: "ad"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "Opss, of course that should be 6 minor releases for KDE 3  (not 11)  Still twice as much as those for KDE 2. \n\nAnd then 10 bugfix releases for KDE 3.5 release versus only 2 bugfix releases for KDE 2.2.\n\nPoint remains. 3.5 is way more mature and stable. "
    author: "ad"
  - subject: "Re: Nice"
    date: 2008-08-29
    body: "Hello ad,\n\nI didn't intend to exagerate, it's just that people seem to think that supporting KDE 3.5 will be easy once the developer distributions have jumped ship to KDE 4.x as default.\n\nThe effects are the same as I mentioned though. KDE 2 used Qt 2, other library versions under rapid development, required older compilers, the same problems already applied at the time 3.1 was released. And they will apply each time that KDE breaks ABI, be it 1.x -> 2.x -> 3.x -> 4.x -> ?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "OK, I didn't say you did .. but the rational behind not supporting KDE3 for 3 years was that upstream would drop support for KDE3 and not update it anymore. At least that is what was reported on the interwebs.\n\n"
    author: "Tom"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "> At least that is what was reported on the interwebs.\n\nWell, it must be true then. ;)"
    author: "mart"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "\"I don't remember saying any such thing.\"\n\nNo, but Canonical certainly did in the discussion on what to do for a LTS release. The premise was that KDE 3.x would not be updated during the lifetime of KDE 4."
    author: "Segedunum"
  - subject: "Re: Nice"
    date: 2008-08-26
    body: "I wouldn't worry what they say.\n\nBecause 3.5 is still a major part of OpenSUSE, you can expect there to be updates for a few years, at least from the OpenSUSE team."
    author: "Anon"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "I've had openSUSE devs tell me directly that they expect KDE 3 to be dropped in the upcoming 11.1 or 11.2.  Fedora already dropped it, and I heard Kubuntu is considering it as well.  Between those three, that would largely kill off a good chunk of the KDE 3 user base.  I'm not sure what Mandriva's plans are."
    author: "T. J. Brumfield"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "3.5 will still be mantained, due to it being part of SUSE, there is no reason to keep it in OpenSUSE 11.1"
    author: "Anon"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "Some people need the functionality of 3.5 more than the bling of 4.x and others just don't want to get used to a new interface, apps (like dolphin), new configurations, etc. Unless KDE 4 will offer some new & useful functionality that people can't live without, there will still be reasons to update KDE 3 and to include it in distros."
    author: "bico"
  - subject: "Re: Nice"
    date: 2008-08-28
    body: "\"Unless KDE 4 will offer some new & useful functionality that people can't live without\"\n\nDoesn't it offer such already? Only compositing is worth the switch (not in the sense of bling, but of improved task management); or look at the new Gwenview, the possibilities with Plasma (even without panel auto-hide), and the upcoming Amarok."
    author: "Stefan Majewsky"
  - subject: "Re: Nice"
    date: 2008-08-27
    body: "openSUSE 11.1 will have KDE 3.5.10\nhttp://en.opensuse.org/Roadmap/11.1"
    author: "ad"
  - subject: "kicker"
    date: 2008-08-26
    body: "Lets hope it doesn't crash as much now."
    author: "blah"
  - subject: "Re: kicker"
    date: 2008-08-26
    body: "... then please stop compiling on your own if you have no clue and start using a proper distribution ..."
    author: "MyLaidy"
  - subject: "Re: kicker"
    date: 2008-08-26
    body: "Don't remember it ever crashing on me on 3.5.9 (openSUSE 10)."
    author: "ad"
  - subject: "Re: kicker"
    date: 2008-08-27
    body: "I've had kicker crashes in Gentoo, Sabayon, Kubuntu, openSUSE, Arch, etc.\n\nI just restart kicker and move on without thinking too much about it."
    author: "T. J. Brumfield"
  - subject: "Re: kicker"
    date: 2008-08-27
    body: "I don't have any crashes of Kicker in Gentoo for ages."
    author: "KejPi"
  - subject: "Re: kicker"
    date: 2008-08-27
    body: "The new kicker crashed for me today on Kubuntu 8.04 when trying to launch pidgin. \n\nKicker crash is an infrequent problem and it is not much of a issue as kicker restarts automatically and tray icons for KDE apps recover without any problems. But none-KDE apps like pidgin and azureus get a new window on the desktop and a corresponding taskbar entry for their tray icon. \n\nThe bug report for this crash is on http://bugs.kde.org/show_bug.cgi?id=133386\n\nI ended up triggering the bug as i was trying to get kicker to display tray icons in two rows like in 3.5.9. Pidgin with 3.5.9 has a known issue which causes a single row display, removing the 48x48 pidgin tray icons and creating a link to the 32x32 icons used to fix this. \n\nI'm not able to get the new system tray to display two rows even with only KMix, Klipper, KNetwork Manager and KBluetooth. Is anyone else facing the same problem after upgrading? Can anyone help?"
    author: "Naveen"
  - subject: "Re: kicker"
    date: 2008-08-29
    body: "The system tray layout algorithm was fixed in KDE 3.5.10, but not entirely. There were bad patches on the bug, and I didn't remove all the patches while removing the bug. Now I think I have finally fixed everything, but you will get it only in a next release.\n\nAs for pidgin: pidgin does not specify any preferred size into its icon (I don't know why, as this is done automatically in GTK+). So you get an arbitrary size. \n\nI committed a patch recently to constraint icons to not be bigger than the default system tray icon size. Fortunately, pidgin scales its icon according to its tray window size.\n\nAs for the crash in kicker, you should give more details about the configuration (see the bug entry in bugs.kde.org).\n"
    author: "Benoit Minisini"
  - subject: "Re: kicker"
    date: 2008-09-03
    body: "Thanks for the reply, i have updated the bug entry with additional details, however I'm not able to reproduce or get any additional details about the crash right now. I will update the bug report when i get more information."
    author: "Naveen"
  - subject: "It's a great news ..."
    date: 2008-08-26
    body: "for people who cannot afford upgrading their computers since KDE 3.5.x runs just fine when you have 256MB of RAM (and there are plenty of such PCs).\n\nKDE 4.1.0 is all shiny and sexy but I cannot imagine running it smoothly on a PC with less than 768MB of RAM.\n\nSo, thank you very much, KDE team, for your continued support of KDE 3.5.x series.\n\nI only wonder since release notes lack any sings of EOL, does that mean that KDE 3.5.11 will follow? When KDE 4.0 was out I predicted KDE 3.5.10 would be released and it happened so, but now KDE 4.x is maturing and I feel like it's time to say good bye to our old friend who served us well.\n\nQt 3.x is no longer maintained and I'd like hear the answer from Aaron Seigo or any other major KDE developer who is responsible for releases."
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "\"KDE 4.1.0 is all shiny and sexy but I cannot imagine running it smoothly on a PC with less than 768MB of RAM.\"\n\nWhich aspect of KDE4.1 do you find consumes the most memory?"
    author: "Anon"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "Double buffered Qt4.  It's pretty well known, and nothing can really be done about it.   Such is the price of advancements in the toolkit."
    author: "Leo S"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "Hello!\n\nDouble buffering is only needed for parts of the screen that are visible and actually change, which is for nearly nothing.\n\nIf what you say is true for Qt, I don't think it's a universal truth that double buffering alone should increase the memory usage noteworthy.\n\nAnd for all I remember, is how we were told that Qt4 has actually reduced the memory usage over Qt3. And there were people to benchmark it.\n\nSo I guess, something else would need to be blamed. My guess would be lack of optimization.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "\"Double buffering is only needed for parts of the screen that are visible and actually change, which is for nearly nothing.\"\n\nThat might be what is *needed*, but it's not how Qt4 operates: every widget in every window, whether it's visible or not, permanently has a double-buffer.  Since most apps basically have their whole windows covered in widgets, this is a large amount of memory consumed.\n\n\"And for all I remember, is how we were told that Qt4 has actually reduced the memory usage over Qt3. And there were people to benchmark it.\"\n\nNone of the benchmarks I saw counted x-server usage."
    author: "Anon"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Hello,\n\nthat combined with the blog, how Qt always wants to convert everything to 32 at one point bits, even if the source and dest are both 16 bits, thus painting with low FPS. It makes me wonder why people seriously consider Qt well suited for mobile devices.\n\nBut who knows, maybe they can refactor it one day to something sane. The useless conversions will go in 4.5 I read.\n\nAnd to Nokia it sure is of interest. You know, like how big a phone do you need, 2G RAM? or 256MB, that's cost.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "There is more in the world than Qt *Desktop* Edition."
    author: "Andras Mantia"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "\"Since most apps basically have their whole windows covered in widgets, this is a large amount of memory consumed.\"\n\nQt 4.4 has alien widgets: Only the windows are real X clients, the other widgets are merely data structures who paint on the window's buffer."
    author: "Stefan Majewsky"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "You did render the argumentation from Anon invalid."
    author: "dave"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "I have already tried a number of Linux LiveCDs with KDE 4.x onboard and running them in a virtual machine with 512MB RAM is a pain. You can do that on your own if you don't trust my words.\n\n(My own PC now contains 4GB of RAM so I don't much care - but) Sometimes I come to people who have quite old PCs and I wanna boot off a Linux LiveCD and KDE 4.x is not an option if your PC doesn't have enough RAM.\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "I asked \"which aspect\"; I didn't say I didn't believe you.\n\nSomething in KDE4 is apparently taking more memory than in KDE3 - the question is, what, and why, and what can be done to fix it?"
    author: "Anon"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "Obviously it sounds like your approach with liveCDs have issues, but it's not KDE 4 memory usage. And live CDs have never been a good option when not having enough RAM, or for running on slow machines for that matter.\n\nI'm currently posting this from one of those old PCs you mentioned, using a distribution known to be heavy without any tweaks, currently peaking at about 161M of physical memory used by applications. The rest of the 256M are used for buffer and cache. \n\nThe machine is not fast, but that is not caused by the lack of RAM(As long as I keep a reasonable amount of applications open). Easily deducted from the fact that no significant amount of swap is used. The most noticeable thing more RAM would give are faster restart time of applications, but that's only caused bye more RAM available for disk cache. And it does not make much sense to use as a significant measure. Since anything close to realistic usage is about working with the applications, not stopping and starting them."
    author: "Morty"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "++\n\nLiveCD's are not a good measure of performance on low-memory systems because loading the LiveCD into a ram disk kills almost all your memory."
    author: "T. J. Brumfield"
  - subject: "Re: It's a great news ..."
    date: 2008-08-29
    body: "> The rest of the 256M are used for buffer and cache.\n\nWhen not having any/enough memory for buffers and cache the system has to access the disk constantly working becomes a PITA. So, saying it uses only so and so much physical memory is only half of the picture.\n\nExpecting KDE 4 (or any other desktop built for the next five to fifteen years) to run on a 256 MB system limits the development of a service-rich desktop framework to an untolerable degree, imho. On the other hand I don't understand the generally poor approach towards KDE 3.5 maintenance of the KDE developer community. It's a pity. I'm grateful for any guy like Benoit Minisini, who make a difference."
    author: "Carlo"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "`Top` says that every KDE4 application eats on average 5-10 MB more RAM  (looking at the RSS field) than their KDE3 counterpart and plasma is the greatest offender (over 50MB RSS), so definitely there's a room for optimizations.\n\nAs for infeasibility of estimating KDE4 RAM usage using LiveCD: I don't run LiveCDs to really account anything - 1) I run them out of necessity 2) I've tested LiveCDs in a virtual machine with ISO image connected as a CD drive - so there's an issue of the increased memory consumption as CD access time doesn't account for anything (ISO image is saved on a HDD).\n\nSo, let me reiterate - what I wanted to say is that KDE4 is not very usable when running on PCs with less than 768MB of RAM. It's not a big deal, but a statement of fact.\n\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "> So, let me reiterate - what I wanted to say is that KDE4 is not very usable when running on PCs with less than 768MB of RAM. It's not a big deal, but a statement of fact.\n\nIt's not a fact.  It is just based on your own experience."
    author: "Juan Miguel"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "And others, me included :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "Runs fine on my laptop, a Thinkpad T42 with 512MB of memory.  (And, OT, the integrated ATI card is so much better than the nVidia card in my desktop machine! Though I'm not yet using the new beta nVidia drivers which I hear make things much better.)"
    author: "Adrian Baugh"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Using top for measuring memory isn't exactly a smart idea:\n\nhttp://www.kdedevelopers.org/node/1445\nhttp://www.kdedevelopers.org/node/1567\n\nAnd your claims that KDE 4 isn't \"usable\" on machines that have *roll a dice* MB aren't exactly as representative as you claim them to be: \n\n- I've installed KDE 4 on a Toshiba Satellite 2430 w/ 512 MB of RAM and it runs smoothly. \n- If you search the internet you'll see that KDE 4 is reported to run smoothly on an Asus EeePC (which has \"just\" 512 MB of RAM). I had installed KDE 4 on my Asus Eee myself, so I can confirm this.\n\n"
    author: "Pumpkin"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "KDE4 alone with X.org server perfectly fits into 512MB or even less RAM, but then try to run a handful of applications and services, and you'll experience what I'm talking about.\n\nGod!\n\nCan you, please, download any of Fedora 9 or/and Mandriva 2009 One KDE4 Live CDs and run them on computers with 512MB RAM or less. This is SO easy to check out and you won't blame me for making unfounded statements!\n\nUntil you do that there's nothing to argue about or discuss."
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Sure... As long as you start a bunch of qt3 and gtk apps, the memory use is going to get higher.\n\nIMHO people tends to consider that kde4 uses more memory because they run a lot of kde3 apps. And loading qt4 + qt3 (worse, kdelibs3 and kdelibs4) is, and there's nothing that can be done about that, more memory consuming than just loading qt3/kde3.\n\nJust give the time to port apps like amarok/k3b to kde4, and memory use of \"kde4\" will decrease greatly :)."
    author: "Julien Blanc"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "\"Can you, please, download any of Fedora 9 or/and Mandriva 2009 One KDE4 Live CDs and run them on computers with 512MB RAM or less. This is SO easy to check out and you won't blame me for making unfounded statements!\"\n\nLike it has already been said: LiveCD:s suck for measuring performance. I have tested KDE4 through LiveCD on my MacBook Pro. It has 2.4GHz dual-core CPU and 2 GB of RAM. And guess what? It ran slow. For starters, it consumes a lot of RAM to load the contents of the CD to RAM. Seconduly, since not everything is loaded to RAM, it occasionally fetches stuff from the CD; and that is REALLY slow.\n\nI tried the LiveCD:s to see the functionality and features. Never in million years did I imagine that they would give me accurate idea what the performance is like."
    author: "Janne"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "LiveCD image stored on your HDD gives you roughly the same performance as a usual system installed on your HDD.\n\nLiveCD's may suck in measuring performance but I can clearly see how much RAM they need to run.\n\n\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "\"LiveCD image stored on your HDD gives you roughly the same performance as a usual system installed on your HDD.\"\n\nUm, then you are not talking about LiveCD anymore. If the system is installed on the HD, then it's not running from the HD, and therefore it's not a LiveCD anymore.\n\n\"LiveCD's may suck in measuring performance but I can clearly see how much RAM they need to run.\"\n\nLiveCD's do not give you accurate information regarding memory-consumption or speed."
    author: "Janne"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Well, I'm not in a mood of arguing with you :-)\n\nIt seems like you disregard my arguments without proving anything - I have facts - you have superficial opinions.\n\nI've been a techie for the last fifteen years and I can tell you for sure, that running LiveCD Linux image gives you a very good impression about its environment speed and characteristics - you CAN compare any KDE3.x based LiveCD distro with any KDE4.x based LiveCD distro. The former will run just fine if you allocate just 256MB of RAM for it, the latter will not be running fine until you give it 768MB of RAM. Once again, I'm talking about running ISO images stored on your HDD.\n\nIf you DO that that come back and give me more reasoning :-)"
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "I think you are absolutely wrong here.  You state that you have facts, and dissenting people have opinions.  That alone counts against you right there.  You're saying exactly opposite things.  One person isn't commenting on objective matter while the other is commenting on subjective matter.\n\nFirst off, we are talking about something objective, and easily verifiable.  Do LiveCDs represent real world memory usage?  No, they do not.\n\nLive CDs have to run off of memory.  Different live CDs do this through a variety of different means.  Sabayon live CDs load through QEMU for some crazy reason.  Some will attempt to see swap and use it.  Some load a huge image into memory to have more apps, while others load less into memory to have more room for running those apps.  Some Live CDs use different file systems, different forms of compression, etc.\n\nPeople compile binaries differently as well.\n\nRegardless, in the simplest terms, if you want to compare KDE 3 versus KDE 4 for performance, you'd have to attempt to compile them both along very similar lines, even though QT and the underlying libraries are quite different.  You'd need to compare a pure KDE 3/QT 3 environment with the same apps/services running against a pure KDE 4/QT 4 environment with the same apps/services running.  And you'd want them to run natively, installed properly on the drive, not off some live CD.\n\nRunning the live CD itself affects memory usage itself and distorts your results."
    author: "T. J. Brumfield"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "<I>Do LiveCDs represent real world memory usage? No, they do not.</I>\n\nLive CD's are useful by their own. So I can't see why you have decided they are an inferior way to compare KDE 3 and KDE 4.\n"
    author: "maximegb"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "livecds are compressing data and uncompressing is memory overhead. basic know how.\n"
    author: "dave"
  - subject: "Re: It's a great news ..."
    date: 2008-08-29
    body: "Live CD's are useful for a great deal many things.  Don't get me wrong.  However, saying that KDE 4 can't be run on a system with less than 768 megs of memory because a Live CD was slow on a 512 box isn't a fair statement."
    author: "T. J. Brumfield"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "\"It seems like you disregard my arguments without proving anything - I have facts - you have superficial opinions.\"\n\nWhat are my \"opinions\" then? Just about only thing I have said is that \"you can't use LiveCD to measure performance\". I have made no claims regarding performance of KDE4.\n\nSo you have facts? Where are they? All I have seen is your OPINION that KDE4 has poor performance. I have seen no facts anywhere.\n\n\"I've been a techie for the last fifteen years\"\n\nI have been a techie since I have been about 6 years old. That was 24 years ago. Do I win a prize?\n\n\"The former will run just fine if you allocate just 256MB of RAM for it, the latter will not be running fine until you give it 768MB of RAM.\"\n\nBut the thing is that I have seen people run KDE4 on Nokia's tablets. I have seen KDE run on OpenMoko. I have seen KDE4 run on computers with 512MB of RAM. And while the former two might be a bit on the slow side (what do you expect, really?), the latter machine seemed to work just fine.\n\n\"Once again, I'm talking about running ISO images stored on your HDD.\"\n\nBut that's not LiveCD. LiveCD runs on the CD, it does not touch your HD at all."
    author: "Janne"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "Yes you can see how much RAM <B>a</B> liveCD need to run, but it's not comparable to real installation or in many cases even to other liveCDs.\n\nRunning the liveCD image from a cd, harddrive or punchcards do not really make any difference, by nature a liveCD need way more RAM than a proper installation. \n\nAs they are designed to run from a readonly media, to achieve proper functionality they allocate a part of the physical memory to use as ramdisk to hold the parts of the filesystem where read/write access are required. Since the size of the ramdisk are dictated by the strategy chosen by of the distribution and the whim of the developers, it will wary between different distributions."
    author: "Morty"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Smootly isn't the correct word for KDE4 on my eeePC.\nIt runs, much MUCH MUCH slower/fatter than KDE3.\n\nI know some people who did the comparsion also and they have all the same results, using ubuntuEEE. Maybe your distro or KDE is very well optimized?"
    author: "Iuri Fiedoruk"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "I'm running openSUSE here."
    author: "Pumpkin"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "It runs slower and fatter on my 1.5G RAM, dual core Thinkpad T60 as well. It's very frustrating to have a three year old laptop treated as \"legacy\" by KDE."
    author: "David Johnson"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "David, who, outside the dot.kde.org peanut gallery, told you your T60 was considered legacy by the KDE developers?"
    author: "boudewijn rempt"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "I have 4.1 on my Eee 900 and I don't see that much of a performance hit compared to KDE 3."
    author: "Luca Beltrame"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "chances are it has nothing to do with memory consumption or cpu cycles otherwise used and everything to do with things like graphics card drivers.\n\nwhen you and others say, \"kde4 runs slow for me\" i don't doubt you. there are known hardware configurations out there that cause problems due to driver related issues, for instance. there have also been issues related to performance in the newer Qt4 technologies which have been worked on (and continue to be improved still).\n\n but, as if by magic, when these problem items are upgraded underneath KDE4, things start to work and flow a lot nicer. that there are others, in this very thread even, whose experience is quite good out of the box says a lot ...\n\nso .. are there issues on some configurations? yes. is there something KDE can do about it? yes: track down the issues, report them upstream, let people know about them so they know what to expect.\n\nfor the large part, however, even with the newness of some parts of KDE 4's codebase .. much of the codebase hasn't changed much, improvements in performance have been made (KConfig in 4.1, for instance) and our biggest challenges currently lie with upstream projects."
    author: "Aaron Seigo"
  - subject: "Re: It's a great news ..."
    date: 2008-08-29
    body: "Hello Aaron,\n\nduring the time I used KDE 4.1, I felt that KDE 4.x felt indeed much less snappy that KDE 3.5 and that's what I blame about it:\n\na) Animations. I don't have these under 3.5 for menus, menu bar, etc, but it was not as easy to disable them for 4.1. Am I right with that? Anyway, I think the animations somehow made it feel slower.\n\nb) The double buffering is what I blame most for the perceived loss of performance. When I open kcontrol in 3.5 right now and switch panes, it flickers. Flicker may be bad and stuff, but it's an _immediate_ response and lasting very short time only. \n\nThat so much that I stoped to perceive it long ago as such. With the double buffering it appears that only when the rendering is done, I will see the result and lack the immediate and direct feedback. So I click and get a feedback like 2ms later or more, that's noticable, is it?  I am not saying that I won't be used to that in a few years. I am not now.\n\n-----\n\nOtherwise, start times, Dolphin vs. Konqueror file views, previews, etc. did all feel faster, that's right. I think give back flicker and stop animations and you will get people to say that it's fast. The last one is probably something that _should_ be an option.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "easy. Loading all the KDE3 libs because not aps have been ported yet."
    author: "AC"
  - subject: "Re: It's a great news ..."
    date: 2008-08-26
    body: "KDE 4 live cd's work fine on my laptop with 256MB of RAM."
    author: "harold"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "I tried Fedora 9 LiveCD and Mandriva One 2009 beta2 LiveCDs - It looks like you tried something else, cause with my distros KDE4 is hardly usable even with 512MB of RAM."
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Sounds to me like it is time to switch distros then. If others report KDE 4 runs fine with their low-powered system, and yours doesn't, I would seriously doubt it to be a problem with KDE 4 itself. Note that it still *could* be, but extraordinary claims require extraordinary evidence..."
    author: "Andr\u00e9"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Well, I have given the instructions how to check out my statements about memory consumption and you still insist that I'm just \"claiming\" something and the world may be different for you.\n\nWhy am I still arguing?\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Livecd's take a lot of ram by themselves already, even without KDE 4.x. I have an Acer Aspire one with 512 mb ram, KDE 4.1 (Kubuntu) runs pretty one."
    author: "jos poortvliet"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Just the desktop, or desktop + apps?"
    author: "Anon"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "desktop+kmail+konqueror+openoffice on 1.5 singlecore+512m ram+no desktop-effects+suse."
    author: "dave"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "\"KDE 4.1.0 is all shiny and sexy but I cannot imagine running it smoothly on a PC with less than 768MB of RAM.\"\n\n4.1 absolutely flies on my Asus eee 701 with 512Mb and underclocked 600Mhz cpu, and that's _with_ compositing switched on.  The \"wows\" I get out of people when they see Cover Flow task switching with live preview..."
    author: "odysseus"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "My eeePC must have some defect...\nWhat distro are you using?"
    author: "Iuri Fiedoruk"
  - subject: "Re: It's a great news ..."
    date: 2008-09-10
    body: "LFS :-)"
    author: "Artem S. Tashkinov"
  - subject: "Re: It's a great news ..."
    date: 2008-08-27
    body: "Scream if you've already been asked this, but what type of graphics card do you use. It's well known that nVidia cards' 2D performance sucks, particularly since KDE4 uses 2D hardware acceleration."
    author: "Michael \"nVidia\" Howell"
  - subject: "Re: It's a great news ..."
    date: 2008-08-28
    body: "\"KDE 4.1.0 is all shiny and sexy but I cannot imagine running it smoothly on a PC with less than 768MB of RAM.\"\n\nI saw it running smoothly on a 6-year old notebook with 256 MB RAM. The only feature which cannot be used is compositing."
    author: "Stefan Majewsky"
  - subject: "Thanks to KDE!"
    date: 2008-08-29
    body: "<b>I only wonder since release notes lack any sings of EOL, does that mean that KDE 3.5.11 will follow? When KDE 4.0 was out I predicted KDE 3.5.10 would be released and it happened so, but now KDE 4.x is maturing and I feel like it's time to say good bye to our old friend who served us well.</b>\n\nOut of my cold dead hand.\n\nYou know, I really wanted to like KDE4.1, and I briefly convinced myself that I did, but I lost interest after two or three weeks of use.  It's just not as flexible and powerful, though it may be one day soon.   I usually run KDE applications from fluxbox, but I'm running KDE3 right now, and after running KDE4.1 for a few weeks, KDE3 never felt so straightforward and stable.  I know KDE4 has potential, but \"potential\" is not a good reason to surrender the most advanced and functional expression of the desktop as we've known it for the past 25 years or so.  KDE4 is important work, and the job of developers is development.  But users have to use, and KDE3 is more useful.  \"Maintanance mode\" is just fine, security updates are all that is really called for, but KDE4.1 is not as useful to me.   Thanks to KDE for taking care of my needs.  I think I owe you a modest contribution.  \n\n\n\n"
    author: "blackbelt_jones"
  - subject: "Re: Thanks to KDE!"
    date: 2008-08-31
    body: "Since you admit you are not really a user of the desktop in KDE anyway, your insistence on KDE3 are a rather pointless. \n\nAs you say you are usually running KDE applications from fluxbox, so upgrading to KDE4 makes more sense for you than most. Just continue as you where before, then you don't get affected by any changes to the desktop made by the switch to Plasma as some users have problems with. But you get access to across the board improvement made to as good as every application ported to KDE4."
    author: "Morty"
  - subject: "Yay, my bug is fixed in it!"
    date: 2008-08-26
    body: "Yay!\n\nI reported a bug and it's fixed in this update :) My fist ever bug report to be fixed and released!  I gotta drink on this one! Now only if 3.5.x choppy word-wrapped scrolling would be fixed... but we can't have everything! ;)"
    author: "Phd student"
  - subject: "3.5.10 in Ubuntu"
    date: 2008-08-26
    body: "Sorry if it's a dumb question, but I can't figure out how to install it in Ubuntu.\nKubuntu's instructions (http://www.kubuntu.org/news/kde-3.5.10) say:\n1. Launch Adept\n2. In Software Repositories enable Unsupported updates in Updates.\n...\n\nBut there's no \"Software Repositories\" in Adept. There's \"Adept\"->\"Manage Repositories\", but it just does \"apt-get update\". I'm sure I've seen the repository editor before, though. What am I missing?\n\nI'd rather just edit /etc/apt/sources.list myself, though. Does anyone know what I should add?"
    author: "Dima"
  - subject: "Re: 3.5.10 in Ubuntu"
    date: 2008-08-26
    body: "Adept -> Manage Repositories -> Update tab -> Check the Unsupported Updates box, fetch updates, voila."
    author: "Aaron"
  - subject: "Re: 3.5.10 in Ubuntu"
    date: 2008-08-26
    body: "Well, as I said, when I click \"Manage Repositories\", it just updates everything - same as if I click \"Fetch Updates\". There's no \"Update tab\" anywhere.\nIs my Adept broken?\n"
    author: "Dima"
  - subject: "Re: 3.5.10 in Ubuntu"
    date: 2008-08-26
    body: "When I hit Adept -> Manage Repositories I get the dialog in the screenshot I'm attaching. I've highlighted the update tab and and Unsupported Updates checkbox. I haven't done anything funny to Adept (that I can remember) so theoretically we should have the same dialog (You're using kubuntu 8.04?). About Adept says I'm using \"Adept Manager 2.1 Cruiser.\""
    author: "Aaron"
  - subject: "Re: 3.5.10 in Ubuntu"
    date: 2008-08-26
    body: "Here's the screenshot I forgot to attach. Sorry :-("
    author: "Aaron"
  - subject: "Re: 3.5.10 in Ubuntu"
    date: 2008-08-27
    body: "I don't get the dialog box at all... But if I run adept_manager from the terminal, I see this:\n/usr/lib/python2.5/site-packages/apt/__init__.py:18: FutureWarning: apt API not stable yet\n  warnings.warn(\"apt API not stable yet\", FutureWarning)\nTraceback (most recent call last):\n  File \"/usr/bin/software-properties-kde\", line 34, in <module>\n    from softwareproperties.kde.SoftwarePropertiesKDE import SoftwarePropertiesKDE\n  File \"/usr/lib/python2.5/site-packages/softwareproperties/kde/SoftwarePropertiesKDE.py\", line 36, in <module>\n    from PyQt4.QtCore import *\nRuntimeError: the sip module supports API v3.0 to v3.6 but the PyQt4.QtCore module requires API v3.7\n\nNot sure what Qt4 has to do with adept.\n"
    author: "Dima"
  - subject: "Re: 3.5.10 in Ubuntu"
    date: 2008-08-27
    body: "software-properties-kde is written in PyQt4."
    author: "Jonathan Thomas"
  - subject: "Kommander from kdewebdev doesn't compile :-("
    date: 2008-08-27
    body: "At the linking stage of kmdr-editor I get this error:\n\nhttp://pastebin.ca/1186020\n\n--enable-final is NOT enabled.\n\nFedora 9/gcc 4.2.4\n\nSad ..."
    author: "Artem S. Tashkinov"
  - subject: "Re: Kommander from kdewebdev doesn't compile :-("
    date: 2008-08-27
    body: "--enable-visibility causes this error."
    author: "Artem S. Tashkinov"
  - subject: "Re: Kommander from kdewebdev doesn't compile :-("
    date: 2008-08-27
    body: "I did not realize this release was on the way and there are some improvements in Kommander for this branch. They were mostly personal, but had I realized I might have added more. Anyway Kommander 3.5.10 is worth looking at."
    author: "Eric Laffoon"
  - subject: "Thanks a lot"
    date: 2008-08-27
    body: "As for me and a lot of colleagues kde4 doesn't fit our needs - no i'm not \ngoing to list the features i'm missing or things that went in a wrong direction\n(don't want to get my post removed) - thanks a lot for maintaining\nthe KDE3 branch. I really enjoy to work with.\nGeared to professional work, but highly customizable though, it's an ideal\nplatform to get things done."
    author: "konqueror"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "This attitude will ensure that KDE 4 never meets your needs.  Drop the negativity, use bugzilla to make sure there are reports for the missing features, be patient, and you'll make KDE 4 better for you and for others."
    author: "Will Stephenson"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "No, his point is \"Don't fix it if it works\"."
    author: "Andreas"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "No, his point is that he prefers KDE 3.5 to KDE 4.1"
    author: "ad"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "It is never the point of negativity. Imagine this, you're working on some let say code file, editing for example, using kate. After you extracted the point of error, and successfully eliminated impossibilities, you save your code, pressing CTRL+S, but somehow in the same time KDE Nepomouk, or whatever is spelling here, emits some garbage over D-bus, your plasma crashes, at the same time the child process crashes too, kate dies, all your work is annihilated. Will you write bugzilla report? Say yes and you're qualify in the same category as the guy who actually invented that D-bus garbage, the category is amateurism.  "
    author: "doc_ds"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "Did that actually happen or are you just speculating? It sounds rather unlikely to me."
    author: "Paul Eggleton"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "The implication that Plasma crashing would crash Kate as it is a child process (at least, I think that's what is implied: coherency is not the poster's strong suit :)) suggestions that it is a fictitious scenario concocted by someone who doesn't really know what they are talking about.\n\nI'm very surprised to see such a thing on the Dot!!"
    author: "Anon"
  - subject: "Re: Thanks a lot"
    date: 2008-08-28
    body: "So... let met get that strait...\nYou are saying that if some catastrophic bug somewhere in the base system triggers a crash in your application, and you take the time to report such a bug, you are an amateur? \nThat statement does not make a lot of sense to me. First off: an amateur in *what*? Second, I don't get why reporting a bug, grave or not, would reduce the reporter to amateurism. Third, I don't get why the inventor of D-bus is qualified in this way. D-bus is the successor to, and heavily based on, D-cop. It seems you quite happily use this (it is everywhere in KDE 3). \nThe unlikelyness your story (Nepomuk crashing plasma via D-bus, where kate is a clild process of plasma) I won't even go in to. "
    author: "Andr\u00e9"
  - subject: "Re: Thanks a lot"
    date: 2008-08-28
    body: "\"Nepomuk crashing plasma via D-bus, where kate is a clild process of plasma\"\n\nwas that meaned serious?\n"
    author: "dave"
  - subject: "Re: Thanks a lot"
    date: 2008-08-28
    body: "Almost all criticism of KDE4 is brushed-off with the standard \"Stop whining and do X\" reply, where X escalates according to the complaint (1. open a bug report 2. Be more verbose 3. Fix the bug yourself 4. Write your own damn DE, etc.).\n\nA few months ago I tried KDE4, and found out I could not use my dual-screen setup, which works with all other DEs and WMs I've tried (including KDE3). Like a good boy, I reported the problem (actually added to an existing bug report). No response. I then complied KDE from source, and submitted the relevant debugging messages, hoping it would facilitate in solving the problem. No response. I studied the code and submitted an initial patch that enabled the second screen. No response. In the meantime, the codebase has changed, and the patch no longer applies. Since option 4 in the escalating response sequence is unacceptable, I have to stick with KDE 3.5. How's that for negativity?"
    author: "Elad Lahav"
  - subject: "Re: Thanks a lot"
    date: 2008-08-28
    body: "link?"
    author: "dave"
  - subject: "Re: Thanks a lot"
    date: 2008-08-28
    body: "http://bugs.kde.org/show_bug.cgi?id=153581"
    author: "Anon"
  - subject: "Re: Thanks a lot"
    date: 2008-08-31
    body: "I don't like the silence you were met with any more than you do, though if your messages sounded like complaints, then that's no good.\n\nI'm sure this is a common question, but were you using KDE4.0 or KDE4.1. It would explain why the patch would not apply.\n\nAdding to the bug report: Did you add any useful information? If it was just a \"Me Too\", then no response is needed.\nDebug output: Did it actually crash? If so, I wouldn't be surprised if someone already uploaded it.\nPatch: How did it function. If it's really mostly a workaround, then not accepting makes sense. If it applied with KDE4.0, that would also make sense. Just silence, again, is surprising and disappointing.\n\nAlso, the escalation: does it escalate depending on the emotion in the complain (\"KDE4 SUCKS!\" met with the \"Make your own DE\") or the serverity of the problem (\"KDE4 performance is very poor on nVidia cards\" met with the \"Make your own DE\"). The former makes sense, the latter is surprising.\n"
    author: "Michael \"silence\" Howell"
  - subject: "Re: Thanks a lot"
    date: 2008-09-01
    body: "1. I was using updated SVN versions (from the 4.1 branch), in order to keep up with the latest development.\n2. No, it wasn't a \"me too\" addition.\n3. There was no prior debug output.\n4. The patch became invalid during development of the 4.1 branch. It's bad enough that the source is not documented, but changing infrastructure all of a sudden makes it quite difficult for people outside the core KDE development community to contribute.\n\nYou can look at the bug and my comments on bugs.kde.org. The bug number is 153581.\n"
    author: "Elad Lahav"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "Are you accusing maintainers of removing posts? Can you prove that?"
    author: "Anon"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "I've seen at least one post get deleted, but that particular post was a pure assault on one of the developers.  I've not seen any standard, inoffensive \"KDE is not perfect\" posts get removed, though."
    author: "Anon"
  - subject: "Re: Thanks a lot"
    date: 2008-08-27
    body: "yeah. It does happen, but only when the discussion gets really bad (eg Holocaust references and such)."
    author: "jos poortvliet"
  - subject: "Re: Thanks a lot"
    date: 2008-08-30
    body: "thats wrong, it usualy gets deleted for much less. I'm not flaming, and i think it's ok to delete some posts, just saying that much less than holocaust things  have to be said that it happens. Perhaps a official policy should be made public?"
    author: "Beat Wolf"
  - subject: "KDE and KDE, perspective."
    date: 2008-08-27
    body: "You know, the problem here (as often) is an either or (all or nothing) type thinking. In other words, both (dominate views) about KDE4.x are completely valid. I'm not saying everything can be both ways. I'm not saying it's good to \"ride the fence\" or to be \"luke warm\". I am saying this appears to be a situation where both are (by in large) correct.\n\nOn the one \"side\", we have people who are saying, do not stop KDE 3.x until we like KDE 4.x better. People are tired of getting \"new\" software system upgrades that they have not approved(see closed, outdated and SLOW endeavors such as Vista). People do not want the choice of left behind systems or only unfinished systems. Sound familiar? This side understands, stability and compatibility are paramount.\n\nThe other side is also \"right\". We can not be too afraid of change in our development. If we do, we'll always be stuck with legacy baggage. This also sounds familiar, doesn't it?  Obviously, this requires stability and compatibility from the new KDE4.x and this is in fact what's promised and in process. Understanding here, that KDE is very large, complex and foundations are (very) important. The pillars and foundation are laid and from VERY hard work indeed. Still, this is not finished and it's in process.\n\nThe major problem (I think) could be, we have far more people sampling the development (Alpha RC or whatever) work BECAUSE open software is so much easier to install today. This is not a bad thing. It's a different thing. Perhaps (even more) extreme clarity about the unfinished nature should be trumpeted.\n\nTherefore please understand, by in large, YOU ARE BOTH RIGHT! Not \"either\", \"or\". The problem is fear. Fear of the unknown. Perhaps we need more evidence that things will get done; in due time.\n\nDevelopers have a great challenge. Fix it good! Plus, (without fail) make users FEEL good about the change. It might seem silly but it's critical at this stage. This is a new challenge to open development, It's had been falsely considered unnecessary. Obviously, I suggest we rethink the effort here. A PR (public relations) team sounds like a good idea to me. Being open to almost never ending volunteers, this need not burden (or slow) coders or testers, accept to coordinate. Collaboration is our strength. Let's apply it to (more) PR as well.\n\nUsers have a great challenge too. You are right. Yet do please, have a little faith (Evidence of things not seen). Don't forget. Development goes faster with open software, when compared to close software travesties. KDE 4 is not another Vista and I think this is what's on the minds of KDE 3 fans.\n\nOn a positive note. In a way, this is a nice problem to have. KDE is that good. Users are that many! Allot is at stake. Let's not fail, just because we might fail to see (by in large) both groups are generally correct. \n\nUnite! Both parties need to concede to some of the others wishes. It's not a compromise but it's synergistic.\n\nIs this making any sense, to anyone, or is it just me?"
    author: "Spanky"
  - subject: "Re: KDE and KDE, perspective."
    date: 2008-08-27
    body: "makes all sorts of sense to me ..."
    author: "Aaron Seigo"
  - subject: "Re: KDE and KDE, perspective."
    date: 2008-08-28
    body: "I think it is great that KDE 4 got such a massive rewrite, and not just the porting to QT 4.  I think things should be revisited and redesigned when the need arises.  I'm glad that KDE 4 is looking towards the future.  Many of the concepts in KDE 4 have me very excited.\n\nThat being said, I certainly don't always agree with the specific direction, nor do I find the current KDE 4 desktop a suitable replacement for _my_ KDE 3 desktop."
    author: "T. J. Brumfield"
  - subject: "Great"
    date: 2008-08-27
    body: "For those who just want things to work and have already spent a lot of time tuning their systems, this is great news. Hope it will be available for <A href=\"http://freebsd.kde.org/\">FreeBSD</A> soon... (Those who want the latest and greatest, don't mind occasional crashes/bugs, having a fairly new computer and a lot of free time to tune their systems again, will be more excited about 4.x.x releases.)\n\nThanks to the maintainers for their effort, I appreciate it!"
    author: "an excited user"
  - subject: "Re: Great"
    date: 2008-08-27
    body: "The FreeBSD ports are being worked on now, and should be done soon."
    author: "David Johnson"
  - subject: "Re: Great"
    date: 2008-08-27
    body: ":)"
    author: "an excited user"
  - subject: "Re: Great"
    date: 2008-08-30
    body: "Yes! 3.5.10 is now available for FreeBSD, time to compile..."
    author: "an excited user"
  - subject: "Upgrade"
    date: 2008-08-28
    body: "Is it possible to have both branches in parallel?"
    author: "KDE 4.1"
  - subject: "Re: Upgrade"
    date: 2008-08-28
    body: "KDE 3 and KDE 4, or KDE 3.5.9 and KDE 3.5.10?\n\nThe first you can do with certain distros (like openSUSE) and not others (like Fedora).  The former, you can only do if you compiled them both manually, and configured them both to operate in different locations."
    author: "T. J. Brumfield"
  - subject: "Re: Upgrade"
    date: 2008-08-28
    body: "By the way, how do I do this? I know there is a KDEHOME variable or such, but if I set this to ~/.kde4, wouldn't that also apply to KDE 3 apps? Is there a compile-time switch for this."
    author: "Stefan Majewsky"
  - subject: "3.5.10 brings a new bug"
    date: 2008-09-03
    body: "In dual monitor setup you cannot maximize a window on the second, larger screen. Strange."
    author: "eimi"
  - subject: "Re: 3.5.10 brings a new bug"
    date: 2008-09-08
    body: "I experience this same bug. Running Ubuntu Hardy 8.04 with dual monitors as separate X screens with nvidia driver.\n\nThe laptop display, X Screen 0, works fine. However on the second monitor, X Screen 1, windows will not maximise (maximize) to full screen. Windows appear to maximise to the same size as the smaller laptop screen. A non-maximised window can be dragged to fill the screen.\n\nI guess some bug is taking the dimensions from the wrong screen."
    author: "Dick Cheney"
---
The KDE community has finalised <a href="http://www.kde.org/announcements/announce-3.5.10.php">another update to the 3.5 series</a>. While not a very exciting release, 3.5.10 brings numerous bugfixes and translation updates to those who choose to stay with KDE 3.5. The <a href="http://www.kde.org/announcements/changelogs/changelog3_5_9to3_5_10.php">fixes</a> are thinly spread across KPDF with a number of crash fixes, KGPG and probably most interesting various fixes in Kicker, KDE 3's panel.



<!--break-->
<ul>
  <li>Improved visibility on transparent backgrounds</li>
  <li>Themed arrow buttons in applets that were missing them</li>
  <li>Layout and antialiasing fixes in various applets</li>
</ul>


<p>Note, as with every release, the changelog is not complete as our developers often forget to document their work. For users of KDE 3.5.9 it should be low-risk to upgrade to KDE 3.5.10 since the rules of what is to enter the KDE 3.5 branch are pretty strict at that point, it is in maintenance only mode.</p>

