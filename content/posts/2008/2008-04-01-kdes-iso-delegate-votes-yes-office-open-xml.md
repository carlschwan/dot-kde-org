---
title: "KDE's ISO Delegate Votes Yes to Office Open XML"
date:    2008-04-01
authors:
  - "jriddell"
slug:    kdes-iso-delegate-votes-yes-office-open-xml
comments:
  - subject: "Timestamp"
    date: 2008-04-01
    body: "Heh, the timestamp of the post still says March 31, though :)\n\nHad me for a second as I was wondering what the hell :)"
    author: "LiquidFire"
  - subject: "Cool!"
    date: 2008-04-01
    body: "Finally! Now I can open that .docx thingies.\n\nFor some more MS interoperability it would be cool if KDE could support DirectX 11 in Plasma:\n\nhttp://tech.slashdot.org/tech/08/03/31/1423247.shtml\n\nA raytraced sun dial on the desktop would rock!"
    author: "Lirpa"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "I think any usable directX 11 support for the coming 4-5 years is not feasible, seeing the progress on directX 9 and 10 at this moment in Wine."
    author: "JeanPaul"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "DirectX 9 support is pretty much complete. Well, not complete as in completely bug free, but all core functionality should be implemented.\nDirectX 10 on the other hand is completely stubbed... But hey, that was just released."
    author: "Jonathan Thomas"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "But why wait all that time for wine to catch up to directX11? We should demand raytraced thingies on plasma right NOW!!! even if it works only on Windows. That will motivate the linux hippies to improve their support to those innovative technologies."
    author: "Santiago"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "Uh, may I ask you 2 things:\n1) What is raytracing?\n2) DirectX is a Windows-specific technology, so by all rights, Linux has no business supporting it (Linux uses OpenGL which seems about >= DirectX). Wine-DirectX seems to be a loose-wrapper around OpenGL."
    author: "Riddle"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "1) Raytracing AFAIK means that a virtual ray is send from each point of your view. It is reflected and thereby manipulated by objects in its way until it hits a light source. Then the effects are added on the source's color to get the pixel to display at this point of your view. (The ray may also go the other way, I do not know exactly.)\n\n2) What you mean with \"wrapper around OpenGL\" is the Wine implementation of DirectDraw and Direct3D. There are more components like DirectPlay (comparable with GStreamer or Xine) and some other minor additions. DirectX is sort of a toolkit for Windows game programmers than just a rendering library."
    author: "Stefan Majewsky"
  - subject: "Re: Cool!"
    date: 2008-04-02
    body: "2) Wine HQ states that Wine DirectDraw (http://wiki.winehq.org/DirectDraw), if it can, uses OGL, and Wine Direct3D (http://wiki.winehq.org/WineD3D) is specifically described as \"Wraps up OpenGL to implement an interface for use by Direct3D 7, 8 and 9\". Also, (http://bugs.winehq.org/show_bug.cgi?id=6847) says that \"Wine does not implement any DirectPlay providers itself\". Others, such as DirectShow (http://wiki.winehq.org/DirectShow), seem to use the native decoder libraries. So, in general, Wine DirectX is simply a wrapper on top of native libraries.\n"
    author: "Riddle"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "To be fair, KDE has had direct X11 support since ages..."
    author: "luciano"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "Heh, not too nit-pick, but that should be \"has had direct X11 support *for* ages...\" I've seen loads of other people say \"since ages\". Why is that such a common mistake?\n\nHmm someone else obviously got annoyed by it!: http://www.usingenglish.com/poll/23.html"
    author: "Tim"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "I can only tell for germans, but the \"for\" from above would be in this context in german \"seit\" which is more similar to \"since\". The most common mistake from germans is \"become\"<->\"get\", so don't be sureprised if you are in a steakhouse and you heare something like \"when do I become my steak\", because in german \"bekommen\" means \"get\""
    author: "Hias"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "It happens when people translate their foreign language to English.\n\nIn some foreign languages it's common to use construct like that. When translated literally, it becomes \"since ages\" which is incorrect English. Likewise, in Dutch you're \"op school\" which is literally translated \"on school\", not \"at school\". These prefixes are a hard part to learn English.\n\nI think you can imagine now why people make these kind of mistakes. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "\"It happens when people translate their foreign language to English.\"\n\n...and vice versa.\nWhen they, the Germans (or is Germen? German's, Germen's?) tranlate \"We have a lot to do in 2008\" from English into German, they say \"Wir haben in 2008 viel zu tun\". But that's wrong, it should be translated to \"Wir haben 2008 viel zu tun.\" The same goes wit Apostrophes and Apostrophe's and Apo'strophe's, and with \"seid\"/\"seit\". And there is another one, wich I think is a failed translation. They say \"Wenn jemand physikalischen Zugang zu einem Computer hat, kann er damit alles m\u00f6gliche anstellen.\"\nBut that's wrong. It should read: \"Wenn jemand physischen Zugang zu einem Computer hat, kann er damit alles m\u00f6gliche anstellen.\"\nIn Germany we have clothing for \"Men's\", we have bakeries called \"Back Shops\" (and of course we have \"Back Shop's\" too) and we have \"Coffee To Go\" and so on. And \"googeln\" can be found in our dictionaries, as the verb for Google. \n"
    author: "reader"
  - subject: "Re: Cool!"
    date: 2008-04-02
    body: "Mad, the thing with the apostrophes is annoying to no end... especially when used with plural-s ... those f*ng german retards i have to live with!"
    author: "MarcG"
  - subject: "Re: Cool!"
    date: 2008-04-01
    body: "As if those .docx thingies had anything to do with Open XML ;-)\n\n--\nhttp://goodbye-microsoft.com/\n"
    author: "Robert Millan"
  - subject: "Aww man!!"
    date: 2008-04-01
    body: "You got me ;)\n\nI was like, what the hell are they thinking? Are they going crazy?? And since when Seigo became \"SUPREME\" leader ;)\n\nAbout 30 solid seconds of despair ;) Pheww...."
    author: "PaT"
  - subject: "Naming standards"
    date: 2008-04-01
    body: "\"KDE's Supreme Leader Alan Psycho\", surely?!"
    author: "Lord Hippo"
  - subject: "So wait,... wha?"
    date: 2008-04-01
    body: "So wait?\n\nIs this a joke?\n\nI thought the world was against Microsoft's Office Open XML. Wasn't it?\nI'm royally confused. I thought ODF was the open standard and Open XML wasn't. Did something change in the last month I stopped paying attention to the whole ODF vs. OOXML debate?\n\nWhy does KDE like OOXML now?\n\nMust be an April fool's joke early.."
    author: "Max"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-01
    body: "I think it's April in most of the Western Hemisphere now. This is obviously an April Fools joke."
    author: "Jonathan Thomas"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-01
    body: "assuming you want to discount all of the americas."
    author: "nobody"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-01
    body: "Most of the americas are fools all days, aren't they?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-01
    body: "Time to switch to Gnome! :D :D"
    author: "Bobby"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-01
    body: "What, and miss out on the OOXML functionality ? :p"
    author: "moltonel"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-04
    body: "It's supported through mono\n\nI am reading this on the 4th the the 1st a dim memory.\n\nThis scared the crap out of me."
    author: "JackieBrown"
  - subject: "Re: So wait,... wha?"
    date: 2008-04-01
    body: "How is it that people are confused by the fact that the whole world doesn't exist in their timezone /every year/ on April 1st. "
    author: "Ian Monroe"
  - subject: "Supreme Leader Aaron Seigo"
    date: 2008-04-01
    body: "They should have linked the Supreme Leader part to this http://farm2.static.flickr.com/1196/747230218_ce3f728204.jpg?v=0  :D"
    author: "Patcito"
  - subject: "Re: Supreme Leader Aaron Seigo"
    date: 2008-04-01
    body: "Agreed."
    author: "Ian Monroe"
  - subject: "Ha"
    date: 2008-04-01
    body: "You almost got me :P"
    author: "Paulo Cesar"
  - subject: "Re: Ha"
    date: 2008-04-02
    body: "Me too... but the part with the 10.000 $ donation made it obvious."
    author: "chris.w"
  - subject: "Re: Ha"
    date: 2008-04-02
    body: "And not \"founder ... Coolio ... changing nappies\"? :-)"
    author: "Sebastian Kuegler"
  - subject: "Re: Ha"
    date: 2008-04-02
    body: "Does that mean he was changing his own nappies, or someone elses?"
    author: "Phil"
  - subject: "the jokes on you!"
    date: 2008-04-01
    body: "happy april fools day everybody :)"
    author: "Vladislav"
  - subject: "AF1, of course..."
    date: 2008-04-01
    body: "For a moment I was shocked, that our beloved KDE Project can make such grave judgemental error.\n\nBut I was relieved after seeing the date on my plasma Clock.\n\n:-)\n\nRegards\nAnand"
    author: "Anand Vaidya"
  - subject: "Supremer"
    date: 2008-04-01
    body: "Bah, komrade Jonathan just missed the chance to name komrade Seigo our Supreme Kommander.\n\nHopefully our faithful debugger KG[d]B agents will correct this bug."
    author: "Bruno Laturner"
  - subject: "Cyrille Burger and Stephan Coolio?"
    date: 2008-04-01
    body: "Bah, I woke up this morning and saw this news, WTF?? But then I realized that this is 1st of April :D - Only the date in the dot.news is still 31 March.\n\nBtw \"Cyrille Burger\" and \"Stephan Coolio\"? But why Aaron Seigo's name is the only correct one, not became for example Aaron Siege? :)"
    author: "fred"
  - subject: "Re: Cyrille Burger and Stephan Coolio?"
    date: 2008-04-01
    body: "two possibilities:\n\n1) to make it seem just real enough to real you in at the start\n2) my name is already funny enough / a joke as it is.\n\nmy money's on #2 ;)"
    author: "Aaron Seigo"
  - subject: "Re: Cyrille Burger and Stephan Coolio?"
    date: 2008-04-01
    body: "Have you ever read the book Ninja?"
    author: "Bobby"
  - subject: "Re: Cyrille Burger and Stephan Coolio?"
    date: 2008-04-01
    body: "Maybe one more possibility?\n\n3) The author was afraid he would be kicked out from KDE e.v if he made fun of our Supreme Leader's name :D\n\njust kidding :)"
    author: "fred"
  - subject: "Re: Cyrille Burger and Stephan Coolio?"
    date: 2008-04-01
    body: "\"Aaron Siego\" would have been a good but subtle prank. :P"
    author: "Jucato"
  - subject: "Happy april fools"
    date: 2008-04-01
    body: "APRIL FOOLS!!!\n\n... and following a $10,000 donation from an anonymous NA source ...\nAnd who could that be! ;)\n"
    author: "Riddle"
  - subject: "Aha..."
    date: 2008-04-01
    body: "I just love this community. Rock on!"
    author: "Winter"
  - subject: "Go KDE Go!"
    date: 2008-04-01
    body: "I was waiting from long time when KOffice starting .docx format support. I will not move to OO.o to KOffice.\n\nThanks! KDE!\n\n;)"
    author: "Kartik Mistry"
  - subject: "You Have Got To Be Kidding"
    date: 2008-04-01
    body: "Is The Author Playing A Cruel April Fools Joke? I Think So....\nPS: The Anonymous NA Donor Must Be M$"
    author: "BCook2"
  - subject: "Joke"
    date: 2008-04-01
    body: "Oh my fucking god, that was the WORST prank ever! I started yelling \"NO! NO!\" when I saw the description, then went \"What?\" to the \"anonymous 10k donation\".\n\nWow. You really had me there. "
    author: "Ben"
  - subject: "Why only $10,000?"
    date: 2008-04-01
    body: "$10,000 is very cheap. I think KDE's support is more worth than this. Others got several 100 million Dollars to abandon the antitrust lawsuit against Microsoft in Europe. Why KDE took only $10,000?"
    author: "Stefan L"
  - subject: "Re: Why only $10,000?"
    date: 2008-04-01
    body: "Maybe Mr Seigo needed a new MAC Air? In any case 10,000 is better than nothing at all :D "
    author: "Bobby"
  - subject: "You got me really hard too!"
    date: 2008-04-01
    body: "I almost cry when I was reading this! I even thought to remove all KDE traces from my PC. Oh the Humanity!\n\nYou are so mean! ;D\n\nPS: In my country April's Fools is in December 28th. I wasn't aware of the joke 'cause I didn't expect it at all! Gosh, you almost kill me!\n\n"
    author: "Andrey"
  - subject: "Re: You got me really hard too!"
    date: 2008-04-01
    body: "I hope your country calls it something different than April Fools then :-)\n\nBut it could make an interesting story. Headline: \"April Fools to be celebrated in December\". Quote: \"April Fools in April is just too mundane, people already known what to expect. So we decided to have it in December, when people really aren't expecting it.\""
    author: "Thiago Macieira"
  - subject: "Re: You got me really hard too!"
    date: 2008-04-01
    body: ">I hope your country calls it something different than April Fools then :-)\n\nYou made my day!\n"
    author: "Tim Gollnik"
  - subject: "Re: You got me really hard too!"
    date: 2008-04-01
    body: "I was thinking the same when reading the parent post :) . Here in Spain, and I guess the same applies to Latin America, it's called 'Innocent's Day', or 'Innocent Saint's Day', but overall is the equivalent to April's Fools... Imagine how confused we are with these kind of news until we realize it's your pranks day ;) ."
    author: "Fede"
  - subject: "Re: You got me really hard too!"
    date: 2008-04-01
    body: "Yes here in Costa Rica -as Fede said- it is called \"El d\u00eda de los Santos Inocentes\" or \"Innocent's Day\" and for us is exactly like April Fools. I wasn't even thinking in April Fools!"
    author: "Andrey"
  - subject: "Re: You got me really hard too!"
    date: 2008-04-02
    body: "The day of the innocents is a religious day. It has its origins on the masacre by king Herodes I, who ordered that all babies borned in Judea be killed, hoping that he could get rid of Jesus. The babies are the innocents (which in Spanish also means gullible).\n\nBTW, I read about ooxml in slashdot and didn't believe it, but I couldn't be 100% sure. Reading that KDE embraced ooxml made me realize this wasn't true at all."
    author: "leo_rockway"
  - subject: "You can't trust anybody these days..."
    date: 2008-04-01
    body: "At first it was hard to believe that the KDE ISO delegate dared to do that, but it's clear that he's been flirting with M$ since long time ago.\n\nhttp://kiso.sourceforge.net/info_me.php\n\nSad. Very sad... :(\n"
    author: "BrokenSoul"
  - subject: "Sold Out"
    date: 2008-04-01
    body: "For 10,000 dollars? Even prostitutes are asking for more :D :D\nFor April Fool's Day not bad but tomorrow it might be a reality, a nightmare that haunt us."
    author: "Bobby"
  - subject: "You had me :-)"
    date: 2008-04-01
    body: "Man, I really was worried for a moment!"
    author: "Bart van Deenen"
  - subject: "aprils..."
    date: 2008-04-01
    body: "...fools.\n\nsucked."
    author: "Adb"
  - subject: "Oh god!"
    date: 2008-04-01
    body: "Come on! In Spain today isn't fools' day... I coudn't believe whay I was reading... I could only think: \"oh god! that donation is a bribe! Don't you realise?\". But thanks god I realised today is Aprils Fool..."
    author: "Bardok"
  - subject: "Blow to GNOME foundation"
    date: 2008-04-01
    body: "This is a very funny april fool's joke here, but it's also a blow to the GNOME foundation who actually supported OOXML standardization not long ago!\nhttp://www.linux.com/feature/121930"
    author: "anon"
  - subject: "Re: Blow to GNOME foundation"
    date: 2008-04-01
    body: "GNOME = Politics (isn't that why the project was started in the first place? now they seem to be taking a move away from freedom, go figure. Politics are shady matters =))\n\nKDE = Technology"
    author: "Kostumer"
  - subject: "in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "at first glimpse, i thought that kde is becoming more pragmatic and accepted the obvious, whether we like it or not, open xml is here to stay, no need to say that Microsoft has a near monopoly in office marketplace. i am just wondering how much time will take before we have a working support for open xml in koffice.\n\nfriendly  "
    author: "mimoune djouallah"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "> i am just wondering how much time will take before we have a working support for open xml in koffice.\n\nWell if you start now, and implement a whole page of the spec every day, you should be finished by 2024 (6000+ pages divided by 365 days in a year).\n\nGood luck with that :)\n"
    author: "JohnFlux"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "Probably 2048 -- you'd have to implement it twice: once the spec and once the variant Microsoft uses themselves but haven't documented."
    author: "Boudewijn Rempt"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "Considering that GNOME and OOo already have functional OOXML support, does this mean that the KDE developers are incompetent compared to the GNOME and OOo developers that it would take KDE developers so long to implement?\n\nSomehow I doubt you are correct in your estimation of how long it would take.\n"
    author: "Martin Lang"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "Incompetent? Hardly.  Underfunded? Definitely."
    author: "Anon"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "Please define what you call functionnal. And which spec have they implemented ? OOXML from MSO, ECMA OOXML or ISO OOXML ? All of them having differences and some incompatibilities."
    author: "Cyrille Berger"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-01
    body: "Why can't we just take their code and just modify it. That's the spirit of FOSS. No need to reimplement everything."
    author: "Anonymous"
  - subject: "Re: in another news, Open XML is an ISO standard "
    date: 2008-04-05
    body: "I doubt you ever looked at OOo code, or ever actually tried to reuse a non-trivial amount of code that wasn't explicitely designed for that. I don't know about OOXML, but the filters for .xls and older word formats in OOo are known to be of unmaintainable code quality and complexity, not separated from the rest of OOo and often not even documented in English (but in German)."
    author: "Frank"
  - subject: "Thank god it's April 1st"
    date: 2008-04-01
    body: "And thanks for KDE not going the 'Icaza' way with regard to OOXML"
    author: "BrettlMaster"
  - subject: "Wait... what?"
    date: 2008-04-01
    body: "This is unexpected, specially after his opinion some time ago http://aseigo.blogspot.com/2007/09/why-ooxml-as-standard-is-big-deal.html\n\nWhat changed?"
    author: "Leiteiro"
  - subject: "Re: Wait... what?"
    date: 2008-04-01
    body: "bah, April fool... nvm."
    author: "Leiteiro"
  - subject: "Made my day"
    date: 2008-04-01
    body: "Darn it, this made me realise how open I am to influence.\n\nReading the first lines, I was like: WTF. When reading on, I began to think: \"maybe OOXML is not all that bad if dot people say so\". The anonymous donation made me realize it's April 1st.\n\nAnd the really puzzling thing is, that the only reason why I visited the dot right now was to see wether there's an April's fool on :)"
    author: "fhd"
  - subject: "It's a sad day"
    date: 2008-04-01
    body: "As an independent open standards developer, I find it sad that the focus is totally on OpenDocument and OOOXML. \n\nMy own superior standard (it counts 600.000+ pages and was written in a mix of a dozen dying languages, including that one in Mexico) which I submitted to ISO last year was never mentioned, despite obviously being of better quantity and quality.\n\nAnd to think that I even bribed the stepdaughter of the hairdresser of the cousin of the baker of ISO's chairman.\n\nThe world ain't fair."
    author: "Darkelve"
  - subject: "ROFL"
    date: 2008-04-01
    body: "\"despite obviously being of better quantity\"\n\nyou made my day!"
    author: "April"
  - subject: "ISO = crap?"
    date: 2008-04-01
    body: "I certainly appreciate the humor, however given the way things have been going over the last few month, it is really hard to tell whether all of today's news are due to the date on the calendar, or some of them are actually real.\n\nLike all those references to the approval that is supposed to be announced today or tomorrow. One would hope it's all just a sick joke, but knowing how things work in the business-as-usual world, it could all be for real."
    author: "uh-oh"
  - subject: "Re: ISO = crap?"
    date: 2008-04-01
    body: "\"in the business-as-usual world\"\n\nIn the end it would be a good thing when one could open an OOXML-document in Koffice and could safe a document as an .ooxml-file in Koffice. for the sake of interchange with the poor Windows-users. i mean, today i am exporting everything to .pdf. it is all about interchange. the spice must flow."
    author: "as usual"
  - subject: "Damn you guys, almost killed me :)"
    date: 2008-04-01
    body: "LOL, nice. On our culture fools day is not 31 of March. Was funny though. :)"
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: Damn you guys, almost killed me :)"
    date: 2008-04-01
    body: "I meant 1st of april, not 31 of march."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: Damn you guys, almost killed me :)"
    date: 2008-04-01
    body: "Timezones dude.\n\nTimezones."
    author: "Ian Monroe"
  - subject: "Nice one!"
    date: 2008-04-01
    body: "Had me for 10 seconds wondering \"what the hell\" :-) I liked it :-)"
    author: "Anony Moose"
  - subject: ":-)"
    date: 2008-04-01
    body: "Nice one haha :-D"
    author: "sim0n"
  - subject: "oh noes. gnome here i come"
    date: 2008-04-01
    body: "That does it. I'm switching to GNOME now. I hope you guys are happy. Corruption ftw, eh?"
    author: "freedomfighter"
  - subject: "April Fool's Day ..."
    date: 2008-04-01
    body: "...Is highly overrated :)"
    author: "Anonymous Coward"
  - subject: "Aaron, I'm Very Disappointed..."
    date: 2008-04-01
    body: "...that we would sell out so cheaply, c'mon, it should be at least $12,000!  \n\nWhen you collect, just make sure they don't try palm off some worthless 3rd world currency on you like Zimbabwe dollars or Canadian dollars, and count your fingers afterwards.  Say high to Bill and Steve for me.\n\nJohn.\n\nP.S.  Can you put my cut in my Swiss account, you have the number from last time..."
    author: "Odysseus"
  - subject: "Re: Aaron, I'm Very Disappointed..."
    date: 2008-04-01
    body: "and avoid US-$ as well... :)"
    author: "Sebastian"
  - subject: "hoho!"
    date: 2008-04-01
    body: "Funny!!\n\nConsidering the dire quality of the standard, the pattern of national body votes could actually be considered a \"corruptibility to US corporate influence\" index and should probably be published as such!\n\nAPPROVAL - our Lords are in Redmond, consider us the 51st US state!\n\nABSTAIN - we're honest enough to -almost- say its a pile of shit, but we've read about Hiroshima in the school textbooks\n\nDISAPPROVE - not part of the neoliberal economic order, some degree of sovereignty. At least as far as technical standards go.."
    author: "max stirner"
  - subject: "Re: hoho!"
    date: 2008-04-01
    body: "Insightful"
    author: "Moderator"
  - subject: "The sad part is..."
    date: 2008-04-01
    body: "... that I hear it's true. OOXML seems to be on it's way to be accepted. :(\n\n\nbtw. that April fools was better than google's. So you did good Jonathan :)"
    author: "Max"
  - subject: "Re: The sad part is... <---"
    date: 2008-04-01
    body: "http://www.news.com/8301-10784_3-9907583-7.html?tag=nefd.top"
    author: "Mike H"
  - subject: "Re: The sad part is..."
    date: 2008-04-01
    body: "http://tech.slashdot.org/tech/08/04/01/1240241.shtml"
    author: "M"
  - subject: "Re: The sad part is..."
    date: 2008-04-03
    body: "http://www.betanews.com/article/Now_an_official_ISO_standard_Microsofts_OOXML_invites_controversy/1207176258"
    author: "Max"
  - subject: "You got me too :)"
    date: 2008-04-01
    body: "that was a good one :) it take me a good while"
    author: "polrus"
  - subject: "Got me"
    date: 2008-04-01
    body: "I knew you were joking as soon as you mentioned $10,000 but I thought it was a clever satire on the OOXML process rather than an April's fools day"
    author: "Ben"
  - subject: "April 1 = April's Fool"
    date: 2008-04-01
    body: "Don't you guys realize that today in the US is April Fools? The hint was in the comment \"\"and following a $10,000 donation from an anonymous North American source...\", implying that MS made the contribution.\n\nHAPPY APRIL FOOLS"
    author: "RV"
  - subject: "Re: April 1 = April's Fool"
    date: 2008-04-02
    body: "But the post is 31st March, not April, it can't be April's Fool."
    author: "TinHo Mak"
  - subject: "Re: April 1 = April's Fool"
    date: 2008-04-02
    body: "Timezone of the Dot is Pacific Time, the story was posted at midnight UTC."
    author: "Sebastian Kuegler"
  - subject: "Finally seeing the light"
    date: 2008-04-01
    body: "I'm hoping that KDE now starts to adopt WinForms, Win32, and other Microsoft technologies, since then KDE would kick ass."
    author: "Ronald Daniels"
  - subject: "woops..."
    date: 2008-04-01
    body: "The only problem with this article is - it's not an april fool anymore. OOXML is ISO standart as of today =("
    author: "Stalwart"
  - subject: "Re: woops..."
    date: 2008-04-01
    body: "http://tech.slashdot.org/tech/08/04/01/1240241.shtml\n\nEven slashdot seemed baffled."
    author: "M"
  - subject: "Sad"
    date: 2008-04-02
    body: "The problem with these jokes is that they're funny because they are far from being real. \n\n\"Ubuntu founder Mark Shuttleworth said the approval of Microsoft's Open Office XML is a sad day for ISO and the computing public.\"\n\nhttp://blogs.zdnet.com/open-source/?p=2222\n\nOnce you think about it, is actually really sad."
    author: "Kapri"
  - subject: "Sad"
    date: 2008-04-02
    body: "The problem with these jokes is that they're funny because they are far from being real. \n\n\"Ubuntu founder Mark Shuttleworth said the approval of Microsoft's Open Office XML is a sad day for ISO and the computing public.\"\n\nhttp://blogs.zdnet.com/open-source/?p=2222\n\nOnce you think about it, is actually really sad."
    author: "Kapri"
  - subject: "Re: Sad"
    date: 2008-04-02
    body: "Yes, ;) I know is supossed to be a joke in case you were wondering. "
    author: "Kapri"
  - subject: "Re: Sad"
    date: 2008-04-02
    body: "The best jokes are made by cabaretists because the background of their jokes is bitter and bizarre reality: They take a common event put it into a somewhat disturbing and unlikely context and suddenly you realize how absurd all this is.\n\nSo kudos to the KDE people that had the idea for this April's fool. You clearly showed that Microsoft must be a poor squirt [German: \"armseeliges W\u00fcrstchen\" ;-)] that they are in need to act like they did.\n\nSo let's laugh at Microsoft and make fun about them. Recite the most idiotic parts of OOXML in public (not just in blogs and among nerds). Everytime someone says \"OOXML is standard\" say, \"Well corruption should be standardized by ISO, too and I eagerly wait Microsoft buying my vote in favour of such a standard\".\n"
    author: "Arnomane"
  - subject: "Now that April Fools is over, can we delete that?"
    date: 2008-04-02
    body: "As said"
    author: "Randalf"
  - subject: "Joke or not M$ has won"
    date: 2008-04-02
    body: "Joke or not joke now this happened:\n\nhttp://www.iso.org/iso/pressrelease.htm?refid=Ref1123\n\nWhat a shame :("
    author: "Giovanni Venturi"
  - subject: "Re: Joke or not M$ has won"
    date: 2008-04-03
    body: "http://www.betanews.com/article/Now_an_official_ISO_standard_Microsofts_OOXML_invites_controversy/1207176258"
    author: "Max"
  - subject: "Brilliant"
    date: 2008-04-02
    body: "This almost got me as I was reading it a day late!!  That was until the $10,000 donation.....\n\nHappy April Fools one and all!!"
    author: "TheGreatGonzo"
  - subject: ":( Wish it were a joke :("
    date: 2008-04-03
    body: "http://www.betanews.com/article/Now_an_official_ISO_standard_Microsofts_OOXML_invites_controversy/1207176258"
    author: "Max"
  - subject: "haha"
    date: 2008-04-03
    body: "haha\nthe date almost fooled me\n\nBut then I read 10.000 donation and it was immediately obvious that IF any bribery is going on, THESE indicators would NOT be disclosed ;)"
    author: "she"
  - subject: "O.o"
    date: 2008-04-03
    body: "OMFG it\u00b4s true, OOXLM is a standar by now, and the donation, and A. Seigo is now ranked Supreme Kommander. thats Fucking surpricing"
    author: "1Debianita"
  - subject: "Dont do that again!"
    date: 2008-04-03
    body: "You almost killed me... If you imagine the names i called you... until i didnt realized  that was a joke..."
    author: "Jos\u00e9 Mendes"
---
This week saw the International Standards Organisation vote on adopting Office Open XML as a standard for office documents. KDE gained a representative late last year through our legal body KDE e.V. realising that the only way to ensure a fair process was to be part of it.  Today our delegate voted yes to adopting the format as an international standard.  "We have studied the standard hard and many changes have been made to it," said KDE's <a href="http://www.siriusit.co.uk/myblog/interview-aaron-seigo-kde-project-lead.html">Supreme Leader</a> Aaron Seigo "and following a $10,000 donation from an anonymous North American source we realised the market should decide the best formats to use, not technical bureaucrats".







<!--break-->
<p>The KOffice developers confirmed their support with Cyrille Burger saying, "The level of detail in this standard is very impressive, previous standards we had to deal with were less than half as expansive in their documentation.  Working with a standard that makes such good use of previously established standards was also a main reason for the quick implementation in KOffice".</p>

<p>KDE founder Stephan Coolio was unavailable for comment because he was changing nappies.</p>






