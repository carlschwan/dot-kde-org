---
title: "KDE and GNOME to Co-locate Flagship Conferences on Gran Canaria in 2009"
date:    2008-07-11
authors:
  - "skuegler"
slug:    kde-and-gnome-co-locate-flagship-conferences-gran-canaria-2009
comments:
  - subject: "I can say it in less press-releasy terms, too"
    date: 2008-07-11
    body: "There were three great bids. I can think of personal reasons to want to go to any three of them; Richard Dale's beer-tour-of-the-island blog helped a bit to form my opinion. The best kind of conference is in a wonderful location that makes life easy and pleasant while keeping the focus on the technical work to be done. I'm confident that GC is a good pick like that.\n\nAnyway, we now have a year to work on our \"dead canary\" jokes, with all possible variants of pining for the fjords and un-washed geeks in there. I'm looking forward to it."
    author: "Adriaan de Groot"
  - subject: "Re: I can say it in less press-releasy terms, too"
    date: 2008-07-12
    body: "What about the non-drinkers?"
    author: "beer?"
  - subject: "Re: I can say it in less press-releasy terms, too"
    date: 2008-07-12
    body: "\"we now have a year to work on our \"dead canary\" jokes, with all possible variants of pining for the fjords\"\n\nLets not...\n\nhttp://xkcd.com/16/\n"
    author: "fred"
  - subject: "I think..."
    date: 2008-07-11
    body: "...it's a trap!"
    author: "James"
  - subject: "xD"
    date: 2008-07-12
    body: "<joke>\nNoooo!!! Ximians will kill any KDE developers they meet! They will eat your meat as breakfast! You can't meet with THE ENEMY at the same time!!!\n</joke>\n\n"
    author: "Alejandro Nova"
  - subject: "I hope everybody uses well the opportunity"
    date: 2008-07-11
    body: "I would love to see more interaction between gnome and KDE, even more now that gtk3/gnome3 was decided :)\n\nIn a perfect world I would made them both:\n- use the same system for icon themes (the icon theme would be located in a common place, much like the current menu)\n- use each-other themes (hard? yes! possible? so it is). both could have \"native\" themes more advanced, but a common theme based on png/svg/xml would be awesome!\n- commons dialogs: why should I load gtk color picker or file save/load while I am in KDE and vice-versa? (wasn't KDE4 using dcop help this one?)\n- agree in a common guideline for programs interface and accessibility. I do not believe this will ever come to happens because of UI different philosophies, but it would be very good for new users that are coming to (l)nix systems if gnome and kde behave in a similar way. Think about using the same keyboard shortcuts, elements position, toolbar icons sizes, etc.\n- unite configurations!! made EVERYTHING that is common to both desktop into common configuration files! think about using the same icon sizes, double-single mouse click, keyboard layout, color scheme, theme, panel size...\nAnd not, that would not kill each desktop standard look, because each would use it's OWN default UNTIL the user decides to change.\n\nOK, now that the dream is over, and I am awake, I wish a great party next year for both GUADEC and aKademy. I hope everybody have a lot of fun and speak about interesting new stuff. :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-11
    body: "I agree with you 100%.\n\nI think they should go even further, and if freedesktop.org is HINDERING them, then they should NOT use that specific part of it.\n\nThe FHS standard hinded AppDirs on Linux, i hope people will understand that not everytime following a standard is the best route."
    author: "markus"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-11
    body: "* use the same system for icon themes\n\nalready done\n\n* use each-other themes\n\nthere's a Qt4 theme that bridges over to Gtk+ themes. would be cool to see if the Gtk people could manage the reverse direction.\n\n* commons dialogs\n\nwill require a much more SOA than what is currently assumed by applications.\n\n* agree in a common guideline for programs interface \n\ni'm doubtful this will occur (tried it once even)\n\n* and accessibility.\n\nfor a11y we're already on the same page.\n\n* unite configurations\n\nwould require a common configuration system underneath to be realistic, and there's some rather different viewpoints on what a config system should be."
    author: "Aaron Seigo"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-14
    body: "\"there's a Qt4 theme that bridges over to Gtk+ themes.\"\n\nI assume you mean Trolltech's qgtkstyle?\n\n\"would be cool to see if the Gtk people could manage the reverse direction.\"\n\nWill never happen.  It's Gnome.  But there's always gtk-qt.  http://www.kde-look.org/content/show.php?content=9714\n\nOr maybe the \"Qt4 theme that bridges over to Gtk+ themes\" was gtk-qt (I couldn't tell) in which case the reverse direction would be qgtkstyle http://labs.trolltech.com/blogs/2008/05/13/introducing-qgtkstyle/\n\nEither way, getting gtk apps to use KDE themes was done by the KDE side and getting Qt apps to use GTK themes was done by Trolltech.  Gnome doesn't do that sort of thing.\n\nSimilarly,  QtGTK (allowing gtk apps to use KDE dialogs) came from the KDE camp (but seems to have died, though kgtk, also from KDE camp, functions and does the same thing in a rather hacky manner) while Trolltech made it possible to integrate the glib event loop into a qt app for possible use of Gnome dialogs in Qt apps.\n\nAgain: both directions, neither done by Gnome.  They don't seem interested in the visual side of integrating things at all."
    author: "MamiyaOtaru"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-14
    body: "KDE 4.1 can now display native Window$ file dialogs while keeping the KFileDialog interface, I don't see why it wouldn't be technically possible to do the same with GTK+/GNOME dialogs, especially with a Qt built using the GLib event loop. Now whether it is desirable is another question."
    author: "Kevin Kofler"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-12
    body: "> - unite configurations!! made EVERYTHING that is common to both desktop into common configuration files!\n\nI don't expect that to happen.. Simple example: GNOME has one wallpaper for all virtual desktops, KDE allows you to choose one per desktop. If you want to unite settings, you need to agree on every single configuration entry. This is unrealistic.\n\nI do like to see some settings shared. For example, I get requests from users who are using my KDE app (KMess) in GNOME. When they click on an URL, it opens the preferred KDE webbrowser. We currently have to explain them how to go into KControl (or even ~/.kde/share/config/kdeglobals) to fix it. This doesn't feel right to me."
    author: "Diederik van der Boor"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-14
    body: ">If you want to unite settings, you need to agree on every single configuration entry. This is unrealistic.\n\nTHAT is the sad thing about open-source projects :-(\nWhy is so difficult to agree such a small thing? Use a simple XML. You can have both ways, gnome could read only desktop #1 option, kde could read all of them. It's just a simple middle of the way solution users want!"
    author: "Iuri Fiedoruk"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-14
    body: "> Why is so difficult to agree such a small thing?\n\nIt is difficult because it is *not* as small thing.\nAgreeing on several hundrets of settings, their keys/names, value types, default values, overwrite rules (i.e. system vs. user values), etc. is not something \"small\".\n"
    author: "Kevin Krammer"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-14
    body: "I was talking only about the wallpaper.\nYes, if you look at the whole picture, the job can be scary.\nBut why not you can just do things one by one, small piece by small piece?\n\nOr in good portuguese: \"de gr\u00e3o em gr\u00e3o a galinha chega l\u00e1\" (from grain in grain the hen arrives where it wants) :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-13
    body: "If the Gnome3 UI is going to be all about tabs (see planet.gnome.org) and the KDE4 UI is all about widgets and containments, what are the odds that a marriage of the two projects will put tabs on the desktop?\n"
    author: "VelvetElvis"
  - subject: "Re: I hope everybody uses well the opportunity"
    date: 2008-07-14
    body: "The posts about tabs are jokes. The code patch just adds a \"Page 1\" tab to all widgets (if it works at all), and the D-Bus tabs are obviously a joke."
    author: "Kevin Kofler"
  - subject: "Correct this, please"
    date: 2008-07-11
    body: "\"the main island of the Canary Islands archipelago\"\n\nThis is false. The island with most area and population[1][2] is Tenerife. Even more, the capital city is shared between Las Palmas and Santa Cruz de Tenerife. So I don't see where you took this information.\n\nPS: Instead that, you can said that Las Palmas is the largest city of the island.\n\n[1]http://en.wikipedia.org/wiki/Tenerife\n[2]http://en.wikipedia.org/wiki/Gran_Canaria"
    author: "cruzki"
  - subject: "Re: Correct this, please"
    date: 2008-07-11
    body: "Thanks for the clarifications!"
    author: "Sebastian Kuegler"
  - subject: "Re: Correct this, please"
    date: 2008-07-12
    body: "Tenerife is bigger and has more population, but we are still the main island :P"
    author: "Momo"
  - subject: "Re: Correct this, please"
    date: 2008-07-12
    body: "Tenerife is bigger, taller, have more population and, more important, is the prettiest island :P :P\n\nPD: Is a very good thing this this type of events are in the Island."
    author: "cruzki"
  - subject: "Re: Correct this, please"
    date: 2008-07-12
    body: "I thought the prettiest was La Palma. Please, try to keep the \"pique\" out of this :P"
    author: "Heimy"
  - subject: "Re: Correct this, please"
    date: 2008-07-12
    body: "Well in my opinion, Tenerife is best admired from a distance, by climbing up one of Gran Canaria's excellent mountains, such as the Roque Nublo (biggest isn't necessarily prettiest), and looking across the 'Mar del Nublos' (sea of clouds) at Tenerife's Mount Teide which still has snow on the peak in early summer. So no need to actually go to Tenerife to do that, and it would be well worth doing if you have some spare time while attending the conference."
    author: "Richard Dale"
  - subject: "Re: Correct this, please"
    date: 2008-07-13
    body: "The island is Gran Canaria (nor Las Palmas) where resides the capital of Las Palmas's province (Gran Canaria, Fuerteventura, Lanzarote and some minor islands). The city is Las Palmas de Gran Canaria.\n\nTenerife is bigger in size but belongs to other province: Santa Cruz de Tenerife (Tenerife, La Palma, La Gomera y el Hierro).\n\nPopulation is similar between capitals but Gran Canaria is more cosmopolitan than Tenerife.\n\nSo, Canary Islands have got 2 provinces: Las Palmas and Santa Cruz de Tenerife.\n\nEvery island has got its own beautiful places and features. Canary people are outgoing and like visitors. So you are welcome!!!\n\nI think that any island could be a good place for this event. But now is Gran Canaria time!!!\n\nP.S.: And remember: The best place where to admire Teide is from Gran Canaria."
    author: "Gonzalo"
  - subject: "Money talks"
    date: 2008-07-12
    body: "the KDE e.V. and GNOME foundation have settled on Gran Canaria because of its position as Port to Africa and the *excellent circumstances* for holding such an event there."
    author: "dolly"
  - subject: "Re: Money talks"
    date: 2008-07-12
    body: "I'm not sure what point you are trying to make. To put on two events with 500 attendees each at the same time without adequate funding makes no sense at all. Even then, it will still involve a lot of volunteer effort to make it happen.\n\nThe sponsors of the event are allowing the people who go to further their projects and improve collaboration, and I can assure you that there won't be advertising breaks by the Gran Canaria tourist board or Nokia in between the conference talks."
    author: "Richard Dale"
  - subject: "Gran Canaria Desktop Meeting: GUADEC+Akademy 2009"
    date: 2008-07-14
    body: "First of all, thanks to everybody for bringing such a big event to Gran Canaria, Canary Islands. As you can see, eventhough we are a little group of 7 islands, each one of us thinks its islands is the best one. It happens everywere.\n\nSince this time GUADEC and Akademy will take place at the same time, at the same place, we will need an extra effort to prepare common activities and hacking sessions. This needs previous work so the event can be productive. Whatever you guys need, please let us know.\n\nThis event should be also different in the implication of the local community. Since we have strong sponsorship that are going to help us a lot, we will have enough resources to organice courses and other stuff so more local newbies get into free software in general and to GNOME and KDE in particular. We will need help from KDE and GNOME developers in this job. \n\nIf this two extra objetives are accomplished, Gran Canaria Desktop Meeting: GUADEC+KDE 2009 will be a success."
    author: "Toscalix"
---
The KDE e.V. and GNOME Foundation today announced that they will hold their yearly conferences, Akademy and GUADEC in 2009 in Gran Canaria. The conferences will be separate events, but co-located and hosted by the same organizers, the <a href="http://www.grancanaria.com">Cabildo of Gran Canaria</a> and its Secretary of Tourism, Technological Innovation and Foreign Trade.
"<em>The GNOME community is very excited about the co-hosted GUADEC and Akademy</em>" says Behdad Esfahbod, president at the GNOME foundation, "<em>GUADEC has traditionally been a very important chance for our community to meet in person, build great working relationships and make new friends. We're looking forward to having the opportunity to extend those relationships to our KDE colleagues at Akademy/GUADEC.</em>" KDE e.V.'s vice-president Adriaan de Groot adds "<em>KDE e.V. is looking forward to a co-located conference, where the GNOME and KDE communities can mingle and cooperate as never before in one location. Gran Canaria is uniquely located at the junction of Europe and Africa, close to the Americas and is a fitting place for a historic 'meet-your-neighbours' conference.</em>"


<!--break-->
<p><a href="http://www.guadec.org">GUADEC</a> and <a href="http://akademy.kde.org">Akademy</a> 2009 will be held on <a href="http://en.wikipedia.org/wiki/Gran_Canaria">Gran Canaria</a>, the main island of the <a href="http://en.wikipedia.org/wiki/Canary_Islands">Canary Islands</a> archipelago. The tentative <a href="http://grancanariadesktopmeeting.eslic.es/index.php/Official_planning">schedule</a> plans the event from Friday, July, 3rd until Saturday, July 11th 2009 in the <a href="http://www.auditorio-alfredokraus.com/web/aud.htm">Alfredo Kraus auditorium</a> and the adjacent <a href="http://www.auditorio-alfredokraus.com/web/pcongs.htm">Congress Palace</a> in <a href="http://en.wikipedia.org/wiki/Las_Palmas">Las Palmas de Gran Canaria</a>.</p>
<p>
This co-located event will turn Gran Canaria into the capital of Freedesktop.org development for a whole week next summer. </p>
<p>
While there were other excellent bids, the KDE e.V. and GNOME foundation have settled on Gran Canaria because of its position as Port to Africa and the excellent circumstances for holding such an event there. Unfortunately, having three proposals, two have to be rejected. The proposals from Tampere in Finland and Coruna in Spain were close contenders. Both foundations would like to thank those organisers for the work they have put into their proposals and encourage them to consider their cities for conferences in future years.</p>
<p>
The <a href="http://grancanariadesktopmeeting.eslic.es/">conference organiser's Wiki has  extensive information</a> about the planned conferences on the Canaries.</p>


