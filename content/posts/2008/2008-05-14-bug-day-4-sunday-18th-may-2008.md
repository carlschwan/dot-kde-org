---
title: "Bug Day 4 - Sunday 18th May 2008"
date:    2008-05-14
authors:
  - "ggoldberg"
slug:    bug-day-4-sunday-18th-may-2008
comments:
  - subject: "firewalled"
    date: 2008-05-15
    body: "is there a way to participate on the irc session when i m behind a firewall that blocks the irc ports? maybe something like a web-irc-client? thanks for any help!"
    author: "j"
  - subject: "Re: firewalled"
    date: 2008-05-15
    body: "Try http://www.mibbit.com"
    author: "George"
  - subject: "Re: firewalled"
    date: 2008-05-15
    body: "yay, thanks. that will do the trick, count me on board then : )"
    author: "j"
  - subject: "Thanks George"
    date: 2008-05-15
    body: "for putting that together. I have not been helping a lot so far but when I did it was a pleasure to do so in a friendly channel!\nIf you have 30 minutes only, it does not matter, you can investigate one or two bug reports only. So I encourage you all to come and test a few reports!"
    author: "annma"
---
<a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KonquerorDay4">Bug Day 4</a> will take place on Sunday 18th May from 0:00 UTC - 23:59 UTC. (That's a start time of 02:00 CEST, or 17:00 PDT Saturday). For this Bug Day, we will be sorting and testing bugs reported against Konqueror.

<!--break-->
<p>Bug Days are hosted by the <a href="http://techbase.kde.org/Contribute/Bugsquad">KDE Bugsquad</a> approximately once every two weeks. Their purpose is to check back through the large numbers of bugs stored in the <a href="http://bugs.kde.org">KDE Bug Tracking System </a> and investigate how to reproduce them. This means that when developers come to the bug reports to fix them, all the information they need is available on the report and they don't have to spend huge amounts of their time investigating the bugs - they can just focus on fixing them. During each Bug Day, we will focus on one area of KDE in particular. For this Bug Day, we will be focusing on general bugs in Konqueror. More information can be found on the <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KonquerorDay4">Bug Day 4 Techbase Page</a>.</p>

<p>Joining Bug Days is a great way to help the KDE project. The only things you need to take part are a computer running KDE 4 and an internet connection. No programming skills or previous contributions to KDE are necessary. You can join at any point during the day, and stay for as long or short a time as you like. If you are a KDE user hoping to contribute, then this is a great way to get started - there will be plenty of experienced Bugsquad members on hand to answer any questions you might have. To get involved, join our IRC channel, <b>#kde-bugs</b> on <b>irc.freenode.net</b>, where we will help you get underway.</p>

