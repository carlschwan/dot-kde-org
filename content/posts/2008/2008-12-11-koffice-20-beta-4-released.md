---
title: "KOffice 2.0 Beta 4 Released"
date:    2008-12-11
authors:
  - "brempt"
slug:    koffice-20-beta-4-released
comments:
  - subject: "Kubuntu"
    date: 2008-12-11
    body: "Krita is great. Such a shame that Kbuntu cripples it due to missing support for GIF and other file formats. This is so annoying (especially because this is going on for years and multiple releases without neither any attempts to fix this nor any information on whether they plan to do so) that I'm considering switching to SuSE after years of Ubuntu just because of this single bug - as ridiculous as it seems. I don't want to compile Krita all by myself - installing scores of missing dev-packages. This is why I use a distro in the first place. They really should have managed to do this in over a year. They just don'T care I suppose. Unfortunately switching isn't that easy - especially with only a single hard-drive at-hand. Nevertheless - looking at the minimal progress in contrast to say SuSE I have the feeling I'm riding a dead horse."
    author: "Michael"
  - subject: "Re: Kubuntu"
    date: 2008-12-12
    body: "I know many will perceive this as trolling, but if you love KDE, you really shouldn't run Kubuntu.  Run a distro that puts out good KDE packages such as PCLinuxOS, Arch, Sabayon, Mandriva, Arch, openSUSE, etc."
    author: "T. J. Brumfield"
  - subject: "Re: Kubuntu"
    date: 2008-12-13
    body: "Arch is so good you mentioned it twice? \n\nWell, I tried Arch few weeks ago but my KDE experience was so horrible I switched back to good old Kubuntu 8.10 :)"
    author: "kanttu"
  - subject: "Re: Kubuntu"
    date: 2008-12-28
    body: "Did you try kdemod?"
    author: "sandsmark"
  - subject: "Sluggish"
    date: 2008-12-11
    body: "First off. I don't want to offend, I really honor all the work of the developers.\n\nBut, the movie shows more the sluggish GUI of KOffice instead of the nice new features. Either the machine of the director is very old and slow or Krita really consumes a lot of calculation power while rendering in the background. Nevertheless which one is the real cause of the sluggish desktop, both are definitely not a good advertisement for KOffice ;-)\n\nSCNR\nAndreas\n\n"
    author: "Andreas Pietzowski"
  - subject: "Re: Sluggish"
    date: 2008-12-12
    body: "low frame rate? no acceleration? I don't think this is sluggish.\n\nVery nice feature addition though. Very very nice... GIMP has just dropped down a notch on my most used app list."
    author: "frozen"
  - subject: "Re: Sluggish"
    date: 2008-12-12
    body: "Yeah I also noticed this in the video. It could also be a capturing problem. Judging from the video alone makes it look like the krita UI is blocked, but I guess we'll have to see it in practise how well it works ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: Sluggish"
    date: 2008-12-14
    body: "It *is* a beta application. And the video was made by someone running it in full debug mode (making it *lots* slower)"
    author: "Thomas Zander"
  - subject: "Re: Sluggish"
    date: 2008-12-14
    body: "Plus, video-capturing software generally exacerbates and slowness."
    author: "Anon"
  - subject: "Re: Sluggish"
    date: 2008-12-14
    body: "I don't think he meant the slow frame rate.\nThe video shows (in its second halve) that the filter dialog is wrongly designed (the preview is shown but you cannot press <OK> until the complete image shows a preview of the filter). There will always be slow filters and this won't become better simply by stripping debug symbols.\n\nAlso the blur filter is extremely slow - I don't think that this can be caused by debug information. "
    author: "hgj"
  - subject: "Subpixel tablet precision?"
    date: 2008-12-11
    body: "Hi, do Krita developers have a problem to get sub-pixel precision tablet events from Qt? It worked perfectly with openSUSE 10.3, but I have never been able to get sub-pixel precision with 11.0, so it may be an X.org, linuxwacom, or Qt problem. What xorg.conf do you use?\n"
    author: "christoph"
  - subject: "Re: Subpixel tablet precision?"
    date: 2008-12-11
    body: "Well... It's very much OS dependent it seems. We do some trickery to get smooth curves from tablet positions. On X11 it seems to work fairly well, though it could be better. OSX is a complete tragedy, and I haven't been able to test Windows."
    author: "Boudewijn Rempt"
  - subject: "Re: Subpixel tablet precision?"
    date: 2008-12-15
    body: "Last I checked tablets worked fine on MacOSX. Please report bugs if you have problems."
    author: "Thomas Zander"
  - subject: "Re: Subpixel tablet precision?"
    date: 2008-12-19
    body: "Forget what I wrote... I just updated to openSUSE 11.1, and I have my subpixel precision back! :)"
    author: "christoph"
  - subject: "Codec. "
    date: 2008-12-11
    body: "I love how this video isn't encoded in an open source codec so I can watch on my fresh install of Fedora 10. \n\nNot a nag on my part cause the first thing I do is install multimedia codecs but is for many else. "
    author: "Jeremy"
  - subject: "Re: Codec. "
    date: 2008-12-12
    body: "At least MPEG is a published standard (ISO, even) instead of some esoteric format few people have ever heard of. There is plenty of open-source software that can play it back. It takes an extra step to install the software to play it on Fedora 10, but consider that .MP4 files (the ISO followup to the mpeg container format) require Windows Vista users to install a little extra software too. That's also a newer ISO standard format.\n\nYou can play it back with Free Software only, and it is available easily for Fedora 10 without fearing installation of malware, BigBrotherware, adware, etc. Be happy! We probably have it even better than the Windows users."
    author: "CF"
  - subject: "What is state of sheet?"
    date: 2008-12-12
    body: "\nThe Beta3 has no different viewstyle than the same what latest 1.x seris has.\n\nIs there something planned to be on \"View > Display Mode\"?\n\nIs there change to get a space between sheets? To top of the first sheet and around too. More like OpenOffice.org and Microsoft Office. That just makes better visual understanding how does the document look when printed when the toolboxed and other panels ain't touching the sheet.\n\nThe current view could be just one style, but default should be one where there is about 20-40px space around sheet."
    author: "Fri13"
  - subject: "Re: What is state of sheet?"
    date: 2008-12-13
    body: "I agree and hope that this gets addressed soon."
    author: "goran"
  - subject: "Re: What is state of sheet?"
    date: 2008-12-18
    body: "> Is there something planned to be on \"View > Display Mode\"?\n>\n> Is there change to get a space between sheets? \n> To top of the first sheet and around too.\n\nThe view->display mode menu is empty now, it just has one view mode indeed. It is planned to expand this to at least the view modes that KWord 1.6 had, but not for 2.0.\n\nI liked your suggestion for extra space so I added space above and around the pages to kword. You'll see that show up in the next beta.\n\nThanks for your feedback!\n\nps. I used 10 pixels on both sides; which looks like a nice balance between having a clear page border and not wasting space."
    author: "Thomas Zander"
  - subject: "Saving Graphics"
    date: 2008-12-12
    body: "Yep, I'm bringing up my main issue again.  Still unable to save flake shapes in a document.  Really hard for me to test and provide feedback with this critical feature missing."
    author: "cirehawk"
  - subject: "Re: Saving Graphics"
    date: 2008-12-12
    body: "It took a bit more time than I thought and there was a more real life than I'd liked this month, so I was a little too late for this beta -- but right now, the Krita code in svn saves shapes. And I'm really close to loading them again. Unfortunately, editing shapes in Krita is broken at the moment. But I'm fairly certain that editing, loading and saving of shapes will work in Krita in the next beta. "
    author: "Boudewijn Rempt"
  - subject: "Re: Saving Graphics"
    date: 2008-12-12
    body: "Thanks Boudewijn.  I'll await the next beta.  How about the other apps, KWord in particular?  Do you think they will have save ability in the next beta as well?  Thanks for all your efforts."
    author: "cirehawk"
  - subject: "Re: Saving Graphics"
    date: 2008-12-12
    body: "You guys are doing an amazing work. KOffice has amazing potential. I hope some big companies eventually realise this and pour some money into your project and hopefully into your pockets too because you guys deserve it. \n\nKeep up the good work! ;)"
    author: "Jad"
  - subject: "Flow of text"
    date: 2008-12-12
    body: "Can you make the text flow _around_ a shape??"
    author: "Axl"
  - subject: "Re: Flow of text"
    date: 2008-12-12
    body: "Yes you can, as shown here: http://www.kdedevelopers.org/node/2593"
    author: "Cyrille Berger"
  - subject: "Re: Flow of text"
    date: 2008-12-12
    body: "It seems you can only have the text run around a shape on either the left or right side, not both sides..."
    author: "Axl"
  - subject: "Re: Flow of text"
    date: 2008-12-13
    body: "I was hoping that too. The feature where text goes _around_ (both sides) would be very nice. But at least the \"one side\" is enough first. \n\nI found that making to text boxes, you can get text go around on both sides of the shape. But it is not so nice thing.\n\nSo the shape is not just \"one side around\" (because multiple text can avoid it anyside) but the problem is somewhere of the text itself. \n\n\n"
    author: "Fri13"
  - subject: "Re: Flow of text"
    date: 2008-12-16
    body: "Maybe for KOffice 2.1 ..."
    author: "Axl"
  - subject: "Re: Flow of text"
    date: 2008-12-16
    body: "Maybe for KOffice 2.1 ...\n\nToo bad... I really liked the DTP-feeling of KOffice."
    author: "Axl"
  - subject: "Krita"
    date: 2008-12-13
    body: "I mean no offence but I have to express my self. I hope that video was a joke. \n\nI think you should look at this:\n\nhttp://fileforum.betanews.com/detail/PaintNET/1096481993/1 \nhttp://fileforum.betanews.com/detail/PhotoFiltre/1088106931/1\n\nI will say no more and good luck.\n"
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "Well... PhotoFiltre isn't open source is it? And Paint.net doesn't have effect masks, does it? So, why should we look at those?"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "\"but I have to express my self.\"\n\nSee, what I don't understand here is that someone would be so compelled to speak their mind, and then go on to say absolutely nothing at all."
    author: "Anon"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "Let me help you out there.  It's quite possible there was nothing really on/in his/her mind.  So you see, if there was nothing on/in their mind to begin with and they spoke it, then it stands to reason that nothing was really said."
    author: "cirehawk"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "Ok, you asked me to speak.\n\n1. Interface is sluggish (not it is not the bandwidth) - at one stage it even froze, I hope this is only because it is a beta but knowing KDE based apps, I fear it will continue this way.\n\n2. There isn't anything in the video that made me say \"wow\" that's nice. The free hand drawing is not the best thing to do when demoing a new product to the public especially if you want to be taken seriously by the business market.\n\n3. I am yet to see how fast it would run when you open up high resolution images with rich colors, 3D graphics. Paint.Net and PhotoFiltre do this very well. \n\n4. I am serious, this video reminds me so much on this: http://www.youtube.com/watch?v=Hxx2KcPWWZg (MS Paint parody) - Please look at it, it is quite funny and only then you will what I mean. I just cannot believe it is actually true. (It is an MS Paint Parody, trust me you will laugh)\n\n5. It doesn't matter if it is closed source or open source software as long as it performs. Yes, Open Source IS better but I don't choose software by its license.\n"
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "To add to this, I am quite impressed with KDE 4.2 (the latest) but it still needs work. I completely skipped 4.0 and 4.1 and I run from it as if it was Vista and I am waiting for 4.2 to come out. So please don't take criticism bad. You get it because someone likes and appreciates your efforts and wants you to succeed. "
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-16
    body: "Sure. Negative comments with totally unspecific and useless criticism is the mark of someone who wants to help..."
    author: "hmmm"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "How about live preview? You set the effect in a dialog and it previews it as you set it?"
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "Yes, Krita does that."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2008-12-13
    body: "Don't tell me you're evaluating just by looking at the screenshots."
    author: "Axl"
  - subject: "Re: Krita"
    date: 2008-12-14
    body: "So let's have a look.\n\nTry to perform a simple task:\n-add a blur effect mask to a CMYK image or a 16 bit RGB image\n\nAs far as I can see Paint.NET does not have masks, color management or support for more 8 bit RGB. It's simply comparing apples and oranges.\nOf course an app that does only 8 bit RGB can always be faster than an app that does a lot more.\n\nAlso note that this is still a beta, so there will be further improvements.\n\nIf you want performance and open source doesn't matter buy Photoshop. \n"
    author: "Sven Langkamp"
  - subject: "Re: Krita"
    date: 2008-12-14
    body: "Well Ok but in my opinion the video should be showing a nice art work which would already be done with Krita - as in, \"lets see what it can do\" (just like Tux and the GIMP). Currently it is only showing a few lines drawn loosely and that is what triggered me off a bit to post the comment above. It is like using MS Paint. Show me some gradient fills, texture fills in \"selected areas\", image resizing, adding borders, adding transparency (just select which color to be transparent), transparant image pasting, water marks etc. I hope you have seen that YouTube video I posted.\n\nNow in relation to Photoshop, well, we all have limits don't we? :) There are cheaper alternatives to PS, you have PhotoFiltre Studio, Paint Shop Pro but nothing beats free.\n\nMost of the above mentioned features are already implemented in PhotoFiltre free except for the texture fills (PSP does this). Just select an area and fill it with a texture.\n\nI have not used Krita but that's why we have demo videos so we can see the cool stuff.\n"
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-14
    body: "The thing is, we wanted to show of _new_ stuff. Gradient fills, texture fills, image resizing, adding border, transparency -- all that stuff was already available in 1.4. Been there, done that, still got the code and the functionality."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2008-12-14
    body: "Oh that's great then! As soon as KDE 4.2 gets released or the developers of KUbuntu, PCLinuxOS or Mandriva create a Live CD, you can be sure I'd be giving this a try. I didn't know about Krita before. It's good to see there is an alternative to the GIMP. If it really has all the stuff I mentioned and of course I believe you then this is just great! I am a C++ developer for Windows but for business software only and I am clueless when it comes to graphics and canvases etc. I use PhotoFiltre primarly when it comes to editing/drawing toolbar icons. I really to try to do some work with QT/KDE, develop some type of open source database program with MySQL support."
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-14
    body: "I meant \"I want to try\" and also thanks for addressing my concerns. I really believe in KDE and I know it has a great potential.\n"
    author: "Observer"
  - subject: "Re: Krita"
    date: 2008-12-14
    body: "Hiya,\n\nThe video is pretty cool; it shows a really cool set of features in a short time, and in a playful way too :)\nThe fact that the blur-filter is added to the layer-stack is pretty darn cool. For those that didn't get it; you can add a filter and instead of applying it once, it will be remembered and any further modification to the layer will also be blurred.\nSame with the invert.\n\nI have to admit I can see the points of poster with the nick \"Observer\".  Creating a screencast is pretty darn hard; having a voice over or some helpful text on screen. As well as having a couple of pictures prepared helps. I think the hand drawn thing is funny; but we all know that people like pretty things and this hand drawn distracts :)"
    author: "Gary"
  - subject: "Basic functionality"
    date: 2008-12-14
    body: "That's a bit misleading. Krita always seemd to me as not supporting the baseline features very well but concentrating on fancy new stuff.\n\nBasics as image resizing had severe bugs in stable versions and filters like unsharp mask were (are?) very slow for large kernel sizes. \n\nEven selecting basics like \"Filter\"-\"Adjust brightnes/contrast\" makes the interface freeze for several seconds. (Krita 1.6.3). Filter preview also takes very long. And that's with a simple Canon 5D JPEG. I don't want to know how slow it will be with 16bit/channel images.\n\nMy recommendation:\nIf you want that people use krita (and some of them to become developers) then make sure that the basic functionality works extremely well.\nIf the basic functionality does not work well, everyone will use gimp, Digikam, Bibble, etc.\n"
    author: "Max"
  - subject: "Re: Basic functionality"
    date: 2008-12-15
    body: "I completely agree and have been put off by Krita because of this exact reason, enough though I would love to use it and have donated money towards it."
    author: "JoeSchmoe"
  - subject: "Re: Krita"
    date: 2008-12-16
    body: "1. \"knowing...\" Ok. Troll\n2. Ah? You know of other Free apps with effect layers? \"Business market...\" Yup. Troll indeed.\n3. Apples, oranges. choice os an MS product for comparison... Trollish.\n4. Non sequitur.\n5. Ok. Failure to apply logic. Exists B not null such that B = A AND (NOT A). Interesting.\n\nBasically, you are a troll, and not a very good one. Work on it, you'll get better.\n\nAlternatively, try to grow up."
    author: "hmmm"
  - subject: "A possible showstopper"
    date: 2008-12-14
    body: "I really like the look and feel of the new KOffice in comparison to OpenOffice, and I would like to give KWord a try, but there is a big obstacle for me:\nFor my profession I have to insert lots of special characters, partly even not implemented in Unicode.\nIn most cases I can use the keyboard - in fact, this is the main practical reason for me to use Linux -, but sometimes I need the pop-up, and I must be able to select some definite signs form definite fonts.\nAnd the solution of KWord is quite horrable, the window shows signs form different fonts, and it took me ten minutes to find a special character, in some cases I would have to click me through three pull-down menues and after that scroll down to the sign needed.\nThe search field is useless: I don't know from which font the signs displayed come from - and if can type the sign for search, I don't need the pop-up anyway, because I can type it in the document.\n\nIs there any possibility to switch the special character view to something like in M$ Word or OOo?"
    author: "Cyril Brosch"
  - subject: "Re: A possible showstopper"
    date: 2008-12-15
    body: "I don't follow your usecase; for most people the new font finder is a definite win as being able to type 'euro' or 'pound' to get that glyph is very useful.\nKnowing where in the code tables your wanted glyphs is, as required by the previous dialog, is not so useful. (who knows that the euro is in table 31!)\n\nSo, while I understand you don't like our change, I'm afraid your usecase seems quite special and we have to accept that one solution doesn't work for everyone.\n\nNaturally you can create or ask others to create a docker-plugin using the koffice plugin structure to do exactly what you want.  Its quite trivial to do, actually ;)\nFind me on #koffice if you want to find out how to do this. A good start is at http://techbase.kde.org/Development/Tutorials/KOffice_Overview (topic Dockers specifically)"
    author: "Thomas Zander"
  - subject: "Re: A possible showstopper"
    date: 2008-12-15
    body: "Thanks for the answer.\nI get your point and I understand the advantages of the new interface, and with the help of your examples I was even able to find some often used signs quickly - I don't want the interface to be changed.\n\nBut the problem are the signs from the user area: These are not many, but I need them often, and they don't have a description by which I could search them.\nI think there a two possible solutions: In the dialogue box there could be a field showing the signs (+/- 10) used recently (I think this is in newer versions of M$ Word), or there should be a possibility to assign a name to the signs in the user area (or to custom all names).\nUnfortunately I can't script, so I don't know whether such additions are complicated or possible."
    author: "Cyril Brosch"
  - subject: "Re: A possible showstopper"
    date: 2008-12-16
    body: "Have a look at the Plasmoids in KDE 4.2 Beta 2. There is one that does what you are searching for: A nice character table where you you can browse characters and just click one character which gets inserted.\n"
    author: "Arnomane"
  - subject: "Re: A possible showstopper"
    date: 2008-12-16
    body: ":-) I know why I love KDE4!"
    author: "Cyril"
  - subject: "why koffice?"
    date: 2008-12-14
    body: "I mean, it's quite sad. The passage to KDE 4 is likely a drop for a lot of good\napps, as basket is actually discontinued, k3b seems stuck, tellico ,kaffeine and quanta don't have even an alpha running in kde4.\n\nAnd in the meanwhile, plasma breaks and gets reworked every month, many core features of kde4 are changing from time to time, the port of other apps is really hard (quite a lot of changes and not so much tutorials/doc about porting).\n\nBut... guess what? The office suite that almost nobody really uses (how much tutorials for krita or karbon are in the web, how much compatibility with oasis standards???!?) is getting developed. Much more than the great apps above. And\nguess what? There isn't a better, or even on-pair alternative for any of those,\nwhile Openoffice still rocks."
    author: "pleasedontpushmetognome"
  - subject: "Re: why koffice?"
    date: 2008-12-14
    body: "\"why koffice?\"\n\nBecause the developers of KOffice want to work on KOffice; simple as that. "
    author: "Anon"
  - subject: "Re: why koffice?"
    date: 2008-12-14
    body: "I use krita a lot. It is a wonderful app. Being a photographer, the only-8-bits of gimp are useless for me.\nKoffice have antialiased vector graphs and support for otf fonts, something that OOo don't have. \nKpresenter 2 will have support for two monitors out of the box, in OOo you need to use an unstable extension.\nKpresenter 2 will have multimedia support through phonon, in OOo you need to use the outdated \"java media framework\" (I'm talking about Linux) or the unstable go-oo based builds.\nKword 2 will match (more or less) Writer in styles management.\n...\nMay I continue?\nOK, I'm a Writer power user and know quite well that Kword 2.0 will not fit my needs... yet, but considering the evolution of both, OOo --which started from the full featured code of staroffice 5.2-- and Koffice --which started from scratch years later-- I may say that Koffice project is a \"must\". "
    author: "RGB"
  - subject: "Re: why koffice?"
    date: 2008-12-14
    body: "Ehm, gimp has more than 8 bits from 2.6 ,so you can finally use a real photo\napp now (there's even sox actually).\n\nI tried (hard!!!!) to substitute inkscape with karbon to not need to install pango and some other gtk deps, I had to give up after 3 days.\n\nDunno about antialiased graphs or 2 monitor presentation, both kword and kspread\nare ages behind writer and calc, and the compatibility is ridiculous.\n\n"
    author: "doh"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "> Ehm, gimp has more than 8 bits from 2.6 ,\n\nNope. Gimp 2.6 is using GEGL for the first time in some way. However it still doesn't offer 16 bit color channels (the 2.6 announcement was a bit misleading with regard to this).  "
    author: "Torsten Rahn"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "> Ehm, gimp has more than 8 bits from 2.6 ,\n\nNope. Gimp 2.6 is using GEGL for the first time in some way. However it still doesn't offer 16 bit color channels (the 2.6 announcement was a bit misleading with regard to this).  "
    author: "Torsten Rahn"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "\nYou should whine (yes, whining) for Kaffeine, Tellico and other application developers than for Koffice developers. Unless same people is developing more than one, there is nothing to do. \n\nI would like to see K3b, Kaffeine and few other applications ported KDE4 now, but because I cant code, I try to give my support by other ways, easiest and most powerfull is to report bugs and wishes what helps developers to enjoy what they do when they do not need to take care from everything alone. "
    author: "Fri13"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "kaffeine and k3b are ported to kde4, don't know what distro you use, but they are available in opensuse's repositories and have been for quite a while now.  "
    author: "anon"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "No, they are both Qt3 apps; KDE3 applications running under KDE4."
    author: "ColinP"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "They are?\nhttp://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Extra-Apps/openSUSE_11.0/i586/kde4-k3b-4.1.3.svn887510-2.6.i586.rpm\nhttp://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Extra-Apps/openSUSE_11.0/i586/kde4-kaffeine-4.1.3.svn887511-2.5.i586.rpm\n\nDon't look like it to me."
    author: "Morty"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "I agree with you.\n\nKoffice is rather, awful.  I downloaded the latest release, and it didn't really last long on my computer.  The layout is a nightmare, and the whole look, awful.  It actually reminds me in same ways about kmail, and that is, that they both look and feel a decade behind the competition.\n\nI'm more excited about openoffice going to release a new gui for their office software, That might finally get me away from using Microsoft Office, but Koffice, I really wouldn't recommend it to anyone that I know.  Sorry, but that is the truth.  And it appears only a loyal few use it, the vast majority use OpenOffice.  Even Lotus's free office software blows this away."
    author: "anon"
  - subject: "Re: why koffice?"
    date: 2008-12-16
    body: "> It actually reminds me in same ways about kmail, and that is, that they both look and feel a decade behind the competition.\n\nBad example, considering the massive rework of message presentation in KMail for 4.2."
    author: "Stefan Majewsky"
  - subject: "Re: why koffice?"
    date: 2008-12-15
    body: "Yawn!  Yet more uninformed trolling by someone who doesn't understand the 'Community' part of 'The KDE Community'...\n\nKaffeine and K3B are being ported and are currently in alpha versions, it just depends on when their volunteer authors get the time to work on them.  \n\nQuanta last I heard no longer had a full-time paid developer working on it so of course the pace would drop, expecially as the underlying kdevplatform isn't finished yet.  \n\nBasket, looks to me like someone is working on it slowly, feel free to step up and help them out.\n\nPlasma is _not_ being reworked every month, the underlying library code is now so stable it's in kdelibs, and the desktop has matured nicely for 4.2.  If you're seeing plasma breakages you must be running the dev version, which means you have to expect things to be broken.\n\n\"many core features of kde4 are changing from time to time\" - Like what? And is change a bad thing?\n\n\"the port of other apps is really hard\" - You realise most of the porting issues are down to Qt?  And it depends on the app really, and whether you just want a straight-port or if you want to take the chance to rework your architecture at the same time.  You make it sound like you're a coder, so how's about telling us what docs are lacking, or better yet jump in and save one of the projects or features you're missing.\n\nReally, the only sad thing here is you."
    author: "odysseus"
  - subject: "hmmm"
    date: 2008-12-15
    body: "I do not want to offend, and I think Krita goes in an ok direction.\n\nPersonally though I am more interested in how KOffice can work as replacement for OpenOffice. I hate OpenOffice. I think Abiword is better but Abiword lacks so many small things, and also developers. :( A sad thing the situation with openoffice ...\n\nNow, about Krita, I think the usability seems a bit annoying. Can the windows be grouped instead of pop-up-ed? I hate those popup designs like Gimp. Gimp suffers a similar UI problem with those popups to apply filters. I rather use key combinations only to apply filters and would like to not popup any window (or if there is a \"popup\" i want it to appear in some right pane or so, where it does not distract me. A bit hard to describe what I want, but easy to notice what I do not want to have. Forced Distractions is something I dont like in general :D\n\nAlso, the elements all look very stylish but are they usable or customizable? Would be interesting to group elements differently and see if it makes a difference on the workflow.\n\nOr in other words, whether Krita has some advantage compared to Gimp (i love and hate Gimp somehow.... you can do really nice effects with it. But the UI i really really hate, despite the improvements made)"
    author: "mark"
  - subject: "Development"
    date: 2008-12-17
    body: "The KDE developers are doing extraordinary work. Everything that comes out for KDE4 really looks great usability wise.\n\nBut the long development cycle of KDE4 (which is not yet finished). So I wonder what can be done to better scale the development community and recruit new persons. Is there a kind of KDE-from-Scratch project? Actually it requires quite a lot of experience to set up a development environment and when you happen to work on fedora be sure the development packages have other names etc.\n\nI would really like to contribute but I don't even manage to compile KDE and I also don't want to shoot my productive system which I require for my daily work.\n\nSo I have e.g. a 16 GB USB stick. Is it possible to install a complete development environment on a usb stick? Software Development is real fun but some entrance barriers are there."
    author: "Andre"
  - subject: "Re: Development"
    date: 2008-12-17
    body: "I'd be wary of trying to fit it on a usb stick: in order to compile KDE you're going to write lots of small files, which is not great on a usb stick. But an external hard disk you can boot from could be ideal. And there's a great tutorial on techbase on setting up a kde environment on your work machine with disrupting anything: http://techbase.kde.org/Getting_Started/Increased_Productivity_in_KDE4_with_Scripts. Even though I'm a full-time KDE4 user now, I still use that setup to separate my production KDE4 environment from my hacking environment."
    author: "Boudewijn Rempt"
  - subject: "Re: Development"
    date: 2008-12-17
    body: "Great!"
    author: "Andre"
  - subject: "kde4"
    date: 2008-12-17
    body: "not bad but i think kde team should complete kde4 first:("
    author: "freeman"
  - subject: "kde4"
    date: 2008-12-17
    body: "not bad but i think kde team should complete kde4 first:("
    author: "freeman"
  - subject: "kde4"
    date: 2008-12-17
    body: "not bad but i think kde team should complete kde4 first:("
    author: "freeman"
  - subject: "odt filters ?"
    date: 2008-12-18
    body: "I am having little success importing openoffice doc saved as odt - only a couple of lines are imported and then the rest of the doc is blank pages.\n\nAnyone else seeing this ?"
    author: "vm"
---
Moving towards the 2.0 release with monthly increments of
improvement-goodness, the KOffice team has once more honoured
its promise to bring out beta releases of KOffice until the
time is right for a release-candidate release. So, before <a
href="http://dot.kde.org/1227137279/">the news of the previous
beta</a> has had a chance to scroll off the Dot news page,
we bring you this beta with many, many improvements across
the board. Incremental as it is, we think it is a genuine
and important step towards a final release. So here it is: <a
href="http://www.koffice.org/announcements/announce-2.0beta4.php">full
announcement</a> and <a
href="http://www.koffice.org/announcements/changelog-2.0-beta4.php">changelog</a>.





<!--break-->
<p>Some highlights in the release announcements: loading of text
styles in presentations saved by OpenOffice.org is fixed, inserting an
image in a document becomes possible again and the first set of results
from the <a href="http://dot.kde.org/1226409524/">Berlin meeting</a>
become integrated into KOffice. And, of course, plenty of bugfixes all
over! And the translation teams have been really busy, and many bugs in
the localisation support have been caught.

<p>It's now possible to paint on mask in Krita:</p><center><a href="http://cyrille.diwi.org/images/kritablog/krita-effectmask.mpeg"><img src="http://cyrille.diwi.org/images/kritablog/krita-effectmask.th.png"></a></center>

<p>It is really, really time to start helping the developers, by testing
and finding bugs, by writing documentation, by helping out with the
website or even by helping to get KOffice release ready by writing code
and fixing bugs. We intend to release the very first version of KOffice2
in a few months: so let's pull together and make this a great platform
for productivity software!

<p>And... Next month you'll get another one!






