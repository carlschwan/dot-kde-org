---
title: "BugSquad turns its attention to Amarok  this Sunday!"
date:    2008-06-11
authors:
  - "aspehr"
slug:    bugsquad-turns-its-attention-amarok-sunday
comments:
  - subject: "Authentification key for neon packages?"
    date: 2008-06-11
    body: "After this operation, 292MB of additional disk space will be used.\nM\u00f6chten Sie fortfahren [J/n]? j\nWARNUNG: Die folgenden Pakete k\u00f6nnen nicht authentifiziert werden!\n  amarok-nightly-qt amarok-nightly-strigi amarok-nightly-kdelibs\n  amarok-nightly-kdebase-runtime amarok-nightly-taglib amarok-nightly-tools\n  amarok-nightly\nDiese Pakete ohne \u00dcberpr\u00fcfung installieren [j/N]?"
    author: "andy"
  - subject: "Re: Authentification key for neon packages?"
    date: 2008-06-11
    body: "Diese Pakete ohne \u00dcberpr\u00fcfung installieren [j/N]? j\n\n:D\n\nok, that was lame... but fun :P"
    author: "Jakob Petsovits"
  - subject: "Re: Authentification key for neon packages?"
    date: 2008-06-12
    body: "Yea this is a problem with Launchpad. PPAs can not be authenticated yet. We are sorry about that but there is not much we can do about it for now."
    author: "Lydia Pintscher"
---
This coming Sunday, June 15, <a href="http://techbase.kde.org/Contribute/Bugsquad">BugSquad</a> is hosting a BugDay to go triaging through old <a href="http://amarok.kde.org">Amarok</a> bugs. Come join <a href="http://irc.freenode.net/#kde-bugs">#kde-bugs</a> anytime to get training and help out! No prior experience is necessary, and you don't need any programming knowledge. 






<!--break-->
<p>If you are a Kubuntu user, there is a nightly build called <a href="http://amarok.kde.org/wiki/User:Apachelogger/Project_Neon">Project Neon</a> that you can use.</p>
<p>Joining Bug Days is a great way to help the KDE project. The only things you need to take part are a computer running a recent KDE 4 with Amarok 2 and a KDE 3 version of Amarok 1.4.9.1, and an internet connection. You can join at any point during the day, and stay for as long or short a time as you like. If you are a KDE user and have been wondering how to contribute, then this is a great way to get started - there will be plenty of experienced Bugsquad members on hand to answer any questions you might have. To get involved, join our IRC channel anytime, #kde-bugs on irc.freenode.net, where we will help you get underway. The day will begin at 0:00 UTC (<a href="http://www.timeanddate.com/worldclock/meetingtime.html?day=15&month=6&year=2008&p1=0&p2=37&p3=224&p4=179">other timezones</a>) on Sunday and continue until the day is over throughout the world, although people tend to stay later!</p>

