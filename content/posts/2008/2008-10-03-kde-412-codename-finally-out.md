---
title: "KDE 4.1.2 \"Codename\" Finally Out"
date:    2008-10-03
authors:
  - "skuegler"
slug:    kde-412-codename-finally-out
comments:
  - subject: "Congrats!"
    date: 2008-10-03
    body: "I'm sure this one is great. I just can't WAIT for 4.2.0!! :)"
    author: "tsb"
  - subject: "Re: Congrats!"
    date: 2008-10-04
    body: "Me too :-)"
    author: "Ray"
  - subject: "Re: Congrats!"
    date: 2008-10-06
    body: "I second that.\nI was using KDE Neon Nightly build and it was really much better than 4.1.X."
    author: "Iuri Fiedoruk"
  - subject: "KHTML "
    date: 2008-10-03
    body: "from changelog, it seems it is kthml bugfixes release !!\n\n Please keep it up."
    author: "Zayed"
  - subject: "Re: KHTML "
    date: 2008-10-03
    body: "And khtml was already greatly improved in 4.1.1\n\n"
    author: "JC"
  - subject: "Re: KHTML "
    date: 2008-10-05
    body: "Well, as one of the silent majority of KDE users who have already switched to Firefox (judging from my friends; but also from all \"desktop Linux votings\" which showed KDE as favorite DE, but by far Firefox as favorite browser), your comment encouraged me once again to try out KHTML. Well, what should I say - I'm still using (i.e. forced to use:( ) Firefox :-/.\n\nDear KDE and KHTML devs, I appreciate your work, often enough done in your free time for no money at all! Especially when hearing that there are only three active KHTML devs, it's astonishing that KHTML is still in a reasonably good state. But, let's face the fact, the WWW is rapidly evolving and other browser engines pour resources (devs and money) into the development at a rate that obviously can't be matched by volunteer-driven KDE.\n\nYou say you need more devs - fair enough. But let's face it - WebKit (backed by glamourous Google and trendy Apple) and Mozilla are probably simply more \"sexy\". Considering the fact that Qt 4.5 and present Plasma already incorporate WebKit, KHTML might also have - despite the official KDE stance - the mental label \"legacy\" for many devs (and users - e.g. me).\n\nYou say you need more bug reports - also fair enough.But see e.g. that infamous \"Ebay\" bug #141907. First reported over 1 1/2 years ago - still not fixed. First developer response only a few days ago! It's obvious that the problem is one of manpower, however, considering this response time to bug reports (of major sites!), you cannot blame users for not taking the time to report all the other bugs.\n\nI also cannot understand the animosity against WebKit. For me, WebKit, evolved from the KDE technology KHTML, is one of the (many) great success of the KDE project. The fact that major corporations switched to a KDE technology instead of e.g. Mozilla shows the quality of code you KDE devs are producing. So re-integrating WebKit for me \"psychologically\" (if not technically, OK) would only be \"bringing home again\" native KDE technology. KDE has a great history of integrating community- and corporate backed technologies (Qt!!!!) and creating a free desktop experience out of the best of both worlds. What would be wrong of doing it again - WebKit will never legally be less free than the KHTML code it was originally based on!\n\nI inititally didn't plan to write that much, sorry if it sounds like a rant to KHTML developpers. As I said before - I appreciate all your work and the time you invest in this project. However, maybe it's time for a re-evalutation.."
    author: "John Holtz"
  - subject: "Re: KHTML "
    date: 2008-10-05
    body: "> But, let's face the fact, the WWW is rapidly evolving and other browser\n> engines pour resources (devs and money) into the development at a rate that\n> obviously can't be matched by volunteer-driven KDE.\n\nwe have had idle bystanders express that kind of \"let's face it\" argument since very early in the project.\nI know it's hard to understand for non-developers, but it simply does not work like that. Development 'speed' does not simply correlate to the number of developers, as is examplified by the fact that we are still lively and kicking after ten years of development with an average of 3 active developppers at any point in time.\n\n> You say you need more bug reports - also fair enough.But see e.g. that\n> infamous \"Ebay\" bug #141907. \n\nthere is no such \"infamous\" bug, because the incriminated menu works fine,\nas is stated in the Bug Report comments, should you care to even read them.\n\nthe bug remained open because noone cared to close it yet.\nDone, thanks.\n\n> First reported over 1 1/2 years ago - still not fixed.\n\ndo you have any idea of what the time frame is for 'fixing' a bug that's not even been confirmed, and does not come with a test case?\ndo you have even an idea how absurd a metric that is?\n\nI'll give you a glimpse : as of today, the number of bugs created on or before 1 and 1/2 years ago that remains open in the WebKit project is 999!\n\nthat must show obviously that webkit developers don't care about their users? ;-)\n\n> I also cannot understand the animosity against WebKit. For me, WebKit,\n> evolved from the KDE technology KHTML\n\nand that is why you are wrong, there is absolutely no fundamental animosity against WebKit - it is another project and we don't have the same goals, that's all.\n\nThe fact that it evolved from the same initial code base is irrelevant... similarly, mice and humans share 80 percent of their DNA code base, and that doesn't make for any special relationship between the two species.\n\nIf anything, the animosity has always been the other way around.\nAs most corporate baked projects - especially Apple's (!!), WebKit strives more toward software monoculture. It is making aggressive moves for instance to threatens Mozilla's Gecko on their own grounds.\n\nKHTML doesn't have any problem with software diversity, so it doesn't have a problem with WebKit's existence.\n"
    author: "Spart"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "So basically, the story is Konqueror will continue to use KHTML, and KDE will continue to not have a usable web browser. Instead, people will have to use the non-integrated Firefox. Very unfortunate.\n\n"
    author: "Longtime_KDE_user"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "You make two assumptions:\n1) That KHTML will always be behind other engines: KHTML is moving at a very quick pace, considering that it only has about three developers actively working on it. KHTML also is not burdened by various things that take up Gecko and WebKit developer time (think platform ports). \n\n2) That Konqueror will only use KHTML: Konqueror (as a testament to KDE technology) supports hot-swappable engines (for example, Konqueror will use Okular to render PDFs). A Konqueror engine (KPart) for WebKit is currently being developed and already displays and navigates web pages.\n"
    author: "Michael \"KHTML\" Howell"
  - subject: "Re: KHTML "
    date: 2008-10-07
    body: "3) That Firefox will always be non-integrated. There is a Qt port of firefox in the works, I'm told."
    author: "KOffice fan"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "Note: KHTML is really _just_ an HTML rendering engine. WebKit encompasses WebCore (KHTML), JavaScriptCore (KJS), and KWQ (the other assorted KDELibs and Qt libraries KHTML uses). I identify the KDE web stack as WebKDE throughout the rest of this comment. Maybe a better name could be developed by the KDE Marketing Team ;). Obviously, I'll use the terms WebCore, JavaScriptCore, KHTML, and KJS to identify the respective parts of the respective web technology stacks.\n\n> that must show obviously that webkit developers don't care about their users? ;-)\nWebKit has many more users than KHTML does. Therefore, WebKit will have more reported bugs. Also, WebKit probably has quite a bit of \"fixed but nobody bothered to close\" bugs as well ;).\n\n> and that is why you are wrong, there is absolutely no fundamental animosity against WebKit - it  is another project and we don't have the same goals, that's all.\nAs some examples:\n1) WebKit is made up of a set of ports. To move the burden of porting off of the rendering engine-developer's back (where it does not belong), the WebKDE developers chose one (portable) toolkit and stuck with it.\n2) WebKDE sticks to a strict KISS design, whereas WebKit does not. Ironically, Apple chose WebKDE for their base exactly because the code base was so simple and easy to hack.\n\n> If anything, the animosity has always been the other way around... KHTML doesn't have any problem with software diversity, so it doesn't have a problem with WebKit's existence.\nWell, except that may WebKit threaten WebKDE's existence, since WebKit inherits many of WebKDE's advantages (speed, relative simplicity, good design, etc) and has been ported to Qt.\n"
    author: "Michael \"I attack everyone!\" Howell"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "> there is no such \"infamous\" bug, because the incriminated menu works fine,\n> as is stated in the Bug Report comments, should you care to even read them.\n> the bug remained open because noone cared to close it yet.\n>  Done, thanks.\n\nWell, it's still there for me (only gone when using an alternative Ebay layout still considered \"beta\" by ebay).\n\nThe way you deal with this bug is symptomatic.... no dev ever cared to ask the reporter about details or about possible test case, the only answers where \"works for me\" or \"i don't want to create an ebay account\". And now it's simply closed.\n\nBye, KHTML."
    author: "Uset"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "> The way you deal with this bug is symptomatic.... no dev ever cared to ask the\n> reporter about details or about possible test case, the only answers where\n> \"works for me\" or \"i don't want to create an ebay account\". And now it's\n> simply closed.\n\nright, no one cared... the trolling is becoming completely silly here ;(\n\n- comment #8 will tell you that I went all the way to *buy an actual object* on\n  the ebay site to check if the bug existed. It did not!\n\n- I then obviously asked for a test case in comment #10 or may be I don't know\n  how to read.\n\n- eventually comment #11 confirms my findings, that the bug is no longer\n  reproducible on the incriminated site, and does not speak in anyway of a beta\n  interface of any kind.\n\n- So no problem? Good, closing with a comment asking to reopen if a real\n  instance of the bug is observed or a test case is provided!\n\nSo my question now is : have you been trolling here about this bug report for a week under various identities because you have an actual problem with this bug? \n\nin which case you are taking the wrong approach, you should reopen the bug and fill it with the details that you apparently have and that we don't.\n\nthank you."
    author: "Spart"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "No I'm not trolling under different names... maybe there are more people out there encountering difficulties, you know, after all, we are talking about EBay ;). I just took this bug because the bugs of KHTML with (at least German) Ebay got on my nerves, too, and some other poster in another thread identified the bug ID and therefore it was easy to reference.\n\nDon't take it personal, but see it from the end user perspective: The thing is, I, as a normal user, see that a bug making a popular page which is often used by me, completely unusable can persist in the bug database for over a year without any activity. \n\nI switched then, sadly and hating it, to Firefox, because in this and other cases I saw no progress.\n\nI appreciate that you can't do anything and many bugs, seemingly this one, too, are really difficult to track down. But, e.g. you asked for a test case. I must admit that I really would not know what you mean with that, or how I, an user not into web programming, could you provide with that! Maybe therefore no more feedback came, because the reporters of this bug didn't get a clue how they could help the programmers in fixing this bug and looked for alternatives? \n\nBut no need for further discussion, this always ends in flaming. I tried to put my criticism in better words and better founded than \"KHTML sucks\" - which it does not."
    author: "John Holtz"
  - subject: "Re: KHTML "
    date: 2008-10-07
    body: "A testcase for a browser is usually just a URL, but in cases like this where cookies and sessions are involved, you probably have to give both a URL and step-by-step instructions of how to get from the URL to the bug."
    author: "Kevin Kofler"
  - subject: "Re: KHTML "
    date: 2008-10-07
    body: "> No I'm not trolling under different names\n\nWell, since that question was a reply to the message written by \"Uset\" you are at least using 3 (two in this thread and one in the report) different names for one topic within a short time. Don't take it personal, but...\n"
    author: "dipesh"
  - subject: "Re: KHTML "
    date: 2008-10-06
    body: "OK, I second that. I've tried to say that before, but you use english far better than me and is much more polite also! :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KHTML "
    date: 2008-10-11
    body: "Have you ever counted the number of developers on some of the other projects? \n\nIf you want to use that as an arguement, we might as well pack up and go home, because KDE can always use more developers/contributers. \n\nCan't code? Art, docs, bugs.\n\nCan code? #kde-devel and offer yourself up. Or ask your favorite program if they need help. Hint: most of the non-sexy things could use more developers..."
    author: "hahaha"
  - subject: "opensuse did it again"
    date: 2008-10-03
    body: "\n\nif you want autohide panel, and icons on desktop ala kde3.5 way, and a lot of improvement to the plasma workspace, all you have to do is just installing kde4.1.2 from opensuse, see http://en.opensuse.org/KDE4 for further information.\n\nAnd I want to say big thanks you for the kde developers, as a user who use exclusively kde trunk ( thanks for the suse build service) I have remarked with a great satisfaction that the trunk branch is very usable, and it is no more   a place where developers make experimental codes ;)\n\nThanks really for your work, and thanks sebastian for this sentence  &#8220; The next feature release of the KDE workspace and applications &#8220;                        I hope someday we will use instead &#8220;  The next feature release of the plasma workspace and kde applications&#8221;\n\n\nfriendly."
    author: "mimoune djouallah"
  - subject: "Re: opensuse did it again"
    date: 2008-10-03
    body: "Is autohide only on opensuse or other distros have this?"
    author: "weintor"
  - subject: "Re: opensuse did it again"
    date: 2008-10-03
    body: "' Is autohide only on opensuse or other distros have this?' afaik it is only opensuse, many disto stick to the official release. "
    author: "mimoune djouallah"
  - subject: "Re: opensuse did it again"
    date: 2008-10-03
    body: "all distros will have it as of 4.2.0, including bug fixes which the code the suse team backported is still lacking.\n\ni'm happy that suse users can get access to some features quicker ... i'm a little queasy about backporting features that aren't complete.\n\noh well, just don't hold it against me if you discover the couple things still missing from autohide ;)"
    author: "Aaron Seigo"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "How about you do those bugfixes before December and we will backport them for our release? :-)"
    author: "binner"
  - subject: "Re: opensuse did it again"
    date: 2008-10-11
    body: "Fedora Rawhide now has it, we're currently evaluating if it will stay for the Fedora 10 release and if it's good enough to push out as a Fedora 9 update. (A Fedora 9 build of kdebase-workspace with autohide (kdebase-workspace-4.1.2-4.fc9) is sitting in our build system \"Koji\" and getting some testing too.)"
    author: "Kevin Kofler"
  - subject: "Re: opensuse did it again"
    date: 2008-10-03
    body: "You make it sound like SuSE did a good thing...\n\nWhere KDE (and just about every other project) has stable branches to avoid introducing bugs and regressions, SuSE then puts various features in there which are not even stabalized for release for 4.2 ?\n\nDo I understand that correctly? If thats indeed what is happening and users find bugs in there (and they will!) I just hope its not going to be held against KDE.\n\nCan someone tell me if I'm missing something obvious here?"
    author: "Dave"
  - subject: "Re: opensuse did it again"
    date: 2008-10-03
    body: "All distros customize packages to some degree, which can include backports.  They also maintain these backports, so that if there's a problem, they can put out a fix right away.  They all encourage users to file bugs with the distro first.  This system has been working pretty well for quite a while now."
    author: "pantsgolem"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "Ok, all distros do \"some\" customization of \"some\" packages somewhere, but Slackware for example tries to keep it to an absolute minimum and ships almost 100% what they get from upstream.\nThis is actually a *very* nice thing since you get the exact behaviour that upstream developers expect you to get and when you find bugs and report them the bugreports are always relevant since it's never due to some distro patch that stuff broke, and generally things \"just work\". I've always wondered why so many distros patch so much stuff when upstream un-modified usually just works...\nI wish more distros would do what Slackware does."
    author: "JJ"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "btw in the case of opensuse it just backported a snapshot of plasma trunk, just to provide some urgent features for people who use kde4.1.x, anyway as long as they ship the same kde4.1.2 platform ( kdelibs, kdesupport, kderuntime, qt etc) all the other modules are just apps. a newer version never heart anybody ;)\n\n "
    author: "mimoune djouallah"
  - subject: "Re: opensuse did it again"
    date: 2008-10-06
    body: "openSUSE customize stuff. AFAIK, they always did. They started Kickoff for instance, which is the default menu in KDE 4 right now.\n\nThey have their own usability folks and want to give the user their own, sometimes slightly different, experience.\n\nDistributions have the benefit to be right between the user and the developer, hence they can do small adjustments to fit their needs. I wouldn't think the dot and planet reader or kde mailing list subscriber is the average KDE user - only the tech freaks. Your average everyday n00b goes to the openSUSE forums and complains about missing autohide there.\n\nTo sum this up: I think it is very important that they offer highly demanded features before upstream does, at least if they do it with their own time and money, and respond to bugs themselves. The main reason why I use openSUSE is the unmatched KDE 4 support and the clever customization. If I wanted vanilla, I'd use LFS or, you name it, Slackware. I don't."
    author: "fhd"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "Yeah, pretty much I am with you and there have been instances in the past where KDE developers have discovered that bugs reported to them have in fact been introduced by distros messing with the code.\n\nHaving said that, openSUSE have the manpower and expertise to generally do a fairly good job of their tweaks and backports - I say this as a former (although not current) suser. Unfortunately my experience has been that Kubuntu tweaks/backports have introduced more issues and there was an interesting issue with Amarok and Fedora a while ago where Fedora made an unstable backend the default (they had their reasons and this has since been resolved). But generally It think following close to upstream has advantages - as Slackware, Arch (without KDEmod) and, increasingly, Fedora are tending to do."
    author: "Simon"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "opensuse 11 is a piece of crap !!"
    author: "panke"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "that's nice.  Personally i find it to be the distro that suits me.  Why morons have to attack each distro I will never understand it just looks bad for the linux community."
    author: "R. J."
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "But he is right, despite lacking arguments in this post."
    author: "markus"
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "Which you provide ...\n\nIMHO openSUSE is by far the best KDE distro and with their build service you get everything you need prepackaged."
    author: "Chris"
  - subject: "Re: opensuse did it again"
    date: 2008-10-06
    body: "I can only second that, you get everything, the latest packages and all without compiling it yourself."
    author: "Bobby"
  - subject: "Re: opensuse did it again"
    date: 2008-10-06
    body: "ah, no he is not right.\n\nOpenSUSE is jsut like every other distro, it has it's good and minor points, but the attitude of people like the previous poster and yourself is what stinks about the linux community.  This complete divison between distro's where certain twats just troll to give negaitve comments about distor's.  Really it is time people like you grow up and realise while you do this, you harm the entire linux community, and the amazing work that each distro puts into not only their distro's but into the kernal, the desktop environments and every piece of linux\n\n"
    author: "R. J."
  - subject: "Re: opensuse did it again"
    date: 2008-10-04
    body: "GNU/LINUX is a matters of flavours(distros), Slackw is a orthodox distro, debian perfectionist, ubuntu a noob distro, openSUSE is the best distro to me, imo....\n"
    author: "draconiak"
  - subject: "Re: opensuse did it again"
    date: 2008-10-05
    body: "Pfft.\n\nUbuntu noob distro..\n\nMay I rant, too?\n\nI agree Ubuntu is deploying MUCH too little manpower into KDE (and perhaps the kernel ;-) )\n\nBut when compared to Debian, can you just imagine that - perhaps - even the power user just doesn't always want to manually tweak 10,000 things before everything works? This was really a problem with the non-stable Debian releases (where some up-to-date hardware required some new sub-systems Debian stable didn't have)."
    author: "Ulrich"
  - subject: "Re: opensuse did it again"
    date: 2008-10-05
    body: "I respectfully disagree.  I really love openSUSE 11."
    author: "T. J. Brumfield"
  - subject: "Re: opensuse did it again"
    date: 2008-10-06
    body: "I find that to be a very aggressive comment. Not only aggressive, but useless since you give no backing as to why \"opensuse 11 is a piece of crap\".\n"
    author: "Michael \"I am a Kubuntu user\" Howell"
  - subject: "Re: opensuse did it again"
    date: 2008-10-06
    body: "That piece of crap seems to be whipping a lot of asses, even in the Linux world. There must be a reason why it's No. 2 on Distrowatch.\nI bet you are an Ubuntu user. I just hate this type of self-discrimination among Linux users."
    author: "Bobby"
  - subject: "Codename \"Codename\"."
    date: 2008-10-03
    body: "Codename \"Codename\".\n\nWho the HELL came up with this one? x^D"
    author: "slacker"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-03
    body: "\"Someguy\""
    author: "Mark Kretschmann"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-03
    body: "<richmoore> can we make it codename 'codename'?\n"
    author: "sebas"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-03
    body: "i think the next one should be called \"Bikeshed\", making all discussion about the codename particularly delicious. =)"
    author: "Aaron Seigo"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-03
    body: "I don't get it. What's \"Bikeshed\"?"
    author: "dont"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-04
    body: "http://en.wikipedia.org/wiki/Color_of_the_bikeshed"
    author: "Aaron Seigo"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-04
    body: "Kolor of the Bikeshed"
    author: "pvandewyngaerde"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-06
    body: "I happen to think bikesheds are important and a key factor in a complicated issue, namely that of congestion and the question how livable a city is. :-) A great codename for the next KDE release, IMHO!\n\n(Yes, I understand the bikeshed argument, but I choose not to apply it)"
    author: "Andre"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-04
    body: "\"Cyclestorage\"?\n"
    author: "Maarten ter Huurne"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-05
    body: "At least the Akademy participants agree what color the bikeshed is... :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-04
    body: "Eh, for a moment I thought its codename was \"Finally Out\"! :D"
    author: "TheInvisible"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-04
    body: "A good idea for the next release! :D"
    author: "slacker"
  - subject: "Re: Codename \"Codename\"."
    date: 2008-10-05
    body: "I wrote a movie script about retail hell that was far too similar to Clerks (I wrote my script in 1995, and then saw Clerks which made me sad) to ever pursue, but we didn't have a name for the script.  So the working title was simply working.  Given that it dealt with shitty retail jobs, we just stuck with \"Working\" as the title.  I thought it was a cute joke that no one enjoyed but me."
    author: "T. J. Brumfield"
  - subject: "Okular"
    date: 2008-10-03
    body: "Oh, I am so happy about the zoom level fix. Great. There are many simple usability glitches with Okular for KDE 4.1. The worst probably that you cannot deactivate the screen-eating side panel or that sometimes page up-down scrolls through the side panel which is pretty annoying. Anyway, I am confident Okular will rock. Another point I had unter Kubuntu and I guess that's a Windows manager problem is that Kubuntu always opened as a very small window on the upper left part of the screen. You can make the \"click\" test of how many clicks you need to make an application usable for your work, in this case to read the opened PDF file. \n\nI am also happy to see Konqueror progress. It really blows you away how the rendering has been improved."
    author: "Andr\u00e9"
  - subject: "Re: Okular"
    date: 2008-10-03
    body: ">> always opened as a very small window on the upper left part of the screen\n\nyou could use kwin's per window settings (size and position) as a temporary fix. I just love this feature and all my frequently used programs use this"
    author: "DanaKil"
  - subject: "Re: Okular"
    date: 2008-10-04
    body: "You are right on that. If you want to read a pdf file the usual format per page is ISO A4. So a mini window is not really helpful. Okular btw. has an excellent fullscreen mode. I was really impressed by Okular and its functionality but you see a few 'would irritate my mum' glitches."
    author: "Andr\u00e9"
  - subject: "Re: Okular"
    date: 2008-10-04
    body: "> There are many simple usability glitches with Okular for KDE 4.1.\n\nThey won't fix themselves in the night, if nobody tell us about them, for instance.\n\n> The worst probably that you cannot deactivate the screen-eating side panel\n\nLike Settings -> Show navigation panel? (exactly like in KPDF, FYI)\nOr maybe right click on the sidebar, and eg hide the text and/or change the icon size?"
    author: "Pino Toscano"
  - subject: "Re: Okular"
    date: 2008-10-04
    body: "\"Hide panel\" should be added to the panel context menu. That's the first place you look when wanting to hide the gigantic panel.\n\nI spent quite some time before I realized that this option was present in Settings->Show navigation panel.\n\nUsability is never obvious, but please add \"Hide panel\" to the context menu."
    author: "user"
  - subject: "Re: Okular"
    date: 2008-10-04
    body: "Do not confuse usability with what you first think of.\n\n\"you\" are not a representative set of the rest of the humans. As an example all the \"joe users\" (those that *really* need usability) i know never ever in their life would think to use a context menu, they probably don't even know it exists."
    author: "Albert Astals Cid"
  - subject: "Re: Okular"
    date: 2008-10-04
    body: "Likewise, don't confuse \"usability\" with \"catering to the lowest common denominator\".  "
    author: "Anon"
  - subject: "Re: Okular"
    date: 2008-10-06
    body: "I would expect this particular option to be in the View menu. I sometimes may want the side panel, and sometimes not. It is not logical to hide an option that you use once in awhile in a settings dialog, IMHO. For me, having the hide option also in the context menu makes sense, but it does present a usability issue. The issue is not, in my opinion, the context menu itself. The fact that there *is* such a thing already indicates that the concept is not being seen as too troublesome UI wise. The problem is that you may run into the situation that people will not be able to find how to reanable the side panel if they (accidentally?) hide it through the context menu. It should therefore warn the user and explain how to re-enable the panel on closing when that closing is done from the context menu.\n\nOther than that: I think the option belongs in the context menu too. It should just not *only* be in the context menu. No option should be. Every option in a context menu should be available through other ways. The idea of the whole context menu is just to present those actions that make sense for the selected object, so you can find them easily. The normal application menu's are organized in groups of types of functionality, the context menu is organized by what object that functionality applies on. "
    author: "Andre"
  - subject: "hi"
    date: 2008-10-04
    body: "i always wondering, does those who made some apps like kwalletmanage used it before release? first thing i do when launch kwalletmanager is make a propper size all parts of it, like a those small panel with a list of all passwords (i use kde-4.1.1, try to upgrade to 4.1.2, gentoo distro with layman kde-svn), and have you some smell of usabillity, why so much new windows? or you do this small applications to increment count of its on in release, and don`t think about users? maybe better to do more stable and functional release but later than this one? sorry if i was little aggressive, i realy like what you`ve done guys "
    author: "ddosia"
  - subject: "Re: hi"
    date: 2008-10-04
    body: "Actually I'm not really able to understand what your problem is. The best would be that you make a short sketch or mockup to better explain what you mean. Put that one in a bug/wish report and you've done a sensible and helpful contribution to KDE.\n\nStupid questions like \"does those who made some apps like kwalletmanage used it before release\" are not helpful at all but insulting. Have it ever occured to you that there are other people on this planet besides you who might use applications in a different way you do or simply have another taste of design/other preferences than you have?"
    author: "anon"
  - subject: "Regressions, anyone?"
    date: 2008-10-04
    body: "I find it sad that KDE 4 devs puts priorities on making fancy fading effects everywhere, when basic features like global shortcuts for applications(khotkeys) isnt working. I still haven't started using KDE 4 as my primary DE, most because of the huge amount of quirks and small bugs everywhere. \n\nKDE rely on developers donating their time on the project, but when you stop targeting developers in KDE apps, then you will indeed lose quite a few contributers over time. (Mouse driven desktop isn't attractive to developers for instance)"
    author: "user"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-04
    body: "I can't be bothered to - yet again - point out that developers are not interchangeable cogs and not all tasks are equally easy; that volunteer-driven projects cannot be said to have a \"focus\" on or \"target\" anything; that writing \"fancy fading effects\" is absolutely trivial with Qt4; that if the people adding \"fancy fading effects\" weren't writing these effects they would likely not be contributing to KDE at all; that, even then, the \"fancy fading effects\" account for a vanishingly small amount of total work going on in KDE; that a new SVN account is requested and granted on average every day; that July and August had the greatest number of commits in the history of the KDE project; etc, so I'll just point you to this instead:\n\nhttp://michael-jansen.biz/blog/mike/2008-09-18/kmenuedit-support-application-shortcuts-again\n\n"
    author: "Anon"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-04
    body: "Your point is valid... however, the original poster's point is valid, too:\nMaking something free of charge (which btw isn't always the case, quite some devs are on someone's payroll) does NOT, I repeat, NOT, make you free of criticism.\n\nQuite the contrary, feedback from the userbase, even if critical, should be taken as an incentive to make it better. Any criticism which is discarded because \"the dev is not payed for it\" meanns discarding another chance of improving KDE.\n\n\nBTW, I'm quite annoyed by khotkeys, too ;)))."
    author: "John Doe"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-04
    body: "\"Making something free of charge (which btw isn't always the case, quite some devs are on someone's payroll) does NOT, I repeat, NOT, make you free of criticism.\n \n Quite the contrary, feedback from the userbase, even if critical, should be taken as an incentive to make it better. Any criticism which is discarded because \"the dev is not payed for it\" meanns discarding another chance of improving KDE.\"\n\nI didn't say anything even approximating that, though :)"
    author: "Anon"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-04
    body: "Yes, but you tried to make the criticism invalid by pointing out that nobody can be forced to fix the bugs described by the OP.\n\nYou're obviously right when saying that; however the OP was also right with his criticism. That's no contradiction.\n\nAnd if every criticism will be put off with this arguments and devs never listen to complaints of end users, exactly those end users will simply vote with their feet. \n\nI'm simply a bit tired of hearing criticism on any open source project (not only KDE) being neglected with the argument \"devs are not paid to do this so they do only what they personally want to do\". \n\nHowever, as far as I see it, most OSS (and most KDE) devs, even if not paid for doing so, listen to their users - and that's good ;))))."
    author: "John Doe"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-04
    body: "Ok, that's fair enough.  The argument \"devs are not paid to do this so they do only what they personally want to do\" isn't intended to \"neglect\" the criticism, however; it is simply an honest statement of fact, and one which people who think a project \"only focuses on X\" should keep in mind :)"
    author: "Anon"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-04
    body: "\ndifferent devs work at different areas and have different priorities ..i am sure the feature you want is somewhere on somebody todo list ... bringing up one area where you dont care about to show how neglected another area you care about will bring the kind of response the first responder made ..\n\nif  you are a simple user, you can vote for this feature(i am sure somebody already requested it and if not, you can open a wishlist) ..or you can bring it up in a place like this to raise awareness and somebody else who know what they are doing can pick it up or the person who already have it in his/her todo list can move it up a bit ..\n\nfollow up comments largely depends on how the original post is structured ..the OP could have raised his complain a little bit better IMO\n\n"
    author: "ink"
  - subject: "Re: Regressions, anyone?"
    date: 2008-10-05
    body: "> I find it sad that KDE 4 devs puts priorities on making fancy fading effects everywhere, when basic features like global shortcuts for applications(khotkeys) isnt working. \n\nSorry, but I find this comment insulting. It implies that KDE only cares about fany effects, which is not fair to the developers who did participate in the discussions about fixing shortcut keys.\n\nThis issue wasn't unhandled yet because KDE developers like nice effects, but because the issue complex and required careful thought. If you google for the blogs you'll find out this particular bug annoys KDE developers as well."
    author: "Diederik van der Boor"
  - subject: "Wrong Codename"
    date: 2008-10-05
    body: "The Codename shouldn't be \"Codename\", it should be \"Kodename\" - we are at KDE, right?"
    author: "zilti"
  - subject: "Re: Wrong Codename"
    date: 2008-10-06
    body: "Actually the latest fun was to take words starting with a K and use them with a C instead. This Codename skipped on that joke ;-)"
    author: "jospoortvliet"
  - subject: "Thanks"
    date: 2008-10-05
    body: "Fantastic project. Please dont forget \"autohide\" taskbar sometime, I had to go to gnme desktop for now to have it, as I really need it, but plan to switch back to kde when possible. I still use some kde apps though, though Konqueror wont remember the position (and width) I leave it's \"date\" column in, otherwise I would use it more as well."
    author: "anonymous"
  - subject: "Re: Thanks"
    date: 2008-10-05
    body: "The feature is already implemented in the development build of 4.2.0 which will come out in January. ;-)"
    author: "Jonathan Thomas"
  - subject: "Re: Thanks"
    date: 2008-10-11
    body: "And we're currently evaluating it for Fedora 10 and a possible Fedora 9 update (backported to 4.1.2), it's sitting in Rawhide right now, I can't promise yet that it'll stay there though."
    author: "Kevin Kofler"
  - subject: "Re: Thanks"
    date: 2008-10-05
    body: "That feature works in KDE 3.5.x\n\nI never understand the argument that KDE 4 lacking a KDE 3 feature makes people go to Gnome.  I presume you used KDE 3 and then switched to KDE 4.  Why not just go back to 3?"
    author: "T. J. Brumfield"
  - subject: "Re: Thanks"
    date: 2008-10-05
    body: "If you are looking for logic and rationality, you've come to the wrong place :)"
    author: "Anon"
  - subject: "Re: Thanks"
    date: 2008-10-09
    body: "because it's deprecated?"
    author: "MamiyaOtaru"
  - subject: "Re: Thanks"
    date: 2008-10-09
    body: "Yeah, I remember when the Offical KDE3 Deprecation Notice was issued from KDE Central Command - all my KDE3 stuff was auto-wiped from my harddrive :(\n\nLuckily, GNOME3 isn't on the horizon, so GNOME2 will obviously be around for many, many years yet!!"
    author: "Anon"
  - subject: "Why people talk about regressions..."
    date: 2008-10-05
    body: "...because there is a lot of \"basic\" stuff that just dont work. Some examples:\n\n- global shortcuts (as said before here)\n- drag and drop is broken in kmenuedit\n- renaming menu entries is broken in kmenuedit\n- kbookmarkeditor is just a mess (drag and drop broken, imports broken, sorting broken and crashes often)\n- this weird dolphin thing, where it takes ages to load the contents of a directory while not using much cpu at all\n\nI dont want to sound offensive, but this is basic stuff for me... Afaik there are already bugs filed. And yes, i would fix it if i would be able to code :) Just look at this as a reminder for stuff that doesnt work properly since 4.0.0...\n\nIts nice to do feature-development, but sometimes you have to look for breakages too, imho..."
    author: "JoMo"
  - subject: "Re: Why people talk about regressions..."
    date: 2008-10-05
    body: "You should report bugs at http://bugs.kde.org/ where they will actually get attention instead of here where they'll just be forgotten..."
    author: "JJ"
  - subject: "Re: Why people talk about regressions..."
    date: 2008-10-05
    body: "kmenueditor basic stuff?\nI always thought that editing the an application launcher menu was an advanced task not done everyday by everyone, which should define \"basic thing\" in a desktop environment."
    author: "Vide"
  - subject: "Re: Why people talk about regressions..."
    date: 2008-10-06
    body: ">Its nice to do feature-development, but sometimes you have to look for breakages too, imho...\n\nThis release is *all* about fixing bugs, not about adding features. That's what 4.2.0 will be about again. I'm sorry for you if your pet bugs are not fixed yet, but don't accuse the KDE devs of not caring about bugs or regressions. If that were the case, this release would not have existed."
    author: "Andre"
  - subject: "Re: Why people talk about regressions..."
    date: 2008-10-07
    body: "> Why people talk about regressions... \n\nMaybe cause not everything we had in KDE 3 is in KDE 4 yet? But since each dot-O release does provide new features+fixes it may turn around soon again into a \"uh, to much features\" talk ;)\n\n> Its nice to do feature-development, but sometimes you have to look for breakages too\n\nAbsolute.\n"
    author: "dipesh"
  - subject: "Still no _Minimize All_ button?"
    date: 2008-10-06
    body: "come on when will kde4 catch up with some good old kde3 functionality?\n\nminimizing windows one by one is kinda backwards, so is crtl+f12 getting rid of the panel :("
    author: "Aeryn"
  - subject: "Re: Still no _Minimize All_ button?"
    date: 2008-10-06
    body: "There's a panel plasmoid that does that. Also (not sure this works on KDE4 since I'm at work and using windows) have you tried Win+M ?"
    author: "NabLa"
  - subject: "Re: Still no _Minimize All_ button?"
    date: 2008-10-07
    body: "which one + where to find it?\n\ntrying to fetch new widgets via the built in function gets me nowhere :\\"
    author: "Aeryn"
  - subject: "Re: Still no _Minimize All_ button?"
    date: 2008-10-07
    body: "Its called \"Show desktop\" ;)\nYou must have that already"
    author: "Yogesh M"
  - subject: "Re: Still no _Minimize All_ button?"
    date: 2008-10-06
    body: "Sounds like a feature to me, not a bug, so it does not belong in this release AFAIK. Maybe you'll have more luck with KDE 4.2.0."
    author: "Andre"
  - subject: "Missing Apps"
    date: 2008-10-06
    body: "Does someone have information about KDE4 version of programs like k3b and quanta+ - that for what I know will be merged into kdevelop, that I think is bad, so my question is also about kdevelop with full html/php support - and such?\n\nYou know, it's a shame there are so much really great KDE3 apps that aren't and will never be ported to KDE4 :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Missing Apps"
    date: 2008-10-06
    body: "A shame? Come on! I can tell you what a shame is: Some people still haven't grasped that somebody has to do it. Somebody they do not have a contract with. Somebody who will not get money, water, bread and a roof from them. And if that somebody does it and shares it with you you should be full of cheers and praises. Instead you demand...\n\nThink about it. Comments like your's don't motivate people, it rather turn them down and frustrate them. How do you feel if you work hard and give the result to someone else who does nothing but point out deficits?"
    author: "King Kong"
  - subject: "Re: Missing Apps"
    date: 2008-10-06
    body: "Man, what did you eat at breakfast today? Ouch!\nI mean for shame that it's too bad, not like \"those bastards developers are just lazy\".\nC'mon, easy man... easy!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Missing Apps"
    date: 2008-10-16
    body: "You want bread and butter? Code for money!"
    author: "You"
  - subject: "Re: Missing Apps"
    date: 2008-10-07
    body: "K3b has been ported to kde4, you can download beta versions for most linux distributions. In the mean time, you can use k3b from kde3. Same with applications like amarok en such: new versions are coming..."
    author: "Whatever Noticed"
  - subject: "Re: Missing Apps"
    date: 2008-10-07
    body: "I know there were some betas of k3b, but I did not found it for ubuntu hardy (and now intrepid).\nYes, I do use amarok for KDE4, it is still very imature in comparsion with KDE3 version, but usable.\n\nUnfortunately, there are some apps without replacement so far:\n- quanta+ a html/php editor, should be replaced by kdevelop, but I don't believe kdevelop suits for web development, way too big for such a small task. Maybe I should start hacking a Qt editor to include HTML/PHP tag editing and such? I could even make it in php/qt, it would be cross platafform and easy to code ;)\nThis is a must for me, as I'm a PHP/HTML/Javascript developer :-P\n\n- Subtitle Composer just got and KDE4 version, so no complain here.\n\n- knetstats: should be easy to port, does anyone here knows of a guide for kde3 to kde4 porting?\n\n- KGmailNotifier: still the best one, but not as good as the google talk notificer. Maybe I (or anyone else) should just code a small and simpler version? Again, I'll avaliate if it's possible to use php-qt.\n\nAny more apps?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Missing Apps"
    date: 2008-10-07
    body: "There you go: http://techbase.kde.org/Development/Tutorials/KDE4_Porting_Guide"
    author: "Boudewijn Rempt"
  - subject: "Re: Missing Apps"
    date: 2008-10-07
    body: "> quanta+ a html/php editor, should be replaced by kdevelop, but I don't believe kdevelop suits for web development\n\nThis is a misunderstanding.\n\nKDeveloper and Quanta will be based on a shared application base called KDevPlatform.\n"
    author: "Kevin Krammer"
  - subject: "Re: Missing Apps"
    date: 2008-10-07
    body: "Really big misunderstanding really, but it was what I heard a looooooooong time ago. Quanta guys are kind of in silent-radio lately, that is too bad :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: Missing Apps"
    date: 2008-10-08
    body: "yup, quanta is one of the programs that made KDE that nice desktop env it was - suitable for developers but also fitting the needs of a nice home-use platform. \nBecause of quanta's excellent tree view, i use it for almost all kinds of development, even things that you would have done with Kate, Kdevelop or the like.  \nHere we are still at KDE3.5 - and for now, we don't want to switch to KDE4 - always afraid that we are forced to, because of one of our distros may switch entirely ... \nIt sounds sadly, and thats what it is."
    author: "abramov"
  - subject: "That's great...."
    date: 2008-10-06
    body: "but I really, really wish they would tend to fixing flash in konqueror. As a non-developer, just how hard can that be? And since one of the talking points about KDE4 is how it will handle media better than KDE3, why is it taking so long to deal with this? I know there has been some contention in the past about flash, but gee whiz. Either flat out say you are not going to support it or fix it please."
    author: "taurnil"
  - subject: "Re: That's great...."
    date: 2008-10-06
    body: "\"As a non-developer, just how hard can that be?\"\n\nHard."
    author: "Anon"
  - subject: "Re: That's great...."
    date: 2008-10-06
    body: "Konqueror (KDE 4.1.2) with Flash Player (Version 10) works for me without any problems. Can you please describe your problem in detail?"
    author: "Cheko"
  - subject: "Re: That's great...."
    date: 2008-10-07
    body: "That's great for you, and it is the version I am running here. See attached png."
    author: "taurnil"
  - subject: "Re: That's great...."
    date: 2008-10-07
    body: "flash10 with kde 4.1.2 works fine for me, but it didn't in kde 4.1.1.\nDunno who was to blame, as i updated flash10 to the most recent version a short while before upgrading to kde 4.1.2, so it could be that the flash10 beta was to blame..\n"
    author: "Whatever Noticed"
  - subject: "Panel wraps around + messed up systray"
    date: 2008-10-07
    body: "I am now trying kde 4.1.2. I have two problems, which really annoy me (and had them with all other kde 4 versions I tried):\n\n- The systray is totally messed up. It just isn't rendered correctly\n- I can move the panel so far below that it wraps around to the top! Instead I would prefer making the panel really tiny like in kde 3 (same size in the screenshot but with smaller icons without wrapping around)\n\nIs anybody else having these problems? Are there solutions out there?\n\nIn the attached screenshot you can see these problems. Have a look at the bottom right. There you can see a small part of the messed up systray."
    author: "knue"
  - subject: "Re: Panel wraps around + messed up systray"
    date: 2008-10-07
    body: "i had the same with kde 4.0, but not with later versions.\nTry to start kde with a fresh configuration (e.g. as a test user or after you renamed ~/.kde4) to see if it is something that is left behind from a previous kde installation.."
    author: "Whatever Noticed"
  - subject: "Re: Panel wraps around + messed up systray"
    date: 2008-10-07
    body: "Are your using openSUSE 11? I also got similar problem but that was gone after logging out-in..."
    author: "Yogesh M"
  - subject: "Re: Panel wraps around + messed up systray"
    date: 2008-10-07
    body: "Tried with a fresh user, but same problem. And at least the wrap around bug shouldn't be a driver bug (nvidia geforce 7600 with nvidia drivers here).\n\nI am using gentoo packages."
    author: "knue"
  - subject: "Re: Panel wraps around + messed up systray"
    date: 2008-10-07
    body: "File a bug report including screenshot.  If you can get the corruption to persist across Plasma sessions, then include the plasma config files, too."
    author: "Anon"
  - subject: "THANKS for the D-BUS controls in Konsole"
    date: 2008-10-09
    body: "I have a pair of scripts I use to launch konsole windows, which allows me to\nselect an existing konsole window with the mouse pointer, and open a new tab\nin that window and execute an arbitrary command.\n\nI am glad to report that I have successfully modified my scripts to work with\nkonsole from KDE4 with D-BUS methods which are now available and working.\n\nThank you very much for supporting a happy power user of konsole!"
    author: "Tad Mannes"
  - subject: "Re: THANKS for the D-BUS controls in Konsole"
    date: 2008-10-09
    body: "Er ... do downloadable sandboxed (e.g QScript) Plasmoids have access to DBUS, does anyone know? If so, then they can immediately break their sandbox and execute arbitrary code if a konsole instance is running."
    author: "Anon"
  - subject: "Re: THANKS for the D-BUS controls in Konsole"
    date: 2008-10-09
    body: "I think my original message may have been misleading in this regard.  The D-BUS\ninterface does not provide a mechanism for executing arbitrary commands by\nitself.  I have have Konsole Profile, which executes one of the two scripts I\nmentioned.  This script is written to look for the command to execute on a\nspecific named pipe that I create.  The other script puts the command I want\nexecuted into the named pipe...\n\nSorry to cause alarm unnecessarily."
    author: "Tad Mannes"
  - subject: "Re: THANKS for the D-BUS controls in Konsole"
    date: 2008-10-09
    body: "Phew - that sounds much better! Thanks for taking the time to reply and set things straight :)"
    author: "Anon"
  - subject: "Re: THANKS for the D-BUS controls in Konsole"
    date: 2008-10-27
    body: "could you post some example code, please?"
    author: "ali"
  - subject: "Re: THANKS for the D-BUS controls in Konsole"
    date: 2009-01-24
    body: "I have attached the two scripts you were interested in.  I create a\nnew Profile and specify the path to the attached \"konsole4-execution\"\nscript for the \"Command:\".  I use the attached \"launch-konsole4\" script\nto launch a konsole window (the name should make that obvious :-)."
    author: "Tad Mannes"
  - subject: "Re: THANKS for the D-BUS controls in Konsole"
    date: 2009-01-24
    body: "Ah, apparently I could only attach one file on the first go.  Here is the\nother script..."
    author: "Tad Mannes"
  - subject: "Really exited"
    date: 2008-10-11
    body: "Really exited to see this.\n\nHopefully when KDE 4.2 comes out it will have a launch event and a release video like KDE 4.0 will.\n\nWill Google host it again?\nWill Aaron Seigo do the keynote?\n\n"
    author: "Max"
  - subject: "KDE 4 Lite ?"
    date: 2008-10-13
    body: "Are you aware of a community effort to create a KDE Lite that would run well on old hardware [5-7 years old computer]? I am thiniking something in the spirit of XFCE for GTK.\nSample desktop manager with a simplified Dolphin. The target would be home users. So focus is on main essential tasks performed by average Joe. No need for all complex, networked, management features that apply only to server administrators.\n\nAny suggestion?\nThanks"
    author: "Jerome"
  - subject: "wish list"
    date: 2008-10-15
    body: "http://www.linux.com/feature/146323 . Got some good points.."
    author: "student"
  - subject: "I am puzzled..."
    date: 2008-11-15
    body: "I upgraded from KDE 3.5.something to 4.1.2, and found that many features I relied on in the previous version were missing in the newer version.\n\nI think that whenever developers come up with a new version of their software, they should consider the needs of those who use features that may not be so popular, and leave them in (or make them available somehow).  What happened in my case was that my system became unusable.  There were too many missing features, too many things that just got in my way as I tried to do my work.\n\nFor example, I have an extra-wide screen on my laptop, so it makes sense to use side panels instead of adding one at the top.  I couldn't figure out a way of putting a panel on the side.  This side panel is where I like to store many shortcuts to programs, so that my windows don't hide them as they would if these shortcuts were on a desktop.\n\nOffering a 'rollback' feature would have been nice, too - something that allows users to revert to the previous version, if they decide they don't like the newer one.  Of course, that's asking a lot, so I don't fault you for not doing that bit.\n\nI like to give each desktop a unique background, so I can easily know where I am.\n\nMany of my settings were destroyed when I upgraded, requiring me to change them all again by hand to something I liked better.\n\nThe new version made my system run noticeably more slowly, almost like That Other OS does.\n\nBeyond that, I didn't see any improvements that would justify the inconvenience and sluggishness.  It seemed like it was mostly eye candy, not improved functionality.\n\nUltimately, I had to reinstall the previous version of KDE, because that at least allows me to do my work.\n\nI'm not trying to diss you guys here.  You do a great job, and I know it's not easy.  All I'm suggesting is that you don't take away functionality, as you're making improvements.  If you don't think they're good, then hide them but make them available to those who really want or need them."
    author: "Chiron613"
  - subject: "Re: I am puzzled..."
    date: 2008-11-16
    body: "A good way to find out if KDE 4.x is \"ready for you\" is to try it using a live CD. If you are satisfied with the available features, upgrade. If not, retry six months later, when a new KDE update is released. The next update (4.2) should be available end of january, which is supposed to fix many panel issues. Placing a panel on the side of the screen is possible with 4.1, though, it is described somewhere on userbase.kde.org."
    author: "christoph"
  - subject: "Re: I am puzzled..."
    date: 2008-11-20
    body: "It doesn't surprise me that there is a way to put a panel on the side of the screen.  What surprises me is that I need to go dig this information out of the Internet somewhere, and can't just do it from a control panel or config file.\n\nI had some major problems with KDE 4, including it forgetting my settings between sessions, crashes using Konqueror and other programs, even the system forgetting my video settings between sessions.  Ultimately, I couldn't log in at all, so I just reinstalled 3.5 and will keep it there.\n\nI'm not sure the Live CD would have shown me the problems with forgetting settings, since it's *supposed* to forget them.  It doesn't save them to disk, to my understanding.\n\nProbably the smartest thing to do is to wait a few months after a release, then look to see who's complaining about what.  I have no great need to be on the bleeding edge anyway.  I'll let other do the beta testing.\n\nUnfortunately, while this version was considered a release, in my opinion it was still beta."
    author: "Chiron613"
  - subject: "Re: I am puzzled..."
    date: 2008-11-22
    body: "\"It doesn't surprise me that there is a way to put a panel on the side of the screen. What surprises me is that I need to go dig this information out of the Internet somewhere, and can't just do it from a control panel or config file.\"\n\nI'm not sure what is so difficult about:\n\nright-click the panel -> Panel Settings -> click and drag the button that says \"screen edge\"\n\nIt even has the mouse-over turn into the \"moving\" cursor."
    author: "Leonardo Aidos"
---
Two days later than initially planned, "Codename" (or more traditionally KDE 4.1.2) <a href="http://kde.org/announcements/announce-4.1.2.php">was released just a few minutes ago</a>. The delay was caused by binary incompatibility issues in the branch. Those have been resolved so we are now looking at a stable release. 4.1.2 is another one of those monthly bug fix and translation updates. No new features are allowed into the 4.x/ branches, so no new features went into KDE 4.1.2, but some nice bug fixes instead. David Faure has fixed a long-standing and annoying performance issue when deleting files using KIO, so you can now accidentally <a href="http://www.kdedevelopers.org/node/3683">delete your home directory 32 times faster</a> For the more faint-hearted, it will also work well with other files. You can read about all the changes that went into <em>Codename</em> in the <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php">changelog</a> which offers links to the comprehensive SVN log files.
KDE 4.1.2 is a recommended upgrade for everybody running KDE 4. The next feature release of the KDE workspace and applications will be in January 2009 when 4.2.0 will be upon you.



<!--break-->
