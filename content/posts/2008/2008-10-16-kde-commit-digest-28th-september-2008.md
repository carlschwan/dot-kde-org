---
title: "KDE Commit-Digest for 28th September 2008"
date:    2008-10-16
authors:
  - "dallen"
slug:    kde-commit-digest-28th-september-2008
comments:
  - subject: "Thanks Danny!"
    date: 2008-10-15
    body: "I really enjoy reading digests, specially since http://www.jarzebski.pl/ is down."
    author: "Mariano"
  - subject: "Re: Thanks Danny!"
    date: 2008-10-16
    body: "Perfect before a good night ! Thanks Danny :)"
    author: "DanaKil"
  - subject: "Re: Thanks Danny!"
    date: 2008-10-16
    body: "Thanks Danny!!"
    author: "Michael \"Thanks Danny!\" Howell"
  - subject: "Desktop Theme Details"
    date: 2008-10-16
    body: "I'm very happy about this. Just a few days ago, I was trying out themes, and wished I could use the clock from one, taskbar from another, and everything else from a third. Of course, I'm lazy and didn't want to hack together my own theme.\n\nDesktop Theme Details is exactly what I was hoping for."
    author: "Soap"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-16
    body: "Regarding Plasma customization... Will there be a way to switch to interacting with plasma components using a \"normal\" right-click menu, rather than having the cashews and floating toolboxes?"
    author: "Martin"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "that might be fun until you get a widget that uses all the mouse events for itself (completely legitimate use pattern, btw).\n\nbut go ahead and give it a whirl; this is possible with a customized Containment.\n\nin any case, there are already context menus provided for most actions.\n\nas for \"normal\", a magical mouse click that brings up something you can't see with actions that are related to wahtever it thinks the context is might be expected by people who learned to use computers well in the last decade, but it's hardly intuitive or consistent."
    author: "Aaron Seigo"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "As ever you are unable to grasp the fact that people use a desktop in different ways. For me the chashew is one of the most annoying things lingering around on the plasma desktop and it rears it's uggly head the moment the mouse touches it and it often distracts the attention away from the intended task (most of the time I touch this uggly beast when I want to close or in other ways want to manage a *proper* window). There are people like me who like an uncluttered destop which does not impede the use of proper windows by drawing attention to itself on every possible occasion. \nThe analogy is that there is an annyoing blob of sticky marmalade on your desktop plate that lingers around next to your post-it notes - every now and then you find that (inadvertendly and annoyingly) until you have had sticky fingers often enough and remove it!"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "Which part of \"but go ahead and give it a whirl; this is possible with a customized Containment.\" didn't you understand? ;P"
    author: "Hans"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "Look through the guy's posting history, especially his interactions with Aaron.  It usually goes something like this:\n\nKurt: <whiny rant, filled with misconceptions>\nAseigo: <corrects misconceptions>\nKurt: <does not reply>\n\nA few days or weeks later:\n\nKurt: < another rant, filled with mostly the same misconceptions as before >\nAseigo: < sobs and kills kittens >\n\nThis one appears to be an interesting change to the pattern:\n\nAeigo: < sure you can do $X >\nKurt: < Gah why do you want to prevent people from doing $X you just don't get that other people want to do $X grrrr >\n\nDude just seems to want to make Aseigo out to be \"the bad guy who stubbornly refuses to listen to end user\", for some reason, and completely ignores any evidence to the contrary.  Or maybe he is just very dense."
    author: "Anon"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "I can't wait till the dot gets moderation. Then we don't have to suffer fools like this..."
    author: "txf"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-18
    body: "Hello,\n\nall moderation will achieve is to open new fighting grounds. I have seen Kharma whoring and didn't enjoy it. I guess Danny will see more thank you then, but it will mean less.\n\nAnd \"why was this moderated down?\" postings too.\n\nI would hope that with release of 4.2 the need for moderation will nearly be gone.\n\nAnd probably one day the light from above will really show why everybody must have the cashew in a place he cannot configure and cannot have it go away. Until then my attention deficit problems would make me want to configure it away. Until I switch to KDE 4 it likely will be solved. \n\nBTW: Seems like little would prevent me soon from switching, except that it appears harder to tone down colors in KDE 4, but the new theme composer may make it easier to achieve. I normally reduce saturation, leaving the saturation to high-light. I feel that is ergonomic and giving me as little distraction as possible and needed.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "> Which part of \"but go ahead and give it a whirl; this is possible with a\n> customized Containment.\" didn't you understand? ;P\nI don't understand the whole concept because noone cares to explain or even document the whole mess that plasma is to the noninitiated... Concrete I didn't find an way to customize a containment in such a way as to completely remove the Cashew without removing vital functionality which on the other hand is needed to manage the few important plasmoids inside to the extent of making it work at all! \nSo instead of calling me a troll, try to explain it to someone who does not care about the inner workings of plasma but who simply wants an unintrusive non-bling-bling desktop that offers the basic functionality needed to get his work (which does not involve playing with plasmoids for 4 hours a day) done!"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "Why don't you just open the code and start working. If you have no intention of knowing anything about the inner workings of plasma, you simply have to look for somebody who wants to do the work for you. Clearly, Aaron has enough to do, and is not interested, so maybe you could stop wasting his time. He (and every FOSS developer) is free to spend his time any way he wants - you're not paying him, nor am I.\n\nI'm not a huge fan of the cashew either, but I can wait until someone willing to spend time on it finds another solution. Complaining about it all the time won't help anyone.\n\nIf you have questions while developing a cashew-free containment, you can ask them on the #plasma channel. \n\n\nAnd if you don't want to or can't solve it yourself and have such grave issues with plasma, please use either the KDE 3.5.x desktop, the Gnome desktop or something else. There is no reason why you can't do that, and still have KWin and the other KDE applications."
    author: "jospoortvliet"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "This is exactly what I find deplorable about the behaviour of the KDE developers right now. Because resources are sparse things that annoy end users get pushed back indefinite because they are not hip (i.e. don't fit the own conception)! This is a highly arrogant behaviour which has besieged the GNOME developers for a long time (removal of highly important options because they required explanation) and which for equally long time was mostly absent from the KDE developers mind set. The KDE developers wanted to deliver a functional desktop environment. But with the new breed of developers this kind of arrogance has become standard in KDE as well. In discussions any criticism is quashed not by explanation or fruitful discussion but by name calling...\nWhile I am able to code (commercial Qt developer for aslmost 10 years now) I have little spare time to contribute especially as I don't get the concepts behind the second layer of window mangement that plasma is. So as long as I don't get the concepts or anyone can explain the benefits of having a set of interactive windows that are exempt from normal window management I have no foothold in this development on which to base my changes. "
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "I didn't call you a troll, although I did think you acted like one.\nNote that this is a reply to your other post too.\n\n> I don't understand the whole concept because noone cares to explain or even document the whole mess that plasma is to the noninitiated...\n\nAs far as I've seen, many persons (including Aaron) has explained a lot about Plasma to you. To back up my statement, see this post as an example: http://dot.kde.org/1219149384/1219200661/1219232051/\nHave you even bothered to search for information yourself?\n\nIt's true that Plasma currently lacks documentation, but that's what Techbase and Userbase are for.\n\n> As ever you are unable to grasp the fact that people use a desktop in different ways.\n\nSince you talked about sticky marmalade I'll make a bad comparison too:\n\nLet's say you've bought a LEGO model and just finished building a car by following the instructions. Now you want to make a boat instead, but there aren't any instructions on how to build one. Being frustrated, you write a post on a LEGO forum. However, the other members just tell you that \"if you want a boat, you have to make it yourself\".\n\nWould you say that it's not possible to build a boat using LEGO?\nWould you call the LEGO creators arrogant for not realizing that some people want to build a boat instead of a car?\n\nSure, you can claim that the model you bought lacks documentation since it didn't include an instruction on how to build a boat.\n\nBut if you have the knowledge, why don't you just come up with a solution yourself?  If it turns out well, you can even share the build instructions with other users. Everyone would be happy.\n\nDo you really think ranting on the LEGO forums would help you to make a LEGO boat?"
    author: "Hans"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "either way there is a patch for your particular problem on kde-look. Haven't tried it but you're welcome to...\n\nFrankly I don't consider it a problem... It always stays below the windows so it doesn't interfere with anything I'm doing. This problem is so minor it is not even worth making the fuss you are. It may be important to you, but not for the rest of us, even if we can see some merit of what you're proposing...\n\nAs others have said you can do something about it, bear it, or just use something else..."
    author: "txf"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: ">  Because resources are sparse things that annoy end\n> users get pushed back indefinite because they are not hip\n\nThat complete bullshit on two levels:\n\na) I implement features on a weekly basis that I have no personal need for nor and which I personally find to be \"hip\" simply because people request them and share the reasons for them, reasons which actually go to the heart of usefulness and not just \"because it used to be that way\"\n\nb) resources have always been sparse. we've always had to pick and choose which features we implement and which bugs we fix. saying this is somethign new is disingenuous, and i think you'll find that if you compare the amount of code that went into making kicker or kdesktop  \"more what people asked for\" compared to plasma, you'd be ashamed of yourself for making the accusation that it's gotten worse instead of better.\n\nYou may not agree with my decisions, that's one thing, but how dare you accuse me and my team of things we are not doing.\n\n> So as long as I don't get the concepts or anyone can explain the benefits of having\n> a set of interactive windows that are exempt from normal window management\n> I have no foothold in this development on which to base my changes.\n\nThat's completely fair. Here's my suggestion to you: I have talked quite a bit about the point and purpose of widgets on the desktop. You have a very skewed viewpoint on them (they aren't \"interactive windows\" any more than Kontact's various plugins and listviews are), but I'll chalk that up to simply not having had time to read through everything I've written on the topic. Which is cool: it's not a small amount of information to chew through, and I don't expect everyone to be able to. It's not a trivial topic, and I don't expect people to become \"instant masters\" of the deep concepts any more than I expect people to become \"instant masters\" on the deep concepts of, say, filesystem design.\n\nSo .. what I recommend in your case is using folderview as your desktop activity (you can set this in the configuration dialog in 4.2 quite easily) and treat plasma as if it were kdesktop+kicker from years previous.\n\nStop trying to bend your mind around the new ideas and concepts: they obviously aren't a fit for you, and that's *OK*. Just because a new thing is there doesn't mean you have to use it, and nobody will be offended if you don't. We've put in huge effort (to the expense of people waiting for all the new things to come to full fruition, btw!) to make sure that you can do exactly this.\n"
    author: "Aaron Seigo"
  - subject: "Quick Question on Desktop Folder View"
    date: 2008-10-22
    body: "Is there a way to make this view appear automatically for a new user as if it was in a skeleton KDE config?\n\nIs there a wiki or website that may hold all the documentation on plasma so its all in one place until such time that it can be all pulled together into a single document? It doesn't need to be organised as yet but it would be a good place to point some of us to when we are finding it difficult to transition to the new paradigm after all the years on the current one. \n\nI've ended up with icons on the \"desktop\" that have no description and can only press it to launch the program or delete the icon, no way to move it or get to the properties - but then again, i've been messing around so i might need to start from scratch.\n\nthanks to you developers for your dedication.\n\nIan"
    author: "Ian"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "   I'm not a huge fan of the cashew either\n\ncashew's purpose looks more brand-related than technical.\nPlasma developers want their brand prominently visible so they used the cashew.\nUnderstandable, but problems with that approach:\n\n1)the logo choice \nis about the worst that could have been made given the market share situation in the free desktop world. Not for aesthetic reasons but purely for a matter of shape.\nI attached an image. Have a look and tell me you never thought of that before :-(\n\n2) brand dilution!! \nif you have that C-topped-with-4-dots thingy in the upper right and in the panel, why is there a K on that application menu? that is *confusing*.\nWhy do you need a special logo for what is perceived as the KDE desktop main gui shell? Aren't plasma developers happy enough to defend and be identified to the KDE brand they have to somehow part from it like a desktop application?\n\n3) too prominent versus application menu\nit looks like an entry point for the shell to the new user, instead it's only about the desktop. that's messed up priorities!\n"
    author: "Ron"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "It's called KDE 3.5.10."
    author: "Phil"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-17
    body: "> As ever you are unable to grasp the fact that people use a desktop in different ways.\n\nSo that's why I designed a system (plasma) with infinite flexibility? hm. Yeah, you're accusations don't map to reality.\n\nWhat you *really* mean to say is \"I dislike your decision to not make the defaults exactly what I used for the last 20 years. I dislike that you won't write the code I want you to the way I want you to.\"\n\nI think that position is really unfortunate on several accounts, but respect your right to feel that way. \n\nHere's my feeling on the matter: the Plasma team is working bloody hard to accomodate the whole user base, not only you. Not being the center of attention sucks, especially when you were before, but times change, things evolve and you are but one of our users. To bow to your personal opinion just because you have the audacity to post it over and over here in a public area and thereby bring to ruin what we've worked, and continue to work, so hard on bringing to life might make you happy as an individual, but would be a massive disservice to every one else out there that would get the shaft.\n\nWe are taking KDE to places it has never been before, nor could ever go before. Some of the product announcements that will be coming out in the next 12 months will spin our heads. If I lose one Karl in order to increase the loyalty of 3 others and gain 5 new users in the process ... so be it.\n\nBut you know what? It doesn't have to *be* confrontational. You don't *have* to be a prick to me here. I keep giving you more and more and you just keep being an ass about it, and that doesn't particularly motivate me to care about you, now does it? Realize that your only currency on the table here is community because you have yet to put a single dime in my pocket or a line of code in my repository. Use your currency wisely.\n\nStep back and let yourself take in the information others are offering to you here in this thread, on forum.kde.org, on userbase.kde.org, etc and you'll find that a lot of your anxiety is likely to exit as you realize that, hey, surprise! Plasma is actually able to do what you want and proceeding at a pace that exceeds anything previous KDE shells managed to put together.\n\nBe my friend, even in disagreement, and you'll get a lot further. Not only because it'll make me want to help you, but because I already have in many cases but your attitude is making it hard for you to see that.\n\nCheers, and I hope you can find a happy place to post from in the future."
    author: "Aaron Seigo"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-19
    body: "One question and please just answer with a yes or no, save the speaches for those who are asking to.\n\nWill there be a way to dissable cashews from panels (containers?) in KDE 4.2?\nwill there be a way do sjhow a desktop w/o the annoying cashew?"
    author: "Dan"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-19
    body: "I already got that in 4.1.2 -- right-click, choose Desktop Settings, choose Desktop Activity -- plain desktop. Done. And there is no cashew on the panel in any version of KDE 4 I've used when you lock the widgets."
    author: "Boudewijn Rempt"
  - subject: "Re: Desktop Theme Details"
    date: 2008-10-19
    body: "Yes. (Lock panels)\nYes. (http://www.kde-look.org/content/show.php/I+HATE+the+Cashew?content=91009)"
    author: "Hans"
  - subject: "Google Summer of Code"
    date: 2008-10-16
    body: "Hi,\n\ni wonder what the Status of the many different Google Summer of Code projects is.\nAre 100% of them done and the features usable ? Are 100% dumped ?\n\nperhaps i missed some parts in an old digest where there were stats on all the projects that were carried out ?\n\nhas only the money been taken for food and drinks ? :-)"
    author: "sfdg"
  - subject: "Re: Google Summer of Code"
    date: 2008-10-17
    body: "there was an article on the koffice projects; all the Plasma ones were successful to at least some extent, with all but one of them having been merged into mainline. i suspect we did better than last year on the success ration, though it certainly isn't 100%. nobody expects that, not even google. in fact, google gets suspicious when a project says all their SoC students succeeded ;)"
    author: "Aaron Seigo"
  - subject: "Re: Google Summer of Code"
    date: 2008-10-18
    body: "Hello Aaron,\n\nI think the KDE project should also point out about SoC and students in general, what a great social service this is giving. You are teaching with mentorship a whole lot to these students in a short time.\n\nHaving access to, help from, guidance from some of the best developers in the world, clearly has value to the students, and value for those they later work for and with.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Mad Props"
    date: 2008-10-16
    body: "Mad props to Danny. Thanks! And mad props to all the devs making KDE real."
    author: "Winter"
  - subject: "Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "First, Planet Developer, now this. Why are so many KDE sites making it so that text falls off the right side of the browser! One should first design for usability/accessibility, and then get fancy if you want. Not the other way 'round."
    author: "Joseph Reagle"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "I don't know what you're getting at.  It doesn't spread/spill/widen Konq for me.  KDE 3.5.10 (Gentoo)"
    author: "Xanadu"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "Works here too. KDE 4.1.2 (OpenSuse)"
    author: "Rippo"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "I think I know what he means. I have the same problem here as I am using larger fonts than normal. Just try to increase the font size and you see that the text will not be wrapped fitting to the window width. Instead the horizontal scroller does appear what makes the article hard to read. I am lucky not to have a small netbook screen or something, so that I can enlarge the window to make the content fit in width."
    author: "gttt"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "If that's the problem Joseph is annoyed about, it comes from the very long lines (filenames) that set an effective minimum window width. It's a common html problem, and happens with every browser (except IE I think, which doesn't care and cut words in their middle).\n\nA while ago I found that the cleanest (render-wise but not code-wise) way to solve it was to insert an invisible 1px gif in the middle of the long string. The browser would then line-wrap at that point in the string. Nowadays I thinks there's a CSS property to do that cleanly, although I can't remember the name and have no idea which browsers implement it."
    author: "moltonel"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-17
    body: "There is also the old unofficial html code <wbr> to allow breaks within long words. &shy; would be even better (since it ideally adds a hyphen on breaks), but support for this in browsers is even worse than the above wbr."
    author: "Anon"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-17
    body: "Oh, IE shits on the commit digest page just as well... It looks horrible and doesn't fit the browser window..."
    author: "jospoortvliet"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "You can see this problem by making your fonts bigger, or just narrow Konqueror. Commit-Digest falls off the screen even with a ~750px wide window. (See screenshot.) This happens in Firefox 3 and Opera 9 too. I don't think it has to do anything with long filenames, and px tricks certainly aren't the solution. Rather, they are the problem: specifying widths in px rather than in %."
    author: "Joseph Reagle"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "Can't really reproduce the issue with Opera 9.6."
    author: "Mark Kretschmann"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "Wow. I guess I've just hit another bug though. The dupes were not intended ;)"
    author: "Mark Kretschmann"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-16
    body: "At a glance, I thought I had more comments on the Digest...\nI like comments ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-17
    body: "Out of curiosity, did you intentionally grey-scale that image, or is that your \"normal\" desktop.  I'm not downing you, I'm just really curious how one looks at a \"desktop\" devoid of color.  I'm color blind and all ( red/green ), but to see no color...\n\nI dunno.  I'm just wondering if you desaturated that, or if that's your normal desktop.\n\nM."
    author: "Xanadu"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-17
    body: "As the images of the Digest site are also desaturated, I would guess it is an effect applied after the screenshot was taken ;)\n\nUnless some kind of compositing window manager effect is in... effect ;o\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-17
    body: "At the smallest possible font size I still need a width of 900px here. That's seems not due to any of the font sizes but actually due to the fixed width world map within the article plus the fixed width (167px) sidebare on the left pushing all of of the remaining digest page to the right (there may be more content forcing this width, haven't checked further)."
    author: "Anon"
  - subject: "Re: Can we make Commit-Digest work decently in Konqie?"
    date: 2008-10-17
    body: "The commit logs come with hard returns at unfortunate locations, and in seemingly random, unpredictable ways.\n\nWhich, unless Danny is doing something that I tried but didn't succeed at, namely stripping the hard returns and doing a paragraph tag, which doesn't work for table type data, or some kind of complicated algorithm on the fly which strips the hard returns and puts them in a more convenient place all the while maintaining the format of the comment.\n\nThis is entirely due to the various editor settings, personal preferences and habits of the hundreds of KDE developers.\n\nIn other words, a hard problem unless you really enjoy hand editing commit logs.\n\nDerek"
    author: "dkite"
  - subject: "NetworkManager plasmoid"
    date: 2008-10-16
    body: "Hi,\n\nGreat work guys!\n\nHowever I got a question: is the NetworkManager plasmoid going to be a replacement for KNetworkManager? AFAIK KNetworkManager is a development done by Novell/SUSE, but to be honest: it is *very* crippled in OpenSUSE 11.0. \n\nBasically I'm afraid that (from a user perspective) something as simple as connecting to a (wireless) network is not going to work 'nicely' in KDE.\n\nSo, just a question, but maybe someone can shed a light on the network thingie :-)\n\nRegards Harry"
    author: "Harry"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-16
    body: "\"NetworkManager\" is a program I think created by Novell?   You're right, it does suck.  Hopefully the KDE guys get it figured out nicely.  I always use a traditional method to bring up my interfaces (ifup) as opposed to a GUI.  It just does it in the background on boot and I don't have to worry about it, but when I'm mobile, i do have to resort to the CLI.  It would be nice if they could get all those kinks worked out."
    author: "Jake"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-16
    body: "I'm a KDE guy and a Novell guy and I'm the guy doing most of the work on NetworkManager-kde4.  It is going to replace knetworkmanager-0.7 on KDE 4 installs.  \n\nThe previous KDE 3 iterations of knetworkmanager were not as good as we would have liked.  There are a couple of reasons for this: \n\n1, connecting to a wireless network is not simple: `man wpa_supplicant.conf` and read the example configs if you have only ever used WPA-PSK secured networks.  NetworkManager itself complicates things with a rewrite between version 0.6 and 0.7 that completely changed the DBus interface, which is undocumented, so my first task was to add automatic documentation to their build system.  That meant knetworkmanager had to be rewritten from scratch between 10.3 and 11.0. Then add all the other complexities and points of failure in the driver stack and the hardware that cause inexplicable failures on both GNOME and KDE. \n\n2, Both KDE 3 knetworkmanagers were written under very tight time constraints, outside the KDE team at Novell by busy guys.  So the code was not easy for others to get into and contribute to.  Building a dev team around an application takes time and effort.  We did have some valuable contributions from the community that fixed bugs (greetings to Michael, Valentine and Ryan, among others) but the apps were always seen as 'Novell property' despite being in branches of KDE svn and most work came from us guys at SUSE.\n\nTo deal with this situation this time around, NetworkManager-kde4 is\n* Developed in KDE trunk\n* built with standard KDE APIs and structure - In the last 2 years I have put a lot of work into Solid's networking components with NetworkManager client support as a major user.\n* modular architecture, each component is independent and as simple as possible\n* Designed with the community (starting at the KDE 4.0 launch in MV, now with Plasma team UI design contributions\n* Documented like crazy\n* Blogged about so other developers have visibility into its design and features: http://kdedevelopers.org/user/77/track\n\nI admit I can't do anything about not having as much time as I would like but we are going to do the best we can and continue to develop it actively after release.  Longer term, we want to provide a detailed Plasma Data Engine that will place the bar for modifying the UI parts of the system very low.\n\nHope that sheds some light.  If you have any other questions, fire away!"
    author: "Will Stephenson"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "nothing to say. Go for it!"
    author: "mangus"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "Thanks for your thorough explanation!\n\nI wished I saw your blogging earlier, because it probably answered all my questions :-)\n\nIt looks very promising!\n\nRegards Harry"
    author: "Harry"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "\"* Blogged about so other developers have visibility into its design and features: http://kdedevelopers.org/user/77/track\"\n\nI saw the blogpost on the Planet. And in it I saw this mockup:\n\nhttp://www.kdedevelopers.org/files/images/NetworkManager_plasma_mock1.preview.png\n\nMaybe I'm alone, but I find that overtly complicated and flashy. It has just way too much bling. I would much rather prefer a simpler, more low-key design"
    author: "Janne"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "I love it, though some simplification (who needs the IP adress visible all the time?) is probably needed."
    author: "jospoortvliet"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "The desktop plasmoid probably should be flashy, it has the space to be. As long as it resizes well when put into the panel into a easy-to-use \"connect me to the dang network\" button like knetworkmanager is currently."
    author: "Ian Monroe"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "The big lump in the mockup is the popup qgraphicswidget that you get on click.  In the panel there will be either a single icon showing merged state like at present or an icon per network interface.  The second mode allows a smaller popup for each icon which will suit small displays better.  There is some other cleverness in the popup to prevent it exploding all over the screen when you are downtown and in range of 255 wireless networks. (Not an ipod view or tabs this time, I promise)"
    author: "Will Stephenson"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-18
    body: "IMHO, the single merged icon is the better default. In fact, the \"one icon per network interface\" UI is one of the things we Fedora folks (at least those who tried it) really don't like about the current knetworkmanager-0.7. (It needlessly consumes panel space and it can also be confusing.) The GNOME nm_applet shows one merged icon and that works really well.\n\nBut if you're willing to support both, which one is the default isn't that important to us, as we can always override it in Fedora's kde-settings. :-)"
    author: "Kevin Kofler"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "I always thought that the previous knetworkmanager iterations was developed something like this... in fact it was (just like Adept, to name some program) the perfect example of how downstream shouldn't develop misusing upstream infrastructure. \nI'm really really glad that this now is fixed, kudos to Novell, Opensuse and especially you, Will! Thanks for your great work!"
    author: "Vide"
  - subject: "Re: NetworkManager plasmoid"
    date: 2008-10-17
    body: "Nothing would make me happier than if upstream had done this one and all I had to do was bash out a specfile for it... are you volunteering to maintain it for me? ;)"
    author: "Will Stephenson"
  - subject: "Simple image viewer?"
    date: 2008-10-16
    body: "Are there any simple image viewers available for KDE4? \n\no For image galleries there is a wonderful tool named Digikam\no For image viewing/simple editing there is a tool called Gwenview that is aimed to be a simple image viewer.\n\nWhat bugs me is that Gwenview seems to move towards Digikam in terms of functionality. And what is worse is that Gwenview does it bad, i.e. it is a resource-eater in any sense. More than one window of Gwenview open and I can forget comparing images on my 2GB-RAM-machine! Gwenview has some nice interface concepts though their implementation is far from being optimized (yet). But for me as an end user the question emerges: What can I use if Gwenview looks nice, but doesn't do its job? Is their anything in development? I have heard of Mosfet being back at Pixie... \n"
    author: "Sebastian"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-16
    body: "actually that is most likely a bug on your computer... I have 2GB an 2 or more instances of gwenview don't even slow anything down... the only thing that is a bottleneck here is the Amarok 2 Beta, the SQL-Database probably need some more optimizations. "
    author: "fabiank22"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-16
    body: "It is really not only NVIDIA in my case ;)\n\nI wanted to explain it, but added my typical usecase to the wrong reply - see below. "
    author: "Sebastian"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-16
    body: "<quote>What bugs me is that Gwenview seems to move towards Digikam in terms of functionality.</quote>\n\nI would suggest for KDE developers to just keep Gwenview very simple image viewer (slideshows etc very small) and just small image editing (crop, resize and rotation) and forget everything else. Then take digiKam as \"official\" photo management application what is designed for KDE4 technologies and this way we could get very powerfull photolibrary.\n\nI dont say it is bad thing for Gwenview to support kipi-plugins, but Gwenview and other application developers should work more with digiKam (kipi-plugins) developers to get things done.\n\n\n"
    author: "Mikko"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-16
    body: "I'd argue that there is no problem with making Gwenview (even) more featureful, just do it in a way that allows for simple functionality as a default with more advanced functionality available to those who want / need it. \n\nMy only concern would be start up times; as the default image viewer it would be a shame if it started to take too long to fire up after clicking an image in dolphin for example because it was busy pulling in plugins."
    author: "Borker"
  - subject: "++1"
    date: 2008-10-16
    body: "For both of you. \n\nI have to add a few more data (yes, bug reports are filed, but the developers believe, memory leaks are no bugs, but features, and postpone optimizations into possible future releases) - I want to clarify some major problems when using Gwenview:\n\nMain situation: A weekend. Take MAANNY fotos, say 500 fotos, around 1.5-2 GB in one folder. \n\n1st task: Open and view\nFull screen view takes half a minute to initialize because Gwenview tries to preload 20-30 images to appear in the thumbnail bar. Then the bar appears and slowly disappears (yes - with animation and still thumbnailing in the background eating 100% cpu). That's no fun at all if you simply want to see the single image in fullscreen mode!\n\n2nd task: Rotate (Konqueror4 does not support this handy feature anymore)\nI believe, Gwenview first extracts the JPGs into full images and stores those in memory (at least it seems so). Although the 8MPixel images are too large for my small WXGA+ screen resolution it seems to store the original size images (therefore, even a 2 GB RAM full after watching a few fotos). When editing the images, Gwenview stores the original images (to enable the \"undo\" feature) as well as the new images (off course in full size!). After rotating 20 or 30 images either Gwenview or Xorg crashes. This is the reason why Gwenview asks you since 4.1.1 to save the edited images after 20 or 30. \n\n3rd task: Thumbnailing. It has improved since KDE3 - but still: It 'feels' slower than Windows Explorer - even if the thumbnails already exist in memory (see Dolphin)\n\nI appreciate that most of you feel the same about the need of a simple viewer. I also appreciate the work going into Gwenview - it has some nice GUI concepts. The current disadvantages of those are listed above... "
    author: "Sebastian"
  - subject: "Re: ++1"
    date: 2008-10-16
    body: "> (Konqueror4 does not support this handy feature anymore)\n\nservicemenus are still supported, so if you aren't seeing this either the kde4 version installed is too old (i don't believe 4.0's dolphinview had servicemenus) or the image servicemenus were not part of the packages installed."
    author: "Aaron Seigo"
  - subject: "Re: ++1"
    date: 2008-10-17
    body: "\"Main situation: A weekend. Take MAANNY fotos, say 500 fotos, around 1.5-2 GB in one folder.\"\n\nFor me it seems that Gwenview is not designed at all for over 30+ photos.\nThats why I use digiKam what is more powerfull for such weekends when photos has got more than just a few in a day. Gwenview should be just a viewer what does not try to be photo management tool. Because then it tries to be everything and fails badly on every aspect.\n\nIf i would be you, i would just forget the whole gwenview and use it for photo viewer for photos what has downloaded from internet and not from camera. So gwenview is actually good for image viewer, not for photo viewer (two different things are images and photos!).  "
    author: "Mikko"
  - subject: "Re: ++1"
    date: 2008-10-17
    body: "> If i would be you, i would just forget the whole gwenview and use it for photo viewer for photos\n> what has downloaded from internet and not from camera.\n\nThe thing is, this does not match Gwenview target users at all. I believe most people do not browse images downloaded from internet. They just view them with a web browser. Most people however have a photo folder which they want to browse. Some of them need advanced cataloging and image manipulation. They should use digiKam. Gwenview is designed for the other users, who only need to:\n- Browse images and videos organized in folders\n- Import images and videos from a digital camera\n- Perform basic manipulations\n- View images in fullscreen and start a slideshow\n- Print one or several images at a time"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: ++1"
    date: 2008-10-22
    body: "Plus\n- Do it fast (as application)\n- Do it fast (as user, i.e. not too many clicks, keys, mouse gestures...)\n\nI reinstalled Kuickshow from KDE 3"
    author: "Sebastian"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-17
    body: "As Gwenview developer, I guess I should answer this one (thanks to Gilles for bringing this post to my attention through the kde-imaging list)\n\n> <quote>What bugs me is that Gwenview seems to move towards Digikam in\n> terms of functionality.</quote>\n\nI am adding features which I believe fits in Gwenview User Research Profile [1]. I am afraid people won't like Nepomuk integration which will hopefully be usable for KDE 4.2. They will probably don't like it either when/if a simple digital camera importer gets added.\n\nI have no plan to add support for raw images, support 16 bits per channel or advanced photo manipulation. This is definitely Digikam field. Nevertheless, both applications work on similar documents, so they are bound to overlap in some areas.\n \n>  I would suggest for KDE developers to just keep Gwenview very simple\n> image viewer (slideshows etc very small) and just small image editing\n> (crop, resize and rotation) and forget everything else. Then take\n> digiKam as \"official\" photo management application what is designed\n> for KDE4 technologies and this way we could get very powerfull\n> photolibrary.\n\nWould be a good idea, IMO.\n \n> I dont say it is bad thing for Gwenview to support kipi-plugins, but\n> Gwenview and other application developers should work more with\n> digiKam (kipi-plugins) developers to get things done.\n\nIt's true that I have been quite inactive on kipi-plugins front lately. Since my plate is quite full, I tend to focus on tasks which are only in my plate, giving \"shared\" tasks a lower priority. Give me more time to contribute to KDE and you can be sure to see me more active on kipi-plugins.\n\nInterestingly, we (kde-imaging people) will be meeting on the first days\nof November for a code sprint.\n\n[1]: http://techbase.kde.org/Projects/Gwenview/User_Research_Profile"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-17
    body: "hi & thanks for commenting :)\nin case you'll see this, i could point out what image viewer i am looking for :\n\n1. as fast to open as possible;\n2. using as little memory as possible;\n3. as fast as possible to change viewed photos;\n4. fast & using little memory when zooming into images;\n5. ability to use exif tags for image rotation (i guess supported in gwenview ;) );\n6. ability to invoke external editor on the viewed image with right mouse click context menu & keyboard shortcut;\n7. a pony. ok, not really - maybe it could brew beer :)\n\non kde3 i am using kuickshow, but it seems to use quite a lot of memory and a LOT of memory when zooming in more.\n\nso quickshow kinda fails on points 2., 3., 4., 7.\ni understand that for some operations fast and low memory usage are exclusive, but if gwenview could be better in those areas... that would be awesome :)\nthanks for developing it."
    author: "richlv"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-18
    body: "Kuickshow also depends on the obsolete imlib 1 which in turn depends on GTK+ 1 (!?!). If something like Kuickshow is really still needed, it has to be essentially rewritten (at least the imlib parts). The efficiency problems are probably also due to imlib."
    author: "Kevin Kofler"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-28
    body: "There's two versions of Imlib1, one using plain Xlib and one using GDK. KuickShow uses the plain Xlib one.\n\nAlso, you cannot really say that Imlib1 is deprecated, as Imlib2 is by no means a replacement for Imlib1. It's a completely different thing (and does not provide all features  of Imlib1, which KuickShow uses.\n\nAnd finally, Imlib1 is/was the fastest image loading/rendering library. That's one of the reasons, I used it for KuickShow. Qt's image io is slower, even Imlib2 is slower."
    author: "Carsten Pfeiffer"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-24
    body: "try gqview, its a good picture viewer. :)"
    author: "Bob"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-17
    body: "Well as kde-imaging developer and (a bit) gwenview contributor i believe \ngwenview started moving to kde4 first and now is into kde-graphics. \nTo get what you ask for, digikam should be in kde-graphics as well imo.\nMoreover i don't see why gwenview should not have some features inside, \nkipi-plugins were included in kde3 as well....\n\nkipi-plugins need more help and that is true, digikam has more developers. \nAnyway it's easy to ask for features and ask for changes, harder is to \nfind people who implement them.\n\nFrom my point of view i like using digikam for my photo collection and to \ndownload images from my camera, while gwenview to browse images using, \nof course, kipi-plugins into both :)\n\nAs said by Aurelien, we're going to meet by the end of this month to \nimprove kde4 kipi-plugins... "
    author: "Angelo Naselli"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-18
    body: "I tend to agree over the fact that all our image applications are more like image management oriented, sometimes user just wants to quickly view a photo or a set of photo and exit the application we don't work very well on that scenario. I haven't used gwenview till now so my opinion is very generic not feature requests or complaints against gwenview.\n\nOften I like to browse a whole set of photos downloaded in a folder and selectively deleted some of them quickly before organizing them using digikam. This needs a simple image viewer application with simple thumbnail functionality to quickly browse though photos.\n\nAt present the closest application I found for this is display command from image-magic, yes I have to revert to command line for being able to browse photos quickly. Display sucks too but since it takes command line argument I can very fast tell it which set of photos I want to spent time looking at. Also rotation of image is pretty much a very important feature. In KDE 3 I use the kuickshow application and showimg command line which worked pretty well, I miss them at times.\n"
    author: "Vardhman"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-18
    body: "You should probably give Gwenview a try. It is easy to achieve what you want to do with it."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-16
    body: "What about Kuickshow? In KDE 3 it is indeed a simple image viewer, loading images very fast and allowing to have a look to an entire directory just pushing Av Page. \n\nAs far as I know is still in KDE 4, but in Extragear. "
    author: "Git"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-17
    body: "Thanks. I didn't know that - it simply disappeared from my distribution..."
    author: "Sebastian"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-17
    body: "or the previewer plasmoid. It was designed for that very purpose. Dunno If your distro has it included or whether you'd have to get it from svn..."
    author: "txf"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-18
    body: "It probably disappeared for the reasons described in my reply to \"Git\". But if your distribution accepts package contributions from the community, you can always package it if you really want it (even if I doubt its usefulness)."
    author: "Kevin Kofler"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-18
    body: "Be warned that:\n* there are no releases of Kuickshow for KDE 4, it's only in SVN,\n* it doesn't look very actively maintained,\n* it requires an obsolete image loading library, see: \nhttp://dot.kde.org/1224100877/1224149076/1224163469/1224196666/1224253800/1224342865/\n* some distributions already didn't include it by default in KDE 3 (Fedora shipped it in a kdegraphics-extras subpackage which wasn't installed by default),\n* it has only actually compiled for KDE 4 since July 19, 2008 (which explains why it isn't widely packaged),\n* it won't help with Gwenview performance problems, its performance is even worse and\n* Gwenview essentially subsumes all its functionality."
    author: "Kevin Kofler"
  - subject: "Re: Simple image viewer?"
    date: 2008-10-22
    body: "I use it now again (KDE3). The features Kuickshow offers are sufficient and still faster than the according implementation of Gwenview\n\n-> opens in a view showing no controls, but the window only.\n-> resizes properly to the maximum size and keeps aspect ratio while zooming (then I can view 2 or more images next to each other using two kuickshow processes)\n-> simple manipulations (eg. rotation) using simple keys, eg. '9' for 90\u00b0 right\n-> simple rezooms/resets of view, eg. 'm' for maximum watchable image resolution or 'o' for 100% zoom. \n-> simple navigation through a folder with simple file actions (delete, move to trash, save)\n\nIs there any of these features where there is some application doing it better/faster? Gwenview offers a nice GUI for these actions, but it \n(1) requires more user interaction (eg. not enough keyboard shortcuts, just toolbuttons, file actions only available when enabling additional space filling panels, manual window resizing by mouse required etc.)\n(2) is slower in doing so due to the overhead (background thumbnailing, animations, bad optimizations etc.)"
    author: "Sebastian"
  - subject: "Thanks for the info"
    date: 2008-10-23
    body: "I used Kuickshow to browse images; I mean, click on a image file and see the image in fullscreen. Other programs load a GUI (as Gwenview) but I just wanted a fast viewer. I've found ShowImg, it opens fast in fullscreen mode and allows to browse current directory, and has a shortcut to exit: in one word, it's agile. \n\n\n\n\n"
    author: "Git"
  - subject: "Showfoto"
    date: 2008-10-17
    body: "Showfoto is part of digikam and one of my favorite KDE apps."
    author: "Ian Monroe"
  - subject: "Thanks"
    date: 2008-10-17
    body: "It seems sometimes so hard to imagine just how fast KDE 4 has developed and grown since it's first public outing.  Thanks to everyone that brings this great piece of candy to our computers, from the developers to the people filing bug reports to everyone that works on it, thanks.  And Thanks Danny for plotting how it is progressing through the commit Digests"
    author: "R. J."
  - subject: "MID-panel?"
    date: 2008-10-17
    body: "There was a mention of a panel aimed at \"Mobile internet devices\". Are there any additional details available about that?"
    author: "Janne"
  - subject: "Re: MID-panel?"
    date: 2008-10-17
    body: "other than \"we're still working on it .. progress is being made\"? =) when there's something more fun and intereting than that, i'll be sure to blog about it again."
    author: "Aaron Seigo"
  - subject: "Web Shortcuts from right click"
    date: 2008-10-17
    body: "niiice, I appreciate the feature !\nnow the dialog could be less cryptics... I mean :\n\n  Search Provider Name:_______\n  URIs shortcuts:______\n\nwtf ? \nI know web shortcuts from the kde 3 usage but this I do not understand.\nhow about an help item in this dialog to explain what these are and how they can be used ?\nI typed \"something:\" in the URIs instead of \"something\" and it did not work... it should say wrong syntax no ?\n\nthanks !"
    author: "lionel"
---
In <a href="http://commit-digest.org/issues/2008-09-28/">this week's KDE Commit-Digest</a>: Continued work on PowerDevil, and the "NetworkManager" and "Weather" Plasmoids. Monochrome action icons in <a href="http://plasma.kde.org/">Plasma</a> expand to cover KRunner. A first working version of QEdje script engine, and the import of a "Window Manager" runner. Work on new containments and a mobile internet devices (MID) panel in Plasma. Various improvements in <a href="http://konsole.kde.org/">Konsole</a> and the Kvkbd keyboard utility. Support for adding actions implemented by <a href="http://kross.dipe.org/">Kross</a> scripts in Lokalize. First version of a MathML presentation markup importer in <a href="http://edu.kde.org/kalgebra/">KAlgebra</a>. Start of work on a Mollweide projection in <a href="http://edu.kde.org/marble/">Marble</a>. More work on integration of Jabber-based network games in <a href="http://home.gna.org/ksirk/">KSirK</a>. Continued work towards <a href="http://amarok.kde.org/">Amarok 2.0</a>. Better support for LilyPond links in and the "--unique" command-line switch (similar to KDVI) in <a href="http://okular.org/">Okular</a>. A new version of Klotz (previously KLDraw) with database update functionality is imported into playground/graphics. The Paint.net red-eye reduction algorithm is incorporated into <a href="http://gwenview.sourceforge.net/">Gwenview</a>, using a "iPhoto-inspired" interface bar. Start of a DNG image format converter in KIPI plugins (used in <a href="http://www.digikam.org/">Digikam</a>, etc). Various work on filters in <a href="http://kst.kde.org/">Kst</a>, including Butterworth, and Linear Weighted Fits plugins. Support for auto-saving/restoring opened tabs in Akregator. A "cost breakdown" view in <a href="http://koffice.kde.org/kplato/">KPlato</a>. The ability to create web shortcuts by right-clicking on the line edit of a search field in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Support for subscript and superscript in KRichTextWidget. Import of KDE Partition Manager to KDE SVN. Ruby and C# bindings are promoted to the KDE 4.1 release branch. Various Plasma applets move to kdereview for official inclusion in KDE 4.2. Amarok 1.92 and KDE 4.1.2 are tagged for release. <a href="http://commit-digest.org/issues/2008-09-28/">Read the rest of the Digest here</a>.


<!--break-->
