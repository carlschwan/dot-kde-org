---
title: "Support Amarok This Roktober '08"
date:    2008-10-05
authors:
  - "mkretschmann"
slug:    support-amarok-roktober-08
comments:
  - subject: "instead of Rocktober"
    date: 2008-10-05
    body: "How about Rocktoberfest? =)"
    author: "Patcito"
  - subject: "Re: instead of Rocktober"
    date: 2008-10-05
    body: "Hehe. Well the name Roktober is a tradition, we've always used this name for the fundraiser.\n"
    author: "Mark Kretschmann"
  - subject: "Re: instead of Rocktober"
    date: 2008-10-05
    body: "We've called it Roktoberfest before.  :)"
    author: "Ian Monroe"
  - subject: "What about Amarok2 builds for some users?"
    date: 2008-10-05
    body: "As much as I love Amarok1, I am too excited for Amarok2 but its hard to support it (either financially or with bug reports and feedback) if the only way to test it is by using *buntu or compiling it ourselves.\n\nIs there no developer or community member able to give nightly/weekly/monthly builds for other GNU/Linux distros of this new code?"
    author: "lefty.crupps"
  - subject: "Re: What about Amarok2 builds for some users?"
    date: 2008-10-05
    body: "Project NEON (which is the official name for our nightly builds) is not specific to Ubuntu, but instead a generic framework for generating packages.\n\nAs far as I know there is work undergoing to provide NEON packages for OpenSUSE and possibly also other distros.\n\nYou can talk to the NEON developer on this mailing list:\n\nhttp://groups.google.com/group/amarok-neon\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: What about Amarok2 builds for some users?"
    date: 2008-10-05
    body: "There are also builds for Fedora in rawhide and in kde-redhat unstable for Fedora 9 at least - though I'm not sure of the frequency of these, not sure if there are any builds newer than the beta. I wouldn't be surprised if some of the other distros also have builds in their development repositories"
    author: "Simon"
  - subject: "Re: What about Amarok2 builds for some users?"
    date: 2008-10-05
    body: "Most distros have builds of the betas. We plan on releasing a new beta every couple of weeks, though this last one was held back a couple of weeks."
    author: "Ian Monroe"
  - subject: "Re: What about Amarok2 builds for some users?"
    date: 2008-10-06
    body: "For OpenSuse search for Amarok in the build service at: http://software.opensuse.org/search/\n\nMaybe they also have builds for other distros there."
    author: "Sven Krohlas"
  - subject: "The button"
    date: 2008-10-06
    body: "That button in the picture, is it availible from somewhere? I'd love one of those as a \"travel bug\".\n"
    author: "Oscar"
  - subject: "Re: The button"
    date: 2008-10-06
    body: "No, it's virtual."
    author: "Mark Kretschmann"
  - subject: "Re: The button"
    date: 2008-10-06
    body: "Are there any other Amarok or KDE merchandise about 5 in diameter (buttons) or 10 cm high (figure or similar) in a rugged material availible from somewhere?"
    author: "Oscar"
  - subject: "Roktober & Udo Lindenberg"
    date: 2008-10-07
    body: "\nhttp://www.derwesten.de/nachrichten/kultur/musik-und-konzerte/2008/10/2/news-80828260/detail.html"
    author: "Ralph M\u00fcller-Welt"
  - subject: "Re: Roktober & Udo Lindenberg"
    date: 2008-10-07
    body: "Heh, I don't really see the connection, but thanks for the link. Lindenberg is pretty cool ;)"
    author: "Mark Kretschmann"
  - subject: "Re: Roktober & Udo Lindenberg"
    date: 2008-10-07
    body: "not so cool:\nhttp://www.heise.de/bilder/106992/0/1\nhttp://www.musikindustrie.de/fileadmin/news/politik/downloads/Offener_Brief_TdGE_FINAL_080424_engl.pdf"
    author: "testerus"
---
Another year has passed and the Amarok team wants to celebrate with its fans: it's <a href="http://amarok.kde.org/en/node/548">Roktober</a> again! This is the time of the year when the Amarok team reviews what they have achieved during the past year and call for your help for the coming year.




<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right;">
<img src="http://mark.kollide.net/fundraiserWoofy.jpg" width="420" height="475" />
</div>

<p>
Your donations of time and money make it possible to create the awesome music player you love so much.
</p>

<p>
Our aim for this Roktober is to raise &#8364;10,000. Our budget is aggressive, but as costs go up, and our plans get more complex, we have to raise the goal. As in past years, for every &#8364;10 donated, you will receive one entry in the drawing for an iAudio7. This year we have two grand prizes and we will also give t-shirts to 4 second chance winners.
</p>

<p>
At Akademy 2008 Amarok won the prestigious <a href="http://amarok.kde.org/blog/archives/796-We-Win!.html">Best Application</a> award at the <a href="http://www.kde.org/history/akademyawards/">Akademy Awards</a>. Your donations make it possible to continue this work, producing what is the best music player for Linux, and will shortly become the best music player on any platform. Amarok 2 is about to let a second beta out of the bag and we look forward to a ground breaking final release.
</p>




