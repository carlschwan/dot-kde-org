---
title: "KOffice Releases 10th Alpha of KOffice 2.0"
date:    2008-08-29
authors:
  - "brempt"
slug:    koffice-releases-10th-alpha-koffice-20
comments:
  - subject: "Krita"
    date: 2008-08-29
    body: "Wow! You guys rock!\n\nJust a question: Why does the screenshot show two scrollbars?"
    author: "KDE 4.1"
  - subject: "nice Work!"
    date: 2008-08-29
    body: "Congrats!! i really love the progress of KOffice, i know here isn't the\nright place to make wishes but i'm lazy to go to the right one..\nwell my wish would be to KPresenter to have a layout to widescreens\ncause that would look nice on my laptop, i know most projectors don't\nuse a wide resolution but not always i use that.. :)\n\nKeep the good work ;)"
    author: "dantti"
  - subject: "Nice Work"
    date: 2008-08-29
    body: "Nice work guys,\n\nI am already running the alpha right now and it surely is shaping up nicely.\nKeep up the good work."
    author: "Mark Hannessen"
  - subject: "Congratulations!"
    date: 2008-08-29
    body: "you're making a wonderful office suite, congrat!! keep it up with this great work!\n\nbye\nMarcello"
    author: "killer1987"
  - subject: "Beta version / functions"
    date: 2008-08-29
    body: "For the beta version coming up, is there a user manual describing all functionality to try out in KOffice 2?"
    author: "Axel"
  - subject: "Re: Beta version / functions"
    date: 2008-08-30
    body: "Not yet -- and it's unlikely we (that's the developers) will manage to come up with one. I try to write bits and pieces of the Krita manual when compiling, but I'm not making much progress. And I've started with the plugin writer's part of the manual first."
    author: "boudewijn rempt"
  - subject: "Re: Beta version / functions"
    date: 2008-08-30
    body: "Ok, hope the GUI is intuitive then.\n\nMaybe there's a list of functions, and where to find them in the GUI."
    author: "Axel"
  - subject: "SDK"
    date: 2008-08-29
    body: "KOffice is really looking good.  Thank you for all hard work.  I know the team is busy building out the software, but when do you think we can see a Plugin/SDK/Documentation for programmers to utilize KOffice in other applications.\n\n"
    author: "Mike"
  - subject: "Re: SDK"
    date: 2008-08-30
    body: "KOffice1, and probably KOffice2, provides a KPart for using it in applications. Also, I'm pretty sure Flake is exported so 3rd-party applications can use it as well.\n"
    author: "Michael \"KParts\" Howell"
  - subject: "Re: SDK"
    date: 2008-08-30
    body: "Today! :)\n\nhttp://techbase.kde.org/Development/Tutorials#KOffice\n\nNote that koffice2.0 will not have a source/binary-compatibility yet. This is postponed to the 2.1 release."
    author: "Thomas Zander"
  - subject: "Bug Reporting"
    date: 2008-08-30
    body: "Is bugs.kde.org the place to report bugs for KOffice2?  I was playing around with the different shapes.  I embedded a picture in a new document and put in some text to wrap around it.  However, the picture doesn't save with the document.  In other words, when I save and close the document and then open it up again, the picture is no longer there.  All that remains is the text.\n\nI really like how KOffice2 is shaping up as well.  The floating toolbars is nice, as are all the different flake shapes.  I also like how easy it is now add a background to a page.  It seems like it will be very easy to create flyers, bulletins, newsletters, etc. when this new version is complete.  Not to mention professional looking letters and such.  Looking forward to the final product.  Keep up the good work."
    author: "cirehawk"
  - subject: "KOffice for Windows"
    date: 2008-09-02
    body: "As my only PC is running on Windows, I wonder that will KOffice for Windows be available at the same time as the version for Linux? I hope it will happen."
    author: "Jo"
  - subject: "Re: KOffice for Windows"
    date: 2008-09-02
    body: "It may not be exactly the same time, but we try to make all releases of KOffice available on windows as soon as possible. Probable gotchas may be compilation problems (almost all developers work on Linux, so it's easy to introduce little linuxisms) or just the time the windows people have available -- it's a very, very small team that's doing this."
    author: "boudewijn rempt"
---
The <a href="http://koffice.kde.org/">KOffice</a> team, developers, students, packagers and bug reporters have prepared the final Alpha release of KOffice 2.0: KOffice 2.0 Alpha 10. KOffice will enter feature freeze in two weeks, so the next release will be the first Beta. And we are committed to releasing as many Beta's as is necessary before declaring Release Candidate status for KOffice 2.0. Read on for more details.


<!--break-->
<p>This Alpha release contains <a href="http://dot.kde.org/1219663347/">all the work done by the Google Summer of Code students</a>. Remember, these are: a bristle-based brush engine for Krita, a calligraphy tool for Karbon (which is available in all applications), a quantum leap in KWord ODF support, especially for styles, lists, page styles, a .doc to .odt conversion filter, a .kpr to .odp conversion filter, the presentation view for KPresenter, and the Kexi web forms feature...</p>

<p>Other highlights of this release are:</p>

<ul>
<li>Presentation view for KPresenter now working</li>
<li>A new style docker in KWord for page and paragraph styles</li>
<li>A new shape style docker that handles the colors, patterns and gradients of shapes in a uniform way.</li>
<li>Many bug fixes in Krita (note: Krita's filter handling got inadvertently broken just before tagging. It's repaired by now, but the fix is not in Alpha 10)</li>
</ul>

<p>As well as many bug fixes, stability improvements and ODF compliance, there are improvements all over KOffice.</p>

<p>Please go to the <a href="http://www.koffice.org/announcements/announce-2.0alpha10.php">announcement</a> for more information, the <a href="http://www.koffice.org/announcements/changelog-2.0-alpha10.php">Changelog</a> for a list of all the changes the developers could remember, the <a href="http://www.koffice.org/announcements/visual-changelog-2.0-alpha10.php">Visual Changelog</a> for screenshots and the <a href="http://www.koffice.org/releases/2.0alpha10-release.php">Release Notes</a> for download information.</p>

<p>And in the meantime, enjoy this screenshot of artwork done in Krita by the French artist Enkhitan:</p>

<a href="http://static.kdenews.org/dannya/krita_girl_by_enkhitan.png"><img src="http://static.kdenews.org/dannya/krita_girl_by_enkhitan-thumb.png"></a>


