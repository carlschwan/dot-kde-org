---
title: "The First Day of the KDE 4.0 Release Event"
date:    2008-01-18
authors:
  - "jpoortvliet"
slug:    first-day-kde-40-release-event
comments:
  - subject: "To clarify"
    date: 2008-01-18
    body: "The DTLS plugin for QCA is a project I had already started before the event and I certainly didn't complete it today. :)  I did make good progress though, and today I solved another OpenSSL mystery (those are the best kind).\n\nIt was great to meet everyone in person.  See you again tomorrow."
    author: "Justin Karneges"
  - subject: "Re: To clarify"
    date: 2008-04-17
    body: "How is this work advanced today ?\nis there any other dtls provider implemented for QCA ?"
    author: "mootrul"
  - subject: "Try Mercurial, not only git"
    date: 2008-01-18
    body: "Git is the new cool kid in town, but when looking at Distributed SCM, also have a look at Mercurial (http://selenic.com/mercurial/). Especially at this great book, teaches also a lot about DSCM: http://hgbook.red-bean.com/ .\n\nMozilla, OpenSolaris, Xen, NetBeans & Co. use Mercurial for a reason!"
    author: "Frinring"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "same goes for git and linux, xorg, wine, samba, $distributions, coreutils and many others. ;)\n\nin our case, hg was too slow and the developers were arrogant."
    author: "kajak"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "Too slow? What operations? Mercurial is pretty fast (by design), one of the fastest (uhm, right, where are the statistics?)."
    author: "Frinring"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "ours is this one, but there are some more there:\nhttp://git.or.cz/gitwiki/GitBenchmarks#head-85df1bb7f019c4c504e34cde43450ef69349882f\n\nof course, maybe in two years the git developers didn't improve anything and the hg ones made a miracle. ;P"
    author: "kajak"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-19
    body: "You honestly think 2 year old benchmarks are still relevant? Oh man..."
    author: "Robin"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-19
    body: "I don't, that's why I said \"it *was* too slow\". Maybe if it had been good then, we would have used it, but it is too late now."
    author: "kajak"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "In my experience Mercurial is *faster* than git for operations involving much disk access (that means most operations). For SCMs the bottleneck is disk seek time and Mercurial apparently has a well-tuned file layout to decrease the number of disk seeks. This is not very important, though. If git became as user friendly as Mercurial tomorrow I wouldn't care which one was chosen - they are both fast."
    author: "Andreas"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "Please explain in what way hg is more user friendly than git? I was able to start working with git in 10 minutes (same with hg by the way), it's just the same clone, pull, commit, checkout, merge, branch etc... I find it much easier than svn actually but just as easy than hg."
    author: "Paul"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "I'm surprised you found the hg developers arrogant; in my experience the hg community has been a rather friendly one both to new developers and to users."
    author: "Mark Williamson"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-18
    body: "I'd agree; they've been very friendly and open. Speed-wise, I think things have certainly improved over two years -- certainly most reports I've read place mercurial and git as being broadly similiar in terms of speed. Mercurial seems to edge out git in terms of being portable (problems with running git on Windows is cited a lot); git seems to be developing a few features that those who use a Linux-like development model appreciate.\n\nEric Raymond has been doing an interesting overview of the VCS scene at: http://thyrsus.com/hg/uvc/ for anyone else interested in comparing these projects.\n"
    author: "Danny O'Brien"
  - subject: "Re: Try Mercurial, not only git"
    date: 2008-01-20
    body: "I did try Mercurial. I even have an import of kdelibs into it (svn -> git -> hg), but I did not like the results. The branches just mumbled up into an incoherent pattern.\n\nSo, take this as a call for help. I know Git and I know how to make the conversion using it. But I don't know Mercurial or DARCS or Bazaar. Please, come and help us investigate the KDE move into those.\n\nUse the kde-scm-interest@kde.org mailing list."
    author: "Thiago Macieira"
  - subject: "User please"
    date: 2008-01-18
    body: "Look like the 1st day is all about developer's corner. Hope to be able to see user's corner on the next day. :)\n\nGreeting from Cambodia."
    author: "Leang Chumsoben"
  - subject: "I wonder..."
    date: 2008-01-18
    body: "I wonder how many of the people there were actually using KDE 4.0 as their desktop..."
    author: "zero"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "I once brought a windows notebook to a linux conference. Forgot to turn off the sound. Everyone turned around when i logged in. Dammit.\n\nDisclaimer: It Was Not My Notebook. I dont have windows machines."
    author: "dooh"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "The funniest variation on this theme I've seen was the HP Linux Roadshow.  The organizers got confused when I gave them an OpenOffice presentation and informed me that everyone else would be using PowerPoint.  Fortunately, I had OpenOffice for Windows burned on the same CD as my presentation, but then the complained because \"it was a Sun product\".  I groaned."
    author: "Scott"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "You could just claim you are working on KDE on windows at this one - people would even believe you so long as you were smart enough to download the source and had a compiler of some sort.   Knowing something about programming would be a bonus though as otherwise someone would try to get into a technical discussion with you and find you out.   (I suppose they would eventially unless you really are working on kde-on-Windows)."
    author: "Hank Miller"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "You can consider yourself lucky to not got stoned ;)"
    author: "anon"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "I wonder if the reaction of the Google engineers to 4.0 was as vitriolic as that of most blogs/ end-users.  I hope they can see the big picture and aren't heckling the KDE guys after they give their talks :("
    author: "Anon"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "I tell you Anon, the KDE devs went through hell on the net in the past few months. I think that it has passed the pain barrier in the meantime. I don't think that the Google people will have the same attitude as the multitude of basher all over the net. Technical people can understand what's going on better than me (even though I can see a lot of improvement as a user) or the thousands of clickies who decide that KDE 4.0 is shit after taking two clicks."
    author: "Bobby"
  - subject: "Re: I wonder..."
    date: 2008-01-18
    body: "Actually, I'm seeing the blogs running at 80% positive, these are people who get that 4.0 is a .0 release.  The 20% negative blogs come from those who missed the message, or are well known for their *ahem* less favourable view of KDE anyway..."
    author: "Odysseus"
  - subject: "Re: I wonder..."
    date: 2008-01-19
    body: "after KDE 4.0, \".0\" will never be the same ;)"
    author: "marcopolo"
  - subject: "Re: who is the girl"
    date: 2008-01-18
    body: "I read your links Chani, still I know many girls from my promotion and the reason they wanted to be project manager instead of coder was not because of sexism, the reason is because they hate coding, that's the reason, like it or not I've never met a girl who actually like to code software except on the internet, all the girl at school hated it. Now that might be pure bad luck but I'm skeptical.\n"
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-18
    body: "First thanks to the moderator who deleted the original thread. Please also delete this one!\n\nAnd \"Paul\", for your information, Chani _is_ a coder and a girl, which proves you wrong, and there are several other highly skilled coder girls in the KDE community, so your posts only make you look retarded and offend other people, so please just go away."
    author: "Anon"
  - subject: "Re: who is the girl"
    date: 2008-01-18
    body: "As I said in my deleted post, women do have the capacity to be good coders and many of the best programmers in history were women. Now it's a fact that there are very few women in the field today compared to men, and this is because most of them don't like coding, no because of sexism. If you really like something, you don't let sexism get in your way. I personally love cooking and I don't care if every time I cook something I get sexist joke from some people (and those include women, no kidding!), I just laugh at it and enjoy my cooking. If you let stupid remarks get in your way than maybe you don't really like what you're doing enough.\nAnd I still stand that saying that a women looks sweet is not inapropriate unless you're a taliban or a neocon or something, and I've worked enough with women to notice that they say the same thing about us so please."
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-18
    body: "Don't know why you keep bringing the Taliban up.  From my understanding they make women cover up head-to-toe because they think men can't be trusted to behave themselves when presented with the female form, ie act like young boys.  This is why I thought your characterization was odd/ironic.\n\nLeaving aside the causes of the gender imbalance in programming (seen in other fields in both directions), I also find it odd when I travel to places like Italy where men seem to have carte blanche to hit on women loudly and unrepentantly in the street, almost as a hobby.  Never understood that one, must ask an italian lady someday."
    author: "Ben"
  - subject: "Re: who is the girl"
    date: 2008-01-18
    body: "> Don't know why you keep bringing the Taliban up\n\nBecause the Talibans reprehend everything that might leads to seduction between man and woman, they even forbid woman from waring white socks cause it's too sexy.\nWell, when I see people getting outraged at a man saying to a woman she looks sweet as if this would lead to sexual assault then yes, it's ridiculous and it looks like Taliban thinking."
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-18
    body: "the numbers about women's involvement in KDE, extracted from the commit demographics of the last 5 commit digests:\n\n06.01.2008: 5,42%\n30.12.2007: 5,64%\n23.12.2007: 3,84%\n16.12.2007: 4,83%\n09.12.2007: 5,45%\n\nwhether this 5% correspond to programming or to translations and other non programming activities I cannot say.\n\n\n"
    author: "patrice"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "paul. stop making generalizations,\n\nplease tell me the sample size of women you've worked with that would allow you to draw a conclusion for the entire gender.\n\neveryday people deal with sexism, racist, classism, homophobia, etc.. and to a great deal of people it does prevent them from doing things they would enjoy.\n\nit's funny you used an cooking example to highlight how you're comfortable doing what? women's work? you fucking sexist, you're too stupid to even realize."
    author: "mike"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "> please tell me the sample size of women you've worked with that would allow you to draw a conclusion for the entire gender.\n\nWell, a sample of women in computer fields is always hard because there are so few of them. In my graduation class, there were 5 women and 25 men, none of these women wanted to code.\n\n> it's funny you used an cooking example to highlight how you're comfortable doing what? women's work? \n\nRe-read my comment, I said I enjoy cooking even though I have to put up with people making stupid remarks about how cooking is a girl thing. That's stupid of course but it doesn't stop me from cooking. There will always be ignorant people that think that cooking is a girl thing and those ignorants won't stop me from enjoying cooking. Get it now? Thanks for the nice words though..."
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "There are plenty of women (not girls!) who do enjoy coding, I know many of them, and most of them are far better programmers than I will ever be.  I also know several who have moved on to management or architecture roles, mostly because they are good at it too and want to further develop their careers (just as many men do), but also a little bit to escape the sexist attitude that seems to permeate the industry.  It's little wonder the gender ratio is so unbalanced with such ignorant viewpoints as yours.\n\nJohn.\n"
    author: "Odysseus"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "What's ignorant about my viewpoint? All I'm saying is that there are very few women coding compared to men, which is true, and that this is not because of sexism but because they don't like coding (znd those that do code though are just as good as men at it). If they really liked it they would do it, believe me women can do what they want nowadays in this part of the world.\nNow the reason they don't like coding is not genetic, just purely historical and social just like in science in general unfortunately."
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "The obvious correction: Women and men do have the same genes.\n\nThe less obvious: You say it's only \"historical and social\" reasons. And just before that, you explain it's not \"sexism\". \n\nSo all it's about, is if there historical and social reasons are sexism. For some reason you don't want to accept, that the decisions women make are not limited by being a woman in a society that discriminates against women. \n\nNot only do my female programmer colleagues get less money than I do. They also get a consistent resistance to that they are coding and consistently are subject to increased interest for some colleagues and clients, that want them to primarily socialize with them.\n\nI would say, women have a hard time to constantly expose themselves with individual achievements and deeds in a mixed group and get pushed out by male majority. \n\nI call it sexism when the reaction to your person is determined by your gender, and you may call it social. Although, I tend to think of the behavior in concrete as quiet anti-social.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "The ignorant part I refer to is your view that women do not enjoy coding.  That's a sweeping generalisation drawn from your too limited experience and sample pool, and I can produce dozens of counter-examples to disprove your theory.  Lets try a few: B, who has a Masters in Operations Research, who is always to be found at work late trying to get that elusive algorithm optimisation just right; L, who hacked the basics of a middleware system over a weekend just for the challenge of doing so long before middleware was even a buzzword (she's now lead architect, but is known to sneak in on weekends to do a little hacking just to keep her hand in); K who will re-write something 5 times over just to reach that level of elegant simplicity she likes in her code.  These women LOVE coding, they love the challenge, they love the logic and maths and creativity that goes into it, just the same as I do.\n\nThe difference, I suspect, is that they had the opportunity to discover that this was something they loved, instead of being discouraged by the image that surrounds computing and the sciences in general, and the behaviour of all too many of its practitioners.\n\n\"Now the reason they don't like coding is not genetic, just purely historical and social just like in science in general unfortunately.\"\n\nOh so close, your deduction on the reason for few women is bang on the money, its such a shame it follows such a false premise.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "> These women LOVE coding, they love the challenge, they love the logic and maths and creativity that goes into it, just the same as I do.\n\nI already said that there has been and always will be women that love coding. The reality is (check the number and not only the KDE ones) that most people coding are not women but men so either women are bad at it (which is not true as some are very good at it) or they don't like it for social/historical reason.\n\n> Oh so close, your deduction on the reason for few women is bang on the money, its such a shame it follows such a false premise.\n\nSexism is a pretty strong word for what you are describing, telling a woman that she looks nice is not sexism.\nCheck out from wikipedia:\n\n\"Sexism is commonly considered to be discrimination and/or hatred towards people based on their sex rather than their individual merits\"\n\nNow this was neither discrimination nor hatred. Got to Iran or Saudi Arabia, there you will meet real sexism, stop using this word for innocent compliments it will make it lose its real meaning and it is not helpful for people that are the real victims of sexism. Actually it's an insult to the real victims of sexism."
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "You don't know what you are talking about, there are at least the same number of women programming as of men, the thing is they are more discrete.\n\nDo you know who Alan Cox is? You do. But do you know who in fact codes most of his patches? His mom. Yes, his mom. And what about Linus' wife, all people know is that she was a karate champion, that's to keep a low profile, actually she is the hax0r behind git and most of the original kernel source.\n\nFinally, are you really that silly to think that only less than 5% of the programmers are women only because you see that ratio in college or in the office?\n\nI give you a hint, women don't study computer sciences in college, because they are born with programming skills and willing to keep a low profile about it.\n\nYou are not only sexist and antisemit, but also a misogenic zionistic nazi.\n\n"
    author: "albert"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "I am the girl.  I don't know what the first comment said, but I'm the one in the middle of the third picture (the only girl I see in any of the pictures).  I am not a contributor to KDE, I'm not a \"Computer Science\" anything, but I am a KDE enthusiast.  This is my first community event and I'm happy to be here. Just wanted to say how weird it is to be reading a thread about myself on the dot :-)"
    author: "Kacie Gene Sander"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "the first comment was by a guy who was very shy to write his name, he says that he found you sweet;) then a polemic raised that it is not decent to say that to a woman.  \n\n"
    author: "djouallah mimoune"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "hey kacey! Some guy (not me) said you 'looked sweet' which started an outrage of people saying how sexist that was. Please confirm or infirm if there's anything wrong with that comment. Cheers"
    author: "Paul"
  - subject: "Re: who is the girl"
    date: 2008-01-20
    body: "I obviously don't have the entire context of the comment, but I don't think of \u0093sweet\" as a sexist remark.  I can\u0092t generalize for the opinions of women everywhere, but it doesn't bother me.  \n\nI had an amazing weekend, and I am so glad that I got to be there!  Thank you to everyone for contributing to such a wonderful atmosphere and great community... I'm so happy to be a part of it!  -- Kacie Gene"
    author: "Kacie Gene Sander"
  - subject: "Re: who is the girl"
    date: 2008-01-20
    body: "<3"
    author: "maninlove"
  - subject: "Re: who is the girl"
    date: 2008-01-20
    body: "Yes, let's hear from Kacey since she can be the spokesperson for all girls in the world. *sigh*\n\nI just hope that this somewhat depressing thread don't discourage girls from ever coming to a KDE event again.\n\n"
    author: "Oscar"
  - subject: "Re: who is the girl"
    date: 2008-01-19
    body: "o rly? nothing better to do today gents?"
    author: "illogic-al"
  - subject: "Discussion request: Amarok 2.0"
    date: 2008-01-18
    body: "I would like to request that you guys please discuss Amarok 2.0 at the Un-conference.\n\nI would love to see that project moving forward and more features and integration added to it.\n\nThanks and Party for us all!!! :)\n\n"
    author: "Max"
  - subject: "Re: Discussion request: Amarok 2.0"
    date: 2008-01-18
    body: "Jeff (an Amarok developer) is giving a talk on Amarok and KDE multimedia on Saturday."
    author: "Ian Monroe"
  - subject: "Re: Discussion request: Amarok 2.0"
    date: 2008-01-19
    body: "Cool... Looking forward to the discussion.. :)"
    author: "Steve M"
  - subject: "KDE on Windows / Mac"
    date: 2008-01-18
    body: "Could you guys also discuss KDE on Windows / Mac at the un-conference?\n\nI think Google would be really helpful with that, it would also offer KDE to a wider audience. (side effect: it would slowly wean people off Windows. :) )\n\n"
    author: "Max"
  - subject: "Re: KDE on Windows / Mac"
    date: 2008-01-19
    body: "i dont think running kde apps on windows will ween them off. afterall they're using it to run the kde apps! think about it.. you have windows, your games work, your os is the most supported in the world, then kde apps are available on windows, great.. you get to use those, but why would you want to switch? you're now happy.. kde has to be better to cause people to switch, and the price tag isnt going to do it. we've seen dell's sell for $25 less than their windows cousins, but... if you're unfamiliar with linux is it worth it to switch to save $25? nope... never.. kde has to be better, that's it, nothing else.\n\n\nkde on windows is not about grabbing windows users, it's about having platform independant software so users can choose."
    author: "mike"
  - subject: "Re: KDE on Windows / Mac"
    date: 2008-01-19
    body: "Its more than that too - its also for people like me.  I use Linux and KDE at home, (almost) everywhere (I use a Mac for video editing and for my gf to use).  My media pc's run Linux, my audio recording pc is Linux, my primary workstation in my home office is Linux - but I can't use Linux for work.  I need AutoCAD (no comments about QCAD or other projects - they don't have dwg compatibility, which is what I need.  If someone wants to start a project using Microstation's now open dgn format, thats definitely a great step - but alternatives to AutoCAD for what most AutoCAD users need do not exist).\n\nSo... my laptop runs Windows, because I need AutoCAD on the go.  Today (Saturday, after the KDE-On-Windows talk), I realized there is now an easy tool - and I have it up and running.  Its great to be able to use open source apps I feel more comfortable with, while still having AutoCAD to get work done on the go.  Thanks go out to the KDE-Windows team for their (continual) hard work :)"
    author: "Joseph Gaffney"
  - subject: "Re: KDE on Windows / Mac"
    date: 2008-01-19
    body: "There are many people who are terrified of open-source, especially in the corporate world.\n\nFirefox and Apache have done a great deal towards altering that view.  KDE apps on Windows would help even more."
    author: "T. J. Brumfield"
  - subject: "Aaron's keynote"
    date: 2008-01-19
    body: "Hey, where is aaron's keynote? I think he already held it a few hours ago, or am I wrong? Isn't it up on youtube or google video or somewhere?"
    author: "Thomas"
  - subject: "Re: Aaron's keynote"
    date: 2008-01-19
    body: "Have patience - apparently, videos will be announced on the Dot when they have been processed and uploaded."
    author: "Anon"
  - subject: "Re: Aaron's keynote"
    date: 2008-01-20
    body: "Have patience?\n\nHell, no! \n\nGive out that URL already, you... you... you...   :-)\n\n/me threatens to switch over to Gnome, starting at midnight (unless that YouTube video link appears pronto-pronto)"
    author: "Repre Hensor"
  - subject: "Re: Aaron's keynote"
    date: 2008-01-20
    body: "We are being punished.\nRightfully so.\n\nAfter all it is our own fault for not attending or organizing local release event parties ;-)\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Aaron's keynote"
    date: 2008-01-20
    body: "Rightfully punished?!?\n\nAs if it were my Free Will to not attend! I can not _afford_ to travel from Z\u00fcrich to Montain View or Hamburg or Bonn or Berlin. But I can afford the YT download....\n\nI'm punished already not being able to afford the travel.\n\nAnd you wouldn't want to come to any event that I do (dis-)organize... Honest."
    author: "repre hendor"
  - subject: "Re: Aaron's keynote"
    date: 2008-01-20
    body: "First, it was meant jokingly.\n\nSecond, a local party doesn't have to be huge.\n\nThird, there is often also the option to persuade others that organizing such a thing would be a great idea :)"
    author: "Kevin Krammer"
  - subject: "Re: Aaron's keynote"
    date: 2008-01-20
    body: "Tooooo late to pull out that \"persuade-others-that-organizing-such-a-thing-would-be-a-great-idea\"-option.\n\nI'll instead concentrate on my \"persuade-the-gatekeepers-to-make-the-video-youtube/google-available\" second choice. That's not too late.\n\n/me eyes his Gnome-install-CD to backup his former threat and tries to make an impression of resoluteness."
    author: "repre hendor"
---
The first day of the <a href="http://www.kde.org/kde-4.0-release-event/">KDE 4.0 Release Event in Mountain View, California</a>, got off to a great start on Thursday, with attendees fuelled by a hearty breakfast provided by Google. Then, the <a href="http://en.wikipedia.org/wiki/Unconference">"un-conference"</a> was ready to get underway, and within minutes the first topics were added to the whiteboards. Meanwhile, the room filled with people from across North America and worldwide, all with an interest in KDE. Read on for more details.



<!--break-->
<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/breakfast.jpg"><img src="http://static.kdenews.org/dannya/breakfast_small.jpg" alt="Google provided us with an interesting selection of food, considering we hackers are used to beer and pizza!" title="Google provided us with an interesting selection of food, considering we hackers are used to beer and pizza!"></a></div></p>

<p>One of the first topics on the whiteboard covered the state of KDE 4 on BSD. According to Adriaan de Groot and David Johnson, they discussed community building, automated builds and other testing, and the KDE-on-FreeBSD website.</p>

<p>Other topics on the whiteboard included a discussion about the Git version control system and KDE, a Marketing meeting, a Distributions gathering, and a Plasma conversation. Git is relatively new for many KDE developers, and so their meeting started with a few technical questions and a short "what's cool" overview. The talk quickly moved on to the "big picture". According to Thiago Macieira, the future plan is to attempt to migrate one application entirely to Git, and then evaluate further options. There are some issues regarding usability for less-technical contributors, but luckily the Git developer community is interested in our endeavours and is working with us.</p>

<p>The marketing meeting had many attendants, and it proved hard to focus the discussion on the "big picture". Some of the issues KDE has in the area of marketing are widely-known, but a lack of time and hands can make it difficult to significantly improve the situation. Still, improvements to the KDE web presence were discussed, as well as the topic of marketing in the KDE 4 era, as well as the more general concerns of marketing within the unique environment of an open community.</p>

<p>Another productive meeting was the Distributions discussion. Participants discussed sharing the workload on packaging, configuring and improving KDE, and what they needed for KDE 4.0. They came to a few agreements (for example, about improving the theming system in KDE), integrating PolicyKit in System Settings, reviving the KIOSK tool, and improving the KConfig backend system.</p>

<div style="text-align: center"><a href="http://static.kdenews.org/dannya/silent-hacking.jpg"><img src="http://static.kdenews.org/dannya/silent-hacking_small.jpg" alt="KDE contributors in their most natural state (behind a laptop!)" title="KDE contributors in their most natural state (behind a laptop!)"></a></div>

<p>The Plasma gathering was probably the busiest, drawing a large crowd. Aaron Seigo, lead Plasma  developer, initiated the session with an introduction of Plasma concepts, explanations of fundamental design decisions, and how Plasma enables new interaction possibilities over the KDE 4 cycle. The special needs of small form factor devices, as well as the widely-touted "10 foot interface" of Media Center computers, was touched upon. Aaron also explained that for KDE 4.0, the focus was on recreating the 'traditional' desktop interface with the new framework that Plasma provides. He noted that a proof that the Plasma concept works is that we were able to build a "traditional" desktop interface (a panel, menu, taskbar, clock, and a desktop background with icons on it) in relatively small amount of time, and now also open the door to innovations in the desktop interface.</p>

<p>Questions from the people attending the Plasma discussion revolved around planned features, scripting using high-level languages, integration of online resources, and feature parity with KDE 3.5. Unfortunately, the attendees were only able to scratch the surface of those topics, the one hour that was scheduled for this session turned out to be too short to cover such an important topic. One thing became very clear though: While feature parity has not yet been reached with KDE 4.0, the tools are now in place to quickly get there and to go far beyond what is and was possible. Exciting times lie ahead for both the Plasma team and users of KDE 4.</p>

<p>The KDE hackers that remained rooted to their chair (or were fighting for access at the power sockets!) not only got to know each other (by talking face-to-face, not online!), but were also productive. Some interesting work was Frank Karlitschek's project which allows users to create groups and use those to share files and communicate (think "social networking") on the <a href="http://kde-look.org/">KDE-Look</a> and <a href="http://kde-apps.org/">KDE-Apps</a> community websites. Meanwhile, Justin Karneges wrote a Datagram TLS plugin for QCA which can encrypt UDP trafic, so that users can do voice and video chats in a secure way. Other developments from the open event room were finalisations to the Oxygen mouse cursor theme, and making Amarok compile on Mac OSX again.</p>

<div style="text-align: center"><a href="http://static.kdenews.org/dannya/social-behaviour.jpg"><img src="http://static.kdenews.org/dannya/social-behaviour_small.jpg" alt="KDE contributors are a happy, social bunch!" title="KDE contributors are a happy, social bunch!"></a></div>

<p>After lunch, the hacking and talking continued, while some attendees were lured away from their laptops for the guided tour of the Google campus. After the tour, more talks and meeting sessions until the shuttle bus returned attendees to our hotel for an evening of social interaction and (presumably!), a small amount of sleep.</p>

<p>Overall, the first day of the KDE 4.0 Release Event was productive and a great opportunity to meet - especially for those who are unable to reach the European-centric annual event Akademy. Stay tuned to the Dot and <a href="http://planetkde.org/">Planet KDE</a> for more coverage of this event, including the "official" scheduled happenings of tomorrow!</p>