---
title: "KOffice 2.0 Beta1 Released"
date:    2008-09-25
authors:
  - "iwallin"
slug:    koffice-20-beta1-released
comments:
  - subject: "Isn't it cute? :-)"
    date: 2008-09-25
    body: "The credit for this cuddly giraffe drawing goes to the OpenClipArt artist 'lemmling'. His profile can be found under http://www.openclipart.org/media/people/lemmling . Thank you!"
    author: "Johannes Simon"
  - subject: "Re: Isn't it cute? :-)"
    date: 2008-09-25
    body: "One cannot have enough cuddly giraffes. Very cute indeed ;-)."
    author: "Segedunum"
  - subject: "ot: status of linuxmce?"
    date: 2008-09-25
    body: "hi,  \ni didn't hear anything about linuxmce in the past time. is there already someone working on it to get it into shape? the dev-ml isn't saying anything about it. i haven't found any roadmap or something like that...\nthanks!"
    author: "know"
  - subject: "Re: ot: status of linuxmce?"
    date: 2008-09-25
    body: "this is very off-topic, but you're right...\n\nhuge announcement:\n\nhttp://video.google.com/videoplay?docid=2176025602905109829&ei=_MjbSNGmMZbC2gKE2YShCw&q=linuxmce+kde\n\nbut it seems nothing has happened...is it dead?\n\nbesides, linuxmce's UI is extremely ugly. xbmc is *way* better than linuxmce.\n\nLook at it!!!\n\nhttp://img140.imageshack.us/img140/377/aeonstarkhomecg1.jpg\nhttp://img140.imageshack.us/img140/8530/aeonstarkhomemovedcx3.jpg\n\nBEAUTIFUL!  \n\nhttp://www.aeonproject.com/\n\nbut even the standard skin is way sexier than linuxmce.\n\nlooks like linuxmce was the wrong choice. I suggest going xbmc!\n\nhttp://xbmc.org/blog/2008/09/18/xbmc-atlantis-beta-1-released-now-serving-all-common-platforms/\n\nthanks for reading...  :)"
    author: "fish"
  - subject: "Re: ot: status of linuxmce?"
    date: 2008-09-26
    body: "I am one of the developers of LinuxMCE.\n\nYes, we agree that the Basic skin currently in use on our Orbiter is not very pretty, we do have the capability for looking a lot better, we just need interested artists to work with us for making a complete replacement for Basic, for not only one target, but 7 different device targets.\n\nWe are far superior to XBMC and other \"Media Center\" solutions, precisely because we are NOT a media center, but a whole house smart platform, and this whole house philosophy extends to each and every display surface in the house, not only TVs and monitors, but:\n\n* Tablets\n* PDAs\n* Cell Phones\n* Touch Screens on IP Phones\n\nand more.\n\nCan XBMC do this? no. not really.\n\nSo instead of people trying to scream at us for it being ugly, why don't interested parties come to LinuxMCE, watch the HADesigner screencasts, and help us make a new look?\n\nThat would be fantastic.\n\n-Thom"
    author: "Thom Cherryhomes"
  - subject: "Re: ot: status of linuxmce?"
    date: 2008-09-26
    body: "Hey!\n\nI'm atm trying to get a media center pc with linuxmce up and running for my brother.. but well.. he HATES that interface and that's the reason he's looking for something else... it feels REALLY slow and looks just plain ugly... well.. we seem to agree on that point.. also it's sometimes badly structured and challenging to use (you get tons of fullscreen messages during install that new hardware was plugged in .. good idea but it should be grouped/...)\n\nNo I'm no artist .. I'm just trying to evaluate the problem... as far as I've heard the HADesigner is pretty powerful but it's horrible to work with it .. doesn't make it really attractive for designers who are (at least the designers I know) always a bit shy about software which looks really complex...\n\nSo.. what has happend to the plans about UI3 .. there were disussions about it long time ago but it seemed like there was no decision (flash, ...) and so nothing happend.. or am I wrong here?\n\nThis post shouldn't be offending .. I'm just trying to find out how the current state of development is\n\nBest regards,\nBernhard"
    author: "Bernhard"
  - subject: "Re: ot: status of linuxmce?"
    date: 2008-09-26
    body: "first of all: thanks for notice our questions, Thom. \nimho the design is another thing; i just wanted to know whether anyone has picked linuxmce-code up and ported it to qt4/plasma/whatever. once it is in kde, i'm pretty sure that the kde-artists are going to have a look at it ;)\ngreets"
    author: "know"
  - subject: "Re: ot: status of linuxmce?"
    date: 2008-09-26
    body: "Actually, I can control XBMC from my computer, phone, tablet, etc. because of python extensions.  I don't see why I would want to however."
    author: "T. J. Brumfield"
  - subject: "Congrats!"
    date: 2008-09-25
    body: "Congrats to all the KOffice devs!  You do great work."
    author: "mactalla"
  - subject: "Congrats and thanks!"
    date: 2008-09-25
    body: "Thank you to the Devs for their commitment to KOffice and all the work that went into this beta release!\n\nKeep up the good work, it is really appreciated!\n"
    author: "Axel"
  - subject: "Typesetting"
    date: 2008-09-25
    body: "Err, I really hope the fonts in KWord will look different from that screenshot...\nWhat's wrong with the letterspacing?\n\nJust look at the line next to the blue man that starts with \"Ut wisi enim...\"\nYou can hardly tell where the words start!\n"
    author: "Typesetter"
  - subject: "Re: Typesetting"
    date: 2008-09-25
    body: "This looks like a fuck-up caused by built-in window$ font rendering routines. I know Qt strives for native look&feel, but... was it necessary to do this with the document view? Isn't it sufficient to use native widgets for the interface, and render the document with some other font rendering engine that doesn't suck as much? "
    author: "slacker"
  - subject: "Re: Typesetting"
    date: 2008-09-25
    body: "If you have antialiased fonts activated, it will indeed look just like you expect it to. This screenshot was taken on a windows machine that has font hinting/antialiasing disabled. I agree, it was not a very good choice."
    author: "Johannes Simon"
  - subject: "Re: Typesetting"
    date: 2008-09-25
    body: "Fonts without antialiasing are much better for my eyes.\n\nExcept only Asus EEE PC witn subpixel rendering."
    author: "anon"
  - subject: "Re: Typesetting"
    date: 2008-09-25
    body: "Again, letterspacing problems are caused by trying to show on your computer screen what will get out of your printer. Your screen is probably around 100 dpi which means that fonts need to get hinted. Hinting means that the width of each glyph will be adjusted to the screen pixels, so it'll be different than the exact glyph width. And that means that at certain points the error becomes one pixel, which means the placement of the next glyph has to be adjusted to not let the error become bigger and bigger, and that results in either big or too narrow gaps between two letters. Your printer doesn't have that issue since it's dpi is much greater.\n\nThe other option would be to turn off all hinting, and then you get a lot of fuzziness, but no spacing problems.\n"
    author: "Ben"
  - subject: "Re: Typesetting"
    date: 2008-09-26
    body: "No, this is simply because KOffice strives to show real-sizes. So 100% means an amount of pixels that is equal to a real world size. I.e. an A4 page in KWord is really A4 size on your screen.  The slight zoom (0.981 on my laptop) causes rounding errors in Qt to be very visible.\n\nWill work on fixing."
    author: "Thomas Zander"
  - subject: "Re: Typesetting"
    date: 2008-09-26
    body: "A good start would be to read http://www.antigrain.com/research/font_rasterization/index.html\nThanks for your work!"
    author: "christoph"
  - subject: "Re: Typesetting"
    date: 2008-09-26
    body: "Hmm, I'm quite sure that what I was saying is the real cause. If it shows nice at 100% on your screen I think that's just luck. Move to another screen with a different dpi and it should look worse then (since measured in pixels 100% on your screen would be 98% on another for example)...\n"
    author: "Ben"
  - subject: "Re: Typesetting"
    date: 2008-09-27
    body: "Trust me, I know what I'm talking about when it comes to Qt and fonts :)\n\nps. never said it shows correctly on my screen."
    author: "Thomas Zander"
  - subject: "Re: Typesetting"
    date: 2008-09-27
    body: "OK, I read your reply again, and looks like I misread it. But now I see you've basically said the same I did in my first reply, only you didn't go into details how it \"exactly\" happens... :-)\n\nIf what I said really was wrong in some way then please tell me what exactly is wrong.\n"
    author: "Ben"
  - subject: "Re: Typesetting"
    date: 2008-09-27
    body: "The screenshot: \"koffice2-beta-windows.png\" is certainly not the way that I want WYSIWYG to be displayed.  The glyphs appear to be hinted since they are aligned with the screen pixels.  I do not want WYSIWYG to use hinted glyphs.\n\nI trust that you are knowledgeable.  However, the issue exists that: 1) Hinted at screen resolution & 2}) WYSIWYG are mutually exclusive.\n\nHinted at screen resolution normally makes the lines longer.  So, it you are going to use hinted glyphs with the same metrics as WYSIWYG, you are going to have spacing issues like the screenshot.  Whatever the cause, there needs to be a way to fix this problem.\n\n"
    author: "JRT"
  - subject: "Re: Typesetting"
    date: 2008-09-26
    body: "Funny as far as I remember, there have been fonts issue in *every* promotion screencapture of KOffice..\n\nThat's too bad: there have been lots of efforts spent on it, yet they're (to some extent) wasted by the usual font rendering ugliness.. "
    author: "renoX"
  - subject: "Re: Typesetting"
    date: 2008-09-29
    body: "i have to agree: if i had to choose between almost unreadable fonts (try to identify all the whitespaces in the screenshot) or a non-exact size, i'd choose the non-exact size, especially if that's what other word processors do too.\n\nbtw, just out of curiosity: how come this problem is so hard to solve (eg not by just using a greater precision in the calculations) ? It seems to me that moving some chars 1 px to the left / right would make the text readable, so it is not yet the \"half a pixel\" problem here.. I'm not implying here that i think it is a simple problem (i know problems sometimes are much harder than they look at first sight), i'm just a bit curious ... Also i guess scribus avoids these problems because they are using a whole different mechanism of font rendering ?"
    author: "anonymous coward"
  - subject: "ubuntu packages?"
    date: 2008-09-25
    body: "Has someone built them? (for ubuntu 8.04.1 with kde 4.1.x/3.5.10)\n\nI'd be interested to try this, but I wouldn't go so far as to build it myself...\n\n/Simon"
    author: "Simon Oosthoek"
  - subject: "Re: ubuntu packages?"
    date: 2008-09-25
    body: "Yes, see the announcement on kubuntu.org:\nhttp://www.kubuntu.org/news/koffice-2-beta"
    author: "Johannes Simon"
  - subject: "Re: ubuntu packages?"
    date: 2008-09-25
    body: "That's for Intrepid Ibex, doesn't work for me in Hardy. :("
    author: "Alex Medina"
  - subject: "Re: ubuntu packages?"
    date: 2008-09-25
    body: "Sorry, i'll reply to myself.\n\nFrom Kubuntu's site: \n\nPackages for Kubuntu Hardy are being worked on, but could still take some time.\n\nSorry."
    author: "Alex Medina"
  - subject: "how can I download the koffice-1.9.98.tar.bz2 ?"
    date: 2008-09-25
    body: "Does anybody know a good link to download koffice-1.9.98.tar.bz2 ?"
    author: "Zayed"
  - subject: "Re: how can I download the koffice-1.9.98.tar.bz2 ?"
    date: 2008-09-25
    body: "You can get the source tarball here:\nhttp://download.kde.org/download.php?url=unstable/koffice-1.9.98.0/src/"
    author: "Johannes Simon"
  - subject: "whoa for krita"
    date: 2008-09-26
    body: "My favorite part of koffice is Krita it an application that looks and feels really useful. I have not use it alot, but compare to krita 1.X it is capable of so much more. A goof job by all the Krita developers"
    author: "tikal26"
  - subject: "the best release announcement ever"
    date: 2008-09-26
    body: "thanks really for mentioning the obvious, koffice works for linux either you use gnome or ( insert you favorite GDE)\n\nmmmm, if only you have used more original names, except krita, honestly how we pronounce koffice, kword ?\n\nDander, kplato rocks ( grrrr, i hope someday we will get cost account assigned to resources )\n\nthanks \n\n "
    author: "mimoune djouallah"
  - subject: "Re: the best release announcement ever"
    date: 2008-09-26
    body: "\"honestly how we pronounce koffice, kword ?\"\n\nKay-office and kay-word.  Sort of like eye-life and eye-photo.  Isn't this complaint played out yet?"
    author: "MamiyaOtaru"
  - subject: "Naming"
    date: 2008-09-26
    body: "Pronunciation is that, but I was always wondering why the word processor component had a name so strikingly similar to the most used word processor from a very large company. The similarities between the two programs are dwindling - a new name for KWord would be brilliant. KType? KText? KProcessor?"
    author: "Morten Juhl-Johansen Z\u00f6lde-Fej\u00e9r"
  - subject: "Re: Naming"
    date: 2008-09-26
    body: "\"word processor\" => \"KWord\". Logical. Send complaints to that other company for using a generic name as a trademark."
    author: "zonk"
  - subject: "Re: the best release announcement ever"
    date: 2008-09-26
    body: "Note the capitalization, they help with pronunciation.\nIts \"Krita\" and \"Kivio\"\nvs.\n\"KWord\", \"KPresenter\"\n\nThe first 2 are just single words, the second two are basically a normal word with a K plastered in front.  We decided on those names many many years ago, no way to change them now :)\n\n"
    author: "Thomas Zander"
  - subject: "Re: the best release announcement ever"
    date: 2008-09-26
    body: "The problem with Knames (or iNames, Gnames etc.) is quite simple really. Suppose that you have a menu that lists your apps. If they all start with a K, it means that you need to look at the SECOND letter and beyond to determine which app is which. \n\nTake Krunner for example. I use it to launch my apps. With Knames, I need to type more letters for it to determine what app I'm trying to launch, since lots and lots of apps start with a \"K\".\n\nWith more varied naming, the names would be more spread out to different letters, making it easier to select them from a list or launch from Krunner."
    author: "Janne"
  - subject: "Re: the best release announcement ever"
    date: 2008-09-26
    body: "> Take Krunner for example. I use it to launch my apps. With Knames, I need to type more letters for it to determine what app I'm trying to launch, since lots and lots of apps start with a \"K\".\n\nThat's a bad example. KRunner will also match \"onq\" to \"Konqueror\", or \"document viewer\" to \"Okular\". So, it will be sufficient to type \"pres\" if you want to launch \"KPresenter\"."
    author: "Stefan Majewsky"
  - subject: "Re: the best release announcement ever"
    date: 2008-09-26
    body: "Try to simply skip typing the K the next time. I do share your sentiment about the names in menu's, although my default menu doesn't even have the names in there anymore. It is also a bit annoying if you need to edit configuration files: every file there starts with a k..."
    author: "Andr\u00e9"
  - subject: "Re: the best release announcement ever"
    date: 2008-09-29
    body: "> thanks really for mentioning the obvious, koffice works for linux either you use gnome or ( insert you favorite GDE)\nI think the author is may be talking about QGtkStyle (http://labs.trolltech.com/page/Projects/Styles/GtkStyle. KOffice runs w/o standing out like a sore thumb natively on GNOME.\n\nYes, I know I've brought this up before.\n"
    author: "Michael \"QGtkStyle\" Howell"
  - subject: "table and printing support"
    date: 2008-09-26
    body: "Two things that kept me away from kword were (1) bad table support; I have never got it right in kword and (2) ugly print-out; a document good looking on screen would be ugly when printed.  Have these things improved in kword ?\n\nexcept for these two annoyances I should say the kword and koffice in general are really great.  \n\nasok"
    author: "asok"
  - subject: "Re: table and printing support"
    date: 2008-09-26
    body: "Print outs have gotten excellent. This is the main reason I picked up my participation again for KOffice2. KOffice makes PDF creation a first-row-citizen and the PDFs created at least in KWord look great!\n\nAny text problems you see on screen are not present in the PDF.\n\nThe tables situation has not improved, we have recognized the problem though and are working on a good way to do tables.  I have great ideas on how to do this and work much better using the text layout itself (meaning it will work in all koffice apps).\nUnfortunately that will not make it into 2.0 so you will see that tables have gotten slightly worse. I personally think that unless we have something that actually works its not worth hacking together."
    author: "Thomas Zander"
  - subject: "Daily SVN builds"
    date: 2008-09-26
    body: "There are ~50Mb binary ArchLinux packages, built from KDE SVN trunk every day, for Koffice available below. ArchLinux binary packages are basically tarballs that can be extracted from / (root) on any linux distro, but, some library dependencies could be different. Could be useful for some folks to try Koffice out.\n\nhttp://pkg.eth-os.org/kde-svn/i686/\nhttp://pkg.eth-os.org/kde-svn/x86_64/\n"
    author: "markc"
  - subject: "Will Koffice 2.0 support ODF 1.2?"
    date: 2008-09-26
    body: "or will that have to wait for the 2.1 release?\n\ni ask because in theory the ODF 1.2 spec is due out around now........"
    author: "Dimble"
  - subject: "Re: Will Koffice 2.0 support ODF 1.2?"
    date: 2008-09-26
    body: "Yes and no.  It will support some parts of 1.2, although I can't really define exactly which parts.  But no, it will not support the full 1.2 spec, which is actually quite big.\n\nBut then on the other hand, no office suite supports all of ODF 1.2."
    author: "Inge Wallin"
  - subject: "Re: Will Koffice 2.0 support ODF 1.2?"
    date: 2008-09-26
    body: "many thanks for the update."
    author: "Dimble"
  - subject: "Re: Will Koffice 2.0 support ODF 1.2?"
    date: 2008-09-27
    body: "But that is not the reason to not try to implent all 1.2 features as fast as possible ;-)\n\nIsn't it the reason why we have the standards so we can be sure it is supported? ;-))"
    author: "Fri13"
  - subject: "Re: Will Koffice 2.0 support ODF 1.2?"
    date: 2008-09-27
    body: "We surely do want to support as much of ODF as possible, but at the same time you have to keep in mind that we have only limited resources. Once we have the time and a reasonable enough idea of how to implement a certain feature, it will get into KOffice as soon as we're done with it."
    author: "Johannes Simon"
  - subject: "kword"
    date: 2008-09-27
    body: "The first thing I have missed in kword is a simple option to change the font. So, I'd suggest to enable the toolbar option by default.\n\nIMHO 80% use only this options:\n- load a file\n- save a file\n- change the font\n- change the font size\n- bold style\nIMHO 15% use also\n- change page layout\n- add a image\n- change the line spacing\n- add a list\n\nIf I can do this tasks very easy, the program is well for me! :)"
    author: "Tom"
  - subject: "Re: kword"
    date: 2008-09-27
    body: "You get a context menu when right-clicking the text. Select the entry \"Font...\" and there you go :-)\n\nI personally use other buttons like 'Italic' or 'Align Block/Justify' quite often, and usually change the font type only one time per document, so I'm satisfied with how things are now."
    author: "Johannes Simon"
  - subject: "Data Pilot / Pivot Table"
    date: 2008-09-27
    body: "Hey guys: Does anyone knows if KSpread would have Dynamic Tables with Data Base support . Excel have his Pivot Table, and OpenOffice his Data Pilot (IMHO OpenOffice Data Pilot sucks compared to the \"other\", i don't know if OO Pivot Table is too slow because of Java or why.). \n\nBut that functionality isn't available in KSpread, would KSpread have this functionality?"
    author: "Alex Medina"
  - subject: "Re: Data Pilot / Pivot Table"
    date: 2008-09-29
    body: "I suggest filing a bug report with this wish. There it is guaranteed that a developer reads it."
    author: "Sebastian"
  - subject: "Re: Data Pilot / Pivot Table"
    date: 2008-09-30
    body: "That's a horrible suggestion.  File a bug report to get an answer as to if a given feature has been implemented yet?  That's a heck of a way to flood the bug database with queries about features that already are complete."
    author: "Evan \"JabberWokky\" E."
  - subject: "Platform Independence"
    date: 2008-09-27
    body: "There is an issue with platform independence that users of Open Office are probably familiar with since there is an option to deal with it.  The problem is that a document will print longer in Windows than with *NIX (actually, it is printing with the Windows printer driver) -- you will only notice this on long documents and probably won't notice it if you use right justification.  \n\nThe reason for this is IIUC, that Windows print drivers glyphs hinted at the printer resolution while *NIX uses the absolute size of the unhinted glyphs.\n\nDoes KOffice-2 have a way to deal with this issue? "
    author: "JRT"
  - subject: "Dont want to make it bad"
    date: 2008-09-28
    body: "hello, dont understand me wrong, i dont want to make the software bad.\n\nbut i find it unusable after playing around like 10 minutes. Did you somehow made contact with the Usability Team ? they can provide you with Tipps..."
    author: "asdf"
  - subject: "Re: Dont want to make it bad"
    date: 2008-09-29
    body: "Does it crash or what do you mean?"
    author: "Sebastian"
  - subject: "Re: Dont want to make it bad"
    date: 2008-09-29
    body: "yes it crashes (i will add bugsreports to bugs.kde.org) and help testing, and i cant find the options i need.\n\nthe \"normal\" thing for me is the following:(kword)\n\n- first care about the text, write down all the stuff you need\n- then do proper formatting, or leave the formatting to the hopefully excellent template i chose @ start\n\ni dont get it how it is meant to work with kword (but could be i dont get the work philosophy here)"
    author: "sfdg"
  - subject: "only to (K)ubuntu?"
    date: 2008-09-28
    body: "You will not release it to opensuse too...? \nnot rpm-packets? only src... "
    author: "Jake"
  - subject: "Re: only to (K)ubuntu?"
    date: 2008-09-30
    body: "We don't do any packaging, it's up to the distribution to do the packaging. That said, there are OpenSuSE packages in the build service, as for being mentionned in the release notes, it's up to the distribution to contact us."
    author: "Cyrille Berger"
  - subject: "KWord Saving Shapes"
    date: 2008-09-28
    body: "I've tried the last 2 KOffice alpha versions and now the beta1 version.  In each case, KWord has not saved shapes within the document.  In other words, I type some text, add a shape, save the file and close.  However, when I open the file again, only the text is there and not the shape.  This is a pretty basic feature, and if it doesn't work KWord is pretty much unusable for anything other than typing text.  Does anyone else have this problem?  I'm using it with OpenSUSE.\n\nOther than that, KOffice 2 is looking pretty good."
    author: "cirehawk"
  - subject: "Re: KWord Saving Shapes"
    date: 2008-09-28
    body: "Update:\n\nI tried saving a shape in KSpread and it doesn't save as well.  However, shpes seem to be successfully saving in KPresenter."
    author: "cirehawk"
  - subject: "Re: KWord Saving Shapes"
    date: 2008-09-28
    body: "Most of the code to save the shapes already exists, and the two or three lines in KWord that trigger the saving of shapes are simply uncommented atm, due to lack of time to write the rest of the KWord-specific code. I'm sure you will see it in the next beta :-)"
    author: "Johannes Simon"
  - subject: "Re: KWord Saving Shapes"
    date: 2008-09-29
    body: "Thanks for the feedback.  It's good to know shape save ability will be in the next beta.  Though it does give me a little concern that there won't be much time to thoroughly test it out before an official release.  Be that as it may, keep doing the great work you guys are doing.  I really want KOffice to succeed and try to use it as much as possible, though often times I have to resort to using Open Office.  Looking forward to the next beta."
    author: "cirehawk"
  - subject: "Re: KWord Saving Shapes"
    date: 2008-09-30
    body: "On a side note, such a bug is considered as release critical, meaning that KOffice 2.0 won't be released as final until there is a known data loss at saving (among other things ;) )."
    author: "Cyrille Berger"
---
The KOffice team is proud to announce the first beta of KOffice 2.0. The goal of this release is to gather feedback from both users and developers on the new UI and underlying infrastructure. This will allow us to release a usable 2.0 release, demonstrating our vision for the future of the digital office to a larger audience and attract new contributions both in terms of code and ideas for improvements.  Read on for more information or see the <a href="http://koffice.org/announcements/announce-2.0beta1.php">announcement</a> and download it from the <a href="http://koffice.org/releases/2.0beta1-release.php">release notes</a>.









<!--break-->
<div style="border: thin solid grey; float: right; padding: 1ex; margin: 1ex">
<a href="http://www.koffice.org/announcements/pics/2008-09-25-kpresenterbeta1.png"><img src="http://static.kdenews.org/jr/koffice2-beta-kpresenter.png" width="430" height="341" /></a>
</div>

<p>This is the first beta release of KOffice 2.0, and the first version we encourage users to download and try out. After a very long development cycle KOffice is now in feature freeze. The development team will from now on shift focus from new features to bug fixes until 2.0 is released.</p>

<p>The release team has decided that the following applications are mature enough to be part of 2.0: </p>
<ul>
<li>  KWord,   Word processor</li>
<li>  KSpread,   Spreadsheet calculator</li>
<li>  KPresenter,   Presentation manager</li>
<li>  KPlato,   Project management software</li>
<li>  Karbon,   Vector graphics editor</li>
<li>  Krita,   Raster graphics editor</li>
</ul>

<p>The chart application KChart is available as a shape plugin, which means that charts are available in all the KOffice applications in an integrated manner.</p>

<h3>Highlights of KOffice 2.0</h3>

<p>KOffice 2 will be a much more flexible application suite than KOffice 1 ever was. The integration between the components is much stronger, with the revolutionary Flake Shapes as the central concept. A Flake Shape can be as simple as a square or a circle or as complex as a chart or a music score.</p>

<p>With Flake, any KOffice application can handle any shape. For instance, KWord can embed bitmap graphics, Krita can embed vector graphics and Karbon can embed charts. This flexibility does not only give KOffice unprecedented integration, but also allows new applications to be created very easily. Such applications could target special user groups such as children or certain professions.</p>

<h3>Unified Look and Feel</h3>

<p>All the applications of KOffice have a new GUI layout better suited to today's wider screens. The GUI consists of a workspace and a sidebar where tools can dock. Any tool can be ripped off to create its own window and later be redocked for full flexibility. The users setup preferences are of course saved and reused the next time that KOffice is started.</p>

<div style="border: thin solid grey; float: left; padding: 1ex; margin: 1ex">
<a href="http://www.koffice.org/announcements/pics/2008-09-25-kword-windows-beta1.png"><img src="http://static.kdenews.org/jr/koffice2-beta-windows.png" width="430" height="323" /></a>
</div>

<h3>Platform Independence</h3>

<p>All of KOffice is available on Linux with KDE or GNOME, Windows and Macintosh. Solaris will follow shortly and we expect builds for other Unix versions to become available soon after the final release. Since KOffice builds on Qt and the KDE libraries, all applications integrate well with the respective platforms and will take on the native look and feel.</p>

<h3>Native Support for OpenDocument</h3>

<p>KOffice uses the OpenDocument Format as its native format. This will guarantee interoperation with many other Office packages such as OpenOffice.org and MS Office. The KOffice team has representatives on the OASIS technical committee for ODF and has been a strong participant in the process of shaping ODF since its inception.</p>







