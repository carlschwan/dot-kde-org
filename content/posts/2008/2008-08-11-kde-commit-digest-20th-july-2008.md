---
title: "KDE Commit-Digest for 20th July 2008"
date:    2008-08-11
authors:
  - "dallen"
slug:    kde-commit-digest-20th-july-2008
comments:
  - subject: "OT: Akademy 2008 videos?"
    date: 2008-08-11
    body: "Will there be at some point videos of all the great talks available?\n\n\nBTW: Thanks Danny!"
    author: "Tom"
  - subject: "Re: OT: Akademy 2008 videos?"
    date: 2008-08-11
    body: "Yes, they need some postprocessing now and will be available shortly (as in some days, hopefully)."
    author: "sebas"
  - subject: "Yay"
    date: 2008-08-11
    body: "Heh, for a little while the post showed the 13th July digest description. So I opened it and though \"Wait, didn't I already see the one for July 13?\". I refreshed the page and, sure enough, the new description appeared. :)\n\nAnyway, great job with the catching-up, and thanks for all the hard work, Danny. We appreciate it!"
    author: "LiquidFire"
  - subject: "Re: Yay"
    date: 2008-08-11
    body: "Heh, I realised my sleep-induced error and quickly fixed it :)\n\nDanny\n\np.s. 4 comments already?!?"
    author: "Danny Allen"
  - subject: "My fault"
    date: 2008-08-11
    body: "This digest would have been out 20-30 seconds faster if I hadn't been tampering with Danny's laptop screen.  But I did plug in his power cord, so I guess life balances out after all."
    author: "Wade"
  - subject: "Re: My fault"
    date: 2008-08-11
    body: "Thanks Wade for joining Danny on the commit-digest team as another contributor ;-)\n\nGood job, dot-machine-danny."
    author: "sebas"
  - subject: "KHotkeys redesign"
    date: 2008-08-11
    body: "Any screenshots of that available?"
    author: "bsander"
  - subject: "Re: KHotkeys redesign"
    date: 2008-08-11
    body: "Or even more: Does any frontend implement this or it is just the backend for now? (like having it in mkenuedit and kcontrol(forgot the new name in kde4) or to set in the config file by hand?"
    author: "Iuri Fiedoruk"
  - subject: "WebKit address"
    date: 2008-08-11
    body: "Hi Danny.\nThe official internet address of Webkit is http://webkit.org/, not http://webkit.opendarwin.org/. Just FYI."
    author: "KAMiKAZOW"
  - subject: "Re: WebKit address"
    date: 2008-08-12
    body: "Ah - I guess my link database was filled back when WebKit was less important ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Great!"
    date: 2008-08-11
    body: "Great work danny! You rock :-)\n\nBtw: holy shit.. the dot is alive again :-) 7 new articles since my last visit here.. which was yesterday :D\n\nPS: of course the whole KDE team rocks and not just you danny ^^"
    author: "Bernhard"
  - subject: "NX in KRDC"
    date: 2008-08-11
    body: "That's awesome! I don't mind the FreeNX client, but having support for the NX protocol in a native KDE application just makes me smile ;)"
    author: "ac"
  - subject: "Re: NX in KRDC"
    date: 2008-08-11
    body: "Hurrah!  NX support in KRDC is indeed very very awesome.  KRDC is a nice app already but it's really cool to have this available too.\n\nNow if only I could make FreeNX work as easily as I can get Nomachine's package to work...  Maybe I should try again."
    author: "Mark Williamson"
  - subject: "Darkroom"
    date: 2008-08-11
    body: "shouldn't Darkroom be part of Digikam, instead of being a separate app?"
    author: "janne"
  - subject: "Re: Darkroom"
    date: 2008-08-11
    body: "Until not short ago RAW import for digiKam was done with kipiplugin. Krita rejected kipiplugins for various reasons. Darkroom is planned as standalone/Krita plugin for RAW import.\n\nNote: since one week digiKam has new, vastly improved RAW import plugin."
    author: "m."
  - subject: "Re: Darkroom"
    date: 2008-08-12
    body: "Can it import CHDK type, 10 bit RAW?\n"
    author: "reihal"
  - subject: "Re: Darkroom"
    date: 2008-08-12
    body: "\"Note: since one week digiKam has new, vastly improved RAW import plugin.\"\n\nBut it's different from Darkroom? That's jut my point: why two separate, yet similar systems?"
    author: "Janne"
  - subject: "Re: Darkroom"
    date: 2008-08-12
    body: "And what is that plugin called? Not that it would matter since digicam still is too dangerous in it's operation (overwrites images much too easily) to be let near any valuable pictures..."
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Commit countries"
    date: 2008-08-11
    body: "I've noticed that map of commit countries is somewhat incorrect. It says \"Serbia & Montenegro\" have 2.42% share, but the map says Serbia has 0%, and Montenegro gets it all. You should either split the two countries, or show them both with the same share. I think Ivan Cukic is from Belgrade, so Serbia probably doesn't have 0% :-)"
    author: "Vlada"
  - subject: "Re: Commit countries"
    date: 2008-08-11
    body: "This was posted at least twice already and it was answered here: http://dot.kde.org/1217886214/1217913463/\n\nPlease don't spam the dot!"
    author: "Martin"
  - subject: "I'm impressed"
    date: 2008-08-11
    body: "Not only by Danny's fast track into becoming current with the week, but also by the development in KDE after the 4.1 release. \nOff my hat for Danny and all KDE developers!"
    author: "Iuri Fiedoruk"
  - subject: "DarkRoom"
    date: 2008-08-11
    body: "Now that's a really cool name that also precisely describes what that application is for (it even has a K in it)."
    author: "Eckhart"
  - subject: "Tiling is a must"
    date: 2008-08-13
    body: "Honestly, I've been using KDE3 under ion3 for now just because I wanted to wait and evaluate xmonad (vs. ion3) when I switched to KDE4.  It's one of those \"I need it, but I know 99% of people don't even know about it, so there's no hope of ever seeing official support in KDE4\".\n\nNow there's hope.  Wow.  Thanks, Lucas Murray!!!"
    author: "Evan \"JabberWokky\" E."
---
In <a href="http://commit-digest.org/issues/2008-07-20/">this week's KDE Commit-Digest</a>: The "Web Browser" Plasmoid now uses the Konqueror bookmarks system, and gains support for favicons. Improvements to the D-Bus interface of <a href="http://plasma.kde.org/">Plasma</a>, with continued developments in the Plasmoids-on-Screensaver project. More animations in Plasma, and custom colors support in the "Binary Clock" and "Notes" applets. First implementation of a browserbar, and wheel scrolling support in the "Previewer" applet. A new "Screen Management" Plasmoid for showing connected displays. Start of a set of 40 <a href="http://oxygen-icons.org/">Oxygen</a> weather icons, for use in various Plasma applets and across KDE. Basic implementation of javascript window operations in <a href="http://webkit.org/">WebKit</a>-KDE. More work in DVCS support in KDevplatform (the basis of <a href="http://www.kdevelop.org/">KDevelop</a>4). Initial work on a Kross backend and automatic Qt bindings for Lua. A QtScript module for scripting C# applications. The start of a flashcard mode in <a href="http://edu.kde.org/parley/">Parley</a>. Return of spellchecking support in the <a href="http://kphotoalbum.org/">KPhotoAlbum</a> annotation dialog, and improved spellchecking support in <a href="http://kopete.kde.org/">Kopete</a> chat windows. "SystemC" syntax highlighting in <a href="http://kate-editor.org/">Kate</a>. Start of using <a href="http://pim.kde.org/akonadi/">Akonadi</a> to store contacts in <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>. A new "Albums" context view applet, and a "Script Console" script (using Qt bindings) in <a href="http://amarok.kde.org/">Amarok</a> 2. Playback of DVD ISO's in <a href="http://www.dragonplayer.org/">DragonPlayer</a>. Support for EMule collection files in KMLDonkey. Support for the NX protocol in KRDC. More page transition effects in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. Start of work on an authorization framework in the <a href="http://www.kexi-project.org/">Kexi</a> "Web Forms" project. A PackageKit backend plugin in the Shaman package manager. KDiff3 moves from playground/devtools to extragear/utils. Start of porting KMathTool to KDE 4. The KHotKeys redesign is merged back into kdebase. A new KWin branch for window tiling experimentation. Initial import of DarkRoom, an experimental raw decoder based on top of libkdcraw. <a href="http://commit-digest.org/issues/2008-07-20/">Read the rest of the Digest here</a>.


<!--break-->
