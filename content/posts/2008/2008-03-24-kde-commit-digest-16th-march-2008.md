---
title: "KDE Commit-Digest for 16th March 2008"
date:    2008-03-24
authors:
  - "dallen"
slug:    kde-commit-digest-16th-march-2008
comments:
  - subject: "Sleep, anyway?"
    date: 2008-03-24
    body: "Good grief, do any of you sleep? ;) `you', in this case, includes Danny. Thanks for the Digest, and thanks to all the developers for the ridiculously inhuman amount of work you put in!"
    author: "Antonio"
  - subject: "Re: Sleep, anyway?"
    date: 2008-03-26
    body: "of course danny sleeps, why do you think commits come out a week after they have been released.  Well, in this case, over a week.  Pity they don't hire a monkey, which would be a lot more efficient than Danny.  \n\nIt's time dot gets rid of Danny and gets someone decent who can do this job.  It's getting worse and worse"
    author: "anon"
  - subject: "Re: Sleep, anyway?"
    date: 2008-03-26
    body: "Writing articles for The Dot isn't a paid job. It is all done by people who use their free time to contribute to KDE. If you don't like that situation, you could hire a writer who creates articles and gets paid for it."
    author: "Whatever Noticed"
  - subject: "Re: Sleep, anyway?"
    date: 2008-03-26
    body: "I guess this is supposed to be funny? Sorry, it's not. It sounds a lot like a frustrated Gnome user who is annoyed his community is unable to get such a large stream of quality development news to it's users. Not that there would be much to tell, of course..."
    author: "jos poortvliet"
  - subject: "New to-do view"
    date: 2008-03-24
    body: "Would someone be willing to post a screenshot of the new to-do view in KOrganizer?"
    author: "The Troy"
  - subject: "Re: New to-do view"
    date: 2008-03-24
    body: "agreed\n\nhopefully someone finds the time to put up a few images of it :)"
    author: "she"
  - subject: "Re: New to-do view"
    date: 2008-03-24
    body: "I'm hoping for a To Do plasma applet. I'm horribly easily distracted (look I'm on the dot right now) and it would be great to have something easily accessible and reminding me to \"DO THIS NOW\" (...other than my girlfriend).\n\nThanks."
    author: "Martin Fitzpatrick"
  - subject: "Re: New to-do view"
    date: 2008-03-24
    body: "Hi\n\nThis is an attached screenshot in bugzilla :\nhttp://bugs.kde.org/attachment.cgi?id=23846&action=view (2008-03-10 11:49)"
    author: "DanaKil"
  - subject: "Re: New to-do view"
    date: 2008-03-24
    body: "What did they re-write about it? It looks exactly like the to-do in KDE3 KOrganizer."
    author: "The Troy"
  - subject: "Re: New to-do view"
    date: 2008-03-24
    body: "I think, first of all it is rewritten to use Qt4"
    author: "b"
  - subject: "Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "I follow the new builds from time to time. I not that some of the plasma apps will kill the window manager, well the screen goes black, and all I can do is a ctrl+alt+delete.  \n\nI know this is not production code, but was wondering what the design philosophy of Plasma is. \n\nWill Plasma be engineered to handle bad plasmoids and not seg fault or go into never land.  Or does it expect all plasmoids to be good citizens and not crash.\n\n\nKDE really looks great. by the way\n\n"
    author: "Mark"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Some plasmoids do have issues as the underlying api is changing a bit.  However, when this happens (wont happen once the api has become more stable) you can hit alt-F2 and type plasma to get it back <-- (the reason krunner is separate from plasma)."
    author: "JP"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Yes. But \"Will Plasma be engineered to handle bad plasmoids and not seg fault or go into never land. Or does it expect all plasmoids to be good citizens and not crash.\""
    author: "illogic-al"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "If a Plasmoid written in native code crashes, it will take down the rest of Plasma (but not the \"window manager\", as the OP claims - I have no idea what's happening there) - this is unavoidable, and the statement \"wont happen once the api has become more stable\" is entirely false.  Which is why scripted Plasmoids are so desirable: they are easy to write, easy to deploy, download and install, sandboxable if the scripting language allows it (Javascript/ QtScript are favoured here, as e.g. Python and Ruby do not have mature sandbox implementations) so that you can in theory fine-tune what resources each one can and cannot access/ modify, and you can guard against having them crash."
    author: "Anon"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Have the plasma developers thought of the security implications of running native code?  It wouldn't be good to run a plasma applet and have it rootkit the system!"
    author: "security guy"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "For a \"security guy\", you sure don't have a good understanding of common terms. For reference, most apps that you are using right now - including your web browser, e-mail client, etc - are \"native code\". "
    author: "Anon"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Yes, but - unlike Plasmoids - the normal method of obtaining that code is not to download them from a random 3rd party on the internet, since they come with your distro.  IMO, the different security concerns for Plasmoids arise from the fact that there's a lower \"barrier to entry\" to getting Plasmoids onto a users' desktop."
    author: "Mark Williamson"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Probably why you're not supposed to download and run apps you don't trust."
    author: "SMB"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Native code apps will always have to be either compiled from source (a power-user task), obtained from distro packages, etc - in other words, Plasma does *not* lower the barrier to entry of getting native code onto the users desktop.  It is precisely as much of an increased security risk as Kicker applets were i.e. \"barely at all\".\n\nNon-native code - which will hopefully form the bulk of 3rd party plasmoids - can, as mentioned elsewhere in this thread, be tightly sandboxed so that it can do no harm."
    author: "Anon"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "> in other words, Plasma does *not* lower the barrier to entry of getting native code onto the users desktop\n\nIt does. Security is not related here but things like a) the time needed to look at how it should be done and b) the time needed to get it working. Plasma helps with a) by providing good, clean and small interfaces Plasmoids need to implement and helps with b) by providing a fast way to test your code and cause of a) it's also not needed to write tons of code to get just something working.\n\nAll in all, it does help to lower the barrier to entry. If we look at scripting code aka Plasoids written or extended with scripts, then there is also no connection between security and barrier since it's not the main goal of most scripting languages to provide a secure sandbox but to get a solution out faster (aka without learning pointer-logic, without compiling, without being such static limited, etc.) an that's exactly what they (may) do in plasma as well :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "> in other words, Plasma does *not* lower the barrier to entry of getting native code onto the users desktop\n\nIt does. Security is not related here but things like a) the time needed to look at how it should be done and b) the time needed to get it working. Plasma helps with a) by providing good, clean and small interfaces Plasmoids can implement and helps with b) by providing a fast way to test your code and get it working and cause of a) the task shouldn't be that complex => lesser code needed to get the job done.\n\nAll in all, it does help to lower the barrier to entry. If we look at scripting code aka Plasmoids written or extended with scripts, then there is also no connection between security and entry-barrier since it's not the main goal of most scripting languages to provide a secure sandbox but to get a solution out faster (aka without learning pointer-logic, without compiling, without being such static limited, etc.) an that's exactly what they (may) do in plasma as well. Security, as in Plasmoids coming from untrusted sources, is only related for the deployment.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Yes, it has been considered. No plasma apps can not rootkit your system."
    author: "Thomas Zander"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Double negative error."
    author: "Anon"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "... or a missingcomma. :-)"
    author: "sebas"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "Are you *sure* it's an error?"
    author: "Anon"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-26
    body: "I suspect it's not..."
    author: "jos poortvliet"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "re security and sandbox;\n\nRuby comes with security-levels, Python can be extended using the RestrictedPython module (like Kross did in KDE3) or the Zope3 security-framework. But I wouldn't trust all of them to be _the_ solution. All they can do is to rise the time needed till some evil actions are possible.\nbtw, QtScript does not provide any kind of sandbox-model while KjsEmbed does remove the security logic from Kjs by extending it with potential insecure functionality. So, none of them is _the_ solution if it comes to security either. I guess Java or Kjs/WebCoreScript direct are the best / most secure ways to go here but even then I wouldn't trust them ;)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "Actually QtScript provides the exact same sandbox as a browser. It's simply a matter of which objects you choose to expose. Ruby's security levels don't seem particularly well thought out when I looked at them either."
    author: "Richard Moore"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "I was refering to http://labs.trolltech.com/blogs/2008/03/10/bind-aid/\n\nAnon: \"Is there any provision for sandboxing\"\nKent: \"Anon: Nothing yet. However, you&#8217;re free to remove e.g. the QFile and QDir constructors from the script engine after importing the bindings. We&#8217;re playing with offering more flexible/fine-grained ways of configuring the bindings though, will keep this in mind.\"\n\nSandboxing doesn't mean to just don't provide functionality that may insecure (else brainf*ck would be the most secure language followed by xslt :) but to be able to control and limit it. So, to e.g. allow to use QFile but to restrict it to reading from /home/friendly_user/for_my_gov_agency and writting to /home/president_bush/next_wars ;)\n\nI guess the Java policy-framework is a very good example imho followed by the Zope3 security-framework.\n\n> Ruby's security levels don't seem particularly well thought out when I looked at them either.\n\nCause of the only both vulnerabilities Ruby had in 2005 (CVE-2005-2337 and CVE-2005-1992) while e.g. Safari had 20 times as much?\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: ">> Ruby's security levels don't seem particularly well thought out when I\n>> looked at them either.\n\n> Cause of the only both vulnerabilities Ruby had in 2005 (CVE-2005-2337\n> and CVE-2005-1992) while e.g. Safari had 20 times as much?\n\nThe problem is one of design rather than implementation. To give an example, at a SAFE level of 2 you are prevented from doing File.chmod, but you're free to do Kernel.system(\"chmod +x blah\").\n"
    author: "Richard Moore"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "> To give an example, at a SAFE level of 2 you are prevented from doing File.chmod, but you're free to do Kernel.system(\"chmod +x blah\").\n\ny, cause T2 is about \"Ruby prohibits the loading of program files from globally writable locations.\" and has nothing to do with chmod+userfiles. Use at least T3 or even better T4.\n\nbut I agree that those \"safe levels\" are not really what someone may understand under sandbox too since it would still be needed to provide secure wrappers for things like readfile, chmod, etc. if they should be limited to defined locations :-( Same was the case with Zope2 RestrictedPython and since I still believe it's better to provide no security then false security, I just did removed it in KDE4 - the Zope3 solution really looks very good but is to huge to be adopted :-(\n\nGuess my initial point of \"none of them is _the_ solution if it comes to security\" is still valid even if I wish that wouldn't be the case :-(\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "and before I forget it, there is the nice document \"Sandboxing of Dynamic Code\" around which does provide some good (but in the case of Python outdated) details related to that topic; http://rune.hammersland.net/tekst/sandbox.pdf"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "Another thing with the ruby safe levels is that they seem very oriented to web apps operating on untrusted data rather than apps embedding the interpreter operating on untrusted code. eg. only at a safe level of 4 is the script prevented from terminating your application.\n\n> Guess my initial point of \"none of them is _the_ solution if it comes to\n> security\" is still valid even if I wish that wouldn't be the case :-(\n\nI think that's a pretty fair assessment. It's not the answer I'd like either though. :-(\n\nThe java security manager approach is the only real way I've seen to get truly fine grained control, and that is extremely complex and requires hooks in all your libraries. Even then it has been bypassed on a number of occaisions (possibly because it is too complex to test effectively).\n"
    author: "Richard Moore"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "> eg. only at a safe level of 4 is the script prevented from terminating your application.\n\nDoes you have a source for this? I ask cause, well, we are using Ruby and I never did note such behavior. What for sure is a bit annoying and probably related is that it's needed to wrap each C-functioncall into a secure-fallback function to catch exceptions explicit. If that isn't done and if the call throws an exception, the application will crash (same with T4 btw). The reason for that design was to maximize the performance by being able to select what C-functions may throw an exception (that needs to be catched) and which one are not. Guess that's very inline with the overall design-goal of Ruby to provide the fastest scripting-environment out there on earth.\n\n> The java security manager approach is the only real way I've seen to get truly fine grained control, and that is extremely complex and requires hooks in all your libraries.\n\ny, true. That's really one of the best solutions I saw so far but also comes with so much downsides. Very much like something like SE-Linux which if used wrong may provide more trouble then solving any.\n\n> Even then it has been bypassed on a number of occaisions (possibly because it is too complex to test effectively).\n\nand judging from the 1.6 showstopper-bug I run into (known since more then 2 years btw) I wouldn't wonder if there are no automated tests for such things at all :-/\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "> Does you have a source for this? \n\nThis is the best doc I've found on ruby safe levels (especially the table at the bottom):\nhttp://phrogz.net/ProgrammingRuby/taint.html\n"
    author: "Richard Moore"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "> http://phrogz.net/ProgrammingRuby/taint.html\n\nThanks for that link and now I am impressed since at $SAFE>=2 it really says chmod should be prevented. Uh, I've to add. That's really new to me and its even more surprising it's written down in a book while being just not true/valid. Guess that was the last destroyed difference between books and TV :)\nRe $SAFE>=4 and \"Can't invoke exit, exit!, or abort.\" Neither exit or abort works if embedded, but exit! does and that even with SAFE==4. Hmpf, http://www.math.hokudai.ac.jp/~gotoken/ruby/man/function.html#exit_bang says unlike abort and exit the exit! function ignores any handlers. Fine :-( Anyway, added to my todo and will be fixed asap, thanks for that hint!\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-26
    body: "Yes, I checked the ruby source code too and the code matches this reference.\n\n"
    author: "Richard Moore"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "It would be good if KDE restarted Plasma if it crashes, like KDE 3 does with Kicker in some cases. Otherwise we reach the quality of Windows 9x where a poorly written component could crash the whole system - average users have no idea about \"Alt+F2 plasma\"."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-24
    body: "iirc it does auto-restart. Re win95; the whole OS could crash by any app and not only the desktop by it's plugins. So, probably it's more fair to compare with extensions that crash the WinExplorer (which is afaik responsible for the desktop), with a winamp-plugin that crashes whole of winamp or with a driver that oop's'es whole of linux.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "Technically you are probably right, but from a non-technical user's point of view I presume that a Plasma crash equates to a complete system crash as his main (and probably solely) means of communication with the system are not available any more. If I had to hazzard a guess people who don't know how to launch plasma via krunner or are not familiar with virtual terminals will probably hit the reset button at this point."
    author: "Erunno"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "> Technically you are probably right\n\nis that different from practical? Sorry, failed to resist :)\n\n>  that a Plasma crash equates to a complete system crash as his main (and probably solely) means of communication with the system are not available any more\n\nThat's why it auto-restarts. Though before we move again to the beginning of the circle, I may like to point to 2 examples;\n\n1. SuperKaramba\nIt's a plasmoid that can be run either _in_ process just like most (all?) plasmoids today OR _out_ of process. Right, both is possible and I can imagine that bigger and more complex plasmoids (so, e.g. whole applications rather then a clock or a status-display) can go the same way. Technical (and practical) it just does not make any sense to move e.g. a clock into an own process. See here also the last note at the bottom of http://ktown.kde.org/~seli/memory/desktop_benchmark.html\n\n2. Trayicons\nThe trayion plasmoid does display those handy icons which are provided by an app and then embedded into the trayicon plasmoid. So, if a running app crashes it doesn't crash the trayicon-plasmoid and with it plasma.\n\nSo, you see. It is technical (and practical *g*) possible already. That the OP does now ask if the crashes he did run into are cause of the \"design philosophy of Plasma\" is even somewhat funny from the pov of someone who's still running KDE3 on his productive systems like suggested by the release-notes, blogs or even the mainstream-press. The problem is, that an answer like \"no, it's cause KDE 4.0.2 isn't as rock stable as e.g. KDE3 kicker+applet are yet, but we are moving rather fast into that direction and once we are there your question will be solved\" really does sound so reused, that it's hard to repeat that. Probably an answer like \"no, crashes are not a design philosophy but an attitude to life\" or \"if you don't like that crash we can add a GUI-option to disable it\" would offer some more variety of fun?! :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "Plasma already gets restarted when it crashes. There is some simple logic in there so it doesn't crash in an infinite loop though. It won't restart itself after a second crash."
    author: "sebas"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "If it behaves this way, it should save a \"last known working configuration\". My experience is that it didn't restart after a crash; I don't know why.\n"
    author: "christoph"
  - subject: "Re: Are plasmoids expected to be good citizens"
    date: 2008-03-25
    body: "it doesn't restart if a crash happens within 10s of restarting. but then we install the crash handler again after 10s of running. this is usually enough to stop infinite recursions of doom."
    author: "Aaron Seigo"
  - subject: "Nice work"
    date: 2008-03-24
    body: "The amount of work going into Akonadi & friends was really impressive. The API review will produce great long-term results, I'm sure, even if such cleanups require quite a lot of tedious work. A very exciting read, thanks to all devs.\n"
    author: "T"
  - subject: "Well done KHTML developers"
    date: 2008-03-24
    body: "It seem we will get fast and rich features web browser in kde 4.1. Does any one have the score of KHTML (development version) in Acid test version 3 ? what about the javascript benchmark in compare with firefox 3 beta 4?\n"
    author: "Zayed"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "Well, if the KHTML developers finally got over their giant ego and already ported WebKit into the KHTML framework, then it's 95/100 points: http://webkit.org/blog/167/webkit-gets-an-a-on-acid3/"
    author: "djvnklhjefjmwfn"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "While I too would like to see webkit used by kde and one common web platform, the khtml developers deserve better treatment. They seem to be helping out all over: http://webkit.org/blog/158/the-acid-3-test/"
    author: "xian"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "Yea, took the WebKit crew quite some time to finally include the CSS3 selectors patches by the KHTML developers (they were available for ages, but only with this ACID3 test there was interest from WebKit side to actually incorporate them).\n\nI'm sure once it's easier to compare output of KHTML and WebKit within KDE the stream of merge patches between the two will increase. I just hope the WebKit side will be more forwardcoming in incorporating KHTML improvements then."
    author: "Anon"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "Agreed, the KHTML developers really deserve better treatment.\n\nWithout them, Konqueror in KDE 4.0.x would be unusable as web browser (KHTML crashed too often and rendered pages like ancient browser before they started fixing bugs for KDE 4, and now it is pretty solid). And I'm also sad that some people never appreciate what the KHTML developers have done, especially those who can only yell: \"dump KHTML, use Webkit!\"\n\nI hope KHTML can remain alive, so that we have an alternative engine inside our own tree. I would also like WebKit to be used by KDE, but I guess having an engine that we have total control would be advantageous."
    author: "eds"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-25
    body: "WebKit is free software and can be forked at any time if WebKit's SVN becomes \"hostile\" towards KDE. But since some KDE and Trolltech developers got SVN write accress hostility is unlikely."
    author: "rdttgukk,fzjkuggh"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-30
    body: "A fork is no good if you've lost all of your developers. "
    author: "Anon"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "Well, do you think merging two source code trees that have been diverged for years is that simple? KHTML developers always do merging whatever can be merged from Webkit tree but I guess not all are as straightforward as you think. You can always check the websvn to see what have been merged ;)"
    author: "eds"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "I got 65/100 points with todays svn version."
    author: "digital.alterego"
  - subject: "Re: Well done KHTML developers"
    date: 2008-03-24
    body: "JavaScript performance also has been improved, I, and will be improved further if kjs-frostbyte branch has been merged to trunk.\n\nNice, kudos to KHTML team!"
    author: "fred"
  - subject: "network manager"
    date: 2008-03-24
    body: "I'm especially interested in the network manager thingy. Is this going to replace knetworkmanager as a kind of \"reincarnation\" as a Plasma-applet? Will it be based on Solid? Does it provide a Plasma engine which can be used by other applets? Or is it directly based on natworkmanager/dbus/HAL ?\n"
    author: "Thomas"
  - subject: "Re: network manager"
    date: 2008-03-24
    body: "possibly, yes, yes, no :-)"
    author: "sebas"
  - subject: "Re: network manager"
    date: 2008-03-24
    body: "So it will be\n\nPlasmoid --> Plasma engine -> Solid --> NetworkManager ?\n\nVery nice indeed. You'll get like a thousand places to feed the bugs.\nHappy hunting."
    author: "kde"
  - subject: "Re: network manager"
    date: 2008-03-24
    body: "4 <<< \"a thousand\".\n\nAlso, most of these places are very thin wrappers and unlikely to introduce many bugs - NetworkManager will do the \"heavy-lifting\"."
    author: "Anon"
  - subject: "Re: network manager"
    date: 2008-03-25
    body: "But you'll have less bugs since you have less duplicated code and more of that well-tested. It's also easy to use networky features on other platforms without rewriting your plasmoid. Using networkmanager is right now the backend for 'networky' stuff used on Linux, though the plasmoid doesn't need to care about that, thanks to Solid. With solidshell, it's easy to find out where it's going wrong.\n\nOf course no one keeps you from just porting knetworkmanager, if that works better for you. "
    author: "sebas"
  - subject: "Re: network manager"
    date: 2008-03-25
    body: "Sure, if your wifi chipset cannot connect to WPA network you'll have to hunt the bug in Plasma, sure.\nBe realistic, troll."
    author: "Vide"
  - subject: "Re: network manager"
    date: 2008-03-24
    body: "It could be a replacement for knetworkmanager but it doesn't have to be.  The dataengine is based on Solid.  Use cases for other apps include verification of network status and network usage stats."
    author: "Christopher Blauvelt"
  - subject: "Plasmoids in apps"
    date: 2008-03-24
    body: "Hi\nas time goes, we are getting more and more stanalone plasmoids.i dont think that having 3 (and counting!) rss reader plasmoids or a standalone 'todo list' plasmoid is good idea.\nI think plasmoids should be provided by apps, like KGet that includes a plasmoid.\nso rss readers should go into Akregator.with a datanengine that gets data from Akregator...\n\nthis way 3 rss readers should go into akregator and notes applet should go into knotes.TODO list viewer should be provided by KOrganizer and...\n\n(btw, the RSSNOW applet is so cool)"
    author: "Emil Sedgh"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-24
    body: "I never use akregator. Why should I have to download (Or, keep it installed since its a KDE application) a whole new application just to get a desktop widget? As long as the widget doesn't depend on the application (such as those provided by Amarok, for example) they shouldn't be bundled with the application."
    author: "Jonathan Thomas"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-24
    body: "I agree, that all those rss reader's should share their data with akgregator, but making those applets part of akgregator isn't the way to go. Since KDE4 has got akonadi for exactly these kinds of purposes. When there's a akonadi dataengine for plasma, all RSS data could be shared by all RSS applets/akgregator. And this way, nobody is forced to install akgregator if they just want one of those applets.\n\nGlad to see you like RSSNOW btw. :)\n\n"
    author: "Rob Scheepmaker"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-24
    body: "If the 'TODO list viewer' is a reply to my comment above asking for one then I should clarify I wanted a plasma applet that was integrated with Kontact/KOrganizer. I use that for managing all my time and would prefer to have it available on the desktop rather than buried in the app.\n\nDefinitely don't want more standalone tools!"
    author: "Martin Fitzpatrick"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-25
    body: "Akonadi is supposed to provide the data to both applications or applets. Till Adam has shown during the talk at last Summer's aKademy that it's relatively easy to have Plasma and Akonadi interact with each other. I'm pretty sure once Akonadi is in use, Plasmoids using that will pop up quickly. That's probably 4.2 timeframe, though."
    author: "sebas"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-25
    body: "i dunno ...\n\nthe fact that we have three rss readers sort of goes to show how easy it is to write these sorts of things now. the rss lib in kdepimlibs and plasma make it really trivial.\n\nand part of the idea of plasma is that no one person knows exactly how you want to do it. so ... we make it as easy as possible to create your own things. magically, people are doing just that.\n\nthat said, i think it is great to see kalzium, kget and akonadi (to name three) shipping plasmoids and engines of their own. that kind of integration can only get us even further.\n\nand thankfully, those two things (proliferation of MyPlasmoids and nicely integrated default plasmoids) are not mutually exclusive. we can do both."
    author: "Aaron Seigo"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "Aaron, \n\nI have only had a little play with a live CD of 4.0.0 so maybe I am missing some of the vision here but are these standalone plasmoids simply going to sit on the desktop?  If I have a lot in use, won't they just clutter up the screen real estate and make it unusable?  I know we can resize applets but I don't want to be doing that every time I need a function.\n\nThat would mean:\n\n1. Minimise running applications in order to see the desktop\n2. Locate and highlight the running plasmoid so that the border appears\n3. Drag to resize so that I can read it\n4. Use the applet\n5. Resize it back down\n6. Go back to using whichever application I want\n\nThis is quite a few steps and seems a little clumsy. Could we call applets to the forefront of the screen with shortcuts so that they appear \"layered\" over the top of running applications? That way, we wouldn't need to return to the desktop all the time.\n\nAlternatively, is that zoom feature going to be used to simply zoom in on the plasmoid we need on the desktop?\n\nPlease explain as I am probably missing some of the \"vision\" for the KDE4 series here! \n\nThanks, mate."
    author: "Kerr Avon"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "You already can do this (from day 1), I believe you have to press ctrl-f8 or 9 or something like that. I don't use it that often cos I don't use plasmoids (yet...no must have plasmoids for me yet)."
    author: "Fool"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "You can allready do this. Just press ctrl+f12, and your desktop with all it's applets will magically appear in front of your applications (which looks really cool with compositing enabled btw). And stuff where you allways want to keep an eye one, you can just put in the panel."
    author: "Rob Scheepmaker"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "1 won't be necessary, see other comments.\n2, 3 - zoom might help there.\n4 - of course you need that ;-)\n5 - zoom\n6 just click on a window which is still visible behind the applets and you go back."
    author: "jos poortvliet"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "Thanks for all of your prompt replies, Gents.  I'll have another play with KDE 4.0 very soon (when Fedora 9 is released!)\n\nCheers."
    author: "Kerr Avon"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "Hi\nhaving standalone plasmoids has a few problems:\n1)every standalone plasmoid needs a maintainer.so we will have the problem of 'unmaintained' plasmoids in future.\n2)The process of playgground->kdereview->kdebase-workspace will be there just for little plasmoids.really, plasmoids are little, they are not full applications...\n3)hunderds of bugzilla entries just for little plasmoids? reporting those bugs would be confusing...\n\nim not saying all plasmoids 'should' be included in an application.sure, some of them should be standalone.for example minimize/show desktop or analog clocks...but look at current status: the new plasmoids for konqueror/kate/konsole profiles should be included in konqueror, kate and konsole.not kdebase or anywhere else.\n\nabout the fact that a little rss reader shouldnt depend on akregator, i must say that third party plasmoids will come for that.but plasmoids that are included in KDE should be intergrated.\n"
    author: "Emil Sedgh"
  - subject: "Re: Plasmoids in apps"
    date: 2008-03-26
    body: "Most plasmoids will be available from kde-look, just like styles, superkaramba applets etcetera..."
    author: "jos poortvliet"
  - subject: "Another C++ Parser?"
    date: 2008-03-24
    body: "Generally I'm in favour of making competing implementations of a thing, but a CPP parser (a proper, full one) is a very large and difficult task, and KDevelop's one seems to be quite far along and sounds like it will be top-notch, from what I hear.  May I ask what is the rationale for the Umbrello devs to write their own one from scratch?"
    author: "Anon"
  - subject: "Re: Another C++ Parser?"
    date: 2008-03-24
    body: "I Agree. I don't know what level of completeless they need, so maybe a simple parser will do for them, but if they want \"real\" code-flow understanding, they should better think about interfacing umbrello with KDevelop through a KDevelop-plugin."
    author: "David Nolden"
  - subject: "Re: Another C++ Parser?"
    date: 2008-03-24
    body: "All they need is to be able to parse .h headers for class, method and property names. Doxygen comments if they are feeling fancy. Nothing like KDevelop's parser."
    author: "Ian Monroe"
  - subject: "Re: Another C++ Parser?"
    date: 2008-03-24
    body: "Aha, that makes perfect sense - thanks, Ian!"
    author: "Anon"
  - subject: "Re: Another C++ Parser?"
    date: 2008-03-24
    body: "> Nothing like KDevelop's parser.\n\nPresumably they would like to be able to read project files as well though - which is necessary to figure out how all the code fits together.\n\nKDevelop 4 has a lot of useful code for this."
    author: "Robert Knight"
  - subject: "Re: Another C++ Parser?"
    date: 2008-03-25
    body: "What project files keep track of interdependencies? Anyways... that's not C++."
    author: "Ian Monroe"
  - subject: "KNetworkManager"
    date: 2008-03-24
    body: "Does anyone know the status of KNetworkManager in KDE4 ?\n\nI see work is being done on knetworkmanager in the work branch, but afaik this has nothing to do with KDE4(trunk).\n\nI depend on this program every day with lots of different wlans and vpn connections, so if it is going to TRUNK I'm more than willing to contribute with some fixes where needed. "
    author: "User"
  - subject: "Re: KNetworkManager"
    date: 2008-03-24
    body: "Isn't KNetworkManager replaced by Solid?"
    author: "djvnklhjefjmwfn"
  - subject: "Re: KNetworkManager"
    date: 2008-03-24
    body: "Well, kinda. Solid will use NetworkManager as the default backend on Linux but as far as I know it's not ready yet. Still, someone will have to write a Plasmoid which offers KNetworkManager's functionality via GUI."
    author: "Erunno"
  - subject: "Re: KNetworkManager"
    date: 2008-03-24
    body: "oh no! \nMy impression of the NetworkManager guys is not very good after they have refused to implement the \"Refresh wireless networks-list\". \nCome'on! This is an invaluable functionality. I took my laptop in the bus. When I arrived I had 200 networks in the knetworkmanager and the one I wanted to use wasn't discovered yet. That is just plain dumb!!!!!!"
    author: "Pascal"
  - subject: "Re: KNetworkManager"
    date: 2008-03-25
    body: "I also hate it.\n\nThere is a solution to work around it. It's not perfect, but it works.\nFor Kubuntu it's the following\n\n/etc/dbus-1/event.d/25NetworkManager stop\n/etc/dbus-1/event.d/25NetworkManager start"
    author: "hias"
  - subject: "Re: KNetworkManager"
    date: 2008-03-25
    body: "Is this actually a functionality missing in NetworkManager itself or is it just not exposed via a GUI in nm-applet/KNetworkManager?"
    author: "Erunno"
  - subject: "Re: KNetworkManager"
    date: 2008-03-25
    body: "It's with NetworkManager itself. The knetworkmanager guys would like to fix it. There is a bugreport for it somewhere in bugs.kde.org where they explained it.\n\nDanm those gnomers ;-)"
    author: "Pascal"
  - subject: "Re: KNetworkManager"
    date: 2008-03-24
    body: "I build KDE trunk today and tested. \nKNetworkManager 0.2 runs without a glitch in the systemtray, so I guess its really not a problem..\n\n\nStill wonder if it will be ported to KDE4 though"
    author: "User"
  - subject: "Re: KNetworkManager"
    date: 2008-03-24
    body: "The networkmanager applet will be a functional replacement for knetworkmanager.  However, rather than AP based it will be profile based.  So you'll be able to click on the applet, and select a profile such as \"Work\", \"Home\" or \"Connect to any wireless network\"  Profile creation will be wizard or systemsettings based.  The network management portion of Solid is being re-worked by Will Stephenson to better accomodate changes in NM 0.7 and be more extensible in the future."
    author: "Christopher Blauvelt"
  - subject: "the constant"
    date: 2008-03-24
    body: "the velocity of development in kde 4.{0,1} make me wonder if there is something like the C constant 10^8 m/sec for speed, 10^8 LOCs per release or whatever ...\n\nkeep going the good works heroes \n"
    author: "Francesco R."
  - subject: "Re: the constant"
    date: 2008-03-24
    body: ":)\n\nIs 4.0.3 still on for tag/release around the beginning of next month?\nhttp://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule#26_March_2008:_Tagging_4.0.3\n\nI tried out the 4.0.2 and although it fell apart to the point of unusability (some self inflicted) I'm looking forward to having another go at it. As soon as it's useable I'll be over full time and helping bugsquash :)\n\nAny know if there's any chance of multiple panels soon?\n\nThanks for all the hard work & Danny for the digest."
    author: "Martin Fitzpatrick"
  - subject: "Re: the constant"
    date: 2008-03-25
    body: "Trunk has multiple panels. I'm not sure if that's been backported last weekend, so have a look at the commits by Riccardo. If it has been backported, it'll be part of 4.0.2 on April 2nd."
    author: "sebas"
  - subject: "Wow"
    date: 2008-03-24
    body: "The commit digest on Easter Sunday. Now if anyone complains once more about delayed publication and alleges Danny of lazyness, I'll hunt them down and break their legs.\n\nThat's just awesome Danny! Major coolness."
    author: "Jakob Petsovits"
  - subject: "Re: Wow"
    date: 2008-03-24
    body: "well, actually it is a week late. \nBut I appreciate reading it as much as the next guy!\nThanks Danny"
    author: "Pascal"
  - subject: "Re: Wow"
    date: 2008-03-25
    body: "pascal, you better watch out for jakob now! :)"
    author: "anonymous coward"
  - subject: "Re: Wow"
    date: 2008-03-26
    body: "10 days late, retarded.  Time Danny got the boot, and someone else given the chance to get these out timely."
    author: "anon"
  - subject: "Re: Wow"
    date: 2008-03-26
    body: "Aaah, I guess you volunteer?\n\nI hope you understand the Commit Digest will stop coming if Danny stops writing them, right? Nobody wants to do it because it is a huge amount of work... So if you consider 'never' timely, just keep on saying these things. Maybe Danny will quit and you can be happy cuz by then we won't have any news left (he's also a very important editor for the DOT)."
    author: "jos poortvliet"
  - subject: "Re: Wow"
    date: 2008-03-26
    body: "They already \"have the chance\" (all information needed to prepare a Commit Digest is freely available) and yet, there is a curious absence of people stepping up.  This is probably because you are the only person in the world who considers the Commit Digest running a few days late to be even worth commenting on, let alone spamming every edition about."
    author: "Anon"
  - subject: "Re: Wow"
    date: 2008-03-26
    body: "\"This is probably because you are the only person in the world who considers the Commit Digest running a few days late to be even worth commenting on, let alone spamming every edition about.\".\n\nAnd the worst thing is that he uses a name similar to yours, only with a small \"a\". Can't you kick his ass?"
    author: "Bobby"
  - subject: "Re: Wow"
    date: 2008-03-26
    body: "\"And the worst thing is that he uses a name similar to yours, only with a small \"a\". Can't you kick his ass?\"\n\nIf only it were that simple :("
    author: "Anon"
  - subject: "Re: Wow"
    date: 2008-03-26
    body: "everyone should stop replying to these idiots...they're just trolling"
    author: "Fool"
  - subject: "Re: Wow"
    date: 2008-03-25
    body: "Don't understand all that you wrote as my maths isn't that good :D But I too would like to commend the dream KDE team for the remarkable job that they are doing up there, down there - wherever.\nI just did a Suse update (KDE 4.0.66) and I can tell you, I got a pleasant surprise! Improvements, bugs squashing and polish all over the place, from Amarok 2 which I am now using to listen my mp3 collection as I type (I never had luck before) to Dolphin (preview now works like it does in Konqueror), Kickoff, which is starting to even look visually attractive ;), Oxygen, which is starting to feel like it's name implies and  Plasma. Yes the Plasma quys would make Asafa Powell look like a tortoise at the speed they are going these days :) I like the slim-glow theme btw.\nThere is also quite a bit of improvement in speed, the present devel-version that i am using feels a lot faster, smoother and more stable that the stable 4.0.2.\nI am just wondering what it will be like by 4.1? In any case I am sure it will be a hit!"
    author: "Bobby"
  - subject: "What about KDE Linux MCE?"
    date: 2008-03-25
    body: "First of all, congratulations for the good job done with KDE. KDE4 has a huge potential, as we can see with every release and every commit-digest!\n\nOn the other hand... Does anyone know anything about Linux MCE integrated into KDE? The last news I can find about it are from at least 6 months ago, and they tell us that Linux MCE will be integrated into KDE4.\n\nThanks in advance for your reply and thanks for this great desktop!\n\nPoldark"
    author: "Poldark"
  - subject: "Re: What about KDE Linux MCE?"
    date: 2008-03-25
    body: "Ask on their mailinglist or read it http://www.charonmedia.org/mailman/listinfo"
    author: "Dog"
  - subject: "Re: What about KDE Linux MCE?"
    date: 2008-03-25
    body: "yeah.. i'm wondering about this fact too. I've also found nothing neither in the linuxmce forum nor on any mailing list I'm subscribed (and those are many ^^)"
    author: "Bernhard"
  - subject: "digikam and raw 16 bits color depth auto gamma..."
    date: 2008-03-25
    body: "In commit log we can see:\n\n\"RAW image loader : backport 8 bits color depth auto-gamma and auto-white balance adjustements from dcraw with 16 bits color depth.\nThese auto adjustments are performed only if Color Management is not used with image editor.\nThe color rendering in 16 bits is exactly the same than in 8 bits.\nThis is want mean than you can process speedly your RAW exactly as JPEG. Just set your usual settings in RAW dedoding and open your RAW pictures as well in editor.\nThis is the same way used by LightZone pro software to process RAW file without color management!\nThis way simplify the task in Raw workflow... and will be suitable in 90% of case. Color Management must be used in others cases...\"\n\nAnybody has tried this feature? There is a comparison somewhere between LightZone and digikam about raw files import ? Sound like a major improvement because current raw file import always give a black image with 16 bits color depth."
    author: "Gandalf"
  - subject: "Re: digikam and raw 16 bits color depth auto gamma..."
    date: 2008-03-26
    body: "Here:\n\nhttp://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma\n\n... there are few screenshots. All are divided in 3 parts:\n\n- on the left: RAW converted to PNG using dcraw with 8 bits color depth.\n- on the midle: RAW loaded in digiKam Image editor with 16 bits color depth.\n- on the right: RAW loaded in LightZone.\n\nLike you can see, it's not exactly the same between digiKam and LightZone, but digiKam auto-gamma is not too bad and give a fast way to handle a suitable image with the best color quality.\n\nGilles Caulier"
    author: "digiKam <-> LightZone comparisons..."
  - subject: "Bennefit of Plasmoids ?"
    date: 2008-03-25
    body: "So Plasmoids can kill the whole window system, at least from the user's perspective.  Is this a step forward in desktop computing ?  I mean the beauty of X applications and the X desktop was that applications were isolated and the window manager was king.  And very very stable.  \n\nSo we have more eye candy, but how functional is this. Is it even necessary.  For instance I can launch knotes application or a note plasmoid, very little difference in how they look. Granted I can rotate the plasmoid, but what value is that. Who really even cares about eye candy.  I mean Vista and Mac have their own plasmoids but who ever uses them, except for maybe a clock placed on the corner of the screen.\n\nPlease somebody explain the value of Plasmoids.  Maybe they are easier to write via a scripting language. \n\nI know KDE has spent a lot of resources on doing usability studies, and it has paid off as things are are more consistent and better laid out.  Has there been a need's study for Plasma, if not maybe there should be one. And if found to be important it stays in as is, if not the KDE Window manager gets redesigned and Plasma gets taken out.  Sounds like heresy I know,as a lot of hard work went into it.  but what is best for KDE, and the KDE users\n\nHow about putting a simple poll on the KDE website. One question \"How do you value the use of Plasma (1 no vaule, 10, grat value)\n\nI give it a 3\n\n\n\n"
    author: "Mark"
  - subject: "Re: Bennefit of Plasmoids ?"
    date: 2008-03-25
    body: "Plasma is not only the Desktop Widgets. Plasma is about the Display and Behaviour of your whole Desktop. This for example includes the Desktop surface itself and the Bar that was formerly Kicker and is now more generally called the Panel, including its typical features like the taskbar and the start menu. \n\nI see it like this: The developers noticed that there is a lot of shared properties in the different parts of our desktop, and they need to cooperate so tightly, that it makes sense to integrate them. So they designed an architecture which generalizes some of the common capabilities, which are now shared between these parts through libplasma.\n\nFormerly, there was code to display icons in kdesktop, code to display desktop widgets in superkaramba and code to display icons in kicker. Now this is all done with (nearly) the same code in plasma.\nThis alone makes perfect sense to me and justifies the use of Plasma. And there is a lot of additional value, like the option to write Plasmoids in Scripting Languages and share data sources across all Plasmoids. \n\n\n> And if found to be important it stays in as is, if not the KDE Window manager\n> gets redesigned and Plasma gets taken out\n\nPlasma is not implemented in the Window Manager, and neither was the desktop in KDE 3. It was also a dedicated process. "
    author: "blubb__"
  - subject: "Re: Bennefit of Plasmoids ?"
    date: 2008-03-25
    body: "first, no, vista and mac do not have their own plasmoids. they have widgets. there are some pretty fundamental design differences between how these things are done.\n\nand yes, if you pick the right use cases (e.g. notes vs knotes) and ignore everything else you can construct an argument for your case, just don't expect me or anyone else with any insight into the matter to take it seriously.\n\nyou're concentrating on widgets in isolation and completely ignoring the componentization, cooperation, grouping and portability of them.\n\nnow, to get to your opening:\n\n> Plasmoids can kill the whole window system\n\nno they can't, no more than any other window in your session can. your rant here starts out on a completely incorrect assumption. way to go.\n\n> if not the KDE Window manager gets redesigned and Plasma gets taken out.\n\nthe window manager has nothing to do with plasma. this sentence is just .. well .. nonsense =)\n\n> I give it a 3\n\nthen don't use it. it's that simple. whoe needs launchers, a pager, a system tray or a taskbar, right?"
    author: "Aaron Seigo"
  - subject: "Re: Bennefit of Plasmoids ?"
    date: 2008-03-26
    body: "My take:\n- from the user perspective, until now plasma is just a fancy superkaramba that kills your desktop when one plasmoid crashes (duh, very nice one!)\n- from developers it's heaven to code and shall lead to better to faster/easier development of the whole desktop\n\nNow if we could get rid of *ALL* plasmoid written in C++, the plasmoid crashing and taking away the whole desktop won't happen, but I don't see much script plasmoids so far, only C++ ones :-P"
    author: "Iuri Fiedoruk"
  - subject: "Boring news?"
    date: 2008-03-26
    body: "It is just me or the exiting news in KDE4 are diminishing?\nI would love to see news about k3b, panel (lots and lots of missing features there), kmail, kopete, etc. To me, this looks actually like a good thing, probally things are starting to get more mature in kde4 code.\n\nAnyway, in this edition I found good news about amarok, ktorrent, phonon supporting subtitles and multiple audio/text/video tracks is very good, I hope they do support font rendering options (font face, size and color).\n\nMeanwhile until KDE 4.1 arrives I wait for 4.0.3 that will come with fixes for my most hated bugs (gray desktop instead of wallpaper and no double click)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Boring news?"
    date: 2008-03-29
    body: "Boring?\n\"David Faure committed changes in /trunk/KDE/kdebase/apps/konqueror/settings/konqhtml:\nGUI: checkbox for the \"Middle click on a tab closes it\" option (which has existed for a long time, but didn't have a GUI).\"\n!!!!!!!!!! :DDDDD"
    author: "Koko"
---
In <a href="http://commit-digest.org/issues/2008-03-16/">this week's KDE Commit-Digest</a>: The beginnings of a network management applet in <a href="http://plasma.kde.org/">Plasma</a>, work on containments, and improvements in the "RSS" data engine and "Devices" applet. Initial support for saving changes to documents in <a href="http://okular.org/">Okular</a>. A <a href="http://kross.dipe.org/">Kross</a>-based scripting plungin in KLinkStatus. "Tip-of-the-day" and "Qt methods to avoid" checkers for the Krazy code quality reporting system. First steps towards a C++ parser in <a href="http://uml.sourceforge.net/">Umbrello</a>. Work on projection independence in <a href="http://edu.kde.org/marble/">Marble</a>. More maintenance and bux fixing in Kooka. Continued work in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Expanded support for subtitles and audio channel selection in <a href="http://phonon.kde.org/">Phonon</a> (prompted by the requirements of <a href="http://www.dragonplayer.org/">Dragon Player</a>). A rewritten "Todo" view in <a href="http://korganizer.kde.org/">KOrganizer</a>. Further work towards <a href="http://amarok.kde.org/">Amarok</a> 2.0. "HTTP webseeding" in <a href="http://ktorrent.org/">KTorrent</a>. Kross-based scripting in <a href="http://koffice.kde.org/kplato/">KPlato</a>, with support for "find in multiple documents" in <a href="http://koffice.org/">KOffice</a>. Caching support extended to all KDE card games. Improvements in <a href="http://edu.kde.org/kturtle/">KTurtle</a>, KNetwalk, and Kubrick. Initial imports and work on the "Killbots" and "Astrododge" games. Import of "RSSNOW", an alternate RSS feed reader Plasmoid. The <a href="http://stepcore.sourceforge.net/">Step</a> physics package moves from playground/edu to kdereview so that it can move to the <a href="http://edu.kde.org/">kdeedu</a> module for KDE 4.1. <a href="http://commit-digest.org/issues/2008-03-16/">Read the rest of the Digest here</a>.

<!--break-->
