---
title: "Three German KDE Deployments"
date:    2008-06-04
authors:
  - "skuegler"
slug:    three-german-kde-deployments
comments:
  - subject: "nice, but ..."
    date: 2008-06-04
    body: "This sounds very nice.\n\nThough I don't really understand the \"IT Service Center Berlin\"-part and I can read german.\nWell I allways had the feeling or rather the experience that MS Software does not work well with Linux, Ubuntu in this case, but this text says the opposite. Or maybe they only mean the server software they are talking about later...\n\nI also do not understand why they opted for KDE on Ubuntu.\nNo LTS there and that's important for large organisations imo."
    author: "mat69"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "I don't understand what you don't understand ;-)\nThe text says:\n\nDurch das erfolgreiche Zusammenspiel von Microsoft-Technologien mit dem Linux-Betriebssystem Ubuntu und der grafischen Oberfl\u00e4che KDE konnten diese Anforderungen erf\u00fcllt werden. Nach einem entsprechenden Integrationsaufwand war die Open-Source-L\u00f6sung einsatzbereit. \n\nSo, they clearly underline that the interesting part of the development was in fact making it work flawlessly with Microsoft. \n\nAnd earlier on:\n\nGeinsam mit Microsoft Deutschland und der LIS AG hat der IT-Dienstleister eine L\u00f6sung konzipiert,\n\nSo Microsoft did help here somewhat to make it work (I assume reveal some interface documentation). Probably, because they stood with their back against the wall considering the fact that they would probably have lost the business contract altogether if they didnt comply.\n\n\n\n\n\n"
    author: "Michael"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "I guess I was just too tired yesterday.\n\nI don't think that MS stands with their back against the wall in terms of losing customers.\nDespite all the talks about Vista being so desperately bad (no intention to say if this is really the case or not) the figures show that MS is more profitable than it has been most times.\n\nMaybe it has to do with the EC and the competition regulations that they \"cooperate\" (under \"\" because I don't know how they cooperate).\n\n\n()()()()()()(just had to add some more brackets :D )"
    author: "mat69"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "\"I don't think that MS stands with their back against the wall in terms of losing customers.\"\n\nWho talks about customers? They talk about buying licenses. And if Microsoft demands expensive licenses, they for sure will look for alternatives.\n\nIt is a win-win situation for taxpayer because they have more choice than before."
    author: "markus"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "Well, I don't think we'd be reading about this article here if they'd have opted for Gnome on Ubuntu. ;) Debian is a great base for nice DE like KDE.\n\nIt's not about MS software working well with Linux. They'd never lift a finger to make something work with linux. It's about Linux working well with Windows. And, yes, Linux works better with Windows than Windows itself. I know. Because I have to admin a couple of domains - MS' concept of a domain. :p"
    author: "winter"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "Uhm. You and the poster below misunderstood me.\nIt's not about pro KDE or con, but rather about long time support. So what I meant is that maybe Ubuntu is not the best (who knows) solution for running KDE in public administration, as there is no LTS for KDE.\n\nAnd about the \"Linux working better with MS than MS\"; that rather sounds trollish and I have no intention to be part of a MS vs Linux flame war. Each to their own.\nFor me this means obviously KDE. ;)"
    author: "mat69"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "Well, LTS is mainly a marketing word. If they would care on such things they would have choosen SUSE, RH or Mandriva.\n"
    author: "Anon"
  - subject: "Re: nice, but ..."
    date: 2008-06-04
    body: "KDE is just better."
    author: "blueget"
  - subject: "Nice!"
    date: 2008-06-04
    body: "In Germany, KDE is pretty popular, so this isn't a surprise.\n\nBut it's nice to see they will go for Ubuntu. From what I experienced, SuSE (would be openSUSE now) is more common in corporate environments there.\n\nNot that there's anything bad about openSUSE, I recently switched over from Kubuntu because Kubuntu feels so immature. And Ubuntu folks don't really seem to care about making the KDE flavour competitive with the GNOME flavour.\n\nDecisions like this could make Ubuntu concentrate more on KDE - and become the KDE distribution of choice.\n\n\nWell, and the rest... they're not switching, they're just developing an alternative from what I understand. But who knows, they might get rid on M$ products sooner or later, which is IMO WAY more important than which DE they chose. I don't care if company X uses Microsoft products until they all get a PWND stamp on their forehead, but a government agency should NOT bow to a foreign (in the case of Germany) monopolist.\n\n\nAll in all, it's Nice!"
    author: "fhd"
  - subject: "Re: Nice!"
    date: 2008-06-04
    body: "Yeap, I really don't understand why would someone go for Kubuntu over openSUSE. And this is an honest question, what kind of advantages do you see in it? \n\nMy experience with Kubuntu was rather brief so I might be biased, but openSUSE seems way more polished. Considering that their admin tools are excellent, the repositories huge, and the updates super quick to show up. So I really cannot think of a motive and would have expected them to go for openSUSE which is quite popular in Germany. \n\nMaybe (K)Ubuntu is slightly easier to install for a linux newbie (so I've heard, never had to install it), but openSUSE is quite easy too so in a corporate environment shouldn't make a difference. "
    author: "ad"
  - subject: "Re: Nice!"
    date: 2008-06-04
    body: "Anyway, just to make it clear, nothing against KUbuntu. You guys must be doing something right ;) So more power to you and congrats :)"
    author: "ad"
  - subject: "Re: Nice!"
    date: 2008-06-04
    body: "every time I've tried openSUSE I soon gave up on it because it was just so unbearably slow. (K)Ubuntu feels much snappier."
    author: "ac"
  - subject: "Re: Nice!"
    date: 2008-06-07
    body: "Slow at which point?\n\nI've compared Kubuntu Hardy and openSUSE 11 (Kubuntu was running on a faster laptop) in the default setup. Kubuntu takes nearly 90 seconds to load KDM, whereas it appears in openSUSE after 20 seconds.\n\nPackage management in openSUSE has overcome a long dry spell and feels now faster than apt."
    author: "Stefan Majewsky"
  - subject: "Re: Nice!"
    date: 2008-06-08
    body: "That HAS to be a lie. kubuntu feels the sluggiest of all distros I tried out recently. Mandriva is the snappiest. Now opensuse seems set to take over from mandriva.\n\nFor the life of me, I cannot understand WHY anyone would want to use kubuntu/ubuntu (except marketing). KDE people should probably realize that KDE based distros are dying or declining and gnome based distros are gaining and they ( KDE devs) are to be blamed. They keep promoting gnome based distros even among the KDE crowd.\n\nThey should get behind KDE based distros and make them better. That is what gnome people did for gnome. They are misinformed by gnome advocated they meet at trade fairs, sites like distrowatch etc. "
    author: "vm"
  - subject: "Re: Nice!"
    date: 2008-06-04
    body: "KDE is very popular in germany, whole of europe, asia and south-america at least."
    author: "mill"
  - subject: "Auswaertiges Amt and German embassies"
    date: 2008-06-07
    body: "There is no link for this third deployment in the article. Can anyone clue me in as to whether their Linux client is for the Auswaertiges Amt itself only or for the embassies as well? Please note, my question is quite selfish. If the German Embassy here in Windhoek is required to use Linux, they probably need a bloody good local consultant which could be yours truly. ;-)\n\nUwe"
    author: "Uwe Thiem"
---
The IT Service Center Berlin <a href="http://www.itdz-berlin.de/presse/85.html">has announced the development of a desktop system</a> for the public services in Germany's capital (<a href="http://translate.google.co.uk/translate?u=http%3A%2F%2Fwww.itdz-berlin.de%2Fpresse%2F85.html&sl=de&tl=en&hl=en&ie=UTF-8">Google Translate to English</a>). This is yet another public body making the switch to the Free Desktop system. The announcement talks about the good integration of KDE with their current infrastructure, which is partly based on Microsoft's software. According to the ITDZ's press release, the integration phase has successfully finished and the KDE-based client for Berlin's administration is now ready for prime time.

At LinuxTag, a big German Free Software conference and fair, the "Bundesamt für Sicherheit in der Informationstechnik", Germany's national information security group, announced their <a href="http://liquidat.wordpress.com/2008/05/30/kde-at-linuxtag-2008-day-2-taking-over-the-world/">successful porting of KDE's Email and Groupware client</a> to the three major platforms, Windows, Mac OSX and Linux. Finally, the "Auswärtiges Amt", responsible for German embassies in more than 200 countries also showed their KDE based desktop client at LinuxTag, which has been deployed to their employees already.




<!--break-->
