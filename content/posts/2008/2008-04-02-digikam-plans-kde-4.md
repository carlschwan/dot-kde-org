---
title: "Digikam Plans for KDE 4"
date:    2008-04-02
authors:
  - "mgilles"
slug:    digikam-plans-kde-4
comments:
  - subject: "stab at KDE 4.0 release?"
    date: 2008-04-02
    body: "> I would not provide a \"stable\" release full of bugs.\n\nlooks like some KDE devs didn't like the 4.0 release full of bugs either :)"
    author: "John"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-02
    body: "Absolutely (:=)))\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-03
    body: "the irony is that a little further below you complain (rightfully) about how it wasn't possible to develop against the always moving libs from last summer (pre-4.0). 4.0 specifically, purposefully addressed that issue by getting the core functionality moved to \"stabilize! complete features!\" rather than \"more new developments!\". the expectation was that this would allow application developers such as yourself to get your efforts on track instead of being continuously held up by a never ending library development party."
    author: "Aaron Seigo"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-02
    body: "4.0 is not a stable release. It is a development release. 4.1 will be the first stable release."
    author: "Anon"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-02
    body: "And what about printing interface ???\n\nIn digiKam for KDE4 i have disable all KDEPrint support because all api become obsolete/unmaintened (fix me i'm wrong)\n\nKDE 4.1 will re-enable it or find alternative (using QT 4.4 for ex.)\n\nCan you said than KDE 4.1 will be the first release ready to use in production if printing interface cannot be used ?\n\nGilles Caulier\n\n\n"
    author: "Gilles Caulier"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-03
    body: "printing already works in trunk/, so there's no need to wonder idly anymore. it uses the printing support from Qt 4.4 and it's progressing rather nicely."
    author: "Aaron Seigo"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-03
    body: "Thanks for the info Aaron. I wil review digiKam printing code in this case.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-03
    body: "Well, then why is KDE filed under the \"Stable\" section on kde.org? And why does the release annoucement for 4.0 not contain any warning about this being a relase targeted mainly at developers? Not that I intend to beat a dead horse but I'm also somewhat allergic to revisionism. And I don't count developer ramblings on obscure blogs and mailings lists. kde.org being the main internet portal for KDE is as official as it gets. And it states that 4.0 was stable although reality disagreed on a regular basis.\n\nAnyway, 4.1 looks like it's going to be a solid release and with 4.2 most applications should be finally ported."
    author: "Erunno"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-03
    body: "When i have started to port digiKam to KDE4 at July 2007, i have seen imediately several problem in core libraries. I don't speak about full desktop which for me have been unsuitable at the date.\n\nThis duing a lots of major changes in API. For big a big library as KDE, the list of regression tests is simply huge...\n\nThis  is why is have never planed to try to plan digiKam 0.10.0 for the first KDE 4.0 release. It been impossible to have a stable appplication.\n\nYou have right. KDE 4.1 will be better, and certainly KDE 4.2 the first release which can be used in production.\n\nThis is not a critic. It's just the reallity. And if you look into the pass, the same situation have been seen with KDE 2.x to KDE 3.x transition.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: stab at KDE 4.0 release?"
    date: 2008-04-03
    body: "> For big a big library as KDE, the list of regression tests is simply huge...\n\nthankfully the number of real regressions has been rather small. there are some important ones, mostly that we knew about already due to technical issues we ran into (the ssl and proxy issues are good examples o those).\n\n> It been impossible to have a stable appplication.\n\nit may have impossible for digikam, i can see that, but dozens of apps shipped for 4.0 and were quite stable.\n\nthat said, you're quite right about \"chasing a moving API\" being a major blocker for application developers. getting 4.0 out the door was a vitally important step for addressing that and increasing the stability / lowering the regression counts in the core libs. this happens best when drastic new changes aren't happening any more, more applications stress it and more users do different things with it. it's far more pleasant and realistic to develop apps against the kde4 libs at this point.\n\nas such, i think we'll see a bloom of applications in the 4.1-4.2 timeframe, digikam being a good example of that with it's release date of september."
    author: "Aaron Seigo"
  - subject: "Google Maps Integration"
    date: 2008-04-02
    body: "Reading the note about the Google Maps/Marble integration made me think - is there an underlying system (one of the new KDE4 backends) that handles map integration? It would be better if the choice of map 'server' was determined system wide and applications queried through a single API.\n\nThat way any application needing to show map information, positioning etc. could do it in a simple unified way - and this could be easily configured to individual (or local) preferences.\n\nAlso, a more general question - is there a KDE4 API for access to any other data sources like these?\n\nbtw. Digikam looks very nice... as does the KDE4/dark theme (where is this available?).\n\nKeep up the good work!"
    author: "Martin Fitzpatrick"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-02
    body: "Having a \"Map Application\" is already quite a step forward. As Marble supports the KML format (the one that's also used by GoogleEarth), it can probably be integrated with other \"Map Applications\" as well.\n\nOne step at a time, though :-)"
    author: "Sebastian Kuegler"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-02
    body: "Hah of course :) I like what I see - it just set my mind whirring!\n\nI really like the way that KDE4 has moved towards abstracting data services to the applications, opens up a lot of possibilities.  It would be nice if there were some kind of subsystem which could accept plugins/data sources and present them to applications. Maybe there already is... I'll be the first to admit I don't really know what I'm talking about ;)\n\nBut, yes, patience... patience..\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Here, i  have started to play with marble widget integration in digiKam right side-bar for geolocation of photo :\n\nhttp://digikam3rdparty.free.fr/Screenshots/marbleintegrationforphotogeolocation.png\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Yay!\n\nI would imagine the following use cases for this:\n\n- tagging a series of photos with geolocations\n- selecting an area in marble and show all the photos that have been tagged with that area"
    author: "sebas"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Yes,\n\nThis will be the second pass : to have a Search GUI based on marble to be able to find a photo using a map. It's in my TODO list...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Thanks. That's actually a possible usecase I've used while presenting Marble. Of course it's something I'd love to use. Awesome to see you're planning to implement this. :-)"
    author: "sebas"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Marble integration in digiKam right side bar is now complete on my computer. I will polish the code and commit this evening.\n\nThe screenshot have been updated:\n\nhttp://digikam3rdparty.free.fr/Screenshots/marbleintegrationforphotogeolocation.png\n\nNext stage is to build an interface to \"Search photo on a map\"...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Done in svn with Rev. 793334.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-04
    body: "New blog entry about marble geolocation in digiKam have been created:\n\nhttp://www.digikam.org/?q=node/306\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-02
    body: "\"btw. Digikam looks very nice... as does the KDE4/dark theme (where is this available?).\"\n\nIf im correct, that isn't a KDE4 color schema, it's Digikam own. Last stable version Digikam has supported limited color schemas, where user could select only a preview background, selected/non-selected image background and foreground and text effects for special text (labels and tags and rating).\n\nBut now Digikam has new theme possibilities what allow to change digikam window color to anykind you like, without effecting to other KDE applications.\nExample, you can have blue-white color schema on KDE but then make a own color schema with digikam own theme tool and apply it so digikam window is using colors what you selected, not blue-white what is applyid from KDE control panel. This is very important feature to bring digikam as a professional application because colors should be so much middle gray as possible and this has be impossible.\n\n"
    author: "Fri13"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-02
    body: "\"If im correct, that isn't a KDE4 color schema, it's Digikam own.\"\n\nThanks for the info, had never occurred to me that this would be useful in photo/image apps, but looking at the screenshots I can see why. I guess if it's just colour changes I can apply them through the normal KDE appearance settings as well.  My eyes are going to be very grateful I think... (although the browser is going to look weird :)\n\nThanks again."
    author: "Martin Fitzpatrick"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-03
    body: "Middle gray is 127, 127, 127 (RGB 0-255 settings) but because it's computer monitor what has backlight and not gray-card what reflects light, it might be better to have 64, 64, 64 colors. \n\nMiddle gray is needed because other wise other surrounded colors affecst the photograph colors and it's \"developing\" is harder. \n\nI were once in a great studio where was own room for Photograph manipulation, there were 15 PC's in that room, whole big room was painted with middle gray, all stuff there was middle gray and there were special lighting what were calibrated once a week and few times a week every monitor were calibrated (all monitors were those 10-14bit and not normal 8bit monitors). And then admins allowed normal users to use wallpaper what user liked. \n\nAnd users were having photoshops etc open and their MacOSX default Blue background really were a shiny blue DOT in that room and I was shocked that they didn't understand that one of new students problem to have too warm images was simple as that, WRONG WALLPAPER in color controlled room :-D\n\nWith middle gray, all colors will be seen much easier and neutral as possible. "
    author: "Fri13"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-05
    body: "What's the point of such an abstraction? Why not just use Marble directly and drop support for the proprietary Google Maps (now that we have a Free alternative in KDE)?"
    author: "Kevin Kofler"
  - subject: "Re: Google Maps Integration"
    date: 2008-04-05
    body: "Resolution of maps in Marble are limited. If you want more details, we need advanced tool. googlemaps is fine.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "openSUSE"
    date: 2008-04-02
    body: "Does anybody know where openSUSE (11.0 factory) users can find the new digikam? It seems neither to be available as single package nor in some extragear-RPM.\n\nThis is strange, as for any other KDE4 package than Digikam (and, equally unfortunately, k3B) openSUSE offers superb support and up to date packages., even of development versions.\n\nOnly for these two apps, my favorites beside Amarok, you can't use a KDE4 version."
    author: "Anon"
  - subject: "Re: openSUSE"
    date: 2008-04-02
    body: "They are still in alpha state, it is not very common (for any distribution) to provide alpha quality software by default. You can create a kde-devel account and compile them from svn everyday, like I do. That not hard at all, and you have cutting edge apps everyday.\n\nBy the way, three more screenshot I took from digikam (compiled today)\n\nhttp://img143.imageshack.us/img143/1347/snapshot2eg3.png\nhttp://img399.imageshack.us/img399/9470/snapshot3fa2.jpg\nhttp://img258.imageshack.us/img258/3431/snapshot4zm2.png"
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: openSUSE"
    date: 2008-04-02
    body: "Whow, nice photos...\n\nand i can see than i'm not alone to love black color scheme theme (:=))))\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: openSUSE"
    date: 2008-04-03
    body: "There is again new suggestion for selecting files, currently i use double click mode and i like ctrl+shift modes because those works for everything else too, on SVG editing, image editing and text editing, not just file management.\n\nIt's just very dificult way to get something better when there is so many posibilies what use one way but cant work with others.\n\nhttp://ppenz.blogspot.com/2008/04/selecting-items.html"
    author: "Fri13"
  - subject: "Re: openSUSE"
    date: 2008-04-03
    body: "you rock, n1 photos !!!"
    author: "nice"
  - subject: "Re: openSUSE"
    date: 2008-04-03
    body: "\"They are still in alpha state, it is not very common (for any distribution) to provide alpha quality software by default.\"\n\nObviously you have either not used openSUSE or not read my posting ;). openSUSE DOES provide quite commonly a wide rande of KDE4-and-apps-RPMs, mostly unstable development versions (e.g. a pretty usable KDE4.1 snapshot repository, updated every few days). That means the average openSUSE user does NOT have to use a SVN account, nor to compile any program, but must only add a \"KDE:UNSTABLE\" repository to the system.\nSee http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/\n\nHowever, while there are lots of other software pieces, from pretty usable to \"crashing all the time\" state in this repo, I cannot find neither Digikam nor K3B (which is told to be quite usable already)."
    author: "Anon"
  - subject: "Thubbar?"
    date: 2008-04-02
    body: " \"I have included the thumbbar in the Image Editor (F4) and in the AlbumGUI with Preview mode (F3). The same cache is used everywhere.\"\n\nIs there still possible to use current view style and not thubbar? I mean that old where are thumbnails and when user double clicks image, it gets open to editor or big preview?\n\nI like this new thubbar but I like current style too :-)"
    author: "Fri13"
  - subject: "Re: Thubbar?"
    date: 2008-04-02
    body: "Yes, in editor, View/Show Thumbnails menu option.\n\nIn Algum GUI, i have forget to add this option. I will fix it. Of course, you can reduce to minimal size the widget separator betview icon view and thumbbar.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Thubbar?"
    date: 2008-04-02
    body: "Done in svn with rev. #793002\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Thubbar?"
    date: 2008-04-03
    body: "Nice! DigiKam just looks more and more very professional application, it has be very pro as in usage but now it looks too!"
    author: "Fri13"
  - subject: "Wow!"
    date: 2008-04-02
    body: "Looks like Digikam is going to ROCK when it arrives for KDE4."
    author: "Joergen Ramskov"
  - subject: "Re: Wow!"
    date: 2008-04-02
    body: "Here you can see more screenshots of new Advanced Search dialog.\n\nhttp://digikam3rdparty.free.fr/Screenshots/NewSearchTools/\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Just thanks"
    date: 2008-04-02
    body: "I really love Digikam, thanks!!!"
    author: "LB"
  - subject: "Phonon ?"
    date: 2008-04-02
    body: "hi :)\nany plan to use Phonon for the embeded video preview ?"
    author: "DanaKil"
  - subject: "Re: Phonon ?"
    date: 2008-04-02
    body: "Already done in svn...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Version numbers"
    date: 2008-04-02
    body: "What about going from version 0.9.0 to 1.0.0 instead of 0.10.0? I would reflect the mature state that DigiKam is in, so it would definitely be justified. Just like K3b - time was overdue.\nI know that version numbers are controversial sometimes and that some will criticize your chosen number as hype-driven and being purely for marketing reasons if you put the number too high. (Well some people will always nag if you don't prefix with two zeros and have an -alpha appendix if they experience a crash and if you don't have the feature list of commercial alternatives).\n\nAnd at release time it certainly would give you extra marketing - big version jump instead of simple port is more newsworthy for many more sites. You could surely reach more photographers and increase your userbase, especially if you have a Mac/Windows port ready, and with the KDE4 port I'm sure that the codebase is ready for that. And you could show people that they can trust the app and that basic features are not a problem. Not only that, with so many pro level features that stage is long behind.\n"
    author: "Anon"
  - subject: "Re: Version numbers"
    date: 2008-04-02
    body: "This subject have been already discuted on digiKam mailing list. I'm not favorable to toogle now to 1.0.0 version... A fondamental feature is not yet implemented : a powerfull backup/restore tool.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Version numbers"
    date: 2008-04-06
    body: "With all the respect due, and even if others \"pro\" apps on other platform have this fuction, I don't think this is what makes Digikam a 1.0 program or just a 0.10. Backup/restore policies should be something outside the scope of a user program and should be part of the \"operating system\".\nWhat's my point? That Digikam is such a great program and that the KDE4 port will be a gigantic leap that deserve a 1.0 version in any case :)"
    author: "Vide"
  - subject: "16-bits is nice for pros, but..."
    date: 2008-04-02
    body: "... for Joes like me, I'd rather see the tagging GUI improved. Using the mouse to click checkboxes is so slow! It would be better to have a good autocompletion feature so tagging could be done entirely from the keyboard.\n\nAlso, I'd like to see tagging where tags correspond to an actual X,Y location on the photo. (Obviously that would require the mouse, but for just one click it's worth the time.) If that was supported, it would be possible to upload directly to Flickr or Facebook from Digicam! It's frustrating to have to tag everything locally AND on online, so for large albums I usually don't bother tagging them locally. The Facebook API is all there (http://developers.facebook.com/documentation.php?v=1.0&doc=photoupload) we just need to support X,Y coordinates for tags. Any Google Summer of Coder's want to take that on? (I'm already invested in another idea this summer.)"
    author: "kwilliam"
  - subject: "Re: 16-bits is nice for pros, but..."
    date: 2008-04-02
    body: "I agree that, at least double-click on a tag (or maybe even single clic) could check/uncheck a tag in the tags treeview (yep, the checkbox is small)\n\nAnother idea : a shortcut that can apply the last applied tag to the current picture"
    author: "DanaKil"
  - subject: "Re: 16-bits is nice for pros, but..."
    date: 2008-04-02
    body: "Same here. I work currently on keyboard shortcut to assign tags on image (like kmail do with folder assignement)\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: 16-bits is nice for pros, but..."
    date: 2008-04-02
    body: "you could add a button at the bottom of the tags treeview with something like \"Set a shortcut for the current selection of tags\" (so I can choose some tags for a picture, assign a shortcut for this set of tags and then easily apply all these tags in one shortcut)\n\njust an other idea (I know dot.kde is not bugzilla but well...) : when I tags a lot of picture, I often works in thumbnails mode and select all pictures that I wants to be tagged with a particular set of tags. And often, I release \"ctrl\" for the multiple selection and lose all my selected images (and then I cry).\n\nMaybe you can add in the Edit menu a mode to easily select multiples pictures : no need to keep ctrl pressed, when you click a picture, it is added to the selection, if you click on it again, it is substracted from the selection\n\n"
    author: "DanaKil"
  - subject: "Re: 16-bits is nice for pros, but..."
    date: 2008-04-03
    body: "Maybe the way Dolphin does allow you to select several items by using a small icon overlaying the item might be a solution."
    author: "jospoortvliet"
  - subject: "Re: 16-bits is nice for pros, but..."
    date: 2008-04-03
    body: "Yes, there is an entry in digiKam bugzilla. Marcel will take a look when he port all icon view to Qt4 model/view (this part still use Qt3 transition classes). It's planed later than 0.10.0 release\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: 16-bits is nice for pros, but..."
    date: 2008-04-02
    body: "I'm currenty working on, for KDE3 and KDE4 version.\n\nLook this entry : \n\nhttp://bugs.kde.org/show_bug.cgi?id=114465\n\nOn the \"Caption and Tags right sidebar, there is a new fied where you can type  tags hierarchies (yes, more than one at the same time) using only keyboard. Auto-completion is supported :\n\nhttp://bugs.kde.org/attachment.cgi?id=24102&action=view\n\nThe Tag Edit dialog have been improved in the same way. Look here :\n\nhttp://bugs.kde.org/attachment.cgi?id=24136&action=view\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Database? Metadata? Strigi/Nepomuk integration?"
    date: 2008-04-02
    body: "Thanks for the great work on Digikam!\n\nSince you discussed a new database schema and more metadata, I was wondering if this is all being stored (and searchable) using Nepomuk and Strigi or if Digikam is maintaining its own database?  It would be great if all metadata in Digikam, including tags, ratings, as well as camera information, could be shared with the rest of the desktop.  For example, Dolphin now has tags and ratings for files.  For pictures, this should be the same ratings you see in Digikam, or whatever other photo application one might be using.\n\nI'm actually wondering the same thing about Amarok.  This is the kind of integration I look forward to seeing throughout KDE 4."
    author: "Yuriy Kozlov"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration?"
    date: 2008-04-02
    body: "Nepomuk integration is planed for the future, but it's not the priority actually : it's time to stabilize source code to be able to release for september.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration?"
    date: 2008-04-03
    body: "I'm sorry to hear that. Please reconsider as it is one of KDE's key features to offer broad integration. It is one of the features what makes it shine compared to other Desktops. Database applications like DigiKam should promote this feature as soon as possible. DigiKam on the other hand will only gain in importance, insight, influence and momentum.\n\nAnyway keep up the great work!"
    author: "DigiKam lover"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration?"
    date: 2008-04-07
    body: "I dont see reason to horry nepomuk integration. I think digiKam has great handling with photographs with it's own ratings and tags. Later nepomuk support would be nice but now I dont see any reason for it. Because I dont see Digikam as a tool for \"normal joe\" who just browse wallpapers and other images what avarage joe downloads from internet. DigiKam is for professional/hobby photographers , who keeps digikam running anyway when needed to handle big amounts of photographs and easily to manage those.\nLater that nepomuk integration can be nice but.... Mayby I'm just wrong person to judge that with my photograph library what includes tens of thousands of photos and browsing those with nepomuk sounds bad because I need to have tags and comments on photos and albums what I dont want to find when doing desktop search."
    author: "Fri13"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration?"
    date: 2008-04-07
    body: "I'm totally agree with this viewpoint. There is not urgency to include Nepomuk support for 0.10.0 release. Of course, later, when new digiKam core Database interface will be stabilized, something can be done to connect digiKam database to nepomuk. But in all case, later 0.10.0 relesase...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration"
    date: 2008-04-07
    body: "No need to hurry indeed, BUT... ther are at least 4 people requesting it (read \"KDE 4.1?\", so you are basically the only one who thinks \"browsing those with nepomuk sound bad\" "
    author: "DigiKamLover"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration?"
    date: 2008-04-08
    body: "Great work! I for one am looking forward to stable KDE4 release :-)\n\nAs for Nepomuk integration, I'm really waiting for that as well, because I understand that it will not only offer the infrastructure to do the indexing (tagging) and allows for the same lightspeed categrization and search we are used to with digiKam, but also allows tight integration with other apps in the platform: marble with the GPS -data, akonadi integration etc. So far my major reason for not using digiKam to its full capacity has been the problem, that its not se well integrated with the desktop.\n\nFor example, I have some 6,000 photos, for which digiKam is great editor, but I'd like integrate a hoard of non-photos with slightly different needs to the same system. And I want to tag them all, and I want to tag audiofiles and keynotes in the same system. Somehow I just don't see why I should manage my .mp3 and .xcf -files with digiKam. And if I have to tag these all, why use digiKam. It's a fine editor but lacking integration keeps me from using it more.\n\nI would think that from the developer's perspective using existing tools is a good idea, because then one can concentrate on the essential. From the user point of view being able to swicth from one tool - digiKam - that is great for one thing, to another, say Dolphin, Marble or Gwenview, which has different strenghts, is a motivation to use the stuff.\n\nAt the moment the issue seems to be that XMP is malformed RDF, and thus 1-10% of the XMP metadata is in danger of being lost in conversion to RDF. The sooner we get a uniform access to the metadata, the better. Therefore I really look forward to integrating all these great new digiKam features into the wider desktop, so that I can keep on using it when the stable KDE4 release comes out in the fall."
    author: "outolumo"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration"
    date: 2008-04-03
    body: "What I fail to get is why it is important that applications ship with KDE4.x.\n\naptitude install digikam \n\nshould work, no?"
    author: "andy"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration"
    date: 2008-04-03
    body: "Yes, but you lost the advertising of a full release. And worse, you can miss the release of digikam if you are not trakking the web or similar."
    author: "cruzki"
  - subject: "Re: Database? Metadata? Strigi/Nepomuk integration"
    date: 2008-04-03
    body: "apt-get update ??"
    author: "andy"
  - subject: "Few more screenshots"
    date: 2008-04-02
    body: "Few more screenshots of new Search interface by Marcel:\n\nhttp://digikam3rdparty.free.fr/Screenshots/NewSearchTools/"
    author: "m."
  - subject: "KDE 4.1? "
    date: 2008-04-02
    body: "Looking at the photos, I am very impressed on the work of digikam. \n\nWill Digikam make it into the release of KDE 4.1? They said September and I thought KDE 4.1 was due in 2 months but I could have my dates wrong. lol. \n\nAnd freaking great work, Digikam pops with the black color scheme.  "
    author: "Jeremy"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-02
    body: "Forgot to ask, but how is the integration with Nepomuk? I saw the five star ratings already and I'm excited. :D"
    author: "Jeremy"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-02
    body: "not done yet, the rating is internal to digiKam\nlook here for a response : http://dot.kde.org/1207150254/1207169696/  :)"
    author: "DanaKil"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-02
    body: "Neopumuk : see my previous comments about this subject.\n\nAlso, digiKam support rating since many year (:=))). We don't need neopumuk for that...\n\nNote : i sync KDE3 and KDE4 version when it's possible. 0.9.4 version will come before this summer with a lots of improvements like simpler entry of tags, or a new time-line tool. See my blog entry for details :\n\nhttp://www.digikam.org/?q=blog/3\n\nGilles Caulier\n"
    author: "Gilles Caulier"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-03
    body: "i vote for nepomuk integration too :-)\n\nwill be really cool if it recognizes people on photos...\n\nmany digikam from today can do this feature ! , would this be possible ?"
    author: "nice"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-05
    body: "I vote for Nepomuk, too. It'll make DigiKam smaller, and better integrated."
    author: "Riddle"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-05
    body: "x2! It would be a great addition!"
    author: "dk"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-02
    body: ">Will Digikam make it into the release of KDE 4.1?\n\nImpossible. There still too many regression tests to do. Sorry...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: KDE 4.1? "
    date: 2008-04-03
    body: "Meh, completely fine with me. I rather you finish the development then release it too early. I'm not a fan of the release early(when buggy) and release often(when should've been released). "
    author: "Jeremy"
  - subject: "image version control"
    date: 2008-04-03
    body: "The #1 thing I'd like to see in Digikam is some form of version control, that lets you go crazy and touch up your pictures while preserving, at least, the original picture. These issues were discussed at length here[1], but all that came out of that particular bug was a confirmation dialog when saving an image from the image editor.\n\nDon't laugh now, but this is the most important thing that is keeping me from migrating my parents to Linux. Here is the scenario, that would apply not just to parents but to any user lacking very strong self-discipline: They find a picture that they like, so they fix it up a little. Apply red eye. Crop it. Tweak the colors and contrast. Make it look better on the screen. That's what Digikam invites you to do. Now they decide that they *really* like the picture, so they ask me to come over and help them make the most of it for printing. And then it is ruined, because of the sloppy, lossy editing that they already did. And that's not their fault, imho, but that of the software that is too unforgiving.\n\n[1] http://bugs.kde.org/show_bug.cgi?id=103350"
    author: "Martin"
  - subject: "Re: image version control"
    date: 2008-04-03
    body: "That is planned as \"next big thing\". 0.10 has already so many great features that need testing, testing, testing. Versioning thing has to be rock stable and reliable, it will require much effort."
    author: "m."
  - subject: "RAW: the wrong way to do it"
    date: 2008-04-03
    body: "Honestly, if you guys need RAW users to jump onto digikam, then you need to learn why on earth one uses RAW. The way digikam works on RAW files is plain wrong. You are planning all this for the average shooter, but the average user doesn't use RAW, because he/she just doesn't need the overhead of work and diskspace.\n\nThose that do use RAW, honestly, don't do autowhitebalance. One ofthe main purposes of RAW is precisely being able to manually correct white balance when the camera's autowhitebalance goes wrong, being able to fix highlights and shadows, curves... without loosing quality \"thanks\" to the camera's extra processing.  \n\n\nWhen I see shots like this one: \nhttp://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma/1.png\n\nFirst, I read \"it's not bad!\". Well, it is bad, real bad. The left shots from digikam are washed out,flat, not contrasty at all. Lightzone one looks properly exposed and all lines are rather defined, contrasty.\n\nThen, I wonder why you had hidden the righthand toolbar from lightzone. I'd love to know how you are comparing results, ie, what you exactly did to them, why Lightzone gets it right and why showfoto didn't.\n\nSo the issue is only about accuracy? No.\nThen, what's wrong? digikam's workflow to process RAWs is all wrong, and anyone using RAW workflows should possibly know why. I give a couple hints in the following lines:\n\n1) Digikam opens the raw photo applying the _settings_ found n the digikam menu.\nIf you, like most photographers do, are processing 800 shots and each requires a different setting, you can't go and change the digikam settings in the menu each time you want to open a file, it's toooooo slow.\n\nYou can't just apply a default white balance and then set white balance _again_ in showfoto. You are doubly processing the image and thus loosing quality.\n\n2) When tweaking raw files, precission is also important. One needs to tweak images once and again until one gets it right... apply sharpness 1, 1.1, no, 0.9... if I wanted to do this in showfoto, it would require me to process->undo->process->undo->process->undo each time, and each time opening and closing the dialog from the menu. Really slow.\n\n3) It'd take me a whole month to process 100 shots using digikam/showfoto's procedure shown on the previous comment. The average raw shooter takes around 1000 shots for a session and needs to process those in a single day, not spend a whole month on it. it's about fast batch processing. If it were this slow with any other apps, people would end up using jpeg instead.\n\nSo honestly, if you want to do RAW, you guys need to learn what RAW is for, and how real users use it. Showfoto coders seem to ignore users here, unlike other opensource projects around.\n\nSometimes coders just forget that they need to approach their customers/users to check what they really neeed...\n\nMy 2 cents.\n\nu\n"
    author: "anon photographer"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: "Good thing to know though, that they are actually learning from other good projects like Lightzone"
    author: "anon photographer"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: "\"Then, what's wrong? digikam's workflow to process RAWs is all wrong, and anyone using RAW workflows should possibly know why. I give a couple hints in the following lines:\"\n\nI would like to see rawstudio ideas taken to DigiKam's RAW editor, where you can have thubbar (already) showing all images and then make a three different versions from one image and fast switch them to see if one is better than other. And let this window/application to mark those good versions and then apply it to images what are in database.\n\nI like the idea that first user just imports images to database, add's few info like albumb name, and then from database, is started the real RAW processing where user open all images and can set one setting for all or process everyone with own custom settings.\n\nMayby biggest \"flaw\" what i can find from digikam is it's editing window, possibilities are GREAT but own window for every edit function and preview window is small by default etc. Mayby i would do somekind mockup for it. If would be possible to get showfoto work so it's editing goes in it's own window, or is it somekind technical limit?"
    author: "Fri13"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: ">I would like to see rawstudio ideas taken to DigiKam's RAW editor, where you \n>can have thubbar (already) showing all images and then make a three different \n>versions from one image and fast switch them to see if one is better than \n>other. \n\nYes, the thumbnail bar is the common method used on all otheer RAW processors, including commercial ones like bibble pro (by Bibble labs), lightzone (by lightcraft), lightroom (by Adobe), Aperture (by Apple), rawtherapee ()...\n\nAlso common, even on the still-baby rawstudio, that you can copy paste settinsg from one photo to another leaving the raw file untouched, and without generating the final jpegs yet.\n\nBut this doesn't seem to be in their plans, given the structure they are following\n\n"
    author: "anon photographer"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: ">When I see shots like this one: \n> http://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma/1.png\n \n> First, I read \"it's not bad!\". Well, it is bad, real bad. The left shots from >digikam are washed out,flat, not contrasty at all. Lightzone one looks >properly exposed and all lines are rather defined, contrasty.\n\nIt's not too bad. I said again : \n\nhttp://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma/1-preview(JPEG).png\n\n==> the preview of JPEG image embeded in RAW file. No auto gamma/Wb are set here. The image is displayed as well.\n\nhttp://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma/1-editor(RAW).png\n\n==> the RAW image decoded in digiKam with 16 bits color depth + auto-WB + auto-gamma. It's look very similar no ???\n\n>So the issue is only about accuracy? No.\n> Then, what's wrong? digikam's workflow to process RAWs is all wrong, and >anyone using RAW workflows should possibly know why.\n\nCertainly no. There is 2 RAW workflow :\n\n1/ a fast way with all auto settings for less important pictures.\n2/ a slow way using color management where users can adjust all (gamma, WB, etc..)\n\nBoth are possible in digiKam !  \n\n>So honestly, if you want to do RAW, you guys need to learn what RAW is for, >and how real users use it. Showfoto coders seem to ignore users here, unlike >other opensource projects around.\n \n>Sometimes coders just forget that they need to approach their customers/users >to check what they really neeed...\n\nAnd you forget than i'm _also_ a photographer and i _always_ use RAW format to take pictures. Thanks for the tip, but i don't need to really read something like that...\n\nMy 3 cts!\n\nGilles Caulier\n"
    author: "Gilles Caulier"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: ">==> the preview of JPEG image embeded in RAW file. No auto gamma/Wb are set here. The image is displayed as well.\n\nokay, this was confusing. Certainly embedded jpeg files cannot be compared to a processed image.\n\n>2/ a slow way using color management where users can adjust all (gamma, WB, etc..)\n\nEven slower?? The process about processing RAW is slow by itself. The point is making it fast by applying a proper workflow. Btw, I tried that plugin, I hope you get it right, since it wasn't doing anything last time I gave it a test.\n\n>And you forget than i'm _also_ a photographer and i _always_ use RAW format \n>to take pictures.\n\nIf you are using RAW on a daily  basis, you sure noticed the way digikam works with RAW files isn't usable. I was questioning you guys on why you follow this way of coding. You know (since you use RAW often), that it's not comfortable. Just too slow for a daily usage. Right now there are much better free alternatives like rawtherapee, ufraw or rawstudio for this task.\n\nI myself even, and other coders (yes, I do code, and I have done code for  digikam), have addressed at you issues in the past and even offered code for doing a proper RAW workflow. But you seem to ignore all of us. Why so?\n\nIf you really want to promote RAW in digikam, maybe some tweaks must be done and help from others offered in the past accepted.\n\n\n"
    author: "anon photographer"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: ">If you are using RAW on a daily basis, you sure noticed the way digikam works >with RAW files isn't usable\n\nIt perhaps unsuitable for you, not for me...\n\n> I myself even, and other coders (yes, I do code, and I have done code for >digikam), have addressed at you issues in the past and even offered code for >doing a proper RAW workflow. But you seem to ignore all of us. Why so?\n\nReally. I don't remember somebody like you to post patch or any coding help. Try again...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: "> Certainly no. There is 2 RAW workflow :\n\nThere a more of them in reality :)\n\n> 1/ a fast way with all auto settings for less important pictures.\n\nYes\n\n> 2/ a slow way using color management where users can adjust all (gamma, WB, etc..)\n\nUsing what? Gilles, for heaven's sake please tell me you did mean something else :) Terminology is the key :)\n\nThough I don't buy into the previous guy's harshness regarding workflow, I tend to agree that digikam basically lacks proper workflow.\n\n- can digikam load >2 pictures simultaneously on light table? (I do remember, you said it takes too much CPU/memory or whatever :))\n\n- can digikam generate cache (full-size previews) so that a picture would not load every time from a RAW file?\n\n- can digikam copy/paste processing parameters?\n\n- does digikam allow non-destructive cropping/straightening/WB/etc. with batch export?\n\nWell, I'd say - \"no\" to all of the above. This is a pity.\n\nWhat is even more pity is that digikam relies on the aged model of dialogs whereas everybody (and rightfully so) is moving to sidebars with expanders. Cropping via dialog is so 90s! :)\n\nSeriously, digikam has most of pieces in place to become a great RAW workflow product. It just needs better UI + some functionality.\n\nCurrently I'm bound to use Rawstudio for sorting, UFRaw for actual processing and F-Spot for managing the library. This is a nightmare. This should be stopped.\n\nP.S. Please do something to make cyrillic XMP tags work properly ;-)\n\nP.P.S. I hope to see you at LGM3 ;-)"
    author: "Alexandre"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: "> P.P.S. I hope to see you at LGM3 ;-)\n\nNo. I have no money to pay travel + hotel from France to Polish. Sorry\n\nGilles Caulier\n"
    author: "Gilles Caulier"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: "Have you asked KDE e.V. for sponsorship? And if the e.V. board declined (and if so, I really want to know why!), the LGM organizers themselves? They are now holding a big donation drive to be able to sponsor as man developers as possible: http://pledgie.com/campaigns/613."
    author: "Boudewijn Rempt"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-06
    body: "Boudewijn,\n\nI have sent an email saturday to KDE e.V about this subject. No response yet...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-04
    body: "If it just the money, please ask the KDE e.V., Boudewijn Rempt (Krita) is sponsored by the e.V. too."
    author: "Tobias"
  - subject: "Re: RAW: the wrong way to do it"
    date: 2008-04-03
    body: "> P.S. Please do something to make cyrillic XMP tags work properly ;-)\n\nXMP != IPTC. XMP use native UTF-8 encoding. There is no problem with Cyrillic char normally...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Confused? - Amarok??"
    date: 2008-04-03
    body: "I'm confused.\n\nI thought Amarok was rewritten from scratch for version 2.0.\n\nWasn't it?\n\nPlease clear it up. Maybe even a separate Amarok update post. I'd rather see false rumors floating around Amarok.\nA lot, and I mean A LOT of people are waiting and looking forward to this great change that is Amarok 2.0.\n\nQuite a few people on campus are predicting that it will be BIGGER THAN WINAMP used to be before AOL let Winamp fall by the wayside.\n\nFinally something that will blow iTunes out of the water..."
    author: "Max"
  - subject: "Re: Confused? - Amarok??"
    date: 2008-04-03
    body: "I meant \"I rather NOT see false rumors floating around.\"\n\nGosh, I'm tired. bedtime..\n\n\n"
    author: "Max"
  - subject: "gallery plugin"
    date: 2008-04-03
    body: "what are the plans on gallery integration plugin ?\nwhat i would ideally like to see, a complete sync of digikam/gallery, so any changes in digikam can be synced to gallery instance, any comments in gallery can be synced to digikam etc ;)"
    author: "richlv"
  - subject: "Re: gallery plugin"
    date: 2008-04-03
    body: "Hi,\n\nWell, I've had a hard time of it of late and there has been little progress. I apologise for that.\n\nIt's still somewhat in flux to be honest.\n\nWith KDE4 there are so many new techniques and technologies that I'm really struggling to work out the most appropriate way to do this.\n\nI've raised a couple of questions to other developers in the past about the use of such techniques but I've not had a huge response (not that I've really pushed it hard as I have not got the time right now to do much about it anyway).\n\nMy current lines of thinking are such.\n\n\n1) I want a nice sync framework that goes beyond Gallery and applies to other web services e.g. Facebook, Flickr etc. etc. as well as e.g. a file or a CD ROM for achieve. The principle is in itself applicable to a lot of things.\n\n2) Other approaches are out there and I want to make sure myself (or others) pick the right model for this with regards to Digikam/KIPI and (more widely) KDE4. A good example of this is Conduit. It is a separate program aimed mostly at Gnome (although there is some vestiges of a Konduit project, it's pretty hard to find out any info about). It integrates to libopensync and can upload FSpot pictures to e.g. Flickr and Facebook etc. I quite like this approach and it may be worth doing.\n\n3) Nepomuk. To me this is the biggest interest but I'm not fully appreciative about what is is capable of at present, but I have a \"feeling\" this is the right way to go. However, this is a *huge* change IMO and not one that is likely to happen overnight. To explain:\n\nNepomuk is a PIM (personal information management) abstraction in KDE4. It's designed such that you can build e.g. contact systems and email clients on top of Nepomuk and it makes your personal data available via predefined interfaces. This allows lots of interesting things to be done in applications and with e.g. plasmoids etc. too. I attended the Nepomuk talk at Akademy and was quite impressed by the principles. The talk focused on the traditional PIM information like contacts and emails. I asked a question enquiring where the developers saw the boundaries of this API as it could be applied to applications such as Amarok or Digikam. They devs responsed saying that indeed this was possible, but they've not targetted these apps at the moment as it was early days. I'm pleased to say that the Amarok crowd now have a GSoC project that aims to build in Nepomuk support there so things are happening along the lines I envisaged (hopefully this project will be a success!)\n\nIn my mind we could create a Nepomuk profile for \"Images and Videos\". We could then impelment a Filesystem backend for this and retarget Digikam to use this. In theory Digikam could then have \"accounts\" (just like an email application has multiple IMAP accounts) and you could plug in a Nepomuk backend for Facebook, Gallery etc. etc. This would be IMO the holy grail implementation and it's this dream that's (in part) causing me not to progress with the work done to date.\n\nHopefully I'll find time to explore the options properly with the main devs, but this approach changes a lot of the fundamentals of Digikam and KIPI as a whole and I feel I'm certainly not qualified or capable (both technically and time wise) to push this approach.\n\nHopefully some of the core devs will take up some of the ideas and either implement them or blow them clean out of the water. Either way it would let me move on!!\n\nCheers\n\nCol"
    author: "Colin Guthrie"
  - subject: "Re: gallery plugin"
    date: 2008-04-03
    body: "I have to reply to correct myself.\n\nHaving been \"out of the loop\" for a while I've talked myself into misremembering project names etc.\n\nThe project I saw at Akadamy that I thought would be interesting for DigiKam/Amarok is actually Akonadi:\nhttp://akademy2007.kde.org/conference/talks/55.php\n\nI'm totally confused now, as I have posted several messages with incorrect premises relating to nepomuk when it's not relevant to what I had in mind!!!\n\nDamn all these silly project names and damn my crap memory.... \n\nWill have to reread things soon to see where it's at!\n\nCol"
    author: "Colin Guthrie"
  - subject: "16bit? Heh"
    date: 2008-04-03
    body: "> Digikam is also the first open source photography tool with 16-bit colour depth support\n\nThis is simply not true. You guys forgot Cinepaint."
    author: "Alexandre"
  - subject: "Re: 16bit? Heh"
    date: 2008-04-03
    body: "16 bits color depth is already supported since 0.9.0 stable release, not only current one !\n\n0.9.0 have been released at 2006-18-12...\n\nGilles Caulier\n\n"
    author: "Gilles Caulier"
  - subject: "Re: 16bit? Heh"
    date: 2008-04-04
    body: "Gilles, I do know it's been around since 0.9.0. I even wrote a thorough illustrated review of that version in RU :)\n\nBut Cinepaint is around for a quite longer time ;-)"
    author: "Alexandre"
  - subject: "Re: 16bit? Heh"
    date: 2008-04-04
    body: "And Krita (with 1.5.0, 11-4-2006). But digikam still rocks!"
    author: "Boudewijn Rempt"
  - subject: "Re: 16bit? Heh"
    date: 2008-04-07
    body: "True :)"
    author: "Alexandre"
  - subject: "Sidebars"
    date: 2008-04-04
    body: "Thanks very much for this - looks much more powerful than f-spot! Just wondering, is it possible to replace the sidebars with the weird text rotated 90 degrees with something better, like an okular-style sidebar? Other than that, the new interface is fantastic!\n\nThanks,\n\nParm"
    author: "Parminder Ramesh"
  - subject: "scaling hell"
    date: 2008-04-08
    body: "Is it now possible to scale a small image to fit the screen (with anti-aliasing) by default? The gwenview guys seem to not know how this is done and you have to zoom in yourself at every image (very annoying considering collection of >10000 pics) and even so, only at fixed intervals (150%, 200%). THIS IS UNACCEPTABLE. You can add all the maps in the world and still not achieve your core functionality of displaying the damn inages WELL."
    author: "pisa"
  - subject: "Re: scaling hell"
    date: 2008-04-08
    body: "First, this article is about Digikam, not Gwenview.\n\nSecond, the tone of your comment is unacceptable. Nobody will want to spend their free time implementing things you shout for, which means you will have to offer your own time to implement it. And, given your incivility, you will then have a hard time finding people prepared to interact with you enough to accept your patch."
    author: "Boudewijn Rempt"
  - subject: "Re: scaling hell"
    date: 2008-04-08
    body: "Are you always this rude to people who give you things for free? Seriously, what personality disorder do you suffer from, precisely, that makes you think this is a) acceptable behaviour, full stop and b) behaviour likely to endear you to the developers you are entreating?\n"
    author: "Anon"
  - subject: "Re: scaling hell"
    date: 2008-04-08
    body: "TO DOT.KDE.ORG administrator.\n\nWe _working_ for Open-Source here. Has lead digiKam developper, i think it's UNACCEPTABLE to read an hurting message like this in this thread!\n\n==> DOT.KDE.ORG need a moderator !!!\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: scaling hell"
    date: 2008-04-08
    body: "Your messages are reading like LOLCATs. Maybe LOLCODE is for you.\nhttp://lolcode.com/\n\nCAN HAS A BOOK TO LEARN HOW TO SPELL ENGLISH ? KTHX"
    author: "Holyshit"
  - subject: "Re: scaling hell"
    date: 2008-04-08
    body: "Please go away and leave the KDE developers alone. \n"
    author: "cm"
  - subject: "Re: scaling hell"
    date: 2008-04-14
    body: "Illiterate developers are illiterate. News at 11."
    author: "Holyshit"
---
Readers of the KDE <a href="http://commit-digest.org/">Commit-Digest</a> have probably noticed that Gilles Caulier is <a href="http://commit-digest.org/issues/2008-03-23/statistics/">again on top with number of commits</a>. On what does he work so furiously? Gilles is the main developer of <a href="http://www.digikam.org">Digikam</a> which is under transition from KDE 3 to KDE 4 whilst simultaneously adding new features. Read more to know more about the future of advanced digital photo management for KDE and Linux.
<!--break-->
<p><em>There are many improvements including a cleaner user interface, improved performance, a new thumbnail bar, XMP support, ability to run on Mac OS X, GPS tagging using Google Maps, multiple album collections supporting collections on network shares and removable media, and auto gamma and white balance with RAW. Digikam is also the first open source photography tool with 16-bit colour depth support. Gilles Caulier tells all.</em></p>

<p>We have plans to release 0.10.0 in September, if all goes well.</p>

<p>Regression tests will take a while. I would not provide a "stable"
release full of bugs.</p>

<p>As you can see, porting of Qt 3/KDE 3 application like Digikam, K3B or
Amarok to Qt 4/KDE 4 is not a simple task. Because changes in the API are huge, all elements must be tested, and some parts even re-written. The advantage of this approach is review of all old code with refactorisation,
simplification and various other improvements.</p>

<p>Digikam 0.10 is still in alpha. I advise against use of it in
production, at least until the first release candidate.</p>

<p>There are also <a href="http://www.kipi-plugins.org">kipi-plugins</a>
and all shared libraries where Marcel and me are working hard to port to
native QT 4/KDE 4: </p>

<ul>
<li>libkdcraw (including new 16 bits color depth auto-gamma/auto white
balance)</li>
<li>libexiv2 (including new full XMP metadata support)</li>
<li>libkipi (partially re-written and cleaned).</li>
<li>7 kipi-plugins fully ported by me (SendImages, RAWConverter,
JPEGLossLess, FlashExport, TimeAdjust, MetadataEdit, and
AcquireImage).</li>
</ul>

<p>I present here a few screenshots of Digikam for KDE 4 in action.</p>

<a href="http://static.kdenews.org/jr/digikam/newsearchwindow.jpg"><img src="http://static.kdenews.org/jr/digikam/newsearchwindow-wee.jpg" width="500" height="313" /></a>

<p>As you can see, a lot of work has been done to design a clean
interface, moving away from the traditional dialog requiring to build a search
step-by-step with comboboxes, and towards a page which offers the available
options at a glance, at the same time retaining the ability to create
complex searches. With this approach we hope to create something more
user friendly. It is not finished yet, the UI in the screenshot can be
considered a preview, but we want to finalise this interface for the Beta 1 release.</p>

<p>The need to redesign the search interface came with the rewrite and
improvement of our database schema. Now, much more photo metadata
information will be stored and will be available for searching. For
example, GPS information will now be stored in the database, which means
that read-only files (think of your RAW images) can as well be
geo-coded.</p>

<a href="http://static.kdenews.org/jr/digikam/thumbbarwithpreviewmode.jpg"><img src="http://static.kdenews.org/jr/digikam/thumbbarwithpreviewmode-wee.jpg" width="500" height="125" /></a>
<br />
<a href="http://static.kdenews.org/jr/digikam/thumbbarineditor.jpg"><img src="http://static.kdenews.org/jr/digikam/thumbbarineditor-wee.jpg" width="500" height="125" /></a>

<p>In Digikam for KDE 4, thumbnail kio-slave disappears. All thumbnail images are now generated using multi-threading (another part implemented by Marcel)
If you have already played with Showfoto, you have certainly seen
a thumbbar. The KDE 3 implementation used kioslaves without a memory cache
mechanism. It was slow. The new mechanism is faster and can be included everywhere in Digikam without a decrease in performance. I have
included the thumbbar in the Image Editor (F4) and in the AlbumGUI with Preview mode (F3). The same cache is used everywhere.</p>

<a href="http://static.kdenews.org/jr/digikam/digikam_for_KDE4_with_XMP_metadata_support.jpg"><img src="http://static.kdenews.org/jr/digikam/digikam_for_KDE4_with_XMP_metadata_support-wee.jpg" width="500" height="125" /></a>
<br />
<a href="http://digikam3rdparty.free.fr/Screenshots/MetadataEditor/"><img src="http://static.kdenews.org/jr/digikam/MetadataEditor-01-wee.jpg" width="500" height="352" /></a>

<p>In KDE 4, XMP is fully supported. I have improved the Metadata Editor
kipi-plugin and rewritten all dialog pages to be more user friendly, and
to be more similar to other tools available under Mac OS X or Windows.</p>

<a href="http://static.kdenews.org/jr/digikam/digikamKDE4_under_MACOSX.jpg"><img src="http://static.kdenews.org/jr/digikam/digikamKDE4_under_MACOSX-wee.jpg" width="500" height="328" /></a>

<p>Gustavo Boiko, have ported some parts of Digikam to compile as a native
application under Mac OS X. </p>

<a href="http://static.kdenews.org/jr/digikam/newkipiimagecollectionselectorwidgetKDE4.jpg"><img src="http://static.kdenews.org/jr/digikam/newkipiimagecollectionselectorwidgetKDE4-wee.jpg" width="500" height="200" /></a>

<p>For KDE 4, I have fixed libkipi to become a pure image collection
interface: no widgets, no dialogs, no translations. All GUI components
have to be re-implemented in kipi-host using the right model/view
implementation. For Digikam, I have already implemented all of them. The
advantage is really visible here: the tree view used for all
physical/virtual albums in Digikam can be used as well with all
kipi-plugins (KDE 3 only provide a flat albums list, which is not really
suitable). </p>

<a href="http://static.kdenews.org/jr/digikam/gpstracklisteditor.jpg"><img src="http://static.kdenews.org/jr/digikam/gpstracklisteditor-wee.jpg" width="500" height="200" /></a>

<p>For KDE 4, I have also implemented a new tool to edit a GPS track list of
several images at the same time. This tool uses Google Maps, but there are plans to use Marble if necessary, especially when the user does not have
network access.</p>

<a href="http://static.kdenews.org/jr/digikam/digikamKDE4_15.jpg"><img src="http://static.kdenews.org/jr/digikam/digikamKDE4_15-wee.jpg" width="500" height="125" /></a>

<p>With KDE 4, multiple root album paths are supported, including
removable media and network repositories. There will be no problems with using Digikam with a NFS server to host your images.</p>

<a href="http://static.kdenews.org/jr/digikam/digikam0.10.0.jpg"><img src="http://static.kdenews.org/jr/digikam/digikam0.10.0-wee.jpg" width="500" height="400" /></a>
<br />
<a href="http://static.kdenews.org/jr/digikam/fullcolortheme6.jpg"><img src="http://static.kdenews.org/jr/digikam/fullcolortheme6-wee.jpg" width="500" height="125" /></a>

<p>Full colour theme interface: this was also backported to KDE 3. Now
colour schemes are applied everywhere within the GUI. With dark themes (my
preferred scheme), Digikam looks similar to professional software available commercially.</p>

<a href="http://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma"><img src="http://static.kdenews.org/jr/digikam/RAW16bitsAutogamma-2-wee.jpg" width="500" height="125" /></a>

<p>This is a very important feature: auto-gamma and auto-white balance
with all RAW file format using 16-bit colour depth! Before, 16-bit
colour depth support required use of colour management to have a suitable
image in the editor. Without colour management, the user would see only a black hole image. This is due to limitations of the dcraw library, which does not provide a homogenous interface between 8-bit and 16-bit colour depth workflow.</p>

<p>In these screenshots, you can compare the same RAW image decoded:</p>

<ul>
<li>On the left by dcraw in 8 bits colour depth and converted to PNG.
Auto-gamma and auto white balance is performed automatically by
dcraw.</li>
<li>On the middle by Digikam in 16 bits colour depth using libkdcraw with
a dedicated auto-gamma and auto-white-balance performed in Digikam
core.</li>
<li>On the right by the LightZone... As you can see, Digikam is
not bad!</li>
</ul>

<p>Digikam is now able to play with all RAW images in 16-bit colour depth
without to use complex colour management settings: in your workflow RAW
pictures can be handled like JPEG files. This is a very productive and fast
method: for example, LightZone uses this style of workflow. Note that this feature was also backported to KDE 3.</p>

<p>I would like to remind to everyone that Digikam has been the first suitable
open source photo-management program with support for 16-bit colour depth
pictures as well! Even GIMP or F-Spot do not support it! Cinepaint can do it, but seriously, who will use it to play with pictures? Of course we have
Krita to also work with layers. We want Digikam with Krita to be considered the perfect photo suite.</p>

<p>On a final note, we would like Digikam to take part in Google Summer of Code: here is a list of potential <a href="http://www.digikam.org/?q=node/305">projects</a>.</p>