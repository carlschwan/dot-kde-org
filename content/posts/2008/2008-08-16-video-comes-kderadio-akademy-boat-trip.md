---
title: "Video Comes to KDE://Radio from the Akademy Boat Trip"
date:    2008-08-16
authors:
  - "jriddell"
slug:    video-comes-kderadio-akademy-boat-trip
comments:
  - subject: "Ack"
    date: 2008-08-16
    body: "Ook ook. Ook! Ook ook ook ooka-ook."
    author: "Team Humongous"
  - subject: "Re: Ack"
    date: 2008-08-18
    body: "The video of this example of perfect Flemmish is now available.  054_ook_ook.ogv\n"
    author: "Jonathan Riddell"
  - subject: "radio.kde.org?"
    date: 2008-08-16
    body: "Why did I never hear about that site before? As I see from the archives, it has been inactive since a long time."
    author: "Stefan Majewsky"
  - subject: "Chani and Aaron"
    date: 2008-08-16
    body: "Chani and Aaron look completely drunk on that video lol"
    author: "Lisz"
  - subject: "Re: Chani and Aaron"
    date: 2008-08-18
    body: "well, we were allowed 6 free drinks, and my alcohol tolerance seems to get lower every year...\n\n...oh god, I just watched the video. I *do* look drunk. ohhh man. and wtf is that accent? a week in europe and I'm reverting to some strange british-like accent already? lol.\n\nand that's going to be on the interwebs forevar now. oh dear. :)"
    author: "Chani"
  - subject: "Good to put names and faces together"
    date: 2008-08-18
    body: "Chani is the living proof that cute girls and computer science are not incompatible."
    author: "Cypher"
---
After a hard week's discussion and hacking the Akademy attendees took a relaxing trip along the river Dijle towards Antwerp.  The KDE discussion continued of course but most of the KDE contributors took the chance to rest from hacking and enjoy the countryside of Flanders.  <a href="http://radio.kde.org/">KDE://Radio</a> has been updated with some short video interviews which will introduce you to your friendly KDE contributors.




<!--break-->
<p>Besides drinks, we had great food too. A couple of the more sensitive types got sunburned, others so overly happy we had a large group hug when we got back to the shore. All in all it was great to be together without laptops for a while, although the KDE Games group did a BOF anyway.</p>

<p>After the group hug the KDE developers spread over Mechelen, looking for fun and entertainment. Some went back to the hostel, where they could get back to writing code.

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px;">
<img src="http://static.kdenews.org/jr/akademy-2008-boat-trip-wee.jpg" height="267" width="400" />
</div>



