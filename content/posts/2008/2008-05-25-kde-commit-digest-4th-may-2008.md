---
title: "KDE Commit-Digest for 4th May 2008"
date:    2008-05-25
authors:
  - "dallen"
slug:    kde-commit-digest-4th-may-2008
comments:
  - subject: "thanx!"
    date: 2008-05-25
    body: "Thank you Danny for your dedication! It's appreciated much more than you could imagine!"
    author: "kmare"
  - subject: "Re: thanx!"
    date: 2008-05-25
    body: "+1 "
    author: "mimoune djouallah"
  - subject: "Re: thanx!"
    date: 2008-05-25
    body: "Thank you Danny, and thank you all kde devs :)"
    author: "IAnjo"
  - subject: "Re: thanx!"
    date: 2008-05-26
    body: "+1"
    author: "Riddle"
  - subject: "Re: thanx!"
    date: 2008-05-26
    body: "Thanks Danny, I didn't expect an update so quick.  Thanks for your time and dedication you put in to write the digest."
    author: "spawn57"
  - subject: "KTorrent - wow!"
    date: 2008-05-25
    body: "The plans for KTorrent sound very impressive indeed! Nice to see a technology like Nepomuk being adopted and integrated so thoroughly and ubiquitously.  "
    author: "Anon"
  - subject: "Re: Metainfo to fresh Files from legacy Systems"
    date: 2008-05-25
    body: "i think all entry points of files to the system need to have some sort of nepomuk integration. When this is the case (Digikam/Ktorrent) then all incoming Files will have nice Tags and Meta Infos - but:\n\nthe most important Interface where Files come to the System does not have such an Integration, this is the Copy/Move to Files Dialogs ....\n\ni dont know how this could be done, but if we get proper tagging there, then the system will have rich meta information ....\n\nwhat do you think?"
    author: "ennel"
  - subject: "Re: Metainfo to fresh Files from legacy Systems"
    date: 2008-05-26
    body: "I think that an ACL aproach here is a good solution. I mean, you \"tag\" certain directories with some metadata and every file that was copied there \"inherit\" this tags. And you can have a tree structure of metadata and things like that"
    author: "cruzki"
  - subject: "Re: Metainfo to fresh Files from legacy Systems"
    date: 2008-05-28
    body: "Doesn't Strigi automatically add metadata? If it automatically adds MD, then it should add it automatically without any code for copying.\n"
    author: "Riddle"
  - subject: "Re: KTorrent - wow!"
    date: 2008-05-25
    body: "I have to agree, though I am concerned about the media player integration. I always lived well with the 3.x version calling an external media player of my choice. I wonder if this would be unneccessary code duplication..."
    author: "Sebastian"
  - subject: "Re: KTorrent - wow!"
    date: 2008-05-25
    body: "Since it's a plugin, I'm guessing that the \"external media player\" is still an option.  \n\nGiven how easy to incorporate Phonon is, the code duplication is likely to be miniscule."
    author: "Anon"
  - subject: "Re: KTorrent - wow!"
    date: 2008-05-25
    body: "it's not code duplication because it is using Phonon which allows to write a media player in a few lines of code."
    author: "Patcito"
  - subject: "Re: KTorrent - wow!"
    date: 2008-05-26
    body: "Are you sure about that?  From what I've seen, phonon provides a very basic player window, which you then have to add all of the play/stop/pause/etc. widgets to.  Does it provide a complete player widget now, akin to a media player in VB or simply embedding a kpart?  Otherwise, it would be quite a lot of work to make phonon actually usable in ktorrent."
    author: "Lee"
  - subject: "Re: KTorrent - wow!"
    date: 2008-05-26
    body: "One of the principal goals of phonon was to give multimedia features (such as audio video playback) to any KDE or Qt application in just a few lines of code, there is even an example code of how to write a player in the phonon code. So using it this way is perfectly ok."
    author: "Patcito"
  - subject: "Re: KTorrent - wow!"
    date: 2008-05-26
    body: "Joris Guisson rocks!\nI just thought \"wow\" when I reported two bugs against ktorrent end he fixed them within one or two days =)\nAnd the open bug list is one of the shorter one I have seen in kde: 11 in this moment.\nA big big thank Joris!"
    author: "Matteo Nardi"
  - subject: "KParts?"
    date: 2008-05-25
    body: "Wasnt kparts there to avoid such duplications? i think 'dragon-part inside ktorrent' looks much better (Dragon part or any available kpart for video which is configured via System Settings->File Associations)\nCorrect me if im wrong, please.\n\n(Thanks Danny, you rock)"
    author: "Emil Sedgh"
  - subject: "Re: KParts?"
    date: 2008-05-25
    body: "And before kparts existed we had libraries to avoid \"duplication\" ! Embedding a kpart or using phonon requires the same amount of code, and while phonon is very likely to be installed on kde4 system, it's not the case for 'dragon-part' or any other video kpart."
    author: "Cyrille Berger"
  - subject: "Re: KParts?"
    date: 2008-05-25
    body: "The duplication is not only about the amount of code.its about features, bugs, maintaining, etc.\nIf ktorrent uses kpart, then it will get all features of tho loaded kpart automatically.it will share features, bugfixes and others.\nAlso the good thing is that with kparts, the user could probably choice weather he wants a simple player like dragon to be inside ktorrent, or an advanced video player like (the future kde4 version of) kaffeine.\n\nbtw im just a user.i have no idea how is that possible."
    author: "Emil Sedgh"
  - subject: "Re: KParts?"
    date: 2008-05-25
    body: "> btw im just a user.i have no idea how is that possible.\n\nIn which case please express your questions or requests in terms of the things that you do understand such as the usability of the user interface, your needs as a user of the software and the available features, perceived performance or perceived stability.\n\nIt is frustrating for developers to read comments from users arguing about the technical implementation because it distracts from getting really useful information from them - how they expect/will/want to use the program.   \n\nHence if I create feature requests about another KDE program, as a user, I always try to focus on what I would like the program to do from my perspective as a 'naive' user and omit any technical details - even if I have read the code and have some idea of how it might be implemented.\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: KParts?"
    date: 2008-05-25
    body: "ok, seems that you are right ;)"
    author: "Emil Sedgh"
  - subject: "Re: KParts?"
    date: 2008-05-26
    body: "KParts provide the UI, whereas Phonon does not provide one.\n"
    author: "Riddle"
  - subject: "Re: KParts?"
    date: 2008-05-26
    body: "Phonon code is pretty trivial, so it doesn't much matter."
    author: "Ian Monroe"
  - subject: "What does the NetworkManager Plasmoid look like?"
    date: 2008-05-25
    body: "In particular, I'm interested in how the \"wifi map\" appears.\n\nAlso is KDE 4.1 Beta1 still on track for a May 27th release?"
    author: "cb"
  - subject: "Re: What does the NetworkManager Plasmoid look lik"
    date: 2008-05-26
    body: "About the Beta release: It is already tagged (http://websvn.kde.org/tags/KDE/4.0.80/) so I'm sure they'll can stick to the timetable"
    author: "Michael"
  - subject: "go vi mode!"
    date: 2008-05-25
    body: "Nice to see so much stuff happening this summer :)\n\nThe kate vi-mode screenshot looks extremely promising, I really can't wait for it!"
    author: "fhd"
  - subject: "KDE devs *rock*"
    date: 2008-05-26
    body: "You guys are the best! KDE is the best!\n\nThe only thing I have a problem with is:\nI just wish more companies could get their heads out of MS's a## and MS's hands out of their pockets."
    author: "Winter"
  - subject: "Palapeli"
    date: 2008-05-26
    body: "Nice to see that Palapeli is noticed in the commit digest. Expect a detailed status update in one of the next digests."
    author: "Stefan Majewsky"
  - subject: "KWallet's rename?"
    date: 2008-05-27
    body: "Minor thing,\n\nCould whoever takes over KWallet please give it a better name, like Keyring (like Gnome) or Keychain (like Apple)? If we have a good word that has K at the start, why make an artificial KName?\n\nThanks, peace."
    author: "KNo KMore KNames please?"
  - subject: "Re: KWallet's rename?"
    date: 2008-05-28
    body: "What's wrong with KWallet? It is a name that is blatently obvious as to its function. We could call it Kollaps or something, but why confuse people?\n\nAnd yes, people did take over KWallet."
    author: "What?"
  - subject: "Re: KWallet's rename?"
    date: 2008-05-28
    body: "KWallet is an artificial name. KeyPouch/Keyring/Keychain is also blatantly obvious as to its function, and has the added benefit of being a real word.\n\nAlso, good to see maintainers were found for this great feature :)"
    author: "KNo KMore"
  - subject: "Re: KWallet's rename?"
    date: 2008-05-28
    body: "IMHO KWallet shouldn't just be renamed, but rather be replaced by keyring (or a successor). :-)"
    author: "nf2"
  - subject: "Hoping to read the digest...."
    date: 2008-06-04
    body: "I'm getting the following error:\n\nWarning: main(/var/network/www/docs/commit-digest.org/index.inc): failed to open stream: No such file or directory in /var/www/commit-digest/issues/2008-05-04/index.php on line 1\n \n Fatal error: main(): Failed opening required '/var/network/www/docs/commit-digest.org/index.inc' (include_path='.:/usr/share/php:/usr/share/php4') in /var/www/commit-digest/issues/2008-05-04/index.php on line 1\n\nCan anyone help figure out what's going on?"
    author: "Anon"
---
In <a href="http://commit-digest.org/issues/2008-05-04/">this week's KDE Commit-Digest</a>: Work on form factor considerations and various applets in <a href="http://plasma.kde.org/">Plasma</a>, with added functionality in the NetworkManager Plasmoid. Work and interface fixes, and support for the CMake cache in <a href="http://www.kdevelop.org/">KDevelop</a>. Spellchecking in <a href="http://edu.kde.org/parley/">Parley</a>. Work on loading and saving games in Palapeli. Integration of GetHotNewStuff into KGoldRunner. An "Update Manager" for Kst. Xesam API makes <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>-related searching more available in KDE applications. Initial implementation of open/read/write/seek/close in the experimental KIO-GIO bridge. Tweaks to tab interactions in Konsole and Konversation. An implementation of a SQLite-based storage for <a href="http://kontact.kde.org/kmail/">KMail</a> indices. Akonadi calendar resources can now be configured using a KControl module, and an initial version of an Akonadi RSS resource. Some new icons in <a href="http://www.digikam.org/">Digikam</a> and <a href="http://ktorrent.org/">KTorrent</a>. Work on the media player and BitFinder plugins for KTorrent. Work on font handling details in <a href="http://koffice.org/">KOffice</a>, with extended work on charting (including scripting support) in the <a href="http://www.kexi-project.org/">Kexi</a> report generator. KAppTemplate and various Plasma applets move to kdereview, krossjava moves to kdebindings. Initial import of <a href="http://commit-digest.org/issues/2008-04-27/">KidDraw</a> and kde4powersave into KDE SVN. KDE 4.0.4 is tagged for release. <a href="http://commit-digest.org/issues/2008-05-04/">Read the rest of the Digest here</a>.

<!--break-->
