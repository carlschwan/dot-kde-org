---
title: "KDE Commit-Digest for 3rd February 2008"
date:    2008-02-09
authors:
  - "dallen"
slug:    kde-commit-digest-3rd-february-2008
comments:
  - subject: "Nepomuk use case"
    date: 2008-02-09
    body: "I guess we're not too far away from the \"search all pictures received from user X and tagged funny\" or \"select all pdf downloaded from website Y\" (that one would need some work from kget or khtml I guess) ?\n\nVery cool anyway."
    author: "Patcito"
  - subject: "Re: Nepomuk use case"
    date: 2008-02-09
    body: "The ability to have smart folders in dolphin would also be cool.\nSmart Folders are folders which contain links to files of a certain search query eg. all *.odp files. Its really handy and with nepomuk there would be many catehories to search through.\n"
    author: "anonimouse"
  - subject: "Re: Nepomuk use case"
    date: 2008-02-09
    body: "Nepomuk has been HUGELY undersold, imho.  It's based on RDF ontologies (ie, a very simple, but flexible and powerful database of knowledge), which essentially makes it the core of an AI engine.  Theoretically, it should be possible to store ANY knowledge in it, and to ask any question.  Some complex questions might not be answered as efficiently as others, as they'd resemble complex SQL queries.  However, given that other systems might not do what you want at all, the imperfect solution is still better than lack of a solution.\n"
    author: "Lee"
  - subject: "Re: Nepomuk use case"
    date: 2008-02-09
    body: "> Nepomuk has been HUGELY undersold, imho.\n\nI think one important lesson from development leading up to KDE 4.0 is that it is best to keep mum until there is something cool to show. \n\nSure, the semantic desktop does have a lot of potential, but right now, as far as the end user is concerned, it gives them simple tagging and rating of files in Dolphin.\n\nPitching to the developers is a different story.  You want to get them fired up about the potential of a particular project and encourage them to use it to improve their own software.  The difficulty is making sure that a pitch to developers or potential developers doesn't land on the front page of Slashdot as a promise that KDE version X will do Y.\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Nepomuk use case"
    date: 2008-02-10
    body: "Yep, agreed.  It's a tough balance to find.  Nepomuk's APIs have a really nice simplicity to them though, so I'm confident that development will catch up soon enough :)  One thing that would really help though, is better documentation.  The examples on techbase are great, but there aren't many of them, and they're fairly limited in terms of use cases.  I haven't yet seen any examples of soprano/nepomuk use from python/ruby/kross -- don't even know if any of those bindings are implemented yet."
    author: "Lee"
  - subject: "Re: Nepomuk use case"
    date: 2008-02-09
    body: "Is there some thinking on how to migrate NEPOMUK data?\n\nData that is automatically scraped from files or email messages is one thing; you can just redo the scaping. But the other type of data is that which cannot be recreated if the database is lost; for instance user entered tags, and the history of files (where they came from).\n\nFor me at least, I would neither manually enter any tags if they will be lost when I switch machines, nor would I make myself dependent on such metadata that cannot be synchronized or at least migrated between machines. Also, I would like control over the synchronization/migration process; I might not want the metadata for my private stuff on my work machine.\n\nAny plan for this? Of course, a prerequisite is that files etc. are primarily identified by checksums rather than paths, which I am assuming to be the case?\n\nThanks in advance for any info!"
    author: "Martin"
  - subject: "Re: Nepomuk use case"
    date: 2008-02-10
    body: "There's a command line tool, sopranocmd, which you can query for anything related to a particular file.  Tools like star (a modern tar replacement) have long-since supported file attributes.  Patching star (or KDE's ark) to support asking Nepomuk/soprano for this data before archiving shouldn't be too hard at all.  Worst case scenario, you can manually run the query, save it along with your files, and import it on a new machine.  But there's really no excuse for archivers not supporting this stuff as standard.  File attributes have been around for AGES in the unix world, and star has no issue with it.  They also work fine on other platforms like BeOS, Mac OS, etc."
    author: "Lee"
  - subject: "Domino"
    date: 2008-02-09
    body: "Forgive me for repeating this comment from another dot article, but all the replies were about QT, and I never got a reply about Domino.   The original author didn't appear to update Domino, and it has been a few years.\n\nWill some kind soul port over Domino to KDE 4? Maybe even somewhat of a cross breed between Oxygen and Domino? And these two improvements to Domino?\n\nhttp://kde-look.org/content/show.php/Domino+menu+hack+for+Beryl+shadows?content=72631\n\nhttp://kde-look.org/content/show.php/Domino+Mockup?content=72412"
    author: "T. J. Brumfield"
  - subject: "Re: Domino"
    date: 2008-02-09
    body: "> about QT\n\nYou write it Qt and pronounce it cute, not QT."
    author: "Patcito"
  - subject: "Re: Domino"
    date: 2008-02-09
    body: "Thanks.  I always assumed it was pronounced with a long e at the end, such as saying the letters Q-T."
    author: "T. J. Brumfield"
  - subject: "Re: Domino"
    date: 2008-02-09
    body: "Nope, that is a multimedia framework by a certain fruity company :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Domino"
    date: 2008-02-09
    body: "snob"
    author: "mike"
  - subject: "Re: Domino"
    date: 2008-02-09
    body: "I disagree. This piece of info has to be posted sometimes so that people know. Lately the (QT)/(Qt) ratio has been way too high in dot postings..."
    author: "Martin"
  - subject: "Re: Domino"
    date: 2008-02-10
    body: "Maybe if you are native english speaker. In swedish I pronounce it \"Q-T\" (but in swedish of course), there is no such word as \"cute\" in swedish. An alternative could be \"kut\" which is a baby seal."
    author: "Peppe Bergqvist"
  - subject: "Re: Domino"
    date: 2008-02-10
    body: "\"Maybe if you are native english speaker\"\n\nI believe you'll find far more German and Norwegian influences in its name than English.\n\n\"there is no such word as \"cute\" in swedish\"\n\nThere's no such words as \"Peppe\" and \"Bergqvist\" in English. Would you like me to call you \"Pepper Burger\"? Seriously, \"Cute\" is how it's pronounced no matter what your native language is. That doesn't get translated.\n"
    author: "Chris"
  - subject: "Re: Domino"
    date: 2008-02-10
    body: "Well, actually, despite my comment above, I often pronounce it \"q-t\" as well. This is so people understand what I'm talking about without additional explanation. Saying \"cute\" just gives me strange looks. And the reason for that is that many people only ever saw it in writing. This discussion might add to the number of people who know how to pronounce it though!"
    author: "Martin"
  - subject: "Re: Domino"
    date: 2008-02-10
    body: "\"Well, actually, despite my comment above, I often pronounce it \"q-t\" as well.\"\n\nWhen I first started promoting Qt to business people years ago, I tried using \"cute\" and got dismissed out of hand after the initial derisive chuckles.  When I started calling it \"queue tee\", I started grabbing people's attention.  It's hard to make people take something new (to them) seriously when it has a comical name.\n"
    author: "Tony O'Bryan"
  - subject: "Re: Domino"
    date: 2008-02-10
    body: "\"Seriously, 'Cute\u00a8' is how it's pronounced no matter what your native language is. That doesn't get translated.\"\n\nSo you don't translate words/abbrivations in to your own language? That is just silly.. I don\u00b4t prononuce Linux the way that americans do but more close to how Linus Torvalds pronounces it, http://suseroot.com/about-suse-linux/how-do-you-pronounce-linux.php\n\nIf I would follow your example then you would have to learn how \"Peppe Bergqvist\" is pronounced in swedish even if you would use that name in a normal conversation in english. So no more localizations of any word, is that your point?\n\n"
    author: "Peppe Bergqvist"
  - subject: "Re: Domino"
    date: 2008-02-13
    body: "Names (proper nouns) are rarely translated. For instance, if I meet someone from a Spanish/Latin country named Jesus, I don't pronounce it 'Jesus' as it would be in English, I pronounce it 'hey-zus' (going for phonetical here) which is what it sounds like in their language."
    author: "SMB"
  - subject: "Re: Domino"
    date: 2008-02-14
    body: "Q-Tee"
    author: "Andre"
  - subject: "Re: Domino"
    date: 2008-02-11
    body: "\" An alternative could be \"kut\" which is a baby seal.\"\n\nWell, it is a female organ in Dutch :o)"
    author: "whatever noticed"
  - subject: "Re: Domino"
    date: 2008-02-12
    body: "Mrs Hammond?"
    author: "reihal"
  - subject: "Re: Domino"
    date: 2008-02-15
    body: "Bwahahahahaha!!!\n\nLaugh of the day. Thanks. :-)"
    author: "Oscar"
  - subject: "Re: Domino"
    date: 2008-02-09
    body: "Sure, a lot of good stuff needs porting. Might as well quit repeating it ;)\nDomino's author works on Oxygen now, and for anyone else porting Domino is a matter of motivation. As is porting any other part of kde3."
    author: "logixoul"
  - subject: "Re: Domino"
    date: 2008-02-10
    body: "I was expecting to see a lot of themes/styles ported to KDE 4 and some hot new ones but that's not the case unfortunately. For the exception of Bespin which is very nice but still not finished."
    author: "Bobby"
  - subject: "Bespin"
    date: 2008-02-10
    body: "I thought the Bespin code is largely the old Oxygen code that was rejected for poor performance, and for being a mess.  If my choices are Oxygen and Bespin, I'll wait for more options.  Oxygen is decent, but my hopes were set high with Pinheiro's incredible mockups."
    author: "T. J. Brumfield"
  - subject: "Quarticurve"
    date: 2008-02-10
    body: "No, those aren't the only options. I have ported Bluecurve:\nhttp://www.kde-look.org/content/show.php/Quarticurve?content=59884\nThis is packaged in Fedora as qt4-theme-quarticurve.\n\nA port of the Bluecurve KWin theme is also available:\nhttp://www.kde-look.org/content/show.php/Quarticurve-KWin+(for+KDE+3.96+)?content=59972\nThis is packaged in Fedora as quarticurve-kwin-theme."
    author: "Kevin Kofler"
  - subject: "More KDE3 themes ported to KDE4"
    date: 2008-02-10
    body: "QtCurve (with a kwin decoration): http://www.kde-look.org/content/show.php/?content=40492\n\nPolyester: http://www.kde-look.org/content/show.php/?content=27968"
    author: "Craig"
  - subject: "Re: More KDE3 themes ported to KDE4"
    date: 2008-02-10
    body: "I actually like Qtcurve and Polester but I haven't tried Polester on KDE 4 as yet (I will try it later today). Anyway my favorite KDE 3.5x theme was Baghira which I still use. I like Bespin because it has a little reseblance to Baghira. I am still hoping to see some fresh, new and excited themes though. New KDE, new themes ;) "
    author: "Bobby"
  - subject: "Re: More KDE3 themes ported to KDE4"
    date: 2008-04-05
    body: "Please do make more classic themes for KDE4. I personally think that no matter how great the new UI is, there should always be a \"make things look like the previous release\" option for loyal users who liked the old theme. Especially since KDE4 is now much more customizable, this shouldn't be too hard. \n\nMy favorite widget and Kwin themes are: Karamik and Plastik \n\nIdeally a KDE3 theme converter (so they all just work) or some instructions for contributors (I couldn't find any instructions on how to do a manual conversion myself) would be most appreciated.\n\nFinally, dashboard widgets that more closely mimic the KDE3 K-Menu and panel would also be nice. Ideally all of this tied together with some default KDE4 classic theme option. \n"
    author: "Scott Knauert"
  - subject: "Re: Bespin"
    date: 2008-02-10
    body: "I don't know how good the code is but I am using Bespin for quite a while now and the performance seems to be quite good. Apart from that I like the subtle effects when one hover over the icons with the mouse and the dark menu background fits the KDE 4.0 desktop quite well but it seems to be somewhat half finished, something is still missing.\nWhat's the present status of Pinheiro's work? Including Raptor?"
    author: "Bobby"
  - subject: "bug or no bug?"
    date: 2008-02-09
    body: "I'm not very pleased about this fix: \n\n> Kickoff should start from the top level menu (and favorites)\n> Bug 155377: Kickoff should start from the top level menu (and favorites)\n\nactually I liked this behaviour, especially when browsing and exploring for example all the games.\nThis was for me the big usability improvement over the old style menu.\n\nProbably one could make this configurable..."
    author: "app browser"
  - subject: "Re: bug or no bug?"
    date: 2008-02-09
    body: "Maybe just create a bugreport for it? :)"
    author: "Sebastian Sauer"
  - subject: "Finally! HTML sigs in kmail"
    date: 2008-02-09
    body: "Thanks, Edwin!!"
    author: "Fred"
  - subject: "Re: Finally! HTML sigs in kmail"
    date: 2008-02-11
    body: "Is there any hope that we will be able to reply to html mails without destroying completely the original message format? At least preserving the indentation, linebreaks, color/bold... would be good start.\n\n\n\n\n \n \n"
    author: "jms"
  - subject: "Mac OS style menu bar"
    date: 2008-02-09
    body: "May I, with all respect for the good work and the priorities of the developers, ask if there are any plans to reintroduce the Mac OS style menu bar? I asked for that a while ago and Lubos Lunak generously offered to work on that somewhen post-4.0.\n\nAre there any news on that?"
    author: "furanku"
  - subject: "Re: Mac OS style menu bar"
    date: 2008-02-10
    body: "I use the Mac style menu too. Please reintroduce it."
    author: "Flavio"
  - subject: "Re: Mac OS style menu bar"
    date: 2008-02-10
    body: "The option is actually still there in kdeglobals rc file.\nYou have to add these two lines to have the toplevel menu.\n\n[KDE]\nmacStyle=true\n\nThe feature is currently quite buggy, however: often the menubar is not refreshed, and obviously the panel integration is missing... or should it be plasmoid integration? \n\nI miss it too, by the way. But so does Lubos, I think so there is still hope :)"
    author: "Luciano"
  - subject: "Re: Mac OS style menu bar"
    date: 2008-02-10
    body: "I see.\n\nThanks for the hint!"
    author: "furanku"
  - subject: "Re: Mac OS style menu bar"
    date: 2008-02-10
    body: "Why not? I like the idea.\n\nLet's also make a Plasmoid that looks like OS-X Leopard dock, Gnome panel, and OS-X like Menu bar.\n\nLong live customization!!! :-)\n"
    author: "Max"
  - subject: "Re: Mac OS style menu bar"
    date: 2008-02-28
    body: "yes i NEED this, and hopefully there will be a better fix available for GTK apps to support this; thats one thing i really hate about non-kde apps. but hey, i hardly use anythnig thats non-kde anyways"
    author: "jyaan bresy"
  - subject: "Thanks for the backports"
    date: 2008-02-09
    body: "Hello,\n\nI'd like to thank those distributions who back port the KDE 4.1 changes most missed from 4.0 already, specifically Riddel and Kubuntu.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-09
    body: "+1 person missin backports in kubuntu :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-09
    body: "Another one here... what has been backported to kubuntu?"
    author: "NabLa"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-09
    body: "Just updated mine kubuntu, didn't knew they had backported :D\n\nJust a few things, like multiple lines in taskbar and more buttons in the plasmoids (actually I thing they should only show those when user click in the plasmoid, not just passing the mouse over).\nIf resizing the panel is possible, I didn't found how.\n\nThere are yet plenty of bugs, most annoyings to be is double-click not working in desktop icons and system tray not always showing icon app (sometimes it just.. don't). I would recomend waiting for 4.0.3 or 4.1.\n\nMy most annoying missing feature: bluetooth support missing in Dolphin :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-09
    body: "You saying that kubuntu's kde4 packages contain or will soon contain backports, especially the resizable panel?\n\nThat'd be awesome!"
    author: "MichaelG"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-09
    body: "it has already, do an apt-get update && apt-get dist-upgrade"
    author: "Patcito"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-10
    body: "Ah, you've to install plasma-playground for the resizable panel? \n\nDoesn't work at the moment, because some dependency on my system is broken, but nonetheless i like the backporting thing."
    author: "MichaelG"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-10
    body: "Meh, seems not my system to be broken, but the current kubuntu... it says:\n\ndpkg: error with /var/cache/.../plasma-playground...deb (--unpack)\n\ntrying to overwrite /usr/lib/kde4/lib/kde4/plasma_applet_battery.so, wich is also in paket libplasma1\n\ndpkg-deb: subprocess killed with signal broken pipe\n\nI hope, JRiddell can work it out..."
    author: "MichaelG"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-10
    body: "OpenSUSE have also done a number of backports for their packages ... nice to see.\n\ntoday in our plasma meeting we discussed getting these backports actually into the 4.x branch. looks like we (plasma team) will be doing a set of backports for 4.0.2 and another for 4.0.3"
    author: "Aaron Seigo"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-10
    body: "I'm sure that will make plenty of people happy."
    author: "T. J. Brumfield"
  - subject: "Re: Thanks for the backports"
    date: 2008-02-10
    body: "The openSuse team is doing an excellent job. \nAlso big thanks to you and the rest of the KDE team for working so fast to get more and more features added + bugfixes so rapidly :)"
    author: "Bobby"
  - subject: "Remove the Taskbar"
    date: 2008-02-09
    body: "Hi,\n\nfirst of all, thanks for KDE 4 ;-).\n\nNow to the subject. Why do we need a taskbar, if we have Plasma with Dashboard and/or KWin with nice 3D-Effects for switching Tasks? IMHO we don't need a \"Taskbar\". Remove it. Or do I oversee somethink?\n\nbye\n\nfrank\n\nP. S.: sorry, for the english ;-)"
    author: "Frank"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-09
    body: "Feel free to remove it if you want. ;) I think there are some tutorials on the internet for removing the panel. The beauty of Plasma is that you can do whatever you want with your desktop.\nI don't think that the majority of KDE users would want to go without it, though."
    author: "Jonathan Thomas"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-09
    body: "thanks to plasma and the new panel you are free to remove the taskbar whenever you want (or put it somewhere else, or have 10 tasbars or whatever)"
    author: "Beat Wolf"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: "Btw this is also true for Kicker."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-11
    body: "But with Kicker, you can't remove the panel outright. There's always got to be one.\n\nAnyway, the task bar is there for people that use the mouse more: if your hand's already on the mouse, it should be faster than using the keyboard. It's also good for giving a familiar environment to people, and showing possibility to switch tasks without seeing the window to complete newbs."
    author: "Soap"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-09
    body: "I almost agree, but some UI elements are best kept always visible, and not only on dashboard. In particular the systray which displays useful notifications and features. The clock also might be best visible.\n\nIn the late days of KDE 3, I had no more task bar (well in fact I had a not-so-useful autohidable avant-window-navigator). I choose to have instead a minimalist top bar with the systray, the clock, and the application menu (\u00e0 la MacOS) and some other less important elements (like the K menu). No wasted space, since that bar had no free pixel and contained only useful stuff.\n\nWell, I like keeping space for things that matter (the currently used application). In KDE4.0, that big unresizable panel hurts me, but well, it will soon become more customizable."
    author: "Aldoo"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: "That's exactly what I do myself. We have to admit that Apple got it right."
    author: "Flavio"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-11
    body: "Hey, me too! With my shallow-monitor lapotop, vertical space is a premium, and I found I've never used the taskbar anyway... so it had to go. It is now an autohide panel on the right... I'd like to geet rid of that too. Somehow, the menu panel cannot be the only panel anymore. I think it used to work, but no more in KDE 3.5."
    author: "Luciano"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: "personally I have a taskbar with the current desktop's windows on my panel, and a taskbar with all windows on my desktop. :) I've gone back to having the systray on the panel again."
    author: "Chani"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: "Have it both ways: Auto-hide."
    author: "reihal"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: ">>Or do I oversee somethink\n\n... dude. I'm german myself, but that's an embarrassing one. Seriously.\n\nhttp://dict.leo.org/ende?lp=ende&lang=en&searchLoc=0&cmpType=relaxed&sectHdr=on&spellToler=on&search=oversee&relink=on\n\nYou wanted to say \"Or do i MISS somethinGGG\"."
    author: "MichaelG"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: "Or \"overlook\", which is based on the same idea as the German word, and actually means almost the same thing (unlike \"oversee\" which is indeed something completely different)."
    author: "Kevin Kofler"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-10
    body: "Yea, add other options for the taskbar.\n\nHave people be able to customize it. (make it thinner..)\n\n\nas I said before: LONG LIVE CUSTOMIZATION!!!\n\n\nSomebody please post videos on YouTube on how to do it for newbies.\nAlso please post feature show-off's.\n\n-Max\n\n"
    author: "Max"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-11
    body: "No Way! KDE Taskbar rules! \n\nfor example, in KDE3 I used to have it in upper-left corner, going down as a simple list, which comes on top when i navigate to upper-left corner, without any preview widows or stuff. having sometimes 20-30 windows open (Matlab and lots of Matlab graphs + KDevelop + 2-3 Konsoles, + 2-3 Konquerors + Kolourpaint + Kontact + Kopete + something else) is my usual desktop. and -- i never use window grouping -- having ALL windwos listed and sorted by applications is very handy and accessible (see attach). \n\nThe most admiring feature for me is... rolling MouseWheel over such taskbar -- for example -- while working in Matlab it allows me to navigate numerous open Figures very quickly! \n\nUsing manual plasma_appletsrc edit i tried to make something similar in KDE 4.0.1, but Vertical Taskbar looks awfull there -- it's items tend to be 200px high!, and then, after 30th window, they open 2nd column. I dont want to see large application icons in taskbar -- i preffer to see window titles' list instead! Even if there will be no GUI for configuring this, as it was used to be in KDE3 (I use Extended Taskbar there) -- I'd like to have ability to configure it manually in config files! \n\n"
    author: "Vitaly Shishakov"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-11
    body: "I like the taskbar, but it's not revolutionary by any means.\n\nIt has been around since before Windows 95 (PC tools, IIRC), and then popularized with Windows 95. So 13 years!!\n\nShouldn't we come up with some more useable alternative? There has to be some novel idea out there, that increases productivity, yet preserves screen Real Estate.\n\n"
    author: "Max"
  - subject: "Re: Remove the Taskbar"
    date: 2008-02-13
    body: "Please note that the TaskBar is an applet.  So, I presume that this is about the panel.\n\nThe reason that you need a panel is so that you can get to things while you have a maximized window covering the DeskTop.  Actually, I have several.\n\nNote that widgets would be more useful if there was a way to bring individual ones to the top with a hot spot on the edge of the screen (like hidden panels in KDE-3).\n\nSo, please keep the panel.  I hope to see hide and auto-hide as well as child panels in the near future."
    author: "JRT"
  - subject: "Its sad."
    date: 2008-02-09
    body: "I just with all the greatness of konqueror as a file mangager was fully restorted.... dolphin is of no use to me."
    author: "taurnil.oronar@hotmail.com"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "Thank you for that very insightful comment. It really motivates us developers, because it gives us something constructive to work with!"
    author: "Huang"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "You welcome for that comment. It was all I trusted myself to say without turning it into a rant."
    author: "taurnil.oronar"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "just to make it clear: this would have happened regardless of dolphin. it was the result of the (much needed) shift to model/view for the file views and was already under development prior to dolphin coming into svn."
    author: "Aaron Seigo"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "To be honest, I don't really care what the underlying technology is. I don't even care if the change was needed because you needed to add wheels to your grandma so you could use her as a wagon. What ever the underlying technology is or you think needs to be used, well more power to you. \n\nBut none of that really suggests to me konqueror as a file manager needed such radical (IMV) outward changes. Konqueror as a file manager as it is in KDE3 is AFAIAC, the greatest frigging application there is. I use it for just about everything and anything. There is nothing IMV that even compares.\n\nRight now the way it feels in KDE4 it is nothing but a shadow of its former self and I can only hope this is only due to the freshness of KDE4 and as time progresses, *all* the things it could do will be restored in KDE4."
    author: "taurnil.oronar"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "I love the Filemanagement of Konqueror in KDE3 too, but i think the KDE4 one will be improved.\n\nabout the underlying technology, i really do care (as a user) because it guarantees the future of the software.\nIts the powerfull technology of KDE which makes Konqueror such a great application and this is exactly why something like Konqueror doesnt exists in other platforms: they havent such a good 'underlying' technology.\n\nalso, one of the motivations of FOSS programmers 'is' the underlying technology: they want to have fun with programming, programming is their pleasure and having fun while programming is just possible with 'underlying technology'."
    author: "Emil Sedgh"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "I think you misunderstand my point. Yes I do understand the technology that makes KDE my favorite desktop is important. I was trying to say in a humorous way, I am comfortable with what ever decisions Aaron and the rest of the KDE developers make in that area. \n\n"
    author: "taurnil.oronar"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "Ehm, what do you miss? I myself mis the right mouse copy to and move to menu's, but I haven't used konqi much..."
    author: "jos poortvliet"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "Extract Here option, Information and Metainformation and Preview on Hover and maybe others...\n(for example i want to see the height of an image, in kde3 i was hovering the mouse on the image and in a tooltip it told me all information, now i have to open gwenview for that (an nah, its not i didnt find it in the Properties)\n\nAlso, im really jealous to the Information bars of the Dolphin.is there any plan to have them on konqueror too?\n\nalso there are some abusing bugs (in trunk) like copy/paste is not working, i have to check preview on every directory i want and..."
    author: "Emil Sedgh"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "This is so confusing.\n\nIs it Doplhin or Konqueror? Will we use both side-by-side?\n\nOr will it up to the distributions to choose, which one is better?\n\n[sarcasm]\nI see no problem with others, but Kubuntu might be in trouble. That would mean they actually *HAVE TO* work on the KDE port and *gasp* customize it. [/sarcasm]"
    author: "Max"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "Konqueror will be the default as a web browser, Dolphin as a file manager. Distros could of course override this, but I don't think many will. Fedora will use the upstream defaults. As for Kubuntu, they are already defaulting to D3lphin as the file manager in their current KDE 3 setup (see, it's not true that they don't customize anything!), so they'll almost certainly use the upstream defaults in KDE 4."
    author: "Kevin Kofler"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "So from the sounds of your post, konqueror as a file manager is pretty much on the chopping block and will never regain the functionality and usefulness it had in KDE3. If that is really what the KDE folks have in mind that simply reinforces my notion \"this sucks\". \n\nThe *ONLY* way I would *EVER* consider Dolphin a usable substitute is if it *HAS* the *SAME* power and usefulness as konqueror in KDE3."
    author: "taurnil.oronar"
  - subject: "Re: Its sad."
    date: 2008-02-10
    body: "Both the lead developer of Konqueror (David Faure) and the developer of Dolphin (Peter Penz) have both stated on several occasions that they do not want Konqueror to lose any features it had in KDE3 as a result of embedding the Dolphin KPart.  So it's basically a case of time and manpower.  "
    author: "Anon"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "That indeed is very pleasant to hear. I would love to help them on the manpower part but I do well to push bytes around fiddling around in bash."
    author: "taurnil.oronar"
  - subject: "Re: Its sad."
    date: 2008-02-19
    body: "That's a relief. It makes improves DolphinPart AND improves Konqueror. There can be no problem with that."
    author: "Riddle"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "I stand corrected.\n\n(forgot about Dolphin in 3) \n\nStill it really feels like Kubuntu isn't trying."
    author: "Max"
  - subject: "Re: Its sad."
    date: 2008-02-11
    body: "hmmm, many KDE devs complain about all the changes Kubuntu does to KDE as it interferes with bugreporting... And you want MORE customizations?"
    author: "jos poortvliet"
  - subject: "Plasma suggestion"
    date: 2008-02-09
    body: "- is panel a plasmoid tha contains other plasmoids or a kde4 app? if it's a plasmoid why not simply adding the buttons to scale/rotate?\n\n- suggestion number two is related. I don't know if you did usability studies or whatever, but changing the plasmoids visual (showing border and scale, remove and rotate button) every time I pass a mouse over one plasmoid is... damn.. a nightmare! Can't you just add a small area/button that does it when I *click* on it? Current behavior is plain ugly IMHO and bad at usability - imagine a person moving the mouse and pressing the button by mistake right when the cursor was above the remove icon (I know there is a confirmation dialog, but still sucks). :)\n\n- once it's basically a SVG border (at least the part that matters), what about adding a option to configure the background color? Sure, it won't work for SVGs themes with degrades or texture, but would be great for simpler ones as in the current version. Nothing agains black, but it would match the color theme better ;)\n\n- least but I hope not fopr ever, I want back those little bars used to move each item in the panel. Did you noticed it's a bit hard to order them when placing in the panel from add plasmoid dialog?\n\nWell, you can refuse all my suggestions, but remember most of them are things that are regressions from 3.x ;)\nThanks in advance!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: "about the second suggestion... right click on your desktop -> \"lock widgets\" and you're good to go ;)"
    author: "knightmare"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: "> is panel a plasmoid tha contains other plasmoids or a kde4 app? \n\nit's a Containment, which is a kind of Applet, though not a plasmoid stricly speaking. it's very similar and has a lot of the same attributes (due to it being an Applet, which is the core of a plasmoid) but it has some additional feature.\n\n> if it's a plasmoid why not simply adding the buttons to scale/rotate?\n\nthat's the essential idea, though it's not that easy due to other constraints. \n\n> every time I pass a mouse over one plasmoid is... damn.. a nightmare! Can't\n> you just add a small area/button that does it when I *click* on it? \n\nso you want *two* clicks? \n\n> imagine a person moving the mouse and pressing the button by mistake right\n> when the cursor was above the remove icon\n\nhave you actually tried doing this? at least in trunk, there is a small delay before showing the handle (so just moving the mouse over a plasmoid doesn't result in the handle) and the close button is *outside* of the applet area. which means you have to hover slowly enough and then move outside the applet.\n\nnot probable, and in fact, i can't even make it happen in an accidental fashion when i *try* =)\n\n> what about adding a option to configure the background color\n\nit already follows the color scheme for the svg theme.\n\n> I want back those little bars used to move each item in the panel.\n\nit's coming. \n\n> but remember most of them are things that are regressions from 3.x \n\nregressions will *always* be there because we aren't creating the same thing. so you're not going to have a 1:1 feature set. plasma is designed to do a *lot* more, but it's also designed to *not* do some of the things kde3 stuff did, mostly because those things can be done better.\n\nand \"most\" is incorrect. i count one of your items as a regression, as the rest of them are regarding new features that come in plasma. =)"
    author: "Aaron Seigo"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: "First of all, thanks for your answers.\nAbout testing passing the mouse.. yes, I've tested. I had tought there was a confirmation dialog to remove a plasmoid/icon but no... it was very easy to remove one by mistake.\n\n>so you want *two* clicks? \nYes, kind of, or right click on a plasmoid and have a menu with options much like the icons in kde3. I *know* that it makes harder to move/delete/resize, but that is actually my idea. Maybe I'm getting old, but things flashing when I pass the mouse over are so.. macromedia flash! :)\n\nBut hey! Developers are free to not listen to users by all means :D\n(just wished oxygen had did it on the style contrast and lack of color in active title, heheehhehehe)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: ">>  I *know* that it makes harder to move/delete/resize, but that is actually my idea. Maybe I'm getting old, but things flashing when I pass the mouse over are so.. macromedia flash!\n\nI think you missed the point above about this being changed in trunk, in fact, it was changed in the current openSUSE packages, don't know about others, though.\n\nOn my desktop, the mouse has to be on a widget for a couple of seconds before the frame appears, that seems reasonable to me.  If that's still an issue that leads to people accidentally clicking and closing them, I imagine they have terrible issues with the window bar and accidentally closing apps with that inconvenient \"X\" button... ;)"
    author: "elsewhere"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: "Not here in my kubuntu. But if it was changed, very good! :)\n\n>I imagine they have terrible issues with the window bar and accidentally closing apps with that inconvenient \"X\" button\n\nThat's why it's placed in the top-right corner, imagine it all over the screen. Clicking of death would be funny :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: "Oh, and I almost forgot: I wish I could tell my boss in HP: \"this isn't a regression, we replaced the old UI for a new one\", or even \"it's not a defect, it's a new feature\".. well the second I use sometime actualy :)\n\nHeheheheheheh, but I do see your point and kind of agree, sadly world is far from perfect and removing features is perceived most times as regressions :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-10
    body: "The first two things you talked about don't exist in KDE3 (KDesktop), so how can there be any regressions in regards to them? The third thing Aaron did acknowledge as a regression.\n\nAaron explained that not everything will be the same because the design objectives are different, so what helped create a great desktop in KDE3 (KDesktop) might be a hindrance in KDE4 (Plasma).\n\nLeastwise, that is how I understand what he said."
    author: "yman"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-11
    body: "> have you actually tried doing this? at least in trunk, there is a small delay \n> before showing the handle (so just moving the mouse over a plasmoid doesn't \n> result in the handle) and the close button is *outside* of the applet area. \n> which means you have to hover slowly enough and then move outside the applet.\n\nI suggest increment this delay because IMO it's still to quick (at least with svn from aprox one week ago)"
    author: "Vide"
  - subject: "Re: Plasma suggestion"
    date: 2008-02-11
    body: "Or just make it configurable. Let the user decide what he wants, and if he wants a delay, let him decide how long the delay should be. It's all about configurability and customizability."
    author: "blueget"
  - subject: "Great Games Website"
    date: 2008-02-09
    body: "Hi,\n\nI wanted to say that the new KDE games website is really beautiful and up-to-date! Thanks for this!\n\nI would be great if other pages were updated (like the Plaska page).\n\nGreetings from Switzerland"
    author: "Dominik Huber"
  - subject: "Re: Great Games Website"
    date: 2008-02-10
    body: "> I would be great if other pages were updated (like the Plaska page).\n\ni agree. now to find those extra 12 hours a day .. =) my hope is that as people are starting to Get(tm) the plasma concepts, that it'll be easier/possible for others to start working on the website so that the content isn't solely up to me.\n\nwhile the site is important, there are other more important things to do with my time right now."
    author: "Aaron Seigo"
  - subject: "Re: Great Games Website"
    date: 2008-02-10
    body: "At least, similar pages for KDE packages (edu.kde.org, pim.kde.org etc.) should present the same nice interface."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Great Games Website"
    date: 2008-02-10
    body: "Cool. I didn't know there was one."
    author: "Max"
  - subject: "Re: Great Games Website"
    date: 2008-02-12
    body: "Shouldn't KJumpingcube be in the Board section? It only uses the concept of dice to create a board game; it does not have an element of chance, as in throwing dice."
    author: "Martin"
  - subject: "Plasma config"
    date: 2008-02-10
    body: "While the nice GUI for configuring the plasma panel is still absent, \nwhere could one find a description of plasma_appletsrc config file? "
    author: "Vitaly Shishakov"
  - subject: "Re: Plasma config"
    date: 2008-02-10
    body: "the official howto doesn't work anyways... myself and others tried numerous variants of editing the file's panel section (while plasma being terminated of course) including size=35 or other things, but it remains just like it was, huge and clunky. Sorry to disappoint"
    author: "MichaelG"
  - subject: "Re: Plasma config"
    date: 2008-02-11
    body: "I just compiled from svn (trunk) yesterday. The panel is now configurable in size (tiny/small/normal..) and location (left/bottom/right...). "
    author: "Thomas"
  - subject: "Re: Plasma config"
    date: 2008-03-02
    body: "The size and placement option seems to work okay. The one thing I miss is the auto dissapear option, that, along with drag n drop placement of panel widgets, and I'd say we just about have our beloved kicker back in all but name.\n\nPanel auto dissapear pretty please (damn I wish I could code C++) ?"
    author: "markc"
  - subject: "Re: Plasma config"
    date: 2008-03-02
    body: "\"Panel auto dissapear pretty please\"\n\nIt's planned - in fact, it was targetted for 4.0.0, but the devs ran out of time :("
    author: "Anon"
  - subject: "Mercator map projection"
    date: 2008-02-10
    body: "Why do we want Mercator projection.  IIUC, it has been deprecated.\n\nWe already have equal angle, which is probably the best.  It would probably be a good idea to add equal area as well.\n\nOrthographic and equal-angle hemisphere projections would be nice.  Would it be possible have the user choose the center point?"
    author: "JRT"
  - subject: "Re: Mercator map projection"
    date: 2008-02-10
    body: "Well that wasn't clear.\n\nI meant to say that it would be nice if we could add equal-angle.  I presume that the current globe view is orthographic."
    author: "JRT"
  - subject: "Re: Mercator map projection"
    date: 2008-02-10
    body: "You are welcome to contribute.  :-)\n\nI know that this \"standard\" Open Source answer is not what you or other people suggesting features want to hear.  On the other hand, we are refactoring the projection system in marble right now so that all calculations that are unique to a certain projection will be collected into one class.  This means that a new projection can be created in just a few hours if you know the maths, and you don't have to know much about the internals of Marble at all.\n\nIn fact, we hope to make the creation of a new projection a Junior Job(tm), which it definitely is not now."
    author: "Inge Wallin"
  - subject: "Re: Mercator map projection"
    date: 2008-02-10
    body: "Yes, the current globe is orthographic projection. Marble is not meant to be a full blown GIS application so we only focus on things that cover most use cases in practice. \nWhile the Stereographic projection is highly popular for starcharts I don't see much use for it in digital geography. That being said Mollweide projection (which is one of the few useful equal area projections - god forbid Peters Projection) is something that is being used quite often and I'd be definetely interested in getting that into Marble."
    author: "Torsten Rahn"
  - subject: "Re: Mercator map projection"
    date: 2008-02-13
    body: "Equal angle (Azimuthal Equidistant) is a well known projection although most people probably aren't aware of it -- it is the projection used in standard fish-eye lenses (Nikon made one Orthographic, but it wasn't a commercial success).  Imagine that you are standing at the center of a translucent globe.  \n\nGetting back to maps.  The advantage of equal angle is that distance from the center point is true -- which I why I suggested that the user be able to specify the center point.  Also, it is a useful compromise between Orthographic and Gnomic.\n\nYes, there are issues with equal area projections, but they have their purpose in that they show land area correctly (although distorted in shape)."
    author: "JRT"
  - subject: "KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "I have a project I started a few hours ago, just to see if I can do it (and to rescue my old Vaio from the depth of a storage closet)\n\nI'm trying to run a distro of Linux on a\nSony Vaio PCG505-Sr7k\n600 mhz Pentium III\n192 megs of Pc-133 SDRAM (I can put in 256 total, but I could not find a vendor that sells SDRAM Micro-DIMMs, but that's another story)\n10\" screen 1024x768 max resolution.\nNetgear 54g wireless cardbus card (atheros chipset, IIRC)\n12 gig hd.\nexternal DVD-ROM drive (plugs in through cardbus)\n\nI could probably do XFCE, but that would be to easy (and unfamiliar) Besides this could finally lay proof to the claim that KDE 4.0 uses less resources than KDE 3.5.X\n\nThe computer was built for Windows 2000, I managed to run WinXP on it. As well as Suse 10 (before openSUSE). Never could get the wireless to work under the Linux partition. \n\nNow will KDE 4.0 run on it? (kinda like \"will it blend?\", but less destructive. :)  )\n\nI'm going to bed now, but based on you guy's prediction, and helpful tips, I will attempt it tomorow morning. Some tips as to the installer would help too, as most live CD's I can think of atm, want at least 384mb ram.\n\n\n\n"
    author: "Steve on a Sony"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "\"Besides this could finally lay proof to the claim that KDE 4.0 uses less resources than KDE 3.5.X\"\n\nKDE4.0 at least uses more RAM than 3.5:\n\nhttp://www.jarzebski.pl/read/kde-3-5-vs-4-0-round-two.so\n\nAll the credible claims about KDE4 using less memory than KDE3 were made years ago based on (educated) guesswork, before KDE4.0 was compiling correctly, even.  It turns out that it was just wishing thinking, alas :("
    author: "Anon"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "edit: I found the memory, but I don't think it's worth it, at $44 for 128 megs. \nhttp://www.4allmemory.com/index.cfm?fuseaction=search.w4s&UniqueId=0109-9609-9782-3215-7746\n\n(besides, maybe that would be cheating. \"192 megs 'outta be enough for everybody!\" :)  )\n"
    author: "Steve on a Sony"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "KDE4 or 3, will run just fine one that machine. \n\n192Megs of RAM is no problem, but you may want to stay away from some of the \"heavy\" distributions like *buntu or Suse. Perhas some of the lighter Slackware based ones(not a KDE issue). \n\nThe thing most likely to not make speed problems are the hd, a old 12 gig laptop drive is not exactly a speed demon. So slow application startup times would not be surprising, but that's not particulary important. After all if you are actually going to use it, one usually keeps applications open to do actuall work not just opening and closing them."
    author: "Morty"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "Seriously, KDE4 runs pretty well on the Asus EeePC, so I could imagine that it will do the same on your computer .."
    author: "Torsten Rahn"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "eeePC runs KDE3 and he is asking for kde4 :)\nIn my experience KDE4 is faster in some parts (program starting mostly) and slower in use. I believe because a lot of small details like rounded corners and fade effects where added (I'm talking about style, not desktop efects/xgl, those turn my computer in a slow and old dog)."
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "It may come default with KDE3, but KDE4 runs pretty well on the Asus EeePC. Which was the question. And since Torsten has a EeePC, and are running KDE4 on it he answered well :-)\n\nhttp://developer.kde.org/~tackat/marble_secrets2/marble-eepc-olpc.jpg\n"
    author: "Morty"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-12
    body: "Yes, but the EeePC is a much more powerful machine than this old Sony.\nIt's  only the LCD that sucks on the EeePC."
    author: "reihal"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-11
    body: "I got the same laptop.\n\nIt's sitting on a shelf somewhere. I used to use it a lot. I think, I too upgraded it to Windows XP before it started collecting dust. (it wasn't exactly a speed demon under Windows XP, didn't feel like downgrading back to Windows 2000)\n\nLet us know how it comes out. Maybe I will put Linux on it too.\n\nI think that laptop is about the size of an eee-pc. Gotta check!\n\nWhat's a good distro that recognizes most components automatically with little fuss? Drivers are a headache if I have to do them manually.\n\nI like the \"will it run?\" challenge. Would show that KDE 4.X is both lean as well as powerful.\n\n"
    author: "Max"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-02-15
    body: "This will probably work but ... (tm).\n\nThere are two issues here:\n\n1.  Distros tend to install a lot of crap that you really don't need on a laptop.  This will fill up your limited disk space and if these things are running, they will fill up your limited RAM.  So, you should be careful to see that you are only running system processes that are needed and that unneeded Kernel modules are not loaded.\n\nNOTE: I would get the extra memory.\n\n2.  The system is going to be running on virtual memory.  So you are going to need at least 1 GByte of swap.  In theory, two 1/2 GByte swap partitions are better than one 1 GByte.  Set \"/proc/sys/vm/swappiness\" [sic] (no it isn't spelled correctly) to 100 to free up as much ram as possible.  \n\nNOTE: If swap fills up, you will need to increase it to 4 x 1/2 GByte partitions.\n\nNow to describe the problem.  When you are running something, everything will be well, but you will find that whenever you switch applications that a Virtual Memory swap will occur before you can do anything on what you have switched to.  If your disk drive is slow, you will find that to be slow.\n\nThere are strange ideas running around about using a flash memory card for swap memory.  There are issues of speed and useful life vs. a mechanical HD.  The hardware to do this on a desktop is available and Intel is promising much faster flash cards.  I don't know if this would be relevant for a laptop since you might not have room to put it in the case."
    author: "JRT"
  - subject: "Re: KDE 4.0 on slow PC's? - challenge"
    date: 2008-03-11
    body: "I installed Slackware 10.2 on my SR7K a couple of years ago. I have 128 MB ram and a 40 gig 16mb cache toshiba drive. I remember having to enter special paramerters to get the installer to recognize the CD-ROM, but they were relatively easy to find online. The video was working after the install, I dont remember what I had to do to get sound working, the modem I was never concerned with, I had to re-compile my kernel to get apm to see my battery status. I also have a wireless card that I used Ndiswrapper to get working. I run KDE 3.5, XMMS, openoffice, and etc.. runs fine....\n\nBut it is a clean lean machine... people always comment on it...\n\nIf you don't mind let me know how KDE4 runs on it... woulden't mind something new to look at.\n\nKyle"
    author: "Kyle"
  - subject: "Krone"
    date: 2008-02-11
    body: "I currently use Eqonomize (http://eqonomize.sourceforge.net) a lot.\nIs a cooperation between these two projects possible?"
    author: "Erwin"
  - subject: "Re: Krone"
    date: 2008-02-13
    body: "There's also KMyMoney2 which is much more advanced than Krone... I understand Krone aims to be much more basic than any of these (and I really don't want to flame), but it would be nice if people could avoid reinventing (and duplicating) the wheel for once... Wouldn't have been better to improve (or simplify) KMyMoney or Eqonomize instead of starting yet another little app that will eventually beg for a maintainer...?"
    author: "Giacomo"
  - subject: "Re: Krone"
    date: 2008-02-13
    body: "I think the two apps play in completely different leagues. Judging from what the Krone author writes, it is a very simple expense logging thingy, whereas KMyMoney2 (which i actually have been using at home for some time) attempts to be a full-fledged personal finances manager application capable of talking to your bank account etc."
    author: "Planetzappa"
  - subject: "Re: Krone"
    date: 2008-02-14
    body: "Yeah, but couldn't they accomplish the same thing by adding a module or a wizard to KMM2? This way one could start logging basic expenses and \"graduate\", if necessary, to the full-fledged features."
    author: "Giacomo"
  - subject: "I thought NOKIA was going to donate money to KDE?"
    date: 2008-02-11
    body: "I just noticed something. I thought Nokia was going to donate money to the KDE project? Something about becoming a Patron?\n\nI've yet to see their name on that list:\n(speaking of which, where did the \"Patron of KDE, Supporter of KDE\" image go. Wasn't it always on the homepage?)\n\nAlso isn't E$ 10,000 nothing more than chump change for a big company like that?\nI think they should donate MUCH MORE MONEY THAN THAT. They bought Trolltech after all. They should at least have a SIGNIFICANT amount of money vested in KDE as a gurantee towards the project, and to show goodwill, IMHO.\n\nCould anybody involved with Trolltech please clear this up?\n\nAlso \"What have they done for me lately\" - Eddie Murphy\nCould anybody give us an update of what Nokia has done for Trolltech,KDE,OpenSource in general in the last few weeks since the aquisition? I'm curious and interested.\n\n"
    author: "Max"
  - subject: "Re: I thought NOKIA was going to donate money to KDE?"
    date: 2008-02-11
    body: "Hopefully the above didn't sound too negative. I'm trying to welcome Nokia with open arms. Inquiring minds just want to know.\n\nupdate: still can't find the\nPatron of KDE\nSupporter of KDE\nlist.\n\nhttp://kde.org/support/donations.php\n\nIn general, even private donations are low. I know it's a recession and such, but the KDE folks also need to eat/code. We all know, that according to the \"ballmer peak\" one needs beer for that. Free beer!! =) j/k.\n\nI think I'm going to donate $50 my next paycheck. (crap, that's like $0.50 cents in Euros these days.. :-/ ) \n\nCan I ask a favor: Could you please write more Compiz type compositioning effects in return? puweese?\n\nbtw.: Kudos for being able to operate on such a shoestring budget. Most businesses wouldn't be able to.\n\nKeep up the great work. Hopefully more donations will come.\n\n"
    author: "Max"
  - subject: "Re:  donate money to KDE? - I'll donate for Compiz"
    date: 2008-02-11
    body: "I'll donate $20 for more compositioning effects. Like, Coverflow, and expose."
    author: "Steve M"
  - subject: "Re:  donate money to KDE? - I'll donate for Compiz"
    date: 2008-02-11
    body: "I still think a mini-competition for a small prize with the Oxygen team as judges would be the best way forward."
    author: "Anon"
  - subject: "Re:  donate money to KDE? - I'll donate for Compiz"
    date: 2008-02-12
    body: "I like that idea too."
    author: "Steve M"
  - subject: "Re: I thought NOKIA was going to donate money to KDE?"
    date: 2008-02-11
    body: "> still can't find the Patron of KDE Supporter of KDE list.\n\nhttp://ev.kde.org/supporting-members.php"
    author: "Anonymous"
  - subject: "Re: I thought NOKIA was going to donate money to KDE?"
    date: 2008-02-11
    body: "> I thought Nokia was going to donate money to the KDE project?\n\nYou have a wrong expectation how fast such things happen...\n\n> Could anybody give us an update of what Nokia has done for Trolltech,KDE,OpenSource in general in the last few weeks since the aquisition?\n\nThe acquisition has not happened yet, so nothing."
    author: "Anonymous"
  - subject: "Re: I thought NOKIA was going to donate money to K"
    date: 2008-02-11
    body: "I doubt they want to just paypal over that much money, \"chump change\" as it may be, so just have patience. ;)"
    author: "Ian Monroe"
  - subject: "Does that mean, that the buyout might not happen?"
    date: 2008-02-12
    body: "Does that mean, that the buyout might not happen?\n\nI see no reason why NOKIA couldn't donate the money well ahead of the purchase. Now, before 4.1 is released is when we would need it the most.\n\nThere is still tons of work and money needed to dof:\nAdd Features for 4.1 (since no features are allowed to be added for 4.1.x *grr*)\n\nMore desktop effects,\nSimple way of finding extra plasmoids, extra apps, etc. (KDE-look isn't newbie friendly)\nMore support for computers with small/low resolution screens. Car-PC, Asus-eee, mobile devices (NOKIA, ARE YOU LISTENING??), other ultra-portable pc's.\nA way for newbies to regain desktop screen real-estate.\nAMAROK 2.0\nKDE on Windows, KDE on Mac\nMUCH MORE PUBLICITY.\nMore input from different distro's (openSUSE, KUBUNTU, Fedora, etc.)\nTons of videos posted on Youtube, and then (stumbleupon, digg, /., de.licio.us, etc.) People crave screenshots, and eye candy, if they were to switch to KDE.\ntons of other features, I forgot to mention\n\nThere is only a few months left."
    author: "Steve M"
  - subject: "Re: Does that mean, that the buyout might not happ"
    date: 2008-02-13
    body: "> Simple way of finding extra plasmoids, extra apps, etc. (KDE-look isn't newbie friendly)\n\nI'm looking foward Get Hot New Stuff integration in Plasma. I have a svn build from some days ago and the button is still disabled. Anyone knows if at least it's planned to come in 4.1?"
    author: "Firmo"
  - subject: "Re: Does that mean, that the buyout might not happ"
    date: 2008-02-19
    body: "If the button is there, then it's planned to 4.1. Right?\n"
    author: "Riddle"
  - subject: "I know this isn't the place, but"
    date: 2008-02-12
    body: "This is a feeling sort of thing, and I wouldn't know where to start fixing it.\n\nThe desktop feels heavy. Mousing around, using the menu, opening and closing things, it feels like you are dragging a weight around. I suspect it has to do with fine tuning the timings. It isn't lag caused by a slow machine or loading libraries, which gives a stutter feeling. It is consistent.\n\nThis is one of those things that will get sorted out by use. I'm impressed how KDE 4 is coming along, and I don't think any developer can help but be excited by the available technologies.\n\nDerek"
    author: "D Kite"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-12
    body: "I experience the same! Strangely enough this is the first time I read anyone else reporting it. Have we all actually seen this, but nobody spoke up? I was already resigning to eventually getting a new machine just to be able to run KDE4, though my current one is damn fast when running anything else than that. \n\nHopefully it can be fixed?"
    author: "Martin"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-12
    body: "Since KDE4 runs smoothly on the pretty low-spec EEE PC and I have heard very few people complaining about it, I'm guessing this problem affects only a minority of users and is likely caused by their particular hardware/ driver configurations."
    author: "Anon"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-12
    body: "Pentium M @ 1.86 GHz, 1 GB, Intel 915GM graphics, freshly installed Kubuntu Gutsy, KDE4 from the PPA archive on launchpad, nothing else running. Any clues?"
    author: "Martin"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-13
    body: "define \"smoothly\"... I just compiled it on a 2Ghz / 1024 ram / ATI radeon 9600 / 7200rpm machine, and it does feel sluggish compared to the recently-tuned 3.5."
    author: "Giacomo"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-13
    body: "My notebook uses the xorg.intel driver for my 945GM graphics (not very powerful hardware). I was using 24bpp, but KDE4 was unbearable slow. After switching to 16bpp it feels much much better (speak \"normal\"). Only thing is, that the desktop background picture now has lost 90% of its colors... looks like a gif-pic in 16 colors ;-)"
    author: "Thomas"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-15
    body: "Perhaps it runs smooth when openGL stuff is turned off, but once 3D accelerated Kwin is turned on it becomes really slow.\nI bought a sony vaio SZ61XN four months ago when it was the newest model, and on that one KDE 4 is also slow. A lot slower than KDE 3.5.x, so let's hope they concentrate on improving speed, before adding any features to kwin."
    author: "Terracotta"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-13
    body: "I've used KDE 4 only for a few moments to peek around.\n\nIt seems most of the complaints are from Nvidia users using composite effects however."
    author: "T. J. Brumfield"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-13
    body: "The problem is not only with composite on. and not only with nvidia drivers.\nFor me for example, scrolling in all applications is very slow (not always, but most of the time), and this is with a ATI and another time with a nvidia. But i believe that that is a qt 4.3 problem and is hopefully solved with qt 4.4"
    author: "Beat Wolf"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-13
    body: "I had the same problem with my ATI card and fglrx but with radeonhd it runs much smoother. So I think it is indeed a driver problem."
    author: "hias"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-13
    body: "I think it is the \"organic\" feel - things fade in/out, nothing just appears/disappears.\n\nI guess that is intentional. Maybe the desktop ends up feeling friendlier, but slower. \n\nPossibly it will feel lighter with a different theme, or with some of the effects turned off.\n\nAlso, my feeling is probably dominated by the debug build...."
    author: "Brad Hards"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-14
    body: "It isn't hardware or responsiveness.\n\nYou are probably right about the fades. I wanted to mention it because another few days and I wouldn't notice it any more. I'm a terrible tester. I route around errors.\n\nDerek"
    author: "D Kite"
  - subject: "Re: I know this isn't the place, but"
    date: 2008-02-16
    body: "This might help you when it lands.  At least to see if the animations are the problem.\n\nhttp://lists.kde.org/?l=kde-core-devel&m=120310169819776&w=2"
    author: "Leo S"
  - subject: "Websites...."
    date: 2008-02-12
    body: "Is it really reasonable to put every application or technologies on a different site? \n\nIts freaking annoying, whats wrong with the regular kde.org site? \n\nYou got gwenview on this website, kross on this site, and then you got amarok on the kde website? Wtf!\n\nSorry, I'm kinda picky. "
    author: "Jeremy"
  - subject: "Re: Websites...."
    date: 2008-02-12
    body: "It would be nice if there was a standard. lets say: apps.kde.org/[app name] for individual apps. projects.kde.org for software collections (KOffice, KDE-PIM, KDE-EDU, KDE games, etc)"
    author: "yman"
  - subject: "Re: Websites...."
    date: 2008-02-12
    body: "Yes, thats what I wanted. Doing what we have now is confusing and needs some standard.\n\nI would be glad to help if thats needed.  "
    author: "Jeremy"
  - subject: "Re: Websites...."
    date: 2008-02-13
    body: "Yep, help is much appreciated.\nStart here:\n\nhttp://techbase.kde.org/index.php?title=Projects/kde.org/Capacity_HOWTO\nhttp://techbase.kde.org/index.php?title=Contribute/Get_a_SVN_Account\nhttp://websvn.kde.org/trunk/www/\nhttps://mail.kde.org/mailman/listinfo/kde-www\n\nYou'll need to get an okay from the maintainer of every involved app, of course."
    author: "logixoul"
  - subject: "whey hey!"
    date: 2008-02-14
    body: "kde 4 is delicious but somehow i can't help feeling there isn't enough cheese in it.  i'm thinking camembert, i'm thinking halloumi, i'm thinking stilton.  exciting cheese with lots of flavour, makes you tingle just thinking about it.  not like that gnome stuff which is just foot cheese, or the mac cheese which is fun with those apple chunks in it but is too prissy.  good solid cheese.\n\ni used to like windows cheese but it went mouldy.\n\nremember folks, more cheese = better!"
    author: "Intrepid Ungulate"
  - subject: "Re: whey hey!"
    date: 2008-02-14
    body: "And don't forget the Port to compliment the cheese !!!"
    author: "Ian"
  - subject: "Is KDE 4.1 going to run on Haiku?"
    date: 2008-02-14
    body: "Will KDE 4.1 be able to run on Haiku?\nOr will the applications run on it?\n\nFrom the little I know about it, it seems to have great potential. It's just missing an up to date window environments --> KDE to the rescue.. :)\n\nIt also seems to be a speed demon on slower computers. Maybe it is one on faster computers as well?\n\n\n"
    author: "Max"
  - subject: "Re: Is KDE 4.1 going to run on Haiku?"
    date: 2008-02-15
    body: "No idea.\nIs Haiku POSIX compatible ?\nThe windowing system may be a problem, it seems it doesn't have X. \n\nAlex\n"
    author: "Alex"
  - subject: "Re: Is KDE 4.1 going to run on Haiku?"
    date: 2008-02-15
    body: "http://video.google.com/videoplay?docid=236331448076587879&q=haiku&total=2759&start=0&num=10&so=0&type=search&plindex=0 (I'm guessing they're further ahead, as the video is about 1 year old now.)\n\nhttp://en.wikipedia.org/wiki/Haiku_(operating_system)\n\nDoes this help with info?\n\nIf I understand correctly Haiku is the open source version of BeOS. Apple almost used BeOS for OS-X, but decided to use Nextstep instead at the last minute.\n\n"
    author: "Max"
  - subject: "Re: Is KDE 4.1 going to run on Haiku?"
    date: 2008-02-19
    body: "If all goes according to plan, Haiku will be POSIX compatible, because BeOS was."
    author: "The Troy"
  - subject: "Any new updates?"
    date: 2008-02-16
    body: "Any new updates coming soon?\n\nIt's been pretty quiet around KDE and Windows KDE this week.\n\nYou guys are doing a great job!!\n\nJust please update us. :)"
    author: "Hi"
---
In <a href="http://commit-digest.org/issues/2008-02-03/">this week's KDE Commit-Digest</a>: Custom legend entries and the beginnings of the Mercator map projection (and evidence of exciting other things to come) in <a href="http://edu.kde.org/marble/">Marble</a>. Support for multiple online dictionaries and the start of a vocabulary <a href="http://plasma.kde.org/">Plasma</a> applet in <a href="http://edu.kde.org/parley/">Parley</a>. <a href="http://kross.dipe.org/">Kross</a> scripting engines (supporting various scripting languages) in Plasma, and the much-anticipated return of the ability to resize the panel. Support for multiple "Picture of the Day" providers in the "Picture Frame" Plasma applet. More work on the redesign (code and visuals) of KWorldClock. Work on theming improvements across <a href="http://games.kde.org/">KDE games</a>. Image information now displayed in fullscreen mode in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. Continued maintenance work in the <a href="http://www.kde.org/apps/kooka/">Kooka</a> scanning application. Support for HTML signatures in <a href="http://kontact.kde.org/kmail/">KMail</a>. Continued development on the IRC <a href="http://kopete.kde.org/">Kopete</a> plugin. Work on snap guides and a threaded tile backend in <a href="http://koffice.org/">KOffice</a>. A migration plugin for Sybase ASE in <a href="http://www.kexi-project.org/">Kexi</a>. Various efficiency improvements in KLinkStatus, <a href="http://kget.sourceforge.net/">KGet</a>, and some KDE games. KDE 4.0.1 (bugfixes) is <a href="http://dot.kde.org/1202239435/">tagged for release</a>. <a href="http://commit-digest.org/issues/2008-02-03/">Read the rest of the Digest here</a>.

<!--break-->
