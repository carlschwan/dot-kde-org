---
title: "Call for Hosts for Akademy 2009"
date:    2008-04-22
authors:
  - "cschumacher"
slug:    call-hosts-akademy-2009
comments:
  - subject: "yeah...cool.."
    date: 2008-04-22
    body: "> to hold Akademy and GUADEC, the GNOME community conference, in the same location\n"
    author: "Thomas"
  - subject: "Love & Peace!"
    date: 2008-04-22
    body: "Nice gesture of peace towards GNOME! Let's hope the gnommies accept. (But don't get too friendly, they're still rivals :P)"
    author: "Rsh"
  - subject: "Re: Love & Peace!"
    date: 2008-04-22
    body: "what do you mean by accept? they announced it too."
    author: "Emil Sedgh"
  - subject: "Re: Love & Peace!"
    date: 2008-04-23
    body: "My bad. I jumped over the details."
    author: "Rsh"
  - subject: "Re: Love & Peace!"
    date: 2008-04-23
    body: "I am a big KDE fan and I respect the Gnome project and it's user.\nbut I disagree is a good idea to hold both event together.\n\ngnome developers should be welcome at Akademy and KDE developers welcome at GUADEC, but that does not means we should do both at the same time.\n\ntotally disagree.\n\n"
    author: "free software user"
  - subject: "Re: Love & Peace!"
    date: 2008-04-24
    body: "It demonstrates solidarity, not in a few attending the other's conference, but in the leadership of both communities coming together so visibily.\n\nAnd I'm sure certain workshops will heavily touch on common ground.  Not to mention, holding two conferences and expecting people to attend both is a drain on the people attending both, asking for time away from work, travel money, etc.\n\nThis makes it much more feasible to attend both."
    author: "T. J. Brumfield"
  - subject: "Re: Love & Peace!"
    date: 2008-04-27
    body: "hi \"free software user\", a.k.a. anonymous coward..\n\nyou disagree but you give no reasons: poor mans posting styles..\n\nyou talk about \"should\", and \"agree\", but have you ever been to free software community conferences? to kde conferences?\n\nyr post is hollow imho.\n\n\ni think the joined conference is a great idea! this can truely bring the communities together. as many have pointed out: face to face contact is very important for large communities... (and that is a 'reason' dear free software user)"
    author: "cies breijs"
  - subject: "Re: Love & Peace!"
    date: 2008-04-27
    body: "Cies,\n\ndid you even closely read the announcement, with enough attention to grokk its *details*?\n\nNo-where does it mention a \"joined conference\" (or a \"joint\" conference, in case you mis-spelled what you meant).\n\nIt talks about co-hosting 2 conferences at the same place, during the same time, with possible over-lap in a few joint, common conference sessions. \n\nBut making it a reality (which I personally would like to see) depends on 3 things:\n\n* someone (or several) submits a proposal that can suite the requirements.\n* the GNOME community accepts the actual submission for their own part.\n* the KDE community accepts the actual submission too.\n\nIn reality it can still happen that GNOME receives a 2009 submission which turns out really compelling for them but that is limited to a GNOME-only conference. And the same can happen to KDE. And then it may occur that either one of the two communities picks to go for one of these, deciding by a majority vote (or whatever mechanism each community follows to reach a decision about their annual get-together event location)."
    author: "Repre Hensor"
  - subject: "Thanks!"
    date: 2008-04-22
    body: "Thanks for all those in the background who worked out the joint announcement and the details behind it! If it works out and a common location can be found, that's for sure a very good thing for both desktops."
    author: "Daniel Molkentin"
  - subject: "fee"
    date: 2008-04-22
    body: "I all for closer ties with the gnome crowd however, as I understand it, currently GUADEC has an entrance fee and Akademy doesn't so won't this limit the interaction between the two conferences somewhat, rather defeating the point of having both in the same location?\n \nhttp://dot.kde.org/1186422013/\n\n\nd. "
    author: "dr"
  - subject: "Re: fee"
    date: 2008-04-23
    body: "I'm going to be cynical and say that'd work well for KDE. It would encourage GNOME folks to casually check out KDE on \"KDE turf'-- So that's all upside. :)"
    author: "James Spencer"
  - subject: "Re: fee"
    date: 2008-04-23
    body: "I'm sure we could find a solution for this problem. It's not that GUADEC tries to make money from contributors.\n"
    author: "Cornelius Schumacher"
  - subject: "In Spain?"
    date: 2008-04-23
    body: "I would be really COOL if it was hosted in Spain. And, if the selected place is Granada, even better :D"
    author: "gskbyte"
  - subject: "Re: In Spain?"
    date: 2008-04-23
    body: "Akademy was at Malaga/Spain 3 years ago (2005).\nI guess other countries in Europe (what about other continents) should be first, before Spain should get a second \"chance\".\nProvided there are appropriate locations...\n\nJust my 0.02\u0080 ;-)"
    author: "Harald Henkel"
  - subject: "Re: In Spain?"
    date: 2008-04-23
    body: "\u00a1Hola! We're putting together a proposal for hosting the conference in Gran Canaria, which is pretty Spanish even if not on the mainland. The infrastructure is good enough to be able to support a Gnome conference in the same location too."
    author: "Richard Dale"
  - subject: "Re: In Spain?"
    date: 2008-04-27
    body: "in asia!\n\neurope is a bit kde's groud, the states a bit gnome's ground -- asia is where both will have to expand their user base in the near future, so..\n\nno really, i think this can really work, and besides the fight costs (that are then more/less the same on average for kde and gnome attendees) are to be earned back by cheaper food and accommodation.\n\nmaybe a country is interested i hosting it and supporting it as it will have a lot of spin off."
    author: "cies breijs"
  - subject: "Incredible News"
    date: 2008-04-23
    body: "I think this is incredible news quite frankly."
    author: "T. J. Brumfield"
  - subject: "happy to hear my prophecy come true "
    date: 2008-04-23
    body: "\ni am really happy that it comes true but one year later, great move ;)\nhappy to hear my prophecy come true\nhttp://dot.kde.org/1186422013/.\n\nok next prophecy, Akademy will be held in the not near future in Africa,(at least you will get some sun, poor kde and gnome developers)"
    author: "mimoune djouallah"
  - subject: "Re: happy to hear my prophecy come true "
    date: 2008-04-23
    body: "If by prophecy you mean that you \"read it somewhere\". :P"
    author: "Ian Monroe"
  - subject: "Re: happy to hear my prophecy come true "
    date: 2008-04-23
    body: "I'd be thrilled to see your proposal for hosting the two conferences in Africa in my board inbox. :)"
    author: "sebas"
  - subject: "Trolltech Dev Days + aKademy"
    date: 2008-04-25
    body: "Combining Guadec and aKademy scares me a bit, I fear the KDE presence might get absolutely diluted. The size difference between the two conferences is dismal, we are talking about a 10:1 factor here. \n\nPeople attending both conferences are actually very different, too: there are lots of suits attending Guadec because it's a combined Gtk+ and Gnome conference. People from companies attending Guadec are not there for the Gnome part of it but for the Gtk+ part of it. Just take a look, do you know any commercial Gnome application? I don't know of any. But I know lots of commercial Gtk+ applications.\n\nWhat I think we should do is to combine aKademy with the Trolltech Developer Days.  IMHO what would make much more sense: lots of Qt developers do NOT know KDE exists at all. Given that KDElibs is LGPL, we might get some converts from those people, after all, for them KDElibs is just a new \"module\", just like Phonon, QtWebkit or QtSQL.\n\nTrolltech holds Developer Days in America and in Europe. From 2009 on there will be aKademy in America and in Europe. I'd say the match is perfect.\n\nWe could then have a FreeDesktop.org Conference as a meeting point not only between Gnome and KDE but between every X Window FLOSS desktop. That's exactly what <a href=\"http://www.guademy.org\">Guademy</a> is trying to achieve.\n"
    author: "Pau Garcia i Quiles"
  - subject: "Re: Trolltech Dev Days + aKademy"
    date: 2008-04-25
    body: "Combining aKademy and Trolltech's Developer Days at least location wise would be great indeed."
    author: "Anon"
  - subject: "Re: Trolltech Dev Days + aKademy"
    date: 2008-04-27
    body: "good point..\n\ni just dont really see how the 10:1 ratio is bad for kde, or good for gnome. (or the other way around)\n "
    author: "cies breijs"
  - subject: "GUADEC"
    date: 2008-05-05
    body: "The 10:1 factor is absolutely not correct.  GUADEC is ~400 people. Akademy is between 100 and 200 from what I hear.\n\nAnd there are no suits at GUADEC.  Have you ever been to any?  I've been to two GUADECs and one Akademy.  Believe me, you see more people with hangovers at GUADEC.  *That* may be a problem, but suits, no."
    author: "behdad"
---
Preparations for <a href="http://akademy2008.kde.org">Akademy 2008</a> are in full swing, but KDE e.V. is already looking forward to next year and asks potential hosts to submit proposals for Akademy 2009. For the first time we also invite proposals to hold Akademy and GUADEC, the GNOME community conference, in the same location. More information can be found in the <a href="http://ev.kde.org/akademy/callforhosts.php">Call for Hosts for Akademy 2009</a> below or in the <a href="http://ev.kde.org/announcements/2008-04-22-akademy-guadec.php">joint press release of KDE e.V. and the GNOME Foundation</a>.



<!--break-->
<h3>Call for Hosts for Akademy 2009</h3>

<p>
KDE e.V. invites proposals to host Akademy, the yearly KDE contributors
conference and community summit, during the Summer of 2009. Akademy is the
biggest gathering of the community and includes a two-day conference, the
general assembly of the members of KDE e.V., and a week of coding,
meeting, and discussing.
</p>

<p>
For the first time we also invite proposals to hold Akademy and GUADEC, the
GNOME community conference, at the same time, in the same location.
Proposals should detail plans for hosting up to 800 attendees, with separate
facilities available for Akademy and GUADEC planned sessions.
</p>

<p>
The conferences should be held independently, in the same location, with
facilities available for joint sessions of interest to both communities. A
co-hosted event would constitute one of the largest events on the planet
dedicated to free software on the desktop.
</p>

<p>
Proposals for Akademy should be sent to kde-e<span>v-boa</span>rd@kde.org, proposals for a
co-hosted event should be sent to boa<span>rd@gno</span>me.org as well. The deadline for
proposals is June 15th 2008.
</p>

<p>
Key points which proposals should consider, and which will be taken into
account when deciding among candidates, are:
</p>

<ul>
<li>Local community support for hosting the conference</li>
<li>The availability and cost of travel from major European cities</li>
<li>The availability of sufficient low-cost accommodation</li>
<li>The budget for infrastructure and facilities required to hold the
conference</li>
<li>The availability of restaurants or the organisation of catering on-site</li>
<li>Local industry and government support</li>
<li>Ability to organise sponsorship</li>
</ul>

<p>
See the <a href="http://ev.kde.org/akademy/requirements.php">detailed requirements</a> on
the KDE e.V. web site for more information about what it needs to host
Akademy.
</p>

<p>
The conference will require availability of facilities for one week,
including a weekend, during Summer. Dates should avoid other key free software
conferences.
</p>

<p>
A few words of advice: organising a conference of this size is hard work, but
there are many people in the community with experience who will be there to
help you. Bear in mind that people coming to these conferences do so
primarily to meet up with old friends and have fun, and so the hallway track
and social activities are very important.
</p>





