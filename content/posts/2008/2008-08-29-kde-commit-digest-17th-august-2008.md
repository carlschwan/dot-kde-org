---
title: "KDE Commit-Digest for 17th August 2008"
date:    2008-08-29
authors:
  - "dallen"
slug:    kde-commit-digest-17th-august-2008
comments:
  - subject: "Hurray!!"
    date: 2008-08-29
    body: "\"Code completion for PHP in KDevelop.\"\n\nI miss quanta+ soooooooo much in KDE4... at least they started something, but I'm really missing a good PHP editor for KDE4, as Quanta+ is already showing it's age :-P\n\nMaybe I should start looking for some cross-plataform editor. Does anyone have a suggestion?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Hurray!!"
    date: 2008-08-29
    body: "Vim :P"
    author: "slacker"
  - subject: "Re: Hurray!!"
    date: 2008-08-29
    body: "Try Eclipse (with appropriate PHP Plugins) or Netbeans. Work both really well and reliable."
    author: "alterego"
  - subject: "cross-plataform editor"
    date: 2008-08-29
    body: "Netbeans 6.5 (currently beta) is the best thing around, trust me.\n(yes, supports php, ruby, c, c++, java, etc)"
    author: "Mariano"
  - subject: "Re: cross-plataform editor"
    date: 2008-08-29
    body: "Does it support HTML tag editing also?\n\nThanks."
    author: "Iuri Fiedoruk"
  - subject: "Re: cross-plataform editor"
    date: 2008-08-29
    body: "yes, with code completition."
    author: "Mariano"
  - subject: "Re: Hurray!!"
    date: 2008-08-29
    body: "Escuse me, but first post ALWAYS thanks Danny for the effort he puts in. You did not. Shame on you. Thanks Danny!"
    author: "ne..."
  - subject: "Re: Hurray!!"
    date: 2008-08-29
    body: "Heh. Dear Sir, THANK YOU for reminding us to thank Danny."
    author: "Mark Kretschmann"
  - subject: "Re: Hurray!!"
    date: 2008-08-29
    body: "The thanks for Danny is a default and implicit, I do not need to verbalize it every holy time ;)\n\nAnyway, Thanks Danny!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Hurray!!"
    date: 2008-08-30
    body: "+1 Thanks Danny!\n"
    author: "Michael \"Just another grateful luser\" Howell"
  - subject: "Re: Hurray!!"
    date: 2008-08-30
    body: "KDevelop is going to be cross-platform, along with a lot of the other KDE applications."
    author: "Michael \"KParts\" Howell"
  - subject: "MythTV engine"
    date: 2008-08-29
    body: "\"A MythTV data engine for retrieving data about a MythTV installation (upcoming recordings, etc)\"\n\nThat sounds really sweet!!!"
    author: "Phil"
  - subject: "Re: MythTV engine"
    date: 2008-08-29
    body: "Yeh, that'll rock!"
    author: "Mark"
  - subject: "gwenview"
    date: 2008-08-29
    body: "i'd like to see gwenview being able to show svg images. atm it ignores them, which is sad, as otherwise it rocks!"
    author: "Name"
  - subject: "Re: gwenview"
    date: 2008-08-29
    body: "+1\n\nadding animated gifs is good, but a bit late already, it's something that an image view needs to implement from the beggining, but I do like gwenvew a lot, so thanks anyway ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: gwenview"
    date: 2008-08-30
    body: "It does already (even if in a really basic way), installing the SvgPart from kdegraphics."
    author: "Pino Toscano"
  - subject: "removing annotations in Okular"
    date: 2008-08-29
    body: "\"removing annotations in Okular\"\nWell it seems to me this is not something good. I like Okular because I can annotate. Why is it removed or do I misinterpretate it totally?"
    author: "Holger F."
  - subject: "Re: removing annotations in Okular"
    date: 2008-08-29
    body: "I think this means that okular can now add _and_ remove annotations, not that the annotation code is removed from okular..."
    author: "Erik"
  - subject: "Re: removing annotations in Okular"
    date: 2008-08-29
    body: "It means \"support for removing annotations\" in Okular ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: removing annotations in Okular"
    date: 2008-08-29
    body: "Don't sweat it, if I read the actual commit description, it seems it adds support for users removing annotations to PDF's, not the removal of the annotation feature from Okular. "
    author: "Andr\u00e9"
  - subject: "Re: removing annotations in Okular"
    date: 2008-08-30
    body: "> do I misinterpretate it totally?\n\nYes.\nThe commit actually slightly improves the popup menu (used also for removing annotations) in the annotation side tree.\nNothing of the previous functionalities has changed."
    author: "Pino Toscano"
  - subject: "Re: removing annotations in Okular"
    date: 2008-09-01
    body: "\"> do I misinterpretate it totally?\n \n Yes.\"\n\nWorld is back to normal now."
    author: "Holger F. "
  - subject: "digiKam: new RAW Import tool..."
    date: 2008-08-29
    body: "About Darkroom program, i can see this comment:\n\n\"I see Digikam as good for managing my collection of pictures, and quickly do some light editing,... but it seems unable to open RAW files at this time.\"\n\nREALLY... darkroom use libkdcraw maintened by digiKam team. \n\ndigiKam support RAW files of course and since a very long time... \n\nWith digiKam 0.9.5 (KDE3) and new 0.10.0 (KDE4) a new RAW Import tool have been added to customized indeep all RAW decoding and post processing settings... \n\nMore info (screencast) are here :\n\nhttp://www.digikam.org/drupal/node/370\nhttp://www.digikam.org/drupal/node/365\nhttp://www.digikam.org/drupal/node/364\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: digiKam: new RAW Import tool..."
    date: 2008-08-29
    body: "RIP Darkroom."
    author: "ohman"
  - subject: "Re: digiKam: new RAW Import tool..."
    date: 2008-08-29
    body: "If you read well what is said in the sentence, and if you avoid using elipses to remove part of the quotation, you will notice that the part about opening raw images concern gwenview... and not digikam..."
    author: "Cyrille Berger"
  - subject: "Sweet feature"
    date: 2008-08-29
    body: "Cornelius Schumacher wrote about the Akonandi clock. http://www.kdedevelopers.org/node/3647\n\nThat's a sweet feature. Expanding this thought I think it would be nice to have documents presented in a similar fashion. Quite often it's easier to remember when you were writing a document rather than what the name was. Since most data is already there (in unix) via the ctime, mtime and atime it should be quite easy to create.\n\nAnd on the same lines it would be nice to be able to see contacts in a timeline. If you're diciplined and add your contacts as you get them you'll be able to see who you connected to and when. And it would help with the question \"What's the name of that guy that I met about a month and a half ago?\".\n\nI should probably refine these ideas some more and add them to the KDE bugtracker... "
    author: "Oscar"
  - subject: "Kdisplay"
    date: 2008-08-29
    body: "i like it. but is the color configurable or changed automatically based on the current schema? in a full red color schema the blue of the osd wouldn't be very good ;)"
    author: "john"
  - subject: "Re: Kdisplay"
    date: 2008-08-30
    body: "I like it too, but what a bad name. Was KOSD taken already? I bet there are applications out there that are already using KDisplay for something totally unrelated to displaying the osd..\n"
    author: "indev"
  - subject: "Re: Kdisplay"
    date: 2008-08-31
    body: "http://www.kde-apps.org/content/show.php/KOSD?content=81457 :)\n(btw yes, the name kosd is nicer than kdisplay and create less confusion (kdisplay remember something to actually regulate the display and other stuff) ;) )"
    author: "john"
  - subject: "Re: Kdisplay"
    date: 2008-08-31
    body: "How about oKsd (pronounced Ox-d) then? Sort of a \"On (kde) screen display\".\n\nJust a thought."
    author: "Oscar"
  - subject: "Re: Kdisplay"
    date: 2008-08-31
    body: "Yes the colour automatically obeys the color scheme, that blue comes from oxygen :) if you use the library in your app directly then you can call setPalette() and set whatever you want as well, or just let the osd handle the colors itself.\n"
    author: "Ben Cooksley"
  - subject: "new NVIDIA drivers!"
    date: 2008-08-30
    body: "This may be a little off-topic, but for those who experience performance problems using KDE4 on NVIDIA driven computers it should be worth reading that NVIDIA just released new beta drivers (177.70). This is the first release that makes my KDE installation work smoothly. \n\nDownload link:\nhttp://www.nvnews.net/vbulletin/showthread.php?t=118602\n\nInstructions:\nhttp://www.nvnews.net/vbulletin/showthread.php?t=118088"
    author: "Sebastian"
  - subject: "Re: new NVIDIA drivers!"
    date: 2008-08-31
    body: "Just as I started thinking about buying a new motherboard with an Intel GMA chip. I am not having a very smooth ride with nVidia on KDE4 because it just doesn't deliver the power that it has. Compiz-Fusion feels snappier though. I just hope that the new driver will be much better than the present one."
    author: "Bobby"
  - subject: "Notifications"
    date: 2008-08-31
    body: "Hi\none of the things i already wanted was a OSD like notification for whole KDE.\nsince OSD is just a notification system, i'd really like to see it as a new 'KDE Notification' so i can choose any kind of notification i want as OSD in System Settings->Notifications.\n\none of the advantages would be that Amarok will remove its own OSD and will use KDE's OSD (kdisplay) and also as a user i will be able to choose Amarok's notifications as normal notifications.\nIm adding a screenshot to show what i mean."
    author: "Emil Sedgh"
  - subject: "Re: Notifications"
    date: 2008-08-31
    body: "hm, dot is just allowing one attachment for each comment, here is another attachment just to clarify what do i mean."
    author: "Emil Sedgh"
  - subject: "Re: Notifications"
    date: 2008-08-31
    body: "already been asked bout that on the lists. a knotify module is already in the works, already in svn. using the kosdwidget lib that i created making kdisplay."
    author: "Ben Cooksley"
  - subject: "Re: Notifications"
    date: 2008-09-02
    body: "i hope you have reply notifications enabled ;)\ni don't have kde4 installation currently, so i don't know the existing functionality of kdisplay.\n\n1. as for feature ideas, it would be nice to have simpler osd that closes on click (as described currently), and another osd, that does not close on click, but can be dragged to other location (like k3b progress bar).\nok, that reminds me of some early kde4 idea mockup, where a reminder from a systray was dragged on a desktop as a note... what about an ability to move notification into systray and back out ? some sort of integration with that unified progress applet, what-was-its-name... so one would treat those progress bars as a notifications that can be attached, detached and moved freely around zones, including panel (i wrote kicker at first :) ), systray, desktop.\nicons to control such peristent notification closure and moving could be similar to current selector in konq/dolphin singleclick mode - they would appear only upon mouseover (probably somewhat like plasma applet handles then).\n\nhey, you said you are out of ideas ;)\n\n2. how does the current popup position work ? that is, do users set popup location for each app ? what if two popups want to appear overlapping, will they be repositioned so that both are visible ?\n"
    author: "richlv"
---
In <a href="http://commit-digest.org/issues/2008-08-17/">this week's KDE Commit-Digest</a>: New "Browser History", "Konqueror Sessions", "Konsole Sessions", and "Kate Sessions" KRunners in <a href="http://plasma.kde.org/">Plasma</a>. Proof-of-concept of simple uploading in Plasmagik. A MythTV data engine for retrieving data about a MythTV installation (upcoming recordings, etc), and the start of a <a href="http://www.rsibreak.org/">RSIBreak</a> engine. An applet for displaying new message information from <a href="http://kontact.kde.org/kmail/">KMail</a>, <a href="http://kopete.kde.org/">Kopete</a>, etc for use with the Plasmoids-on-Screensaver project. Support for panel form factors, and a configuration dialog in the <a href="http://ivan.fomentgroup.org/blog/2007/10/27/lancelot-revealed/">Lancelot</a> alternative menu. Various improvements in the "Desktop Grid" KWin-Composite effect. More bugfixes for Kicker in KDE 3.5. A backtrace browser plugin for <a href="http://kate-editor.org/">Kate</a>. Code completion for PHP in <a href="http://www.kdevelop.org/">KDevelop</a>. More levels added in the <a href="http://stepcore.sourceforge.net/">Step</a>game project. Lots of improvements in KGo, support for themes in Kapman. Window title tagged images in KSnapshot to assist indexing by <a href="http://strigi.sourceforge.net/">Strigi</a>. Support for reading form actions and removing annotations in <a href="http://okular.org/">Okular</a>. Animated image support (eg. GIF) in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. First steps towards a <a href="http://www.mailody.net/">Mailody</a> <a href="http://www.kontact.org/">Kontact</a> part. More work for <a href="http://amarok.kde.org/">Amarok</a> 2.0, especially regarding playlist handling. Start of a PDF import filter (for <a href="http://koffice.org/">KOffice</a> 2.1, using Poppler) in <a href="http://koffice.kde.org/karbon/">Karbon</a>. Initial <a href="http://kross.dipe.org/">Kross</a> integration in the Shaman package manager. More user interface work in KColorEdit. New device notifier moved into kdebase, new KsCD moved into kdemultimedia. Removal of viewer functionality in <a href="http://pim.kde.org/components/kpilot.php">KPilot</a> to become a syncing application only. Import of <a href="http://kaffeine.sourceforge.net/">Kaffeine</a> video player into extragear. Tagging of Amarok 1.4.10, a security fix release. <a href="http://commit-digest.org/issues/2008-08-17/">Read the rest of the Digest here</a>.

<!--break-->
