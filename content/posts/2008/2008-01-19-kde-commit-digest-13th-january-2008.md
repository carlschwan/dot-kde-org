---
title: "KDE Commit-Digest for 13th January 2008"
date:    2008-01-19
authors:
  - "dallen"
slug:    kde-commit-digest-13th-january-2008
comments:
  - subject: "Cool"
    date: 2008-01-19
    body: "Thanks Danny, you're the man.\n\nAnd KWin is awesome in KDE4.0.  Even in this early state, it works a lot better for me than compiz ever did (even recent versions).  It makes way more sense to add composite support to a window manager that is actually smart than try to put the smart into compiz."
    author: "Leo S"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "I have the opposite experience (Compiz was quite smooth, KWin incredibly jerky) but I have faith in the KWin developers that it will improve quickly. They also seem to have a better sense of what \"effects\" are useful (e.g. the keyboard filtering in Present Windows)."
    author: "kwilliam"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Could it be that you have a nVidia card and are having driver issues? I had the same problem with my nVidia care but after applying a so-called Hack (that was mentioned here) and doing a few adjustments in the xorg.conf file the effects are now very smooth.\nOn the other hand I can't get Compiz-Fusion to run with Xorg on KDE 4.0. It seems to need XGL, which KDE 4.0 seems to be allergic to. I also have problems with Kaffeine when using Compiz-Fusion."
    author: "Bobby"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Can you point us to the hacks you made? Please :)"
    author: "Jorge S. de Lis"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "KWin release notes for KDE4.0, see the right section : http://techbase.kde.org/Projects/KWin/4.0-release-notes . I don't know if there are any other possible improvements, so I don't know what those few adjustements could be, but I can add more if people tell me them.\n"
    author: "Lubos Lunak"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Written by Marcel Partap in thread The Start of Something Amazing:\n(attention: put 'export KWIN_NVIDIA_HACK=1' in your /etc/profile or so if you have an nvidia card, it does make a huge difference!)\n\n\nAlso add this to your xorg.conf file:\n\nSection \"Extensions\"\n    Option         \"Composite\" \"Enable\"\nEndSection\n \nAnd make sure that you are using the latest nvidia driver.\nYou should tell us if it worked for you.\n"
    author: "Bobby"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "I think the posted referred to this discussion:\n\nhttp://dot.kde.org/1200050369/1200126492/"
    author: "Diederik van der Boor"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "I agree.  I'm not ready to fully migrate to KDE4 yet, but I've switched to the KDE4 versions of several apps, including kwin.  Other than some weirdness with the password dialog in kscreensaver, and the fact that setting KDEWM doesn't seem to work right, so I have to launch it manually, it's great."
    author: "pantsgolem"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "If the rotating transparent cube and the wobbly windows get implemented in kwin, i'm sure 90% of compiz users would switch."
    author: "Fred"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Everyone wants wobbly windows! It isn't rational, it is a simply human ;) But keep the wobbling very stiff by default, so as to be noticeably, but no more than that.\n"
    author: "Esben Mose Hansen"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Somebody needs to do those :). We decided not to do those for 4.0 because of expected bad gain/effort ratio. They will be probably technically a bit harder to implement than others, and I'm not personally very convinced of their usefulness - e.g. the cube is good for giving non-advanced users a way to grasp the concept of virtual desktops, and for the hype of course (since compositing _is_ the cube, right?), but I was getting motion sickness rather quickly every time I tried to use it, and I had problems to actually find what I was looking for (especially with a higher number of desktops or the additional stuff enabled like cube-from-inside or transparent-cube). So they will be done when somebody finds time to do that, and that can be also you.\n\nPS: The parts about KWin in this commit digest are from KWin's release notes for KDE4.0 : http://techbase.kde.org/Projects/KWin/4.0-release-notes .\n"
    author: "Lubos Lunak"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Well, it was definitely the right decision to postpone those features, as they're really just eyecandy, nothing more. I love the composite features that are in KDE4.0, but apart from maybe a Vista window switcher (I love that one!), I don't think I need anything else. Please concentrate more on speed, it's way more important IMO!"
    author: "Thomas"
  - subject: "Re: Cool"
    date: 2008-01-20
    body: "didn't the Compiz Application Switcher come before the Vista one?\n\nanyway, Wobbly Windows make moving windows around feel much better. it's like the movement is much more... natural."
    author: "yman"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Those were my favourites too + the shiver effect of drop-down menus etc.."
    author: "Bobby"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "When I set up comiz (or beryl or whatever it was then), I had to use wobbly windows, because it was the only way to get windows to snap to edges. Also, the stiffer the windows, the less they would snap.\n\nThat's when I decided KWin was more practical. That wasn't the only reason though, there were also desktop issues, and pager issues, and hotkey issues, etc. "
    author: "Soap"
  - subject: "Re: Cool"
    date: 2008-01-21
    body: "I would!!!\n\nWell I'm using KDE 4.0 already, but I really am missing the: CUBE, EXPOSE, COVERFLOW, MAGIC LAMP\n\nAnd yes.. Compiz ran smoother on this machine than Kwin does.. I'm patient though.. I know you guys will come around and fix this..\n\nAll in due time.. (I hope... :-/  )"
    author: "Max"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Stay serious, Compiz is 20 times more performant than this KDE 4 thing and has 10 times better effects."
    author: "abc"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "20 times, 10 times ... speaking of staying serious, how about you tried to lead the way?\n"
    author: "Lubos Lunak"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "20 times is the sad reality. \nI don't know why.\n\n10 times is subjective impression."
    author: "abc"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "And KDE 4 is 30 times better integrated in KDE :) Some of Compiz's effects are also over-kill."
    author: "Bobby"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "I mean Kwin4 is better integrated in KDE4."
    author: "Bobby"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "KWin4 is a game *giggles* but yes :)\n\n(You're looking for KWin Composite ;) )"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Not any more - KWin4 the game was renamed to KFourInLine in order to prevent this clash :)\n\nhttp://websvn.kde.org/?view=rev&revision=748209"
    author: "Anon"
  - subject: "Re: Cool"
    date: 2008-01-20
    body: "It's not a game anymore, it's now called KFourInLine ;)"
    author: "Bobby"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "\"Stay serious, Compiz is 20 times more performant than this KDE 4 thing and has 10 times better effects.\"\n\nWhat on Earth do you think Compiz [Fusion] or Beryl were like when they first started? It's also not being developed as part of a desktop project though, but rather as a separate window manager. Additionally, there are also a lot of corner cases and situations where Compiz doesn't work well, which negates its usage as a day-to-day window manager - no matter how it performs or how many effects it has."
    author: "Segedunum"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "And KWin is a 50x better Window Manager than Compiz, which really is its primary function right???"
    author: "Odysseus"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "I am serious.  Kwin performs fine for me (Geforce 7900 something) and the effects are nicer (present windows+filter, switching desktops, show desktops, etc).  Compiz (0.6.2) had lots of issues with window placement, and weird bugs like wobbly windows never stopping to wobble.  The only thing that I miss from Compiz is a zoom function that can be controlled with the mousewheel (the zoom in Kwin doesn't work for me)."
    author: "Leo S"
  - subject: "Re: Cool"
    date: 2008-01-19
    body: "Thanks from me too Danny. Well done. I just hope that the whole thing about Compiz and Kwin4 is now clarified.\n\n@ leo S: I am a big fan of Compiz-Fusion but I have to confirm what you said. Compiz-fusion is amazing but it doesn't integrate in KDE very well. Kwin4 at this point doesn't have the glitz, glamour and flair of the ingenious Compiz-Fusion (although I am convinced that it will come in due time) but what it does it does it very well and it's perfectly integrated in KDE :)"
    author: "Bobby"
  - subject: "Also Yakuake"
    date: 2008-01-19
    body: "Thanks for that Eike.  For a few weeks I had to live without it as 2.8 crashed in KDE4.0, but now 2.9 beta is in Debian experimental and I have my Yakuake back!  What a great app that is, and I haven't seen any bugs yet."
    author: "Leo S"
  - subject: "Re: Also Yakuake"
    date: 2008-01-19
    body: "Odd, though - I used the 2.8.x codebase in KDE 4 for quite a bit of time, and it shouldn't have nor did it crash here :-). Anyhow, a native KDE 4 codebase is a better fit of course. Glad you like it! :)"
    author: "Eike Hein"
  - subject: "Re: Also Yakuake"
    date: 2008-01-19
    body: "Yeah I don't know what was going on.  But it immediately crashed on start with 3 machines.. They were all running debian unstable though.  \nThe only issue I have with Yakuake 2.9 is that it takes a while (~0.5 second) to appear when I hit F12 (even with zero animation time).  But that's an issue with KWin I think, because I only see it when I turn compositing on."
    author: "Leo S"
  - subject: "Re: Also Yakuake"
    date: 2008-01-19
    body: "There's also a known speed problem between Qt 4 and nVidia's proprietary graphics driver's XRender implementation, especially when using non-scalable bitmap fonts in Konsole's terminal widgets (which are also used in Yakuake). Check out the KDE4FAQ file I put in the tarball, there's a bit about that in there :)."
    author: "Eike Hein"
  - subject: "Re: Also Yakuake"
    date: 2008-01-19
    body: "This is one of my favorite KDE apps!  Thanks a whole bunch!"
    author: "T. J. Brumfield"
  - subject: "Re: Also Yakuake"
    date: 2008-01-20
    body: "Yakuake did, for me, never crash in a KDE4 session. The only issue I had is that it was looking quite odd if console transparency was enabled without a running kdesktop instance (meaning that the blinking caret stayed there and filled the console white; fortunately I can type blind)."
    author: "Stefan"
  - subject: "Re: Also Yakuake"
    date: 2008-01-20
    body: "At least as far as Yakuake's UI elements are concerned, this is something that 2.8.1 (the new KDE 3 release released alongside the KDE 4 version, and probably the last KDE 3 release ever) actually fixes as well by checking for whether kdesktop is present and falling back to a configurable solid BG color in the event that it isn't - also relevant for non-KDE users. In the KDE 4 version the situation is different because the pseudo-translucency support is gone in favor of XComposite translucency (which is toggleable as well, however)."
    author: "Eike Hein"
  - subject: "Re: Also Yakuake"
    date: 2008-01-21
    body: "Question: will/has Yakuake configurable tab labels just like Konsole4? I mean, with variables like hostname, user etc."
    author: "Vide"
  - subject: "Re: Also Yakuake"
    date: 2008-01-21
    body: "Yep, it does have those."
    author: "Eike Hein"
  - subject: "Re: Also Yakuake"
    date: 2008-01-22
    body: "Great! It's one of my personal killer features in Konsole4, and I'm a yakuake freak, so... :D\nGreat job again, yakuake is a godsend for every unix sysadmin out there :)"
    author: "Vide"
  - subject: "kio-gio bridge"
    date: 2008-01-19
    body: ">>Initial import of kio-giobridge, a bridge \n>> interface between the KDE and GNOME I/O methods\n\nWhat exactly this means? Can somebody please explain it?\n\nThanks in advance.\n"
    author: "ohcalcutta"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-19
    body: "From what I can consider, this could know that Gnome progs can use the various KIO slaves from fish:/ to audiocd:/, and that KDE programs can use Gnome I/O."
    author: "Stefan"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-20
    body: "The plan is to have a \"common vfs layer\" for all applications and file-managers, when working with network protocols like smb, ftp, sftp, webdav and others. The new GIO/GVFS (the successor of Gnome-VFS) pretty much fulfills the requirements for such a \"common vfs layer\" (desktop/toolkit independent design). \n\n\"kio-giobridge\" is an adapter to run KIO on top of GIO/GVFS, in order to resolve file-management inconsistencies between KDE apps, and Gtk+ or 3rd party applications like Openoffice, Mozilla,...\n\n\n"
    author: "Norbert"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-20
    body: "I don't understand. can someone please give me a more dumbed-down explanation?"
    author: "yman"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-20
    body: "Some Gnome developer are working on something called GIO/GVFS, a subsystem delivering functionality akin to KIO. Making it possible to use things like ftp:/, smb:/ in filedialogs. Something KDE users has been able to for years with KIO. \n\nThe \"kio-giobridge\" makes it possible for KDE programs to using GIO/GVFS. Of course in places where KDE already have native IOSlaves handling the protocoll, it does not make much sense for the users.\n\n"
    author: "Morty"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-20
    body: "> The \"kio-giobridge\" makes it possible for KDE programs to using GIO/GVFS. Of \n> course in places where KDE already have native IOSlaves handling the protocoll, \n> it does not make much sense for the users.\n\nWell, the idea behind \"kio-giobridge\" is that it does make sense to switch off KDEs native IOslaves and to use the shared implementation instead. This is because GIO/GVFS works similar to UNIX mounts: You mount a certain ftp server or smb share and all applications/file-managers can use them without logging in again. The servers/shares stay mounted until you hit the unmount button. Therefore working with network file systems should feel a lot more coherent. You don't have to log in twice when working with KDE apps and Gtk/3rd party apps on the same shares and you don't have to pray that KIOs and GIOs protocol handlers are functionally equivalent. \n"
    author: "Norbert"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-22
    body: "Except the implementation is not shared. It is just a crappy GNOME attempt to copy KDE. So let's stick to our own implementation instead of promoting second-rate copy-cat implementations."
    author: "Allan Sandfeld"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-22
    body: "If there's this \"mount like\" features, it's definetely better than KIOslaves, at least from an architectural PoV. The fact that KIOslaves are KDE-only and so from the shell or whatever program not using kdelibs (even Qt ones!) you cannot use them directly, sucks so hard... don't get me wrong, if KDE ruled the world, KIOslaves would be the non plus ultra, but since biodiversity is something directly bind with FLOSS, than we have a problem.\nThe best Linux implementation should be using FUSE, but then we have a problem with other platforms. Don't know exactly how GFS is solving this, but if they can get a mounted point transparent to the system, then they have a winner.\n(I mean, the clear example here is OSX, which has the best way to implement remote access)"
    author: "Vide"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-22
    body: "KIO slaves are no more KDE specific than GIO is gnome-specific --- so what's your point? Actually, you don't have to link to KDE libraries to use I/O slaves --- they are out of process, w/a dbus + socket protocol, so there is absolutely nothing preventing one from writing implementations of slaves or client libraries w/o using KDE libraries.\n\nP.S. KIO does a lot more than VFS, too. And having two overlapping systems used for the same codebase is freaking insane.\n"
    author: "SadEagle"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-22
    body: "> KIO slaves are no more KDE specific than GIO is gnome-specific --- so what's your point? Actually,\n> you don't have to link to KDE libraries to use I/O slaves --- they are out of process, w/a dbus +\n> socket protocol, so there is absolutely nothing preventing one from writing implementations of\n> slaves or client libraries w/o using KDE libraries.\n\nWell - in theory KIO could be implemented desktop independent, that's true. But in praxis rewriting the client and daemon/slave parts isn't easy. As GIO/GVFS already is desktop independent in the implementation, and also seems to have some advantages in design, why not go for it?\n\n> P.S. KIO does a lot more than VFS, too. And having two overlapping systems used for the same\n> codebase is freaking insane.\n\nWhy is it overlapping? Parts of the functionality in KIO (particularly network transparent file-management like smb, ftp, sftp, webdav) would be delegated to GIO/GVFS. That's just putting the KIO interface on top of a shared implementation.\n\n\n"
    author: "Norbert"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: "> Well - in theory KIO could be implemented desktop independent, that's true. > But in praxis rewriting the client and daemon/slave parts isn't easy. As > GIO/GVFS already is desktop independent in the implementation, and also seems >to have some advantages in design, why not go for it?\n\nFirst of all, there is no reason to rewrite anything. Multiple implementations can talk on the same wire protocol.  And GIO is \"Desktop independent\"? That's just nonsense, to put it politely.  It's written exact way GNOME developers like to write things.  And \"why not\" throw out something that works pretty well for a new toy, just because GNOME is being NIH again? Are you being freaking serious?\n \n> Why is it overlapping? Parts of the functionality in KIO (particularly \n> network transparent file-management like smb, ftp, sftp, webdav) would be\n> delegated to GIO/GVFS. That's just putting the KIO interface on top of a \n> shared implementation.\n\nBecause you would need the core KIO infrastructure for things like kio_http, kio_imap4, etc. And as someone who may have to fix things in kio_http, I sure as heck won't want to deal with a mismash of g* code in KIO core.\n"
    author: "SadEagle"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: "> First of all, there is no reason to rewrite anything.\n\nI think he didn't mean to use the word \"rewrite\", i.e. replacing an existing implementation with a new one, I am quite certain he meant \"from ground up\", i.e. starting to implement a second stack starting at the wire level.\n\n> And GIO is \"Desktop independent\"?\n\nAgain I think this is just unfortunate wording. In this context it more likely means \"not linking against any GUI library\", e.g. an implementation just linking with QtCore would also be \"Desktop independent\".\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: ">> First of all, there is no reason to rewrite anything.\n>\n> I think he didn't mean to use the word \"rewrite\", i.e. replacing an existing \n> implementation with a new one, I am quite certain he meant \"from ground up\", \n> i.e. starting to implement a second stack starting at the wire level.\n\nYes, thats right. \n\n> And GIO is \"Desktop independent\"?\n>\n> Again I think this is just unfortunate wording. In this context it more likely \n> means \"not linking against any GUI library\", e.g. an implementation just linking\n> with QtCore would also be \"Desktop independent\".\n\nYes, exactly. It's not possible use KIO and it's protocol handlers without installing and running some KDE specific stuff at the moment. Perhaps that's all more on the edges of KIO, but it makes it less attractive for using it cross desktop.\n\n\n\n"
    author: "Norbert"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: "> And \"why not\" throw out something that works pretty well for a new toy, just because GNOME is being NIH again?\n\nI don't think it's fair to call it a \"toy\". They have implemented something that from design comes very close to what has been discussed since many years (\"D-VFS\", \"Common-VFS\", \"Shared-VFS\",...). Unfortunately it was not there before KDE4 development started.\n\n\n> Because you would need the core KIO infrastructure for things like kio_http, \n> kio_imap4, etc.\n\nSure, but they are kind of in a different domain (used internally by certain KDE applications).\n\nI agree that what i'm proposing looks a bit weird, it's just that i don't know of a better pragmatic solution. \n\n"
    author: "Norbert"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: "> Why is it overlapping? Parts of the functionality in KIO (particularly\n> network transparent file-management like smb, ftp, sftp, webdav) would be\n> delegated to GIO/GVFS. That's just putting the KIO interface on top of a\n> shared implementation.\n\nYou seem to be saying instead of maintaining a single well tested code base, we'd now need to maintain two of them. One of which is unproven, and only covers part of the problem space. I don't see this as a winning argument.\n"
    author: "Richard Moore"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: ">> That's just putting the KIO interface on top of a\n>> shared implementation.\n>\n> You seem to be saying instead of maintaining a single well tested code base, \n> we'd now need to maintain two of them. One of which is unproven, and only covers\n> part of the problem space. I don't see this as a winning argument.\n\nFirst, i would always rate usability higher than maintainability. But i'm pretty sure that moving towards a shared implementation, even if it's only for the VFS parts of KIO, will in the long run reduce maintenance \"cost\" as well. \n\nHowever, as things are evolutionary, lets just try with an optional plugin and see if users like it. It's far too early to discuss maintenance cost or whether GVFS should really replace certain parts of KIO by default. \n\n\n\n"
    author: "Norbert"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: "Software development is 90% maintenance.\n"
    author: "SadEagle"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-23
    body: "And what about shell access? Legacy apps? Well, the fact is that KDE should use FUSE as its KIO backend, and be the \"weight behind\" to get FUSE ported to platforms where it's not present or not fully implemented. This way if there's KIO, GIO or whatever else *IO, it's not that relevant, everyone could be happy and work better (I would do, at least :-)\n\nI know this is armchair programming but that's the direction I would like to see for the *nix desktop experience"
    author: "Vide"
  - subject: "Re: kio-gio bridge"
    date: 2008-01-22
    body: "Seems to me that the only proper way to implement network filesystems is in the actual filesystem layer, underneath both gnome *and* kde."
    author: "Jel"
  - subject: "Dragon Player..."
    date: 2008-01-19
    body: "Dragon Player seems OK, but my multimedia solution in KDE 3 is Kaffeine. It's an amazing app, playing CDs, VCDs, DVDs, Video, a useable Konqueror plugin, etc. Will it be ported to KDE 4?"
    author: "Apple Pie"
  - subject: "Re: Dragon Player..."
    date: 2008-01-19
    body: "kaffeine4 does exist in svn however, at the moment, it lacks features present in the kde3 version. I'm sure that will change over time.   \n\n"
    author: "dr"
  - subject: "Re: Dragon Player..."
    date: 2008-01-19
    body: "Kaffeine is my no.1 Multimedia Player and Amarok for mp3, streams etc. \nI also miss Kaffeine as a KDE4 app but I use the KDE 3.5x version on KDE4 for DVDs, CDs etc. in the meantime and Smplayer (which looks nice but can't match Kaffeine) for video files. I wasn't so impressed by Dragon Player.\nI hope to have a full-featured kaffeine4 soon."
    author: "Bobby"
  - subject: "Re: Dragon Player..."
    date: 2008-01-19
    body: "Same here. I like Kaffeine. And I want it on KDE 4."
    author: "user"
  - subject: "Re: Dragon Player..."
    date: 2008-01-19
    body: "Dragon Player is being the ultra-lightweight video app, making it ideal for embedding in Konqueror etc. No doubt Kaffeine will be ported and continue to have all the functionality you want."
    author: "Matt Williams"
  - subject: "Re: Dragon Player..."
    date: 2008-01-19
    body: "I also dig Smplayer.\n\nhttp://smplayer.sourceforge.net/"
    author: "T. J. Brumfield"
  - subject: "Re: Dragon Player..."
    date: 2008-01-19
    body: "\"I also dig Smplayer.\"\n\nI was going to say that... it acts amazingly 'smart', resuming playback from the position you left off etc."
    author: "Darkelve"
  - subject: "Re: Dragon Player..."
    date: 2008-01-20
    body: "well, that was actually the defining feature of codeine (predecessor of dragonplayer): keeping state."
    author: "jospoortvliet"
  - subject: "Re: Dragon Player..."
    date: 2008-01-20
    body: "I found quotation from unknown source and author: \"Yes, we have started. Kaffeine4 will use Phonon, unfortunately Phonon does not yet implement all features needed by Kaffeine, so do not expect a kde4 version of Kaffeine until at least kde 4.1\""
    author: "Poborskiii"
  - subject: "Re: Dragon Player..."
    date: 2008-01-20
    body: "Source of quotation: http://hftom.free.fr/phpBB2/viewtopic.php?t=18"
    author: "Poborskiii"
  - subject: "Re: Dragon Player..."
    date: 2008-01-21
    body: "As a simple UI for playing videos fast, dragon player is the best and is a must \naddition to kdemultimedia.  We also need a full-fledged, feature rich, easy to \nuse player for which kplayer seems to be the best choice.  Unfortunately it is \nnot as popular as kaffeine, but it works much better, particularly the way it handles removable media is excellent. Looking forward to kplayer being ported...\n"
    author: "Ask"
  - subject: "KDE Release Event video"
    date: 2008-01-19
    body: "Hello! Where can I find videos of the KDE Release Event (keynote,etc.)? Thanks!"
    author: "goran"
  - subject: "Re: KDE Release Event video"
    date: 2008-01-19
    body: "We will announce the release event videos at the Dot (this website) when they are available.\nIt might take a few days to process them.\n\nDanny"
    author: "Danny Allen"
  - subject: "KWIN is such a WIN!!"
    date: 2008-01-19
    body: "How great KWin is!! The integration of compositing and window management is a huge improvement, it makes you think why in the beginning there were 2. I can't stop showing my friends how cool is to toggle compositing in KWin, often without even noticing a flicker!\n\nI was skeptical when I first heard about that, but now I foresight a successfull future (an present!) for KWin. Thanks Lubos !!!\n"
    author: "an user"
  - subject: "Feature Request for 4.1"
    date: 2008-01-19
    body: "I'd like to see a start-up wizard for the first time you use KDE 4 with a new profile.  I believe SUSE had one for a while.  There was an older version of SUSE I installed, that when I first used it, it prompted me to configure KDE through a wizard.\n\nIt asked me if I want to use single clicks or double-clicks to launch apps.  It asked me if I wanted to mimic OS X, XP, or use KDE defaults, etc.  With QT including a Clearlooks engine, you could even configure KDE to mimic Gnome to a certain extent.  I sure wouldn't want to, but there are plenty of people (for whatever reason) that like Gnome.  It might help people consider trying it out, and eventually switching.\n\nA start-up wizard helping you configure KDE might go a long way to help ease some of the complaints of people not being able to configure KDE 4, not to mention help ease the transition of potential new users we might get from the Mac, Windows and Gnome crowds.  KDE 4 is new and shiny, and we have to expect that new users are going to want to try it out.\n\nThis no doubt would require that KDE bundle some themes that mimic the above systems, but I'm sure there will be plenty of them on kde-look.org"
    author: "T. J. Brumfield"
  - subject: "Re: Feature Request for 4.1"
    date: 2008-01-19
    body: "There is a Plasma applet which is currently doing nothing but the plan is to make it something like KPersonalizer (That first time wizard).\n"
    author: "Emil Sedgh"
  - subject: "Re: Feature Request for 4.1"
    date: 2008-01-19
    body: "Thanks!  Good to know!"
    author: "T. J. Brumfield"
  - subject: "Next Release ?"
    date: 2008-01-19
    body: "Hi\n\nCould someone please point me to the release schedules for 4.1 ? Next Kubuntu will offer KDE 4. I wonder if it will be the KDE 4.0 or KDE 4.1 branch, and whether there is a chance of one more KDE release before April. \n\nThanks a lot, and THANK YOU for the awsome work !"
    author: "KubuntuUser"
  - subject: "Re: Next Release ?"
    date: 2008-01-19
    body: "Tentative schedule is for 4.1 in July 2008.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Next Release ?"
    date: 2008-01-19
    body: "...so Hardy will have KDE 4.0 as an option."
    author: "Ian Monroe"
  - subject: "Re: Next Release ?"
    date: 2008-01-19
    body: "Many thanks guys! Any chance of a point release, say KDE 4.0.1 ? Would that just be bug/security fixes ? Or, in other words, has the development jumped mainly to the 4.1 branch or is it still in KDE 4.0.x ? "
    author: "KubuntuUser"
  - subject: "Re: Next Release ?"
    date: 2008-01-19
    body: "Ok, I found some info. There will be at least one point, maybe two point releases I guess before Kubuntu freeze:\n\nhttp://techbase.kde.org/index.php?title=Schedules/KDE4/4.0_Release_Schedule#Upcoming_milestones\n\nAs per features, it looks like there are lots of features still coming in the 4.0 branch, I would expect they will be released in the point releases:\n\nhttp://developer.kde.org/development-versions/kde-4.0-features.html\n\nThanks!"
    author: "KubuntuUser"
  - subject: "Re: Next Release ?"
    date: 2008-01-19
    body: "\"As per features, it looks like there are lots of features still coming in the 4.0 branch, I would expect they will be released in the point releases:\n \nhttp://developer.kde.org/development-versions/kde-4.0-features.html\"\n\nThat's old, and represented the goals intended to be done in time for 4.0.0.  The 4.0.x branch is basically feature-frozen, although there are some rumblings about slipping some configurability into the Plasma panel somehow.\n\n"
    author: "Anon"
  - subject: "Re: Next Release ?"
    date: 2008-01-19
    body: "That's exactly the kind of info I was looking for. Thanks a lot! It looks like I'll switch to KDE 4 by the time Kubuntu 8.10 is released (two releases ahead). Cheers!"
    author: "KubuntuUser"
  - subject: "Re: Next Release ?"
    date: 2008-01-20
    body: "By the way, I just tried kde4-core in hardy, in a separate partition ... there is a lot of unpolished stuff, it really shows it's a first version of KDE-4, and an alpha version of kubuntu ;-). But KDE-4 is looking really, really exciting ! Kudos to everyone involved!"
    author: "KubuntuUser"
  - subject: "Re: Next Release ?"
    date: 2008-01-21
    body: "Well if 4.1 won't be ready by the next Kubuntu release...\n\n(I hope the Kubuntu folks are actively working on KDE 4.0 and not treating it like a second class citizen like they have been the KDE branch of Ubuntu in the past...)\n\n\nThere is always openSUSE, which I think might actually make the the 4.1 release..\n\nWe'll see.. We have a couple interesting months ahead..\n\n"
    author: "Max"
  - subject: "Re: Next Release ?"
    date: 2008-01-26
    body: "As you're bringing other distros on the plate, let me do the same: Fedora 9 will ship with KDE 4.0 (as the default and only KDE; from KDE 3, we're shipping only compatibility libraries (kdelibs3, kdebase3) and modules not available in 4.0 (kdepim(*), kdewebdev(**), kdevelop)).\n\nThe plan (subject to change) is to push KDE 4.1 as an update once it will be available. (This has been done before, Fedora Core 4 was upgraded from 3.4 to 3.5 in updates.)\n\n(*) enterprise branch, KitchenSync from OpenSync0.30API branch\n(**) I know kdewebdev is technically available in 4.0, but it's missing its main application (Quanta), so shipping kdewebdev 4.0 wouldn't be very useful."
    author: "Kevin Kofler"
  - subject: "Poor nvidia drivers"
    date: 2008-01-19
    body: "> Note that current XRender implementations (in X/drivers) often perform rather poorly and therefore the OpenGL mode should usually have much better performance.\n\nHow poorly they are can be for example seen here:\nhttp://www.nvnews.net/vbulletin/showthread.php?t=106338\n\nAt the moment, buying a new nvidia card will only decrease your performance. If a new one is needed, you will go best with an used AGP card.\n"
    author: "michaell"
  - subject: "New Release?"
    date: 2008-01-19
    body: "That mean we will see a new release soon? I thought there won't be any untill beginning February (4.0.1)."
    author: "Ramon Antonio"
  - subject: "Re: New Release?"
    date: 2008-01-20
    body: "Right - the next bugfix release (4.0.1) is scheduled for February. That will be our next release.\n\nDanny"
    author: "Danny Allen"
  - subject: "Kioslaves documentation?"
    date: 2008-01-20
    body: "Hi.\nIs there a kioslave documentation page somewhere? A howto or something that will get me started writing one, or perhaps discourage me from trying. I've googled for it but can't seem to find anything appropriate.\n\n"
    author: "Oscar"
  - subject: "Re: Kioslaves documentation?"
    date: 2008-01-20
    body: "http://techbase.kde.org/index.php?title=Development/Architecture/KDE3/Network_Transparency"
    author: "Kevin Krammer"
  - subject: "Samba kio slave broken?"
    date: 2008-01-20
    body: "I really like KDE 4, and I use it as my main desktop now. However, one little bug frustrates me a lot. I can't really copy files over samba. I already created a bugreport for it (https://bugs.kde.org/show_bug.cgi?id=154929), but so far, nobody confirmed this bug, which lends me to believe it might have something to do with my configuration / the kubuntu packages. I'm wondering if anybody here can confirm this bug. I'm stuck using the command line smbget to download stuff over samba, which ain't that convenient."
    author: "pinda"
  - subject: "Re: Samba kio slave broken?"
    date: 2008-01-20
    body: "I would like to add that on kde3 , this issue doesn't exist."
    author: "pinda"
  - subject: "Re: Samba kio slave broken?"
    date: 2008-01-20
    body: "I would like to add that on kde3 , this issue doesn't exist."
    author: "pinda"
  - subject: "Re: Samba kio slave broken?"
    date: 2008-01-21
    body: "Yes, looks like they just removed some good old friends in KDE4.\nThis is why (together with speed that sucks in my machine) I keep using 3.5, and will continue to do so until a really stable version comes out or 4.1.\nAnd after I buy a new computer!!\n\nSadly, this should talk per-se about KDE4 current state in my experience :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Samba kio slave broken?"
    date: 2008-01-21
    body: "Well, the samba kio slave is there, but after a couple of seconds of copying, it starts to stall, and draw a very high cpu load. Were you able to reproduce this bug btw, or don't you have a 4.0 installation of KDE?\n\nBut this is actually one of the few remaining things that frustrate me a bit. On my machine most of KDE 4 actually runs faster then KDE 3. (athlon XP 2500+, 512 MB ram, old Geforce 2 videocard) Applications start significantly faster, the DE environment starts faster, and most things feel more responsive in general. Only downside is that it uses more RAM than KDE 3, but still not too much for my moderate 512 MB (due to double buffering), and scrolling in most applications draws a higher cpu load."
    author: "pinda"
  - subject: "Re: Samba kio slave broken?"
    date: 2008-01-22
    body: "I have a Duron 1.6Ghz with 512 RAM and a GeForce4.\nI found some things really faster, but dolphin and moving plasma applets is really a pain in the ass.\n\nOther day I wanted to move a icon in the desktop and the whole desktop (including all icons, desktop, plasmoids) moved! Very funny, but not good ;)"
    author: "Iuri Fiedoruk"
  - subject: "nize.. niiizzzzzeeee..."
    date: 2008-01-20
    body: "..and funny too: they made the M$N butterfly more evelish:\nhttp://commit-digest.org/issues/2008-01-13/moreinfo/758135/#visual\n\nAnd good to see the pace of improvement everyone is putting up.. KDE rules! My panel is back too! Watching svn up never has been so much fun ;)"
    author: "eMPee584"
  - subject: "Plasmoids in javascript?"
    date: 2008-01-20
    body: "Maybe slightly irrelevant and the wrong place to ask, but heck, I'll do it anyway. : )\n\nI've just installed KDE 4.0 and it has been, despite its flaws, been fun using it.\n\nHowever, I'm somewhat disappointed by the lack of documentation for Plasma. I know there's a how-to-build-your-first-plasmoid-in-c++ tutorial at Techbase, but there aren't any that show you how to make one in e.g. JavaScript. That's something I've been looking forward to do. Apparently, there's support for scripting Plasmoids in JavaScript, but I can't find any documentation for it, nor any good examples (there are some JavaScript examples in playground, I know, but they wouldn't run here). Can anyone tell if there are any plans of doing this in the near future?"
    author: "Anonymous Coward"
  - subject: "Re: Plasmoids in javascript?"
    date: 2008-01-21
    body: "yeah, it's just been a rediculously crazy past few months. comprehensive docs are already on the 4.1 roadmap for plasma. i appreciate your patience in the meantime. =)"
    author: "Aaron J. Seigo"
---
In <a href="http://commit-digest.org/issues/2008-01-13/">this week's KDE Commit-Digest</a>: A whole set of bugfixes and feature additions in <a href="http://plasma.kde.org/">Plasma</a>, and various optimisations across KDE. Usability improvements in <a href="http://edu.kde.org/blinken/">Blinken</a>. More work on the timeline tool, including fuzzy selection in <a href="http://www.digikam.org/">Digikam</a>. Support for XComposite translucency in the <a href="http://konsole.kde.org/">Konsole</a> KPart. QtScript can now deal transparently with all scripting backends supported by <a href="http://kross.dipe.org/">Kross</a>. Improvements in KWin Composite effects. Support for an old feature request, "parenthesis highlighting as an expression" in <a href="http://kate-editor.org/">Kate</a>. Continued interface work in the KHotNewStuff2 library. Progress in the Bonjour protocol implementation in <a href="http://kopete.kde.org/">Kopete</a>. Support for stroke gradients, and adding and removing rows, columns and cells in the new Table Flake shape in <a href="http://koffice.org/">KOffice</a>. Initial import of kio-giobridge, a bridge interface between the KDE and GNOME I/O methods. Work is resumed on the CIA Plasma applet. A Luna Plasmoid (moon phase display) is added to KDE SVN. <a href="http://dragonplayer.org/">Dragon Player</a> (formerly Codeine) is moved from playground/multimedia to kdereview, with a view to moving into kdemultimedia for KDE 4.1. The KDE 4 version of <a href="http://yakuake.kde.org/">Yakuake</a>, essentially a rewrite, is imported into KDE SVN. <a href="http://commit-digest.org/issues/2008-01-13/">Read the rest of the Digest here</a>.

<!--break-->
