---
title: "KDE 4.2 Beta1 Out for Testing"
date:    2008-11-26
authors:
  - "skuegler"
slug:    kde-42-beta1-out-testing
comments:
  - subject: "So where are the packages?"
    date: 2008-11-26
    body: "So where are the packages for intrepid?"
    author: "kubunter"
  - subject: "Re: So where are the packages?"
    date: 2008-11-26
    body: "Maybe Kubuntu have enough other problems? We will see..."
    author: "SONA"
  - subject: "Re: So where are the packages?"
    date: 2008-11-26
    body: "What problems?"
    author: "kubunter"
  - subject: "Re: So where are the packages?"
    date: 2008-11-26
    body: "e.g. kbluetoothd vs bluez 4.x API\n(*me* stays on 8.04 till Kubuntu Intrepid is ready.)"
    author: "schnebeck"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "Don't use Kubuntu. Kubuntu sabotages KDE by shipping broken releases. Use a distro that takes KDE seriously, like OpenSUSE."
    author: "The Devil"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "Could you either back up your statement with specific examples, or quit talking trash?"
    author: "3vi1"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "He is absolutely right. Kubuntu was always a little broken (especially at translations), but with Intrepid and KDE 4.1.x it has reach a state of alpha-stability at my System. Enough to switch to another distri next weeks. Also, while i have Systems with other distris for testing-purpose, i can say it's really kubuntu which has the problems, and not KDE."
    author: "Chaoswind"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "Again, you cite nothing specific.\n\nIf it's unstable on your system, that's just you.  I use it every day and have no stability problems."
    author: "3vi1"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "It's true I was using kubuntu and I did a test with intrepid. It's completely unusable. It's not possible to print, the plasmoid are crashind very often, fill bug report does nothing. Some of y bug report on launchpad are still valid even after 2 or 3 different version of kde, the bug have been corrected upstream most of the time but are still present in kubuntu (I let you imagine why), bug report seems to not be read by any packager. Kubuntu packager are passing more time to introduce problem by tuning kde than doing a real package with only upstream code. I know that they are very few, so why continue to change upstream code instead to just do a good package? No idea.\n\nI switch to opensuse, I don't like the distribution maintenance (I'm used to debian like for years) but at least for kde it's the one to use and right now the only one! "
    author: "Albert"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "> It's not possible to print.\n\nUntrue.  I print all the time.  Point your webbrowser at the cups port and configure it.\n\n> Plasmoid are crashind very often.\n\nNone of the ones I use (pager, news-ticker) ever crash.  Which ones are crashing for you?  I highly doubt that's a KUBUNTU issue though.\n\n> The bugs have been corrected upstream.\n\nAnd things have been broken upstream.  If you want the latest KDE every day, just install the project neon packages.  Guess what, it's going to be a whole lot less stable."
    author: "3vi1"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "It's really funny to see that most of kubuntu people which tried intrepid are really really disapointed. I told most I can believe that for some lucky people the problem are not visible (and I told also not visible). \nFor the printer I'm sorry but to configure it I had to go to gnome software to configure it, same things for bluetooth etc. But the thing that annoid me the most with kubuntu is the silence on the launchpad report and that the bug, corrected upstream, are still present on kubuntu. There are some time, really funny for a certain point of view, where the bug report have been consider as invalid by kubuntu team but investigate and corrected by upstream. A little bit strange, isn't it? Because of this and because everytime you are critisicm kubuntu I have been basically call a liar or insulted I decide to stop to use it. That's it and it's the beauty of linux. I'm not happy with one distribution I can try another one. I'm not happy with ubuntu so I'm using another distribution. It's a choice and it's mine but I'm pretty sur that most of the people, like me, which were the bug reporter using another distribution will have an impact, and a bad one, on the quality of the distribution. Point the problems of a distribution is not a shame."
    author: "Albert"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: ">It's really funny to see that most of kubuntu people which tried intrepid are really really disapointed. I told most I can believe that for some lucky people the problem are not visible (and I told also not visible). \n\nThat is because those who are happy with it have no reason to complain. A silent majority.\n\n>For the printer I'm sorry but to configure it I had to go to gnome software to configure it, same things for bluetooth etc.\n\nDid the printer configuration tool in KMenu -> System not work? It works fine for me, and nobody has seen any bug reports about it not working at all.\n\n>kubuntu is the silence on the launchpad report and that the bug, corrected upstream, are still present on kubuntu. \n\nIf the bug is fixed upstream it will be included in the next release, unless the fix was committed for something like KDE 4.2. There would be no way that the fix *couldn't* be not included unless we purposefully took the fix out of the tarball.\n\n> There are some time, really funny for a certain point of view, where the bug report have been consider as invalid by kubuntu team but investigate and corrected by upstream.\n\nExamples please? Bugs usually get invalidated because the reporter stops responding to requests for information. We can only find or report the issues upstream if we get proper information. But since they're fixed upstream I guess it really doesn't make a difference? Anyway, without examples this is a moot point anyway.\n\n> A little bit strange, isn't it? Because of this and because everytime you are critisicm kubuntu I have been basically call a liar or insulted I decide to stop to use it. \n\nVague accusations with little to nothing to back them up is a good way to get that."
    author: "Jonathan Thomas"
  - subject: "+1"
    date: 2008-11-27
    body: "> Vague accusations with little to nothing to back them up is a good\n> way to get that.\n\n:)"
    author: "Anon"
  - subject: "Re: So where are the packages?"
    date: 2008-11-28
    body: "so it's possible to install gwenview and digikam on intrepid at the same time?\n\nJust a stupid example of kubuntu packaging problem."
    author: "Albert"
  - subject: "Re: So where are the packages?"
    date: 2008-11-28
    body: "Wrong.\nThis was an issue in the Intrepid alphas, but both are living just fine with each other here on my Intrepid install. You really ought not assume that bugs that you found in beta are still present in the final release without actually checking.\nBasically the problem was that the old version of libkipi and libkipi5 (one was a dependency of digikam and the other of gwenview) both were installing icons to the same location. Was fixed here: https://edge.launchpad.net/ubuntu/+source/libkipi/0.1.6-1ubuntu1"
    author: "Jonathan Thomas"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "\n> For the printer I'm sorry but to configure it I had to go to gnome software to \n> configure it, same things for bluetooth etc. But the thing that annoid me the \n> most with kubuntu is the silence on the launchpad report and that the bug, \n> corrected upstream, are still present on kubuntu.\n\nStrange, I've never had an easier experience printing than on Kubuntu Intrepid the other day.  I plugged in a printer I'd never used before, and went to print, and there was the printer and it printed!\n\nBug numbers?"
    author: "Yuriy Kozlov"
  - subject: "Re: So where are the packages?"
    date: 2008-12-02
    body: "\nI think that the best KDE distro. is openSUSE."
    author: "xavier"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "OpenSUSE backports functionality that should not be in an earlier release than whas released by the KDE maintainers. OpenSUSE guys do it with their best intention, sure, but also the KDE guys, so you have to choose who do you trust more, mommy or daddy."
    author: "Anon"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "Even with Shuttleworth becoming a KDE patron and all, Kubuntu still feels like an afterthought of the Ubuntu project. Their KDE experience feels often uneven and unpolished, a lot of that might stem from the completely inane decision to use Qt-based apps for anything, regardless of the quality of what's available. This results in a lot of default tools that aren't up to snuff, which is especially dire when it comes to anything system-related. For instance, while I've used that distro from the Breezy or Dapper releases (don't remember exactly) all the way through to Hardy I still wonder what Adept is supposed to accomplish. Not meaning any disrespect to its devs, but there's not a single Kubuntu user I am aware of who does not use Synaptic. And don't get me started on the slew of GTK apps with unnecessary GNOME dependencies, i.e. Inkscape.\n\nAnyway, I recently jumped ship to OpenSUSE 11.0, since YaST is desktop agnostic and has seen a lot of improvement lately, particularly in terms of package management. Plus I'm not forced to upgrade my entire distro, just to check out a new KDE release."
    author: "Cyrus"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "Sadly but true, KDE is a second class citizen in Ubuntu.\nThe switch to KDE4 is done with way to few manpower. And many features are integrated into GNOME 1-2 versions eariler.\nI switched to OpenSUSE because of it's really nice KDE support. Mandriva was an option too."
    author: "Birdy"
  - subject: "Re: So where are the packages?"
    date: 2008-11-28
    body: "I have to very sadly agree with all the people bitching about Kubuntu, and especially it's Intrepid incarnation. Yet, I still use and will likely continue to do so, and file the odd bug report, and hope it gets better. Lots of stuff seriously broke for me when upgrading to Intrepid. Lots of struggles. I hope KDE 4.2 upgrade fixes some of those that haven't been fixed yet.\n\nSome specifics for the rude fellow demanding such:\n\n- icons in konqueror and other places being very confused \n- konqueror plugins disappearing (still can't figure this out, so use firefox most of the time now --- firefox integration with Kubuntu has always been very poor and continues to be...)\n- the new adept manager being generally horrible in a few ways (but at least it's significantly faster)\n- network manager being way more confusing for wireless and manual net config\n- digikam being left at KDE3, and broken in a few places because of it (some of those issues fixed now, but some not)\n\nThere's more, but I have limited time.\n\nOf course KDE 4 itself is not all wonderful either. Mainly the file view plasmoid vs desktop paradigm continues to be awkward and painful. I've installed it for newbies and they all hate it. The new 'start menu' is pretty terribly awkward for navigating (i'm using lancelot now, which is pretty neat in many ways, but pretty heavy/ugly in some ways).\n\nAh well. Growing pains. KDE seems to have a lot of them. But it's still the best.\n\nI stick with Kubuntu mainly because I see it as the best chance of getting KDE/Linux into my workplace. So i use it everywhere I can. Otherwise I'd probably go to ArchLinux.\n\n\n\n"
    author: "Tim"
  - subject: "Re: So where are the packages?"
    date: 2008-11-28
    body: "Hmm, not quite sure what you mean by the icons being confused.\n\nInstalling konqueror-plugins should provide lots o' plugins.\n\nHeh, yeah Adept's search sure ain't perfect, that's for sure. (And putting it nicely :P) But it's still young... At least it's not crashing all the time and, as you mentioned, runs a lot faster than 2.x ever did. :-)\n\nI believe the NetworkManager plasmoid most distros will ship with their next release should fix those issues.\n\nThe more recent Digikam betas now depend on libraries provided by KDE 4.2's kdegraphics module. Unfortunately, since we are not shipping KDE 4.2 we cannot provide the latest beta, and it is simply bad practice to include old betas in a final release. (Very impolite to the Digikam devs)\n\nHope I could have been of help. It's much nicer talking to people who I can actually help, as opposed to vague bashing, and I thank you for voicing your pain points in a civil manner."
    author: "Jonathan Thomas"
  - subject: "Re: So where are the packages?"
    date: 2008-12-02
    body: "Tried using some other icon theme than the default?\n\nFrankly, on Intrepid, the bugs were so many and so in my face that filing bug reports seemed a waste of time - and in fact the kde-4.1.3 packages in backports actually did fix the most obvious ones, like I expected.\nAfter toying with kde-4.1.3 for a few minutes, I went back to kde-3.5.10 though, there's still so much that cannot be done with kde4.\n\nFor example I could not set hue etc on background of the desktop, even though I could set it for the background of the login screen - why?"
    author: "Kolla"
  - subject: "Re: So where are the packages?"
    date: 2008-12-05
    body: "I use Adept. What's the problem with it? It does what I want it to: search for and install packages. If you're serious about an apt distro, use apt-get. The latest KDE 4.2 BETA packages for Kubuntu are quite nice. Believe me, I thought Kubuntu 8.10 was garbage and just left it on one machine because I couldn't be bothered to take it off(using bb instead). When 4.2 BETA packages became available, I upgraded, and what an improvement. Of course the MySQL bullshit is quite a shocker, but that is *not* Kubuntu's issue. That just gives me a bit of insight into the minds of the devs. If the devs are thinking: \"We'll use MySQL until SQLite is ready for us.\" then I understand. It's a pain, but I understand. KDE4 might be BETA all the way till KDE 4.9. But, but, if they are thinking, \"Oh brilliant! MySQL is perfect for what *we* need and it's what we'll use.\" Then fine. Be like that and stick it to your use base. I understand how FOSS works. If I released some code under and OS license, I would say STFU to whiners. However, KDE devs and patrons should *not* position themselves as some kind of MS or corporate environment with the current attitudes they have. 80% of the software I use is FOSS. However, I'm not your average user and I don't really care about listening to music from my desktop. From my console, now that I regard as important. ;)"
    author: "frozen"
  - subject: "Slackware packages"
    date: 2008-11-27
    body: "To all Slackware users,\n\nI stopped building slackware packages after the 3.5.x release after doing it for many years. I hoped that someone will do the job but unfortunatly no one did it :(\n(Robby Workman is providing packages in /testing of -current)\n\nAs at home the babies finally do their nights :), and I need KDE upgrades on Slackware, I will provide packages again.\n\nI will redo them after the Christmas holidays.\n\nJean-Christophe"
    author: "JC"
  - subject: "Re: Slackware packages"
    date: 2008-11-28
    body: "Hello there JC.\n\nI have been using Slackware since 1997 and KDE since the same year (it was version 0.8 by then).\n\nI believe I can help you there. Nowadays I am running Bluewhite64 (which is an unofficial port of slackware to x86_64) and I have been making KDE-4 packages for it (same process as in Slack). But since I have a VM with Slackware 12.1 (or I can create another with another version) I believe I can help you there.\n\nJust let me know.\n\nKenjiro"
    author: "Yucatan \"Kenjiro\" Costa"
  - subject: "Re: Slackware packages"
    date: 2008-11-28
    body: "/*\n\nAfter JC reads that, can anyone remove my email address from the post? I didn't think the address would be visible for bots and spammers ;)\n\n*/"
    author: "Yucatan \"Kenjiro\" Costa"
  - subject: "Re: Slackware packages"
    date: 2008-11-28
    body: "I have read it :)"
    author: "JC"
  - subject: "Re: Slackware packages"
    date: 2009-01-21
    body: "So, JC... need me to help on that or you already got someone else to do it?\n\nPlease contact me over that email which wasn't deleted from the first reply *LOL*"
    author: "Yucatan \"Kenjiro\" Costa"
  - subject: "Re: So where are the packages?"
    date: 2008-11-27
    body: "A friend of mine, Yucatan Costa, is doing 64 bits KDE4 packages, maybe you can contact him for at least providing 64 bits packages?\n\nHere goes his blog: http://kenjiro.blogspot.com/"
    author: "Iuri Fiedoruk"
  - subject: "If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Well, I read a lot of \"new shiny features and bug fixes\" in announcement but I did not sow real fixes that really annoyed users. Hope panel now can be resized vertically, other number then 3 desktops available with compiz, keyboard switcher can cycle on 2 layouts and a lot of good things that where in KDE3 but disappeared in KDE4. \n\nYes, I see, icons can be placed now on desktop. Thank you very very very much!\n\nAs always, KDE public relation people eager with promises of heaven but reality is not so good as they say."
    author: "Alex Lukin"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Nobody forces you to use KDE, if you're not happy with it, use something else, if you stay with it, shut up."
    author: "kubunter"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "I wouldn't say \"shut up\", rather \"give constructive feedback\". And I think Alex' comment is far better than some rants we've seen on the Dot, although the last line was quite unnecessary.\n\nPlease don't tell people to shut up because they're free to use whatever they want, it'll only make KDE look bad (in my opinion)."
    author: "Hans"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Actually, I think the last line was the MOST necessary.\n\nI do understand that the KDE team is not paid for their work, and we should be grateful for what they do. I do understand that time is needed to reimplement the missing features, and we should not expect every KDE 3.5 feature to be here from the beginning.\n\nBut what really pisses me off, is that whole PR coming with KDE4, telling me how great and awesome KDE4 is, while the reality is quite different. If what you have is an unfinished beta, then, for God's sake, TELL IT CLEARLY it's an unfinished beta. Don't tell me it is a replacement for KDE 3.5, as it doesn't look like we're get there at least until 4.3, and possibly even 4.4.\n\nI really like where KDE is going with KDE4. I see the possibilities given by the new infrastructure, and I respect the work put there to create it. But DON'T make promises it isn't (yet) able to fulfill. It is NOT a finished desktop, and don't market it as such until it gets reasonably finished."
    author: "slacker"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "I sometime wonder if the non-marketing approach done in the KDE 3.x days (due to lack of people interested in that) should have been kept. The amount of people just coming out of the woods for complaining about \"promises\" not being kept is annoying."
    author: "Anon"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Well actually, my understanding was that the promise of great things was based on a new vision the developers had for the desktop.  That vision was a lot different from KDE3, so they may not have necessarily implemented features from KDE3.  However, because of constant complaining about missing features (relative to KDE3), they have gone back and implemented many of them.  Being different, as you say, does not make it less awesome.  It just makes it different.  For 4.3, Aaron has stated that they will refocus on the vision they had for KDE4, and I for one am looking forward to what it might look like.\n\nAs far as replacing KDE 3.5?  It may not for you, but I am already using it a lot more than 3.5 and have not missed a thing. So for me it is already fulfilling my needs and can only get better.  And to your last point, I hope it never becomes a finished desktop.  I hope it will always continue to grow and innovate."
    author: "cirehawk"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "> . However, because of constant complaining about missing features (relative to KDE3), they have gone back and implemented many of them.\n\nYou have a point, but it's not entirely accurate.  Some plasma developers indeed focused on getting features back because people started to complain too much.\n\nHowever, the main reason why those features were gone wasn't because they had a different vision. They were only _temporary_ gone because those features couldn't be ported in time for KDE 4.0. ...and delaying a release for a full year would be more devastating.\n\n(...and for some reason, people instantly thought those features were removed on purpose...)"
    author: "Diederik van der Boor"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Most people can't understand that kde has gone 7 steps back, to move 1 step forward (and is now just at step 3 or 4). It's not logical that a Major-Version of a long grown Software get published striped off of every grown advantage."
    author: "Chaoswind"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "No, they didn't go 7 steps back to move 1 step forward. They had to redesign all the cruft designs which accumulated in the span of KDE 2.x-3.5.x, the beginning of 2000 until 2006, most of which commanded binary compatibility for new changes so fundamental redesigns were postponed to the next major release. The redesigns mostly got necessary as the then status quo became harder to maintain by the day, a worst case scenario for a project like KDE relying on voluntary developers working for them while ideally having fun doing so.\n\nThen Qt 4 itself turned out to be a major redesign over Qt 3 so KDE developers opted to not only do the accumulated postponed improvements, but also adapt all the frameworks to new concepts of Qt. Those were the areas with the most work since once finished major changes can't be done anymore without breaking binary compatibility. But applications developers also waited until all the frameworks were finished so they needn't to adapt their apps to changes again and again. The frameworks are done with the release of KDE 4.0, which is why it was a major release for all involved. Now it's the time that the application and interface (not only from KDE but also Qt Software, X.org, NVidia etc.) developers are playing catch up. The end result is ideally a collection of frameworks and applications which can stay at the cutting edge of technology and interface design while being relatively easy to maintain for like the next decade.\n\nAlways keep in mind that after the big redesigns between KDE 1 and 2, it took 6 years to reach the pinnacle that is KDE 3.5.x. KDE 4 is only the second major rethought of the project, and not even a year passed yet since its release and all the progresses and possibilities are already plenty visible."
    author: "Anon"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "That's all unimportant for an enduser. They only see a new Major-Version which get ripped of everthing worthy. And also a year later, the situation hasn't changed much. So, following on the usual experience with software and oss, it normal to thing that the desirable state is far away and the software has become trash.\n\nIt's the usual geeks-are-incompatible-to-their-audience-problem."
    author: "Chaoswind"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "See, it does not work that way.\n\n1) KDE 4.2 will be (is in svn) better&#8482; than the latest KDE 3.5. At least for me\n2) When going from KDE 1 to 2, progress was obvious from the start because 1 was so simple[1]. When going from 2 to 3, there was _no_ progress, but no regression, and it took 3.1 and 3.2 to show that progress had indeed occurred. Now this last change, 3 to 4 is even more important, and so there is some regression. however...\n3) the new foundations are so good that all the goodness[2] in 3.5 could be reproduced within a year. So basically, dev speed is increased by a factor 3.\n4) 4.2 is this release where 3.5 looks aged. 4.3 is the release where 3.5 looks as silly as KDE 1 compared to KDE 3.\n\nThis is not a question of geeks and their audience, it is a question that open source development works well in the open, with incremental releases that might be broken and little fear of rewriting for the sake of code beauty. That is, if the coders are good.\n\nAnd KDE coders are world class.\n\n\n[1] Still, I wish dolphin was as fast as kfm -- but on the other hand it is faster on larger directories and does so much more...\n[2] I assume you talk about the desktop shell, because the apps were already an improvement from 4.0"
    author: "hmmm"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "There the Geeks argumentation goes again. So, forget it."
    author: "Chaoswind"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "In a project driven by developers (like KDE still is) the developers are the actual end users. Distributions are secondary end users (they could easily be \"first class\" end users if they worked more closely with upstreams, something that mostly happens only in the kernel and server space). The end users of distributions are, well, end users of the distributions with all the pros and cons inhereted (see Aaron Seigo's recent blog entries for problems caused by the lack of communication between up-/downstreams).\n\nI doubt you actually use KDE though, the progress since 4.0.0 has been very visible."
    author: "Anon"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "It doesn't matter where the target lies. It's just matter who use it and who complains. Like i said, the involved persons view is different."
    author: "Chaoswind"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "To the project's survival the only people mattering are those who contribute in some way. \"Plain\" users, \"complainers\" etc. are nice (or not so nice) to have, but don't really matter much to the project's existence in the end.\n\nKDE's amount of svn accounts and commits are on a steady upward slope. Going by this everythings fine."
    author: "Anon"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "This is very interesting irrelevant of the discussion about the features above. \n\nIn addition to being a KDE user and sympathizer for a long time, I manage couple of research projects quite similar in structure -at least in spirit- to what you guys do. Just like yours, people are mostly voluntarily involved in my projects and the output is open for all to see and criticize. \n\nAccording to my experience, I believe you are making a grave mistake regarding who matters. Of course you are right in the short run. However, in the long run the people you dismiss matter even more than short term contributors. Because those \"whiners\" are nothing but noisy elements of a huge group of real users like me, who rarely come out and say something. With whiners signal to noise ratio may be low but filters can be deployed in some form.\n\nIf I were you, I would worry when nobody says anything or only a small core group (like the one you favor so much) cheers. Because then, it is only a matter of time until the project dies. Without a large base, where do you think the real contributors will come from? If large groups of people do not find KDE useful, do you really believe that you will still have tons of real contributors involved? Of course not. I think developers intuitively know this, too, for they accommodate user wishes to some extend regardless of original plans. \n\nFinally, my thanks for all the great work. I have been waiting for KDE 4 to mature for almost a year now and I think 4.2 will be the point where I will switch finally from KDE 3!\n\n"
    author: "Anonymous"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "I think your definition of a \"contributor\" is more strict than what I would have liked for my above post. To me a contributor is someone who contributes something that either is directly usable by the project or indirectly motivates another contributor to do something (more).\n\nBy that definition \"plain\" users from whom the project gets neither direct nor indirect feedback at all could as well not exist. \"Complainers\" only rise the noise ratio and make the place where they tend to complain unproductive. But contributors can contribute as easily as making fitting constructive criticism at the right place to the right person, giving as simple \"thumb up\" \"I like that\" response to a particular new feature described by a developer on his blog or in a mailing list. Stuff like that are building a positive environment.\n\nDot with its high noise ratio is a rather good example for a unproductive environment, too many people who give unconstructive criticism or show plain pessimism or even malice. It's no suprise not many of the actual developers come here anymore, and the forum upgrade to the dot with the capability of voting comments up or down is way, way overdue indeed. This, along with other places turning unpleasant, has made the community more fragmented than necessary. E.g. kde-apps.o is a great place with plenty fresh contributors and a very vivid community, it's really unfortunate it's not more closely connected to the core KDE project. The question of course is if communities just become completely unmaintainable after reach a specific size. E.g. bugs.k.o always has been on the verge of maintainability even without much of the noise here, while developers are demotivated by the amounts of reports and users angered about the lack of feedback."
    author: "Anon"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "If you don't want to use unfinished software, use the kde3 series. Everything still works.\n\nIf you want to see how a desktop is built, how the process happens, where the resources are lacking, where brilliant ideas are implemented, where shortcuts are taken and ultimately fixed properly, how incremental improvements are made, and how with time, care and hours of work just about everything comes together as envisioned, use KDE4.\n\nFree software has a very unfortunate characteristic. The best experience comes to participants.\n\nOtherwise all you have is whatever is there for free.\n\nAs for the geeks/audience dichotomy, there would be no audience without the geeks.\n\nDerek"
    author: "dkite"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "\"But what really pisses me off, is that whole PR coming with KDE4, telling me how great and awesome KDE4 is, while the reality is quite different.\"\n\nQuestion: who made you the person to make that judgement? I use KDE4 all the time, and I think it's great. To claim that \"KDE4 sucks!\" is a personal opinion, it's not an universal truth you try to make it sound like."
    author: "Janne"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "\"I really like where KDE is going with KDE4. I see the possibilities given by the new infrastructure, and I respect the work put there to create it. But DON'T make promises it isn't (yet) able to fulfill. It is NOT a finished desktop, and don't market it as such until it gets reasonably finished.\"\n\nhttp://www.kde.org/announcements/4.1/\n\nThe announcement of KDE 4.1 doesn't make any false promises. It says:\n\n\"While KDE 4.1 aims at being the first release suitable for early adopting users, some features you are used to in KDE 3.5 are not implemented yet. The KDE team is working on those and strives to make them available in one of the next releases. While there is no guarantee that every single feature from KDE 3.5 will be implemented, KDE 4.1 already provides a powerful and feature-rich working environment.\"\n\nNo one says that KDE 4 is a KDE 3 replacement yet/or a finished desktop.\nKDE 4 is stable enough for everyday use, and there are no missing features that blocks you from using as a working environment.\n\nI remember the days when KDE 3.0 was out. There were a lot of unimplemented features. IMO KDE 3.3 was the \"first\" version of KDE 3 series."
    author: "Antonios"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "You are right Hans. That was really rude what he said."
    author: "Bobby"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Gents, I think Alex's comment was somewhat constructive, but the second gentleman's comment had an aggressive \"flavor\" and it's certainly unnecessary.  Saying \"shut up\" isn't nice, especially in this \"collaboration community\".  The KDE devs are doing an excellent job, yes there is still some place for improvements, but let's face the truth, there is still windows and other closed source alternatives out there.  The choice is there, I, myself left the windows environment because of several factors, and today, if I have problems with the opensource environment, including KDE or Linux itself, I seek help, I address my problems to knowledgeable people, and I'm extremely thankful for the precious work developers are doing.\n\nThat was my 2 cents! :) "
    author: "Louis"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Hasn't the panel been able to be resized vertically for quite a while now? And wouldn't a restriction that is only imposed by using compiz be a compiz restriction? I'm not sure what \"keyboard switcher can cycle on 2 layouts\" means. You can quick switch between two keyboard layouts?\n\nCan you explain more about what it is you are missing?"
    author: "Daskreech"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Have you even used KDE 4.1? You can resize panel vertically and compiz \"uses\" as many virtual desktops as you have i believe. Don't know about that layout thing though...\n\nBTW reality is now better than ever."
    author: "lolwut"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Having the feature, doesn't mean it's really usable. It's really problematic to make a panel vertical in kde 4.1.x and involves usage an 'undocumented' mod-key."
    author: "Chaoswind"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Now it doesn't, it's drag and drop like we know it for *decades*."
    author: "Sebastian Kuegler"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "But he's right that it's not usable, after every plasma crash the panels are back at the bottom with the default settings.\n\nWhich is annoying, especially because the only way I've found to get rows of icons on the panel (my panels relatively wide because my screen's 16:10, the Windows quicklaunch bar has rows of 4 icons each, KDE has icons the size of Texas) is to use a second panel."
    author: "ac"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Everyone repeat after me.  Beta 1."
    author: "cirehawk"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "4.2 comes with a quicklaunch applet. try it."
    author: "Aaron Seigo"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "btw, what crash? \n\nand maybe you're basing this on 4.1? because in 4.2 we also sync plasmarc whenever the Corona syncs its config, meaning that the view configuration is coordinated with the scene configuration. i just tested it in fact by dragging my panel to the top, letting 10 or so seconds pass and then `killall -9 plasma` (the unhappy nasty way to put a knife to its throat), restarted plasma and there it was: still at the top.\n\nwe do make progress from release to release ;)"
    author: "Aaron Seigo"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Did you get it to the top or have you been able to move it from the bottom? I wanted to test AWN Dock just yesterday but I just couldn't find a way to move the panel to another position like it's possible with Kicker."
    author: "Bobby"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "No."
    author: "Chaoswind"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "yes it is.\nclick on the cashew. drag and drop by clicking on an empty space on the configuration thingie. resize it to make it wider.\n\nwhere is the problem?"
    author: "hias"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-26
    body: "Panel can be resized since KDE 4.1 was released. Icons on desktop are also possible since 4.1 (via folderview plasmoid)."
    author: "AdeBe"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "Please, do not mistake the openSuse backports with original 4.1 release. I have seen those features in 4.1 as well, but they started to be introduced with 4.1.1 as openSuse prepared 11.1 release... "
    author: "Sebastian"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "So why does it work fine on Intrepid?"
    author: "Stefan Majewsky"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-27
    body: "OK I am reading various wrong things on the above posts and I want to put things straight.\n\nAt least under Kubuntu 8.10 (KDE 4.1.2+) you can resize the panel in the usual way by dragging. It's kind of hidden (i.e. you have to unlock widgets and click on the small cashew, on the right side of the panel) but I guess users are not stupid and they have the ability to learn such things. Then you simply drag the panel edge you want to move.\n\nUnder KDE 4.1 plasma saves it's configuration when you log out (in fact when plasma itself shuts down), so if you crash after changing the settings then they will get reset. An other way is to force restart it using this command:\nkquitapp plasma && plasma &\n\nKeyboard layouts work as well (I am using us/el all the time). If layouts are disabled just enable and configure them in system-settings > Regional & Language. Actually that's the way you did it under KDE 3.5...\n\nFinally someone said that the only way to add more than one row of icons (I guess he meant application launchers/shortcuts) is to stack more panels. hmmm that sounds like an interface abuse to me. All you have to do is install and add to your panel the plasmoid version of QuickLauncher [http://www.kde-look.org/content/show.php/QuickLauncher+Applet?content=78061].\n\nTo the guy that said shut up... please when you talk online try to think if you would say the same thing to someone face to face. That's an easy tip how to learn netetiquette. \"Shut up\" is one of the most insulting things you can say!\n\n\nMY OPINION AS AN INFORMED KDE USER, COMPUTER POWER USER AND CS STUDENT:\nKDE 4 did something very wrong (as it turned out) for most people. They released 4.0 without making it clear enough that those \"most people\" shouldn't install it with a ten foot pole(TM). For those guys it was only a preview of what was coming. That confused a lot of them, who in turn confused a lot more with their whining and bad criticism.\nAlso you have to understand how things work with distributions. I am using Kubuntu which unfortunately is not that great at keeping KDE very stable and bug free (that's a whole different discussion). There are a lot of bugs that make things look bad and for the most people KDE itself is responsible. That's not true but it still adds up to the previously mentioned confusion and a false bad image for our beloved desktop.\n\nPeace and thanks from the bottom of my heart to all people involved in KDE!"
    author: "Leonidas"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-12-05
    body: "Thank you for explaining how to do that! It was something I missed from the 3.5 days, and I hadn't found the option (I was looking for an interface similar to the old one for changing the width). Good to see the functionality's there, although I'd argue from the number of people missing it that the UI change was maybe suboptimal.\n "
    author: "gnb10"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-28
    body: "The panel has always been resize-able vertically, even in KDE 4.0. KDE 4.1 improved resizing the panel even more. So what exactly is the provlem with resizing the panel vertically?"
    author: "Anonymous"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-11-30
    body: "Panel resizing is there since 4.1, kthxbuye."
    author: "blueget"
  - subject: "Re: If panel resizing finally fixed?"
    date: 2008-12-05
    body: "This is so BS. It works better than ever. And, if you're a seasoned KDE user, you'll know about saving sessions. Don't forget about that. ;)"
    author: "frozen"
  - subject: "Hmmm, well..."
    date: 2008-11-26
    body: "About keyboard: a lot of people using 3 and more languages/layouts. For instance, EN,RU,UK. An old keyboard switcher can cycle on EN/RU and EN/UK. This is very helpful because in most cases EN is for command line and one other for current national language. If you need more than 3 languages then you'll get crazy switching it. \n\nAbout panel resize. As I see in last Kubuntu (KDE 4.1.3), it resizes vertically only by changing schema or hacking, not by selecting it's size usimg panel preferences. It is very inconvenient for users with wide display. I do not like panel that eats 10% of work[place. \n\nWell, I can explain every \"feature\" that me and a lot of people like me, who uses KDE for decade, feel inconvenient or even annoying.  But who listens? Most of comments are like this one above: \"Shut up and be grateful\".\n\nBTW, I write with real identities, you may send your \"shut up\" to my mailbox directly."
    author: "Alex Lukin"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "You can already configure panel height in KDE 4.1. Just drag the edge of the panel configuration bar to set the height. In KDE 4.2 there will be a more self-evident button for you to drag to do this."
    author: "Jonathan Thomas"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "Vertical resize is in the Kubuntu 8.10, at least I used the LiveCD and it was in, just press the plasma button on the panel and place the mouse on the border of the adjuster thingy.\n\nI guess most of the complaints are more a PR problem than lack of features. Since many things changed or are done differently, not all old users will find the new stuff as they will most likely look for them in the old places. Maybe what KDE needs right now is more of tutorial videos or just something that is short and well arranged so to show the new stuff quickly for anyone who's switching.\n\nMost of the \"shut up\" is because people expect everything from the start, KDE as any OSS project uses a \"realease soon, release often\" which means it might not have all the feature you took for granted in the old version (just like those old versions where built based on old and incomplete versions). Basically, all those \"shup up\" are said because no one expectes the Spanish Inquisition when releasing software which is knowingly \"incomplete\" (in the KDE feature sense).\n\nSo if 4.2 isn't there yet for you, just place the bug reports of the missing features (as most should probably be ported to KDE4) instead of just complaining that they are not there yet (they know that and are working on it - and at a quite impressive pace I should say)."
    author: "AC"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "You're probably right... but... we're at 4.2 and everything is still simply feeling \"beta\" to me, both concerning functionality and stability. I fill out bug reports, but there would be lots to fill out, and you don't know whether it's a real bug, something that has been decided to do so because of some obscure reason, and if anybody is interested at all.\n\nCurrently KDE developpers have a tendency of \"not taking the users with them\". Many a complaint is currently brushed off by \"shut up and code yourself\". \n\nThis statement is completely justified, as many coders are not paid for their work. However, in \"former times\" I as a longtime KDE user felt definitely more \"respected\" by the coders. \n\nTake small examples like the \"cashew\". BTW, I really don't mind that thing. But some vocal critics disliked it. In former times, KDE devs would have said: \"Well, I personally don't understand the need for removing that thing, but what the f**k, some users want it, it's easy to do, so let's make it an option\". This discussion always reappears, be it the \"old desktop paradigm\" stuff or the reimplementation of \"good old kicker features\" and all that. All critical KDE users - but the fan boys - are dismissed (or feeling like being dismissed) as trolls. At least sometimes distributors like openSUSE listen to their user's feedback.\n\nTake all the plasma hype. I really like the possibilities of plasma. However, I'm really sick of all the crashes, plasmoids taking the desktop with them, difficulties with different gfx cards, and so on. \n\nI'm missing stuff like drag'n'drop in menus and panels, Windows 95 offered this and it only gradually reappears in KDE. Somebody decided it's an outdated desktop paradigm maybe?\n\nTo point this out: I'm absolutely in favour of most of the improvements KDE4 offers. I like the possibilites of plasma, it's simply great. But for months now I have the feeling that besides all those shiny things stuff like \"integrating user feedback\" and \"concentrating on a solid, non-crashing desktop base\" has simply been abandonded."
    author: "Lars"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "The reason 4.2 feels beta is because it still *is* a beta."
    author: "kubunter"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "I'm not talking about the specific 4.2 beta, but about any release of 4.x - be it beta, rc or release... doesn't seem to make that much difference."
    author: "Lars"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "Sometimes I wonder what you guys are doing with KDE.\n\nI use KDE4 since 4.0.3 just for writing stuff, watching videos, surfing the Internet, listening to music etc., with less words: for normal work.\n\nFor that normal purpose I hardly feel any regression compared to KDE3, and at least since release 4.1 I don't have any crashs (up to then it was not optimal, but less than in Windows 9x ;-)).\n\nSo maybe KDE4 isn't mature now for Linux power users making I-don't-know-what, but an average user like me just doesn't have these show stoppers, but simply enjoys the wonderful look&feel of KDE and it's apps (e.g. I love Dolphin - I know, power users hate it, but who forces them to use KDE4?) - I feel it every time I have to use WinXP, which got really ugly in comparison to KDE."
    author: "Cyril"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "I dunno, I would consider myself a power user, and I find KDE4 (.1 especially) generally a much nicer experience than 3.5. I do wish Konsole could use the system bell, though; having to have my speakers on in order to hear when I'm hilighted in irssi is kind of annoying at ungodly hours of the night/morning when people are asleep.\n\nI think the main reason people are complaining is because KDE4 is still reimplementing many of the features present in 3.5, features which probably weren't even in the 3.x line *until* 3.5. They'd do better to express themselves in bug reports, though, instead of trolling the Dot."
    author: "Kiyoshi Aman"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "Let me quote you then:\n\n\"we're at 4.2 and everything is still simply feeling \"beta\"\""
    author: "kubunter"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "I understood his meaning.  Typically a .0 release has a few bugs, a .1 is pretty solid, and a .2 release is very solid.\n\nWhat he's saying is that we're almost to the .2 release on the numerical scale, but we're not yet to the .0 release on the quality scale (i.e. \"beta\" quality).  I definitely agree.  I happily used KDE 2.0 and 3.0, but don't really expect to use KDE4 until KDE 4.2.  I think you'll find most distros will offer KDE 3 until around that time too, for the same reasons.\n\nPart of this is due to the KDE 4.0 release being a \"developer release\", which uncoupled version numbers from traditional usability/stability expectations.  Those who don't follow KDE closely may still be surprised by the new numbering scheme."
    author: "ac"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "Then happily use KDE 2.0 and 3.0 again please. With the high expectations of today you'll get quite into a huge eye-openers this way."
    author: "Anon"
  - subject: "Re: Hmmm, well..."
    date: 2008-12-06
    body: "Actually I got the chance to use an ancient machine with a 2.x install on it (not 2.0 though) and it was quite good, certainly better than 4.1 in my opinion.  Not to worry, my expectations are low enough that I expect KDE4.2 to finally surpass the 2.x series.  As I said, I expect 4.2 to be usable, just like a .0 release should be."
    author: "ac"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "\"I'm not talking about the specific 4.2 beta, but about any release of 4.x - be it beta, rc or release... doesn't seem to make that much difference.\"\n\nKDE4 is 10 months old. After 10 months, KDE2 also had \"issues\" (KDE3 was better, mostly because it wasn't that big of a leap from KDE2).\n\nDuring that 10 months, KDE4 has progressed A LOT. I admit that that I thought 4.0 to be a mess as well. So I didn't use it. 4.1 felt A LOT more solid.\n\nPeople keep on comparing KDE4 to KDE3, but they are forgetting that KDE3 is 6.5 years old. It has had A LOT of work done to it. KDE4 is 10 months old. It has had ONE major release after 4.0."
    author: "Janne"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "I both agree and disagree with you. I'm still using 3.5 as I feel KDE4 isn't ready yet for me but that kind of instability is necessary to cut out new bugs like that RGBA or what not problem in some graphics drivers. The devs could take a different approach and make some hacks to get it to work however that's not really a practical solution since they should be solved upstream. Therefore some instability in Plasma in the start was necessary so others could fix their own code (apparently Plasma was the first real world app to use some of their feature and as such they where not properly tested...)\n\nThe cashew problems highlights one of the many communication issues of KDE. It's not that we all have to adjust on having it on the desktop it's just that removing it with a simple hack solves nothing. I read somewhere that there are plans to provide different desktop Containments (some without the cashew). When this happens the user can easily switch between them and decide what is best. If they removed the cashew right now you would have problems acessing/testing new features like the \"zoom out\" etc.\n\nSince there is so much unnecessary \"noise\" about the changes this will inevitably wear out anyone who tries to explain it to each missinformed user which leads to some unintencionally harsh replies like \"shut up\" or what not...\n\nThe whole point of Plasma isn't deciding what is outdate or new, it's giving the user choice to select anywhere from old to new. Plasma is meant to be flexible at its core, however any dev is probably more motivated to try new things than do the same old stuff...\n\nSo peace out... they'll get there, eventually."
    author: "AC"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "\"In former times, KDE devs would have said: \"Well, I personally don't understand the need for removing that thing\"\n\nwhich is why the interfaces for KDE3 apps tend to look like a dog's breakfast.\n\ncompare/contrast gwenview in kde3 vs kde4.\n\nyes, we spoiled users. we also spoiled the software in the process. now that we're taking a much more measure approach, despite adding lots of features (but often in more elegant, powerful ways), the users used to getting their way even if it screwed up the software complain.\n\nthis, too, will pass."
    author: "Aaron Seigo"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "While I agree with this in general, picking gwenview as an example is somewhat unfortunate.\n\nGwenview is one of the few KDE3 apps I still use, not because of functionality or stability but because of the interface (and speed). Unless the interface becomes a lot more configurable, gwenview4 is not for me."
    author: "djf"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "Ask it's developer for the features you're missing - he's very responsive... Personally I don't miss anything and I'm a big fan of the new Gwenview."
    author: "jospoortvliet"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-28
    body: "the #1 reason most people we talked to didn't use gwenview was the interface.\n\ngwenview may be an unfortunate example for you, but i maintain that taking the entire user base into consideration it's a great example.\n\nthis is, btw, one of the differences in my perspective and many others in these conversations: i am required to keep a broad cross section of users in mind that represent our actual user base. i do wish i could take the simpler \"focus set of one\" approach most people are afforded; it's so much less complex! =)"
    author: "Aaron Seigo"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-30
    body: "For me, it's the other way around, I really love the new Gwenview... (as I really love KDE4), you see, all users are different...\n\nCheers.\n\n"
    author: "Neil"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "Please tell me: What THE F**K is wrong with spoiling end users? Isn't software supposed to give the end users a good feeling? All this crap about \"educating users\" pisses me off."
    author: "Larx"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "The word spoil can mean two things.\nI think Aaron said that the users got the features that they wanted, but the software became bad because features were added uncritically."
    author: "pascal"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-28
    body: "nothing is wrong with spoiling end users, as long as you don't ruin things in the process.\n\nmy point was that in kde3 users often \"got their way\" without anyone asking if it was actually a good idea.\n\nand so a lot of very, very poor ideas made their way into the code base and everyone suffered for it.\n\nas i noted, kde4 has a huge number of features in it, many from user request or by observing users in action.\n\nbut we're no longer going to just shove any random idea into the software simply because someone suggests it. it leads to poor results.\n\n\"All this crap about \"educating users\" pisses me off.\"\n\nwhere did i say anything about educating users? please, calm down, stop swearing at me and use your brain."
    author: "Aaron Seigo"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "Yes, your guess is quite precise. It is PR problem. It is project management problem. PR people represented KDE4.0 as finished work. Project management guys followed M$ paradigm that says: \"we are gods, user will eat everything we offer\".\nAnd yes, some guys take \"release early\" part more seriously then it should.\n\nI do not complain. I'm trying to explain. Yes I do not use KDE4 now because it is inconvenient for me. I do not want to change my old bad work habits. I'd like it better, not quite different. Yes, it is free world and developers are free to be creative. Users are free to choose.\n\nI work in quite different field (java servers) and do not know what independent developers say about porting to KDE4. But I guess ('cos a lot of apps still not ported) porting is not trivial and they say a lot of harsh words. \n\nI write those postings just to say that in my humble opinion entire KDE4 thing got a lot of PR and management mistakes. Hope things will go better in 4.2 and 4.3. Hope thing never go such way in 5.x\n"
    author: "Alex Lukin"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "@Alex Lukin:\n\nI think that kind of the point of 4.2 is that you don't have to change your habits if you don't want to. I mean you can change panel height pretty easily from a long time now, for example just look at this video:\n\nhttp://www.youtube.com/watch?v=iV8wor5VYTw\n\nAs other people said you can have as many workspaces as you want with compiz, I haven't completely understood what is what you want to do with your keyboard layout...\n\nKDE developers are actually listening to their users, that's why you can have icons in your desktop now, and in OpenSuse you can choose a containment without the cashew. If you look at the developers blogs you will see that they are very receptive to user input and they are really interested in reintroducing the missing features.\n\nUnfortunaly during the KDE 4.0 days there were a lot of confusion about the state of KDE 4 and some people got really angered and started to attack some of the developers. I suppose that's why a lot of people is suspicious and intolerant at post like yours (where you stated that you like KDE, but you also said it was lacking some features that it actually has had since some time ago). \n\nI really hope we can all calm down and start comunicating better again, I hope that with the release of 4.2 a lot of people is going to realize that their complains are being listened and solved and can start appreciating all the work that has gone into KDE 4. In the meantime maybe you want to take a look at the KDE forums, as you may know they just started a new one at\n\nhttp://forum.kde.org/\n\nmaybe the features that you are missing are already there and you still don't know :), in any case keep in mind that although not all KDE 3.5 features are in KDE 4 just yet, there are also a lot of features that are exclusive of KDE 4, enjoy them.\n\nSincerely,\n\nRaul Gomez\n"
    author: "Raul"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "\"Yes, your guess is quite precise. It is PR problem. It is project management problem. PR people represented KDE4.0 as finished work. Project management guys followed M$ paradigm that says: \"we are gods, user will eat everything we offer\".\nAnd yes, some guys take \"release early\" part more seriously then it should.\"\n\nThis was only down to people not reading/understanding the \"release early\" so app writers could get on and port their software.  I'm not a developer and I understood the point of 4.0/4.1. If i could understand it then everyone should have been able to. The thing is that PR can only dumb down a release document a little otherwise you'd be insulting people by treating them like children. \n\nTo be honest, this \"argument\" was very tired and boring after 1 day and its even worse now after nearly a year.  Let it rest because it smells of a troll post. \n\nI stick with 3.5 for working and test 4.x for fun as another user. Do the same and you can upgrade when you are happy with changing your bad old habits."
    author: "Ian"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "Ian, don't want to be harsh but you've to consider that people is stupid or trolling (or both) in a large number of cases.\nTake Aaron's blog for example... sometimes he explain clearly feature XYZ that is going to be implemented in the immediate future and then suddenly someone ask in a comment \"will be XYZ implemented?\".\nThere is for sure positive criticism in the KDE community but there's a lot of noise and negative criticism too.. well, in many cases the latters outnumber the former."
    author: "Vide"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "I didn't say \"shut up and be grateful\", I said you have all the right to not like it and use something else instead, but if you're going to stay with it and do nothing but complain and criticize kde PR as out of reality then indeed, you'd better STFU."
    author: "kubunter"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-26
    body: "No one needs to shut up about anything.  Perhaps you think that people can be more constructive, but you do a terrible job of getting that across.  Your response to non-constructive criticism is no better than the criticism itself."
    author: "KDE User"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "Maybe the new STFU should be \"think twice what you write\"? People always get wordy everywhere over minor issues which were completely non issues if the same amount of people would write the same amount of text in a programming language instead. But not only doesn't this happen, people even move to places like Aaron Seigo's blog and actively hinder programmers to continue doing their work by distracting them with mostly nonsensical complaints. Productivity clearly looks different (unless the end product wanted is indeed nothing more than trolling)."
    author: "Anon"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "Does anybody else have the following problems:\n- system tray icons cuddled together and unusable\n- kicker (I still think that sound better than \"plasma containment resembling the classical bar\") because of compositing problems completely translucent at least with Nvidia cards (and as no \"solid\" theme is available, there's no workaround)\n- At least one plasma crash per session, mostly at logout\n- Konsole crashes after every session\n- Konsole cannot properly manage profiles\n- Startup sound cut short\n\nI'm just wondering, because all this problems seem obvious to me (so I didn't file a bug report), but they accompany me since 4.1.x times and I had high hopes, that 4.2, even beta, would cure them.\n\nBasically, I don't really see a stability difference between stable 4.1 and unstable 4.2, so I decided to run 4.2 as I hope that problems will be solved quicker in this tree :-/"
    author: "Bert"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "To reply to myself, some more remarks:\n- Konqueror simply not accepting certificates of https-sites (you always have to manually accept them)\n- Dolphin for som reasons simply does no longer accept the \"sort by alphabet\" setting. This has occured suddenly during the 4.1.x cycle.\n\nDon't get me wrong, I know that this should be in Bugzilla, however I'm a bit surprised that nobody else does seem to have these obvious problems, and so I'd like to rule out that this are local flaws of my openSUSE installation.\n\nOn the other hand, the LOOK of 4.2 is simply better than 4.1. Some ideas, like the small progress window, are really great ideas. On a whole, I like what KDE4.x (with x obviously >2) is going to be, but I see that there is still some way to go."
    author: "Bert"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "> Dolphin for som reasons simply does no longer accept the \"sort by\n> alphabet\" setting. This has occured suddenly during the 4.1.x cycle.\n\nI cannot reproduce this issue in Dolphin for KDE 4.1 + 4.2. Do you have more details how to reproduce this issue (e. g. which view mode did you use, which sorting did you have selected before, etc.). Thanks!"
    author: "Peter Penz"
  - subject: "Re: Hmmm, well..."
    date: 2008-12-01
    body: "Peter, I logged this bug a few weeks ago:- https://bugs.kde.org/show_bug.cgi?id=172413 about (lack of) ability to cancel a deletion, there's been no updates - could you take a look please?  I notice you're pretty good at responding to requests here on the Dot, so I thought I might mention it :-)\n\nMany thanks!"
    author: "Phil"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "The Dolphin problem you are reporting is something only happening in your environment, cause I've no problem at all with alphabetical sorting in KDE 4.1.3 (Kubuntu packages), and never I did (or at least I wasn't aware of :)"
    author: "Vide"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "You hit a point here:\n\nI'm usually quite active in filing bug reports. But for some of the flaws in KDE4 I'm not really sure whether to report them, as they seem so obvious.\n\nTake that \"startup sound\" problem e.g. It is, functionality-wise, only a very very very very minor problem. However, the chopped sound is the first impression you get of your shiny new desktop. This gives you an \"alpha\" feeling quite from the start. \n\nBug has been reported as #157810 in February, and seems to have a fix available now. A small thing, but a bit symptomatic for the current state, as nobody seemd to care.\n\nTo be sure, I'm using the latest bleeding edge KDE4, because I simply like the shiny new effects and features KDE4 offers. It's great, devs, go on like this!\n\nBut with 4.2 approaching I still don't see a desktop I can - without a second thoughts - install on my \"non-geeks\" friends and family-computers.  As long as e.g. many plasma crashes require a removal of the config files, KDE4 is still restricted to the die-hard fans."
    author: "Larx"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-28
    body: "> As long as e.g. many plasma crashes require a removal of the config files,\n\ni'd love to see what those are."
    author: "Aaron Seigo"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-27
    body: "This is not really the [lace to be discussing this, but I thought I'd respond with my experiences anyway. For reference, I'm using Kubuntu Intrepid with KDE 4.1.3.\n\n> - system tray icons cuddled together and unusable\nNo.\n\n> - kicker (I still think that sound better than \"plasma containment\n> resembling the classical bar\") because of compositing problems completely \n> translucent at least with Nvidia cards (and as no \"solid\" theme is available, \n> there's no workaround)\nNo, but my laptop has an ATI card. I find that with recent updates, compositing now works but I can't have it enabled because the last time I tested it, the system locked up after about 20 minutes. For that I blame the drivers, not KDE, and personally I can live without compositing.\n\n> - At least one plasma crash per session, mostly at logout\nNo. It's likely we are not using the same plasma configuration though.\n\n> - Konsole crashes after every session\nNo, works fine here.\n\n> - Konsole cannot properly manage profiles\nYes this is a known issue. This was being worked on, I thought it looked like it might be fixed for 4.2.\n\n> - Startup sound cut short\nYes."
    author: "Paul Eggleton"
  - subject: "Re: Hmmm, well..."
    date: 2008-11-28
    body: "> - system tray icons cuddled together and unusable\n\nnope; report it on bugs.k.o with a screenshot\n\n> - kicker (I still think that sound better than \"plasma containment resembling \n> the classical bar\")\n\nhow about \"panel\" or even \"plasma panel\"? i mean, sure, you can make anything sound stupid if you try hard enough.\n\n> because of compositing problems completely translucent at least with Nvidia \n> cards (and as no \"solid\" theme is available, there's no workaround)\n\nthere absolutely are \"solid\" themes provided. install kdeplasma-addons.\n\nand if you use the latest nvidia beta drivers, things should work nicer with kde4.\n\n> - At least one plasma crash per session, mostly at logout\n\nbacktrace? bugs.kde.org report? a few weeks back there was a crash-on-logout that was introduced during 4.2 dev, but i fixed that a while ago.\n\n> - Konsole crashes after every session\n\ni'm not seeing that either.\n\n> - Konsole cannot properly manage profiles\n\nit works here, but then as you didn't give a definition for \"manage\" it's impossible to know what you actually mean.\n\n\nlook, go to bugs.kde.org, file some reports and follow up on them as needed. vague lists here don't help anything get fixed."
    author: "Aaron Seigo"
  - subject: "openSUSE snapshots - Kopete"
    date: 2008-11-26
    body: "I have installed them a few hours ago. They work nicely, but you have to bear in mind that Kopete does not have MSN support out of the box because libmsn (on which now it depends for Messenger support) is not packaged yet AFAIK.\nAs it's unsupported etc. I wonder if I should file a bug report or not."
    author: "Luca Beltrame"
  - subject: "Re: openSUSE snapshots - Kopete"
    date: 2008-11-26
    body: "If your distribution hasn't packaged the new version of libmsn yet (yes, it's beta, but will go final before the KDE 4.2 release), then you should file a bug with that distribution."
    author: "Matt Rogers"
  - subject: "Re: openSUSE snapshots - Kopete"
    date: 2008-11-27
    body: "I found it in the repository after a few hours, so I assume this has been taken care of already."
    author: "Luca Beltrame"
  - subject: "Mandelbrot fractals"
    date: 2008-11-26
    body: "I didn't manage to finish the Mandelbrot wallpaper plugin for Plasma (contrary to what the announce suggests), so it'll have to be for 4.3. Meanwhile you can find it in its current state in playground."
    author: "Benoit Jacob"
  - subject: "WOW!"
    date: 2008-11-26
    body: "Looking at this screenshot: http://www.kde.org/announcements/announce_4.2-beta1/desktop-folderview-dolphin.png \n\nI really love the new progress-bar and scroll bar look, it's so nice! Also that plasma theme!. \n\nKDE4 it's getting better and better.."
    author: "Alex Medina"
  - subject: "Re: WOW!"
    date: 2008-11-26
    body: "True, it looks like a \"I must have it!\"."
    author: "James B."
  - subject: "Re: WOW!"
    date: 2008-11-26
    body: "Actually that isn't a progress bar. So far I've only seen that bar in Dolphin and Kickoff.\n\nAnyway, the Oxygen progress bar has got a new look too. :)"
    author: "Hans"
  - subject: "Re: WOW!"
    date: 2008-11-26
    body: "You're right, it's not a progress bar (it doesn't show progress). It's a new UI element called capacity bar."
    author: "sebas"
  - subject: "Re: WOW!"
    date: 2008-11-26
    body: "Wow, that tooltip is so sexy! Much better than KDE3's."
    author: "Anon"
  - subject: "Re: WOW!"
    date: 2008-11-27
    body: "Mine looks better with the Naked theme ;)"
    author: "Bobby"
  - subject: "Re: WOW!"
    date: 2008-11-27
    body: "It looks cool and I hope it does actually work.\nIn the current version you can't copy by dragging from a SFTP folder to a FTP folder on a split window... a show stopper for me."
    author: "Dexter"
  - subject: "Re: WOW!"
    date: 2008-11-28
    body: "Sounds like a serious bug... If you find this issue in the 4.2 series, PLEASE file a bugreport (if it doesn't yet exist)"
    author: "jospoortvliet"
  - subject: "Actually.."
    date: 2008-11-26
    body: "Actually I found SUSE beta 4 with KDE 4.1.3 much more stable than than the newest Fedora Cambridge GNOME. The only thing that frustrates me is not being able to easily re-arrange the items on the taskbar (and if I'm just being ignorant, please let me know in the comments)"
    author: "Rahux"
  - subject: "Re: Actually.."
    date: 2008-11-26
    body: "Just right button on the bar, \"unlock components\" and click on the icon that appears on the right.\n\nThen in \"edit mode\" you can rearrange plasmoids."
    author: "Anon"
  - subject: "Re: Actually.."
    date: 2008-11-26
    body: "I think Rahux meant the tasks in the taskbar. In trunk there is an option to sort the tasks manually. It isn't perfect yet but you can reorder the tasks with drag and drop."
    author: "Hans"
  - subject: "Re: Actually.."
    date: 2008-11-26
    body: "Who still needs a taskbar when you have active desktop corners? ;-)\n\nBye\n\n  Thorsten"
    author: "schnebeck"
  - subject: "Re: Actually.."
    date: 2008-11-26
    body: "Me. I have an ATI card, and the fglrx drivers do a good job, but because they lack DRI2 support I'm not able to watch video or run an OpenGL program when 3D compositing is enabled.\nTime to our darling ATI upstream to catch up."
    author: "Taupter (Cl\u00e1udio Pinheiro - Kopete)"
  - subject: "Re: Actually.."
    date: 2008-11-27
    body: "For me, it is faster to look up a window in the taskbar than in all the window switching effects...  "
    author: "Sebastian"
  - subject: "Re: Actually.."
    date: 2008-12-01
    body: "Arranging them in logical order in the task bar helps me to organize what I'm doing, though I usually switch with Alt+Tab. I also like it for drag-and-drop. Not to mention that I care about battery-life, and fancy effects seem to drain it faster."
    author: "yman"
  - subject: "Looks"
    date: 2008-11-26
    body: "It looks very good! I'm using it with nightly builds and it seems to be much faster here on my computer then the official intrepid's build.. strange..\n\nMy only complain is that the blue border when compositing is disabled is really really ugly.. And I didn't found how to remove it.. My solution is to use compositing, because the borders looks very smooth and beautiful with it\n\n"
    author: "Paulo Cesar"
  - subject: "Re: Looks"
    date: 2008-11-26
    body: "Sorry, but which border are you talking about?"
    author: "Hans"
  - subject: "Re: Looks"
    date: 2008-11-26
    body: "Yeah, I hate this ugly blue border too. It's really a shame! Really bad is that you can not disable it, only the colour itself can be changed thought System settings > Appearance > Colors > \"Hover Decoration\"\n\nI hope in the final this \"stupid-artwork-error\" is gone, but I dont think so!"
    author: "SONA"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "well goes back into how come we need ozone problems.\n\nBut you can change that in kcm its the active windeco color...  "
    author: "pinheiro"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "Hi Nuno, I really like your original mockups, but this modifications being made to Oxygen win decoration are pretty devastating in my opinion.. \n\nfirst that stripes thingie, and now this blue border.. I'm glad I can disable these things, but for the average user looking for a nice visual isn't that good IMHO\n\nBy the way, why the heck people has so many difficulty to distinguish between active and inactive windows??? Why they need so many excessive visual clues? It just makes everything look polluted.."
    author: "Paulo C"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "\"but for the average user looking for a nice visual\" ...\n\n... for them, $DEITY invented compositing window managers.\n\n\"why the heck people has so many difficulty to distinguish between active and inactive windows?\"\n\nbecause people aren't good at distinguishing grey on grey ;)"
    author: "Aaron Seigo"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "\"because people aren't good at distinguishing grey on grey ;)\"\n\nCount me in. And that's why I love the \"dim inactive windows\" compositing effect. Much better than all the window decorations tricks (color, stipples, shadows). Thanks !"
    author: "Vincent de Phily"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "Maybe KDe should not only detect if compositing is possible or not, but also to chose the right windows decoration if compositing is either dis- (Ozone) or enabled (Oxygen)."
    author: "Sebastian"
  - subject: "Re: Looks"
    date: 2008-11-28
    body: "It does change the windowdecoration based on compositing status. If you have compositing? Blue glow. If you don't? Blue border."
    author: "jospoortvliet"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "Why, I actually find the blue glow very useful. It makes the current window much easier to track, and it's not that obtrusive.\nAnd in my personal opinion, it's nice :) ."
    author: "Peppe"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "The blue glow is indeed very nice, but it requires compositing.. The problem comes when compositing is disabled and it appears a rough blue border"
    author: "Paulo Cesar"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "I second that. Thanks to our artist-team for the fantastic work done on 4.2! :)"
    author: "Sebastian Sauer"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "Yes I know this, but when you TURN OFF the desktop effects there is a blue 1px border around the active window. That`s looking terrible, especially when you use a brighter wallpaper.\n\nWith activated desktop effects it looks great, but without desktop effects the 1px border should be removed. It simply does not fit into the beautiful design of KDE4!"
    author: "SONA"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "totally agree"
    author: "Paulo Cesar"
  - subject: "Re: Looks"
    date: 2008-11-27
    body: "It doesn't look terrible to me because it's very thin and as some people already said, it helps me to distinguish between active and inactive windows very quickly.\nIt's okay man :)"
    author: "Bobby"
  - subject: "Very nice and solid."
    date: 2008-11-26
    body: "Thanks, KDE 4.2 is really nice, one small thing though, compositing and shadows\nseem to be really slow on my GF9800GT, it actually worked fine before. I guess i'll fill out a bugreport instead ;)\n"
    author: "anon"
  - subject: "Re: Very nice and solid."
    date: 2008-11-26
    body: "Make sure you have the latest nvidia drivers installed. While most nvidia cards do perform decently now (with 180.something), there seem to be cases where the nvidia drivers still show performance problems. If you can make sure that the reason for you performance issue is not the nvidia driver before reporting the bug, that would help us immensely."
    author: "sebas"
  - subject: "Re: Very nice and solid."
    date: 2008-11-27
    body: "It actually is the latest beta and every tweak available has always been used, everything else works fine, moving around windows get slow only when enabling the shadows. I'll file a bugreport then."
    author: "anon"
  - subject: "Re: Very nice and solid."
    date: 2008-11-27
    body: "It's strange, I also use a 9800GT with the latest (stable) drivers, no (speed) issues whatsoever."
    author: "Simon Kr\u00e4mer"
  - subject: "Re: Very nice and solid."
    date: 2008-11-27
    body: "Try using the standard shadows on all windows, size 10 and x 3, y 6, this is what i use, not very insane imo, the standard settings are somewhat ok, but not that good either."
    author: "Fredde"
  - subject: "Re: Very nice and solid."
    date: 2008-11-27
    body: "May be worth disabling all those tweaks and see if it still happens on a sane config?"
    author: "NabLa"
  - subject: "Re: Very nice and solid."
    date: 2008-11-27
    body: "and writting in the wiki if it changes something. So people with the new driver will know about the new tweaks :-)"
    author: "Beat Wolf"
  - subject: "Re: Very nice and solid."
    date: 2008-11-27
    body: "Well, it still happens, whatever used, as i use the beta at this time, no tweaks are used as those are on by default. Everything else flies, but as soon as shadows are enabled it's really noticeable when moving windows around."
    author: "Fredde"
  - subject: "Digg it!"
    date: 2008-11-26
    body: "http://digg.com/linux_unix/KDE_4_2_Beta_1_Released"
    author: "Anon"
  - subject: "Re: Digg it!"
    date: 2008-11-27
    body: "Dugg. Some more places to help spread the awesome news:\nhttp://www.fsdaily.com/EndUser/KDE_4_2_Beta_1_Provides_First_Look_at_Upcoming_User_Experience\nhttp://www.reddit.com/r/linux/comments/7fvm3/kde_42_beta_1_released/"
    author: "Tsiolkovsky"
  - subject: "kde4 rocks my pants :)"
    date: 2008-11-26
    body: "4.0.x was a good start.\n4.1 has proved to be a very stable release.\nI've been using 4.2 for some weeks and it's starting to get better and better.\n\nSo...\nCongrats to all developers. You're doing a very good joob release after release.\n\nKeep up your great work!"
    author: "Antonio"
  - subject: "Re: kde4 rocks my pants :)"
    date: 2008-12-17
    body: "Yep you are right i'm also using suse 11 factory with 4.2b and i love it. there are a few issues but they are manageable. thanks kde team!"
    author: "Pedro M. S. Oliveira"
  - subject: "Very nice panel!"
    date: 2008-11-27
    body: "I've _never_ seen such a nice panel! I mean it. The colors and gradients are decent and pleasant. It's absolutely clear which window is active, the small numbers indicating the window-count are beautiful, too. I think KDE4 will be my first KDE that I will not customize concerning the appearance. Thank you for this.\n\nDont' forget:\n\nAll this polishing, all these small details are really important.\nAll this polishing, all these small details are really important.\nAll this polishing, all these small details are really important.\n\nKeep up the good work :)\n\n\nTwo litte things:\n1. IMHO the border of the panel is just a little bit to thick\n2. The second icon from the left, the one for the device list: What's pictured there? I see a display, displaying the usb logo. And behind? Hmm, asking this question maybe points out, that maybe there is a better logo for this.\n\nThank you, KDE.\n"
    author: "klebezettel"
  - subject: "Re: Very nice panel!"
    date: 2008-11-27
    body: "thanks, thanks, thanks, and yeah my bad :) its dosent realy work that icon I need to do somthing else for device notification :)"
    author: "pinheiro"
  - subject: "Re: Very nice panel!"
    date: 2008-11-27
    body: "Yeah, the device notification icon is somewhat strange ;)."
    author: "Larx"
  - subject: "Re: Very nice panel!"
    date: 2008-11-27
    body: "No don't change it. I think it looks great :)"
    author: "Yilmaz"
  - subject: "Re: Very nice panel!"
    date: 2008-11-27
    body: "Hmmm not sure if that helps but I would suggest a generic (if such metaphor exists) external device with an green arrow (about 1/4 the size). If you can't find such a generic device then use the most common removable media which would be usb flash drive of an external HD.\n\nThanks for everything guys. You make using my computer beautiful!"
    author: "Leonidas"
  - subject: "Great release! but.."
    date: 2008-11-27
    body: "Great release as always :) Loving the changes so far.\nThere's just one thing I'm wondering about: How far is the work on the NetworkManager plasmoid going? Is one of the goals for the 4.2 release having that plasmoid or not?\nAnd can we expect to see it next beta or sometime soon? :)\n\nThanks!"
    author: "Ahmad Yasser"
  - subject: "Re: Great release! but.."
    date: 2008-11-27
    body: "NetworkManger plasmoid can not make it in kde4.2.  But do not worry, most distributions will provide it. "
    author: "Zayed"
  - subject: "Re: Great release! but.."
    date: 2008-11-27
    body: "i read that opensuse 11.1 will ship it...."
    author: "els"
  - subject: "Anybody notice..."
    date: 2008-11-27
    body: "Anybody else notice that those screen shots don't have the cashew? Is that 'for true'? If so Merry Christmas... (Yes, I know there'll be a containment selector. Actually shipping a containment without the cashew would make a fair number of people happy.)"
    author: "James Spencer"
  - subject: "Re: Anybody notice..."
    date: 2008-11-27
    body: "Plasma is about choices ! In kde 4.2 you can disable it and also you can move it!"
    author: "Zayed"
  - subject: "Re: Anybody notice..."
    date: 2008-11-27
    body: "it's a distributon customization.\n\nnot giving people guaranteed access to things like the appearance settings (which lets you switch containments among other things) is a little crazy. (remember that a containment may decide to provide a useless, or even no, context menu!).\n\nbut as the other person said, plasma is about choice. you can choose to be as crazy as you wanna be."
    author: "Aaron Seigo"
  - subject: "Re: Anybody notice..."
    date: 2008-11-27
    body: "> not giving people guaranteed access to things like the appearance settings\nWhich should not be the case as there should be a central place where all the settings are accessible: The settings program! \nI still can't get this to work (plasma crashes endlessly although I recompiled everything from scratch), so I don't know if the appearance setting is where it belongs, but if it isn't...\nAnd if you need a reason why people are disabling the cashew (which will be my first action if I ever get plasma to work at all): I use my computer for image editing, unfortunately the new wide colour gamut monitor I now have has garisch colours if you show things in sRGB (it's tuned to AdobeRGB in which it delivers 98% of the defined colour space), so anything on the desktop that is not colour managed to the AdobeRGB colourspace is an eyesore and might even trick the dynamic contrast engine of the monitor to kick in and modify one area of the backlight into behaving differently and messing with the appearance of things in that area!"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Anybody notice..."
    date: 2008-11-27
    body: "this sounds more like a qt problem than a cashew problem..."
    author: "Beat Wolf"
  - subject: "Re: Anybody notice..."
    date: 2008-11-27
    body: "So, I now have plasma only crashing every 10-15 minutes. Unfortunately the settings don't survive logging out and in again - some reset to their defaults... What is gone though (lo and behold) ist the abomination that is called the cashew. It was there at the beginning but somehow didn't survive the first switch to different settings and hasn't resurrected itself like those other settings did.\nThe screen settings are there in three disjunct and functionally incomplete (each one of them has differnt omissions against the others) configuration screens. One can be reached through the context menu (no cashew neede here), the other two are buried under similar headings in the system settings program. I'll file a bug report to join those three because things like this not only annoy me, they are inconsistencies which I hoped would not find their way into a new KDE..."
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Anybody notice..."
    date: 2008-11-28
    body: "If plasma crashes so often for your, and you don't run beta or alpha plasmoids from the playground, you really need to thoroughly check your hardware. If you're using Gentoo, you need to check the compile flags you are using. The problems you mention are not common at all: I see people using KDE4 at work, I use it at home (opensuse factory packages no less) and my wife uses it, on all kinds of hardware and I never see a crash in plasma."
    author: "Boudewijn Rempt"
  - subject: "Re: Anybody notice..."
    date: 2008-11-28
    body: "So for me on two very different machines. I think I didn't have any plasma crashes since the update to 4.1, although my OpenSuse has many backports (autohiding rocks!) from svn."
    author: "Cyril"
  - subject: "Re: Anybody notice..."
    date: 2008-11-28
    body: ">Which should not be the case as there should be a central place where all the settings are accessible:\n\nAlso you can just right click the desktop!\nI do not know why Aaron is so in love with that thing, maybe to expose the Plasma icons to users?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Anybody notice..."
    date: 2008-11-28
    body: "> Also you can just right click the desktop!\n\nunless you're using some other custom Activity type .. you know .. like a folderview.\n\n> I do not know why Aaron is so in love with that thing, \n\ni've explained it, including to you, a few times. you may not understand or agree with my reasons, but i've shared them.\n\n> maybe to expose the Plasma icons to users?\n\nand that isn't it. c'mon, Iuri, why do you have to be like that?"
    author: "Aaron Seigo"
  - subject: "Re: Anybody notice..."
    date: 2008-12-20
    body: "I know there has been loads of work done to plasma. I'm using KDE 4.1.3 right now. But as a user I see no need for the cashew. After I have all the widgets set where I want them then I have no need for it to be in the corner there. At the very least give us an option to move it or remove it. I've read that plasma is about choice...except with the cashew.\n\nits kind of like a pimple that can't be popped. //no offense to the devs. I'm just personally not into it. "
    author: "Clifford"
  - subject: "Re: Anybody notice..."
    date: 2008-11-28
    body: "> Which should not be the case as there should be a central place where all the \n> settings are accessible\n\nso Add Widgets should be in the settings program? not even kicker did that. why? because it doesn't actually make sense.\n\n> which will be my first action if I ever get plasma to work at all\n\nreading the comments, it looks like you have a very messed up installation. =/\n\n> that is not colour managed to the AdobeRGB colourspace is an eyesore\n\ncan you provide a screenshot of this so i can see the problem?\n\n"
    author: "Aaron Seigo"
  - subject: "Re: Anybody notice..."
    date: 2008-11-28
    body: "> reading the comments, it looks like you have a very messed up installation. =/\nI don't think so, I do a fresh install from source every 2-3 days now and update to current every day... Since I removed the desktop view I have far fewer crashes. There still are some when I try to log out or access settings in plasma - but the latter I have stopped trying since I have finally managed to get the desktop to be as empty as possible - i.e. completely empty by pointing the folderview to an empty folder. The cashew has gone (it was getting visible at times when the panel did it's autohide). \nThe argument that the context menu of the folderview doesn't allow access to the appearance settings is moot, it does...\nOh and someone should do something about the splash screen background, it is awful in terms of posterisation of the colour gradients..."
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Anybody notice..."
    date: 2008-11-30
    body: "A fresh install from source does not necessary not help if you have messed up install, like something wrong with the building blocks you base your KDE install on. Or even how you do your build, repeating the error each 2-3 days will not fix the problem. \n\nIf it's common, the amount of problems you describe, it's very unlikely Aaron and the other Plasma developers would not have seen it them selves or at least had a large quantity of reports saying the same thing. As this seem not to be the case, it leaves your installation as the most likely suspect."
    author: "Morty"
  - subject: "Impressions"
    date: 2008-11-27
    body: "I'm using builds from svn.\n\nAkonadi migration worked flawlessly on two user accounts. I can now easily and reliably use gmail from kmail. Nice.\n\nPanel has resizing in all directions, placing widgets where you want, hiding. Works like we all want it to. Still has a cashew.\n\nThe desktop cashew is gone. Noticed it last week or so. I'm waiting patiently for the ability to put non plasma windows into activities. Then it will be very useful. Now, just potential.\n\nNotifications are nice, unobtrusive and where they should be. Well done.\n\nThe menu window and other plasma stuff seems to do double redraws. And the first time you use the menu, it pops up a window with noise, then draws.\n\nI use dragon player for watching dvd's and avi's. The sound works periodically. This was a problem, then it seemed to be fixed, but is back again the last week or so. DVD's work all the time. avi's are where the problem shows up. I can fix it by running mplayer from the command line, quitting then opening dragon player again, and it works.\n\nAll in all, very nice. The whole thing seems to be hitting the sweet spot where all the new ideas work, and you can hear the gears grinding out neat ideas based on the new infrastructure.\n\nDerek"
    author: "dkite"
  - subject: "Re: Impressions"
    date: 2008-11-27
    body: "A couple questions/comments.\n\n1) Where can I find some good information on how Akonadi is supposed to work, what apps it depends on, and how to configure it.  After the last update, when I start my desktop, what seems to be the akonadi migration tool tries to start.  However, I get a lot of errors.  I didn't want to post the errors here, but I could if necessary.\n\n2) Regarding notifications, a ton of them seem to pop up when I log into my desktop.  Many times I don't close konqueror when I log out, so a lot of the notifications seem to be related to web pages being transferred.  Is this normal?  How can I turn those off?"
    author: "cirehawk"
  - subject: "Re: Impressions"
    date: 2008-11-27
    body: "Hi,\n\nAkonadi is a desktop-agnostic cache for Personal information data. For most users akonadi is invisible, but it allows developers to write less code to integrate personal data into your user experience. The personal data I refer to is your emails, contacts, appointments and notes. You might have contacts stored in several resources, such as several vcard files, some kind of groupware server, and your gmail account. If you want 'All my contacts' for example in a plasmoid, in kmail, or in kopete, previously the developer of each application would have to write code to support each resource. Now, because Akonadi knows how to access each of those resources, all contacts simply and easily available to applications which support Akonadi.\n\nhttp://pim.kde.org/akonadi/\n\nPlease send the migration errors to kde-pim@kde.org.\n\nCheers,\n\nSteve."
    author: "Steve"
  - subject: "Re: Impressions"
    date: 2008-11-27
    body: "> The desktop cashew is gone.\n\nthat would be a distribution customization."
    author: "Aaron Seigo"
  - subject: "Re: Impressions"
    date: 2008-11-27
    body: "\"I'm using builds from svn\".  \n\nMaybe someone ate it.  I love cashews, especially roasted with salt, sugar and a drop of lime juice."
    author: "Will Stephenson"
  - subject: "Re: Impressions"
    date: 2008-11-27
    body: "perhaps the bug that was fixed now that placed the cashew at strange places. So perhaps the cashew is still here, but not where it should be"
    author: "Beat Wolf"
  - subject: "Re: Impressions"
    date: 2008-11-30
    body: "or more likely a flakey configuration. Occupational hazard when using svn builds.\n\nDeleted the plasma config files, and it's back.\n\nDerek"
    author: "dkite"
  - subject: "Re: Impressions"
    date: 2008-11-27
    body: "\"The menu window and other plasma stuff seems to do double redraws. And the first time you use the menu, it pops up a window with noise, then draws.\"\n\nYes this is the most irritating thing... maybe it's a QT problem, maybe a driver/xorg/qt combination problem, but definitely shouldn't happen."
    author: "Vide"
  - subject: "Re: Impressions"
    date: 2008-11-28
    body: "Yes, this is a really ugly regression since 4.1, that doesn't have this problem. Hope this will be fixed for 4.2 final"
    author: "John Doe"
  - subject: "Re: Impressions"
    date: 2008-11-28
    body: "Same bug in kubuntu with default nvidia drivers....\n\nNo bug with beta 180 drivers ;) So it's a nvidia bug."
    author: "Cedric"
  - subject: "Re: Impressions"
    date: 2008-11-29
    body: "It is not an nvidia bug, since I'm on intel hardware and with the same hardware/driver i don't get this behaviour in 4.0/4.1"
    author: "John Doe"
  - subject: "Go binaries!"
    date: 2008-11-27
    body: "Erm, seems openSuse did it again.... \n\n\neagerly awaiting my Kubuntu binaries.... wondering which distro will be releasing packages up next.. :D"
    author: "Dread Knight"
  - subject: "Device Notification"
    date: 2008-11-27
    body: "Will the device notification plasmoid be placed into the system tray in KDE 4.3? I understand that putting plasmoids into the tray in 4.2 is a little funky right now.\n\nWith that said, KDE 4.2 looks amazing!"
    author: "Anon_Coward"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "\"Will the device notification plasmoid be placed into the system tray in KDE 4.3?\"\n\nwe haven't decided on that yet. we have a round of \"defining the system tray and its uses, circa 2009\" to do for 4.3 before we can answer that question."
    author: "Aaron Seigo"
  - subject: "One little thing missing"
    date: 2008-11-27
    body: "There is one little thing that I am personally missing Aaron and that's a little programme that asks me which application should be used to open an inserted disk. Yes I am referring to that nice little application in KDE 3.5x that pops up when I insert a particular disk for the first time and gives me the possibility to choose. I think that it even works with digital cameras. BTW, will Digikam be ready for 4.2 (I am too lazy to check the Feature Plan)?\n\nThe device notification is fine but it's only allows me to open devices in Dolphin and it dosn't respond to digital cameras."
    author: "Bobby"
  - subject: "Re: One little thing missing"
    date: 2008-11-27
    body: "Sorry about Digikam, it  wasn't installed with the desktop but I found it in the repo. I was using the KDE 3 version all the time."
    author: "Bobby"
  - subject: "Re: One little thing missing"
    date: 2008-11-27
    body: "Digikam is not in the core KDE packages (which are shipped with this release) but in extragear, so it certainly won't be found in the feature list - regardless of whether it's ready in time or not :P"
    author: "Jakob Petsovits"
  - subject: "Re: One little thing missing"
    date: 2008-11-28
    body: "yes, we will be providing a customization UI for that at some point.\n\nand currently, if there is more than one action installed you get to pick between them."
    author: "Aaron Seigo"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "\"we haven't decided on that yet. we have a round of \"defining the system tray and its uses, circa 2009\" to do for 4.3 before we can answer that question.\"\n\nInstead of that, how about \"should we kill the systray, yes or hell yes?\" instead ;)?\n\nI think that systray could be easily replaced by notifications and plasmoids. For example, software-updates. Instead of having an icon in the systray that tells me about updates to my distro, how about using the KDE's built-in notification-system instead? That's just one example. \n\nThere are \"legacy apps\" out there that use systray, and some functionality could be provided to them, but KDE4-apps could use new and better things, as opposed to using the systray as a landfill of semi-useless icons."
    author: "Janne"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "1000000% agree.\nAnd if someone states \"hey but the play/stop/etc\" functionalities of Amarok's tray icon are a must then you have two options:\n- Amarok shipping and installing a control plasmoid\n- Implement the systray right click features in the amarok window context menu (yes, this sounds like osx, and indeed it is)\n- provide a legacy systray for other not-controlled environments (gnome, windows)"
    author: "Vide"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "I like it, the sound icon remains me the \"windows 95\", why not replace it with a real volume bar plasmoid (like kde 3 a sound applet with 2 channels , pcm and wave) more beauty and useful. yes i will tried to do it myself , but i don`t grantee successful. tnks for this beautiful release, kudos developers"
    author: "Draconiak"
  - subject: "Re: Device Notification"
    date: 2008-12-12
    body: "Actually, eliminating the systray would be a huge improvement. Simply because if this tray were eliminated one could also replace the taskbar with interactive plasma icons with scrollwheel support for a mixer interface or application window switching grouped by application. Normal taskbar menus could be 'emulate 3 buttons' or mousewheel-click hidden. Kmail icon could be overlaid by an unread-messages count, and Xsytray(tasktray?) could be piped through KDE notifications with a notification history for KDE notifications in a kickoff tab."
    author: "James Smith"
  - subject: "Re: Device Notification"
    date: 2008-12-12
    body: "I dont get this, if you dont need the tray, then dont use it.\nI for one, need it - why wont you let me keep it?"
    author: "Kolla"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "I kinda agree with you... I do think KDE should make this transition as time goes by, from systemtray \"icons\" to plasmoids and notification... Amarok, for instance, would be great with that...\n\n but there are some problems regarding killing the systemtray:\n\n\nThe problem is what if someone use a non-kde app that works with the freedesktop systemtray specs and doesn't use kde notification system?\nFor instance, I use aMule (I haven't found a qt/KDE version of it yet ... :( ). There are a lot of gtk apps that do so... and unfortunetly they are not legacy apps, just apps from another DE.\n\nFrom a user perspective, there are many many linux users that don't know the difference of Gnome and KDE, (for them.. they are just apps on the screen), so for them, a systemtray is just a systemtray.\n\nThe main problem here is in the freedesktop's systemtray specs. I am amazed how kde developers got a solution that works with this old specifications and even brings something new (plasmoids inside the systemtray).\n\nUnfortunately, our dream of killing the systemtray will have to be postponed... it depends on another spec that KDE people can not change...\n\n\n"
    author: "SVG Crazy"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "\"The problem is what if someone use a non-kde app that works with the freedesktop systemtray specs and doesn't use kde notification system?\"\n\nUm, I did mention that scenario in my post.... there would be legacy-support for non-KDE-apps, but KDE-apps would use new technology. But right now both KDE and non-KDE-apps use systra, and the end-result is a systray that is brimming with icons."
    author: "Janne"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "Yep, systemtray can be full of icons! I personally don't like it ... Thanks God (and kde developers) now we can hide icons on systemtray.\n\n"
    author: "SVG Crazy"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "Hiding the icons does not resolve the problem, it's just a workaround."
    author: "Janne"
  - subject: "Re: Device Notification"
    date: 2008-11-28
    body: "yeah, that's is a really big problem, dont know how this start(win??).\n\nbut now is a really mess, is good that you can \"hide\" apps and dont waste space in the task bar, but if you have too much apps in the tray you waste space.\n\na good idea maybe call for a brainstorm to solve this, and keep a plasmoid for \"legacy apps\"(aka non kde-apps)."
    author: "rudolph"
  - subject: "Re: Device Notification"
    date: 2008-11-27
    body: "I am missing this feature, too. I would find it really cute if I could select \"open with DragonPlayer\" or something in the device notifier. Opening in dolphin is pretty useless for DVDs for most cases."
    author: "knue"
  - subject: "fine"
    date: 2008-11-27
    body: "ah, looks very promising, as always. i'm using kde4 since their first beta-releases for 4.0. granted, it was a \"rocky road\" in the beginning, but now on 4.1.3, everything is just working and i don't miss a thing. kde4 ist the first kde i did not customize. \n\nplasmoids are great, especially folderviews, work is being done on the nvidia-drivers (thank god!). now where i really would like to see some progress is the new \"raptor-menu\". the first concepts looked promising..."
    author: "kubuntu-user"
  - subject: "About \"file size view\" in konqueror"
    date: 2008-11-27
    body: "Thanks to the KDE devs for their efforts. I'm using KDE 4.1.3 now but I'm still missing the \"file size view\" in Konqueror 3.5. Konqueror 4 and Dolphin appears to not have it. Is it planned to be back soon in Dolphin or dropped definitely?"
    author: "Andry"
  - subject: "Re: About \"file size view\" in konqueror"
    date: 2008-11-27
    body: "Ask your distro :) It's been available for Konqueror since at least 4.1 (quite probably 4.0 - I didn't check) but is part of extragear/kdebase so will likely not be installed by default.\n\nI don't think it will appear in Dolphin, though."
    author: "SSJ"
  - subject: "Re: About \"file size view\" in konqueror"
    date: 2008-11-27
    body: "I found it. Thx a lot"
    author: "Andry"
  - subject: "Xmas?"
    date: 2008-11-27
    body: "It must be an early Xmas\n\nKDE 4.2 Beta and then within a day Opensuse 11.1 RC1\n\nThanks everyone for the work you have done on it, it is looking damn fine.\n\nThanks to all who have reported bugs so that they could be fixed"
    author: "R. J."
  - subject: "No Administrator Mode Possible in Log-in Manager"
    date: 2008-11-27
    body: "Bug 151669, sounds almost like a song. This is a 1 year old bug. 4.0 was released with it, 4.1 and now we are at 4.2 Beta 1 and it's still there. Will 4.2 be released with this bug? Is it so difficult to fix?\nWell I don't know because I can't code and I appreciate the programmers' work and effort a lot. \nI don't want to sound like a whiner because I hate complaining but this bug is really annoying and one of the few glitches in KDE 4 that causes me to still install KDE 3.5x even though I only use bits and pieces of it.\n\nAnyway I would still like to say a big thanks to the KDE team for it's great effort as usual."
    author: "Bobby"
  - subject: "Re: No Administrator Mode Possible in Log-in Manager"
    date: 2008-11-27
    body: "Hi Bobby,\n\ni am using kdemod 4.1.3 and i have this function, maybe it is an improvement from the kdemod packaging team. But there is another possibility: open your konsole, type sudo systemsettings et voila there you are. You can change everything in administrator mode.\n\nCheers,\n\nFelix"
    author: "Felix"
  - subject: "Re: No Administrator Mode Possible in Log-in Manager"
    date: 2008-11-27
    body: "Thanks for the tip, I will try it. I guess you are using KUbuntu so maybe it's a distro modification since it still exist as a bug. OpenSuse hasn't made this modification as yet so I will try the console trick ;)"
    author: "Bobby"
  - subject: "Re: No Administrator Mode Possible in Log-in Manager"
    date: 2008-11-27
    body: "If by KDEmod you mean http://kdemod.ath.cx/ then it's Arch not Kubuntu."
    author: "Leonidas"
  - subject: "panel"
    date: 2008-11-27
    body: "why does the panel have to look exactly like in windows vista? feels kinda out of place to me."
    author: "Jurgis"
  - subject: "Re: panel"
    date: 2008-11-27
    body: "I guess any panel will look like Vista's if you use a black background ;)"
    author: "Bobby"
  - subject: "Re: panel"
    date: 2008-11-27
    body: "The panel much more looks like in Windows7 - MS did a good job in copying ;-)"
    author: "Birdy"
  - subject: "Re: panel"
    date: 2008-11-27
    body: "That's true. I notice that they are using a really high panel that's mostly common in KDE until now. Remember how people complained about the KDE panel being too high. Well somehow MS likes it and has copied it, one of the main changes in Windows 7.\nDo you know what will happen at the end of the day? People will still be saying that KDE copied it from MS."
    author: "Bobby"
  - subject: "Re: panel"
    date: 2008-11-27
    body: "i don't really care who copied what because xerox made the first GUI anyway. it's just that in windows this gradient thing on panel looks good, but in kde it feels out of place because that kind of gradient isn't used anywhere else in kde. even the plasma widgets look different"
    author: "Jurgis"
  - subject: "Re: panel"
    date: 2008-11-27
    body: "Panel looks exactly like in Vista if it is transperent\n\nI prefer using slim glow (theme) panel"
    author: "George"
  - subject: "Re: panel"
    date: 2008-11-27
    body: "Well, I didn't want to agree with you but I have too...\n\nI think this new panel theme is out of place with the rest of the oxygen theme. On KDE 4.1, I think the only thing in common with windows vista theme was the color of the panel (black), nothing more. There was a white line with gradient on the top of the panel that matched with the rest of the plasmoids theme. It didn't look like vista at all (although some people think that vista is the only OS that can use black.. everything that's black is a \"copy\" of vista for those people.. :) ). \n\nNow the panel background is really looking like vista in appearance(IMHO)... with that gradient... even more when you use dark wallpapers.\n\nI do like oxygen theme, so what I am doing now is using the 4.1 panel background theme with the 4.2 oxygen theme. This can be done with \"Desktop Theme Details\" on System Settings.\n"
    author: "SVG Crazy"
  - subject: "Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "Count all the bugs I encountered after using it for only 30 seconds (from svn, clean ~/.kde4):\n\n1. I wanted to resize the panel -> background goes gray. Only fix is to log out\n2. I wanted to start systemsettings using krunner. It didn't work as expected. No completion.\n3. Add widgets dialog has white fonts on white background\n4. Adding widgets to the panel - plasma didn't let me add that analog clock to the left side of the panel (it reserved the space for it on the right, see the picture). It worked after three attempts. \n\nCtrl+alt+backspace and log in to kde 3.5. I'm waiting for beta2. ;-) \n"
    author: "vf"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "As a professionnal user I fully agree with you : KDE4 = Katastrophic Disturbing Experience.\nJust a minor exemple : Dolphin  don't even know to properly count its files ... and a major one : Klipper likes to silentiously drop its last content in the file you are writing (try to update a website with Quanta under Kde4...: most of the scripts will become broken and hard to clean.)\nScore more stop-overs in Shop... from Ark to Kttsd nothing is yet usable for production...(dont answer me to post bugs : it seems useless...)\n"
    author: "jerome"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "\"As a professionnal user I fully agree with you : KDE4 = Katastrophic Disturbing Experience.\"\n\nLike I said elsewhere: KDE4 is 10 months old. After 10 months, OS X was still a disaster. after 10 months, KDE2 still had issues. And now you guys are complaining about a BETA-release. After 10 months, GNOME2 was still unusable. And Vista is still crap ;).\n\n10 months. Keep that in mind."
    author: "Janne"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "I agree with you.\n\nUnfortunately, people still do not see the huge step KDE is taking in such a short period of time. Plasma, for instance,  is less than one year old, and is already doing great things. It is not perfect yet, of course... but I can imagine many amazing things plasma (and other new KDE technologies)  will be doing in a not so far future (3, 4 years)."
    author: "SVG Crazy"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "Thank you :) I guess even Jesus would feel overexerted at this point.\n"
    author: "Bobby"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-28
    body: "I am a professional user. I moved to KDE 4 slowly after 4.1 was released. Very happy with it. It's really stable... some things missing but nothing major, and overall it's much better for me than 3.5.\n\nKeep up the good work guys!\nCan't wait for KDE 4.2 :) \n"
    author: "Jad"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "quite a good bugreport, with screenshot and all that. but could you post this on bugs.kde.org? most revelopers wont read all comments on this newsentry. And probably you will have to help a little with how to reproduce, because i can't reproduce any of those bugs. But perhaps they have been fixed after beta1 has been tagged. there was alot of bugfixing just after the tagging"
    author: "Beat Wolf"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "I used trunk from yesterday to test so it's still not fixed. I can reproduce it always with clean configuration. Tested on a laptop (intel graphics and driver). I won't report bugs to the bugzilla as these are obvious, other bugs which I reported/voted for are still open (after many months), and my suggestions are ignored. So bugzilla is, for now, of no use, at least for me.\n"
    author: "vf"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "it might be that your distro's default set up scripts are messing things up. using a clean .kde4 with svn in intrepid would do all sorts of bizarre things that should not have been happening (and didn't with another distro). I can't say for sure it is intrepid's fault as I only tried another distro, or that this is even the same problem...but it is worth bearing in mind if you can't get others to reproduce this bug"
    author: "txf"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "I tested the mentioned use cases.\n\n1. True. Should be reported, I will check the b.k.o if it is reported or not.\n2. True. Autocompletion didn't work when trying to run the systemsettings for the first time.\n3. Could not reproduce.\n4. Could not reproduce.\n\nOtherwise my 4.2b1 instalation runs pretty well on my machine."
    author: "shaga"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-27
    body: "The resizing stuff filed as http://bugs.kde.org/show_bug.cgi?id=176280"
    author: "shaga"
  - subject: "Re: Stop looking at the pictures and try *using* it!"
    date: 2008-11-28
    body: "2. Second time works because it has systemsettings in history. It doesn't execute it like it should, like kate for example.\n3. I don't know. That dialog doesn't use KDE color settings, it takes that info from somewhere else.\n4. Adding plasmoids to panel is buggy, it sometimes works and sometimes doesn't.\n\nOtherwise, it crashes all the time. :-)\n"
    author: "vf"
  - subject: "Re: Stop looking at the pictures and try *using* i"
    date: 2008-11-28
    body: "I've got another one:\nafter a plasma crash, and they happen often with 4.2 (in current version, I know it is a beta!), the plasma configuration icon that normally is on the right side of panel goes to left, same for the one in desktop!\nThe problem is that it occupies more than half of the k-menu, then you must click on the little space left to use it.\nLocking plasmoids hide this icon, so you can use k-menu, but still a ugly bug.\n\nTo be honest, more and more I'm looking and testing another window managers and DEs. KDE4 apps in general are really nice (I love dolphin and krdc!), but the desktop (hello plasma!) is fat, full of bugs yet, and not worth the trouble if you just do not use plasmoids and like nice desktop effects.\n\nAnd nowadays, using KDE3 is not a option anymore, since distros are dropping it :-("
    author: "Iuri Fiedoruk"
  - subject: "Eyes applet"
    date: 2008-11-27
    body: ">> Beta1 offers critical features like the Eyes applet (an XEyes clone)\n\n!!! is it critical really?\n\n"
    author: "VJP"
  - subject: "Re: Eyes applet"
    date: 2008-11-27
    body: "Fineprint: If you find sarcasm in there you can keep it."
    author: "Anon"
  - subject: "Re: Eyes applet"
    date: 2008-11-28
    body: "because without them your computer wont be able to see you ;)"
    author: "R. J."
  - subject: "KDE4"
    date: 2008-11-27
    body: "Is is \"getting there\". Still progress is very slow and the beta mostly indicates what won't be ready for the next release. \n\nThe \"critical feature\" of an Eyes applet sounds funny. What about critical bugs as https://bugs.kde.org/show_bug.cgi?id=157630 which made Faure to recommend users to use KDE 3.5.x?"
    author: "Plumplum"
  - subject: "Re: KDE4"
    date: 2008-11-27
    body: "That is not critic. We are trying to consolidate the plasma effects first and then we'll bring our attention to such secondary things for a file manager such as moving/copying files around. Expect that and similar bugs fix in 4.4 or 4.5."
    author: "Max Maxell"
  - subject: "Re: KDE4"
    date: 2008-11-27
    body: "Dont have such bug in any of my computers versions of kde4"
    author: "Pinheiro"
  - subject: "Re: KDE4"
    date: 2008-11-27
    body: "Same here, i dont have this problem. I can move folders in any crazy way i would like to and also i gwenview and dolphin. I am using kdemod 4.1.3 and not KDE 4.2. But your bug seems to be related to the version 4.1.3. Sorry, dont understand your concern."
    author: "Felix"
  - subject: "Re: KDE4"
    date: 2008-11-28
    body: "\"Still progress is very slow\"\n\nHuh? Progress has been humungously fast. It's been only 10 months since 4.0, and there is a night and day difference between 4.0 and the latest version!\n\n\"What about critical bugs as https://bugs.kde.org/show_bug.cgi?id=157630 which made Faure to recommend users to use KDE 3.5.x?\"\n\nLooking at the bugreport, it seems like a bug that is hard to reproduce, but people seem to be actively discussing it and looking for a fix. So people are working on it, what more do you want? Do you think that developers should stop everything else, and focus on individual bugs only?"
    author: "Janne"
  - subject: "Re: KDE4"
    date: 2008-11-29
    body: "KDE4 is nowhere near beeing rock-solid. I don't even mind missing features. My definition of bad software is software where you bumb into a bug within 3 minutes."
    author: "Plumplum"
  - subject: "Re: KDE4"
    date: 2008-11-29
    body: "KDE 4.2 isn't released yet (eg this is the first beta) so how can you know you'll jump into a bug within 3 minutes? Sure, you can do that in 4.0 and 4.1 but then again, those are meant for the adventurous users, not ppl looking for super-stable anyway. And we've been clear about that."
    author: "jospoortvliet"
  - subject: "Some questions"
    date: 2008-11-27
    body: "Hi I am using KDE 4.1.3 now and I really like it. I'm looking forward to 4.2. However I have some questions.\n1. What's the state of raptor?\n2. What's the state with strigi integration? In 4.1.x it just doesn't work.\n3. What's up with kget? It is unusable buggy (tried 4.1.3 gentoo and 4.1.3 kubuntu)\n4. In 4.1.x are dozens of layout problems. (kmail config too large, wasted space in Konqueror menu/toolbar, similar problem in kopete, the display system setting module, just to name a few). Has been work done to address these problems?\n\nCheers\n"
    author: "knue"
  - subject: "Re: Some questions"
    date: 2008-11-28
    body: "dunno about raptor.\nstrigi got a bit better, I believe. I don't use it, but KRunner does show all kinds of results when I search so apparently it works ;-)\nKGet was hopeless for a while, it's slowly getting better. Not there yet.\nLayout stuff has been steadily improving, but I don't think it'll be perfect in 4.2"
    author: "jospoortvliet"
  - subject: "Re: Some questions"
    date: 2008-11-29
    body: ">strigi got a bit better, I believe. I don't use it, \n\nhum... But you are strigi's number 1 dev and first creator of strigi and you have pushed for its integration into kde4 ? What do you mean with \"you don't use it?\"\n\n>but KRunner does show all kinds of results when I search so apparently it works ;-)\n\nI see your point ;-).\n\nQuestion: do you know when one will be able to call stigi search as an io-slave like my old kio-clucene or the beagle io-slave did ? This would so nice for the kde4 experience.\n\nHappy day\nxh"
    author: "anonymous"
  - subject: "Re: Some questions"
    date: 2008-11-30
    body: "\"hum... But you are strigi's number 1 dev and first creator of strigi and you have pushed for its integration into kde4 ? What do you mean with \"you don't use it?\"\"\n\nYou're thinking of the other Jos ;) (Jos van den Oever)"
    author: "Anon"
  - subject: "Re: Some questions"
    date: 2008-11-30
    body: "ohoh. You are right. Sorry for this silly comment.\n\nCheers"
    author: "anonymous"
  - subject: "Any progress on the Mac OS Menu bar? "
    date: 2008-11-27
    body: "Sorry, if I ask again. I really appreciate the good work of the KDE devs!\n\nThe last time I asked for the Mac OS style menu bar, Aaron expected it to be fully ready for 4.2.\n\n"
    author: "furanku"
  - subject: "Re: Any progress on the Mac OS Menu bar? "
    date: 2008-11-27
    body: "No, no progress.  This is something that someone else is going to have to step up and fix."
    author: "Anon"
  - subject: "Re: Any progress on the Mac OS Menu bar? "
    date: 2008-11-27
    body: "A dock would be more interesting to me for a little eye-candy. Any progress on the OS X like Dock? Someone mentioned it some time ago but i forgot the name (not XQDE)."
    author: "Bobby"
  - subject: "Re: Any progress on the Mac OS Menu bar? "
    date: 2008-11-27
    body: "The bespin style is bundled with some goodies like the xbar plasmoid. Put it on a panel and you have OSX menu bar working flawlessy!\n\nAnd since this is OSS, why don't you just use that existing code?"
    author: "xbullethammer"
  - subject: "Re: Any progress on the Mac OS Menu bar? "
    date: 2008-11-28
    body: "But it works only with Bespin set as UI style..."
    author: "Sebastian"
  - subject: "Wireless manager ?"
    date: 2008-11-27
    body: "From what I can see in the changelog and on the screenshot, most of the missing features of kde 3.5.x are back in 4.2\n\nThat is excellent news. I'm using 4.1 but many tools and features are not present.\n\nBut I didn't see any info about some wireless manager/tools/plasma integrated in 4.2.\n\nAnyway I will give it a try during this weekend.\n\nMany thanks to the KDE team."
    author: "JC"
  - subject: "Re: Wireless manager ?"
    date: 2008-11-27
    body: "as Aaron Seigon wrote on his blog there are no networkmanager plasmoid for 4.2. Opensuse is giving one but for what I saw it's not working but showing the different wifi available."
    author: "Albert"
  - subject: "Re: Wireless manager ?"
    date: 2008-11-28
    body: "But there will be a network manager avalibel for 4.2. It will not be a part of the official KDE 4.2 release, wich is nothin new since no KDE release ever have had a networkmanger. But it will be relesed as a 3rd party application, so no one needing the functionality will suffer. "
    author: "Morty"
  - subject: "Are there packages for Fedora 10 yet?"
    date: 2008-11-27
    body: "Cause I want to test this :D"
    author: "fabiank22"
  - subject: "Re: Are there packages for Fedora 10 yet?"
    date: 2008-12-01
    body: "No, we're still working on getting it to build in Rawhide."
    author: "Kevin Kofler"
  - subject: "42!"
    date: 2008-11-27
    body: "But what was the question? ;)"
    author: "Robin"
  - subject: "Will this ever stop?"
    date: 2008-11-27
    body: "So once again I start reading and then - Everywhere I look I see complaints about this and that and and and... From the developers point of view it must really suck sometimes. Well, look at it this way - while you go over to a friends house there will be a FOSS dev that will rather spend some time implementing something new, while you go away for a weekend there will be someone coding and testing a new idea that could make things a lot better for us users, while you spend time with your family there will be developers who drop everything to fix a major security hole, while you enjoy your undeserved sleep there will be someone staying up coding because just one person motivated him/her by saying thank you for all their sacrifices. What have you done lately for FOSS? If you are not happy why don't you rather shut up and fix it yourself? After all, you have the source code? Why don't you rather look at what has been done so far? Why TF do you not get off your @ss and do something about it? Why can't you see that these developers are the people who made it possible for us to have freedom of choice when it comes to software? Without them we will all be stuck with MS and everything that comes with it. Really guys, why can you not be gratefull that there are people out there doing what you don't feel like doing, not because they have to but because they want to? If you have a problem with FOSS do something about it. Learn programming or do something that can help. Don't break down the developers, that will only make them think twice next time when something needs to be fixed. Come on people, working together is what got Free Open Source Software where it is today. Ask yourself, as part of a community, what did you contribute today?\n\nTo all the KDE devs - Awesome job guys/ladies!! The progress on KDE4 so far is amazing and thank you so much for all the small and big sacrifices you always make for us. Keep it up :)"
    author: "ScorpKing"
  - subject: "Re: Will this ever stop?"
    date: 2008-11-27
    body: "Right man!\n\nThis is why the dot urgently needs the Digg-like rating for comments. :D"
    author: "xbullethammer"
  - subject: "Re: Will this ever stop?"
    date: 2008-11-28
    body: "It's the me generation, where people only think about themselves, are rather rude and arrogant, and take take take, never stopping to think of other sand say thank you.\n\nIt's why it's important that when these people pop up, we make sure that those working on kde know that we are grateful for their work."
    author: "R. J."
  - subject: "Re: Will this ever stop?"
    date: 2008-12-05
    body: "It's actually the gay generation. (Garsh, I'm lol... har har har...)"
    author: "frozen"
  - subject: "Re: Will this ever stop?"
    date: 2008-11-28
    body: "Amen Bro. @ss is really spealt @rse tho."
    author: "ne..."
  - subject: "Re: Will this ever stop?"
    date: 2008-11-28
    body: "Bummer. s/spealt/spelt/"
    author: "ne..."
  - subject: "Re: Will this ever stop?"
    date: 2008-11-29
    body: "Amen brother! Hey, Aaron and other KDE Dev's, KDE 4.1 totally rocks and I'm realy looking forward to 4.2. Fyi, I was a 3.5 user, went to gnome/ubuntu for a more clean and consistent look and now I've installed KDE 4.1 on all my systems (5 in total) and I'm back in KDE land. Still a little work to go on the consistent part, but KDE has come a long long way! I think the re-architecting has been well worth it. KDE 4.1 is so gorgeous, so slick, it's incredible. Thanks for all your hard work! Don't let the whiners get you down!\n\nCheers from Ottawa!"
    author: "Tormak"
  - subject: "Re: Will this ever stop?"
    date: 2008-11-29
    body: "Well, bugs steal the time of users who expect things to just work. If the base is not solid how can you build upon?"
    author: "Plumplum"
  - subject: "Re: Will this ever stop?"
    date: 2008-11-30
    body: "It's a beta, you have to expect bugs, file a bug report, instead of flooding dot with constant posts bitching.\n\nThank you gets you a lot more than moaning."
    author: "R. J."
  - subject: "Google gadgets?"
    date: 2008-11-27
    body: "So how do I install Google Gadgets as plasmoids (openSUSE 11.1 running 4.2 beta 1)?"
    author: "TheLee"
  - subject: "Re: Google gadgets?"
    date: 2008-11-28
    body: "See http://code.google.com/p/google-gadgets-for-linux/wiki/BinaryPackages for further details."
    author: "ne..."
  - subject: "Re: Google gadgets?"
    date: 2008-11-29
    body: "I suspect that opensuse beta packages aren't compiled with google gadgets support.\nI installed the google gadgets qt libs and it still does not show up in the add widget dialog"
    author: "txf"
  - subject: "opensuse 11.1 RC and knetworkmanager KDE4"
    date: 2008-11-27
    body: "Does anybody test it? It's a joke, isn't it?"
    author: "Lars"
  - subject: "Re: opensuse 11.1 RC and knetworkmanager KDE4"
    date: 2008-11-28
    body: "Saw it too. But after having installed the necessary after a fresh installation, it had disappeared from the repo!!"
    author: "sid"
  - subject: "Re: opensuse 11.1 RC and knetworkmanager KDE4"
    date: 2008-11-28
    body: "I am using the SVN version from playground and it works like a charm. I know that openSuSE 11.1 is offering binary packages for it, but it runs a couple of days behind SVN. \n\nUnfortunately I haven't been able to test it yet with WEP and WPA keys, but ethernet and wireless are working. \n\nIt takes some time to get used to as you have to configure the new wireless network first in systemsettings (automatically redirected from the plasmoid), but then you can easily connect to it. \n\nStable enough for daily usage :-)\n\nRegards\n\nRaymond"
    author: "Raymond"
  - subject: "Why with so negative attitude?"
    date: 2008-11-27
    body: "\n\nIt seems that many KDE3 users are still so negative against KDE4. \n\nMayby the reason is that they have not understanded that KDE4 is not KDE3 + Development = KDE4.\n\nKDE4 is a new, fresh and new... (did I say a new?) Desktop technology...\nIt is not adding features for KDE3, it is building a totally new KDE. Thats why it toke so long time and KDE 4.0 was for developers only.\n\nKDE 4.2 and 4.3 are there where normal users can actually adapt it and then wait that all the wanted features gets added to it. KDE team wanted to make fresh codebase for what base a future development without draggin a heavy and unmaintainted codebase all the time. \n\nSo, think that KDE4 is like a KDE1 and then you understand the amount of great work what they have got done. They just did not add a new fancy theme for \"kicker\" and few new themes etc... they build \"everything\" from ground to top... and more is coming.... just wait..."
    author: "Fri13"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-11-29
    body: "Nobody told them. And sure this was not the deal.\n\nKDE 4 development took soooo long and progress on the KDE 3.5 platform was abandonned, also with regard to third party applications and when KDE4 was out it didn't have the essential features and was full of bugs. Now we are at 4.1 and still KDE4 is not ready for most users or a \"technology preview\". You run into a lot of bugs within 3 minutes including trivial glitches as missing Icons.\n\nDo you expect users to wait another two years? They won't."
    author: "Plumplum"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-11-29
    body: "the 3.0 users had to wait for it to become stable, how is the situation different this time around?"
    author: "txf"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-12-05
    body: "So it's just as bad as it was last time. Yup, I remember. However, saying that is not a solution. Whinners and greedy takers will abound. How do you protect yourself from them though?\n\nI'll be the first one to walk away from a FOSS project that is incomplete and if it survives and improves, I'll have a look later. But KDE is bigger than just a piece of software. It's am ecosystem. If you kill an ecosystem, it will probably give out yelps of pain and shed tears. KDE4 might be a phoenix. It's just that the death and pain wasn't necessary. Programmers, myself included, are not well know for being good at communication. If KDE4 had stayed in BETA till now, 4.2 or maybe 4.3, the ecosystem would probably be a lot more healthy. You'd think that we'd have learned from 1 to 2 or 2 to 3. But no, we'll do it again and suffer the whinners and death of the n00bs in the ecosystem. Just when KDE4 reaches maturity, we'll kill it off with, \"KDE5, the latest greatest DE for all your computing needs.\" TM."
    author: "frozen"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-12-05
    body: "No, I'm afraid that hadn't we released, the ecosystem would have been much, much worse of. For one thing, without the KDE 4.0 release, application developers wouldn't have had a baseline to develop against. That would have lost us a year of application development. And without a release, volunteer developers disappear. We're facing that situation with KOffice now.\n\nNo -- despite all the \"opinions\" from outsiders, non-developers, non-contributors and anynomous cowards or cowards hiding behind a handle, releasing 4.0 and 4.1 was a good idea. 4.0 was a good and stable platform to develop against -- as advertised, 4.1 is a good and stable platform to use, and 4.2 is shaping up to be really good."
    author: "Boudewijn Rempt"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-12-08
    body: "You guys are very very defensive about criticism. \nJust face it - from an end-user  point view the 4.x series is lousy. 4.1 is certainly better than 4.0 but lots of things are still lacking - must irrating is the lack of consistancy in user interface. There do not to seem to be any guidelines to follow.\nI dont even want to to into the new weird and user unfriendly way to list mails in kmail or why a enduser have to configure a \"akonadi server\" - what the heck is akonadi? (i'm enduser).\nIt just seems that KDE developers want to drive endusers to other enviroments - stop it.\nMore time is spent in making idiotic glossy/glassy themes - unconfigurable by the way - than improving on real usable features.\nThis is okay if thats your interest but i doubt that stuff should be a part of KDE.\n\nPower to end-users!"
    author: "Anders And"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-12-09
    body: "Do you really expect to be taken seriously if you adopt \"Donald Duck\" as a handle?\n\nEspecially if you make all kinds of statements you cannot back up -- have you, for instance, done any real, quantitative research into the proportion of time spent making things beautiful and time spent laying down the foundations for fundamental innovations? And compared that to the demand by end-users for pretty things compared to fundamental things, quantified by counting dot comments, for instance?\n\nAnd what kind of a weird slogan is \"power to end-users\"? End users already have all the power they can possibly have without becoming contributors. Because. you know, someone who creates will always have more infuence than someone who merely quacks."
    author: "Boudewijn Rempt"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-12-10
    body: "I didn't say it was a bad idea to release 4.0 and so on. However, if you read any prerelease literature (read: propaganda), from a user point of view, KDE 4 is a complete let down. That's why I said 4.0 and 4.1 and maybe even 4.2 should be labeled BETAs. 4.0 was not useable. 4.1 was flakey. 4.2 is shaping up. The 4.2 BETA is better than the 4.1 release. I thought 4.1 was supposed to be stable? But 4.2 BETA performs better and is more stable than 4.1. You get what I'm saying? It's just a communication thing to the (noob) users.\n\nI've seen other projects release like this XYZ 3 BETA = xyz 2.989283. If 4.0 was considered a developers release, then don't advertise it to the users. And plenty of KDE devs have admitted there was a problem the way this was handled. I hope the people learn from this. FOSS is young. We can learn."
    author: "frozen"
  - subject: "Re: Why with so negative attitude?"
    date: 2008-12-10
    body: "Oh and as for handles, look, I may be a European, but I'm certainly from a different culture. Before real programmers came along, there were aliases. I don't consider information privacy laws that nice that I want to go posting my real name all over the internet. How naive are you?"
    author: "frozen"
  - subject: "[Semi OT] Good Eee PC distro with 4.2 beta?"
    date: 2008-11-28
    body: "I'm asking this because I want to migrate my Eee (currently running Kubuntu Hardy) to 4.2. Obviously I can't really compile the thing on that, so I'm open to suggestions. "
    author: "Luca Beltrame"
  - subject: "Re: [Semi OT] Good Eee PC distro with 4.2 beta?"
    date: 2008-11-28
    body: "Kubuntu Intrepid and wait for packages to be released? (I think in a week or so we should have them)"
    author: "Vide"
  - subject: "Re: [Semi OT] Good Eee PC distro with 4.2 beta?"
    date: 2008-11-28
    body: "Kubuntu disabled activities (although I assume their packages will be fairly vanilla), which I need (yes, I know about using shortcuts, but I can't add new ones), so I think it's not the best choice for me."
    author: "Luca Beltrame"
  - subject: "Re: [Semi OT] Good Eee PC distro with 4.2 beta?"
    date: 2008-11-28
    body: "For Eee's and similar I would usually reccomned Mandriva, but I'm not sure if they priovid binaries for 4.2 beta yet. The announcmnet only list OpenSuse binaries, so you'll have to check on Mandrivas mirrors."
    author: "Morty"
  - subject: "Re: [Semi OT] Good Eee PC distro with 4.2 beta?"
    date: 2008-12-03
    body: "Opensuse 11.1rc1 supports eeepc 901/1000 and has packages for 4.2 (and should support also the other  models) out of the box. Install it or wait 2 weeks (18th december) for the stable version. It also has a very better kde support than kubuntu."
    author: "Giovanni Masucci"
  - subject: "Re: [Semi OT] Good Eee PC distro with 4.2 beta?"
    date: 2008-12-04
    body: "I have in fact 11.1 RC1 installed now. Works great, although with a few quirks I am investigating (to see if it's my setup or not)."
    author: "Luca Beltrame"
  - subject: "Apps vs. Desktop"
    date: 2008-11-28
    body: "I've got another one:\nafter a plasma crash, and they happen often with 4.2 (in current version, I know it is a beta!), the plasma configuration icon that normally is on the right side of panel goes to left, same for the one in desktop!\nThe problem is that it occupies more than half of the k-menu, then you must click on the little space left to use it.\nLocking plasmoids hide this icon, so you can use k-menu, but still a ugly bug.\n\nTo be honest, more and more I'm looking and testing another window managers and DEs. KDE4 apps in general are really nice (I love dolphin and krdc!), but the desktop (hello plasma!) is fat, full of bugs yet, and not worth the trouble if you just do not use plasmoids and like nice desktop effects.\n\nAnd nowadays, using KDE3 is not a option anymore, since distros are dropping it :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "Please quote bug numbers if you mean \"full of bugs\". Most plasma crashes I had were on logout, and they don't happen anymore for me. "
    author: "Luca Beltrame"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "Well, I see his point. I have plasma crashes constantly, too. Mostly when logging out, but often at other points. I cannot reproduce them, but they are there, often enough to get on my nerves.\n\nBut what am I supposed to do? Report a bug that says \"Plasma crashes and I don't know why?\". I'll get flamed for that report, too. And besides, it's happening so often that I can't help thinking somebody else - e.g. the devs - should have noticed it already.\n\nBesides - it might also be a buggy plasmoid taking the hole desktop with it. I don't know. Plamoids are all over the place, in different configurations, in fact, plasma without plasmoids makes no sense. (In future, this vision will indeed make KDE4 a great desktop, but now...) \nThis constructs simply leads to many not reproducable crashes. Something you can (and are supposed to) load down from the Internet and install without any hassle can make your whole desktop unusable. Seems not like a good idea to me.\n\nI've watched many of those \"internet and open source controversys\". The complaints about KDE4 are one of the really longest lasting controversies. Not that I want to lecture, but maybe now would be the time to reconsider whether something has went wrong in\n- communicating with the users\n- setting priorities with coding\n- integrating user's needs and wishes.\nor whether all those complaints are really only from trolls or whiners.\n\n\nI basically really like the vision of KDE4. But: I really do not like the actual state the project is in now. Not codewise - I mentally labelled KDE4, even 4.2, as a beta. My problem is rather with those three points mentioned above, which should IMHO be adressed.\n\n"
    author: "Larx"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "\"But what am I supposed to do? Report a bug that says \"Plasma crashes and I don't know why?\". I'll get flamed for that report, too\"\n\nI don't think I've ever seen someone get \"flamed\" for submitting a bug report with a valid backtrace."
    author: "Anon"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "Then please tell me - and the others here - how we could get a valid backtrace from plasma. I have it crashing every now and then but can't get a backtrace no matter what I try."
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-29
    body: "http://techbase.kde.org/Development/Tutorials/Debugging/How_to_create_useful_crash_reports"
    author: "Hans"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "Basically, we have sat down and discussed this, and there have been changes made in the way the project communicates. But that doesn't help at all. Right now, what you see happening is a classic pavlovian reaction: something gets posted on the dot, people restate their complaints, whether they are still valid or not. My own guess is that there is a small group of people who have such a low self-esteem that the only way they can boost it is by denigrating the work of others. Or they just have a really bad attitude. \n\nFree software developers do listen to users: I love hearing from Krita users and will often drop whatever it is I am working on to fix a bug or implement a feature. But sometimes the wishes are so big (while still valid) that I cannot get something implemented in under a year of after-hours hacking; and then there will be complaints about slow development. And sometimes I decide to implement what I want to have myself first. Selfish, that's true, but the only way to keep motivated is to also do things you enjoy.\n\nAnd, sure, communicating is important. But it's a two-way channel, and people need to listen. If the reason for something is explained already, it's no use demanding that something be done your way anyway. Having an opinion doesn't entitle anyone to have other people follow that opinion.\n\nAs a project, we cannot, want not and should not control the way our members communicate. We cannot forbid enthusiastic blogs about what developers are up to.  So when people start working on something they are enthusiastic, elated: look what I'm going to do, they blog about it. And then when the first, incomplete installments of the results of that work are made available people attack them for not adhering to \"promises\" and demand apologies. And then those people go on complaining about those broken promises, and go on, and when the thing they are complaining about is given them, in a desperate attempt to make the complaining stop, they complain that the time spent giving them their wish wasn't spent making new stuff. It's not just people in the project who need to improve the way they communicate, the users need to improve their communicative habits, too. Jerome might think he's a professional user, but his way of expressing himself is far from professional: it is extremely childish.\n\nAnd then developers burn out and start posting snide comments. Or get so tired they even add rude responses in bugzilla. So when a developer is rude at you, you need to consider the possibility that he's burned out over to many hassles. Burn-out is so common in the free software world that according to one research paper I read last year more than half of the free software developers have suffered from it. Don't contribute to it!\n\nBah, I've been way too wordy. Anyway, about your plasma crashes -- yes, sure, post a bug, if you can get a backtrace. t's a really well-known fact that software developers cannot find the crashes in their own applications, mostly because they follow one path through their code when testing, use their own software in one particular way -- and that means that you might be the first to see the crash and tell the developer. There's no developer who thinks crash bugs are a waste of their time. You don't have to know what causes it: the backtrace helps with that. And in the case of plasma, a list of all plasmoids will help, too, I'm sure. I"
    author: "Boudewijn Rempt"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "\"Bah, I've been way too wordy\"\n\nNot at all; that was a great piece :)"
    author: "Anon"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "That was really a good statement. I feared that my post would be labelled as trolling, so by doing a long answer you partly contradicted my statement ;).\n\nHowever, I think it's too easy to always blame others - despite the facts that some critics make it easy to dismiss them as trolls. The complaints really go on for quite some time.\n\nI see it in my case. I consider myself a very dedicated KDE user. However, I dislike some developments with KDE4. And I'm not able to recommend KDE4 even to computer savy friends, as too much has still to be fiddled around.\n\nTake the example of the \"new desktop paradigm\" (no more ~/Desktop folder). Maybe I'm getting old and crusty, but I liked (and still like) that simple principle. Many others thought the same at that time.\nThe devs' communication in this case went like this:\n1. dismiss all the critics as incapable of grasping a new destop concept\n2. and only much later and grudgingly implement a new (and admittedly now better) version of this ~/Desktop thingy.\nwhich means basically, piss the users off first and only later show them that their complaints would be taken into account. \n\nMany developments in KDE4 seem to work this way round. I'm a big KDE fan, but I don't like it if my criticism is dismissed, even if there are lots of trolls around getting on the devs nerves. I as somebody who wants to make constructive critic cannot make the trolls around shut up."
    author: "Larx"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "\"Take the example of the \"new desktop paradigm\" (no more ~/Desktop folder). Maybe I'm getting old and crusty, but I liked (and still like) that simple principle. Many others thought the same at that time.\n The devs' communication in this case went like this:\n 1. dismiss all the critics as incapable of grasping a new destop concept\n 2. and only much later and grudgingly implement a new (and admittedly now better) version of this ~/Desktop thingy.\n which means basically, piss the users off first and only later show them that their complaints would be taken into account.\"\n\nThat's not what happened. What did happen was a new model for achieving the old Desktop paradigm while being more flexible was in the works. However, the explanation is pretty technical (a folder view plasmoid being used as your primary desktop containment which will house your icons and other plasmoids). The reason it's difficult to understand is not because we're stupid, but because it uses a bunch of concepts we're not used to (it's a whole new paradigm built on a whole new paradigm!). It wasn't \"grudgingly\" as you say, but rather it just took time and manpower. Combine that with trying to implement a 1:1 panel:kicker (kicker was a boatload of code), and it's easy to see where the last year+ has gone for the plasma team. (Of course, sensationalist titles in blog posts by Aaron didn't help...)\n\nThe real problem is very few of the critics sound like you. You're polite, reasonable, and open to discussion. Others... not so much.\n\nBut I just want to paraphrase what Boudewijn Rempt said for emphasis: most devs don't use your computer like you do, so just file a bug, submit a backtrace, and answer their questions. "
    author: "Anon"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "\"2. and only much later and grudgingly implement a new (and admittedly now better) version of this ~/Desktop thingy.\n which means basically, piss the users off first and only later show them that their complaints would be taken into account.\"\n\nI have to disagree on this one. I think it was an idea right from the start (like extenders, that have been planned since the start of Plasma). There wasn't just enough time with the release schedule and all. \nNote to the Plasma guys: if I was incorrect, feel free to point that out."
    author: "Luca Beltrame"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-28
    body: "openSUSE probably won't drop 3.5 until 4 is stable enough."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Apps vs. Desktop"
    date: 2008-11-29
    body: "Slackware still keeps 4 in /testing/, 3.5 being the ONLY version in main tree."
    author: "slacker"
  - subject: "Anybody here with some Trolltech connections?"
    date: 2008-11-28
    body: "I really like KDE4 and it runs very nice on my Ubuntu machine (KDE compiled from scratch), congrats to te devs. But, as a tablet/pen user, I simply cant use it until the QT problem related to submenus is present. I really wonder why this usability itch doesn't get more attention :(  \n\nhttp://trolltech.com/developer/task-tracker/index_html?method=entry&id=164725\nand see\nhttps://bugs.kde.org/show_bug.cgi?id=167488\n\nAnybody here with some QT connections? \n\nRegards,\nJens"
    author: "jens"
  - subject: "Re: Anybody here with some Trolltech connections?"
    date: 2008-11-29
    body: "I was talking to someone on #qt, and it seems Qt only cares about pen/tablet on Qt/Embedded (or whatever its new name is). I still use KDE 3 because of this problem on a slate tablet PC. What's worse: I cannot click on HeaderView headers to change sorting, it immediately starts dragging, because no drag distance is respected there.\n\n"
    author: "christoph"
  - subject: "Re: Anybody here with some Trolltech connections?"
    date: 2008-11-29
    body: "I can't understand that, this must be annoying even with a regular mouse. Btw, I don't have this effect when using eg. skype on Windows, so it seems only related to QT/X11 (no idea about OSX). \n\nMay be you could also vote on the KDE bugtracking system for some more attention.\n\nRegards,\njens"
    author: "jens"
  - subject: "Re: Anybody here with some Trolltech connections?"
    date: 2008-11-29
    body: "\"I can't understand that, this must be annoying even with a regular mouse. Btw, I don't have this effect when using eg. skype on Windows\"\n\nSkype for windows is written in Delphi. yes... Say hello the 90's.\nSkype for OS-X is written as a native OS-X app.\n\nLooks like the Trolltech salesguys were not up to their task..."
    author: "a.c."
  - subject: "dual monitor support STILL BROKEN"
    date: 2008-11-29
    body: "I have two graphics cards. Each has one monitor attached.\n\nThis setup works great with every window manager except KDE 4.x's kwin. It works with fluxbox, xfce4, kde 3.x, gnome, ion, xfce4 and so on.\n\nKDE 4.x's kwin will only use one monitor. This has been utterly broken since KDE 4.0. And guess what? It's still broken in KDE 4.2 beta 1 (4.1.80).\n\nIS THIS SO DAMN HARD TO FIX, and how come that KDE 4.x is the ONLY window manager which has managed to mess this up???\n\nThis is a major issue; being able to use both monitors is so important to me that this alone makes using KDE 4.x out of the question.\n\nYes, there are several bugs about this at bugs.kde.org, and those have been there for a very long time. They probably still are - but I would not be amazed if they are closed with a claim that this is somehow fixed.. (I have the impression that bugs are more often closed without them being fixed than not)\t"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-11-29
    body: "Answering your question \"is this so damn hard to fix\", the answer is yes.\n\nBy the way, I have two monitors, but I'm using my dual-head NVidia card through TwinView. It works without a glitch.\n\nIf you're not using Xinerama, then the problem is known. No one in the KWin developer team uses that setup, so you'll expect the problem to remain there until someone decides to send a patch."
    author: "Thiago Macieira"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-03
    body: "Actually, it is not that hard to fix. Qt has built-in support for multiple X screens (Xinerama and TwinView create a single screen out of two or more monitors).\nI had the same problem, and started working on a fix a few months back. Sadly, the developers were not interested, and by the time I had an initial patch the code base changed so that the patch no longer applied.\nAfter 8 years of using KDE (and developing for KDE), I finally gave up and moved to Xfce4. It's the attitude more than anything else that made me do so."
    author: "Elad Lahav"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-11-29
    body: "I agree that this is a show stopper.\n\nIt sure would be nice for someone to donate a video card to a developer so that they could fix it.  Although, I assume that it is VERY hard to fix; otherwise, it would have been by now."
    author: "Nate"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-02
    body: "Sorry but dual monitor setup are so rares that this hardly is a show stopper. Maybe it could be a serious bug, but not show stopper (take a deeper look at the show stopper definition)"
    author: "Vide"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-03
    body: "Rare?\n\nThis poll indicates over 40% of users use multi-head:\nhttp://www.phoronix.com/scan.php?page=article&item=924&num=3\n"
    author: "christoph"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-03
    body: "I find that number pretty dubious - amongst the users I meet locally none use it. That said, I did have a user reporting a bug recently that turned out to be caused by him having the TV out on his graphics card turned on by mistake causing him to have a multihead setup without knowing it."
    author: "Richard Moore"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-04
    body: "Maybe they all use KDE 4 :)"
    author: "christoph"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-05
    body: "Rare? Come on. Over half of my friends use two monitors. Oh, yes, maybe that's because we have enough money to buy two 26 in. LCDs and we are mostly in IT. As another poster said, it's the attitude that hurts, not the bug. It is a show stopper. Funny thing is, somehow I had it working on KDE3. Dual monitor support is, IMHO, more important than sound, desktop effects, and a bunch of other stuff. It's as important as proper font rendering. OK, if that's not a show stopper, then what is? Shall we use the console for artwork?\n\nAnyhow, plasma is working a *LOT* better now. It's actually usable and handles the crashes that have occurred gracefully. Props on that."
    author: "frozen"
  - subject: "Re: dual monitor support STILL BROKEN"
    date: 2008-12-03
    body: "As I am running this on an older, single dual head Nvidia card, I can't say for sure what multi-card problems are like.  \n\nThe only issue I seem to be having is that Plasma Desktop is quite glitchy.  Zoom in/out does not work right, and it affects both monitors, instead of just one.  Changing desktop settings in monitor \"1\" seemed to work OK, but once I moved to monitor \"2\" and attempted a bit of personalization, both desktops began acting as clones of each other, and messing up.  The panel and running applications are not affected, fortunately. Both environments still run and show up if I zoom out, but I cannot place them back properly in their respective monitors.  In many ways, it is reminiscent of odd desktop behavior that was happening back in the KDE 4.0 days, and that was mostly gone in 4.1.  (Mostly because the removal of \"Zoom\" and \"Events\" from the plasma desktop available features.  That kept me from getting into too much trouble, even if they where working mostly OK before they went away.)  Currently, my desktop experience in 4.1 is mostly stable, even as the desktop cashew has become useless there.  \n\nThis beta gives it some use again, even if it is mostly of an unintendedly comedic nature at the moment.  But the \"out-of-the-box\" settings work, even if they are rather ho-hum generic.  It may take a little bit of google (and work) to return my 4.2 plasma desktop to \"default\" but it looks like that may be the way to go to have a wallpaper that covers each screen again in 4.2.\n\nStill, it's a beta, so stuff will break. not work right or go away eventually, so here's a cheer to the many things that do work better in 4.2 than 4.1!  (But I think I'll stick to 4.1 for a little longer, as this desktop thing is a little too buggy for me to like too much.)"
    author: "Pablo A."
  - subject: "to all KDE devs: thank you!"
    date: 2008-11-29
    body: "I'll just stick to a big thank you for now. There has been enough talking about what's missing and what's still not working in all what is written above. \n\nI am using my KDE4.1.3 desktop daily on my laptop and I am very happy with it. It is free (which is really really cheap)! It is pretty! It is functional (for me)! It gives me the opportunity to use a legal desktop environment at no cost! It makes me smile when a happy Vista user proudly shows me a sexy window manager feature, which I can reproduce easily and even beat it with tons of other cool features in my KDE4.1.3! \n\nSo to all the KDE devs out there, thanks from a happy dutchie! I hope you will keep up this fast tempo in creating an awesome desktop experience!\n\nDeech\n"
    author: "deech"
  - subject: "Looking better."
    date: 2008-11-29
    body: "I've been very skeptical of KDE 4, but I have to say, it looks like you've made some great progress.  Keep up the good work!"
    author: "Nate"
  - subject: "Dragon Player and xine phonon plugin"
    date: 2008-11-30
    body: "Hi!\nI've been running trunk for several months and I must say I do not experience any special crash or weird behaviour as far as I am concerned, everything works fine for me and I enjoy the KDE4 experience very much.\nI just noticed yesterday an image quality problem with Dragon Player using the Phonon xine plugin. It is lower than the one provided by xine-ui one the very same platform. Basically, it doesn't seem to do any deinterlacing, the image seems interlaced and a bit edhy compared to xine-ui output.\nIs this a known issue or does phonon uses a particular xine.cfg file?"
    author: "Richard Van Den Boom"
  - subject: "Why does it require libs from GNOME ?"
    date: 2008-11-30
    body: "I wonder why I need to compile libs from GNOME to continue further compiling of KDE from SVN ? I speak about glib here which shared-mime-info seem to require. At this point I stopped further investigation into KDE SVN. Regardless how great KDE 4 becomes it's a no-go for me until all parts from GNOME has been removed cleanly from it."
    author: "ac"
  - subject: "Re: Why does it require libs from GNOME ?"
    date: 2008-11-30
    body: "Well, first glib isn't a part of GNOME.\nIt is an utility library often used by C programmers to make their life easier. Some parts of it are similar to things C++ programmers have in the C++ Standard Library and/or QtCore.\n\nSecond, the glib dependency is for the update helper program update-mime-database and not required for accessing the MIME info itself.\nIf you feel you need to replace the update tool with something written in a different language nobody is going to stop you, though IMHO your time might be better spent on the things you intended to develop instead of replacing parts of your toolchain.\n\n"
    author: "Kevin Krammer"
  - subject: "KOffice 2 question"
    date: 2008-12-01
    body: "Can't text run around shapes on _both_ sides?"
    author: "Axl"
  - subject: "Apps vs. DE"
    date: 2008-12-08
    body: "Well, I think the debate opened by the original post is a good one. App vs. Desktop. Maybe is it a matter of Classical vs. Modern. The moderns would like to lead us to a virtual env. Web based OS and App... While the classics would like to stick with traditional desktop and App. powered by their own workstation.\nWhile I am always thinking being a modernist, I think that for my computer I would like to stick with Classical view. Why that:\nAfter several attempts to use Applets with many OS (Vista, MacOS, KDE4, gOS,...XP+Applets), I just can only conclude that desktop Applets are a waste of ressource and only distracting. At least to me, they have no value.\nSo I would prefer having a simple QT DE (lightweight, fast) with all powerfull KDE4 Applications.\n\nPlease Make a KDE4-Lite !!!!!!!!!"
    author: "Jerome"
  - subject: "Apps vs. DE"
    date: 2008-12-08
    body: "Well, I think the debate opened by the original post is a good one. App vs. Desktop. Maybe is it a matter of Classical vs. Modern. The moderns would like to lead us to a virtual env. Web based OS and App... While the classics would like to stick with traditional desktop and App. powered by their own workstation.\nWhile I am always thinking being a modernist, I think that for my computer I would like to stick with Classical view. Why that:\nAfter several attempts to use Applets with many OS (Vista, MacOS, KDE4, gOS,...XP+Applets), I just can only conclude that desktop Applets are a waste of ressource and only distracting. At least to me, they have no value.\nSo I would prefer having a simple QT DE (lightweight, fast) with all powerfull KDE4 Applications.\n\nPlease Make a KDE4-Lite !!!!!!!!!"
    author: "Jerome"
  - subject: "Re: Apps vs. DE"
    date: 2008-12-08
    body: "to continue my post:\n\nI know that in the past there were several attempts to make lightweight QT based windows manager.\nBut what I mean is closer to a KDE4 without many heavy weight features, lower memory footprint, faster screen actions... On my XP1800+, it feels very slow since 4.0RC. 4.0beta2 was the fatest for me!\nWould it be possible that the developpers could put Flags in their code for a KDE4-Lite profile?\n\n "
    author: "Jerome"
  - subject: "Re: Apps vs. DE"
    date: 2008-12-10
    body: "There is a QuickTime based window manager?"
    author: "Anon"
  - subject: "konqueror and kicker from kde3 set high standards"
    date: 2008-12-09
    body: ".. and i hope KDE4 is finally catching up. I jumped on the SVN bandwagon after the first 4.0 beta and since now, (sorry to say) it has been looped disappointments all the way. The biggest pain was that while KDE3 was fulfilling every true geeks dream of highest efficiency (both concerning f.e. file management but also screen estate), up to now KDE4 has been sooooo VISTA (that being meant as an insult) thx to some folks on the usability front. Giant margins and unused space everywhere. Stupid folders that 'remember their view mode' and save it in a hidden file in each folder you visit (veeeery non-destructive - careful with those rw mounted broken filesystems!). Buttons and settings hidden by default so that 95% of potential users will *never* witness the functionality they might use otherwise to be more productive. Plasma desktop mostly broken and with all the bling bling not able to reach the 'basic' practicability of a well configured kicker..\nWell it can only get better. After almost a month i am just compiling QT4.5 and after that KDE4 trunk, let's see if i can start it again or will be stuck with fluxbox forever. Hopefully some progress has been made on the geek pride issues. Oh i miss the time when i had a working Desktop that pleasantly surprised me on a regular basis (when folder trees would still expand automatically upon dragging something onto them..)..\nKDE4.4 should really focus on making the geek happy again. Efficiency, configurability, more features exposed on the interface. The distributions can slim down the interface to make it easy and they will, but they'll not add options to the default look! So the default should be a interface exposing every functionality in the best-organized possible way. And kill that waste of screen space (i'm repeating myself huh)!"
    author: "eMPee584"
  - subject: "Spacer, please!"
    date: 2008-12-10
    body: "Dear KDE developers.\n\nCan you include in a default install of KDE 4.whatever a spacer plasmoid? Yes, a spacer app who sits in a panel, does NOTHING but occupying (a configurable amount of) space. AFAIK OpenSUSE already has it.\n\nI would thank you for that."
    author: "Alejandro Nova"
  - subject: "Sluggish, and buggy, does feel right..."
    date: 2008-12-13
    body: "I try every release of KDE that maybe they will fix the sluggish feel it has. Just resizing windows, shows blank grey screens on the windows, lag when opening menus, lag when moving windows, and poor performance overall, with or without compositing.   \n\nMy Specs are fairly average computer parts: \nAthlon X2 3.0GHZ Duel Core \nAsus M3A78-EM Mobo\n2GB Ram\nAti 3200HD\n\nIf it doesn't run on these specs, then I don't understand whats the problem. "
    author: "Jeremy"
  - subject: "If you don't like the ugly"
    date: 2009-02-27
    body: "If you don't like the ugly blue border:\r\nSystemsettings > Appearance > Colors > Colors tab > Active Titlebar"
    author: "nferenc"
---
Today, the KDE team <a href="http://www.kde.org/announcements/announce-4.2-beta1.php">invites interested testers and reviewers to give KDE 4.2.0-Beta1 a go</a>. The release announcement lists some significant improvements. The purpose of this release is to get feedback from the community, preferably in the form of bugreports on the new <a href="http://bugs.kde.org">bugs.kde.org</a> bugtracker.
Beta1 offers critical features like the Eyes applet (an XEyes clone), but also a more streamlined user experience all over the workspace and applications.
With the KDE team being in bug fixing frenzy after the recent hard feature freeze, now is the time to help us smoothing the release for your pleasure starting in January.

So install KDE 4.2-Beta1 and help us make it rock.



<!--break-->
