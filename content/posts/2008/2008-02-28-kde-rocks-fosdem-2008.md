---
title: "KDE Rocks FOSDEM 2008"
date:    2008-02-28
authors:
  - "jvan\nmourik"
slug:    kde-rocks-fosdem-2008
comments:
  - subject: "Suggestion"
    date: 2008-02-28
    body: "update kgpg and integrate it with kmail please, so we don't have to use thunderbird and a plugin for gpg. thank you."
    author: "Anonymous"
  - subject: "Re: Suggestion"
    date: 2008-02-28
    body: "But kmail can use GPG without any problems. Am I missing something?"
    author: "jj"
  - subject: "Re: Suggestion"
    date: 2008-02-28
    body: "There is one problem: you cannot use the group encryption mechanism. In gpg.conf, you can define groups of keys. Now, when you have a mailing list with the same name, you ought to simply encrypt emails which you send to the mailing list address. Kmail now refuses to encrypt the email as there is no *single* key with the mailing list email address. Kmail does not honor the gpg groups."
    author: "anon"
  - subject: "Re: Suggestion"
    date: 2008-02-29
    body: "KMail does not honor gpg groups, because the Author of GnuPG believes this should be solved on the MUA side. It is a too less known funcationality that you can select several keys in KMail. I am using Kontact and create a contact with my mailinglist as email address and then add all the encryption keys in there. This basically is the \"group\" feature, but it is implemented in a higher layer just has Werner Koch recommends."
    author: "Bernhard Reiter"
  - subject: "Re: Suggestion"
    date: 2008-02-28
    body: "I have helped someone testing the (then new) GPG support in Thunderbird using KMail - it has had excellent KGPG support for years... So unless you are talking about KDE 4 (but KDE PIM hasn't released for 4.0 yet) I wouldn't know what you want."
    author: "jospoortvliet"
  - subject: "Wow"
    date: 2008-02-28
    body: "What a post! Aaron, we need a huge event in canada!"
    author: "mike"
  - subject: "KOffice"
    date: 2008-02-28
    body: "\"[...] Due to the developers having less time to work on KOffice, the current state of several applications was disappointing.\"\n\nHow many are developers are exactly working on KOffice?"
    author: "Stefan Majewsky"
  - subject: "Re: KOffice"
    date: 2008-02-28
    body: "Right now, about a dozen."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice"
    date: 2008-02-28
    body: "(At most one of which are paid - contrast with OO.o's sprawling and horrific codebase, which has about *50 *full-time* programmers working on it).\n\nA really sad mis-allocation of funds if you ask me: if I was rich, I'd definitely be putting money into KOffice rather than OO.o.  It's amazing how far the KOffice team have come on a non-existent budget, and a real testament to how good the devs and underlying technologies are.\n\nKeep up the excellent work, KOffice devs! :)"
    author: "Anon"
  - subject: "Re: KOffice"
    date: 2008-02-28
    body: "50? There are about 10 full-time folks in o.org from SUN and who knows how many in Novell (not counting marketing and lawyers ;) ). You can find not that many commiters with 2000+ commits (http://www.ohloh.net/projects/29/contributors). Most of the code still comes from closed source development at StarDivision.\n\nThanks for the offer and we all are waiting for you when you got rich :)\n\nest. KOffice project cost: $16,478,614 ;)\n(http://www.ohloh.net/projects/3802)"
    author: "j"
  - subject: "Re: KOffice"
    date: 2008-02-28
    body: "According to this  http://www.oreillynet.com/onlamp/blog/2005/09/opening_the_potential_of_openo.html\n\nit's 1 RedHat, 80 Sun, and 8 Novell developer. So by now it will probably be more than that."
    author: "AC"
  - subject: "Re: KOffice"
    date: 2008-02-28
    body: "They most likely count the translation teams and we are not. 100k LOC per dev could mean a lot of code nearly unmaintained too."
    author: "j"
  - subject: "Re: KOffice"
    date: 2008-02-28
    body: "I doubt they count the translation teams either when talking about numbers of developers. About 120 developers for OpenOffice sounds about right to me -- it's what I'd expect for a codebase of that size."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice"
    date: 2008-02-29
    body: "\"[120] it's what I'd expect for a codebase of that size.\"\n\nBut much of the code has been provided by StarDivision. The question is about actual # of devs, not history. While oo.org 2.0 alpha/beta stage, the team has been complaining about shortage of manpower."
    author: "j"
  - subject: "Re: KOffice"
    date: 2008-02-29
    body: "my guess - that includes testing, user experience and whatnot teams as well."
    author: "richlv"
  - subject: "Sad news about KOffice"
    date: 2008-02-28
    body: "The news about KOffice are a bit sad, though not exactly new : KOffice has always lacked developpers, it seems.\nIt's probably too late for KOffice 2.0, but maybe there could a good focus on KOffice for the Summer of Code to attract young dev to this office suite?\nI really wish it to grow, as the latest Alpha version I've tested was very promising.\nBy the way, I've read the blog on planetkde about MS binary format and why they're so complicated. I promise I'll never ask again for better MS filters in KOffice. :-)\nWould be nice to have a command line tool using OOo filters to immediately convert to ODF without having to load and save in OOo though..."
    author: "Richard Van Den Boom"
  - subject: "Re: Sad news about KOffice"
    date: 2008-02-29
    body: "I wonder if one of the problem of KOffice isn't the rewrites, I don't follow its development closely but each time, there's talk about 'ground rewrites'.\n\nIf memory serves, KWord has been rewritten from scratch, which explain that it may not be ready soon..\n\nI agree with your point about using OOo as a filter: no need to waste development hour on this point, it may be a bit big in disk space but stripping everything not needed from OOo could reduce this point."
    author: "renoX"
  - subject: "Re: Sad news about KOffice"
    date: 2008-02-29
    body: "KWord had some problems with text rendering in 1.x which couldn't be solved without rewritting of some parts."
    author: "ac"
  - subject: "so"
    date: 2008-02-28
    body: "so kword might not make the first offical release.  Hmmmm.  That's not exactly a smart idea, especially when a word processor is the main reason people have for office software.\n\nPersonally I tried KOffice 2, what ever alpha or beta it is up to now, and it really was a waste of my time.  It's like comparing an apple (microsoft office) to a peach (open office novell edition) to a sour old dried up lemon (koffice)  I really do not think any windows users will be using that software, and the vast majority of linux users wont either, it's so lacking, and well, ugly.  The GUI looks dated a decade compared to how office suits look now."
    author: "anon"
  - subject: "Re: so"
    date: 2008-02-28
    body: "What do you expect from ALPHA quality software? It is lacking feautures and the GUI indeed needs some attention, but they are working on that. And if you've got good ideas... just let them know by bug reports.\n\nAlthough I agree that a word processor is the key element of an office suite it might be usefull to have the base libraries and base programs (without kword then) out, just to make other developers aware of the infrastructure, IIRC Koffice 2 is using new baselibs and infrastructure, just like KDE 4 is using a new base.\n\nI do like though that kpresenter has gotten a bit of love, the 1.6 version wasn't that great at all.\n\nJust my  0.02&#8364;"
    author: "Terracotta"
  - subject: "Re: so"
    date: 2008-02-28
    body: "With \"how office suits look now\", do you mean these cluttered icon conglomerates and menu trees? If you think this interface is bad, improve it, or at least make suggestions, or shut up. "
    author: "Stefan Majewsky"
  - subject: "Re: so"
    date: 2008-02-28
    body: "I really don't get how you got to that impression. Didn't you by any chance try OpenOffice and mistake it for KOffice? I mean, that one looks like MS Office 96, sure. But KOffice looks rather different, and has a few pretty cool things. I just gave the latest SVN a try. It's not ready for use, sure, but some stuff is incredibly cool - esp vector graphics stuff in there. I'm really looking forward to a more stable KOffice."
    author: "jospoortvliet"
  - subject: "Re: Joining force with openOffice"
    date: 2008-02-28
    body: "FIrst i would like to show my appreciation to the KOffice devs for the wonderful work that they are doing, for their time sacrificed and their talents. They are few and therefore have come a long way but it's just not enough compared with openOffice like you pointed out.\nI am just wondering if it wouldn't be a smart idea and more productive to join force with the openOffice guys. I mean, openOffice is improving so fast that I think it will be impossible for KOffice to catch up with it's present resources.\nI am not a coder so forgive me if I am wrong but wouldn't it be easier to develop a Qt interface for openOffice and have it better integrated in KDE than to programme a whole office suite?\nI personally think that there is too much duplication of efforts and wasting of resources in the opensource world.\n"
    author: "Bobby"
  - subject: "Re: Joining force with openOffice"
    date: 2008-02-28
    body: "OO.o's codebase is so huge and wretched that I honestly can't imagine anyone volunteering to work on it on their own time, and there's no guarantee at all that Sun would grant commit-rights to outsiders (OO.o is developed very much in a Cathedral-style).  Frankly, I consider working on KOffice a vastly more appealing prospect, *and* in terms of technology it is by far the \"best bet\" if you want to make rapid progress.  The lack of money spent on it is the *only* thing keeping it from absolutely crushing OO.o, in my opinion."
    author: "Anon"
  - subject: "Re: Joining force with openOffice"
    date: 2008-02-28
    body: "No, it would be much harder to rewrite OpenOffice with Qt. OpenOffice.org codebase is so complex that it will really take away good developing time only to port it to Qt. In contrast, KOffice 2.0 is being rewritten and easier for developers to code (cleaner code base as well).\n\nI believe currently KOffice has a very good technology (flake, pigment, etc) that will create a superior office suite in the future (given enough developers to pursue the dream). For example, using Flake technology you can embed any shape (chart, spreadsheet, vector graphics) in any KOffice application. This technology enables good component reuse and also better interaction among application."
    author: "eds"
  - subject: "Re: Joining force with openOffice"
    date: 2008-02-28
    body: "1.) I don't think OOo is improving fast. They have to invest huge efforts to clean up the code and architecture (there a lot of work left for several years). And to be honest, I didn't see a lot of real improvements from 2.1 to 2.3 (and even the upcoming 2.4).\n\n2.) Imagine the few KOffice developers helping the huge OOo team. That wouldn't make a lot of difference for OOo. Especially as the KOffice's developers expertise is in slihgtly differnent topics.\n\n3.) KOffice has a extremely nice basis. Just a little bit more development power, and it would show up as a nicely polished Office package.\n\n4.) It's very nice to have several different office packages. They can serve different purposes and thus have differnet focus/strengthes/GUI (does OOo have something like Krita, KPlato or Kivio?). And having different bases helps to detect defects (like ODF support in the past in OOo an KOffice). Imagine a piece of software that has no competition - progress stalls!\n\nTo sum it up: If KOffice would \"join\" OOo, it would mean very very little gain, but a big loss.\n"
    author: "Birdy"
  - subject: "huh"
    date: 2008-02-28
    body: "Meh, something went wrong - apparently I send the wrong file to DotMaster Danny. The last paragraphs aren't what they should be ;-)\n\nAaah well..."
    author: "jospoortvliet"
  - subject: "Re: huh"
    date: 2008-02-28
    body: "There was repeated paragraphs in the file... it was confusing ;)\n\nDefinitely some hacking and slashing needed!\n\nDanny"
    author: "Danny Allen"
  - subject: "Qt 4.4 Beta 1, painting got faaaaaaaaast"
    date: 2008-02-28
    body: "I post this with the demobrowser that comes with Qt4.4, and while typing in this textarea is slow, and takes around 30% CPU, I have to say painting is soooo fast now, the browser feels more lightweight than whatever I have seen in 8 years of linux.\n\nAlso, the speed of painting is very fast visibly when hovering over the open menu in the browser.\n\nI like it, it looks that Qt is finally faster than everything else in painting :-)"
    author: "yves"
  - subject: "KDE Clothing"
    date: 2008-02-28
    body: "Where can I purchase (in the U.S.) some of the KDE clothing and other gear thats shown in the last picture above?"
    author: "David"
  - subject: "Re: KDE Clothing"
    date: 2008-02-28
    body: "For Amarok, we have EU and US stores:\nhttp://amarok.kde.org/gear"
    author: "Ian Monroe"
  - subject: "Re: KDE Clothing - Amarok"
    date: 2008-02-29
    body: "Cool.\n\nI'm looking forward to it. Hopefully they'll have T-shirts in black.\n\nNot only would I get a great T-shirt, it would also raise some money for the Amarok team. (hopefully) :)\n\n\nIs KDE going to feature their clothing more prominently in the future as well?\n\nI would love a KDE on Windows T-shirt. I think it would bring more awareness to the uninitiated. :)\n\n"
    author: "Max"
  - subject: "Re: KDE Clothing"
    date: 2008-03-01
    body: "I put some buttons and stickers on cafepress long ago. some of them are using the old kde3 logo, others are using the generic two-colour one.\ngiven the amount of effort it took to tweak the images into looking decent on cafepress, I don't think I'll bother updating it with the kde4 images.\n\ndisclaimer: I did put a markup of a few cents on each item (I didn't see any rules disallowing this where I got the logos). I'm planning to use the money to buy more of the mini-buttons - there's almost enough for a pack of ten now :) http://www.cafepress.com/chanika"
    author: "Chani"
  - subject: "Re: KDE Clothing"
    date: 2008-03-02
    body: "Cool.\n\nAre you going to do KDE 4 in the future? (when demand increases?)\n\nCould you make T-Shirts about KDE on Windows?\n\nLike \"Switch... or don't... Use KDE anyway :) \""
    author: "Max"
  - subject: "Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-28
    body: "Does anyone know what amount of money would be required? Is there a plan to set up a fund? \n\n\n\n"
    author: "Gerry"
  - subject: "Re: Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-28
    body: "As far as I know money isn't a problem. Thomas Zander(?) wrote on the mailing list that he secured a sufficient amount of money to pay a full-time developer for several months (I think it was 2-3). The difficult part is finding someone who is very familiar with the KOffice codebase and willing to work on it in the time span before the 4.1 release (after all, most people probably already have a day job are are otherwise impeded by other commitments)."
    author: "Erunno"
  - subject: "Re: Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-28
    body: "I loved the last release of KWord and the gang.  It'd be a <i>huge</i> shame if the base office apps were not available with 4.1, especially in regards with the offerings of KDE for OS X and Windows."
    author: "jamms"
  - subject: "Re: Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-28
    body: "That's really a shame, then. Oh, how I wish I hadn't taken this new Job two weeks ago! :-)\n\nWhere and when did Thomas announce it, anyway?"
    author: "toba"
  - subject: "Re: Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-29
    body: "Here you go:\n\nhttp://lists.kde.org/?l=koffice-devel&m=120336358106066&w=2"
    author: "Erunno"
  - subject: "Re: Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-29
    body: "He didn't say that the money was already there."
    author: "AC"
  - subject: "Re: Geting KWord and Krita into KOffice 2.0"
    date: 2008-02-29
    body: "\"The money is mostly a done deal, now we need to find a qualified person \nthat can drop everything he is doing for the next 3-4 months. Thats the \nhard part :)\"\n\nhttp://lists.kde.org/?l=koffice-devel&m=12"
    author: "Anon"
  - subject: "Amarok is Cute!!"
    date: 2008-02-28
    body: "The Amarok mascot is cute :)"
    author: "Max"
  - subject: "Re: Amarok is Cute!!"
    date: 2008-02-29
    body: "It's called Mike :)"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok is Cute!!"
    date: 2008-02-29
    body: "Shouldn't the developers develop instead of watching Stargate Atlantis... (just kidding).\nMike's really cute indeed."
    author: "Terracotta"
  - subject: "Re: Amarok is Cute!!"
    date: 2008-02-29
    body: "Haven't gotten in to Stargate Atlantis yet. (it's on my todo list, first Star Trek Enterprise (the new-old Enterprise :p) ) \nNot enough time for TV. :(\n\nHopefully I'll get the reference then. :)"
    author: "Max"
  - subject: "Re: Amarok is Cute!!"
    date: 2008-03-02
    body: "It's a reference to Mike Oldfield (the composer of the namesake album Amarok)."
    author: "Ian Monroe"
  - subject: "Get composers to name songs after Amarok. :)"
    date: 2008-03-02
    body: "I wonder if we could get some Trance/Techno composers to name songs after Amarok.\n\nThat would be really cool.\n\n\nDoes anybody know any composers/DJ's that could do that? Unfortunately I live in the land of country music and alternative rock. I don't have access to any."
    author: "Max"
  - subject: "merchandise"
    date: 2008-03-01
    body: "the merchandise looks really nice! Is there an online shop where i can buy it? Or is it only available at trade shows?"
    author: "cc"
  - subject: "Re: merchandise"
    date: 2008-03-01
    body: "http://kde.org/stuff/merchandise.php\n\nhttp://amarok.kde.org/gear\n"
    author: "Mark Kretschmann"
  - subject: "Re: merchandise"
    date: 2008-03-02
    body: "Great links. Next paycheck gets spent there.. :)\n\nPlease feature this more prominently. It's hard to find. Most will agree: It's a great way to promote KDE/Amarok.\n\n\n-Max"
    author: "Max"
  - subject: "Re: merchandise"
    date: 2008-03-03
    body: "Thanks. But i like the T-Shirt at this picture: http://static.kdenews.org/dannya/fosdem2008-3_thumb.jpg\n\nThis isn't available at http://kde.org/stuff/merchandise.php"
    author: "cc"
  - subject: "Re: merchandise"
    date: 2008-03-02
    body: "My suggestion is to make an announcement at KDE.News about KDE, Amarok and related merchandise.\n\nIt would be a good fundraiser, and raise awareness about KDE, Amarok and other related products.\n\n"
    author: "Max"
---
The combined KDE/Amarok booth and developer room at the annual <a href="http://www.fosdem.org/2008/">Free and Open Source Developers' European Meeting (FOSDEM)</a> in Brusssels was a great experience (as usual!). Many people showed up from the KDE and Amarok communities, and we had a hard time fitting all our cool hardware and people in the booth. Luckily, the talks drew quite a crowd, and the booth became less busy as the day progressed. Read on for an overview of FOSDEM 2008 from the KDE perspective.




<!--break-->
<p>FOSDEM started out with a large group of KDE and Amarok people working like crazy to get the booth set up. We had all kinds of weird (and more standard) hardware, from a KDE-branded SUN-based thin client and a small VIA box, to large and small monitors connected to laptops - and all of it running KDE, in some cases the bleeding edge of development (destined to become KDE 4.1). Meanwhile, the Amarok people showed off their latest work on Amarok, with an appearance of their Mascot, Mike, who came in from the woods to 'Rok with the crowd.</p>

<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/fosdem2008-1.jpg"><img src="http://static.kdenews.org/dannya/fosdem2008-1_thumb.jpg" alt=""></a><a href="http://static.kdenews.org/dannya/fosdem2008-2.jpg"><img src="http://static.kdenews.org/dannya/fosdem2008-2_thumb.jpg" alt=""></a></div></p>


<br><br><b>Amarok</b>

<p>After our group photo and a short welcome by Bart Coppens, Nikolaj Hald Nielsen gave a talk about <a href="http://amarok.kde.org/">Amarok</a> 2.0 - the new way to "rediscover your music". Nikolaj also introduced a new friend, the Amarok Wolf <a href="http://www.google.com/search?q=amarok+mike&btnG=Search&hl=en">Mike</a>. Nikolaj then gave an outline of the history of Amarok, mentioning Free Software and the new trend which is going on besides the Free Software movement itself, the Free Culture movement - think <a href="http://creativecommons.org/">Creative Commons</a> or the rise of more open music sites such as <a href="http://magnatune.com/">Magnatune</a>. Nikolaj is employed by Magnatune now, and he discussed some details of their contributions. It is great to see a company who really gets Free Culture and Free Software! In the future, thanks to the support from Magnatune, Amarok will support a variety of different music stores.</p>

<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/fosdem2008-3.jpg"><img src="http://static.kdenews.org/dannya/fosdem2008-3_thumb.jpg" alt=""></a></div></p>


<br><b>KOffice</b>

<p>In the next talk, Bart Coppens started to show us some cool and less cool things about <a href="http://www.koffice.org/">KOffice 2</a>. Starting with the bad, he told us how, due to the developers having less time to work on KOffice, the current state of several applications was disappointing. The developers therefore recently decided to restrict their release goals, focusing on the applications they can make stable for KOffice 2.0. Those applications will most likely be KPresenter, KChart, KSpread and Karbon. Yes, KWord and Krita both might not make it for the first KOffice 2.0 release! This is an very unfortunate state of events for the one of the most innovative office suites, and there has been talk of hiring someone to get KWord into a usable state for the release. Meanwhile, Krita is very complete in terms of architecture and features, but those features aren't reflected in the interface yet - and that may not change any time soon either.</p>

<p>Now, on to the good stuff: the target of the big refactoring of the KOffice codebase, getting a tighter, lighter and more integrated KOffice is finally starting to bear fruit. Two technologies were presented by Bart to illustrate his points. To explain the idea behind <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a>, imagine rough terrain which is flattened out and made more consistent by snow flakes. With Flake as the backend technology for KOffice 2, the similarities between many different components are put into a common core. This makes the components much more lightweight, more flexible, and easier to write. Another technology is <a href="http://wiki.kde.org/tiki-index.php?page=Pigment">Pigment</a>, the color management system which now allows all KOffice applications to have proper color management - something very interesting for people working with graphics professionally.</p>

<p>Bart showed elements from Karbon and KWord, and he talked a few minutes about KPresenter and the plans for that application. Apparently, Aaron Seigo has promised to work on it to ensure it works nicely with Xinerama and generally does what he needs it to do while giving talks all over the world. Unfortunately, earlier experiments with sound & video in KPresenter failed, mostly due to Phonon not being ready for Flake shape integration.</p>


<br><b>Nepomuk</b>

<p>After Bart, <a href="http://behindkde.org/people/trueg/">Sebastian Trueg</a> entered the stage to talk about the <a href="http://nepomuk.semanticdesktop.org/">Nepomuk project</a>. Nepomuk is a research project with more than 20 organisations from across the European Union. Sebastian is mainly tasked with integrating the results of the research into KDE. The target of the Nepomuk project is to create a Social Semantic Desktop. Explaining, Sebastian gave the example of a file you once received from someone by mail and then saved somewhere. It can be difficult to find the file with current technologies, as that specific piece of information is lost when you save the file. Semantics are about relationships - and this file had a relation with the email. Nepomuk integration in KDE will allow you to find the file when looking for files related to the person you received the mail from. The Semantic Desktop will try to gather this kind of information automatically, or allow you to easily augment it so it can help you find your data or accomplish tasks easier.</p>

<p>Sebastian provided examples, explaining the benefits of these technologies and then continued to talk about the current state of Nepomuk in KDE. Currently, we have basic tagging, rating and commenting in Dolphin, and a basic search interface with a nice syntax. It allows you to search for rather specific things, but is not very easy to use yet. A final thing we will see soon is the ability to browse through the tags as if they were folders in Dolphin.</p>

<p>Similar functionality is planned for <a href="http://pim.kde.org/">KDE-PIM</a>, which Sebastian Kuegler now turns to. During a recent meeting, the KDE-PIM people decided they would love virtual folders (or live searches) within KMail. Such folders can show, for example, emails from a certain person or about certain subjects - and are updated live when new emails arrive or become tagged. Better search is generally something the PIM hackers want, and doing so by tightly integrating Nepomuk sounds like a smart move. All in all, the compelling vision behind Nepomuk left us impressed again.</p>


<br><b>Free software in Telecommunications focusing on Qtopia</b>

<p>Knut Yrvin works as community manager at <a href="http://trolltech.com/">Trolltech's</a> office in Oslo. He started his talk by telling the crowd inside the developer room about the history of both Qt and KDE. Then, Knut started to explain the concepts behind Qtopia, Trolltech's extention framework which has its focus on developing applications for mobile and embedded systems. Qtopia allows developers to deploy quite advanced applications everywhere. The ease of porting desktop applications to Qtopia, especially with Qt 4.x is really amazing! We can only agree: as at the KDE booth <a href="http://behindkde.org/people/soc2007-four/">Marijn Kruisselbrink</a> (of KOffice fame) was showing <a href="http://edu.kde.org/kstars/">KStars</a> on a <a href="http://wiki.openmoko.org/wiki/Neo1973">NEO1973 phone</a> - and according to him, he only had to recompile it!</p>

<p>After the technical description of Qtopia, Knut went over the actual implementation of Qtopia running on hardware. Knut started showing off the Trolltech Greenphone (A developer tool which has been discontinued) and explained its advantages and disadvantages. Trolltech decided that they are actually not a hardware company and therefore shouldn't be really providing hardware but instead software that runs perfectly on it.</p>

<p>After the Greenphone went through the rooms and the usual "ooh" and "aaah" responses were expressed, Knut started talking about the next generation Open Source mobile appliances. The next phone running Qtopia is going to be the NEO1973, manufactured by a partner vendor with quite a few differences in comparison with the Greenphone. For example, the NEO is being developed very closely with the community. Another nice thing Knut mentioned was that Trolltech is also sponsoring the OpenEmbedded project.</p>


<br><b>KDE on Windows</b>

<p>Later, <a href="http://www.fosdem.org/2008/schedule/speakers/holger+schroeder">Holger Schröder</a> gave a talk about <a href="http://windows.kde.org/">KDE on Windows</a>, starting with its history and progressing to the current status. Holger gave a demo to the crowd and showed off KDE applications running on Windows and interactively asked the public to ask him to show their favorite KDE program of choice. After the demo, he discussed the technical difficulties the team had to face when getting kdelibs ported to the (non-POSIX) Windows platform and explained the distribution system they had to set up for getting all KDE software distributed in a centralized way.</p>


<br><b>KDE Games</b>

<p>On Sunday, the KDE Devroom turned into a shared room with GNOME and other projects as it became the "Cross-Desktop" room. Unfortunately, we were unable to cover all talks going on, but we do have a short overview for you. After another warm welcome by Bart Coppens and Christophe Fergeau, Kurt Pfeifle and Simon Peter gave us an overview of the current status of <a href="http://klik.atekon.de/">KLIK</a> on KDE and GNOME.</p>

<p>And during these talks, our booth received many interested visitors asking about KDE 4.x and the upcoming stuff, or looking at Amarok. Some were even working on code or hunting bugs - or blogging about the event. And, of course, discussing important issues like beer or things of less importance like code. We saw a few cool new things, like Marijn Kruisselbrink demonstrating Plasma running on Mac OS X and many different checkouts that each looked totally different... And don't forget about the neat Sun-provided thin client running Solaris and KDE. Or the fact we ran out of merchandise again ;-)</p>

<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/fosdem2008-4.jpg"><img src="http://static.kdenews.org/dannya/fosdem2008-4_thumb.jpg" alt=""></a></div></p>

<p>And of course, both days ended with lots of beer, fun and good food in Brussels! Until next year...</p>


