---
title: "Akademy-es 2008 in A Coru\u00f1a"
date:    2008-11-15
authors:
  - "acid"
slug:    akademy-es-2008-coruña
comments:
  - subject: "La Cerveixa"
    date: 2008-11-21
    body: "Hello everyone! I'm writting this with my N810 from  La Cerveixa; a place were we are having a great dinner all the Akademy-es participants for free tanks to the sponsors of the event. You can chech the attached photo of me watching a tower of beer done here some minutes ago."
    author: "Eduardo Robles Elvira"
  - subject: "Re: La Cerveixa"
    date: 2008-11-22
    body: "XDD\nUnfortunately Akademy's sponsors budgets aren't too big, eh? Next time try to convince them to get something better than that \"cutrebar\", like P. Almod\u00f3var would say, ;D.\n\nP.D: Don't be cheap and ask for one of those great galician white wines like Albari\u00f1o or Ribeiro instead of beer, :p."
    author: "I\u00f1aki"
  - subject: "Re: La Cerveixa"
    date: 2008-11-22
    body: "Don't worry we have already bought Alvari\u00f1o, Caffee spirits, Mencia and other surprises for Saturday night Queimada :)"
    author: "txenoo"
---
Again it is that time of the year the Spain KDE community gathers in the <a href="http://www.kde-espana.es/akademy-es2008">Akademy-es</a> event.
This year the event will be held in <a href="http://en.wikipedia.org/wiki/A_Coruña">A Coruña</a> from 21st to 23rd of November. <a href="http://gpul.grupos.udc.es/MaKaC/contributionListDisplay.py?confId=13">The talks</a> cover quite a wide range of topics, going from Python KDE development to translation workshops without forgetting more general talks about upcoming KDE 4.2 and KOffice 2.

If you are interested in coming, do not forget to <a href="http://gpul.grupos.udc.es/MaKaC/confRegistrationFormDisplay.py/display?confId=13">register</a> before 17th of November or we cannot guarantee you will get the nice T-shirt!

We would like to thank the <a href="http://www.udc.es/">University of A Coruña</a> for the hosting, <a href="http://www.gpul.org/">GPUL</a> for co-organizing and <a href="http://softwarelibre.udc.es/">Oficina de Software Libre de la Universidad de Coruña</a>, <a href="http://www.mancomun.org/">Mancomun</a>, <a href="http://www.igalia.com/">Igalia</a>, <a href="http://www.trolltech.com/">Qt Software</a> and <a href="http://eyeos.org/">eyeOs</a> for sponsoring.
<!--break-->
