---
title: "KOffice 2.0 Beta 3 Released"
date:    2008-11-20
authors:
  - "iwallin"
slug:    koffice-20-beta-3-released
comments:
  - subject: "Impressive ChangeLog"
    date: 2008-11-20
    body: "Nice improvements everywhere. If you keep fixing at that rate, I am confident OpenOffice.org will be history on my machine. I have other uses for that disk space ;)\n\nThanks, and happy hacking!"
    author: "christoph"
  - subject: "GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "Is there any issue installing both GraphicMagick and ImageMagick on the same system?\nThe first is a strong dependency for Krita, while the later is more universally installed and is dependency for other programs I use, like transcode, so I probably need to install both to fully use Krita without crippling other tools.\nA bit anoying and useless, but I know that this has to do with the ever changing API of IM, so I can understand developpers."
    author: "Richard Van Den Boom"
  - subject: "Re: GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "I've written a small program that uses GraphicsMagick; at one point it was picking up ImageMagick's headers (due to the fact that /usr/local/include was listed in the include search path before /usr/local/include/GraphicsMagick); this, as you might guess, caused problems.\n\nMy current install of ImageMagick lives in /usr/local/include/ImageMagick, so the same problem shouldn't occur.  I can only presume that at one time ImageMagick was in /usr/local/include, causing a potential problem--my svn logs convince me this was so.  It looks, though, that things should be safer if you have new enough versions of both packages."
    author: "KDE User"
  - subject: "Re: GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "Well, to run krita, you only need the lib installed, and those shouldn't conflict with anything from imagemagick. You don't need the tools, the headers or anything else (you obviously need the headers to compile krita). And if you're willing to forego gif, xcf, psd and some other less important fileformats, you can do without altogether: Krita only uses graphicsmagick in an file import/export plugin, not in the core.\n\n(It used to do that: in the KImageShop days there was a period when the application was designed to be a gui wrapper around ImageMagick, but already when I joined up in 2003, Krita only used ImageMagick for file import/export.)"
    author: "Boudewijn Rempt"
  - subject: "Re: GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "OK, I think I'll continue testing KOffice without GraphicMagick, then.\nI thought you used it for almost all graphic import/export filters, but since it's not the case, I can do without it."
    author: "Richard Van Den Boom"
  - subject: "Re: GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "OK, I think I'll continue testing KOffice without GraphicMagick, then.\nI thought you used it for almost all graphic import/export filters, but since it's not the case, I can do without it."
    author: "Richard Van Den Boom"
  - subject: "Re: GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "The important ones -- jpg, png, tiff, raw, have all their own filters, some of which are really, really good :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: GraphicMagick and ImageMagick"
    date: 2008-11-20
    body: "GraphicMagick and ImageMagick can be installed alongside. I have both GM and IM on my machine. In fact, the only tool provided by GraphicsMagick is called 'gm', and the libraries have of course different names. (GM also have a compatibility layer with IM which of course conflict with IM... but you can do without it)."
    author: "Cyrille Berger"
  - subject: "Screen shots"
    date: 2008-11-20
    body: "any chance of some images of this release?"
    author: "R. J."
  - subject: "Re: Screen shots"
    date: 2008-11-20
    body: "There are no changes in the GUI.  There are a few changes in the behaviour but those are impossible to show in a screenshot anyway.\n\nRemember that this the only difference between this release and the last one is bug fixed and wrinkles ironed out."
    author: "Inge Wallin"
  - subject: "Re: Screen shots"
    date: 2008-11-20
    body: "thanks, I was hoping that there had been a change in the GUI to make it more professional looking.  It is hard to try and convince a business to move away from ooffice 2007 when what you are trying to convince them to move to looks like it is dated a decade  behind what they are using.  Is it possible for a more professional look for KOffice as it would majorly help in getting people to shift to it"
    author: "R.J."
  - subject: "Re: Screen shots"
    date: 2008-11-20
    body: "Well, yes, the GUI has changed a lot since 1.x, and we think it has the potential to be both better looking and easier to use -- although we know we still have a lot of work to do to realize the potential we've been working for. There's a nice screenshot in the announcement on the dot, btw."
    author: "Boudewijn Rempt"
  - subject: "Re: Screen shots"
    date: 2008-11-21
    body: "Define professional when associated with the GUI of an office suite.\nIf your answer is, like the folks \u00e0 openoffice.org, \"looks like ms office\" then I hope koffice will never look professional."
    author: "Renaud"
  - subject: "bug fixes"
    date: 2008-11-20
    body: "Is the theme now complete?"
    author: "Andre"
  - subject: "Re: bug fixes"
    date: 2008-11-20
    body: "If you mean Oxygen, no - KOffice is not well supported.\n\nOn my screen the dockwindows and the spreadsheet in KSpread share the same space (the borders of the dockwindow cover the spreadsheet) And even the dockwindows do not work well if arranged next to each other (Here again, they cover the space of the other widgets!) \n\nMoreover, KSpread still does not let you use curser keys in edit mode (when hitting F2)\n\nStill, when editing for example cell E4 containing a math equation with cross reference to A4 and B4 it may happen that either\no the format changes of A4 or B4\no the contents changes of A4 or B4\nIf I hit Ctrl+Z to undo these changes, weird thing happen if you select another cell (Undo works as long as you stay on cell E4, but if you move to C4, for example, Undo is applied to other cells??!) \n\nmy first impression...\nstrange..."
    author: "Sebastian"
  - subject: "Re: bug fixes"
    date: 2008-11-20
    body: "The dockers covering outside is a bug in Qt caused by oxygen being a bit optimistic as to what Qt can do (we are breaking new ground). Qt 4.5 will fix this.\n"
    author: "Casper Boemann"
  - subject: "Re: bug fixes"
    date: 2008-11-20
    body: "Good to hear that one..."
    author: "Sebastian"
  - subject: "Re: bug fixes"
    date: 2008-11-21
    body: "The problems with edit mode (there is more of them than what you describe) are on my fix-asap list. It's actually the only item there for KSpread :)\n\nAs for the undo issue, this is new to me, did you file a bug report ? Not sure if there's one yet about this ..."
    author: "Tomas Mecir"
---
The KOffice Team has announced the release of KOffice version 2.0 Beta 3, the third beta version of the upcoming version 2.0.  The goal for the third beta is to show progress made since beta 2, as well as to gather feedback from both users and developers on the new UI and underlying infrastructure. This will allow the team to release a basically usable 2.0 release, demonstrating our vision for the future of the digital office to a larger audience and attract new contributions both in terms of code and ideas for improvements.  Since the last beta release a significant set of improvements and speedups have been integrated for all applications and this release shows the continuous focus on bug fixes until 2.0 is released.  More information on <a href="http://www.koffice.org/announcements/announce-2.0beta3.php">the full announcement</a> while <a href="http://www.koffice.org/releases/2.0beta3-release.php">the release notes</a> tell you how to get it.



<!--break-->
