---
title: "JOLIE to Bring Service-Oriented Computing to KDE"
date:    2008-08-11
authors:
  - "skuegler"
slug:    jolie-bring-service-oriented-computing-kde
comments:
  - subject: "interview"
    date: 2008-08-11
    body: "For those who like the sound of 'Jolie in KDE' but don't understand what it's supposed to do (like me) there will be an interview with Fabrizio coming online. Hopefully during this week, if not, next week..."
    author: "jospoortvliet"
  - subject: "Java VM"
    date: 2008-08-12
    body: "Does this imply running a Java VM in my KDE?"
    author: "moon"
  - subject: "Re: Java VM"
    date: 2008-08-12
    body: "only as long as:\n\na) you want to use jolie based services (it's a runtime dep only)\n\nand\n\nb) nobody ports the metaservice app to something like c++\n\ni don't think java is so bad that people will be motivated much to do (b), and having that bit in java allows us to run it on random phones as well."
    author: "Aaron Seigo"
  - subject: "Re: Java VM"
    date: 2008-08-12
    body: "Also:\n\n- JOLIE is very light-weight and modular: you can reduce the whole installation directory to ~1MiB.\n\n- Add to that the fact that the Java guys are reducing dramatically resource consumption and start-up time for Java applications by doing things similar to the QT splitting in QTCore, QTNetwork and so on.. and JOLIE doesn't need the Java GUI library (which is the most heavy part), along with a lot of other parts.\n\n- Speaking of runtime performance (porting to C++ might have other reasons): porting MetaService to C++ wouldn't be much of help for the performance of a service-oriented language... we are speaking of network communications, so the bottleneck is somewhere else."
    author: "Fabrizio Montesi"
  - subject: "ECOS"
    date: 2008-08-16
    body: "Hey, the name ECOS is already taken!\nhttp://ecos.sourceware.org/"
    author: "Kevin Kofler"
---
Fabrizio Montesi of italianaSoftware <a href="http://akademy.kde.org/conference/presentation/5.php">showed at Akademy</a> how <a href="http://jolie.sourceforge.net/">JOLIE</a> brings new ways of interaction through the network to KDE. One of his examples is the ECOS media controller that organises control of your multimedia player (in our case that's Amarok of course) through web interfaces, handheld devices and other applications. JOLIE takes care of synchronising and concerting all those different interfaces. Read on for more details.
<!--break-->
<p>Another use-case would be Vision, a service that distributes presentations over the network. Vision enables you to show a presentation on different computers or devices and follow what is being browsed and annotated. Montesi's blog has a screencast showing off this wonderful new way of collaborating over the network. More use-cases include e-commerce offerings through geo-located services and distribution of Plasma applets via a JOLIE-powered peer to peer network. Imagine you travel to Mechelen and JOLIE offers a localised desktop with interesting events, restaurants, hotels at your fingertips.</p>

<p>JOLIE is being integrated into Plasma as we speak. Montesi announced the availability of JOLIE in KDE 4.2, slated for January 2009, so writing applets that do neat things over the network will become really easy. Another alleged goal of Montesi is to kick Angeline Jolie from her first position in Google's search results when entering "jolie".</p>