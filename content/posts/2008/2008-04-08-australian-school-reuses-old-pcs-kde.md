---
title: "Australian School Reuses Old PCs with KDE"
date:    2008-04-08
authors:
  - "canllaith"
slug:    australian-school-reuses-old-pcs-kde
comments:
  - subject: "The Jackal In The Forest"
    date: 2008-04-08
    body: "excellent, and in other linux news:\n\n* The brazilian Election Supreme Court migrates 430 thousand voting machines to GNU / Linux http://techforce.com.br/index.php/news/linux_blog/tse_migrates_to_linux\n\n* Red Hat Chairman Sells $3.4M Shares of Stock\nhttp://www.localtechwire.com/business/local_tech_wire/news/story/2696581/ (and how many shares did MS buy?)\n\n* Disgusting: http://arstechnica.com/news.ars/post/20080408-from-saboteur-to-member-microsoft-joins-kerberos-consortium.html\n\nA site to keep bookmarked along with Groklaw to keep the truth in mind:\nhttp://www.boycottnovell.com/"
    author: "Jackal In The Forest"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-08
    body: "Yeah let's boycott the company responsible for hiring the highest number of KDE devels. \n\nA company that provides one of the best KDE desktops out there, with regular updates, maintaining three branches as we speak 3.5, 4.0 and 4.1.  \n\nA company that took an already excellent but somewhat closed distribution (SuSE Pro), and made it free and its development open to the community. \n\nThat is really smart."
    author: "ad"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "'A company that took an already excellent but somewhat closed distribution (SuSE Pro), and made it free and its development open to the community.'\n\nI am not in agreement with the original poster at all, albeit for other reasons than you.\n\nBut if you are so quickly to name this company's advantage, how about the strategic alliance with Microsoft and the patent clause deal? Do you defend that as well?\n\nBy the way, I also think it is wrong to give any company too much power - we see it with the Linux kernel. Those that hire devs get it much easier to see changes in the kernel. Understandable - but unfair to \"normal\" users just as well."
    author: "she"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "No I don't defend that. I wish they hadn't done it. \n\nAnd I agree. But it's not the case, there are many other big players in the field, plus tons of small ones, plus a huge community. \n\nI think we'll be just fine ;)"
    author: "ad"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "I still haven't read any true argument to who (except Red Het) and why is the deal bad."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-10
    body: "> By the way, I also think it is wrong to give any company too much power - we\n> see it with the Linux kernel. Those that hire devs get it much easier to see\n> changes in the kernel. Understandable - but unfair to \"normal\" users just as\n> well.\n\nThat is just totally untrue. Read the report in the link below specially the section about \"Who is Sponsoring the Work\"\n\nhttps://www.linux-foundation.org/publications/linuxkerneldevelopment.php"
    author: "DA"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "If you wish to reward a convicted monopoly (Microsoft) fine, I do not.\n\nIf Microsoft is so pro-Linux, why can't I find a free software repository for Linux on their site like Google has? When I search for Linux on Microsoft's site, why are there not mounds of pro-Novell pro-Linux articles and features? They (Microsoft) have made claims of interoperability but they have done nothing to bring a working non-trojaned non-crippled DirectX to Linux or provide Linux sales or downloads of ports for their commecial software.\n\nIn 2000 Corel had some partnership with Microsoft and we heard many of the same claims we hear now with Novell and Microsoft and Linux lost out then as it will now. Go to Corel's website and search for Linux, have fun! No Corel Linux (it became Xandros after the pact and Xandros signed some patent agreement too so the circle is complete there), no tons of programs like Word Perfect for Linux. It appears to all have been abandoned. So we see where Linux and Microsofts pacts go based on the Corel and other such agreements.\n\nLinux has lasted as long as it has because it wasn't in control of one or two companies to be bought out, but through agreements with Microsoft, give it time and all of the castles will fall and the community scuttled. Just give it time. To think otherwise is to ignore the history of Microsoft and its dealings with other companies and operating systems.\n\nThe camel nose is well under the tent. The clock is ticking, the patent wagons are being gathered to circle, we'll see how long the beast remains quiet. In the land of nothing for free, you can expect Microsoft to win any patent battle against Linux. Notice how IBM has been pushed out of the government recently even after a LOC deal with Microsoft.\n\nMicrosoft's power is on the rise, we are yet to see the full monopoly in action, these are dark times as you buy your EEE (embrace extend extinguish) Xandros PCs (how much of the money for the purchase goes back to Microsoft) and whatever else Microsoft-approved-Linux.\n\nThe shills are in full force, while Linux lands on desktops it's often under Microsoft-approved-Linux like Linspire and Xandros, Novell and perhaps others.\n\nYou can either bury your head in the sand and spew Simpsons quotes to laugh and remove yourself from the facts or you can pay attention and participate somehow to make a difference.\n\nRead the history of Microsoft's actions, most of you would either suck it up like you do with your Xboxes and Halo anyway were Linux to all fall under the Microsoft banner, with the few running to BSD until that became popular and squashed too."
    author: "Q"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "\"Microsoft's power is on the rise, we are yet to see the full monopoly in action...\"\n\nHah hah! Microsoft is definitely on the decline. They lost their browser monopoly. They lost the cheap server monopoly. Mac OSX and Linux are eroding Windows' desktop market share. The received wisdom is that it is impossible to compete with a monopoly, yet it was during the height of the Microsoft monopoly that the Open Source movement flowered, that Apple made a complete turnaround in fortune.\n\nPatents are indeed a serious problem, and it needs to be addressed. But it needs to be addressed directly, without using Microsoft as a proxy scapegoat. Get rid of software patents and Microsoft ceases to be a threat. Get rid of Microsoft and the patent threat remains as strong as ever."
    author: "David Johnson"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "\"If Microsoft is so pro-Linux\"\n\nWho said that Microsoft is pro-Linux? Novell and Microsoft are still concurrents, just agreed in a few areas.\n\n\"In 2000 Corel had some partnership with Microsoft and we heard many of the same claims we hear now with Novell and Microsoft and Linux lost out then as it will now.\"\n\nAnd what's the problem with Corel not shipping Linux any more? They have the right to do so - of course it is better for us if they don't. But you want to boycott Novell right now, and not use Novell-developed Linux distros even when they ship them, so you don't need them, so what would be the problem if they stopped to develop Linux distros? Anyway, it is not likely as Novell makes much money with Linux and more and more every year.\n\n\"Linux has lasted as long as it has because it wasn't in control of one or two companies\"\n\nAnd it won't be just because some developers, even who have deal with MS, take part in its development. If the code is good, Linus will include it in Linux and it should be in Linux. If the code is bad, it simply won't go into the official Linux.\n\n\"while Linux lands on desktops it's often under Microsoft-approved-Linux like Linspire and Xandros, Novell and perhaps others.\"\n\nMostly Novel SLED or (K)Ubuntu, sometimes Red Hat, sometimes even Debian. (Xandros is on EeePC but the big migrations usually go to these.) And this is because these are the most usable distros with the largest supporting community in the \"user-friendly\" corporate desktop category.\n\n\"In the land of nothing for free, you can expect Microsoft to win any patent battle against Linux.\"\n\nIf Novell, Xandros, Linspire have a deal with MS, and, considering the worst case, MS sues evribody who use a Linux distro not purchased from a distributor with a deal with MS, you will be allowed to use SLED, Xandros, Linspire and nothing else. If there aren't these deals, you cannot use Linux at all. I wouldn't say that the first case is better as the good in Linux is the community but it is still not worse."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "..."
    date: 2008-04-08
    body: "-1 Knobhead"
    author: "Anonymous Coward"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "Novell have been a major contributer to various OSS projects including Linux and KDE. A recent report states that Novell is the second largest contributer to Linux kernel. Still I cant figure out why anyone should boycott Novel? If so then I think we may have to boycott KDE, Gnome, Linux etc since they all are using code contributed by Novell or by developers paid by Novell."
    author: "Jst4Fun"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-09
    body: "You make it sound as if this is a unique and only advantage.\n\nBut there are two sides of a medal to it.\n\nIf we look at only one side, we will not see the bigger picture."
    author: "she"
  - subject: "..."
    date: 2008-04-09
    body: "Do or do not, there is no try."
    author: "Yoda"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-12
    body: "> A site to keep bookmarked along with Groklaw to keep the truth in mind:\n> http://www.boycottnovell.com/\n\nPlease take your FUD somewhere else. Read: http://opensuse.org/FAQ:Novell-MS"
    author: "apokryphos"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-13
    body: "\nHere is other page for you, because you gave the novell fact page, this is just makes your argument complete ;-)\n\nhttp://www.microsoft.com/windowsserver/compare/default.mspx"
    author: "Fri13"
  - subject: "Re: The Jackal In The Forest"
    date: 2008-04-13
    body: "The Novell FAQ obviously cannot be regarded as unbiased (btw what apokryphos linked is the openSUSE FAQ based on the Novell FAQ) but you can only say it is not an acceptable argument if you can prove that it is not true. So which point of that FAQ is false?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Good news..."
    date: 2008-04-08
    body: "Congrats to the school (&KDE!). It may only be a few computers, but success like this spreads as teachers talk to one another.\n\nEducation is one area where even small savings can be important for buying materials, providing trips etc. to students. Even if the benefit of the computers themselves is small, the knock on effect can be huge in the long run.\n\nI suspect teachers will also appreciate the self-reliant nature of using OSS away from outside interference and budget constraints: It'll allow them to get on with their jobs.\n\nKeep it up :)"
    author: "Martin Fitzpatrick"
  - subject: "Kiosk in KDE 4?"
    date: 2008-04-08
    body: "Way to go KDE!  This is great news.  BTW, I'm using KDE 4.03 and loving it, but I haven't found out any info on the progress of Kiosk in KDE 4.  Does anyone know about its status?"
    author: "scanady"
  - subject: "Re: Kiosk in KDE 4?"
    date: 2008-04-09
    body: "KIO will be droped in favor of policy kit I think."
    author: "Div"
  - subject: "Re: Kiosk in KDE 4?"
    date: 2008-04-09
    body: "huh, what can policy kit do which is even remotely close to KIOSK? They have nothing in common...\n\nKIOSK restricts what the user can do in the GUI, Policy kit is like a smarter SUDO replacement."
    author: "jos poortvliet"
  - subject: "Re: Kiosk in KDE 4?"
    date: 2008-04-09
    body: "Yeah makes me wonder too... especially as Policy Kit introduces namespaces to users, but its an outside-KDE project and not that easy to setup (takes time)\n\noh well hope this info is not correct"
    author: "she"
  - subject: "Re: Kiosk in KDE 4?"
    date: 2008-04-09
    body: "kiosk (not kio, which is about file access (ergo the \"io\")) will not be dropped in favour of policykit; the two are complimentary but orthogonal concepts.\n\nkiosk lets one define defaults and lock down policies for settings, policykit allows one to allow access to and define processes that need to be run with auth priveleges other than what the user may be logged in with.\n\nboth allow user/group definitions and are rather flexible at runtime, but both are needed rather than one replacing the other."
    author: "Aaron Seigo"
  - subject: "Re: Kiosk in KDE 4?"
    date: 2008-04-09
    body: "Kiosk ist of course still working in KDE 4, just like it worked for KDE 3. But you should keep in mind that some keys changed: for example all kicker keys are not valid anymore, but that's clear because there is no kicker in KDE 4 :)\n\nHowever, this article mentions the GUI to kiosk, kiosktool - and this might be a problem with KDE 4: at the moment the development of kiosktool has stopped. There is no maintainer who is willing (or better: who has time) to port the tool over to KDE 4."
    author: "liquidat"
  - subject: "RSS Feed?"
    date: 2008-04-10
    body: "Is there something wrong with the RSS Feed at the Moment. The Dot RSS Feed does not show the last few Articles (The last one is KDE and Wikimedia Collaborate) \nOther RSS Feeds on my PC are fine. \n"
    author: "Kavalor"
  - subject: "Yup."
    date: 2008-04-10
    body: "I get the same here.\n\nStill doesn't contain the article bodies either. Pah :)"
    author: "Martin Fitzpatrick"
  - subject: "This is an important but overlooked tool"
    date: 2008-04-12
    body: "IMHO, Linux machines in public places like schools, libraries, cafes, bars, etc. are a great (and free!) way to promote Linux to new users. Therefore, it would be important to continue the development of Kiosk tool and also port it to KDE 4 as soon as possible.\n\nUnfortunately, Kiosk tool isn't currently very well suited for this in \"a promotional way\": when you use the tool you'll end up hiding (instead of just locking) almost all the great features KDE has to offer. This will give you a DE that will appear boring and featureless and won't give a good impression about KDE (or \"Linux\") for potential new users.\n\n(Of course, whether the Kiosk tool hides or just locks features could also be optional.)"
    author: "trg"
  - subject: "Re: This is an important but overlooked tool"
    date: 2008-04-12
    body: "I forgot to mention what I mean by locking features instead of hiding: a feature (or option) is visible for the user but when he/she tries to use the feature the system would give a message like \"this feature has been disabled by the admin\" or something like that."
    author: "trg"
  - subject: "Re: This is an important but overlooked tool"
    date: 2008-04-12
    body: "How does it work, does it show a lock icon in place of the greyed out checkbox?"
    author: "kol"
  - subject: "Re: This is an important but overlooked tool"
    date: 2008-04-12
    body: "Well, here is one example:\n\nSay we want to restrict user's ability to change display settings. Instead of taking away all the possibilities to get to the settings dialog the user would be presented with a greyed out dialog along with a message that \"The administrator has restricted access to these settings.\" (or something else along those lines).\n\nIn this case the curious user might think something like \"Ok, I can undestand why the restricted access here, but this \"Linux system\" is pretty cool nonetheless. I should try it at home.\".\n\nBut if the settings are totally inaccessible the user might think \"Nah, there is nothing here. Linux sucks. No wonder nobody's using it except for this damn school of mine.\".\n\nOh, and BTW. My example is pretty much in line with the standard behaviour of the KDE control panel. Just take a look at the settings that require root access..."
    author: "trg"
  - subject: "skolelinux?"
    date: 2008-04-20
    body: "How about trying out skolelinux as well? :) Never beeen easier to set up server, thin clients, laptops and half thin clients ever! :) And of course, KDE is default ;)\n\nThe whole idea behond skoleinux is for someone non technical to be albe to install it all. I've never seen anything beeing so close as skolelinux. It just blows away the competition. And it makes me wonder why Canoical goes down the Edubuntu path. Feels like just such a waste of time. Skolelinux is based on Debian and is now sort of a part of Debian. Please check it out :)"
    author: "Jo \u00d8iongen"
---
A secondary school in Melbourne, Australia, is <a href="http://www.computerworld.com.au/index.php/id;609377314;pp;1">using KDE on Kubuntu</a>, with KDE's own <a href="http://extragear.kde.org/apps/kiosktool/">Kiosk tool</a> to lock down its library workstations. Implementing a kiosk mode Kubuntu setup allowed Westall Secondary School to save money, exact greater control over security measures, and extend the life of older and previously abandoned hardware without sacrificing performance. The school's IT support manager said the Kiosk admin tool was chosen as there was not enough flexibility with other desktops to allow "decent" lockdown. He was also surprised to discover that Kubuntu desktops ran some applications faster on Linux than when they ran on Windows. 


<!--break-->
