---
title: "Alitheia Online Demo Available"
date:    2008-07-21
authors:
  - "sk\u00fcgler"
slug:    alitheia-online-demo-available
comments:
  - subject: "Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "The difference between commercial and \"free\" software:\n\nYou can either use commercial (eg. Microsoft) software and pay for it, or not use it and NOT buy it.\n\nAs for \"free\" software, you pay for it in taxes, whether you use it or not. Heck, you can be a computer-illiterate pauper, you STILL pay taxes to finance \"European Commission's Framework Programme 6\" and similar things.\n\nI hate this soviet understanding of the word \"free\"."
    author: "Mr. Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "+1"
    author: "John Galt"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "Nice job."
    author: "Hank Rearden"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "-1"
    author: "Ioannis Gyftos"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "-1"
    author: "markus"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "In the summary, 'free' refers to free as in speech and has nothing to do with what it costs the end user.  If you were talking about one of the linked pages, please state which.\n\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "So, if someone's opinions are being spread wider by government subsidized propaganda, and everyone, including that someone's opponents are forced to pay for it, that would be the \"free speech\" you are talking about in your \"free as in speech\"?"
    author: "Mr Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "Again, what you say has nothing to do with the issue at hand. It is about being free to say whatever you want. Money is not involved at all."
    author: "jospoortvliet"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "Then if you are free to say whatever you want, BUT you have to pay your opponents to help them convince people to ideas opposite to yours, this is \"freedom of speech\"? DON'T underestimate the power of money. I view freedoms given by \"free as in speech\" as a strict superset of \"free as in beer\"."
    author: "Mr Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-26
    body: "So, do not pay taxes if you're not aligned with the political party has won last elections. You have a distorted sense of democracy (not to speak about your notions about what \"free software\" is)"
    author: "Vide"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "What the heck are you talking about?\nFirst, they told you that free refers to a freedom of studying and using the source code. Even derivatives of it quite easily.\n\nSecond, what you are describing counts for companies as well. Or what else are tax cuts for big corporations please? It is a subsidy as well.\n\nBesides if they have losses they dont need to pay as much - this is money laundering as well, though not as well known as \"old school\" money laundering.\n\nLast but not least get your ass out of your head. You dont want to claim that only governments subsidize propaganda right? What else is advertisement?\nEveryone pays for monopolies as well, less competition means that you can regulate and dictate a market more easily (though more competitors alone does not help always, because they can control a market too with join discussion groups against consumers)"
    author: "markus"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "Others have pointed our your incorrect assumption on the meaning of the word \"free\" in this context, so I'll reply to the general sentiment of your post.\n\n> I hate this soviet understanding of the word \"free\".\nThat's your right.\nBut there is a large number of people who don't mind having their tax money put to purposes that help society as a whole.\nIn fact, there's a very large proportion of people who believe that the entire _point_ of government is to help society as a whole.\nThere's nothing soviet or even Marxist about that, it's how governments have been since the dawn of time.\nIf a government isn't doing what it can to better society, then what _is_ it doing?\n\nHowever, it's the nature of people that you can't please all of them all the time, so there will always be those who disagree.  That's good, as long as those that disagree can understand that there will be people that disagree with _them_ too.\n"
    author: "mabinogi"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "> But there is a large number of people who don't mind having\n> their tax money put to purposes that help society as a whole.\n\nAnd just _WHO_ is deciding what purposes will \"help society as a whole\"? I like to decide about my hard-earned money myself. Besides, as you yourself noted:\n\n> it's the nature of people that you can't please all of them\n> all the time, so there will always be those who disagree.\n\nThat's right. And they do have the moral right NOT to be forced to pay for something they do not approve of.\n\n> That's good, as long as those that disagree can understand that\n> there will be people that disagree with _them_ too.\n\nExcellent! Just keep in mind my previous point. DON'T force people to finance something they disagree with, or just don't care about.\n\n> In fact, there's a very large proportion of people who believe that\n> the entire _point_ of government is to help society as a whole.\n\nWell, given that the government makes a (not so modest) living off \"helping society as a whole\", it is unsurprising that it showers us with propaganda telling us how good and helpful it is. And given the amount of propaganda, it is equally unsurprising that many people fall for it. A government that tries to control the society (of course, they always call it \"help\"), is called TOTALITARIAN."
    author: "Mr Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "Well, Mr. Bean, I definitively disagree with you. That kind of money would still be spent on project you and me have little control. At least, as a free project, the people can use it if they want. "
    author: "edomaur@gmail.com"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "If you mean \"the government would either way spend that money on something\", then I agree completely! It's better that they spend it this way (though it is my opinion, and other people have right to disagree. And not pay, of course ;)). The problem is, the government shouldn't have that kind of money in the first place!"
    author: "Mr Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-22
    body: "I could extend your reasoning yet further: People without a car will probably disagree with taxes being spent for maintaining the street quality. People without children will disagree with child benefits. And more globally, if any person had the choice of paying taxes or not, most of them would choose to not pay. In this case, the government would become obsolete.\n\nTaxes make sense because they enable the government to (try to) influence the society's development (see campaigns against drugs and HIV). The main difference between democratic and totalitarian governments is the intention of the influence.\n\nIn the case of SQO-OSS, the European Union (in the long run) probably aims to raise the quality of software produced in the EU. The point with SQO-OSS being F/LOSS is probably that it should be free to use for anyone. Without the active F/LOSS culture, the EU would have closed a contract with some company to do that job, and any customer would have had to pay for it. That is the only difference I see, and it is definitely not desirable (except for the contracted company)."
    author: "Stefan Majewsky"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "As far as money and paying for things you don't want goes: the government is supposed to do what is good for society, all of it. Take research for example. It's expensive, and often doesn't bring advantages immediately. So the government pays for it, and in time, all of society benefits from the discoveries made. You might not care for some discoveries, but you surely benefit from others, be it technological, medical or otherwise.\n\nNow who decides? Well, again, society as a whole is supposed to decide that. We use democracy to pick ppl capable of making decent decisions (never perfect, but that's not possible anyway).\n\nOf course, if the democratic system doesn't work properly (like, for example in the US), the results are sub-optimal. But there's a flaw in every system - you always have to pay a bit for things you don't agree with. But in a way these things even themselves out. Suppose you pay 200 euro a year. You don't want to pay 100 euro for more roads because it's bad for the environment but you DO wanna pay 100 euro for a natural preservation fund. Your neighbor wants more roads but doesn't like the natural preservation fund. Beautiful, in a way you can say the 200 euro you each pay goes fully to what you wanted ;-)\n\nOf course, that's a bit optimistic, but hey, part of the game. Of course, there are also some humanitarian feelings involved - you paying for diseases you'll never have, you pay for helping ppl who experienced an earthquake or other more or less natural disasters..."
    author: "jospoortvliet"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "> Well, again, society as a whole is supposed to decide that.\n\nThere's no such thing as a \"will of society\". The society is composed of millions of individuals, having their own wills and interests. Usually conflicting with other people's. Think of it this way: you call a person with two distinct personalities \"mentally ill\". How do you call someone with MILLIONS of distinct personalities?\n\n> We use democracy to pick ppl capable of making decent decisions\n\nPuh-leez...\nPick your favorite democratic country and look at it's government. It's obviously full of people \"capable of making decent decisions\". Okay, sorry. Bad joke.\n\n> Of course, if the democratic system doesn't work properly\n> (like, for example in the US), the results are sub-optimal.\n\nYou're wrong if you think US government is special in any way. You will find things exactly the same in other democratic countries.\n\n> Suppose you pay 200 euro a year. You don't want to pay 100 euro\n> for more roads because it's bad for the environment but you DO\n> wanna pay 100 euro for a natural preservation fund. Your neighbor\n> wants more roads but doesn't like the natural preservation fund.\n> Beautiful, in a way you can say the 200 euro you each pay goes\n> fully to what you wanted ;-)\n\nCorrect. Add to this 5 other people such as the guy you're talking about. And 5 people like his neighbor (I admit my views are like his ;)). So far so good. Now throw in a guy that doesn't want to pay NEITHER for more roads, nor for the NPF. He's screwed.\n\n> Of course, there are also some humanitarian feelings involved -\n> you paying for diseases you'll never have, you pay for helping ppl\n> who experienced an earthquake or other more or less natural disasters...\n\nJust tell me Where_The_Hell are \"humanitarian feelings\" involved in being robbed of justly earned money for whatever purpose!? If I donate of my own will to a cause that I consider just and morally worthy - then we can talk about \"humanitarian feelings\" on my side. If I don't have a choice - where do you see any feelings involved?"
    author: "Mr Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "duh.. it boils down to the point that you just don't want to pay taxes for two reasons\n\no  first, because you don't want to give the money that you earned so hard or at least not in the way it is atm.\n\no  second, if the taxes take (part of) your money away you have no control over what happens with your money.\n\nThe first one is of course a point you could agree on with a lot of other people. The views range from \"it's too much\" to your point \"robbed of justly earned money\"\n\nThe second one is a bit more complex. If you're living in a democratic country you're free to vote a party which voices your opinions or better... you're even free to try to get yourself elected, which gives you ultimate control over what happens with the tax-money (or the ability to do away with the taxes altogether)\n\nIf this is not going to work out (despite the fact so many people are complaining about the taxes and apparently agreeing with you) the majority of other people must still have a fundamentally different point of view than yours.\n\nIf you don't find a majority supporting your ideas it's never going to happen in a democracy, which I fear, is just happening to you.\n\nI for one am fine with some careful governmental funding of OSS as I think it can be well-spent. The contracts can be done with local companys providing jobs and the results are ultimately going to be free in the OSS-wild.\n"
    author: "Thomas"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-22
    body: "Mr. Bean has just do not understood what democracy mean: \n\nDemocracy means the majority decides. Full stop.\n\nOnly exceptions: Human rights and the constitution in each society which gives borders to the possible decision of the majority.\n\nAnd for that reason, the individual has no right to decide on what the governement is doing with each money. It is the society in general through elecetions of a representative/governement and the control of the governement with free speech, free press, other parties, etc...\n\nAnd in case you don't agree on democracy for your personal reasons. Let's repeat what Churchill said once:\nDemocracy is the worst system I can think about, but I also doesn't know any better system."
    author: "Philipp"
  - subject: "Re: Commercial and \"free\" software - the differenc"
    date: 2008-07-21
    body: "Hello,\n\nif it was not for the government: The roads that you use to go to work wouldn't exist, and not so many more things. The government is building infrastructure for everybody to use. And Free Software is infrastructure. I would better pay a little share of state funding for software, than eternal Microsoft tax.\n\nit it was not for the govenment: The house that you live in would not be yours as somebody stronger would have it. There would be no currency, as nobody could give the guarantuees needed for it, so you weakling would be left with what you can produce, can protect yourself, and can swap for other goods.\n\nThere is no moral right to not pay taxes, just because you don't agree with some goal, unless the government is not democratically elected or you are contributing to its replacement with one that is.\n\nThe fact with democrazy as of today is that the majority of people is happy enough and doesn't care. So they let minorities and interest groups take charge of the decision processes. And in fact that works pretty well.\n\nBecause in this example at hand, even the minorities normally discover that you can't ruin the state, but have to invest in building infrastructure. The project may or may not have much impact. But if it does, it sure was worth the investment, because Free Software is a major driving force of growth. And the advantage of Europe leading in Free Software may well turn out to be an important advantage in this century. It is certainly going to enlarge the social product.\n\nAnd if only to make it clear for guys like you: De facto taxes that go to Redmond/US are not going to help you. Anything that contributes to liberating more companies from them, means higher wins for European companies, and an advantage of them in the worls, compare to those parts that are behind on this issue.\n\nAnd sure, Microsoft can buy whole countries and governments out of the idea of liberating themselves. But not Europe. It's too big to be bought. It has recognized the enourmous damage our Microsoft dependence has and it is set to do something about it. And the parts that did not, will be at huge disadvantage.\n\nNow talking about the project. Something like this is badly needed. I just hope its done in a way that the project gains steam on its own, so it can become a part of the Free Software society, indepedent of government funding.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Commercial and \"free\" software - the differenc"
    date: 2008-07-21
    body: "you are absolutely right, mr. bean. i too don'T want my hard earned money to be used for purposes i don't support such as:\n\n* troops in afghanistan and/or iraq\n* costs for the military in general (i don't need it, so why am i forced to pay for it, right?)\n* public schools (i'm out of school for a long time, i don't want to pay for them, since i don't need them)\n* the space shuttle (stupid thing, always explodes)"
    author: "tk"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: ">> But there is a large number of people who don't mind having\n>> their tax money put to purposes that help society as a whole.\n \n> And just _WHO_ is deciding what purposes will \"help society as \n> a whole\"? I like to decide about my hard-earned money myself. \n> Besides, as you yourself noted:\n\nOf course that's something you should have researched yourself. Those decisions are made by a jury of volunteers. It's actually quite easy to get involved with this decision making process yourself. Not as easy as complaining about it, though.\n\nBoudewijn Rempt, Krita maintainer has for example taken part in this jury in the past, he also blogged about his findings and encourages people to take part in this decision making process. (His involvement didn't have anything to do with the SQO-OSS project, though.)"
    author: "sebas"
  - subject: "Re: Commercial and \"free\" software - the differenc"
    date: 2008-07-21
    body: "> And just _WHO_ is deciding what purposes will \"help society as a whole\"? I like to decide about my hard-earned money myself. Besides, as you yourself noted:\n\nThat would be the voters. Don't like how your money is spent. Vote for someone else.\n\n> That's right. And they do have the moral right NOT to be forced to pay for something they do not approve of.\n\nI don't agree. The society do have the right (and obligation) to use our money to do things that all might not approve of. Say that you're vaccinated against some nasty and really bad disease, then you'd probably object to your tax money being used to give vaccine out to anyone, but in the interest of society that's probably a good idea anyway. Innoculate the population before half of it gets too sick to work or similar.\n\n> DON'T force people to finance something they disagree with, or just don't care about.\nWhere I live taxes are used to pay for daycare for children. Even those who don't have children have to pay this. They probably object now, but the children that they don't want to pay for now are the ones that will help pay for their pension later.\n\n"
    author: "Oscar"
  - subject: "Re: Commercial and \"free\" software - the differenc"
    date: 2008-07-25
    body: "Indeed. Most ppl are way to stupid to be allowed to decide about what to do with their money anyway... ;-)"
    author: "jos poortvliet"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-22
    body: "What gets me is when government spending is beneficial to the people, it is called \"soviet\" in a negative fashion.\n\nI would hope there is more \"soviet\" spending in this definition."
    author: "T. J. Brumfield"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "You apparently don't get the meaning of the word free. It's a bit more complicated than 'being able to do whatever you want'. If you knew a bit about Free Software, you would get that, I think.\n\nTotal freedom of every individual would lead to a paradox. I suppose you know enough about logic to know what that means - it's not possible.\n\nConsider this: Say one person would be perfectly free. That would mean he/she would have the power to take away freedom from others. In other words, only one person can be truly free at any given point in time.\n\nTo have as much freedom for everyone as possible, you have to restrict freedom. More specifically, you have to restrict the freedom of one person to act in a way which hinders the freedom of others to protect freedom for all! How to do that is an entirely different matter, and the various opinions about it explain the differences in governments around the world.\n\n\n\nAnyway. Nice flame. Going to a FOSS site and talk about Freedom, nice one, really. I suggest you take a screenshot of your accomplishments before this thread gets removed for being completely off-topic."
    author: "jospoortvliet"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "> You apparently don't get the meaning of the word free.\n> It's a bit more complicated than 'being able to do whatever you want'.\n\nI understand \"freedom\" in the classic liberal way (not to be confused with the upside-down meaning the world \"liberal\" has taken in the US). That is, the freedom of my fist is limited only by the freedom of your nose.\n\n> To have as much freedom for everyone as possible, you have to\n> restrict freedom. More specifically, you have to restrict the\n> freedom of one person to act in a way which hinders the freedom\n> of others to protect freedom for all!\n\nCorrect. The question is, what is understood under the word \"freedom\". If it is the soviet meaning of \"You are free to pay for and use whatever we shove down your throat\", then my concept of \"freedom\" is completely opposite.\n\n> Anyway. Nice flame.\n\nIt was not supposed to be a flame, though I was aware that many people would read it as such. It's just my honest opinion.\n\n> I suggest you take a screenshot of your accomplishments\n> before this thread gets removed for being completely off-topic.\n\nIt won't be removed. The dot is ran by hackerly people who value free speech, so I don't expect censorship. It's not like I spammed the thread or flamed someone, did I?"
    author: "Mr Bean"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "thanks for pointing out the obvious, great insight. anything else you'd like to point out?"
    author: "mike"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "First, you need to define which version of 'free' you're using here. Free in the context of this article (on the dot) is referring to Freedom/the source code being opened under a permissive license. Secondly, commercial and FLOSS aren't mutually exclusive (take a look at Qt, MySQL, Wine, the enterprise Linux distros like RHEL, and *lots* of other examples) and often a very good business model.\n\nYou also act as if all Free software is funded by governments, and it most certainly isn't (at least I haven't gotten any money for writing my web browser :P). Unfortunately, a lot of software funded/developed by governments is released as closed source, and occasionally even requiring additional direct payment. IMO, any software developed/funded by governments *SHOULD* be released either as public domain or a permissive license (BSD would probably be best, then LGPL and lastly GPL for the most well known licenses) as it was created by the public after all.\n\nI also don't particularly like governments spending taxes on non-vital projects, but on the abuse scale this seems rather minor (although I'm not sure how much funding is being put into this, I have a gut feeling it isn't that huge). Assuming this project is a success, this project should indirectly (and some directly) help everyone in Europe (and realistically other countries as well), as nearly no one in any developed country goes without indirectly using one (hell, the taxes you're paying are probably processed using a computer system).\n\nOverall, your post comes off as being rather trollish as you have many false assumptions as well as being directly offensive to most people that'd be regularly reading this website. If your goal wasn't to troll, I'd suggest choosing your words more carefully as well as a better venue for this discussion. Poor word choices and limited understanding of a topic are exceptionally ways to sully your position in an argument and often will only push people even further away (as in the future they'll be less likely to take that side seriously). If this hadn't been an interesting topic on which to muse for a little while, I probably would have simply ignored your post, but I've enjoyed a little musing this morning."
    author: "Kitsune"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "If you had a choice between your tax money going to either Free Software or Micro$oft, which would you choose? I know where I live, the governments use M$ software, so, as you can guess, my tax money is going to Micro$oft (boy do I wish it wasn't true).\n"
    author: "Michael \"this is a grey area. Is he a troll?\" Howell"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "\"The difference between commercial and \"free\" software:\"\n\nYadiyadida...\n\n\"You can either use commercial (eg. Microsoft) software and pay for it, or not use it and NOT buy it.\"\n\nAh! Thats why noeone buy's Windows. It's just there on their hardware, for free! Eh.. Monopoly?\"?\n\n\"As for \"free\" software, you pay for it in taxes, whether you use it or not. Heck, you can be a computer-illiterate pauper, you STILL pay taxes to finance \"European Commission's Framework Programme 6\" and similar things.\"\n\nSo all OSS is funded by taxes? How about your government spending \"your\" taxmoney on free SW instead of giving your money to one of the richest men in the world? By choosing free software every one in the world will benefit from the money \"you\" spent, while going for MS will only benefit Mr. BG.\n\n\"I hate this soviet understanding of the word \"free\".\"\n\n?!? Soviet? So being able to take code and do what ever you like with it (as long as you share) is Soviet like? You must have a strange circuit board in your brain. Free software is about freedom of thought, freedom of will, freedom to express one self, freedom to innovate, freedom to develop. Now, where does this relate to Soviet society? Sorry Mr. But this was a poor troll. Or worse, not thought trough at all."
    author: "Jo \u00d8iongen"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-21
    body: "> I hate this soviet understanding\n\n???\ni18n(\"soviet\") == \"board\" or \"council\"\n\nWhat do you want to say ?\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-22
    body: "Whether you use Microsoft or non-Microsoft software, you still pay the same taxes.  However, because the EU is not a big Microsoft fan, they've also levied some massive fines on Microsoft.  Last time I checked, the two fines came to be about 2 billion dollars.\n\nI'm going to go on a limb and guess the EU hasn't spend 2 billion dollars on the \"European Commission's Framework Programme 6\", so you could look at it that they're still using the \"house's\" money."
    author: "T. J. Brumfield"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-22
    body: "My taxes pay for Microsoft products (and through that Microsoft research) because my government decides to buy Microsoft products for government offices and schools. From my point of view as a non-MS user that sucks because that money/research doesn't benefit me in any way (copyright and patents prevent the research my taxes paid for filtering through into the software I actually use).\n\nAnd really, if you have a problem with this then why not raise it with the EU/your European MP/national MP who can actually control where your taxes go. We, the KDE developers and users cannot control this."
    author: "Simon"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-26
    body: "Yea, right. In the real world however, proprietary software vendors such as Microsoft are ultra-major tax money beneficiaries. "
    author: "j95"
  - subject: "Re: Commercial and \"free\" software - the difference"
    date: 2008-07-28
    body: "What I don't understand is why so many people reply to a obviously provocative and useless post and try to convince someone who obviously doesn't want anything else than a _stupid_ discussion. You will not convince him, because he does not want to hear anything reasonable. It's good that in a democracy even people like the anti-tax guy have freedom of speech (take that, Mr Bean) but they are far away from being a majority and they don't deserve too much attention, I believe."
    author: "Stephan Ehlen"
  - subject: "typo in the text"
    date: 2008-07-21
    body: "\"consists of a number of European organisations with knowledge relevant to build such a platform, among which KDE e.V..\"\n\nthis sentence seems to have suffered from copypaste at some point, it's either missing some words or has wrong forms of them :)"
    author: "richlv"
  - subject: "PHP"
    date: 2008-07-21
    body: "Does this work with php as well?\nIf it does then i am very interested in evaluating this product.\n"
    author: "Mark Hannessen"
  - subject: "Re: PHP"
    date: 2008-07-22
    body: "Metrics that do not need to parse the source code will work out of the box"
    author: "Georgios Gousios"
  - subject: "Will it be useful?"
    date: 2008-07-21
    body: "Could someone please explain how this is/will be actually useful? The front page promises \"scientific proof of [code] quality\". This gets watered down to \"metric-based assessment techniques to assess quality characteristics\" on the \"What is SQO-OSS?\" page. And the actual demo [1] does little more than count the number of lines of code.\n\nI can see how that sort of statistics could be nice to look at for a manager type, in a weekly report, but not how it would actually improve the quality of, say, Abiword.\n\n[1] http://demo.sqo-oss.org/projects.jsp?pid=1"
    author: "Martin"
  - subject: "Re: Will it be useful?"
    date: 2008-07-22
    body: "This is an early demo with a not so good user interface. It is true that is shows little more than what wc -l would show. However, underneath the surface, there are more than 50k locs of code that enable this very calculation (wc -l) to be run and stored for all ~13500 revisions of (say) project XMBC in less than 15mins. That is the equivalent of checking out every revision, running wc -l and storing the results in an accessible way. There are more interesting metrics being developed as we speak, check project hsqldb[2] for example.\n\n\n[1] http://demo.sqo-oss.org/projects.jsp?pid=12\n[2] http://demo.sqo-oss.org/projects.jsp?pid=10"
    author: "Georgios Gousios"
  - subject: "Code Swarm: subversion visualization"
    date: 2008-07-22
    body: "http://www.nabble.com/Fancy-MediaWiki-codeswarm!-to18544723.html\n\njust came across this nice tool and wonder whether KDE is too big to visualize it. Perhaps a module like \"kdelibs\" will work?"
    author: "MK"
  - subject: "How to use"
    date: 2008-07-23
    body: "Hey, I finally found out how to use the tool. You can go and select single files from svn (make sure to find the panel on the left, as the main view doesn't work). You may then see the \"quality outcome\" calculated for the files. Currently I didn't find a .c file yet, but hey, who knows, I might come up with one and post another \"HOWTO\"."
    author: "Chris"
  - subject: "Re: How to use"
    date: 2008-07-27
    body: "I found a Cpp file!\n\nhere's the link:\n\nhttp://demo.sqo-oss.org/files.jsp?did=47734\n\nLOC & LOCOM metrics are available, and if you select versions of the file you get a nice graph showing the metric values over diferent versions, \n\nIt would be nice to have some more metrics, perhaps some design flaws, i'm interested what their framework really does in the direction of parsing C, C++, Java, Python, PHP?\n"
    author: "Mihai"
---
The <a href="http://www.sqo-oss.org">SQO-OSS</a> project aims at developing a software quality assessment platform to Free Software developers. SQO-OSS is a project funded through the European Commission's Framework Programme 6 and consists of a number of European organisations with knowledge relevant to build such a platform, among which KDE e.V.. After more than one and a half years of research, design and development the SQO-OSS developer now have made available a first demo showing some capabilities of the Alitheia system. Alitheia stands for the ultimate and business-like truth. Read on for more details.



<!--break-->
<p>The significance of Alitheia 0.8.1 is the core being in a reasonably stable state and able to run metric plugins. Results of those plugins <a href="http://demo.sqo-oss.org">are being displayed</a> in the webinterface. Some sample projects are available in the demo. Do note that the demo only shows the front-end of the system and little about its back-end, administration system. More information about the SQO-OSS project can be found on <a href="http://www.sqo-oss.org">its webpage</a>. If you want to get in contact with the developers of SQO-OSS you can <a href="https://lists.sqo-oss.org/mailman/listinfo/devel">subscribe</a> to the SQO-OSS devel mailinglist or drop by on #sqo-oss on irc.freenode.net.</p>

<p>If you have any questions, such as how to develop your own plugin that combines metrics based on KDE's bug database, subversion repository or mailinglist archives, the places listed above are the right places to get started.</p>