---
title: "KDE Commit-Digest for 22nd June 2008"
date:    2008-07-31
authors:
  - "dallen"
slug:    kde-commit-digest-22nd-june-2008
comments:
  - subject: "Statistics"
    date: 2008-07-31
    body: "Why are the statistics missing?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "kewl"
    date: 2008-07-31
    body: "kewl, a digest that mentions kde 4 beta 2.\n\nCome on danny, forget these old ones and put out the latest digest, go back to the old ones when you have time.  Personally I am more interested in reading the latest releases, not stale news.  Do people go and buy a month old paper to read the news?  No.  So give us the latest news please.\n\nHow many people here would love for Danny to give us the latest digests now and work on the ones he missed when he has time?"
    author: "anon"
  - subject: "Not me"
    date: 2008-07-31
    body: "No, thanks. I would rather like to read them in chronological order, despite being \"old\" (if you call them that way). If you really want \"hot\" news, you could get that information elsewhere.\n\nHaving said that, I should mention that I love reading the digest, thanks Danny!"
    author: "christoph"
  - subject: "Re: Not me"
    date: 2008-07-31
    body: "i agree. keep the order. one thing that could help - maybe make the older ones slightly less in-depth to skim faster over them :)\nhaving more than a month old news looks a bit odd.\n(i guess it's obligatory lately to add here that i love digests, thanks and so on ;) )"
    author: "richlv"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "While I always love to read old digests and everything Danny puts up, I have to agree:\n\nPlease start from scratch with the current digest.\n\nYou could summarize the news from all the old digests in ONE digest. - that covers the gap between the old digests and the current one. (maybe even just the highlights) Faster for you. \n\nI don't think playing catchup is really worth it. You're just going to burn yourself out.\n\nWe'll be fine with a brief gap in the knowledge, but timely news might be better.\n\nWhat do you guys think?\n\n\n\n"
    author: "Mike"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "+1.\ndigest for the \"missing\" month (doesn't have to be that in depth) would be fine and then the current one.\n\nI highly appreciate Dany's work but a 1 month old digest doesn't have much information that hasn't already been covered by planetkde posts. :-("
    author: "KAMiKAZOW"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "I get latest news from planet.kde.org. Naturally, it seems awful odd to see a Commit Digest for 4.1b2 right AFTER 4.1final announcement, but if Danny would rather be complete than up-to-date, he has a right to. At the rate he is going, he'll catch up.\n"
    author: "Michael \"cmon\" Howell"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "Danny can do what he likes, but I can see the value in not jumping over a month or so of commits.\n\nFirst, the work done during the weeks that Danny was off is of equal value and interest. The developers who gave their time deserve recognition.\n\nSecond, the Digest is a historical recap of the development process. For interest's sake, look back a year or two or three. The highlights and commit logs show things that we take for granted now, and actually forget that someone sat down and spent hours writing and testing. And we can see how far the project has moved. It's amazing, and the fuss in the last while gets put in perspective.\n\nThird, when Danny took over the Digest, he pestered me for all the archives, some of which I had lost during the changes and rewrites and the weekly brain farts. Historical record is important to him, obviously. Good for us all.\n\nFourth, if having it every week up to date is so important, why not step up and be available and ready to fill in when Danny needs a break (which he does). It is a major endeavor to crank them out every week, and burnout is a real possibility.\n\nFifth, the only way to crank them out is to impose discipline on oneself, deciding something, then doing it. I wouldn't want to disrupt Danny's routine for my selfish pleasure, since it may disrupt the whole production.\n\nDerek"
    author: "Derek"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: ":-)\n\nI couldn't agree more, and feel mostly the same about this!\n\n\"Let the man do his job, instead of destroying the passion.\""
    author: "Diederik van der Boor"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "Feedback, suggestions and advice are not destroying the passion. "
    author: "ad"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "That depends on the matter and frequency these are delivered, I'd say. "
    author: "Andr\u00e9"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "It does actually. Danny is putting together this digest, and there's people hooting for him to put together a different one.\n\nThat kind of thing is draining on a site like this. I mean look at all the comments on the dot so far. Most of them are in this thread. There's even another thread exactly like this one. \n\nIt doesn't matter how long after the commits the digest comes out. If you want bleeding edge, read http://lists.kde.org/?l=kde-commits.\n\nDanny, we salute you. If there's anything others can do to help, I've got two threads full of volunteers here... "
    author: "Steve"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "Yes, please Danny: Just skip the old digests!\n\nPretty much the most important factor of the digest is its freshness; it really isn't very useful to read months-old digests.\n\n\nIt's comparable to reading a daily newspaper two weeks later..\n"
    author: "Mark Kretschmann"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "I think it is useful, actually. I find it interesting, and it takes away from the hype of the moment. What's more, after a freeze the digests should become less interesting, as less features go into the main branch. This should provide an opportunity to catch up rather quickly."
    author: "Andr\u00e9"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "What kind of newspaper are you reading? News just for the sake of news?"
    author: "Hans"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: "Not me. i like reading the digest as it is. Even better if it's old that way i could know what cool feature i have at my hand, not some feature that i cant played with now.\n\nKeep it going Danny and thank you for tde commit digest."
    author: "angun33"
  - subject: "Re: kewl"
    date: 2008-07-31
    body: ">Personally I am more interested in reading the latest releases, \n>not stale news. Do people go and buy a month old paper to read \n>the news? No. So give us the latest news please.news.\n\nWhats the diff? Really, how's it any different? Either way it's news to you if you haven't been religiously reading the planet, and if you want the /latest/ 'news' you could just read Planet.KDE.org and go straight to the sources.\n\n>How many people here would love for Danny to give us the latest \n>digests now and work on the ones he missed when he has time?\n\nIf he left off the date and the mention about 4.1 beta 2, would you have known the difference? Is it really that big of a deal? Which ever Danny wants to do he should do, if you don't like his choice you could always step up and help."
    author: "Kit"
  - subject: "Thanks man"
    date: 2008-07-31
    body: "You are actually catching up. I didn't think you would. Don't work too hard. ;)"
    author: "winter"
  - subject: "kmail..."
    date: 2008-07-31
    body: "...is going to be awesome.when these changes are going to be merged in trunk/ ?"
    author: "Emil Sedgh"
  - subject: "what's this 'grid' containment for plasma ?"
    date: 2008-07-31
    body: "anyone has more information about this 'grid' containment for plasma? is this part of kde 4.1 feature?"
    author: "angun33"
  - subject: "Re: what's this 'grid' containment for plasma ?"
    date: 2008-08-02
    body: "Many icon-based desktops (kdesktop, Window$) had the ability to align the icons to a grid. The grid containment aligns the plasmoids to a grid.\n"
    author: "Michael \"correct me if I'm wrong\" Howell"
  - subject: "I love this historic articles"
    date: 2008-07-31
    body: "This makes me remember of that great commit on october 1999, boy those were the days. I hope someone writes about them soon! Why do we restrain the commit digests to modern history?\n\n\n\n"
    author: "Martin Popser"
  - subject: "Re: I love this historic articles"
    date: 2008-07-31
    body: "Where can we read your version that *is* up to date?\n\n(Thanks Danny! I enjoy them!)"
    author: "Andr\u00e9"
  - subject: "Re: I love this historic articles"
    date: 2008-07-31
    body: "Heh, well said :)"
    author: "Mark Kretschmann"
  - subject: "Re: I love this historic articles"
    date: 2008-07-31
    body: "Was that your commit in october 1999? What, you have never commited anything?\nMartin, your armchair needs a new cover...\n\nDanny, please rock on!"
    author: "Hans"
  - subject: "Message List View"
    date: 2008-07-31
    body: "Will that also support a Gmail-esque view of all mails in a conversation/thread, without the tree-like structure and no need to click on each individual message to read it? That would definitely get me to use KMail! Looking good :)"
    author: "bsander"
  - subject: "Amazing"
    date: 2008-07-31
    body: "It's always amazing... Thanks for these infos for non-devs all the time. Great work Danny."
    author: "-Flo-"
  - subject: "Thanks!"
    date: 2008-08-02
    body: "Thanks again for the digest Danny!  I look forward to reading your next one!"
    author: "spawn57"
---
In <a href="http://commit-digest.org/issues/2008-06-22/">this week's KDE Commit-Digest</a>: Work on a "Grid" containment for Plasmoids. A <a href="http://plasma.kde.org/">Plasma</a> applet to monitor the WiFi signal strength (on Linux systems). Infrastructure in place for a network settings daemon in the NetworkManager Plasmoid. An <a href="http://pim.kde.org/akonadi/">Akonadi</a> Plasma data engine, intended for initial use by a "Plasmobiff" applet. "Previewer", a new Plasmoid for previewing files using KParts technology. KDevPlatform (the basis of KDevelop4) gets a plugin for basic Git source versioning control. Start of resurrecting C# support in <a href="http://www.kdevelop.org/">KDevelop</a>. Key bindings added to display various debugging information in <a href="http://edu.kde.org/kstars/">KStars</a>. Progress in sound and scripting support in <a href="http://edu.kde.org/parley/">Parley</a>. KTron gets a new maintainer, with work on porting to KDE 4 and moving to SVG graphics. "Find duplicates" tool in <a href="http://www.digikam.org/">Digikam</a> is now regarded as fully functional. Initial efforts at using PolicyKit for the management of system-wide fonts. Work on scrolling interaction in <a href="http://korganizer.kde.org/">KOrganizer</a>, and further development in the new "Message List View" of <a href="http://kontact.kde.org/kmail/">KMail</a>. Copying music tracks to an MP3Tunes locker, and experimental auto-fetching of album artwork in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Support for authentication algorithm selection for WEP connections in <a href="http://en.opensuse.org/Projects/KNetworkManager">KNetworkManager</a>. Early beginnings of scripting plugin for <a href="http://ktorrent.org/">KTorrent</a>. Initial support for searching for placemarks, and undo/redo support in the "GeoShape" of <a href="http://koffice.org/">KOffice</a>. Beginnings of work on the "Presenter View" in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>, with two synchronised canvasses. "Web forms" component of <a href="http://www.kexi-project.org/">Kexi</a> becomes based on the kde.org template, for consistency and accessibility reasons. Provisional .wav support in TagLib. Initial import of a Perldoc KIOSlave for KDE 4. Import of a KDE 4 port of sysinfo:/ (as seen in openSUSE 11). <a href="http://dot.kde.org/1214311382/">KDE 4.1 Beta 2</a> is tagged for release. <a href="http://commit-digest.org/issues/2008-06-22/">Read the rest of the Digest here</a>.

<!--break-->
