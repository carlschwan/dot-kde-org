---
title: "KDE at LinuxDay 2008 in Dornbirn, Austria"
date:    2008-12-09
authors:
  - "skeferb\u00f6ck"
slug:    kde-linuxday-2008-dornbirn-austria
comments:
  - subject: "Bling vs substance"
    date: 2008-12-10
    body: "That's great work, Stephanie and Franz.\n\nYou mention \"showing off some ... bling\".  While I'm sure in your case this is probably just a glib writeup of the content, I'm concerned that we need to do more to show off the substance of KDE 4 as well as the easily-demonstrated visual aspects of Plasma.\n\nA section of the userbase which has, until now, been avoiding KDE 4 until it matures, is skeptical of graphical polish and looking for material benefits KDE 4 has that KDE 3 does not.  We had a large thread about this on the openSUSE mailing list recently. And the GNOMErs find it tempting to hold up KDE 4 as style over substance too, even if they subsequently realise that this is puerile.\n\nIt would be great to broaden our message (oh no, say the MWG) to spotlight the productivity features as much as the 'bling'."
    author: "Will Stephenson"
  - subject: "Re: Bling vs substance"
    date: 2008-12-10
    body: "Additionally users should not only try KDE4 because of the phantastic bling, but first and foremost because it is built using the best buildsystem in the world ;-)\n\nSeriously, Will, I agree with what you say, and I had the same feeling when reading the announcement. I'm also not sure (as non-native speaker) that \"bling\" is actually a word with a positive meaning (http://dict.leo.org/?search=bling)\n\nAlex\n\n"
    author: "alex"
  - subject: "Re: Bling vs substance"
    date: 2008-12-10
    body: "From weblink \"auffaelliger Modeschmuck\" \n\nI've got no idea what it actually means, but it sounds like a good definition for \"bling\""
    author: "NWA"
  - subject: "Re: Bling vs substance"
    date: 2008-12-11
    body: "Can some native speaker please confirm that \"bling\" is really positive and not too much slang ?\nE.g. in german we have \"Klunker\", which may be similar, and which doesn't sound too positive (e.g. can imply \"too much\" or \"cheap looking\" or \"pretentious\").\n\nAlex\n"
    author: "alex"
  - subject: "Re: Bling vs substance"
    date: 2008-12-11
    body: "No, \"bling\" generally means an unsubtle display of wealth via jewellery.  As I understand it, \"bling\" has been used ironically in a KDE context to indicate that it's the mainstream desktop with the most modern shell.  For a wholly unironic discussion of the term, see http://en.wikipedia.org/wiki/Bling-bling."
    author: "Will Stephenson"
  - subject: "Re: Bling vs substance"
    date: 2008-12-10
    body: "I agree with your point that KDE4 does indeed have depth as well as substance.\n\nBut what many people might not know is that the graphic effects of Plasma are a result of its architecture. The 'bling' is not something that can be bolted on as an afterthought as the name suggests. \n\nThe visual look of Plasma could only be created by designing it to use the QGraphicsView classes, and pushing them to the limit. I hope the Plasma team can go even further when they use the 'Kinetic' features that will be in a future release of Qt. Now we have the Plasma classes in kdelibs, every KDE application is free to use them, not just applets. That is most certainly not a minor 'bling' thing.\n\nThis wouldn't be possible with Qt3 or the current GTK+ toolkit in Gnome. Neither is it possible to do this kind of work without having an integrated team of artists and developers. So yes, there are many other important things in the architecture of KDE4, but we are selling ourselves short if we are forced to cave in to people who just can't see the importance the architecture behind a dynamic state of the art UI like Plasma or the one in Mac OS X."
    author: "Richard Dale"
  - subject: "Re: Bling vs substance"
    date: 2008-12-10
    body: "Hey Will!\n\nIn fact, using the term \"bling\" here does not only apply to KDE4's visual appearance yet also what it \"shines\" with under the hood!\n\nSimple \"features\" like no-arts-blocking-your-sound-card, polished Applications and configuration dialogs (i love to show off gwenview here, i used to do a v3-v4 comparison but v3 isn't installed on my system any more), notification-integration (update notifications are shown with KDE4.2's new notification system), ...\n\nThere's a lot more \"bling\" in KDE4 than the surface, and it has been shown at the booth as well as in the talk. I fully agree with you that it's that substance that makes KDE4 so different and will prove it being way more useful as well as powerful than earlier versions.\n\nYet again, most people got caught by a real-transparent panel ;-)"
    author: "Franz Keferb\u00f6ck"
  - subject: "Re: Bling vs substance"
    date: 2008-12-14
    body: "> There's a lot more \"bling\" in KDE4 than the surface\n\nAfter reading the link Will posted, http://en.wikipedia.org/wiki/Bling-bling , it really seems \"bling\" is about the surface, so we shouldn't be surprised if some people get the impression KDE4 would be mainly about looking nice.\n\nAlex\n"
    author: "Alex"
  - subject: "there..."
    date: 2008-12-10
    body: "wasn't much happening at the kde stand at dornbirn. i was there the whole day. the artivle is a little bit (seems to be kde style) overexagerated! This is too bad and not really the thing you expect from a big project like kde."
    author: "someone"
  - subject: "Re: there..."
    date: 2008-12-11
    body: "There has been two people answering questions and showing the System - except for the time of the presentation.\n\nThere's been people addressed when walking by and questions answered. I do not quite know what else you expect??? (I didn't quite feel like jumping and shouting, i got up at 2am in the morning to drive all night to get there, and all night back; The printed \"poster\" ended up unusable as my printer ran out of blue ink; If you can tell me what else to do, please do so and join in!)"
    author: "Franz Keferb\u00f6ck"
  - subject: "Re: there..."
    date: 2008-12-12
    body: "sorry, but, i noticed, that there weren't too many people interested in (your) kde (stand). as i said, the whole article is really a bit like \"hey look, we succeeded in showing people kde\", but as a matter of fact, it didn't end like this."
    author: "someone"
  - subject: "Presentation"
    date: 2008-12-11
    body: "Too bad that I couldn't be there\nbut I am sure Franz did a perfect presentation\nsimilar to the one in Linz"
    author: "Me"
  - subject: "Re: Presentation"
    date: 2008-12-12
    body: "not really!"
    author: "ouch"
---
On Saturday 29th of November 2008 <a href="http://www.lugv.eu/">LinuxDay</a> was held for the 10th time already in Dornbirn, Austria. Organised by the <a href="http://www.lugv.at/">LUG Vorarlberg</a> in cooperation with <a href="http://www.htldornbirn.at">HTL Dornbirn</a>, the well-received event is a platform for open source projects to answer questions and show off their latest and greatest versions to a broad public, but also for the students at the HTL Dornbirn to show what they achieved and created by using software libre. KDE was there to show off KDE 4.



<!--break-->
<a href="http://static.kdenews.org/jr/dornbirn-talk.jpg"><img src="http://static.kdenews.org/jr/dornbirn-talk-small.jpg" style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right" /></a>

<p>The latter made it extra-important for the highly active KDE promo team to be there and show support for these integration efforts and take the chance to help recent KDE 4 switchers with their migration problems. Besides a booth staffed by Stephanie Pfleger and Franz Keferböck, an interactive talk was given answering standard questions, explaining basic design goals of KDE 4, showing off some of the future KDE 4.2 bling and creating some "wow"s from the audience in the packed room ensured a pervasive KDE presence.</p>

