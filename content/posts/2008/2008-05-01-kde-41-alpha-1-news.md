---
title: "KDE 4.1 Alpha 1 in the News"
date:    2008-05-01
authors:
  - "tunrau"
slug:    kde-41-alpha-1-news
comments:
  - subject: "There has got to be a better way...."
    date: 2008-05-01
    body: "There has got to be a better, more efficient way to draw shadows around windows than to put a shadow behind the whole window. Compiz, doesn't do this and thus when you wobble windows, you don't see the shadow layer lagging behind the top layer. Now in compiz, the window decorator is a separate process, does this make a differenece?"
    author: "Joe"
  - subject: "Re: There has got to be a better way...."
    date: 2008-05-01
    body: "Hi !\n\nI have write a bug report for this :\nhttp://bugs.kde.org/show_bug.cgi?id=161330\n\nSeems that you have add a comment to this report  \n\nI don't know the architecture of kwin but perhaps that drawing shadows around windows is in fact an optimisation ? More easy to just compute a rectangular area, and perhaps when the area is behind something else there is not any draws made for it ?   "
    author: "okparanoid"
  - subject: "Re: There has got to be a better way...."
    date: 2008-05-01
    body: "I like it more than I liked the compiz one.\n\nI hope it stays as is."
    author: "JackieBrown"
  - subject: "great"
    date: 2008-05-01
    body: "KDE alpha1 looks amazing. When will kubuntu get packages? Most KDE centric distributions have them already :/"
    author: "cool"
  - subject: "Re: great"
    date: 2008-05-01
    body: "Kubuntu is no distribution, the distribution is called Ubuntu. And Ubuntu is not KDE centric.\n\nI'm really not very satisfied with Kubuntu lately, the Hardy Heron KDE 4 flavour is just a bad joke: vanilla KDE, feels like compiling the packages yourself (besides many plasmoids and most of the KDE 4 wallpapers are missing in Kubuntu). I expect more from a Linux distribution.\nThen this awful kindergarten wallpaper for Hardy while those GNOME guys get such an ubercool one...\n\nI'm really thinking about switching to a more KDE centric distribution lately."
    author: "fhd"
  - subject: "Re: great"
    date: 2008-05-02
    body: "I'm not satisfied with Kubuntu, but credit where it's due: the wallpapers are not missing, they're in a different package, called \"kdebase-workspace-wallpapers\" (probably to cut down the size of kdebase)."
    author: "Luca Beltrame"
  - subject: "Re: great"
    date: 2008-05-02
    body: "Oh, so that's where they got off to...\nThanks for the tip!"
    author: "Jonathan Thomas"
  - subject: "Re: great"
    date: 2008-05-02
    body: "But why the hell are they not installed with kubuntu-kde4-desktop?? What's that as a first impression?"
    author: "Donovan"
  - subject: "Kubuntu has 702 votes currently - Please vote"
    date: 2008-05-03
    body: "Kubuntu has 702 Votes currently. \n\n\nPlease go out and vote for it."
    author: "D"
  - subject: "Re: great"
    date: 2008-05-02
    body: "I'm personally impressed with what OpenSUSE has done with KDE 4 in the 11 beta 1\n\nhttp://en.opensuse.org/Screenshots/openSUSE_11.0_Beta1\n\nI was not impressed with kubuntu, it really just doesn't look very professional"
    author: "anon"
  - subject: "Re: great"
    date: 2008-05-02
    body: "OMG, this rocks. I should really think about switching to SuSE... their KDE 3.5.x desktop looks a hundred times more professional than Kubuntu's too.\n\nSad openSuSE only ships KDE 3.5.7... and sad they've become GNOME centric too.\n\nBTW, the wallpapers thing:\nI filled a bug about this yesterday. Because there is a package called kdewallpapers-kde4 which is a dependency of kdeartwork-kde4. This package installs some ugly nonsense KDE <= 3 wallpapers instead of the KDE 4 ones. This can't be!\nI guess kdebase-workspace-wallpapers is some relic from the alpha and beta packages. Kubuntu revised their package names often enough.\nIt just can't be, this is supposed to be STABLE?!"
    author: "fhd"
  - subject: "Re: great"
    date: 2008-05-02
    body: "OpenSUSE is not Gnome centric.  Opensuse has the biggest KDE team amongst all the distro's.  If anything they are more KDE centric than Gnome. "
    author: "anon"
  - subject: "Re: great"
    date: 2008-05-02
    body: "In addition, online surveys show that a whopping 72% of users use KDE.  Becoming \"GNOME-centric\" in the face of this would be insane."
    author: "Anon"
  - subject: "Re: great"
    date: 2008-05-02
    body: "Online surveys are not even remotely reliable, and different surveys show conflicting results.  Don't quote them in public."
    author: "Leo S"
  - subject: "Re: great"
    date: 2008-05-02
    body: "47% of all statistics are made up on the spot."
    author: "T. J. Brumfield"
  - subject: "Re: great"
    date: 2008-05-02
    body: "See I've read somewhere that 42.3% of all statistics are made up on the spot. :p\n\nYou can't trust these online statistics."
    author: "M"
  - subject: "Re: great"
    date: 2008-05-03
    body: "actually it's not a survey, the figures come from those accessing the opensuse repositories, which have 72% of people accessing them using KDE, and the remainder using Gnome."
    author: "anon"
  - subject: "Re: great"
    date: 2008-05-02
    body: "I forgot to ad, that opensuse ship with the stable KDE version at the time of release, but you will find the KDE updates hit the opensuse repositories quicker than any other distro.\n\napt-get is nice,  Yast  is improving constantly "
    author: "anon"
  - subject: "Re: great"
    date: 2008-05-02
    body: "you can easily add suse buildservice repositories, and have latest versions of almost everything, including kde.\n\nas for opensuse being gnome centric, well, while it has somewhat departed from kde-centric roots, i don't feel that they are leaving out kde or something."
    author: "richlv"
  - subject: "Re: great"
    date: 2008-05-02
    body: "I'm thinking about that too. I just like apt-get so much that to this point I tolerated the generally bad kde4 performance of kubuntu.\n\nBut seeing how e.g. OpenSuse rolls things with KDE 4, polishes it and constantly gives updates, I might switch over the next weeks.\n\nI heard impresseions like this from a lot of people. So if Kubuntu continues to frustrate users with KDE 4 they'll be in big trouble. And I know it's not the fault of JRiddell, he's all alone on this after all."
    author: "Donovan"
  - subject: "Re: great"
    date: 2008-05-02
    body: "Same here... Apt-Get is THE reason I use Kubuntu.\n\nSo much easier than dealing with package dependencies with openSUSE (although it got much better)\n\nPLEASE, Please put some effort into Kubuntu.\n"
    author: "M"
  - subject: "Re: great"
    date: 2008-05-03
    body: "It isn't up to KDE to put effort into Kubuntu.  It is up to Cannonical to put effort into Kubuntu.\n\nopenSUSE has been making some great KDE packages and they're are starting to win me over these days.\n\nArch Linux has great KDE 3.x packages availble with the KDE-mod repository, and Sabayon uses those patches as well."
    author: "T. J. Brumfield"
  - subject: "Re: great"
    date: 2008-05-03
    body: "If you like apt-get, then you will love what openSUSE 11 has done to software management."
    author: "anon"
  - subject: "Re: great"
    date: 2008-05-02
    body: "\"Kubuntu is no distribution, the distribution is called Ubuntu. And Ubuntu is not KDE centric.\"\n\nI'm sorry, but that sentence makes no sense at all.\n\nIf you don't like Kubuntu, then by all means, switch to something else or help improve it."
    author: "Joergen Ramskov"
  - subject: "Re: great"
    date: 2008-05-02
    body: "It was meant as an answer to this sentence:\n\"When will kubuntu get packages? Most KDE centric distributions have them already :/\"\n\nIt implies that Kubuntu is a KDE centric distribution.\n\nWith my answer, I meant that Kubuntu is not a distribution, but a flavour of a distribution called \"Ubuntu\". And the distribution Ubuntu is not KDE centric.\nWith this answer, I was emphasizing my frustration about KDE in Ubuntu not getting the love it deserves.\n\nI don't have time to really help improve it. I report the bugs I find and I critisize it (trying to be constructive of course, but shit happens) - that's it for now. \nAnd my critisizing happens today, on the dot, in the post you just quoted.\n\nThat's my way to help Kubuntu - nothing will change if nobody complains.\nAnd while complaining, I'm asking for alternatives with better KDE4 implementations. Can this be wrong?"
    author: "fhd"
  - subject: "Re: great"
    date: 2008-05-02
    body: "Trust me, the Kubuntu team wants KDE 4.1 as much as you do. But it's not as simple as that, as Hardy Heron was just released, and KDE 4.1 will be released 3 months from now.\n\nAs of now, there has been absolutely no time to do any migration to KDE 4.1. This is in fact a huge job, and not one that could have been sanely accomplished in the time between now and hardy's release. You have Qt 4.4 that you have to migrate to, and then package. You have to write migration scripts for configuration files, and you have to do this without bugs. Then, you also have to package KDE 4.1. The Ubuntu developer's summit where this is all going to be concretely fleshed out hasn't even taken place yet.\n\nThe main problem is with the timings of the releases of *buntu and KDE. All of the above hurdles are related to the timings of Kubuntu and KDE releases. If you sync Kubuntu to KDE, people will complain about getting Kubuntu 3 months late. If you don't, and keep releases synced with Ubuntu, people will complain about not getting prerelease versions of KDE 4.1 as early as the other distributions. Trust me, the last thing that the Kubuntu team needs is people complaining, since they'll have people complaining about any choice that they make."
    author: "Jonathan Thomas"
  - subject: "Re: great"
    date: 2008-05-02
    body: "Should be: \"about choices they make either way.\" at the end there, heh."
    author: "Jonathan Thomas"
  - subject: "Re: great"
    date: 2008-05-07
    body: "\n I don't think Kubuntu should by any case switch to kde 4.0.x or 4.1 till all impressions has been taken. Apparently, kde 4.x is a big mis, which takes me to freeze to 3.5.x or to switch to gnome, which i already did at my home fedora distro. At work still using kde because i used to it (since '95) and i did lot under qt. So Kubuntu should stay focused on the stability prior to some polish shell of the empty, and more sadly, unusable content.\n Even Akademy needs to extend dead line because they will not have enough participants this year. Kde team should consider this or they will need to extend developments of 3.5.x serials, like MS did to the XP winning still over vista.\n It is a bid sad, because qt 4.x is a really good, and really misused in kde 4.x. "
    author: "xddule"
  - subject: "Re: great - Sadly, I agree: Kubuntu is a joke!!!"
    date: 2008-05-02
    body: "Sadly, I have to agree. Kubuntu 8.04 KDE Remix is a joke.\n\nUsually Kubuntu is a half a$$ed version of Ubuntu. (don't they have only like 1-2 people working on Kubuntu??)\n\nThis time it takes the cake!!! It feels so unfinished, almost Beta version.\n\nCanonical, please put some effort into the KDE branch. I understand that Ubuntu is your big \"seller\" but Linux is about choice!!\n\n.\nYes, I know I can defect to openSuSE, and probably will, but without some friendly competition between the distro's development will starve. More distros need to embrace KDE."
    author: "M"
  - subject: "Re: great"
    date: 2008-05-02
    body: "Well, on distrowatch.org, at the time being ubuntu is the number 1 distro, while kubuntu is number 14, while is OpenSUSE is number 3, supporting multiple desktop environments very nicely.\n\nI think Ubuntu with kde4 lacks big time compared to openSUSE. I even tried switching to openSUSE, but the complex/sluggish installer, hardware support and perhaps no shipit blown me away xD\n\nI really wish Ubuntu/Kubuntu/Xubuntu/Shumuntu/etcbuntu will \"merge\" in the way openSUSE does things...\n\n\nI stumbled myself in k/ubuntu kde4 with the wallpapers package issue xD it should get fixed..."
    author: "Dread Knight"
  - subject: "Re: great"
    date: 2008-05-03
    body: "slugish installer on opensuse?\n\nOn 10.3 it took me 20 minutes to install opensuse\n\non opensuse 11 beta 2 KDE 4 cd it took me 10 minutes."
    author: "anon"
  - subject: "Vote for Kubuntu!!!! :)"
    date: 2008-05-03
    body: "If you want Kubuntu to have the same rights as Ubuntu, vote here:\nhttp://brainstorm.ubuntu.com/idea/478/"
    author: "M"
  - subject: "OpenSUSE Screenshots"
    date: 2008-05-04
    body: "I've uploaded some more detail screenshots of OpenSUSE 11 beta 2 if people want to see the improvements to it.  \n\nhttp://www.myspace.com/alienwithin"
    author: "R. J."
  - subject: "w00t!"
    date: 2008-05-01
    body: "Tabs in Dolphin! Omg, that's terrific news. Best of all, I didn't expect that feature at all. I always thought Dolphin was a nice file manager but that I can't migrate from Konqueror because I can't miss its tabs. And now this!\n\nOn the other hand, I could really have expected tabs in Dolphin when I saw that Peter Penz worked on close buttons for the tab widget. So much for my deductive skills x-)"
    author: "Jakob Petsovits"
  - subject: "interesting"
    date: 2008-05-01
    body: "Good article, but when you look at the screen shots you can see how far ahead opensuse is with tweaking and polishing 4.1.  Just look at the screenshots for 11 beta 1.  I guess to be expected considering the large KDE team they have.  But thanks for the article."
    author: "anon"
  - subject: "Re: interesting"
    date: 2008-05-01
    body: "openSUSE will release a heavily modified version of 4.0 as 4.1 is scheduled to be released after openSUSE 11.0."
    author: "Erunno"
  - subject: "Where is the desktop"
    date: 2008-05-02
    body: "Where is the desktop? the ability to create folders, files. Where are the useful options of the right click of a mouse? Widgets should only be one more option on the menu of these options click Direct. KDE 4 ta should do this, I think you forgot essences. Do what is useful and enjoyable. This form kde 4 vai follow the same path of Windows Vista...\nI hope that these technologies are doorways in the future!"
    author: "Emanuell_BR"
  - subject: "Re: Where is the desktop"
    date: 2008-05-02
    body: "That's what the folderview widget is for."
    author: "Jonathan Thomas"
  - subject: "Re: Where is the desktop"
    date: 2008-05-02
    body: "The dependence of some UIs (XP and to a lesser degree KDE) on \"right clicks\" is a major annoyance for me if I use my laptop's touchpad. So I hope KDE will not make extensive use of this feature.        "
    author: "Ego"
  - subject: "Re: Where is the desktop"
    date: 2008-05-02
    body: "Your touchpad doesn't have right and left buttons? Mine even emulates the wheel. The single button interface died with the single tasking pre OS X Mac. Without right clicks how do you have a context menu? Having recently added context menus to Kommander I experieced first hand the vastly improved potential to design a rich and responsive user interface with far less clutter. It's beyond absurd that KDE developers should trash a rich interface and emulate Xerox Parc because you don't like your touchpad. I was never fully comfortable with my touchpad so I got a small USB laptop mouse. Problem solved."
    author: "Eric Laffoon"
  - subject: "Re: Where is the desktop"
    date: 2008-05-02
    body: "It seems that most people get USB mouses for their laptops and some even use a \"real pc keyboard\" while at their home desk etc. =)"
    author: "Dread Knight"
  - subject: "Re: Where is the desktop"
    date: 2008-05-02
    body: "I prefer to use tap-to-click (instead of the left button), tap-and-drag and mouse wheel emulation. This allows me to stay with one finger on the pad all the time and is very convinient as long as I do not have to use the context menu. I do not propose to trash an existing UI (XP or KDE3), but, as you may have noticed, the evolving Plasma in it's current state does not make extensive use of this feature, and I hope this is a design decision. I really  love my touchpad and I think it is even more ergonomic than the mouse. \n      "
    author: "Ego"
  - subject: "Re: Where is the desktop"
    date: 2008-05-05
    body: "\"The single button interface died with the single tasking pre OS X Mac\"\n\nIIRC, one of the design-guidelines of OS X and related apps is that they should not rely on context-menus. After all, all macs still ship with single-button mouse. Yes, Mighty Mouse and trackpads can act as a multi-button mouse, but at their core they are still single-button devices.\n\n\"Without right clicks how do you have a context menu?\"\n\nHow about thinking of ways to NOT rely on context-menus? Context-menus seem to be a trash-heap of functionality that the developers could not fit elsewhere.  The app should be perfectly usable without any context-menus at all.\n\nIMO, it might do some good to remove the right mouse-button from the developers mouse, that way he would be forced to think of alternative means of doing things, instead of relying on context-menus.\n\nYou mention that context-menus allow you to design interfaces with less clutter? Does that mean that the context-menu is the only way to access certain functionality in your app?"
    author: "Janne"
  - subject: "Re: Where is the desktop"
    date: 2008-05-08
    body: "Dude, typing is away faster than any mouse positioning, so the context menu is the essential for any constructor, engineer, doctor, programmer, and many more professions. This way i could choose what i need, in the right application i could even configure my context menu, which suits my needs. And i don't even bother to use mouse for it. Because there is meta-key which pops context menu, and intuitive is associated with the right mouse button, even if I'm left hander, because my meta key is on the right side of the keyboard. The mac users also have they 'apple' keys, even old amiga has the context menu button.\n\nThe plasma is killing environment for every usable desktop. The first poster has very right to point it.   "
    author: "xddule"
  - subject: "Kubuntu"
    date: 2008-05-03
    body: "I am using the latest Kubuntu and I am experiencing still very old and odd quality problems:\n\n- icon set switch gets you uber-big icons in the menu that clutter it (why doesn't the software check it and convert it??)\n- Systemsettings with a non-visible ok button of the keyboard/mouse module, you need to use the scroll bars, who designed that shit?\n- with dolphin in the trash the default option is not to empty it. It is impossible to delete files in the trash bin window\n- translation: incomplete and with invalid line-breaks in the system settings selection dialogue, looks like shit.\n- Kubuntu installs the ugly Orage calendar tool that pops up on every start-up\n- some keys of my laptop keyboard don't work anymore and I have no idea how to switch the keyboard, probably a GRUB problem.\n- folder system has an entry for printing and another one for printing. One is Gnome.\n- at least 5 errors during system upgrade\nwith these oddities in the kubuntu standard installation I don't really want to test Kde 4...\n- Kde 3.5.9 file selection dialogue **still** jumps over entries when you scroll via keyboard. The bug is there still since at least Kde 3.0.\n\n"
    author: "Andre"
  - subject: "Re: Kubuntu"
    date: 2008-05-06
    body: "Then go fix it, mate!"
    author: "litb"
---
Ryan Paul over at Ars Technica is at it again, this time with an <a href="http://arstechnica.com/news.ars/post/20080430-first-look-kde-4-1-alpha-1-very-promising.html">early review</a> of KDE 4.1 <a href="http://www.kde.org/announcements/announce-4.1-alpha1.php">Alpha 1</a>.  He writes, "<em>This alpha release marks the start of the 4.1 feature freeze, so virtually all of the remaining developer effort between now and the official 4.1 release in July will focus on bug-fixing, polish, and stability.</em>"  Features that are listed as <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">planned for 4.1</a> can still be implemented for 4.1.  Next, our friends at Polish Linux are at it once again, bringing us a <a href="http://polishlinux.org/kde/kde-4-rev-802150-work-in-progress">visual review of KDE 4.1</a> of revision 802150, which roughly corresponds to Alpha 1. Buried in the article, I found this interesting bit of news, "<em>Kwin features a new visual effect known to most of you from Compiz: the Wobbly Windows.</em>"

<!--break-->
