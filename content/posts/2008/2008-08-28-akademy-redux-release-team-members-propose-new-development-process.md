---
title: "Akademy Redux: Release Team Members Propose New Development Process"
date:    2008-08-28
authors:
  - "jpoortvliet"
slug:    akademy-redux-release-team-members-propose-new-development-process
comments:
  - subject: "Discipline..."
    date: 2008-08-28
    body: "Well, this frightens me: \"relying on discipline in the community to get everyone on the same page and focus on stability\", since it's given as the single solution for the problem that quite a lot of people prefer to hack on new stuff they never finish but want to commit anyway. And looking at the disaster that befell KOffice last week, when suddenly KDE trunk started using an entirely different way of linking that completely broke all of KOffice, I doubt there is enough discipline to go around.\n\nAnd then git... If I look at what happens in the linux kernel development community, I see the best developers being permanently busy with merging, merging, merging and merging and very little else. And, actually, a scheme where we only seldom integrate, where every developer at one, infrequent, moment publishes their branch for merging sounds like a recipe for integration hell, no matter how developer-friendly and good at branching and merging a tool might be."
    author: "boudewijn rempt"
  - subject: "Re: Discipline..."
    date: 2008-08-28
    body: "Certainly true. The \"discipline\" one is also one of my biggest issues. On the one hand, that would be alleviated by an allegedly less broken trunk, and on the other hand, with better tools, it should be easier to switch to that branch (I'm assuming here that developer reluctance to test release branches has to do with SVN making it hard to switch to a different branch quickly, not from unwillingness). Usually, I see developers taking great pride in their code and want to make sure it looks good when released. Making the rules to merge it stricter would then use the \"you want it merged, then first make it work well\" mechanism, rather than the \"you're on that branch now, either feel the pain of switching branch or fix bugs\" mechanism. The first feels much stronger to me.\n\nI guess the real questions is \"Why do we have bugs in trunk at all?\", and we should work on reducing that.\n\nThe Linux kernel development process is not very similar to KDE's of course. The overhead of what's going in is higher, it's quite formal and bound to specific persons that decide it all. KDE is quite different in that it's team-based, so potentially making it easier to share code in those teams and merge it upstream as it becomes interesting for others until finally bugs have been shaken out and it's ready to go into trunk."
    author: "sebas"
  - subject: "Re: Discipline..."
    date: 2008-08-28
    body: ">I'm assuming here that developer reluctance to test release branches has to do with SVN making it hard to switch to a different branch quickly, not from unwillingness\n\nI wouldn't come to that conclusion. There are substantial benefits that come automatically from being in trunk that will not happen if branched.\n\nThere are a few things in free software that are hard and rare. Development isn't one of them. Testing and code review are hard and rare. Trunk provides automatic access to the rare resources. Limited, but as good as it's going to get. Branches force the developer to build those resources, skills that some may have, some may not. Or more likely, do without.\n\nThat doesn't solve the issues at hand however.\n\nWhat about release in retrospect? Currently development is halted at freeze, which makes out of tree development very enticing, or even necessary. What if each module decided in retrospect the release point? New or major reworks that break trunk could be done out of tree (and having tools that support this would help), and merged at the point where testing and review are desired.\n\nThe kernel is different, but testing and review hasn't happened in the spun off branches. One of the problems they face is untested code showing up in merges. Testing and review happens in trunk.\n\nDifferent targets require their own tree, and having a tool that supports that well would solve the problem.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Discipline..."
    date: 2008-08-28
    body: "Yup, that's one of the points. We need better tools to support more diverse needs and practices. A more staged development process (such as we tried to describe) would be one step towards making this possible."
    author: "sebas"
  - subject: "Re: Discipline..."
    date: 2008-08-28
    body: "> >I'm assuming here that developer reluctance to test release branches has to do with SVN making it hard to switch to a different branch quickly, not from unwillingness\n\nWell honestly, at least for me, it's not switching branches which I find takes time, or going back to a previous revision, since I often hear \"ah with git it is fast\", for me it's often 99% of build time for 1% of checkout... With git, darcs, cvs, svn, it will allways be the build time that will makes switch branches, going back to a revision, a pain. Not saying that optimizing the 1% isn't good ;) just that this is not the real main reason for me."
    author: "Cyrille Berger"
  - subject: "Re: Discipline..."
    date: 2008-08-29
    body: "I just end up with having multiple entire source + build trees of KDE for this very reason, and for the stable version I don't update outside my work area much, so I can actually build & test a backport in a sane amount of time. A DVCS or such would not do anything to help.\n\nWhere things like git do help (from my experiments with git-svn) is in managing in-progress work that's not quite ready for commit, so I don't end up with 50 different patches lying around. Although even there, while branches have low computational load, they still have a high cognitive one..."
    author: "SadEagle"
  - subject: "Re: Discipline..."
    date: 2008-08-29
    body: "True. I think the build times are affected quite differently actually.\n\nAlso, using git is only part of the picture here. A way to tackle this problem \"switching is expensive\" within our possibilities is to make it easier to switch to one of those branches. That means documentation, knowing which branch your colleagues are working on, having a good back- and forwardport tool and workflow. And with that goes that we need the right tools to make this cumbersome process as easy as possible.\n\nSwitching to a branch at the right point in time actually saves you compilation time as those branches have less and smaller changes compared to the freeze state. Staging the development process also has this effect, less experimenting means fewer / shorter recompiles."
    author: "sebas"
  - subject: "Re: Discipline..."
    date: 2008-08-29
    body: "May be it's a silly thing, but what about stting up an infraesrtucture of distributed compilation as Icecream for KDE developpers? At least for the more common source tree."
    author: "Git"
  - subject: "Re: Discipline..."
    date: 2008-08-29
    body: "distcc instances that community could donate to developers ? :)"
    author: "richlv"
  - subject: "Re: Discipline..."
    date: 2008-08-29
    body: "Distributed compilation doesn't work over the Internet.\n\nYou need a local farm, in your LAN, for that to work. The preprocessed C++ source file is often in the order of several megabytes in size. Uploading that to a machine on the Internet would take too long for most people.\n\nNot to mention, of course, that allowing someone to upload a compiler to your machine and run it is an enourmous security risk. I'd never place my machine in a compile farm whose users I didn't trust."
    author: "Thiago Macieira"
  - subject: "Re: Discipline..."
    date: 2008-08-30
    body: "Using git you can make the tool do what you want without too much effort, its very flexible. \nI set up a workflow for myself using git-svn for koffice where I always work on a branch.  And thus my workflow is geared towards making the compile time as easy for me as possible.\nThe workflow I have seems to be successfull, I compile much less than I did when I was using svn only, and I only have one tree I'm actually compiling in.\n\nI can explain the workflow in another forum, if you want, but I feel confident in saying that in general your worries can to a large extend be managed quite successfully with git."
    author: "Thomas Zander"
  - subject: "Re: Discipline..."
    date: 2008-08-29
    body: "Regarding \"less broken trunk\": in my experience starting from early 3.0 alpha cycle trunk has always been stable enough for everyday use except during pre-4.0 development. So I am not sure it's a real problem.\n"
    author: "SadEagle"
  - subject: "Great idea! But who is going to be your Linus?"
    date: 2008-08-28
    body: "At first I thought this will be like the kernel development, then I thought it was different, but it really isn't.\n\nLinus tries to have his tree in good shape all the time. But his tree would correspond to the KDE release trees. And linux-next = kde-next. \nAnd the various parts of the kernel have maintainers too. \n\nOnly question is who is going to be KDE DVCS benevolent dictator for a period of time (KDEDVCSBDFAPOT) ?? Will he/she be elected?\n\nDamn, it would really help if you would release the Akademy videos or did the dog ate them? Maybe watching Dirk and Sebas would answer my questions."
    author: "Tom"
  - subject: "Re: Great idea! But who is going to be your Linus?"
    date: 2008-08-28
    body: "No benevolent dictator, just more strict rules and better review to get your code into trunk. Review by the community and people who care (but you should try harder to find someone to review it, not just hope that no one finds issues within two weeks -- which also could mean \"make it hard to review and it's more likely to go in\" -- bad idea.\n\nVery interesting WRT review and vcs tools is Ben's blog about it: http://benjamin-meyer.blogspot.com/2008/08/code-review-should-be-free.html\n\nIt confirms that our current review process is about as inefficient as it could possibly be."
    author: "sebas"
  - subject: "Re: Great idea! But who is going to be your Linus?"
    date: 2008-08-29
    body: "We may be using a DVCS: I for sure look forward to easily being able to pull in changes from another Amarok developer that they don't think is ready for trunk.\n\nHowever KDE is essentially sticking to the centralized model."
    author: "Ian Monroe"
  - subject: "Developers from EVERY continent?"
    date: 2008-08-28
    body: "Who was the Antarctic contingent?  ;-)"
    author: "Ed Cates"
  - subject: "Re: Developers from EVERY continent?"
    date: 2008-08-28
    body: "We're working on that one, actually. :-)"
    author: "sebas"
  - subject: "Re: Developers from EVERY continent?"
    date: 2008-08-28
    body: "Not a dev, but a KDE user was in Antarctica for the 2005 season :-)\n\nhttp://www.fsf1532.com/image/kde_everywhere.png  (435Kb)\n"
    author: "Liz"
  - subject: "Re: Developers from EVERY continent?"
    date: 2008-08-28
    body: "Probably the penguins."
    author: "zonk"
  - subject: "Re: Developers from EVERY continent?"
    date: 2008-08-29
    body: "LIARS!!!! LIESLIESLIES!!!! LIES!!!! LIES! Lies! lies! LIES. Lies. lies.\n\nI don't think there was a developer from Antarctica.\n"
    author: "Michael \"Antarctica\" Howell"
  - subject: "Git vs others"
    date: 2008-08-28
    body: "This is just concerning tools, so it's very basic. And it's just for information, not to start a flame war of any kind.\n\nThis is a comparison of git, mercurial and bazaar and there are some nice piecharts there that shows how many languages are being used and how much.\nhttp://www.infoq.com/articles/dvcs-guide\n\nMercurial uses 4% Lisp (!) for example.\n\nSo from a multi platform kind of view I guess Bazaar is the stuff to use, but that's just my guess."
    author: "Peppe Bergqvist"
  - subject: "Re: Git vs others"
    date: 2008-08-28
    body: "I've tried Darcs. It's not bad. Could be something to have a look at.\nhttp://darcs.net/"
    author: "Oscar"
  - subject: "Re: Git vs others"
    date: 2008-08-28
    body: "bzr is too slow!"
    author: "kajak"
  - subject: "Re: Git vs others"
    date: 2008-08-28
    body: "Of course, a troll with no arguments at all..\nAnd what is \"too slow\"? Slow at creating repos? committing? diffing? branching?\nhttp://laserjock.wordpress.com/2008/05/08/git-and-bzr-historical-performance-comparison/ give you some numbers, but that is just for that particular case."
    author: "Peppe Bergqvist"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "bzr svn is hellishly slow and memory hogging. Because of that, despite trying a couple of times, I could never really use it.\n\nSo even though it shouldn't be this way, the 'svn' compatibility is very important. KDE devs use Git because they can.\n\nGit is dangerous - I lost part of my work the other day with a failed git rebase interactive command. Mercurial or Bzr might be better, but I don't know, never had the opportunity to use them."
    author: "Ian Monroe"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "\"Git is dangerous - I lost part of my work the other day with a failed git rebase interactive command.\"\n\nThis is a myth.  You have to fairly skilled to actually loose your work in git.  In almost all cases including yours you can use the reflog to recover your work.\n\nCheckout \"git help reflog\""
    author: "Shawn Bohrer"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "A myth? It happened to me on Monday."
    author: "Ian Monroe"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "Ah ok, so if I was more familiar with the 150 commands I could have recovered my work. Right. ;)"
    author: "Ian Monroe"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "For such problems, using git-reflog would have saved you."
    author: "S.F."
  - subject: "Re: Git vs others"
    date: 2008-08-30
    body: "The fact that Git commands aren't atomic seems like a major problem though. git-rebase gave an error, and rather then resetting to the previous state it sent my latest commits to purgatory. \n\nWhen people talk about Git being atomic I suppose they just mean its commits are atomic. There are so many git commands though, its no wonder that there's a lack of quality control."
    author: "Ian Monroe"
  - subject: "Re: Git vs others"
    date: 2008-09-05
    body: "\"There are so many git commands though, its no wonder that there's a lack of quality control.\"\nThere are a lot of hg or bzr command too (maybe a bit less, but enough). It's not a problem: to work you don't need all of them."
    author: "Evgeniy Ivanov"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "I bet it really isn't that slow. Besides as someone else said build time is the real annoyance when developing.\n\nI'd rather have a slightly slow but understandable VCS any day."
    author: "Tim"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "> I'd rather have a slightly slow but understandable VCS any day.\n\nWhy not have both? Try Mercurial and judge for yourself.\n\nhttp://www.selenic.com/mercurial/wiki/index.cgi/Tutorial\n\n\n\n"
    author: "Al"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "... funny you should mention Lisp. it's a great language that has relatively few day to day users. bzr may well be a good tool, but most people using a DVCS are using git. seeing as it is rather debatable as to which is better between and git and bzr, current usage is important. a lot of KDE devs are already using git (i'm a casual user myself these days), so the ramp up effort for git should be considerably less than with bzr for that reason alone.\n\nand my main concern is that if/when we switch that it interferes with development as people get used to the switch. any change in the toolset we use will cause discomfort and transitional costs at first. keeping those to a minimum is a priority from my perspective."
    author: "Aaron Seigo"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "Which is actually a good reason to find a DVCS system thats easier to use then Git. I've been using Git with git-svn for about a year and a half now, I still come across confusing situations. \n\nGit has the best SVN support so there are KDE developers using Git which can't really be said for any of the other DVCS's. That's a major advantage no doubt. But it just seems like no one sat down and thought about the UI for Git. \nian@gomashio:~> git-\nDisplay all 133 possibilities? (y or n)\n\nFrom the little I've seen of Mercurial and Bzr they seem to created with more of a 'vision' of how the user will work with it. And I know Bzr is designed from the start to work with centralized repos (eg launchpad), seems like it might be a closer fit. Dunno though!\n\nKDE switching to Git would make my life a lot easier, half of the problems I have are related to Git and SVN not playing well together. And being able to actually share branches and work directly with others will be great indeed (if git-svn allowed this, we probably wouldn't even need to switch). So I'm not really against the plan at all, just a little worried."
    author: "Ian Monroe"
  - subject: "Re: Git vs others"
    date: 2008-08-30
    body: "> Which is actually a good reason to find a DVCS system\n> thats easier to use then Git\n\ngit is pretty easy to use these days; i found git annoying to use a couple years back but it's quite straightforward now. so the main issue i see is existing familiarity, tools that can help transitions and availability of people who will do the work.\n\n> Bzr is designed from the start to work with centralized repos\n\ngit does just fine with this as well; i'm using this model for some side projects with gitosis on the server side to make that all a bit easier as well."
    author: "Aaron Seigo"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "I see your point, but if we had to take our decisions based on current usage, we would have never chosen CMake, which we know was the best option.\n \nBesides, Launchpad will be released as free software within a year[0]. As apparently it is much better than other hosting services such as Sourceforge or Google Code, we can expect the number of Bazaar users to increase dramatically.\n \nMySQL recently migrated to Bazaar[1]; some other big projects are using Mercurial.\n  \n \nIMHO, we should choose which DVCS to use based on their actual features, easy of use, documentation, future perspectives, etc, and not on the current number of users.\n \nThis page seems to be a good starting point:\n    http://www.infoq.com/articles/dvcs-guide\n \n \n[0] http://arstechnica.com/journals/linux.ars/2008/07/23/mark-shuttleworth-launchpad-to-be-open-source-in-12-months\n[1] http://blogs.mysql.com/kaj/2008/06/19/version-control-thanks-bitkeeper-welcome-bazaar/"
    author: "Peter Plys"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "We will choose the tool based on having developers with expertise to execute the conversion and then maintain the repository. That also includes teaching and helping others.\n\nIf you inspect the traffic for the kde-scm-interest mailing list, you'll see there's only one contender.\n\nIf anyone is interested in a different DVCS tool, you had better catch up with the last 6 months of work I have put into Git. \n\nNow."
    author: "Thiago Macieira"
  - subject: "Re: Git vs others"
    date: 2008-08-30
    body: "You've shown the *technical* merits of git, and based on the post above there is *emotional* weight behind this direction, but the decision should be made from a *global* perspective.\nQuoting the original article:\nKDE 0.0 to 3.5 took 420.000 new revisions in 8 years \nKDE 3.5 to 4.0 took 300.000 new revisions in 2 years\nWhat allowed such a progression, the technology of the source code control system, or the socially viable concept of \"wanting to contribute\"?\nThe same concept applies right up the life cycle.\nIf changes can to be tested by 10 people, that's great, but if changes can be tested by 10,000 people that's... well, that's open source."
    author: "jerry"
  - subject: "Re: Git vs others"
    date: 2008-08-30
    body: "No, I think you misread the \"emotional\" part.  The point that Thiago made is simply that if you think something not git should be a contender, start doing the legwork.  Since KDE will end up with a tool that actually has a conversion done of the >800000 revisions to that revision model.\n\nSo, to back your favorite horse, get on it and make it move."
    author: "Thomas Zander"
  - subject: "Re: Git vs others"
    date: 2008-08-30
    body: "let's assume that Launchpad does get released as Free software, that this does result in an increase of bzr users ... how does that event happening sometime in the next year help change either the situation today or give us a reasonable guarantee that it will even catch up with git usage? it doesn't.\n\nmoreover, this is not simply about the current number of users globally, it's about the current number of users within the KDE contributor community. which matters to lower the associated costs of transitioning and to ensure we actually have the manpower to do the transition in the first place.\n\nas for features, git has everything i need and then some."
    author: "Aaron Seigo"
  - subject: "Re: Git vs others"
    date: 2008-08-30
    body: "I actually don't see the relevance of Launchpad at all. Its not like KDE is going to switch to it or that it'd be impossible for Launchpad to have a \"insert-SCM-here\" backend."
    author: "Ian Monroe"
  - subject: "Re: Git vs others"
    date: 2008-08-29
    body: "Lisp is a nice language, I have used it myself in my education. The possibility to alter a running program is very nice and of most importance when dealing with parsing natural language and building phrase tress (for example). So Lisp is great language, but with few users as you say, maybe this can hinder some development of git (don't know since I haven't checked where the code occurs).\nMaybe my point is: the less mixture of different languages the easier to maintain, or something."
    author: "Peppe Bergqvist"
  - subject: "Re: Git vs others"
    date: 2008-08-31
    body: "\nMercurial only uses lisp for the emacs integration. Which, of course, makes sense.\n\n"
    author: "Thomas Capricelli"
  - subject: "Hm..."
    date: 2008-08-28
    body: "Hm... I didnt really understand the new system completely, but the people in charge sound like they know what they are doing.\n\nI hope that things will work out as planned though. Changing the development model completely could also scare people away and cause breakages. The transition period will definitely slow KDE4-Development down for a while.\n\nMaybe such a big change should not be attempted before KDE4 gets \"more done\" and more widely accepted. I guess that a year from now KDE4 will have succeded KDE3 completely, also (3rd-party-)application-wise. That might be a good point in time to do it...."
    author: "Hannes Hauswedell"
  - subject: "but"
    date: 2008-08-29
    body: "Arguably SVN is slowing down development now, as explained in the article.\n\nThere will be a transition cost of course. Just keep in mind the current \"branching sucks\" cost that we are paying daily now."
    author: "Ian Monroe"
  - subject: "Excellent"
    date: 2008-08-28
    body: "Yay!  This has me really excited.  I've always been a great fan of the \"stable-trunk\" model.  It's so much cleaner.  The Linux and Git projects both have a policy of keeping master more stable than the most recent release (anything unstable remains in \"next\").  This provides great confidence in the master branch, and would certainly give me confidence to start hacking away, unlike KDE's current trunk, that I've been unfortunate enough to find broken on more than one occasion :(\n\nI've been an avid Git lover ever since I first tried it about a year ago.  Before someone claims Git isn't cross-platform, I'll jump in to say that I use Git with Linux, MacOS X, and Windows on a daily basis, and I can testify to the fact that Git works just fine in Windows now :)  Oh, and Git is really not hard to learn at all; it just takes a little time to get started.  Everyone should be taking time to learn the tools they use anyway if they want to be efficient.  Learn to touch-type and you'll be more efficient; learn Git and your code will love you :p"
    author: "Paul Gideon Dann"
  - subject: "Schedule"
    date: 2008-08-28
    body: "From what I see both as a user getting updates and reading comments by developers, a problem seem to be the development schedule. A 6 months schedule sounds like it's neither here nor there. It's both to short and too long, depending on what you work on and how it fits with the current development phase. I think a more adaptable schedule would work better, using a policy of dual release schedule. But still retaining the freeze periods, making it less relying on developer discipline.\n\nSomething like a 4 month schedule, but with the option for selected modules to skip to a 8 month schedule, and instead releasing the latest from the previous stable branch. This will give the developers shorter time to market when the development process are at a stage where small incremental changes makes the most sense. And still have the ability to calmly work on bigger things when the need are present."
    author: "Morty"
  - subject: "Re: Schedule"
    date: 2008-08-28
    body: "Well if the \"always summer in trunk\" principle is adopted then, in theory, one could take a snapshot of trunk any old time and release binaries that would most likely at least build and run. The necessity of a release schedule diminishes in importance. I'd say a 3 month official release schedule could be feasible where the last 2 weeks is focused on bug-fixes-only and intense testing of trunk builds. If the released tarballs prove to be a disaster then it would only take a week or two to release a fixed followup release.\n\nPart of the problem with longer release cycles is that the buildup of future changes during the stabilizing phase become so intense that the impedance mismatch between the previous and next versions, particularly at the crossover point just after a release, is in itself a hugely destabilizing factor. With more frequent release there is not such a great leap between the stabilized code and the pent up changes about to rush into the next release cycle leading to less time spent patching things up after each release and more time developing code against a well known and highly usable trunk up until the next bugs-only-freeze stage.\n"
    author: "markc"
  - subject: "Re: Schedule"
    date: 2008-08-28
    body: "As it is there are not often that KDE trunk already at least build and run, so that would not change much. Afterall it's simplest form of developer discipline, don't commit things that don't build. The major problem with \"always summer in trunk\" is that it relay on more developer discipline, when it comes to stopping and concentrating on bug fixing. A freeze period makes it less a issue, as it to some extent forces it. And it's basicly what you suggest anyway, just without calling it for what it is.\n\nPart of the problem with short release cycles is that there are features that you don't have time to finish and stabilize in a satisfying degree. Either leading to rush them in before release creating lots of problems, or in a state where they never get committed as it's never time to sync and stabilize them with head in a sane way. \n\nSince KDE is a very large project there will always be parts that are in state where it's feasible to do small incremental changes fitting to a short release cycle, and other parts where larger more time consuming changes are needed. With a dual release cycle, developers of the different modules can decide and plan their development efforts accordingly.\n\nTo make up an example. For instance the Plasma developers may have lots of small features and fixes that is possible to stabilize quickly and they want to push those out to user as son as possible. While the PIM developers may want to port the whole module to use Akonadi and perhaps do some refactoring, needing more time to implement it correctly and stabilize it compared to the Plasma developers. A dual release schedule would accommodate this, taking into account the different needs of both cases."
    author: "Morty"
  - subject: "Re: Schedule"
    date: 2008-08-28
    body: "Point taken but if the release cycles are short enough then the parts of the system that take more work can skip a release and not be stuck with becoming too stale by the time that new functionality does get released to the public.\n\nSay a developer, or team, is working on a large subsection using Git then they can keep working amongst themselves during any bugs-only-freeze and sync up with trunk after only a dozen or so days of isolated work. They can then hook up with the next release cycle instead of holding back the current one if he release cycle is short enough.\n"
    author: "markc"
  - subject: "Re: Schedule"
    date: 2008-08-28
    body: "You could translate the \"always summer in trunk\" idea to \"the last stable version of my code is in trunk\".\n\nI think for developers, it's a great asset to know that the features in trunk can actually be expected to not be work in progress anymore. Less uncertainty when you run into the unavoidable question \"is this my code that's doing something wrong, or has this just not been implemented yet\". If you got it from trunk, it should work. Otherwise, get the developer in question to fix it (or fix it yourself)."
    author: "Sebastian Kuegler"
  - subject: "Re: Schedule"
    date: 2008-08-29
    body: "Sebastian really knows what he's talking about :)  The whole purpose of the master (or trunk) being always stable is that you can tag and release master at pretty much any arbitrary time without concern for instability or bugs.\n\nWith the suggested model, a release can be made whenever the release team wants, without affecting the developers at all.  Developers continue to produce bug-fixes and features, which are only merged to master when they're stable enough for release.  This model really works, and it's wonderfully elegant."
    author: "Paul Gideon Dann"
  - subject: "tool replacement cycle"
    date: 2008-08-28
    body: "I'm still not sold on the idea that we need a new revision control tool every two years. Yeah, some people don't like subversion. So what? Some people don't like git! You will never find one tool everyone likes, but I guarantee you that switching to a new tool every two years isn't going to please everyone either. Let's just skip git and jump directly to 2010's tool!\n\nI understand the desire for a distributed development process, but I'm not sure changing the way everyone works is the only way to get it."
    author: "David Johnson"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-28
    body: "The current DVCS options did not exist 2 years ago when the (right at the time) choice was made to adopt Subversion. Now there are 3 or more good contenders for fully distributed version control systems available, and more importantly, the concept of how to use a distributed system is now widespread whereas it wasn't \"back then\".\n\nThere is a reasonable upgrade path towards adopting Git, in particular, by using git-svn as a bridge for a year until it's decided to, hopefully, adopt Git as the central repo as well. There is nothing stopping developers using Git right now and taking advantage of fast branching for themselves and to push/pull between other developers also using Git, then eventually merge with the central master Subversion repo. If after ~12 months experience, of developers using this hybrid approach, that it was decided that Git should also replace the central master repo then you can be sure it would be the right decision... and not just a fad because because the kernel guys are doing it.\n"
    author: "markc"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-28
    body: "You got it backwards.\n\nThe objective is not to change the way everyone works to get git. The objective is to adopt git so that they can change the way everyone works. \n\nAs TFA says, they are considering other DVCS that support the \"always summer on trunk\" development model. Git just happens to be the one everyone is familiar with because of the integration with subversion.\n"
    author: "Paul"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-29
    body: "I can agree with that. It's also a matter of the fact that I particularly LIKE the model we use. I know it's got it's downs, but it's got it's ups to.\n\n<rant size=\"big\">\nThe advantage it has is also it's disadvantage: it's centralized. That means that it's easier to keep all developers on the same page. Because of this, it's also easier to test the whole code-base. If everything is going on in different branches, it's extremely difficult to get the testing that you get when all the development happens in one place.\n\nThere is also the pain of merging. Sometimes, a development branch may diverge from the original code-base too much to merge it with the other branches. Depending on the extent of the diversion, you may end up picking a branch, and basically rewriting all the changes implemented in the other branches on it. Naturally, such merging is going to cause serious problems with the large amount of people working on KDE (no, don't cite the Linux Kernel: it follows a model with only a few select people having commit rights who can easily collaborate to ensure merging works easily).\n</rant>\n"
    author: "Michael \"+1\" Howell"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-29
    body: "Hmm.  I think you're still thinking too centralised.  With a decentralised model, there would be a hierarchy of maintainers rather than a central codebase.  For instance, if you want to work on KDE-Games, there would be a maintainer for each (maintained) game, and an overall KDE-Games maintainer.  If you create a patch for a game, you send it to that game's maintainer, who merges it.  The KDE-Games maintainer will pull changes from each game maintainer, and a global KDE maintainer will pull changes from him.  The patch is propagated up the chain in this way.  The global KDE maintainer will probably also be responsible for release.\n\nYou'll find that merge conflicts are pretty rare in this model.  I think this is largely because the emphasis on frequent incremental merging (both up and down the hierarchy) prevents any branch from becoming too separated from its parent.  In SVN, the emphasis is on large branches that are only merged right at the end, and that is certainly tough."
    author: "Paul Gideon Dann"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-29
    body: "Is this actually the proposal?"
    author: "Ian Monroe"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-29
    body: "No.\n\nThe proposal for KDE is to still maintain a central repository."
    author: "Thiago Macieira"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-30
    body: "This is the way a see such a system working. Having maintainers of different modules _actively_ maintain these - and this wasn't the case with KDE 3 thoroughly - would do KDE good, because if such a maintainer is slacking or doing bad work, it'll be noticed rather quickly, since developers will complain when stuff doesn't get merged and merging or code quality issues fall directly back to the maintainer of a module branch. It's more hierarchical in the sense that there's more responsibility on different levels and should help to improve overall code quality."
    author: "Carlo"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-30
    body: "Hmm; I guess this wasn't touched on specifically in the article.  It's the model the Linux kernel uses, and generally seems to be the most logical and elegant model for large projects.  I'd be really disappointed if KDE switched to DVCS only to keep the SVN-Wiki model of repository management."
    author: "Paul Gideon Dann"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-29
    body: "The changes you describe would need to be done in a branch anyway. With SVN, you'll quickly tear your hair out when you need to merge multiple times to keep up with trunk."
    author: "sebas"
  - subject: "Re: tool replacement cycle"
    date: 2008-08-30
    body: "i don't think we're actually talking about stepping away completely from a centralized model here, but making development in branches or decentralized prior to merging easier.\n\nwe 'fake' this fairly well with plasma right now with feature branches and playground, but it's rather painful (to say the least).\n\n> it's extremely difficult to get the testing that you get when all the\n> development happens in one place\n\nfor plasma, where we do use branches to keep some of the more invasive or experimental changes away from trunk for safety purposes, it's actually the opposite. those branches get nearly no testing except by the involved developers until merging due to the cost and annoyance of trying other branches, and more people stick with the last stable branch versus trunk because of all the raw devel happening there.\n\ni'm hoping we can have a small number of hot branches folding code at stabilization points back to a warm trunk on a regular basis. so everyone can follow trunk and be at most a couple weeks behind the bleeding edge where the developers live. the developers can find and fix the problems they run into, and the rest of those who follow kde's mainline can test the results of that process a few weeks behind.\n\n> a development branch may diverge from the original code-base \n\nwell ... don't let them diverge so much then. it's a matter of coordination, and we'll have the main share repo to help with that. that said, svn gives merging branches a really bad name, worse that it actually deserves."
    author: "Aaron Seigo"
  - subject: "Always summer in trunk"
    date: 2008-08-28
    body: "Some good arguments for DVCS in general, and Git in particular, is made in this article by Assaf from apache.org who is currently \"stuck\" with a centralized Subversion repo. A similarly large project in a similar situation...\n\nhttp://blog.labnotes.org/2008/04/30/git-forking-for-fun-and-profit/\n\n\"Always summer in trunk\" is the important principle, regardless of dVCS, and what toolset best enables that principle is debatable but Git, IMHO, has the edge and probably the best future inertia. To me, where Git best aligns with KDE over the long term, is it's significant C code base (other contenders being python based) which could become the core of a future C++ Qt/KDE development frontend, perhaps as part of Kdevelop, and provide the best KDE specific workflows. If a python based solution is adopted then KDE is forever locked into having a significant part of it's infrastructure always dependent on python, which is fine by some and not so by others, but those python parts will probably never get translated to C/C++ libraries that would make for the most efficient backends to KDEs C++ frontend. There may be some short term gains with another dVCS but over the long term (5+ years), as various native C++ frontends develop for whatever dVCS is deployed, I believe Git would provide the most powerful backend bindings for a C++ project like KDE.\n\nThere is also a GitTorrent project underway and that would help with a large multi-gigabyte project like KDE, and also illustrates the adoption range of Git.\n"
    author: "markc"
  - subject: "Re: Always summer in trunk"
    date: 2008-08-29
    body: "Having to choose your VCS based on the language it is implemented is debatable, to say the least.\n\nThe only program _remotely_ affected would be Kdevelop, and seeing the good Mercurial support available for Eclipse and Netbeans I doubt it...\n\nIf you want to make a well thought decision look at the work some people did when trying to decide what to use. \n\nhttp://weblogs.mozillazine.org/preed/2007/04/version_control_system_shootou_1.html\nhttp://www.opensolaris.org/os/community/tools/scm/history/\n\nPS: People seems to jump into the wagon \"This is good because Linus made it\". Please, lets made informed informed decisions based on facts, not on philias and phobias."
    author: "Al"
  - subject: "Re: Always summer in trunk"
    date: 2008-08-30
    body: "> People seems to jump into the wagon \n\nthat's not what we've been doing; KDE rejected dvcs as an option 3-4 years back, and have arrived at git only after looking at all the options and the skills and manpower we have available to us.\n\nLinus, or any other bandwagon chase, has bugger all to do with this."
    author: "Aaron Seigo"
  - subject: "Re: Always summer in trunk"
    date: 2008-08-29
    body: "GitTorrent? You can't get much more distributed than that ;)!\n"
    author: "Michael \"Distributed\" Howell"
  - subject: "Re: Always summer in trunk"
    date: 2008-08-29
    body: "Git isn't a library. Judging VCS by language choice is ludicrous. This is actually points in Mercurials favor which actually has a API I believe. And python libraries can easily be accessed in KDE with Kross."
    author: "Ian Monroe"
  - subject: "Re: Always summer in trunk"
    date: 2008-08-30
    body: "yes, hg and bzr both have good APIs. there is a git library, however, and i believe that qgit uses it even? it was a SoC project last year.\n\nbut yes, the language the vcs itself is written is isn't really important. (though it can impact things that are, like performance)"
    author: "Aaron Seigo"
  - subject: "Re: Always summer in trunk"
    date: 2008-08-30
    body: "I've done a bit of hacking on QGit and I'm afraid it uses output parsing just like everything else.  There was a little work done on a libgit, but the general consensus is that maintaining such a library would be costly, especially as it would make internal restructuring of Git code difficult.  Anyway, no official libgit on the immediate horizon as far as I'm aware, but the command-line interface is very parsable (if that's a word), and works fine as an API generally.\n\nThere are plenty of Git tools around.  All of them use command-line parsing, and none of them has any real problems with it."
    author: "Paul Gideon Dann"
  - subject: "Makes an awful lot of sense"
    date: 2008-08-28
    body: "I think it will help a great deal."
    author: "T. J. Brumfield"
  - subject: "Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: "Right now, with the open trunk model, everyone who is using trunk is using and testing development versions of every application they use. So if I find a bug in, say, Kate, I can report it via bugzilla, or fix it myself, or fix cooperatively with Kate developers, etc. The same with other people using  code I worked on --- just in the last few days I've gotten 4 or 5 very helpful khtml bug reports from fellow developers. OTOH, if there was a \"kate-development-branch\" or something, I'd have to remember to use that, plus \"plasma-development-branch\", plus \"kwin-development-branch\", etc. \nYes, you mention user-volunteer testers, but how will they deal with a mess of branches?  \n\nAnd really, I'd say lack of such testing was the biggest reason for KDE4.0.0 being so disappointing a release. Most developers started using KDE4 way too late in the cycle, and so a lot of bugs weren't seen. I know for my stuff there were a whole bunch of easy-to-fix but high-impact bugs that I was told about about shortly after 4.0.0 was tagged. If more people made the jump to 4 earlier, those bugs won't be in 4.0.0 at all.\n\nFurther, I think you're severely underestimating traditional stability of trunk. I don't remember when I started using it daily --- I think late in 2.x cycle as a user, and in alphas of 3.0 --- but as a general rule, it's almost always perfectly useable for every-day work. Developers are already generally using work branches for highly invasive changes, anyway.\n\n"
    author: "SadEagle"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: "Yes, the \"traditional\" concept of using trunk for development and tagging stable branches has been drifting towards the opposite approach for some time. One of the problems with branches is that they (the target URLs) keep changing over time and it requires extra meta info somewhere to help developers stay on the same page, or same code. If the tendency towards a stable trunk is actively pushed then there is only, or mainly, a single long term persistent target URL that any developer, or packager, needs to know about... HEAD, and the more folks that develop with and build against that single target the more it improves it's visibility and therefor it's quality.\n"
    author: "markc"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: "Agree 100%. ++\n"
    author: "Michael \"+1\" Howell"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: "It's a valid concern, but I don't think it'll be a problem in practice.  Because of the hierarchical nature of DVCS, developers won't clone from the global KDE maintainer's master branch.  They'll clone from, for example, the KDE-Games' maintainer's master branch, which will contain all the latest goodies that aren't yet considered stable enough for general release.  This way, KDE-Games developers will be testing KDE-Games daily, but using stable versions of all the other modules.  Each module's developers will be testing their module just as before, without it breaking for everyone else.  As it's deemed stable, it'll be merged further and further upstream (contributor's private repo => KBounce maintainer's repo => KDE-Games' maintainer's repo => KDE maintainer's repo).  By the time it reaches the root KDE repo, it's pretty rock-solid.\n\nI imagine there will be KDE-next snapshots made available as well, which would include unstable features that aren't ready for master yet."
    author: "Paul Gideon Dann"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: "> This way, KDE-Games developers will be testing KDE-Games daily, but using \n> stable versions of all the other modules\n\nAnd that's EXACTLY what I consider the problem. The rest of KDE just lost a large bunch of highly skilled developers as part of its testing pool.\n\nPlus, the idea of Linux-like centralized development model, with a tree-like structure centralized at one point simply doesn't match how KDE community works."
    author: "SadEagle"
  - subject: "Re: Big flaw in my view: lack of continuous testin"
    date: 2008-08-30
    body: "> And that's EXACTLY what I consider the problem. The rest of KDE just lost a large bunch of highly skilled developers as part of its testing pool.\n\n\"testing pool\" or victims of the development model?! Nothing stops anyone to create a testing branch, comparably to current trunk and invite developers to use it - but: No one is forced to it anymore."
    author: "Carlo"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-30
    body: "at least in Plasma, the plan is to use trunk as a continual warm branch that people can use just as they do trunk now but which will lag behind the hot branches by a number of weeks. we'll still get the same testing coverage we do now (or even more, perhaps), but on code that we've used and tested ourselves for a short while. this means people following mainline will still be testing pre-release code, but not \"just committed five minutes ago, not complete yet\" features."
    author: "Aaron Seigo"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: "In my view, this becomes easier because you can be sure that if you encounter a bug in trunk, it should actually be reported. No \"ow, don't report bugs yet, I'm not done\" -- that would be a sign that it shouldn't be in trunk/ already. The status of trunk simply doesn't change anymore: It's always the tip of development that should be bug-free.\n\nIf you want more unstable, more bleeding edge stuff, then there could be kde-next, a tree that has development versions that are not really done yet, and that will be merged into trunk once they're better. That's basically the plasma and kate and whatnot development branches lumped together.\n\nSo the stability of trunk right now is not the main problem, it's the fluctuation and the constantly changing meaning of trunk that is. It makes it harder for people to stay on top of what's going on.\n\nWe also need to be careful with \"it's worked for years\", we're growing faster than we ever did, and the signs say we will for some time. We need to get ourselves and our development process ready for this growth. One of the important aspects of this new development model is scalability of the KDE developer community."
    author: "sebas"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-29
    body: ">In my view, this becomes easier because you can be sure that if you encounter\n> a bug in trunk, it should actually be reported\n\nYes, you would not have to worry about in-progress stuff being broken, but \nthe regressions won't be in trunk. \n\nAnd I understand your concern about not having to coordinate with 5 zillions of developers, but in my view, quality requires more coordination, not less.\n"
    author: "SadEagle"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-30
    body: "> Yes, you would not have to worry about in-progress stuff being broken, \n> but the regressions won't be in trunk. \n\nYou have to merge at one point ;)\nIf there are not going to be any regressions in trunk, then we just got perfect.  But I doubt that will happen.\n\nThere are two extremes; on one side you have the commit to trunk as soon as it compiles (thats actually how some people work right now).\nAnd on the other side is the concept of only committing something when its perfect.\n\nNeither of these extremes are comfortable, and I'm afraid we are working too close to the one extreme that everything always goes to trunk.\nThe proposal makes the point of moving more to the middle ground of doing much more stabilization of your features before moving it to trunk.\n\nNobody is suggesting that merging can only be done when the code is perfect, though.\n\nBottom line; we don't have to go from one extreme to another. There is sufficient gray area to use."
    author: "Thomas Zander"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-30
    body: "Well, taking a look at the Linux kernel, we see that it's extremely rare for regressions to make it to the master branch of Linus's tree.  Of course this is more important for the kernel, and it's unlikely that patches will be scrutinised so carefully in KDE, but I think it's very reasonable to expect there to never be any regressions in the master branch if we truly embrace DVCS."
    author: "Paul Gideon Dann"
  - subject: "Re: Big flaw in my view: lack of continuous testing."
    date: 2008-08-30
    body: "The point is that the choice is still up to the developers. Lots of regression tests help there and the kernel probably doesn't get as many testers.\n\nSo, kde devs might integrate into trunk when they feel its ready, not waiting till its perfect."
    author: "Thomas Zander"
  - subject: "Git support"
    date: 2008-08-29
    body: "I'm excited by the possibility that KDE might chose Git as it's official DVCS and general overall Git adoption and documentation is one reason to consider it. As a simple and rudimentary test, check the number results of git, bzr, bazaar, mercurial and hg at Google... git returns 10+ times more than any of the others. Also, when it comes time for developer adoption, sites like http://gitcasts.com/ certainly help and there are at least 2 books available for Git (Pragmatic Version Control Using Git & Git Internals) whereas I'm not aware of any printed books for either Mercurial or Bazaar (they may exist but I couldn't easily find any, which is my point)."
    author: "markc"
  - subject: "EBN"
    date: 2008-08-29
    body: "Regarding http://www.englishbreakfastnetwork.org/\n\nAre there actually policies to review modules before release that these minor issues that affect code quality get fixed?\n\nOr are there any attempts to continue and expand this testing?\n\n\"KDE's current release team has come a long way in finding module coordinators for various parts shipped with KDE, but currently not every module has a maintainer.\"\n\nWe do I find a list of orphaned modules?"
    author: "Malte"
  - subject: "Re: EBN"
    date: 2008-08-29
    body: "> Regarding http://www.englishbreakfastnetwork.org/\n> Are there actually policies to review modules before release that these minor issues that affect code quality get fixed?\n \nSorta.  New code that goes into kdereview should be clean\nBut no other policies.\n\n> Or are there any attempts to continue and expand this testing?\n\nI'm always adding new tests and refining existing tests.\nWe have lots of ideas, but little manpower.\n\n> \"KDE's current release team has come a long way in finding module coordinators > for various parts shipped with KDE, but currently not every module has a maintainer.\" We do I find a list of orphaned modules?\n\nThe list of module coordinators can be found on TechBase on the Release Team project page.\n\n"
    author: "Allen Winter"
  - subject: "SVN 1.5"
    date: 2008-08-29
    body: "I've to admit that I haven't used git so far. But to me it sounds like the workflow of git is about same as a feature branch in subversion for every developer. Now making a feature branch in SVN is easy too.\nMerging a feature branch back to truck I have felt no so easy so far. Now with SVN 1.5 they introduced merge tracking which is supposed to simplify this process. Has anyone experience with SVN 1.5's merge tracking? Couldn't it help here?"
    author: "XCG"
  - subject: "Re: SVN 1.5"
    date: 2008-08-29
    body: "It's not.\n\nYou're thinking that every developer with a DVCS tool will get one branch where he can play as much as he wants. And, given proper merging abilities (which SVN lacked until 1.5), you can easily merge code from other branches back and forth.\n\nThat is correct, but that's not the whole picture.\n\nFirst and foremost you forgot the disconnected part. You can't commit to Subversion unless you can reach the repository, which is often in a server over the Internet.\n\nAlso, each developer isn't restricted to one branch. He very often has a lot of them. Right now I have 28 separate branches of Qt in my workstation: they range from previous stable releases of Qt (to test regressions and fixes with) to branches I created to start working on fixing tasks to research projects.\n\nAnd that's just my private branches. When I am collaborating with other people in projects, I have more branches. For one project right now in Qt, we are tracking 4 or 5 different branches, each with a different \"theme\": optimisations, new features, animations, etc. And there's an extra branch which is the merger of all those \"theme branches\", so that we can get a feel of what it will be when it's done.\n\nFinally, you're also forgetting the ability to undo, redo, and modify your work. Once you commit to Subversion, it's there for life. Removing something from the repository means dumping and reloading it. With a Git, you can undo your commits, change them, squash them together without problems. (You can do that after you've published them, technically, but you shouldn't)\n\nThis is actually something where Git is better than Mercurial: in Mercurial, changing the history isn't that simple.\n\nSo, no, SVN 1.5's merge tracking isn't the solution. It's definitely a hand in the wheel for the current problems, but not the full solution. If you want an analogy, it's covering the Sun with a sieve."
    author: "Thiago Macieira"
  - subject: "Re: SVN 1.5"
    date: 2008-08-29
    body: "For people who are curious about Mercurial(hg) this is a good source and this chapter explains how to rollback commits http://hgbook.red-bean.com/hgbookch9.html"
    author: "sven"
  - subject: "Re: SVN 1.5"
    date: 2008-08-30
    body: "All that stuff is completely unrelated to the \"always summer in trunk\" proposal which can be implemented just as well with SVN."
    author: "Kevin Kofler"
  - subject: "Re: SVN 1.5"
    date: 2008-08-30
    body: "Its actually not; you can't have 'always summer' if everyone just commits his latest thought experiments and every todo item he just came up with for the world to see.\nThat just doesn't make for a trunk thats usable due to the amount of people coming into our growing community.\n\nSo if you read the article more closely you will see that the basis is that people are asked to only commit stuff to trunk when they are done with it.  Which for refactors and bigger things means it may be a week or more before you can commit it.\n\nAnd due to that requirement, thiago's post becomes very relevant.  Those tools are essential to a scalable workflow."
    author: "Thomas Zander"
  - subject: "Re: SVN 1.5"
    date: 2008-09-01
    body: "> Its actually not; you can't have 'always summer' if everyone just commits his latest thought\n> experiments and every todo item he just came up with for the world to see.\n\nI don't see why. The current trunk is working fine, the only difference would be that trunk development can continue during a release freeze because the release would be branched earlier, I don't see why that would prevent working on the trunk the same way as previously.\n\n> Which for refactors and bigger things means it may be a week or more before you can commit it.\n\nThat's what branches/work is for. Keeping features on a developer's HDD is a very bad idea, it means zero testing, no way for other developers to coordinate their changes, no way to help implementing the feature etc. Basically it's a return to the \"big code drop\" antipattern which SCMs were designed to solve, and getting the history added after the fact as part of the big push is only a mild consolation (and not even that is guaranteed because git can \"flatten\" history and that \"feature\" has been touted as an advantage)."
    author: "Kevin Kofler"
  - subject: "Re: SVN 1.5"
    date: 2008-08-30
    body: "Oh, and to reply to your actual arguments:\n\n> First and foremost you forgot the disconnected part. You can't commit to Subversion unless you can reach the repository, which is often in a server over the Internet.\n\nAnd that's a good thing, as it enforces open development, i.e. people can always test the current development version, nobody has to wait for the developer to \"push\" his/her changes. Committing to the central repository as often as possible is just a part of \"release early, release often\" best practices. And the nice thing is that you don't even have to release, just commit and it's automatically public, unlike with a DVCS where you have to explicitly push.\n \n> Finally, you're also forgetting the ability to undo, redo, and modify your work. Once you commit to Subversion, it's there for life. Removing something from the repository means dumping and reloading it.\n\nThat's also a good thing, history should not be altered, and people should be able to see the progress of development, it's part of openness.\n\n> With a Git, you can undo your commits, change them, squash them together without problems. (You can do that after you've published them, technically, but you shouldn't)\n\nAnd that's just evil. And it's also moot because people should be pushing after each commit (remember: release early, release often) unless they have a good reason not to (e.g. being in an airplane).\n\nDVCSes encourage development behind closed doors, and that's a bad thing, because it goes against the community development model which has made the success of KDE and many other Free Software projects. (According to the \"Open Source\" folks, it's even the main advantage of \"Open Source\" software in the first place.)"
    author: "Kevin Kofler"
  - subject: "Re: SVN 1.5"
    date: 2008-08-30
    body: "Complete agreement...\n\nBesides for the few contributors who can't do without disconnected features, either occasionally or because it matches better their way of working, there already is git-svn.\n\nSo what are the arguments for forcing git's horribly steep learning curve on every contributor?\n\nHas the great raise of the bar of entry for the average contributor even been considered?\n"
    author: "L."
  - subject: "Re: SVN 1.5"
    date: 2008-08-30
    body: "> nobody has to wait for the developer to \"push\" his/her changes\n\nno, you just have to wait for them to commit their changes. and making branch management easy encourages people to work in published branches and be able to switch between them more.\n\n> And it's also moot because people should be pushing after each commit \n\ni couldn't agree less.\n\nwhen working on a git repository, i often do a bunch of much smaller commits than i normally would with svn. i can use these as nice \"save points\" in my local repository without inflicting my work in progress on others in the same branch, while gaining the benefits of being able to save my work as often as i want even when it isn't done.\n\ni end up pushing about as often as i would commit with svn, but i commit far more often with git and it's a great boost to productivity.\n\ncombine with squashing, i can make 50 commits in a day to finish one bigger feature and then squash it down to just the three or four important events before publishing to everyone else. or.. i can just push the whole lot if i'm lazy =)\n\n> DVCSes encourage development behind closed doors\n\nthat's at least partly why people want to keep the centralized model, even when using git. it's not the tool that encourages closed development, it's the workflow used. with svn there's basically only one possible workflow, with a dvcs there are several including ones that give similar openness benefits.\n\nas for making points moot, i'm not sure if you are aware of how many git-svn users there are out there with all their completely unpublished, unsearchable and un-clonable repositories. i'd much prefer to see these branches publishable to a central location so people don't have to choose between git and sharing, which is exactly what's happening already.\n\nso ... the problem is already here. i'd like to see us deal with it, and improve our workflow in general in the process =)"
    author: "Aaron Seigo"
  - subject: "Make development easy"
    date: 2008-08-29
    body: "If Git is to be used, I __strongly__ recommends taking a look to:\n\nhttp://www.github.com. \n\nIt is a Git hosting service with social networking bits on it.\nAlso, contribute and fork one owns private branch is as easy as click the \"fork\" button. Make improving software not interesting but something one can't stop doing!"
    author: "Jordi Polo"
  - subject: "Re: Make development easy"
    date: 2008-08-30
    body: "Looks neat :)\n\nIs it open source?  I liked http://gitorious.org/ as well, which is open source (and so easy to extend and not dependent on a company)"
    author: "Thomas Zander"
  - subject: "Re: Make development easy"
    date: 2008-08-30
    body: "Cool. Gitorious seems to be the same idea. \nYeah I just supposed that github was open source but it may be not. \nIt that casa I guess Gitorious is better"
    author: "Jordi Polo"
  - subject: "Re: Make development easy"
    date: 2008-08-31
    body: "Github is not open source."
    author: "Lisz"
  - subject: "Can't wait to see this in use"
    date: 2008-08-29
    body: "KDE really needs to go in the Linux direction, as it is also a huge and rather dynamic project.\n\nWhere are the other akademy news? Only the two first days were published :("
    author: "Br\u00e1ulio Barros de Oliveira"
  - subject: "Re: Can't wait to see this in use"
    date: 2008-08-30
    body: "i think they just stopped calling them \"day 1\" and \"day 2\" and started giving the articles meaningful names =)"
    author: "Aaron Seigo"
  - subject: "Re: Can't wait to see this in use"
    date: 2008-08-30
    body: "I agree that KDE needs to go in a Linux direction.  I'm gradually become aware, however, that there is a difference between the two projects' methodologies.  Linus is the sole owner of the authoritative tree, and has several \"lieutenants\" that own their trees, etc...  However, KDE relies strongly on a large number of equally-important contributors.  I'm guessing that the hierarchical model that Linux uses isn't going to map to KDE so well (at least not for the time being).  I guess KDE will keep a central repo, with the most active developers having direct push access, and more \"fringe\" contributors working either on their own repos or in sort of \"workgroup clusters\" (for those working on the same feature).\n\nThe big difference I think, is that there simply isn't one owner of the KDE project, like there is for Linux.  Maybe that will change as DVCS methodology starts to take root.  It'll be interesting to see."
    author: "Paul Gideon Dann"
  - subject: "Related to Akademy...."
    date: 2008-09-01
    body: "When can we see the videos of the various talks that were held at Akademy?"
    author: "Janne"
  - subject: "Git vs SVN vs TortoiseSVN"
    date: 2008-09-05
    body: "One thing that should be studied carefully I think, is the tools coming with Git and SVN.\n\nFor SVN I've been using for several years the awesome TortoiseSVN (yes works only under Windows...) http://tortoisesvn.tigris.org/\n\nSo easy, so simple, TortoiseSVN simplifies all the hard work with a perfect UI.\nTry it once, and it will be impossible for you to come back to svn command line.\nThis the reason I keep with SVN.\n\nQuestion is, can we expect equivalent tools for Git in the near future?\n\nA good UI above git/svn/... is a good way for \"lowering the barrier for entry\" and \"is another important concern\""
    author: "Tanguy Krotoff"
  - subject: "Re: Git vs SVN vs TortoiseSVN"
    date: 2008-09-05
    body: "Already exists for Hg so I think Git will follow\n\nhttp://tortoisehg.sourceforge.net/"
    author: "joe"
---
At <a href="http://akademy2008.kde.org/">Akademy 2008</a>, KDE Release Team members Sebastian Kügler and Dirk Müller discussed the future of KDE's development process. Describing the challenges KDE faces and proposing some solutions, they spawned a lot of discussion. Read on for a summary of what has been said and done around this topic at Akademy.





<!--break-->
<p>Our current development model has served us for over 10 years now. We did a transition to Subversion some years ago, and we now use CMake, but basically we still work like we did a long time ago: only some tools have changed slightly. But times are changing. Just have a look at the numbers:<br><ul>
<li>KDE 0.0 to 3.5 took 420.000 new revisions in 8 years
<li>KDE 3.5 to 4.0 took 300.000 new revisions in 2 years</ul>
Also, this year's Akademy was the largest KDE event ever held, with more than 350 visitors from every continent of the world.</p>

<p>This enormous growth creates issues both for the wider community, for developers, and for the release team. Patches have to be reviewed, their status has to be tracked - these things become progressively harder when the size of the project balloons like it currently does. The centralized development system in Subversion's trunk doesn't support team-based development very well, and our 6-month release cycle - while theoretically allowing 4 months development and 2 months stabilizing - often boils down to barely 50% of the available time suitable for new feature development.
</p>

<p>KDE's current revision control system doesn't allow for offline commits, making life harder for people without a stable internet connection. Furthermore we're still looking for more contributors, so lowering the barrier for entry is another important concern.</p>

<h2>Changing requirements</h2>

<p>We will have to allow for more diversity and we must be able to accommodate individual workflows. Not everyone is happy with a 6-month schedule, not everyone prefers Subversion. Companies have their schedules and obligations, and what is stable for one user or developer is unsuitable for another. Meanwhile, new development tools have surfaced, such as the much-praised distributed revision control tool Git. Together with new tools for collaborating, new development models are emerging. KDE is in the process of adopting a much wider range of hardware devices, Operating systems (OpenSolaris, Windows, Mac OS) and mobile platforms such as Maemo. And we have an increased need for flexible and efficient collaboration with third parties and other Free Software projects.
Sebastian and Dirk believe it is time for a new way of working. In their view, KDE's development process should be agile, distributed, and trunk freezes should be avoided when possible. While there are still a lot of culprits in their proposal, KDE needs to get ready for the future and further growth.</p>

<h2>Agile Development</h2>

<p>The most fundamental idea behind <em>Agile Development</em> is "power to the people". Policies are there to avoid chaos, and to guide (but not force) people in any way.</p>

What is Agile Development supposed to offer us?<br><ul>
	<li>Shorter time-to-market, in other words, less time between the development of a feature and the time users can actually use it
	<li>More cooperation and shortened feedback cycles between users and developers
	<li>Faster and more efficient development by eliminating some current limitations in team-based development processes.
	<li>Simplicity. Not only good in its own right, but it also makes it easier to understand and thus contribute to KDE development.</ul>

<h2>How can we do this?</h2>

To achieve this, we have to reflect upon our experiences as developers and share our thoughts on this. Our process should be in our conscious thoughts. Sebastian and Dirk talked about a specific lesson they have learned: plans rarely work out. As a Free Software project, we don't have fixed resources, and even if we did, the world changes too fast to allow us to reliably predict and plan anything. We have to let go. We should set up a process aimed at adaptation and flexibility, a process optimized for unplanned change.</p>

<p>This needs to be done in one area in particular: our release cycle. Currently, our release cycle is limiting, up to the point of almost strangling our development cycle. So Dirk and Sebastian propose a solution:

<h2><em>"Always Summer in Trunk"</em></h2>

Our current release process, depicted in the graphic below, can be described as using technical limitations to fix what is essentially a social issue: getting people into "release mode". Over 4 months, we develop features, then enter a 2 month freeze period in which increasingly strict rules apply to what can be committed to trunk. This essentially forces developers to work on stabilizing trunk before a release. Furthermore, developers need to keep track of trunk's current status, which changes depending on where in the release cycle KDE currently is, not taking into account diverse time schedules of both upstream and downstream entities. At the same time, many developers complain about Subversion making it hard to maintain "work branches" (branches of the code that are used to develop and stabilize new features or larger changes in the code), subsequent code merges are time-consuming and an error-prone process.</p>

<img src="http://static.kdenews.org/dannya/development-timeline-current.png" alt="">

<p>The proposal would essentially remove these limitations, instead relying on discipline in the community to get everyone on the same page and focus on stability. To facilitate this change, we need to get the users to help us: a testing team establishing a feedback cycle to the developers about the quality and bugs. Using a more distributed development model would allow for more flexibility in working in branches, until they are stabilized enough to be merged back to trunk. Trunk, therefore, has to become more stable and predicable, to allow for branching at essentially any point in time. A set of rules and common understanding of the new role of trunk is needed. Also, as the switch to a distributed version control system (which is pretty much mandatory in this development model) is not as trivial as our previous change in revision control systems, from CVS to Subversion. Good documentation, best practice guides, and the right infrastructure is needed. The need for better support for tools (such as Git) in KDE's development process does not only come from the ideas for a new development model though. Developers are already moving towards these tools and ignoring such a trend would mean that KDE's development process will clutter and ultimately become harder to control.</p>

<img src="http://static.kdenews.org/dannya/development-timeline-summer.png" alt="">

<p>In Sebastian and Dirk's vision, KDE's current system of alpha, beta and release candidate releases will be replaced by a system which has three milestones:
</p>

<h3>The <em>Publish</em> Milestone</h3>
<p>This is the moment we ask all developers to publish the branches they want to get merged in trunk before the release. Of course, it is important to have a good overview of the different branches at all times to prevent people from duplicating work and allow testers to help stabilize things. But the "Publish Milestone" is the moment to have a final look at what will be merged, solve issues, give feedback and finally decide what will go in and what not. The publish milestone is essentially the cut-off date for new features that are planned for the next release.</p>

<h3>The <em>Branch</em> Milestone</h3>
<p>This is the moment we branch from trunk, creating a tree which will be stabilized over the next couple of months until it is ready for release. Developers will be responsible for their own code, just like they used to be, but one might continue using trunk for development of new features. To facilitate those developers who do not want switch between branches, we could have a tree which replicates the classic development model. Developers are encouraged and expected to help testing and stabilizing the next-release-branch.</p>

<img src="http://static.kdenews.org/dannya/development-timeline-traditional.png" alt="">

<h3>The <em>Tested</em> Milestone</h3>
<p>The "tested" milestone represents the cut-off date. Features that do not meet the criteria at this point will be excluded from the release. The resulting codebase will be released as KDE 4.x.0 and subsequently updated with 4.x.1, 4.x.2, etc. It might be a good idea to appoint someone who will be the maintainer for this release, ensuring timely regular bugfix releases and coordinating backports of fixes that go into trunk.</p>

<h2>Technology</h2>
<p>A prerequisite for this new development model would be a proper distributed source code management system. Git has already stolen the hearts of many KDE developers, but there are other options out there which should be seriously assessed. Furthermore we need tools to support easy working with the branches and infrastructure for publishing them. Getting fellow developers to review code has always been a challenge, and we should make this as easy as possible. We also need to make it easy for testers to contribute, so having regularly updated packages for specific branches would be an additional bonus. Trunk always needs to be stable and compilable, so it might be a good idea to use some automated testing framework.</p>

<p>Under discussion are ideas like having some kind of "KDE-next" tree containing the branches which will be merged with trunk soon; or maybe have such trees for each sub-project in KDE. Another question is which criteria branches have to meet to get merged into the "new" trunk. Especially in kdelibs, we want to ensure the code is stable already to keep trunk usable. Criteria for merges into various modules have to be made clear. What happens if bad code ends up in trunk? We need clear rules of engagement here. How can we make it as easy as possible to merge and unmerge (in the case the code that has been merged is not ready in time for a release)?</p>

<p>Having a page on <a href="http://techbase.kde.org/">TechBase</a> advertising the different branches (including a short explanation of their purpose and information about who's responsible for the work) will go a long way in ensuring discoverability of the now-distributed source trees. A solution also needs to be found for the workload around managing trunk. Especially if we have tight, time-based releases, a whole team of release managers needs to take responsibility. KDE's current release team has come a long way in finding module coordinators for various parts shipped with KDE, but currently not every module has a maintainer.</p>

<br><br>

<p>While there are still a lot of questions open, we'd like to work them out in collaboration with the KDE community. KDE's future revision control system is discussed on the <a href="https://mail.kde.org/mailman/listinfo/kde-scm-interest">scm-interest mailing list</a>. Discussion on a higher level can be held on the Release Team's mailing list, and naturally KDE's main developer forum, <a href="https://mail.kde.org/mailman/listinfo/kde-core-devel">kde-core-devel</a>.</p>

<p>With the release of KDE 4.0, the KDE community has entered the future technologically. Though timescales for the above changes have not yet been decided upon, Dirk and Sebastian took the talk as an opportunity to start discussing and refining these ideas: it's time that KDE's primary processes are made more future-proof and ready for the new phase of growth we have entered.</p>