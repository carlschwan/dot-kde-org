---
title: "KDE Commit-Digest for 24th February 2008"
date:    2008-03-02
authors:
  - "dallen"
slug:    kde-commit-digest-24th-february-2008
comments:
  - subject: "Games framework"
    date: 2008-03-02
    body: "Wasn't there a new app/framework to easily create other games?  I thought it was largely designed around board and game games.  I can't recall the name and I wanted to check up on it."
    author: "T. J. Brumfield"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "It died before it was born.  One of them projects that talked before it could walk, and was satisfied there."
    author: "Dan"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "There's a nice project that's been in dev for a _long_ time, called stencyl(.com).  It's in Java, and I followed it's mother (that eventually died) when I first started programming. Should be pretty good..."
    author: "J Klassen"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "How about Tagua:\nhttp://dot.kde.org/1189276647/\n\n(yeah I think it rocks)"
    author: "jospoortvliet"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "http://www.tagua-project.org/timeline\n\nIt looks like Tagua is still in development."
    author: "T. J. Brumfield"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "Sorry but I do not understand why talented developers waste time on those silly games. \n\nWhat about the rest of the KDE4?\n4.0.1 barely works and is mostly unconfigurable and unusable. FFS, work on Kmail, Konqueror or the new Kmenu-thingy (now you need more clicks than ever before to launch an application?) it sure is a usability frak up! :)\n \nThere is billjon more important things to do than waste time on apps that are  for... wasting time :) \nI have nothing against the KDE games (how about a good Go GUI that can connet to KGS etc or play Go against your PC? Massive waste of time btw!). \n\nPlease build the house first and then add the useless pink flamingo crap. \n\n(BTW, I use only Linux at home and work. Our office runs 100% on KDE 3.5.9)\n\nAnd yes, I know, this is only the beginning for KDE4 bla-bla-blaa... ;) \n\n"
    author: "K\u00f6ki-K\u00e4ki-M\u00f6ki-\u00d6ki-b\u00e4\u00e4\u00e4"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "Send me some money, and I'll work on what you want me to.  Otherwise, go away :P"
    author: "JohnFlux"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "Not this whine again. Apart from the fact that you can't just reassign people away from something that they presumably enjoy doing and have fun writing and maybe they don't have the skills to write a mail client or a web browser, the games aren't a \"waste of time\" as you say. The games were some of the most complete apps early on in KDE4 development and put to use some of the new technologies, this was no doubt fuelled by the enthusiasm of doing something fun. I have never played any of them but I can appreciate the efforts of the games developers in the form of better tested KDE4 tech.\n\nI hate having to waste my time like this."
    author: "Matt"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "The new KMenu can be replaced with a classic style menu. Just remove the Application launcher plasmoid and place an Application Launcher Menu plasmoid in place of it."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "More important to YOU, perhaps.  But application developers tend to work on what they have some particular skill for, or interest in.  Programming for fun (as opposed to professionally) is essentially a self-improvement exercise, where people work on things that challenge them in some way.  That self-challenge naturally leads to better KDE devs, whatever they choose to work on.  The result is that we get a better KDE."
    author: "Lee"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "Game developers are not Office-app developers, and visa versa. Moving people away from the things they're best at won't improve a lot.\n\nIt seams kde-games and kde-edu have a low barrier to enter KDE developer. They make it easy for people to join KDE. I guess that's both technically (code wise), mentally (you can't screw up really bad) and socially (becoming part of a team)."
    author: "Diederik van der Boor"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "Are you thinking of Tagua, maybe?\n\nhttp://www.kde-apps.org/content/show.php/Tagua?content=65571"
    author: "Anon"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "Yeah, Tagua must be what you are looking for..."
    author: "jospoortvliet"
  - subject: "Re: Games framework"
    date: 2008-03-02
    body: "sorry, forgot:\nhttp://dot.kde.org/1189276647/"
    author: "jospoortvliet"
  - subject: "Re: Games framework"
    date: 2008-03-03
    body: "DirectX ?"
    author: "CJ"
  - subject: "KDE and Google"
    date: 2008-03-02
    body: "I posted this a week or two ago and didn't get many replies, but the Dot has been busy and it may have been missed.\n\nI was thinking long and hard, and I believe KDE should form a strategic partnership with Google in much the same way Mozilla did.\n\nAs far as the API goes, you only get so many uses for your API key, so KDE couldn't just take the API and use it without paying for it, or striking a deal. They'd go over API usage with all the people who use KDE.\n\nHowever, imagine Google contributing code to NEPOMUK and improving Strigi.\nImagine fully integrating Google services like GCalendar, GTalk and Gmail into your desktop.\nImagine easily integrating Google Docs to share documents.\nImagine being able to search an index with your account, and have it know that what you're looking for is on another computer you've used recently.\n\nKDE 4 is now cross-platform. With plasmoids, open APIs, and the beginning of the Semantic Desktop, you could fully integrate your desktop experience with an online community, and simultaneously integrate online services into your desktop.\n\nThe partnership would profit both parties, and the end users would get much better features.\n\nYou could take it even further. It could create in-roads for KDE usage in the Enterprise environment through the strength of the Google brand. I can tell you first hand that integrating Sharepoint is very costly. Imagine an OSS alternative that allows the entire enterprise to communicate via email, calendar, IM, share documents, collaborate, search, etc. intuitively, and directly through your desktop apps.\n\nWe need to brainstorm this, and someone needs to approach Google about this."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "They seem much closer to GNOME-based technology at the moment; look at the number of SoC projects they got. There was also an article recently about GNOME getting help to improve its accessibility options from several companies, including Big G (http://www.linux.com/feature/127801). Could it have anything to do with the fact that KDE has Marble, which may one day rival Google Earth? Who knows."
    author: "Cynical"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "\"They seem much closer to GNOME-based technology at the moment; look at the number of SoC projects they got.\"\n\nI thought KDE got more last year?"
    author: "Anon"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "If I remember correctly KDE got more SoC students than any other mentoring organisation.\n\nCommenter Cynical might refer to the combined numbers of all organisations from the GNOME ecosystem, e.g. AbiSource or Pidgin.\n\nHowever, getting one huge amount of resources for the KDE umbrella has some advantages as well, e.g. if some subproject doesn't have enough mentors on its own someone from a different subproject can probably step in and delegate only project specific questions.\n\nOf course this kind of self organisation requires that the KDE subteams reach a consensus on the allocation of SoC slots and we certainly can improve the internal allocation process, probably persuading Google to give us an increased number of students overall.\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "Google has supported QT and GTK, Gnome and KDE.  However, past support doesn't rule out a KDE partnership."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "I second that!!!!\n\nGoogle would be a great partner. I'm sure they can support both gnome and KDE.\nLet's email them and request it. The more people email, the more they see people want to.\n\nI for one would love a Gmail, Google Calendar, etc. integration into KDE. Let google write it, the KDE guys are busy enough already. :)"
    author: "Max"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "Free Software is about Freedom and independence.  It fundamentally should NOT get into bed with proprietary software/services companies.  And that's aside from all the reasons that a desktop shouldn't depend on access to a website."
    author: "Lee"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "Google gives to open source as much as any company.  They have open API's for their proprietary services.  And I'm sure that integrating those services would be an option you could opt out of when compiling KDE if you so chose.\n\nClaiming that Google is evil and proprietary is pretty silly.  Google is the anti-MS."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: ">Google is the anti-MS\nThat's nonsense. Google is the other (not yet) MS.\n\nOTH Google is also a datasteelingandselling monster.\n\nThis is now my personal preferation, but I won't ever use any Google based services for my personal data.\n\nIf others do, I let them do as it is their live. I just want to have them warned.\n\nLet the future tell us to where it will go when companies know all about your whole live..."
    author: "Philipp"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "> Claiming that Google is evil and proprietary is pretty silly.\n> Google is the anti-MS.\n\nYou are silly. Of course is Google against MS. MS is Google's competitor. Just like Wal Mart is against Amazon.\nMS was also a strong supporter of open standards when Novell Netware ruled corporate networks. Once WinNT took over, all the standards support disappeared.\n\nGoogle's most important goal is to spy on users to better target ads on them and in turn make more profit. Google supports open source to improve its image within the geek community. And it works. Every other company would get lots of bad \"press\" for the spyware included in e.g. the Google Firefox Toolbar, its use of tracking cookies, and so on. Google does not. It's sometimes even seen as messiah.\n\nIf Google wants to contribute to OSS projects (because they use those prohjects internally and don't want to maintain an internal patchset or for whatever reason) then fine. But don't become dependant on Google alone."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Nobody ever said we would. We just want their support. The more people work on KDE, the better, the wider audience it gets.\n\n"
    author: "Max"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Well Google at least TRIES not to be evil!!\n\nName another big software services company that has so openly embraced that philosophy. Google is a good partner.\n\n"
    author: "Max"
  - subject: "Re: KDE and Google"
    date: 2008-03-04
    body: "Or for you cynics out there:\n\n\"The enemy of my enemy (Micro$oft) is my friend (Google).\"\n\nSo at least for now it's great to ask for Google's help, and to work TOGETHER with Google.\n\nWhy would you give up privacy if you did that?"
    author: "Steve"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "There are people who are concerned about the privacy of their data, who do not trust Google (or any other company for the matter). \n\nSo when you \"brainstorm this\", please make sure that such problematic service stuff is kept optional - at best opt-in - so KDE does not loose part of its user base."
    author: "Carlo"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "The Mozilla way is wrong. It's a full sell-out to a single company."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "Maybe, but Firefox is sure a great product."
    author: "Max"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "What have they sold out?\n\nDefault search engine is the only thing they've sold out.  I think Konqui defaults to Google, but does it for free. It has been said time and time again, that Firefox's Customize Google add-on hurts Google by blocking ads, and yet Google has never once complained about it.  Google has never once requested that Firefox block it, or not host it on their site.\n\nGoogle has made no demands to Firefox/Mozilla that I know of."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "What is probably the most popular Firefox extension? (You know it...)\nNow guess why it is not shipped and enabled by default in Firefox. Do you *really* think that this has nothing to do with the huge amount of sponsorship money they receive?"
    author: "Andreas"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Adblock Plus doesn't block Google ads.  Customize Google does.\n\nAnd Firefox doesn't ship with any popular mods.  You can't blame that on Google.  That has always been the stance of Mozilla, to be modular and not ship with mods."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-04
    body: "See, I'm really not jealous for Google to make money. They deserve it. They give me the information I need, when I need it. Something Yahoo, and Microsoft can't do anymore.\n\nI also like Altavista as a search engine. I had good results. (I tried it again for the first time last fall after a good 10 year hiatus. They got better. I actually found something with it, that I couldn't find with google.)"
    author: "Max"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Besides the default entry in the search field:\nStart page and anti-phishing.\n\nThat led to the effect that Mozilla is now completely dependant on Google. All main Firefox programmers are paid with money from Google. Of course they'll continue to add features that please Google while locking out the competition (eg. not using the community-based PhishTank.com blacklist for anti-phishing).\nFF will also never ship with AdBlock due the Google involvement. While you claimed that Google ads are not blocked by AdBlock Plus, let me remind you that DoubleClick is a subsidiary of Google and indeed blocked. I think it also depends on the subscribed filterset, if regular text-based Google ads are blocked.\nI'm sure that Thunderbird was also dropped because Google prefers that GMail via browser is used.\n\nSome corporate sponsoring is OK but a \"sponsorship monopoly\" is bad. How much development community does Mozilla really have besides paid programmers?\n\nBTW: I don't think that Google even wants a close collaboration. Google (with Android) and Trolltech (with Qt/Qtopia) are competitors. Why would Google want to promote a competing technology?"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Start page and anti-phishing were directly part of the Firefox/Google deal.  Without the financial support and paid development, I doubt Firefox 3 would be would it is today.\n\nAnd Thunderbird is installed with Google's internal distro, so I don't see how you can pretend Google hates on Thunderbird.  It was dropped largely due to a lack of developer interest."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "I replied to this last time, and unsurprisingly you've ignored my response in this message.\n\nlike...\n\"Imagine fully integrating Google services like GCalendar, GTalk and Gmail into your desktop.\"\n\nThey already do this for the Gnome desktop."
    author: "Ian Monroe"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "All the more reason people should be willing to explore this in KDE as well."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-02
    body: "google is not interested in a partnership with KDE.\n\nto the person who said Google isn't evil because they are anti MS, just more microsoft bullshit from the linux community.  I trust you know the set up of google, and their close ties with the CIA including some of their top personal being former CIA.\n\n\n\n"
    author: "anon"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Conspiracy theory much?\n\nThe US Government was demanding private search data.  Yahoo handed it over.  Microsoft handed it over preemptively even before they asked.  Google said they'd fight in court before they handed over one ounce of private data to the government.  Google was the only search company to even attempt to fight Chinese censorship.  They promote open source and open standards.  They give almost all of their services away for free.  Name one evil thing they've done.\n\nThe facts contradict your conspiracy theories.\n\nhttp://xkcd.com/258/\n"
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Imho you are right. I would object to integrating with Google if it's exclusive, but otherwise I prefer Google greatly over their competitors. They spend a lot of energy on 'not being evil', which I respect (it's hard for such a big company to appear non-evil: some people will consider pretty much anything as evil, just because you're big)."
    author: "jos poortvliet"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Just because other companies are bad, doesn't mean that Google is good. Google collects a huge amount of user data though all its web services.\nGoogle is not a charity. Google is a company and the goal of every company is to make as much profit as possible.\nThere are various explanations why Google resisted. It could be that Google didn't want others to find out how much data Google collects. It could have also been a publicity stunt.\n\nIt was just relatively recently that Google agreed to anonymize its saved user data after two years. Without pressure from EU data privacy regulators Google had never done that."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "That's right. I just have to say this:\nEmail clients are great because you can send and receive mail from various servers with various protocols. RSS readers, web browsers, media streamers, etc etc etc are great because you can use them with what you want to use them with at you do not have to use them at all or be online to use them. Some of these apps you are talking about can only be used with one service. So they are IMHO useless. IMPO(in my paranoid opinion) they are dangerous because they keep users masked from the details and promote reliancy on companies and services. Google Talk - what is it? Skype - What is it? Pidgin - what is it? YATE - what is it? etc etc etc ... I think you get my point."
    author: "winter"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "I really like some of the things Google does. However, I dislike much of it. Google is already a company that has a lot of power. It's not, \"we are the good guys you can trust us.\" It's important to think about what happens when one entity controls so much and what they *would* be capable of doing.\n\n"
    author: "winter"
  - subject: "Re: KDE and Google"
    date: 2008-03-03
    body: "Agreed. Either way. Their help and their \"coding power\" at the moment would be very appreciated. \n\nKDE is still GPL 3'ed so we don't have to worry about Google taking the project away from the community.\n\nAt the very least Google could help contribute some.\n\nHopefully even BEFORE Google Summer of Code. :)\n\nKDE 4.1 is not too far away. It's nearing crunch time.\n"
    author: "Max"
  - subject: "Friends, not Partners"
    date: 2008-03-04
    body: "NO! Don't be stupid. KDE and Google should keep their relationship as is - just friends.\n\nI don't trust Google enough to let KDE marry him."
    author: "kwilliam"
  - subject: "Re: Friends, not Partners"
    date: 2008-03-04
    body: "Why would they marry? Do you even know what open source means?\n\nKDE is destined to forever bachelorhood, thanks to GPL V.3. Google can only help. They can't harm it, as the KDE team won't let that happen.\n\n"
    author: "Max"
  - subject: "Re: Friends, not Partners"
    date: 2008-03-05
    body: "Right now, KDE and Google are not friends. Google and Apple are friends. GMail works w/o hacks in Safari, but Konqueror requires hacks to work with GMail (Standard View)."
    author: "riddle"
  - subject: "Re: KDE and Google"
    date: 2008-03-04
    body: "> I posted this a week or two ago and didn't get many replies, but the Dot has been busy and it may have been missed.\nThe Dot is probably the least suitable place to post this kind of suggestions. Those things simply do not happen by having a 'great' idea, but by making things happen. So if you're that fond of you idea, start implementing libraries for all those great services now, call someone at Google to pay you for it.\n\nOtherwise, I'm afraid your suggestion sounds like wannabe-strategist-ideas-with-nothing-behind-it.\n\nIn general, integrating commercial services is fine, making KDE dependant on a large company is out of the question. Google being evil or not, it's just very stupid and against pretty much all KDE stands for.\n\nThe fact that you post this on the Dot suggests to me that you don't understand how KDE works, and that you also don't understand how corporate partnerships work, or are being worked on. Sorry, no dice."
    author: "sebas"
  - subject: "Re: KDE and Google"
    date: 2008-03-04
    body: "\"making KDE dependant on a large company is out of the question. Google being evil or not, it's just very stupid and against pretty much all KDE stands for.\"\n\nGood thing KDE isn't dependent on TrollTech, and thusly Nokia, who has been repeatedly anti-OSS and anti-open-standards.\n\nGood thing KDE doesn't have a financial board (oh wait, they do!), nor do they court companies to attain \"Patron of KDE\" status (oh wait, they do!)\n\n\"you don't understand how KDE works\"\n\nPot, kettle, black."
    author: "T. J. Brumfield"
  - subject: "Re: KDE and Google"
    date: 2008-03-04
    body: "You're somewhat mistaken here. While I agree with your concern on dependence on TT, it's important to point out that KDE e.V. is NOT KDE, and has no role in development. Hence, KDE does not have a financial board, only KDE e.V. does. Though the \"patron of KDE\" label is indeed inaccurate then.\n"
    author: "SadEagle"
  - subject: "Re: KDE and Google"
    date: 2008-03-04
    body: "Unlike Mozilla, whose development was originally Netscape-centric, KDE has a strong tradition of volunteer run development, with only a few paid developers in its history. Hence, there wouldn't be an easy way of setting up partnership...\n\nAs for your specific examples... IMHO, it's against everything KDE stands for: a well integrated desktop that gives you control over data, using open data interchange standards. You'd be replacing high-quality, high-integration, standards-following apps like KMail, using IMAP/POP3/SMTP; KOffice using OpenDocument, with web apps using proprietary technologies, with unclear privacy implications to boot.\n\n"
    author: "SadEagle"
  - subject: "Re: KDE and Google"
    date: 2008-03-05
    body: "http://blogs.cnet.com/8301-13505_1-9885248-16.html\n\nPlease, while I understand fanatical worshipping of Foss. Please some pragmatism here.\n\nOr at the very least, please come to terms with reality.\n\nKDE is a joint effort. And there is nothing wrong with commercial support. \n\nI swear sometimes I think open source zealots are the hippies of this decade. \n\nOpen source cannot and should not exist in a vacuum without Big commercial companies. Some open source programmers like to get paid, believe it or not."
    author: "Steve"
  - subject: "Ubuntu brainstorm (again)"
    date: 2008-03-02
    body: "Thanks for the always wonderful commit digest Danny!  For anyone who hasn't yet heard about KDE on Ubuntu Brainstorm, please vote the following idea up.  http://brainstorm.ubuntu.com/idea/478/  .  I know I already submitted this as a comment on the dot elsewhere, but I'd really like to see it get voted up.  The more distros out there that treat KDE like a first class citizen, the better off KDE will be.  Thanks for your vote in advance!"
    author: "scanady"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-02
    body: "I already voted.\n\nTotally agree with your statement. Even non Kubuntu people will benefit from this. :)\n\nPlease vote."
    author: "Max"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-02
    body: "It's at 324 votes for KDE.\n\nWe still have a long way to go until it's in the 4-digits.\n\n-M"
    author: "Max"
  - subject: "Re: Ubuntu brainstorm - current count 374 votes"
    date: 2008-03-03
    body: "I just checked the votes again.\nOur current count is 374. Thanks people for voting. We still have a long way to go though.\n\n-M"
    author: "Max"
  - subject: "Re: Ubuntu brainstorm - current count 376 votes"
    date: 2008-03-04
    body: "376 :)\n\nIt's slowly going up. Gnomies keep voting it down. :("
    author: "Max"
  - subject: "Ubuntu brainstorm - current count 401 !! votes"
    date: 2008-03-05
    body: "We're getting there... 401 votes111"
    author: "Max"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-02
    body: "Me too. Please Vote!"
    author: "Steve"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-02
    body: "I voted for KDE. Did you?"
    author: "Richard M."
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-03
    body: "I voted: now 339. We have a LONG way to go.\n"
    author: "riddle"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-03
    body: "i voted, err.. 338! :( lets go everyone! go over there and place your vote"
    author: "mike"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-03
    body: "I disagree. The way Ubuntu treats Debian is bad, and the way Kubuntu will treat KDE will be bad too. I prefer other company based distributions (SuSE, Mandriva, etc.) to Kubuntu. If just they were dpkg and apt based..."
    author: "Anonymous Coward"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-03
    body: "Why does Canonical treat Debian bad?"
    author: "winter"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-03
    body: "That might be true, but votes still help. The more distro's work on KDE, the more exposure KDE gets. So it's important to vote, even if you're not a Kubuntu fan.\n\n"
    author: "Max"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-03
    body: "why will kubuntu treat KDE badly? and in what way? \n\nI can't think of any other quality dpkg distro's. I think suse is much better quality than kubuntu but yast is a pile of turd. I think that there is a distinct lack of options in KDE distros. They also tend to be more broken in general than gnome based...still prefer kde even if things are more buggy."
    author: "Fool"
  - subject: "Re: Ubuntu brainstorm (again)"
    date: 2008-03-04
    body: "All this is sad but true..\n\nThat's why we need to vote."
    author: "Steve"
  - subject: "Thanks, Martin!"
    date: 2008-03-02
    body: "I'd like to thank Martin Gr\u00e4\u00dflin for being one of the few third-parties to be contributing to the KWin effects (and I'd pre-emptively invite those of you who'd dismiss his efforts as \"pointless eye-candy\" to stick a sock in it, thankyou :)) - I for one hope to see more of this from you!\n\nThere's a youtube video here - as usual, I suspect the standard disclaimers of \"it's faster when you're not running recording software\" apply:\n\nhttp://de.youtube.com/watch?v=sjNKZVKgXP8\n\nNot to reduce his achievements, but this fairly rich effect, with complex motion, fading, 3D stuff, onscreen text etc, is achieved in a scant 818  lines, much of which is comments and general boilerplate:\n\nhttp://websvn.kde.org/*checkout*/trunk/KDE/kdebase/workspace/kwin/effects/coverswitch.cpp?revision=779045\n\nI wonder if the reason more people aren't stepping up to help with this is that they feel it must be much harder than it really is?"
    author: "Anon"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "Beautiful! How much code in compiz for a similar effect I wonder?"
    author: "Matt"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "I wonder why there are Youtube video's showing the KDE effects ported to Compiz ;-)\nI thought Compiz already had every effect under the sun (after all, they've been around for a while), yet apparently the KDE ppl still managed to come up with some cool and must-have effects Compiz wanted and even showcases on Youtube..."
    author: "jospoortvliet"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "This is awesome. We're finally ahead of compiz on something... :)\n\nThey're copying us!@!!!"
    author: "Max"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "Third-parties? He's got an SVN account :).\n"
    author: "Lubos Lunak"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-03
    body: "He probably meant 'anyone who isn't named Lubos Lunak' ;-)"
    author: "jos poortvliet"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "There is a better vid here with reflection and smoother motion:\n\nhttp://de.youtube.com/watch?v=sjNKZVKgXP8"
    author: "Matt"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "Ooops I meant here:\n\nhttp://de.youtube.com/watch?v=dv1Nu4425g8"
    author: "Matt"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "Really cool :)"
    author: "Bobby"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "More effects???\n\nAwesome!!!!!!!!!!!"
    author: "Richard M."
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "I would like to say a big thanks to him too. I just can't imagine computing without eye-candies anymore. As for those who consider them as useless, well they don't have to use them."
    author: "Bobby"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "Same here...\n\nIf it wasn't for eye candies I wouldn't have switched to Linux. Totally true!!"
    author: "Max"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "Thanks for listening and thanks for making more eye candy!!\n\nCan't wait to see the video."
    author: "Max"
  - subject: "Re: Thanks, Martin!"
    date: 2008-03-02
    body: "I love those effects to but I must admit that some things like the selection effect in dolphin, in open/save dialog box... (fade in...) are a little too much :-/"
    author: "Heller"
  - subject: "Re: Thanks, Martin! -love the effects :)"
    date: 2008-03-03
    body: "Thanks Martin Gr\u00e4\u00dflin!\n\nIt's great to see more people are coding cool eye candy. Can't wait to see what you come up with next. I'm on the edge of my seat.\n\n"
    author: "Steve"
  - subject: "Open Bugs"
    date: 2008-03-02
    body: "16124 Open Bugs. That's quite a lot for a open source project.  Are there any plans to close them in the near future?"
    author: "Loki"
  - subject: "Re: Open Bugs"
    date: 2008-03-02
    body: "What - you mean, just literally *close* them, without fixing them?"
    author: "Anon"
  - subject: "Re: Open Bugs"
    date: 2008-03-02
    body: "You have to realize just how big KDE is and how long its been around.\nLots of these bugs may not even be valid anymore, which is why they hold a bug squashing event ever so often.\n\nPersonally I think it'd be nice if I could flag a bug as invalid or cannot reproduce so the admins could close it. But thats a feature request for bugzilla not kde devs."
    author: "Stephen"
  - subject: "Re: Open Bugs"
    date: 2008-03-02
    body: "if you are looking to do bug triage, just let us know and we can give your bugzilla account the permissions to do so. best place to hunt us down for this is on irc in #kde-devel"
    author: "Aaron Seigo"
  - subject: "Re: Open Bugs"
    date: 2008-03-02
    body: "I think this huge amount could be reduced rapidly with something like 'Bug Reproducing Weekends', so in a few months, people just start to reproduce bugs (in weekends).I think Many of bugs will go away."
    author: "Emil Sedgh"
  - subject: "Re: Open Bugs"
    date: 2008-03-03
    body: "I like this.\nMaybe make the bug reporting easier accessible from the desktop with a reminder for bug reporting weekends. :)"
    author: "Max"
  - subject: "superkaramba?"
    date: 2008-03-02
    body: "(not trying to emulate aseigo, shift key not working. damn ms wireless kb) \n\ni might be modded a troll here, but this is a legit question: i thought one of the goals of plasma was to get rid of all these disjointed pieces of software that composed kde.  and from what i'm seeing, plasma is indeed integrating sk's functionality, already.  so why is superkaramba still being worked on?\n\n(and, no, this is not flamebait)\n"
    author: "bruno m."
  - subject: "Re: superkaramba?"
    date: 2008-03-02
    body: "Compatibility.  The Plasma devs want as many kinds of Widget (Dashboard, Superkaramba, Opera etc) to be supported as possible."
    author: "Anon"
  - subject: "Re: superkaramba?"
    date: 2008-03-03
    body: "Does this mean that SuperKaramba is Plasma-based? Or is a different codebase? I was wondering more or less the same than the original poster."
    author: "suy"
  - subject: "Re: superkaramba?"
    date: 2008-03-03
    body: "No, Plasma is something completely new. However, it has been designed to easily add support for other widget types. As most widget systems are based on HTML engines (Dashboard -> Webkit, Superkaramba -> KHTML, Opera -> Presto) and use normal web technologies such as Javascript and CSS it is quite easy to support them in Plasma without any legacy code."
    author: "Stefan Majewsky"
  - subject: "SK Widgets on KDE 4.0.1"
    date: 2008-03-04
    body: "I just copied a link that shows SuperKaramba widgets being added to the Plasma Desktop http://www.kdedevelopers.org/node/3305\nHas anyone here got it working and could you please explain how it's to be done?\nI would be happy to get Liquid Weather working.\nThanks."
    author: "Bobby"
  - subject: "Nepomuk and Dolphin"
    date: 2008-03-02
    body: "I don't know if it is my fault but since KDE 4.0.0 rating and comment files in Dolphin don't work, only tags are remembered.\nI remember that this used to work for me prior 4.0.0 release, and it was a handy feature.\nIs this only happening to me?\nI'm using current 4.0 svn branch with soprano 2.0.2."
    author: "Josep"
  - subject: "Re: Nepomuk and Dolphin"
    date: 2008-03-03
    body: "There was an issue in KDE 4.0.0 that the rating has not been remembered, but commenting files always worked AFAIK. The rating issue has been fixed for KDE 4.1 by Sebastian Tr\u00fcg, but has not been backported yet. I'll check with Sebastian whether backporting this bugfix is possible."
    author: "Peter Penz"
  - subject: "Re: Nepomuk and Dolphin"
    date: 2008-03-03
    body: "Works for me on 4.0.2."
    author: "Stefan Majewsky"
  - subject: "Speed? SPEED?"
    date: 2008-03-02
    body: "I must admit that I was, in the beginning, rather sceptical of the new look of KDE4. But I use it now for weeks, and I like it. It's great, it's the right way to go, and I see the potential, and I like it. I have to say this in advance, because in day-to-day usage, the shortcomings are still obvious. All the new features mentioned in the commit digest don't help here...\n\nHowever, despite everything, despite all new features, is there anybody working at thing like speed and stability?? KDE4 (openSUSE, snapshot always updated) keeps to suck for me in this respects :(.\n\nExamples:\n\n\"Show desktop\" icon: Takes seconds\n\"Log off\": When \"darkening\" the desktop for the log off window, I can in fact see the deskopt redrawn line by line. Haven't had that since my early Amiga times.\n\"Desktop effects\": unusable because of speed, bugs, and stability since the beginning of KDE4\n\"Settings\": .kde4-directory has to be erased every couple of days\n\nand so on... everything feels sluggish. I can't pinpoint anything, it's just that Windows XP on the same machine runs like hell, and KDE feels like having lost 1 Ghz or something like that.\n\nThe machine is a AMD dual core with an GeForce 6. The proprietary NVidia driver should have been correctly set up. \nHowever, I don't really see why me as a user should have to fiddle with settings for the festest way to do desktop effects and stuff, and doing \"NVIDA_HACK\" environment variables and stuff like that... but that's another point. Even in the most basic settings KDE4 is still way behing of Windows in terms of \"catchiness\" and \"stability\" on this machine, and this I don't like.."
    author: "KDE user"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-02
    body: "Definitely a video driver problem.  The drawing performance is so poor that it makes everything appear sluggish.  I haven't had any problems with my geforce but lots of other people have.  I also sometimes see it on the EeePC.  Sometimes it will be fast, then other times it will be glacially slow.\n\nSo you have the proprietary drivers set up?  (it shows the nvidia logo when X starts).  Might be worth a try with the nv driver.  Might actually be faster if you don't use opengl stuff."
    author: "Leo S"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-02
    body: "> \"Show desktop\" icon: Takes seconds\n\nthat's because we're creating a full screen widget to do this with. there are no x11 wm hints to do what we want properly yet, so we have to do it all manually. we delay the cost to the first time you hit the button rather than at start up, to keep start up fast. once we have the needed wm hints, i hope this can be sped up somewhat.\n\n> \"Settings\": .kde4-directory has to be erased every couple of days\n\nugh. it would be great if you could try and pinpoint what it is in there that causes the problems so we can fix it. deleting the whole thing doesn't help much. perhaps try moving it aside, adding half the files in share/config/ back and restarting the desktop .. and do a binary search from there to find the culprit.\n\nthe rest definitely sounds like graphics driver issues.\n"
    author: "Aaron Seigo"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-03
    body: ">> \"Show desktop\" icon: Takes seconds\n> that's because we're creating a full screen widget to do this with.\n\nHm. Whatever the reason for this, it doesn't seem to be the best solution ;). Concerning the desktop icons, I BTW cannot resist the feeling that KDE4 is in many aspects taking a somehow awkward approach (e.g. the way ~/Desktop is handled), but I also know that \"desktop icons\" have been quite a controversial issue ;))).\n\n> ugh. it would be great if you could try and pinpoint what it is in there \n> that causes the problems so we can fix it. deleting the whole thing \n> doesn't help much. \n\nWell, most of the time deleting only \"plasmarc\" does the job, too. However, this might also be an openSUSE (development version) related thing. After all, I seem te be not the only one experiencing this.\n\nIt's also very hard to pinpoint - it works a few days, and then one day I try to start and the whole desktop stays blank. As there are openSUSE updates in beteween, too, you cannot really say what was the reason. It's pretty annoying nevertheless, and no programm should IMHO crash because of problems parsing a config file.\n\n> the rest definitely sounds like graphics driver issues.\n\nPossibly. The frustrating thing is that anything else (compiz, KDE3 compositing effects, 3D and so an) works flawless on this computer :(. What makes KDE4 so different (for me and lots of other users)?\n"
    author: "KDE user"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-03
    body: "from what i know, there is a severe bug in the nvidia binary drivers.\n\nQT 4.4 uses a special function in that driver, that nobody else realy uses. and it uses it very often. This means, only kde 4 is affected by that nvidia bug. Lets hope it gets fixed soon."
    author: "Beat Wolf"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-03
    body: "that rare function is, afaik, most of XRENDER functionality - which really sucks in NVidia binary."
    author: "jos poortvliet"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-03
    body: "Hm. Aren't the effects done by OpenGL and not by XRENDER?"
    author: "KDE user"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-03
    body: "> What makes KDE4 so different (for me and lots of other users)?\nit's brand new ;-)\n\nSeriously, that's it. There are many issues in underlying technologies (Qt, X.org, drivers) and in KDE itself which KDE 4.0 is supposed to expose - and as you can see, it does. This bites users, sure, but thanks to this exposure we can expect KDE 4.1 to be much more stable and predictable.\n\nOf course, users helping to fix the issues is important to KDE: if you could find the issue for the plasmarc breakage, for example, that would be really great."
    author: "jos poortvliet"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "Well put. That's why we pay you the big bucks. ;)"
    author: "Ian Monroe"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-03
    body: "Have to agree with the speed. Though not of this kind, MS Vista seems to be rocket fast compared with KDE 4.0.1. But I am encouraged to wait for future patches. \n\nI use 4.0.1 (latest patches) on openSuse 10.3 with latest NvIDIA driver on 8400GT.\n\nmy problems are:\no when starting a drag-action, I have to push the mouse button for a few seconds (at least too long) in order that KDE understands that a drag event is started.\no Sometimes dialogs just need ages to be drawn (for example kcontrol, kget->save-to)\no I could associate most of the speed issues with KNotify4 and plasma. In plasma, some Qt bugs (from what I have learned) yield 100%cpu usage (for a few plasmoids). For KNotify4 - well - this is a really dangerous software!  On my box, it almost runs all the time. Just switch windows and desktops for several time and KNotify wakes up to run and run... at least 80% cpu usage even when the system is idle. The same goes for system sounds: The first time KNotify4 plays a system sound, the complete desktop freezes for a minute... I already filed a bug report, please vote for it!\n\n"
    author: "Sebastian"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "Currently the nvidia driver is broken in regards to kde4. the only solution as a nvidia user right now is NOT to use kde4 and wait for a corrected version of the driver (which could take anything between 2 months and 1 year)."
    author: "Beat Wolf"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "I'm afraid your sample size of 1 is invalid, for me everything is working fine (nvidia+opensuse10.3+kde4.0.*).  Undoubtedly there seem to issues for some users but such a sweeping statement is uncalled for and inaccurate. "
    author: "daz"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "His sample size is more than one; I've read multiple complaints on the Planet. I've got the same problem myself. You are only the 2nd person I've heard of who has an nvidia card that works fine with KDE 4.0. Consider yourself lucky!"
    author: "kwilliam"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "> You are only the 2nd person I've heard of who has an nvidia card that works fine with KDE 4.0\n\nOh, then you are probably working at the nvidia-helpcenter? :) Well, I did run KDE4 so far on multiple configurations with nvidia's and never had a problem there. So, probably the \"multiple complaints on the Planet\" don't reflect most ppl?!\n"
    author: "Sebastian Sauer"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "What nVidia cards are we talking about here?\n\nOpenSUSE 10.3 says that I don't have a 3d card at all,\nbut Mandriva greets me with a big fat nVidia logo and works \nlike a charm. (GeForce FX 5600)"
    author: "reihal"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "Probably a dumb question, but: Do you have the NVidia drivers installed? (Look at en.opensuse.org/Nvidia for details.)"
    author: "Stefan Majewsky"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "eeh.... no.\nToo lazy, I'll stick with Mandriva."
    author: "reihal"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-04
    body: "Just to correct all previous posters to my comment:\n\nThe NVIDIA issue is only one thing - it indeed feels slower, but not in a way that KDE4 is unusable. My impression is that compositing is just not optimized yet. \n\nMy impression is that other things like KNotify4 and the Oxygen style urgently need stablization/optimization. "
    author: "Sebastian"
  - subject: "Re: Speed? SPEED? -slow :("
    date: 2008-03-03
    body: "Unfortunately I have to agree.\nIt is kind of slow. I'm sure you guys will figure out how to bring all of the above up to speed by KDE 4.1. I have faith in you."
    author: "Steve"
  - subject: "Re: Speed? SPEED?"
    date: 2008-03-31
    body: "I don't seem to be affected by these strange problems using KDE 4.0.2 w/ openSuSE 10.3.\n\nWell I could say that I guess ATI FGLRX proprietary drivers are not so bad after all ;) however this I know not to be the case lol. For desktop KDE 4 usage however, sounds as if FGLRX screams (fast) in comparison to NVidia drivers...\n\nJust go tell NVidia on their forums that ATI FGLRX is faster at KDE 4. I GUARANTEE YOU that if you do this (and report a bug to their driver team about it with very specific test conditions and system setup, with benchmark comparisons), if you do that they will have it fixed in 30 to 90 days.\n\nFYI, when I used a GF 5900 Ultra and reported 12 bugs in one session (using the aforementioned criteria) it was fixed in 14 days. TWELVE BUGS IN TWO WEEKS SQUASHED. While I do not believe I was the only person reporting all of those bugs, I do know for a fact that atleast 4 of them (which affected a vast number of applications) my information/instructions helped to reproduce the bugs in their labs.\n\nIf you do not report a bug \"properly\", then do not expect it to ever get fixed...\n\nRegards,\ngot root\n\n"
    author: "root"
  - subject: " WebKit"
    date: 2008-03-03
    body: "Will it be ready for kde4.1 ? "
    author: "Zayed"
  - subject: "Re:  WebKit"
    date: 2008-03-03
    body: "Well yes, it's actually part of Qt 4.4."
    author: "Mark Kretschmann"
  - subject: "Re:  WebKit"
    date: 2008-03-03
    body: "That doesn't mean that WebKit can be used by Konqueror. I hope it will be done (at least optionally)."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re:  WebKit"
    date: 2008-03-03
    body: "There's been some activity here by mr Wolfer :\nhttp://websvn.kde.org/trunk/playground/libs/webkitkde/?sortby=date\nSo maybe... "
    author: "Shamaz"
  - subject: "Re:  WebKit"
    date: 2008-03-07
    body: "Yes, you can compile the Webkit-KPart. It's working mostly, you can e.g. change Konqueror from KHtml to Webkit...\n\nLukas"
    author: "Lukas"
  - subject: "Re:  WebKit"
    date: 2008-03-06
    body: "Nor does that mean it will be done well."
    author: "anon"
  - subject: "Re:  WebKit"
    date: 2008-03-03
    body: "Cool. KDE 4.1 will be an exiting release.\n\nAaron Seigo, please make another keynote video when it is ready to be shipped. It helps users a lot, at the same time it creates buzz for the KDE project."
    author: "Max"
  - subject: "KGET Web Interface"
    date: 2008-03-03
    body: "I'm not sure I like this. From a architectural viewpoint, I think this is not the way to go. For apps like this, a separate \"engine\" should be made, wich can run as a daemon on a server. Then, one can choose a client to communicate with the server: a KDE/QT client, a web client, a CLI, etc. For a single machine setup, the daemon runs on the local machine. One could even accept multiple deamons. Look at the way MLdonkey does it. That's the correct approach. Same thing for Amarok: Create a amarok \"engine\" that run's as a daemon on a server, then use Amarok (the KDE app) for communicating with it. Next, a web client, a CLI, etc could be made. I'm sure more apps could be architected this way. Use open standard for the communications where available. Another app that comes to mind is Ktorrent.\n\nJust my $0.02"
    author: "Fred"
  - subject: "Re: KGET Web Interface"
    date: 2008-03-03
    body: "Bah.  I still have yet to see an app like that that makes any sense for most users.  This design adds another layer of failure potential, and a UI that most users shouldn't care about.  I remember mldonkey or a similar app, and you had to start some server in the background, and half the time it would work, and half the time it wouldn't.  Then that server wants to run as a daemon, but really I dont care about that, and I only want it to be running when I start the app.  \n99% of amarok/kget/ktorrent users will only use it on the local machine.  So the server architecture only brings downsides for them (reliability, memory consumption, ui complexity)."
    author: "Leo S"
  - subject: "Google Summer of Code submissions"
    date: 2008-03-05
    body: "http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008\n\nPlease provide more submissions to Google Summer of Code projects.\n\nPlease also tell all your friends about GSoC, both mentors and students that would like to make extra cash in the summer.\n\nMaybe we could have a separate dot topic for this, so more people notice. (just a suggestion)"
    author: "Max"
  - subject: "Re: Google Summer of Code submissions"
    date: 2008-03-05
    body: "http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas\n\nThe specific thread. :)"
    author: "Max"
  - subject: "Kwin Desktop effects at Google Summer of Code?"
    date: 2008-03-05
    body: "Will there be any new Desktop effects for Kwin at this years GSoC?\n\nI looked at the site, and there don't seem to be any projects submitted under Kwin. Are they under a different heading, or am I just not looking in the right place?"
    author: "Max"
  - subject: "Re: Kwin Desktop effects at Google Summer of Code?"
    date: 2008-03-05
    body: "I mean compositioning effects."
    author: "Max"
  - subject: "Re: Kwin Desktop effects at Google Summer of Code?"
    date: 2008-03-05
    body: "Those are not project submissions but a list of ideas what could be used as a SoC project.\n\nThese are mainly things that we think would be useful but haven't time for ourselves or are specifically interesting for new developers (e.g. don't require much internal knowledge)\n\nAll students interested in doing a SoC project with KDE are encouraged to think of something interesting themselves and apply with that idea.\nThat might not only be more interesting the stuff we thought about, but also introduces new views and new use cases.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Kwin Desktop effects at Google Summer of Code?"
    date: 2008-03-05
    body: "Oh Cool....\n\nCould you put Compositioning effects on there as a cool idea though? That would be great."
    author: "Max"
  - subject: "Great to watch it grow..."
    date: 2008-03-07
    body: "Hey there. I've been building KDE4 from trunk since it produced anything clickable on the desktop. I updated almost every second day and watching your progress was really fun. Lately I've been on a trip for about a week and was away from my PC. Rebuilding took a while longer but it is absolutely wicked what can change in just one week of absence.\n\nI've never been this deep into a live development process - sometimes it makes me think I've become the wrong type of engineer ;-)\n\nKeep up the great work! \n\nCU and greets form Germany\ndanielHL"
    author: "danielHL"
---
In <a href="http://commit-digest.org/issues/2008-02-24/">this week's KDE Commit-Digest</a>: More work on runners, bindings into WebKit, and the beginnings of better composite manager support in <a href="http://plasma.kde.org/">Plasma</a>, with support for multiple feed sources in the RSS Plasmoid. The addition of a "BBall", "Show Desktop", "KMLDonkey", and "IM Presence" (using <a href="http://decibel.kde.org/">Decibel</a>) Plasma applets. An alternative alt+tab window switcher (similar to Compiz Fusion's "Cover Switch" effect). <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> gets support for Qt 4.4 "widgets-on-canvas". A long-overdue "major" rewrite of KCron is undertaken. Undo support in the KShortcutsEditor dialog. New plugins in <a href="http://www.digikam.org/">Digikam</a> and <a href="http://www.koffice.org/krita/">Krita</a>. Various improvements in <a href="http://ktorrent.org/">KTorrent</a> and <a href="http://amarok.kde.org/">Amarok</a> 2.0. Interface work and MusicBrainz integration in KsCD (student project). Lots of work on page transition effects in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. The start of work on integrating online reader support into <a href="http://akregator.sourceforge.net/">Akregator</a>. Kubrick, a Rubik's Cube game, is imported into playground/games. KDiamond moves from playground/games to kdereview, Kollision from kdereview to kdegames for KDE 4.1. kdebase (trunk, KDE 4.1) now requires Qt 4.4. Akonalendar (a small app to demonstrate <a href="http://pim.kde.org/akonadi/">Akonadi</a> KCal models), and the Quasar graphics library are imported into KDE SVN. <a href="http://commit-digest.org/issues/2008-02-24/">Read the rest of the Digest here</a>.



<!--break-->
