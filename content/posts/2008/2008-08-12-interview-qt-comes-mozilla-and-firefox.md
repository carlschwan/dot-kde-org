---
title: "Interview: Qt Comes to Mozilla and Firefox"
date:    2008-08-12
authors:
  - "jriddell"
slug:    interview-qt-comes-mozilla-and-firefox
comments:
  - subject: "Interesting"
    date: 2008-08-12
    body: "It is interesting to read that this time around, there is cooperation from Mozilla for a porting effort. If I remember correctly, this is where previous efforts have failed in the past. I hope this will develop into a viable alternative rendering engine. It would be good to be back in the old days where you could switch your rendering engine in Konqueror between the Mozilla engine and KHTML. Maybe this time around, we will have three options?"
    author: "Andr\u00e9"
  - subject: "Re: Interesting"
    date: 2008-08-15
    body: "Yes, I really hope this gets developed and refined to the point where we have a Mozilla Firefox KPart.  It would be just lovely to be able to switch back and forth between the KHTML KPart and the Firefox KPart for displaying pages in Konqueror.\n\nEven lovelier if the Firefox KPart would interact properly with its container to switch to a different KPart depending on the content type of a clicked link.  I love having text files come up in Konqueror using the Embedded Advanced Text Editor KPart.  And having URLs passed to the appropriate KIO-aware applications like KPDF based on content type.  KDE really got it right with KIO and KParts, and that is the main reason why I use Konqueror as my primary browser.  Every other browser, Firefox included, seems awfully primitive with its treatment of content types and helper applications."
    author: "Matt Whitlock"
  - subject: "KHTML?"
    date: 2008-08-12
    body: "Hi\nall news around is about Webkit...but i really want to know whats happenning on khtml side.how is the SVG stuff going? was that bytecode interpreter got merged into trunk/? what happened to WYSIWYG editors support, etc..."
    author: "Emil Sedgh"
  - subject: "Re: KHTML?"
    date: 2008-08-12
    body: "Yes, what is going on the KHTML?"
    author: "Zayed"
  - subject: "Re: KHTML?"
    date: 2008-08-12
    body: "KHTML is going to have a hard time trying to keep up if they want to continue a completely separate development. A more practical and viable approach would be trying to port whatever improvements are unique in KHTML to the current QtWebKit. I imagine several obstacles will need to be overcome if this is to happen, not the smallest of them being the NIH syndrome. OTOH consider the fact that the more WebKit code does not belong to Apple or TT/Nokia the freer the resulting library will be."
    author: "hmmm"
  - subject: "Re: KHTML?"
    date: 2008-08-12
    body: "Another option: port whatever improvements are unique in WebKit to KHTML. The resulting library will be even freer."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KHTML?"
    date: 2008-08-12
    body: "1) how is the SVG stuff going?\n\nVery well.\n\n2) was that bytecode interpreter got merged into trunk/?\n\nThe initial version is in KDE 4.1, and is a nice improvement \nover KJS4.0.x\n\n3) what happened to WYSIWYG editors support\n\nThe basic version is in KDE 4.1 --- the fundamentals work,\nbut the command set support is somewhat limited.\n\n"
    author: "SadEagle"
  - subject: "Re: KHTML?"
    date: 2008-08-13
    body: "Are you not surprised that you get this kind of questions?\nNormally every project tries to tell the users at least a little bit what the current state is, but KHTML does nothing like it - not at all.\n\nHeck, there are enough bloggers out there who would love to bring news about KHTML to the masses, but no, the KHTML project doesn't publish any information, it could... well, do something. I honestly don't understand the reason behind that. It's just sad :("
    author: "Blubb"
  - subject: "Re: KHTML?"
    date: 2008-08-14
    body: "http://vtokarev.wordpress.com/ \nIs the developer's blog for the current SVG SoC project.\n\nFrostbyte was actually announced in SadEagle's blog. \n\nWhere else would you like? I'm curious.\n\n"
    author: "blauzahl"
  - subject: "Addons"
    date: 2008-08-12
    body: "That are amazing news! Will it be possible to use existing Firefox addons with the Qt version? That would be a real killer feature for a Qt based Firefox Browser in KDE. "
    author: "schulte4e"
  - subject: "Re: Addons"
    date: 2008-08-12
    body: "No reason why not - extensions are just interpreted, XUL-ly code :)"
    author: "Anon"
  - subject: "Re: Addons"
    date: 2008-08-12
    body: "Yes, extensions work flawlessly.\nPlugins like Flash on the other hand crach Firefox-Qt in it's current state, but that's pre-alpha software for ya. ;-)"
    author: "Jonathan Thomas"
  - subject: "Re: Addons"
    date: 2008-08-12
    body: "s/crach/crash\nblargh."
    author: "Jonathan Thomas"
  - subject: "Re: Addons"
    date: 2008-08-12
    body: "By pre-alpha software, do you mean Flash? :-)\n"
    author: "SadEagle"
  - subject: "Re: Addons"
    date: 2008-08-12
    body: "Nope, Firefox."
    author: "Jonathan Thomas"
  - subject: "Re: Addons"
    date: 2008-08-12
    body: "Flash 10 is actually release candidate now.\nFlash 9, well, eww."
    author: "ethana2"
  - subject: "Awesome"
    date: 2008-08-12
    body: "This is great! I think that Firefox is the only GTK+ application on my system, so this will be VERY welcomed :D"
    author: "Lucianolev"
  - subject: "Re: Awesome"
    date: 2008-08-13
    body: "You should install Gimp and Inkscape. They are still much better then Krita and Karbon14."
    author: "Tobias"
  - subject: "Re: Awesome"
    date: 2008-08-13
    body: "The interface of Gimp drives me up the wall.  I can't draw so I don't use Inkscape, but I find Krita and Digikam take care of all my image manipulation/editing needs just fine."
    author: "T. J. Brumfield"
  - subject: "Re: Awesome"
    date: 2008-08-13
    body: "Gimp and Krita really aren't any more comparable than Kate and KWord. Sure, Kate and KWord are both text editors, but beyond that the target audiances are pretty much unrelated.\n\nGimp, like Photoshop, is much more so meant to be photo-manipulation software first and foremost (hence the name for both), although it is frequently abused to create images. Krita, like Corel Painter, is intended much more so far artists wanting to paint pictures (not to manipulate photos).\n\nOf course, you're also assuming that he actually wants to use *any* of those programs."
    author: "Kit"
  - subject: "Brilliant!"
    date: 2008-08-12
    body: "Great to hear. Firefox has always seemed out of place, and this was possibly top of my wishlist for... all software really.\nNice to see it's coming along well, hope it's all finished soon - default in Intrepid maybe?\nPerfect way to polish up KDE 4.1."
    author: "Andy Smith"
  - subject: "Awesome bar with Nepomuk integration"
    date: 2008-08-12
    body: "What I would really like to see is something as the awesome bar integrated with nepomuk. Either an awesome bar in konqueror with nepomuk integration or integrating the awesome bar of firefox with KDE and make it use nepomuk would be great.\n\nImagine, for example, opening krunner and start typing the tag of one of your bookmarked sites, then krunner does its magic, you click enter and you have your web browser opening the page that you want."
    author: "Raul"
  - subject: "Re: Awesome bar with Nepomuk integration"
    date: 2008-09-04
    body: "That would be.. umm.. awesome :-) What bothers me about most web browsers is that they do not use the system stores for bookmarks, certificates, history, et cetera, so you end up with lots of silos of data. Hopefully nepomuk integration can go some way to remedying it."
    author: "Chaz6"
  - subject: "Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "This is WONDERFUL! Imagine Firefox integrating perfectly with the KDE4 Oxygen style! Most importantly: ARE THEY GOING TO FIX THE FILE > SAVE DIALOG??? The darn GTK file open/save dialog has bugged me for ages. I've always been mad that Firefox doesn't detect the DE and show the KDE file dialog on KDE desktops. (I guess I could say the same for the GIMP, but the GTK+ *is* \"The GIMP Toolkit\" so I figure they have a right to promote it. ;-)\n\nI could imagine some other useful things coming from a more KDE-centric Firefox, such as using kuiserver for downloads (http://pindablog.wordpress.com/2008/07/24/more-on-kuiserver-and-extenders/), and using Nepomuk to store bookmarks. That would be so nice. Thank you Nokia, for starting this!"
    author: "kwilliam"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "Actually it still doesn't use the KDE file dialog. It uses the Qt one.\n...but at least it isn't the Gnome one. :P"
    author: "Jonathan Thomas"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "...and starting with KDE 4.1, Qt apps will use the KDE dialog (unless they use some special functions)."
    author: "christoph"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "Currently, Firefox-Qt displays the Qt file dialog, as does several other Qt apps I tried. In fact, it only seemed to use the KDE file dialog with Qt Designer."
    author: "Jonathan Thomas"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "I'd love that!\n"
    author: "Mark Hannessen"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-16
    body: "That only works if the app links kdelibs and is a KApplication, as it's KDE overriding QFileDialog's static functions, not Qt loading KDE's KFileDialog by itself."
    author: "Kevin Kofler"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2009-01-09
    body: "i hate the gtk and qt file dialogs too. For a long time I used this to get kde file dialogs from gtk apps such as firefox:\n\nhttp://www.kde-apps.org/content/show.php?content=36077"
    author: "Alan Ezust"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "My open/save dialogues look ok, but are missing the \"Places\" section down the side - so say I'm trying to attach a file from a USB stick, I have to copy it into my home directory first, very irritating. I would love to see full native Dolphin-style dialogues."
    author: "Andy Smith"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "There's a neat trick available in Firefox:\nEnter about:config in the location bar, then search for 'ui.allow_platform_file_picker' and set it to false.\nRestart Firefox and voila. It's not the KDE dialog, but it's better than the GTK crap."
    author: "Gusar"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "Works and makes ff much more usuable."
    author: "dave"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "Wow, thanks.  It's basic, but at least has some usefullness rather than the default. "
    author: "Leo S"
  - subject: "You *CAN* get the KDE file-picker, right now. "
    date: 2008-08-16
    body: "Yeah, the \"built-in\" file picker (which results from setting that pref to \"false\") was written by a mozilla bigshot (asa) who agrees with us that GtkFileChooser looks like ass, and lacks important capabilities (e.g., precise modified TIMES instead of just dates, and a <size> column). But you can get exactly what you want (the KDE 3.5.x filepicker, WITH BOOKMARKS AND ALL CONTROLS) if you're still back on 3.5.x:\n\nJust Use the kgtk2-wrapper to intercept and \"fix\" the file chooser calls. Anywhere Firefox would normally use GtkFileChooser, you get the KDE panel! With a usable filesize column, all of your bookmarks (and the ability to create more of them), the works! Just run it like this:\n\n/usr/bin/kgtk2-wrapper firefox\n\nMost distros have prebuilt kgtk2-wrapper for you, in some \"kde addons\" type of package. Just install it. If you liked You're gonna LOVE IT!\n\nAnd of course, if you use Thunderbird too, just do this:\n/usr/bin/kgtk2-wrapper thunderbird\n\nI haven't tried wrapping GIMP, but I suspect that it would work too. "
    author: "rick stockton"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "KDE does have a Gimp-like drawing program: Krita.\n\nIt has a cleaner layout. What has always bothered me about Gimp is the lack of a \"Line\" tool. *shrugs*\n\nKeep up your hard work, mozilla team.\n"
    author: "NuclearPeon"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-12
    body: "*click* shift *second clock* voila"
    author: "Long H."
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-13
    body: "KDE does have a Gimp-like drawing program: Krita\n\n\nActually, neither The GIMP or Krita is a drawing program.  They are paint programs.  A drawing program normally supports some sort of vector file format.  So, Karbon14 is a drawing program.  It would be nice if the same program could do both painting and drawing AND support both vector and raster output files."
    author: "JRT"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-13
    body: "Of course Krita can do that. We're talking about a part of KOffice here. It can even do music notation, wordprocessing or work with an (interactive!) worldmap... There is nothing Karbon14 can do and Krita can't, just like there is nothing Krita can do and KWord can't. It's just the interfaces are optimized for their respective tasks. Below, it's one system."
    author: "jospoortvliet"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-19
    body: "All aboard the pedantic train! \n\nNo seriously, you can't just say \"drawing means this, painting means that\"... you don't own the English language, so stop telling me what to say.\n\nI'm sorry about this post, but pedantic stuff like this really gets me going."
    author: "Level 1"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-13
    body: "If you try to use Krita you will see that there is a lot of space for improvements. Until Krita is ready I'll use Gimp, because it's the best free Image-Manipulating-Program."
    author: "Tobias"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-13
    body: "I'll let you in on a little secret: software is never ready. There's always a lot of space for improvements."
    author: "Boudewijn Rempt"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-13
    body: "You are right, but at some point the software is \"ready\" to use. And Krita isn't at that point, jet. "
    author: "Tobias"
  - subject: "Re: Finally, Firefox will \"fit\"!!"
    date: 2008-08-13
    body: "Photoshop must be the one exception.\n\nI've never really heard someone say that Photoshop is missing crucial features they need.  In fact, Photoshop is too powerful to the extent that Adobe has been talking about seriously trimming down Photoshop and making future versions simpler for the average user, while continuing to support CS3 for professionals."
    author: "T. J. Brumfield"
  - subject: "Is Nokia planning to acquire Mozilla?"
    date: 2008-08-12
    body: "Given the fact that Nokia in the last period has done a lot of \"shopping\" I will not be surprised to read of such an acquisition.\nWe will see. Maybe it depends from the result of this porting.\n"
    author: "A lazy KDE user"
  - subject: "Re: Is Nokia planning to acquire Mozilla?"
    date: 2008-08-13
    body: "Uhh, no. The Mozilla Foundation is a non-profit, isn't for sale, and a completely different different mission and goals than Nokia."
    author: "Some Guy"
  - subject: "Re: Is Nokia planning to acquire Mozilla?"
    date: 2008-08-13
    body: "The Mozilla Foundation is non-profit, but the Mozilla Corporation *is* for-profit (they created it for that entire reason). The Mozilla Corporation was created in 2005 as a subsidiary of the Foundation, IIRC.\n\nThough I gotta agree that it doesn't seem very likely, theres a sight overlap but not close to as much as Nokia and TT."
    author: "Kit"
  - subject: "Re: Is Nokia planning to acquire Mozilla?"
    date: 2008-08-14
    body: "Yes sure, an eventual Mozilla acquisition is another matter than the TT ones.\nBut still there are some good reasons to do it:\nNokia's new strategy is heavily based on internet services (http://www.ovi.com) and the convergence between the desktop and the mobile devices. \nThe browser is a fundamental piece of software to access those services.\nOn Mozilla labs are developing technologies to better integrate web services on the browser (http://labs.mozilla.com/projects/weave/) and hence on the device that runs it.\nOtherwise if Nokia isn't interested in Firefox why it's working hard to port FF to Qt? While at the same time if it is interested in FF while not acquire Mozilla given the fact that has acquired everything else (Symbian, TT, a number of social site)?\n\nI'm not an expert on acquisitions, but Nokia has recently bought the whole Symbian (something) and made the Symbian Foundation (fully owned by Nokia). So I'm really curious to know if there is some legal reason why can't Nokia do the same with Mozilla corp and/or foundation in a way or in another to suite their desire to control everything related to its business.\n\n"
    author: "A lazy KDE user"
  - subject: "Re: Is Nokia planning to acquire Mozilla?"
    date: 2008-08-15
    body: "Nokia is clearly interested in Mozilla otherwise they wouldn't have created the Qt port. I don't think we'll ever see Nokia buy Mozilla - they don't need to buy them to influence Mozilla as can be seen from this Qt port. If they need something to be supported, they simply need to code it. "
    author: "Joergen Ramskov"
  - subject: "Is the Maemo using Qt instead of GTK/Gnome"
    date: 2008-08-12
    body: "Why did you do this port?\nBecause we were going to make Maemo use Qt in the future and we were making a browser based on Mozilla for the N810, so now we are making this new browser version based on Qt.\n\nSounds like they are going to base Maemo on Qt in the future. If that is true, I would consider getting one."
    author: "Michael"
  - subject: "Re: Is the Maemo using Qt instead of GTK/Gnome"
    date: 2008-08-13
    body: "From http://maemo.org/news/announcements/view/qt_to_be_supported_in_addition_to_gtk.html\n\nNokia will introduce Qt to the maemo platform in addition to GTK+."
    author: "Tobias"
  - subject: "WebKit is to some extent controlled by Apple"
    date: 2008-08-12
    body: "\"They are also concerned that WebKit is to some extent controlled by Apple, who are in competition to Nokia with their iPhone.\"\n\nIf Nokia is concerned about Apple's control on WebKit, that's a pretty bad sign, isn't it? KDE should be even more worried, because there are less resources to push the development to where KDE needs.\n\nIs normal that Company Foo is only interested to do what is useful for Company Foo's customers, and likewise for Company Bar. But that makes me think if there is an organizational way of making all parties happy, or we will end seeing plenty of branches. I wonder if something like the Java Community Process would had prevented KHTML's fork, or if Apple's secrecy would blow it all no matter what KDE would do on its side."
    author: "suy"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-12
    body: "Which direction does KDE need for WebKit that's incompatible with Apple's? Performance and standards support are best for both.\nAnd it's been said before but if it turens out that Apple is somehow hostile towards KDE, GNOME, Nokia, and Google (Android) then these organisations can fork WebKit at any time. WebKit is free software -- no matter who hosts the SVN repository."
    author: "KAMiKAZOW"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-12
    body: "\"Standard support\" is a nebulous thing. First of all, standards change. \nMozilla people think EcmaScript 4 would be a great thing for the web, \nwhile I think it's a disaster, and hope we'll never have to support it.\n\nA lot of the things that Apple likes to back --- things like CSS transforms, etc., --- \nI would judge to be bloat and hope they would be kept away from the web, \nin part  because I don't think our primary graphics platform is \nwell-suited for it. Of course, I tend to be a minimalist on those things,\nand I am not a huge fan of web applications as-is (well, the most popular ones \nget an increase in the user's mobility at the expense of the degree of their \ncontrol over their data). \n\n\"Performance\" also doesn't exist in vacuum --- often times performance improvements result in increase in complexity and decrease of maintainability of code\nand do not provide any tangible benefit besides looking good on marketing materials\n(and yes, I am guilty of that as well, though largely because performance stuff is fun). Apple also seems to have strayed away quite a bit from KHTML's founding KISS philosophy, in part for performance optimizations.\n\nAnd while it's been said that people can '\"just' fork Foo' repeatedly, lots of wrong things have been repeated many times. And this one is one of them. Most of these organizations are just mainly port masters thus far (well, Google isn't, and KDE isn't involved, period), and there would have to be a lot of readjustment of roles, etc.  to get something like that off-the-ground. And the increased complexity of the codebase, and its readjustment into commercial-vendor centric development make it even more difficult. The community structure and technical roles would have to adjust dramatically. Can it happen? Sure. \nCan you assume that it'll happen if needed? Nope.\n\nAnd yes, people always bring up egcs and Xorg just about now except... those weren't actually forks --- they were disputes over governance of projects between the actual development community and the community's \"leadership\", which basically ended up with the developers telling the leadership to lead the empty set.\n\nHaving said that, I have a lot more trust in the direction of Apple's team than Nokia's/TrollTech, since they:\n\n1) Have at least talked to us consistently over the last few years. I won't call it a cooperative technical relation (there is an awful lot of talking past each other), but a friendly acquaintance is better than nothing, and there were improvements going both ways from some discussions. And really, while I am being 100% unobjective, a \"hi\" goes a long way towards building trust. We have a reasonable understanding of where we stand, of what our differences in technological philosophies are, and I have a great deal of respect for Apple's developers.\n\nOn the other hand, most QtWebKit developers don't even seem to communicate with the KDE development community outside of aKademy and the occasional \"hey, look at cool stuff we're doing\" blog posts. This has fortunately changed a bit lately since Ariya at least hangs out in #khtml.\n\n2) While Apple competes w/Nokia on cell phones, and they compete with KDE on desktop (well, we -wish- we competed with them on desktop, anyway...)  they -are- on desktop, and they value a lot of things we value as well (or at least used to, pre-4.0 *sigh*): consistent, integrated, desktop experience with full-featured native applications. Nokia's business pushes them in an opposite direction, where their focus is near certainly away from the desktop.\n\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "Yes, Apple competes with KDE on the desktop, but Apple also learned that collaboration on underlying libraries helps them just like Novell and Red Hat benefit when both work on the Linux kernel.\nWebKit started rocky but later evolved into a platform agnostic framework. I think it's awesome that KDE technology through WebKit is now being integrated into GNOME.\n\nEven though Apple employs most WebKit developers, the WebKit project itself is now meant to be an independent technology provider. That's something that can't be said about Mozilla. Gecko releases are bound to Firefox releases and that's one major releases roughly every two years. (Gecko 1.8 was released with Firefox 1.5 at the end of 2005, Gecko 1.9 was released just recently.)"
    author: "KAMiKAZOW"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "You know what's funny about this whole nonsense? There have been declarations and forests destroyed (figuratively) about the wonderfulness of Webkit, QtMozilla and on and on.\n\nYou know what I can use today? And have been able to use for the last number of years? Khtml.\n\nI thank you and the khtml crew for that.\n\nOdd isn't it.\n\nDerek  "
    author: "dkite"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "\"+1\", as they like to say."
    author: "Heja AIK"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "For those who love Konqueror, more power to you.  I'm happy you enjoy your browser.  Whether or not KHTML is a great rendering engine I'll likely never know, because I so greatly prefer the features and interface that Firefox offers.\n\nOkular is this great viewer now.  Dolphin is becoming a great FM.  I'd really like to see a dedicated browser.  If Konqueror is going to be a super-app, let it pull from each of these projects dedicated individually to being the best apps they can be.  Konqueror is already pulling kparts from Dolphin.\n\nI hope/assume this is the future of Konqueror."
    author: "T. J. Brumfield"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "Indeed. You are able to use and enjoy Firefox now and have been able to for quite a while.\n\nMy point isn't about which is better or worse. With all the noise about all these wonderful things, it comes down to you having firefox, me having khtml. I am not convinced that anything will change.\n\nDerek"
    author: "dkite"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "Theres a couple dedicated browsers for KDE in the early stages of development. Theres Eureka (http://kde-apps.org/content/show.php/Eureka+Web+Browser?content=84590) which is based on KParts, it looks rather minimalistic (at least currently). Theres also Foxkit (http://www.kde-apps.org/content/show.php/Foxkit+Web+Browser?content=82962, disclaimer: I develop it :P) which directly uses QtWebKit (and not KParts) but really isn't ready for prime time yet.\n\n\"I so greatly prefer the features and interface that Firefox offers.\"\n\nCould you go into some detail here? Just curious since I'm developing a potential competitor :P"
    author: "Kit"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "Add-ons and extensions are great, but I can't see any other browser supporting Firefox add-ons, unless they bring in XULRunner, which isn't likely to happen (I assume).\n\nSome Firefox things I love include dragging and dropping tabs to rearrange them.  I love the awesomebar.  I love the menu and interface being designed for web browsing, and not file managing.  Conversely, I like my file manager interface to be designed file managing, and not web browsing.  I like being able to have my bookmarks in folders, and tag them as well.  I love built-in spellcheck.  I like a built-in download manager.  I like having a built-in RSS reader.\n\nFor all I know, Konqueror does many of these things.  However, I stumble with the interface enough, and prefer some Firefox feature so much more (like Adblock plus and better Flash support) that I can't bring myself to use Konqueror and find out."
    author: "T. J. Brumfield"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-14
    body: ">dragging and dropping tabs to rearrange them\n\nBeen in Konqueror for a long time, perhaps as long as it has had tabs.\n\n>the awesomebar\n\nIt's more advanced than Konquerors current standard location bar. It seem more akin to the Alt-F2 with multiple runners approach of KDE4. Adding Nepomuk to the mix, it's most likely only a matter of time before such advanced features are avaliable in ALL location fields across KDE.\n\n\n>I love the menu and interface being designed for web browsing, and not file managing.\n\nKonquerors profile has handled this for years. My web browsing profile has never been like the filebrowsing one, they are both designed for their different usagepatterns.\n\n> have my bookmarks in folders, and tag them as well\n\nKonqueror does tagging(Comments), and even kfm the KDE1 predecessor did do bookmarks in folders.\n\n>built-in spellcheck.\n\nBuilt in spellcheck in Konqueror predates any Gecko based browser.\n\n>built-in download manager\n\nKGet is a plug-in, but the major distributions install it by default.\n\n>built-in RSS reader.\n\nKonqueror has intergrated support for KDE RSS reader Akregator.\n\n\nKonquerors adblock works very well, altouch it has room for some improvements. Since Flash is broken, improving support are hard until it gets fixed."
    author: "Morty"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-16
    body: "Note that the Akregator plugin doesn't work in konq-plugins 4.1.0 (some required files are not installed), I fixed it for 4.1.1 (and for the Fedora 9 4.1.0 package)."
    author: "Kevin Kofler"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-14
    body: "Hi!\n\nI like looking videos over the web. My problem is, that the flashplugin doesn't produce any sound on my system. It's really a shame, but I tried everything possible from alsa-configuration to oss to different flash versions. No sound at all :( With the video-downloader extension of firefox, that never really bothered me. It's abled to download every embedded media I found till today. \nNow that I switched to KDE4.1, I don't want to use the gtk-version of firefox anymore. Konqueror is pretty slow on my system, but what's much worse: There is no extension that helps me download embedded media and watch it with my favorite media-player. So I ended up using Opera, wich I really don't like, but at least I can download videos with some annoying widget. \nThe hole interface of opera (though it's qt4) doesn't really fit into KDE4 as well, so I have high interest in a video-downloader for konqueror and I guess there are lots of other users penetrated by adobe in the same way.\nMake a plugin for konqueror (or arora), that's abled to find and download embedded media, even if flash is deactivated, and you would have a kickass feature. (Almost) Nobody likes flash :)"
    author: "con"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-14
    body: "Are you using a 64-bit distro?  I've seen some people using Flash in a 64-bit distro that don't have the necessary 32-bit libraries installed to get sound working in Flash.\n\nI'd have to know more about your setup to be sure."
    author: "T. J. Brumfield"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-14
    body: "No, it's a simple 32bit system with an athlonxp 2000 and a soundblaster16 soundcard and sometimes a terratec phase 22. Have this problem since flash moved to alsa and tried several soundcards and linux-distros. Nevertheless, since I'm so used to download all flash-vids I want to see, I wouldn't be happy to have flash with sound but no option to download the media.\n\nWhat just comes into my mind: there are quite some browser-extensions that can detect every embedded media by just getting point to the containing website. Shouldn't it be possible to stream those media to the favorite media-player and giving the choise to download it?\nAgain, I think a lot of people do not actually need flash for interactive websites or flashgames and would therefor be very happy if they wouldn't need this big, buggy and lame plugin inside the browser."
    author: "con"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-14
    body: "You likely need to configure ALSA then.  And you may want to see if you're running the in-kernel ALSA, or the out-of-kernel ALSA.\n\nAs far as embedding media in the web, many people use Flash for a variety of reasons.\n\n1 - It uses less bandwidth for people who don't want the entire clip.  It only streams while I have the page open.\n2 - There is less wait with streaming as opposed to waiting for the while clip to download.\n3 - It (somewhat) prevents you from just downloading the media.  There are extensions to take media from Flash, but it helps with liability to make the effort.\n4 - If you use Flash, you can be pretty sure almost everyone on the web can access the media in the page, as opposed to embedding some other media playing device that might not work as well for some people.\n\nHTML 5 should include new media tags to allow people to just use one tag, and then link to the media.  Each browser/platform will determine then how to open and play that media."
    author: "T. J. Brumfield"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-16
    body: "sure, flash has it's qualities. Also I really tried hard to set up alsa, but it won't work. I have sound in every application, also properly set up primary and secondary soundcard. Everything is fine, but flash won't give a damn bing.\nIt's ok to me, since both firefox and opera offer me possibilities to download the media and view it with my favorite media player. Opera even gives me the possibility to stream the media to vlc, so there is not really a bandwith-problem with this concept. Still I can't use konqueror for not offering me that.\nAlso, I will probably buy a pandora in a couple of months and it's almost sure, there won't be flash available to this device, as well as every non-mainstream device with a different architecture then x86 will not have decent flash support. I guess most users of such devices won't use konqueror because of that. Flash videos are a part of the internet experience and html5 won't solve this to it's fullest. It may improve the situation, but it's pretty sure, a lot of video-portals will keep with flash."
    author: "con"
  - subject: "What I love in Firefox...."
    date: 2008-08-16
    body: "is entirely extension-based:\n\n1) Adblock+, with the BRILLIANT element hiding selector tool, is the most critical.\n\n2) Gotta have mouse gestures, at least as configurable as the Firefox extension \"Firegestures\" (or Opera).\n\n3) Gotta have Flashblock, with a \"whitelist\".\n\n---------\nNice but not absolutely required by me:\n\n4) colorful tabs and tab-mix-plus functionality.\n\n5) sometimes, it's easier to \"rip\" unwanted ads using \"RIP\" and Greasemonkey than Adblock.\n\n6) Mozilla's implementation of \"userchrome\" is really nice, when they mess up something as badly as the \"bookmark\" dialogs in FF3. (Defaults sizes of folder trees are way to small, and I've got lots of space for bigger pop-ups or my 1920x1200 screen). So I just fixed 'em, easy as cake. Fighting with Devs about layout choices is much less satisfactory for everyone, because they *DO* have to fit everything into small screens for other users (eePC, for example). \n\nBTW, here's my list:\nGenerated: Fri Aug 15 2008 20:38:00 GMT-0700 (PDT)\nUser Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008070206 Firefox/3.0.1\nBuild ID: 2008070206\n\nEnabled Extensions: [47]\n- about:safebrowsing 1.0: http://www.google.com/search?q=Firefox%20about%3Asafebrowsing\n- Adblock Plus 0.7.5.5: http://adblockplus.org/\n- Adblock Plus: Element Hiding Helper 1.0.5: http://adblockplus.org/\n- AutoFormer 0.4.1.5: http://autoformer.mozdev.org/\n- Classic Compact Options 1.1.3: http://blog.environmentalchemistry.com/2008/03/firefox-theme-classic-compact.html\n- CLEO 4.0: http://customsoftwareconsult.com/extensions\n- ColorfulTabs 3.3: http://binaryturf.com/\n- Connect to address 1.1.9: http://www.blackbirdblog.it/progetti/connect-to-address\n- Console\u00b2 0.3.9.2: http://console2.mozdev.org/index.html\n- Ctrl-Tab 0.18.3: http://en.design-noir.de/mozilla/ctrl-tab/\n- Download Statusbar 0.9.6.3: http://downloadstatusbar.mozdev.org/\n- ErrorZilla Plus 0.4.4: https://addons.mozilla.org/en-US/firefox/addon/5398\n- Extension Developer 0.3.0.20080526: http://ted.mielczarek.org/code/mozilla/extensiondev/\n- FEBE 6.0: http://customsoftwareconsult.com/extensions\n- FireGestures 1.1.3: http://www.xuldev.org/firegestures/\n- Flashblock 1.5.6: http://flashblock.mozdev.org/\n- Forecastfox 0.9.7.7: http://forecastfox.mozdev.org/\n- Form Saver 0.9.0: http://www.botsko.net\n- Greasemonkey 0.8.20080609.0: http://www.greasespot.net/\n- Html Validator 0.8.4.0: http://users.skynet.be/mgueury/mozilla/\n- JavaScript Debugger 0.9.87.4: http://www.hacksrus.com/~ginda/venkman/\n- JSView 2.0.5: http://forum.softwareblaze.com\n- Linkification 1.3.5: http://yellow5.us/firefox/linkification/\n- Mozilla Quality Extension 0.1.5: http://quality.mozilla.org/\n- MR Tech Toolkit (formerly Local Install) 6.0.1: http://www.mrtech.com/extensions/\n- Nightly Tester Tools 2.0.2: http://www.oxymoronical.com/web/firefox/nightly\n- Nuke Anything Enhanced 0.68.1: http://www.google.com/search?q=Firefox%20Nuke%20Anything%20Enhanced\n- Open Addons 1.0.8: http://openaddons.extra.hu\n- PatchForLibrary 4.4: http://space.geocities.yahoo.co.jp/gl/alice0775\n- Platypus 0.80: http://platypus.mozdev.org\n- QuickJava 0.4.2.1: http://quickjavaplugin.blogspot.com/\n- Redirect Remover 2.5.5: http://redirectremover.mozdev.org\n- Remove It Permanently 1.0.6.4: http://rip.mozdev.org/\n- Right-Click-Link 1.1.3: http://rickardandersson.com/\n- RSS Validator 3.0.0: http://www.nu22.com/firefox/rssvalidator\n- Screen grab! 0.95: http://andy.5263.org/screengrab/\n- Stop-or-Reload Button 0.2.2: http://v2studio.com/k/moz/\n- Tab Mix Plus 0.3.6.1.080416: http://tmp.garyr.net\n- Text Complete 0.9.9.4: http://www.cfavatar.com/textComplete.cfm\n- Tinderstatus 0.2.10: http://tinderstatus.mozdev.org/\n- Toolbar Buttons 0.5.0.5: http://codefisher.org/toolbar_button/\n- Total Validator 5.3.0: http://www.totalvalidator.com/tool/extension.html\n- User Agent Switcher 0.6.11: http://chrispederick.com/work/user-agent-switcher/\n- View Dependencies 0.3.3.0: http://mozilla.queze.net\n- View Frames 1.0: http://mozilla.queze.net/\n- Web Developer 1.1.6: http://chrispederick.com/work/web-developer/\n- YesScript 1.3: http://www.google.com/search?q=Firefox%20YesScript\n\nInstalled Themes: [2]\n- Classic Compact 3.0.9: http://blog.environmentalchemistry.com/2008/03/firefox-theme-classic-compact.html\n- Default: http://www.mozilla.org/\n\nInstalled Plugins: (3)\n- Default Plugin\n- Java(TM) Plug-in 1.6.0_05-b13\n- Shockwave Flash\n\n\n "
    author: "rick stockton"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "...and I've got swampland in Florida to sell you. What worries Nokia about WebKit and Apple is the fact that Nokia doesn't have an equivalent that it OWNS.\n\n"
    author: "Marc Driftmeyer"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-08-13
    body: "Didn't Nokia write the version that runs on scaled down screens, which then showed up on the Iphone?\n\nDerek"
    author: "dkite"
  - subject: "Re: WebKit is to some extent controlled by Apple"
    date: 2008-09-29
    body: "Great post, not sure I agree completely but definitely a good read.\n\nhttp://www.computer-juice.com/forums/"
    author: "Forum"
  - subject: "Time for consolidation?"
    date: 2008-08-12
    body: "I guess a company like Nokia has enough experience with it ;) But jokes aside: I fully understand why people prefer choice and there are lots of good reasons. There is no problem that there is QtFirefox, Konqueror, Dolphin and Arora.  But as an computer scientist it annoys me to see so much code duplication on core technologies. \nQt currently ships with two JavaScript implementations and two SVG-renderes.  Additionally KDE comes with its own JavaScript implementation as well as its own HTML- and SVG-renderer. \nPlasma wants to use QtScript and QtWebKit. Konqueror wants KJS with KHTML. I am not sure which SVG is used by Plasma but I guess both ;)\nWhen there will be a QtMozEmbed available I am sure some Qt-apps will use it instead  (surely for some good reasons). But now my RAM is filled with yet another HTML+SVG renderer...  \nSo without wanting to troll I would like to ask if there are any plans how to handle the situation.   \n\n"
    author: "Bob"
  - subject: "Re: Time for consolidation?"
    date: 2008-08-13
    body: "Don't forget that Qt also has two different XML APIs, as well as having both a lite and full HTML renderer."
    author: "Nach"
  - subject: "Re: Time for consolidation?"
    date: 2008-08-13
    body: "No worries, I'm sure things will shake out."
    author: "Ian Monroe"
  - subject: "Re: Time for consolidation?"
    date: 2008-08-14
    body: "I don't really dislike KHTML vs. WebKit (they have very different philosophies, and getting rid of one of them would not make sense). What I dislike, though, is the duplication inside of Qt: Why does Qt contain QtScript as well as WebKitJs (sorry, don't know the name), QStyle and WebCore, QML and WebCore .... Wouldn't it be possible to port Qt's existing stuff to WebKit or port QtWebKit  to Qt's existing infrastructure (less likely, due to the fact that many web sites would expect QtWebKit to behave like other WebKit ports)?\n\nAs for the QtMozEmbed, I can't really imagine very many non-Mozilla programs using it, primarily because QtMozEmbed won't become part of KDE (KHTML) or Qt (QtWebKit), removing most motivation to choose it over the former ones.\n\nAs for Nokia, it's probably (to be perfectly tackless) greed. They don't want any tweaks they make to their software appearing on other software (read: the iPhone), even though WebKit is probably more appropriate for embedding (WebKit is smaller and faster than Gecko). I don't think it'll help them too much, either, since Apple could've written the code Nokia did, anyway. And, any tweaks Apple makes to WebKit, Nokia would get them, too). Oh, well, we get a Qt Firefox this way.\n"
    author: "Michael \"We are the world\" Howell"
  - subject: "mh"
    date: 2008-08-12
    body: "Bigger screenshots would be nice. (Besides, it would have been really cool to see Mozilla use, say, wxWidgets instead to put the Windows and GTK under one hut, because then it would be \"easy\" to add Qt to wx, thereby benefitting not just Mozilla.)"
    author: "cjk"
  - subject: "Re: mh"
    date: 2008-08-13
    body: "Mozilla already uses such an abstraction, which makes such an alternative backend implementation possible in the first place.\n\n"
    author: "Kevin Krammer"
  - subject: "Eight years and bang!"
    date: 2008-08-12
    body: "The first attempts to port Mozilla suite to Qt toolkit date 2000.\n\nI wonder why it has taken so long to finally admit that such a port is a good thing to have?"
    author: "Artem S. Tashkinov"
  - subject: "Re: Eight years and bang!"
    date: 2008-08-13
    body: "I've suggested it several times and I was told a QT replacement of the GTK widgets would be too hard and take too long.  It seems the initial work was done by one guy in a matter of days, but now a team is working on really finishing it."
    author: "T. J. Brumfield"
  - subject: "Re: Eight years and bang!"
    date: 2008-08-23
    body: "I see, and I suppose a native windows and a native macOS port was trivial. People keep saying all kinda $hit and worse, people keep believing that.\n\nAll the earlier claims about XUL by mozilla was hogwash, then ? Why didn't they just come out and say they hated anything KDE and that would be that ! "
    author: "vm"
  - subject: "One word:"
    date: 2008-08-12
    body: "\"awesome\""
    author: "Anders"
  - subject: "SWT Qt port"
    date: 2008-08-12
    body: "This could really help a Qt port of SWT, especially since SWT already uses Cairo for 2D graphics rendering (on Linux anyway)."
    author: "David Walser"
  - subject: "Re: SWT Qt port"
    date: 2008-08-12
    body: "There is with Scribus already a Qt application which uses Cairo. "
    author: "Dan"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "Scribus uses Cairo to render to an off-screen pixmap, then blits that to the Qt canvas. It's very slow and makes it hard to update only dirty regions. Instead, it redraws a whole tile if any part of the tile is dirty. In practice, it lands up redrawing the whole visible canvas a lot of the time.\n\nThe ability to use a Qt canvas as a direct Cairo backend will be really beneficial to Scribus, though plenty of work on the Scribus drawing code (which is a bit scary in places) would be required to get the most out of it."
    author: "Craig"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "The last couple of years indicate that the SWT community, i.e. both SWT developers as well as users, have very little interest in a Qt based SWT implementation, otherwise somebody would have at least tried to create one.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "Nonsense, while it may be true of the developers, it isn't true of the users...I know I'm not the only one.  Remember, it was only recently that the licensing became such that it would even be possible."
    author: "David Walser"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "Licencing was commonly used as an excuse not to put any resources into a Qt backend because all the resource the already existing backends had consumed.\nIt has always been possible to create a Qt based SWT because of Qt/X11's QPL licence.\n\nIf there are users who would prefer a Qt based SWT for whatever reason, they so far have failed to communicate that need with the SWT project.\n"
    author: "Kevin Krammer"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "Iirc there were rumours about a SWT/Qt branch, but kept behind the curtain, because of licensing issues, years ago. Maye the QPL didn't suite for the one or the other reason.\n\nSince v.4.4.1 Qt is not available under QPL anymore, but the linking exception list includes, among various other licenses, the Eclipse Publice License 1.0."
    author: "Carlo"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "I am pretty sure it has never been a licencing issue but that this is always used as a quite convenient excuse since it sounds correct (i.e. GPL being one of Qt's licences)\n\nUnfortunately this myth has spread a lot, but luckly the change in licencing (the exceptions you mentioned) now show clearly that there obviously have been and are other, undisclosed, reasons not to do a Qt based SWT."
    author: "Kevin Krammer"
  - subject: "Re: SWT Qt port"
    date: 2008-08-13
    body: "Well, the exception is nice, but not a free ticket. Since all other code you want to use in some way, has be compatibly licensed, too, you cannot freely choose from the pool of GPL licensed software and distribute resulting binaries.\n\nIt would be quite an improvement, if companies like IBM wouldn't create yet another license for every single of their open source projects and either dual-license or simply use the LGPL or GPL with some linking exception to protect their commercial interests."
    author: "Carlo"
  - subject: "what about thunderbird ?"
    date: 2008-08-13
    body: "if this is about 'mozilla platform', cairo backend and such, will this also mean qt thunderbird ?"
    author: "richlv"
  - subject: "Why not Opera"
    date: 2008-08-13
    body: "Nokia could buy Opera Software (also from Norway as Qt) and it's Qt based browser Opera.\nWhy didn't tat happen? "
    author: "Ole"
  - subject: "Re: Why not Opera"
    date: 2008-08-13
    body: "The Maemo is already using Gecko/Firefox for the GTK side, it'd be less work for them to have a single rendering engine for both GTK Maemo and Qt Maemo. Open sourcing Opera would also probably be quite a challenge and take a while to make sure all the code could even be open sourced."
    author: "Kit"
  - subject: "Re: Why not Opera"
    date: 2008-08-14
    body: "Not to mention it is much cheaper to do a little coding rather than just buy Opera."
    author: "T. J. Brumfield"
  - subject: "Awesome"
    date: 2008-08-13
    body: "Awesome news! "
    author: "Me&Myself"
  - subject: "AT port"
    date: 2008-08-13
    body: "Nokia is smart and is afraid of dependencies. That is a good sign."
    author: "andy"
  - subject: "Great news!"
    date: 2008-08-14
    body: "With the advent of KDE 4.1, I made the switch from Ubuntu to Kubuntu. I was never able to get rid of Firefox from my computer, however, and I have permanently avoided using Konqueror. Not that Konqueror is a bad web browser, it isn't; I just simply can't live without Firefox's superb plugins which so much enhance the entire browsing experience. I truly hope that Nokia completes the XUL-QT port. Not only will this port directly benefit Firefox, but it may also support XUL as a graphical front-end for many other applications. Gtk+ has never caught on with the Mac, while QT has enjoyed huge success on Windows/Linux/Macs. This is truly great news! Thank you Nokia!"
    author: "irrdev"
  - subject: "Re: Great news!"
    date: 2008-08-14
    body: "I'm pretty sure XUL already supports native OS X widgets, so it doesn't use GTK on OS X nor will it need Qt on OS X."
    author: "Anon"
  - subject: "Re: Great news!"
    date: 2008-08-14
    body: "You would be correct."
    author: "Jonathan Thomas"
  - subject: "Re: Great news!"
    date: 2008-08-23
    body: "why support a multitude of widgets ? QT is supposed to solve that problem but it seems the QT/KDE haters, some of whom dictate to mozilla, convinced them to eschew QT in favour of OSX and even windows widgets ! Enemy of my 'enemy' is my friend ? In this case, the enemies exist in the minds of these FSF/gnome lovers."
    author: "vm"
---
Developers from Nokia and Mozilla have been <a href="http://browser.garage.maemo.org/news/10/">working hard to port the Mozilla Platform and Firefox to Qt</a> and there are now some solid results available. An experimental build of Firefox Qt is available, and you can download the sources from Mozilla's mercurial repository. The plan is to merge the Qt branch into the central Mozilla branch to make the port official. <i>KDE Dot News</i> spoke to developer Oleg Romaxa from Nokia who came to Akademy 2008 from Finland.





<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/akademy-2008-oleg-romaxa.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-oleg-romaxa-wee.jpg" width="400" height="267" /></a><br />
Mozilla Qt Developer Oleg Romaxa
</div>

<h3>Why did you do this port?</h3>

<p>Because we were going to make Maemo use Qt in the future and we were making a browser based on Mozilla for the N810, so now we are making this new browser version based on Qt. Before, there was no Qt port available for Mozilla. It only took us 5 days to port it. We asked for help from Mozilla and they sent a team to Finland for a week.</p>

<h3>What's the current state of the port?</h3>

<p>The Qt port is mostly ready now for our browser. For full Firefox support, we need to implement XUL widgets, theming, and make some implementation for Flash player (NPAPI) support: it works but doesn't currently draw anything.</p>

<h3>Will this be supported in future?</h3>

<p>Yes, we will support it. We will cooperate with Trolltech to ensure it continues to be supported.</p>

<h3>What is Mozilla's interest in this?</h3>

<p>Mozilla are interested in mobile devices. Nokia make mobile devices, and will be using Qt. If you look at the mobile Firefox requirements, Qt support is in that.</p>

<h3>Why are Nokia now developing two browsers? This one and QtWebKit?</h3>

<p>Nokia will use the best browser for the job. Currently, we cannot make a full-featured and integrated browser with WebKit in mobile. But with Mozilla, we do not need to do anything, we can take existing models and API's which are available. Also, NPAPI support is already in the Gecko web rendering engine. They are also concerned that WebKit is to some extent controlled by Apple, who are in competition to Nokia with their iPhone.</p>

<h3>What are you hoping to get out of Akademy?</h3>

<p>I'm hoping to find people and ask them what they think about the Qt port and having Mozilla integrated into KDE. So far people are just talking about WebKit.</p>

<h3>Will distributions package it?</h3>

<p>That would be interesting to know. Maybe Kubuntu would like to have it...</p>

<p>Also, i've heard of Russian companies making KDE-based distributions and they might be interested in having Firefox without having GTK installed.</p>

<h3>Do you think Mozilla will get rid of GTK?</h3>

<p>No, Mozilla will support officially only the ports they have a particular interest in. If it helps to expand their market they will be more interested.</p>

<h3>Where can people find more information?</h3>

<p>There is a branch available on Mozilla's Mercurial repository.  http://hg.mozilla.org/users/romaxa_gmail.com/index.cgi/mozilla-qt/ is the latest branch, it should soon be merged when I fix a final outstanding bug.</p>

<h3>What needed doing for the port?</h3>

<p>We ported Cairo to gain a QPainter backend. It's not full-featured, but it's enough for a web browser. Qt's implementation of the Cairo backend is much simpler than with GTK, because Qt is better able to integrate graphics and widgets.</p>

<h3>Can the Cairo backend be used for other Cairo applications, like swfdec?</h3>

<p>I'm sure it's already possible to do most of what's needed, images and compositing are there. It's not fully implemented but it's mostly complete. We are talking to Cairo developers to push the Qt backend into the Cairo source tree.</p>




