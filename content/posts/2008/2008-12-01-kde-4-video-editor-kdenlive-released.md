---
title: "KDE 4 Video Editor Kdenlive Released"
date:    2008-12-01
authors:
  - "mmoeller-herrmann"
slug:    kde-4-video-editor-kdenlive-released
comments:
  - subject: "Powertools for KDE"
    date: 2008-11-30
    body: "\nThere are few applications in my list what can be considered as KDE power apps... those are\n\nK3b\nAmarok (1.4, not so sure about Amarok2)\ndigiKam\nKontact\nKtorrent\nKMplayer (Kaffeine mayby)\nkdenlive\n\nAnd now kdenlive has come even better. I hope KDE will support these apps as their \"officials\" so normal user gets as integrated desktop for their computer as possible.\n"
    author: "Fri13"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "I would add Kicker to this list... though I know that saying this here makes me a big, fat flame target :)."
    author: "slacker"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "No, just ignorant."
    author: "JackieBrown"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "nah, kicker is/was a pretty great app. it was even nearly stable ;)\n\nit didn't really do all that much, to be honest, especially given that it was 6 or so years in development. \n\nhowever, much of what it did, it did at least decently and often well. it did some things rather poorly and making it do new things was overly difficult. extending it's bag of tricks was rediculously difficult, and it was graphically very limited. it was still ahead of most other FOSS panel systems, though.\n\nnow, if kicker was a power app ..... =)"
    author: "Aaron Seigo"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: ">now, if kicker was a power app ..... =)\n\nPlasma is the Interstellar StarDrive capable of launching us far into the deepest reaches of space.\nLove it. Thank you plasma team *hug*"
    author: "Heine Salte"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-03
    body: "Not sure... I agree with ktorrent, k3b, maybe even amarok...\n\nbut kicker? I never really found a use for that.\n\nI would add konsole and yakuake to that list. KMplayer however used to lack behind somehow... i found smplayer to be nicer IMHO.\n\ndigiKam i never tried, but i dont think it would be as useful as kdenlive for instance"
    author: "mark"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-09
    body: ">>>digiKam i never tried, but i dont think it would be as useful as kdenlive for instance\n\ndigiKam and kdelive dont compete at any level how can one be considered more or less \"useful\" than the other?\n\ndigiKam is quite useful, give it a try! :) I cant wait for a stable kde4 version!"
    author: "mike"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-04
    body: "Hello,\n\nkicker _is_ still stable and _is_ still ahead of (in my opinion) all other panel systems. Or does your past tense imply that KDE 3 is no longer supported? I thought that wouldn't be so fast? \n\nMind you, Debian will release KDE 3.5 as stable, so normally it really should be unsupported by then at least, whenever it is. ;-)\n\nLast time I tried KDE SVN, I was thinking that Plasma is slowly getting there, so not all hope is lost. It speaks volumes how long it takes (2 releases or more) to get even on the functionality level. \n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-04
    body: "Yes, indeed. Volumes: looking at the whole plasma development cycle, we're talking about a year and a half, maybe two of development to develop the whole desktop thing, including panel. When you compare that to the five or six years needed for Kicker, the only logical conclusion is that plasma development is amazingly rapid."
    author: "Boudewijn Rempt"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "I largely agree with the OP.\nKDE 4 is a great framework with lots of nice 'applets' and 4.2 is on schedule to deliver a nice, polished user experience.\nHowever, a desktop environmnet needs more than just a pretty face and the basic apps. (OS/2 anyone?)\nI think it will stand or fall however with the avaialbility of large, end-user applications using its libraries.\nThe applications mentioned are mostly the ones I woudl list (except for maybe KMplayer). \nI would add KOffice for the non-work related 'office' tasks (for work related, Oo.org is way better albeit rather bloated) KOffice's Kita also seems a promising app.\nI would even add KDevelop which, while not a 'main-stream' app will put more developers on KDE4\nThe good news is of course that most of these apps are well on track to have fully formed KDE4 variants in the near to mid future."
    author: "UglyMike"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-03
    body: "\"Oo.org is way better albeit rather bloated\"\n\nNot only bloated but very limited. Maybe they can get their act together but to be honest I think in the long run the koffice team will deliver the better suite. Better as in better for users.\n\nWith OO i just feel as if a dead Sun company is somehow still over-shadowing it and letting it die a slow horrible death ...."
    author: "mark"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "The kde application I use most is in fact klipper. I can't think of a better program.\n\nAlso I think SMplayer is way better then KMplayer, though I use plain old mplayer the most :)"
    author: "Svein"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "yeah... KMplayer is in my experience broken and quite crashy. Haven't looked at it so recently but I was under the impression that it wasn't developing very fast (at least not at the speed of SMplayer). \n\nSMplayer is really nice, I wish there was a kde integrated app with the same power and flexibility as SMplayer. Last thing i heard was of the kaffeine guys complaining about the lack of features in phonon for them to do their stuff."
    author: "txf"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-02
    body: "VLC is the best here...\n"
    author: "Mihas"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-05
    body: "Whatever happened to Kaffeine?"
    author: "Anonymous"
  - subject: "Re: Powertools for KDE"
    date: 2008-12-01
    body: "I have great expectations about this app, I think if fits really nicely in the strong KDE multimedia offering. I hope it can get the recognition of the other apps in this lists.\n\nHope to hear more about kdenlive, either in the dot or in planet KDE."
    author: "Raul"
  - subject: "niiice, question :"
    date: 2008-11-30
    body: "can i easly rotate a movie ? if i recorded a movie with my camera in 90 degrees - perhaps some dopplhin integration to rotate the movie like a picture.. ?\n\n"
    author: "asdf"
  - subject: "Re: niiice, question :"
    date: 2008-12-01
    body: "Easy.\n\nJust drag the rotate effect onto a clip. You can rotate in all three (X, Y and Z) axis."
    author: "Cinephiliac"
  - subject: "Re: niiice, question :"
    date: 2008-12-02
    body: "Drag the rotate effect onto the clip?  That seems.. an odd sort of workflow.  Maybe I'm just saying that because (especially because) I've never seen anything like that before.\n\nIt has to be dragged from somewhere, why isn't that somewhere just a button?  For ease of use applying it to a specific clip?  That's actually interesting sounding.  Alternative would be an \"active\" clip that button presses acted on."
    author: "MamiyaOtaru"
  - subject: "Re: niiice, question :"
    date: 2008-12-03
    body: "I agree with you, but just because we are more used to it. Because most Windows(tm) apps work this way.\n\nBut think twice, forcing the user to know what is the \"active\" clip seems awkward to me. The Kde4 goal (imho) is to a desktop experience \"that is not on your way\". And to date it's a great succes with basics apps (dolphin and such).\n\nAnd now, imagine using that work-flow on a touch-screen : draging the effect on the clip with your finger. It's look intutive for me. At least it don't ask the user to check which is the \"current active clip\".\n\nmy 2\u00a2"
    author: "sebt3"
  - subject: "Re: niiice, question :"
    date: 2008-12-05
    body: "> Drag the rotate effect onto the clip? That seems.. an odd sort of workflow.\n\nYou are adding a rotation filter to the clip's filter stack, so dragging it there is perfectly intuitive."
    author: "Melchior FRANZ"
  - subject: "Re: niiice, question :"
    date: 2008-12-03
    body: "> You can rotate in all three (X, Y and Z) axis.\n\nGlad to see you support holographic video cameras! Now if only they weren't so damn non-existent... :-)"
    author: "Tim"
  - subject: "Re: niiice, question :"
    date: 2008-12-03
    body: "\nYou can rotate a video on Z axis too, because after all, it is just like on 3D application rotating. You have all three axes.\n\nAt least it was good that they did not speak about 4th dimension... the time ;-)"
    author: "Fri13"
  - subject: "Re: niiice, question :"
    date: 2008-12-07
    body: "Well, I've spent several hours learning how to rotate an AVI in Ubuntu. I've used mencoder with success - as you would expect with a CLI tool, I had to read a fair bit of documentation.\n\nThen I saw this thread and though I'd try Kdenlive - being a GUI I'd expect it to be pretty straight-forward. Well, it's so unintuitive I've given up. I dragged my AVI into a new project (because I could not see how else to add it!). The 'rotate' effect is listed in the Effects menu, but I can find no way to apply it. The 'drag' action described here is impossible from the menu.\n\nSo, sorry guys I'm sure it's a great tool but as the GIMP boys found, you need to spend a lot of time on the UI and beginner tips if you're going to get any take-up. I'm off to build my movie in iMovie on the Mac. Which is a great pity, but it will work.\n\nCheers, al."
    author: "Allan Kelly"
  - subject: "independent"
    date: 2008-12-01
    body: "\"Video rendering is now completely independant*\"\n\n* \"independent\""
    author: "Jad"
  - subject: "Re: independent"
    date: 2008-12-01
    body: "pedant"
    author: "AC"
  - subject: "Re: independent"
    date: 2008-12-02
    body: "LOL! 0wn3d!\n(ok, I just looked like a kid, I know :P)"
    author: "Vide"
  - subject: "Re: independent"
    date: 2008-12-02
    body: "You say that like it's a bad thing.  Better that than willful ignorance."
    author: "MamiyaOtaru"
  - subject: "Re: independent"
    date: 2008-12-10
    body: "Not to speak English correctly is being an ignorant?. Sadly, countries like yours (I'm assuming you're japanese; but this applies to most of Europe, especially Germanic Europe and many of Latin Europe, and many countries all over the world as well) have been so deeply colonized by their former invaders that you unconsciously believe that speaking English is almost as common as the sun raising every day. Well, probably non native English speakers (who are most of people, ironically) should post in their native languages and call \"ingnorants\" all those who weren't able to understand and post replies in said languages; or perhaps we should learn Esperanto, Modern Latin, or whatever non-national language you want and create a real standard for international communication instead adopting English and benefit American culture and companies, who have the whole world as a market which speaks their language.\n\nAnd yes, being pedant is a bad thing, especially because it's usually a fake attitude. The \"savant\" doesn't need to show off, just the one who is insecure, ;p.\n\nSo, to Moritz, thanks for your software and go ahead with your incorrect English or may them learn German, sch*ize! :)\n\n\nPace."
    author: "Pugliesallaspagna"
  - subject: "Re: independent"
    date: 2008-12-10
    body: "Not to speak English correctly is being an ignorant?. Sadly, countries like yours (I'm assuming you're japanese; but this applies to most of Europe, especially Germanic Europe and many of Latin Europe, and many countries all over the world as well) have been so deeply colonized by their former invaders that you unconsciously believe that speaking English is almost as common as the sun raising every day. Well, probably non native English speakers (who are most of people, ironically) should post in their native languages and call \"ingnorants\" all those who weren't able to understand and post replies in said languages; or perhaps we should learn Esperanto, Modern Latin, or whatever non-national language you want and create a real standard for international communication instead adopting English and benefit American culture and companies, who have the whole world as a market which speaks their language.\n\nAnd yes, being pedant is a bad thing, especially because it's usually a fake attitude. The \"savant\" doesn't need to show off, just the one who is insecure, ;p.\n\nSo, to Moritz, thanks for your software and go ahead with your incorrect English or may them learn German, sch*ize! :)\n\n\nPace."
    author: "Pugliesallaspagna"
  - subject: "Very cool!"
    date: 2008-12-01
    body: "I love this app. It's fantastic. Keep up the good work!"
    author: "winter"
  - subject: "Kdenlive 0.7.1 Out in Around 3 Weeks!"
    date: 2008-12-01
    body: "Just so you all know, as much as I love kdenlive 0.7, there's something better out in about 3 weeks...\n\nKdenlive 0.7.1!\n\nDon't let the small version number increase fool you. Even now, there's already over 40 improvements comitted including enhanced screen recording (with mouse follow option), speed improvements, more comprehensive shortcuts, recursive search for missing clips and a brand new (to this version) tool, the spacer tool, which lets you easily move many clips and adjust the space between them.\n\nThe number of bug fixes and crash hardening is equally impressive.\n\nAll this is just in the 18 days since 0.7 launched, and there's 25 days to go (yes, Christmas is d-day). I just wish I could code, so I could get Christmas early. ;-)\n\nOh well, I'll just have to settle for using the GUI Builder Wizard to compile it as I wait impatiently *taps fingers on table*."
    author: "Cinephiliac"
  - subject: "Re: Kdenlive 0.7.1 Out in Around 3 Weeks!"
    date: 2008-12-02
    body: "I try it again and again under Suse 11.0 with Kde4.2 and it still crashes. Oh, now I see, a killer app.\n\n"
    author: "isi"
  - subject: "Windows version"
    date: 2008-12-01
    body: "Any chance of running this under Windows, like other KDE4 apps?"
    author: "extropy"
  - subject: "Re: Windows version"
    date: 2008-12-08
    body: "One can only hope. I hate Windows Movie Maker with a passion.\n\nAsk in #kde-windows on freenode IRC if you want a quicker answer ;)"
    author: "O_o"
  - subject: " I cant install it"
    date: 2008-12-01
    body: "I have used the old Version already and was very pleased to her about that news bunt unfortunately I cant use it. I have the new Kubuntu, and the debian-Multimedia in my sources.list but I have difficulty's with some packages witch wont Install.\n\nI even consider switching Distro because of that.\n\nBut anyway, Thanks for that nice peace of software.\n\n~Hermann\n\n"
    author: "Hermann"
  - subject: "Re:  I cant install it"
    date: 2008-12-01
    body: "Debian Multimedia is for Debian, so it's not surprising it doesn't work on a different distribution.\n\n"
    author: "Jonathan Riddell"
  - subject: "Re:  I cant install it"
    date: 2008-12-01
    body: "Usually it has no problems with ubuntu. I installed plenty of SW from Debian-Multimedia on my Kubuntu."
    author: "Hermann"
  - subject: "Re:  I cant install it"
    date: 2008-12-02
    body: "You're generally better off rebuilding such packages and get better compatibility to boot.  Chuck in the deb-src line from Debian Multimedia rather than the deb line.  Say you want to build and install devede on your Ubuntu box:\n\napt-get install build-essential debhelper (just to make sure)\n\nmkdir devede (otherwise you clutter up where you're doing this)\ncd devede\n\napt-get build-dep devede (this fetches the -dev and library packages you need to build the package from your normal Ubuntu repository.  Unless you're backporting to an ancient distro, this should go without hitches. Otherwise you may have build and install a few -dev packages first.  I don't bother with this if the build deps I have to port are excessive.)\n\napt-get source devede\ncd devede-3.11b-0.0\nfakeroot dpkg-buildpackage -b\ncd ..\n\nIf all went well you can now install the deb you'll find in this directory.  It will be built against the Ubuntu dev libraries but will otherwise act like you installed it from Debian Multimedia and won't cause the potential problems bringing in a lot of libraries from a cousin distro can cause.\n\nThis basic procedure can be used on Debian or any Debian derivative to build packages from cousin distros.\n"
    author: "frogstar_robot"
  - subject: "Re:  I cant install it"
    date: 2008-12-08
    body: "Thanks very much for the very straightforward and well-explained instructions! That just answered like 3 of my questions at once, even tho I didn't come here to read about that :p\n\nCheers!"
    author: "O_o"
  - subject: "Re:  I cant install it"
    date: 2008-12-04
    body: "Have you tried using the Kommander script at http://kde-apps.org/content/show.php?content=85826 ?  The info page lists the prerequisite packages you will need to install for Kubuntu, and then describes how to use the tool.\n\nI used it to installed the latest version on my Debian Sid machine last night and it was suprisingly easy to use.  (To be fair ffmpeg took a couple of attempts to build but that was because I ignored the package list and only added missing dependencies one at a time following each build error!)\n\nMy first impression of Kdenlive is wow!  I've only just started to play around with video editing, but of the packages I've tried Kdenlive seems the most stable and has the cleanest interface.  Great work."
    author: "Matt"
  - subject: "wow"
    date: 2008-12-01
    body: "I'm officially impressed! Had to install a few dependencies (I used all from their respective SVN repos), and then building went smooth as silk. My first attempt to make an *.flv from screen grabbing was a success. I didn't even have to read a manual -- everything was quite intuitive. Thanks a lot for kdenlive!"
    author: "Melchior FRANZ"
  - subject: "Question about features"
    date: 2008-12-01
    body: "With the current version, is it possible to have the typical \"chroma\" effect? (record yourself in front of a green/blue background and then render a video in that background)\n\nIs there any masking support? subtitles?\n\n\nMonths ago I made a fun short video to help me to learn Cinelerra. The result was 3 \"me\" speaking with each other in the same room. Two on the sides and one in the middle near the camera, obscuring partially the other two. To make the first two, I put one clip at the back of the stack, then the other above it, with one half masked out. That did the trick. The third was more difficult. I had to compute the differences between the 3rd clip and the background (an empty room), and discard those pixels that were equal. It was not perfect, but it help me to learn some basic things with masks.\n\nSo, now I have to ask: is it possible to create such a video with the current version of Kdenlive? If not, are such features planned for anytime soon?\n\nThanks for your patience.\n"
    author: "David"
  - subject: "Re: Question about features"
    date: 2008-12-01
    body: "Yes, this should indeed be possibly.\n\nPlease note, that AFAIK, kdenlive 0.7 does not include masks, but you should be able to some extent to use the composite filter (combined with a the chroma filter) to achieve something similar.\n\nRegards\n\nMads"
    author: "Mads Bondo Dydensborg"
  - subject: "Nice job but let's get realistic"
    date: 2008-12-01
    body: "MLT is far from a stable framework.\n\n0.3.2 is not exactly what I call, mature. MLT itself is only in beta."
    author: "Marc Driftmeyer"
  - subject: "Re: Nice job but let's get realistic"
    date: 2008-12-04
    body: "Uh, yes, you are correct that it's API is not stable; there is still much work to be done. It's implementation is very stable for specific commercial applications for which I have used it. Of course, v0.3.2 is not mature; it was released less than a month ago. Now, v0.1.0, released in May, 2004 after the initial 6 months of intense development - about 4.5 years old - now, that is mature!\n\n\n"
    author: "Dan Dennedy"
  - subject: "This looks promising"
    date: 2008-12-04
    body: "If KDE e.V. would run a fundraiser for the app I am willing to contribute."
    author: "Plumplum"
  - subject: "Concern about dependencies"
    date: 2008-12-06
    body: "This appears to be a great app and excellent work.\n\nHowever, I am concerned about some of the dependencies.\n\nI first note that installing it on LFS is a major project, but it can be done.\n\nHowever, some of the dependencies are old and not actively maintained.  Three of them would not compile with the current GCC release:\n\n     swh-plugins-0.4.9\n     libmad-0.15.1b\n\nand secondary dependency:\n\n     gnome-libs-1.4.2\n\nwithout some tweeking.\n\nI also wonder about it being dependent on GNOME-1 and GTK+1 rather than the current GNOME-2 and GTK+-2 since these also old and not actively maintained."
    author: "JRT"
  - subject: "Re: Concern about dependencies"
    date: 2008-12-07
    body: "Am I confused by following the directions exactly?\n\n\"gdk_pixbuf\" and \"gdk_pixbuf-2\" are not the same library!"
    author: "JRT"
  - subject: "Release URL"
    date: 2008-12-06
    body: "Sorry, we have been modifying Kdenlive website.\nThe release announcement is here: http://kdenlive.org/users/j-b-m/kdenlive-07-released"
    author: "Jean-Michel"
---
The promising nonlinear video editor Kdenlive has made its first non beta for KDE 4, version 0.7 is on us. This closes another gap of the free desktop world: a usable open source video editor.  Kdenlive has the potential to become the Amarok or K3b of video editors, offering comfort and elegance so far not available in alternative programs. The feature set looks amazingly complete and far exceeds the KDE 3 version already.  Check the <a href="http://www.kdenlive.org/content/2008-11-12-kdenlive-07-released">release announcement</a>.



<!--break-->
<img src="http://static.kdenews.org/jr/kdenlive-01.png" width="300" height="322" align="right" />

<p>This is the official announcement:</p>

<p>We are glad to announce the immediate release of Kdenlive 0.7</p>

<p>This is the first release of Kdenlive for KDE 4. Here is a quick list of improvements over the previous KDE 3 version:</p>

<ul>
<li>Complete rewrite of the communication with the MLT video framework, which means a huge speedup in all timeline operations</li>
<li>Capture from different sources: DV, HDV, webcam and screen grab</li>
<li>Better KDE integration (notifications, job progress, Nepomuk annotations)</li>
<li>More effects and transitons (improved support for Freior)</li>
<li>Full undo support with history</li>
<li>Video rendering is now completely independant from main application, you can safely work while rendering</li>
<li>Initial support for Jog Shuttle devices</li>
</ul>




