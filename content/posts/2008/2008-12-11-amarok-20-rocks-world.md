---
title: "Amarok 2.0 Rocks the World"
date:    2008-12-11
authors:
  - "lpintscher"
slug:    amarok-20-rocks-world
comments:
  - subject: "EPIC WIN"
    date: 2008-12-10
    body: "Congratulations to all involved, I'm really looking forward to this! My friends will be jealous of me once more (until I tell them it'll run on Mac and Windows as well..)"
    author: "Sander"
  - subject: "I for one"
    date: 2008-12-10
    body: "after my experience with kde4 I'm waiting until amarok 1.2.2"
    author: "Martin Mopps"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "100% agree, now the kde software should have \"What's new\" & \"What is not here anymore\" sections.\n\nI'm a KDE fan, but Kde 4.0 and 4.1 were a big disappointing for me. And now Amarok takes the same way.\n\nI can't understand the reason they launch a 2.0 version with less functionalities. \n\nQueueing and filtering in the playlist were two of the most used functionalities in my case. Hopefully they will be back within a few features. It's good to know we must wait probably more than one year to use Amarok the same way we use it now.\n\n\"Some features, such as the player window or support for databases other than MySQL, have been removed\" bye bye to my PostgreSQL database. I hope it still have support for SQLite.\n\nThis is frustrating.\n"
    author: "Adrian"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "The reason you don't understand is because you haven't the talent to work on projects people care about.\n"
    author: "Max Howell"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "Why do you say that?\nPerhaps you'r right and I don't have the talent, but I really care about open source projects. I'm just saying I don't understand this philosophy.\n\nIf I could, be sure I'd help the Amarok project."
    author: "Adrian"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "This is pretty simple. There is no \"philosophy\" that dictates the removal of features. They didn't start with amarok1.4 release it with \"less functionalities\". They started with Qt4, KDE4, and nothing, and built amarok 2.\n\nFeatures take time. Amarok 1 (and KDE3 for that matter) were in the cooker a long time, and during that time every little feature got tacked on and worked in. It's going to take time to get to that level again. It already has taken time. You've made it your entire life without Amarok 2; there's no reason you can't make it through another \"year or so\". So just eat your sarcasm, because it contributes nothing to the project. It certainly doesn't make feature parity come any faster, nor does it motivate the devs to do work that you won't do and won't pay for.\n\nHere's an open source philosophy: release early, release often. That encourages higher adoption, more developer involvement, and more user involvement. This is a stage in the growth of the project, a signal that it is ready for the next phase. Now that it has basic functionality (collection management, playlist management, music playback), a stable API to write scripts to, and release quality stability, it should be tagged and released into the wild."
    author: "Anon"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "Thank you for replying me and make the things clear.\n\nI do apologize for my post, the most important thing you said is:\n\n\"It certainly doesn't make feature parity come any faster, nor does it motivate the devs to do work that you won't do and won't pay for.\"\n\n\n"
    author: "Adrian"
  - subject: "Re: I for one"
    date: 2008-12-11
    body: "hmm... I think the release announcement says it all:\n\n\"\" It is important to note that Amarok 2.0 is a beginning, not an end. Because of the major changes required, not all features from the 1.4 are in Amarok 2. Many of these missing features, like queueing and filtering in the playlist, will return within a few releases. \"\"\n\n\"\" Other features, such as visualizations and support for portable media players, require improvements in the underlying KDE infrastructure. They will return as KDE4's support improves. \"\"\n\n\"\" Some features, such as the player window or support for databases other than MySQL, have been removed because either they posed insurmountable programming problems, or they didn't fit our design decisions about how to distinguish Amarok in a saturated market of music players. \"\"\n\nI think this really clear communication, and like to applaude the Amarok team for it!"
    author: "Diederik van der Boor"
  - subject: "Re: I for one"
    date: 2008-12-17
    body: "This is clear at a glance:\n\n\"They started with Qt4, KDE4, and nothing, and built amarok 2.\"\n\nThe words: \"from scratch\" or \"from the ground up\" or \"rewritten\" might nice to see for some of us."
    author: "frozen"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "What did Postgres give you that the database in 2.0 doesn't give you?"
    author: "Foo Bar"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "Nothing talking about amarok, but I develop projects using PostgreSQL so I use it also for amarok.\n\nNow if amarok only runs with MySQL, I have to run two DB servers in my machine.\n\nBut I think SQLite is supported in 2.0."
    author: "Adrian"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "There is no SQLite for 2.0 ;-("
    author: "Bogdan"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "Well, I guess that no user should ever need anything to know anything about databases. My wife doesn't know what MySQL is, or Postgres. And I see no reason to offer such an option unless there is a good technical reason to offer two (or more) databases. For examples. perhaps for Joe User DB1 is good (and DB2 sucks) while for Mike Onemillionsongs DB2 is better and DB1 would suck. \n\nBut AFAIK that technical reason doesn't exists. \n\nAll in all: The less code to maintain the better.\n\n(IMHO)"
    author: "Foo Bar"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "well, DB2 does in fact suck"
    author: "Bar Foo"
  - subject: "Re: I for one"
    date: 2008-12-15
    body: "This is exactly the reason we decided to not maintain more then one database."
    author: "Ian Monroe"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "If I'm not entirely mistaken it uses the embeded version of MySQL as default, running without a server. Making it work just like SQLite, but with the MySQL syntax."
    author: "Morty"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "No you are not mistaken, they use an embeded version of MySQL which in debian sid/experimentai is not working at all. This is a good thing too, I discover how much i love juk. "
    author: "Bogdan"
  - subject: "Re: I for one"
    date: 2008-12-11
    body: "That's interesting. Why isn't it working on Debian? Mysql embedded is nothing but a shared library. Do you have any details?"
    author: "Andre"
  - subject: "Re: I for one"
    date: 2008-12-12
    body: "In MySqlEmbeddedCollection::MySqlEmbeddedCollection,  mysql_library_init return an error. \n\nhere is the code:\n..\n    if( mysql_library_init(num_elements, server_options, server_groups) != 0 )\n    {                                                                         \n        error() << \"MySQL library initialization failed.\";                    \n        reportError( \"init\" );                                                \n        return;                                                               \n    }         \n..\n\n"
    author: "Bogdan"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "> run two DB servers\nUmm, no, it's MySQL embeded, not a separate server.\nAnd, no again, you don't have a choice.\t"
    author: "Tom Chiverton"
  - subject: "Re: I for one"
    date: 2008-12-17
    body: "This is good to hear and clarify. Because on my debian derived distro(what could that be), IIRC, MySQL proper was installed. Probably because of the above mentioned flaw with debian based systems. I thought Amarok guys were out of their minds when I heard they were using MySQL. I thought, why not SQLite? A look here was interesting: http://dev.mysql.com/doc/refman/5.1/en/libmysqld-example.html"
    author: "frozen"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "By the way, I didn't mean to criticise the Amarok project, as I said before I'm a KDE fan and Amarok is running in my machine near 16 hours per day.\n\nI'm sorry if some of the developers felt were not apropiate, that was not my intention. I'm just saying I don't like new versions with less features than the previous ones.\n\nAnyway, I still have KDE 3.5.8 and Amarok 1.4.10 and I love to work with them."
    author: "Adrian"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "I'm not an Amarok developer any longer, please don't associate me with the project or its people.\n\nI'm just a rude person, that's all."
    author: "Max Howell"
  - subject: "Re: I for one"
    date: 2008-12-17
    body: "You made my day (one week later)\nLOL :)"
    author: "Vide"
  - subject: "Re: I for one"
    date: 2008-12-11
    body: "\"I can't understand the reason they launch a 2.0 version with less functionalities.\"\n\nit's not so black and white, 1:1; there are new features, improved features as well as some things not there. so the idea of \"less functionality\" is incomplete at best.\n\nthat said, i don't know if you remember (or used) amarok 1.0 or releases prior to that. put that next to 1.2 and then step back and consider that to get there they had to release 1.0.\n\nif you wish, continue to use 1.2 while the rest of us move on.\n\nbut you ought to be happy that some people have the foresight to revisit their accomplishments so that you don't end up with a world of ever diminishing returns as new releases are made that are ever more incremental in improvement, but instead make hard decisions so that they can build even better software.\n\nif you ever do end up using and loving amarok2, you may want to consider revisiting this comment of yours, pondering the shortsightendness of it and altering the way you approach these things in life.\n\nin any case, i'm using the shiny new amarok and actually *gasp* enjoying it rather much. =)"
    author: "Aaron Seigo"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "Well, looks like it's your lucky day! Check it out: http://amarok.kde.org/en/node/48"
    author: "X"
  - subject: "Re: I for one"
    date: 2008-12-11
    body: "Who, them are some good news! :)"
    author: "Dado"
  - subject: "Re: I for one"
    date: 2008-12-10
    body: "No need to wait. Amarok 1.2.2 has been released during the first half of 2005, according to Wikipedia.\n\nHave fun :)\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: I for one"
    date: 2008-12-11
    body: "lmao"
    author: "illogic-al"
  - subject: "Amarok stop playing some mp3"
    date: 2008-12-10
    body: "This is only my problem, that Amarok 2.0 playing some mp3 and then stop. Nothing say.. stop.. click next track and again start playing some mp3 and again stop.\nBut other things seems work :)"
    author: "User from Mandriva"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-10
    body: "Hm, if you use Mandriva, it could be that your Phonon backend is set to Gstreamer. You should switch to Xine."
    author: "Mamarok"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-10
    body: "Amarok 2 doesn't play on my System at all! I am running KDE 4.2 beta 1 and using Xine as my Phonon backend but no sound from Amarok 2. I can see that the music is playing, I have OSD and everything seems to be normal, no error message but still no sound. The funny thing about it is that Kaffeine 2 uses the same Backend and is working fine, all other multimedia apps are working fine. Don't have a clue what's the problem."
    author: "Bobby"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-11
    body: "This is a typical usage question and belongs in our forum or mailing list. This here is _not_ the right place for user support.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-11
    body: "I wrote that because the author of this thread mentioned that Amarok played some mp3 and some not so I don't see what I wrote as out of context. \nAnyway it was only a temporary thing. My system obviously needed a restart because Amarok worked fine after I started the computer this morning.\n\nNice job and thanks very much for this wonderful programme. "
    author: "Bobby"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-10
    body: "Try switching to the xine Phonon backend.\n\nMandriva currently ships with the GStreamer backend by default, which has a few known bugs.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-11
    body: "Yes.. now working :)"
    author: "User from Mandriva"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-10
    body: "I have this problem too witk juk and debian sid/experimental (phonon-xine)."
    author: "Bogdan"
  - subject: "Re: Amarok stop playing some mp3"
    date: 2008-12-10
    body: "Yup, you better switch to Xine. That's the first thing I do on Mandriva. And if possible use the Xine packages from PLF (Penguin Liberation Front). This makes it play everything just fine here on Mandriva 2009."
    author: "Jure Repinc"
  - subject: "Spread the great news"
    date: 2008-12-10
    body: "http://digg.com/software/Amarok_2_0_0_Released\nhttp://www.fsdaily.com/EndUser/Amarok_2_0_0_Released\nhttp://www.reddit.com/r/reddit.com/comments/7imcb/amarok_20_released/"
    author: "Jure Repinc"
  - subject: "Re: Spread the great news"
    date: 2008-12-10
    body: "The news has made it on to \"The Register\"\n\nhttp://www.theregister.co.uk/2008/12/10/amarok2_released/"
    author: "Gerry"
  - subject: "LOL @ the song"
    date: 2008-12-10
    body: "Congratulations to all involved.\n\nWhat really caught me is the song which is being played in the screenshot - Dance Pe Chance. Was this taken on someone else's computer, or your like Hindi songs (your name isn't Indian)? Personally I hate this song - it has awful meaningless lyrics, no sense of form or structure, no \"beauty\". Nevertheless, I am happy to see that Bollywood has spread so far and wide."
    author: "Rohan Dhruva"
  - subject: "Not usable"
    date: 2008-12-10
    body: "The queueing functionality and the playlist search field were the greatest features of amarok and they're not included in amarok 2?! I don't care about all the UI revolution."
    author: "David"
  - subject: "Re: Not usable"
    date: 2008-12-11
    body: "Everyone has his/her favourite set of features. Personally, I couldn't care less about playlist search or queuing.\n\nOne of my personal favourites is Amarok 2's seamless Internet service integration, which it handles extremely well.\n\nSo you see, you cannot be all things to everyone. But as we said many times before, the features you mentioned are planned for future releases.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Not usable"
    date: 2008-12-11
    body: "Great to hear playlist search is planned. I like having a big (huge!) playlist on random and narrowing / broadening my search parameters when I hear something I like, when I get tired of my current selection or when my mood changes. My way of rediscovering my music ;) I guess this is quite cruel of me for my computer when having thousands of items in my playlist :)\n\nBut of course Amarok is (much) more than usable without playlist search, it is by far my preferred player.\nThe (internet)services integration is awesome.\n\nThanks to the Amarokers!"
    author: "peter"
  - subject: "Re: Not usable"
    date: 2008-12-11
    body: "there's a collection search; they playlist seems to be much more \"transient\" with the collections taking a much more central position in amarok2.\n\nqueueing suport would be nice, but that's really not a huge feature so if it's desired i'm sure it'll appear without too much time.\n\nthe application over all looks and feels a lot better and the number of new or improved features is impressive.\n\ni just don't get \"the glass is 10% empty\" types."
    author: "Aaron Seigo"
  - subject: "Re: Not usable"
    date: 2008-12-12
    body: "I expected more people to make a stink about portable media player support honestly.  Then again, Amarok 1 still exists for you to use until Amarok 2 gets the features you want."
    author: "T. J. Brumfield"
  - subject: "Re: Not usable"
    date: 2008-12-15
    body: "I really do hope Amarok 1.4 will stay easily available for openSUSE, because Amarok 2 is worthless to me. I have a mass storage device mediaplayer which I can't fill using Amarok 2 (mass storage device isn't supported). While in Amarok 1.4 it gets recognized and you can setup a path how to fill that device (in my case /media/disk/MUSIC/Artist/Album/##-Title.ext). Also, I can't create the same playlists I can with Amarok 1 and I use those playlists (using my ratings) to fill that same portable media player.\n\nLooks like some missing features which will (hopefully) be in a .1 release. Without them, Amarok 2 is useless to me.\n\nAmarok 1.4 is very flexible and the playlist/queue is, in my opinion, much more useful than in Amarok 2.\n\n*hopes for the best*"
    author: "parena"
  - subject: "Kool!"
    date: 2008-12-10
    body: "Great!"
    author: "gerd"
  - subject: "Congrats and some small remarks..."
    date: 2008-12-10
    body: "Thanks for a great release, Amarok continues to rok :)\n\nJust two remarks:\n\n1. There is too much grey and too little contrast in the main window. things are not distinguishable easily - and i did the (non-techy) girlfriend test too, to be sure about that :)\n\n2. The \"main view\" (db | context | playlist) is just too crowded. I doesnt fit at all here and much content is cut out, even on fullscreen, on my 1400x1050 display with 9px fontsize in KDE. A solution could be to put the context view into its own tab on the left and make it cover the whole window. This would also allow to do more fancy stuff. I just imagine a layer of plasmoids over a graphical effect reacting to the music for example, that would be awesome :)\n\n\nWell, thanks again for a great release and laying the groundwork for further innovation. Although i have my few remarks and criticism here and there, i am fully confident in all the people working on Amarok - you rok! :)\n\n\n\n"
    author: "Jan"
  - subject: "Re: Congrats and some small remarks..."
    date: 2008-12-10
    body: "Forgot something about the context stuff:\n\nMy concern about it is 1. screenspace, and 2.: i see a lot more potential in the context view, like the effects reacting to the music in the background of the context widgets for example.\n\nWith the context view in its own tab you could also combine the context view + graphical effect plugins + maybe even a fullscreen party mode directly into the window. Sounds very clean imho.\n\nAnd now i stop :)\n\n"
    author: "Jan"
  - subject: "Re: Congrats and some small remarks..."
    date: 2008-12-11
    body: "\"The \"main view\" (db | context | playlist) is just too crowded. I doesnt fit at all here and much content is cut out, even on fullscreen,\"\n\nHuh, what are you viewing exactly? My screen resolution (and font size) is bigger than what you list ... and yet it's taking up perhaps 60% of my screen. You can move the playlist out, or the sidebar on the left. Would be interesting to see a screenshot of your setup."
    author: "Aaron Seigo"
  - subject: "Why themes?"
    date: 2008-12-10
    body: "I'm just curious: why does Amarok contain theming capabilities. Certainly, this makes the code more complicated (using non-standard widgets, for instance). Also, and probably more importantly, it makes Amarok look less integrated (non-standard widgets), however blending the default theme may be. I know that this is generally considered mean, but I can't help but compare to Windows Media Player.\n\nWhy does Amarok include themes support?\n\n"
    author: "Michael \"I'm seriously looking at Juk\" Howell"
  - subject: "Re: Why themes?"
    date: 2008-12-11
    body: "\"Theming\" is a bit of a broad term. It usually implies having user selectable themes, which Amarok does not support.\n\nMore generally:\n\nI don't think a media player is a typical desktop application, unlike an office word processor or a mail application. People expect a little more \"bling\" from it, and we try to balance the good looks with the desktop integration you seek.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Why themes?"
    date: 2008-12-23
    body: "I understand your point of view, but the Amarok widgets seems Oxygen based and don't integrate well with others themes.\n\nThe volume and progress bars are (IMO) poor designed, and I don't see a reason why they are not aligned.\n\nBut those are minor problems, this is a great .0 release."
    author: "Laerte"
  - subject: "Re: Why themes?"
    date: 2008-12-12
    body: ">> \"\"Theming\" is a bit of a broad term. It usually implies having user selectable themes, which Amarok does not support.\"\n\nCan we expect to see such feature someday? When I saw those theming capabilities  on the Amarok GUI I got horny.\n\nAnyways, Thanks to everyone who helped making the best audio player out there!"
    author: "xbullethammer"
  - subject: "I don't want to bash Amarok, but..."
    date: 2008-12-11
    body: "I been using KDE 4 since before it was released, and Amarok it's the only true disappointment.\n\nThe GUI is amazingly clutter and wrong designed (even if it isn't as bad as it looks), the playlist approach is so awful, it you don't have many songs per album it becomes incredibly clutter.\n\nIt has some new amazing features, but it lacks the basic ones! Come on, I can't filter my playlists? What the hell...\n\nAlso, as today, the Plasma context view is useless and buggy.\n\nKDE 4.x is awesome, but it lacks a decent media player (well, Dragon Player is good, but its mostly for viewing one video or listening one song and nothing more). "
    author: "Luis"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-11
    body: "I agree completely with this. I do not want to criticize right on release but I do want to make sure the developers are aware (And yes I'll be submitting these to the tracker as well).\n\n- Why are the play buttons so massively huge and do not support removal from the UI? I have buttons on my keyboard, I don't need to waste so much screen real estate to ugly buttons.\n- I have large amounts of singles, the current playlist is _completely_ (And I mean that) unusable when used with them. There is absolutely no graphical difference between an album name and a track name and due to the wasted vertical space I am forced to scroll more.\n- Where's playlist search? Why can't I use my playlist like I have been able to in other programs over the last decade? How do I use this new system?\n- Why can't I remove the Plasma area? I never use anything that's in it and whenever I resize it to nothing it just comes back after I minimize the window.\n\n</criticism>\n\n'Grats on the big release!"
    author: "Lucas Murray"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-11
    body: "It's hard to please all the people all the time. I personally like the UI, well playlist search could make things better but this is not the end of the world so let's just be patient and support the devs. We will all get our favorite features in due time :)"
    author: "Bobby"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-11
    body: "Actually, all these years, you wanted to use juk, and not amarok. But for some reason, you decided amarok was the player for you, and you coaxed it into resembling something it was not meant to be.\n\nAmarok was always about putting music in context, with information, lyrics and stuff. It never was about having a great huge playlist with all your songs. That is what juk and xmms are about.\n\nThe integration with web services is excellent, and I use it all the time. I do miss some of thy dynamic playlist modes (getting new songs similar to the one playing, for example), But amarok 2 is like KDE 4 a huge step forward in terms of smooth experience and beauty.\n\nOn a side note, I think the reason people mis-used amarok this long was that xine as a backend allowed nice playback, whereas juk being tied to arts inherited all its problems. Amusingly though, for me, the akode backend was the only amarok 1 backend that worked reliably -- and it was not supported, so I had to track changes in svn to keep it compiling :)"
    author: "hmmm"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-14
    body: "As stated in another thread, wanting to have a large playlist and having all of your music in one playlist are not the same thing.  Amarok's method of queueing tracks is much more powerful than the iTunes-style (like JuK), not to mention all of the other things that make amarok amarok, rather than some other player.  A dislike of one feature does not necessarily override the appreciation of many others."
    author: "Xiong Chiamiov"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-11
    body: "as a follow-up to point (2) The early development snapshots had the ability to collapse the individual songs thus hid album art etc. thus saving vertical space. \n\nWhat happened to it.\n\n3) All the other media players that I know have some sort of filtering as you type that works in the playlists. I generally find this to be quite a big omission. I used to have large playlists in Amarok 1.4 and it was nice due to the ability to filter.\n \nNow I have to have short playlists, then search the left hand bar, then drag it to the pud or playlist, then scroll the playlist (somewhat related to (2) then I can play the song/album etc I want.\n\n4) The plasma area is neat and it will only get better but there really should be the option of hiding it. \n\nOther than that I quite like it. Especially the integration with web services"
    author: "txf"
  - subject: "Better Audio-Player for KDE4:"
    date: 2008-12-11
    body: "... just use Amarok 1.4.10.  (it runs under KDE4, KDE3, Gnome, XFCE, ...)\n\nAmarok 1.4 just works incredible well, for me I don't see any reason to downgrade(!) to Amarok 2.\n\n\n\n"
    author: "hgj"
  - subject: "Re: Better Audio-Player for KDE4:"
    date: 2008-12-12
    body: "I, sincerely, started using Banshee, 1.4 feels very outdated (but it was awesome in its time)."
    author: "Luis"
  - subject: "Re: Better Audio-Player for KDE4:"
    date: 2008-12-14
    body: "What feature do you miss in Amarok 1.4.10?"
    author: "hgj"
  - subject: "Re: Better Audio-Player for KDE4:"
    date: 2008-12-15
    body: "I like Banshee video library, how you can browse by album covers, the way recommendations are implemented, and, basely, pure GUI stuff.\n\nHowever, I miss from Amarok the SQLite database and from Amarok 2 the Jamendo \"store\". "
    author: "Luis"
  - subject: "Re: Better Audio-Player for KDE4:"
    date: 2008-12-14
    body: "Although many of us have our complaints, I don't think most would categorize amarok 2 as a \"downgrade\".  Vista from XP, yes, the new last.fm, perhaps, but amarok 2, no."
    author: "Xiong Chiamiov"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-12
    body: "I must say that I agree with this comment. I keep on looking at the screenshots, and I'm just utterly confused. I have no idea what's going on. Everything is so cluttered and confusing.\n\nSay what you will about iTunes, but they got the UI right. It's simple and efficient. And somehow Banshee manages to be very simple and clean as well:\n\nhttp://www.davehayes.org/gallery2/d/5509-2/banshee.png\n\nFor comparison: Amarok 2.0:\n\nhttp://amarok.kde.org/files/Amarok-2_0_0-Overview_1.png\n\nPlease, for all that is good and holy, make the UI a top priority in 2.1! Feature-wise Amarok already has everything you need (apart from the few features that went missing in 2.0), what needs A LOT of work is how those features are presented. You don't HAVE to show the user EVERYTHING at once. How can Apple have a jukebox that also has a store, video-playback, playlists, ratings, albums and what have you, but still they manage to have an UI that is about order of magnitude simpler and cleaner?\n\nAre there any clean and simple jukeboxes for KDE? Is Juk still maintained? Because as far as Amarok is concerned, it seems to be getting more and more comoplicated all the time, with more and more confusing UI. It's starting to feel that using Amarok for playing back your music is more or less similar to using Boeing 747 for your daily 40 kilometer commute...."
    author: "Janne"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-12
    body: "Rather than judging usability from screenshots yoy should actually try using the application. It's rater slick and simple to use. \n\nJuk is maintained and still a part of the official KDE multimedia package."
    author: "Morty"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-12
    body: "\"Rather than judging usability from screenshots yoy should actually try using the application. It's rater slick and simple to use.\"\n\nScreenshots is what most people are going to use to judge the software. And if the screenshots do not reflect what the app is actually like, why are those screenshots being used to promote the software? "
    author: "Janne"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-17
    body: "So you are complaining against marketing, not usability. Really, I for one thought that A2 was an UI mess, but when I tried te 2nd beta I think, I changed my mind, completely"
    author: "Vide"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2008-12-12
    body: "I can't stand the UI in iTunes.\n\nNavigating to artists/songs/albums to make a playlist is a chore.  Heck, I'd take WMP over iTunes and that is saying something."
    author: "T. J. Brumfield"
  - subject: "Re: I don't want to bash Amarok, but..."
    date: 2009-01-22
    body: "I don't have a problem with Amarok's UI. Instead I find it baffling to see how sluggish and buggy this app is: taking 4 hours to build a collection of 11000 songs isn't okay, as isn't taking 2 minutes to bring up a playlist with 5000 songs. Not to mention the seemingly random crashes, or crashing on trying to guess tags from a song's name. It just feels like a beta app, really. JuK and Rhythmbox work fast, just how I want them to. Amarok 1.4 was no star either, but even it feels better than 2.0. Honestly, I just start the whole thing over again, it's that bad, I feel."
    author: "Cristian"
  - subject: "CONGRATS! YOU ROCK!!!"
    date: 2008-12-11
    body: "Congrats to devs!! Awesome work!!again YOU ROCK!\n\nI do have these two weird issues though. One of them is that i cant seeks .flac files with phonon-xine-backend...odd...(Gstreamer makes this ear-killing scratching) and other one is that it doesn't save my \"layout\" settings. I like to keep it with just the middle and the right one (looking a little bit like Amarok 1.x), but everytime i restart Amarok it \"pushes\" the left \"panel\" and sort of ruins that setup by changing the sizes and so on...\n\nI also miss osd :'( it's just so much fun to watch...bouncing with the music and all... ;D any idea when phonon dudes/chicks \"lets\" you have it?"
    author: "Huaaa"
  - subject: "Re: CONGRATS! YOU ROCK!!!"
    date: 2008-12-11
    body: "This thread explains how to get seeking work with FLAC and xine:\n\nhttp://amarok.kde.org/forum/index.php?topic=13977.msg23743\n"
    author: "Mark Kretschmann"
  - subject: "Missing features. . ."
    date: 2008-12-11
    body: "1) Custom collection grouping.  I like my collection grouped by Genre->Album.  I couldn't care less about grouping it by artist and now the only way I can get it the way I want is to set all my songs' artists to NULL or something like that.\n\n2) Stop after this track is finished.  I loved this feature of Amarok 1.x.  I often listen to music while web browsing and when I come across a video I want to watch, I generally also do not want to stop my music until the current track is finished, so I use this feature and when the music automatically stops I start up the video."
    author: "Ostsol"
  - subject: "Re: Missing features. . ."
    date: 2008-12-11
    body: "Well, both of those features will return, so don't you worry!"
    author: "Dan"
  - subject: "Congratulations, but ..."
    date: 2008-12-11
    body: "IMHO the new GUI simply sucks!\n\nThe problem I have with it is, that, for me!, there is absolutely no need for the middle / context column which occupies the most space and is, apparently, impossible to get rid of and the actual playlist shows barely no information and needs 2 lines if songs are from different artists.\n\nNow that wouldn't be any problem at all if you (the Amarok devs) hadn't repeatedly said that you have no intention to change this.\n\nSo let me propose another solution:\n\n1. The title of the currently playing song gets moved above the progress bar.\n\n2. The rest of the context column gets moved into a separate tab.\n\n3. The playlist gets extended over the middle column and get its old, \"excel\" style, appearance which shows track name, length, album, artist and so on.\n\n4. The left column shows the album cover on top and other albums from the same artist listed bellow (I use this & the ability to filter playlists all the time)\n\nNow, as you might noticed, this describes the Amarok 1.4 user interface. Flame me all the way to hell & back but IMHO it is lightyears ahead of what is in Amarok 2.\n\nOR: Since layout / appearance is a matter of taste and one shouldn't argue about taste: Since you already started to use plasmoids inside Amarok: How about you continue down this road and make the whole interface consist of plasmoids which can be configured & placed where one prefers them?\n\nE.g. a \"Show album cover\" one and a \"List collection information\" which can be filtered to Artists, Albums, Tracks, Genres and so on. If you then add the ability to somehow connect those plasmoids - e.g. 2 \"List collection information\" plasmoids, one shows artists and one shows albums and selecting a different artist triggers listing the albums of the just selected artist in the other one - one can simply create ones personal interface of choice and everyone is happy. Of course we would also need a \"Playlist\" plasmoid which can show any tag as column one wants.\n\nIMHO the plasmoid way would be the best since it would have the power to make everyone happy (regarding the gui) and would be easily extendable (just like the desktop currently starts to show).\n\nTherefore, _please_ give it a thought!"
    author: "Stephan"
  - subject: "another \"but\":"
    date: 2008-12-11
    body: "(1) Why does Amarok always need to implement its own widget styles?\n\nI really do NOT like the progressbar and the volume bar in Amarok. The one from Oxygen/Bespin just looks much better. \n\nAnd I also do NOT like the vertical buttons on the left hand side. again: The native vertical buttons are MUCH better.\n\n(2) Amarok2 has the same big \"bug\" in the interface as Amarok1.4:\nThe left pane is too small for many options (at least in German). If I activate \"Wiedergabelisten\" (\"Playlists\"), then the widget in the left pane wants to cover hald of the whole window (obviously it covers only a third and, thus, has scrollbars!) Ugly and really not very usable. \n\n\nSorry - I really liked the very first versions of amarok - but since 1.4 I frequently started using JuK!! "
    author: "Sebastian"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "JuK caters to a very different usage scenario than Amarok as it is based almost entirely around searching in the playlist. \n\nAs Amarok cannot possibly be everything to everyone, I actually have no qualms about recommending JuK to people whose usage pattern just does not fit well with Amarok. :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "Well - JuK is polished, amaroK not (at least at the mentioned points). Therefore, I learned to use the other one... "
    author: "Sebastian"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "Then again, Juk didn't go through a major redesign last year... Just wait for Amarok to get it's capabilities and flexibility back. Using Juk is totally OK, btw ;-)"
    author: "jospoortvliet"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "To each his own. Juk has a simple interface and isn't complicated but it doesn't really cater for online people like me. \nAmarok is simply the best and even though Amarok 2 doesn't satisfy the needs and taste of everyone (no media player can), it's more poised at a broader masses that Juk and it's prettier, at least to my taste."
    author: "Bobby"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "I did not want to flame Amarok, nor praise JuK. i am sorry if this impression got evident. I just listed some bugging properties of Amarok's interface... "
    author: "Sebastian"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "Actually I didn't get the impression that your are flaming. I know you by now. We all have different tastes, preferences and priorities within our use of every programme, which of course we are free to express. We wouldn't have such a great variety and choices otherwise."
    author: "Bobby"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "It's perfectly fine that you prefer JuK. I've used it sometimes myself and I have to say that JuK is a very well made application.\n\nIt also happens that we're pretty much friends with the JuK developers (at least I know Scott Wheeler personally, and he's a great guy), and Amarok even uses an excellent library (TagLib) which was originally written for JuK.\n\n\nSo, no problems there, everyone can be happy :)\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "Which doesn't address anything I said in the slightest bit ...\n\nAnd no, currently I can't be happy because the stuff that made Amarok 1.4 a great music player and a great tool to manage my collection is missing in v2.0. And in exchange I got that middle context column I have absolutely no need for (which would be perfectly ok if it would be _possible_ to put it in another tab) and which takes up most of my screens real estate instead of letting me use it for something I actually need / want - e.g. a detailed playlist.\n\nAgain, I have no problem if that is just the default layout _IF_ it can be changed . So, to put this question short: \n\nDo you (the Amarok developers) still have no plans to make that column optional?\n\nI'm sorry if this sounds impolite but currently I'm kinda desperate cause I really love Amarok 1.4 but most of the stuff I love / need / use is gone from v2 and it wont return (if earlier blog entries are still valid).\n\nOnce again: Please consider using plasmoids for _all_ of the user interface. This way it would be configurable and everyone could create ones prefered layout. Or even write ones own plasmoids so it can be easily extended as well."
    author: "Stephan"
  - subject: "Re: another \"but\":"
    date: 2008-12-11
    body: "See, there is a music player that corresponds to what you ask! In KDE! By default!\n\nIt is Juk. Juk is meant to have one big playlist with all your songs. This is what you want, so use the program which does things your way instead of whining!\n\nSome of us want a music player that puts music in context. This is what amarok does. Some of us want seamless integration with community music sites. This is what amarok does.\n\nSome of us want a big playlist with all their songs, this is what juk does, juk is the default also because it caters to users who have simpler needs. \n\nSo instead of asking (not even nicely) people to do something they don't want to (because it is ALREADY done, and they want to do something innovative) JUST LOOK INTO YOUR BLOODY K-MENU AND TRY OUT PLAYERS UNTIL YOU FIND ONE WHICH SUITS YOU.\n\nJuk actually was better suited to your \"needs\" right from the start. Of course it is simpler to insult people than applying intelligence... Or say even look randomly."
    author: "hmmm"
  - subject: "Re: another \"but\":"
    date: 2008-12-12
    body: "1. I know Juk and it is not what I want. That is why I use Amarok and am very happy with it (also cause it doesn't force all those \"community\" things right in my face but lets me enable or disable them depending on my preferences. And that's how it should be IMHO).\n\n2. I wasn't under the impression that asking to make something optional which I don't need and simply, for me, is a waste of screen real estate (and, according to the overall feedback I'm not alone with that) is an insult.\n\nPerhaps it could have been put down in a better way but english isn't my mother tongue and neither was it my intention to insult anyone nor do I think I did this.\n\nSo, thank you for addressing anything I said and not running the usual \"It's free and therefore you aren't allowed to criticize it\" aka \"Everything except praising is whining which is forbidden\" crap ..."
    author: "Stephan"
  - subject: "Re: another \"but\":"
    date: 2008-12-14
    body: "You will notice that he asked for a playlist that has more information.  While JuK most certainly does that, it displays *all* of your music in one playlist (which you pointed out), which is one of the great things that amarok does _not_ do, allowing for much more flexibility in creating a list of music to listen to.\n\nI understand Stephan's sentiments very much, as I have many similar feelings from what I've seen of amarok2 (I have not yet tried it out, so keep that in mind while reading what I have to say).\n\nFirst, I am incredibly tied to amarok because of it's support for \"Various Artist\" grouping.  While I originally did not like amarok's UI (as I was used to the winamp-style 3-panel interface), I couldn't find anything else that would handle compilation albums correctly.  I have, among other things, the entire archives of Over-Clocked Remix and Remix.Kwed.Org, which pollute my artist listing so much that I cannot browse my music by artist in any other player.  So then, understand that that one feature by itself prevents me from switching to anything else, although there are quite a few other very nice things I enjoy about amarok.\n\nThe so-named \"Excel-interface\" allows me to quickly process my playlist visually.  I can, at a glance, see the distinction between artists, albums and song ratings.  From the screenshots, it looks like less of this information is displayed, and while albums and artists are very visually distinguished, the fact that each song gets 2 lines (if it is not adjacent to another song by the same artist) cuts down my \"visual grepping\" ability significantly.  The fact that the left alignment of track names differs depending on the presence of an album cover, or whether adjacent tracks are in the same album, (as seen in screenshot #2 on the screenies page) is just another little annoyance.\n\nI am a heavy user of Last.fm (as well as Pandora, and an occasional buyer of music from Magnatune), so I *like* the integration with so-called \"community music\" sites.  However, I am willing to sacrifice a click or two to get most of that information for the sake of my playlist, as the latter is what I spend most of my time looking at.  The same goes for the lyrics.\n\nNow, I most certainly don't expect everyone to have the same preferences as me.  As the OP stated, \"appearance is a matter of taste and one shouldn't argue about taste\".  Of course, any project has to choose a direction to head in.  The fact of the matter, though, is that there really is no other media organizer that can compare to amarok in terms of features.  If you want a lightweight player, then amarok is not a good choice for you, but it most certainly is powerful, and the number of GNOME (or rather, non-KDE) users who use amarok as their media player is proof of that.  Just the fact that it is developed primarily for Linux is bound to lead to more complaints about the interface, and such, and we are used to having many, many choices for everything, and being able to customize [application] if we don't like any of the presets.  I have a feeling there are many Linux users who, like me, cannot give up using Firefox, no matter how bloated it gets, simply because they have installed an incredible number of addons that they cannot live without.\n\nNow, I'm sure I've forgotten whatever I was originally trying to say, but I want to make it clear that not all of us who disapprove of _parts_ of the new interface are nay-sayers who shouldn't be using amarok.  I really, *really* like amarok; in fact, it's one of my top 5 applications.  I'm also very excited about the new features in a2, but I'm holding back a little until it becomes a player that I won't dislike, because I really don't want to dislike it.  I'm hopeful, though, from reading some of the dev's comments, that it won't be like KDE4 - pretty, but without any of the customization that we've gotten used to (read Machiavelli?).  In fact, KDE4's interface drives me so nuts that it started me on a search for another wm to use (ended up with awesome), although I still use mostly KDE applications, because everything besides Plasma ended up really nice!\n\nSo, devs, don't take any of this criticism hard.  We criticize because we love. :)"
    author: "Xiong Chiamiov"
  - subject: "Re: Congratulations, but ..."
    date: 2008-12-11
    body: "Note that Amarok 2.0 is just the beginning - using plasma for the whole interface is actually something they'd love to do, but currently the infrastructure (X.org, graphics drivers, Qt and KDElibs/Plasma) don't allow it. In time, it'll probably happen."
    author: "jospoortvliet"
  - subject: "Re: Congratulations, but ..."
    date: 2008-12-14
    body: "This is utterly wrong, by the way.\n\nWe've only theorized, one afternoon, about the possiblity of making amarok one graphicsview and allowing for configuration of everything.  We pretty much nixed this because we really don't want to develop a lego ui, and the work to implement this would be extreme to satisfy only a small subset of power users.\n\nThe infrastructure was not a problem with this idea at all, actually."
    author: "Dan"
  - subject: "Congrats!"
    date: 2008-12-11
    body: "Congrats on the release. Looking forward to trying it!"
    author: "Joergen Ramskov"
  - subject: "Oh NOOO, the GUIIIII!"
    date: 2008-12-11
    body: "It's seems to me a bit that \"oh no the awful GUI\" bashing is rather hip, so I counter with \"I really _LIKE_ the new GUI, so AmaROCK-ON you Dev-Guys!!!!!\"\n\n"
    author: "Matthias"
  - subject: "Your mother's fat!"
    date: 2008-12-11
    body: "Wow, that's an ugly baby!\n\nHoly hell, I hate your new car!\n\nYou bought *that* house??!?!\n\nYour novel *sucks*!\n\nYour genitals make me laugh!\n\n...anybody have any more?  I mean, there's feedback, and then there's crushing souls, and clearly the Dot's comments has moved into \"the last year of your life that you gave up to craft something free to the public is worthy of nothing but derision\" territory.\n\nThank you for the nice write up, Lydia."
    author: "Evan \"JabberWokky\" E."
  - subject: "Amarok 2 royally SUX!!"
    date: 2008-12-11
    body: "I tried the new Amarok 2 and found it to be much inferior to the older version.\nAmarok 2 is harder to use and lacks many features of the older version.\nI couldn't import years worth of playlists, no equalizer, couldn't rip or burn an audio CD among my numerous complaints with it."
    author: "Jennifer Cato"
  - subject: "Re: Amarok 2 royally SUX!!"
    date: 2008-12-11
    body: "it's the very first release of a major rewrite, a redevelopment for the KDE community that occured whilst KDE4 was still being finalised (API Changes, code changes, etc. etc.)\nNot all features are in this version as there is in 1.4, but the mere fact they've managed to release something this quickly, so soon after KDE4 was released is a massive achievement.  It's the first release, and I am sure they know more than anyone what work lays ahead.  As for me, I am so very thankful that there has been developers who have worked so hard to bring us this release, and I know from past experience, the Amarok team will make future releases of Amarok something the entire KDE community will be proud of.\nThankyou developers, for making my KDE experience so much better. "
    author: "Judd Baileys"
  - subject: "Theodore Roosevelt"
    date: 2008-12-11
    body: "To all the whiners, i give you this... its a quote that comes up in my bugzilla searches :D\n\n\"It is not the critic who counts: not the man who points out how the strong man stumbles or where the doer of deeds could have done better. The credit belongs to the man who is actually in the arena, whose face is marred by dust and sweat and blood\" - Theodore Roosevelt\n\nSo to the people that feel the need to whine and complain about something that they get for free, without ever putting themselves on the line and giving something back................... well you get the idea\n"
    author: "Gareth"
  - subject: "Outdated users..."
    date: 2008-12-11
    body: "...is there really no chance that the missing \"outdated\" features which didn't make it into Amarok2 and are not intended to be included any more (searchable playlist and stuff) will be implemented? It seems the official party line in this case is \"use juK\", but I'd rather like a amarok 1.4 brought up to KDE4 standards...\n\nPlease please Amarok devs! Maybe then I'll begin to donate again... seems fund raising is not going as well as it did in former times..."
    author: "Anon"
  - subject: "Re: Outdated users..."
    date: 2008-12-12
    body: "It's not a party line. Since when the complainers are asked what their problem are, in most cases their explenation end up describing Juk. It's alll about pointing the user to the correct tool. "
    author: "Morty"
  - subject: "Re: Outdated users..."
    date: 2008-12-12
    body: "Nobody's talking about JuK, but about Amarok 1.4-features. I'd like to have the new features of Amarok 2, the eyecandy a la KDE 4, together with the cleaned-up and easy-to-use, powerful interface of Amarok 1.4. Yes, JuK has a clean and non-cluttered playlist-based interface. But it lacks, compared to Amarok, lots of features I like.\n\nDon't you guys realize, that even if programming is done in your spare time, the complaining users are your \"customers\", and if asked to vote with their feet against Amarok (as you do with saying \"use JuK and don't whine\"), the \"customers\" maybe will do that?!\n\nI really don't like the attitude which seems to prevail in some KDE4-projects nowadays, in which users complaining about the developments in their favorite programs are more or less told to \"shut up\".\n\nAt least KDE4 is finally shaping up and K3B and Digikam are, even in their KDE4 incarnation, still the apps I like, so this seems to be possible. "
    author: "Larx"
  - subject: "Re: Outdated users..."
    date: 2008-12-12
    body: "'Don't you guys realize, that even if programming is done in your spare time, the complaining users are your \"customers\"'\n\n++ Does Not Compute ++\n++ Unpaid Volunteers Do Not Have Customers ++\n\n'I really don't like the attitude which seems to prevail in some KDE4-projects nowadays, in which users complaining about the developments in their favorite programs are more or less told to \"shut up\".'\n\nActually, the prevailing theme I see is \"we're working on it\".  People, of course, would much rather be perverse and simply assume that the Bad Old Incompetent KDE Guys are purposefully working against everyone's (including their own) interest because, hey, melodrama and Raging Against The Man are more fun.\n\n\"At least KDE4 is finally shaping up\"\n\nFun Fact (assuming by \"KDE4\" you mean \"Plasma\") - Plasma is the poster-child project for the \"OMG KDE devs don't listen to their users\" brigade.  Now - what does the fact that, by your own admission, it's \"shaping up\" tell you about the attitude the devs have towards their users? The answer may surprise you!! \n\nA hint, in case you can't figure out the reason: I'm sure there will be lots of reports of Amarok 2 \"shaping up\" as it progresses."
    author: "Anon"
  - subject: "Re: Outdated users..."
    date: 2008-12-12
    body: "Customer is in common speech somebody who aquires and uses a product, no matter whether it is produced by billionaires, slaves or volunteers.\n\n\"We are working on it\": then please tell me why Amarok 2 developpers' blogs - which I am as a Amarok 1.4 fan am following, are you? - show a pretty negative attidude towards quite some of the features users are missing since Amarok 1.4? Old-style-playlist, searchable playlist, cleaner layout, and stuff?\n\nKDE4: I never said I'm completely happy with it. However, slowly but gradually, it's getting usable and more stable. Nevertheless I don't like some of the developpers' attitude towards the user base.\n\nTo your hint: How will Amarok 2 be shaping up if developpers say they don't care about those features? By some magic or what? Yes, I'm sure, Amarok 2 will gradually integrate more of the features, but it seems it will take a lot of user's bickering..."
    author: "Larx"
  - subject: "Re: Outdated users..."
    date: 2008-12-12
    body: "\"Customer is in common speech somebody who aquires and uses a product, no matter whether it is produced by billionaires, slaves or volunteers.\"\n\nhttp://dictionary.reference.com/browse/customer\n\n1. \ta person who *purchases* goods or services from another; buyer; patron.\n2. \tInformal. a person one has to deal with: a tough customer; a cool customer. \n\nIf your intended meaning was 2., I can hardly think of a worse choice of word.\n\n\"To your hint: How will Amarok 2 be shaping up if developpers say they don't care about those features?\"\n\nHow will Plasma \"shape up\" if developpers say they don't care about those features?"
    author: "Anon"
  - subject: "Re: Outdated users..."
    date: 2008-12-12
    body: "Users -- whether you want to call them customers or not -- do not have any value in open source beyond the egoboo having many users may give contributors, or the possibility that they might become contributors. It's no problem if people go and use or implement something else -- it really isn't.\n\nThat said, many developers are human enough to like to please people, so they like to please users, too. But if they have a strong vision, they won't compromise on that vision just to please people. The amarok developers have a vision, and they implement on that vision. They have more than once explained that vision, and no doubt they consider that amarok is shaping up very nicely according to that vision. I don't use amarok -- I play my music with ogg123 on the command line -- but I can see from the screenshots that amarok is indeed the application the developers have been telling us they want to develop.\n\nAnd that should be enough for you, too: they have told you what they want to do, they have done what they said they wanted to do, and they have given you a roadmap telling you what else they want to do. If you don't like that, tant pis, but it's not a failure on the part of the developers, it's a failure on your part to realize that in this new world you now longer have to make do with what you're given, but that you can make things yourself, if other people don't give you what you crave.\n\nYou like what you're given? Say thank-you to the people who have given you this gift. You like it but want it improved? Contribute. You don't like it at all? Create something that's more to your liking, and share it. But don't act as if people owe you something or as if your opinions, criticism or ideas have any value in and of themselves: they haven't. Your chances of telling the people doing the work something they haven't considered before are vanishingly small. Only actual contributions have worth. That's the payment you as a \"customer\" can give the project."
    author: "Boudewijn Rempt"
  - subject: "Re: Outdated users..."
    date: 2008-12-14
    body: "You did not do a wonderful job of reading the release announcement (or we did not do a wonderful job of writing it, but the wording seems fairly clear to me..)\n\nA searchable playlist, a queue, and many other amarok 1 features are going to return.  Equalizer, visualizations, and most other features from amarok 1 are coming back.\n\nThe only things that are definatly not coming back are the Player window and multiple database support.  Thats not much to lose, considdering that the former can be replaced easily by applets and the latter is not a huge deal no matter how you slice it."
    author: "Dan"
  - subject: "Different output on Interface. "
    date: 2008-12-11
    body: "I think the Amarok team really tried something different with the UI on this release. Personally, just looking at pictures, it looks great for me but I guess alot of people just don't like it. \n\nI haven't used it, so I don't know on usability but I can say congrats on this release. Probably the most missed application in KDE 4 and the last version of amarok was too outdated for me. "
    author: "Jeremy"
  - subject: "Re: Different output on Interface. "
    date: 2008-12-12
    body: "Well it is a bit clunky, but web services are quite well integrated. The plasma area is currently a big annoyance for many. I can see where they are coming from in that it doesn't do anything particularly new in comparison to the old sidebar with wikipedia and stuff, however knowing it is powered by plasma I have high hopes for the future."
    author: "txf"
  - subject: "Feature comparison vs version 1.4 ?"
    date: 2008-12-12
    body: "Thanks for your fine work !\n\nI was wondering if there's a list somewhere of features that were in 1.4 but are not (yet ?) in amarok 2 ?\n(No I won't use it to complain about missing features ;))"
    author: "Chris C"
  - subject: "Re: Feature comparison vs version 1.4 ?"
    date: 2008-12-12
    body: "Yes, please see this article on my blog:\n\nhttp://amarok.kde.org/blog/archives/809-Missing-features-in-Amarok-2.html\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Feature comparison vs version 1.4 ?"
    date: 2008-12-12
    body: "Have a look here: http://amarok.kde.org/blog/archives/809-Missing-features-in-Amarok-2.html\n\n:-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "I want..."
    date: 2008-12-12
    body: "This is an \"I want\" list, not an \"Amarok devs are obligated to capitulate to my requests\" list.\n\nI want a media player that hides and doesn't get in the way when I'm working.\nI want a media player that allows me to quickly find and queue up music.\nI want a media player that doesn't choke on 10,000 songs.\nI want a media player that helps me discover new music.\nI want a media player that helps me organize my library and edit tags.\nI want a media player that allows me to sync with portable media players.\nI want a media player that allows me to rip and burn from my library.  Yes, there are dedicated ripping and burning apps, but I want to be able to burn the playlists I made in my media player, and have ripped tracks immediately added to my library.  It makes sense for this to be in my media player.\n\nAmarok adds functionality that I didn't initially want or expect (tons of great contextual information about my music from Wikipedia), but neither version 1 nor 2 fulfill my above criteria.  Sadly, the best media player for my above list is WMP right now, and I hate the UI.\n\nAs far as the UI revolution of Amarok 2, I see why people feel strongly about it, either positively or negatively.  Heck, all I want is the power to customize it, and then I think most people will be happy.\n\nI'd also like to see Amarok develop some \"common sense\" features.  I haven't used 2 very much, but version 1 had issues importing my library that started on Windows with WMP.  I had tags on all my MP3s, but Amarok split albums and artists all over the place on case sensitivity issues.  I don't know why.  I'd have an album tagged in Windows, boot into Linux and Amarok would see it as two albums with case sensitivity issues, when the entire album is tagged the same way in Windows with no case differences.  Amarok would change artists and split The Beatles into Beatles and The Beatles, when all my songs were tagged The Beatles to begin with.\n\nThe two-disc Phantom of the Opera soundtrack suddenly became 4 albums, with it messing up artist and album title tags.\n\nAmarok does some things I love that no one else does, but it seems to me that no one gets the essentials right yet.http://amarok.kde.org/en/node/48"
    author: "T. J. Brumfield"
  - subject: "Debian Sid Amarok 2.0.1"
    date: 2008-12-13
    body: "From commandline:\n\namarok(10159) Phonon::KdePlatformPlugin::createBackend: using backend:  \"Xine\"    \nQLayout: Attempting to add QLayout \"\" to MainWindow \"MainWindow\", which already has a layout\nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nObject::connect: No such slot VolumeWidget::setVolume(int)                                  \nQWidget::insertAction: Attempt to insert null action                                        \nQWidget::insertAction: Attempt to insert null action                                        \nQWidget::insertAction: Attempt to insert null action                                        \nQWidget::insertAction: Attempt to insert null action                                        \nQWidget::insertAction: Attempt to insert null action                                        \nQWidget::insertAction: Attempt to insert null action                                        \nObject::connect: No such slot MainWindow::slotMenuActivated(int)                            \nObject::connect:  (receiver name: 'MainWindow')                                             \nQWidget::insertAction: Attempt to insert null action                                        \nObject::connect: No such slot MainWindow::slotMenuActivated(int)                            \nObject::connect:  (receiver name: 'MainWindow')                                             \namarok(10159) Plasma::Applet::save: saving to \"1\"                                           \namarok(10159) Context::ContextView::setContainment: \"\" Line:  603                           \namarok(10159) Plasma::ThemePrivate::config: using theme for app \"amarok\"                    \namarok(10159) Plasma::Applet::save: saving to \"2\"                                           \namarok(10159) Plasma::Applet::save: saving to \"3\"                                           \namarok(10159) Plasma::Applet::save: saving to \"4\"                                           \namarok(10159) CurrentTrack::dataUpdated: CurrentTrack::dataUpdated                          \nlink XMLID_5_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3709                                               \nlink XMLID_9_ hasn't been detected!                                                         \nlink XMLID_9_ hasn't been detected!                                                         \namarok(10159) Context::ColumnContainment::insertInGrid: \"\" Line:  602                       \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \nlink XMLID_7_ hasn't been detected!                                                         \nlink XMLID_7_ hasn't been detected!                                                         \nCouldn't resolve property: radialGradient3986                                               \namarok(10159) MagnatuneConfig::load: load                                                   \namarok(10159) CurrentTrack::dataUpdated: CurrentTrack::dataUpdated                          \namarok(10159) CurrentTrack::dataUpdated: CurrentTrack::dataUpdated                          \namarok(10159) CurrentTrack::dataUpdated: CurrentTrack::dataUpdated                          \nX Error: BadWindow (invalid Window parameter) 3                                             \n  Major opcode: 20 (X_GetProperty)                                                          \n  Resource id:  0x420000c                                                                   \nQPixmap: Invalid pixmap parameters                                                          \nX Error: BadAlloc (insufficient resources for operation) 11                                 \n  Major opcode: 53 (X_CreatePixmap)                                                         \n  Resource id:  0x13b                                                                       \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 4 (RenderCreatePicture)                                                     \n  Resource id:  0x4200493                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 8 (RenderComposite)                                                         \n  Resource id:  0x4200494                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x4200493                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x4200493                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 5 (RenderChangePicture)                                                     \n  Resource id:  0x4200494                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x4200496                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x4200495                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 73 (X_GetImage)                                                             \n  Resource id:  0x4200493                                                                   \nQPixmap: Invalid pixmap parameters                                                          \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 7 (RenderFreePicture)                                                       \n  Resource id:  0x4200494                                                                   \nX Error: BadPixmap (invalid Pixmap parameter) 4                                             \n  Major opcode: 54 (X_FreePixmap)                                                           \n  Resource id:  0x4200493                                                                   \nX Error: BadAlloc (insufficient resources for operation) 11                                 \n  Major opcode: 53 (X_CreatePixmap)                                                         \n  Resource id:  0x13b                                                                       \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 4 (RenderCreatePicture)                                                     \n  Resource id:  0x420049b                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 8 (RenderComposite)                                                         \n  Resource id:  0x420049c                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x420049b                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x420049b                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 5 (RenderChangePicture)                                                     \n  Resource id:  0x420049c                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x420049e                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x420049d                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 73 (X_GetImage)                                                             \n  Resource id:  0x420049b                                                                   \nQPixmap: Invalid pixmap parameters                                                          \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 7 (RenderFreePicture)                                                       \n  Resource id:  0x420049c                                                                   \nX Error: BadPixmap (invalid Pixmap parameter) 4                                             \n  Major opcode: 54 (X_FreePixmap)                                                           \n  Resource id:  0x420049b                                                                   \nX Error: BadAlloc (insufficient resources for operation) 11                                 \n  Major opcode: 53 (X_CreatePixmap)                                                         \n  Resource id:  0x13b                                                                       \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 4 (RenderCreatePicture)                                                     \n  Resource id:  0x42004a2                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 8 (RenderComposite)                                                         \n  Resource id:  0x42004a3                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x42004a2                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x42004a2                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 5 (RenderChangePicture)                                                     \n  Resource id:  0x42004a3                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x42004a5                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x42004a4                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 73 (X_GetImage)                                                             \n  Resource id:  0x42004a2                                                                   \nQPixmap: Invalid pixmap parameters                                                          \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 7 (RenderFreePicture)                                                       \n  Resource id:  0x42004a3                                                                   \nX Error: BadPixmap (invalid Pixmap parameter) 4                                             \n  Major opcode: 54 (X_FreePixmap)                                                           \n  Resource id:  0x42004a2                                                                   \nX Error: BadAlloc (insufficient resources for operation) 11                                 \n  Major opcode: 53 (X_CreatePixmap)                                                         \n  Resource id:  0x13b                                                                       \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 4 (RenderCreatePicture)                                                     \n  Resource id:  0x42004a9                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 8 (RenderComposite)                                                         \n  Resource id:  0x42004aa                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x42004a9                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 55 (X_CreateGC)                                                             \n  Resource id:  0x42004a9                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 5 (RenderChangePicture)                                                     \n  Resource id:  0x42004aa                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x42004ac                                                                   \nX Error: BadGC (invalid GC parameter) 13                                                    \n  Major opcode: 60 (X_FreeGC)                                                               \n  Resource id:  0x42004ab                                                                   \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Major opcode: 73 (X_GetImage)                                                             \n  Resource id:  0x42004a9                                                                   \nQPixmap: Invalid pixmap parameters                                                          \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 7 (RenderFreePicture)                                                       \n  Resource id:  0x42004aa                                                                   \nX Error: BadPixmap (invalid Pixmap parameter) 4                                             \n  Major opcode: 54 (X_FreePixmap)                                                           \n  Resource id:  0x42004a9                                                                   \nX Error: BadAlloc (insufficient resources for operation) 11                                 \n  Major opcode: 53 (X_CreatePixmap)                                                         \n  Resource id:  0x13b                                                                       \nX Error: BadDrawable (invalid Pixmap or Window parameter) 9                                 \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 4 (RenderCreatePicture)                                                     \n  Resource id:  0x42004b0                                                                   \nX Error: RenderBadPicture (invalid Picture parameter) 176                                   \n  Extension:    153 (RENDER)                                                                \n  Minor opcode: 8 (RenderComposite)                                                         \n  Resource id:  0x42004b1\nX Error: BadDrawable (invalid Pixmap or Window parameter) 9\n  Major opcode: 55 (X_CreateGC)\n  Resource id:  0x42004b0\nX Error: BadDrawable (invalid Pixmap or Window parameter) 9\n  Major opcode: 55 (X_CreateGC)\n  Resource id:  0x42004b0\nX Error: RenderBadPicture (invalid Picture parameter) 176\n  Extension:    153 (RENDER)\n  Minor opcode: 5 (RenderChangePicture)\n  Resource id:  0x42004b1\nX Error: BadGC (invalid GC parameter) 13\n  Major opcode: 60 (X_FreeGC)\n  Resource id:  0x42004b3\nX Error: BadGC (invalid GC parameter) 13\n  Major opcode: 60 (X_FreeGC)\n  Resource id:  0x42004b2\nX Error: BadDrawable (invalid Pixmap or Window parameter) 9\n  Major opcode: 73 (X_GetImage)\n  Resource id:  0x42004b0\nX Error: RenderBadPicture (invalid Picture parameter) 176\n  Extension:    153 (RENDER)\n  Minor opcode: 7 (RenderFreePicture)\n  Resource id:  0x42004b1\nX Error: BadPixmap (invalid Pixmap parameter) 4\n  Major opcode: 54 (X_FreePixmap)\n  Resource id:  0x42004b0\namarok(10159) CurrentTrack::dataUpdated: CurrentTrack::dataUpdated\n\n\nDouble-clicking on a CD from my collection immediately crashes the application:\n\nAnd it pukes this up:\n\nASSERT: \"s_instance->m_nullPort\" in file /home/mdriftmeyer/Temp/KDE/kdebase-runtime/kdebase-runtime-4.1.3/phonon/xine/xineengine.cpp, line 256\nKCrash: Application 'amarok' crashing...\nsock_file=/home/mdriftmeyer/.kde4/socket-horus/kdeinit4__0\nASSERT failure in QList<T>::operator[]: \"index out of range\", file /usr/include/qt4/QtCore/qlist.h, line 395\nUnable to start Dr. Konqi\n\nNice to see they got this sewed up.\n\nNice to see Kdenetworking still has bugs in it, eh? Dr. Konqi?\n\nClearly it's an issue:\n\nhttps://bugs.launchpad.net/kdenetwork/+bug/188836\n\nEither Debian needs patches, Amarok was built against a different version of KDE 4.1.3 in Experimental or it's a known issue with Kdenetwork and QList range issues that must wait on a fix from Trolltech?"
    author: "Marc J. Driftmeyer"
  - subject: "Suggestions."
    date: 2008-12-14
    body: "I think Amarok 2 is very close to the greatest player you can imagine... but it's missing the mark. Let's make some suggestions.\n\n1. The tab bar, in the side, is causing me a LOT of headaches. To use Internet Services, to browse Shoutcast, to enable Ampache or to browse my files, I have to:\n\na) Turn my head sideways and read the tab titles.\nb) Resize my Amarok 2 window, to avoid losing my useful Plasmoids.\n\n2. The placement of the playlist is odd. I must avoid falling in the Plasma hole while dragging my music files from the collection view to the playlist.\n\nMy suggestion? SCRAP COMPLETELY THE SIDED TAB BAR, and replace it with:\n1. A straight tab bar at the top of the Plasma area.\n2. A reimplementation of all the interface you've created (I mean, the buttons, the trees, BUT NOT THE API) in Plasma. It should take you no time, because you have all the framework in place.\n3. You can make possible to add custom tabs, integrating them as Plasma activities. In fact, in my view, the entire File browser , Internet provider browser, Playlist browser and the like should be LOCKED PLASMA ACTIVITIES. If you want to create your own activities, you can (like you do now). Delete unnecessary duplication, use the impressive Plasma framework and take it to its next level, that is your work for 2.1.\n\nIf you do it the way I'm thinking, you'll have a two sided player, with a Plasma container in the left, and the playlist in the right. If you want to browse your collection, select the appropiate tab and you have it. And if you want to add later your Visualization plasmoid or your Video plasmoid, you'll have the screen real estate you need.\n\nGo, guys. It's not that difficult."
    author: "Alejandro Nova"
  - subject: "Cool"
    date: 2008-12-16
    body: "This is the amarok I've been waiting for. Big thumbs up to the devs!"
    author: "Gazza"
  - subject: "Oh the Bazaar... "
    date: 2008-12-17
    body: "They should have named it something else so that the whining users didn't tag along..."
    author: "frozen"
  - subject: "Really nice!!! Thanks!"
    date: 2008-12-17
    body: "Downloaded and installed it yesterday! Played music all night!\n\nReally, really nice!\n\nNo complaints at all. It runs like a charm on KDE 4.1.\n\nThanks!!!\n\n\n\n\n\n"
    author: "dkfjh"
---
After two years of intense development, Amarok 2 has become a reality! Some of the highlights that are included in the 2.0 release are a completely redesigned user interface, tight integration with online services such as Magnatune, Jamendo, MP3tunes, Last.fm and Shoutcast.  There is an overhauled scripting API and plugin support to allow better integration into Amarok.  Much of the work has gone into migration from the KDE 3 to KDE 4 framework using core technologies such as Solid, Phonon, and Plasma.  Read more about the new release in the <a href="http://amarok.kde.org/en/releases/2.0">release announcement</a> and start Amaroking!


<!--break-->
<img src="http://static.kdenews.org/jr/amarok-20-wee.jpg" width="500" height="299" alt="amarok 2.0 screenshot" style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex" />

<p>The user interface has been redesigned to make context information like lyrics and albums from the same artist more accessible and allow the users to decide which information is most important to them by adding applets to the Context View in the middle. The new Biased Playlists offer a way to let Amarok take care of your playlist in an intelligent way similar to Dynamic Playlists in previous versions. A new service framework allows for a tight integration of online services like Jamendo, Magnatune and Ampache. New services can easily be written as a script. More applets and scripts are being worked on and users are welcome to contribute more to make Amarok suit their needs. The migration from the KDE 3 to KDE 4 framework allows to make use of technologies like Plasma, Phonon and Solid which make Amarok easier to use and maintain and ready for the future of music on your computer and on the internet.</p>


