---
title: "PolishLinux KDE 4.1 Preview (Rev 790000)"
date:    2008-03-31
authors:
  - "jthomas"
slug:    polishlinux-kde-41-preview-rev-790000
comments:
  - subject: "Great stuff"
    date: 2008-03-31
    body: "Congratz to the Polish Linux guys and gals; there visual changelogs always show KDE4 at its best and they did a great job tracking their county's efforts in the recent ooxml vote also."
    author: "borker"
  - subject: "Re: Great stuff"
    date: 2008-03-31
    body: "I would like to second that, the articles are always very high quality.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "webkit in konqueror?"
    date: 2008-03-31
    body: ">> Soon we can all expect 100% compatibility, though, since WebKit, the Konqueror engine has been already fixed in Safari to pass the new Acid test.\n\nThis is going to make KHTML devs happy :)"
    author: "sloppy bobby"
  - subject: "Re: webkit in konqueror?"
    date: 2008-03-31
    body: "For those who are still confused: WebKit is a fork of KHTML and it isn't actually called KHTML anymore. There is occasional merging of changes, nowadays probably more WebKit->KHTML than the other way. There are simply so many people from different projects and companies working on WebKit that it has sorta kinda taken the lead.\n"
    author: "Andreas"
  - subject: "Re: webkit in konqueror?"
    date: 2008-03-31
    body: "I don't think the webkit part will be ready for KDE 4.1. There is no interest from webkit developers to do that ( at least from what I can see in the weekly digests ). So yes we need to relay on our KHTML developer. Keep it up guys."
    author: "Zayed"
  - subject: "Re: webkit in konqueror?"
    date: 2008-03-31
    body: "KHTML is already very stable now and some developers are working in KHTML to add more features and squash bugs. Plus the addition that javascript bytecode interpreter for KJS is work in progress, I think Konqueror + KHTML will rock in KDE 4.1.\n\nAbout webkit in KDE, I think it is still work in progress to create Webkit KPart for Konqueror, so it might be possible that someday we can change the rendering engine on-the-fly. (Although I still prefer a separate Webkit dedicated browser is created from scratch in Qt/KDE)\n\nMerging those changes from Webkit to KHTML is always done by the developers, but merging two branches that already diverged for years is not that easy :)"
    author: "fred"
  - subject: "Re: webkit in konqueror?"
    date: 2008-04-03
    body: "Why would you want a seperate browser?  Konqueror is the best browser in the world no matter rendering engine it uses; theres no need to recreate such a great product just because its backend has changed, especially since konqueror is *designed* to swap out backends."
    author: "Level 1"
  - subject: "Re: webkit in konqueror?"
    date: 2008-03-31
    body: "also EditContentMode is coming for 4.1\nthis means we will have WYSIWYG editors on Konqueror.\ngo KHTML devs, GO."
    author: "Emil Sedgh"
  - subject: "Where's 4.1 in Kubuntu?"
    date: 2008-03-31
    body: "I know I could compile it manually but I don't have time for that and I picked Kubuntu because they promised the best KDE4 dev package and support. All we get though is stable release :/ I'm stuck with kubuntu now so I can't switch to OpenSuse, please Kubuntu dev comply with your promise and bring those KDE 4.1 preview packages already!"
    author: "John"
  - subject: "Re: Where's 4.1 in Kubuntu?"
    date: 2008-03-31
    body: "since opensuse are the strongest developers of KDE out of the distro's, I'd switch to that if you want strong KDE support"
    author: "anon"
  - subject: "Re: Where's 4.1 in Kubuntu?"
    date: 2008-03-31
    body: "I'm almost sure that 4.1 will make it into Hardy Heron, most likely as a backport or something.\n\nKDE 4.02 is available for Gutsy if you want.\n\nhttp://www.kubuntu.org/"
    author: "Peppe Bergqvist"
  - subject: "Re: Where's 4.1 in Kubuntu?"
    date: 2008-04-02
    body: "KDE 4.0.3 is already in the repositories for Hardy Heron.\n\nI am also sure that 4.1 will be backported to Hardy Heron."
    author: "Socceroos"
  - subject: "Re: Where's 4.1 in Kubuntu?"
    date: 2008-04-03
    body: "Don't give your hopes up too high for Kubuntu KDE support.\n\nIt's not called the \"red-headed-stepchild\" distro for nothing.\n\nIf you wish to change it, please vote for KDE support at the ubuntu brainstorm.\nOtherwise you'll be stuck with the 1-man KDE show.*\n.\n\n______\n\n*(last I've heard there is only 1, count it \"one\" full time KDE developer at Canonical. :( )"
    author: "Max"
  - subject: "Re: Where's 4.1 in Kubuntu?"
    date: 2008-04-03
    body: "My bad. I meant \"don't hold your breath\".\n"
    author: "Max"
  - subject: "Amazing progress"
    date: 2008-03-31
    body: "My own experience completely jibes with the PolishLinux review. Especially in the last 2 weeks (compiling from SVN) or so it seems like a lot of things have started coming together making it a much more usable desktop. Not just new features but stability in the existing ones. Can't wait to see what stable 4.1 looks like if this is what it is 3 (4?) months in advance."
    author: "Aaron"
  - subject: "Re: Amazing progress"
    date: 2008-03-31
    body: "+1"
    author: "Emil Sedgh"
  - subject: "Re: Amazing progress"
    date: 2008-03-31
    body: "+1"
    author: "Max"
  - subject: "KDE4 rocks!"
    date: 2008-03-31
    body: "As soon as I get a substitute for KSensors, KAlsaMix and KNetStats/KNemo then I will switch to KDE4 immediately. Unfortunately none of this applications is now  being ported.\n\n"
    author: "Artem S. Tashkinov"
  - subject: "KDE 4 is great!"
    date: 2008-03-31
    body: "Just one problem with KDE 4: lot of application not ported from KDE 3.x, in particular I will delay my migration to KDE 4 until Kontact is not ported. But I hope this may be as soon as possible! ;)"
    author: "Dmitry"
  - subject: "Re: KDE 4 is great!"
    date: 2008-03-31
    body: "You realise you can run KDE3 apps in KDE4 without any problems, including Kontact3?  It's how most of us devs work at the moment."
    author: "Odysseus"
  - subject: "Re: KDE 4 is great!"
    date: 2008-03-31
    body: "Yes, I can run KDE3 applications in KDE4, but I not want doing this on production environment without need. Also I can run GNOME applications in KDE - and so what? ;)\n\nI'm just using my old good KDE 3.5 and waiting for KDE4 Kontact because it should be very cool."
    author: "Dmitry"
  - subject: "Re: KDE 4 is great!"
    date: 2008-03-31
    body: "As of KDE 4.0.67, Kontact at least starts on my laptop and displays most of the tabs correctly.\nI don't have net access right now so I can't test all the features, but it speaks well for inclusion in KDE 4.1.\nJust tested QT4 Scribus and works well. K3B seems to start, not tested it yet.\nI still lack some little things I like (Copy/move right menu entry, access to the calendar with clicking on the clock, one wallpaper per virtual desktop, adjust taskbar width, small stuff like that) but nothing showstopping.\nI think KDE 4.1 will be very good."
    author: "Richard Van Den Boom"
  - subject: "Really slick!"
    date: 2008-03-31
    body: "Great!\n\nWith each week KDE 4.1 looks better. Much kudos to all KDE developers for their excellent work and to Korneliusz for keeping us informed.\n\nDerek's digest is excellent but very technical and it's nice to see from time to time what is all that about!"
    author: "m."
  - subject: "Re: Really slick!"
    date: 2008-03-31
    body: "Derek is called Danny nowadays :)"
    author: "Kevin Krammer"
  - subject: "Re: Really slick!"
    date: 2008-03-31
    body: "Umm, sorry De^WDanny :) Old habits die hard.\n\nHeck, checked Korneliusz's name twice but this one I was sooo sure."
    author: "m."
  - subject: "Bespin!"
    date: 2008-03-31
    body: "I love the look of Bespin!  I've often felt that Oxygen did look a little too much like other proprietary desktop environments (not that I'm hating on it, I still think it looks nice).  But Bespin looks rather slick, with some unique style to it.  I love the look of the progress bar and tabs in particular."
    author: "Riothamus"
  - subject: "Re: Bespin!"
    date: 2008-03-31
    body: "Well, there are some elements of Bespin which I would like to see in Oxygen like the better use of gradients but overall in direct comparison I prefer Oxygen due to Bespin sporting some questionable design decisions (e.g. tab bar which looks like button bat and doesn't make it clear that tabs are exclusive and the less functional scroll bar)."
    author: "Erunno"
  - subject: "Re: Bespin!"
    date: 2008-03-31
    body: "As opposite to your view, I always want a style which is more like \"other proprietary desktop environment\", and Oxygen does not belong to this category. Honestly, I like the glossy looks there as it looks modern and bright. I love the Oxygen style also, I just wish the team will make it closer to nuno's mockups: \n\nhttp://www.nuno-icons.com/images/estilo/oxygen41tests.png\nhttp://www.nuno-icons.com/images/estilo/version 3 tabs and version 2 scrollWIP.png\n\n(I don't know if they had changed the design)"
    author: "fred"
  - subject: "Great visual changelog, thanks Polish.."
    date: 2008-03-31
    body: "Thanks Polish-linux for this visual changelog.\n\nIt makes it much easier to show and recruit newbies to the wonderful world of KDE 4.X.\n\nIt also helps in creating buzz for the KDE cause.\n\nI hope people will post a visual changelog like this for Amarok 2.0 as well. :)\nMaybe even videos."
    author: "Max"
  - subject: "MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "Does someone know how to install Dashboard Widgets? I compiled trunk but the only option i get is \"Plasmoid: Native plasma widget\" - no Superkaramba and no MacOS Dashboard Widgets."
    author: "stormwind"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "Yea I saw that, and I was like wow!!!\n\n\nwe'll definitely need tutorial videos that show off all these features. The apple guys are doing it, I see no reason why we can't have a collection of KDE tutorial videos somewhere."
    author: "Max"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "You know the apple guys have one odd thing called employees, which can be directed to do stuff like this. KDE on the other hand don't. And the small amount of contributors are kind of busy with other stuff like writing code, documentation, translations or doing artwork.\n\nPlaces to put such collections should not be a problem, the only thing missing are contributors doing the job. You could volunteer, just start making them videos and you'll receive instant fame in the KDE comunity."
    author: "Morty"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "You could download/burn a OpenSuSe KDE 4 live CD. I know that the latest livecd has support for MacOS dashboard widgets."
    author: "Jonathan Thomas"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "Thanks - i will try that.\nNevertheless is it odd that the version i compiled from trunk yesterday, doesn't offer this option to me. Apparently i am missing a module or a dependency."
    author: "stormwind"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "Maybe it's in playground. I also use trunk and don't habe dashboard widgets, but I think I need the webapplet from playground. The problem is, that there is a patch that needs to be applied to webkit to get the webapplet compiled. I'm trying to build qt-copy with that patch at the moment."
    author: "Hias"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-01
    body: "Could you post here, if you are successful?"
    author: "stormwind"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-02
    body: "I was partially successful. I can now add dashboard widgets, but when I add them to the desktop I get the following error\n-----\nThis object could not be created for the following reason:\nCould not open the dashboard_com.Mike.widget.ChuckNorrisFacts package required for the ChuckNorrisFacts widget.\n-----"
    author: "Hias"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-02
    body: "Hmmmm, at least a step in the right direction.\nSo you compiled /trunk/playground/base/plasma/applets/webapplet/? Where did you get the patch for qt-copy? \nI would like to try it myself."
    author: "stormwind"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-02
    body: "I compiled the whole playground/base/plasma, except the worldclock, because it fails to build.\nThe patch is in the webapplet directory, it's contents.diff. It adds the contentsSize() function to the webframe class.\nGood luck"
    author: "Hias"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-03
    body: "Didn't work for me. For some reason compiling of kdebase failed. Seems, that i have to hope that qt-work will be updated soon.\n\n"
    author: "stormwind"
  - subject: "[SOLVED] Re: MacOS Dashboard Widgets "
    date: 2008-04-04
    body: "The new version of qt-copy solved all my problems: after compiling the new qt-copy, kdebase and playground/*/plasma i can now add working Dashboard Widgets to Plasma.\nThanks for your help!"
    author: "stormwind"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-02
    body: "Same here.\n\nI can compile the web applet from playground, and am able to install widgets, but I cannot add them to the desktop.\n-----\nCould not open the dashboard_com.whatsinthehouse.widget.wikipedia package required for the Wikipedia widget.\n-----\nRunning qt-4.4.1 snapshot from 20080329 with qt-copy patches applied + today's KDE trunk.\n\nI also tried loading the provided HelloWorld test applet:\nthat one does load, but looks a bit flakey."
    author: "Tom"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-02
    body: "From where did you get the qt snapshot?"
    author: "Hias"
  - subject: "Re: MacOS Dashboard Widgets"
    date: 2008-04-05
    body: "From the trolltech FTP server.\nftp://ftp.trolltech.com/pub/qt/snapshots/\n\nFor Gentoo there are ebuilds available on the forums."
    author: "Tom"
  - subject: "Testers wanted  => Packages wanted !!"
    date: 2008-04-01
    body: "KDE 4.0.67 (snapshot):\nplease can someone build and provide openSuSE 10.3 (64bit) ?\nthanks\n:)"
    author: "opensuse"
  - subject: "Re: Testers wanted  => Packages wanted !!"
    date: 2008-04-06
    body: "see http://en.opensuse.org/KDE4\n\nbasically need to add the KDE4:UNSTABLE repos (don't mix the KDE4:STABLE and KDE4:UNSTABLE repos or you will have problems...)\n\nd."
    author: "dr"
  - subject: "Re: Testers wanted  => Packages wanted !!"
    date: 2008-04-06
    body: "I found I needed to include KDE:/Qt44/   also to update to kde4.068  this weekend.\n\nBTW this last snapshot looks very polished to me.  I can move the taskbar to any side and can shrink it in vert and horiz.  The pager applet can now be put in as many rows as you like, GHNS seems to be working fine for themes and wallpaper.  In fact for the first time I would say it is on a par with kde3.5's desktop shell.  The only thing I miss is a working knetworkmanger, at the moment I am forced to use nm-applet :(   (give it its due though, it does work and it shows up fine in kde4 systray)."
    author: "jed"
  - subject: "Thanks!"
    date: 2008-04-01
    body: "That's a very nice article and KDE 4.1 really starts looking good and usable!\n\nOne question: I tried KDE 4.0.2 (the Fedora Live CD) and one thing that made it unusable for me is that having the panel on the left side, while possible, simply isn't usable (I have a widescreen which is why I like having it there). Anyone knows what plans there are for the panel and whether that will be fixed in KDE 4.1?"
    author: "Joergen Ramskov"
  - subject: "We have made it to Slashdot's main page!! w00t!!"
    date: 2008-04-01
    body: "http://linux.slashdot.org/linux/08/03/31/2231251.shtml\n\nKDE 4 has made it to slashdot's main page!!! FTW!!!!"
    author: "Max"
---
PolishLinux is at it again. 7000 svn revisions later PolishLinux has posted their next <a href="http://polishlinux.org/kde/kde-4-rev-790000-better-stability-and-performance/">"visual changelog" article</a> highlighting changes in KDE 4.1. The article highlights new features in numerous pPlasmoids, new Plasmoids, Plasma in general, Dolphin, Amarok, Kget and KWin compositing customization. From the article: <em>"Now I can be almost sure that when a stable edition of KDE 4.1 is published, I will definitely say good [bye] to my old good KDE 3.5."</em>



<!--break-->
