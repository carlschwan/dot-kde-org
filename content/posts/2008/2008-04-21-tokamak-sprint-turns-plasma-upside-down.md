---
title: "Tokamak Sprint Turns Plasma Upside-Down"
date:    2008-04-21
authors:
  - "skuegler"
slug:    tokamak-sprint-turns-plasma-upside-down
comments:
  - subject: "Thanks"
    date: 2008-04-21
    body: "I'd like to say thanks to everyone for making this a successful sprint, and I hope that everyone's caught up on their sleep now! Incidentally, the movement to the new API is already underway (a big thanks to those who transcribed all the whiteboard photos)."
    author: "Richard Moore"
  - subject: "Nice"
    date: 2008-04-21
    body: "Sounds great! Can't wait for 4.1!"
    author: "bsander"
  - subject: "Nice"
    date: 2008-04-21
    body: "The really cool stuff is starting only now in plasma, not only things like.. well amor, eheheh :)\n\nFuture seems bright and promissing in the plasma front."
    author: "Iuri Fiedoruk"
  - subject: "Still broken?"
    date: 2008-04-21
    body: "As of yesterday SVN, is plasma still \"broken\"? I mean, the panel simply doesn't work, and other plasmoids (New device for example) have serious layout problems.\nPlease, keep us informed (blog posts for example), I want to use KDE 4.1 :)\n\nThanks for the great work in any case!"
    author: "Vide"
  - subject: "Re: Still broken?"
    date: 2008-04-21
    body: "yeah, i had board meetings starting the very next day, then flew home friday, took the weekend (mostly) off to do housework and other stuff that piled up outside of KDE while i was away, and today's my first Real Day back.\n\nso things should start to get back into order over the next 1-2 weeks. \n\nof course, there's already been a good number of fixes committed between the sprint and now which have improved things on a number of fronts.. it's just that now i can also join in =)"
    author: "Aaron Seigo"
  - subject: "Re: Still broken?"
    date: 2008-04-22
    body: "you should get a maid to volunteer cleaning your house in the name of kde development"
    author: "anon"
  - subject: "Re: Still broken?"
    date: 2008-04-22
    body: "I volunteer, as long as I don't have to wear the costume."
    author: "Anon"
  - subject: "Breakage"
    date: 2008-04-21
    body: "It's serious, OMG plasma's totally broken events like this that make me yearn for the kde devs to adopt git. Stuff like this could be easily branched off, hacked on, broken all to heck, have all the pieces put back together and merged back to the mainline without most users of svn having their plasma hosed for a couple of weeks. Supposedly that is what playground is for, but perhaps svn isn't as accommodating of this style of development?"
    author: "Joe"
  - subject: "Re: Breakage"
    date: 2008-04-21
    body: "playground is for new things. branches/ is for, well, branched development.\n\nunfortunately, svn rather sucks in this regard. the WoC stuff was done in a branch, the merge clobbered a number of commits that happened during/around the merge time and it wasn't until after the merge that it was at all easy to test all the components against the changes (it's the components that broke, really; ergo the whole \"porting\" thing)"
    author: "Aaron Seigo"
  - subject: "Re: Breakage"
    date: 2008-04-21
    body: "Some devs use git locally, but merging is still annoying and boring especially because there are many stuff committed in libplasma and kdebase/plasma. Putting the new API in the trunk allows all the community to port faster applets than plasma team can do alone. Svn is for sure not accommodating for this kind of stuff but i'm quite happy by what we had just after the end of plasma sprint. "
    author: "M\u00e9nard Alexis"
  - subject: "Re: Breakage"
    date: 2008-04-22
    body: "Fully agree. :) SVN is easy to use and scales up well for large projects like KDE. But it needs to be better at branches or it'll find itself with the technology trash heap next to CVS."
    author: "Ian Monroe"
  - subject: "local dataengines and browser"
    date: 2008-04-21
    body: "resolution independance and OS / Browser independance , WOW\n\nbut how does a plasma applet in a browser connects to local data engines ?"
    author: "nice"
  - subject: "Re: local dataengines and browser"
    date: 2008-04-21
    body: "for dataengine using plasmoids, that will require access to the scripting API. these will therefore rely on support in the web rendering kit used (khtml, webkit, gecko, ie, opera, etc), probably via a plugin not unlike, say, flash. (except open .. )\n\nbut for simpler widgets that don't use data engines, we can replicate the widgetry in plain embedded javascript."
    author: "Aaron Seigo"
  - subject: "Re: local dataengines and browser"
    date: 2008-04-21
    body: "are there dataengine exposed via webservice ?\n\nwith a wsdl description ? that would be great, so javascript could query via xmlHTTPrequest...\n\nperhaps add a interface for that to plasma ?"
    author: "nice"
  - subject: "Re: local dataengines and browser"
    date: 2008-04-21
    body: "first off, this has huge security implications. the security infrastructure for non-trusted/trustable widgets is fledgling right now (though design has been discussed) ... \n\nwith that proviso, assuming the JOLIE integration works out, that would be the most likely route for us to go."
    author: "Aaron Seigo"
  - subject: "Re: local dataengines and browser"
    date: 2008-04-22
    body: "That depends on whether you really want the engines to reside on the client. If not, then one could make a pure JS implementation of the API based on AJAX to get the data from the server."
    author: "Riddle"
  - subject: "Re: local dataengines and browser"
    date: 2008-04-22
    body: "if the data to display *comes* from the client, it should reside @ the client\n\nif the data can be produced somehow in the browser ? then it would make sense to put the dataengine in the browser too.\n\nbut i think about 99% percent of useful data comes from different clients, not outside a browser\n\n"
    author: "asdf"
  - subject: "Thanks."
    date: 2008-04-21
    body: "Thanks to all plasma team wich is really impressive. Thanks to kde e.V and Riccardo's family for all.\n\nPlasma is in a good way with a very good future. Svn is now in a correct way too, there are still little problems but it's now near before.\n"
    author: "M\u00e9nard Alexis"
  - subject: "Sweet..."
    date: 2008-04-21
    body: "Not only did the plasma guys get some good work done, I actually helped. My small contribution to KDE e.V. has been put to use. I'm so happy.\nI think I'll have to donate some more. ;-)"
    author: "Oscar"
  - subject: "Caption, please...? "
    date: 2008-04-21
    body: "Ahem... would anybody mind putting some captions to the photos in the gallery so that the Dot/planetkde readers could map the faces to nicknames? I'm getting somewhat curious about who is who :) "
    author: "knrd"
  - subject: "Re: Caption, please...? "
    date: 2008-04-22
    body: "Yeah, the nicknames could be given relative to Aaron's position.\nHis mad grin is actually very unique :)"
    author: "fhd"
  - subject: "Great work"
    date: 2008-04-21
    body: "I really appreciate the great work, thanks all. At the same time I have to say it was pretty annoying that plasma was that broken in trunk ;) I think doing the API transition at least for the core parts in a branch would have been great."
    author: "Louai Al-Khanji"
  - subject: "Re: Great work"
    date: 2008-04-22
    body: "well, a branch was used for the WoC introduction (which is what caused the breakages). the branch was getting very brittle however (yay, svn!) as trunk and it continued to be developed simultaneously with lots of activity in each. checking out a second branch for all the plasma code was also a bit too much to ask, and we had limited time at hand (4 days in total) to get things working so expediency was a primary issue.\n\nyes, it's pretty easy to sit on the outside and make obversations as to what would've been better, but it often gets a bit more \"interesting\" when you're actually faced with the reality of the everything that must be considered for these decisions.\n\nit's also interesting how many people assume trunk/ will always be 100% functioning, though: it says a quite a lot about the high level of stability usually seen in the main development branch."
    author: "Aaron Seigo"
  - subject: "Re: Great work"
    date: 2008-04-22
    body: "Well, I do try to help out as I can, so I am not \"sitting on the outside\" in that sense. I have an svn account and occasionally submit fixes here and there. The thing is, to do that effectively does require a functional desktop - in this case the plasma breakage came as a bit of a surprise as I missed the announcement on kcd. I elected to fall back to KDE3 kicker.\n\nI do not assume that trunk always functions 100%, that's not reasonable. I do assume that the major parts compile, as per the svn guidelines, which for a (short) while wasn't true.\n\nPlease don't take this the wrong way, I'm just trying to give some constructive feedback."
    author: "Louai Al-Khanji"
  - subject: "Re: Great work"
    date: 2008-04-22
    body: "Trunk is for development by definition, so it's normal to have breakages. I'm sorry but when the merge was done kdebase was ok for compilation so it's correct with svn guidelines. Plasma sprint was announced some weeks ago, with a WoC planned. Applets in PLAYground and EXTRAgear was broken but it's additionnals applets. There is a lot of plasma applets all around the code base so it's normal that applets not compile."
    author: "M\u00e9nard Alexis"
  - subject: "Re: Great work"
    date: 2008-04-22
    body: "I think people forget that this is all still in the making, still \"beta\", and that things will get broken, fixed, re-broken, then fixed again as it goes on.  If you're not interested in having parts of KDE broken, then you.\n\nbtw, I hope dot will link to the new screenshots of opensuse 11 beta 1.  Some good work there with KDE in the screenshots ;)"
    author: "R. J."
  - subject: "Re: Great work"
    date: 2008-04-22
    body: "I think people forget that this is all still in the making, still \"beta\", and that things will get broken, fixed, re-broken, then fixed again as it goes on.  If you're not interested in having parts of KDE broken, then you shouldn't be running kde 4 until 4.1.  Although I am finding it all very interesting, and very stable.\n\nbtw, I hope dot will link to the new screenshots of opensuse 11 beta 1.  Some good work there with KDE in the screenshots ;)"
    author: "R. J."
  - subject: "Layman's Explanation Of Plasma"
    date: 2008-04-22
    body: "I love KDE and have followed KDE4 development with great anticipation.  I have a pretty good understanding of all the other core technologies of KDE4, but plasma eludes me a bit.  I'm not a programmer at all, so that could be a big part of it.  So far the major plasma news seems to revolve around plasmoids.  However, since the stated goal of plasma is to revolutionize the desktop and how we view it, I know it has to be about a whole lot more than plasmoids.  So my question is, can someone give a general description (or example) of how plasma will fundamentally change the desktop experience?  I realize what I'm asking may be  difficult to do since I admittedly am not a programmer.  If so, I have no problem waiting until the plasma vision becomes more of a reality.  I'm sure it will be very clear then.  :)"
    author: "cirehawk"
  - subject: "Re: Layman's Explanation Of Plasma"
    date: 2008-04-22
    body: "I found the Wikipedia article on it to be quite useful: http://en.wikipedia.org/wiki/Plasma_(KDE)"
    author: "bsander"
  - subject: "Re: Layman's Explanation Of Plasma"
    date: 2008-04-22
    body: "There's an interesting piece here by a fellow Anon:\n\nhttp://dot.kde.org/1197522021/1197543708/1197546904/\n\nIt sounds like he or she is not a developer, though, but it's still worth a read.  I'm not sure what the actual Plasma Devs think of it, though."
    author: "Anon"
  - subject: "Re: Layman's Explanation Of Plasma"
    date: 2008-04-22
    body: "Basically the idea is to unify the panel, kicker, and desktop into one seemless entity that widgets (aka \"plasmoids\") can be moved around on and change configuration as necessary depending on whether you put them into a panel, on the desktop.  It's also trying to integrate ideas from superkaramba, Mac OS X's Dashboard, etc.\n\nNow, that doesn't sound so great.  And, in fact, it's not really.  It's a little nicer, and more polished, I suppose.  The thing I like best about all this is the scalability and standardisation of ways to add desktop widgets/panel applets to KDE.\n\nThat's especially true if plasma is going to fully support Kross (development in any language) anytime soon, but it sounds like it only supports things like Javascript, C, and python directly right now.\n\n\nAnyway... the claim to revolutionise your desktop comes from the fact that these things are only *currently* setup to emulate a typical desktop with panels, etc.  They're more flexible than that.  The plasma panel is actually a specific kind of \"containment\".  Containments are generic things which you can put plasmoids into.\n\nThen there's the concept of \"zooming\" your desktop.  This is still very up-in-the-air, as far as I can tell, in terms of how it'll actually work, what the benefits will be, etc.  Aaron seems to have a firm idea of what he wants it to do, but I haven't yet seen a good explanation.  This zooming will let you move out of normal desktop view, and see your applications or data in new ways, I guess.\n\n\nI don't think any one thing in KDE 4 is hugely spectacular.  Together though, they make a great platform for future apps.  Things like arthur & svg; webkit; kross (only useful to me if it allows everything for rapid development of plugins AND **apps using plugins** that the C++ api allows); akonadi; phonon; kitchensync; and, last but certainly not least, Soprano/Nepomuk"
    author: "Lee"
  - subject: "Re: Layman's Explanation Of Plasma"
    date: 2008-04-22
    body: "OOPs, didn't mean to exclude plasma itself from that list of great tech :)"
    author: "Lee"
  - subject: "Re: Layman's Explanation Of Plasma"
    date: 2008-04-23
    body: "Thanks for all the input.  I'll check out the links you all gave."
    author: "cirehawk"
  - subject: "sounds good!"
    date: 2008-04-22
    body: "I await eagerly Kde 4.1  ;-))"
    author: "TomiF"
---
Tokamak, the first International meeting of <a href="http://plasma.kde.org">Plasma</a> was held in Milano in northern Italy over the last weekend. 14 people joined the fun and spent some days hacking on the KDE 4 desktop shell. For the most part, it was like meeting friends, only that some had never met each other in person before. The meeting was filled with small sessions, such as discussing target users for Plasma to optimise the Plasma interface for. Topics were target users, underlying technology, scripting, integration with other parts, webservice integration, visual presentation, porting of Plasma to new technology in Qt, Italian profanity and how everybody loves pizza.
















<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 499px;">
<a href="http://plasma.kde.org/media/tokamak/IMG_0800_MED.JPG">
    <img src="http://static.kdenews.org/jr/plasma-tokamak-meeting.jpg" width="499" height="278" />
</a><br />
Reaching out to new levels.  More <a href="http://vizzzion.org/?id=gallery&gcat=Tokamak">photos in the gallery</a>.
</div>

<h2>Widgets-on-Canvas</h2>

<p>Part of the meeting was dedicated to large changes under the hood in Plasma. In Qt 4.3, which KDE 4.0 is based on, it was not possible to use QWidgets on the QGraphicsView canvas. Trolltech has addressed this problem in Qt 4.4, so we're now able to use QWidgets in Plasmoids. This deprecated quite a lot of code in libplasma, so the Plasma developers were able to remove that, reducing maintainance burden, size of the codebase, memory footprint, but at the same time Plasma becomes more powerful and easier to use for those that already know Qt. Moving over to QWigets involved tearing large parts of Plasma into pieces and putting them back together. Alexis spearheaded this effort and had other people jump in from both, Tokamak hacking place, but also via IRC directly. The worst breakage happened during Sunday, by Monday night, quite a lot of Plasmoids were functional again, some with reduced featureset, others with issues fixed. In any case, the Widgets-on-Canvas port will be well worth the effort and pain of porting and will allow for much more powerful Plasmoids.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 314px;">
<img src="http://static.kdenews.org/jr/plasma-tokamak-bindings-demo.jpg" width="314" height="283" /><br />A Plasmoid scripted with JavaScript
</div>

<h2>Scripting</h2>

<p>Richard Moore led the scripting effort that should result in a dual API that provides both easy and powerful ways to script Plasma. The developer base
divides into two groups, people that prefer a very simple API to quickly do easy things, and people that want full access to the Plasma API and being able to use this powerful technology. This way, writing Plasmoids should be easy for everyone. Details about this can be found on <a href="http://techbase.kde.org/index.php?title=Projects/Plasma/NewWidgetDesign">Techbase</a>. Richard has also shown the power of the Plasma scripting API by writing a nifty demo plasmoid. Fredrik Höglund provided <a href="http://ktown.kde.org/~fredrik/canvas/animatedrects.html">a version of Richard's JavaScript demo that runs in a webbrowser</a> (works with KHTML trunk, recent Firefox and Safari). In the future, we will be able to run more Plasmoids in webbrowsers, blurring the traditional lines between web and desktop.

<h2>API review</h2>

<p>In the KDE 4.2 timeframe, the Plasma developers plan to have libplasma moved into kdelibs. Kevin and Richard started going through the whole visible Plasma API shared thoughts about how to improve it, noted them on the whiteboard and provided detailed suggestions how to improve the API. <a href="http://techbase.kde.org/index.php?title=Projects/Plasma/Tokamak1">Transscripts</a> of the whiteboard are up on Techbase. Porting the API to this will start soon.</p>

<h2>Artwork</h2>

<p>Nuno Pinheiro presented his vision about Plasma. He made other developers aware of how artwork works, and how that relates to writing Plasmoids and theming capabilities. Nuno provided entropy to the developers by broadening the scope of the meeting and providing additional input from a non-developer's point of view.</p>

<h2>Containments and KRunner</h2>

<p>Chani has done some work on Containment switching. When zooming out, the Plasma toolbox now shows an "Add Activity" button that can be used to add a containment to the desktop. Those containments are 'customized' desktops that carry their own set of Plasmoids. This makes it possible to have, for example, a "fun" containment with twitter applet, a game plasmoids, comic and the like for freetime usage, and use another containment with RSS Feeds from commits and a folderview Plasmoid for coding work. This also lifts a bit of the curtain what this "Zoom out" button in the toolbox actually is about. How this feature is related to KWin's virtual desktop remains to be sorted, however.</p>

<p>Riccardo and Davide have been working on the new user interface for KRunner. While there is nothing visible yet (and the authors prefer to keep mockups for it private for now), there is not much to see yet. Some polishing of the artwork and text input has already been done, however.</p>

<h2>New Technologies</h2>

<p>Andreas Aardal Hanssen dropped by for one-and-a-half days to tell us about plans in Qt for the next months, and solve some problems with us we have with QGraphicsView. Performance improvements in Plasmoids are the direct result of this session, but it was also interesting to see how well Trolltech listens to the Plasma community by planning for implementation of features Plasma will greatly benefit from.</p>

<p>Another visit to the Tokamak people was paid by two Italian researchers who are currently working on a service oriented architecture that could be used in Plasma to decouple services, such as DataEngines from local machines and make them available on the network. Some Plasma developers agreed to work with the <a href="http://jolie.sourceforge.net/">JOLIE</a> researchers on a paper that will be presented to the research community later this year.</p>

<h2>Target User Profile</h2>

<p>After Celeste <a href="http://weblog.obso1337.org/2008/the-1-problem-in-oss-usability-and-what-im-going-to-do-about-it/">started</a>  the target user research project</a> only a couple of weeks ago, the <a href="http://weblog.obso1337.org/2008/preliminary-results-of-modified-delphi-interviews-to-discover-plasma-user-types/">first results</a> were presented to the Plasma developer to <a href="http://aseigo.blogspot.com/2008/04/who-are-our-users.html">seed them</a> into the community and reflect those changes in Plasma's user interfaces.</p>

<h2>The First Meeting...</h2>

<p>And most definitely not the last. The Plasma community, being quite young is already flourishing. The humongous amount of work done proves that this kind of sprint is an excellent means to jump ahead on technology. It is surely not the last Plasma meeting, and yours truly is already looking forward to "Tokamak II" (although there are no plans for it yet, and some weeks without pizza will be appreciated). Also, Tokamak would not have been possible without the support of the KDE e.V. and our friendly host Riccardo Iaconelli and his family for their great display of support for the Free Software desktop.  </p>















