---
title: "Qt Centre Programming Contest 2008"
date:    2008-04-08
authors:
  - "rjohnson"
slug:    qt-centre-programming-contest-2008
comments:
  - subject: "On a related note.."
    date: 2008-04-08
    body: "..what happened to the \"Trolltech Open Source Development Award\"? I remember them announcing the finalists a while ago, but haven't heard from it again.\n"
    author: "Mark Kretschmann"
  - subject: "Contest"
    date: 2008-04-08
    body: "It's great to do competitions, but I really don't get why this is a competition.\n\nIn the last competition, the winners were people that had already spent a year or so on their app.  What's the point in having the competition open for four months when the winner is pretty much decided before hand?\n\nEverybody knew that marble was going to win, before the competition even started.  Why not just skip the time period thing, and just go directly to awarding prizes to the best Qt program that currently exists?\n"
    author: "JohnFlux"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: "Hi John,\n\nNot referring to your doubts and not defending people who spent their time evaluating all the entries let's skip to the good part.\n\nI have a separate little contest for you (and actually everybody else who wants to play with us). Nominate winners (including 2nd and 3rd places) in each category here within this week before nominees start being announced and if you manage to get 50% right (including the order), you'll get a contest gadget (it's not yet decided what it is going to be, probably a t-shirt) and a nice book. If you fail, you will not accuse people regarding this disagreement ever again.\n\nIf someone else wins I will throw in a t-shirt from last year's contest as well If I remember correctly John already received one after the contest, but if he wants another one, that can be achieved too. If there are more people reaching the 50% limit, the one who makes the highest result gets the book and a gadget and the rest of \"TOP 5\" receive the gadget only. Is that a fair deal?\n\nOf course everybody is invited to participate in the \"big\" (aka programming) contest. A brief list of prizes should be announced soon, so get something to eat (nuts are good for your brain activities) and start thinking about neat ideas for entries! Especially participation in Plasmoid and Newcomer categories is strongly encouraged!\n\nWith kind regards,\n  W."
    author: "Witold Wysota"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: "Awesome - I'll give it a try *grin*.  I'll wait to see what the submissions are\n\nBtw, I didn't get a t-shirt from the last contest.  I didn't get a mention or even an email or anything.  Poor me, hehe :)"
    author: "John Tapsell"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: "Actually you have to guess the winners before you know the submissions... That's the whole point of knowing who wins before the contest has even started, isn't it?\n\nAs for the shirt, a medium sized t-shirt was sent to the address you provided. I sent it myself half a year ago. It didn't bounce back, so either it was lost or you missed it somehow...\n\nCheers!"
    author: "Witold Wysota"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: "oh, the irony!\nrofl"
    author: "lol"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: " and they want to remove the dot comments, i love this community"
    author: "mimoune djouallah"
  - subject: "Re: Contest"
    date: 2008-04-09
    body: "We don't want to remove the dot comments. However there have been people recently who have abused the comment area for their own agenda. Of course we value feedback and consider freedom of speech essential. We need to find a better way to deal with abuse while preserving the ability to provide constructive feedback and freedom.  "
    author: "Torsten Rahn"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: "Marble the application won the application category, but Marble the widget did not win in the widget category (although it did come close)."
    author: "David Johnson"
  - subject: "Re: Contest"
    date: 2008-04-08
    body: "Then you knew better than I did ;-) \nBetween the announcement of the contest and the day we delivered the application to the jury, Marble had significantly improved (in terms of features as well as code polishing). The contest did encourage us to put even more work into the development of Marble and has been a significant milestone for us. Also don't forget that there were even new pretty cool apps (like the one Jos came up with)."
    author: "Torsten Rahn"
  - subject: "Plasma docu?"
    date: 2008-04-08
    body: "I wonder if there is some documentation available helping newbies writing plasmoids. There is nothing on the plasma web site (yet). "
    author: "Sebastian"
  - subject: "Re: Plasma docu?"
    date: 2008-04-08
    body: "I have seen a tutorial on it somewhere although it didn't seem complete (I think it was pre-4.0). You can probably bug Aaron about it, his blog entry regarding the contest suggests he wants to fill in some of the docs. By the way, he is going to be one of the judges in Plasmoid category.\n\nThere is a discussion on our forum (http://www.qtcentre.org/forum/f-qtcentre-programming-contest-2008-28/t-kde-40x-requirement-12922.html (sorry, I don't know how to provide a proper link) - feel free to join it) regarding differences in Plasma between KDE 4.0 and 4.1, so even a 4.0.x based tutorial might be inadequate if you want to implement something that is going to last longer than few months."
    author: "Witold Wysota"
  - subject: "Re: Plasma docu?"
    date: 2008-04-09
    body: "The plasma sprint starts on Thursday in Milan. We'll be looking into that kind of thing while we have everyone locked in the same room.\n"
    author: "Richard Moore"
  - subject: "Re: Plasma docu?"
    date: 2008-04-09
    body: "Don't let them out until they come up with \"Plasmoids for Dummies\".\nI also want a Lego type Plasmoid IDE called \"Bostick\"."
    author: "reihal"
  - subject: "Re: Plasma docu?"
    date: 2008-04-08
    body: "Have you checked out the KDE techbase?\nhttp://techbase.kde.org/index.php?title=Projects/Plasma"
    author: "Morty"
  - subject: "Re: Plasma docu?"
    date: 2008-04-09
    body: "Ah, at least a beginning to start from! Thanks a lot"
    author: "Sebastian"
  - subject: "Re: Plasma docu?"
    date: 2008-04-09
    body: "KAppTemplate which is a program that generates templates so people can easily start developing integrated yesterday a plasma applet template. This will allow you to start with an already made base app which compiles and the README gives you basic indications to get you started.\nThis KAppTemplate version is currently in /playground/devtools and will hopefully be moved to kdereview and kdesdk for KDE 4.1.\n\nAnne-Marie\n"
    author: "annma"
  - subject: "Re: Plasma docu?"
    date: 2008-04-09
    body: "I will look into that one. Thanks"
    author: "Sebastian"
---
<a href="http://www.qtcentre.org">Qt Centre</a> is announcing its second edition of their <a href="http://contest.qtcentre.org">Qt Programming Contest</a>. This contest is focused around the Qt and Qtopia communities and starts today and continues until September 30th. This year's categories include collaboration, education, project management, automation, demo, plasmoid, and newcomer. If you are interested in reading more about this contest then please review the <a href="http://www.qtcentre.org/contest-">contest page</a> and the <a href="http://contest.qtcentre.org/rules">contest rules</a>.


<!--break-->
