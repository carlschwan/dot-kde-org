---
title: "KDE Commit-Digest for 21st September 2008"
date:    2008-10-01
authors:
  - "dallen"
slug:    kde-commit-digest-21st-september-2008
comments:
  - subject: "*Finally*..."
    date: 2008-10-01
    body: ":-P\n\nI can't be the only one obsessively checking the dot for digests..."
    author: "Tim"
  - subject: "Re: *Finally*..."
    date: 2008-10-01
    body: "RSS, dude :)\n\nAnd thankyou, Mr Allen :)"
    author: "Anon"
  - subject: "Re: *Finally*..."
    date: 2008-10-01
    body: "yeah thanks Mr Allen, you rock"
    author: "mimoune djouallah"
  - subject: "Re: *Finally*..."
    date: 2008-10-01
    body: "Surely you're not alone!"
    author: "Danilo Luvizotto"
  - subject: "Re: *Finally*..."
    date: 2008-10-02
    body: "No, you are not ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: *Finally*..."
    date: 2008-10-06
    body: "I guess Denny liked the last rush to catch up with the Digests so much, that he is now deliberately falling behind again only to do another catch-up spree later on? :-)\n\n"
    author: "Andre"
  - subject: "now it is time for some eye candy useless stuff "
    date: 2008-10-01
    body: "first i must say plasma is rock solid for my daily use, it has all what i need an even extra bonus, i know it is not crucial, but i really miss is  the ability in taskbar applet to show icons ala Mac os, i have used this in E17 and it is really sweet. \n\nthanks for work, and thanks in advance for animated wallpapers.\n\n "
    author: "mimoune djouallah"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-01
    body: "Combine that with the ability to make a panel background completely transparent, and you could put a dock at the bottom of the screen (obviously, the growing icons on over would have to be implemented, but that's not a sticking point for me)."
    author: "Leonardo Aidos"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "http://xqde.xiaprojects.com/\n\nThat looked extremely promising, but it appears development stopped.  Perhaps someone could pick it up."
    author: "T. J. Brumfield"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-01
    body: "First a big thanks to a dedicated and very indistrous Danny.\n\nI second your request for some more OSX like eye-candies. it's not necessary for life but Plasma is now rock solid so I think it's time to get that taskbar really sexy ;)\nI also appreciate and thank the developers for their excellent work."
    author: "Bobby"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-02
    body: "there's something in playground/base/plasma/applets/ called \"peachydock\" ... the fisheye effect is a little bit off in it still, but it's rather smooth. would need a bunch more work to get it \"there\" .. and it could probably be a containment proper .. \n\ni've got a lot more interesting things to be working on in plasma that ripping off Apple, so i personally have no interest in working on it, but that shouldn't stop other people.\n\n"
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "I love plasma and I think it's been a step in the right direction.\n\nI do find that, over time, plasma memory usage ends up being fairly high, 100-120MB, and also it ends up sucking up quiet a lot of cpu power (a constant 10-15% on a 1.7GHz pentium m). Killing plasma and restarting plasma again solves it for a few hours. Only plasma stuff running are the desktop with two folderviews that are static, and then the panel with the taskbar, clock and systray."
    author: "NabLa"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "I want and AWN / MAC OS X dock too. Couldn't get peachydock running so far.\nI think Ivan CuKic will be working on that kind of stuff.\n\n\nI really find the 'windoze' type of bar rather 'deprecated' as well as menu bars (see Google chrome for that xD ); HIGs should change a bit...\n\nMy main concern would be unification area for:\n-commonly used applications\n-running applications\n-system tray\n\nBecause it feels really redundant and silly to have something like pidgin or quassel / konversation in system tray as a small icon, then press it and get a bigger icon+text somewhere more to the right each time you need to check them out for a bit; it's just not as intuitive imho :P"
    author: "Dread Knight"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "If you check out the comments from Aaron Seigo's blog, most systray applets will be replaced with plasmoids, which you could conceivably stick in a dock, and they would scale nicely.\n\nhttp://aseigo.blogspot.com/2008/08/ktorrent-plasma-good-times.html\n\nHe talks about it more elsewhere, but this is just where I remember."
    author: "Leonardo Aidos"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "Please, no change just for the sake of change. I've seen lots of people get confused (myself included!) by an application without a menu bar. See MS-Office or IE7 as an example (I've switched people to the OSS equivalent simply because they are _more_ familiar-looking).\n\n<rant>The whole reason for tool bars is to allow one to quickly access commonly used functionality in the menus. KDE reflects this by unifying the menu bar and tool bar actions and allowing any menu bar action to be placed in the tool bar.\n\nAs a result, the mentality the \"if in doubt, it's in the menus\" has (rightfully) been created in the minds of the average users. Since the tool bar is, as I already mentioned, simply an extension on the menu bar, removing the menu bar makes the various actions more difficult to find. It is generally considered bad usability to make actions more difficult to find, while simply including a menu bar does not hinder usability in any real way (the single line of text that a menu bar is does not take up much space and makes all actions readily accessible in a well-known and intuitive way).</rant>\n"
    author: "Michael \"NO!\" Howell"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-03
    body: "\"Please, no change just for the sake of change. I've seen lots of people get confused (myself included!) by an application without a menu bar. See MS-Office or IE7 as an example (I've switched people to the OSS equivalent simply because they are _more_ familiar-looking).\"\n\nIt's not about radical changes to the present taskbar. It's more about creating something flashy for those who find the Windows-like taskbars too boring and would like to have an alternative - something more OSX-like.\nThe present taskbar is quite functional but the wow-effect and the eye-candies are not there like somebody pointed out. \nCan you believe that I once wanted to buy a MAC just because the Dock impressed me that much? The reason why I didn't was because of the price."
    author: "Bobby"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-03
    body: "I wasn't talking about the OS X-style dock. I was talking about the menu bar as shown in applications (think File, Edit, View...). That is why I gave examples of programs that do not have menu bars.\n"
    author: "Michael \"Context\" Howell"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-03
    body: "OK, sorry for misunderstanding."
    author: "Bobby"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "Try this:\n\nhttp://kde-look.org/content/show.php/Sigma+Dock+%28Alpha%29?content=89992\nhttp://kde-look.org/content/show.php/PeachyDock?content=78494"
    author: "blueget"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "I would rather have KDE3 parity first."
    author: "testerus"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "What's missing?"
    author: "Anon"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "task grouping in task bar! multiline task bar!\n\nhttps://bugs.kde.org/show_bug.cgi?id=152700\n\nhow can you do any serious work if you can only read the first two letters of the name of running programs?\n\n"
    author: "John"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-02
    body: "since we have that in svn, again, what's missing?\n\nthe \"kde3 parity\" thing is getting boring. we've had features kde3 never had, and we have re-implemented the most useful and important features in order ... \n\nif you really want to belly ache about somethin, we still haven't implemented per-virtual-desktop wallpapers.  =P"
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-02
    body: "\" if you really want to belly ache about somethin, we still haven't implemented per-virtual-desktop wallpapers. =P \" eh i thought activitybar can switch between activities which of course can have different wallpapers and icons plasmoid etc....\n,\n\ni hope mr seigo you don't have strong opinion against taskbar with icons, even gnome 3.0 will have an optional one,   http://www.bomahy.nl/hylke/wip/gnome-art-roadmap-draft.pdf.\n\nPlasma is damn awesome, but imho it is not perfect in the wow factor department. \n\nFriendly yours. "
    author: "mimoune djouallah"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "that pdf is a work-in-progress and hardly concrete.\n\nas for \"strong opinions against taskbar with icons\" i really couldn't care. i don't particularly feel a need for it, think it's yet another one of those cute fads that strikes the community every so often, but whatever. if someone wants to code it, go for it.\n\ni'm not exactly required to write all the code myself."
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-05
    body: "What do you mean by \"taskbar with icons\"?"
    author: "Kevin Kofler"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-02
    body: "There is still something missing, something that you have promised us for many months but I have a lot of patience and wouldn't dare to put you nice developers under preasure. \nHere is what it is:\nI would like to be able to configure the clock so that I can see the American, Jamaican, Chinese, Ethiopian and of course the German time when I hover over it with the mouse, just like it is in KDE 3.5x. You told me back then that you also needed this feature, that's why it was implemented in the first place ;)"
    author: "Bobby"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "it's already in svn."
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "Thanks, then there is really nothing missing :)"
    author: "Bobby"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-04
    body: "I just updated my openSuse package and tried it - wow! I have been missing that feature for so long and now it's there! You did a great job Aaron. It looks much more beautiful on KDE 4 and it's much easier to configure, so easy that one doesn't get it at first ;)"
    author: "Bobby"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "Aaron, sometimes your arrogance is just TOO much.\n\nSVN is nice and good, but only when you release the code into packages does it really \"count\".\n\nSo, saying \"Yeah, KDE 3 parity, in SVN, what's yer bitching about?\" is a bit...doucheish?"
    author: "Joe"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "let's recap:\n\nsomeone asked for more eye candy.\nsomeone said \"kde 3 parity first!\"\nsomeone said \"why, what's missing?\"\nsomeone said \"grouping and multiple lines in the taskbar!\"\n\nand that's when i stepped in because that work is actually already done in svn (well, in playground, working its way through to base; grouping in the lib is already in base).\n\nthe conversation was not about released code, but where we developers spend our efforts today. and as i said, the \"kde 3 parity!\" thing is getting tired.\n\nyou can call it arrogance if you wish, but i'm ready to stop hearing complaints for things we've put the work into already.\n\nremember, this whole commit digest thing is about *work in svn*."
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "Seems the dot is still a troll magnet. Your patience with them is quite amazing. :-)"
    author: "Svempa"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "How about working on some debugging? I have over 30 bugs reported, and asiego has only touched one of my twelve Plasma/Kwin bugs. Performance perhaps? Why can't I have two picture applets on my \"Desktop\" without killing performance? Why is the default setting at one second between pictures? Don't you think that's a bit much?"
    author: "Joe"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "i rarely touch kwin. maybe you're thinking of lubos? nor am i exactly required to deal with your personal bug entries.\n\nin any case, there is lots of work on debugging and stabilization. counting your own personal bugs isn't exactly a measure of global progress, now is it?\n\nadditionally, we tend to do more debugging and stabilzation work towards the end of the release cycle (last 30% or so usually) not at the beginning.\n\n> Why can't I have two picture applets on my \"Desktop\" without killing performance? \n\ndunno; it doesn't affect performance here. perhaps you could be more explicit an include information such as: video driver, settings on those two applets (collections? size of widgets?), etc.. \n\n> Why is the default setting at one second between pictures?\n\nit's 10 seconds actually\n\n> Don't you think that's a bit much?\n\nyes. maybe the author and maintainer of that widget should change it. perhaps i will, but .. yeah, complaining about a default setting of 10s in one widget is getting pretty nitpicky. which is great .. if these are the issues that are annoying you now, things must be pretty decent.\n\nan we're, what, less than 10 months past the 4.0.0 release?"
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-09
    body: "How about gestures?  Last I checked there's no khotkeys equivalent yet."
    author: "MamiyaOtaru"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-09
    body: "This thread is about Plasma.  Gestures in KDE4 would be great, though :)"
    author: "Anon"
  - subject: "Re: now it is time for some eye candy useless stuff "
    date: 2008-10-02
    body: "The function I miss the most: KDE3 taskbar had a button to display tasks of all desktops (\"window list button\")."
    author: "testerus"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "i wonder if that would make sense as part of the taskbar as it used to be ... or as its own plasmoid? it would be easy to code as its own thing and keep the taskbar simpler; not to mention let you put it wherever you wanted ... since you used that feature, what do you think?"
    author: "Aaron Seigo"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "More flexibility is fine with me. As long as I do not end up with a huge button on my vertical panel. In KDE3 this button takes up about 138x22 pixels.\nAnother downside of an extra plasmoid might be that the feature is no longer exposed in the configuration dialog."
    author: "testerus"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "\"As long as I do not end up with a huge button on my vertical panel. In KDE3 this button takes up about 138x22 pixels.\"\n\nGoing forwards, Plasmoids will be able to be placed in the system tray, and I think (please correct me if I'm wrong here, Aaron) that there is a Grid containment that can be used to arrange and constrain the sizes of Plasmoids on your panel ( can Containments be \"nested\" in this way ... ?).  So eventually, I see no reason why one would have to worry about Plasmoids taking up loads of space if you don't want them to :)\n\n\"Another downside of an extra plasmoid might be that the feature is no longer exposed in the configuration dialog.\"\n\nPlasmoids can certainly have their own config dialogs, so this shouldn't be a problem.  Whether it can be configured centrally from, say, System Settings is another matter that I'm not sure about.  Aaron? :)"
    author: "Anon"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-04
    body: "Personally I would prefer it to be a own plasmoid for the following reasons:\n+ You can move it wherever you want, as you said.\n+ I also like the idea of keeping the taskbar simple. Make it possible to change the icon of the button - without cluttering the taskbar settings dialog. ;)\n+ You can use the plasmoid without a taskbar. Wicked!\n\nOn the downside,\n- It's harder to discover. Or is it really?"
    author: "Hans"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "That can be really replaced with current \"expose like\" kwin features. They need better keyboard navigation but fundamentals are there."
    author: "mikmach"
  - subject: "Re: now it is time for some eye candy useless stuf"
    date: 2008-10-03
    body: "Personally I can not see how a graphical representation can deliver the same information as a list of window titles."
    author: "testerus"
  - subject: "Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "I always like to read all the new developments around KDE4.\n\nHowever, while Plasma et al. are finally taking shape and are really improving the desktop experience, one thing is drastically detoriorating in KDE4: The \"web browsing experience\" :(. I always read about fixes, commits, fast JS-engines and so on on the dot and in blogs - however, the number of pages in real life I can render flawlessly with Konqueror is getting smaller day by day.\n\nI don't mean to flame here, I appreciate all the work put into Konqueror/KHTML, but as I currently need to start Firefox for every second page, I have now finally switched completely to Firefox. All my friends - all KDE supporters - have made this switch long before.\n\nA few examples:\n\n- Smaller, non-critical but annoying rendering errors everywhere (many text input fields too small to read the entered text, some pictures are not displayed, frames distorted)\n- Critical rendering problems e.g. at maps.google.de - in fact, online map services nowadays almost never render flawlessly. www.ebay.de unusable since KDE 3.x days (no picture upload possible, \"my ebay\" dropdown menus unusable.\n- The rendering of almost any page with plugins (flash, nspluginwrapper, xine-plugin), which work without any problem in Firefox, is a pure game of chance.\n- FavIcon handling completely flawed - e.g. dot.kde.org-icon completely distorted, others are not displayed at all (packman.links2linux.de), FavIcons are often mixed up between different pages.\n- Cookie management a mess - Konqueror keeps on bugging the user over and over again whether to accept certain cookies, even if it's been told time and again to accept all cookies from this domain.\n- Sometimes links are simply not clickable, or clicking links does not work as expected.\n\nThat's only what I can think of at the moment, lots of small quirks simply add up to a completely annoying web browsing experience. It currently looks as if the KHTML development cannot keep up with the real web...\n\nAs I said before, Firefox does all this without problems. In fact, many of the rendering problems are even gone when using the experimental WebKit-part. Many of those bugs are in fact already reported as bugs, but nothing is done about them.\n\nI'd like to have a working, KDE-native browser again, as Firefox looks really ugly and un-integrated (even with Oxygen-theme) on a KDE desktop. Which are the plans for the further development of Konqueror as a web browser?"
    author: "Anon Coward"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "\"All my friends - all KDE supporters - have made this switch long before.\"\n\nOdd, I switched to Konqueror/KHTML because I was irritated by Firefox's bloat and complete lack of integration in KDE.\n\nAnd for the rest, did you file bugs?"
    author: "Luca Beltrame"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "actually, there is quite a nice KDE4 Theme for firefox3. It integrates firefox nicely into the KDE environment, except for the file dialogs. Besides, Nokia is working on a QT Version of firefox, so it should blend in even more nicely, in the days to come. \n\nI don't mean to put down konqui, it's a great program, too, but mozilla just is an organization with plenty of resources (actually millions of $) that is dedicated to deliver a state-of-the-art web browsing experience, which is hard to beat. KDE's main focus is-and-should-be on improving the overall desktop experience, which is taking great leaps, if you look at applications like dolphin, plasma, okular, kopete, koffice and the countless other apps."
    author: "chris"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-06
    body: "I am afraid to agree with you. Konqueror integrates perfectly in the KDE desktop because it's a native programme but as a web browser it's not quite there to beat the Fox - not yet. I would be happy to use Konqueror as my default Browser if it could give me the web browsing experience that the Fox does. It's getting better though and I am sure that it will get there because the KDE developers are very ambitious and hard working. Already one can see the tons of improvements.\nAs for the KDE Firefox theme, it's cool, not perfect but very good and the Qt port will make the fox feel even more at home on KDE."
    author: "Bobby"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "Well, you know, the number of smaller and greater bugs in my everyday web experience with Konqueror is really mounting. There are bug reports for some of the errors I describe (e.g. the Ebay thing), but as there is really not the slightest improvement for months and years, there's no real incentive to report all those bugs. \n\nAnd BTW, WebKit doesn't have those rendering errors (but other flaws), so fixing them should be possible.\n\nI tried to use Konquror exclusively since KDE2. And therefore yes, Firefox is bloated, yes, I hate its file requesters, yes, I'd like to be able to use something smaller, better integrated.\n\nHowever, my computer's horsepower can stand a bit bloat, I can cope with some ugly dialogues, I can even live with a slightly different look'n'feel - as long as the main reason d'\u00eatre of a web browser. i.e. rendering real life web pages - is fulfilled."
    author: "Anon Coward"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "> There are bug reports for some of the errors I describe (e.g. the Ebay thing)\n\nI looked up your menu thing, and found https://bugs.kde.org/show_bug.cgi?id=141907\n\nthere's a German there saying it works with current ebay.de...\n\n"
    author: "Leo"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "Firefox 3 runs considerably faster than Konqueror on my box.  For one, it doesn't take ages to start.\n\nIt uses quite a bit of memory by default, but I turn off the page caching features and disable all that memory usage.\n\nThere are KDE 4 themes for Firefox 3, but frankly I'm more concerned with the file dialog.  There is a QT branch of Firefox 3 being developed which will take care of that."
    author: "T. J. Brumfield"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "A Firefox built with Qt will improve the integration slightly. When using Konqueror I also have access to ioslaves and friends. I doubt that will happen with Firefox. \nOh, and I started using Konqueror when I got fed up with the slugginess of Firefox on my Eee 900 (especially temporary freezes with high SSD access after loading a page). Konqui is noticeably faster there. \nConsidering I only needed Adblock (easily fixed), making the switch was rather easy."
    author: "Luca Beltrame"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "On my install, Konqueror always started faster than Firefox.... Do you have any Gtk+ applications starting on login?\n"
    author: "Michael \"Konqueror\" Howell"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Firefox is usually the only GTK app on my box."
    author: "T. J. Brumfield"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "If you don't mind using the KDE 3 file requester, Firefox works quite well with KGTK's LD_PRELOAD runtime patching."
    author: "Stephan"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Correct me if I'm wrong, but I'm pretty sure that the version on KDE-Apps.org is based on KDE4. I know I have a version on my disk that is.\n"
    author: "Michael \"Maybe you can get both\" Howell"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Well it actually has both a KDE3 and KDE4 version."
    author: "Craig"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Does other KDE4 apps take so long to start as well? Because there could be a problem with your scim installation (It caused such delays in my Ubuntu box as well, removing scim and now almost every KDE4 app starts in ~1sec"
    author: "Vide"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Do other KDE4 apps take so long to start as well? Because there could be a problem with your scim installation (It caused such delays in my Ubuntu box as well, removing scim and now almost every KDE4 app starts in ~1sec"
    author: "Vide"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I did the switch myself to the ugly looking Firefox long time ago. Konqueror is a joke because of KHTML. People need to stop making tests showing KHTML is so uber great and fast, because that's all bogus. And are you serious about filing bugs that are so damn obvious that anyone encounters in the first 10 seconds? KHTML it's really deprecated, don't see the point of duplicated effort rather than just going with Webkit asap. I know this sounds pretty harsh, but it's how many of us KDE4 users see things. But i can only brag about it since I'm an artist, not a coder."
    author: "Dread Knight"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I don't see the need to bitch on KHTML if you want to use WebKit instead. If you like WebKit, just use that. If you can't, build your own webkit based browser or plugin for Konqueror, of pay someone else to do it for you. I'm not bitching on you if I don't like your artistic designs either, I'll just not use and ignore them."
    author: "Andr\u00e9"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-04
    body: "So what is your solution? Konqueror is the worst browser. Both Opera and Firefox are better.\n\nThis is a fact. Pages render incorrectly in Konqueror since a long time and the fixes are a joke so far.\n\nI dont care much because I am using 99% Firefox. But if you compare konqueror to things like k3b ktorrent konsole then konqueror really is a bad example for SUCH a critical mission. (Cuz k3b ktorrent konsole yakuake etc... all rock, while konqueror does NOT)"
    author: "markus"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-04
    body: "\"So what is your solution?\"\n\n\"If you can't, build your own webkit based browser or plugin for Konqueror, of pay someone else to do it for you\""
    author: "Anon"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "Funny. It was recently (with the addition of contentEditable) that one of my most used web applications (GMail) became usable for me again (sending mail). I guess it's just a matter of what pages you use.\n\nIt might also be diminishing because of more complex web pages, perhaps?\n"
    author: "Michael \"Konqueror\" Howell"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "\"I guess it's just a matter of what pages you use.\"\n\nYeah, you're obviously right ;). However, I don't want to make the pages I visit dependant on the browser I use. Nowadays most pages on the net can be rendered well enough with IE, Firefox, Opera and Safari, and I don't have to switch browsers when using one of those. With Konqui in its current stage, I have to do that.\n\nThe fact that popular sites are not rendered in an usable way, and the fact that this status might change (not only, as it would be good, for the better, but often enough for the worse) with every new Konqi release makes the web experience shaky. \n\nWith the importance the web has nowadays, a reliable browser is for me an abolute must. Konqueror sadly enough isn't reliable :(.\n"
    author: "Anon Coward"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I agree that more work should be put into making KHTML render everyday pages and have noticed some pages getting broken. I'm saying that not _all_ changes are for the worse in terms of rendering everyday pages. Right now, most of KDE is in flux (plasma is being improved at an astonishing rate, most applications are getting spit polished, old applications are being ported, etc). KHTML is no exception, and due to it's fragile nature some pages will break.\n"
    author: "Michael \"I don't disagree\" Howell"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I know I am really harping on this bug report thing, but this is the case where it definitely matters --- we strive to make things improve every single day, so while regressions happen (despite a giant testsuite), them happening is not in any sense the \"expected\" behavior, and quick accurate feedback on things can definitely lead to improvement... And if it's done by people running branch or trunk, it may mean that the regression never sees a tagged release. Also, please don't assume developers use the same websites you do. I see people mentioning digg --- well, I don't use digg, so I won't know when problems show up on it. \nNow, it sounds like I may want to start using that just for testing, but there is no way I can do this for every single website people like; and things that need accounts are in particular hard to keep track of (especially for a scatterbrain like me, who can't keep track of his TODO list half the time)"
    author: "SadEagle"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "> Now, it sounds like I may want to start using that just for testing,\n\nNo, please don't do that... seing digg.com really takes a huge toll on one'faith in humanity.\n\nBesides I just noticed I fixed the bug some days ago as a side effect of another fix."
    author: "Spart"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "(in my repository)"
    author: "Spart"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "\"No, please don't do that... seing digg.com really takes a huge toll on one'faith in humanity.\"\n\nWell, he's already reading the comments on dot.kde.org."
    author: "Anon"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-03
    body: "Arguably the fact that comments on digg don't work could be seen as doing me a favour ;)\n\nbut it still is a bug.\n"
    author: "txf"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "> many of the rendering problems are even gone\n> when using the experimental WebKit-part\n\nSo, why you don't use the experimental WebKit-part then (switch the view and/or assign that part as fileextension for *.html)? Be free to choose what matches best to you :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Because WebKitPart renders better, but has numerous some flaws. It's very beta, you know?"
    author: "Anon Coward"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-09
    body: "But I thought the answer was to start using webkit :(  Now you're saying it will take time and effort to get it running smoothly?  Now what do we do?"
    author: "MamiyaOtaru"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "Are you sure that rendering errors are really errors? May be web pages uses hacks and tricks that are not completely standard or are tested against other browsers (FF, IE, Safari, etc).\n\n"
    author: "Git"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "That is probably 95% the case. And, if you are a KDE fan and make site/blogs/whatever, check that it works and looks good with KHTML. I've made all my sites do this. From a web dev point of view, KHTML and Gecko render and respond much more similar that IE. So I don't know if it's 100% a KHTML issue. I'd say it's more of a market share issue. Not many devs test for KHTML. It's also a javascripting issue. Lots of the things that \"Don't work\" are javascript related - menus and the like. Perhaps promoting Konqi and decent platform may help."
    author: "Winter"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I'm using KHTML to test my webpages, because its implementation is very near to the standard. My pages are valid XHTML 1.0 Strict, and render flawlessly in KHTML. And when it works in KHTML, I do not have to test it in FF and Opera, because I know they work.\n\nThe only thing I'm using FF for in webdesign is to check layouts with Firebug."
    author: "Stefan Majewsky"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "And that's the answer you always get when reporting those Konquror rendering errors :(((.\n\nBut I repeat: IE, Firefox, Safari, Opera, i.e. 99,x % of the web browsers used today, DO NOT HAVE THESE PROBLEMS. I repeat, any other browser - free and non-free alike - on the market does not have these problema. \n\nThe argument that the rendering errors are because of faulty web pages is valid. But considering that we are living und www surfing in the real world the statement that the rendering of KHTML won't get more tolerant but the web devs should rather change their pages is slightly arrogant and completely unrealistic. \n\nStatements like this are which are really putting me even more off. As any problem of KHTML is attributed to faulty web pages, I as a user get the impression that there is simply no interest in fixing this problems.\n\nHowever, as a user I'm interested in surfing the web and not in being part of a holy (but pointless) crusade concerning web standards."
    author: "Anon Coward"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Your last sentence tells everything. I'm interested on stantards, because I think is the best way to surf the web. \n\nAnd remember, how can a KHTML dev fix a problem on a web if the web developer uses no standard hacks? Making KHTML behaviour like an other web browser? I don't think that's the way.\n\nMay be switching to webkit, because wevdelepers have webkit on mind when they make their webs."
    author: "Git"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "It doesn't work even right on kde-look. The selection to not show wallpapers doesn't even work in konqueror"
    author: "awa"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-03
    body: "Thanks for bringing this up. I have a fix in my tree, but it needs some further testing. Though, again, there are better channels than the dot for these sorts of reports. Even if we developers miss incoming reports (that unfortunately does happen), the folks who help us with bug triage usually don't, and, well, bugzilla has very long memory. Plus, at least I read my bug e-mail every day, and I don't read the dot anywhere near that often.\n\n\n"
    author: "SadEagle"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-03
    body: "The thing is i don't understand the bug reports of kde, i'm used to launchpad. But i will try it again. Thxs for the fix"
    author: "awa"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-09
    body: "thanks ubuntu"
    author: "MamiyaOtaru"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Your whole posting tells everything and your attitude maybe is symptomatic for the problems KHTML is facing in the real world currently :-/\n\n1. It's pretty easy to blame every KHTML problem on the web developpers not implenting standards. I personally can't check this, somethimes it simply sounds like the standard excuse.\n\n2. Reality check! That's what I can only advise to people like you. What's the use if KHTML is standard compliant as hell but can't render real world web pages?\n\n3. WWW-standard conformity is, thanks to Firefox's success and new alternative browsers, nowadays important to web developpers. The time of IE's monopoly are almost over. So the remaining problems are simply a question of KHTML being tolerant of sloppy implementations of standards. And face it, as long as there are humans developping for the web, there will be smaller errors and sloppy implementations of WWW standards. So the ability of a rendering engine to deal with the unavoidable is important!!!!!!!!!!! "
    author: "John Doe"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Yup...\n\nIn konq I often get the rendering error ladybug in the corner. It could be that other browsers don't report bugs hence I don't notice anything, but often I do see slight differences in page rendering compared to other engines (which don't really bother me).\n\nSo I ask is it khtml being intolerant? khtml bug? web site too buggy? or all the previous?"
    author: "txf"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "All of the above, actually. Often times websites have coding errors that show errors on every browser, just our indicator is a lot more visible. Sometimes it can be the website doing too much UA sniffing and serving us crap. And yes, it can be (and often is) a KHTML or a KJS bug (or functionality); in fact there is a KJS bug I have a fix pending for right now which throws an error on a commonly used statistics tracking package. "
    author: "SadEagle"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Actually the worst problems are sites using browser detection(broken), they tend to send Konqueror pages looking quite different than what IE/Firefox/Safari get. And from what I have observed more often than not, those pages are broken too(It would not render correctly in any browser). \n\nAs long as some webdevelopers decide to send crap to Konqueror, the improvments it's developers make will have little impact on the percived result. It's even easily seen, wsimply set Konqueror to identify as a different browser on those sites, and in the majority of cases you DO NOT HAVE THESE PROBLEMS."
    author: "Morty"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Run mail.google.com/mail?ui=1 with FF UA if you don't believe it.\n"
    author: "Michael \"That's my personal experience\" Howell"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "> And that's the answer you always get when reporting those Konquror rendering\n> errors :(((.\n\nI call bullshit on that. Virtually the only time you get this response is in case of UA sniffing.\n"
    author: "SadEagle"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-01
    body: "Are you using trunk, or a release?\n\nTrunk is quite good right now, if I remember correctly, around release time it was not so good. Awful in fact.\n\nThe non clickable links seems to have been fixed a little while ago. I haven't seen the cookie issue for a while, and only when I had a mismatched kdelibs/kdebase. Flash is much more stable, doesn't crash and loads much more reliably.\n\nKDE4 is a work in progress, and each week things get a bit better. Until it all breaks again.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Flash - ah yes. Flash + Firefox = memory hog and crashings. Flash + Konqi = no problem."
    author: "Winter"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Last I saw, Flash wasn't working in Konqi at all."
    author: "T. J. Brumfield"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "And, therefore, no memory problem ;)"
    author: "Sebastian"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Yeah, the nsplugin thingy crashes everytime with flash content on 4.1.1"
    author: "NabLa"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I haven't seen this issue with either OpenSUSE 11.0 (w/KDE4.1.1) or Kubuntu w/KDE 4.1. I can view Flash just fine."
    author: "Luca Beltrame"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "Are you using the GtkQt style thingie or such?\n"
    author: "SadEagle"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-03
    body: "It's working right now. I'm not using a release.\n\nThere were issues in the recent past.\n\nThe problem here is a closed source buggy thing that everyone wants to use. Not khtml.\n\nDerek"
    author: "dkite"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I'm using the 4.1 version supplied by openSUSE (the regularly updated \"Factory\" development version). Are you talking about 4.2 prerelease versions? These would be available in openSUSE, too....maybe I'll give it a try."
    author: "Anon Coward"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "I try to use konq as much as possible, but on sites like digg konq just fails (try reading the comments). \n\nweb interfaces like my router's config page and the uTorrent webUI fail to even log in. Also flash (non free) is much more buggy under konq than it is under firefox.\n\nI really dislike having to open firefox for every other page I visit and the fact that it uses the hideous gtk file dialogs (or its crappy inbuilt one), or that when using gtk-qt4 it still looks out of place (and causes weird page rendering errors). Sometimes I just give up and use firefox for the whole session \n\nI just can't wait for Qt 4.5 so that then we can have a more compatible webkit kpart. I just don't think that as hard as the khtml guys work it will never reach full parity with webkit gecko etc. which have far more resources put into them (more devs, more money, more sponsors and more visibility)."
    author: "txf"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "<quote>but on sites like digg konq just fails</quote>\n\nThis one should be one of the most visible bugs, as the community diggs (and is encourage to digg) stories about kde related matters. Yet we cant use kde s own browser for digging.\nA lot of things are coming along nicely in kde4, fmpov konq has the longest way to go.\nWords of encouragement: some of the disappointment that can be noted concerning Konq4 s performance displays how valuable konq has been so far for the ppl missing it s full functionality atm."
    author: "anon"
  - subject: "Manpower shortage."
    date: 2008-10-02
    body: "There is shortage in manpower in KHTML project. I think there are just three persons working in KHTML (Germain, Rafael and Maksim). I think khtml project needs more manpower and fresh blood."
    author: "Zayed"
  - subject: "Re: Manpower shortage."
    date: 2008-10-02
    body: "Other rendering engines do not have manpower shortages. And the 3 people working on khtml are doing a phenomenal job, but let's face it keeping up to other browsers is becoming harder and harder. \n\nIn earlier years khtml was advanced and I believe this is primarily to do with the fact that web development happened on a slower pace. Nowadays it is just moving too fast. \n\nGetting fresh blood is easier said than done. It is hard in the first place as khtml isn't exactly a glamorous job, and doubly so when other rendering engines have so much more momentum."
    author: "txf"
  - subject: "Re: Manpower shortage."
    date: 2008-10-02
    body: "> Getting fresh blood is easier said than done. It is hard in the first place as > khtml isn't exactly a glamorous job, and doubly so when other rendering \n> engines have so much more momentum.\n\nAnd even more so when there is FUD going around from one's \"friends\" telling people the project is dead. \n\nBut really, there is an opportunity for gutsy people who want to contribute yet --- do you want to be a mercy of a few corporations, or do you want to keep KDE infrastructure in community's hands and match those with far more resources blow-for-blow by developing smarter?\n"
    author: "SadEagle"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "\"And even more so when there is FUD going around from one's \"friends\" telling people the project is dead.\"\n\nIt's not dead, but it's not going well, and the people who actually initiated KHTML many years ago have accepted the inevitable and are working on WebKit. KHTML will not be able to do what the original poster in this thread wants standing by itself, that much has become clear for the past *ten years*.\n\n\"do you want to be a mercy of a few corporations, or do you want to keep KDE infrastructure in community's hands\"\n\nWhy are you using Qt then? There is also no rule, written or unwritten, that says a web rendering engine is a part of KDE's infrastructure. Additionally, that's just implied FUD about WebKit and probably Gecko as well. People are contributing and using them without the 'big bad corporation' influence you are trying to insinuate."
    author: "Segedunum"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "> the people who actually initiated KHTML many years ago [...] \n> are working on WebKit\n\ncome on, that \"founding father\" crap is getting old...\nand I'm sure they do not want to be reminded of that either, as all the two of them did is sold out their names on complacent revisionist PR articles.\n\nI've been reading every WebKit commit messages since 14/11/2005, that's 24000+ of them, so I guess if those KHTML 'founding fathers' were contributing en masse to the core engine, I'd have noticed by now... :-)\n\nThe only guy that used to contribute to KHTML in 1999 that's working on the engine is Antti, an Apple engineer.\n\nWhich is not surprising given the WebKit engine is more than ever *locked up* by Apple engineers. They are happy to accept patches from the occasional student commiter, that will later on be duly engaged by Apple if he proves valuable, but anytime a corporation or organization attempts to contribute something, anything, significant it is mercylessly drawn into pointless bickering or their patches are let to rot in the bug system.\n\nTrolltech engineers (now Nokia) for instance have never ever made *even a single* contribution to the core engine. Never. How awkward is that for an alleged \"key contributor\"?\n\nAll they do is porting to Qt, fixing the Qt build, fixing the Qt build system, and other spiritless work, adapting to code whose behaviour has been decided for them by Apple engineers alone.\n\nFor more of the same, have a look at the latest thread of poor Google engineers that have not yet understood the rules of the game, trying to propose a new URL framework on WebKit mailing lists and being told to basically stick it somewhere by the whole gang of Apple employees:\n\nhttps://lists.webkit.org/pipermail/webkit-dev/2008-October/005200.html\nhttps://lists.webkit.org/pipermail/webkit-dev/2008-October/005221.html\n\nan instructive read.\n\nThis is what trolls like you want as a future for KDE?\nThis is your ambition for a lively and innovative free desktop?\n\nWell, thank you for your point of view...\n\n> People are contributing and using them without the 'big bad corporation'\n> influence you are trying to insinuate.\n\nthey are being abused, and are even willing to be abused for now,\nfor the sake of the comfort they get from it.\n\ndoes your Nokia shipped webkit engine says \"Webkit Free Software project\" as vendor when you paste the below?\n\ndata:text/html,<script>alert( navigator.vendor )</script>\n\nno, it says \"Apple Computer, Inc\" and that's just the real situation today,\nso be honest and don't try to lie about it.\n\n"
    author: "spart"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "very interesting. go team kthml go :-)"
    author: "Beat Wolf"
  - subject: "Re: Manpower shortage."
    date: 2008-10-04
    body: "\"The only guy that used to contribute to KHTML in 1999 that's working on the engine is Antti, an Apple engineer.\"\n\nLars Knoll and George Stalkos to name another two famous contributors over the years, but hey, let's not let facts get in the way of painting WebKit as some Apple oriented anti-KHTML spin because that's all you have left.\n\n\"Trolltech engineers (now Nokia) for instance have never ever made *even a single* contribution to the core engine. Never. How awkward is that for an alleged \"key contributor\"?\"\n\nThe thing is, they don't have to. That's why there is QtWebKit and not QtKHTML, amongst one other key concern which is that more sites actually work properly with it. But, hey, let's not let practical matters that matter to users and developers alike get in the way of politics.\n\n\"For more of the same, have a look at the latest thread of poor Google engineers that have not yet understood the rules of the game, trying to propose a new URL framework on WebKit mailing lists and being told to basically stick it somewhere by the whole gang of Apple employees\"\n\nIt still doesn't make the code any less useful to Google or they would have used something else. I don't recall many KHTML developers, the ones who are left at any rate, being less of a pain in the ass to deal with.\n\n\"no, it says \"Apple Computer, Inc\" and that's just the real situation today,\nso be honest and don't try to lie about it.\"\n\nAlas, all anyone cares about is their web pages rendering correctly. Arguing about anything else is fruitless, and alas, after ten years plus KHTML is simply not going to achieve that goal by itself. Ever."
    author: "Segedunum"
  - subject: "Re: Manpower shortage."
    date: 2008-10-04
    body: "> Lars Knoll and George Stalkos\n\nThey just maintain the Qt port, they do not work on the engine.\n\n(as the rest of your post is back to repeating your usual heinous drivel and does not bring anything new, I'll just ignore it. Thanks ;-)\n\n\n\n\n\n"
    author: "spart"
  - subject: "Re: Manpower shortage."
    date: 2008-10-04
    body: "Why KDE if the Linux desktop has 5% marketshare?"
    author: "Ray"
  - subject: "Re: Manpower shortage."
    date: 2008-10-09
    body: "I wish this could be stickied somehow"
    author: "MamiyaOtaru"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "Isn't this just one more reason to stop having a \"fork\" (ok, I know the khtml is the original and webkit is the fork) instead of investing time and energy into integrating webkit that have tons of people and groups like google, nokia and apple working on? For me, and a thousand sorry if I'm wrong, it just looks like an ego thing.\n\nI still do not see any reason why khtml must survive, exept the developers do not accept that most users just want webkit?\nI use firefox in Linux, and will switch ASAP to Google Chrome, because, I'm sorry to say, it is sad to me also, but webkit evolved much-much over khtml.\n\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "\"I still do not see any reason why khtml must survive\"\n\nIt's part of kdelibs in KDE4, so it must be supported until at least KDE5.  That's just one reason."
    author: "Anon"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "OK, you're right of corse, so let me correct the sentence:\n\"I still do not see any reason why khtml must be used in a default engine in konqueror of better alternatives do exists\".\n\nThanks!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Manpower shortage."
    date: 2008-10-03
    body: "The only \"alternative\" html engine for Konqueror that I'm aware of is the webkit KPart, which is still very immature.  Calling it \"better\" at this early stage is a bit of a stretch, if you ask me."
    author: "Anon"
  - subject: "Re: Manpower shortage."
    date: 2008-10-04
    body: "No. I prefer stick to khtml and does not move to webkit. We need the freedom to do what we want in our web rendering engine ! I suggest to do a campaign  to attract more developer to khtml ."
    author: "Zayed"
  - subject: "Re: Manpower shortage."
    date: 2008-10-04
    body: "Actually, you don't even have to know c++ or be super smart to dive into deep khtml core code, just helping testing, dealing with bugs, reducing test pages (compare 2 reports: website doesn't render correctly or specific value of css attribute in some particular case doesn't work) will be a great help to project. And that should be something our Free community provides, in proprietary world you don't expect users to act"
    author: "vyacheslav"
  - subject: "Re: Plea for a better web browsing experience!"
    date: 2008-10-02
    body: "If you want fixes, please report bugs to bugs.kde.org, not here. We do our best to fix things, but we have to know about the problems to fix them.\n\nP.S. The dot favicon thing is a Qt bug. "
    author: "SadEagle"
  - subject: "Ark context menus"
    date: 2008-10-01
    body: "Ark was in serious need of some love since the 4.0 launch, and from what I've been reading on the Planet, it has been receiving some.  I am very curious to try a recent build.  Thanfully openSUSE provides near-snapshot builds."
    author: "T. J. Brumfield"
  - subject: "Re: Ark context menus"
    date: 2008-10-01
    body: "Ark was in serious need of love since it was first created, IMO, so the recent developments are very much appreciated by me :)"
    author: "Anon"
  - subject: "Re: Ark context menus"
    date: 2008-10-02
    body: "xarchiver is generally the second GTK app on my system, due to Ark. (Firefox being first) This was true for KDE3 too. The interface did a whole bunch of unintuitive crap that \"just worked\" in xarchiver.\n\nBut then xarchiver started segfaulting opening up .tar.gzs and all was dark in the land of archive-managing-on-my-computer. Ark in KDE 4.1 came a long way, but at the time of release (and right now for all of us 4.1.x users) still has a long way to go. Luckily 4.2 should fix all these issues, and all will be well with the Force. Er, the cause for my computer with less GTK. ;-P"
    author: "Jonathan Thomas"
  - subject: "Re: Ark context menus"
    date: 2008-10-02
    body: "Agreed, it always annoys me when when distributions has it as default for compressed files. I much more prefer using Konqueror with KIOslaves and servicemenu."
    author: "Morty"
  - subject: "KFormula"
    date: 2008-10-02
    body: "It's nice to see that the range of KFormula is being expanded. However, I am still looking for a formula editor that translates LaTeX into a scalable formula representation that is well integrated and that used LaTeX for rendering. OOoLatex for OpenOffice is a killer app for me, I'd like to see something similar in KOffice."
    author: "ac"
  - subject: "Re: KFormula"
    date: 2008-10-03
    body: "It would be quite a big dependency for KOffice to depend on LaTeX."
    author: "JohnFlux"
  - subject: "minor typo"
    date: 2008-10-02
    body: "text says \"Now almost all the UML widgets use TextItemGroups and TextItems to display their text which has cleaned freed them\" - it seems it could do with one of those words ;)"
    author: "richlv"
  - subject: "Powerdevil really good"
    date: 2008-10-02
    body: "Thanks, Danny, for the Digest!\nI really appreciate the design of Powerdevil (kded daemon, system setting configuration, profiles, plasmoid). I wonder if wifi/network support should be done the same way: a kded daemon checking available wifi networks, a system settings configuration to configure connections as profiles and allowing for full configuration (including routing) and a plasmoid to monitor networks and select profiles to use.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-02
    body: "Seconded!"
    author: "Tim"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-02
    body: "Take a look at:\nhttp://websvn.kde.org/*checkout*/trunk/playground/base/plasma/applets/networkmanager/DESIGN\n\nFor openSUSE there's already a version in the buildservice, though I haven't tried this out yet:\nhttp://software.opensuse.org/search?baseproject=openSUSE%3A11.0&p=1&q=networkmanager-kde4\n\nhave fun :)"
    author: "Robin"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-02
    body: "Yes, please. +1 from me.\nPlease ditch those OpenSuse tools that doesn't integrate at all, doesn't follow mainstream development and has very slow release cycles (yes, knetworkmanager, I'm looking at you)."
    author: "Vide"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-02
    body: "I disagree. IMHO the design of NetworkManager, with a system level daemon, is dead on. The design of PowerDevil (and most other power management tools), using daemons that live in the desktop session, is quite flawed. The latter don't allow for power management when no user is logged in, and it is not clear what should happen when more than one user is logged in. Relying on the whole desktop stack also makes these tools unreliable. In particular, kded will be no more stable than the least stable application that latches onto it. On one of my machines, I have to kill and restart kded at least once daily.\n\nSo, please take those sexy configuration tools, separate out the daemon part, design a DBUS protocol for specifying power management settings for communicating between the GUI and a *system level* daemon, and make power management just work!"
    author: "Martin"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-02
    body: "You are so right, doing the network with a desktop session daemon would be even worse. And it would be totally useless in the enterprise, where centralized passwordservers are very common (In my experience NetworkManager does not work particularly well in such cases either:-(). Not to mention breaking stuff like NTP on bootup."
    author: "Morty"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-03
    body: "I was talking about wifi, not all network, and this is usually something done on user space anyway.\nDo you guys ever consider that a good part of computer users just run their own laptop or desktop, not servers?\nI mean you don't need X11 on a server either...\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-03
    body: "Well I was actually talking about wifi too, never mentioned anything about servers. \n\nAnd using wifi on a laptop, or even with wired ethernet, in a corporate setting are close to useless with such session dependant tools. You are forced to first log in with a cached account, then you need to get the network running before you have to (manually) log in to your network resources/disks etc.\n "
    author: "Morty"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-03
    body: "What I'm talking about is basically a tool to change the current configuration easily which does not stop you from having defaults. After all, it will still use wpa_supplicant and DHCP (which already run as a system daemon), which means you'll connect to default network at boot anyway if it's available.\nChanging wifi network configuration is usually something you do when connected, not something done in background anyway.\nTypically, I'd like something like wicd, which works well but is not completely well integrated in KDE.\n\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-02
    body: "I agree. There is actually work on a FreeDesktop.org specification for a DBUS interface, so this type of thing should be possible (PowerManager?).\n"
    author: "Michael \"+1\" Howell"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-03
    body: "You're talking about something different.\nPowerdevil uses Solid and this uses whatever power management tool goes underneath. This means you can still have power management when nobody is connected, it just means the default power management settings (or last Powerdevil settings, I don't know) is used. \nPowerdevil makes power management very simple in the exact situation where you need it (someone on his laptop or on his desktop wanting to change manually the configuration for a specific occasion) without being mandatory for power management to happen. \nAnd what I propose would be exactly the same, since Solid would use something like NetworkManager for network configuration anyway.\n\nThere's nothing wrong wanting something that works now and is actually somewhat futureproof (nothing stops Powerdevil to use in the future some daemon doing the job). Especially since doing daemons that seem able to deal with every situation seems to be a complicated enough task to postpone anything usefull for years.\n\nBottom line : since Powerdevil is actually more or less what you described : a daemon, separated from the configuration tool and from the monitoring GUI, all talking using DBUS, it's closer to what you want than anything existing. I guess that once there will be a system-level daemon doing the job, the porting effort will be small. Please do go on and write such daemon.\nSince then, I'm happy there's something working."
    author: "Richard Van Den Boom"
  - subject: "Re: Powerdevil really good"
    date: 2008-10-06
    body: "PowerDevil currently (as in current SVN) supports the standard FreeDesktop.org DBus interface. If a system daemon were running in the background (obviously, using the standard communication interface), PowerDevil would never be launched at all. As a result, zero porting effort would be necessary. Am I correct?\n"
    author: "Michael \"DBus\" Howell"
  - subject: "What about basket?"
    date: 2008-10-02
    body: ">> A KDE 4 port of KnowIt, a note taking application, is imported into KDE SVN. <<\n\nAnyone knows what's up with Basket? Is it still developed? I love this tool: http://basket.kde.org/"
    author: "Florian"
  - subject: "Re: What about basket?"
    date: 2008-10-02
    body: "Ask on #basket-devel, and report back with your findings :)"
    author: "Anon"
  - subject: "Re: What about basket?"
    date: 2008-10-02
    body: "There has been activity as recently as 24 Sep 2008 so I presume it is still in active development.\n\nSource for kde4 is at http://github.com/kelvie/basket/tree/master."
    author: "hbc"
  - subject: "Umbrello"
    date: 2008-10-02
    body: "I'm impressed with the work being done in Umbrello, it looks really great. Being able to segment the lines is just what I need for my projects full of inter-class links.\n\nActually Umbrello is one very nice tool in KDE (gladly the 4 version is not like the KDE3 one that crased often) that no one really praises as we should."
    author: "Iuri Fiedoruk"
  - subject: "Re: Umbrello"
    date: 2008-10-02
    body: "I agree. This looks really nice. I think it would look even better if the lines where somewhat smoother and if the colors would be \"cooler\". Maybe its possible to use the oxygen color scheme or something like this. Besides that it looks cool, even though I dont have a use for this.\n\nWhen I look at it I think this is kind of similar to a mind mapping software. I think this would be a great addition to the koffice suite. Right now I work a hell lot with mind mapping software and it can really stramline your workflow."
    author: "Dr. Schnitzel"
  - subject: "Okular questions"
    date: 2008-10-02
    body: "Is it a \"parser\" for fax documents, or a \"generator\"? Has the font rendering regression in Okular versus KPDF 3.x been addressed yet?"
    author: "christoph"
  - subject: "Re: Okular questions"
    date: 2008-10-02
    body: "if I choose minimum memory usage strategy in okular, it crashes less often."
    author: "Nick Shaforostoff"
  - subject: "Re: Okular questions"
    date: 2008-10-03
    body: "So you've reported all the crashes you get? Or expect them to be fixed miracously?"
    author: "Albert Astals Cid"
  - subject: "Re: Okular questions"
    date: 2008-10-04
    body: "> if I choose minimum memory usage strategy in okular, it crashes less often.\n\nWhich crashes?\nAnd beside that, there's exactly nothing in the code that would make a difference so radical to produce crashes between low memory and high usage configurations."
    author: "Pino Toscano"
  - subject: "Re: Okular questions"
    date: 2008-10-03
    body: "It's an okular generator, which means it generates pixmaps so the app can show you the contents. yeah probably it's a bad name for non okular developers.\n\nHas the font rendering regression in Okular versus KPDF 3.x been reported yet?"
    author: "Anon"
  - subject: "Re: Okular questions"
    date: 2008-10-04
    body: "> Is it a \"parser\" for fax documents, or a \"generator\"?\n\n\"generator\" is the internal name for Okular document backends.\n\n> Has the font rendering regression in Okular versus KPDF 3.x been addressed yet?\n\nNope, as Okular does not do own rendering (unlike KPDF).\nRendering bugs in PDF documents go to the Poppler bug tracking system, https://bugs.freedesktop.org, \"poppler\" product."
    author: "Pino Toscano"
  - subject: "4.1.2 is great"
    date: 2008-10-03
    body: "Heh... the announcement is not out yet, but Kubuntu has already pushed out the 4.1.2 packages, and they're frickin' awesome. At least two of my major pet peeve bugs have been fixed (cursor works nicely again in location bars, Klipper doesn't pop up its \"Actions\" menu twice anymore), and Plasma feels smoother every time I try to make it work for me. Spelling errors in text boxes are now underlined with those wavy lines instead of being painted red. And probably tons of stuff that I haven't discovered yet.\n\nWhoo! That's the way to make the wait for 4.2 worthwhile!"
    author: "Jakob Petsovits"
  - subject: "MathML support in KHTML"
    date: 2008-10-03
    body: "KFormula as a widget could bring MathML support to KHTML, couldn't it?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: MathML support in KHTML"
    date: 2008-10-04
    body: "If I remember correctly, this is one of the ideas of it."
    author: "Pino Toscano"
  - subject: "Gwenview feature set?"
    date: 2008-10-04
    body: "First I wanted to say again that Gwenview is excellent work (and everybody knows that I am a hard grader :-D).\n\nThere is the question of the feature set of different application.  Specifically how much overlap there should be between a viewer and a paint application.  Apparently, it has already been decided that a viewer should also be able to modify and save images.  I'm not sure about this, but it is probably much more convenient than opening them in KolorPaint to make minor changes.\n\nI read in the Gwenview blurb:\n\n> Gwenview does not try to replace your favorite image editor, so it only \n> features the most simple image manipulations: rotation and mirroring. \n\nNow we have red eye reduction.  Which seems to contradict that.  I would think that this would be better in a photo viewing application.  Or, is GwenView going to be the photoviewing application?  Which is OK with me.\n\nSomeone suggested to me that we port KuickShow to KDE4.  IIUC, the reason to do this would be that it has features that GwenView doesn't.  Anybody with thoughts on this, please tell me what you think.\n\nWhat I think is that KuichShow has:\n\nBrightness\nContrast\nGamma\n\nwhich are currently available in KolorPaint but not GwenView.  I find these necessary when viewing images on the web.  If these were available in GwenView, I would see no reason to port KuickShow.\n\nI was also considering the way that GwenView (and other KDE apps) \"import\" RAW files.  They don't actually import them but rather convert them to 8 bits per color using the camera's white balance setting.  Using RAW this way tends to confirm what I think about many users just wanting a lossless compression format.  Perhaps a Bayer CFA format can be added to JPEG2000.  But the immediate concern is white balance.  If we are going to convert RAW images this way, we also need:\n\nWhite Balance.\n\nand it needs to be properly calibrated in DecaMireds."
    author: "JRT"
  - subject: "OUTLOOK EXPRESS ERRORS"
    date: 2008-10-12
    body: "recieving email thru outlookexpress but cant send.error msg.soket error 1061\nerror number 0x800ccc0e.please HELP ME!!!!!!!!!!!!"
    author: "marshall drake"
---
In <a href="http://commit-digest.org/issues/2008-09-21/">this week's KDE Commit-Digest</a>: Various work across <a href="http://plasma.kde.org/">Plasma</a>, including improved applet handles with monochrome icons, work on the Weather Plasmoid and the start of an extender-based notification applet. Continued development in PowerDevil, including support for suspend. Long-standing "slow deletion of many files" bug is finally fixed. A System Settings module for choosing the default file manager. Basic implementation of red eye reduction in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. A generator for G3/G4 fax documents in <a href="http://okular.org/">Okular</a>. Support for filter plugins in <a href="http://kst.kde.org/">Kst</a>. More work on code completion in <a href="http://www.kdevelop.org/">KDevelop</a> 4. Start of a D-Bus interface in Lokalize. First working implementation of KMenuEdit global shortcuts. Work on supporting different resources in the <a href="http://pim.kde.org/akonadi/">Akonadi</a> OpenSync plugin. The return of Ark context-menu actions. Liechtenstein, Oman, and San-Marino maps in <a href="http://edu.kde.org/kgeography/">KGeography</a>. Previews of slide transition effects in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a> now happen directly on the affected slide. A <a href="http://www.koffice.org/kformula/">KFormula</a> widget is extracted from <a href="http://koffice.org/">KOffice</a> and moved into kdelibs for use in other KDE applications. Work on porting Keep, a backup utility, to KDE 4. <a href="http://nepomuk.kde.org/">NEPOMUK</a> query libraries move from kdereview to kdebase/workspace, with the search KIO slave moving into kdereview. A KDE 4 port of KnowIt, a note taking application, is imported into KDE SVN. <a href="http://eigen.tuxfamily.org/">Eigen</a> 2.0 Beta 1 is tagged for release. <a href="http://commit-digest.org/issues/2008-09-21/">Read the rest of the Digest here</a>.

<!--break-->
