---
title: "KDE 4.0.5 Available"
date:    2008-06-04
authors:
  - "skuegler"
slug:    kde-405-available
comments:
  - subject: "advantages"
    date: 2008-06-07
    body: "Are there any particular aspects that might be nicer in 4.0.5 than 4.1 beta 1? Anything particularly more stable for instance?"
    author: "ad"
  - subject: "Re: advantages"
    date: 2008-06-07
    body: "i don't really think so, i have kde4.0.81 and it is damn stable and fast desktop"
    author: "mimoune djouallah"
  - subject: "Re: advantages"
    date: 2008-06-08
    body: "If only I could say the same. It's hard to say what's broken versus what's not implemented. It's unusable as a desktop. Some individual apps are working well, but others are not. I'm sorely disappointed in this \"beta1\"."
    author: "David Johnson"
  - subject: "Re: advantages"
    date: 2008-06-08
    body: "so, you are disappointed because the beta-version is not 100% stable? I think you need to remind yourself what \"beta\" means..."
    author: "janne"
  - subject: "Re: advantages"
    date: 2008-06-09
    body: "There was a time not long ago, when \"beta\" meant that the software was almost ready. It still had some bugs, but it was usable and fairly stable. There were some bugs but they were documented.\n\nhttp://en.wikipedia.org/wiki/Software_release_life_cycle#Beta\n\n"
    author: "David Johnson"
  - subject: "Re: advantages"
    date: 2008-06-11
    body: "In some cases maybe, but it's pretty dumb to think that all beta-software is \"almost ready\". \"Beta\" means that the software is not ready. Why is it not ready? Because it has bugs. If it didn't have bugs, it would be called \"Release Candidate\" or it would be the final version."
    author: "Janne"
  - subject: "Re: advantages"
    date: 2008-06-11
    body: "For me, as a simple-minded KOffice developer, alpha means \"all features are present in one form or another, but they still need a lot of work to get them working\". Beta means \"all code to make the features we want to release work is present, but probably buggy, please help us find the bugs\". Release candidate means \"it works pretty well for me on my box, but I would like to know whether it works for other peoples on their systems, too.\""
    author: "Boudewijn Rempt"
  - subject: "Re: advantages"
    date: 2008-06-07
    body: "In the 4.1 branch Plasma underwent some major changes so Plasma might be more stable in 4.0.5. For the most part KDE 4.0.X works on my system but 4.0.80 has a major problem. The desktop shows up on the upper 2/3 of the screen so 1/3 of the screen on the bottom isn't used. I think the problem might have something to do with the fact that I have dual-head graphics. When I checked the display configuration both heads were reported as being connected even though I'm only using the first head (VGA). The first head was set to the correct size for my monitor (1680x1050) but the second head (DVI-D) was set for a smaller size (1280x800). Maybe it's sizing my desktop's y size appropriate for the second head but should be sizing it for the first head. KDE 4.0.X only detected the first head (VGA) and sized the screen properly.\n\nMore precisely it appears that:\n(1) The bottom panel uses the wrong head for it's x and y values. The right hand side of the panel should appear at the bottom right hand side of the screen but appears at 2/3 of the screen in both horizontal and vertical directions.\n(2) The desktop (the area that gets filled when you maximize an application window) uses the correct head for it's x value and the wrong head for it's y value. When I maximize an application window the window uses the whole screen horizontally but only 2/3 of the screen vertically."
    author: "Jeff Strehlow"
  - subject: "Re: advantages"
    date: 2008-06-08
    body: "Weird, because we've seen that bug in 4.0.x too in Fedora (I had it happen personally on my laptop, I ended up disabling the external video completely in xorg.conf to work around it)."
    author: "Kevin Kofler"
  - subject: "Re: advantages"
    date: 2008-06-08
    body: "To be honest I haven't tried all the versions of 4.0.x only the earlier versions. Maybe the problem first showed up in a later version of 4.0.x. I'm using AMD G690 on-board graphics and don't have a graphics card. Maybe I can somehow disable the second head in xorg.conf."
    author: "Jeff Strehlow"
  - subject: "Re: advantages"
    date: 2008-06-08
    body: "Sorry that's AMD 690G on-board graphics not AMD G690 on-board graphics"
    author: "Jeff Strehlow"
  - subject: "kopete bugs fixed"
    date: 2008-06-07
    body: "I'm so happy those kopete bugs got fixed. That was the most annoying thing in my KDE 4.0 environment. :) I hope the 4.0.5 packages are soon in fedora repos."
    author: "Thomas"
  - subject: "Re: kopete bugs fixed"
    date: 2008-06-07
    body: "Some of the Kopete bugs are already fixed in kdenetwork-4.0.4-4.fc9 in the Fedora 9 updates, but KDE 4.0.5 packages with more Kopete fixes are now in updates-testing."
    author: "Kevin Kofler"
  - subject: "Re: kopete bugs fixed"
    date: 2008-06-07
    body: "There are already 4.0.5 packages in fedora-updates-testing. And yes it's good that these kopete bugs are getting fixed, was using pidgin for the time being ..."
    author: "Gerwin"
  - subject: "Error in the changelog"
    date: 2008-06-07
    body: "The links \"all SVN changes\" don't work =)"
    author: "Eduardo Robles Elvira"
  - subject: "KWin"
    date: 2008-06-07
    body: "Hmm ... \"Hello, this is Lubos Lunak, and I pronounce KWin as KWin!\" :) ?\n\nSCNR. Or do we need Matthias for that?\n"
    author: "Lubos Lunak"
  - subject: "Re: KWin"
    date: 2008-06-07
    body: "Whenever there is a sudden frenzied burst of activity in KWin, can we call that a Kwin Storm?"
    author: "Anon"
  - subject: "ArchLinux packages updated"
    date: 2008-06-07
    body: "ArchLinux packages updated on AUR and repositories"
    author: "Br\u00e1ulio Barros de Oliveira"
  - subject: "Re: ArchLinux packages updated"
    date: 2008-06-08
    body: "Shouldn't those be mentioned in binary-4.0.5.inc then?\nIf you don't have commit access to KDE SVN, I can add something if you write up a blurb (HTML, but please only use plain text and <a href=\"...\"> tags, and <strong></strong> around \"ArchLinux\", otherwise the formatting will be inconsistent)."
    author: "Kevin Kofler"
  - subject: "Re: ArchLinux packages updated"
    date: 2008-06-08
    body: "Sorry, what is binary-4.0.5.inc?\nI don't have commit access to KDE SVN. Couldn't understand what for is the HTML code"
    author: "Br\u00e1ulio Barros de Oliveira"
  - subject: "Re: ArchLinux packages updated"
    date: 2008-06-10
    body: "It's what ends up in the info page for the release, telling users where to get binary packages for your distribution."
    author: "Kevin Kofler"
---
The KDE Community has today <a href="http://www.kde.org/announcements/announce-4.0.5.php">made available</a> the fifth update to KDE 4.0. Although the changelog is not particularly long, the release should be worthwhile to upgrade to. KWin (according to some pronounced "quinn" instead of Kay-Win) has gotten some clipping fixes, also some dim and fade effects work on cards that can only do compositing via XRender. Juk, the lightweight music player has received some fixes regarding keyboard shortcuts, playback and cover art. Kopete, the instant messenger in 4.0.5 has some crashes fixed. The <a href="http://www.kde.org/announcements/changelogs/changelog4_0_4to4_0_5.php">changelog</a> has the details (although probably not all bugfixes have been recorded here). Overall, we expect to make the users' experience with this release a bit nicer.


<!--break-->
