---
title: "openSUSE 11.0 Released with KDE 4.0.4"
date:    2008-06-22
authors:
  - "fgiannaros"
slug:    opensuse-110-released-kde-404
comments:
  - subject: "Congrats"
    date: 2008-06-22
    body: "Congrats to the openSUSE team on this release, it seems like a good one.  \n\nOne question: how much of the work put into your plasma modifications has been obsoleted by KDE trunk already, and what (of those changes) will not be going into trunk?\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Congrats"
    date: 2008-06-22
    body: "I think most of their work was mostly selective backporting from trunk, to get the 4.0 plasma a bit more feature complete, so when 4.1 does get released most patches won't add anything new.\n\nAnyways, thanks to all the openSUSE distro, I've always said it, and I say it again: openSUSE is *THE* KDE distro, the one which in my opinion nails it completely when packaging and delivering KDE.\n\nKudos to all openSUSE devs!"
    author: "IAnjo"
  - subject: "Re: Congrats"
    date: 2008-06-23
    body: "Maybe they do put a lot into it. Though, I'd say \"The\" KDE distro would only package a very lean base system and a vanilla KDE. Include all the tools needed to admin the system via the KDE, then you'll have \"The\" KDE distro. I know one distro that comes very close, but I won't mention it because I'm not wearing fire resistant apparel."
    author: "winter"
  - subject: "Re: Congrats"
    date: 2008-06-24
    body: "It packages vanilla KDE but it is not a good idea for the users when the vanilla KDE is unusable (for most users)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Congrats"
    date: 2008-06-22
    body: "Beineri wrote about this few days ago -> http://www.kdedevelopers.org/node/3516\n\n\"Most of the new Plasma features developed upstream until the Plasma sprint in April have been backported. Additionally we added our own new features and patches (all on KDE SVN server), which we tried to get upstream until review board went down and the sprint kicked off incompatible plasma API changes. A few of those changes already found their way into other distributions like an early version of our branding patch into Kubuntu or the moving of plasmoids on panels into Fedora.\n\nSo here is a list (excluding bugfixes) of changed things I still remember:\n(long list)\"\n\nThank you openSUSE for great release! I will install it after exam session for sure. "
    author: "Anon1234567"
  - subject: "Yast Package Manager"
    date: 2008-06-22
    body: "In the 10.x series the Yast package manager was incredibly slow and forced an update of the repositories every time you updated it. Very good news for people that switch to 11 because now it's FAR faster and you can skip the automatic updates. The openSUSE devs have really done a great job fixing a lot of the annoyances from the 10.x series (the package manager's speed being the worst IMO) and deserve congratulations for such a good release!\n\nA while ago I switched my openSUSE 11 install (technically rc install) over to using the KDE 4.1 development repositories and it's actually *quite* stable, adding several nice features. I believe the amount of improvements in 4.1 over 4.0 should illustrate quite well that the KDE4 series will completely obsolete the 3rd series much sooner than a lot of people would expect for such a massive re-architecting. The entire KDE team does *amazing* work!"
    author: "Kitsune"
  - subject: "Re: Yast Package Manager"
    date: 2008-06-23
    body: "I'm glad the rpm installer is fast again.\nI was a suse user in the 9 and 10 series, but novel started using mono for packages and everywhere they could and I more than soon switched to ubuntu, where I'm still happy.\nSeems like openSuse is finnally become enough independent from novell to not accept every single feature the company wants to place in the suse distro."
    author: "Iuri Fiedoruk"
  - subject: "Suse"
    date: 2008-06-22
    body: "Suse is still one of the best distributions for Desktop users who use the KDE technology. But when will OpenSuse be switched to an apt-based package installer? This really keeps me away. YaST is great, the KDE is polished but I still prefer Kubuntu just because of apt.\n\nDo you think the LSB can be helpful to standardize the fundamental architecture?\n\nJust look at this:\nhttp://wiki.winehq.org/LibraryDependencies\n\nWhy aren't Linux distributors able to standardize at least the fundament, the naming of standard libraries?\n\nFor KDE Suse stands for quality. The advantage of using Vista as an OS is that you can always install the lastest free software and it just runs. Firefox 3 out, you can install it. Under Linux package availability is a problem. And Suse worked very hard to improve the situation. Still I wonder why Linux distributors can't standardize at least the core. What are the advantages of diversity here?"
    author: "fooz"
  - subject: "Re: Suse"
    date: 2008-06-22
    body: "> Why aren't Linux distributors able to standardize at least the fundament, the naming of standard libraries?\n \nThey even standardize on the required set of symbols contained in each library.\n\nThere is even a test suite to check whether an application needs any stuff outside the standard set: http://www.linuxfoundation.org/en/LSB_Application_Testkit_Getting_Started\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Suse"
    date: 2008-06-22
    body: "> Suse is still one of the best distributions for Desktop users who use the KDE technology. But when will OpenSuse be switched to an apt-based package installer?\n\nHave you tried YaST and Zypper in openSUSE 11.0? They have seen a big collection of changes (see http://news.opensuse.org/2008/06/06/sneak-peeks-at-opensuse-110-package-management-with-duncan-mac-vicar/ ), which means that resolving depends etc. is practically instantaneous now (faster than apt). Because of the switch to LZMA payload for RPMs, RPMs are now smaller (faster to download), and faster to decompress (faster to install). \n\nBoth YaST and Zypper are an awful lot more featureful than APT and Synaptic (i.e. support for local/remote RPMs, wildcards, shell-like mode, etc.) and contain a smarter (SAT) solver. "
    author: "Francis"
  - subject: "Re: Suse"
    date: 2008-06-23
    body: "personally, I never find package availability a problem.  I find the OpenSUSE repositories to be rather over flowing with packages."
    author: "anon"
  - subject: "Re: Suse"
    date: 2008-06-23
    body: "That's not the point.\n\nWhat about applications that are not in the repository - are you expected to compile them from sources, and install all dependencies yourself? Is the developer supposed to make packages for every version of every distro out there?\n\nEven if the repository contains the package, it's probably the \"stable\" version. What if I want the latest one?\n\nWhat if I'm stuck with an older release of the distro, because my company only supports releases that have been tested for a while? Most people here use Ubuntu Dapper - the LTS release from three years ago. Want Firefox 3? Good luck compiling the recent Gtk+ and other dependencies.\n"
    author: "Dima"
  - subject: "Re: Suse"
    date: 2008-06-24
    body: "have you even used SUSE or OpenSUSE?  your questioning makes it sound to me like you haven't.  I suggest using it before you go on and on about pointless things, and you will find they are pointless once you start using it.\n\n"
    author: "anon"
  - subject: "Re: Suse"
    date: 2008-06-26
    body: "Dima, now you are talkin lots of things mixed in together, about freedom, about they way of thinkin (what is reason why we have lots of different distributions from one OS and lots of versions/editions from one distribution).\n\nAnd you are referring to debian's invented apt package management. You can install apt to opensuse, but do not expect that opensuse would change package system, the RPM > DEB. If you dont like zypper, what is btw faster than apt, then install apt on it. I dont like apt because it's so dificult to type apt-get install <package>. I use Mandriva myself and I just love urpmi. I just give urpmi <package> and it install it, if the name is little different, it list packages what includes that name. Altough I can always give urpmi -f <name> to find package names. And to remove package, I dont need to write apt-get remove <package>, I just command urpme <package>.\n\nI'm not here demanding that OpenSUSE changes package manager from zypper to urpmi.\n\nIf I dont like the distribution what I have chosed among others, I'm free to change to more bleeding edge. You are right about the LTS version of Ubuntu, after 5months of release, it's very hard to get new and stable software on it without compiling itself, atleast I dont need to be coder to make them my self. \n\nI dont know are coming from different OS like Windows or from other distribution like Kubuntu, when you are even suggesting this kind things.\n\nMark Shuttleworth has suggested that all Linux distributions moves to standard release cycle, two times on year. Because Canonical is on problems when it cant controll the whole Open Source world like Microsoft does on it's OS. Canonical knows their OS is just one distribution among others and they want to standard cycle because then they could grow faster, and start controlling the software headings because they are so popular distribution.\n\nI dont like to go that direction, I dont like that Canonical comes to replace Microsoft! I like that software develops faster and more freely like it currently does... \n\n"
    author: "Thomas.S"
  - subject: "Re: Suse"
    date: 2008-06-23
    body: "zypper is awesome in 11 now. It has zip. So no worries. :)"
    author: "Ian Monroe"
  - subject: "LSB"
    date: 2008-06-24
    body: "You call for distros to standardize about the LSB, and at the same time you are upset that openSUSE uses RPM's.\n\nThe LSB calls for distros to use RPMs as the standard."
    author: "T. J. Brumfield"
  - subject: "Thanks!"
    date: 2008-06-22
    body: "Thanks a lot to all the developers for this release. I installed it yesterday and I feel I'm going to have a lot of fun with it. The new package management rocks!"
    author: "galarneau"
  - subject: "Good impressions"
    date: 2008-06-23
    body: "I'm having really good impressions with the Live CD. I've never seriously used SuSE, and I'm quite happy with Debian Sid on my Desktop, however, I'm seriously thinking in using OpenSUSE as a recommended distribution for my newbie friends, instead of Kubuntu. I will have to refresh my knowledge of RPM distros (I used Red Hat and Mandrake years ago), but I think that this gorgeous KDE will be worth it. Plasma and Kickoff looked awesome: it all ran smooth, and maybe it was a placebo effect, but I found the styles/themes/colors a bit more distinguishable than with vanilla KDE.\n\nWhat the hell, I can even try it on my old laptop. How good is the PowerPC port? It's supposed to be the same than on x86(-64)?\n\nSeriously, congratulations to the OpenSuSE team."
    author: "suy"
  - subject: "Re: Good impressions"
    date: 2008-06-23
    body: "> What the hell, I can even try it on my old laptop. How good is the PowerPC port? It's supposed to be the same than on x86(-64)?\n\nThe DVD should work very well for it, yup. "
    author: "Francis"
  - subject: "Kubuntu"
    date: 2008-06-27
    body: "I still prefer to keep family and friends on Kubuntu and Xubuntu for the sake of uniformity and brand recognition which is something gnu-linux users care little about.\nSome people dont like that Ubuntu is becoming synonymous with Gnu-Linux but it is a name that many of my non FLOSS knowing friends ask about.\nCanonical seems to understand the marketing game better.\n\nAs for distros, people who dont know, dont care. I gave Live CD to friends and family and they said they all looked the same depending if it is Gnome or KDE. (KDE always wins by a 2 to 1 margin when given the choice)\nI chose Kubuntu because of the way it works, it looks, its visibility and for them trying to do the right thing when it comes to free software and the community (I even run Gobuntu on one of my machines).\nAnd while I know the small levels of separation between Suse and OpenSuse, as a Non-Compensated Individual Hobbyist Developer, the name still gives me douche chills.\n\nI would never push my Gentoo and Slackware on friends/family but unless I see some enormous shift a distro has achieved, I see no reason changin from one KDE based distro to another.\nMy brother however feels our parents should run a KDE distro like PCLinuxOS and installed them on each of their machines. In the long run, both my parents can run PCLinuxOS at home and Kubuntu at our sisters place when they babysit and have no problems because of the KDE environment. The distro matters less than the graphical desktop environment.\n"
    author: "robbie enderle"
  - subject: "Amazing!"
    date: 2008-06-23
    body: "I had a really great experience installing openSUSE 11.0 (KDE4) on my new laptop... seems much more polished than Kubuntu 8.04."
    author: "Apple Pi"
  - subject: "Re: Amazing!"
    date: 2008-06-23
    body: "I've always found OpenSUSE to be the more polished KDE distro.  I've been using it since 10.2.  With the release of KDE 4.1 it is only going to get better."
    author: "anon"
  - subject: "Re: Amazing!"
    date: 2008-06-23
    body: "You've always found it that way since 10.2. Fabulous. How about the pre-Novell days? Those were novel."
    author: "winter"
  - subject: "Disappointed"
    date: 2008-06-23
    body: "I loved LiveCD but I've never had this many problems installing a distro. I tried beta 3 and it didn't install at all and wouldn't even load KDE, the initial boot would go straight to windows without showing GRUB etc. Gave the final a shot and again, LiveCD works nice and it was brilliant to see how much KDE 4xx has improved but I had installation errors with the DVD and the CD version installed but then my wired ethernet wouldn't connect and my sound card stopped. \n\nHad installation problems on another machine too. Guess I'll wait till Sidux moves on to 4.xx, really looking forward to it though. "
    author: "rkphil"
  - subject: "Re: Disappointed"
    date: 2008-06-23
    body: "Could you please file a bug report about this? I haven't heard of any such issues so far, so it would be really helpful to track those down!"
    author: "Francis"
  - subject: "Re: Disappointed"
    date: 2008-06-23
    body: "I had some problems too, but still love it!\n\nProblem I had: the touchpad of my laptop (dell latitude d830) was not configured correctly. It did random scrolling etc, \nSolution: I copy a piece of xorg.conf regarding the touchpad config I found on the internet.\n\nA problem I still have: xvid won't play, mp3 sounds distorted when I turn volume up. Probably a codec problem, though I installed w32-codecs-all, xvid and other codecs from packman repository...\n\n\nWhy I love it? It was very easy to upgrade to KDE 4.1, and I really like it :-)"
    author: "john"
  - subject: "Re: Disappointed"
    date: 2008-06-23
    body: "> Problem I had: the touchpad of my laptop (dell latitude d830) was not configured correctly. It did random scrolling etc,\nSolution: I copy a piece of xorg.conf regarding the touchpad config I found on the internet.\n\nPlease remember to file a bug report about this! http://bugs.opensuse.org\n\n> A problem I still have: xvid won't play, mp3 sounds distorted when I turn volume up. Probably a codec problem, though I installed w32-codecs-all, xvid and other codecs from packman repository...\n\nSounds like libxine1 wasn't installed. Use the community 1-click-install: http://opensuse-community.org/Multimedia"
    author: "Francis"
  - subject: "Re: Disappointed"
    date: 2008-06-23
    body: "I have the same touchpad problem on my d830. will file a bug this evening.."
    author: "mxttie"
  - subject: "The best openSUSE ever!"
    date: 2008-06-23
    body: "Seriously, everyone who loves KDE (as a mere user, without svn and cmake) should consider using openSUSE. I've switched after several years of Kubuntu and I can't help but recommend it. You really feel the love they put into beautiful configuration, the ONLY thing I'm wondering is why there's a huge firefox icon on the desktop and no visible way to make it the standard browser.\n\nBut really, other than that, I'm 100% satisfied. zypper is great, the artwork is uber, installation and booting is bloody fast and it features the most usable KDE I've ever seen, in version 11 even an everyday usable KDE 4.0!\nWhen using KDE 4.0 back on Kubuntu, it was like a joke. Almost no plasmoids shipped, if, at all, monthly updates, and no more than zero love.\n\nBut on openSUSE, I saw the beauty (not only visual) of KDE for the first time with my own eyes rather than on mockups, I love it!\n\nReally, if you're looking for a good KDE distro (KDE 3 is great on SUSE too), try it."
    author: "fhd"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-23
    body: "The KDE control center (or Systemsettings) has an option to set the default application... Might work for you ;-)"
    author: "jos poortvliet"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-23
    body: "Yeah sure, but it's not \"visible\" in my opinion.\n\nI think the typical openSUSE user will be into using two browsers:\n- Firefox normally because there's the fancy icon on the desktop and it's pretty popular\n- Konqueror whenever a link in any KDE program is clicked\n\nAnd will naturally end up searching for bookmarks and previously visited pages.\n\nI think a distro should feature their idea of a \"default browser\". If firefox is installed per default, it should be the default browser out of the box.\nIf the distro doesn't want firefox to be the default out of the box, there's no reason to install it and put that icon on the desktop. IIRC, Kubuntu sets up firefox as the default.\n\nThat's my opinion. If they really think the user should chose it's browser, the browsers should ask whether you want to make them the default browser. Thinking of it, I never saw a browser doing that on Linux.\n\n\nBut really, this is the _only_ problem I found in openSUSE so far, and I used this ridiculous \"problem\" to emphasize that I didn't find a \"real\" problem yet :)"
    author: "fhd"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-23
    body: "IIRC, Kubuntu doesn't even install FF. I wish it wouldn't install OO. Then, it'd be more pure KDE. I guess there can be a big button on the desktop just for you to make FF the default browser. That should be visible if your monitor works. ;) "
    author: "winter"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-23
    body: "I think kubuntu installs Firefox (at least it's installed on my two kubuntu machines).\nIn the last months it happened more and more often that konqueror didn't work, so that I had to use firefox (e.g. the webinterface for scalix, or openexchange, and others)\n\nAlex\n"
    author: "Alex"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-24
    body: "I must be using a stone age Kubuntu version. Yeah, Konqueror doesn't work for a lot of those Ajax pages. Doesn't Opera use QT? Wouldn't that be more inline with a KDE? IIRC, it's not in the software repos. Too bad..."
    author: "Winter"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-25
    body: "You'll laugh, I'm in fact not using firefox on my openSUSE, I use konqueror. (Using firefox on windows at work though)\n\nI just saw my wife using my system and using two browsers per default as explained by me earlier, I find this wrong.\n\nBTW: I investigated a bit, and firefox does very well think it's the default browser. For some reason though, it is not in KDE apps."
    author: "fhd"
  - subject: "Re: The best openSUSE ever!"
    date: 2008-06-27
    body: "\"For some reason though, it is not in KDE apps.\"\n\nIt is not a KDE app, is it?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "OpenSUSE vs. MacBook"
    date: 2008-06-23
    body: "I tried out the 11.0 livecd and it definitely seems to be a very polished distribution, let alone the best for KDE. The only thing keeping me from using it is that it won't install on a MacBook without the use of rEFIt. I used refit before and it caused serious pains when having to upgrade from tiger to leopard\n\nUbuntu and fedora install fine, I wish opensuse was the same. also I've always found yast vastly more confusing than kcontrol, and with systemsettings/the solid KCM, redundant"
    author: "Mike Wyatt"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-23
    body: "> I tried out the 11.0 livecd and it definitely seems to be a very polished distribution, let alone the best for KDE. The only thing keeping me from using it is that it won't install on a MacBook without the use of rEFIt.\n\nCould you file a bug report about this? Would be interesting to investigate another way (since it is ok on Fedora and Ubuntu). "
    author: "Francis"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-23
    body: "We had the same problem when trying to install openSuse 10.3 on a macBook Pro.  It simply could not be started (either kernel not found or a boot menu could not be installed). \n\nFedora could NOT be installed as well (X was unstable, input devices did not work)\n\nUbuntu did a great job - almost everything (except the typical sound card problem) worked out of the box. Only problem: Ubuntu is not really \"polished\" in my eyes.\n\nMy experiences are not enough to fill a bug report. I am not really willing to spend more time on testing the installation of openSuse. Hope one can understand this. "
    author: "Sebastian"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-23
    body: "I can. I often report bugs, but I don't really do it to make to help the project, I do it to have my problem fixed. BTW, openSUSE is really fast at processing bug reports, while my Ubuntu bug reports have been around for weeks until someone found the time to have a look at it.\n\nI did a huge regression test on wine lately, taking me approximately 5 hours. That sucks, but that's what reporting bugs can be like :)\n(And the problem IS solved now! :) )"
    author: "fhd"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-24
    body: "to be completely honest, when installing 11.0 this time around I had the KDE4 desktop effects enabled and when the installation was nearing complete, the screen flickered, a new X server was started, closed, and then it went back to the previous server but only the mouse cursor was visible. switching to a different VT didn't give me any diagnosis/input either. so I rebooted the macbook and held option to see if there was any option for opensuse (even though it would be labelled windows) and there wasn't. which was the exact same result after setting up the bootloader failed in previous beta/RC releases of 11.0\n\nI'll go through the install again tomorrow and try to file a bug report. could you help me with how I would go about obtaining the necessary information?\nfrom what I can remember it was a lot like this comment on the bug report posted last july\n\nhttps://bugzilla.novell.com/show_bug.cgi?id=220839#c12\n"
    author: "Mike Wyatt"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-23
    body: "KControl/Systemsettings is for per-user settings (that each user can set independently, with the exception of a few modules  like login manager  or printer configuration) while YaST2 is for system-wide administration for the administrator. Just look at the modules of YaST2, you can't set most of those settings from KControl."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-23
    body: "...with the exception of almost everything under System Administration. YAST is SuSE only. There are plenty of KDE apps/config tools for \"admining\" linux. "
    author: "winter"
  - subject: "Re: OpenSUSE vs. MacBook"
    date: 2008-06-24
    body: "Not really, the existing KDE apps/config tools for \"admining\" are not very comprehensive. It consist of a \"random\" collection of tools of varius quality and coverage of feature sets. The avalible modules are simply not even close to match the functionality of Yast.\n\nExcept for Mandrivas MCC (also used by PCLinuxOS), no distribution has anything comparable to Yast. "
    author: "Morty"
  - subject: "No GUI on Dell Latitude C400"
    date: 2008-06-23
    body: "I am a new convert to Linux, OpenSuse11 in particular, I tried the live CD on my old Dell Latitude C400( PIII 1.2Ghz, 1GB Ram) for the first time,and it was love at first site, But after installing, there is no GUI only a command prompt login"
    author: "sp1507"
  - subject: "Re: No GUI on Dell Latitude C400"
    date: 2008-06-23
    body: "The same happened to me in RC and now on the new released version. Both times on Virtualbox though. All you have to do is login and type \"startx\" . \n\nHope it helps."
    author: "Willard"
  - subject: "Re: No GUI on Dell Latitude C400"
    date: 2008-06-23
    body: "This happened to me 5 years ago with SuSE 8.1 when I first installed Linux. :)\n\nLog in as root at the login prompt and try the \"SaX2\" command. It should bring up the X Window System configuration tool. You can also try \"YaST2 runlevel\" and check that the default runlevel is 5  (graphical display manager)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Problem with livecd and firewire"
    date: 2008-06-24
    body: "I have a cdrom-drive which is connected via firewire.\n\nWhen I boot the livecd, it stops very soon in the bootprocess, with the error \"Failed to detect CD\" in the kiwi installer...\n\nIs there any kernelparameter to make the livecd find the firewire cd drive?"
    author: "Yves"
  - subject: "Re: Problem with livecd and firewire"
    date: 2008-06-24
    body: "prolly best asking in the official opensuse forum for help\n\nhttp://forums.opensuse.org/"
    author: "anon"
  - subject: "opensuse 11.0"
    date: 2008-07-02
    body: "The presence of the wireless driver ath5k (not finished prerelease)\nare giving trouble when you instal madwifi drivers .\nConfiguring the card with yast  chosing ath0  all go's well until you reboot.\n\nATH5K is still loaded and gives trouble.\n\nSo you have to delete the map ath5k from lib/kernel/drivers/net/wireless\nto get a atheros wireless card working right.\nFurther more you have to add a line in /etc/modprobe.conf  \"alias ath0 ath_pci\"\nor the driver won't load, or you have to use modprobe ath_pci in boot.local\n\nOn a quad Q6600 processor with 6 gig mem  kde4 is slow in starting slow in opening what ever via the menu. Video card is Geforce 8600GT with nvidia 173.09 drivers.\n\nSo i deleted kde4  and now KDE 3 is running like a chevey 12 cyl on nitro\n\nComplements for this great version 11.\n\n"
    author: "susegebr"
  - subject: "KDE 4.1"
    date: 2008-07-09
    body: "I'm on to install opensuse 11 with KDE. Juts thinking that when KDE 4.1 comes available(released as stable) is it possible to upgrade the opensuse 11 KDE to 4.1? Coming as update? Like firefox 3 beta 5 came firefox 3?"
    author: "Niko Rosvall"
  - subject: "Re: KDE 4.1"
    date: 2008-07-09
    body: "Possible? Yes. As online update like Firefox 3 final? No."
    author: "Beineri"
---
openSUSE 11.0 <a href="http://news.opensuse.org/2008/06/19/announcing-opensuse-110-gm/">has been released</a> (<a href="http://opensuse.org/Screenshots/openSUSE_11.0">screenshots</a>), offering KDE 3.5.9 and an <a href="http://arstechnica.com/news.ars/post/20080620-first-look-opensuse-11-out-offers-best-kde-4-experience.html">excellent experience</a> of KDE 4.0. There has been a <a href="http://en.opensuse.org/Testing:Features_11.0">huge collection of changes</a> and additions in this new release. For an overview of the improvements in KDE, see the KDE <a href="http://news.opensuse.org/2008/06/17/sneak-peeks-at-opensuse-110-kde-with-stephan-binner/">Sneak Peeks article</a> over at openSUSE News, which features an interview with KDE developer <a href="http://en.opensuse.org/User:Beineri">Stephan Binner</a>. He talks about the challenges faced, plans for the future, and what changes you can expect in the upcoming KDE 4.1.



<!--break-->
<h3>KDE 4.0</h3>

The openSUSE 11.0 release includes an installable Live-CD with a SUSE-polished KDE 4.0.4 desktop, while the DVD contains KDE 3.5.9 <a href="http://en.opensuse.org/Media_Layout/11.0">as well</a>. While many applications such as the openSUSE updater applet (with an <a href="http://news.opensuse.org/2008/06/06/sneak-peeks-at-opensuse-110-package-management-with-duncan-mac-vicar/">optional PackageKit backend</a>) have been ported to KDE 4, not all KDE applications are ported to KDE4 yet. In these cases, KDE3 versions of applications such as Amarok, K3b, KOffice or KNetworkManager (adapted to NetworkManager 0.7) are used, which integrate pretty seamlessly. A native KDE4 NetworkManager plasmoid is <a href="http://www.kdedevelopers.org/node/3462">in development</a> and will become available via openSUSE Build Service repositories. There has also been a whole horde of <a href="http://www.kdedevelopers.org/node/3516">Plasma updates and fixes</a> put into the release.

<div align="center"><a href="http://files.opensuse.org/opensuse/en/2/24/OS11.0-plasma.jpg"><img border=0 src="http://files.opensuse.org/opensuse/en/e/e9/Plasma-thumb.jpg"/></a><a href="http://files.opensuse.org/opensuse/en/6/66/Kde4-desktop.jpg"><img border=0 src="http://files.opensuse.org/opensuse/en/5/52/Kde4-desktop-thumb.jpg"/></a></div>

As KDE 4.0 doesnt include KDEPIM (Kontact, KMail, KOrganizer etc.), therefore openSUSE 11.0 includes beta versions of KDEPIM  4.1. These applications work fairly well, and will be updated to final versions via official online update as soon as possible. The online repositories contain many more KDE 4 applications, such as Dragon Player, Okteta, KSystemLog, and Yakuake. Webkitpart is optionally included which makes use of the WebKit part of Qt 4.4. 

<h3>YaST Ported to Qt4</h3>
openSUSE's administration and installation tool, YaST, and SaX2 have <a href="http://news.opensuse.org/2008/06/05/sneak-peeks-at-opensuse-110-new-installer-with-stephan-kulow/">been ported to Qt4</a> for this release. This allowed the YaST developers to use CCS-like Qt stylesheets for the installer, giving it a themed look:

<div align="center"><a href="http://files.opensuse.org/opensuse/en/4/47/OS11.0-inst-6.jpg"><img border=0 src="http://files.opensuse.org/opensuse/en/d/d7/Install-thumb.jpg" /></a> <a href="http://files.opensuse.org/opensuse/en/e/e4/Opensusedvd-install7.png"><img border=0 src="http://news.opensuse.org/wp-content/uploads/2008/06/os110beta2-inst4-thumb.jpg"/></a></div>

YaST is now using Oxygen icons to give it an integrated look in KDE 4.

<div align="center"><a href="http://news.opensuse.org/wp-content/uploads/2008/06/yast4.png"><img border=0 src="http://files.opensuse.org/opensuse/en/2/28/Yast4-thumb.jpg" /></a><a href="http://news.opensuse.org/wp-content/uploads/2008/06/sax2.jpg"><img border=0 src="http://news.opensuse.org/wp-content/uploads/2008/06/sax2-thumb.jpg" /></a></div>

<h3>KDE 4.1, KDE Four Live</h3>

While KDE 4.1 did not manage to make it into openSUSE 11.0, its packages will be available via 1-click-install in the openSUSE Build Service. You can track KDE4's development by using the regularly updated <a href="http://opensuse.org/KDE4">KDE 4 snapshot packages</a>. The openSUSE-based <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CD will be based on openSUSE 11.0 in future releases.


