---
title: "KOffice Sprint 2008"
date:    2008-11-11
authors:
  - "jospoortvliet"
slug:    koffice-sprint-2008
comments:
  - subject: "hmmm"
    date: 2008-11-11
    body: "Ok, I take back the 'notice how they manage to look almost sane' - sebas is in there with his usual \"hey someone is taking a picture of me, let's look like a loonatic\". Meh. Then again, Thomas looks like a real teddybear, so I guess he's compensating. BTW I did manage to shoot a couple of balls in the goal of the tablefootbaltablethingthang - quite something considering my incredibly limited skills.\n\nAnd yesterday I've learned KDE needs more Poka-Yoke solutions."
    author: "jospoortvliet"
  - subject: "Outstanding KWord stuff"
    date: 2008-11-11
    body: "One thought given the lack of table support.  If it doesn't already exist, would it be worth putting together a list of all the ODF functionality currently missing in KWord, and ordering by importance and/or effort to implement.  At least as a list for university projets to get their teeth into?"
    author: "pagaboy"
  - subject: "KOffice the only true Linux Office Tool"
    date: 2008-11-12
    body: "I hope we also see a published API for KOffice. I think other programs could benefit from being able to integrate some of KOffice functionality.  KOffice represents the only true Linux Office solution.  Keep up the good work.\n\n"
    author: "Minty"
  - subject: "Re: KOffice the only true Linux Office Tool"
    date: 2008-11-12
    body: "There are plenty of good true linux office solutions, that you have just insulted.  Or if you are hinting because the others also work on windows and apple they aren;t true then KOffice can't be true either since it is ported to windows.\n\n"
    author: "anon"
  - subject: "Re: KOffice the only true Linux Office Tool"
    date: 2008-11-12
    body: "I do not care for Open Office on Linux or Windows. It smells like Java when you run it, and its just butt ugly. KOffice is a nice fresh approach, something OpenOffice could say 15 years ago when it came out as Star Office.\n\nOh excuse me for insulting Java. (But Java desktop apps really suck)\n  \n\n"
    author: "minty"
  - subject: "Re: KOffice the only true Linux Office Tool"
    date: 2008-11-12
    body: "I try to be nice and constructive..\n..but it's true.\n\nI think java is an ugly hack that only exists as an unfortunate result of Windows' lack of POSIX compliance.\n\nSince I'm always doing odd things with my desktop, like global menu hacks, various system theme setting changes and whatnot, I'm ALWAYS running into the fact that OO.o.. ...well, I just look forward to the KOffice 2.0 release, I'll leave it there."
    author: "ethana2"
  - subject: "Re: KOffice the only true Linux Office Tool"
    date: 2008-11-16
    body: "POSIX is very small and by itself nearly useless standard.  You can't write any serious programs using POSIX alone, certainly not graphical programs.  A great deal of what Java provides is not stuff that is covered by POSIX.  AWT, Swing, the container classes, various utility classes, XML libraries, JavaBeans, etc. are all beyond the scope of POSIX and are definitely not standard across platforms, or even across languages on platforms."
    author: "Joel"
  - subject: "Re: KOffice the only true Linux Office Tool"
    date: 2008-11-16
    body: "I hate Java and the sloth of OpenOffice as much as the next guy, but I must correct you.  It only uses Java for a small subset of the functionality and that can be turned off.  The rest is C++ or glue code in other languages.\n\nWhat makes it so ugly and slow is that they basically wrote their own half-baked POS toolkit which is ugly and slow and doesn't work well or look nice on any platform."
    author: "Joel"
  - subject: "Saving Flake Shapes"
    date: 2008-11-12
    body: "I keep asking, but when can we expect to have the ability to save flake shapes (graphics) within a document?  It's a little strange the KOffice2 is this far along and this basic feature isn't available.  I'd love to put beta2 through it's paces, but I'm not going to create a document with text and graphics only to have the graphics not saved within the document."
    author: "Cirehawk"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-12
    body: "Hehe, this was actually one of the jokes-of-the-day at the meeting - we've got this great suite of applications, it's pretty much ready for the user. Sure, it still can't save properly, but that's a minor bug, let's release!\n\nMore seriously, this is high on the list of things to fix, obviously. But it'll still might take a while."
    author: "jospoortvliet"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-12
    body: "Interesting.  So when you say it still may take a while, surely you aren't saying that KOffice2 might ACTUALLY be released without this ability are you?  I am a strong proponent of KOffice and I use it over OpenOffice whenever I can for my personal needs, but I would not use KOffice2 final if it couldn't save graphics.  I'd just stick with 1.6.3 until KOffice2 is ready.  There's actually a real good reason for my interest (other than personally).  I'm trying to convince my church to switch from MS.  We do our bulletins in MS Publisher, but from the looks of things I can easily do it in KOffice2.  But what I CAN'T do is recreate it every week.  :)  So here's hoping this issue is resolved sooner than later.  And thanks for all your hard work."
    author: "Cirehawk"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-12
    body: "Of course we would never release without proper saving and loading.  Rest assured that ODF support, including shapes, is prio 1 for KOffice 2.0.0.\n\nBtw, you may want to test beta3 that was tagged today and will be released next week."
    author: "Inge Wallin"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-13
    body: "Thanks for the input Inge.  Even though I asked the question, I was pretty confident you guys would NOT release it without proper saving.  It's good to have the assurance that it is priority one.  As I said, I'm a big KOffice fan and use it whenever I can.  I look forward to testing beta3 and will provide feedback."
    author: "Cirehawk"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-12
    body: "It might be a surprise, but I learned in Berlin that it's already possible.\n\nIt's basicly a GUI problem: If you insert a shape in KWord it will be floating, but ODF doesn't support saving of floating shapes. So at the moment you have to make it inline to save it (Insert->Make inline)."
    author: "Sven Langkamp"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-12
    body: "Indeed a surprise.  I'll try this once I get home from work.  Here is a question though.  What do you mean about floating versus inline?  Do you mean floating in the sense that you can't move the shape around on the page (meaning it's locked to the position you insert it at)?  If so, then this isn't a problem with KOffice but rather with the ODF spec.  And it's one I hope ODF corrects soon."
    author: "Cirehawk"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-13
    body: "As far as I know the floating can be done by anchoring (to page). I'm not a KWord developer, so I don't know what exactly are the differences."
    author: "Sven Langkamp"
  - subject: "Re: Saving Flake Shapes"
    date: 2008-11-13
    body: "The proper way to save a shape that is just positioned somewhere random on a page in ODF is to make it anchored to the page with the offset from the page top/left.\n\nApparantly KOffice doesn't do this currently."
    author: "Thomas Zander"
  - subject: "Nice ui concept"
    date: 2008-11-12
    body: "The more I look at the screenshots the higher I have to praise some decisions on the new KOffice ui. \n\nWe use the recent MS Office at the office now. They replaced the pulldown menus by some sort of toolbox, a kind of toolbar that changes its items depending which \"menu\" is to be opened. It is horrible. There is a screen full with buttons (even Copy and Paste buttons are visible) and you simply can not find the functions you want to use. One can not edit these toolbars (remove some not often used icons), because it will mean a loss of functionality and one often can not find the \"advanced actions\" because the button to access them is invisibly small.\n\nThe old concept, all functions be being accessible in the menus and often used functions accessible in the toolbars, was simply replaced by toolbars containing ALL actions. It is very good that KOffice still uses traditional menus with access to all functions and enhances the toolbars by the toolbox->tool properties panels. \n\nThanks!"
    author: "Sebastian"
  - subject: "Templates, Sample Documents and Tutorials"
    date: 2008-11-12
    body: "Since KOffice has a unique way to work things out, one major point on the new Website should be a big and well supported section of Templates, Sample Documents and Tutorials with different grades of complexity and beauty.\n\nso it's not only \"developers, developers, developers\" ;o) \nbut also designers educators, supporters and users..\n\n\n"
    author: "Lassi"
  - subject: "Templates, Sample Documents and Tutorials"
    date: 2008-11-12
    body: "Since KOffice has a unique way to work things out, one major point on the new Website should be a big and well supported section of Templates, Sample Documents and Tutorials with different grades of complexity and beauty.\n\nso it's not only \"developers, developers, developers\" ;o) \nbut also designers, educators, supporters and users..\n\n\n"
    author: "Lassi"
  - subject: "Re: Templates, Sample Documents and Tutorials"
    date: 2008-11-12
    body: "Sure, we need contributions other than code. And ways to share those (GNX to the rescue!). But:\n- who's volunteering?\n- KOffice first might need to actually work :D\n\n\n\nBTW, the quotes on the bottom of the dot are amazing. If and when the dot gets a redesign, we should definitely keep 'em. This was there when I was posting:\n\"I'm holding out for a computer interface that plugs directly into my cerebral cortex.\" -- Sirtaj Singh Kang "
    author: "jospoortvliet"
  - subject: "Re: Templates, Sample Documents and Tutorials"
    date: 2008-11-13
    body: "Indeed. And I can assure you: this is in the planning. :)"
    author: "Alexandra Leisse"
  - subject: "kudos"
    date: 2008-11-12
    body: "my best wishes and kudos for the Koffice and KDE teams (really everyone involved. oxygen and all)\n\ni can't wait for the real experience. (will it be 4.2 named as \"4.2 The Real Expereience\" :P)"
    author: "cga"
  - subject: "great!"
    date: 2008-11-15
    body: "from the screenshot it looks great. Really, I'm so looking forward to the release!\n"
    author: "Thomas"
  - subject: "I agree that some things"
    date: 2009-06-05
    body: "I agree that some things probably could be done differently and to greater effect\r\n\r\n\r\n<a href=\"http://www.onlineeducationfacts.com\">life experience degree</a>"
    author: "fordglen"
---
Time is up, and we are home again. <a href="http://www.kdab.com">KDAB</a> has once again proved to be a great host, and so has the city Berlin. We have had a great and productive weekend. Read on to learn more details about the KOffice 2008 meeting in Berlin.











<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right; width: 350px;">
<a href="http://static.kdenews.org/jr/koffice-sprint/kofficesprint-groupphoto.jpg"><img src="http://static.kdenews.org/jr/koffice-sprint/kofficesprint-groupphoto-wee.jpg" width=" 350" height="204" /></a><br />
The KOffice team. Notice how they manage to look rather sane.<br />
Inge Wallin, Peter Simonsson, Cyrille Berger, Patrick "SaroEngels" Spendrin, Sven Langkamp, Jos Poortvliet, Camilla Boemann, Pierre Ducroquet, Franczesca<br />
Jaros&#322;aw Staniek, Thomas Zander, Alexandra Leisse, Marijn Kruisselbrink, Jan Hambrecht, Sebastian Kügler
</div>

<p>On Friday night, most of the KOffice developers went out for dinner, and returned to the hotel soon after that. With the many plans for Saturday, they stayed off too much beer. As a consequence, almost everybody made the kickoff at 9 in the morning. Inge kickstarted the day with a short talk about the plans and goals for the meeting, and we then split up into two groups. Both went to work immediately. The developers spend most of their time watching the beamer-screen showing a KOffice application and discussing it. They were joined by Ellen Reitmeyer of the KDE usability team (and <a href="http://www.openusability.org">openusability.org</a>). Much time was spend discussing the current docker system and drafting new guidelines to improve the situation. At some point there was a sudden outbreak of laughter and even applause. They finally managed to crash KWord - it took them over two hours. Luckily (or not...) KSpread was much less of a challenge - just starting it and clicking around for a couple of seconds did the trick. Still, it is clear the work on stabilising KOffice is slowly paying off. During the discussions, the coffee grinder (KDAB has an excellent coffee machine) regularly made its creaking noise, making sure the developers stayed on their toes.</p>

<h3>Food</h3>

<p>
But after hours of talking, bug hunting and coding, it was time for food. We split up in smaller groups so we could fit in the small hole-in-the-wall foodplaces they have around here. After food - more work. UI was discussed again and questions were asked. Ranging from 'how can we make this smaller' to 'this looks like crap', the discussion went up and down. Strong disagreement, happy agreement. As users might note, the interfaces of the different KOffice applications are looking more and more similar. According to the developers, the dockers will become even more consistent, but at the same time the different applications will focus more on their core business, thus diverging their user interfaces.</p>

<h3>Marketing</h3>

<p>
Meanwhile, in the marketing room, the marketing team was working on world domination. Under the inspiring leadership of Inge they came up with a sound marketing strategy for KOffice summarised in one sentence, "we should leverage buzzwords and synergize the utilization of neologisms". When this was presented to them, the KOffice developers loved it. Then proceeded to ask why it took almost a full day to come up with that crap. Luckily, the marketing people also did a nice SWOT analysis, prepared the release announcement and discussed several strategic issues. There now is a rather solid communication plan for the 2.0 release, and the KOffice developers liked the work which had been done.</p>

<p>
The marketing talk spawned a discussion about the release date and what would/should be ready. Previously some choices had been made, but the lack of developers in some areas required some reassessment. Among others, it was decided that KSpread, which was supposed to be released with 2.0, did not seem to be ready. Further it was decided the team would focus on a release somewhere in February next year.

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left; width: 350px;">
<a href="http://static.kdenews.org/jr/koffice-sprint/windows1.png"><img src="http://static.kdenews.org/jr/koffice-sprint/windows1-wee.jpg" width="350" height="263" /></a><br />
Disclaimer: it does look better running on a Free platform!
</div>
<p>
<h1>Release</h1>
<p>
Many KOffice developers have never seen their software released to the public. The previous KOffice release was years ago, and it is time the code gets in the hand of some interested users and developers outside the KOffice project. So the goal for the coming months is clear: release. The applications will have a reduced feature set, everything which is not ready will be removed to ensure stability. "No Crashes" is the target, as long as the applications are stable, it is possible to have a decent look at them. The project has been doing rather exciting things, going where no other Office has gone before. The level of integration and general technical excellence is certainly unique. The project feels it is time to present the architecture they developed to the world, with the main goal to create some excitement and attract new developers. Despite its immaturity, the potential of KOffice is huge, and reviewing the software will clearly show this. </p>

<h3>Status</h3>

<p>Talking to developers revealed the status of several of the applications. The many changes in the core of KOfficelibs but also further down the stack, like KDELibs and Qt 4 forced Kexi to rewrite large parts of the application. This means despite the fact the KDE 3 version was very mature and stable, Kexi won't be joining the 2.0 release. Nonetheless, the developers stress that version 1.6.x is still ahead of the competition, at least in the Free Software world. Hopefully they can release together with the rest of the KOffice apps when 2.1 becomes available, and they expect the application to be pretty good by then. Meanwhile, as said before KSpread seems to be too unstable to do a release, and KWord is still lacking table support. As the rest of KWord is relatively stable, this lack of a 'minor feature' will not be a show stopper, KWord will be available in the 2.0 release. Krita, which has became the most powerful Free Software painting application during the 1.x release, faces high expectations. The 2.0 release probably will not fulfil all of them. The application is, despite relative maturity, still rather unstable and it might be necessary to remove some features in preparation for the release.</p>

<p>
KPresenter will be part of the 2.0 release as well. All the big parts are in place, even though many details still need to be worked out and stability is currently lacking. According to the developers, the application will be ready for a basic presentation. Karbon and KPlato seem relatively stable, both being maintained and in a state where they can be released. Karbon even prides itself on having pretty cool new guide snapping. Kivio, on the other hand, probably will not make it. Its maintainers will not have time to get it into a releasable state. For each and every of these applications one thing should be clear: if you feel the application should be released, you can make it so by providing patches and help.<p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right; width: 350px;">
<a href="http://static.kdenews.org/jr/koffice-sprint/inge_is_watching_you.jpg"><img src="http://static.kdenews.org/jr/koffice-sprint/inge_is_watching_you-wee.jpg" width="350" height="233" /></a><br />
Evil Marketing Genius Plotting World Domination
</div>

<p>
In the evening there was good food again courtesy of KDAB (kudos!). We did have an unintended tour the KDAB area of Berlin, as we left the office into the wrong direction. No complaints, though, as the weather was great, and at least those with cool route-planning-gadgets enjoyed testing them. In the end, it was Sebas who noted the 'good looking people are in that direction', which finally got us where we needed to be. Good food, some weird photos, and after another few hours at the KDAB offices it was time for sleep. Obviously not for all, several developers went out for beer.</p>

<h3>Sunday</h3>

<p>
Sunday started early again. Day two began with a talk by Alexandra Leisse from the Marketing team. She had spend a lot of time preparing the future of the KOffice website, and presented a compelling vision. The website should get a stronger focus on the community, probably even leading the way for KDE in this. One idea in this area would be creating a life stream or social stream, a site showing all the stuff that is going on in the KOffice community by connecting blogs, commits, twitter but also Facebook and other web services. The following discussion lead to interesting places. Jaroslaw showed the website he wrote for Kexi some years ago. It is a very professional looking site, yet entirely wiki-based. Some surprised faces showed how unexpected this was. KOffice will probably use this technology in the future.</p>

<h3>The End</h3>

<p>
After the marketing discussion, Thomas Zander started his talk about unit tests, and the team went back to coding and discussions. At 14:00 hours, the first developers started to leave. We had a good time - now it is coming to an end. But many will probably fire up their laptop when they get home, or even during traveling. Yes, there is a lot to do, but many here believe the future is bright. We are having fun creating something the world has never seen before. Our architecture will be able to do things nobody else can - an exciting prospect. So if you need the freedom to be creative, if you want to do things you can not do anywhere else - join us! Now's the time...</p>










