---
title: "KDE Commit-Digest for 13th April 2008"
date:    2008-04-23
authors:
  - "dallen"
slug:    kde-commit-digest-13th-april-2008
comments:
  - subject: "OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "WHY IS IT TEN DAYS LATE DANNY?"
    author: "The James"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Please don't complain. Creating the Commit-digest is very tedious, difficult work that requires a lot of time and effort. If you would really like to see the delay decreased, please volunteer your services. If you aren't going to contribute, then STFU."
    author: "The James"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "I'm glad we got that out of the way."
    author: "The James"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Lol! That was awesome :D"
    author: "ad"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "That was pretty funny."
    author: "James Spencer"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "10/10 :D rotfl, it made my day"
    author: "Anon1234567"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-24
    body: "Brilliant!!!"
    author: "A"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Repeat after me:\n\nDO NOT FEED THE TROLLS\n\n\nPS: I realize this appeal is futile; moderation is needed anyway.\n"
    author: "Mark Kretschmann"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Look at the authors, he did all the trolling and answering by himself ;-)"
    author: "Nik"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Damn, your right. This might be supposed to be some kind of funny joke."
    author: "fhd"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "It is, and people who have the joke gene appreciate it"
    author: "MamiyaOtaru"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "people like you that shout for moderators are that need to be moderated ;-)"
    author: "she"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Not only that, it seems the troll has stolen your name too! D:\n\nAs always, thank you very much Danny. Your work is much appreciated.\n(Note: I did get The James' joke...)"
    author: "Hans"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-24
    body: "Wow! I didn't get it until I heard the other's laughter. I guess I need to learn to check the names ;).\n\nNow that I noticed, rotfl. Great joke.\n"
    author: "Riddle"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "Let's see, one guy + 2,245 emails from the commit logs...can't see a possible reason a guy could be a little late on a completely unnecessary and unrewarding task.\n\n</sarcasm>\n\nThanks for another great digest, Danny."
    author: "a monkey"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "What a fucking moron... don't worry Danny, you're great. Thanks for the digest, always a highlight of the day!"
    author: "MarcG"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "I love how basically everyone in this thread has completely missed The James's joke."
    author: "Anon"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-23
    body: "I got the joke ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: OMG!!! SO LATE!!!"
    date: 2008-04-24
    body: "I think you should be 10 days early next time.  Tomorrow release a digest for April 33rd.  Just predict what you think might be committed in the future.\n\n\"New patch submitted for Kontact, changing all entries for Aaron Sergio to read Tom Green.\""
    author: "T. J. Brumfield"
  - subject: "Why do we have to go through this every week???!!!"
    date: 2008-04-24
    body: "Like somebody said last week.\n\nWhey do we have to go through this every week?\n\nDanny volunteers his time and \"it's ready when it's ready\"!!!!!\n\n\nThanks Danny for doing these.\n"
    author: "D"
  - subject: "Re: Why do we have to - Thanks Danny!!!"
    date: 2008-04-24
    body: "Just got the joke..\n\nI'm slow this morning..\n\nGreat digest Danny!!!!\n\n"
    author: "D"
  - subject: "Thank you, Danny!"
    date: 2008-04-23
    body: "Yet another great digest. :) You are doing wonderful work!\n\nThank you!\n"
    author: "I love KDE"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-23
    body: "+ 1"
    author: "mimoune djouallah"
  - subject: "Re: Thank you, Danny!"
    date: 2008-04-24
    body: "+91455431065267592305743250342765290856\n265295175144675232575756995734543541532\n676575975754756476599943564254354654365\n513577905454267786653123459307679605543\n143215865754354325324593406576034154350\n436415415236547534321421565475930099576\n23524353465477\n"
    author: "Riddle"
  - subject: "No mention of Dolphin Tabs?"
    date: 2008-04-23
    body: "Hi Danny,\n\nThanks for the Digest - highlight of my week, as always :)\n\nI don't see any mention of this bombshell, though, which I think made it in time for the cut-off date and which will interest a lot of people:\n\nhttp://websvn.kde.org/?view=rev&revision=796101\n\nHope I haven't scooped something intended for next week's digest ;)"
    author: "Anon Reloaded"
  - subject: "Re: No mention of Dolphin Tabs?"
    date: 2008-04-23
    body: "Ah - I've just found it in the article body - never mind :)"
    author: "Anon Reloaded"
  - subject: "Re: No mention of Dolphin Tabs?"
    date: 2008-04-23
    body: "Yes! At last! Tabs in Dolphin, making it usable for me! Huzzah!"
    author: "Barsanuphe"
  - subject: "Re: No mention of Dolphin Tabs?"
    date: 2008-04-23
    body: "meet the new boss.."
    author: "MamiyaOtaru"
  - subject: "Re: No mention of Dolphin Tabs?"
    date: 2008-04-24
    body: "I think it would look better if the tabs were under the address bar. Also, is it consistent with other apps? I seem to recall Konqueror having a single \"close current tab\" button instead of individual ones for each tab."
    author: "yman"
  - subject: "Re: No mention of Dolphin Tabs?"
    date: 2008-04-24
    body: "Dolphin Tabs rockz!"
    author: "TeeZee"
  - subject: "Strange"
    date: 2008-04-23
    body: "Ozone is a great development. That was a thing I always disliked about Oxygen that you could hardly make out which window is active and which is not. Always thought it was a bug. Didnt know that such a fierce debate has been going on about this. Great that a solution has been found for us non-fundamentalist users."
    author: "Michael"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "And think that a thin rule visible only when the window is active and which could have glowed in the user-selected colour could have done the trick (\u00e0-la modern system).\n\nI have ambivalent feelings about that one, on the one hand, the oxygen artists are right in that their version is more beautiful; on the other hand, Lun\u00e1k is right in that his version is more practical.\n\nOh, well, some genius will come up with an optimal solution, I guess. At some point.\n\nSomething like the attached screenshot, but less ugly.\n\n"
    author: "hmmm"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "forgot to attach"
    author: "hmmm"
  - subject: "Re: Strange"
    date: 2008-04-25
    body: "HOLY SHIT! That is ugly as hell."
    author: "blah"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "I would be ok with a lot of changes, as long as the (one slab thing) would not be corropted. But this was about a patch to make the windeco be colorable that would totaly brake that. That is an ok feature but iw not oxygen, its brakes the frist guidlines in oxygen theme. "
    author: "nuno pinheiro"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "There is no need to make the whole windowdecoration colorable, the titlebar is enough. You can make it a nice gradient from the selected colour to the windowcolour, making a smooth transition. This would both be efficent and solve two major bugs, and it would look nice.\n\nIf the oxygen guidelines will not allow this, it's obviously flawed and need to be fixed. When the spesification don't work in the real world, you have to change it. It's a no brainer, real world always thrumps spesifications and visions.\n\n"
    author: "Morty"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "Show me a mock that visuialy works and you have a believer :)"
    author: "nuno pinheiro"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "Just take a glance at Enlightenment e17 . There is a wonderful animation, i.e. a bright flash appearing from time to time on the currently active title bar. This is just NEAT! Means: I'd love to see this in KDE as well..."
    author: "Sebastian"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "The effect I'm thinking about is like when you dip the edge of a paper in paint, and it soaks up the paint. \n\nI have minimal skill with drawing tools, so this only shows the principle of how it will look with enabled titlebarcolour, obviously it needs tweaking. "
    author: "Morty"
  - subject: "Re: Strange"
    date: 2008-04-24
    body: "What about a glow similar to the focus glow of buttons and other areas in the Oxygen theme? It could fit in well, since similar glows are used throughout Oxygen."
    author: "David B."
  - subject: "An idea of which i don't know the consequences"
    date: 2008-04-23
    body: "When a window goes to the background, how about removing 50% of the colour - similar effect when you log out and the desktop goes black and white."
    author: "Ian"
  - subject: "Re: An idea of which i don't know the consequences"
    date: 2008-04-23
    body: "this is really the type of difrence i would like to see, but its seeams its not very good and couses fliquering."
    author: "nuno pinheiro"
  - subject: "Re: An idea of which i don't know the consequences"
    date: 2008-04-24
    body: "Mm I think is a great idea though. Perhaps this is one for the developers to stop moaning about and fix the flickering? :)"
    author: "NabLa"
  - subject: "Re: An idea of which i don't know the consequences"
    date: 2008-04-23
    body: "There's the composite effect \"darken inactive\" that does pretty much the same. 20% works very well for me, and I find it ergonomically and aesthetically more pleasing than a coloured titlebar."
    author: "Flitcraft"
  - subject: "Re: An idea of which i don't know the consequences"
    date: 2008-04-24
    body: "Yeah, but composite ain't enabled by default, and Oxygen needs to be able to work without it.\n\nBesides, composite performance at the moment is not optimal"
    author: "NabLa"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "Agreed :)"
    author: "Lee"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "For one the name Ozone makes me smile. But than I think Ozone looks pretty ugly, pardon me. Still I also need more help with Oxygen to find the active window or recognize that one is passive. What about a coloured outline as indicator, like all focussed widgets have? "
    author: "Frinring"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "I'm ok with changes, and I posted quite a few of possible ones in my blog and we are working on it, If some one shows me a colorable windeco that looks nice and oxygen I would be glad to make that into the oxygen windeco. The fact remains that I dont belive such an visual anser exists (and I have tried several variation).\nAt the time the intire discucion was about submiting a patch that made and makes the intire windeco a difrent color. Again that is an ok feature but its not oxygen.  "
    author: "nuno pinheiro"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "I really really like your suggestion to color the signs in the buttons of active windows:\n\nhttp://www.nuno-icons.com/images/estilo/nocoloringmoreoptions.png\n\nKeep up your great artwork!"
    author: "Goodjob!"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "We are thinking on redoing the butons all toguether, dont think they fit kite well on the grand look. "
    author: "nuno pinheiro"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "Your artwork is great and you are the creators of oxygen, meaning you guys have to make the decisions.\n\nDon't listen to the anal people, i work with oxygen for some time mow and I had no problems with active/inactive windows.\n\nKeep it up, you rule!"
    author: "MarcG"
  - subject: "Re: Strange"
    date: 2008-04-25
    body: "Who's being anal?! Nuno's responses are like Volvo telling me I can't change the front grill on my car because, \"we think it should look the way we sold it.\" Sorry, but it's _my_ car, not Nun^WVolvo's."
    author: "Anonymoose"
  - subject: "Re: Strange"
    date: 2008-04-25
    body: "Well, that's the thing... you BOUGHT the Volvo, and Oxygen is GIVEN to you. Contribute or accept it as it is. Sorry, totally anal ;)"
    author: "MarcG"
  - subject: "Re: Strange"
    date: 2008-04-25
    body: "Err... Contribution has been submitted, and it has been accepted by the KWin maintainer, though the name had to be changed due to Nuno not  being willing to be associated with the unauthorized change.\n\nSo people can have the Volvo as shipped it, or, if they so prefer, they will be able to change the colour of the bumpers.\n"
    author: "Luciano"
  - subject: "Re: Strange"
    date: 2008-04-24
    body: "Sounds great, it's probably the thing I dislike most with Oxygen. I've tried to make own buttons for the windeco, but it's really hard to come up something that looks good."
    author: "Hans"
  - subject: "Re: Strange"
    date: 2008-04-25
    body: "god idea. the round buttons seem out of place.\n\nand btw, whoever came up with the default placement of the buttons (no oxygen-guy, i've heard) should be shot."
    author: "blah"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "> ... you could hardly make out which window is active and which is not....\n\nI agree, this is a problem with Oxygen.\n\nApart of that, I like and prefer the fact that the titlebar has the same colour than the rest of the window. It looks now finally like the window and the application are \"the same\", and not an ugly window wrapped around a naked application.\n\nThere were some nice mockups with faded-out titlebar-buttons which helped to distinguish active and non-active windows. I hope they will be considered."
    author: "Yves"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "Yes I'm willing to try lots of things. And we are trying them, but the bug was, oxygen dosent follow the color setings for kwin. Not \"I cant tell active from inactive apart\".\nTo the frist bug we can and are trying to make it beter, to the second one im not sure it will work and the patch really does not work, at least for the oxygn team.    "
    author: "nuno pinheiro"
  - subject: "Re: Strange"
    date: 2008-04-23
    body: "Ups got the frist and second mixed up :)"
    author: "nuno pinheiro"
  - subject: "system settings -> change frame color?"
    date: 2008-04-23
    body: "On OpenSuse there is the option to colorize the frame of the active window. Is this a special opensuse port? I just do not understand the matter in creating the new deco.\n\nPersonally, I agree with the Oxygen team: As long as desktop effects are enabled, the active window may be recognized easily - and it looks more consistent! Perfectly consistent."
    author: "Sebastian"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "The problem, you see, is that you are assuming desktop effects can and will be enabled.\n\nThis is not the case for many common configurations, and the simple fact is that the _default_ configuration should not rely on that to be usable.\n\nThe default theme should be the one that works well-enough everywhere; if it cannot be extra-smooth, so be it.\n\nClearly, Ozone is not the smoothest implementation of window border/title recoloration; but it's a starting point and it can be improved. \n\nI would hope the situation could clear up, and that we could still end up with a single O_n decoration that could respect the user preferences with regards to color and border size, and still look smooth. "
    author: "Luciano"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "Or how about two similar themes, one optimized for compositing effects, and one for the lack-of?"
    author: "yman"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "Well ben asking that 2 for some time now. there area alot of pesqui flaws in the curent oxygen windeco that can only be fixed via compositing.\n\nBut this is not the question here, and certanly not the anser to this problem "
    author: "nuno pinheiro"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-24
    body: "regardless, I for one would be disappointed if any change what so ever is made to th Oxygen theme."
    author: "yman"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "For crying out loud, let them do their thing and play around with the more subtle solutions Nuno spoke about. \n\nThey did a great Job with oxygen and that's mainly owed to their artistic judgement. If someone else wants to friggin paint the window decoration in some random color, that's his right and proves the absence of style at the same point, but that doesn't change the fact that the oxygen artists have the right to adress the issue in their own way.\n\nAnd I challenge your assumption that the active/inactive thing is that big of a problem alltogether. It isn't for me."
    author: "MarcG"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "Don't forget that for many people being practical is more important than looking artistically good. And that if the default theme completely ignores them, many of them will choose an other DE (the same way as if the default theme completely ignored beauty)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: ">>Don't forget that for many people being practical is more important than looking artistically good.\n\nFirst I challenge that notion too. It may be more important for some, but no way for many. A bunch of people prefer cool looking over practical with ease. Look at sports cars, ipods and fake boobs.\n\nSecond thing is, being practical in most cases, the oxygen theme does in no way \"completely ignore\" the user. As i said, it works for me. And without all this hysteria going on, a good number of users wouldn't even know that immediately knowing what window is active is that important for them."
    author: "MarcG"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "I still think that, while there are many people who prefer looking cool (I didn't say the contrary) but there are many (who use the computer for productive work) who value practical aspects.\n\nNot knowing which window is active, or even the idea of the active window leads to confusion to the users, especially with modal dialogs. They open a config dialog and inside it they open a sub-configuration dialog which is modal. They do some settings therend click on OK in the main dialog because they don't realize that first they have to close the sub-dialog. I have seen this multiple times even with clearly different window decorations. Similarly if the user starts typing but the focus is on the wrong window, the result is unexpected to the user."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-24
    body: "\"A bunch of people prefer cool looking over practical with ease. Look at sports cars, ipods and fake boobs.\"\n\n\nLol! This quote is an instant classic!"
    author: "Congrats!"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-24
    body: "heheheh yeah :)"
    author: "Nuno Pinheiro"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-23
    body: "I think that is an openSUSE patch."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: system settings -> change frame color?"
    date: 2008-04-24
    body: "Thanks. I didn't understand the excitement about Ozone since I already got what Ozone was meant for. Personally, I love the OpenSuse solution. Whenever I must turn off compositing I change this checkbox (though it seems awkward why one has to use two different windecos instead of one single with an additional option). \nThe only thing I do not like about the OpenSuse option ( I do not know Ozone, but assume it is equivalent) is that the background gradient is not adopted to the blue active window frame. "
    author: "Sebastian"
  - subject: "Thumbs up!"
    date: 2008-04-23
    body: "Thumbs up for the developers! What a lot of changes! :-)"
    author: "Diederik van der Boor"
  - subject: "konsole size?"
    date: 2008-04-23
    body: "Does anyone know if the regression in konsole regarding the ability to set the columns and rows has been fixed?\n\nSettings->Size->80x24 was probably the menu option I used most often in Konsole 3.x.  Hope it's coming back."
    author: "Lee"
  - subject: "Re: konsole size?"
    date: 2008-04-23
    body: "Under KDE 4.0.3 and in trunk Konsole displays the size of the display in rows and columns as it is resized and of course when you close and re-open the terminal it remembers the last window size."
    author: "Robert Knight"
  - subject: "Re: konsole size?"
    date: 2008-04-23
    body: "Hi Robert,\n\nI'm intrigued - what is happening exactly in your port-to-mono branch? I assume it's not what I think it is ;)\n\nCheers!"
    author: "Anon"
  - subject: "Re: konsole size?"
    date: 2008-04-24
    body: "> of course when you close and re-open the terminal it remembers \n> the last window size.\n\nSince we have different profiles now; I still think this is broken behavior. Its the one thing that makes me stick to kde3's konsole.\nIf anyone else has thoughts about that; http://bugs.kde.org/show_bug.cgi?id=152385"
    author: "Thomas Zander"
  - subject: "Re: konsole size?"
    date: 2008-04-26
    body: "... and if you're on 4.0.3, you'll want this patch (backported from trunk) to get an 80-column width by default:\nhttp://cvs.fedoraproject.org/viewcvs/rpms/kdebase/F-9/kdebase-4.0.3-kde%23160422.patch?rev=1.2&view=markup\n\nSee also:\nhttp://bugs.kde.org/show_bug.cgi?id=160422\nhttps://bugzilla.redhat.com/show_bug.cgi?id=439638\n\n@Robert Knight: Any chance this can be backported to 4.0?"
    author: "Kevin Kofler"
  - subject: "Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "I am a bit disappointed in what I perceive as a very childish response to this bug from KDE artists. Specially response #31:\nhttp://bugs.kde.org/show_bug.cgi?id=152030#c31\n\nThey are clearly wrong. The lack of contrast is a huge issue. It does look pretty but it ain't practical. A computer is not something to stare to IMO. If you can make it pretty while being usable, that's brilliant, but if it starts getting in the way... well, it kind of defeats the whole point, doesn't it?\n\nI don't understand why they are being so anal about it. As far as I can see, the only weak point in the oxygen bunch of styles is the windeco. The icons are amazing, and I cannot imagine how much work they took not only to be consistent, but also to be recognisable at lower sizes. The widget theme is simply stunning too. With the right font just looks really polished, both artistic & professional.\n\nMy other issue with the windeco is that the buttons are a bit on the small side, but it's not much of a biggy for me. I for one will probably end up using Ozone."
    author: "NabLa"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "quite a shame. i've tried live kde4 cds now and then. the lack of clear indication of the active window was very annoying and i considered that a bug that will be sorted out later.\nwell, ozone it will be for me, thanks to lubos for realising how important the issue is :)"
    author: "richlv"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "It's a bit problematic, using default widget color for the windeco sure is gorgeous, but it is less usable to distinguish the active window. Compositing can overcome this, though, but some people simply don't have or don't want that compositing.\n\nThe sad thing is the oxygen devs do not allow it to be configurable (merely one checkbox). They can make the color setting default to what they wish, but offering one checkbox doesn't hurt much, does it?\n\n"
    author: "AC"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "No, it doesn't hurt at all... that's what the patch does anyway. Just gives you the choice of the windeco actually following configured colours on kcm.\n\nI guess that all defaults have to go together on a coherent way. For example, window shadows help in this situation. As does the ADD plugin by dimming inactive windows. However all this is disabled by default, therefore Oxygen should ship with defaults that honour this.\n\nI do understand the artists holding to their initial concepts, as they are not easy to come up with, and one takes pride on their hard work. But once you put them in the real world weaknesses are just going to show up when people starts using it. This is good, allows you to evolve whatever needs evolving and ultimately leads to a better result; it's natural too, you just can't get everything right when first outlining a spec. There is a weakness here, well, evolve, look for a solution. \n\nThat was the whole point of releasing KDE4 so early wasn't it? Shedloads of feedback on real usage later, KDE4.1 is taking shape nicely from what I can read and try. Many things needed to be improved and are being improved."
    author: "NabLa"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "Alot of work was puten into oxygen to make sure it work in almots any color variations, this patch simply makes that not work.\nAgain showme somthing that visuly work's wen making the windeco colorable and you have a beliver.\nSomtimes things are just as simple, we can't have it all, a theme that looks good in alot of color variations and still look good with difrent windeco colors.\n\nRemember the ground fundament of oxygen is one coerent slab.\nAnd again we are more than willing to test new ideas to make that distinction more obvius. "
    author: "nuno pinheiro"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "I do understand your position, Nuno. However the patch changes nothing and just adds the possibility of some further customisation. And indeed Oxygen as it is it visually works, but functionally it certainly doesn't. Visual appeal is very important, but usability has to be put first.\n\nI cannot see any way of making active windows stand out apart from changing colours. Not just add a subtle thing. It really needs to stand out. Please do not take it personally, but there is a possibility that that the coherent slab thing is conceptually flawed and not suitable for this particular task. If changing the windeco colour requires the rest of the window to change colour too, and at the moment is not technically feasible for whatever reason, perhaps you should concede on this patch until it can be done."
    author: "NabLa"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "I'm not saying the we are 100% correct just that we dont feel that coloring is the anser. Btw nor did the usability people we talked to.\nIn all true honesty I blive that conceding to this patch is the easy route to have a ever lasting poor anser to the problem.\nIts a complicated problem sure but, this is not the anser. in oxygen style at least.\nThe fact is that ozone thoes not realy look like it belongs there so a difrent name is cool, like using plastic windeco is cool 2, or any other colorable windeco.\n"
    author: "nuno pinheiro"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "Don't forget one thing Nuno:\nThere is an option in kcontrol/systemsettings in colors to change the titlebar color.\nNot having this in oxygen is pretty confusing to new users that do not know oxygen window decoration WILL NOT obey the configuration properly.\nUnder this point of view, this is a real bug, and must be solved ASAP for KDE 4.1.\nIf you do not like the color solution for windeco, I belive you will have to rethink not only oxygen windeco, but all colors settings altogether. :-P\n\nSad, but true, this is very bad indeed :-P\nFor now, I would just accept the patch and start working on a better solution, in the meantime we can't kill the user in a pool of confusing options that do not work :)\nInconsistency IS BAD!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "Yes that might be the problem, oxygen and coloring the windeco seems a bit incompatible.\nLooks inconsistent. and has you said Inconsistency IS BAD!"
    author: "nuno pinheiro"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "This really isn't a reason to not add the feature. I can change my Plastik windows to have hot pink for the widget colours, lime green for the window decoration and brown for the text. How are you going to stop the user from making things looking horrible? Should we disallow colour changes everywhere? Should we add some code to detect tasteful colour schemes? It's not your job to stop the user from making things look the way they want them to look. Your taste in widget/window style is completely subjective; you should not be imposing it on people."
    author: "Bob"
  - subject: "Let them decide"
    date: 2008-04-23
    body: ">>Your taste in widget/window style is completely subjective; you should not be imposing it on people.\n\nThis is bullcrap. It's oxygen, and it'sOSS, and that means: The ones doing the work are to make the decisions, this is no democracy after all. \n\nSo Nuno and the oxygen artists are right to make their very own decisions, and i admire them for sticking to their artistic look on things, not taking the easy (and garbish looking) way out. And who reads Nuno's blog knows that they are on to things and looking for a solution that doesn't seem like some sort of retarded third grader just painted the decoration of a window in some random color.\n\nOne last thing, the usability is NOT as bad as some say it is, I never had problems working productivly with kde 4 and oxygen for some time now. "
    author: "MarcG"
  - subject: "Re: Let them decide"
    date: 2008-04-23
    body: "Not enabling the user to do what they want (when technically it is possible) only makes users to use other software, or, as in this case (luckily this is OSS) in forks and patches. Developers can make decisions and not allow people change the settings but other developers can patch it and people will use the patched version.\n\nYou and many people find the usability of the style good, many others find it bad. Why do you want those people to use that you find good? You would surely be angry also if the developers forced you to use some settings that are bad for you, wouldn't you."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Let them decide"
    date: 2008-04-23
    body: "Again we are ok with the patch its an ok feature, but if we have some sort off respect for the visual holle of oxyen we can't alow it to be oxygen, its somthing else, it my heven be ok but to us its not oxygen. "
    author: "nuno pinheiro"
  - subject: "Re: Let them decide"
    date: 2008-04-29
    body: ">The ones doing the work are to make the decisions, this is no democracy after \n>all. \n\n-1e+99\n\nThis is a DeskTop.  It MUST be based on standards.  You are saying that it is up to the coders to decide if they want to follow the standards for KDE4.  This is totally unacceptable.\n\nBy similar logic to what others have stated: if they don't want to follow the KDE standards, that is OK, but it won't be KDE."
    author: "JRT"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "Alot of work was puten into oxygen so that you can't do that heven if you want to.\nLet me explain beter, in other themes making color sckeams is a bit triky and dificult its easy to get them all lokking bad. In oxygen its rather easy to make good loking ones, and as a mater of fact it works nicely enough in almost any color variation.\n "
    author: "nuno pinheiro"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "Good to see we agree in most parts of the problem :)\nWhat about a tone of color?\nI mean, make the color much like white, just with a bit of the user chosen color, as if you just add 90% of bright (just look at attached image).\nIt does not look *that* bad in comparison with using the full-color tone."
    author: "Iuri Fiedoruk"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "Come on, this looks awful. It's wrecking the whole style, I may be able to notice the active window a split second earlier, but at the same time my skin crawls. Not a good deal imo.\n\nOn Nuno's blog one can see some less brutal propositions, and they will eventually do the trick."
    author: "MarcG"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: ":D not a good deal indeed!"
    author: "Tomas"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: ">Come on, this looks awful\nAnd a lot of people think the white title looks awful, this is why the patch proposed makes it a OPTION.\nIf you think it looks bad, I on contraire mon ami, think it's great.\nSo instead of fighting each other tastes, the idea is to find a common ground, and yes, if you do not remove the option for the user to choose the color of window title from systemsettings, it must be used somewhere in the title of active windows. But how is another question that neither me, neither Nuno (as he stated) have a answer yet.\n\nSo stop bashing suggestions and work on a nice solution altogether, ok? ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-24
    body: ">>So stop bashing suggestions and work on a nice solution altogether, ok? ;)\n\nThat's exactly the point that is bugging me so much. Nuno and the oxygen artists are willing to try a lot of things as Nuno's recent blog posts show. As is said before, they are the artists, they have the expertise and the right to decide what's best for oxygen. And they specifically asked\nfor suggestions, too.\n\nBut instead of giving them some time to come up with an awesome solution inside a modern style frame, someone decided to make a crippled version the DEFAULT for kwin, as i understand. This is outrageous, as well as Nuno wouldn't be allowed to dictate patches to kwin (e.g. to fix the awful performance on nvidia cards) they shouldn't be allowed to dictate a crippled style. This should be common sense."
    author: "MarcG"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-24
    body: "> Nuno and the oxygen artists are willing to try a lot of things as \n> Nuno's recent blog posts show. \n\nA lot of things, but not a profen working one.  In other words, they want every KDE user to use a known bad solution just because their preferred solution is technically not possible.  It looks like they just want everyone to suffer so people will fix the technical problems.\nI for one dislike being used like that."
    author: "AC2"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-24
    body: "As far as I understand a decision had to be made because KDE4.1 is entering feature freeze, and the discussion has been going on for months without reaching any results."
    author: "NabLa"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-24
    body: "Cosmetic things can be changed all the way to kde 4.1. Feature freeze's not a valid reason, since it concerns FEATURES, not artwork."
    author: "MarcG"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-24
    body: "But this contains a feature and even a string to translate, so it has to go in now. Besides the problems has been known for over 6 months, and no \"cosmetic\" changes has been made or even proposed to fix it."
    author: "Morty"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-25
    body: "If you say so... but from the past we know that feature freeze doesn't necessarily comprehend artwork and the 'looks' in general... look at the developement of kde 4.0, plasma related things where changed up to the very end. Same will go for the style, it isn't critical to stability and such.\n\nI feel someone wants to simply create pressure and move ozone in as default. I give you that, it might be good to have shaken up the oxygen guys. Nuno's working at it, and made another blog entry with a new proposition. I have a strong feeling that they will come up with something working AND sufficiently polished right in time now.\n\nBut sticking to ozone as default for 4.1 would be a crime and a shame for the whole \"awesome first impression\" thing that everybody was speaking about before 4.0. So look at it this way: pressure was created, the weel is spinning again, and an improved oxygen will be there in time."
    author: "MarcG"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-25
    body: "\"f you say so... but from the past we know that feature freeze doesn't necessarily comprehend artwork and the 'looks' in general... look at the developement of kde 4.0, plasma related things where changed up to the very end.\"\n\nTrue, but I think we can all agree that KDE4.0 and Plasma were a very special case.  I wouldn't depend on this happening ever again, necessarily."
    author: "Anon"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-25
    body: "This is nonsens. There are NO difference between Ozone and Oxygen, except that Ozone has the OPTION to make the windowdecoration use the windowdecoration colour.\n\nAnd if it's the thing needed to shake up the oxygen guys to find a solution, it's about time. They have had over 6 months to come up with something, and still none of the proposals have been close to solve the issues. The last one in Nuno's blog included, it even introduces other problems. \n\nOzone on the other hand has working and very simple solution, solving not only one but two bugs in Oxygen. And it would require minimal tweaking to make Ozone look just awesome with the option enabled as without."
    author: "Morty"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-25
    body: "Non monsieur, it will not only have the option... it will have the option turned on and therefore wrecking the overall style. Awesome first impression is one of the stated goals of KDE 4, so I'd consider that a bug.\n\nSo ozone might solve the active/inactive bug, but it introduces another one. That is not a good deal.\n\nAnd Nuno is going after a solution that doesn't kill the awesome first impression goal - he's asking for input and ideas on that. So i suggest you help him by doing that. \n\nIf on the other hand you think this can alone and only be solved by painting the window decoration and wrecking the style, maybe you should consider being quiet on this instead, because that would be pretty small minded. But as I'm sure that's not who you are, i suggest you go to his blog and do a constructive comment."
    author: "MarcG"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-25
    body: "Well mister, then it's actually about agreeing on a sensible default on the option. And in this regard KWins maintainer is very reasonable, so it's a minimal problem. Even if he is a little grumpy after being forced to crate Ozone on count of the oxygen developers not accepting the option in the first place.\n\nThe Ozone solution solves two bugs, not one. But I agree it's a little heavy handed with the coloring, and need a little tuning.\n\nThe Oxygen team has got lots of input and good solutions already, many only requiring minor adjustments to look great.\n\nI don't think the only solution to the problem of the active/inactive bug are by painting the windowdecoration. But so far it's the only thing, except when using compositing effects, that solves it.\n \nAnd the second issue, people want to have the option to put colour on their window decoration. And it's pretty small minded not to give people that option, since it's entirely possible to do this and still having the style look awesome and not wrecking anything. For the users it's irrelevant if it's according to the so called \"vision\" of Oxygen or not, as long as they think it looks good and are usable."
    author: "Morty"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-26
    body: "Morty sory coloring the windeco simply  dosent visuly work, and belive me I have tried alot. Do you think i would be anal about it if I havent tried.\nThat the reason my position right now is \"Show my one that works and you have a belever\", Until now all of the ones i have seen are as bad as puting windec plastic, this meens not bad but clearly not a part of the window.\nEnce Ozone not oxygen.\n\nI meen i think its prety obvius, if oxygen is all a bout a coerent slab and then somthing comes out that does the oposite it may be alot of things but its not oxygen right?\n\nSo some one might say ... but then its oxygen that needs to be rethought, but then again if that is the case it wont be oxygen anymore, will it?   "
    author: "nuno pinheiro"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-28
    body: "Coloring of the window decoration works very well, and it can even be implemented in many ways. Giving the artist many way to achieve a good looking solution. Besides it's only a windowdecoration, and it's supposed to be interchangeable. If the user feels that using the oxygen style with the Keramik,  Platsik or one of the other windowdecorations, he is correct and the style is still Oxygen.\n\nIf it breaks the whole coherent slab vision it does not matter, it's how the user views it thats important. And if the user wants the Oxygen window decoration in blue/green/purple it will still be Oxygen to him, regardles if it's a coherent slab or not. Like all art when it's shown, it's all about the experience the user, not the artist.\n\nThe hard fact is that Oxygen will stay Oxygen regardless of any changes made, as long as it stays in KDE as Oxygen."
    author: "Morty"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-24
    body: "This is indeed gross to say the least."
    author: "Congrats!"
  - subject: "Re: Bug #152030 - oxigen windeco colours"
    date: 2008-04-23
    body: "What about a glow similar to the focus glow of buttons and other areas in the Oxygen theme? It would fit in rather well and not be all that obstructive."
    author: "David B."
  - subject: "ozone?"
    date: 2008-04-23
    body: "When we speak of ozone we speak of:\nhttp://www.kde-look.org/content/show.php?content=39826\n\n:-D\n\na fork isn't a good solution. \nmaybe they could make a colour line (option colour line) under the decoration window... and everybody would be satisfied  "
    author: "Autumn Autist"
  - subject: "Re: ozone?"
    date: 2008-04-23
    body: "That's a very nice mockup you link to."
    author: "NabLa"
  - subject: "Re: ozone?"
    date: 2008-04-23
    body: "yeah! :D"
    author: "Autumn Autist"
  - subject: "Re: ozone?"
    date: 2008-04-23
    body: "openSUSE has patched Oxygen so that it has a (default) setting that the windeco does not blend with the window content. It's OK I think."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: ozone?"
    date: 2008-04-24
    body: "Why is a fork not a good solution?\n\nIts the traditional way things are done in open source.  Lots of options for users and lots of people can give input (using code).\nThe users eventually decide which one will get the largest userbase and the rest of the forks will fade away.\n\nIts very much like evolution, only on a much faster scale ;)"
    author: "Thomas Zander"
  - subject: "Re: ozone?"
    date: 2008-04-24
    body: "Visual consistency suffers for instance... have you seen the suse patch? Is like kde 3..\n\nSo must work trying to make a step forward, the revolution that would be kde4 and then, some dev (because they can fork) ruin a visual work, a design, the face of kde4... and transform it back to kde3. Because within oxygen there's the chance of making active/inactive difference visible without transforming the style in something like kde 3.\n\nBy the other hand make a fork put it on kde-look, with the crowd, with natural selection, but this is not the case is it?\n\nWhy use another's idea if you don't like it? How/who will be maintaining the ozone visually, the dev?\n\n  "
    author: "Autumn Autist"
  - subject: "Re: ozone?"
    date: 2008-04-25
    body: "Why not, if it's *almost* what you want, but not quite?\n\nThat's the entire point of open source, you can have things *exactly* your way."
    author: "Luciano"
  - subject: "nice but..."
    date: 2008-04-23
    body: "All this is quite nice but actually I'm a bit concerned to what happened to\nPhonon.\n\nI mean, Qt got it's development and that should be good, but actually\n(qt-4.4.0-rc1) not only gstreamer is the default backend, it's\neven needed just to compile the qt version of phonon and seems that\nthis is not a temporary solution ( http://doc.trolltech.com/4.4rc1/phonon-overview.html#backends ) .....\n\nThis smells like Microsoft way: \"you can add wathever backend you want\nprovided that you have installed our before\" (not much people wants 2\nmultimedia backends, then).\n\nCan anybody confirm/deny that Gstreamer will not be an option??????"
    author: "Nae"
  - subject: "Re: nice but..."
    date: 2008-04-23
    body: "If you implement another backend and write a patch to enable that backend instead of gstreamer, I'm sure you could get that accepted."
    author: "John Tapsell"
  - subject: "Re: nice but..."
    date: 2008-04-23
    body: "ehrr.... there were xine, mplayer and vlc backends last time I checked,\ndid them died??? I'd just like to know what happened,anyone has better info?"
    author: "Nae"
  - subject: "Re: nice but..."
    date: 2008-04-23
    body: "> Can anybody confirm/deny that Gstreamer will not be an option??????\n\nNote that you do not need to get your Phonon from Qt: The canonical, upstream source for Phonon remains KDE. Qt happens to ship a copy, but should work with KDE's Phonon as well, as does KDE anyway, of course. Right now, your backend choices are xine, GStreamer, VLC and soon MPlayer on Linux. None of them are required to build Phonon itself."
    author: "Eike Hein"
  - subject: "Re: nice but..."
    date: 2008-04-24
    body: "Thank you very much, that explains it."
    author: "Nae"
  - subject: "Re: nice but..."
    date: 2008-04-23
    body: "Unlike Microsoft, we have distros between us and upstream sources like Trolltech.\n\nSo this is a total non-issue."
    author: "Ian Monroe"
  - subject: "Sugestion"
    date: 2008-04-23
    body: "Both k3b and kopete are two of the most used KDE apps and also have a lot of commits each week.\nCan you please give them a little of attention?\nI mean, I just want to know what's new in those programs lately :)\n\nThanks!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Sugestion"
    date: 2008-04-23
    body: "K3b and especially Kopete haven't had many commits at all in the last few weeks. But I do plan to contact them anyway for a status update.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Sugestion"
    date: 2008-04-23
    body: "I tought \"buzz\" was number os commits :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Sugestion"
    date: 2008-04-24
    body: "Cheers dude :)"
    author: "NabLa"
  - subject: "Re: Sugestion"
    date: 2008-04-24
    body: "+1 Please go into more detail about most used apps. thanks."
    author: "D"
  - subject: "Still no MacOS-style menus..."
    date: 2008-04-23
    body: "And still no \"Mac OS Style menu\" option... \n\nGiven that the KDE 4.1 schedule plans the \"hard feature freeze\" for May, 19th, I start fearing this feature will not make it in time. I also wonder if anybody is actually working on that; I'm not even sure on who is in charge of this, since the related bug report (#153027) lists \"Lubos Lunak\" as the assignee, while the 4.1 Feature Plan lists \"Friedrich W. H. Kossebau\" for the \"Mac-like menu bar plasmoid\".\n\nAny information on this ? Is there still anybody working on that ?"
    author: "Keiran"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-23
    body: "There is work done about this. Check out this link: http://article.gmane.org/gmane.comp.kde.devel.panel/10226"
    author: "TeeZee"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-23
    body: "Uhm, in KDE 3, this works with or without the kicker panel extension.\n\nOf course, it works better if it is embedded in a panel, this way it's possible to use part of the panel for the clock and the notification area.\n\nStandalone, it used to almost work with early KDE 4 snapshots, at least.\n\nDoesn't it work anymore?"
    author: "Luciano"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-23
    body: "I'm not a dev, but I'm wondering if Alien widgets are going to cause problems with this: does Mac OS Menu require that the app's menu bar has its own \"window\"? This is precisely what Alien prevents, if I understand it correctly."
    author: "Anon"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-23
    body: "Interesting hint, thanks. Yes, the menu bar is just made a toplevel window, so that it can be embedded. But that Alien mechanism is still alien to me, so:\n\nFellow reader, if you are more familiar with Aliens, could you tell what had to be changed in KMenuBar::setTopLevelMenuInternal() to workaround any Alien problem? Thanks a lot by all top menubar fans.\n\nSee quickly at \nhttp://websvn.kde.org/trunk/KDE/kdelibs/kdeui/widgets/kmenubar.cpp?view=markup\n\n\n"
    author: "Frinring"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-23
    body: "any Qt widget can be turned into a \"real\" widget (from the windowing system's perspective) with a single call at runtime, so this isn't an issue. in fact, we've had to be very careful not to accidentally trigger this behaviour in places such as the control center."
    author: "Aaron Seigo"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-23
    body: "[And still no \"Mac OS Style menu\" option...]\n\nHe, that sounds like complaining. Please think about your style.\nWhat is your reason? Did anyone stop you to work on it? ;)\n\nPeople who have a \"bug\" (it's a wish, after all) assigned against or noted to a plan that they think about implementing something are not doing any contract with anyone besides themselves by this. So going to them and demanding something from them is ... well, judge yourself. At least not welcomed by those.\n\n[Any information on this ? Is there still anybody working on that ?]\n\nActive help is needed. As you can read in the mailing list thread referenced above, there is some basic code, compiling and running. And it looks like there is also some bug with making KMenuBar a proper toplevel window in kdelibs. And close to no time available currently for those who touched that code before.\n\nIf you want to increase the chance of getting something done, just do it yourself :) If you can't, well, bad luck for now. Learn to can then. Other's did, too. Start now: http://techbase.kde.org/index.php?title=Getting_Started"
    author: "Frinring"
  - subject: "Re: Still no MacOS-style menus..."
    date: 2008-04-29
    body: "[He, that sounds like complaining. Please think about your style.]\n\nI was not really *complaining*. I was actually *worried*.\n\nBesides that, even if I had been complaining, I don't see why I'd have to \"think about my style\": AFAIK, comments are not limited to the \"gushing praise\" genre. \nDon't participate on public forums if you can't accept comments with a possibly negative connotation.\n\n[What is your reason? Did anyone stop you to work on it? ;)]\n\nThe tired old elitist argument.\n\nIn a previous similar question on the dot about this, I got the answer that it was \"It was almost working until they deleted the config dialog from kde3 the day before KDE 4.0.0 was tagged\", so I was under the assumption back then that it was a rather trivial work that simply didn't make it for 4.0. There was no feeling that it was needing so much resources that the current developers couldn't manage to finish it on time for the 4.1.\n\nWhen this last digest came, I then realized that no work had been done on this since March, and that there may be a resource problem. Thus the comment.\n\nIf you had read me more thoroughfully instead of stopping at your first preconception of its content, you'd have noticed that I tried to determine if there was somebody working on this, and if so, who it was, with the obvious intend of providing support if needed. That you preferred to answer with sarcasm is disappointing for sure.\n\n[So going to them and demanding something from them is ... well, judge yourself. At least not welcomed by those.]\n\nAgain, I was not *demanding* - if you like to see intends behind my words that I didn't put, that's your problem, not mine. \n\nIs that offensive to ask whoever works on a part of the project, with the intend of providing help ? Is that offensive to underline that a rather popular feature of KDE3 will probably not make it to 4.1 ?\n\n[Active help is needed.]\n\nThis was not apparent until this changelog, which seems to make obvious that nothing has been done about this since the beginning of the year - and that answers my first question. Next time, try to spare me the rhetorical lecture, you'll save your time as well as mine.\n\n[ If you want to increase the chance of getting something done, just do it yourself :) If you can't, well, bad luck for now. Learn to can then. Other's did, too. Start now: http://techbase.kde.org/index.php?title=Getting_Started ]\n\nWhy do you think I asked if there was still anybody working on that ? Please, refrain yourself from considering any enquiry about a possibly missing feature as an offense. And, BTW, I already know where to find the KDE Development resources.\n\nI'm sorry if you find my answer rather rude, but try to understand my own position: when asking for informations about a given issue that is obviously harder to solve than first estimated, and for which little information transpired for a long time, with the probable intend to work on it myself, I get slapped by the tired old \"don't complain, do it\". And I still don't know who, of the bug assignee or the feature plan assignee, I should get in touch with for this. So indeed, I admit I'm a little disappointed."
    author: "Keiran"
  - subject: "Session management in Konsole?"
    date: 2008-04-23
    body: "Please allow this stupid question: Will there be full support for session management as in KDE3? I really got used to it and find it quite annoying in 4.0.3 that only konsole is reopened on log in, but not its tabs and working directories..."
    author: "Sebastian"
  - subject: "Re: Session management in Konsole?"
    date: 2008-04-23
    body: "I gotta agree. Session management has never been working right for me in Kubuntu Hardy, with KDE 4.0.3. Same symptoms as you describe.\n\nAlso Opera always comes up on desktop 1, instead of desktop 2, where it was running. Some konsole windows even seem to be placed entirely randomly.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Session management in Konsole?"
    date: 2008-04-23
    body: "> Will there be full support for session management as in KDE3? \n\nYes, this will be re-implemented but no promises for KDE 4.1.\n"
    author: "Robert Knight"
  - subject: "Re: Session management in Konsole?"
    date: 2008-04-24
    body: "Thanks. I hope you will manage it for 4.1. I do not want to wait for 4.2 before I switch from 3.5 in my office..."
    author: "Sebastian"
  - subject: "And what about Linux Media Center Edition in KDE?"
    date: 2008-04-23
    body: "Hello!\n\nI'm subscribed to the mailing list of linux MCE and nobody knows when we will have an integrated Media Center inside KDE...\n\nI've found some really nice screenshots:\n\nhttp://matthaus.woolard.googlepages.com/linuxmce_mockups%28mark2%29\n\nDoes anyone know anything else?\n\nOn the other hand... Thanks for your work Danny, and remember that most of people who doesn't interact so much in the Commit Digest Posts, are very thankful for your great work!! "
    author: "Poldark"
  - subject: "Linux MCE + more please!!"
    date: 2008-04-24
    body: "That looks great!!!\n\nThanks guys!\n\nMore please."
    author: "D"
  - subject: "New (?) idea for plasma containment"
    date: 2008-04-23
    body: "I have an idea for a new type of plasma containment.  I can imagine some situations where it would be really annoying to have an applet stuck to the desktop or in the panel - it would be nice for it to behave as a normal application window, that can be independently raised, lowered, kept in front or minimized etc.  So what about a plasma containment that behaves as a normal window, just offering a space for the applet to render its output?  Particularly for things like pastebin applets or picture viewer applets, where they could be useful both stuck to the desktop or as normal applications I think this could be a really good idea.\n\nTaking the idea the other way, what about a different kind of containment to allow a normal application to be stuck to the desktop, rotated etc. just like a plasma applet?\n\nTaken together, the 2 ideas would get rid of the slightly weird distinction between having plasma applets only on the desktop or panel, and traditional applications only in traditional windows. Anything could be put wherever it was most useful."
    author: "Adrian Baugh"
  - subject: "Re: New (?) idea for plasma containment"
    date: 2008-04-24
    body: "From how I understand it, plasmoidviewer (which comes with Plasma) might be a solution for your problem."
    author: "Stefan Majewsky"
  - subject: "Whats Happened to the \"What's This\" Icon"
    date: 2008-04-24
    body: "Does Oxygen not have a \"Whats This\" window icon.  The program I have been developing has What's This text, but how do I implement it.  I hope the thinking is that it is not from a Menu as it would not be as fast and as useful as from a ? icon located at the top of the screen.\n\nDoes anyone know about the missing \"?\" Window Icon\n\nThanks\nDavid"
    author: "David"
  - subject: "Re: Whats Happened to the \"What's This\" Icon"
    date: 2008-04-24
    body: "Hmm that it is there AFAIK?"
    author: "NabLa"
  - subject: "Re: Whats Happened to the \"What's This\" Icon"
    date: 2008-04-24
    body: "but unfortunatly there is no \"keep window above all\" :( (I'm using 4.0.x)"
    author: "Koko"
  - subject: "Re: Whats Happened to the \"What's This\" Icon"
    date: 2008-04-26
    body: "Right click the icon in the top left corner of the window, Advanced -> Keep above others."
    author: "Jonathan Thomas"
  - subject: "BUG #157017"
    date: 2008-04-24
    body: "Please Please Please have a look at bug #157017 -\n\nThank you so much!\n"
    author: "Luigi"
  - subject: "RAPTOR"
    date: 2008-04-24
    body: "Will Raptor be shipped with KDE 4.1 ???\nIts been a loong time since i read article about it in this diggest"
    author: "m_goku"
  - subject: "Re: RAPTOR"
    date: 2008-04-24
    body: "As much as everyone stand behind Kickoff's usability study (how in the world can a slow menu that forces you into several clicks, and obfuscates items so that you can't find what you're looking for be good for usability is completely beyond me) I can't imagine then changing the default to something like Raptor.\n\nI imagine Raptor will have a seperate release cycle and exist on kde-apps.org for some time.  That being said, I can't wait to try it."
    author: "T. J. Brumfield"
  - subject: "Re: RAPTOR"
    date: 2008-04-24
    body: "People supporting 'Kickoff' should be shot."
    author: "ac"
  - subject: "Re: RAPTOR"
    date: 2008-04-24
    body: "People anonymously and unconstructively slagging other people's hard work should be keelhauled ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: RAPTOR -> People supporting Kickoff ..."
    date: 2008-04-24
    body: "Should NOT be shot. I mean free software is about using whatever suits best to one's need.\n\nThat said: I personally don't like it. But what s the deal? I accept kickoff as something being there ... and am glad I don't use it ;-)"
    author: "nathanael "
  - subject: "Re: RAPTOR -> People supporting Kickoff ..."
    date: 2008-04-25
    body: "I myself don't think kickoff is that bad. I use it everyday. However i feel Raptor  look better. I'm curius about it"
    author: "m_goku"
  - subject: "Re: RAPTOR"
    date: 2008-04-24
    body: "According to feature plan:\nhttp://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan\nRaptor guys want to move it in for 4.1\nbut i think it all depends if all goes well and they complete the code before feature freeze."
    author: "Emil Sedgh"
  - subject: "Re: RAPTOR"
    date: 2008-04-24
    body: "Just yesterday a lot of code went in.\nBut we really dont want to force raptor into any one, so baby steps on that one. \nWe would like to do some extencive usability studys maping were people are cliking, how long does it take them to get from A to B. and how they evolve using the tool. In order to make raptor beter. "
    author: "nuno pinheiro"
  - subject: "Re: RAPTOR"
    date: 2008-04-24
    body: "Yes. More usability studies for menus, because they made the kickoff menu work so well.\nAt least we can take solace in the fact that it looks pretty, even if it does its assigned job poorly.\nOxygen windeco anyone?\n\nI know, I know. Low blow.\nRemember now kiddies, don't feed the troll. \nEspecially if he only assymptotically approaches a valid point..."
    author: "illogic-al"
---
In <a href="http://commit-digest.org/issues/2008-04-13/">this week's KDE Commit-Digest</a>: Complete source rewrite, with many improvements, in KInfoCenter. Important work on the "Quick Launch", "Folder View", and "RSSNOW" <a href="http://plasma.kde.org/">Plasma</a> applets. Initial work towards future support for a list of timezones tooltip for the digital-clock Plasmoid. KMoon is obsoleted by the Plasma "Luna" applet. "Ozone", a fork of the <a href="http://oxygen-icons.org/">Oxygen</a> window decoration style which respects system colour preferences. Get Hot New Stuff support for icon themes in KDE. KNotify notifications interface now conforms to the <a href="http://www.galago-project.org/">Galago</a> specification. Screen selection in "presentation" mode in <a href="http://okular.org/">Okular</a>. Work on tooltips in <a href="http://enzosworld.gmxhome.de/">Dolphin</a>. Enhancements, including theming, for error pages in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a> (<a href="http://www.konqueror.org/">Konqueror</a>). WebKit adaptations for various applications with HTML rendering widgets. Support for the "<a href="http://dot.kde.org/1206028949/">Space Navigator</a>" hardware device in <a href="http://koffice.org/">KOffice</a>. Work on duchain support for QMake in <a href="http://www.kdevelop.org/">KDevelop</a>. New "PIMOShell" tool for administration of data in <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. Backup functionality and work on the system tray application in <a href="http://pim.kde.org/akonadi/">Akonadi</a>. Initial import of WordKubes, and Parsek, a game implementing the Thousand Parsec framework. Various improvements in Kubrick, which moves from kdereview to kdegames. Skanlite moves from kdereview to extragear/graphics. KBoggle moves to the "unmaintained" module. <a href="http://amarok.kde.org/">Amarok</a> 1.4.9, a bugfix edition fixing Amazon cover art downloading, is tagged for release. <a href="http://commit-digest.org/issues/2008-04-13/">Read the rest of the Digest here</a>.

<!--break-->
