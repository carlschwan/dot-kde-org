---
title: "KDE Commit-Digest for 31st August 2008"
date:    2008-09-01
authors:
  - "dallen"
slug:    kde-commit-digest-31st-august-2008
comments:
  - subject: "Danny is amazing..."
    date: 2008-09-01
    body: "... in the end, he caught up! Thanks a lot for this valuable service and source of information!"
    author: "Luca Beltrame"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "It's cool.\n"
    author: "Michael \"Wow!\" Howell"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "++"
    author: "gttt"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "Didn't I just read a digest? Congratulations for catching up! I'm looking forward to reading regular digests again. Your work is really appreciated. Keep on rocking!\n"
    author: "fliegenderfrosch"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "Awsome that you managed to catch up Danny! Thanks for providing us with a lot of interesting reading material over the last weeks. I will miss the frequent reads now that you will go back to a once-a-week schedule. To the nay-sayers that complained about the \"old\" digests that you were producing and told you you should skip ahead: you proved them wrong bigtime!"
    author: "Andre"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "Congrats denny!\n\nNow what would be funny would be a digest for 07 Sep tomorrow :)"
    author: "IAnjo"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "VERY impressive. I too was a skeptic of publishing \"old\" digests, but that was only because I didn't think someone would be able to catch up and I was afraid you'd only fall behind even more. I shall never doubt thee again.\n\nSo like someone said in an earlier edition of the digest: The devs are going to have to keep up with Danny now :)\n"
    author: "bsander"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-02
    body: "I thought he was going to catch up eventually, but no where near this fast.  Great work!"
    author: "TheBlackCat"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "WOW, hey danny slow down or maybe you should change the name to daily news, anyways you rocks"
    author: "draconiak"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "++\n\nThanks a lot!!!"
    author: "LB"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-01
    body: "Thank you Danny :)"
    author: "m."
  - subject: "Re: Danny is amazing..."
    date: 2008-09-02
    body: "Whoa.  Danny- please go relax and take a vacation now.  Great job!"
    author: "Steve"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-03
    body: "No! We can't catch up only to fall behind. :D"
    author: "Ian Monroe"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-02
    body: "Dan the man! True to your word. Great job man. Very impressive."
    author: "winter"
  - subject: "Re: Danny is amazing..."
    date: 2008-09-02
    body: "Hello,\n\nindeed. That's such a rare combination of skill and traits. Have to love it.\n\nYours,\nKay"
    author: "Debian User"
  - subject: ":O"
    date: 2008-09-01
    body: "Thank you Danny ! \nThis deserves an akademy award :) \n(... I just checked... and he already gained one in 2007)"
    author: "shamaz"
  - subject: "Holidays"
    date: 2008-09-01
    body: "I think you can take a few well deserved days of holidays now. Thanks Danny."
    author: "Med"
  - subject: "100e8 stars in KStars"
    date: 2008-09-01
    body: "Woot, what a *huge* number!\nbut... why not 10^10? :)"
    author: "ZeD"
  - subject: "Re: 100e8 stars in KStars"
    date: 2008-09-01
    body: "Actually it is rather 100x10^6 stars, which is a fairly reasonable number (a factor 1000 improvement from 4.1). The ultimate goal is to be able to handle over 10^9 stars.\n\nTalking about kstars, in trunk we now draw stars in a different way, they look prettier. If anyone has the abilities/motivation to add OpenGL rendering so we can add atmosphere effects, as in stellarium for instance, please contact the mailing list, that would be a tremendous feature. For now skilled kstars developers do not have enough free time to undertake such a task."
    author: "Med"
  - subject: "Re: 100e8 stars in KStars"
    date: 2008-09-02
    body: "100e8 is 1e10 which is 10 Billion Stars."
    author: "Marc Driftmeyer"
  - subject: "Re: 100e8 stars in KStars"
    date: 2008-09-02
    body: "100e8 is a typo in the digest; it should be 100e6 or 1e8.  I assure you, kstars cannot display 1e10 stars!\n\nThis typo should in no way detract from the enormous accomplishment of having 100 million stars.  Akarsh Simha worked on adding 1 million stars for his GSoC project, and he completely blew past our original goal by a factor of 100.\n\n"
    author: "LMCBoy"
  - subject: "Re: 100e8 stars in KStars"
    date: 2008-09-03
    body: "Danny could not make typo - he is super human!"
    author: "Andrew Lowe"
  - subject: "Thanks!"
    date: 2008-09-01
    body: "Congratulations for catching up Dany, and thanks!\nThanks also to the devs for the new entry in Desktop Effects allowing to choose the effect for windows switching and desktop switching. Quite useful indeed."
    author: "Richard Van Den Boom"
  - subject: "Impossible"
    date: 2008-09-01
    body: "Denny is impossible!"
    author: "Edney Matias"
  - subject: "Re: Impossible"
    date: 2008-09-02
    body: "Yes, but Danny is possible."
    author: "reihal"
  - subject: "Big Thanks and Respect to the Devs"
    date: 2008-09-02
    body: "As I read Danny's Commit-Digest (excellent job Danny and thanks) I just shortly reflected on what it was like back in those days (not long ago) when people used to bomb this section with complaints, whining and of course bashing. Nowadays however, there are only a few responses to the articles. It's like people don't have the guts to come out and say Thank you Aaron and the rest of the KDE crew for doing an excellent job!!!\nWell that's exactly what I would like to say.\n\nI am presently using the development version of KDE 4.2 and man, I just can't believe that a pre-alpha software can be so solid. I haven't had a single crash up to now and it really rocks.\nKeep up the good work you all :)"
    author: "Bobby"
  - subject: "Re: Big Thanks and Respect to the Devs"
    date: 2008-09-03
    body: "They're just waiting for the Curing Cancer plasmoid, which, at the current rate of development, will be in KDE 4.3."
    author: "explainer"
  - subject: "Re: Big Thanks and Respect to the Devs"
    date: 2008-09-03
    body: "So, even before focus follows mind? That would be disappointing. I have been waiting for that one for so long now..."
    author: "Andr\u00e9"
  - subject: "Re: Big Thanks and Respect to the Devs"
    date: 2008-09-04
    body: "\"Nowadays however, there are only a few responses to the articles.\"\n\nMaybe that's because everyone left."
    author: "MamiyaOtaru"
  - subject: "Re: Big Thanks and Respect to the Devs"
    date: 2008-09-08
    body: "Tiny little coward trolls left? Impossible!"
    author: "Vide"
  - subject: "Extensions"
    date: 2008-09-03
    body: "Hi\n\nthis could be the wrong forum, but maybe you could help.  What is the possibility of having certain file extensions e.g., .jpeg and .MP3 on a global basis (installed once for all applications)and not on a per application basis.  Some applications in KDE contineously ask to access the internet to download these and especially people without internet access have a problem with this issue.\n\nSomething like this might have been implemented allready, but I am still running KDE 4.0 on SUSE 11.0 and it is quite problematic. \n\nCJ"
    author: "CJ"
  - subject: "Re: Extensions"
    date: 2008-09-03
    body: "Um... I do not understand your question. Why would KDE apps (which exactly?) ask to download files that are already on your computer (as I'm reading from \"without internet access\")? Indeed, there is a global management for file extensions through KMime (see \"Advanced > File type associations\" or similar in System Settings)."
    author: "Stefan Majewsky"
  - subject: "Re: Extensions"
    date: 2008-09-03
    body: "Applications asking for internet access?\n\nTo me this happens sometimes when I use the word completion in Konqueror's address bar. If I start typing an invalid URL, Konqueror automatically adds \"http://www.\" in front... "
    author: "Sebastian"
---
In <a href="http://commit-digest.org/issues/2008-08-31/">this week's KDE Commit-Digest</a>: Interface work and new applets specialised for use on MID (small form factor) devices, beginnings of applets-in-the-systray, and work on a new calendar popup widget design in <a href="http://plasma.kde.org/">Plasma</a>. A collection of new comic provider sources, and use of <a href="http://solid.kde.org/">Solid</a> to detect network availability in the "Comic" Plasmoid. The "Spellcheck" runner moves to kdeplasma-addons, a revival of the "<a href="http://strigi.sourceforge.net/">Strigi</a>" Plasmoid, and a new "XEyes" Plasma applet. Two new layout modes for the "present windows" effect in KWin-Composite. Even more bug fixes in Kicker for KDE 3.5. A basic "revision history" implementation, and the beginnings of code generation support, in kdevplatform (the basis of <a href="http://www.kdevelop.org/">KDevelop</a> 4). Support for loading 100e8 stars in <a href="http://edu.kde.org/kstars/">KStars</a>. Get Hot New Stuff for downloading new skins in <a href="http://home.gna.org/ksirk/">KSirK</a>. Support for exporting to JPEG in Darkroom. The ability to pick a colour from the desktop in KColorEdit. Support for video annotations (using <a href="http://phonon.kde.org/">Phonon</a>) in <a href="http://okular.org/">Okular</a>. <a href="http://edu.kde.org/marble/">Marble</a> integration in <a href="http://www.mailody.net/">Mailody</a> displaying the network route an email has taken. Automatic language detection and a range of bug fixes in Sonnet. Dramatic speedups in AdBlock filtering in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. A configuration dialog and KConfig support in kio_bookmarks. Initial implementation of KOSDWidget-based KNotify OSD plugin. Various work on PowerDevil, with a move into kdereview. Import of a KIO thumbnailer plugin for RAW camera files. An experimental library to abstract away media player interfaces. Initial version of an Open Collaboration Services client, "Attica", and an <a href="http://pim.kde.org/akonadi/">Akonadi</a> resource for handling users. Version 1.0 of the Lancelot alternative menu is tagged for KDE 4.1. KDE 4.1.1 is tagged for release. <a href="http://commit-digest.org/issues/2008-08-31/">Read the rest of the Digest here</a>.

<!--break-->
