---
title: "Report from Open Mind 2008"
date:    2008-05-28
authors:
  - "gventuri"
slug:    report-open-mind-2008
comments:
  - subject: "thanks"
    date: 2008-05-28
    body: "Thanks for spreading the word about Amarok! :) Sounds like a fun event."
    author: "Ian Monroe"
  - subject: "Re: thanks"
    date: 2008-05-29
    body: "It's been Daniele and it's been interesting. The second morning there was a library room full of teenagers that listened about Amarok, Digikam, K3b and Kopete. A lot of people asked: \"when Amarok2 will be released? When will be released for Windows?\" By the way, have you got specific plans? :)"
    author: "Giovanni Venturi"
  - subject: "Perfect!"
    date: 2008-05-28
    body: "It's great to educate children in this way. Most just get mis-educated via pre-installed OSes."
    author: "winter"
  - subject: "Re: Perfect!"
    date: 2008-05-29
    body: "Yes, but believe me it's not so easy to keep quite the teenagers. You have always surprise them with something of special and it's not easy, so the bluetooth's Konqueror helped me so much to capture their attention."
    author: "Giovanni Venturi"
  - subject: "Whow"
    date: 2008-05-28
    body: "looks like you had an enervating experience here, and amazingly interested students :)"
    author: "Diederik van der Boor"
  - subject: "Yes!!"
    date: 2008-05-29
    body: "It's common knowledge that if you target children, you usually have them for life. YEA!!!!!!"
    author: "Riddle"
  - subject: "Re: Yes!!"
    date: 2008-05-29
    body: "Fortutately that was not the case with me, since I started with windows :-)"
    author: "ETN"
  - subject: "Re: Yes!!"
    date: 2008-05-30
    body: "I also started w/ Windows. However, M$ never specifically targeted children.\n"
    author: "Riddle"
---
Earlier this month <a href="http://www.kde-it.org/">KDE Italia</a> attended <a href="http://openmind05.it/">Open Mind 2008</a>. A Free Software event organised by Roberto Dentice in San Giorgio near Naples.  There were KDE talks and KDE demonstrations.  Read on for the report.





<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://www.kde-it.org/e107_images/articles/openmind/kde.jpg" alt="Giovanni's KDE presentation in the library" title="Giovanni's KDE presentation in the library"><img src="http://static.kdenews.org/jr/kde-italia-kde.jpg" width="300" height="197" border="0"></a><br>Giovanni's KDE presentation in the library
</div>

<p>At the three day event, a lot of school children with their teachers were involved to participate in the educational labs, for the talks and the workshops. We tried to show them why it is a good reason to replace Microsoft Windows on their computers to host GNU/Linux Free Software on their disks, they can learn more and be really free using KDE.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://www.kde-it.org/e107_images/articles/openmind/amarok.jpg" alt="Daniele showing how Amarok rocks :)" title="Daniele showing how Amarok rocks :)"><img src="http://static.kdenews.org/jr/kde-italia-amarok.jpg" border="0"  width="300" height="222"></a><br>Daniele showing how Amarok rocks
</div>

<p>We had about 1500 attendees. Me and Daniele Costarella, as KDE Italia, demonstrated KDE and its applications to the school children. Especially the ones that let you move to GNU/Linux very simply, without regret for Windows. I made a general presentation of KDE and then during the 3 days we had our KDE workshops on K3b, Amarok, Digikam, Konqueror, Dolphin, Kopete and KOffice. Daniele explained about KDE very well and very precisely which let me discover some nice Digikam and Amarok features I did not know. To get the students full attention I had to use some tricks with Konqueror. When I wrote "bluetooth:/" in the Konqueror address bar, we saw that all the audience were more involved when more than 30 icons appeared in the virtual Bluetooth folder.  They were interested when I said, "now we can connect to this phone and spy in it...". Of course, I spied in my Bluetooth phone showing them a Konqui photo.</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://www.kde-it.org/e107_images/articles/openmind/qt4.jpg" alt="Qt4 in action on Giovanni's Laptop" title="Qt4 in action on Giovanni's Laptop"><img src="http://static.kdenews.org/jr/kde-italia-qt4.jpg" width="300" height="200" border="0"></a><br>Qt 4 in action on Giovanni's Laptop
</div>

<p>The Open Mind organiser told us that at the end of the event people were very crazy about what we had shown and a lot of people wanted a GNU/Linux distribution with KDE. A boy asked me about the KDE distribution :) so I explained him that KDE is a Desktop Environment not a distribution itself. We burned some Kubuntu 8.04 CDs and told people that they can also download it from the Internet in legal way, copy it and redistribute it in the same legal way we did.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<a href="http://www.kde-it.org/e107_images/articles/openmind/kde-italia.jpg" alt="our KDE Italia booth" title="our KDE Italia booth"><img src="http://static.kdenews.org/jr/kde-italia-small.jpg" width="300" height="171" border="0"></a><br>Our KDE Italia booth
</div>

<p>Someone asked me and Daniele about "programming Linux" and "where is the source code?" so, at out booth, we showed KDE and explained them about KDE and Qt programming. They had a lot of questions, often very specific questions, so I understood that they were really interested and then I showed KDevelop in action. I created on the fly the classic Qt 4 simple text editor application and the very few line of code to have a browser with Qt 4.4 WebKit. I showed them the rich Qt documentation. I spoke all day but at the end everyone was satisfied, so I hope to see some new young Italian developer in the KDE team in the near future.</p>

<p>We also socialised with the other speakers, had beers and pizza.  See you on the next Free Software event,<br>
Giovanni Venturi</p>




