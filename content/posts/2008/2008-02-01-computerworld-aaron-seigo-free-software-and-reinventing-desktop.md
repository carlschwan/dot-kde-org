---
title: "Computerworld: Aaron Seigo on Free Software and Reinventing the Desktop"
date:    2008-02-01
authors:
  - "jriddell"
slug:    computerworld-aaron-seigo-free-software-and-reinventing-desktop
comments:
  - subject: "Great interview"
    date: 2008-02-01
    body: "Speaking of the future, does anybody know what's up with Decibel? The mailing list has almost no traffic and I read that basyskom kind of dropped the development."
    author: "Patcito"
  - subject: "Re: Great interview"
    date: 2008-02-01
    body: "Nothing but compilation fixes for the last few months:\n\nhttp://websvn.kde.org/trunk/playground/pim/decibel/\n\nIf it's not outright \"dead\" then it is, at the very least, on life-support.  Not looking promising :/"
    author: "Anon"
  - subject: "Re: Great interview"
    date: 2008-02-01
    body: "> If it's not outright \"dead\" then it is, at the very least, on life-support. Not looking promising :/\n\nI'm afraid so, plus Decibel is supposed to be using Tapioca which I think was dropped too :/"
    author: "Patcito"
  - subject: "Re: Great interview"
    date: 2008-02-01
    body: "This is a feature I'm sad to see drift away, having chat and VoIP accessible in a uniform way would be priceless these days."
    author: "Skeith"
  - subject: "Re: Great interview"
    date: 2008-02-01
    body: "No! This can't happen... I was really excited about Decibel. Way more excited than I was about Plasma!\n\nI want my presence-aware, task-aware applications! E.g., my Voip app that shows small pop-ups if I'm watching a movie, but rings if I'm not, and all music/video pauses if I accept a call... my email client that shows a \"Reply by IM\" if the sender is currently online... my Plasmoid that changes my Facebook and IM status at once... not having to reconfigure IM accounts every time I try a different client... my I-want-to-talk-with-Joe button that picks between video conferencing/voip/IM/SMS/email for me..."
    author: "kwilliam"
  - subject: "Re: Great interview"
    date: 2008-02-01
    body: "+1\n\nhopefully someone gives decibel his or her love, and continues to maintain, and develop it!\n\n"
    author: "Richard"
  - subject: "Re: Great interview"
    date: 2008-02-02
    body: "THIS.\n\nThis is the type of thing a desktop overhaul should bring.  If there was more stuff like this, I'd have far less heartburn over what I see as useless changes designed to appeal to people who don't and will never use KDE."
    author: "AS"
  - subject: "Re: Great interview"
    date: 2008-02-02
    body: "+1 I like these features you described."
    author: "Max"
  - subject: "Re: Great interview"
    date: 2008-02-02
    body: "In the feature list for KDE 4.1, they are a few plans for Decibel. "
    author: "Jeremy"
  - subject: "Re: Great interview"
    date: 2008-02-07
    body: "Decibel has been somewhat dormant for a while, but don't worry its coming back to life again. It is definitely not dead. Look out for some bigger progress in the near future! And if anyone wants to help it I'm sure it would be very welcome."
    author: "George Goldberg"
  - subject: "plasma runtime?"
    date: 2008-02-01
    body: "So when do we get this plama based flash competitor that we can use with python, js or ruby and that runs on all browsers IE, FF, Opera, KHTML/WebKit etc? Sounds great, but wouldn't the runtime be huge compared to the flash plugin? doesn't matter though."
    author: "Patcito"
  - subject: "Re: plasma runtime?"
    date: 2008-02-01
    body: "Why should it? After all, Plasmoids can be written in Javascript, which already is used in many - if not most - webpages already, and is integrated in all of these browsers. Unlike Flash, where you have to load an external, proprietary plugin.\n\nBut yeah, it's a pretty crazy idea, and that's what Aaron said in the Interview as well. ; )"
    author: "Anonymous"
  - subject: "Re: plasma runtime?"
    date: 2008-02-01
    body: "> But yeah, it's a pretty crazy idea, and that's what Aaron said in the Interview as well. ; )\n\nMaybe it's not that crazy, it's not the first time I see people talking of a qt run time that would act as a java or flash plugin on all platform with svg. Actually, given that TT are including phonon and webkit, it wouldn't surprise me if they come up with a qt run time by the time 4.4 or 4.5 comes out."
    author: "Patcito"
  - subject: "Nice talk, but a warning"
    date: 2008-02-01
    body: "The speech is good, well trayined, but reality is still some years ago.\nDon't take me wrong, from the start I always tought the ideas behind plasma were wonderfull, what isn't it the timeline.\n\nLet's se:\n- working to get plasma do the basic work of a nowdays desktop: still very incomplete in 4.0, probally will reach maturity in 4.1.1 or 4.1.2.\n- social/colaboration desktop/tools: huh? nothing there to see yet.\n- change the desktop paradigm is very very far away still. People actually like the nowdays desktops, they are familiar with it and works ok. Can be better? Oh hell, yes, yes! But won't happen in a short time, and won't happen without friction.\n\nIs this a flame/critic? Not in any way!!\nI actually agree 100% (ONE HUNDRED PERCENT for those who won't read very well the messages before posting a reply) with Aaron vision, and I'm very exited about what future will bring to KDE desktop (left the Nokia/Qt case for a while in the dark), it's just that I'mtaking extra triple care to avoid the 4.0 vapor-creation, I assume my share of guilty in the case ;)\nI belive things will move and happen, but let's give it a time and keep the good ideas coming!\n\nOh, and before I forget: the criticism over KDE in my opinion IS a good thing (tm). Those will make developers work even more hard, and when aplause comes in the future, it will be much deserved ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice talk, but a warning"
    date: 2008-02-01
    body: "++ - it's good to be honest about the amount of time and work needed to bring these things to fruition."
    author: "Anon"
  - subject: "Don't give up on KDE people!! Help make this best"
    date: 2008-02-01
    body: "C'mon guys!! Don't slow down. You're doing a great job so far.\n\nWe have a long way to go. 4.0 is not an excuse to slow down. It's a reason to really start developing. Harder, Faster!! We need to show the naysayers that KDE 4.x.y is the future! We're not anywhere near being able to take a break.\n\nLook, many people slowed down, or took a break because of the Nokia deal. While I understand the \"wait and see approach\" we really can't do that. We won't know how the Nokia deal will pan out for quite a while 6+ months, maybe. \n\nIn the meantime we have to work on getting 4.1 out the door and for it to be the greatest release EVAAAR!! *sorry, had to.* =) \n\nAmarok 2.0 will probably be one of the \"killer apps\" Rightfully so. It's a great program with tons of potential. I can't wait to use it cross platform (everything from a Smartphone,Car Pc, Media Center, products we haven't conceived yet...)\nWe need more killer apps like these though. Please keep working on them.\n\nVista proved to be a dud. \"Windows 7\" is just around the corner. (Yahoo's help might make it even better than we fear.) If we want the free desktop/mobile phone OS/Set top box/Media Center/Car PC/etc. to succeed, our product needs to be on the market before then. Heck it needs to be better than the competition. I'm sure even Nokia and Google will help us if they see we gain acceptance and marketshare. Nokia is smart enough not to destroy something that's thriving. \n\nEspecially Nokia can make devices like KDE powered Media centers, Smart phones, Car Stereo's/PC's, Cross platform devices. *hint, hint to Nokia and others*\n"
    author: "Max"
  - subject: "Re: Don't give up on KDE people!! Help make this best"
    date: 2008-02-01
    body: "Don't worry about the devs slowing down, commit levels have been quite high lately. ;)\n\nAnd as for Windows 7 being around the corner, ha! I'd doubt it. Vista was just released, XP hasn't died, and Vista took fooooreeeeverrrr."
    author: "Jonathan Thomas"
  - subject: "Re: Don't give up on KDE people!! Help make this best"
    date: 2008-02-02
    body: "Yea but Vista has proven to be a dud. Even Bill Gates himself admitted to that. (if you read between the lines - I think it was an interview with engaget IIRC)\n\nI think \"Windows 7\" will be out much faster than that, unfortunately. I give it less than 2 years. Mark my words. \n\nAll the \"leaked\" material is pointing that way. It scares me.\nI also think that Vista was the last closed source release Microsoft made. It seems that Microsoft will \"copy\" Apple's idea and mix in open source components. - Just differently.\n\nHopefully you're right. Hopefully development hasn't slowed down. It seems that there are many abandoned projects, or projects that are just creeping along. No big leaps yet.\n\nWe'll see with 4.0.1, and 4.0.2 and so on. Hopefully the changes will be made steadily.\n\nDon't give up people. You can always code during the Superbowl. :) juust kidding."
    author: "Max"
  - subject: "critical != not visionary"
    date: 2008-02-01
    body: "Dear mr king,\n\nBeing critical does not nessecarily imply not being able to understand or have or appreciate visions.\n\nLots of the critic that amongst others I have expressed was and is valid, and helped opening some eyes to critical issues with kde 4. Not enough that kde 4.0 became a vital choice for ones daily desktop, but I'm sure that will come. Even with a little bit of contributions from me, althoug the attitude from higher level geniouses makes my time spent with coding kde less ;)"
    author: "Anders"
  - subject: "Podcast?"
    date: 2008-02-02
    body: "Is there going to be a video of Aaron's speech at Linux.au?\n\nSince it's all the way down under it would be nice to at least \"virtually\" be there."
    author: "Max"
  - subject: "Re: Podcast?"
    date: 2008-02-02
    body: "The video and presentation slides are available at http://linux.conf.au/programme/presentations\n\nBut the site is down at the moment, check back later."
    author: "Gusar"
---
Computerworld interviews <a href="http://www.computerworld.com.au/index.php/id;885892575;pp;1">Aaron Seigo on life, free software and reinventing the desktop</a> at this week's <a href="http://linux.conf.au">Linux.conf.au</a> conference.  He talks about some of KDE's successes, the targets of the 4.0.0 release, the future of Plasma and Nokia's purchase of Trolltech.  He concludes that "<em>The KDE project has never been more exciting</em>".

<!--break-->
