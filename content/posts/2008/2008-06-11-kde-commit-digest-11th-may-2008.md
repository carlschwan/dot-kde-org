---
title: "KDE Commit-Digest for 11th May 2008"
date:    2008-06-11
authors:
  - "dallen"
slug:    kde-commit-digest-11th-may-2008
comments:
  - subject: "Hello."
    date: 2008-06-11
    body: "I took a break.\nThe rest of the Digests, up to the present, should be out by the end of the week.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Hello."
    date: 2008-06-11
    body: "Danny, ur teh BEST."
    author: "ur teh best"
  - subject: "Re: Hello."
    date: 2008-06-11
    body: "Amazing dedication and work Danny! The Commit Digests are really an *incredibly* useful resource!"
    author: "Kitsune"
  - subject: "Re: Hello."
    date: 2008-06-11
    body: "Thank you very much! Always an interesting read..."
    author: "Thomas R"
  - subject: "Re: Hello."
    date: 2008-06-11
    body: "Thanks, and keep up the good work :)"
    author: "IAnjo"
  - subject: "Re: Hello."
    date: 2008-06-11
    body: "Thanks for your continued effort, Danny!"
    author: "mactalla"
  - subject: "Re: Hello."
    date: 2008-06-11
    body: "Great - I hope you are a happy and well-rested Dannya :)"
    author: "Anon"
  - subject: "Re: Hello."
    date: 2008-06-12
    body: "If anyone deserves a break, it's you.  Don't burn yourself out.   Every digest is great."
    author: "Leo S"
  - subject: "Re: Hello."
    date: 2008-06-12
    body: "+1"
    author: "Yogesh M"
  - subject: "Re: Hello."
    date: 2008-06-12
    body: "+1. Keeping track of all of the stuff happening in KDE is no trivial task. I hope you're well-rested."
    author: "Riddle"
  - subject: "Re: Hello."
    date: 2008-06-12
    body: "danny is my hero!"
    author: "markus"
  - subject: "KaffeineGL"
    date: 2008-06-11
    body: "What is it? A 3D video player? sounds cool."
    author: "Patcito"
  - subject: "Re: KaffeineGL"
    date: 2008-06-12
    body: "Yes, I'm curious too, although my favourite video player at the moment is smplayer"
    author: "NabLa"
  - subject: "Kaffeine status"
    date: 2008-06-12
    body: "Anybody knows what's up the Kaffeine for 4.1? I am using the KDE 3.5x version on 4.1 beta 1 and it's okay but it would be really great if we could get a Qt 4 version of Kaffeine."
    author: "Bobby"
  - subject: "Yihaaaaa"
    date: 2008-06-11
    body: "Thomas McGuire is my king ;-)\n\nSome additions to the highlighter API.\nNow suggestions for new words can be done with reasonable speed, and there is no need for the client to keep a map of replacements around.\n\nSee my mail to k-c-d.\n\nAlso, it is now possible to ignore words or add them to the dictionary.\n\n--> I really hope this will make it possible to just right-click a word and see suggestions... Oh, and thanks to Mirko Stocker for getting this into Kate!!!\n\nSebastian K\u00fcgler is also cool, btw, the work on the KWin effects rocks... Though I'm still waiting for a 'appear' effect for windows which looks as good as the one on Vista (sorry, but I really like the smooth, unobtrusive, clean effects in Vista - OK, the windows flip sucks of course).\n\nAll in all, KDE 4.1 SVN is rocking more and more..."
    author: "jos poortvliet"
  - subject: "Re: Yihaaaaa"
    date: 2008-06-11
    body: "About Sonnet, is it possible to automatically add some entries about contacts (name, forname, city...) in the dictionary ?\n"
    author: "DanaKil"
  - subject: "Re: Yihaaaaa"
    date: 2008-06-11
    body: "> Also, it is now possible to ignore words or add them to the dictionary.\n\nthat was always possible.\n\n> I really hope this will make it possible to just right-click a word and see suggestions\n\nthat was also always possible"
    author: "Zack Rusin"
  - subject: "Re: Yihaaaaa"
    date: 2008-06-12
    body: "I knew it was possible to ignore words or add them to the dictionary (though in my experience the ignore function rarely worked) but right-clicking a word and seeing suggestions - never saw that. At least not in Konqi, KMail, dunno about Kword but I believe it didn't work there either. And it doesn't work right now and here, in Akregator, while writing this response.\n\nWhich is very unfortunately, it's not very useful to have the ability to start the whole spellcheck dialog - if you quickly want to change one word at the end of the text. After all, there always are a lot of red words you'd first have to go through."
    author: "jospoortvliet"
  - subject: "Re: Yihaaaaa"
    date: 2008-06-12
    body: "KMail (and KNode) now has support for adding a word to the dictionary / ignoring or word or fixing it with a suggestion by right-clicking it for a few weeks now, which was possible due to the kdelibs commit you mentioned.\nThe commit in kdelibs was actually fairly trivial, it just exposed some functionality which was there in Sonnet already.\n\nI plan to eventually move the right-click context menu to kdelibs so that all apps using KTextEdit (e.g. Konqueror) will also get it. The only KMail-specific thing there right now is that it doesn't spellcheck words in quoted text, I have to think about how to preserve that when moving it."
    author: "Thomas McGuire"
  - subject: "Addition to the Palapeli report"
    date: 2008-06-11
    body: "We have created a concept for arbitrary patterns with a generic algorithm. Do not expect anything to appear in the GUI before Akademy's coding marathon."
    author: "Stefan Majewsky"
  - subject: "Re: Addition to the Palapeli report"
    date: 2008-06-13
    body: "Champion! Nice to see a replacement for good ol' KPuzzle taking shape.\nHere's the website of a Windows puzzle game in case you guys need inspiration :)\n\nhttp://www.dgray.com/screenshots.htm"
    author: "Darryl Wheatley"
  - subject: "Re: Addition to the Palapeli report"
    date: 2008-06-14
    body: "Thank you for the link. Inspiration is a good idea, and the feature set described there shows where the journey goes for Palapeli. Yet, I see that the mentioned game does only offer tradiontial pieces. I'm imagining jigsaws with triangular pieces or the puzzle patterns created by M. C. Escher (example at http://www.worldofescher.com/store/jpgs/Z53BL.jpg)."
    author: "Stefan Majewsky"
  - subject: "Re: Addition to the Palapeli report"
    date: 2008-06-16
    body: "You're welcome :)\n\nYour multi-shape puzzle idea sounds really interesting - it's great to see yet another awesome program take shape with the KDE 4 series."
    author: "Darryl Wheatley"
  - subject: "Vi mode for Kate"
    date: 2008-06-12
    body: "Hello!\n\nDoes anyone know if this: \"The start of a vi input mode support is merged into Kate.\" actually means that KatePart, the KPart used by Kate, will get a Vi mode, ie it will be embedded into other apps too? Hint: KDevelop :)\n\nIf this is the case, it's great news. Having Vi input in every application that uses the KPart would be just wonderful.\n\n\nGreat work on the digests, Danny, they're a great resource. I'd miss many of these news without you :)"
    author: "oblio"
  - subject: "Re: Vi mode for Kate"
    date: 2008-06-12
    body: "It does. KatePart will get a vi input mode. :-)"
    author: "Erlend"
  - subject: "vi input mode"
    date: 2008-06-12
    body: "(I *assume* they meant the kpart, so any app using the kate kpart will have support for it.)\n\nI do really which you best luck in getting this done as soon as possible (4.1?), a kate/kdevelop with a VIM-alike input mode and :commands might really finally get me to use kate/kdevelop in the end (once kdevelop's c++ debugging support improved, too :D)."
    author: "Christian Parpart"
  - subject: "Re: vi input mode"
    date: 2008-06-12
    body: "http://websvn.kde.org/?view=rev&revision=814815\n\nlol, it was too good to be true."
    author: "Christian Parpart"
  - subject: "Re: vi input mode"
    date: 2008-06-12
    body: "The alterations seem to be in kdelibs, so it seems like that this is indeed being implemented in the KPart.  Why the negativity?"
    author: "Anon"
  - subject: "Re: vi input mode"
    date: 2008-06-12
    body: "\"remove the vi input mode from kate trunk and back to branches/kate/vi_input_mode\"\n\nThis could not be added in time for 4.1 due to the soft feature freeze."
    author: "Hans"
  - subject: "Vi-Mode @Kate?"
    date: 2008-06-12
    body: "Any source for more infos about this? Will this just a bit of hardcoded keys, or is it embedded in the normal shortcut-framewort of kde-apps?"
    author: "Chaoswind"
  - subject: "Re: Vi-Mode @Kate?"
    date: 2008-06-12
    body: "I'll blog about it soon.\nThis will be a vi input mode for the katepart. You should be able to use vi(m) commands in kate and kdevelop and other programs using the kate kpart."
    author: "Erlend"
  - subject: "Wrong date in title"
    date: 2008-06-12
    body: "It is June, folks. May is already gone. I am sure there will be another May next year, but for now you have to get along with June ;-)"
    author: "anon"
  - subject: "Re: Wrong date in title"
    date: 2008-06-12
    body: "The Digest was for May, though."
    author: "Anon"
  - subject: "Re: Wrong date in title"
    date: 2008-06-12
    body: "Yep, sorry.\n"
    author: "anon"
  - subject: "Flickr"
    date: 2008-06-12
    body: "I love this plasmoid but it's missing on my desktop for quite a while now. is it still available for KDE 4?"
    author: "Bobby"
  - subject: "Nokia loves the open source..."
    date: 2008-06-13
    body: "So how long until the community has to fork?\n\nhttp://linux.slashdot.org/article.pl?sid=08/06/13/123206"
    author: "News at 11"
  - subject: "Re: Nokia loves the open source..."
    date: 2008-06-14
    body: "I think you need to re-read the article.  Nokia isn't talking about forcing concepts like this into your current desktop.  They are saying these are fixtures in mobile phones that aren't going away immediately just because they are embracing open source.\n\nThe mobile industry won't abandon those things overnight.  Just like DRM-free music, it is slowly being adopted, and most major companies don't understand why DRM might be bad.  Many believe it to be absolutely necessary.  All they know (believe) is that DRM prevents them from losing money.  Abandoning DRM grants them no benefit, and runs the risk of losing money.  How well do you think that goes over for a suit?\n\nOften the people who make such decisions aren't geeks.  And just because a geek might have a political ideal, that doesn't mean the world will all change and accept it.\n\nThat is what Nokia was saying.\n\nTo quote the article (emphasis is mine):\n\n<quote>\"Dr Ari Jaaksi told delegates that the open-source community needed to be 'educated' in the way the mobile industry currently works, because the industry has <b>not yet</b> moved beyond old business models.\"</quote>\n\n"
    author: "T. J. Brumfield"
  - subject: "Re: Nokia loves the open source..."
    date: 2008-06-15
    body: "Well said.\n\nA thought experiment: Let's assume, Nokia would introduce DRM classes into Qt. We could always update our qt-copy, then remove these DRM functionality. Most distributors would then surely adapt such patches in their packages, with the result that the biggest part of Qt 4 installations would not have DRM functions. You see that introduction of DRM and such is point-less in the desktop Qt."
    author: "Stefan Majewsky"
---
In <a href="http://commit-digest.org/issues/2008-05-11/">this week's KDE Commit-Digest</a>: A wordprocessor-like ruler for repositioning and resizing the <a href="http://plasma.kde.org/">Plasma</a> panel. Scripting support re-enabled in KRunner. More developments in the NetworkManager Plasma applet. Initial work to allow closer interaction of Plasma with KNotify's popups. Work on theming, Magnatune membership support, and the ClassicView in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Work on adding support for plugins to <a href="http://edu.kde.org/marble/">Marble</a>. General work across KDE games, with many new application icons. Work on project management handling and Ruby support in <a href="http://www.kdevelop.org/">KDevelop</a>. Functional improvements to the Sonnet spellchecking engine. Undo/Redo support in Krone. Exploded pie charts in <a href="http://koffice.kde.org/kchart/">KChart</a>. The start of work on notes in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. Scripting support for images in the <a href="http://www.kexi-project.org/">Kexi</a> "Reports" plugin. A <a href="http://koffice.org/">KOffice</a> <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> shape which uses Marble to display a map. A return to work on the <a href="http://raptor-menu.org/">Raptor</a> alternative menu. Initial commits for KaffeineGL, and the next-generation tile system of <a href="http://www.koffice.org/krita/">Krita</a>. The start of a vi input mode support is merged into <a href="http://kate-editor.org/">Kate</a>. Winning themes from the first <a href="http://dot.kde.org/1206097090/">Plasma Theme Contest</a> added to KDE SVN. KsirK and KBreakOut move from kdereview to kdegames, ksaneplugin from kdereview to kdegraphics. Goya moves into kdereview. guidance-power-manager, written using Python (PyKDE), is added to kdereview, for later inclusion in extragear/utils. KSim, KMilo, KLaptopDaemon move to the unmaintained module of KDE SVN. KWorldClock is officially replaced by the world clock applet of Marble. <a href="http://commit-digest.org/issues/2008-05-11/">Read the rest of the Digest here</a>.

<!--break-->
