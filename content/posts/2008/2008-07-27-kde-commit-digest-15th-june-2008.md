---
title: "KDE Commit-Digest for 15th June 2008"
date:    2008-07-27
authors:
  - "dallen"
slug:    kde-commit-digest-15th-june-2008
comments:
  - subject: "Love the \"Oxygen\" style in Plasma"
    date: 2008-07-27
    body: "The buttons and tab animations look so real and smooth. Too bad non-plasma apps won't be able to use this style. Or will they?"
    author: "Bill"
  - subject: "Re: Love the \"Oxygen\" style in Plasma"
    date: 2008-07-27
    body: "Yes, I would really like that too. I've seen it in Gtk (Aurora style I think?) and in Opera too. It really adds that natural, professional look."
    author: "miggols99"
  - subject: "Re: Love the \"Oxygen\" style in Plasma"
    date: 2008-07-27
    body: "The BESPIN (Original Oxygen theme) got those kind of effect for all application. Long time ago, it was said that eventually, New Oxygen will have these effect too, but 1 years and 2 month later, only a basic crossfade is available for KTitleWidget and fade out ofr Q/KlistWidget.\n\nYou may try bespin from SVN, it is far away from the 1 years old dropped Oxygen style, it is now almost complete (4-5 widget left)."
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: Love the \"Oxygen\" style in Plasma"
    date: 2008-07-27
    body: "It did have fancy cross-fade effects indeed.  Unfortunately it was also unacceptably slow under common circumstances on medium-spec PCs.  This isn't the developer's fault - any kind of animation which covers a large number of pixels is difficult to do fast without reliable hardware acceleration.  \n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Love the \"Oxygen\" style in Plasma"
    date: 2008-07-28
    body: "Yes, but animations like overmouse on kwin button, overmouse on menubar menu, fading \"glow\" around button and many other doesn't cover a lot of pixel. Even the plastik kwin style had some. Oxygen style still need some fancy effect to meet the \"2008 standard\" of fancy effect. I tried bespin SVN on a pentium 4A 1.6ghz (2002) with a rage128 16mb card and it did not drop a single frame at 1280x1024, so those effect can be enabled on low end/medium hardware without too much problem. I even have compositing (xrander) of this computer with transparency and shadow and nothing lagg (around 25 fps while moving a maximised windows with 50% opacity. Without compositing, it is around 35fps)."
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Thanks"
    date: 2008-07-27
    body: "Thank you Danny for your efforts and work! Even when tailing the commits it's always more interesting to read your summaries about the changes!"
    author: "JJ"
  - subject: "Re: Thanks"
    date: 2008-07-27
    body: "Danny you rock! Thank you!"
    author: "Dirk"
  - subject: "skip to 4.2?"
    date: 2008-07-27
    body: "Nice, but there are still some big bugs for kmail. Some POP filter crash kmail\n(like \"download everything\" filters to use as last one),the bad being that it crashes kmail on startup, so fixing is a bit hard (go offline, open kmail and remove all pop filters), also passwords bounce from kmail to kwallet a bit too much.\n\nAmarok, K3b, Kaffeine and Kshutdown for KDE 4 are all in very alpha state,\nKtorrent is the only beta.\n\nI'd say that common user might want to skip even 4.1 to get 4.2 when it will be ready."
    author: "nix"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-27
    body: "The KMail POP filter crash is fixed in the final release of KDE 4.1.0 (but it didn't make it to RC1)."
    author: "Thomas McGuire"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-27
    body: "Ktorrent is in a very stable state...\nBesides, K3B, Amarok and KOffice have different planning than KDE, so your message is pointless ; they may be released before KDE 4.2"
    author: "MakoSol"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-27
    body: "+1"
    author: "Jonathan Thomas"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-29
    body: "> Ktorrent is in a very stable state...\n\nThe only stable state at current Ktorrent is the crash-counter, which let it crash under my kde3 for at least 4-5 times after his first start and then randomly after some minutes or an hour."
    author: "Chaoswind"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-29
    body: "And if such instability was a norm with KTorrent, should it not be noticed by the developers or the myriad of KTorrent users out there?\n\nJudging by the entries in bugzilla and postings on the KTorrent forum, it don't look like it. Would lead to one thinking perhaps the problem lies solely on your system, making it a case of broken installation or setup."
    author: "Morty"
  - subject: "Re: skip to 4.2?"
    date: 2008-08-01
    body: "> And if such instability was a norm with KTorrent, should it not be noticed by \n> the developers or the myriad of KTorrent users out there?\n\nNo.\n\n> Judging by the entries in bugzilla and postings on the KTorrent forum, it\n> don't look like it. Would lead to one thinking perhaps the problem lies solely\n> on your system, making it a case of broken installation or setup.\n\nSame behaviour on different installs and different distris."
    author: "Chaoswind"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-27
    body: "I'd say the really _common_ common user won't care whether he is using a kmail that uses kde3 libraries or a kmail that uses kde4 libraries: what I _do_ know is that all my family is panting to get KDE 4.1 on their laptops -- they've seen me work with it and want it, want it now!"
    author: "Boudewijn"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-27
    body: "KTorrent is currently at 3.1.1, where 3.0(released 17 feb) was the first stable KDE4 release. How you make that out to be only beta are rather strange, and even with your other \"issues\" they are still not resons for common users to stay away from 4.1\n\nCBesides common users can happily use the KDE 3 versions of Amarok, K3b and Kaffeine. Besides KDE 4 still comes with JuK a very capable music player, and likevise for video with Dragon Player both great alternatives for the common user. Kshutdown(at beta 1, released today btw) have never been a common user application, as most users manage very well with the standard KDE logout/shutdown dialog. "
    author: "Morty"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-28
    body: "This is KDE. That should be KBesides not CBesides.\n\n(sorry, couldn't resist :) )"
    author: "Matthew"
  - subject: "Re: skip to 4.2?"
    date: 2008-07-28
    body: "And definitely not GBesides xD"
    author: "Alejandro Nova"
  - subject: "kuiserver?"
    date: 2008-07-27
    body: "Will kuiserver be ready for 4.1 ?"
    author: "Marco"
  - subject: "Re: kuiserver?"
    date: 2008-07-27
    body: "No, that will be 4.2. Check http://pindablog.wordpress.com/2008/07/24/more-on-kuiserver-and-extenders/ for some more info on it's current state."
    author: "Rob Scheepmaker"
  - subject: "Re: kuiserver?"
    date: 2008-07-28
    body: "Wow, that looks incredible.  KDE 4.1's just coming out and I already can't wait till KDE 4.2!"
    author: "Matthew"
  - subject: "Re: kuiserver?"
    date: 2008-07-28
    body: "This only shows how healthy the KDE project is. Instead of stagnation, there is a lot of progress happening. I am not going to compare this with other desktop projects ;)"
    author: "christoph"
  - subject: "Re: kuiserver?"
    date: 2008-07-29
    body: "It just shows how much back is the implementation behind the plans/promises."
    author: "Chaoswind"
  - subject: "Re: kuiserver?"
    date: 2008-07-29
    body: "and your link to the plans / promises about the delivery time for this feature are where? Or were you just making stuff up?"
    author: "Borker"
  - subject: "Mini-browser, sweet"
    date: 2008-07-29
    body: "For a long time I have been impressed by Mac OSX:s dashboard and especially the minibrowser from Shiira, http://shiira.jp/mini/en.html.\nHopefully the web browser-plasmoid will work something similar.\n\nThanks Danny for the digest and thank you all nice developers that just keep on improving the KDE experience."
    author: "Peppe Bergqvist"
---
In <a href="http://commit-digest.org/issues/2008-06-15/">this week's KDE Commit-Digest</a>: In a long-planned move, the FolderView Plasmoid also becomes a containment (which enables it to fill the desktop space). The FolderView <a href="http://plasma.kde.org/">Plasma</a> applet gets standard folder interaction context menu items. First version of CommandWatch Plasmoid, which displays output of a given console command. Support for displaying the running state of plugins and terminating jobs, abstraction of code completion (leading to initial code completion support for Java), and the clearing out of bug reports in <a href="http://www.kdevelop.org/">KDevelop</a> 4. Integration of the Panaramio online service into <a href="http://edu.kde.org/marble/">Marble</a>. Work on loading themes in <a href="http://edu.kde.org/parley/">Parley</a>. A "drawing history" and support for undo/redo in the sketch widget of Fuzzy Search in <a href="http://www.digikam.org/">Digikam</a>. More features in the "vi input mode" project for <a href="http://kate-editor.org/">Kate</a>. More work on the new MessageListView project of <a href="http://kontact.kde.org/kmail/">KMail</a>, and a keyring database editor for <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>. Various developments across <a href="http://amarok.kde.org/">Amarok</a> 2, including an early "<a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a>Collection". Start of an implementation of the famous "cube switch" effect for KWin-composite. Fully auto-generated Kimono C# bindings. More work on the "Table" tool in <a href="http://koffice.org/">KOffice</a>, with other developments including progress in the <a href="http://www.kexi-project.org/">Kexi</a> reports and web interface components. The "GeoShape" (based on Marble) moves to playground/office. <a href="http://commit-digest.org/issues/2008-06-15/">Read the rest of the Digest here</a>.

<!--break-->
