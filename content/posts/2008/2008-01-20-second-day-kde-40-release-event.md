---
title: "The Second Day of the KDE 4.0 Release Event"
date:    2008-01-20
authors:
  - "jpoortvliet"
slug:    second-day-kde-40-release-event
comments:
  - subject: "GREAT review"
    date: 2008-01-20
    body: "This is a great review. If you read this it is almost like you're there.\nLots of info about what's going on within KDE.. all seems very positive."
    author: "Peter"
  - subject: "Re: GREAT review"
    date: 2008-01-20
    body: "thank you, it is a pleasure to write them. Note that i seem a better writer than i am thanks to the great editing danny does."
    author: "jospoortvliet"
  - subject: "Re: GREAT review"
    date: 2008-01-21
    body: "I agree.. Thank you so much for posting this!! :)"
    author: "Max"
  - subject: "Why video only next week??"
    date: 2008-01-20
    body: "why's that damn video available only next week???\n\n/me clip-clops his hooves in impatience"
    author: "repre hendor"
  - subject: "Re: Google Video"
    date: 2008-01-20
    body: "1. Youtube sucks regarding quality, and the lenght is limited, too. So Please use GOOGLE VIDEO, since it allows for better quality.\n\n2. Your organisation is great normally, but this time it really sucks. Lots of people couldn't be there or on an official release party. It shouldn't be that hard to just take the video and upload it within an hour after the keynote is finished. \n\nThis is a great opportunity for kde PR-wise, why don't you take the opportunity and try to communicate the \"be there\" feeling to the world by being a bit quicker about all that?"
    author: "MichaelG"
  - subject: "Re: Google Video"
    date: 2008-01-20
    body: "Both YouTube and Google Video, thank you."
    author: "reihal"
  - subject: "Re: Google Video"
    date: 2008-01-20
    body: "we had a live stream to the release parties but bandwidth was limited. We also tried to get help from fluendo but they were not very responsive. Finally google offered to help but they couldn't do realtime. Well its already online so not that bad."
    author: "jospoortvliet"
  - subject: "Re: Google Video"
    date: 2008-01-21
    body: ">> Well its already online so not that bad.\n\nKeynote was finished around Friday 20:30, and to my knowledge it took until Saturday close to midnight until it was up. That's really bad imo, considering that there are fans out there waiting and wanting to share the experience.\n\nDon't get me wrong, i appreciate it, but i wished someone just would have taken the video and uploaded it to youtube or google video within the day of the keynote."
    author: "MichaelG"
  - subject: "Re: Why video only next week??"
    date: 2008-01-21
    body: "I think we have been spoiled by Apple Keynotes (Stevenotes :p) and expect everything to be a grand scheme and be streamed right away to all devices possible..\n\nGive those guys time..\n\n\nYea, I'd prefer something higher resolution than Youtube (hopefully Google is reading this and considering bumping the YouTube resolution...) but I'll take what I can get. :-)"
    author: "Max"
  - subject: "where can I get one of these?"
    date: 2008-01-20
    body: "http://franz.keferboeck.info/gallery/main.php?g2_itemId=565\ncoolest thing ever :)\n\nlol @ these:\nhttp://franz.keferboeck.info/gallery/main.php?g2_itemId=513\nhttp://franz.keferboeck.info/gallery/main.php?g2_itemId=522\nhttp://franz.keferboeck.info/gallery/main.php?g2_itemId=525"
    author: "Patcito"
  - subject: "Re: where can I get one of these?"
    date: 2008-01-20
    body: "I had heard that Konqui and Kate were going to make an appearance, but wow they look great."
    author: "Ian Monroe"
  - subject: "Re: where can I get one of these?"
    date: 2008-01-20
    body: "Yeah, that's what i thought! :) Now, i'm just left wondering - will our honoured mascots be visiting FOSDEM, so that Amarok's mascot Mike can meet them? ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: where can I get one of these?"
    date: 2008-01-21
    body: "Awww.... They're cute...\n\nI gotta show them off to my friends later today. :)"
    author: "Max"
  - subject: "what ugly mascots"
    date: 2008-01-20
    body: "Those KDE mascots are rather ugly, why doesn't KDE with 4 now released hold a comp to design and vote for a new mascot.  Those are really uninspiring that look like some child put them together."
    author: "Richard"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "Obviously a comment from someone not actually at the event: these guys were great, and one of the (many) highlights of the event. And the costumes are technically excellent, and very true to the mascot design.\n\nA mascot is not a logo, and in KDE 4.0 the use of the Konqi mascot throughout the KDE interface has been scaled back for reasons of \"professionalism\". So what is the issue with keeping a fun and long-serving part of our project alive at events like this?\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "The costumes don't look extremely professional, but the mascot artwork is pretty nice."
    author: "T. J. Brumfield"
  - subject: "Re: what ugly mascots"
    date: 2008-01-22
    body: "The costumes were actually professionally designed based only upon konqi/katie artwork that was available on the internet. I had seen photos of their design sessions with the costume designer they used, and they are quite good up close too! "
    author: "Troy Unrau"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "(This has nothing to do with the costumes or event, I think that people dressed up as Konqi is an awesome idea!)\n\nI agree with the grandparent about the current Konqi imagery. I think in the same way that the K-gear was improved for KDE 4.0, someone should improve the Konqi mascot for 4.1. The current Konqi looks sadly dated and out of place, and a little cheesy. I'd like to see an SVG remake. Something professional, but fun. I mean, he's a dragon right? Dragons are COOL! Or at least they can be. Konqi could be a big draw for users; I mean, who could resist a desktop environment with a sweet dragon as it's mascot? (Especially when the competing mascots are a window, a foot, and an apple. ;-) "
    author: "kwilliam"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "The \"window, a foot, and an apple\" are logos, not mascots!\n\nThe KDE logo is the K surrounded by a gear that is seen in the bottom left of a KDE desktop and elsewhere.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "Ok, there's a point. Windows and OSX do not have mascots technically, I guess. Would a penguin be Linux's mascot and the little devil (daemon?) be BSD's mascot? Or is the penguin a logo, because there is really only one popular image of him? I apologize for not knowing exactly the difference.\n\nAnyway my point is similar to Richard's point (as expressed in his second post, http://dot.kde.org/1200788475/1200797159/1200799340/1200821861/). However, I'm thinking just about the artwork in the \"About KDE\" and \"Log Out\" dialogs. I noticed the Log Out dialog switched from showing Konqi to showing the moon, presumably because the old Konqi \"photo\" wasn't cool or professional enough. That made me sad, because Konqi is part of KDE's heritage, and dragons are cool. If we can't display him proudly in our artwork, we should update his photograph so he fits in with the Oxygen/Plasma \"aura\". If Konqi and Katie are 3D models on somebody's computer (the images I've seen of him look like CG) then some inspired people should have a contest to recreate the models in Blender or something similar. Add more detail, such as scales and sub-surface scattering (or whatever 3D artists do these days) to make some sweet, realistic renderings. Just like how Peter Jackson improved King Kong while keeping true to the original's spirit, we could have a more detailed model of Konqi.\n\nI noticed that this 2007 video (http://www.kulma.org/linux/kde/kone.php?categ=anims) portrays Konqi is a sea serpent, which is a nice twist (I think sea dragons are cooler, more like traditional Chinese dragons - and it makes Konqi seem more international). There's no reason Konqi couldn't be a sea serpent, or have bigger wings, etc., as long has he keeps the \"green, friendly dragon\" concept."
    author: "kwilliam"
  - subject: "Re: what ugly mascots"
    date: 2008-01-22
    body: "Konqi is a blender model: http://www.kulma.org/linux/kde/kone.php?categ=model"
    author: "Yuriy Kozlov"
  - subject: "Re: what ugly mascots"
    date: 2008-01-24
    body: "Apple's unofficial mascot is Clarus the Dogcow.  Tux the Penguin is both Linux's logo and mascot.  Windows doesn't have a mascot (unless you count Clippy).   On the other hand, most colleges and universities have both mascots and logos, as do sports teams.  I'd say KDE's use lines up pretty much with the university use... for promotional events and things like clothing and toys, the mascot is used.  For identification (for instance, login and logout screens), the logo is used.\n\nBy the way, I saw Kandalf recently off to the side of the road with a handlettered sign \"Will mascot for bandwidth\".  Very sad."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "If that's the best complaint you can come up with, then I think it's clear the KDE team has done a fantastic job."
    author: "elsewhere"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "wrong a mascot can, if done right pull in clients, get people switching to it.  KDE needs a new mascot for 4 (Not logo, I didn't say logo, and I know the difference, but you obviously can't read, and decided to put that in there)  There should be a comp to design a new mascot, one that looks better than those ugly things.  You want to inspire people to come to it, having those two ugly mascots aren't going to do it. They may bring in the nerds at google, but you want to bring in the regular folk that aint inspiring, actually they are very ugly."
    author: "Richard"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "What kind of reasoning is that? KDE isn't a junk food restaurant or a theme park! It's a desktop environment and the best one at that. Konqui and Kate are there because we love 'em, not because they are necessary.\n\nAre you from 500 years in the future?*\n\n\n\n*) http://en.wikipedia.org/wiki/Idiocracy"
    author: "Nonymous"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "BEST REPLY OF THE YEAR!!!\n\n<sorry for the caps>"
    author: "Nonymous"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "Those costumes actually took months to put together; I know because I'm wearing one of them in that picture. Just a funny (strange) observation: I didn't really feel like part of the community until I walked in wearing that costume and now I understand at least some of what it must have been like for the developers when people who hadn't used KDE4.0 for very long, if at all were constantly saying really negative things. You are, of course, entitled to your opinions and I'm actually not upset (anymore) that you have such a low one. The only thing I want to ask is that before you start trashing somebody elses work (mine, the KDE developers, anybodys), especially when all you've seen is a single picture (I'm assuming you weren't at the release event, correct me if I'm wrong), try to remember that these things that look simple often aren't (I'm almost positive you've never made anything like them yourselves; if you had, you would know just how much work goes into them) and be a little bit more polite. \"Rather ugly\" and \"look like some child put them together\" isn't the best way to start a productive conversation."
    author: "reldruh"
  - subject: "Re: what ugly mascots"
    date: 2008-01-20
    body: "hey, i found the costumes cool. sure, on the first look i was, uh.. ok. but then i saw all the detailed work that had to be done for this costumes, and i was remembering myself that the person who did it, probably did it we nearly no budget, and thats why i think your costume is great, it's realy authentic."
    author: "Beat Wolf"
  - subject: "Re: what ugly mascots"
    date: 2008-01-21
    body: "dude, those costume rocked my world. seriously. the fact that you actually *made* them yourselves blew me away; i mean, katie even had the right necklace! they were awesome, they made the event feel even more KDE than it already did and i don't think there was a single person who wasn't smiling ear-to-ear when we saw you guys in costume. heck, that's why we stood outside for 20 minutes taking pictures of konqi and katie in various poses with various people there =)\n\nrock on!"
    author: "Aaron J. Seigo"
  - subject: "Re: what ugly mascots"
    date: 2008-01-21
    body: "I think your costumes were really cute. I don't know why people are so hard on you.\nIf you'd want to, I'd like to invite you to Baycon '08 to come with that costume and show it off at the costume contest. I think people there would really like it/appreciate it.\n\nAlso a great promo opportunity for KDE fans.. :)\n\nWho knows, maybe one of the writers there will take a liking to the image and start a KDE themed Scifi/Fantasy book series?"
    author: "Max"
  - subject: "Re: what ugly mascots"
    date: 2008-01-22
    body: "I for one think they look awesome. While I don't have first-hand experience, I know how much time, effort, and money such costumes can require.\n\nOf course, there are things that could have made it even better: it would have been more \"true\" to have padded out the arms and body more. However, this is not at all trivial to do, so I can't exactly blame you. I also think the head looks like it has sharp edges (at least with Konqi, though not so much Katie).\n\nStill, they are very nice."
    author: "bersl2"
  - subject: "Re: what ugly mascots"
    date: 2008-01-21
    body: "I happen to like Konqi (sp?)\n\nHe's cute, mascots are supposed to be cute!!\n\nI think he might benefit from a redesign, that's for sure, but other than that he's fine.\n\nI'd also like to see more videos featuring Konqi. There is only one video on the KDE homepaage. Also fanvids would be nice. <-- With a KDE 4.0 theme they would be great promo material. \n\n"
    author: "Max"
  - subject: "Watch the hair, the hair!"
    date: 2008-01-20
    body: "Aaron, sorry to say but are much better looking with your hair cut short. :-) Emo cosplaying don't suit you well hehe."
    author: "Bruno Laturner"
  - subject: "Re: Watch the hair, the hair!"
    date: 2008-01-20
    body: "hehe! I wanted to say something about the hair too, but didn't have the guts. I mean, really it's all a matter of preference, but dude. I think you look way better in every other photo I've seen."
    author: "anon"
  - subject: "Re: Watch the hair, the hair!"
    date: 2008-01-20
    body: "The original emo kid:\nhttp://www.encyclopediadramatica.com/Image:Emokidhitler.jpg"
    author: "Paul"
  - subject: "Re: Watch the hair, the hair!"
    date: 2008-01-20
    body: "Complaining have really taken a dive.\nHair and Konqi costume, pfft, it used to be much better whining in the old days.\n"
    author: "reihal"
  - subject: "Re: Watch the hair, the hair!"
    date: 2008-01-20
    body: "\"Complaining has really taken a dive.\nHair and Konqi costume, pfft, there used to be much better whining in the old days.\"\n\nthere, fixed that for you. now for the new one:\n\n\"Complaining has really taken a dive.\nGrammar and complaint quality, pfft, there used to be much better whining in the old days.\""
    author: "yman"
  - subject: "Re: Watch the hair, the hair!"
    date: 2008-01-20
    body: "eh what you say? That dinna make sense."
    author: "reihal"
  - subject: "Re: Watch the hair, the hair!"
    date: 2008-01-31
    body: "I was trying to correct your English, while poking fun at my nit-picking."
    author: "yman"
  - subject: "Give him a break."
    date: 2008-01-21
    body: "The man lives in Canada.  \nI grow my hair long in the winter and cut it short for summer. That is due to having short hair when it was -40 and nearly had to lose my ears for it (went black due to severe frostbite; I still pay for that stupidity when it is cold outside even though it was nearly 30 years ago). \nIn addition, if his family loses hair early in life, he may simply be keeping it that way until he starts to lose more.\nOf course, he may appreciate hearing what looks better :)"
    author: "a.c."
  - subject: "Great event!"
    date: 2008-01-20
    body: "I great time was had by all! My big question now is, do I share the KDE wine I won, or do I drink it all myself?"
    author: "David Johnson"
  - subject: "Re: Great event!"
    date: 2008-01-20
    body: "Save it for the KDE 5 launch."
    author: "T. J. Brumfield"
  - subject: "Re: Great event!"
    date: 2008-01-21
    body: "Or for the next KDE release party.\n\nI understand these parties are expensive, but it would be great if Google would host another one for the next major milestone.\n\nPeople enjoy reading about them, and they're great publicity, not to mention tons of fun.\n\nGlad that Google is also involved in open source projects such as KDE 4.x.y.  THANK YOU SO MUCH!!!  We love you google.\n"
    author: "Max"
  - subject: "Research"
    date: 2008-01-20
    body: "Thanks for mentioning the \"research\" aspect of some KDE subprojects. I sometimes think that Eigen is also a research effort i.e. it's the \"math department of KDE\", especially as a large part of the time invested in it is used in \"thinking its design\" not actual coding.\n"
    author: "Benoit Jacob"
  - subject: "Re: Research"
    date: 2008-01-21
    body: "indeed; and this is really what makes a reasonable length presentation on \"KDE\" so rediculous to do: i can only cover so much. that i didn't cover eigen or the cooler features of kalzium, pigment, etc... man.. it just sucks.\n\nall you kde people do way more cool stuff than i can talk about. but don't let that stop you ;) \n\nbtw, the last kde4 presentation i did before this one, i actually did fit in eigen and kalzium. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Research"
    date: 2008-01-21
    body: "Hi Aaron, no problem at all, I was absolutely not expecting or hoping Eigen to be mentioned in one of the presentations (but I'm pleased it was in your previous presentation), in fact the current stable Eigen, 1.0, is a very little thing and is not much used (outside of Kalzium/Avogadro, only a little bit in Krita and KSpread) but I hope that Eigen 2.0, which is a rewrite from scratch, will be much more useful.\n\nI'd like to say thanks for making the whole KDE4 project so inspiring to contribute for."
    author: "Benoit Jacob"
  - subject: "Re: Research"
    date: 2008-01-21
    body: "I've read quickly your blog about Eigen2 and as I don't know how to program C++, I'm not sure I understand how it works.\nI understood that you implemented your library in such a way to allow calculation by explicitely writing explicitely something like :\n\na = c + d;\nor\na = c * d;\n\na,b and c being matrices.\nAm i right?"
    author: "Richard Van Den Boom"
  - subject: "Re: Research"
    date: 2008-01-22
    body: "Answered by mail :)"
    author: "Benoit Jacob"
  - subject: "nice review, with one exception"
    date: 2008-01-20
    body: "\"Jeremy showed off the many cool and sexy features from KDE-Edu\"\n[exasperated sigh] you know, some of us DON'T have testicles in their skull, or think in terms of getting an orgasm. I know this is just a figure of speech, but I find it really annoying. besides, how can a software application be sexually attractive? it can have attractive looks, it can have useful features, it can be well designed, but to make you want to have sex with it? how does that fit in?\n\nsounds to me like someone here either has a limited vocabulary, or doesn't have a clear idea of what he wants to say."
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "Eye-candy? No you can't eat software let alone it tasting sweet. It is just a metaphor for visually looking good though, ooh and it doesn't mention sex! Is that just as annoying?\n\nThe writer could have said novel, distinctive and powerful features or something but cool and sexy are an alternative for \"great\" in this case I think. Sexy is universal across gender so you are right, not everyone has testicles in their head!\n\nTry not to be so annoyed by a metaphor, some of us find your complaint a big turn-off ;)"
    author: "Matt"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "where I come from sex is one of the holiest things, which is why it's private rather than public.\n\nmaking sex something casual and meaningless is a big part of what I object to, as well as encouraging the misuse of sex. all I'm asking is that you should please not offend me and turn me off. I think you'd also sound smarter if you did that.\n\nif that is too much to ask, maybe I'll take my eyes and brain elsewhere. you'd lose a potential developer, seeing as I'm about to start learning programing in college.\n\nit's your choice whether to appeal to a wider audience by not using phrases that can be offensive, or only appealing to a certain kind of person. one lone incident isn't going to turn me away from KDE, and I knew what kind of people I'm dealing with (take it literally), but if this persists I might feel like there really isn't room for me in this community. Just recently there was this whole thing about women in KDE, and when I said \"maybe there aren't woman in KDE cause they just don't want to code\" (quote from memory), I was told that it's the environment which discouraged women's participation even when they want to get involved. well, the same applies to me and moral values. I can tolerate some things, but other things turn me away. this kind of thing is the reason I don't do translations for Songbird anymore. I just couldn't tolerate the drawing of that bird having sex with Tux on the front page.\n\nBTW, expressions like \"Konqueror needs some love before it's ready\" don't bother me too much, because the meaning of love is giving and helping and doing stuff for the sake of others. it only irritates me because a lot of people use the word love to mean sex, but when people say stuff like the above they are using the word the correct way."
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I think the problem here is that you think sexy means only about sex and it doesn't. Example:\n\nIn the UK they talk about sexy football. They mean stylish and interesting football that is exciting to watch. Nothing to do with sex.\n\nTake a look at sexy from a dictionary. Here is the third meaning from dictionary dot com:\n\n3.  excitingly appealing; glamorous: a sexy new car.\n\nI hope that with this information you might be less offended by the use of the word sexy. The usage of the word in the article is completely appropriate, perhaps you disagree that the word should mean this but it is hardly the fault of the writer that you are offended."
    author: "Matt"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I'll think about it.\n\non the other hand why is it that some words are impolite, while others aren't?\nsaying \"nonsense\" is OK, but \"crap\" isn't, even when they are used for the same thing. (like \"that's nonsense\" and \"that's crap\")\n\ncan't there be some standard of discretion? or maybe there is and it doesn't cover what bothers me?"
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I would accept that sexy is more informal (informal is not necessarily impolite) than \"great\" or \"exciting\" or \"interesting\" but this was a report from a day at the release event and it seems OK to me, its hardy an academic paper or something like that. "
    author: "Matt"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I could accept that, if this weren't an official KDE website.\n\nand why should manners be something that is outside the realm of real, living people?* does this mean humanity has lost it's dignity?\n\n*academia doesn't count with me, because they live in theory rather than reality. can't tell you how many times I've been annoyed by theoretical discussions that ignored the fact that they are dealing with real issues. I'd rather not delve further into this subject"
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I think that more important than the word is the intent with which it is used,  obviously it is harder to judge in these days of text only communication.  As an example, in the UK at least, there is far more swearing on television than ever before.  People from 30 years ago would be shocked to see what is allowed these days, but nowadays the words have been desensitised.  The emotion behind the words are the same but the words have changed.  eg.  in the 1950's if my train was cancelled I would say \"bother!\" or maybe even \"bugger!\" (which is another story) but these days they say \"shit!\" or \"crap!\".   As the offence is taken in the tone of how it is phrased (from which you gauge the emotion) I think it is fine.  In short I think it can be rude depending on how it is said - which causes confusion and unintended offence over the internet where it lacks context. \n\nAnyway, KDE is a community not a corporate body, and it has members all over the globe so until the world homogenises these small problems are unavoidable I'm afraid."
    author: "anon"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "well, just because something is common doesn't make it good. all the swearing and cursing doesn't bother me any less just because they do it twice a minute on most TV shows. (and no, thats not a real number. it's a figurative figure used for emphasis)*. same goes for the pretty much pornographic content you get on TV and in movies, or gore that has finally even reached the milder cop shows.\n\n*I think I might be making fun of myself (and my sensitivities) here. or maybe all the apologies people make, like: \"I love Ubuntu, but it *ahem* doesn't run well at all on my XYZ system\" finally really got to me."
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "Using the word sexy for something new, exciting etc... isn't that uncommon, and splits the meaning of the word sexy from the meaning of the word sex, in certain cases, so it doesn't make sex casual and meaningless.\n\nSecond, using sexy, cool ... whatelse, is a way to attract more regular users to KDE, since using only political correct words/sentences would certainly give KDE a nerd-only feel, and that's not what they are aiming for."
    author: "terracotta"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "cool, eye candy, etc: these words don't have any connection to objectionable subject matters.\n\nI can understand the marketing (or whatever) reason, but I haven't met this expression until now in the realm of KDE, and somehow I didn't feel like anything was lacking."
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "oh, and I forgot to mention that on the whole, I do hate anything that has to do with being \"politically correct\".\n\nanyhow, this isn't a threat of \"do as I say or else\". I'm just telling you how you can make yourselves more hospitable and welcoming towards me. I may stick with KDE in spite of the differences, or I may feel like I can't hang out with you folks any longer, and I wouldn't know how until I'm already out the door.\n\nI do appreciate your attempts to explain things to me. maybe if I understand where you're coming from I'll be able to put up with how you express yourselves. I've already learned how to say \"weekend\", and be casual about other people consuming alcohol for fun. maybe I'll get used to this too, but then maybe not, who knows?"
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-23
    body: "I suggest reading William Isaacs book \"Dialogue and the art of thinking together\" as it deals with the situation of conflict that occurs in dialogue and suggests ways to get beyond the conflict and towards reflection where you put aside your own opinions with the aim of trying to appreciate where the other party(ies) are 'coming from'.\n\nHarmony is about tolerance and I'm sure we all appreciate being made aware of the challenges we face in communicating to a diverse community such as KDE and how to better address that."
    author: "Ed"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "let me add to the commends already made: yes my vocabulary is limited, im not a native speaker. I copy what i see elsewhere... Sorry if that is offensive."
    author: "jospoortvliet"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "\"Sorry if that is offensive.\"\n\nIt's not - some highly-strung dude flew off the handle for the flimsiest of reasons.  Keep on doing what you're doing :)"
    author: "Anon"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I think I'm all chilled by now. maybe listening to that keynote put stuff into proportion.\n\nI didn't mean to be offensive [biting lip in order not to add some more stuff that's quite irrelevant], but comforting those I offended by offending me isn't exactly going to lower the flames. or at least, it wouldn't have if I were someone else. fortunately I'm me, and I've already decided to give the subject a rest.\n\nnow, about flying off the handle: just understand one little thing, please: not everyone is like you. like I said, where I come from, that is offensive. in your culture, it's no big deal. in mine it is. I can pretty much understand why it's not a biggie for you (at least I think I can, by going into emulation mode), but you don't seem to understand me. that might be because I've simply not told you the name of the culture I come from, and I also have no intention of doing so.\n\nabout that limited vocabulary comment: I took you to be a native English speaker. lots of those (I'm not saying most) know even less words than I do. this is quite surprising, because I didn't grow up in an English speaking environment. in truth, I don't know a good drop-in replacement for that word in that sentence either, so I would have probably wrote it completely differently. \n\nthere are plenty of intelligent people who can't express themselves in the clearest manner possible for various reasons. I, for instance, usually explain things in every technical way, trying to get every word to literally mean what I want to say, regardless of what it means to the person I'm speaking to. (guess I just called myself intelligent. heh). or for example someone managed to explain to me the definition of feeling hate in a very \"fuzzy\" way. just because you aren't (at the moment) a poet in the English language, doesn't mean anything. or maybe you are, but the kind of words you use don't speak to me?\n\nI guess what I'm trying to do is apologize for offending you, without withdrawing what I said. this is indeed an odd affair for all parties involved."
    author: "yman"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "Well, I didn't (and don't) want to be involved in the discussion you had with the other guy - but I would like to say I understand what you said. You might have overreacted, but your point is valid, and I won't promise anything (I tend to forget) but I'll try to use other terms in the future."
    author: "jospoortvliet"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "> I'll try to use other terms in the future.\n\nPlease don't, I love your articles. Let's not censure ourselves to make some people happy. If you have to obey to every single religious sensibility then you might as well stop talking about technologies and drop KDE to be sure not to upset the Amish reading this."
    author: "Paul"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I just want to say that I think you're handling this situation better than most others I see in this thread.  We can never know the effect that our words or actions will have on every single person in the world.  As the project grows we will continue to have more and more people with different backgrounds.  Without knowing someone's past or current situation, we can't anticipate how something we feel is innocent might negatively affect them.  As a physical example, peanuts seem quite benign, even tasty.  But to certain people they could spell death.  Similarly, if someone had been attacked by a dog as a child, they would likely shy away from visiting anyone with a pet dog.\n\nSo, Jos, thanks for trying to understand yman and where he/she is coming from.  And yman, thanks to you as well, I see you're also trying to understand others' culture and the definition of the word to realize it wasn't intended to be offensive.  English is a very confusing language.  One of the most common mistakes I hear from others is 'fun' vs. 'funny'.  They both have the same root and similar positive connotation, however they are quite distinct in their meaning.\n\nI don't want to prolong this off topic conversation, but wanted to encourage understanding of the different people and cultures within our community."
    author: "mactalla"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-22
    body: "As a native English speaker, I didn't even notice the original comment in the piece. Not using it ever would definitely seem to fall under the \"political correctness (in a bad manner)\" realm. \n\nAnd I'm not one for casual profanity either. But this doesn't strike me as it. Nor as disrespectful. \n\nThis conversation does strike me as somewhat off-topic, though."
    author: "Anon chick"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "\"Hey! Is this guy boring you? Why don't you come talk to me instead? I run KStars. Seriously! You want to see my spaceship?\""
    author: "David Johnson"
  - subject: "Re: nice review, with one exception"
    date: 2008-01-20
    body: "I'm sooooo glad Jos didn't talk about the funky cafe at Google. :-P"
    author: "David Johnson"
  - subject: "Excellent!"
    date: 2008-01-20
    body: "Very nice review. I haven't read such a good review in a long time. The Mascots are great. All of you take it easy.\n\nPS\n\nSome of your comments are really just too much."
    author: "winter"
  - subject: "Re: Excellent!"
    date: 2008-01-20
    body: "Thank you for the kind words, but what did you mean by \"Some of your comments are really just too much.\""
    author: "jospoortvliet"
  - subject: "Re: Excellent!"
    date: 2008-01-20
    body: "He was referring to the sillyman."
    author: "reihal"
  - subject: "great!"
    date: 2008-01-20
    body: "KDE ist a great project and it shows. Especially on occasions like this, when all the people gather together, because a great project is (first of all) made of ...\n\ngreat people!\n\nAnd that's what you are... you all....\n\ngood job,.. hats off... though only the tip of the iceberg is visible atm, I think now even the most doubtful realized, that there's actually a very powerful vision behind all this... It _is_ exciting to watch this happen step by step... from thought to reality\n\nThough, openness seems to attract some hostility from time to time... it's the openness which attracts the brilliant minds!\n\n\n"
    author: "Thomas"
  - subject: "Qt 4 is not 4.0"
    date: 2008-01-21
    body: "We have all been insisting on the fact that KDE 4.0 is not all of KDE 4, so I will add here that Qt 4 is not 4.0 either. This is not the first that I've seen this news snippet published like that, but it's factually wrong.\n\nQt 4 is immediately available under the GPLv3, but not Qt 4.0. The first release under said license is Qt 4.3.4 (coming within one month). The snapshots of 4.3.4 and 4.4.0 already incorporate the license.\n\nBut it is not retroactive. Qt versions 4.0.x, 4.1.x, 4.2.x, 4.3.0, 4.3.1, 4.3.2 and 4.3.3 are licensed under the GPLv2 and commercial licenses only; that will remain so.\n\nThe same goes for Qt 3: only the 3.3.8 release is available under the GPLv3 (under the 3.3.8b packaging). Previous versions are not affected. Distributions that want to apply the new license must update their packages to the new tarballs."
    author: "Thiago Macieira"
  - subject: "Re: Qt 4 is not 4.0"
    date: 2008-01-21
    body: "Great post.\n\nThanks for clarifying that.\nI think it's a good idea that the next version will be GPL 3 compatible, rather than it being made retroactive. More people will look forward to the new version.\n"
    author: "Max"
  - subject: "Amarok is portable but plasma isn't so..."
    date: 2008-01-21
    body: "how the context browser will work then?"
    author: "Koko"
  - subject: "Re: Amarok is portable but plasma isn't so..."
    date: 2008-01-22
    body: "libplasma is portable. No problems at all :)"
    author: "Troy Unrau"
---
The second day of the <a href="http://www.kde.org/kde-4.0-release-event/">KDE 4.0 Release Event in Mountain View, California</a>, was a very busy day. Reporters and users joined the hackers, peeking over their shoulders, asking questions and generally trying to figure us out. Talks were given - most notably the keynote by Aaron Seigo, but also covering <a href="http://koffice.kde.org/">KOffice</a>, the <a href="http://edu.kde.org/">KDE-Edu</a> project, and multimedia. Read on for more details.


<!--break-->
<p>Today we were joined by even more hackers and others outside our own project, like representatives from AMD, Sun, Kubuntu, Mandriva, SUSE, and most of the Slackware team. Even some kernel hackers showed their faces, which resulted in a kernel discussion meeting.</p>

<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/room.jpg"><img src="http://static.kdenews.org/dannya/room_thumb.jpg" alt="A packed conference room" title="A packed conference room"></a></div></p>

<p>The day started with Adriaan de Groot introducing the release event, mentioning that this is not just a technical event, but also very much a social one.</p>

<p>Aaron Seigo, President of the <a href="http://ev.kde.org/">KDE e.V.</a> then began the keynote, with an introduction to KDE 4, talking about with the history of the project and expressing how far we have come in the last 11 years. Then, Aaron explored what KDE is, and what our community is based on - freedom and openness. Freedom to do work, have fun, and connect with others. Further, Aaron moved on to KDE 4, and discussed the near-future plans and ideas. The vision of KDE 4 is based upon three principles: beauty, accessibility, and functionality. He ventured into the many areas that KDE has improved upon, and pointed to our roadmap for the KDE 4 cycle.</p>

<p><div style="float: right; text-align: center"><a href="http://static.kdenews.org/dannya/keynote.jpg"><img src="http://static.kdenews.org/dannya/keynote_thumb.jpg" alt="" title=""></a></div></p>

<p>Aaron explained how users expect certain things from their computer, like games, internet connectivity, communication. So we have to meet those expectations. KDE 3.5 already widely met these goals, and so for KDE 4, we had to do chase new ideals. Aaron talked about how KDE is put together, how KDE uses many frameworks and other technologies, and introduced individually introduced them to the audience.</p>

<p>The audience watched an impressive video of the new KWin compositing features, and later, Aaron demonstrated KDE 4 applications live to the audience. After showing the state of our current KDE 4.0 release, Aaron continued with the roadmap for KDE 4. When can we expect KDE 4.0.1, 3.5.9, when will KDE 4.1 arrive? For those curious, that would be respectively this month, next month and July. Furthermore, the future is bright, as KDE will no longer be restricted to running on Linux, BSD and Solaris - yes, Windows and Mac OS X will be supported officially as well for KDE 4.1. KDE-on-Mac and KDE-on-Windows developers came up on stage and showed us what they have. Benjamin Reed, the leading KDE-Mac developer showed us Konqueror, KStars and several other applications on Mac OSX, then Holger Schroder demoed KDE-on-Windows.</p>

<p>Aaron continued, and showed us some crucial applications like KPat (the solitaire card game!) and the scalable graphics it now has. Marble and the OpenStreetMap project were discussed, and finally it was time for questions. Icons, mixing a KDE 4 desktop and KDE 3 applications, the target group for KDE 4.0, and the viability of large deployments were discussed, and Aaron ended the talk with some teasers concerning Media Center systems. The keynote was streamed to several of the KDE 4 release parties worldwide, and it will be put on YouTube and made available for download as soon as possible. Look out for an announcement on the Dot, probably at the beginning of next week!</p>

<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/suits.jpg"><img src="http://static.kdenews.org/dannya/suits_thumb.jpg" alt="" title=""></a></div></p>

<p>The next speaker was Inge Wallin, who showed us KOffice and the many improvements it has seen since its last KDE 3.x release, KOffice 1.5. Inge began by introducing the many components KOffice consists of, which is fairly impressive in itself - KOffice is the most comprehensive office suite in existence. The KOffice architecture provides for a large amount of flexibility, where users can mix and match any type of object into any document you want. So you can have a chart in a picture, use graphical effects and vector graphics in your spreadsheet, or have music notes in a vector graphic.</p>

<p>Inge continued talking about ODF and standards, telling us how KOffice was the first office suite to support ODF, and how the KOffice developers are working closely with the OASIS standards committee to ensure the long-term viability of the format. As with the KDE desktop, KOffice is also portable to Windows and Mac OSX - portability and integration are definitely the strong points of KOffice. Embedding in Konqueror or other applications, working with the okular developers - in time, the OpenDocument support from KOffice will end up in the KDE libraries, so that any KDE application will easily be able to support the format. Interest has already been expressed by several KDE-Edu developers.</p>

<p>Further, Inge went on to demonstrate some advantages of the flexible structure of KOffice, which leads to abilities like easy automation and integration into specific workflows, and the ease of extensibility. Real examples of these attributes were provided, such as a version of KOffice applications with a simplified user interface, the music notation shape, KDissert (mind mapping), and Inge also mentioned the start of the <a href="http://www.kofficesource.com/">KOfficeSource</a> support company.</p>

<p>After the KOffice talk, Google brought in some excellent food and we continued with a talk from Haavard Nord, the CEO of <a href="http://www.trolltech.com/">Trolltech</a>. Haavard talked about the symbiosis between the KDE project and Trolltech. He began detailing the state of the Linux desktop in 1996, when KDE was started. The biggest challenge back then was that writing a GUI application was very hard. A simple "hello world" application, based on the technology available back then needed more than 200 lines of code. The founder of KDE, Matthias Ettrich, wanted to use Qt, the premier Trolltech product, because Qt would make it much easier to write a full desktop. Trolltech was founded only 2 years before KDE, and has now grown into a company of over 250 employees, thousands of clients, and many thousands of Open Source developers using it in their projects.</p>

<p>Haavard continued explaining their business model (dual-licensing: GPL and a proprietary license), and he explains how Trolltech makes money from proprietary software developers in order to improve their product for everyone. So for instance, Skype and Google Earth help fund the development of the Qt framework, which in turn benefits us all. Trolltech receive almost half of their customers through their connection with KDE, and many improvements to Qt are suggested or even provided by KDE developers, for example the <a href="http://dot.kde.org/1197535003/">Phonon</a> multimedia framework. Further, Haavard talked about Qt 4 and KDE 4, how they improved Qt, and how KDE can (and does) benefit from it. A great piece of news is that Qt 4.0 will be available under the GPL version 3 license, effective immediately. The KDE project is very happy to receive this news, and KDE has been working on our own license transition in order to take advantage of other Open Source projects, such as Samba, which have already made the switch. According to Haavard, <a href="http://www.stallman.org/">Richard Stallman</a> had noted that he "was very pleased that Trolltech has decided to make Qt available under GPL v3".</p>

<p>Next up was something very exciting, the Linux MCE people had finally arrived at the Release Event. They showed a demo video showing the great functionality of LinuxMCE, and even provided attendees with free LinuxMCE DVD's containing a 20-minute installation guide movie and a full software stack. Then, Aaron Baalbergen from <a href="http://wwww.plutohome.com/">Pluto</a> started to talk about the future of Open Media Center software. LinuxMCE wants to integrate tightly with KDE - both in the area of underlying frameworks, and in the user interface. Technologies like Phonon and the other pillars allow them to improve their technologies faster while also bringing new functionality to KDE.</p>

<p>A question that came up was how the ecosystem worked. There is a company, Pluto, which is behind the code of LinuxMCE, but while they released the software for years, it wasn't used a lot outside of their company. The LinuxMCE project turned this around, and Pluto supports the project in getting a wider audience. They hope to work with KDE and get improvements into the software which will then be available to everyone, so they can become more interoperable and working together, creating an ecosystem which helps them to improve their software further. The final plan is to do what Trolltech does, and give their product away under the GPL while collecting money from those who don't want to release their software under the GPL.</p>

<p>After a short break, we had Kyle and Aron, the winners of our "all expenses paid" release event invitation contest on stage. They were asked to say a few words on their winning entries. They expressed their thanks to the KDE contributors, and offered us free beer!</p>

<p><div style="text-align: center"><a href="http://static.kdenews.org/dannya/aaron-and-mascots.jpg"><img src="http://static.kdenews.org/dannya/aaron-and-mascots_thumb.jpg" alt="" title=""></a></div></p>

<p>Paul Adams of Sirius talked about how the Free Software community has matured over the years into a complete product ecosystem. It is no longer just about development, but also about artwork, marketing, selling... and also research? Large parts of the community have traditionally been slow to formally embrace research in their work. Through <a href="http://www.sqo-oss.eu/">SQO-OSS</a> and <a href="http://nepomuk.semantic-desktop.org/">NEPOMUK</a>, KDE has indirectly benefited from millions of Euros in public funding with noticeable effects on the KDE 4.0 release. Strigi is a great example of this within KDE. However, KDE can still do more in this arena. If we can develop a culture of embracing both formal and informal research efforts throughout our work, then the results can be even stronger than we have seen to date. In the Free Software world, we don't have commercial shareholders to worry about, and so we can afford to ask questions and be more creative in our endeavours (because instead of shareholders, we have users who want cool software). By engaging in research and with researchers, we place ourselves in a much better position to deliver.</p>

<p>The sixth talk was by Jeremy Whiting on the KDE-Edu project, which started with a brief history of one of the most popular and active KDE sub-projects. Jeremy explained how many KDE-Edu applications are often used to showcase the great stuff KDE has, as they often look very cool - and KStars is a prime example of this. The educational apps also attract many new users and developers, and give the KDE libraries a good workout. Good examples are the many issues the education applications exposed in KIO, the SVG rendering engine, Phonon, GetHotNewStuff (of which they are clearly the largest user), and many other frameworks. Jeremy showed off the many cool and sexy features from KDE-Edu, from applications like KAlgebra, Kalzium, Parley, Step, and Marble. KStars was actually demonstrated by Jason Harris, its main developer, who showed how educational KStars can be. KStars should be in every science classroom, and thanks to the release of Qt 4 for Windows and Mac, we will hopefully see that happen in the future. As you can imagine, this would bring many new users and developers, and the KDE-Edu people are very excited about that.</p>

<p>Next up was the <a href="http://amarok.kde.org/">Amarok</a> talk, where Jeff Mitchell explained how Amarok wants to help us all to "rediscover" our music. Amarok 2.0 aims to redesign their most appreciated feature, the context browser. Currently, it is a HTML-based view, and there are some issues with that. It is not incredibly fast nor without rendering glitches, and it is not enormously flexible either. So something new was needed. It was decided to base the new Context Browser on Plasma. It was easy to use for Amarok developers, and is fast and flexible. And Qt 4.4 will introduce QtWebkit, so that they can still render HTML if they want.</p>

<p>Another area where Amarok 2 is focused is on hardware support. The current hardware support implementation is pretty complex, and still doesn't always work. KDE's media manager had some limitations, and Amarok had a lot of complex, custom code to be able to handle more than just generic storage devices. They are closely working with the Solid developers to ensure that Solid delivers what Amarok needs. A lot of work was spent on working with the hardware abstraction layer technology to ensure that it all works properly. The third focus of Amarok 2 is portability - they get hundreds of requests to get Amarok on Windows and Mac OSX, and they want to cater to that user segment. The opinion of Amarok developers is that running Free Software on proprietary platforms is better than running proprietary software on proprietary platforms - so Amarok on Windows or Mac OSX is a good thing. At first they planned to get rid of KDE and go Qt only, but now, with KDE available across platforms, Amarok can continue in the KDE family. Phonon is an essential component in the cross-platform implementation. Another big reason to work with KDE is the friendly developer community which didn't make it a burden to contribute back, but made it fun. The Amarok developers are incredibly happy being part of the KDE community and are proud to be a part of KDE Extragear.</p>

<p>After this last talk, we were treated to cocktails, accompanied by excellent Japanese-inspired food. We spent another hour at the Google headquarters socialising, before going back to the hotel, where the fun and interesting discussions continued long into the night. And the Karaoke is always enlightening. Really an excellent, interesting and productive day for the KDE community in North America!</p>

<p>Some pictures of the Release Event are <a href="http://franz.keferboeck.info/gallery/main.php?g2_itemId=280">available here</a>.</p>