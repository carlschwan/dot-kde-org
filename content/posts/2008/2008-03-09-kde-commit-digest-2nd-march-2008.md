---
title: "KDE Commit-Digest for 2nd March 2008"
date:    2008-03-09
authors:
  - "dallen"
slug:    kde-commit-digest-2nd-march-2008
comments:
  - subject: "Thanks Danny"
    date: 2008-03-09
    body: "This is a special thanks, to you, for spending time for one hundred digests.\nWhole community appreciates this.\nThanks DannyAllen, you rock.\n\n( /me is so happy that dannya's exams went fine )"
    author: "Emil Sedgh"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "I'd also like to thank Danny for the time and effort he puts into the digest. It is always an interesting read, and I am sure the whole KDE community appreciates all of the work that has gone into it. Thanks!\n\nAmarok 2.0 is starting to look great, it makes me want to get the svn version (regardless of how functional or buggy it is). As a matter of fact, it makes me want to start contributing to Amarok, but unfortunately I currently don't have the time. Here's a thank you to the Amarok developers for the most wonderful player in existence :)"
    author: "LiquidFire"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "Yeah. I love reading the digest!\n\nThanks a hundred times!\n\n/Pascal"
    author: "Pascal"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "true!\n\nthank you Danny, I love to read the digest"
    author: "peter"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "++\n\nI always look forward to reading the digest!"
    author: "Joergen Ramskov"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "yes, just another 'Thank you Danny !'\nDo you still care to read them after a hundred digests ? ;)\n"
    author: "shamaz"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "100 thanks !"
    author: "galarneau"
  - subject: "Re: Thanks Danny"
    date: 2008-03-09
    body: "Danny, thank you very very much for all the work you do for KDE!!"
    author: "LB"
  - subject: "Re: Thanks Danny"
    date: 2008-03-10
    body: "Thanks for the Commit-Digest."
    author: "Riddle"
  - subject: "Re: Thanks Danny"
    date: 2008-03-10
    body: "I second.. Thanks"
    author: "Mike"
  - subject: "Re: Thanks Danny"
    date: 2008-03-10
    body: "Thanks Danny!!!\n\nI love these snapshots. They're are a quick and easy way to check up on KDE's status. Being 1 week behind helps even, because it feels good that all the announced stuff is fixed and even further along than in the digest. :)\n\nYay for progress!!!"
    author: "Max"
  - subject: "Re: Thanks Danny"
    date: 2008-03-10
    body: "Thanks thanks thanks for your work :p"
    author: "logixoul"
  - subject: "Re: Thanks Danny"
    date: 2008-03-10
    body: "Yes, definitely many thanks to Danny for this ongoing series of digests.\nAs a KDE User and not (yet) developper, I always am very interested in reading it.\n\nPhilippe"
    author: "Philippe"
  - subject: "Re: Thanks Danny"
    date: 2008-03-12
    body: "Just adding mine: Thanks Danny"
    author: "Francois"
  - subject: "webkit in konqueror!"
    date: 2008-03-09
    body: "Seems to me that one of the most important parts was unmentioned in the summary:\n\n\"Port the WebKit part to the current Qt 4.4 snapshot. Now I am able to surf in Konqueror with QtWebKit!\n\nIt is not really usable at the moment because there are still some glitches / missing features.\"\n"
    author: "Lee"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "\"Work on WebKit integration\" is the first thing I wrote ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "btw.. how can I use the webkit kpart in konqueror?\nI installed the library and desktop file (opensuse rpms) but haven't found out how to tell konqueror to use webkit instead of khtml\n\nany hints?"
    author: "Robin"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Just found out... renamed the khtml.desktop and ran kbuildsycoca4 --noincremental.\nKonqueror now uses webkit.\n\nGreat!\nWhile some large pages (for example www.bild.de) are unbearable slow and buggy in khtml rendering mode (rather unusable), it really flies with webkit! Great :)\n\nWould be nice to see it easily configurable to change to webkit rendering (or even make it default?)"
    author: "Robin"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "I suppose that webkit integration is not planned to be backported to 4.0.x, so by the time, should webkit kpart be good enought, it may be default, but I don't see why one would urge defaulting to webkit wile it's not close to be released ( as it is available with little tweaks too )\n\nAnyway, I'm starting to use some of KDE4 apps in my KDE3 environement, such as gwenview, dragon player, konqueror and sometimes dolphin.\nI just miss kontact before I've 99% of my computer time spent on KDE4 apps ^^\n\nThanks devs for the progress, and thanks danny for the digest"
    author: "kollum"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "I hope by the time for release kde 4.1 WebKit part will be in very good shape. I want to see what it can provide me which KHML can not. "
    author: "Zayed"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Or what it can't provide you with that KHTML can."
    author: "Anon"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "For one thing working google apps... right now gmail, reader and others don't work well on konqueror :("
    author: "amonymous"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Ah, but wouldn't you still use Konqueror, thus still being mistreated by Google's browser check?\n\nDo you think web \"developers\" will magically learn to check for engines instead of browsers?"
    author: "Kevin Krammer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Do you mean if I let konqueror pretend like Firefox all the Google applications will work smoothly ?"
    author: "Zayed"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "Yes."
    author: "NabLa"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "No.\n\nTry using gmail in konqueror sometime.  Its very buggy and not very useful.  I love konqueror, but any time I have to log into my gmail account I use firefox."
    author: "Level1"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "I haven't seen any bug reports to that effect lately.\n\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "I've found I need to spoof as Firefox 1.0.x for Gmail to work correctly.  Spoofing as 1.5 or 2.0 doesn't seem to work."
    author: "mactalla"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "Yes, but when you have the Webkit Kpart enabled, you should better pretend that your're Safari. "
    author: "blueget"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "but there is no such problem because QtWebKit allways pretends to be Safari running on Mac OS X!\n\nIn my logs I see:\n\"Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en) AppleWebKit/523.15 (KHTML, like Gecko) Safari/419.3 Qt\"\n\nIt's neat because that way all the sites work because they think its Safari on Macintosh!\n\nI wonder why Konqueror developers never thought of that?\nJust pretend to be a popular browser and sites work better...\n"
    author: "Channi"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "> I wonder why Konqueror developers never thought of that?\n> Just pretend to be a popular browser and sites work better...\n\nthey did - settings | configure | browser identification"
    author: "dr"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "And this means server statistics will show you all as Mac users instead of GNU/Linux users. I don't think that's a good idea, it will destroy our market share estimates completely. As GNU/Linux is rarely sold in retail, the web server statistics are one of the best estimates we can get."
    author: "Kevin Kofler"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "mhm, so that's the reason why apple's releasing safari on windows, to let businesses think they have a bigger marketshare than do actually do...\n\nWell, as long as the sites work properly, and I do believe developers will work better if they have to try to be compatible to three browser engines instead of two, I am glad enough."
    author: "Terracotta"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "3? did we went one step future and also exclude Opera now? :-(\n"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Opera is semi-excluded --- they have extensive features to work around broken \nsites, in fact."
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Systemsettings -> Advanced -> File Associations -> text/html -> Embedding -> Select Webkit -> Move Up (as often as needed) -> Apply."
    author: "Frinring"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Some dreaming:\n\nThis should have been an URL, so you could click on it and have the lowest possible config page opened... like system://settings/file_associations/text/html/embedding\n\nOr even better a script, that also does the needed actions\n   #/usr/bin/skript\n   Object usedComponents =\n      System.Settings.FileAssociations.text.html.Embedding.UsedComponents;\n   usedComponents.moveToTop(\"webkit\");\n   System.Settings.FileAssociations.applyChanges;\n\nSomething for another GSoC project?"
    author: "Frinring"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "There is kreadconfig (a command line tool). I've never used it (because I've never had the need of), but maybe it's worth a try."
    author: "suy"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "thanks very much, it worked.\n"
    author: "Robin"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-12
    body: "Do I need to enable an option while compiling?  I am using the last qt 4.4 from svn and kde from trunk but do not have that as an option."
    author: "JackieBrown"
  - subject: "Re: webkit in konqueror! - in playground"
    date: 2008-03-16
    body: "For anyone else that was clueless, webkit is in playground/libs"
    author: "JackieBrown"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "One thing I'm wondering: could we also solve the code duplication problem by, instead of wrapping QtWebKit in a KPart, implementing the QtWebKit API in terms of KHTML? How many APIs would need to be added to KHTML for that (if any)? Is anyone interested in trying to pull this off? In particular, are KHTML developers interested? (IMHO they should be, it'd instantly allow using KHTML in Plasma and Amarok.) The KHTML developers are giving good reasons why they prefer KHTML, so I think it could be useful to have it available as an option everywhere (even in apps coded as Qt-only, though they'd gain a dependency on KHTML that way, and the \"some KDE APIs need KApplication\" problem might come up)."
    author: "Kevin Kofler"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "I am not exactly sure of what you're getting at here. KHTML has a very rich API as is, and had it for years. No reason for apps like Plasma and Amarok not to use it (well, if there are reason, they sure as heck didn't communicate them to us).\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "nope, there are really no reasons. It's even damn easy to just decide at runtime. It works btw already pretty fine to embed KHTML into the Plasma-desktop: http://techbase.kde.org/index.php?title=Development/Tutorials/SuperKaramba#Embed_KHTML_Webbrowser\n\n:)"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "That's pretty cool. Is that just using general QObject bindings or such?\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "y, signals, slots and properties. So, KHTML is already in a very good state to use it without linking against it what makes it pretty easy to deal with that lib :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "Can you arbitrarily rotate and resize the khtml pages with that? That would be neat :)"
    author: "Anon"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "yes since it's a Plasma applet :)"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "I was impressed when I saw the iPhone commercial with webpages being zoomed in etc - I'm very pleased that Plasma can accomplish the same thing :)"
    author: "Anon"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "I believe that Plasma is about the use of QGraphics*\n\nSo is it possible to display KHTML on the canvas like Webkit ?"
    author: "phD student that should stop reading the dot"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-09
    body: "yes, it is and it works already fine. See my replies above ;)"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "So what's exactly the point in using webkit under konqueror or plasma anyway? KHTML works really well and it's quite fast..."
    author: "NabLa"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "web pages support webkit better."
    author: "Riddle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "That may be true or perhaps not, truth is that I haven't found many sites as of now that won't work on KHTML (both on last KDE3 and KDE4), but wouldn't be the answer to make KHTML better? Never done anything with webkit, however I'm under the impression that KDE devs think webkit is a bit clunky and complicated to work with. I know that reinventing the wheel is always silly, however KHTML is clean and performant, and has been an integral part of KDE for a very long while. Not to mention that webkit was based on it.\n\nMind you, the only reason I use Firefox is because of the extensions. If Konq supported them it would be my browser of choice.\n\nNot being funny, but I'm beginning to agree with others about that functionality, stability and performance is being neglected in favour of looks, seemingly ignoring the fact that KDE4 *is* quite buggy at the moment. KHTML is an example of this IMO. File management on desktop, another one. I do use KDE4 on a daily basis though. I miss, though, the rock-solidness and snapiness of KDE3.5. But that's what you get when running very new software so I don't complain and file bug reports instead.\n\nI do remember that KDE3.0 was, well, quite crap, and 3.1 was really good, while 3.2 was amazing, so I would expect the same for KDE4.1."
    author: "NabLa"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "KHTML renders a lot of sites good and it's fast, but when it comes to sites for banking, or other interactive stuff, it leaves a lot to be desired, most possibly because the sites are bad engineered. Safari OTOH has found more acceptance with webdevs and therefore renders the sites I (perhaps other people too) really need, where KHTML does not. Rendering glitches are not really a big issue, as long as the content is shown, but when you really need something interactive to work and it doesn't, it IS a showstopper, therefor three engines that are widely used would most possibly help other html-engines, (opera has the same problem as KHTML, in fact KHTML has less problems than opera, except from maybe the gmail thing), plus the fact that even in the gtk camp, devs are looking more and more in the direction of webkit instead of gecko which makes webkit a more attractive choice."
    author: "Terracotta"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "> That may be true or perhaps not, truth is that I haven't found many sites as of now that won't work on KHTML (both on last KDE3 and KDE4),\n\nI'm starting to find more and more sites where KHTML doesn't cut it for me sadly. That's mostly sites doing ajaxy things. I'd love to report those, can't find time to make a simple testcase and therefore ignored it."
    author: "Diederik van der Boor"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-12
    body: "I can report one or two for you if you post here the URLs"
    author: "NabLa"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "That's OK for WebKit as an alternate renderer for Konqueror. But why for Plasma? What's the point in depending on two HTML rendering engines in a default KDE installation?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-12
    body: "Because if KDE4 is a resource hog, everyone will want to use it!"
    author: "Anon "
  - subject: "I wish more people were asking this question!"
    date: 2008-03-13
    body: "I'd like to know the answer too!"
    author: "butterfly"
  - subject: "proof?"
    date: 2008-03-13
    body: "Are you just saying this or do you have some kind of statistics? Sounds like FUD."
    author: "anon"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "See it that way; we have the opportunity to don't only have one very good webbrowser but two of them and both are greatly supported. One is full under our control, community-FOSS and has a rocking codebase while the other is mainly commercial orientated and provided to us. So, looks for me in any case like a win-win situation :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "only that khtml will die, because people will choose webkit over khtml, and when there are no users, there are no bugreports, no devs etc.\n\nI'm not saying this is realy a bad thing, just that it is going to happen."
    author: "Beat Wolf"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "You should define which people do you mean !!\nI will stick with KHTML if it serves my needs "
    author: "Zayed"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "Probably the people that are under the impression that the websites supported by KHTML are a true subset of the sites supported by WebKit. In other words, you get all the sites supported by KHTML plus some additional which currently work under WebKit.\n\nI cannot verify or falsify claims about KHTML's and WebKit's compatability since I don't use any of the so-called \"Web 2.0\" applications and the websites I visit regurarly work well with Konqueror. If I had to hazard  a guess I'd say that some distributions shipping KDE will switch to WebKit as soon as they deem it stable hoping to get some compatibility problems people claim to have with KHTML out of the way (and thus easing support). So I doubt that the actual user's choice will play a significant role which of the two engines will be used in the future.\n\nIt doesn't also help KHTML's cause that some \"other\" developers are pushing WebKit hard either via code or by propaganda in the blogosphere."
    author: "Erunno"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "Oops, that reply was intended for Zayed:\n\nhttp://dot.kde.org/1205065133/1205072654/1205088902/1205089355/1205100860/1205102627/1205108027/1205113759/1205137589/1205141830/\n"
    author: "Erunno"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "The sheer amount of projects, personnel, testing and general use cases for WebKit far outpaces KHTML allowing KDE to leverage a lot of resources without having to have the team resources to duplicate such efforts.\n\nIt's a nice example of community re-use."
    author: "Marc Driftmeyer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "I do see the technical reasons, however, I wonder whether the KHTML guys feel left behind... Are they shifting towards working with Apple on WebKit?"
    author: "NabLa"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "No. There have been many attempts to work with Apple before, but they never worked out. Apple is good at having people work for them, not so much at working with them. And while they have some very nice and very smart people who I respect deeply working for them, they do a lot of things which, while they make sense for them, would be simply insane in a community-driven project.\n\nAnd, speaking for myself, I will not work on any project which is disconnected from the KDE community and provides no way of protecting our interests. I am not willing to hand over control to a good chunk of our infrastructure to Apple. That Qt will be under control of Nokia is bad enough.\n\nWill there be more tries to work together? Who knows. But at this point, it's all moot, since there aren't enough people interested in proper integration of WebCore (no, thin wrappers around incomplete Qt ports of out-of-date versions of the renderer don't count) for it to happen, while there are plenty of people who will spread FUD and lie to our users. I am personally open to that from the technical POV (the community issues are big, and I am not the sort of person to resolve them), but there are things I want to do myself, bug reports from our users I want to deal with, and bug reports from our users I have to deal with because it seems like no one else will. \n\nAnd to the grandparent poster: go use IE. It has the best financial backing in the business.\n\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "> I am not willing to hand over control to a good chunk of our infrastructure to Apple\n\nisn't the LGPL supposed to take care of that?"
    author: "Patcito"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "A lot of people believe that, but it's simply not true in 99% of cases. You need people to make things happen, they don't show up out of thin air. \n\nkdeprint was LGPLd, too, but there wasn't an active community around it, so when the main driving force behind it left, it was orphaned, and then abandoned entirely in KDE4. The result is that KDE4 has made a huge step back in printing functionality. \n\nWhat some people want to see happen with QtWebKit, and in particular the way they are pushing it, make a chance of any KDE community connection with it close to nil, which means things will soon bitrot, and if one of the big corporate pushers has a change in priorities, we'll be pretty screwed.\n\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-12
    body: "That's what I would imagine. Personally I'd continue work on KHTML and stuff out Webkit. \"Just\" port over good features and the like. What I mean is that KDE has got total control over KHTML, its quality is very high and improvements can be made. I don't know about the quality of the source code as I'm not fit to judge that. But I keep my word on that I've yet to come to a site kthml can't handle since... well, probably KDE 3.4.\n\nNot sure if some parts of the KDE project realize that KDE is made by people and people, as such, can get demotivated, angry, etc. If I was a khtml dev I would feel as awkward about trying to shove in Webkit as a kdelibs guy would feel if a different libs infrastructure was pushed in too.\n\nHow is Webkit going to be maintainable at all if it ain't provided by apple as a maintainable piece of software?\n\nSo I ain't a KDE dev, but the more I think about Webkit and KDE, the less sense it makes. I think whoever's working on Webkit kde is just wasting time they could be using into something more productive (let it be improving KHTML itself, or getting on with their own lives). The \"choice is good\" argument is a bit weak here. I've got a choice already: Firefox."
    author: "NabLa"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "What I'm getting at is that they're written to use the QtWebKit API for whatever reasons, so if we want them to be able to use KHTML, we have to either patch them or emulate the API they're using."
    author: "Kevin Kofler"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "They most likely decided to use QtWebKit because it is the native rendering engine for Mac OS X widgets so they are more likely to work.\n\nAnd there are more developers and users for WebKit, and TT supports it. Once some technology moved from KDE to Qt, why support it ourselves any longer? Personally I don't see use for that, even though there are some minor differences between KHTML and WebKit..."
    author: "jos poortvliet"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "> why support it ourselves any longer?\n\nand this is what I don't understand. Why do ppl who actually don't work on something believe that they are able to deny others to work on it too? Shouldn't such a decision be up to those who do the job? One of the great things KDE is about is to \"be free\" and afaik the \"those who codes decides\" rule is still valid. Also there is just no reason to go with A or B since we can have both and somehow that even matches the \"free to choose\" way I love so much in KDE.\n"
    author: "Sebastian Sauer"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "Since KHTML is part of kdelibs it will have to be supported until KDE 5 as far as I understand it. And the next major revision is years away so it's probably in the best interest of KDE to stay on the good side with the KHTML developers since their loss might not be easily replaced judging by the complexity of the KHTML codebase."
    author: "Erunno"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "Not really, if khtml development really dies, it wouldn't be that hard to make the kthml API just wrap the webkit API (which we get for free from QT).  There would be some extra overhead to doing it this way, but not much - you can already choose webkit or khtml, so most of the time this wouldn't even be used as we can make the default webkit.\n\nWhat would be really interesting is to separate khtml out (like webkit was once separated from khtml), and get some of the gnome guys working on webkit into khtml.  I'm not sure if this is possible, but it would be really interesting.   If Apple decides to close up webkit just a little bit (I'm not sure if they can anymore) khtml might be the way to go.    Perhaps we can even make khtml an alternative plugin for safari browsers."
    author: "Hank Miller"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "I stand corrected then. But 'm curious if such a wrapper wouldn't break ABI compatability which is also guaranteed until KDE 5. I confess that my C++ knowledge is medicore at best when it comes to compiler voodoo."
    author: "Erunno"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "The QtWebKit API is only a tiny portion of what KHTML provides. And if it weren't, what makes you think anyone would do it? In fact, if people were actually willing to do work, instead of spreading FUD,  you might have seen a libkhtml based on WebCore already.\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "\" In fact, if people were actually willing to do work, instead of spreading FUD, you might have seen a libkhtml based on WebCore already.\"\n\nIs this still possible in 4.x while maintaining BC? What kind of skill level would be required for someone interested, and roughly how much work would you say it was?"
    author: "Anon"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "For most classes, BC is trivial, for things like KHTMLPart it's not, but doable.  The hard part, and the incredible tricky kind of work, is making it integrate properly. That means, for example, that a web page should still be able to embed, and script KMPlayer, that all the settings are followed, etc, that the KIO and KParts hooks are still in there, etc.\n\n\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "Perhaps it is even an advantage of WebKitQt that it does not use KIO with its excessive reliance on DBus-based IPC. I've got a notion that Konqueror feels exspecially sluggish (in comparison to other browsers) when it has initiated a lot of KIO Slaves.  "
    author: "ac"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "KIO doesn't use DBus for communications with an I/O slave.\n"
    author: "SadEagle"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-12
    body: "Yes, in this case \"socket-based IPC\" would be more accurate. Nevertheless, if  Konqueror's sluggish behavior on some sites is not caused by the rendering engine but by KIO, the KDE bindings of WebKit you are proposing (as opposed to pure Qt bindings in QtWebKit) would not be of much help.          "
    author: "ac"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-10
    body: "I stand corrected then."
    author: "Erunno"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "This is the strangest situation I have ever seen in a community project. \n\nSomeone demonstrably hostile to our interests has a neat shiny thing and we shun a community based project for one controlled by interests at best undefined (Nokia) to allow loading third party widgets into a single process user interface.\n\nDerek"
    author: "D Kite"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "> interests at best undefined (Nokia)\n\nand for the goals, it suffice to read the announcement at http://trolltech.com/webkit/webkit-announce to get a glimpse of the new corporate focus:\n\n\"Announcing the Qt WebKit Integration\n\nTrolltech's Qt WebKit Integration brings Web 2.0 services to *mobile phones*\nHelps *mobile operators* and *handset manufacturers* enrich *phone* applications with live web content such as online maps, music stores and instant messaging\" (emphasis mine)\n\nQuick, what is the missing keyword in this announcement? ;-(\n"
    author: "S."
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-11
    body: "Nokia worked on Webkit long before Trolltech was involved in it. Long before Apple 'opened up' development to the free software developers that created the original codebase.\n\nNokia's interests towards KDE are undefined.\n\nDerek\n"
    author: "D Kite"
  - subject: "Yes! It is silly!"
    date: 2008-03-13
    body: "Unfortunately, there are some in the KDE-world who have decided for some arbitrary reason that a beta QtWebKit port is somehow better than what we already have. I say arbitrary, because they haven't done development work on either one.\n\nAnd also unfortunately, they are very good at propaganda.\n\nUsers lose."
    author: "anon"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-13
    body: "Put that way it really does sound outlandish.  Unbelievable in fact.  "
    author: "MsmiysOtaru"
  - subject: "Re: webkit in konqueror!"
    date: 2008-03-13
    body: "FFS I can't even type my own username"
    author: "MamiyaOtaru"
  - subject: "key phrase"
    date: 2008-03-13
    body: "\"It is not really usable at the moment\" is probably the key phrase."
    author: "guntherb"
  - subject: "Return of kiosktool?"
    date: 2008-03-09
    body: "Its great to see the return of kiosk tool. Hopefully it will get the love that it deserves. "
    author: "xian"
  - subject: "Re: Return of kiosktool?"
    date: 2008-03-09
    body: "It is very important tool in the corporation environment . It deserves more."
    author: "Zayed"
  - subject: "Re: Return of kiosktool?"
    date: 2008-03-09
    body: "+1 from me."
    author: "Kerr Avon"
  - subject: "usability  and stability"
    date: 2008-03-09
    body: "In the last years a few companies and municipal authorities migrated to OSS.\nIn one of those, I am responsible for the decision to prefer KDE to other \ndesktops. We needed a stable, customizable, feature-rich but functional\nenvironment, and KDE3 seemed (and really was - in my opinion) the right choice.\n\nAfter quite a long while of observing the KDE4 development with anxious hope, \nfear and - sorry - disappointment, it's pretty clear that this KDE4 will never fit \nour needs. Ok, no one to blame for. But just to mention, after making dolphin \nthe default file-manager, it was rather 'demagogic' to claim that konqueror will \nstill be available, as although konqueror is still there, it seems it uses now\nthe file-manager-part of dolphin. No \"View Mode/Tree View\" anymore! A not so unimportant \nfeature at all, for those to have handle large amounts of documents.\n[ http://aseigo.blogspot.com/2007/02/konqueror-not-vanishing-news-at-11.html ]\n\nThe default start-menu, space-wasting borders, icon-handling on the desktop, \nsystem-settings replaced kcontrol - thats all about usability, but let alone \nplasma-stability and performance ...\nSo is there any chance to get a KDE4 (maybe a fork) without those plasma?\nWith a Konqueror with fully functionality? \nBecause - the hope dies at last - and, although I'm not a programmer, \ni think the other underlying changes sounded very promising. It would be \ntoo bad that those were overlayed by wrong decisions related to the \"front end\"."
    author: "h.steen"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "> although konqueror is still there, it seems it uses now\n> the file-manager-part of dolphin. No \"View Mode/Tree View\" anymore!\n\nA tree view is already available again in trunk -> will be part of KDE 4.1 :-)"
    author: "Peter Penz"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "Yup, the tree-view in every single window (including split-windows) was one \nof those outstanding features that made konqueror (and in succession also KDE)\nthe tool/environment of choice. But - stay patient - there will be a 4.1 ..."
    author: "h.steen"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "... if i only could wait so long ;-)\nthanks for the good news Peter!"
    author: "h.steen"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "\"After quite a long while of observing the KDE4 development with anxious hope, \n fear and - sorry - disappointment, it's pretty clear that this KDE4 will never fit  our needs. Ok, no one to blame for.\"\n\nSo, you're basically judging the whole of the KDE4 series, which has at least 4 years of development still ahead of it, by - at best - it's 4.0.2 release, which is basically a series of bug-fixes and a couple of tiny features added to the initial 4.0.0? Seriously?\n\n\"So is there any chance to get a KDE4 (maybe a fork) without those plasma?\"\n\nWhat possible reason could you have to do this? You do realise that the architecture of Plasma will make it so much more configurable and flexible than KDE3 that it will make Kicker and the old K-Menu look pretty sad and rigid in comparison? Heck, once it's matured a bit, it probably wouldn't take more than a few weeks to \"clone\" the old Kicker *and* all of the old Kicker applets in the Plasma framework.  Maybe even days.\n\n\"With a Konqueror with fully functionality?\"\n\nWhy on earth would you fork KDE4 to add features that are *already planned* by the devs? Both Peter and David have stated on numerous occasions that they don't want Konqueror to lose any of its KDE3 features as a result of embedding the Dolphin KPart.  Any \"fork\" would be better off just contributing code to the mainline Konqueror.\n \n\"In one of those, I am responsible for the decision to prefer KDE to other \n desktops.\"\n\nThat's great, but it would be nice if, instead of posting alarmist FUD here on the Dot, you'd spend a few minutes doing some research on KDE4, its current status, and its goals :)\n"
    author: "Anon"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "To keep things cool - after looking at 4.0.2, a 2nd maintainance release-version, i think there is much reason to \nto be honestly in doubt about the stability and usability. \n\n> You do realise that the architecture of Plasma will make it so much more configurable and flexible \n> than KDE3 that it will make Kicker and the old K-Menu look pretty sad and rigid in comparison?\n\nThat really isn't _obviously_, looking at the current state. \n\n> Why on earth would you fork KDE4 to add features that are *already planned* by the devs?\n> Both Peter and David have stated on numerous occasions that ...\n\nHow should a user know that? A greater deal of the users and adopters don't ever read \nthose mailing-lists and digests. Do you seriously mean they have to, to are \nallowed to judging a software? \n\n> ... doing some research on KDE4, its current status, and its goals\n\nI did, and probaly missed important things - but thats not the point. Even if i would be convinced,\ni cannot expect that others are by reading announcements. In the end those will be judging by looking \nat the current state of the subject matter. And here again goes the first sentence.\n(maybe something went wrong with the release schedule?)\n\nNevertheless, good news, thanks for informations."
    author: "h.steen"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "I think a lot of the goals and long term strategies can be found on techbase.kde.org which will help you get that research done so you know that certain currently missing features will get addressed and thus that you can plan on that."
    author: "Thomas Zander"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "\"To keep things cool - after looking at 4.0.2, a 2nd maintainance release-version, i think there is much reason to \n to be honestly in doubt about the stability and usability. \"\n\nAgain, you're assuming you can predict what KDE4 will be like in several years based on the first *two months* of development after its initial release.  This just doesn't make sense.\n\n\"That really isn't _obviously_, looking at the current state. How should a user know that? A greater deal of the users and adopters don't ever read \n those mailing-lists and digests. Do you seriously mean they have to, to are \n allowed to judging a software?\"\n\nYou can judge KDE4 as it is right now by using it - that's absolutely fine.  You can not judge what KDE4 will be like several years down the line without finding out some of its goals.  Your comment that Konqueror will never have its KDE3 functionality back without a fork is a perfect example of this - your judgement was wrong or, at best, hugely premature.\n\n\"I did, and probaly missed important things - but thats not the point. Even if i would be convinced,\n i cannot expect that others are by reading announcements. In the end those will be judging by looking \n at the current state of the subject matter. And here again goes the first sentence.\n (maybe something went wrong with the release schedule?)\"\n\nThe release schedule lead to a very rough and unfinished release, just like all projects that made a major break from their existing codebase (KDE2.0, GNOME 2.0, Linux 2.6, Apache 2.0, etc).  What's going wrong is, again, that people are judging the entire future of KDE4 based on its initial release.  Put it this way: If you had made a decision of whether KDE would ever fit your needs based on the horrific KDE 2.0.0, you would probably never have recommended it, and your clients would never have used KDE3 which, based on what you have said, they are quite happy with.\n\n\"Nevertheless, good news, thanks for informations.\"\n\nYou are welcome :)"
    author: "Anon"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "> How should a user know that? A greater deal of the users and adopters don't\n> ever read those mailing-lists and digests. Do you seriously mean they have\n> to, to are allowed to judging a software? \n\nSorry Mr. Steen but if you an IT integrator and are responsible for deployment of FL/OSS desktops, then you HAVE to read \"those mailing lists and digests\" (and blogs, and maybe even commit logs), because this is a source of inormation in the FL/OSS software. I'm sorry but it works this way. If you are used to different methods, stay with your current solution."
    author: "Vide"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Your advice is welcome - though, once again - I did. At least to a certain degree - \nI've hardly ever read commit logs. I thought that would be evident by posting here \nquoting a year old blog. But never mind."
    author: "h.steen"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "> How should a user know that?\n\nA user does not have to know that. A user will not see KDE 4 until their distro packages it as the default KDE, and that will happen when KDE 4 is mature enough."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: usability  and stability"
    date: 2008-03-11
    body: "Wasn't the decision to release that early motivated by the expectation to get more users on it?\n\nBtw. that would apply to all software. So what do you think are release schedules for? There would be no reason anymore to mark versions as alpha, beta aso. because ofcourse the distros will judge and handle the versions corretly.\n\nThats silly man!"
    author: "Martijn"
  - subject: "Re: usability  and stability"
    date: 2008-03-11
    body: "I think the expectation was to get more of the technically interested power users try to use KDE 4. It was said many times that it is not aimed at regular end users as a replacement of KDE 3 yet."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "\"KDE4 will never fit our needs\"\n\nIt does not fit now. How do you know that it never will?\n\nYou can switch to the traditional start menu, switch the widget style back to plastik/windows/whatever. AFAIK better desktop icon handling is planned for 4.1 as well as the old kicker functionality in Plasma, so these problems are likely to be solved as of 4.1 or in the worst case 4.2.\nWhy do defaults matter so much? In a large deployment you can set the user defaults anyway and make those the default settings for every user on every computer."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "Thats an eligible question.\n\nIn (not only) my opinion, those changes to the defaults (the default applications (konqeror, kicker, kcontrol are good examples) \nbut also seemingly little usability-related things like widget- icon- or border-sizes and handling) were not made randomly. \nSo we arrived at the conclusion, that KDE's target audience had changed. That in companion with the (ok, got it - currently) \nunsatisfying stability led us to that 'never' - Of course i hope we have to reconsider that ;-)"
    author: "h.steen"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "You're not the only one.\n\nI find the fact that any work at all is being done on eye candy before the thing is fully feature complete and stable quite disturbing.\n\nI think the natural inclination for a lot of people is to think that \n\nLuckily, debian Lenny is scheduled to release with 3.5 and support it for three more years so I'm not too worried.  \n\nI expect slackware to keep it around well into the next decade as well.\n\nAs annoyed as I am that KDE chose to use its own definitions for alpha, beta, and release candidate, it's the distros that are planning on depreciating KDE3 before KDE4 is feature equivalent that are really running full speed off a cliff here.  Those of us who use our computers to do work and don't give a damn about bling will switch to whatever distro still supports 3.5 (or buy macs).\n\n"
    author: "Velvet Elvis"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "finishing this thought...\n\n<i>I think the natural inclination for a lot of people is to think that</i>\nif work is being done on silly stuff like and icons and desktop effects, the important parts must be finished.  You don't paint your house until you've got all the siding on it.  Unless you're KDE.  Then you do.\n\nIt really makes me worry about the priorities of the KDE project.  And yes, if the target audience is people who care more about what their desktop looks like than how functional it is, I'm done with it.  \n\nI really think there wouldn't be nearly as many complaints about missing functionality right now if there had not been as much effort to make KDE4 outwardly look like a finished product."
    author: "Velvet Elvis"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "maybe the people who do the artwork are not the ones who (can) do the programming"
    author: "pvandewyngaerde"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Oh so true. Besides, we do NOT paint the walls until the building is done. Plasma has only 1 theme, which is very basic and doesn't even look so great. The panel lost it's temporary (horrible) artwork only a few weeks before the final release of 4.0. So what's all the eyecandy work? The parent might think the artwork is a lot of work, but he's wrong - it's not. Plasma does good looks by default, so if you just work, it will look decent. In other words, the dev's DON'T spend much time on eyecandy - it's just looking good cuz the candy is almost free in KDE 4. Not entirely, true, but almost :D"
    author: "jos poortvliet"
  - subject: "Re: usability  and stability"
    date: 2008-03-12
    body: "In that case, please consider my complaint as a compliment.\n"
    author: "Velvet Elvis"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "Well, I'm a bit in the same situation as I have 6 desktops running with fully loaded KDE and Konqu as filemanager and browser in a business here. It's just not wise to switch to KDE4 anytime soon (as much as I would like to). It needs to mature at least until 4.1 (better 4.2). What I'm particularly disappointed about is that the PIM apps won't be back until 4.1 (and probably even 4.2). Still, this is like 6 months - 1 year time from now, which is sad, because I'd really like to use Konqueror (Browser) from the 4 series now.\nAs everybody uses icons on the desktop for temporary files (like opening files from emails for editing), it's even sad to see, that it's simply not usable in KDE4 (and it's still not working in trunk). \n\nThe icons can't be dragged from the desktop to a dolphin view. It's not an icon, it's a Plasmoid which can be rescaled and rotated, but the file operations are not possible anymore. (And users often drag pictures/photos from the desktop to OO.draw for quick printout with the company logo on the page. This is not working anymore!) It's supposed to be a completely new way of using the desktop, but the downside is that it removes a fair amount of funtionality everybody got used to....!\n\n\nI can use the PIM apps from 3.5.9 with 4.0.2 but that still does not solve the problem with the desktop icons... Damn, me needs to wait some 1/2 year more to upgrade the desktops..."
    author: "Thomas"
  - subject: "Re: usability  and stability"
    date: 2008-03-09
    body: "i second this. But i'm not worried. Adding the files features to those plasmoids should be easy i think. The most important that has to be done is to remove the right click menu of the icon plasmoid, and replace it with the normal file right click menu."
    author: "Beat Wolf"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Huh? usability, work flow? come on dude, now you can instead rotate every single \ndesktop-icon! and every single desktop icon even got its own neat tool-bar \n*** i'm dreaming of an icon's menu-bar *** "
    author: "Niklas"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Seconded. The current 4.0.2 desktop is unusable for many professional users, and disappointing to many long-time KDE3 users. Unfortunate, but true to many of us."
    author: "Victor T."
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Of course, there are others, like myself who disagree.\n\nI have long forsworn using the desktop as a junk pile for files. Instead, I keep files organized in folders like ~/tmp or ~/Documents/Downloads. I don't know what qualifies as a \"professional user\" in your mind, but I don't find using the Desktop as a file directory very professional. \n\nI agree with the KDE 4 idea of the desktop as a place to present information. I currently use SuperKaramba on my KDE 3 desktop to display post-it-note reminders, current weather conditions, and what processes are currently running.\n\nIn fact, I have an old blog entry in which I rant about the problems with the Desktop paradigm (http://kwilliam.blogspot.com/2007/06/thing-about-desktops.html). I pride myself for being rather visionary with that post; I almost predicted Plasma.\n\nAlso, anybody know why I can't use HTML in this post? The only option I get is \"Plain Text\". :-/"
    author: "kwilliam"
  - subject: "Re: usability  and stability"
    date: 2008-03-11
    body: "Sure, it's all nice and cool if you don't like using the desktop as a temporary file space (many hobby users even use it as a storage place), but most people do, and they do so on all OS and desktop enviroments. And you can't change them, it's simply the way we work with computers since many many years.\n\nSure, you should be allowed not to use the desktop as a temporary storage place, but you should also be allowed to do so."
    author: "Beat Wolf"
  - subject: "Re: usability  and stability"
    date: 2008-03-13
    body: "replied to for truth.  It's a perfect holding bin for things I need to get done.  It annoys me to see them there so I do them.  /tmp just isn't the same.  \n\nOverall I'm just hugely underwhelmed with plasma.  Each commit digest we here stuff like \"now we can drag from the desktop to the panel.\"  \"Now we can drag from the menu to the desktop!\"  It's like each possible combo of source and destination has to be coded separately.  For all that, it's hard to believe it's running in a single process.  I look forward to future changes that will allow me to drag from the menu to the panel, or from the old kmenu style menu to the desktop or the desktop to the second panel or whatever."
    author: "MamiyaOtaru"
  - subject: "Please NO Fork!!!"
    date: 2008-03-10
    body: " I know that Plasma still isn't anywhere near useful. That's no reason to give up on it though. See if can get some of those companies or municipal authorities to give back to the project which software they are using and DONATE!!!\n\nThink about the money you saved, divide it in 1/2 and donate that to the KDE project. Then they can hire more full time programmers and make KDE better and more like what you want. \n\nAlso Plasma is the best thing that happened to KDE. I wouldn't want to give that up. There is no equivalent to Plasma anywhere else in the open source, or closed source world. With enough momentum and critical mass tons of things are possible with Plasma.\n\nThe huge borders and screen real estate wasting is unfortunately \"in style\" and \"hip\" now. Bugs the crap out of me. Look at Vista as an example. Their reasoning was that it's \"easier for beginners to grab the borders of windows to resize them\". I'm not making this up, that really was their reasoning. Same reason Apple decided to make the icons in the \"quicklaunch\" so huge. They figured it's easier to click and see them for beginners.\n\nUnfortunatelly just because our monitors got bigger over the last few years, people decided to waste the \"newly gained land\". So now it takes a 22\" widescreen to get the same \"computing power\" that was possible decades ago with a 14\" 4:3 monitor. I wish it wasn't so. Unfortunatelly I'm the only one it seems that want's to \"SAVE DESKTOP REAL ESTATE\". \n\n\"KEEP DESKTOP REAL ESTATE FROM BECOMING AN EXTINCT SPECIES. DONATE NOW!!!\". \n\n\"SUPPORT SCREEN REAL ESTATE, OR YOUR KIDS WON'T HAVE ANY WHEN THEY GROW UP\".*\n\n\n\n_____\n* Yes, this is a bit cynical, but I'm trying to drive home a point!! Stop making all icons, Windows, Titlebars, Taskbars, Window borders so damn huge. Pleeeeaaassee!!!\n"
    author: "Max"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "Please don't fork KDE. That would be the worst thing to happen to the KDE environment.\n\nWe're already so fragmented against Gnome and others. I'm not even speaking about the mass appeal of OS-X and Windows. We need to gain critical mass, and that can only happen if KDE stays united.\n\nIt's built to be modular enough already. No need to fork it.\n\nAnd stop with the Konqueror worshipping. Those are old times. Konqueror was in the past. It's still there for legacy reasons. Dolphin is the future.\nWe have to support it, or we won't have a core for KDE. It will get better. But only if people will use it.\n\nIf you need to use a webbrowser use Firefox, or similar. They are much better than Konqueror ever was at \"just working\" with fancy features anyways. :)"
    author: "Max"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "If I wanted nautilus I'd use Gnome.\n\nFor that matter, If I wanted to drink any of the Havoc Pennington koolaid I'd use gnome.  Right now the biggest threat to the open source desktop is the cult of self-styled usability \"experts\" who haven't taken a single HCI class.\n\n"
    author: "Velvet Elvis"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "To make that clear. Breaking apart the browser and the file-manager part isn't the point. The point is, that the latter now lacks some important capabilities. It was often argued, that konquerors old source-code wasn't to maintain anymore. \nSure - the code of a less capable tool always (mostly :-)) is cleaner. But it matters how it looks after the tool is comparable feature-wise (if it ever will...)\nOften implementations of last and apparently small features make the difference in code-beauty.\n\nBtw. i think this applys also to a lot of other \"programs\" that were abandoned in favor of easier to maintain ones besides konqueror/dolphin. I would be rather curious how the traditional kicker-, menu-, applets-framework relate to their plasma-successors compared by 'code-cleanliness' and 'maintainability' when the latter will be as usable, stable and configurable as the first are. Of course, you could always raise such objections, but there are oviously some particular reasons to do it here now. Was it back in 2005 when i first heard of the plasma-desktop-revolution? And when i look at it now in 2008 - \n\nBtw. konqui still is my preferred webrowser (khtml isn`t bad at all), and dolphin? Youre kidding right? But some posters above proved me that konqui as file-manager isnt only \"... still there for legacy reasons\". So dont spread those nonsens. \n \n\n"
    author: "Jaros on Monday"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "Please go back to older digest and other dot article about konqueror and dolphin\nPlease read those articles"
    author: "phD student that should stop reading the dot"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "Firefox in Linux is terribly *slow* and a PITA, Konqueror/KHTML is much faster and I use Firefox only for some stubborn websites out there. Viva Konqueror/KHTML!"
    author: "fred"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "Please don't for KDE.\n\nDevelopment is already all over the place as it is.\nI wish more development was consolidated and not divided among a million projects.\n\nInstead of working on Konqueror. Just add more features to dolphin. It does all I need a file manager to do.\n\nDon't get rid of Plasma either. It's what makes KDE 4 so much fun.\n\nI can't wait for more desktop effects and more Plasma eye candy.\n\nFrom what I gather Plasma also allows for easy cross platform applications that can run right on the desktop. I like that feature.\n"
    author: "Mike"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "so why the hell not stay with the concept of having both (at least...) filemanagers in KDE?\ndo you really mean adding features to dragon player and get rid of kmplayer, Kaffeine... would be the right way?\n\n## It does all I need a file manager to do. ##\nif the simpler versions fit your needs - okay, use it. but you have to accept that there are use-cases completely \ndifferent from yours and that it is a legal interest to use a desktop for a daily work with requirements unlike yours.\n \n\n"
    author: "Jaros"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "What the hell do you think I use KDE for? Games? *lol* At least not until major game developers acknowledge Linux's existence.\n\nIt's purely used as a work machine. \n"
    author: "Mike"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "maybe you wont believe it, but let me asure you - even jobs can have completely different requirements. - must have, considering your posts ;-)"
    author: "Jaros"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-13
    body: "guess what? thanks to the magic of kparts, developers can add features to both dolphin and konqueror at the *same* *time*. they share code. everyone can be happy regardless of which program they use - just give the developers some time to bring back the lost features. there are only so many hours in a day, and most of us aren't even getting paid for this :P"
    author: "Chani"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-11
    body: "Why not?\n\nKhtml was forked and it was the best thing to happen to KDE.\n\nMaybe plasma (or the desktop) should be forked for the same reason. Developers that are not cooperative will eventually go away. Then all will be well.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-12
    body: "Huh?\n\nYou are trying to push the plasma devs away?"
    author: "Anon "
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-11
    body: "It was told many times that the feature set of Dolphin is supposed to be a proper subset of Konqueror's for better usability but Dolphin is not a replacement of Konqueror as a file manager as it is not supposed to have important features of Konqueror. So dropping Konqueror would be a huge feature-loss for KDE for many KDE users, and things like that would be definitely the things that would mostly fragment the community and cause forks. However, the problem about which h.steen wrote was that Dolphin (and Dolphin part in Konqueror) missess some features of the Konqueror 3 file management views. But Dolphin those missing Dolphin features are coming. And what is great in Konqueror: Konqueror develops because Dolphin develops, without any special work on Konqueror, because it uses the same file manager views.\n\nThere is no such thing like \"better\" (comparing Firefox and Konqueror as a web browser), just better for you, or me, or somebody. For you, FF is better, OK. For me, and some other peolpe Konqueror is better. I would hate to use a non-KDE application for such a daily task when I only use KDE applications otherwise. As it is non-KDE, besides looking different, it starts slower, consumes more memory, it cannot use kio slaves, it cannot include kparts, it cannot store passwords in kwallet, it doesn't open files with KDE's file associations etc."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "No getting around it.  Konqueror must be forked."
    date: 2008-09-19
    body: "[quote]And stop with the Konqueror worshipping. Those are old times. Konqueror was in the past. It's still there for legacy reasons. Dolphin is the future.[/quote]\n\nIs this the part where we all stand up and sing \"Tomorrow Belongs to Me\", like the Hitler Youth from Cabaret?\n\nThe deal with Konqueror is that capabilities that were redundant in KDE made it an awesome addition to a window manager like fluxbox or fvwm.   The tragedy is that to really understand Konqueror, you have to use it seperate from KDE.  So, naturally, with KDE 4, Konqueror has become devalued, diluted, and disabled.\n\nDolphin is actually quite wonderful.  When I say it looks and feels like a really good microsoft application, that's not intended as an insult. It's a beautifully thought-out response to a specific set of tasks.  Konqueror, as it used to be, is a completely different concept: a versatile and powerful,  comprehensive application that challeges the user to invent new uses and applications.  Dolphin is so brilliantly user-friendly, it runs itself.  There is no creative input.  It's easy, but in my opinion, it's no fun.\n\nAnd you're correct.  It's only being kept around for legacy purposes.  The awesomest desktop application ever is being gelded and put out to pasture, because the new KDE is all about a planned, managed desktop experience.   That's all well and good, but KDE is no longer the place for Konqueror.   Konqueror is about creativity, and writing your own ticket.  To see Konqueror play a supporting role to Dolphin is like watching the Sex Pistols open for the Jonas Brothers.   \n\nThis will not stand.  As God is witness, this will not stand.   "
    author: "blackbelt_jones"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-10
    body: "> huge borders\n\nchange your theme then. there are two plasma svg themes i know of that have no borders to speak of."
    author: "Aaron Seigo"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-03-15
    body: "Offering a KDE4 port of the KDE3 DeskTop (hopefully with some of the major bugs fixed) would not be a fork.  With perfect hindsight, it appears that this might have been a good idea.  However, now, we would have to consider how long this would take and how long it will take till Plasma starts to be usable."
    author: "JRT"
  - subject: "Re: Please NO Fork!!!"
    date: 2008-11-28
    body: "PLEASE DO FORK!\nThe Problem in KDE 4.x is not the features or the readynes, but the mere ugliness. Why in the world kick something so beautiful as KDE 3.5 for that Vista clone?\nWhy are nerds so eager to have everything fotorealistic, like in an Egoshooter. I dont like the glas, water, whatever real light and shadow look, its just like the ugly new cases PC's have. They look shiny but cheap, basta! \nI like the more organic looking things an on an completly artificial thing like a desktop that would be the more comic like look of kde 3.5 (used with primary, or mono icons and the warm ubuntu colors ist just cosy).\nKDE 4.x looks cold, clinical and cheap!!! \n\nNot to mention that a lot of good and configurable apps have been changed to pseudostylish crap.\n\nA fork is the only way to protect Kde for me and I thing a lot of other people. And Gnome ist just like looking back in time on older KDE Versions.\n\n"
    author: "Florian"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "truly, i`m not up to offend someone personally, but to me it appears, that the KDE4-GUI (Plasma) approach \nwas made by web designers or at least with the very same intentions web designers probably have. \n\nThe site may look nice, maybe fancy flash animations make you wow!, but as soon as you want/have to use \nit more than once, you'll find out, that navigating around takes you more clicks than actually needed, \nfind a specific content is needless circuitous and so on. In the end using it gets soon boring :-/ \n\nFor a website that may be sometimes bearable as a playground, you don't have to drop by if you don't want to, \nbut apparently it was forgotten that a Desktop isn't for _occasional_ use. Simply just the _exact_opposite_ is true."
    author: "abramov"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "i've definitely disagree to this. designers wouldn't do that. because it's ugly too!"
    author: "Paulo"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: ">> space-wasting borders, icon-handling on the desktop\n\nI really hope that will change soon! I'm running the latest trunk at there is still so much white space around everything! Oxygen is just a waste of space and I hope that this will change as soon as possible so that everything else can adopt these changes.\nAn the icons on desktop need changes too. I saw that they know about it (black background, borders) but they didn't have time to change that yet."
    author: "Michael"
  - subject: "Re: usability  and stability"
    date: 2008-03-11
    body: "I've been saying this for months.  Amazingly enough, today's Oxygen is much better than it used to be, and there is still quite a bit of wasted space."
    author: "T. J. Brumfield"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "I do agree in some level to the criticism.\nI was even thinking that a pure-qt desktop would be a good idea (no more kdelibs).\nKDE 4.0.2 is very buggy (drag and drop on dolphin crashes it) and incomplete (double click in desktop icons does not work).\n\nBut I foresee that KDE 4.2 (a little) and 4.2 (mostly) will fix those problems. So the real thing resumes to what most people says: 4.0 should had been named 3.9 or 4.0-dev. 4.1 would be a nice 4.0 and then things would be really good at .1 release.\n\nI hope KDE developers start accepting that they made a mistake at some point, because just saying \"we had to release to bring more developers\" is not working anymore :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Probably.\n\nEither way. It's done now. Hoefully they'll get it done in time for 4.1 to come out. It's only a few months away and I'm getting nervous. There is still so much to do.\n\n"
    author: "Mike"
  - subject: "Google Summer of Code - it'll help"
    date: 2008-03-10
    body: "I said it before. I'll say it again:\n\nPlease add projects to Google Summer of Code.\n\nThan please tell everybody you know about it. The deadline is approaching... FAST.\n\nIf you want changes to KDE, GSoC is a really good way to do it."
    author: "Max"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "\"Hoefully they'll get it done in time for 4.1 to come out. It's only a few months away and I'm getting nervous. There is still so much to do.\"\n\nIf you're expecting 4.1 to solve every issue with 4.0, you're in for a disappointment."
    author: "Anon"
  - subject: "Re: usability  and stability"
    date: 2008-03-10
    body: "Hm, plasma itself is one of the \"underlying changes\" ... but nevertheless i feel with you, actually i would also like we get rid of this 'plasmaism' as soon as possible. But like every other \"...ism\", you can count on it's instinct of self preservation "
    author: "Quentin"
  - subject: "Re: usability  and stability"
    date: 2008-03-11
    body: "Perhaps that \"all-the-desktop-stuff-belongs-to-plasma-or-depends-on-it\"-mantra isn't just the unix way to solve tasks with collection of *seperate* and *independent* tools. seems me there is some evidence that at least stability would benefit from such independence - thats why former kde versions were much more stable, even their early versions. buggy yes i remember, but not like now.\n(sorry for my bad english)"
    author: "artjom W."
  - subject: "Keyboard access to the start menu?"
    date: 2008-03-09
    body: "Is there a way to open the start menu from the keyboard? The \"search\" there is just what I need to start my apps but it's just so damn annoying to let go of the keyboard, grab the mouse to click on the icon just to grab the keyboard again to start typing the name of the app I want to start.\n\nCan it be done? I looked at configuring shortcuts but couldn't find out how."
    author: "Oscar"
  - subject: "Re: Keyboard access to the start menu?"
    date: 2008-03-09
    body: "ALT-F2 + typing name of app will achieve a similar result"
    author: "dr"
  - subject: "Re: Keyboard access to the start menu?"
    date: 2008-03-09
    body: "Yes and no. \nThere is a subtle but important difference in what apps are showed on ALT-F2 and StartMenu if you type \"web\", and I can't scroll with ALT-F2, I can only choose the one at the top of the list.\n\nSo it's not quite what I'm after but thanks anyway."
    author: "Oscar"
  - subject: "Re: Keyboard access to the start menu?"
    date: 2008-03-09
    body: "you can tab on to the list then scroll with the cursor keys"
    author: "dr"
  - subject: "Re: Keyboard access to the start menu?"
    date: 2008-03-09
    body: "Oh, thanks. It does work. I can choose different apps. It's not quite the same but it'll do. Thanks for answering.\n\nAlso a big thank you to he who pointed to the bug-report. Good to know that this feature might show up."
    author: "Oscar"
  - subject: "Re: Keyboard access to the start menu?"
    date: 2008-03-09
    body: "http://bugs.kde.org/show_bug.cgi?id=155347\nProbably going to be fixed in the future."
    author: "Hans"
  - subject: "Re: Keyboard access to the start menu?"
    date: 2008-03-10
    body: "The shortcut should be alt-f1 by default, but it might be broken right now."
    author: "Level1"
  - subject: "Status of Plasma and Widget on Canvas"
    date: 2008-03-09
    body: "Hi,\nthanks for the digest, it is very interesting.\n\nI have one question for plasma devs.\nWhat is the current status of the transition from Plasma Widget to WoC and other QT 4.4 new stuff ?\nDo you need testing ?\n\nThanks in advance\n\n(I am affraid I cannot help coding due to lack of time and the need to learn a lot of things before being usefull, still I can compile (not complicated to type make) )"
    author: "phD student that should stop reading the dot"
  - subject: "466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "http://brainstorm.ubuntu.com/idea/478/ \n\nLet's see if we can get that number go up by at least 100 by the next digest.\n\n"
    author: "Max"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "467"
    author: "Riddle"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "if you want KDE improved then switch to a distro like opensuse who are major contributors with developing KDE."
    author: "anon"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "I would, but I just love the Kubuntu package manager. It's so dead easy to use. I love it. Every time I go and use openSuse, I miss how easy Debian/Kubuntu treats packages, and \"universes\", and \"multiverses\".\n\nRpm's, while better are still frustrating to use. I HATE DEPENDENCIES."
    author: "Max"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "And you mean that deb's have no dependencies?"
    author: "LP"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "Of course it does. It's just more transparent to the user. It does it automatically. No questions asked that a beginner can't answer.\n\nI tested both openSuse with beginners and with semi knowledgeable Linux users. Beginners preferred Kubuntu. (even over ubuntu, because it reminded them more of WIndows.)\n"
    author: "Mike"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-11
    body: "Questions that apt-rpm asks that are different from the ones apt-dpkg ask? i'm sorry, i'm not sure i understand where you're getting at here...\n\ni always liked how people seem to put \"rpm\" and \"apt-get\" on the same level, when in fact what you should do would be \"rpm\" vs. \"dpkg\" and \"apt-rpm/yum/smart/watever\" vs. \"apt-dpkg\" ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "The Kubuntu defaults are really a lot better than the openSuse settings. It's a clean and neat desktop, though I'd love to see yast replace the several settings things, but for the rest :D gotta love it."
    author: "Terracotta"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "Define \"clean\".\n\nI never had the impression that my desktop was untidy after a fresh installation of openSUSE 10.3."
    author: "Erunno"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "For starters Yast stinks to high heaven...it's clunky, slow not praticularly nice to use. Opensuse repositories are really fractured and I keep having to add custom repos to give me simple things like non crippled ktorrent. All the xtra repos all seem to end up giving me package conflicts, I seldom manage to install a lot of packages with that crappy dialog popping up telling me to resolve the conficts (ever had this happen with zypper in the terminal? its a nightmare) ... grrrrrrr.\n\nYast system settings are a good Idea (kubuntu does not have anything as comprehensive) but they are poorly executed.\n\nI think out of the box suse is a lot more polished and fine tuned than kubuntu...It's just that tweaking it further leads to headaches. I always end up going back to kubuntu...might try sidux as its also based on debian."
    author: "Fool"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-11
    body: "I think YaST is not slow at all, only package management is slow, which is expected to be solved for 11.0.\n\nI haven't noticed that KTorrent would be crippled. Some software are crippled but because of legal reasons which are outside the scope of openSUSE devs, and whose installation is \"next-next-finish\" with \"one-click\" install.\n\nZypper messages are really not nice and hardly understandable by a beginner. I hope it will be improved."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "Please stop posting this link on every dot article.\n\nIt's annoying.\n\n*goes off to vote against it just out of spite*"
    author: "Dan"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "He didn't. I think it's some one else each time.\n\nDon't vote against it. That just makes it worse."
    author: "Mike"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "If you vote against it, you make it MORE necessary to post the link. Very bad idea."
    author: "Riddle"
  - subject: "469   for KDE - Please vote"
    date: 2008-03-10
    body: "Current count is 469.\n\nDo people not like KDE or something?\n\nI like Kubuntu because it's so easy and mindless to install on ANY computer. I've yet to encounter a problem making everything just work.\nI cannot say the same about openSUSE -- Although Suse \"feels\" more finished. Custom taskbars, graphical Grub menu, graphical startup, etc."
    author: "Mike"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "just submitted my vote :D"
    author: "spawn57"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-10
    body: "480\n"
    author: "Riddle"
  - subject: "Re: 482 people are for KDE - Please vote"
    date: 2008-03-10
    body: "Good morning everybody!!\n\nCurrent count is 482. Thanks for voting people.\n\nHopefully we can make it into the 4-digits by the time 8.04 comes out. That would be a great goal."
    author: "Mike"
  - subject: "Re: 484 people are for KDE - Please vote"
    date: 2008-03-11
    body: "484 current count."
    author: "Max"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-11
    body: "Can I vote anywhere against continued Ubuntu spam here?"
    author: "Anonymous"
  - subject: "Re: 466 people are for KDE - Please vote"
    date: 2008-03-11
    body: "What's the point in posting a comment here on every vote? Even if Kubuntu users need votes there, we don't need hundreds of comments here."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: 539 - awesome!!!"
    date: 2008-03-14
    body: "Cool.\n\n539 people voted for KDE."
    author: "Max"
  - subject: "yawn"
    date: 2008-03-10
    body: "yawn, commit digest for the 2nd march, published after the latest updates.  really getting slack and slow here.  Perhaps .dot could hire a monkey, I am sure they could get it out to us faster than Danny."
    author: "anon"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "Maybe we could hire an anon.  Not quite as intelligent as a monkey of course, but always quick to type stuff up anyway.\n\nThanks Danny for the time it takes to do this.  I know I wouldn't want to do it for any amount of money and you do it for free."
    author: "a monkey"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "+1 Funny"
    author: "yman"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "+1 For being modded \"+1 Funny\"..\n\nOkay... I'll admit it I'm addicted to slashdot."
    author: "Max"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "+1 for modding me up."
    author: "yman"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "If a monkey is what's needed, you fit the bill quite nicely.  I for one am looking forward to your weekly digests.\n\nThanks for your efforts Danny!"
    author: "cirehawk"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "Ok, we will hire you. When can you start working?"
    author: "alice in wonderland"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "Not again...\nEvery week the same whiners..\n\nYou want news faster? Go read the commits and write one yourself...\n\nWhy do you need digests to be up to date anyways? They're just a snapshot."
    author: "Mike"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "You, are nothing more than a coward. I don't believe you have the guts or brainpower to post anything other than anonymous junk. You have no place within the KDE community, or any community. Take your pathetic complaints elsewhere. Danny? You're doing an excellent job as ever!"
    author: "Maarte"
  - subject: "Re: yawn"
    date: 2008-03-10
    body: "Don't feed the troll, and we love you Danny."
    author: "kwilliam"
  - subject: "Serious response"
    date: 2008-03-11
    body: "Danny does this for free, and I really like reading the digests.  People must assume he can type this up in a couple of minutes.  I think they overlook how much data is in the digest, and how much data must be sorted/filtered for him to put together the digest.\n\nIn that regard I won't flame, but rather sincerely suggest that someone should volunteer to perhaps help him."
    author: "T. J. Brumfield"
  - subject: "yawn"
    date: 2008-03-11
    body: "yawn, read and discussed 2 times this comment already."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Just out of curiosity"
    date: 2008-03-10
    body: "What good is it to have 3 stars appear in password fields, rather than 1?"
    author: "yman"
  - subject: "Re: Just out of curiosity"
    date: 2008-03-10
    body: "Not sure, but I'm guessing this makes it more difficult to guess the length of a password when you see someone typing it? Maybe someone has a better explanation though..."
    author: "Darkelve"
  - subject: "Re: Just out of curiosity"
    date: 2008-03-10
    body: "To make you look cool when you type it.\n\"Woah, what a long password you have. And you typed it so fast!\""
    author: "Hans"
  - subject: "Re: Just out of curiosity"
    date: 2008-03-10
    body: "I didn't want to use 3 stars before, but now that you say that I'm totally gonna do it."
    author: "Level1"
  - subject: "Congratulations Danny!"
    date: 2008-03-10
    body: "Congratulations Danny for the 100th Digest! Thanks for all your work for the community :-)\n\nAnd also thanks to Derek Kite who started and produced our old CVS Digest!\n\nHats off to you guys :-)"
    author: "fred"
  - subject: "Ligature?"
    date: 2008-03-10
    body: "Has nobody maintained Ligature anymore? It is pretty sad :( We had a long debate Okular vs Ligature, and now one of them is no longer maintained..."
    author: "anon"
  - subject: "Re: Ligature?"
    date: 2008-03-10
    body: "Natural selection."
    author: "Anon"
  - subject: "Re: Ligature?"
    date: 2008-03-17
    body: "Natural Selection is part of evolution.  There was no evolution here.  So, I would have to say that this was unnatural selection which resulted from the KDE decision to drop KViewShell and replace it with an almost totally new application.  Very sad indeed."
    author: "JRT"
  - subject: "Re: Ligature?"
    date: 2008-03-10
    body: "sad, indeed. I preferred Ligature over okular's new interface. It was more \"standard\", more Adobe like..."
    author: "Kevin"
  - subject: "Re: Ligature?"
    date: 2008-03-17
    body: "As I recall, the KViewShell developers were rather thoroughly dissed by KDE.  This is a very unfortunate situation.\n\nKViewShell was a shell viewer that should have had KGhostView, KPDF, & KView added to it.  Instead, the wheel was reinvented and the Mouse swallowed up the Elephant.\n\nThe KDE project gains very little from this lack of planning and coordination.  Doing things over is not the best or most efficient method -- constant improvement is a much better option.\n\nI am very sad to see KViewShell being abandoned."
    author: "YAC {yet another critic}"
  - subject: "Kicker vs. plasma how-to"
    date: 2008-03-10
    body: "Many users still have problems using plasma in multiple ways, i.e. featurewise (layout, number and quality of plasmoids...) and stabilitywise (just take the visual artefacts appearing since 4.0.2). I found the idea interesting to take a load from the shoulders of our plasma developers who will probably not be able to provide a satisfying solution before 4.1 while receiving even more bashing from us users. The solution was discussed in one of the last dot discussions. It is to enable KDE3 kicker to use in connection with KDE4.\n\nThere is a How-To available:\nhttp://www.kde-apps.org/content/show.php/HOW-TO:+Kicker+on+KDE4?content=76539\n\nThis How-To shows users how to enable Kicker for regular use in KDE4 instead of the default plasma panel. Not to understand it wrong: Users still use KDE4 - Kwin, Plasma and the session manager. It is just the panel that will be replaced.\n\nI hope, the How-To will help users who are not satisfied with KDE4 and who have been disappointed with KDE's very misleading version numbering (namely: who expected more of a x.0.0 release) and it will hope KDE developers since they can calm down users by refering them to using kicker until 4.1.\n\nFor me (openSuse 10.3) it works excellent together. Praise the design concept of KDE2/3! \n\nBest regards\nSebastian"
    author: "Sebastian"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-10
    body: "It's been said numerous times, if you want your features and stability, 3.5.9 is shiny, new and feature rich, srock solid table and mature.\n\nKDE 4.0.2 is stable (more or less), but not rock solid and most certainly not mature. It needs a lot of programs to be ported, but the K Desktop Environment is there."
    author: "Terracotta"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-10
    body: "Please, save you educational advice! Read my post again! I do completely agree with you.\n\nAll developers are moaning about the \"whining\" of disappointed users. Of deconstructive optinions. The only thing I did was to investigate a way to satisfy those users who can not wait for 4.1, but for whom the plasma panel is too buggy or disfunctional. It is just a How-to which helps them to combine all the fine things of both: KDE4 with desktop plasmoids, KWin and oxygen and - on the other side - KDE3 with kicker. \n\nI probably should have sent this as a separate dot story in order to let these discussions end. This was just a proposal and how-to as a work around!"
    author: "Sebastian"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-11
    body: "i believe the knee-jerk comes from your choice of terminology... Words like \"vs.\" and your inversion of the compliment sandwich (which *should* have been compliment, critique, compliment, not the way around you have it) will to the casual reader make it seem as though the post is just more petrol on an already raging fire. \n\nIn fact, i might suggest rephrasing the thing a lot, make it seem like a big compliment to Plasma, and then release it into the blogosphere in the spirit i'm sure you actually want it to be perceived: As a compliment to the Plasma devs, and a help to the early adopters :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-11
    body: "Thanks, but no, I won't post it on the dot. Either way, the developers are currently not willing to agree with mistakes while users will continue to show their disappointment. As long as both sides are not interested in understanding the other (or in finding a solution for the other) it would be better to silently wait until 4.1 and to hope that people will then be satisfied. Bye"
    author: "Sebastian"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-10
    body: "A slapt in the face for plasma-devs and lovers....\nI simply loved it, heheheheheheheh!!!\n\nYes, plasma will be great, we know it, but even before 4.0 was near I (and more people also) was saying that it would be ready for 4.1 and most problally 4.2.\nThe answer I got from Aaron Seigo was; \"and on what basis do you tell this?\".\nWell, looks like reality-check basis is a good one.\nNothing like time giving you the reason, ahahahahah!!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-11
    body: "Nothing like being insulting to people who do that on a volounteer basis. Sad."
    author: "Luca Beltrame"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "Volunteer?  Aaron Seigo is paid by TrollTech to work on KDE.  He isn't a volunteer.\n"
    author: "Jim"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "A) He's not the only person who works on Plasma. Far from it.\n\nB) What difference does it really make?"
    author: "Paul Eggleton"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "Also, I'm pretty sure aseigo isn't paid to work until 3AM and during weekends which he seems to do almost always."
    author: "Anon"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "Anyway, I did not want to insult.\nIt was much more like a revenge after being told to shut up ;)\n\nAaron is kind of a bit arrogant about Plasma, and I hope that when he sees that people prefer to use Kicker on top of KDE4, or run KDE4 apps inside KDE3 that Plasma is wrong in some ways, mostly very-very incomplete.\nIf he and other KDE developers could be a bit more humble, most of KDE4 discussion would not exists, because it would not be released (at least as 4.0) before being more feature-complete and having other apps time to port (yes, I mean kde4-dev or kde 3.9).\nSee where we are: no decent menu, no decent panel, no decent desktop, no k3b, no amarok, no kaffeine, buggy dolphin... man those are basically all the things I use in KDE!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-16
    body: "> people prefer to use Kicker on top of KDE4, or run KDE4 apps inside KDE3\n> that Plasma is wrong in some ways, mostly very-very incomplete.\n\nYou're saying people that want to use KDE3 are complaining because they use KDE4. There was no plasma in KDE3, it's new, it's different, if you like what KDE3 had then use it, fresh version has 3.5.9 stamped on it's butt even.\n\nDid you expect 2 years of porting to make KDE4 using Qt4 to be exactly like KDE3 with a new library underneath?\n\nPerhaps the old icons on the desktop, a start menu on the left, a systray on the right, and text representation of of the running apps isn't the long term future for the bottom of computer screens and desktops? Maybe something new will at the same time be something... different! What a shock!\n\n"
    author: "tms"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-17
    body: "> Did you expect 2 years of porting to make KDE4 using Qt4 to be exactly like KDE3 with a new library underneath?\n\nThat was the original goal for KDE 4.  That's what porting is.\n\nAnd I don't see anybody complaining that KDE 4 isn't *identical* to KDE 3.  I see people complaining that KDE 4 is *less functional* than KDE 3 in many ways.\n\nWhat a shock!  People have features they use taken away from them and they seek out ways of getting them back.  How unreasonable!\n"
    author: "Jim"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "> A) He's not the only person who works on Plasma. Far from it.\n\nHe's the one that was being talked about.\n\n> B) What difference does it really make?\n\nPeople repeatedly point out that KDE is volunteer work to make out that it is above criticism.  Clearly they think it makes a difference, otherwise they wouldn't point it out.\n"
    author: "Jim"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "> People repeatedly point out that KDE is volunteer work to make out that \n> it is above criticism. Clearly they think it makes a difference, otherwise \n> they wouldn't point it out.\n\nI have no numbers to back it up but I would say that most of the work on KDE is still unpaid, if it makes any difference.\n\nIn any case, just because *some* KDE developers get paid by a third party to work on it, it does not mean that it becomes open season to say anything you like about what they do. They're not above criticism, but I believe they should be above some of the nasty comments made on this board."
    author: "Paul Eggleton"
  - subject: "Re: Kicker vs. plasma how-to"
    date: 2008-03-12
    body: "Look, either you think getting paid to work on it is relevant or you don't.  You clearly don't.  Luca Beltrame clearly does.  I was responding to him.\n\nI'm not saying that it's relevant, I'm pointing out to somebody that *does* think it is relevant that they are wrong about Aaron, the subject of this sub-thread, being a volunteer.\n\nIf you want to convince somebody that being paid / being a volunteer is not relevant, then convince Luca, he is the one that was trying to make a point out of it.\n"
    author: "Jim"
  - subject: "KDE Brainstorm"
    date: 2008-03-10
    body: "After reading most of the replies briefly, I think that a brainstorm for KDE as exists for Ubuntu is a great idea to keep devs and users in touch with each other for ideas and preferences.\n\nIt's a very clear and plain way to convey ideas and votes, to me at least.\n\nI think that many users complaints about 4.0.x releases could have been better addressed that way, in the sense that users could have better expressed their desires and do so more conveniently. Brainstorming is easier than submitting bugs, after all :)"
    author: "Ron"
  - subject: "++"
    date: 2008-03-10
    body: "++"
    author: "anwort"
  - subject: "Re: KDE Brainstorm - Cool"
    date: 2008-03-10
    body: "That would be nice. :)\n\nWould a brainstorm list be a lot of work to maintain and edit though?\nI want KDE developers to spend more time coding, than having to deal with \"busywork\" The bug reports already take more time than coding itself. (necessary evil.!)"
    author: "Mike"
  - subject: "Re: KDE Brainstorm - Cool"
    date: 2008-03-10
    body: "First, I guess Ubuntu can be persuaded to help creating it, maybe by contributing code (Probably Kubuntu people will be more enthusiastic about it though).\n\nSecond, maybe we can start a new KDE category for the meanwhile.\n\nThirdly, maintenance need not be done by devs, and can be done by others with much less tech knowledge but with willingness to participate in KDE.\n\nAnother idea occurred to me: posts on KDE Brainstorm can be linked to bugs and feature requests. This can be done by QA or others, again no need for hardcore devs for most."
    author: "Ron"
  - subject: "Re: KDE Brainstorm"
    date: 2008-03-11
    body: "+1\n\nBut developers should let aside their \"do it yourself if you want X or Y feature/bug\" just for a while. This does not mean they should accept everything, but just try to hear a bit of the users complains and suggestions.\nAs users should respect hte \"vision\" of developers (when there is one, or they tell us about it). :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE Brainstorm"
    date: 2008-03-12
    body: "Agreed."
    author: "Riddle"
  - subject: "Re: KDE Brainstorm"
    date: 2008-03-16
    body: "It's kind of existing already, KDE Bugzilla allows voting for bugs and feature requests, eg <a href=\"http://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist&votes=21&order=bugs.votes\">Most wanted features</a>\n\n\n"
    author: "binner"
  - subject: "Observation on Amarok and QuickTime Player"
    date: 2008-03-10
    body: "Is it just me or does the top of Amarok look like the footer of QuickTime Player? Move the volume slider to the left and make the height to width ratio lower and it looks like they just moved it up to the header from the footer that is in QuickTime.\n\nNot that I'm complaining because it looks better than the 1.4 UI but definitely a nod to Apple's team."
    author: "Marc Driftmeyer"
  - subject: "Re: Observation on Amarok and QuickTime Player"
    date: 2008-03-11
    body: "If so, it is very much a coincidence, i guarantee it - we discussed it on the dev channel last night (someone made the same comment on the blog), and well... None of us have used QuickTime Player since a number of years now ;) Thanks, too, but nope - it's not a copy, just a coincidence :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Observation on Amarok and QuickTime Player"
    date: 2008-03-11
    body: "There is no such thing as a coincidence, only willful unwareness or unawareness of all variables in play.\n\nUnless you've lived in a cocoon for 4 years and never visited a web site that has screen shots of the QuickTime player and video in question embedded, then I might believe you've not seen it. Even then I'd be ignoring the reality that their is no such thing as coincidences--Coincidances, perhaps."
    author: "Marc Driftmeyer"
  - subject: "Re: Observation on Amarok and QuickTime Player"
    date: 2008-03-12
    body: "Where did people get this absurd idea that there are no coincidences?  Prove it to me.  No seriously, prove it to me.\n\nWhen did the world turn away from rational thought?  Has everyone gone mad?\n\n/rant"
    author: "Level 1"
  - subject: "For the naysayers who say no improvements were mad"
    date: 2008-03-10
    body: "Here's something for the naysayers who say no improvements were made to KDE in the last few months.\n\nLook at this and drool:\nhttp://polishlinux.org/kde/kde-41-visual-changelog-rev-783000/\nbtw.: this was on digg.com. Yes, we made dig!!! :D :D :D\n\nNow please go and vote for Kubuntu (link is above in another thread..)\n\n\n____\nP.s.: Did anybody notice that the coolest screenshots lately come from Poland?\nWonder why this is..\n\nCan't wait to see tons of really cool HOW-TO's and Effects videos when KDE 4.1 Comes out. I'm expecting a Keynote presentation that puts \"Jobs-notes\"/Mac-notes to shame.. :)\n*crosses fingers*\n\n\n"
    author: "Max"
  - subject: "Re: For the naysayers who say no improvements were mad"
    date: 2008-03-12
    body: "Is it polishlinux as in people from poland or polishlinux as in refined linux?"
    author: "Level 1"
  - subject: "Re: For the naysayers who say no improvements were mad"
    date: 2008-03-12
    body: "The former, although the other meaning kind of applies too... check out their about page:\n\nhttp://polishlinux.org/about/\n\nQuite a nice website overall IMO."
    author: "Paul Eggleton"
  - subject: "Not satisfied with KDE and/or the commit-digest"
    date: 2008-03-10
    body: "Well fix what you think are wrong yourself!\n\nSeriously there is nothing that stops you. Go ahead and do something by yourself. Thats the beauty of open source, you can actually do whatever you want to improve the situation that you are not satisfied with.\n\nThere is actually only one thing that gets you nowhere and that is expect that other people should do whatever you think is important and by the time you want it to be done. Sometimes there are people that might do what you want but consider that as a bonus.  "
    author: "Mats L."
---
In <a href="http://commit-digest.org/issues/2008-03-02/">this week's KDE Commit-Digest</a>: Work on WebKit integration, the ability to access <a href="http://plasma.kde.org/">Plasma</a> data engines in Plasmoids rendered through WebKit, and a HDDtemp daemon data engine are added to Plasma, plus work on Plasmoid packaging and KRunner. Items can now be dragged from the <a href="http://en.opensuse.org/Kickoff">Kickoff</a> menu to the desktop or the panel. More work on syncing Akregator</a> with online reader services. A GUI for declinations in <a href="http://edu.kde.org/parley/">Parley</a>. Support for DGML tags in <a href="http://edu.kde.org/marble/">Marble</a>. Genuine progress in the KTankBattle game. General improvements and the removal of the Helix engine in <a href="http://amarok.kde.org/">Amarok</a> 2. A visual redesign of the <a href="http://kget.sourceforge.net/">KGet</a> "Web Interface", with added translation capabilities. Continued work on <a href="http://koffice.kde.org/kpresenter/">KPresenter</a> slide transitions, and KCron. Work on importing and exporting shortcut configurations in KControl. The "three stars per character" password mode returns to KDE 4 (from the KDE 3 series). Various speed optimisations across KDE applications. Ligature moves to "unmaintained" status. <a href="http://dot.kde.org/1204735202/">KDE 4.0.2 is tagged for release</a>. <a href="http://commit-digest.org/issues/2008-03-02/">Read the rest of the Digest here</a>.

<!--break-->
