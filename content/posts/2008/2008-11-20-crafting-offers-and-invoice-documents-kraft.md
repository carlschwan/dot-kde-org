---
title: "Crafting Offers and Invoice Documents with Kraft"
date:    2008-11-20
authors:
  - "sk\u00fcgler"
slug:    crafting-offers-and-invoice-documents-kraft
comments:
  - subject: "Having a small business..."
    date: 2008-11-22
    body: "I would be interested in this, but it requires you to manually set up a mysql server.   No thanks.  If there isn't an option to just use a local sqlite db, I can't be bothered to see if the program is any good.  \n\nSure, I'm being lazy, but I bet a lot of people will do the same.  You have to make it simple for people to try out the software if you want to attract more users."
    author: "Leo S"
  - subject: "Re: Having a small business..."
    date: 2008-11-23
    body: "The attitude you're showing is quite interesting to me. It sounds like we (or Kraft developers in this case) should be grateful if you try the software, or that  we should at least listen to someone who only posts, generalizes quickly (\"a lot of people will do the same\") and tell us how we have to do the work (\"You have to ...\").\n\nThat's not how it works, unfortunately. In fact, I as a developer often can't be bothered to even care about things people sharing your attitude demand. In most cases, I'll just turn over to people who are being nicer, and less arrogant. Your net-effect is the opposite of what you try to preach then.\n\nAs to this particular case -- and I'm not really familiar with the inner workings of Kraft -- setting up a MySQL server is trivial to do (\"install mysql-server\" on this box), even more so if you're looking at the whole software package you need for running your own business. \nBesides that, it's something that can easily be done by the distribution (most have wonderful mechanisms for any post-install chores) -- making your point (\"installation is too hard\") moot.\n\nSure, making installation as easy as possible is a good thing to strive for, but that doesn't mean that it's technically feasible in a particular case. (If you're interested in that, read why Akonadi is *not* using sqlite, and what they opted for.)"
    author: "sebas"
  - subject: "Re: Having a small business..."
    date: 2008-11-24
    body: "Having to setup a mysql server is a pain, but no problem.\n\nMy real question is whether it's being ported to kde4?\n\nDerek"
    author: "dkite"
  - subject: "Re: Having a small business..."
    date: 2008-11-26
    body: "The port to KDE 4 is on the way. It's a lot work since Kraft already is more than 30k lines C++ code. \n\nAdditionally I am easy to distract by user wishes (or requirements ;-) of people who tell me \"I would love to use Kraft in my company but this and that is still missing and I need it!\". Even though I said that 0.30 is the last KDE 3 version I will release 0.31 soon with two important and nice features on the KDE 3 base which came out of this.\n\nBut after that again concentrated work on the KDE 4 port :)\n\nAnother interesting fact that we geeks usually oversee is that people working not with computers as their day job such as gardeners, carpenters or builders usually do not care so much about the versions they have running. It's a huge success if they realize that it is _KDE_ that makes their computer experience so much nicer than they had it before :-)\n"
    author: "Klaas Freitag"
  - subject: "Re: Having a small business..."
    date: 2008-11-26
    body: "> In most cases, I'll just turn over to people who are being nicer, \n> and less arrogant. \n\nI can understand your point of view perfectly fine.\n\nHowever, I understand his point of view just as well. \n\nAt least you reply which is not always the case with developers, but there are also developers who do not invest ANY time bothering spending with users even before listening to _ANY OTHER_ opinion. As long as there are different developers this is not really a big problem either - and often, a given user is a developer of something else as well - but in key areas of an operating system this can become a real stopping wall.\n\nNow this is not the case here so there should not be any problem to find enough people who use MySQL happily anyway."
    author: "markus"
  - subject: "Re: Having a small business..."
    date: 2008-11-23
    body: "It could probably be easily changed to use the MySQL server as a child process, like Akonadi does.\n\nThis would remove the need for any manual setup, still leave the option for using a stand-alone mysqld and keep all the database related code untouched."
    author: "Kevin Krammer"
  - subject: "Re: Having a small business..."
    date: 2008-11-23
    body: "I think another good idea might be a KCModule for managing databases and access to them and a way for applications to take advantage of this module.\n\nI personally hate to setup MySQL databases and prefer embedded databases or if the application itself completely takes care of them. Having a well designed GUI for setting them up in a central place might improve the situation significantly."
    author: "JoselB"
  - subject: "Re: Having a small business..."
    date: 2008-11-26
    body: "I like the idea with the KCModule very much. That would take away lots of the pain of the users when using a database server. The pain is not installing a package, but setting up the right database permissions. Why isn't that done by the application like Kraft? Well, in a healthy environment this step can require root or other users permissions, depending on the database server settings. It's hard to cope with that in an application. \n\nThe reason for depending on MySQL instead of SQLite is that Kraft is going to be a multi user application. There are still issues but in general several people on different desktops should be able to work on the same Kraft document base. That's challenging with a file database. "
    author: "Klaas Freitag"
  - subject: "Re: Having a small business..."
    date: 2008-11-23
    body: "There are plenty of piece of software you can buy for a lot of money that do require you to set up a database server as well. Pfffffffff."
    author: "Segedunum"
  - subject: "Re: Having a small business..."
    date: 2008-11-24
    body: "Yes, the kind of software that have a dedicated machine to run them. \nDoes KDE provide a database abstraction layer? I won't bother me if KDE requires MySQL/Postgres but at application level (Kraft, Amarok, etc.) I tend to avoid it."
    author: "Dexter"
  - subject: "Re: Having a small business..."
    date: 2008-11-26
    body: "If so then KDE should do this on a global scale for EVERY KDE app, in kcontrol where using MySQL or Postgre would be easily switchable. \n\nIf a user has to setup a database, KDE can make life easier."
    author: "markus"
  - subject: "Re: Having a small business..."
    date: 2008-11-24
    body: "I am reading those silly \"OMG, I have to set up MySQL server\" and I am starting to think, that most of you do not even know, wtf the MySQL \"server\" is! :) \n\nOn most distros (99.9%?), setting up a MySQL \"server\" is as easy as typing 2-3 short words and hitting the enter key.  \n\nKraft team, good job!\n\n\n\n"
    author: "What!?"
  - subject: "Re: Having a small business..."
    date: 2008-11-25
    body: "The problem is not the installation, the problem is setting it up. I prefer all servers to have their data somewhere in /home when used for production use. I have normally only / and /home on my system and / gets formated from time to time (due to being no elite admin and having often problems with packages from different repositories).\nAnd I'm a rather lazy person who prefers to code instead of learning the concepts, configuration options and where all the files are for some servers. As they are specific to the individual server they are of no further use for me...\n\nSo having a nice GUI for setting up servers in a not absolute-default way, and maybe for accessing things like databases without a need for knowing their basic concepts may be useful.\nWhat those concepts are? E.g. How many databases are on a server? What are databases? How can I remove a database after testing? Where are they stored? Ok, most of us will know this. But the average user of software like Kraft might not.\n\nThis is no critique on the Kraft developers. It shouldn't be their job. This is more a request for some abstraction layer which reduces the required knowledge. I would write one, but currently I have more projects than I'm able too finish.\nSo someone who wants to step in?\n"
    author: "JoselB"
  - subject: "Re: Having a small business..."
    date: 2008-11-28
    body: "what are you worried about mysql? clearly the program is not ready for users if you have to edit the VAT/GST in the source code."
    author: "mike"
  - subject: "Re: Having a small business..."
    date: 2008-12-06
    body: "I tried an earlier version of KRAFT. Setting up this MySQL-Thingy was a pain in the ass and it took me like 1 hour. But in the end the whole thing worked. So what? "
    author: "ishmael"
---
<a href="http://www.linux.com/feature/153136">Linux.com covers Kraft</a>, an administration software package for small and medium businesses (SMB), covering activities such as customer management, document workflow, material management, calculations for positions and templates for most of these. Linux.com concludes 
"<em>Kraft takes some of the drudgery out of tracking work offers and invoicing. If you are a KDE user, being able to use a single contact manager for issuing your invoices will cut down on your mail merging worries. But Kraft's handling of VAT/GST could be improved."</em> We will have to add to that, that you do not need to be a "KDE user" already to use Kraft. As with all KDE applications, it runs just fine on other Free desktops too.  The <a href="http://ev.kde.org">KDE e.V.</a> uses craft for membership management, and gets first-hand support by <a href="http://kraft.sourceforge.net/index.php?body=&amp;lang=en">Kraft</a>'s main developer, our very own Klaas Freitag.


<!--break-->
