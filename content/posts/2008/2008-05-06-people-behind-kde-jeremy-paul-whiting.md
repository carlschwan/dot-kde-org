---
title: "People Behind KDE: Jeremy Paul Whiting"
date:    2008-05-06
authors:
  - "aspehr"
slug:    people-behind-kde-jeremy-paul-whiting
comments:
  - subject: "Nice interview!"
    date: 2008-05-06
    body: "That was a very good interview - a few stock questions, sure, but there was a decent amount of back-and-forth and follow-ups to responses that gave it the personal touch, rather than the \"fill out and return these standard forms, please\" feel of some earlier interviews.  A few bonus chuckles along the way, too :)"
    author: "Anon"
  - subject: "Doctors"
    date: 2008-05-06
    body: "Gees. He was doing fine until the end. Everyone knows Tom Baker was the best ever! ;)"
    author: "xian"
  - subject: "Re: Doctors"
    date: 2008-05-06
    body: "Strange topic to start a flame war on, but William Hartnell is unsurpassable. \n\nOld enough to remember him first time around, recent watching of remastered boxed set confirmed remembered quality.  Quaint, dated? Yes. Dark, edgy? Yes. Alien worlds? Yes. All took place in London? No. Full of stupid gadgets? No.  \n\n:)   "
    author: "Gerry"
  - subject: "Re: Doctors"
    date: 2008-05-06
    body: "I love the Sylvester McCoy era!  (Not a great actor but I was in love with Ace (Sophie Aldred) at that tender age...)"
    author: "PG"
  - subject: "h0tt"
    date: 2008-05-06
    body: "Hello Jeremy! Your work on KDE is much appreciated and you are very sexy!"
    author: "AC"
  - subject: "Re: h0tt"
    date: 2008-05-06
    body: "+1, "
    author: "anon"
  - subject: "Re: h0tt"
    date: 2008-05-07
    body: "i agree, and just a fyi i don't share ;-)"
    author: "Stephanie"
  - subject: "dunno"
    date: 2008-05-06
    body: "btw i never hit if you are all wondering....nice jer, nice blauzahl"
    author: "Stephanie"
  - subject: "Re: dunno"
    date: 2008-05-06
    body: "We know, women (wives especially) have more subtle means of revenge than that :-)"
    author: "Odysseus"
  - subject: "Re: dunno"
    date: 2008-05-06
    body: "me?! have another way for revenge?! no!! never!!! ;-) I think I'll just break more stuff and make Jeremy fix it for me ;-)"
    author: "Stephanie"
  - subject: "Great view"
    date: 2008-05-06
    body: "What a fantastic view.  I want that as my wallpaper."
    author: "Leo S"
  - subject: "Re: Great view"
    date: 2008-05-06
    body: "Jeremy -- you were kidding, right? That has got to be a postcard, not the actual view through your window?\n"
    author: "Benoit Jacob"
  - subject: "Re: Great view"
    date: 2008-05-06
    body: "Ok, that's not exactly the view out of my window, but it's pretty close.  There is red-rock all over the place here (I pass a couple hills that look just like that on my way to work every day)."
    author: "Jeremy"
  - subject: "Re: Great view"
    date: 2008-05-07
    body: "As I remember from traveling out west, Utah is a beautiful state. Both Bryce Canyon and Zion are wonderful national parks, but there are also many things to see just driving down the road."
    author: "Adam"
  - subject: "Re: Great view"
    date: 2008-05-06
    body: "This is Zion National Park. I believe, there are some camping grounds over there... Good to have an employer offering telework (/travelwork?) -"
    author: "Sebastian"
  - subject: "Re: Great view"
    date: 2008-05-06
    body: "Actually, that pic is of Snow Canyon, which is about 5 minutes ride away from my home.  I don't telecommute either, we have our main office here in town. :)\n\nSt. George is a nice place to live for those that can/are able to telecommute though."
    author: "Jeremy"
  - subject: "Re: Great view"
    date: 2008-05-07
    body: "Wow!"
    author: "Sebastian"
  - subject: "Understand"
    date: 2008-05-06
    body: "I noticed that Understand puts the application name before the file name in the window title, which is different to other \"native\" KDE applications. (Besides cutting off some texts, but that's a quite obvious bug.)\n\nYou might consider rebasing Understand on the KDevPlatform - the platform as well as the very advanced C++ parser (automatic resolution of macros, anyone? :] ) are under the LGPL and provide a lot of stuff that you would otherwise need to maintain by yourself."
    author: "Jakob Petsovits"
  - subject: "Re: Understand"
    date: 2008-05-06
    body: "Heh, actually, we do have our own parser that we maintain for all the languages we support. :)  Not sure if I should mention now or not, but we are working on an open-source license for Understand, so developers of Open-Source software can use it free."
    author: "Jeremy"
  - subject: "Awesome"
    date: 2008-05-06
    body: "Great interview! Nice to see the People Behind KDE series returning, and with some added flair on top of the standard questions too. Thanks :)"
    author: "Paul Eggleton"
  - subject: "Great Interview"
    date: 2008-05-06
    body: "Nice interview. I was just looking through kde to find out when the release date was for the next in 4 . I use kde on OpenSuse and well has alot of bugs yet. So i see Opensuse is releasing before the next kde release. which I guess is not part of this interview but i was just stating why i was here. Keep up the awesome work people.\nif you want to see what people are saying about kde in opensuse here is the new announcement on the beta 2 of 11\nhttp://news.opensuse.org/2008/05/03/announcing-opensuse-110-beta-2/\nOnce again keep up the great work. I have been using kde since its beginning and will continue to."
    author: "Velocity"
  - subject: "Breaking Errors"
    date: 2008-05-06
    body: "Stephanie broke some documentation errors.\n\nHm... How do you break errors? And if she managed to do so: Good on her! Errors ought to be broken.\n\n;-)\n\nAnyway, good interview. Enjoyed it a lot.\n\nJeremy, just remember your promise to extend KNewStuff2 so that sysadmins can install the data system-wide! That's really crucial for thin client environments in education.\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Breaking Errors"
    date: 2008-05-07
    body: "you know i don't know really how i broke them, just that i did! oh well..."
    author: "Stephanie"
---
In a new series of <a href="http://behindkde.org/">People Behind KDE</a> interviews, we visit the United States of America to meet a KDE developer with an affinity for education, accessibility, and Asian culture, a person who works on getting you Hot New Stuff - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/jpwhiting/">Jeremy Paul Whiting</a>.

<!--break-->
