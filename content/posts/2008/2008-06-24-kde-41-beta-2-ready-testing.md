---
title: "KDE 4.1 Beta 2 Ready For Testing"
date:    2008-06-24
authors:
  - "skuegler"
slug:    kde-41-beta-2-ready-testing
comments:
  - subject: "Nice!"
    date: 2008-06-24
    body: "beta1 is really good, let's hope this time nixternal can place the new packages for kubuntu faster than first 4.1 beta that needed a lot of changes in packages from 4.0 :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice!"
    date: 2008-06-24
    body: "Fix: beta1 is good, beta2 should be even better ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice!"
    date: 2008-06-24
    body: "no need to wait, beta2 was here since 3 days already ( before the official release) http://amarok.kde.org/wiki/User:Apachelogger/Project_Neon/KDE\n\nfriendly "
    author: "mimoune djouallah"
  - subject: "Re: Nice!"
    date: 2008-06-24
    body: "hello,\nyou can also try the Neon packages for Kubuntu\n(check the Harald Sitter blog at http://apachelog.blogspot.com/2008/06/project-neon-kde-nightly-builds.html)\n\nThe downside is they don't include debug symbols for now but they work very well and install cleanly :)"
    author: "DanaKil"
  - subject: "Re: Nice!"
    date: 2008-06-24
    body: "Wow thanks (to both that indicated neon).\n\nI tought neon was for amarok nightlies only :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice!"
    date: 2008-06-26
    body: "http://amarok.kde.org/blog/archives/681-Project-Neon-KDE-Nightly-Builds.html\n\nIt grew."
    author: "Riddle"
  - subject: "Re: Nice!"
    date: 2008-06-25
    body: "That's the word, Nice!\nBest KDE4 beta so far, I'm really enjoying today fooling around with KDE Four Live.\nThis is the FIRST KDE4 where I have a feeling of actually knowing where plasma is going, and I like it!\n\nI really hope some devs or nice people will find the time to update the plasma website and write some up-to-date words about plasma. That should really reduce user frustration and confusion.\n\nIt's sad to see many destructive threads on loads of webpages right now. (e.g. OMG NO DESKTOP ICONS or OMG THE PEANUT SUCKS. It's all not true)\n\nI can't understand it, KDE4 is at an early stage right now. Why so impatient? Some people (ok, thats mostly not here on the dot) behave like spoiled kids on christmas this year.\n\nNon-KDE-Devs: Please let the devs finish their step and see where they'll be standing THEN. It's hard to critisize in an useful way when being unable to see the whole picture.\n\nKDE-Devs: Keep it up! In almost every aspect, KDE 4 rocks already, and I can't wait to see how hard it rocks tomorrow."
    author: "fhd"
  - subject: "Is KDE really modular at all?"
    date: 2008-06-24
    body: "It appears most of the release highlights are application based, not platform like KDE should be.\n\nWhy can't they release new versions of Dolphin and Gwenview regardless the release cycle of KDE itself? Somehow it seems that the KDE apps are more intergrated in kde than Windows Media Player was in Windows when EU fined them."
    author: "kanttu"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "You could say that KDE basic is kdelibs + kdebase, which would only give you Konqueror and Dolphin as the few basic apps.\nGwenview is clearly part of kdegraphics, which consists of a bunch of helpful apps dealing with viewing/editing graphic files. It's not \"build into\" kdelibs nor kdebase (which would be nonsense anyway in the *nix way of thinking).\n\nOf course it's possible to only update kdegraphics without updating kdelibs/kdebase (at least with minor version numbers like 3.5.8 -> 3.5.9 or later on e.g. 4.1.0 > 4.1.1 this is not a problem at all)\n"
    author: "Thomas"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "That's often not possible as applications might depend on newer features in the libraries. kdelibs being binary compatible means that you can run older applications (say gwenview from 4.0) with newer kdelibs. So in case you want to update kdelibs, you don't necessarily have to update all your apps as well -- but not the other way round."
    author: "sebas"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "I have to correct myself here. For bugfix updates, we usually don't add any feature, so in most cases, it's possible to update e.g. gwenview without newer kdelibs. The general BIC rule (older apps run on newer libs) is still correct though. Not necessarily the other way round, but often the case for x.y.z releases. Sorry for the confusion."
    author: "sebas"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "\"It appears most of the release highlights are application based, not platform like KDE should be.\"\n\nUm, those applications are part of the platform...\n\n\"Why can't they release new versions of Dolphin and Gwenview regardless the release cycle of KDE itself?\"\n\nThose apps are part of the whole we call \"KDE\". If they start doing individual releases, then there won't be a release of \"KDE\" anymore, just several seprate releases of different parts of KDE.\n\n\"Somehow it seems that the KDE apps are more intergrated in kde than Windows Media Player was in Windows when EU fined them.\"\n\nAre you surprised that KDE-apps that ship as part of KDE are integrated with the desktop? Yes, I can see why that is such a surprise.\n\nAnd what does Media Player have to do with KDE? Nothing. Absolutely nothing. Utterly, 100% nothing. I fail to see why you bring up Media Player to this discussiion."
    author: "Janne"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "The metaphor about WMP was that the developing KDE and its applications is so maid together so eventually they would merge together.\n\nMy concern was that the release cycle of KDE limits the release cycle of KDE apps like Dolphin and Gwenview since they are bound to release their stuff when KDE is shipping new major release.\n\nAs far as I know, minor releases 4.1.x are for bug fixes, so to get new features for dolphin on gwenview you have to wait for 4.2 or use the svn head losing all the stability of the current stable version.\n\nIf you're an independent developer you could release new version for your application like every week using the current stable platform.\n\nHope you got my point :) "
    author: "kanttu"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "\"The metaphor about WMP was that the developing KDE and its applications is so maid together so eventually they would merge together.\"\n\nWMP and Windows are not merged either. And there's nothing wrong with integration (unless it's done for monopolistic purposes, like what MS did with WMP and IE). It makes the system cohesive and it makes sure that the apps take advantage of the advanced features the desktop has to offer.\n\n\"My concern was that the release cycle of KDE limits the release cycle of KDE apps like Dolphin and Gwenview since they are bound to release their stuff when KDE is shipping new major release.\"\n\nUm, that's the way it has been since the beginning of KDE... That's kinda the point of desktop-environment. A desktop with apps and underlying systems that forms a cohesive whole.\n\n\"As far as I know, minor releases 4.1.x are for bug fixes, so to get new features for dolphin on gwenview you have to wait for 4.2 or use the svn head losing all the stability of the current stable version.\"\n\nAnd if those apps were released separately, you might still have to wait for 6 months before you would get a new feature-release. So what would you gain, exactly? Do you want to have feature-releases once a month or something? When would you have the time for bugfixes, translations, artwork, documentation?\n\n\"Hope you got my point :)\"\n\nI do get your point, I just think that it's wrong and misguided."
    author: "Janne"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-06-24
    body: "\"If you're an independent developer you could release new version for your application like every week using the current stable platform.\"\n\nAnd nobody forbids you, but being in KDE gives you the pleasure of forgeting about releasing, dealing with translators, packaging translations, etc. You just code and then someone does the \"boring\" release tasks."
    author: "Albert Astals Cid"
  - subject: "Depends on your distro"
    date: 2008-06-24
    body: "Some distros do release each app as a seperate package, and then bundle them in meta-packages.  So yes, KDE is modular."
    author: "T. J. Brumfield"
  - subject: "Re: Is KDE really modular at all?"
    date: 2008-07-01
    body: "Yes, KDE apps are individual apps.  The thing is, have you ever installed  Xorg or GNOME from source?  If so, you would understand the wisdom of putting the individual things in fairly large packages."
    author: "JRT"
  - subject: "I'm already on SVN since 2 weeks"
    date: 2008-06-24
    body: "And I must say... I'm confident now, that world dominance is only a small step further... no... really, I'm very impressed! This is clearly not only an evolutionary step compared to the 3.5 series, but really some kind of revolution. It's such a joy to see KDE 4.1 shaping up in the last couple of weeks. Guys, you must be really proud of your baby.. you know you've created something awesome... do you?\n(I'm really looking forward to KOffice now..., but so far it has only been crashing on me, really difficult to test atm...)"
    author: "Thomas"
  - subject: "Spread the good news"
    date: 2008-06-24
    body: "Vote for the story here:\nhttp://digg.com/linux_unix/KDE_4_1_Beta_2_Released\nhttp://www.reddit.com/info/6oqjs/comments/\nhttp://www.fsdaily.com/EndUser/KDE_4_1_Beta_2_Released_and_Ready_for_Testing"
    author: "Jure Repinc"
  - subject: "Congratulation"
    date: 2008-06-24
    body: "Hi!!\n\nWell done. Beta 1 is already great... I am downloading the new Bata2 and looking forward to enjoy the new space feeling!\n\nHave a nice day\n\nNicola"
    author: "Nicola"
  - subject: "100%"
    date: 2008-06-24
    body: "Dope"
    author: "winter"
  - subject: "Re: 100%"
    date: 2008-06-24
    body: "Is that a + or a -."
    author: "Riddle"
  - subject: "Re: 100%"
    date: 2008-06-25
    body: "Dope = A+"
    author: "Winter"
  - subject: "Re: 100%"
    date: 2008-06-26
    body: "(takes mental note to remember that)"
    author: "Riddle"
  - subject: "Sort of working, help please"
    date: 2008-06-24
    body: "For first time (on my openSUSE 10.3 -> 11.0 system) it looks like KDE 4.x is working :) except all I get is a white screen and KMix.  I remember someone posting a way to cure the white screen thingy but I can't find it.\n\nMay I ask for some help?  \n\n"
    author: "Gerry"
  - subject: "Re: Sort of working, help please"
    date: 2008-06-24
    body: "As a last resort you could try deleting your .kde4 .kde folders to see if this helps (of course then you would lose settings, history etc..)."
    author: "bill"
  - subject: "Re: Sort of working, help please"
    date: 2008-06-24
    body: "That did it, thank you.\n\nAm I dreaming or is everything faster?"
    author: "Gerry"
  - subject: "Re: Sort of working, help please"
    date: 2008-06-24
    body: "To anyone else thinking of giving this advice, don't.  The correct advice is \"Move your .kde or .kde4 folder to .kde-nogood and see if it works\".  Otherwise you run the risk of the user deleting all their downloaded mail, contacts, calendars, torrents and other valuable stuff."
    author: "Bille"
  - subject: "Re: Sort of working, help please"
    date: 2008-06-24
    body: "Erm, I'm pretty sure I did warn about that.  \n\nStrikes me that the fact any actual data that is valuable to you is stored in a hidden folder is far from optimal anyway, no doubt in the future there will be a proper central repository of user info which holds useful stuff like firefox bookmarks etc in a clearly marked not-easily-deletable-or-loseable location.  Perhaps Nepomuk will help here, in that way I could drag my personal settings folder to my usb thumbdrive and use on my Mac or XP desktop at work."
    author: "opensuse user"
  - subject: "Re: Sort of working, help please"
    date: 2008-06-24
    body: "As the recipient of the original advice, I think your advice is correct but excessive. I'm ignorant (as in unaware of all the ins and outs) not reckless (stupid). After all, knowing what a .file is and how to find it was a prerequisite to understanding the advice.  \n\nI for one read \"delete\" as \"make a copy first\" (Moreover, in openSUSE, if you delete something it ends up in trash, so you do get a second chance).\n\nDeleting .kde4 was just what I needed to do, in that KDE4 is now working and wasn't before.  I had never successfully used KDE4 (indeed I've posted a few bug reports) so my .kde4 folder was empty, all that stuff is in my .kde folder\n\nSo I'm still happy and think you were slightly harsh on the donor :)\n\n\n\n\n\n\n\n\n\n\n   "
    author: "Gerry"
  - subject: "Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Reading http://bugs.kde.org/show_bug.cgi?id=154535 I got the impression that KDE Desktop experience is defined by a single person. Please tell me that I got it wrong."
    author: "testerus"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "yeah he is our guru, our president, and our visionary, we trust him, we love him, he is sent by God to bring some love and beauty to linux desktop ;)"
    author: "anon"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "So, who is this god guy anyway?"
    author: "panzi"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "As far as I can see, the kidney in the top-right corner can't be removed completely because plasma is not only designed for your common desktop, but the future is to target different devices as well, some of them won't have a right-click available. So the task was to design it in a way that is as unobtrusive as possible and still making it usable for e.g. touch-screens. From my pov this has been nicely solved, as it just greys out when not used. I don't even notice it anymore. If you hover the mouse over it, it'll fade in with color (no animation), which is as undisturbing as possible. Only if you click it, it'll animate and show you some possible actions. If it stays the way it is atm, I'm fine with it."
    author: "Thomas"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "You got it wrong. There are quite a few people writing the defining code.\n\nAnd if Plasma sucks for you, just write a replacement. And much fun with those for whom your approach will suck. ;)\n\nYou could even start with a port of KDesktop and Kicker, if you liked it that much. Noone will stop you. But if many will encourage you, I wonder. Because if Plasma is done (someday) you should be able to configure a Plasma setup which resembles the KDE 3 desktop closely. Yes, includes waiting for now."
    author: "Hans"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "Curse that Peter Noone"
    author: "MamiyaOtaru"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "Thanks for your arguments. You have convinced me."
    author: "Hans"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Would it be better if the \"KDE desktop experience\" is designed by a committee?"
    author: "Janne"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "It is ;-)"
    author: "jospoortvliet"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Several of the issues raised in that thread have IIRC been solved without simply removing the cashew. Quickly looking at that thread I saw at least 2 Plasma developers post in it so it isn't defined by a single person either. Being defined by a single person isn't really bad either, lots of projects benefit from having a central leader that decides what gets in and what doesn't (look at the Linux Kernel and how well its done).\n\nIIRC, it was also mentioned that someone could write an alternative Desktop containment that didn't include the cashew."
    author: "Kitsune"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "\"Being defined by a single person isn't really bad either, lots of projects benefit from having a central leader that decides what gets in and what doesn't (look at the Linux Kernel and how well its done).\"\n\nThat's true if you have the right person."
    author: "Jeff Strehlow"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "\"That's true if you have the right person.\"\n\nAre you hinting that Aaron is the wrong person for the task? And who exactly are you to decide that?"
    author: "Janne"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "I didn't say that. I'm just saying that having a leader doesn't always work out well."
    author: "Jeff Strehlow"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "There is absolutely nothing wrong with questioning whether some leading person or party is the right one for the respective task.\nWe had political systems where this wasn't really allowed, and I'm glad we are over it. \n\nThere is no need for him to justify for this.\n\nNobody has chosen Aaron to be the KDE desktop hacker leader, he happened to become it when he took over maintainance of kicker.\nSo basically, code decided. But I have the impression it has become somewhat more than that...\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "but, like Linus uses to call it, it can and will not become more than 'benevolent dictatorship' - if most KDE dev's disagree with him, he has to follow. Besides, I know he wouldn't want it any other way."
    author: "jospoortvliet"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Plasma is primarily led by one person. However, Plasma is not the entire KDE experience."
    author: "Riddle"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "I've read the thread and it was quite disturbing to me also. It seems Aaron has his own idea how he wants things to be and doesn't listen to the users. I don't buy his argument that the people that were arguing against him don't know what's going on and are unqualified to comment.\n "
    author: "Jeff Strehlow"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: ">  and doesn't listen to the users.\n\ndisagreeing with a self-selected small group of users on one issue does not equal \"doesn't listen to the users\". the numerous other times i have done just that, publicly and privately, say otherwise.\n\n> I don't buy his argument that the people that were arguing against\n> him don't know what's going on and are unqualified to comment.\n\nand yet .. .they don't. they aren't familiar with the code (as evidenced by the lack of understanding of the role of Containments) nor the design (ditto). that's my assertion. you can support yours with similar fact if you'd like."
    author: "Aaron Seigo"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "I kind of like the whole Software Engineering thing that KDE is doing at the moment.  (Yes I am a \"Software Engineer\").\n\nI think a lot of people just don't understand how brilliant the idea of desktop containments is.  You can do everything you could do before, you can just do so much more now.  It's elegant, simple, and really quite clever (though admittedly I haven't actually played with it - it's something that can be ruined through bad user interface issues)"
    author: "matthew"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "You don't need to understand the code to make judgments on the user interface. Users can decide for themselves whether they like the way something in the user interface works. A small group of users brought the cashew issue up but there aren't many people that post on boards such as those. Surely there must be many more people that would like to have an easy way to disable it.\n\nI guess all users that post on boards with opinions are \"self-selected users\" including the ones that have nothing to say but praise since they made the decision themselves to post. I don't think that being a \"self-selected\" user makes  ones opinion any less valid."
    author: "Jeff Strehlow"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "> You don't need to understand the code to make judgments on the user interface.\n\nsometimes you do. this is one of those sometimes.\n\n>  I don't think that being a \"self-selected\"\n\nno, it just makes them not \"all the users\""
    author: "Aaron Seigo"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "> > You don't need to understand the code to make judgments on the user interface.\n>\n> sometimes you do. this is one of those sometimes.\n\nActually, never. The regular user should not have any programming skills to be able to run some HMI, or GUI, or what-so-ever. This is the main mistake, which most programmers do, the engineers not, though. Because the principle of making thing works and designing thing have different approach. No one will credit if you have the best lyrics if your song doesn't fit it. No one will credit if you wrote the best book ever if the reader population is very small, and they can all be doctors of the science, still doesn't matter. The common end user is very power, and he decides, the engineer needs to feel the common user needs and to tackle it smooth into the code, bringing new ideas, yes they could be revolutionary, why not? So, never ever say that any user needs to understand any code you do or wrote. You need to understand user needs and to be able to say, yes I could do, or no I couldn't. But, no, pal, you need to look my code, to have some programming courses and then you could say \"what-a-boy\" to me, because what I did is so genius, that your small brain cannot fit, because you don't understand singletons :)"
    author: "doc_ds"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "I'm pretty sure the argument boils down to:\n\nSome people: The cashew is useless and the animation is annoying. Please make a way for me to remove it.\n\nAaron (& some other people): No, it isn't finished yet and will become awesome. I promise.\n\ngoto 10"
    author: "Tim"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "\"Aaron (& some other people): No, it isn't finished yet and will become awesome. I promise.\"\n\n->\n\n\"Aaron (& some other people): No, it isn't finished yet and will become awesome. I promise.  And if you don't like the result, a clone of the \"classic\" desktop, with icons as they were in KDE3 and no cashew, will eventually be made available by checking a tickbox\"."
    author: "Anon"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-07-01
    body: "I don't understand why the Cashew is needed.  But supposing that the menu choices you get by clicking on it are necessary, why does the Cashew have to be there all the time?\n\nIn KDE3, I have a Panel with only a clock on it and it appears when I move the cursor to the upper right corner of the screen.  Seems like the same thing could be done for the Cashew, or the Cashew could be eliminated and the menu items could come up directly."
    author: "JRT"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "Just because everyone that doesn't care, doesn't mind, or actually likes and understands the cashew didn't comment in the bug report doesn't mean that the few people bitching in the bug report are correct and deserve to have it their way. In fact bug reports are about reporting and discussing and solving a problem if one exists, not for people to chime in about how something is good so you don't see dozens or hundreds of BUG reports about something that is RIGHT. Vocal pushy people of the world don't get to run things even if they think bitching the loudest makes them deserve it."
    author: "tms"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "I hope you're not one of the developers."
    author: "Jeff Strehlow"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Wow.  Maciej writes:\n\n\"...Which is sad because it would be more flexible to have several components, pretty much as Lego bricks, not monolithic like current one. But somehow Aaron sees it, it is more flexible in current way.\"\n\nAaron blasts Maciej for being unprofessional.  The above comment seems to offer a differing opinion, but I don't see how it is uncivil or unprofessional.  Conversely, Aaron repeatedly calls everyone ignorant and says no one understands the issue, and furthermore he has no intention to explain it.  He also states that if someone wrote a patch giving the option to configure it, said patch would not be committed.\n\nHe then wrote a lengthy post championing against giving users choice on principle.\n\nFor months I've said I was worried that KDE was moving towards a new philosophy that restricted choice, and most respond that in time KDE will again present the end user with plenty of choice.  Aaron's own words seem to belay that."
    author: "T. J. Brumfield"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "\"For months I've said I was worried that KDE was moving towards a new philosophy that restricted choice, and most respond that in time KDE will again present the end user with plenty of choice. Aaron's own words seem to belay that.\"\n\nJust switch to Windows then. You have plenty of \"choice\" there, right? Or maybe you could write your own damn desktop, then you could make it exactly the way you want.\n\nNow you are basically saying that since the plasmoid-cashew is not removable by default (although it CAN be removed), it somehow proves your point that KDE is moving towards non-configurability. Um, OK...."
    author: "Janne"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "No, I said because Aaron wrote a lengthy post where he said he was against choice in principle, not one specific instance.  He said it was a bad idea to allow for configuration for most things.  I'd content that the configuration options are perhaps the biggest difference to an end-user between Gnome and KDE.  Linus has been quite vocal about the configuration differences being the reason he blasts Gnome and advocates KDE.\n\nWhen the head of KDE advocates against configuration and choice as an overall philosophy, which is what he did, that concerns me.\n\nAre you honestly going to suggest such a notion does not concern you?"
    author: "T. J. Brumfield"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Aaron is not the head of KDE.\n\nOk, he is the president of the e.V. (which doesn't have technical influence).\nHe is also the main developer of plasma, which is the interface users get when using KDE.\n\nStill this is not all of KDE.\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "> the interface users get when using KDE\n\nto be crystal clear: it's the desktop interface users get when using the KDE workspace. there's also kwin for windows, konqueror, dolphin, system settings, etc, etc. in the workspace, and there's a lot of KDE outside the workspace."
    author: "Aaron Seigo"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "because there is a way to provide choice that doesn't make it mind bendingly hard to use. is that really a hard concept to understand? according to the comments it seems the answer is \"yes\"."
    author: "Aaron Seigo"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "Several people made varying suggestions, including treating it like any other widget (and thusly allowing removal) and allowing the toolbox to disappear when locked.\n\nNeither are mind bendingly hard to use.  Both make sense in that they would be consistent with existing paradigms.\n\nPerhaps even another idea would work better.\n\nStill others just wanted the option to move it so it wouldn't interfere with maximized applications.\n\nI agree with a recent blog post of yours in which you said that bugzilla may not be a good place for a \"discussion\" of such things.  Mailing lists and forums work much nicer because of threaded views and such.\n\nYou've suggested that you have something in mind for the toolbox, and that most people don't know what you've got planned.  You also said in your blog, and that thread, that you don't want to explain yourself because you could be doing better things with your time.\n\nFrom a user's perspective, they operate off what they're told, and what they see on their current desktop.  It would seem KDE 4 offers some half-realized concepts currently.  One possible solution is to hide things and not implement them until they are ready to be more fully realized.  The other idea is to attempt to document and explain your vision for these things.  Internal documentation within the apps themselves would be ideal rather than repeating it across mailing lists, blogs, forums, the dot, interviews, etc.  Yet anyplace beats no explanation.\n\nIf you want to focus on hacking on plasma, or doing bug triage, then you're certainly entitled to do so.  However, I believe it would attract/entice/help developers, as well as comfort users if you managed to get out word on your plans.  You've often encouraged people to just jump in the code and discover things for themselves, but a specific verbalized plan might encourage more people to look at the code in the first place.\n\nIt might help to attempt to delegate either some of the hacking, or some of the PR if you're having trouble finding time for both."
    author: "T. J. Brumfield"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "> Several people made varying suggestions, including treating it like any other\n> widget (and thusly allowing removal)\n\nof course, it isn't any other widget and special casing it as such leads to nasty issues in the code. nasty issues in code tend to percolate up eventually to constrain what we can do for the user.\n\n> and allowing the toolbox to disappear when locked.\n\nwhich would only really make sense if it's locked from the context menu. more special casing.\n\n> Perhaps even another idea would work better.\n\nlike Containments, and a drop down in Desktop Settings to choose your favourite?\n\n> Mailing lists and forums work much nicer because of threaded views and such.\n\nagreed.\n\n> You've suggested that you have something in mind for the toolbox, and that\n> most people don't know what you've got planned.\n\nbecause most people don't read (and/or remember) every blog post i make\n\n> You also said in your blog,\n> and that thread, that you don't want to explain yourself because you could be\n> doing better things with your time.\n\ni've explained in my blog what that idea is. i've repeated it here in this thread twice now. at what point will \"most people\" get it? when it's in the code so they can see it themself without consulting design discussions, or when we have a place for such design documentation that is easily archived, searched and found.\n\n> they operate off what they're told, and what they see on their current desktop\n\ni think the latter is a far larger part of it. i've been telling people for months and months now about Containments.\n\n> One possible solution is to hide things and not implement them until they are\n> ready to be more fully realized.\n\nother than the ability to alter the toolbox (or make it go away) Containments have *nothing* to do with it. moreover, Containments were there from 4.0. so this idea isn't applicable here.\n\n>  The other idea is to attempt to document and explain your vision\n> for these things.\n\nthere's actually already an FAQ online for this.\n\nhttp://techbase.kde.org/Projects/Plasma/FAQ\n\nit's a wiki. maybe you can help make it better.\n\n> Yet anyplace beats no explanation.\n\nnot by much. i still deal with the abuse from you and others despite having explained it here and in my blog and elsewhere.\n\n> then you're certainly entitled to do so.\n\nthat entitlement includes dealing with this thread though. which doesn't make it much of an entitlement.\n\n> attempt to delegate \n\nlots of that happens already, actually.\n"
    author: "Aaron Seigo"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-28
    body: "Is it possible to have a flash movie or something like that, that shows your vision about how plasma desktop would look like after coding is complete. Sort of like simulating a fully functional desktop in flash presentation. This would answer lot of users concerns and would save you lot of time justifying why the desktop is looking the way it is currently\n\nI personally like KDE4 and I think its the best desktop environment ever"
    author: "Kris"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "At the first glance, you seem to have right and Anon looks like the stubborn narrow minded developer who does not care for anybody's opinion.\n\nHowever, when I think about it with a cool head, the truth is the reverse. Anon is not only the programmer but also the _designer_ of that component. And design cannot be done by committee or through user studies. It is a realization of the designer(s) vision and is an artistic process (good example: Apple's designs).\n\nAnon has every right to do his design as he likes it first and surely not everybody will agree with it. I am sure he will also take into account user feedback once the design is reasonably complete (but does not even have to do that). It is take it or leave it. He is giving it away for free. He is fully entitled to his artistic freedom. Nobody prevents anyone from coming up with an alternative and compete openly. STFU already and let the man realize his vision. If he really messes up significantly and there is mass agreement on it, alternatives would emerge anyway. But don't expect people doing public work cater to your personal tastes!!!\n\nThe whole thing reminds me of the saying \"No good deed goes unpunished!\""
    author: "AC"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "You want a configuration to remove something that does nothing unless you intentionally interact with it. You also want to make others have to enable something when it's possible on those systems it would be physically impossible to enable it easily. You also need to file a bug report with YOUR desktop provider which is downstream from KDE for the end user since customizations come from the distros in our current form of desktop on *nix.\n"
    author: "tms"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "\"You want a configuration to remove something that does nothing unless you intentionally interact with it.\"\n\nActually all the people in this thread, and in the bug report complain that it gets in the way.  It causes problems when you are intentionally trying to interact with it.  That is the problem.\n\n\"You also want to make others have to enable something when it's possible on those systems it would be physically impossible to enable it easily.\"\n\nIt can be enabled by default, as it should be.  I never said people should have to go out of their way to enable it.  Don't put words in my mouth.\n\n\"You also need to file a bug report with YOUR desktop provider which is downstream from KDE for the end user since customizations come from the distros in our current form of desktop on *nix.\"\n\nDistros do sometimes customize KDE, but more often that, most of the settings and features of a shipped KDE desktop are the defaults.  If I ask say openSUSE or Mandriva to code a feature, it doesn't benefit people outside those distros.  For some strange reason, even though the SRPMs and such are open, most distros don't really take from other distros."
    author: "T. J. Brumfield"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-27
    body: ">\"You want a configuration to remove something that does nothing unless you\n>intentionally interact with it.\"\n \n\"Actually all the people in this thread, and in the bug report complain that it gets in the way. It causes problems when you are intentionally trying to interact with it. That is the problem.\"\n\nAnd all those bugs/issues has been addressed and fixed, if there had been an option to disable it they would most likely not. This happend when the actual problems got identified, and not hidden by a configuration option. This also proves how Aaron in this case was right, refusing the option. "
    author: "Morty"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-27
    body: "You can't move it or remove it.\n\nPeople who want their panels on top have issues.  People who have maximized windows have issues.  How has this been fixed?"
    author: "T. J. Brumfield"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-27
    body: "-The cashew no longer animates when moused over. (No more distraction)\n\n-Cashews also play nicely with full-width panels on the top of the screen.\nThe only issue left is that if the panel isn't full width the cashew is still only a quarter circle even if there is no panel above it. This obviously should/can be fixed, and if distros feel that is a big issue they can use Suse's cashew-removal patch."
    author: "Jonathan Thomas"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-27
    body: "\"People who want their panels on top have issues\"\n\nNo, since it moves automaticly when you put the panel on top, or right side for that matter. It should perhaps get a smarter way to decide where to move, but thats a minor issue. And that is not fixed by making it removable.\n\n\"People who have maximized windows have issues.\"\n\nNo they have not, as it's already hidden below the maximized window. Removing it would not make any difference what so ever. And if they use the close button in the corner, it still require you to click it to activate. So there are no issue there.\n\n\nSo both your issues have already been fixed, and they are actively being improved even further. A hiding option would not have aided in detecting and removing these issues."
    author: "Morty"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-07-01
    body: "Janne, we all know that you love Aaron. Love him to death. Provide good arguments or stop trolling."
    author: "Kapri"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-24
    body: "Reading through the mess of this bug I found \n\nShowDesktopToolbox=false\n\nfor SuSE. I'd like to turn the cashew off as well (though, given what I've read inside the bug report, it seems necessary to me to state that I'm definitely not fanatic about it and clearly want to note here that for my part I think Aaron does an absolutely great job of leading the KDE desktop into the future and I understand that he cannot possibly discuss every single design decision of the plasma desktop with an anonymous internet community inside a bug report). So, my question here is, if something similar exists or will exist for Kubuntu packages as well. If it isn't, I'll live with it - it won't mean the end of civilization for me like some people are pretending it to be."
    author: "Michael"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "It's an openSUSE addition. Because openSUSE people know why people like KDE."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-25
    body: "it's an openSUSE hack, because they are willing to cut corners in the short term. that's fine for a distribution, imho, and i think that for their audience they did a fine thing. \n\nupstream we're addressing the issue properly, which unfortunately takes time.\n\nnice thing about having the source is that openSUSE can do this, and others can borrow their work if they choose to."
    author: "Aaron Seigo"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "> it's an openSUSE hack, because they are willing to cut corners in the short\n> term. that's fine for a distribution, imho, and i think that for their\n> audience they did a fine thing.\n\nthis and the other bug to do with the openSUSE knotify or whatever it is should be a good reason to create some guidelines for distros so they make it clear what are their changes so they don't just let all the blame come back to KDE in general on these kind of issues when it's something they have created themselves.\n\n"
    author: "tms"
  - subject: "Re: Who defines the  KDE Desktop?"
    date: 2008-06-26
    body: "Guidelines?\n\nAre you suggesting KDE dictate what distros can do with open software, or are you asking distros to be clear with what their changes are?"
    author: "T. J. Brumfield"
  - subject: "nvidia issues"
    date: 2008-06-24
    body: "hi\n\nwhere can i find more info about the preformance issues with nvidia chips?"
    author: "adrian"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "me too, me too... On my crappy laptop with intel 945 graphics it feels snappier (after some tweaking in xorg.conf) than on my 4Gb, 3Ghz Core2Duo, Quadro FX570 ;-(\n"
    author: "Thomas"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "... and the temporary lockup / black screen issue in with konsole: http://lists.kde.org/?l=kde-core-devel&m=121224059101922&w=2"
    author: "Sebastian Kuegler"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Funny enough I accidentally turned on Compiz on openSuse 11 (it's so easy) running KDE 4.1 beta 1 and I almost got a shock at what I saw. It was at least 4 times as fast as Kwin with the effects turned on.\n"
    author: "Bobby"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "> On my crappy laptop with intel 945 graphics it feels snappier (after some tweaking in xorg.conf)\n\nWhich tweakings exactly? I'm on 945 graphics, too, and Xorg makes about 15% CPU load all the time. (I just can't work without some subtle desktop effects anymore...)"
    author: "Stefan Majewsky"
  - subject: "Re: nvidia issues"
    date: 2008-06-26
    body: "KDE4 with OpenGL on Intel 945GM hardware \n\nhttp://dot.kde.org/1213447279/1214036849/"
    author: "Thomas"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "For example\n\nhttp://bugs.kde.org/show_bug.cgi?id=160467\n\nThis is reproducable here. Resizing a konsole window with transparent background takes 5 seconds from dragging the window border with the mouse until it's painted. The user experience is just not snappy at all.\n\nHere is another user suffering from performance problems:\nhttp://www.nvnews.net/vbulletin/showthread.php?t=109990\n\nMy nvidia setup is suffering from serious lag in window management, it takes 1 second or more to switch virtual desktops (more windows open makes it take longer). This is a dualhead setup (2 1280*1024) and a Geforce 7600GS w/ 512MB). I'm using the driver from the NVidia repository for OpenSuse 11, and I have experienced the same performance problems on Kubuntu.\n\nJos Poortvliet has been blogging some time ago, although I'm not sure he could really pin it down to a single component.\nhttp://nowwhatthe.blogspot.com/2007/12/performance-and-qt-44.html\nhttp://nowwhatthe.blogspot.com/2007/12/performance-and-qt-44-again.html\n"
    author: "Sebastian Kuegler"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "<b>\"Performance problems on NVidia chips remain, but we are confident that those will be solved by the teams over at NVidia in one of the next releases of their graphics driver.\"</b>\n\nShould I read this as, \"we've talked to Nvidia, they know what the issue is, and they've assured us it should be fixed\" or, \"like Flash working in every browser but Konqueror, we have no intention to fix the issue, so we're putting it off on someone else.\"\n\nAnd before everyone starts calling me a troll again, I think it is a very valid question.  Everyone was all over me when I questioned the folder view fiasco (and I've seen screencasts and screenshots.  I understand it, and still loathe the current incarnation of it.  The second it operates like an actual desktop with wallpaper and all, I'll look at it again).  In the spirit of being open-minded, I was doing a new openSUSE install on a computer for my parents and tried KDE 4.\n\nTheir computer is low-end for a brand new computer, but it is still a dual core rig with 2 gigs of ram, and KDE 4.0.82 was unusuably slow.  Not only was the system sluggish as all heck (though it was better when I turned off KDE 4's composite effects and switched to compiz fusion, which ran much nicer), but many of the new interface hoops reminded me of Vista.  Not in that all shiny UI's are all like Vista, but more like options that were quickly accessed before now took extra steps to reach.\n\nI was open minded, and I gave it a try, but within two hours I had installed KDE 3.5.9 and rolled back to that.  The Nvidia issues were a big part of that.  For years Linux users have been encouraged to buy Nvidia cards because they provided superior Linux drivers.  On the dot I read repeatedly that those issues would likely go away with QT 4.4, which wasn't the case.\n\nDoes anyone actually know specifically what the issue is, and how likely it is to be addressed?"
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "\"And before everyone starts calling me a troll again\"\n\nEveryone calls you a troll because you complain all the time and your comments are so utterly negative.\n\n\"Everyone was all over me when I questioned the folder view fiasco\"\n\nThe only \"fiasco\" there was caused by people like you who were unable to read and understand what they were being told."
    author: "Janne"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "Look up the definition of a troll.  And as I've said repeatedly, I've said a variety of positive things about KDE for years and years.  Being upset about one feature does not make a person a troll.  A troll is somehow who stirs pots just because they like pissing people off.  I conversely ask simple, direct questions that Aaron apparently had no intention of answering.  I had been asking them repeatedly for months.  I searched and searched for answers, and then posted in the dot looking for that same information.  That does not make one a troll.\n\nI've kindly asked repeatedly for you to cease with personal attacks.  They don't serve any purpose.  Furthermore, your attacks are without merit since you do not understand the term you attempt to slander me with.\n\nAnd frankly, releasing a major milestone that lacks a very basic expected feature is a fiasco.  Several reviews and blogs have ripped it accordingly.\n\nBut thanks for avoiding the issue here, which is piss-poor performance for Nvidia users."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "\"I've said a variety of positive things about KDE for years and years.\"\n\nAs they say in finance-business: \"past performance is no guarantee for future winnings\". Just because you said something nice in the past does not mean that you can start spreading BS today.\n\n\"Being upset about one feature does not make a person a troll. \"\n\nIt does when people try to tell you that you are wrong, and you basically cover your ears and shout \"LALALALA I CAN'T HEAR YOU!\".\n\n\"A troll is somehow who stirs pots just because they like pissing people off.\"\n\nAnd your utterly negative comments are doing just that.\n\n\"I conversely ask simple, direct questions that Aaron apparently had no intention of answering.\"\n\nIs this about desktop-icons? Because you were given answers OVER AND OVER AGAIN. If you refuse to listen or don't understand the answers, that's your problem. \n\n\"I've kindly asked repeatedly for you to cease with personal attacks. \"\n\nYou seem to be constantly whining about non-existant \"personal attacks\". Where exactly have I done that? I have not attacked your person in any shape or form.\n\n\"And frankly, releasing a major milestone that lacks a very basic expected feature is a fiasco.\"\n\nAgain with the icons? They are there. It's just 100% obvious that you are utterly clueless. And no, that is not a \"personal attack\". I'm utterly clueless about car-engines, and I have no problems admitting that fact.\n\nCould you FINALLY quit with your endless whining about the icons? Rest assured that you are wrong on this issue. Hell, maybe you should just switch to Windows, then we would be spared from your whining.\n\n\"Several reviews and blogs have ripped it accordingly.\"\n\nThose reviews and blogs were written by people who were ignorant on the issue. They basically wrote about something they had no clue about. They basically claimed that KDE will not support icons on the desktop, when nothing could be further from the truth.\n\n\"But thanks for avoiding the issue here, which is piss-poor performance for Nvidia users.\"\n\nComplain to NVIDIA then. Is it KDEs fault if NVIDIA writes buggy drivers?"
    author: "Janne"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "Grow the fuck up."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "Heh, is throwing around obscenities when you've been bested logically the best you can do? Grow up."
    author: "Jonathan Thomas"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "\"As they say in finance-business: \"past performance is no guarantee for future winnings\". Just because you said something nice in the past does not mean that you can start spreading BS today.\"\n\nAccusing someone of BS is a personal attack which Jannes continues to deny.  Furthermore, the point in contention here is that Janne repeatedly accuses me of being a troll, and now contends that one negative comment denotes being a troll regardless of past behavior.  Clearly Janne does not understand that assigning a label to a person does include past behavior.  Furthermore Janne continues to miss the point that a troll has nothing to do with complaining.  A person is a troll because of motivation.  If they say things solely to incite people rather than state their sincere opinions, they are a troll.  If anyone is trolling here it is Janne.\n\n\"It does when people try to tell you that you are wrong, and you basically cover your ears and shout \"LALALALA I CAN'T HEAR YOU!\".\"\n\nYou call this logic?  Seriously.  What the hell does this even mean?  This has been Janne's typical tactic, to throw out some nonsensical accusation rather than address issues.  And you claim that Janne outwitted me with logic?  No, I just want nothing to do with such childish behavior.\n\nAnd frankly, opinions can't be wrong by definiton.  No one proved me wrong.  I stated that I hated a feature, and would not use KDE if that was the future of KDE and Janne repeatedly suggested that I was wrong.  Wrong about what?  Does Janne understand the concept of subjectivty versus objectivity?\n\n\"And your utterly negative comments are doing just that.\"\n\nThere is a trend here that people are chastised for offering and dissenting opinions on the dot.  This is apparently an unmoderated open forum for discussion.  And \"yes-men\" mentatlity benefits no one.  Dissenting opinions are in fact invaluable in any community driven product that strives for quality.\n\n\"Is this about desktop-icons? Because you were given answers OVER AND OVER AGAIN. If you refuse to listen or don't understand the answers, that's your problem.\"\n\nI fully understand the way the system works.  I get an applet running on top of my desktop which looks terrible, or I replace the desktop containment for a new one that has no support for wallpapers.  Both look awful.  We're regressing horribly here.  This isn't a usable state to ship a finished product in, but then again, neither was the half-hearted attempt at plasmoid icons either.  I'd delay the release, or perhaps focus more on the very basics of a usable desktop before I focused on other things.\n\nThe question I've asked for months is what is so horribly broken about the old system that it must be thrown away before the new system even works.  Why can't the two live side by side?  If you want a folder view applet, then have it.  I hope it brings you no end of pleasure.  But why remove choice and functionality?\n\nAaron said the concept was horribly, horribly broken.  Why?  No one ever answered that.  People suggested it was bad practice to have anything on the desktop period.  Then don't put stuff there.  Others want to.  Why remove that functionality from them?  It makes zero sense.\n\nYou're forcing people to operate the desktops differently and I guarantee you will lose a bunch of users over it.\n\n\"You seem to be constantly whining about non-existant \"personal attacks\". Where exactly have I done that? I have not attacked your person in any shape or form.\"\n\nYou repeatedly call me a troll and then deny making personal attacks.  You call me a whiner and deny making personal attacks.  Do you understand what constitutes a personal attack?\n\n\"Again with the icons? They are there. It's just 100% obvious that you are utterly clueless. And no, that is not a \"personal attack\". I'm utterly clueless about car-engines, and I have no problems admitting that fact.\"\n\nYou insist they are directly on the desktop, which they are not.  They exist in an applet.  You insist nothing changed, when it did.  Your insistance does not change the facts.\n\n\"Could you FINALLY quit with your endless whining about the icons? Rest assured that you are wrong on this issue. Hell, maybe you should just switch to Windows, then we would be spared from your whining.\"\n\nHow am I wrong about an opinion?  Agains subjective versus objective.  Or are you saying I'm wrong that the icons aren't in an applet?  Because then clearly you don't understand how it works.\n\n\"Several reviews and blogs have ripped it accordingly.\"\n\n\"Those reviews and blogs were written by people who were ignorant on the issue. They basically wrote about something they had no clue about. They basically claimed that KDE will not support icons on the desktop, when nothing could be further from the truth.\"\n\nNo, several people didn't want their icons in an applet.  For once, an applet does not operate the way a normal window does.  You insist anyone who doesn't like what you like is ignorant.  I won't even begin to describe that philosophy.\n\n\"Complain to NVIDIA then. Is it KDEs fault if NVIDIA writes buggy drivers?\"\n\nThat depends on the issue here.  That is why I raised a question."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "> > \"Complain to NVIDIA then. Is it KDEs fault if NVIDIA writes buggy\n> > drivers?\"\n>\n> That depends on the issue here. That is why I raised a question.\n\n\"Complain to NVIDIA then. Is it KDEs fault if NVIDIA writes buggy drivers?\".\n\nThis is no viable answer for a project as KDE.\nIt means, that KDE as it is will have problems on a big percentage of machines, while all other desktops don't have this problem. Relying on that NVidia will fix the issue in some future update and that users will update then (and will still be running KDE4 by then) is a bad choice.\n\nAlex\n"
    author: "Alex"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "This is becoming rather ridiculous, do you really read what you reply to and what you write? Creative and incomplete quoting by you, does not make it any better btw.\n\nLets take a closer look at the first  one:\nT. J.:\n\"I've said a variety of positive things about KDE for years and years.\"\n\nJanne:\n\"As they say in finance-business: \"past performance is no guarantee for future winnings\". Just because you said something nice in the past does not mean that you can start spreading BS today.\"\n \nJanne:\n\"Accusing someone of BS is a personal attack\"\n\nSo to spell it out for you. No, this is not a personal attack, since no one are accusing you of anything. It's just pointing out that whatever you have done before, have no relevance on current performance. Why you chose to interpret it as an attack are rather odd. Please stop inventing personal attacks when none exist.\n"
    author: "Morty"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Incomplete quoting?\n\nIn almost every post between the two of us, I had quoted every single line from Janne verbatim.\n\nHow does that constitute incomplete quoting?"
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Troughout the post that triggered my reply, you left out your own orginal statement and crafted your reply to Janne in a way that it become out of context. How much you qouote does not matter when you take it out of context, hence incomplete quoting."
    author: "Morty"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "It is standard practice not to quote the entire thread.  I quote his additions.  My previous comments are right above.  I didn't selectively remove bits to editorialize.  I consistently remove my previous comments, and consistently include every word of Janne's comments.\n\nHow is that selective quoting?\n\nI'd contend my method is standard practice by many on the internet.  I was just following due form."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Since by removing your original comment in which Jannes reply was aimed, making your out of context reply harder to spot. So intentionally or not you did editorialize.\n\nThe standard practice is not to consequently remove everything one level above in a discussion, when required you should include necessary information to keep the context clear."
    author: "Morty"
  - subject: "Re: nvidia issues"
    date: 2008-06-26
    body: "http://dictionary.reference.com/browse/editorialize\n\nPlease look it up.  I took 2 years of journalism classes, so I'm quite familiar with the definition.  You suggest I altered my quoting practice to editorialize, except I don't alter my quoting practice.  I'm extremely specific about habits, practices, and semantics.\n\nI reply directly to what is said, and consistenly quote all of it.  You insisted I was quoting \"incomplete\".\n\nGiven that I quoted every word Janne wrote verbatim, by definition, it was complete.  Complete is the opposite of incomplete.\n\nhttp://dictionary.reference.com/browse/incomplete\n\nLastly, you insist my replies are out of context.  You claim my comments are construed out of context because I haven't quoted \"completely\".  Given that the parent comments are immediately above the current post, they aren't hidden or removed from view.  I'm not hiding anything.  I'm not circumventing anything, or misdirecting anything.\n\nFrankly I feel very confident that I haven't intentionally replied out of context, but feel free to bring up an example since you feel so strongly about it.\n\nGiven that you're batting 0 for 2 so far, I'm really not all that worried."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-27
    body: "\"Please look it up.\"\n\nExactly, it matches my understanding of the word. Editorialize:\n1.  to set forth one's position or opinion on some subject in, or as if in, an editorial. \n2.  to inject personal interpretations or opinions into an otherwise factual account.\n\n\n\"I took 2 years of journalism classes,\"\n\nPerhaps you should ask for your school money back?\n\nGiven that the parent comments are immediately above the current post, they aren't hidden or removed from view. I'm not hiding anything. I'm not circumventing anything, or misdirecting anything.\n \n\n\"You suggest I altered my quoting practice to editorialize\"\n\nNo quite the opposite, actually. Perhaps the problem lies in reading comprehension, it will explain most of your responses and comments.\n\n\n\"Given that I quoted every word Janne wrote verbatim, by definition, it was complete.\"\n\nBy removing the original issue, you removed the context of the answer. An hence witout the part, context, making it incomplete. Basicly you are editorializing.\nIt's easily seen if you merge the comments.\n\nAnd this was obviously the third three-pointer, but given your problems it may have come as a surprise.\n"
    author: "Morty"
  - subject: "Re: nvidia issues"
    date: 2008-06-27
    body: "\"Perhaps you should ask for your school money back?\"\n\nOne of the two of us works for a large newspaper company.  Don't attempt to lecture me on editorializing.  Stating an opinion is always editorializing.  When I state an opinion, I'm clear about it.  I try to clearly seperate subjectivity from objectivity.  Perhaps you missed the 5 times or so when Janne was saying my opinion was factually wrong, missing the difference between facts and opinions.\n\nYou insist that my quoting style is editorializing.  I follow a standard style and quote every word.  You insist that it is incomplete.\n\nHere is the kicker.  Not only is it not incomplete, and not only have I not gone out of the way to slant things by quoting (since I quote the same way for everybody) but you are doing exactly the thing you are accusing me of.\n\nI've got another definition for you.\n\nhttp://dictionary.reference.com/browse/en:hypocrisy\n\nYou are only quoting my words, and not the context.  Let me repeat.  You are only quoting my words and not the context.  So how is it horribly wrong when I do it?\n\nGet off your high horse immediately.  It doesn't suit you, or your attempted position.\n\n\"Perhaps you should ask for your school money back?\"\n\nAgain, it landed me a job in an incredible newspaper company.  And one of us has a good understanding of said industry."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Since by removing your original comment in which Jannes reply was aimed, making your out of context reply harder to spot. So intentionally or not you did editorialize.\n\nThe standard practice is not to consequently remove everything one level above in a discussion, when required you should include necessary information to keep the context clear.\n\nAnd my parent comment including a example with 3 levels clearly show this."
    author: "Morty"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"Accusing someone of BS is a personal attack\"\n\nNo it isn't. Let me spell it out for you. If I called you a \"retard\", that would be a personal attack. If I called you an big-eared shithead, that would be a personal attack. If I called you a fatso, that would be a personal attack. In short: if I attacked your PERSON, it would be a personal attack. And I'm noitn doing any of that stuff.\n\nIf I call your comment BS, it's NOT personal attack, since it's directed at your comment, not to your person.\n\n\"Furthermore, the point in contention here is that Janne repeatedly accuses me of being a troll, and now contends that one negative comment denotes being a troll regardless of past behavior\"\n\nWhat I heve seen is this comment here, and the constant whining about the desktop-icons. And do you want to know what the problem with your comment is? It's this:\n\n\"Should I read this as, \"we've talked to Nvidia, they know what the issue is, and they've assured us it should be fixed\" or, \"like Flash working in every browser but Konqueror, we have no intention to fix the issue, so we're putting it off on someone else.\"\n\nIs that latter half REALLY necessary? You could have made your point with a lot less negativity, but no. You had to add in some uber-negative whining and complaining.\n\n\"Dissenting opinions are in fact invaluable in any community driven product that strives for quality.\"\n\nDissenting opinions are indeed valuable, and I have provided my share of those (when talking about Krunner for example). What is NOT valuable is whining that basically says \"So are you going to keep on sitting on your asses and do nothing?\".\n\n\"I get an applet running on top of my desktop which looks terrible\"\n\nThe look of the applet depends on your plasma-theme.\n\n\"or I replace the desktop containment for a new one that has no support for wallpapers\"\n\nSo this is about your wallpaper, is that it? And at least the Plasma-faq says this:\n\n\"You can have icons and launchers (shortcuts) by dragging them from Dolphin or the K-menu.\" \n\n\"We're regressing horribly here.\"\n\nAn old and busted system is being replaced by a new system that is more flexible and offers more features. Regression indeed....\n\n\"And frankly, opinions can't be wrong by definiton. No one proved me wrong.\"\n\nYou were utterly defeated. You ran around claiming that KDE wont support icons on the desktop. You were 100% wrong there.\n\n\"You're forcing people to operate the desktops differently and I guarantee you will lose a bunch of users over it.\"\n\nEvery UI in the world forces the user to do certain things, including KDE. Even your old precious desktop-system forced it's way to users, since either you used it in the only way it works, or you didn't use it at all. This new system offers more flexibility and features.\n\nWhy aren't you whining how the old desktop-system also forced users to do certain things?\n\n\"You repeatedly call me a troll and then deny making personal attacks. You call me a whiner and deny making personal attacks. Do you understand what constitutes a personal attack?\"\n\nI do, but apparently you do not.\n\n\"You insist nothing changed, when it did.\"\n\nIt changed for the better. If you were in charge, we would still be stuck at KDE1, since nothing could ever be changed.\n\n\"No, several people didn't want their icons in an applet. For once, an applet does not operate the way a normal window does. You insist anyone who doesn't like what you like is ignorant. I won't even begin to describe that philosophy.\"\n\nI read some of the blogposts in question. And they basically claimed that you can't have any icons in your desktop. But you can.\n\n\"That depends on the issue here. That is why I raised a question.\"\n\nWhat you could have done then is to ask something like \"what exactly is the issue here?\". Did you do that? No. What you basically said is \"Or is this really a problem in KDE/Qt, but you guys are lasy assholes who feel like passing the puck to someone else?\"."
    author: "Janne"
  - subject: "Epilogue"
    date: 2008-06-26
    body: "\"If I called you a \"retard\", that would be a personal attack. If I called you an big-eared shithead, that would be a personal attack. If I called you a fatso, that would be a personal attack. In short: if I attacked your PERSON, it would be a personal attack. And I'm noitn doing any of that stuff.\"\n\nYou call me a troll, and you call me a whiner.  In direct response to those two things I called personal attack, and you said those two items weren't a personal attack.  You then repeat the charges just paragraphs after you insist that you don't make personal attacks.\n\nAnd frankly, saying that someone is full of BS is paramount to calling them a liar.  My word is very important to me.  Calling me a liar is very much a personal attack.\n\nYour posts are without merit.  You rarely post anywhere remotely related to the topics at hand.  You throw about incenidery remarks without care or apparent cause.  You displayed positive emotion when you thought you had me riled.  You repeatedly use terms like \"win\" and \"defeat\".\n\nThese are the systematic patterns of a troll.  I can tell by your worsening typing skills with each post that you emotionally invest in trying to get under my skin.  Let me be explicit.  I could care less.  I told you to grow up precisely because you need to.  You attacks are precisely that attacks, transparent and needless.\n\nLet me preempt the accusations of hyprocrisy.  I am now in return addressing you directly.  Some might suggest I turn have become a hypocrite for making such a personal response.  I have only responded in due form to a litany of personal attacks.\n\nStating that I don't like the apparent direction of KDE isn't factual.  I'm not sure how you're so utterly convinced I'm been proven wrong.  You fixate on minute details and miss the overall message.  You argue incessantly when there is no need.\n\nI understand that some people feel very strongly about KDE.  I'm one of those people.  I've long advocated people to use KDE.  Those who insist I'm some troll or hater have completely missed the mark.  Yet attacking me is not the same as defending KDE, which I feel you mean to do.\n\nI'm certainly not the only person unhappy with the 4.0 release, nor with how the 4.1 release is shaping up.  I'm certainly not the only person unhappy with the future direction of KDE.\n\nI voiced some concerns during the 4.0 alpha stage, and was repeatedly rebuked for blind advocates who insist one must only say positive things about a project.  I was repeatedly told everything would be golden by 4.0 and it certainly wasn't.  Then I was told everything would be golden by 4.1 and it won't be.\n\nFurthermore, I believe one of the biggest mistakes of the 4.0 to 4.1 cycle was to hold back important features and fixes for 4.1 when they could have been put in the 4.0.x trunk.  It was only when people voiced their displeasure did backporting happen.  Voicing displeasure can in fact lead developers to realize what their audience wants.\n\nI'm not concerned reading blogs that many things that just missed the 4.1 cut are being aimed for 4.2, when again some of these things might best be served in a 4.1.x release.\n\nI voiced repeatedly displeasure at empty white space all over the Oxygen design, and was rebuked.  Yet when review after review mentioned the same things, eventually people listened and the design was tightened considerably.\n\nI mentioned that lack of consistent design, which many reviewers picked up on as well.\n\nI think Oxygen is nearly great.  I think KDE 4 is nearly great.  But I worry a great deal about the direction of the project.  Microsoft is losing market share, and there are some opportunities to make powerful in roads right now.  I'm worried that the KDE team is going to squander those opportunities.  Even worse, I fear the philosophy that made KDE so great is dying."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-27
    body: "Beside what he did in the past, he's right here.\nHe has the right to request when/if nvidia issue is gonna\nbe resolved and if it's just another try to hide an issue\nunder the carpet, as:\n\n1) Linux users +3D = nvidia (no choiche here, gaming or blender)\n\n2) It's a bad habit of oss to hide issues under the carpet. Nobody\nis saying things like \"kde 4 is trash if don't work as I want with\nmy nvidia card\", so please don't act as if anyone did. We're asking\n\"does it makes sense for us (nvidia users) to wait or should we move\nto something else?\" (no offense, no judging, just please consider\nthat we MAY have some needs, after all)\n\nAbout what I did in the past, yes, you can tell me I was trolling and BSing\nbecause I was the first to announce that KDE-4.0 (.0) wasn't ready for\nproduction.\nI got attacked for that and I don't care, everyone who tried it can now\nconfirm that I was widely right....."
    author: "nae"
  - subject: "Re: nvidia issues"
    date: 2008-06-27
    body: "\"It's a bad habit of oss to hide issues under the carpet.\"\n\nExcept, of course, that this problem has exactly nothing to do with open source: the problem is caused by your choice for closed source software. You may feel that you had no choice, but the fact is that KDE4's effects work really well on my intel chipset with its open source drivers."
    author: "Boudewijn Rempt"
  - subject: "Re: nvidia issues"
    date: 2008-06-28
    body: "Again, there's NO CHOICHE for who needs 3D,really.\n(gaming but expecially graphics like blender)\nATI isn't a choiche at all, and after 3 tries and 3 boards\nI'll try another ATI next life (thanks to ati fanboys\ncontinuing to say the driver was perfect while it was crashing\neverywhere).\n\nIntel is not a choiche for performance, and anything else\nis ridiculously slow or expensive."
    author: "nae"
  - subject: "Re: nvidia issues"
    date: 2008-06-28
    body: "So what? It still has nothing to do with bad habits of open source development but is caused by the bad habits of proprietary, closed source software. Because you want games and blender, you apparently need an nvidia card with a proprietary, closed source driver driver. Shucks. That may mean you will have to do without fancy window effects in KDE4 until you have convinced nvidia (whose customer you are) that they need to fix the software you get from them. Open source developers cannot help you there."
    author: "Boudewijn Rempt"
  - subject: "Re: nvidia issues"
    date: 2008-07-06
    body: "No, the real issue is that I want (games OR blender) AND Kde. As Compiz\nis free from that bug, this means there's a way to have composite working fine.\nStill I'm not asking for people to fix these bugs for me, today or tomorrow, just for a bit of transparency in how this issue is managed. \n\nIs this to be pretending? Is this to asking open source to be more open than it is?????"
    author: "nae"
  - subject: "Re: nvidia issues"
    date: 2008-07-06
    body: "Everything is completely open and transparent, it cannot be more open. \n\nYou just have to do some homework to track the issue yourself. Lurk at all relevant mailing lists. Lurk at the relevant irc channels. Subscribe to the commit mailing lists of all relevant projects. Gather the information yourself. \n\nExpecting busy people to make time to explicitly feed you the information you need, yes that is asking too much."
    author: "Boudewijn Rempt"
  - subject: "Re: nvidia issues"
    date: 2008-07-12
    body: "Are you kidding me?????????????????\n\nT.J.Brumfield asked POLITELY if the nvidia bug \n\"left in the hands of nvidia developer\"\nwas meaning that:\n\na) KDE Team started a collaboration with nvidia Team to discover and resolve that bug (and in this case I'll wait)OR\n\nb) KDE Team washed his hands and sent the hot ball to nvidia, (you may add \"damn closed drivers\" if you like, but in this case waiting would just be \nmuch more frustrating, more or less like waiting for a 100% working 3D openGL \ndriver on an ATI card).\n\nAnd the thread became an infinite flame and various attacks for a 100%\nlegitimate question (\"will my nvidia work in some months, or should I change \ngraphic card or move to another DE?\"). \n\nIT'S \"A)\" OR IT'S \"B)\" , THERE'S NO SOMETHING LIKE \"AB)\" OR \n\"A but it's closed drivers faults as them are evil and mum will sent me sleeping without dinner if I help there\". \n"
    author: "nae"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "T.J., do you actually see the irony in *what* you say and *how* you do so?\n\nI'd kindly like to ask you to not post to the Dot anymore. Your comments clearly show that you don't care about understanding the actual issue -- not only in this particular thread. I therefore conclude that you either don't have the communication skills discuss on a level reasonable for this forum, or that you just don't care about the information you seem to be asking for and aim at just being negative. Both of those are showstoppers when it comes to interacting in the KDE community.\n\nSo please don't post to the Dot anymore. Thanks for understanding."
    author: "sebas"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "T.J brought up a valid technical issue, which Janne turned into a personal attack. So why ban T.J..? I see his contribution to be more useful for KDE than the Janne kind of fanboyism.\n\n"
    author: "andras"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "I think it is a valid question to ask if the issue really lies with Nvidia, and if this will be addressed.  Someone did thankfully answer stating that the issue is xrender code in Nvidia's driver.  So perhaps this really is their issue.  QT 4 apparently heavily relies on xrender, and KDE heavily relies on QT 4.\n"
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Most of T.J comment did not bring up anything valid, most of it was simple trolling. Bringing up one small relevant issue in a large troll post and lines like \"calling me a troll again\" are both classic troll technics. He could easily have brought up the valid issue whitout it. And with Jannes respons you clearly see whats happen when you feed the troll."
    author: "Morty"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "T.J. indeed brought up a valid technical issue, just a pity that this has been discussed several times, and the problem is _really_ because of the nVidia driver (has been explained by another guy here)\n\nAnd also usually I see T.J. as the one who brings valid issues and not trolling, the problem is just how he conveys the message and sometimes it is quite hard to convince him how a change (especially a radical one) can be good for us in the future :)\n"
    author: "fred"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"T.J brought up a valid technical issue, which Janne turned into a personal attack.\"\n\nWhat \"personal attack\"?\n\n\"I see his contribution to be more useful for KDE than the Janne kind of fanboyism.\"\n\nIf you see utterly negative whining as \"useful\", then go ahead. That does not make it a fact though."
    author: "Janne"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Janne, I think you should try and read T.J with an open and fair mind. A lot of what T.J brought up are facts. I am a hard core KDE lover but I have to admit that KDE 4.1 still has quite a few issues, which I can live with because it's a beta! Look at Vista, it has a bag of misconstruction and still it's been sold as a finished product. \nT.J is not a KDE hater and because he is not I am sure that what he is saying is meant to be constructive."
    author: "Bobby"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"Janne, I think you should try and read T.J with an open and fair mind.\"\n\nI did. And while the issue (problems with NVIDIA) is real, his attitude sucks. He basically asked \"are you just going to sit on your lazy asses, and let others fix this problem?\".\n\n\"I am a hard core KDE lover but I have to admit that KDE 4.1 still has quite a few issues, which I can live with because it's a beta!\"\n\nSo how about reserving your final judgment until it's actually released? We can't really comment on the qualities of 4.1 since it hasn't been released yet.\n\nIt seems to me that quite a few people don't understand what a huge undertaking KDE4 is. 4.0 got quite a bit of flak because it felt half-baked when compared to 3.5. That issue was partly created by communication-problems, when people expected something else than what was going to be delivered. \n\n\"Look at Vista, it has a bag of misconstruction and still it's been sold as a finished product.\"\n\nIt's a good thing that you brought up Vista. Microsoft expects you to BUY Vista. They are going to kill support for XP and they are trying to force everyone to move to Vista instead. Vista is a pig, and it will likely require the user to buy new hardware.\n\nWhat about KDE? Well, the developers are not trying to sell KDE4 to you. They are GIVING it to you, for free! You have the choice of using it, or not using it. If you want, you can keep on using KDE3.5. Both 3.5 and 4 will work fine on older hardware, and KDE3.5 is a fine desktop that can serve you for a long time.\n\nLooking at the amount of complaints and vitriol directed towards KDE4, I keep on thinking \"do these people forget that they are being given all this software for free?\". I could understand the complaints if paying customers complained about being forced to move to a half-baked system (as is the case with Vista). But that is not the case here. We are being given a gift. A gift of software.\n\nThis whole situation is kinda similar if I gave someone a car for free, and then he started complaining \"what is this crap? I don't want a car, I want a motorcycle! This sucks!\". There's a feeling of entitlement that should not be there. When I should see gratitude, I see feeling of entitlement."
    author: "Janne"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Somebody with brain at last. I often critizize T.J but if the man is right then by God accept it people!\nLike you said andras, it's a valid technical issue that T.J brought up so I can't understand the fuss. \nT.J has made a tremendous contribution here even if it might come across as negative at times. On the other hand, would we see such great strides of improvement in KDE 4 if people like T.J weren't pointing out issues that they are having with the DE? a-non is one negative .... and nobody bans him so why do they want to ban T.J?\nPeople, let the man express himself, after all open source is about freedom, isn't it?"
    author: "Bobby"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"T.J., do you actually see the irony in *what* you say and *how* you do so?\"\n\nIrony is by definition unexpected.  If I understand the contrast you apparently see, then it wouldn't be irony.\n\nHowever I don't see what you're getting at.  I'm not above admitting I'm wrong.  If you feel that I'm doing something wrong, then please address it directly and I will take your post into consideration.\n\nHowever, my growing temperament has more to do with how others have dealt with me in very uncivil fashion rather than address my questions directly.  I ask questions not to infuriate or instigate, but because I'm looking for answers.\n\nUnless I'm mistaken, the civil response to a question is an answer.  I don't feel that I'm out of line for expecting exactly that."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"However I don't see what you're getting at. I'm not above admitting I'm wrong. If you feel that I'm doing something wrong, then please address it directly and I will take your post into consideration.\"\n\nUntil you actually see that, I'd be happy if you refrained from posting. I've given the whole discussion some thought, and while I also don't like Janne's attitude, yours crossed this line. This kind of behaviour, indicated by profanity and just not understanding others puts you in the category of \"poisonous people\". Note that I did not say there's nothing useful in your posts, it's the annoyance / use ratio that makes it prohibitive. Or put simply, you're doing more harm than good. (And I'm not talking about this single thread, it's a behaviour that postings under your name have exposed repeatedly in the past.)\n\nIt would good tactique to not just respond to every reply and instead follow those of others, keeping more silent. People like you have drained enough energy from real contributors already.\n\n"
    author: "sebas"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"Until you actually see that, I'd be happy if you refrained from posting.\"\n\nI asked very politely for to explain exactly what I'm supposedly doing wrong.  I very civilily stated that when I ask questions, I'm just looking for answers.  You response was to evade my questions.  How appropriate.\n\n\"puts you in the category of 'poisonous people'.\"\n\nYou're not addressing ideas here, but rather me as a person.  How are comments like these not personal attacks?  When someone disagrees with you, the appropriate response is not to attack them personally.  I post a whole lot on a whole lot of forums.  Feel free to Google up enderandrew, as I'm the only one.  You'll find someone highly regarded and respected in every community I'm in.  Someone seeks me out and replies to every one of my posts with vitrol, makes repeated personal attacks, and the issue is me because I told someone to grow up?  I responded in kind after weeks of carefully directed attacks.\n\nThat by definition makes me a poisonous person?\n\n\"It would good tactique to not just respond to every reply and instead follow those of others, keeping more silent.\"\n\nApparently the ideal model in an OSS community is to keep silent."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "I'm in full agreement with TJ about just about everything, from his concerns with the project to his feelings about being attacked.  Difference being, I don't post about it all the time.  Like you are suggesting to TJ, I rarely post here anymore.  I also don't use KDE4 and at this rate never will.  I'm consoled by the thought that KDE4 probably won't be in Debian Stable for years, so I can keep using KDE for a while.  But I don't like how KDE4 is evolving (or how it performs), and I don't like the way you treat people who express concerns.  TJ, maybe you should give up.  Let them have their grand experiment."
    author: "MamiyaOtaru"
  - subject: "Re: nvidia issues"
    date: 2008-06-26
    body: "Please Sebas,\n\nAlthough T.J. can be annoying he did have a very valid question here.\n\nIf someone should be corrected here it is Janne for his rather plump communication in this thread.\n\nAnd therefore I think you overstep a line here. Do you only want the fanboy users? That is what you will get with this kind of response.\n\nAnd T.J. , can you please stop responding to the attacks? Please keep strictly to the case? It seems like all your posts have put you in a negative light and only seeing your name anoys some people.\n\nRegards Birger"
    author: "Birger"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "\"Grow the fuck up.\"\n\nI'll take this as an admission of defeat. Thanks for playing."
    author: "Janne"
  - subject: "Re: nvidia issues"
    date: 2008-06-26
    body: "OK. You just proved that you were, after all, just trolling. Only trolls seek to \"defeat\" their \"opponent\", rather than aiming for a constructive discussion. Have a nice day."
    author: "Martin"
  - subject: "Re: nvidia issues"
    date: 2008-06-26
    body: "Please Janne,\n\nThis does not help. Can you also please stop fueling the fire?\n\nRegards Birger"
    author: "Birger"
  - subject: "Re: nvidia issues"
    date: 2008-06-26
    body: "So out of the two posts:\n\n\"Grow the fuck up.\"\n\nand the response to this:\n\n\"I'll take this as an admission of defeat. Thanks for playing.\"\n\nyou take issue with the latter?\n\nThe mind boggles, quite frankly."
    author: "Anon"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "> Does anyone actually know specifically what the issue is, and how likely it is to be addressed?\n\nthe problem is bad hardware xrender acceleration and an unnamed problem that amplifies this on 8xxx and 9xxx cards. making nvidias xrender actually slower than some software implementations (at least on my machine - 8800gt and an old amd64).\n\nqt4 and newer versions of gtk/cairo use xrender way more extensivly than qt3. it seems qt3 doesn't use much of xrender at all, when comparing desktops on my machine. qt3 based painting is quick, everything else is slow as hell. meaning scrolling of simple webpages (like this one) isn't smooth with kde4-konqueror or cairo-based-firefox on my machine. thats quite impressive when you consider this is a allmost top-of-the-line graphicscard, way oversized for a simple desktop pc.\n\nthe solution is working xrender acceleration. thats planed for an later driver release. currently they are working on memory management issues, that should bring 8xxx and 9xxx cards back in line with nvidias xrender performance.\n\n\nto me it seems that's what you get for not using x-technology. nvidia tries to reuse as much code as possible bitween their driver implementations. so there is no xaa or exa, and there will probably be no glucose for nvidia. they have their own acceleration engine that didn't keep up with the advancements of the linux desktop.\n\nthe current \"solution\" is to use xgl. ironically, nvidia provides one of the best opengl drivers to date, so you'll get an amazing desktop performance.\n\n\nso, to address your trolling: this has nothing todo with kde. trolltech isn't to blame either. nvidia still has the best 3d drivers in the world. but their 2d linux driver just SUCKS for current hardware. unfortunate, when you look at the smooth drivers they produce for windows and osx."
    author: "ac"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "Seems like some people have problems with 7600s as well; \nwhile I often can't reproduce them with a 7100 (which is really a 6200, of course, and hence has even more mature drivers), and the far lower-end Intel embedded GPUs also seem to perform well.\n\nHowever, I would not completely absolve our end and TrollTech from responsibility, either. Qt4.4 has some serious performance regressions --- see e.g. http://troll.no/developer/task-tracker/index_html?id=203591&method=entry,\nand in some cases we may not be making the best use of it to get top performance. Unfortunately, however, the vast differences in performance of different drivers on different things makes it really quite hard to track and address things on our end properly.\n"
    author: "SadEagle"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "i don't think one can blame desktop software for not being 100% efficient on all hardware when it comes to rendering.\n\nwhen we talk about those nvidia problems, we are not talking about a few percent in some benchmark, but about a complete unuseable desktop for the all users with certain hardware.\nnvidias xrender acceleration is currently completely cpu bound on my machine, so something must be realy wrong.\nwhen i do updates on my gentoo machine using kde4-konsole, i can literally see it repaint the whole window, from top to buttom, line after line.\nkde3-konsole renders smooth on the same system.\n\nthose problems don't exist on any other driver i know of. and it doesn't happen on windows nor mac. so, while neither qt or kde may render everything perfect, they most likely don't have anything to do with this problem."
    author: "ac"
  - subject: "Re: nvidia issues"
    date: 2008-06-24
    body: "Thanks for your response.\n\nI have the same issues however with my 7600GT, and with an onboard Nvidia 6100 chipset."
    author: "T. J. Brumfield"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "I agree with you here. I don't use Konqueror as a web browser because of such issues and it's as slow as a lawyer going to hell.\nCompiz Fusion (like I mentioned above) is at least 4 times faster than KWin with it's effects turned on.\nThe Folder View is not a bad idea but it's looks like something that is still in the early development stage - I don't know, it feels like something is missing but I can't say exactly what... I trust the developers though and I am sure that they will deliver as usual :)\n\nNvidia is like you put it, bugging the heck out of us all. It started with the installation of openSuse 11 RC1. I installed the OS - all went well, booted and then I was greeted with a blinking screen, decorated with every imaginable colours on earth. Imagine I were a Newbie, I would say that Linux is utter crap and go back to Windows. These are issues that have to be solved before Linux can really challenge Windows at the noob level.\n"
    author: "Bobby"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "I'll tell ya what's missing (not sure if it's been done for b2 as I left it installing this morning), right click > [file operations].\n\nI think it's great to be honest, not only I've got one for desktop icons, but also another one for my mldonkey incoming folder so I can quickly access anything I'm downloading."
    author: "NabLa"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "> I'll tell ya what's missing (not sure if it's been done for b2 as I left it installing this morning), right click > [file operations]\n\nIt's available on my 4.0.83 (= 4.1 Beta 2) packages from openSUSE."
    author: "Stefan Majewsky"
  - subject: "Re: nvidia issues"
    date: 2008-06-25
    body: "Yes the present version has improved, it even has a slidebar so that I can make it really small and still can access my folders but I think that there is still room for improvement ;)"
    author: "Bobby"
  - subject: "oneclick link for beta2"
    date: 2008-06-24
    body: "The link for the one-click install file points to the KDE:/KDE4:/Factory:/Desktop repository.  It seems that this does not have 4.0.83 yet (rather it has 4.0.82), no doubt it is building as we speak but in the interim you can use the KDE:/KDE4:/UNSTABLE: repo instead which does have it.  ie use this link \n\nhttp://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_11.0/KDE4-BASIS.ymp"
    author: "opensuse user"
  - subject: "Re: oneclick link for beta2"
    date: 2008-06-24
    body: "The upload of the packages might still be in progress, and AFAIK some dependency issues are still being worked out."
    author: "Sebastian Kuegler"
  - subject: "Re: oneclick link for beta2"
    date: 2008-06-24
    body: "Yep. It's come though now!  So use the original links folks http://www.kde.org/info/4.0.83.php"
    author: "opensuse user"
  - subject: "Perfomance problems not only for nvidia-cards"
    date: 2008-06-24
    body: "KDE-4.x is also unusable for me with a ATI Radeon card. Scrolling is not smooth as in KDE-3.x, all GUI elements react slower."
    author: "Jens"
  - subject: "Re: Perfomance problems not only for nvidia-cards"
    date: 2008-06-24
    body: "As I wrote in my blogs about this topic, KDE/Qt 4 uses hardware acceleration quite extensively. Most proprietary drivers don't really work very well in that area, thus Qt 4 seems much slower. Rather unfortunately, but nothing Trolltech or KDE can do about it. The graphicscard vendors will have to fix it, and they probably will - in time. In the meantime, Intel hardware performs superior - thus that's what I'm gonna buy, and I advise you to do the same..."
    author: "jospoortvliet"
  - subject: "Re: Perfomance problems not only for nvidia-cards"
    date: 2008-06-25
    body: "Particularly hard to do that as intel hardware doesn&#8217;t seem to exist in AGP / PCI Express variants, only on-board."
    author: "Henrik Pauli"
  - subject: "Could bennchmark app help make drivers faster?"
    date: 2008-06-25
    body: "Maybe it would help to make some great looking benchmark software, like LinuX Eyecandy Mark or somethig, based on Qt 4. It would be an order of magnitude more demanding of hardware than the currently released Qt4/KDE4 software. There could also then be a site where people could freely upload their benchmark results and compare them. And these results would include info about drivers used and hardware used. Maybe this braging with higher scores could drive in some way the demand for driver writers to make better drivers with more performance. Hopefully no hardware manufacturer would like to be the slowest in that benchmark. Probably the graphics ninja Zack Rusin could come up with something like this, if he has any time left of course."
    author: "Jure Repinc"
  - subject: "Re: Perfomance problems not only for nvidia-cards"
    date: 2008-07-01
    body: "I guess I'm having the same problems with my S3 Savage 8MB on my notebook. KDE4 apps seem to draw a lot slower.\n\nThe drivers are fully opensource and just included with X.org, so I hope the devs will (can?) still fix them :)"
    author: "Z_God"
  - subject: "Re: Perfomance problems not only for nvidia-cards"
    date: 2008-07-02
    body: "Well, it's not like we don't have work to do on our end, but, well, \none wouldn't expect the latest graphics card models to perform comparable to an ancient S3 card card now, would one? THAT is the performance problem with some NVidia HW + driver combos --- they don't perform anywhere near as well as they should. Mid-range graphics cards from the most current generation should not perform much worse than low-end/cheapo cards from a few generations ago. \n\n\n\n"
    author: "SadEagle"
  - subject: "Re: Perfomance problems not only for nvidia-cards"
    date: 2008-06-25
    body: "Do you use the proprietary fglrx driver? I always thought along these lines: Hey, fglex is proprietary but it comes directly from the manufacturer - it surely must be better than the Open Source driver, even if I sacrifice the \"openness\".\n\nBut no so - I found out after *LOTS* of trial and error that the \"radeon\" driver is WAY better than fglrx. There are some minor display artifacts, BUT: It uses much less CPU here (with fglrx quickly around 40%, even if simply moving a window, with radeon 3%). So, if you didn't do this already (and there are no other reasons why you are using fglrx), I'd recommend to at least give radeon a try. I didn't need many additional settings in xorg.cofn and even video runs much better. The only settings I did enter were:\n\nOption          \"DRI\"   \"true\"\nOption          \"EnablePageFlip\"        \"true\"\n"
    author: "Michael"
  - subject: "Re: Perfomance problems not only for nvidia-cards"
    date: 2008-11-30
    body: "I used the free radeon driver. My radeon 9250 is too old for the actual proprietary driver."
    author: "Jens"
  - subject: "MacOS X packages?"
    date: 2008-06-24
    body: "Hello,\n\nanyone knows where to get precompiled Mac OS X packages of Beta 2? http://mac.kde.org is still Beta 1, are they expected to show up there later?"
    author: "FL"
  - subject: "Slim Theme"
    date: 2008-06-24
    body: "I just bought a eeePC 701, and was looking for a way to use less screen space, because a lot of windows just fo not fit it's resolution. Changing font size to 7 helped a lot, but still widgets like buttons take too much space.\nSo my questions are:\n- is anyone willing to create a slim/compact theme for kde3 or kde4? (I'm using 3 on the eee because is can better customized to fit)\n- is it possible to use qt4 stylesheets with kde apps? if so, it would possible to create a generic theme that only loaded one of those and users could create a qss file to theme KDE4 apps?\n\nI could give a try on creating a theme, but I do not know if it's possible to change widgets spacings on a kde theme :-(\n\nThanks for the help."
    author: "Iuri Fiedoruk"
  - subject: "Re: Slim Theme"
    date: 2008-06-24
    body: "Reduce dpi in xorg.xonf"
    author: "Andrius"
  - subject: "Re: Slim Theme"
    date: 2008-06-24
    body: "Won't this only affect fonts? \nI've already reduced fonts a lot, but I'll give a try anyway."
    author: "Iuri Fiedoruk"
  - subject: "Re: Slim Theme"
    date: 2008-06-24
    body: "Maybe you can try Plastique, a plastik clone by Trolltech. It's already smaller than oxygen, and in time someone might create an even better style."
    author: "jospoortvliet"
  - subject: "Re: Slim Theme"
    date: 2008-06-24
    body: "You mean, for example a style like 'skulpture' (available on kdelook). It is the best style for kde4, even outrules oxygen in terms of polish and smoothness... "
    author: "Sebastian"
  - subject: "Re: Slim Theme"
    date: 2008-07-01
    body: "\"outrules oxygen in terms of polish and smoothness\" if you are talking about this theme:\nhttp://kde-look.org/content/show.php/Skulpture?content=59031\n\nIt has no relations with oxygen style at all it's absolutely different theme. And in my opinion worse. Oxygen should be polished a lot, but not changed with some new theme. Basicaly oxygen is very good style and it's important to develop it. Ability to do it more slim would be essential for it."
    author: "Justas"
  - subject: "Re: Slim Theme"
    date: 2008-06-24
    body: "To make me clear:\nhttp://protomank.googlepages.com/spacing.png\n\nI want to find a way to remove/minimize the spacings marked as red lines.\nThose are useless actually, in small screens. I want something like:\n\nhttp://martin.ankerl.com/2007/11/04/clearlooks-compact-gnome-theme/\n\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Slim Theme"
    date: 2008-06-25
    body: "This non-compact gnome theme really has huge buttons, so the need for a compact theme is urgent there. But I do not like the compressed horizontal size of the buttons, the margin is a bit too small for my taste.\n\nWith Skulpture the sizing and spacing depends on font size, so with a smaller font, you get a smaller layout. The width of buttons is increased in steps to make similar-sized buttons get the same size; this could be improved, though."
    author: "christoph"
  - subject: "Re: Slim Theme"
    date: 2008-06-25
    body: "As far as I can tell the minimum width of the left hand splitter with the buttons Iuri feels are too wide is controlled by the radiobutton group and its contents - the buttons at top and bottom are just expanding to fill the available space as set by their default size policy."
    author: "Bille"
  - subject: "Re: Slim Theme"
    date: 2008-06-25
    body: "Yes, but in most cases buttons are not aligned to another widget, they are just side by side (you know, the ok, apply, cancel triad).\n\nThere is too much spacing wasted in most KDE apps. Yesterday I was testing this stile you guys told me about and others, and there was one (sorry, I do not recall witch one) that was using a ABSURD amount of emptiness in it's configuration dialog. \n\nIt's hard having to move windows with alt key pressed often, but I know this is not 100% because of theming, I would love to go and make changes in my most used programs code for them to use less spacing, but this does not make sense, even because those then would look bad in my desktop computer that have a decent resolution :)\n\nThe good news is that I was able to run dolphin using a .qss file to change some parts of it's appearance - currently I saw only tabs being colored. So maybe I can do the tricky using a .qss file. Plus, it's a way to theme KDE that I saw no one using yet that is much, MUCH easier than coding a style in C++. I'll open a bug for KDE to support themes throught qss files, let's see what happens with the idea."
    author: "Iuri Fiedoruk"
  - subject: "Re: Slim Theme"
    date: 2008-06-25
    body: "If anyone is willing to vote/comment, here is my idea:\n\nhttp://bugs.kde.org/show_bug.cgi?id=164955"
    author: "Iuri Fiedoruk"
  - subject: "Re: Slim Theme"
    date: 2008-06-30
    body: "here is a rather basic patch accomplishing what you want. HTH."
    author: "logixoul"
  - subject: "Re: Slim Theme"
    date: 2008-06-30
    body: "Mmmm ... magic numbers ..."
    author: "Anon"
  - subject: "Re: Slim Theme"
    date: 2008-06-30
    body: "yeah, it really really sucks. the code was written in an ad hoc manner rushing to get Oxygen usable with some limited time for coding; if you want to help with cleaning it up, cool, that would be terrific; cheers"
    author: "logixoul"
  - subject: "Re: Slim Theme"
    date: 2008-07-02
    body: "Looks nice :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Slim Theme"
    date: 2008-11-27
    body: "With Skulpture 0.2.0 you can create a \"compact\" layout. But many application use hard coded values, so it will not work everywhere. And you can experiment with .qss files, just pass \"-stylesheet file.qss\" as an argument to the application you want to run."
    author: "christoph"
  - subject: "Xgl?"
    date: 2008-06-25
    body: "Someone mentioned in this thread that using Xgl would reduce the performance issues on proprietary graphics card drivers. On suse10.3/kde4.0.4 I can simply switch to Xgl (and yes, I can even do smooth scrolling in konqueror again!), but all desktop effects are gone. I wonder why this happens. Are there any recommendations on my desktop settings? Will it be possible to use Xgl in 4.1?"
    author: "Sebastian"
  - subject: "Re: Xgl?"
    date: 2008-06-25
    body: "I'm afraid you'll just have to work around the graphics driver issues until the vendors fix their stuff, sorry... You might try to complain at the NVidia forums?"
    author: "jospoortvliet"
  - subject: "Re: Xgl?"
    date: 2008-06-25
    body: "Same problem here.\nThe standard ouput of kwin says something about missing compositing extension, which does not make sense as it works (slowly) when using xRender in Xgl, and anyway, compiz works at normal speed.\nIf Xgl was a real, though temporary, fix that would really be nice !"
    author: "Aldoo"
  - subject: "Re: Xgl?"
    date: 2008-06-26
    body: "Hi,\n\nThough KWin4 was showing good effects already using Xorg, its performance was simply in no comparison to what KWin3 has. But I always thought that was because it was still beta.\n\nBut I tried XGL after seeing posts here and to my surprise (yes!) performance was near to what KWin3 has. For me all the effects (those which were also working before) were working with smooooothness. And scrolling is also smooth, which was not earlier even in KWrite.\n\nNow I am *more* eagerly waiting for KDE 4.1 final...\n\nI'm on openSUSE 10.3 (downloading 11.0) running KDE 4.1 beta1 (openSUSE built)\nMy hardware:\nIntel P-4 2.4C on Intel 865GBF\nXFX nVidia FX5200 128 MB (Driver: 100.14.19)\nSingle X screen running 1024x768\n\nApart from few lines added manually to xorg.conf (for TV output) I did the following: -\nsu -\nnvidia-xconfig --composite\nnvidia-xconfig --render-accel\nnvidia-xconfig --add-argb-glx-visuals -d 24\nas directed here at http://en.opensuse.org/Nvidia\n\nAlso I had (in Kwin4 conf.): -\nCompositing type: OpenGL\nOpenGL mode: Texture from pixmap\nTexture filter: Trilinear\n\n"
    author: "Yogesh M"
  - subject: "Re: Xgl?"
    date: 2008-06-26
    body: "Sorry, but it seems that the speedup was only because I changed (because that was the only change I made except for enabling XGL)\nOpenGL mode: Shared Memory \nto\nOpenGL mode: Texture from pixmap\n\nThough I had logged-in and out several times after enabling XGL, XGL was really not loaded at that time, it was only after restarting that system that I found that what Sebastian was saying was completely right.\n\nSorry again for the XGL thing... but my experience (in earlier post) was 100% *true*\n\nBtw I had disabled shadows."
    author: "Yogesh M"
  - subject: "Raptor Menu?"
    date: 2008-06-25
    body: "I have some questions:\n- Where is the new KDE4 menu Raptor?\n- small panel icons, e.g. kwalletmanager, doesn't have transparent backgrounds?\n- How can I move my icons in my panel?\n"
    author: "Norman"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "If you want to move icons to the desktop or panel then first you have to unlock the widgets (right click on desktop/panel - unlock widgets) then you can right click on any icon in the start menu and add it to the panel/desktop. You can also drag and drop the icons to any desired position on the panel or desktop.\n\nIf you have a transparent panel (use the plasma-theme Glasified) then the icons should have transparent background given that you have a 3D video card which is properly (driver) configured.\n\nCan't help you with Raptor unfortunately. However, you can follow the development here:\nhttp://raptor-menu.org/"
    author: "Bobby"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "Sorry, I guess I misinterpreted moving the icons to the panel with moving them in the panel. It looks like it's still not possible to move them around in the panel like one can with Kicker."
    author: "Bobby"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "yes you can move the Icons within the Panel. \ni am using Opensuse 11.0 with KDE 4.0.4 \n\nRightclick on the Icon in the Panel - select Start Move of Icon\nMove the Icon to where you want to\nRightclick again and select Stop Move of icon\n\n"
    author: "Kavalor"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "Maybe openSuse did some extra tweaks to 4.0.4 but it's not possible to move icons within the taskbar with KDE 4.1 Beta 1 and 2."
    author: "Bobby"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-26
    body: "I am surprised that SuSe didn't commit their changes into trunk, as it is a very requested function..."
    author: "houba"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-26
    body: "I don't believe the openSUSE team can commit directly to the KDE trunk.  They can submit patches upstream and suggest the KDE devs commit them to trunk.  According to openSUSE's idea pages, they do try whenever possible to submit upstream.  It is then up to the KDE team to accept the patches or not."
    author: "T. J. Brumfield"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-26
    body: "Many of the openSUSE members have write access to KDE's SVN.  "
    author: "Troy Unrau"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-26
    body: "I stand corrected."
    author: "T. J. Brumfield"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-27
    body: "Actually I think all of us do: http://en.opensuse.org/KDE/Team . You should recognise a few of those names.\n\nStephan Binner's comments about merging stuff back into Plasma describe normal interaction between a project and its periphery; the same traffic happens between KDE PIM and its Enterprise branch."
    author: "Will Stephenson"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-27
    body: "I think you guys could possibly lay claim to the best shipped KDE desktop out there.\n\nSeriously, I'd love to see you guys work with the KDE Mod guys who develop the kdemod packages for Arch Linux."
    author: "T. J. Brumfield"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "This is an openSUSE addition."
    author: "Stefan Majewsky"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-27
    body: "It wasn't committed to trunk because of the big plasma code change thing, I believe. The code they modified probably doesn't really exist anymore in 4.1"
    author: "kane"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "1)Raptor isnt ready, not going to be released\n2)AFAIK thats not a KDE bug, but a X bug.\n3)You cant (but you can remove them and them them again where you want)"
    author: "Emil Sedgh"
  - subject: "Re: Raptor Menu?"
    date: 2008-06-25
    body: "s/them them/add them/\n\n:P"
    author: "Emil Sedgh"
  - subject: "missing feature (taskbar)"
    date: 2008-06-25
    body: "Wow, kde 4.1 is great! Thanks. But well, I'm still missing one feature I really liked about KDE3, and that's the possibility to configure the taskbar (maybe I just didn't find it yet, if so please tell me). What I liked about KDE3 was that I had my tasks one over the over, whereas they are one next to the other in KDE4. So I had 2 or more colums with 3 tasks each. Is it possible to configure this in KDE4 (meaning I didn't find out how to configure that as for now)? Or isn't it implemented yet? Thanks"
    author: "Anonymous"
  - subject: "Re: missing feature (taskbar)"
    date: 2008-06-25
    body: "You probably find what you are looking for here:\nhttp://www.kde-look.org/content/show.php/multirows+task+manager+by+8siem?content=83177"
    author: "Axl"
  - subject: "Re: missing feature (taskbar)"
    date: 2008-06-25
    body: "The taskbar now automatically starts to use 2 rows if space is tight. As the other poster said, there are ways to get 2 rows by default with another taskbar plasmoid."
    author: "jos poortvliet"
  - subject: "Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "First of all congratulation to the KDE team for reaching another millstone towards the upcoming and much anticipated KDE 4.1 release. \nKDE 4.1 continue to improve but there are quite a few programmes and plasmoids that I am missing in the recently released openSuse 11.\n\nAmarok - am I the only one who can't find Amarok 2 in openSuse 11 repos?\nPlasma extragear: extragear is available but in an outdated version that conflicts with the present KDE 4.1 beta release. \nI am also missing plasmoids like Flickr, where did Flickr disappear to? Bball is not in the repo and the Kweather plasmoid is also missing.\nBtw, has anyone here managed to migrate from Kmail 3 to 4 without much workaround?\n\n"
    author: "Bobby"
  - subject: "Re: Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "I guess extragear should be named kdeplasmoids as it was moved in repo."
    author: "anon"
  - subject: "Re: Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "I have kdeplasmoids plasmoids installed but there are quite a few nice plasmoids that are missing. There was also a plasmoid that shows your partitions and other system infos, I don't remember the name but it's also missing. These were useful plasmoids that were working quite well in previous versons of KDE 4."
    author: "Bobby"
  - subject: "Re: Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "Plasma went through a refactor so probably some plasmoids just aren't ready for KDE 4.1 yet."
    author: "Ian Monroe"
  - subject: "Re: Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "The package extragear-plasma has been renamed to kdeplasmoids4 in KDE:KDE4:UNSTABLE:Desktop because trunk/extragear/plasma was moved to trunk/KDE/kdeplasmoids in KDE SVN. If you used extragear-plasma previously, YaST should automatically upgrade extragear-plasma to kdeplasmoids4."
    author: "Stefan Majewsky"
  - subject: "Re: Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "Like I said, the package kdeplasmoids is installed (extragears not, although they can be found in the repo) but the plasmoids that I mentioned are nowhere to be found."
    author: "Bobby"
  - subject: "Re: Missing Programmes and plasmoids on openSuse 11"
    date: 2008-06-25
    body: "What about Amarok 2? Have you found in in the openSuse repo?"
    author: "Bobby"
  - subject: "Amarok 2 is still in development."
    date: 2008-06-25
    body: "Amarok 2 is still in development."
    author: "Ian Monroe"
  - subject: "Re: Amarok 2 is still in development."
    date: 2008-06-25
    body: "I know but it's possible to install it on openSuse 10.3. I was using Amarok 2 until I updated my system to openSuse 11 a few days ago."
    author: "Bobby"
  - subject: "buggiest release ever"
    date: 2008-06-25
    body: "first : i dont want to blame anyone . i just downloaded the opensuse live cd just to get a taste of the progress kde 4 has made . i am very pleased with the polished kde 4 opensuse has released the other day .\nso after the new kde 4.1 beta 2 desktop has appeared just right from the first klick i got a fatal error message after another . ok klicking it,  helped to continue . but after trying to adjust somethimg i got a white screen - so goodbye kde 4 . whats that ??"
    author: "pete"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "\" got a fatal error message after another \"\n\nPresumably the message said something more than just \"fatal error\" ... ?"
    author: "Anon"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "or buggiest packages? or buggiest video driver? or ..\n\nif you can say what the fatal error message was and what the something you tried to adjust was it might be possible to give some advice."
    author: "Aaron Seigo"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "needless to say all the error messages i got from knotify (i got them after all the adjustments i tried in the kontrolcenter ). the white screen appeared after trying to adjust the panel .\nmeanwhile i found out that others also had big probs with knotify .\nbtw: i was not trying to get some help , i only wanted to share my experience"
    author: "pete"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "Why head your post 'buggiest release ever' if all you wanted to do was 'share your experience'. As far as I can see you're suffering from a single bug of some sort which may or may not be something to do with KDE 4.1."
    author: "Richard Dale"
  - subject: "distro patch (now removed) to blame"
    date: 2008-06-25
    body: "A cool-headed response from Richard.  The knotify4 crash didn't have anything to do with KDE 4.1beta2.  It was an experimental patch we added at openSUSE and thanks to all the bug reports we have already replaced it with a safer one, so no need for any hysteria."
    author: "Will Stephenson"
  - subject: "openSUSE"
    date: 2008-06-26
    body: "I was wondering if it was an openSUSE thing as I ran across it myself today.\n\nThat being said, I have been very impressed with the openSUSE packages.  I'm seriously considering going away from Gentoo to openSUSE on my desktop.  I switched my wife's laptop from Sabayon to openSUSE last night.\n\nIf only you guys supported ext4 and reiser4 as options (even largely hidden by the installer and only opened with a disclaimer).\n\nI'd also like to see you guys work with the KDE Mod team.  They also provide some great patches.\n\nhttp://kdemod.ath.cx/"
    author: "T. J. Brumfield"
  - subject: "Re: openSUSE"
    date: 2008-06-26
    body: "openSuse is the best KDE distro that I have used upto now. I can only recommend this distro and especially the latest version. OpenSuse is also the best distro for people who love bleeding edge software.\nI would also like to try Reiser4 or EXT4 but unfortunately there isn't any option to use these formats on openSuse :( "
    author: "Bobby"
  - subject: "Re: distro patch (now removed) to blame"
    date: 2008-06-26
    body: "Thanks for that :)"
    author: "Bobby"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "And don't you think the developers would have noticed the KNotify error message every other seccond, and not released a beta with such a obvius bug. Did it not cross your mind at all before you worded your rather trollish post?\n\nAnd did you consider that the Suse liveCDs are more or less auto generated and don't see much testing, other than perhaps a quick run on Beineris computer.\n\nAnd the workaround was rather obvious if you bothered to read the error message. Open System Settings->Notifications->Systems Notification->Player Settings and select no audio output. Then enjoy for a live CD, a quite stable desktop beta."
    author: "Morty"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "And I forgot to write, had you said the error message was from KNotify everyone could have told you how to remedy it. So it would not have been needless to say at all, it would(with a different title and less offensive wording) actually have transformed you post from poisonus trollish to relevant."
    author: "Morty"
  - subject: "Re: buggiest release ever"
    date: 2008-06-26
    body: "I have the same problem (but not white screen) but I haven't got the time to file a proper bug report as yet. And like you said, ALL the error messages came from knotify."
    author: "Bobby"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "I can say that it seems a little buggy than beta1, what is bad, but it seems on the other hand more mature in the sense of being more complete and having some parts better finished.\nOverall, I like it, but did not see major differences from beta1, let's hope from now on they focus on stability over features as release date approaches."
    author: "Iuri Fiedoruk"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "Well... I have to disagree with you on that one. 4.1beta2 is a lot more stable than the 4.0.x realeases were. Tell me: did you do as recommended when one gets \"update-errors\"? You know the \"delete your .kde4-dir\"?? \n\n"
    author: "Linus Berglund"
  - subject: "Re: buggiest release ever"
    date: 2008-06-25
    body: "I have the same problem and a bug has already been posted: http://bugs.kde.org/show_bug.cgi?id=164859 (it looks like an openSUSE-specific problem). There is a workaround: go to \"Notifications\" \"System Settings\" and change \"Use the KDE sound system\" to \"Use an external player\" (I've put mplayer),"
    author: "Matteo Agostinelli"
  - subject: "Status of KDEPrint and CUPS 1.3.7?"
    date: 2008-06-26
    body: "Does anyone know the status of KDEPrint, CUPS and KDE 4.1? Are we still using a Qt Print Panel to interface to CUPS in lieu of a release for KDEPrint?"
    author: "Marc J. Driftmeyer"
  - subject: "A question"
    date: 2008-06-26
    body: "I am very impressed with KDE 4.1 betas, but I have a question about adding widgets to the panel and about already added widgets. When I add a new widget the old ones moves to the middle of the panel. Is there a possibility to move them back to their previous position? I would like to have the digital clock on the right side of the panel, but now it is not possible.\n\nCheers"
    author: "Toki"
  - subject: "Congratulations to all KDE guys!"
    date: 2008-06-26
    body: "Well, I've been running 4.1 betas for some time now and I think it's going pretty well. Plasma has now a very good shape and I believe we can see now where the devs are heading, which should provide a configurable desktop as no other exists. Together with the improved XRender composite support (my laptop still have some problems with OpenGL composite, should be improved by the next X release), I must I'm really starting to enjoy the KDE4 experience as a desktop.\nThe betas have been very stable to me for several monthes now, another very good point.\nI also like where KOffice seems to be going and have found apps like Gwenview or Okular really nicely improved.\nOn top of that, there's a pretty obvious sense that there is a lot underneath that will appear as time goes on. I'm convinced that that successive releases of KDE 4.X will bring quite a lot to the table.\nMy thanks all of you for your work!"
    author: "Richard Van Den Boom"
  - subject: "Some steps bakwards in KDE 4 design"
    date: 2008-07-01
    body: "Nice improvement in KDE 4.1, but sadly in my opinion there are and some steps backwards in design. Is it bug?\n\nWhat happened with oxygen style minimize/maximize/close buttons? Buttons was best thing in all style. So if some of them was changed why not all including \"apply\" \"ok\" etc buttons was changed. Now they are completely from different stories. \n\nActive inactive window problem was simply solved with all dimm and etc. effects. Stripes are useful for somebody too, but they are optional, but thees new buttons not.\n\nButtons become ugly when are inactive and I don't know how to turn off that thing.\n\nOk it's subjective opinion, but why delete old style, when already was created new ozone style. If somebody like it do it, create dozens of new styles, but keep old ones and let people choose. \n\nAnother example analog clock, old was very good. Again if somebody like new one, why not to do possibility to have both styles.\n\nDigital ckock and task manager improved, but gone ability to change task managers  size. Tasks on that taskbar looks much worse, pager too, they was nice and simple, why change?\n\nWorst thing in KDE 4 design is wasting desktop area. Huge empty spaces, large bars, and inability manually adjust and this was left  untouched. \n\nI am architect and designer, so watch in to KDE from designer point of view. I think good design is essential. And would like to see KDE as best DE in design aspect.\n\nGood luck"
    author: "Justas"
  - subject: "Re: Some steps bakwards in KDE 4 design"
    date: 2008-07-01
    body: "So you can participate to this project :) feel free to expres your suggestions with developres."
    author: "giricz81"
  - subject: "Re: Some steps bakwards in KDE 4 design"
    date: 2008-07-01
    body: "I subscribed to kde-artists@kde.org mailing list, wrote my opinions and started to participate I hope :) \n\nIs it right place? I'm absolutely new in this area, but I believe in KDE and see it's potential!"
    author: "Justas"
  - subject: "yattah"
    date: 2008-07-02
    body: "I upgraded this weekend from 4.0.1 to 4.1beta2\nit's rockin' solid.\n\nI wish I could move applets by a simple d&d from panel to panel which does not seems to be yet possible. but hey, i'm sure it's coming... \n\nbut so far, it is stable enough for me for daily use."
    author: "somekool"
---
Another milestone on the road towards KDE 4.1 has been packaged and put online for testing. The <a href="http://www.kde.org/announcements/announce-4.1-beta2.php">release notes</a> highlight some features in Dolphin and Gwenview, as well as additional information on where to get the release, make sure you also check your distributor's websites as well. While there are some bugs left, the release already works quite solidly on most people's machines. Performance problems on NVidia chips remain, but we are confident that those will be solved by the teams over at NVidia in one of the next releases of their graphics driver. In KDE 4.1, there is also some preliminary Mac and Windows support coming up. Several apps can be tried by a wider audience on those proprietary platforms this summer already. On the side of Free operating systems, support for OpenSolaris is coming along nicely, but is not free of bugs yet.


<!--break-->
<p>As every Beta, we release this software to gain feedback and to provide a preview of our upcoming technology. When encountering problems during testing, please help us by reporting bugs through <a href="http://bugs.kde.org">KDE's Bugzilla</a> so developers are aware of them and can make the necessary changes. When trying this release you will encounter a number of new things, most of the new features are listed on <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Techbase</a>, make sure to check out that list and give the next KDE a whirl.</p>

