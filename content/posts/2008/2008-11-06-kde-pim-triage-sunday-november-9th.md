---
title: "KDE PIM Triage this Sunday November 9th"
date:    2008-11-06
authors:
  - "mleupold"
slug:    kde-pim-triage-sunday-november-9th
comments:
  - subject: "KDE's PIM is #1"
    date: 2008-11-06
    body: "It's the main reason I use KDE. Good work guys."
    author: "winter"
---
KDE 4.2 is approaching. Now that the end of the creative feature implementation phase is almost at an end with the Hard Feature Freeze on the 17th of November, BugSquad is once more striving to help the developers clean up their old bug reports and do the best it can to prevent new, vital bugs creeping into our release.


<!--break-->
<p>We will be starting the marathon this Sunday the 9th with a KDE PIM triage day. We feel that KDE PIM is a valuable package that is widely used and we know that the developers are pretty busy with implementing the last features before time is up. This means we will be going through already reported bugs and checking if they are still relevant, which are already fixed and which of them should get a higher priority so they can be handled prior to the release.</p>

<p>If you are willing to lend the developers of your favourite PIM package a hand, you are very much invited to join in. We will be meeting in #kde-bugs (Freenode) starting around 08:00 UTC. All you need to bring is some time and a recent copy of KDE PIM (KDE 4.1.x or compiled from trunk). No programming skills needed. Developers and senior triagers will be around to help you get started.</p>

