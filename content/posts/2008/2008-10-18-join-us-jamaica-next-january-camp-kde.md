---
title: "Join us in Jamaica next January for Camp KDE"
date:    2008-10-18
authors:
  - "wolson"
slug:    join-us-jamaica-next-january-camp-kde
comments:
  - subject: "Camp?"
    date: 2008-10-18
    body: "Shouldn't it be called \"Kamp KDE\"?\n\n:-)"
    author: "David Johnson"
  - subject: "Re: Camp?"
    date: 2008-10-18
    body: "Given the current release codename practices, it seems lots of KDE related names will start with a C even *if* they could be written with a K. You've got to live with it - the K times are over and gone! Except for existing apps and people who are not keeping up with the trends."
    author: "Jakob Petsovits"
  - subject: "Re: Camp?"
    date: 2008-10-18
    body: "Given that life is getting faster and faster, perhaps the time of retro-KDE has already come, so K is full-in again ;)"
    author: "King Kong"
  - subject: "Re: Camp?"
    date: 2008-10-19
    body: "You're just saying that so you won't have to change your nick to Cing Cong :)"
    author: "IAnjo"
  - subject: "Re: Camp?"
    date: 2008-10-21
    body: "I thinK the K names should Kome baKK, they were just Kool. :-)"
    author: "Kevin Kofler"
  - subject: "Re: Camp?"
    date: 2008-10-21
    body: "I disagree."
    author: "Cevin Cofler"
  - subject: "Re: Camp?"
    date: 2008-10-21
    body: "I agree though."
    author: "Kevin Krammer"
  - subject: "What a news!"
    date: 2008-10-18
    body: "I am actually planning to fly home next year (between January and February) but now that I know that Camp KDE will be on the Island I will plan my travel for January because I want to be there!\nYou couldn't have chosen a better place to celebrate the launch of KDE 4.2. I feel so proud.\nNuff respect to all a you KDE guys :)"
    author: "Bobby"
  - subject: "Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-18
    body: "maybe not all love, peace and sunshine:\n\n * http://www.time.com/time/world/article/0,8599,1182991,00.html\n * http://en.wikipedia.org/wiki/LGBT_rights_in_Jamaica\n * http://www.google.com/search?q=homophobia+jamaica\n\nthis is not meant to start any kind of fight, just to inform the uninformed.\n\nkde rocks."
    author: "nobody"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-18
    body: "You're welcome to hold it here in holland next time ;)"
    author: "Mark Hannessen"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-18
    body: "There are neo nazis living in Germany, yet we've held Akademy there. Belgium has had to do with very nasty child abuse cases and corruption in the past, yet we've held Akademy there. (I could probably go like that with other locations I've visited to attend KDE events, but you probably get the point already.)\n\nIn case you do assume that something like that matters (it doesn't matter to all, Free Software is supposed to be non-politicial), it's still the question if this is more or less of a reason to a Free Culture conference to such a place."
    author: "sebas"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-18
    body: "Even worse, there are also Nazis living in Belgium. And child abuse(abuse?! sexual violence it is) is also happening in Germany. And more sad news, this is true for every, really every country on earth, because humans suck sometimes.\n\nIt is more a question of not where you go to but who you work with, what means, who you cooperate with, and under which message. Just compare with e.g. Doctors Without Borders. They work under dictators, because they want to help the people, not the dictator. And this get's messaged clearly, at least they try.\n\nAnd Free Software is everything but non-political. It is about Freedom, access to and control of information. Very political, no? There are a lot of laws in this area, if you need a proof."
    author: "King Kong"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "\"Just compare with e.g. Doctors Without Borders. They work under dictators, because they want to help the people, not the dictator. And this get's messaged clearly, at least they try.\"\n\nYes, DWB is the ones you want to use to prove your point. \nBunch of ass-kissing wankers. Your friends disbarred their greek branch when those doctors dared in 1999 to go and help the serbians while they were being bombed (it was big here in Greece. My favorite 1st division soccer club travelled there and played in betweeen bombing runs.). DWB plays god and decides who deserves to live and who deserves to die. The greek doctors were punished because they tried to do what was claimed: take care of the sick in war zones.\nPlease do not use DWB as some symbol of morality.\n\nAs for morality, how about going to a country that has bombed/invaded between 30 and 40 countries since WW2 (not too mention the shadier overthrows)?\nHow about that?\nAre we going to be playing that game?\nLet's play, which country has its soldiers in the most countries in the world right now, bases and so on..... C'mon, which country would that be?\n\nThat's not what we are talking about here. We are talking about Free Software.\nThe dangers against in the US are much higher than elsewhere in the world. If we decide to not go to the US, Jamaica or any other distasteful country, it better be because it pertains to Free Software, not the host country's reputation and objectionable actions.\n\nI've been to Jamaica 3 times and to Cuba 5 times.\nThe behaviours are the same as elsewhere except the social element and violence are vastly different in both islands. If I retired, I would go to Cuba in a heartbeat.\nz.\n"
    author: "zeke"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-19
    body: "It does not matter that it has been held in places where there is child abuse, that is a global issue, not an issue any country has legalized.  New Nazi is also not legal in germany.  These laws are legal in Jamacia\n\nBut, as a gay male, I wont step foot in a country like this.  Why would I come to, or support a KDE event where I can be beaten up for being gay, placed in jail for being gay.  Why would I step foot in a  country that has made it illegal to be who I am.  And more to the point, why would I support a product that sees nothing wrong with holding their conference there? \n\nKDE is non political, but it doesn't have to be without morels as well.\n\nHere's the thing, I have now lost all selling points of KDE to the gay community because very few people in the gay community will support KDE now, so I now have to convince people in the gay community to shift to linux and use Gnome.\n\nWhat's next, KDE 2010 in zimbabwae?\n\nYeah, support KDE, go to a country where homosexuals are beaten, murdered and imprisoned.  Seriously think about how this is going to reflect on KDE to the gay community."
    author: "Anon"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-19
    body: "Hello,\n\nI fully understand that you won't go to a country that does that sort of things to your kind (non-native speaker). Anybody has the freedom not to go there, and there are KDE events in all parts of the world now, so nobody is going to miss out something. Well maybe, not in China yet, right? But that's sure going to come as well.\n\nOn the other hand, people who work on KDE, _do_ in parts come from Jamaica too. Should they rather not meet? Should they really not try and host an international meeting at all? \n\nWhat do you expect Jamaicans that are part of KDE to do? Do not organise the event?\n\nAnd why would anybody in the gay community consider to boycott KDE because it is also made in Jamaica. Are you sure you aren't overdoing? I strongly doubt they would consider that as a factor. Market pressure hardly applies to Free Software, you know. German developers are simply going to shrug that one off, and rightfully so.\n\nI said it before: I think in Free Software, we ought to not pride outselves where we help bad governments, and I mentioned China before. But even then I said it's personal for me. I thought it ought to be a no-brainer as probably few of us are part of Chinese govenment, that we shouldn't help them, and not be part of enhancing their stuff. When Free Software is used to suppress people, obviously that's not the intention of most of us. But few care. And Chinese government was once a welcome Linux adopter.\n\nWe sure should be aware that \"we\" as a community are made up of _every_ kind of people with not necessarily many common grounds on beliefs. I can bet with you that some contributors do hate homesexuals, while some are homesexual. Some are $RELIGION and some are anti-$RELIGON. Some are pro-Nut in Plasma, some are against the nut in Plasma.\n\nThat also means, that sure as hell, we will have people, that couldn't care less about forcing one-child-per-family or internet-content-filtering of critical opionions in a country they don't visit. \n\nAnd it means, we will also have people that accept that not countries are fully developed when it comes to civil rights of minorities. I think the Jamaicans live in a democracy and this is from AI:\n\n\"The public appears to strongly endorse the idea of differential treatment. A recent poll showed that 96% of Jamaicans were opposed to any move that would seek to legalise homosexual relations. Many churches have released statements indicating their support for the retention of current laws. The Prime Minister of Jamaica has many times confirmed his intention to retain legislation which discriminates against homosexuals.\"\n\nIf a society wants to be like that, there is little we can do to change its opinions. \n\nAnd remember, the only thing we can agree on, is to make good software. And even then, not always on what is good there. \n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-19
    body: "\"Here's the thing, I have now lost all selling points of KDE to the gay community because very few people in the gay community will support KDE now\"\n\nHonestly, that doesn't say much good for your friends, and I'm pretty sure the average gay people are better educated than that. KDE has done nothing against the gay community, as I see. And well, \"I will shift to gnome because akademy is hosted in foo country\" sounds pretty silly. If you really want, just go use gnome.\n\n"
    author: "doooba"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "Hosting an event in a country is not a political statement, nor is it a validation of the laws or practices of the land.\n\nPoliticizing out of context (which is what you are doing) is not a great way to garner support for a cause.\n\nKDE has many LGBT individuals involved, some out and some not, but all of whom are respected and treated well. And that's what's really important here: how KDE people treat others."
    author: "Aaron Seigo"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "I can tell you that even gays are welcome in Jamaica as long as they don't wear a T-shirt saying \"I am gay\". All that people have to do is just keep it simple. I have seen gay tourists walking around in Ocho Rios last year when I was there and nobody interfered with them. Jamaica has a problem with Jamaican gays. The reason is fear, fear that it's contagious and that their little boys will turn gays. It's ignorant yes but people who has a problem with this type of thinking should first blame the Queen who is still the official head of State (and thus responsible for the laws there) and secondly the Holy Bible, Jesus Christ and the Prophets who condems homosexuality and homosexuals to hell.\n\nI am convinced that Jamaica will change it's point of view one day but for the time being it's about bringing something to the Island that has no boundaries, no discrimination, segregation nor political preferences - free software that reflects free thinking. Something that no forces on earth can stop.\n\nI am sure that this (the KDE Camp) is a good idea and that it's a step in the right direction to free up the closed source thinking there ;) Jamaica is a very beautiful island with a lot of loving and hospitable people and I am sure that you will all have a wonderful time there."
    author: "Bobby"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-21
    body: "\"KDE has many LGBT individuals involved, some out and some not\"\n\nI just read that as \"some hot and some not\" :D \nAnyway I'll shut up now and do something productive..."
    author: "RandomOne"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "Don't exaggerate, I know Jamaica more than anybody here because I was born there. I never saw or heard of a tourist being beaten in Jamaica - never and as I have said, cases of people being arrested for being gays are almost unheard of.\n \nIt's not a secret that the most Jamaicans are homophobic but that's not the biggest problem in Jamaica and I am sure that no gay person will be in danger if he doesn't go to the island and lead a protest against the laws of the country.\n\nThere is more to Jamaica than homopho0bie just like there is more to the USA than bombing other people's countries, killing innocent lives and stealing these countries' natural resources. There is more to the USA than the death penalty, environmental pollution and racism. It might be safe for gays there but not for a lot of blacks in certain areas so it's 6 a one and half dozen a the other like we say in jamaica.\n\nAnd the reason why I mention the States is because the most of this blah, blah, blah about Jamaica being the most homophobic place on earth is coming from there. I have met a lot of wonderful people from the US even though it has one of the most fucked up politics in modern history and I don't condemn these people just because they are from there.\n\nEvery country has a right to mind it's own business and clean up it's own back yard before pointing fingers at others."
    author: "Bobby"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "> It might be safe for gays there\n\nthat really depends where you are in the USA or Canada. while both have good laws protecting LGBT individuals, there are still far too many ultra-conservative ignorants concentrated in certain areas of those two contries. Chicago has even been considering opening a \"gay friendly\" high school due to issues amongst teens that lead to higher drop out and other academic failure rates amongst LGBT students.\n\nthankfully there are many places in the US and Canada where people truly don't care what your gender alignment is, and same sex marriage is even starting to get recognized, finally.\n\nso ... yeah, if the person complaining is from the US or Canada, i do find it all a bit hypocrtical. they could be from a more uniformly liberal society, though =)"
    author: "Aaron Seigo"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-26
    body: "In fact, it wasn't until 5 years ago that the supreme court invalidated the sodomy laws in the US.\n\nhttp://en.wikipedia.org/wiki/Sodomy_laws_in_the_United_States\n"
    author: "Anon"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-19
    body: "> In case you do assume that something like that matters (it doesn't matter to\n> all, Free Software is supposed to be non-politicial), it's still the question\n> if this is more or less of a reason to a Free Culture conference to such a\n> place.\n\nI'm not saying it's a bad idea to hold a Free Culture conference in Jamaica.\n\nBut I actually do think Free Software is political and most people I know in the\nFree Software world actually care about racism, sexism, homophobia. As I guess\nthat many people do not know about homophobia in Jamaica, I provided this\ninformation for those who care, especially for those who're planning to go\nthere.\n\nBtw, I don't fear that KDE loses all selling points to the gay community. In\ncontrary, talking openly about the situation could actually act as a sign\nagainst ongoing discrimination. Ignoring the problem would a be very bad sign."
    author: "nobody"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "You might have told them that ganja smoking is illegal there. That would have been a better piece of information in my opinion. There are a lot of people I met who are aware of homophobia in Jamaica but think that the weed is free and grows in the police backyard :D "
    author: "Bobby"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-20
    body: "Please tell us of a country with no problems at all so that we can host it there instead.."
    author: "JohnFlux"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-10-19
    body: "Good arguments, and may I mention the KKK in the USA? People, there is no holy land on earth apart from Israel where they bomb each other everyday.\nApart from that if one keeps his private business in his bedroom then he is safe even as a gay in Jamaica. Weed is illegal in JA but which Jamaican hadn't smoked weed at least once? Homosexuality is illegal by law in Jamaica but how many people have been arrested for being gays (and there are enough there). I am not supporting violence against any human being but the most gay victims in Jamaica have been provocative.\nAnd what does it have to do with KDE?"
    author: "Bobby"
  - subject: "Re: Jamaica - The Most Homophobic Place on Earth?"
    date: 2008-11-16
    body: "Mostly Jamaican women hate batty men because, they do not stick to themselves. They pretend that they are men and infect the women with aids. How would you feel as a woman, if a man charmed you and after having sex with you, he said Jenny I just dipped my dipstick in a load of sh_i_t before sexing you. How would you feel.\nJenny"
    author: "Jenny Straight"
  - subject: "~"
    date: 2008-10-19
    body: "Such a way to ruin a nice article. Will you guys stop doing politics (yes, all countries suck, that's a fact with no exception), and discuss what really matters? what talks will you attend, what weather will be like, how you will travel there, and if food will be better or worse than past events???\n\nSomebody has done a great work organizing this for you. Somebody is trying to sort it all so that you go there and have no trouble. Somebody is spending his precious time to help you out. Just enjoy, have fun and say thankyou.\n\nJust my 1&#8364;cent\n\nuga"
    author: "uga"
  - subject: "Re: ~"
    date: 2008-10-19
    body: "Whether you like it or not KDE has become political by holding it's conference in a country with terrible human rights.  There are companies who no longer do business with the country or hold conferences there because of the fact that doing so financially supports the country.  One of the last companies to stop is the cruise ship and the loss and pressure on them to stop this is immense.\n\nSo yes by holding a conference in a country where homosexuals are in prison, where the police turn their backs on people who murder and beat up homosexuals, KDE has become political and has decided to finanically support a country with terrible human rights.  T\n\nJust enjoy and have fun knowing that people are being killed for being gay, beaten up for being gay in the country you are in and that the police and the law turn a blind eye to it?  Yes, enjoy being in a country where people ARE serving time in jail for the fact that they are gay.  And when you hand your money over for the accommodation, for the food and drink, know that money is going to help keep a country running that has terrible human rights abuses.\n\nSo yes, KDE has become political.  And they have shown their support for human rights abuses by holding their conference in Jamaica."
    author: "Anon"
  - subject: "Re: ~"
    date: 2008-10-19
    body: "If you name a single country that the event can be hosted, where not a single human right is broken issue happens, you get a prize.\n\nEurope and US are killing 3rd world kids by not supporting them with food, impeding immigration and providing them with armor. Other countries use all money to produce bombs. Others just play war in the name of Allah. Others will permit abortion killing a newborn. Oh, and don't tell me you support the US by using the internet!\n\nKDE is not financing any country. They got not enough money for that. Maybe you are financing it indirectly though... did you check the imports?\n\nTo sum it up... Will you just leave us alone and use gnome? Next year host the event yourself in any gay paradise. In the meantime, the world will be happier. Each country got their troubles for somebody."
    author: "doooba"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "Europe and the US don't have laws that make human rights abuse legal.  And they do a far better job at trying to right the wrongs than your silly little rant.  US and Europe support third world countries with billions in aid, in food and medical equipment, medicines etc.  So your argument shows a complete lack of understanding there as well\n\nKDE by holding their conference are financially supporting what is happening there.  And no, my country does not import or export to Jamaica.\n\nTO sum it up, grow a spine and stand up against abuses instead of putting your head in the sand and claiming KDE isn't political. KDE has made itself political by holding an invent in a country that has terrible human rights abuses.\n\nAnd no, I wont go away just so you can continue to put your head in the sand to the abuses that are happening where you want to hold your conference.  In fact I will go further and contact the companies that support KDE, and I will also contact people in the gay community to stand against KDE and those companies.  Funny thing is how American companies re-act when they face boycotts from the gay community, which have been known to financially cripple companies."
    author: "anon"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "Maybe you failed to check how much good they could have done moving the monies of the \"crisis\" to 3rd world? Sure, US is great, the paradise! ... errrm... not man, is guantanamo the perfect paradise for those jailed in there?\n\nSee, I still have some respect for gay people. Don't ruin it the way you are doing so. Most aren't as dumb.\n\nWhat's your trouble with Jamaica? nobody is telling you to go. No, actually I ask you NOT to go there, please, please. And if you, as you say, feel like not using KDE anymore, you will make me the happiest ever man in this world. Thankyou. \n\n\nIf it's political, sorry to say that jamaica uses linux. Wow,you shouldn't. I'd go use Windows. Please do, please do. \n\nmorons..."
    author: "doooba"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "LOL. So what if Gnome decides to follow suite and host the launch Gnome 3.0 in Jamaica? Then he can move to XFCE on FreeBSD.\nI don't know why people can't simple accept the things the way they are. There are a lot of countries with laws and policies that I and a lot like me hate but who am I to dictate how they should run their countries.\n\nI have one advise to gays: if you want Jamaica to accept your life style then try something other that force. Show Jamaicans that you are loving people who do not want to force your way of life on them, that all you need is the freedom to be what you are. \nI have met a lot of gays in my life and they certainly haven't given me the aggressive impression that some of the ones on the net do."
    author: "Bobby"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "I think you intended to respond to Anon, not me since I was pointing out the same as you =)"
    author: "doooba"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "\"Europe and the US don't have laws that make human rights abuse legal. And they do a far better job at trying to right the wrongs than your silly little rant. US and Europe support third world countries with billions in aid, in food and medical equipment, medicines etc. So your argument shows a complete lack of understanding there as well.\"\n\nI am relieved that bombing other countries and killing millions of innocent people before stealing their oil fields and exploiting their natural resources is not a terrible human rights abuse or are these people lesser human beings than gays?\n\ni would like to discuss your so-called help to third world countries but this is not a political forum so I will try to restrain myself. Still I would like to advise you to get the facts, take the blindfolds from your eyes and then you will see that even this is one of the biggest human rights abuses because it is modern slavery. How did it start? How did these countries become \"Third World\"?\n\nAnd I repeat, the laws of Jamaica are from Europe because they all have to be approved by the Governor General as representative of the Queen of England so next time that a law in Jamaica sucks you then go and lodge your complaint at the Queen in England or boycott them too. You might get a more speedy result since Europe doesn't allow laws like these."
    author: "Bobby"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "Well, to add something nice about discrimination:\n\nAmerica, Florida:\nAny form of sexual contact other than missionary position is a misdomeanor.\n\nIndiana:\nBathing is prohibited during the winter.\n\nIowa:\nKisses may last for as much as, but no more than, five minutes.\n\nMassachusetts\nAn old ordinance declares goatees illegal unless you first pay a special license fee for the privilege of wearing one in public.\n\nNew Mexico:\nFemales are strictly forbidden to appear unshaven in public.\n\nNew York:\nA fine of $25 can be levied for flirting. This old law specifically prohibits men from turning around on any city street and looking \"at a woman in that way.\" A second conviction for a crime of this magnitude calls for the violating male to be forced to wear a \"pair of horse-blinders\" wherever and whenever he goes outside for a stroll.\n\nOklahoma:\nViolators can be fined, arrested or jailed for making ugly faces at a dog.\n\nPennsylvania:\nNo man may purchase alcohol without written consent from his wife.\n\nTexas:\nIt is illegal to take more than three sips of beer at a time while standing.\n\nWashington:\n- All lollipops are banned.\n- In King County, in Seattle Washington, it is illegal to sit on a man's lap on a metro bus, unless you are married.\n\nTennesee:\nIn Tennessee a man must walk in front of any car driven by a woman while waving a red flag as a warning.\n\nTell me anything where no stupid laws exist. Tell me any place that is perfect.\nI don't think that it will be a problem as a tourist being gay. I don't think you'll be arrested or violated in any way.\n\nIf you don't like the law, don't go there. That's also a statement, not giving your money to any state with laws like that.\n\nDon't make Free Software political in any way. It is not. It is about Free Software vor everybody, Free for anybody - who ever you are.\n\nFree software is something, which is a political statement about SOFTWARE. Don't interfere with things your work is not about. There enough people caring about human rights.\n\nDo we say that we like some things done to human rights in other countries than Jamaica? Where people are killed for being from another nation, or another religion?\n\nNo, since it this is nothing KDE should politically care about. Keep on caring for producing free and patent free software."
    author: "Georg"
  - subject: "Re: ~"
    date: 2008-10-21
    body: "Hello,\n\nthe US:\n\na) Tortures people they just think may be terrorists. That will come to an end now, luckily.\nb) Imprisons people and keeps proof of their guilt secret to them\nc) US soldiers are above law when e.g. raping natives, because they won't allow the UN tribunal to do anything to them\nd) Is an aggressive state obeying the \"Bush doctrine\", which is ca. \"attack who may be able and want to attack you later\", which lead to the Iraq war.\n\nin EU:\n\na) In Poland e.g. women are not granted their limited, but expressive right to abortion in case of health danger. One women has e.g. become blind, because not any single doctor dared to conduct the abortion in fear of prosecution. Those dead won't ask as many questions.\nb) In Germany we didn't want an US prisoner freed and sent back to Germany, even though we knew he was free. Well, only the government knew. And we are such good friends with US, they chose to better keep him imprisoned until they really really didn't want him anymore.\nc) Several EU states seem to have tolerated secret US prisons on their lands where people were tortured.\nd) The EU sends lots of goods for free to countries, yes. Like e.g. near free milk powder to milk producing nations. It was produced with the help of EU subventions and then exported. Of course there is no milk production anymore in countries that receive that kind of help.\ne) German banks e.g. successfully invested in Chinese companies that produce stuff with forced labor at very low costs.\nf) The EU tolerates/mandates that for every bank transfer, credit card charge and every flight I do, all information is passed to US authorities. The department of homeland security happens to know more about me than anybody else in the world, against my rights.\n\nNot a single country and society is without a flaws. Not all of this will be accurate, but more so, it will be incomplete...\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: ~"
    date: 2008-10-20
    body: "> Such a way to ruin a nice article. \n\ni'm sorry about it. this was not my intention."
    author: "nobody"
  - subject: "Lets have it in North Korea"
    date: 2008-10-19
    body: "comrades! we should have it in glorous north korea korea-dpr.com uses Apache/1.3.34 (Debian) PHP/4.4.4-8+etch6 ;P"
    author: "Anonymous"
  - subject: "Re: Lets have it in North Korea"
    date: 2008-10-20
    body: "Or Cuba, gays have a better life there that they do in Jamaica and they also use linux but Cuba also have a lot of Comrades. Man, the world is so fucked up! Everywhere you turn there is one shit or another."
    author: "Bobby"
  - subject: "Re: Lets have it in North Korea"
    date: 2008-10-20
    body: "noooo not cuba.. because...\n\nI'm So Ronery \nI'm so ronery \nSo ronery \nSo ronery and sadry arone..........."
    author: "Anonymous"
  - subject: "Homepage"
    date: 2008-10-20
    body: "I really like the look of the conference website. It looks nice, clean, very modern = amazing!"
    author: "Dr.Schnitzel"
  - subject: "Re: Homepage"
    date: 2008-10-21
    body: "Gotta second this, the page is pretty nice for a change."
    author: "Stefan"
  - subject: "Really nice"
    date: 2008-10-20
    body: "Looks great! I wish you guys and girls a lot of fun!"
    author: "Georg Grabler"
  - subject: "Would like to see KDE training sessions"
    date: 2008-10-20
    body: "Several years ago, KDE did what I think was a two day training session in San Diego.  It was targeted at those considering creating or porting to KDE.  The attendance wasn't bad either.  \n\nKDE's API is large with so much technologies in it, that it can be a little intimidating to beginners.  I think it would be great to offer a few seminars that are somewhat regional, much like Trolltech does with its Dev Days.  Of course there is a cost associated with this kind of event, but it might be recouped by registration fees, and sponsors such Nokia, KDAB  and ICS.\n\n\n\n\n"
    author: "David"
  - subject: "KDE 4.2 Timeline"
    date: 2008-10-21
    body: "Hope that you guys will be able to work in a such great environment.\nPersonally, when I'm going to a such nice place, I'm not working at all. My laptop stay in it case :)\n\n\n"
    author: "JC"
  - subject: "Re: KDE 4.2 Timeline"
    date: 2008-10-21
    body: "I am impressed by the hacking ability of some folks. Like old soldiers who can manage take a quick ten minute nap anytime and anywhere, some KDE hackers can manage to crank out two or three bug fixes while waiting for the bus. At DevDays last week I saw a few fixes come through from Trolls between presentations.\n\nAs for me, I can barely manage email if I'm in a strange hotel room. I'm not sure if I'll get to Jamaica, but I know if I do I'll be spending my free time Jamaicaning instead of hacking."
    author: "David Johnson"
  - subject: "Re: KDE 4.2 Timeline"
    date: 2008-10-23
    body: "How? Ha --- if I'm in a strange hotel room, I have nothing to do but computer things. :D\n\nI just might do it next to the pool in Jamaica. Actually, I thought all the work during aKademy happened during talks. With planning outside. \n\nBut really, the people will be the main draw... Everybody should go!"
    author: "blauzahl"
  - subject: "Re: KDE 4.2 Timeline"
    date: 2008-10-23
    body: "You better bring pool stuff. The Pool is huuuuuge but has good Wifi Coverage for you to get all your bug reports back in :) Should be fun to be doing the BugSquad Sunday from the spa."
    author: "DasKreech"
  - subject: "Re: KDE 4.2 Timeline"
    date: 2008-10-25
    body: "But unfortunately laptops tend not to be waterproof. ;-)"
    author: "Kevin Kofler"
  - subject: "Jamaica Problems..."
    date: 2008-10-26
    body: "I live in Jamaica...Enjoy being in the murder capital of the Caribbean, and the 3rd worst country in the world in terms of crime.  I've been living here for all of my life, and I am afraid to leave the house at night on foot.  Be careful..."
    author: "Jamaican"
---
In January 2008, the KDE community <a href="http://www.kde.org/kde-4.0-release-event/">celebrated</a> the release of the much anticipated KDE 4.0 in Mountain View, CA.  When the event was celebrated by a packed house,  we realised that there was a strong demand for KDE events in the Americas.  One year later, the community will celebrate this new conference series at <a href="http://camp.kde.org/">Camp KDE 2009</a>, to be held in Negril, Jamaica.








<!--break-->
<div style="border: thin solid grey; float: right; margin: 1ex; padding: 1ex; width: 500px">
<img src="http://static.kdenews.org/jr/camp-kde-2008-resort.jpg" width="500" height="375" /><br />
Camp KDE
</div>

<p>
Just as the Release Event heralded KDE 4.0 and <a href="http://akademy2008.kde.org/">Akademy 2008</a> celebrated <a href="http://www.kde.org/announcements/4.1/">KDE 4.1</a>, Camp KDE 2009 will be held just as KDE 4.2 is <a href="http://techbase.kde.org/Schedules/KDE4/4.2_Release_Schedule">readied</a> for release.  
</p>
<p>
Camp KDE 2009 will be held in three months time, starting on January 17, 2009 and continuing until January 23.  Like Akademy 2008, the event will start with 1-2 days of presentations, followed by BoF meetings and hackathon sessions.
</p>
<p>
After flying into Sangster International Airport (MBJ) in Montego Bay, attendees will be staying at the nearby <a href="http://www.travellersresorts.com/">Travellers Resort</a> in Negril.  The Camp KDE organisers have negotiated an extremely reasonable lodging rate and the event will be held on site at the conference centre.  With many resorts nearby, attendees have plenty of options and alternatives.
</p>
<p>
Conference presentations and sponsor details are currently being formed.  As more information becomes available, please frequent our new Camp KDE 2009 <a href="http://camp.kde.org/about">event site</a>.  The registration system, designed by Akademy 2008 organisers, has been modified for this event and will be open shortly.
</p>
<p>
We are pleased to announce two sponsors already: <a href="http://www.ixsystems.com/">iXsystems</a> and <a href="http://www.google.com/">Google</a>.  Organisers are excited to be working with both companies and look forward to more sponsors being announced.  Let us know if you are <a href="http://camp.kde.org/sponsors">interested in sponsoring</a>.
</p>
<p>
Set to be held every northern hemisphere winter, the Camp KDE series will bring a much needed presence in North, Central and South America over the coming years.  Help us celebrate KDE 4.2 and encourage growth of a new conference by attending Camp KDE 2009.
</p>







