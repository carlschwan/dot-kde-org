---
title: "Akademy 2008 - Day 2 and the Akademy Awards"
date:    2008-08-12
authors:
  - "jospoortvliet"
slug:    akademy-2008-day-2-and-akademy-awards
comments:
  - subject: "Cool!"
    date: 2008-08-12
    body: "Enough said.\n"
    author: "Michael \"I'm not at Akademy\" Howell"
  - subject: "Wade"
    date: 2008-08-12
    body: "tnx to wade for the part about Zack's talk... Nice to have people helping out, as I'm still having a hard time trying to be at two talks at the same time. Somehow I can't manage to split myself in two. If anyone has any suggestions.. ;-)"
    author: "jospoortvliet"
  - subject: "Re: Wade"
    date: 2008-08-12
    body: "I have a suggestion, but you might not like it...\nIt's pretty gruesome! ;)"
    author: "Danny Allen"
  - subject: "Re: Wade"
    date: 2008-08-12
    body: "I'm sure I don't want to know. And I could ask you to cover something, if I want, anyway ;-)"
    author: "jospoortvliet"
  - subject: "great event"
    date: 2008-08-12
    body: "wish i was there . thx for that great report ........."
    author: "pete"
  - subject: "Gallium3D confusion"
    date: 2008-08-12
    body: "Can someone please point me to a link, or help explain, the differences between Gallium3D, Cairo, Xserver, OpenGL?  I tried to read up on this in wikipedia, but get more confused the more I read.  A lot seems to overlap, and I just don't seem to get the differences.  Thanks for any help.  I hope it is easier than I am making it out to be!"
    author: "rich"
  - subject: "Re: Gallium3D confusion"
    date: 2008-08-13
    body: "Cairo is some sort of 2D rendering application programming interface (API), which means that software can ask it to draw things like lines, shapes, and whatever else, I'm not that knowledgeable about the details. It has several choices of backend, one of which is OpenGL. i.e. Some code would ask Cairo to draw things, and then the OpenGL backend would ask OpenGL to draw the stuff.\n\nThe X Server is a server that runs on a machine, providing client applications with access to your monitor and keyboard. An example client application would be Firefox, or OpenArena. The X Server might also control access to a graphics card, but software that uses hardware accelerated 3D wouldn't be aware of this, because it would probably do the rendering through OpenGL. The implementation of OpenGL would then talk to the X Server on the software's behalf.\n\nOpenGL is another API, but it's standardised, meaning that multiple companies and groups have implementations of it that are supposed to work a certain way, when an application uses any given implementation to draw things. This is similar to how HTML is a standard that specifies how a browser will act when loading an HTML page.\n\nGallium3D is an abstraction layer where there are backends (drivers) for various pieces of graphics hardware, and then frontends for things like OpenGL, DirectX, high performance computing APIs, etc.  This means that an OpenGL implementation would ask Gallium3D to draw things or whatever, and Gallium3D would ask a backend to do the drawing.\n\nSo, in summary, the order of delegation is:\nCairo -> OpenGL -> Gallium 3D -> graphics drivers\n\nThe X Server is somewhere in between OpenGL and Gallium3D, I think, but it probably doesn't sit directly in between, and I don't know the details."
    author: "Simon"
  - subject: "Re: Gallium3D confusion"
    date: 2008-08-13
    body: "Thank you.  It WAS simpler than I thought!"
    author: "rich"
  - subject: "Oxygen"
    date: 2008-08-13
    body: "Congratulations Nuno!  (and the rest of us) \ncare to tell us where you get the extra hours in a day? :-)"
    author: "David Miller"
---
The second day of <a href="http://akademy2008.kde.org/">Akademy 2008</a> started a bit later in the morning than the previous one, yet somehow most visitors managed to look much more tired. Maybe the social event (read Nokia sponsored beer) from yesterday has something to do with that. Although tired, people visit the talks and write code, so you can expect more code, discussions and blogs today. Read on for more!



<!--break-->
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/akademy-2008-audience.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-audience-wee.jpg" width="400" height="267" /></a><br />
The Akademy Audience
</div>

<p>The day kicked of with several technical talks, like the talk about the Qt component framework by <a href="http://www.prashanthudupa.com/">Prashanth Udupa</a>, and about applications like <a href="http://digikam.org/">Digikam</a> and <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a>. These talks are being recorded and videos will be put online, most likely sometime next week.</p>

<p>During and in-between the talks, you could see a lot of code flowing over the screens. Outside the 2 conference rooms there were people hanging around in the large lunchroom downstairs, which featured two always-busy table tennis tables. It was great to see the KDE guys and girls have fun batting that little ball around...</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/akademy-2008-table-tennis.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-table-tennis-wee.jpg" width="400" height="267" /></a><br />
Playing Table Tennis
</div>

<p><br>During lunch, a long but fast moving line developed for the food tables again. We found the <a href="http://live.gnome.org/VincentUntz">GNOME</a> from yesterday among them, who noted he was 'hungry!' again. Those who got their lunch gathered in several places, continuing to hack and talk. The division of work generally seemed to be one person hacking, while the other looked over his/her shoulder eating his/her lunch. The weather got better, so many were eating outside on the tables, Aaron walking around calling people sexy. Not sure what response he was looking for. We found <a href="http://blauzahl.livejournal.com/3675.html">blue-haired Seli</a> sitting on the floor, not willing to get in line yet. When asked about his blue hair, he noted he would do "anything for KDE", then claimed we would abuse that quote. We'd never do that, of course...</p>

<p>A surprising number of people can be found wandering the corridors of the building, looking for other people. Yeah, Google Maps doesn't really help here, so they are harassing others with questions like 'has Sebastian Trueg arrived yet' and 'where is Paul, have you seen him?'. A second GNOME came by, and told us he enjoyed Akademy a great deal. And Niels Slot, new at Akademy, noted: "wow, I just spoke to the guy writing the KWin effects, and I've been drinking beer with the hacker from the KDE-Ruby bindings, how cool is that?". Yes, people enjoy it here. Although Chani was complaining about a lack of sleep - and not the only one. Better get used to that, guys and gals...</p>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/akademy-2008-waffles.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-waffles-wee.jpg" width="400" height="267" /></a><br />
Waffle Queue
</div>

<p>The NEPOMUK talk was given by <a href="http://www.deri.ie/about/team/member/laura_josan/">Laura Josan</a>, and she mentioned the recent improvements to NEPOMUK. Dolphin already had NEPOMUK integration, and Konqueror has followed, allowing you to tag and rate websites. Amarok and Gwenview also support NEPOMUK these days, and a KIO slave for NEPOMUK search has been implemented. This allows you to rate a music file in your file browser and see the changes in Amarok. Laura presented a compelling vision, talking about how Marble and Amarok could work with NEPOMUK to show artists from a certain area in the world using Last.FM information. If you want to know more about NEPOMUK and how to integrate it in your application, there is a <a href="http://nepomuk.kde.org/">website</a>, a <a href="http://lists.semanticdesktop.org/mailman/listinfo/nepomuk-kde">mailing list</a> and an IRC channel: #nepomuk-kde.</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/akademy-2008-bart.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-bart-wee.jpg" width="400" height="267" /></a><br />
Organiser Bart Commands All
</div>

<p>In the last time slot of the day, <a href="http://akademy.kde.org/conference/presentation/9.php#bio">Zack Rusin</a> presented on <a href="http://akademy.kde.org/conference/presentation/9.php"><em>Gallium 3D: Graphics Done Right</em></a>. A historical walk through graphics history (dinosaurs, Pong, and VA Linux) set the stage for <a href="http://www.tungstengraphics.com">Tungsten Graphics</a>, creator of Gallium 3D. This new framework intends to bring sanity to the graphics driver world through a <a href="http://www.tungstengraphics.com/wiki/index.php/Gallium3D">sensible abstraction layer</a>. Gallium3D will hopefully not only accelerate graphics but graphics driver development. This model also allows for platform portability, and this portability can be tested with back-end simulators. Gallium 3D is currently at 0.1 version (although not reflective of its maturity), however Zack expects an update before the end of this year. The talk concluded with graphics demos and explanations.</p>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/akademy-2008-zack.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-zack-wee.jpg" width="400" height="267" /></a><br />
Zack the Bling
</div>

<p>After the last talk everybody gathered in the main room for the closing of the conference part of Akademy and a little community love. Adriaan de Groot led the ceremony for the Akademy Awards. Danny Allen, well-known for the <a href="http://commit-digest.org/">KDE Commit-Digest</a> and Matthias Kretz, the founder of Phonon, formed the jury with Sebastian Trueg from K3b fame. Being the hot guys from last year, they were the perfect candidates to tell us what the new black is - and who wrote it. Unfortunately, Sebastian Trueg could not come - fortunately, it is due to his wife and him having a baby - so congratulations to them!</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; margin-top: 50px">
<a href="http://static.kdenews.org/jr/akademy-2008-akademy-awards.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-akademy-awards-wee.jpg" width="400" height="267" /></a><br />
Akademy Award Winners, Oxygen
</div>

<p>The three prizes, for best application, non-application, and jury's award, were announced by Danny and Matthias.</p>

<p><strong>Application Award: Mark Kretschmann</strong> and the Amarok team.  
We all love Amarok, and welcome it to join our plans for world domination! It has raised KDE's profile as the best multimedia application on the Free desktop (and soon to be on other platforms as well). Mark Kretschmann started this, and it is still his strong vision and leadership which pushes the project forward.</p>

<p><strong>Non-application Award: Nuno Pinheiro</strong> and the Oxygen team.
Oxygen brought a breath of fresh air to our desktops with KDE 4. He has formed a bridge between developers and artists and lead the artwork team in an amazing way. As he was not at Akademy, the members of the Oxygen team who were, Riccardo Iaconelli, Marco Martin, and Urs Wolfer received the award in his name.</p>

<p><strong>Jury award: Aaron Seigo</strong> for his work on Plasma.
As someone we all love, for providing leadership and guidance to the community, representing us and for his work on leading a full-refresh of a fundamental piece of our Environment, Aaron received the Jury's Akademy Award.</p>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/akademy-2008-ade.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-ade-wee.jpg" width="400" height="267" /></a><br />
Ade the Compere
</div>

<p>After the awards, everybody went rather silent. Adriaan noted this was a kind-of sad moment, considering the fact the first two days are over. But he cheered everybody up by telling us we have another group to celebrate: the organisers of Akademy, Bart Cerneels, Wendy van Craen, and Kenny Duffus, for their months of hard work. They came forward, and we gave them a well-deserved standing ovation, while they asked the whole Akademy team to come forward.</p>

<p>Photos by Bart Coppens, more photos are <a href="http://techbase.kde.org/Events/Akademy/2008/Photos">linked from TechBase</a>.</p>





