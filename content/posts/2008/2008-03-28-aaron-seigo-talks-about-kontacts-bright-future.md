---
title: "Aaron Seigo Talks About Kontact's Bright Future"
date:    2008-03-28
authors:
  - "skuegler"
slug:    aaron-seigo-talks-about-kontacts-bright-future
comments:
  - subject: "Oh baby"
    date: 2008-03-28
    body: "I just love Aaron photos. Nothing like a good devilish grin in a serious-looking article. (Or was that a Sirius-looking article? Well, whatever.)"
    author: "Jakob Petsovits"
  - subject: "Re: Oh baby"
    date: 2008-03-28
    body: "Really scary"
    author: "JC"
  - subject: "Re: Oh baby"
    date: 2008-03-28
    body: "You haven't seen him in real life yet ;-)"
    author: "jospoortvliet"
  - subject: "Re: Oh baby"
    date: 2008-03-28
    body: "I agree. I don't know him personally but many photos I see of him on the web somehow remind me of the movie \"Shining\". Then again, it's probably good to prevent people from becoming to demanding on plasma features ;-)\n"
    author: "Michael"
  - subject: "Re: Oh baby"
    date: 2008-03-28
    body: "Not to mention the choice in screenshots."
    author: "Ian Monroe"
  - subject: "Sonnet"
    date: 2008-03-28
    body: "Aaron again mentions Sonnet as an important part of KDE 4.\nBut what happened to it? Is it still developed? Which features were implemented until Jacob Rideout disappeared?\nDifferent people asked this several times on the Dot, but nobody answered. ( http://dot.kde.org/1200050369/1200289711/ )\n\nThere was only one Message on the Sonnet mailinglist this year!\nhttp://mail.kde.org/pipermail/kde-sonnet/\n\nSo I assume it's indeed a dead project."
    author: "Sepp"
  - subject: "Re: Sonnet"
    date: 2008-03-28
    body: "I think Zack Rusin was doing some work on it.  But it certainly seems not quite complete yet.  In Konq, misspelled words do get highlighted, but I can't for the life of me figure out how to actually run a spellcheck on the textbox or right click a word to correct it."
    author: "Leo S"
  - subject: "Re: Sonnet"
    date: 2008-03-28
    body: "Jacob Rideout wanted to developed a full-featured language engine, including spellchecker, grammar checker, even with language detection.\n\nBut we already had KSpell2 in KDE3, which was then transformed into \"Sonnet\" in KDE 4. So our current version of Sonnet only does spellchecking, no fancy grammar checker, no language detection and no other cool stuffs, but we _still have_ Sonnet inside kdelibs and the spellchecker is working just fine."
    author: "fred"
  - subject: "Re: Sonnet"
    date: 2008-03-28
    body: "AFAIK Jacob Rideout's work never made it into kdelibs. All the interesting stuff is in a separate svn branch: http://websvn.kde.org/branches/work/sonnet-refactoring/.\n\nWould be great if somebody would pick up the work and finish it. Maybe a good Google SoC project?"
    author: "cloose"
  - subject: "Re: Sonnet"
    date: 2008-03-29
    body: "I might find some time for sonnet magic if I get accepted for GSoC project on Lokalize or KNotify... ;)"
    author: "Nick Shaforostoff"
  - subject: "Re: Sonnet"
    date: 2008-03-28
    body: "No, I just don't use the list. \nI've been doing this linguistic stuff for a long time, from KSpell, through KSpell2 to Sonnet and while I'd absolutely love to get more help, you can be sure that no matter how busy I am I'll always find a bit of time for it.\nFor KDE 4.1 the most important features that I want to finish are removal of our custom text segmentation algorithm and use of QTextBoundaryFinder recently introduced in Qt and the addition of a proper dictionary class (not spell-checking, dictionary - meaning definitions of words)."
    author: "Zack Rusin"
  - subject: "Re: Sonnet"
    date: 2008-03-30
    body: "Thank you for the info!"
    author: "Sepp"
  - subject: "Good and Bad"
    date: 2008-03-28
    body: "Good for him to admit KDE team made a big mistake in the KDE \"4.0\" fiasco, looks like it's a consensus it should had been called preview edition or something like that. I applaud him for that!\n\nBut he is REALLY wrong when he says KDE4 apps start faster and use less resources. This is the same kind of propaganda I see every new Windows release. They place it against the older version on a modern machine and compare speeds. The real test is to install KDE4 on a machine that runs KDE3 well.\nI have a Duron 1.6 Ghz with 512 RAM, it runs KDE3 nicely and KDE4 slow, not a disaster, but not even close to being as fast as KDE3.\nNow on my new machine, a Core 2 Duo 2.2Ghz with 1GB RAM KDE4 runs nicely, and maybe I could tell KDE3 is slower, if I had it installed, but is thus a fair comparsion for people that does not live in a rich country/place and could barely aford a lower end PC? I don't think so!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "In comparison to kde3 kde4 apps start a lot faster. In apps that are not preloaded there is significant pause on startup, In kde4 most of the same apps (i.e. their kde4 versions) start near instantaneously. I guess it depends on your comp. Mine is quite a bit more powerful "
    author: "Fool"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: ">I guess it depends on your comp\n\nThis is my point. Microsoft always tell everybody the next windows is faster, they even stated that vista is faster than XP!\n\nKDE3 vs. KDE4 in my vision:\nOn modern rich-people machines? Yes.\nDevelopment world machines? Maybe, with luck.\nCurrent third world: No way! Do you think EeePC can run KDE4 faster than 3? I do not. \n\nLucky I am in the middle of the equation, have a not bad job to work for HP here in Brazil, but let's please take some reality check, some machines here still sell with 256 MB RAM, most with 512 and my shine new one is only 1 GB."
    author: "Iuri Fiedoruk"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "ehm... what you're saying is that KDE 4 is faster than KDE 3 if you use a dualcore 2.4 ghz proc with 3 gb of ram, but it is SLOWER than KDE 3 if you use a 800mhz proc with 256 mb ram?\n\nThat's not true. As I have both systems I describe above, I can assure you KDE 4 apps start faster. They draw slower. They use more memory thanks to the double buffering. But they start faster, no doubt possible."
    author: "jospoortvliet"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "Thats the crux I think.  KDE4 apps start faster, no matter the processor speed.  \nBut if you have an older video card, the slower painting speed is really noticeable, so the whole desktop feels very sluggish.  That's why you notice on older machines and not on newer ones.  "
    author: "Leo S"
  - subject: "Re: Good and Bad"
    date: 2008-03-29
    body: "Define \"older video card\"."
    author: "reihal"
  - subject: "Re: Good and Bad"
    date: 2008-04-02
    body: "Oh, I can do that: \"all videocards\". Qt4 is simply much slower. Most of that seems to be due to bad drivers, but it is still annoying..."
    author: "jos poortvliet"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "> Do you think EeePC can run KDE4 faster than 3? I do not. \n\nthe best part about having access to devices like this is that you don't have to think, you can know. then you can avoid making arguments based on conjecture =) i've used kde4 on an EeePC and it is indeed as fast or faster than KDE3. soo.. there you go."
    author: "Aaron Seigo"
  - subject: "Re: Good and Bad"
    date: 2008-03-29
    body: "and I write this from an eee :-) KDE4 on eee is quite ok and its by far the best experience watching plasma on x11-intel compared to x11-ati or x11-nvidia. But I can say that KDE4 likes RAM so now I use 2GB on eee but 600MHz and KDE4 is ok.\n\nBye\n\n  Thorsten"
    author: "schnebeck"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "SO, if you have an eee with, let's say, 512 RAM, will it run kde4 faster or slower than 4?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "Depends entirely on what you are doing, obviously - neither KDE3 nor KDE4 with a webbrowser with a few tabs open and a few Kwrite sessions, say, will come close to using 512MB, so the amount of RAM is entirely irrelevant.\n\nIf you have more/ more demanding apps open, then KDE4 and its apps will push you into swap faster than KDE3, which will slow things down when switching apps.  Assuming an app is fully loaded into RAM, though, then again, the amount of RAM is completely irrelevant."
    author: "Anon"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "And it's important to remember linux/kde is much less resource hunger than Winodws Vista, but still a bit over XP (in my humble testing case) :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "i have kde4 on my laptop with opensuse 10.3 and 256 mb ram (of which 128 mb is shared video memory), and kde4 does indeed run faster than kde3.\n"
    author: "Whatever Noticed"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "Maybe I just wasn't clear, Kde4 apps start a lot than kde3 apps on the same computer. Now we may have different definitions of speed. Kde4 apps paint more slowly and in general work slow as sin (scrolling,tabs,rubberband, etc) if that is your meaning of speed, then yeah they are slower. But if you mean speed in terms startup times , then they are clealy quicker. \n\nsigh, I wish I had a laptop with an intel gfx card."
    author: "Fool"
  - subject: "Re: Good and Bad"
    date: 2008-03-29
    body: "> sigh, I wish I had a laptop with an intel gfx card.\nHah, and I regret trusting the freedom of Intel drivers. Freedom is no good if the product is C-R-A-P. Really, NVidia despite of being close provides really good support. My friend has a comparable notebook to mine with the only difference being the NVidia card instead of Intel one, it does wonders! Benchmarking in UT2004 gives him 75FPS while my Intel can barely handle 5FPS, even software rendering is faster. Do yourself a favor and don't buy Intel. :)"
    author: "Rsh"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "... and end up with proprietary binary-only drivers which are completely useless (i.e. won't work at all) if you're running a newer version of the X.Org X11 server than NVidia is willing to support (e.g. 1.5 which will be in Fedora 9, but this comes up each time there's a new X server)? No thanks! Don't buy NVidia!"
    author: "Kevin Kofler"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "\"modern rich-people?\" 800Mhz machines are out of the shelves since year 2000,\nand 2,5-3,5 Ghz semprons aren't modern or expensive machines too.\n\nAnyway, on a <1Ghz machines nobody would suggest to use kde3 too, if you\nreally want to put a desktop there go for something really light, as xfce or\nother really minimal windows managers (damn small linux?)."
    author: "mixx"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "Nonsens, KDE both 3 and 4 runs just fine on <1GHz machines. The eeePC are prof of that. \n\nRunning something so called \"light\" gives you nearly noting, it's an illusion. You still have to run applications, and the lighter alternatives does not provie a comprehensive application suite as KDE[1]. \n\nWhen you begin starting applications like browsers, emailclients, filemanagers and terminalwindows[2] the gain the lighter alternative gave you are more or less gone. As long as you don't exclusively use console applications that is, but since XFce are a GUI/DE you are supposed to use it as such.\n\n\n[1]. You may end up running KDE applications anyway. Nothing wrong with that, but the 'it's lighter' argument are void.\n[2]. Konsole with several tabs open are proven to use less resources than the same number of xterm windows"
    author: "Morty"
  - subject: "Re: Good and Bad"
    date: 2008-04-02
    body: "I used KDE 3 on a laptop with 486 MHz processor and 256 MB RAM. It ran fine."
    author: "yman"
  - subject: "Re: Good and Bad"
    date: 2008-04-09
    body: ">\"modern rich-people?\" 800Mhz machines are out of the shelves since year 2000,\n\nNot true: the EEE PC is underclocked to 600MHz..\n\n\n>Anyway, on a <1Ghz machines nobody would suggest to use kde3 too\n\nIf this was true (and I don't think that this is the case), then it would be an indicator that KDE needs to be improved: a desktop environment shouldn't be resource hungry or more precisely should be configurable to use a minimum amount of CPU and memory.\nI remember having a *snappy* desktop with BeOS on a Celeron333 PC, and it was a normal environment not a stripped down one."
    author: "renoX"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "I don't think that you and I read the same interview.  Could I have the link to yours?"
    author: "Wade"
  - subject: "Re: Good and Bad"
    date: 2008-03-28
    body: "hi Iuri ....\n\n> KDE team made a big mistake in the KDE \"4.0\" fiasco\n\nto make it clear, i feel the mistake we made was not finding verbage that fit with the enthusiast community's communications expectations soon enough.\n\ni don't think that there was any material mistake with 4.0 as it had clear goals for that release which have been met. (this is different than saying \"the code was perfect\", mind.)\n\nit's interesting that the \"fiasco\" was completely drummed up by the user community, continued by the user community while solution was actively fought against by the user community. *that* is the true tragedy.\n\ni think we (kde) recognize now in retrospect what we could've done to make it better, but i don't see any of that sort of recognition on, e.g., your part and that is a real failing point.\n\ni continue to be deeply disappointed in the lack of flexibility and the absurd willingness of people in the enthusiast KDE user community to pointlessly fling mud (or worse) at others. it's really unbecoming. in fact, it's the sort of behaviour that would cause me, if i were a new comer, to not get involved with KDE and/or free software.\n\n> wrong when he says KDE4 apps start faster and use less resources.\n\nwell, except that it's accurate: apps do start noticeably faster and there are noticeable resource improvements.\n\nsome new features, like compositing window management, take more resources. in the case of kwin, you can turn those features off. but those are new features.\n\nat the same time, we slimmed down so many of our background processes, share more with the rest of the operating system (slimming our own part of the overall footprint by leverage pieces that are already there), etc.\n\nand yes, i've seen and used kde4 on \"less impressive\" devices (1Ghz CPUs, 256MB ram, etc) and it runs very well in comparison to kde3."
    author: "Aaron Seigo"
  - subject: "Re: Good and Bad"
    date: 2008-03-30
    body: "I did not want to be agressive, but re-reading my text.. yeah, pretty lame on my part, it's hard to use irony when writing instead os speaking :-P Sorry for that.\n\nThere is a clear discompass between KDE developers and |KDE users on the 4.0 matter, we all  have our part of guilty, but if we users (before people start saying they like kde 4.0 I'm not assuming *everbody*) do not complain.. well developers will keep making things that can be seen as fool and wrong.\n\nAbout speed, well, my experience is not the same as yours, KDE on my duron was useable, but stating that on that kind of system kde4 is faster AND (notice this is not a OR) uses less resources does not seem to be true. But looks like people only read the part I was speaking of speed and forgot the AND on the equation ;)\n\nYes, as the user above stated, putting extra RAM on a Asus makes KDE4 runs very well, but as I previously told, it's not hard to find 128 MB RAM PCs to sell, much less 512.\nAbout CPU clock, again people are getting only part of the equation."
    author: "Iuri Fiedoruk"
  - subject: "Re: Good and Bad"
    date: 2008-03-31
    body: "> i think we (kde) recognize now in retrospect what we could've done to make it better, but i don't see any of that sort of recognition on, e.g., your part and that is a real failing point.\n\nWhen you say things like \"the fiasco was completely drummed up by the user community\", it sounds like you are blaming the users for being disappointed in KDE 4. That doesn't sound like \"recognising what you could've done to make it better\" at all.\n"
    author: "Jim"
  - subject: "Admitting a fiasco?"
    date: 2008-03-29
    body: "I won't use KDE 4 every day yet.  But it isn't a fiasco, and no one admitted it was a fiasco.  He admitted that the release needs serious work, which is what was said well before release. "
    author: "T. J. Brumfield"
  - subject: "Re: Good and Bad"
    date: 2008-03-29
    body: "Just to say, as a trunk user of kde, i must say that 4.1 is going to rock because from about 2 weeks after the move to 4.4, trunk/ if becoming more and more stable and usable.im talking about the whole experience...\nwe have months to 4.1, but from now, im using it as my daily DE.\nmy main issue is the IRC and the fact that i have to load a client from kde3 (and load all kde3 libs) just for a client.\nKate needs some usability work on new Find/Replace andautocomplete dialogs.but its 100% usefull and stable.\nPlasma is working good, like kicker+kdesktop and does much more.Amarok 2 is shaping up and is really rocking.Kmail works nice (i dont know about other pim parts, but kmail is stable)\nwhat else? games are in a nice shape.Konqueror is much better than 4.0.x.it has a much better rendering (kudos to khtml devs) and is really more stable.it doesnt crash often and with new settings options that it got from dolphin for file view, its much more usable.\nalso kwin is getting lots of new compositing effects that many users like to see.\nall im saying is that 4.1 will be as usable as 3.5 (probably much better).but for the new stuff like Akonadi, Sonnet and Nepomuk, __i think__ we still have to wait for 4.2 or even 4.3(for a really good experience) and thats why the software never finishes ;)\n\nand 4.0 was never a fiasco.the reactions to 4.0 was what developers excepted.it was a 0.0 release..."
    author: "Emil Sedgh"
  - subject: "With or without compositing"
    date: 2008-03-29
    body: "There is something that's often forgotten when comparing KDE 3.5x apps start up with that of KDE 4 and that's whether or not the compositing effects are turned on. I have an AMD 1.8 ghz Sempron processor with an Nvidia GeForce 6600 LE video card on an ASRock board with 1.5 MB of RAM - not the most modern machine of course. With the compositing effect turned on (on KDE 4) KDE 3.5x apps start up faster that those of KDE 4. Without Compositing, I click and the KDE 4 apps are there! It does make a big difference."
    author: "Bobby"
  - subject: "Re: With or without compositing"
    date: 2008-03-29
    body: "Which OS that runs on 1.5 MB of RAM runs KDE? Not 1.5 GB?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: With or without compositing"
    date: 2008-03-29
    body: "Sorry, we all make mistakes in writing sometimes. I meant 1.5 GB (gigabyte) :)"
    author: "Bobby"
  - subject: "Re: Good and Bad - Firefox?"
    date: 2008-03-31
    body: "Everybody always keeps talking about KDE 4.0 running some applications faster or slower that are KDE native...\n\nHow about an important application?\n\nHow about an application that I use EVERYTIME I use ANY computer?\n\nHOW ABOUT FIREFOX???\n\nThat's the application I'm mostly worried about."
    author: "Max"
  - subject: "Re: Good and Bad - Firefox?"
    date: 2008-04-01
    body: "Wrong place. This site is for KDE. If you are worried about Firefox performance (even within KDE) you need to go to the Firefox community."
    author: "Anon"
  - subject: "Bad promotion"
    date: 2008-03-28
    body: "Sadly, Kontact have so many flaws.\n\nAnd dadly, this is the story of all KDE applications, they don't finish the basic issues and start promoting the other unfinished features.\n\nFix the basics first, Oh, and I know the song of \"We can work on both at the same time\", honestly, my experice say you can't, Kontact have lots of serious basic issues that haven't been fixed in years, and I think is idiotic to start promoting it as the \"Next big thing\", get real people. Kontact in its current state ain't not even close to be mediocre."
    author: "Div"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Care to list a couple of this \"basic issues\"?"
    author: "Kevin Krammer"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "The only \"basic issue\" I can think of is that KMail fetches mail in its GUI thread with synchronous operations. In a more understandable manner: KMail freezes when checking mail. This is certainly going to be fixed with Akonadi in the 4.2 release."
    author: "Stefan Majewsky"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Yeah, KMail is a pain in the ass. It's the only KDE app that makes me feel like throwing my computer through the windows sometimes and it's slow like a lawyer going to hell. I wish they would fix this before 4.2."
    author: "Bobby"
  - subject: "Re: Bad promotion"
    date: 2008-04-02
    body: "Ever tried Outlook? It's like a million times worse in almost every way... Incredibly hard to configure, unintuitive to use, and boy, don't try search on remote email folders - you'll be looking at a white window you can't even minimize for up to several minutes... Horrible experience, really. KMail rocks in comparison."
    author: "jos poortvliet"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Well..lousy imap implemantation in Kmail... and usabilty wise, korganiser sucks big time and has done so for years.. just compare it to google kalender or the kalender thing on OS X.\nAaron Seigo has been a disaster with is promotion of KDE4, plain and simple. \nIf (K)Ubuntu 8.04 would just include KDE 4 and not KDE 3.5.x then we would see people jump for gnome insted of KDE.  I can say that as a 10 year KDE user (since pre 1.0) KDE4 is the most overhyped and worst quality release ever. Seiga promoting this crap is totally absurd and really really makes me mad. "
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "I view kde4 like vista.\n\nVista is the driving force of people leaving Windows.  I believe KDE4 will be the driving force that has people shift to Gnome. I really don't like that it looks so much like windows.  But thankfully, Opensuse really don't like the look of it either and will be adding their own touches to it to make it look more like opensuse before they release it in 11.  So here's hoping they can improve it.\n"
    author: "anon"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "I view anon troll posts here like I view anon troll posts on other forums.\n\nThese posts are the driving force to help people realize they have to make up their own mind and choose software that works well for them.  I really don't like that anon troll posts bait me into replying.  But thankfully, millions around the marble appreciate the hard work put into KDE.  So here's hoping they continue to enjoy it.\n\nSee how just a few word changes can make something positive?  Please keep criticism 1) constructive 2) clear 3) actionable."
    author: "Wade"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Amen, brotha, amen. I for one switched _permanently_ to KDE4 a long time ago and see no reason to go back. Please count as one of the silent many who greatfully use KDE. I see the improvements everytime I update my openSUSE 10.3 install. Fact is, I have not used my FC installs since I started using KDE4. So in closing, all I can say/write is please continue the good work you are doing.KDE4.0 was released at the right time."
    author: "ne..."
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "The technology under the hood of KDE 4 is what will drive the \"interface nazies\" (Gnome Users) to KDE unlike what the troll a-non want people to believe. KDE 4 is not quite ready for the masses yet but when it is then I am sure that it will blow away any- and everything out there including OS X. This is something that the Gnomes should worry about ;)"
    author: "Bobby"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "I do agree that it will drive people to Gnome - but that will depend on KDE 4.1 or maybe 4.2 and depond on what K/Ubuntu will do, since its such a popular distro (even tough Kubuntu feels unpolished compared to Ubuntu DE-integrationwise) . I was glad to learn that Kubuntu still will have 3.5.9 along KDe 4.0-x - If it only had 4.0.2 then i would after 10 years have left for Gnome - i wond do it this time."
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "KUbuntu is not a very good example of what a KDE distro should be like."
    author: "Bobby"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Can you please elaborate a bit?\nAs an end-user, confronting concepts about 'good linux distributions' is appealing."
    author: "Pol"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Several of the modifications they make to KDE are less than well thought out. They are supposed to be some kind of usability improvements, but in most cases it's only removal of functionality whitout any real gain in usability.\n\nBut I find the most telling is when you watch Bugzilla, it seem like it has a higher amount of distribution specific bugs than other distributions. Bugs that don't exist if you compile KDE from source or use other distributions."
    author: "Morty"
  - subject: "Re: Bad promotion"
    date: 2008-03-30
    body: "Install KUbuntu, PCLinuxOS and openSuse 10.3 on the same computer, all running KDE and then you will understand what I mean."
    author: "Bobby"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Hello,\n\nit's not like there is no chance for return if somebody tries out Gnome now, he can always switch back, once KDE has surpassed Gnome again.\n\nThere is one reason to have 2 big desktop projects, and that is to allow the users to use them both and switch between them without hassle.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "First off, as it has been said, anon posters have no sack.\n\nSecondly, you're talking about comparing KDE 4 to Gnome.  Why?  If you're not happy using KDE 4 right now (and many aren't) there is KDE 3.5.9, which is still being supported.  If you honestly think Gnome is better than KDE 3.5.9, then why haven't you switched already?  Please do so, and stop posting here."
    author: "T. J. Brumfield"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Well said!"
    author: "Paul Eggleton"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Hello,\n\nactually I _did_ switch to Gnome on my work desktop out of complete disappointment with KDE4.0.x and my desire to try out something new (I have not tried Gnome since 1.4 days methinks).\n\nTruth is, it's not as good as KDE 3.5.x, but the compositing of Metacity is one of a mature window manager and therefore practical as such. \n\nIt's also true that Tracker appears to work and be well integrated, where as Strigi never really worked for me. \n\nOf course gg: and my custom variants of it are much missed in Epiphany now, so that's a bigger drawback. \n\nBut on the whole, it's not an as bad experience. In KDE 4.1.x I will return as a user, but for now it's good to break with habits. And KDE 4.0.x, seriously, is such a total and complete failure due to Plasma, that the KDE president should step down. ;-) It certainly would have the right drama about it, not... but still. :-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Bad promotion"
    date: 2008-03-30
    body: "I switched to XFCE. I see the whole KDE 4.0 move as a partial betrayal to their current users. I personally just don't know how people can love KDE 4.0. In my opinion it is going in the completely wrong direction. It has become \"trendy\", \"hyped\" and not easier to use.\n\nXFCE is not as feature complete as KDE 3.x, but in my opinion it is certainly more mature than KDE 4, and I don't really care what is under the hood, I care about the user experience, and where it is going.\n\nSeeing that KDE 4 probably will keep evolving towards what is Vista and MacOS X(with a windows touch), I thought switching to something else now would be a good idea. Some said that KDE 3.5.9 would be the last 3.x release, and in addition, v4 is clearly blocking the 3.x path, so, KDE as I know it is seemingly dead.\n\nBTW: those who point out twm/blackbox etc as \"Light\" alternatives are clearly wrong. AmigaOS 1.3 had more features than those and ran on a 7mhz computer. Why nobody has made a truly light weight desktop which is truly *fast* is beyond me. Perhaps programmers have become sloppy and ignorant to effective use of resources. In other words, you don't need megabytes of ram to show icons in a window =)\n"
    author: "m0ns00n"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Wow, with your grammar and spelling it sounds like you'd be a much better candidate for the promotion job! \n\nGreat to read such a well thought out, clearly articulated post as yours. Please, I'm there will be 'negative Nellies' out there that will call you 'an over opinionated tool who writes at a 3rd grade level and contributes nothing to the planet but greenhouse gas' and other such unsubstantiated, poorly thought out personal attacks with no visible basis in reality, but you keep up the good work! As I'm sure you must be aware, you're a great asset to the community and if more people spent time writing posts like yours the world would obviously be a much better place and certainly KDE would be a better project. Kudos."
    author: "Borker"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Screw my grammar.. im no native english speaker and if you understood what was written then it good enough. Remenber that most of us here are not native speakers.\nAnd.. KDE4 really sucks and we make a world a better place."
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "nice come back... I love the way you went into a detailed list of what 'really sucks' in KDE4 and your proposals for making it better. It certainly went a long way towards repairing the mistaken idea people may have come to that you are in fact a pointless, complaining troll."
    author: "Borker"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Well, i find you to be a pointless complaining moron .. but i can live with it. Why should i make proposals?  for years proposals have been ignored and you can just search for Seigos comments to see what he thinks about proposals - its a complete waste of time. Something you would know if you had been here for more than two months."
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "coward. "
    author: "borker"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "\"you can just search for Seigos comments to see what he thinks about proposals - its a complete waste of time\"\n\nTrue, he has repeatedly asked for constructive criticism and well reasoned proposals. Not useless and pointless whining and complaining, or repeating of the same already answered \"issues\". Since you obviously are incapable of that, in your case it's a complete wast of time."
    author: "Morty"
  - subject: "Re: Bad promotion"
    date: 2008-04-02
    body: "\"True, he has repeatedly asked for constructive criticism and well reasoned proposals.\"\n\nWell, when KDE 3.5 started, I realised that KsCD was broken, it simply stopped randomly at the middle of a track, and started from Track 1. This bug (121405)was reported by other people using various distributions.\n7 months after this bug had been reported I added a sarcastic (desperate) comment for the maintainer to tell us at least if this bug will ever (within KDE3) be fixed or we should switch to GNOME to listen to audio CDs. There was no answer unless you count it as an answer the fact that KsCD was moved from kdemultimedia3-CD to kdemultimedia-extra.\nWell, guess who the maintainer of KsCD was at that time (2006)? Yes, Aaron J. Seigo.\n\nThe problem with this is only that Kaffeine and Amarok with the Xine engine cannot play back audio CDs without adding gaps between the tracks. So KDE, while having several programs to play back audio CDs does not have a single one which does it properly.\nI am not using Kontact, but can understand the exasperation of KDE users who see that certain programs want to do everything without mastering the basics. (This exasperation started this thread off.)\n"
    author: "Imruska"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Screw my grammar.. im no native english speaker and if you understood what was written then it is good enough. Remenber that most of us here are not native speakers.\nAnd.. KDE4 really sucks and we make a world a better place."
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Just like re-reading a great book or watching a great film for the second time, I think reading your post a second time has only increased my appreciation of it's timeless brilliance. \n\n'KDE4 really sucks and we make a world a better place' just magic!\n\n"
    author: "Borker"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Hehe.. glad to assist you. Happy to know that you have nothing better to do than enjoying my posts - it realyy helps me appreciate your comments.\nKDE 4.0.x sucks ;-)"
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "How about explaining to us why kde4.1 will suck, or how kde 4.0.x sucks for you?\n\nJust imagine if we all communicated by shouting opinions at each other without providing context or explaining anything. Comments like this are a waste of space.Most of this thread has been you trolling and everyone else trying to get a decent argument out of you or just making fun of you."
    author: "Fool"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Hehe.. glad to assist you. Happy to know that you have nothing better to do than enjoying my posts - it really helps me appreciate your comments.\nKDE 4.0.x sucks ;-)"
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "Just like re-reading a great book or watching a great film for the second time, I think reading your post a second time has only increased my appreciation of it's timeless brilliance."
    author: "assist"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "A lot of work has been done on Korganizer interface, you're not very respectful for the developers. Have a look at the nice agenda view; they also introduced a new month view  which looks promising usability-wise.\n\nAnyway, you when you implement a new software, you have to promote it. People are free to use it or not. If it doesn't suit their purpose/expectation, they can wait for the next version. If there was no  \"promoting\" there won't be bug reports, there won't be bugfixes and the \"crap\" would stay crap.\n\nSuch a post is very disrespectful for developers. You might be a 10-years KDE user but it is not because you are not satisfied for one release that you can be so brutal ...\n\nBlah."
    author: "Tom"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "There is not much to promote in KDE4 so its pretty shameless to advocate its use - the quality is really bad. It just feels like a clumsy version of Vista - and Vista is already clumsy. KDE4 is pretty much hyped the same way Koffice has been hyped, when only Kword is usable - its ridiculous and turns people away from KDE when reality hits them.\nI'll wait for 4.1 before i try Korganiser. I know i had a hard time using it in 3.5.x before, and it was so unlogic to use that i gave up on it long ago.\n\nDon worry about developers find posts disrespectful - its only a opinion from a end user and its clear that we really dont count. Its just sad that every time someone is critical  then its judged as a troll post.\n"
    author: "Jos And"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "It wouldn't be judged as a troll post if the criticism would actually be, you know, constructive. Just saying that something sucks or is clumsy or unusable doesn't help anyone - and certainly not the developers.\n\nIs the current version of KDE4 as feature-filled and solid as the KDE3.5.x series? Certainly not, but none that I know of claims that it is either. It's been made VERY, in my opinion, clear that 4.0.x is for early adopters, developers, and/or people not afraid of getting their hands dirty every now and then while others (that is: end users) should wait at least until 4.1.\n\nWhile I'm no developer by any stretch of the imagination, KDE 4 IS usable if you don't mind some glitches here and there not to mention using KDE 3.x apps to fill the gaps (for example, Kontact, Amarok, K3B, Digikam, Koffice - the programs I personally look forward the most to come out of alpha/beta). And yes, I use KDE 4.0.2. as my main desktop and for the most part it works just fine. Not as well as KDE 3.5.8, mind, but at this point in time I didn't expect it to.\n\nAnd honestly, if you think Kontact is unlogical you can't have used many PIM suites. It's no more unlogical than say Evolution or Outlook. It may not suit your needs, but that's another issue entirely."
    author: "Jonas"
  - subject: "Re: Bad promotion"
    date: 2008-03-28
    body: "Sure.\nNot a complete list but some of the issues that I feel needs to be fixed.\nNo, I can't code well enough and I don't have time to learn but I do give some money to KDE e.v. every now and then in the hopes that the Kontact suite will benefit.\n\nI think Kontact is the best there is, but there are issues to be solved. A few quite critical.\n\nhttps://bugs.kde.org/show_bug.cgi?id=41514\nhttps://bugs.kde.org/show_bug.cgi?id=95064\nhttps://bugs.kde.org/show_bug.cgi?id=83414\nhttps://bugs.kde.org/show_bug.cgi?id=115426\nhttps://bugs.kde.org/show_bug.cgi?id=150790\n\nAnd a few of mine...\nhttps://bugs.kde.org/show_bug.cgi?id=151977\nhttps://bugs.kde.org/show_bug.cgi?id=151975\nhttps://bugs.kde.org/show_bug.cgi?id=151976\nhttps://bugs.kde.org/show_bug.cgi?id=151978\nhttps://bugs.kde.org/show_bug.cgi?id=151979\n"
    author: "Oscar"
  - subject: "Re: Bad promotion"
    date: 2008-03-29
    body: "I love KDE and Kontact especially, but the following bug has meant that for people like me who have accessibility issues / visual impairment, Kontact is sadly painful to view. \n\n\nhttp://bugs.kde.org/show_bug.cgi?id=159106\n\nPlease, please can this be looked at too?"
    author: "Maarte"
  - subject: "Re: Bad promotion"
    date: 2008-03-31
    body: "My main issue with \"korganizer\" is that the LDAP-backend for Kaddressbook is awfully slow, bordering to completely, utterly useless. I guess that if I was mad enough to use KMail, that would put this little annoyance way down in the priority list, but alas, I not that mad :)"
    author: "Kolla"
  - subject: "My two cents ... again and again"
    date: 2008-03-28
    body: "I couldn't help saying that. (Before you start parrying, please bear in mind, that I've been using Linux/KDE for the last five years exclusively).\n\nAaron utters very nice words but in general he's lying here and there.\n\nFirst of all, enterprises do not care about cross platform possibilities of KDE/Qt. They care much more about money savings and viability of their software solutions. Do you know why Windows is still tenfold more popular amongst developers than Linux and even Java? Because once you wrote your application you can run it for decades (!) without recompilation and burdensome duty of keeping track of your [open source] API breakage which happens daily in Open Source libraries. Do you know why people are very much unwilling to move from KDE 3.5.x to KDE 4.0.x? Because while KDE/Qt devs think how wonderful new KDE and Qt are there are plenty old really necessary applications which are almost abandoned by their own developers and it's next to impossible to use them in your newer shiny KDE. Open Source is wonderful but general users are *not* programmers. And general users have strong habits. My wonderful KDE 3.x applications are now left in dust because ... no one cares.\n\nMy second point is that ... we already have Java. We have .Net managed code. We already have XML driven user interface. While KDE4 is a fantastic DE, the world has already moved further to a point where recompilation is not necessary at all. And you just cannot supply your shiny KDE4 binary to Windows users.\n\nThe third point is that ... well, Qt is not a native UI renderer in Windows. It doesn't feel native in Mac OS. Have you ever seen Qt applications in Windows - they all look oddly. They have many problems with elements positioning and they feel and act differently then native Windows applications.\n\nThe fourth point is that Qt is not under LGPL or any other less restrictive than GPL license. That's why most ISVs use LGPL'ed GTK+ libraries under Linux. I still hope that Nokia will relax Qt's license and I hope KDE E.v. will follow.\n\nThe last and and not the least point of weakness is applications start up time. KDE team alone have more than 50 active developers and the same application compiled for both Linux and Windows, will start in Windows at least two times faster than in Windows. Users *do* want instant applications launch.\n\nOther random thoughts on the topic of Linux/KDE.\n\nOne of KDE developers has recently said: \"nobody in their right mind would choose Windows over GNU/Linux based on the desktop experience alone.\" I became almost furious over this sentiment.\n\nDo you know why people still prefer to *pay* for buggy, virus prone, registry  breakage prone Windows? Because Windows offer extremely *polished* desktop experience. Once set up properly Windows doesn't tend to crash here and here. Windows offers *keyboard only* driven GUI (you can virtually do anything without using a mouse) - the thing which is still *not* possible in *any* Linux DE.\n\nKDE developers still argue that smooth scrolling is not necessary, while Windows users have had it since ... 1997. And some people dare to say that smooth scrolling is very CPU expensive. Down right lies.\n\nWhat people really care is the smoothness of their desktop experience. The absence of crashes and annoying bugs. And there are bugs and wishes kept abandoned for years whereas hundreds of users vote for them.\n\nIt's all so sad.\n\nI'm very glad KDE has progressed that much in recent years, but I feel like nothing has changed since 1998 when I'd first encountered Debian Linux.\n\nAll hail KDE!"
    author: "Artem S. Tashkinov"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-28
    body: "If that cost you 2 cents I'd ask for my money back if I were you. Perhaps the clue store is having a sale and you could pick one up with the money you saved."
    author: "Borker"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "LOL - I give that a 10 out of 10."
    author: "ZOMG BBQ"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-28
    body: "to expand a little... when calling someone a liar it might be nice to back it up with something a little more solid than your vague opinions"
    author: "Borker"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-28
    body: ">> First of all, enterprises do not care about cross platform possibilities of KDE/Qt.\n\nAnd you know this how?\n\n>> Do you know why Windows is still tenfold more popular amongst developers than Linux and even Java?\n\nA true PFTA statistic\n\n>> The third point is that ... well, Qt is not a native UI renderer in Windows.\n\nQt integrates very well into Windows.  I've never received a complaint from a customer that it is not native.  Lots of windows apps use Qt and nobody even notices.  \n\n>> They have many problems with elements positioning and they feel and act differently then native Windows applications.\n\nYou obviously have no idea what you're talking about.\n\n>> will start in Windows at least two times faster than in Windows\n\nNow you're not even making sense.\n\n>> KDE developers still argue that smooth scrolling is not necessary\n\nThank god.  I hate smooth scrolling\n\n\n"
    author: "Leo S"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: ">>> KDE developers still argue that smooth scrolling is not necessary\n \n> Thank god. I hate smooth scrolling\n\nI like it but not when it feels laggy. It takes longer to smooth scroll because the animation takes (a little) time so delays are unavoidable. Thus, smooth scrolling is laggy. Maybe with a speed setting or a very high default speed we could talk about it. In fact, smooth scrolling when using search in KPDF/Okular is awesome =)"
    author: "Andreas"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "Ditto that.  Smooth scrolling drives me nuts.  At least Kopete offers a way to disable it."
    author: "MamiyaOtaru"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-30
    body: "> Ditto that. Smooth scrolling drives me nuts. At least Kopete offers a way to disable it.\n\nSomehow 1 billion Windows users don't run mad over it. How come?"
    author: "Artem S. Tashkinov"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-30
    body: "\"Somehow 1 billion Windows users don't run mad over it. How come?\"\n\nBecause they're stuck with it if they have it, that's why.\n\nIt has a tendency to feck screen readers as well as mouse scrolling. It's not a spectacular feature. Really."
    author: "Segedunum"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-28
    body: "Lying is not the right word.. he's just promoting KDE 4.0.x and he just shouldn't because the quality is not there.\nYou can do what i intend to do if you stick with Linux/*BSD/Whatever and things do not improve substantially for 4.1  - use Gnome as desktop and KDE apps as needed. I'll try to get used to Gnome even tough its going to be pretty annoying and use my prefered KDE apps - Kmail, Kate, Quanta, Konqueror etc."
    author: "Jos And"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "You can use KDE 3.5.* until 4.x matures. Kate and Quanta (AFAIK) are not yet ported to KDE 4 do you have to use the KDE 3 version so it makes more sense to use KDE 3 as DE."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "FYI, Kate is working, Quanta for KDE 4 is a work in progress IIRC. I'm happily using KDE 4.0.2 on Kubuntu here on my laptop, with apps like K3B, Kontact, KNetworkManager, digiKam still as KDE 3 applications."
    author: "Paul Eggleton"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "Sorry, somehow I associated with Kile. Kate in KDE 4 is great, indeed."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "Kate KDE4 is not doing it for me, so i just removed it and most other KDE 4 apps. In fact only my wife uses KDE 4 apps - and it is only the games; patience and minesweeper of some kind.\nWhat i find pretty amazing with KDE is that there is no compliancelist for apps and Desktop enviroment and therefore no way to verify apps and DE. So in fact releases are made without verification and improvements are mostly bug-squatting. That, I suppose, is the reason for  - NOT the bugs - the missing functionality."
    author: "Jos And"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-30
    body: "> Lying is not the right word..\n\nMy fault, sorry.\n\nHowever KDE will not ever become a good development platform without the things I've mentioned. Even relicensing of Qt/KDE libraries under LGPL will greatly increase the adoption of KDE amongst developers. Even most prominent distros (Ubuntu and Fedora/RHEL) have chosen GTK over Qt due to this reason. And Qt is such a great toolkit (actually much more than that)!"
    author: "Artem S. Tashkinov"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-30
    body: "\"However KDE will not ever become a good development platform without the things I've mentioned.\"\n\nKDE is the only Linux desktop with a half-decent development platform, and that comes from using Qt. Gnome doesn't have any kind of development platform that developers can pick up and use in a straightforward fashion, unless they use Mono. Beyond that, you're pulling in GTK, libXML, Cairo and all sorts of other libraries that behave and program completely differently. No ISV looking from the Windows and Mac worlds will touch that with a ten foot barge pole.\n\n\"Even relicensing of Qt/KDE libraries under LGPL will greatly increase the adoption of KDE amongst developers.\"\n\nQt already has a very vibrant software community, and quite frankly, I see very few actual ISVs out there using GTK (at all) because it is simply LGPLed. ISVs want quality development tools they can pick up, not zero cost stuff that doesn't work well and isn't usable.\n\nThe dual licensing means that investment goes into Qt, and KDE then gets better with each successive version of Qt. LGPLing something means that you accept people will have less incentive to contribute code back to the software, and that's the price you pay for 'developing for nothing'.\n\nI suggest you do some googling, because this has been discussed umpteen times before.\n\n\"Even most prominent distros (Ubuntu and Fedora/RHEL) have chosen GTK over Qt due to this reason.\"\n\nAh, not that old chestnut. It's done Ubuntu and Red Hat a fat lot of good supposedly standardising on GTK. The quality and depth of the graphical tools produced is woeful, and as long as that continues GTK will continue to hold Ubuntu and Red Hat back."
    author: "Segedunum"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-30
    body: "What do you think this is?\nhttp://fedoraproject.org/wiki/SIGs/KDE"
    author: "Kevin Kofler"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "\"Because once you wrote your application you can run it for decades\"\n\nIf the software is maintained, recompilations do not make a problem and for open source software under Linux it is usually done by distributors. Unmaintained open source software usually is not worth much and one cannot rely on it. Closed source Qt software are usually available with statically linked Qt - in this case future API changes (which occur only at major version changes) also do not matter.\n\n\"My second point is that ... we already have Java. We have .Net managed code\"\n\nI am not sure if it would be better to interpret some bytecode for the convenience of providing one binary for all platforms. (More precisely, I am sure: I wouldn't waste my CPU with using non-native applications much.)\n\n\"the world has already moved further\"\n\nSay the world has moved (or is moving). I am not sure if further.\n\n\"recompilation is not necessary at all\"\n\nOn Linux that is done by distributors. For Windows binaries, AFAIK it will be done by the KDE on Windows team. Up to this point it works smoothly (as KDE developers do not have to care about compiling, just writing the code and binaries appear in distros). If you develop proprietary software, it is more difficult to make it work on all Linux distros (although solvable with static linking) but it is not more difficult to compile (statically) a Qt program on Windows than to compile a native Windows program.\n\n\"your shiny KDE4 binary to Windows users\"\n\nAgain, you can, just an other binary.\n\n\"will start in Windows at least two times faster than in Windows\"\n\nWhat do you want to say?\n\n\"Windows offers *keyboard only* driven GUI (you can virtually do anything without using a mouse) - the thing which is still *not* possible in *any* Linux DE.\"\n\nWhat can be done with keyboard on Windows that cannot be under KDE?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: My two cents ... again and again"
    date: 2008-04-02
    body: "\"Windows offers *keyboard only* driven GUI (you can virtually do anything without using a mouse) - the thing which is still *not* possible in *any* Linux DE.\"\n\n\"What can be done with keyboard on Windows that cannot be under KDE?\"\n\nWell, I do not know, but unfortunately there are certain things that cannot be driven by keyboard in KDE.\nOne issue in question are the vertical \"side tabs\" in Konqueror. Navigating between the various tabs (root directory, home directory, services etc.) can only be done by mouse (I tried to find a possibility to assign a key combination, but I failed. So if there *is* a way to do it, I would welcome any suggestions.) The same with Kaffeine and Amarok (they also have these vertical \"side bars\").\n\n"
    author: "Imruska"
  - subject: "Re: My two cents ... again and again"
    date: 2008-04-02
    body: "with tab you should be able to give these tabs focus, and then you can open them... Haven't tried it, though."
    author: "jos poortvliet"
  - subject: "Re: My two cents ... again and again"
    date: 2008-04-05
    body: "I could open sidebars with tab but I couldn't focus in them. Anyway, I don't se any reason to navigate such an interface with keyboard - with keyboard typing in paths with completion is much more convenient. I don't think many users choose Windows because they can navigate such things in Windows."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: "Well, I will only respond to two points here. \n\nFirst, I've seen (and used) Qt apps run under Windows and they certainly feel native to me. \n\nHowever, even if they were slightly different in an aspect here and there...well, they're in good company then. Not even Microsoft's own software maintain a consistent look and feel to them. Vista looks and behave in one way. Internet Explorer is slightly different and not consistent with Vista as a whole. Office 2007 deviates even more, and so does Windows Media Player. Windows users don't seem to mind, so if Kontact (for example) doesn't blend in completely is unlikely to be a major showstopper. It may or may not be suitable for other reasons but it's un-nativeness is essentially a non-issue.\n\nAnd as far as your beloved KDE3 apps go...what's stopping you from using them in KDE 4? Those I've tried run just fine in KDE 4 with no recompiling necessary. True, they don't take advantage of the new features but a Windows app made for XP won't take advantage of Vista's new features either (but there's no guarantee that they will run. API breakage is not exactly unheard of in Windows or MacOS X either).\n"
    author: "Jonas"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-29
    body: ">> Do you know why Windows is still tenfold more popular amongst developers than Linux and even Java?\n\nBecause most of the best applications and the most useful applications for users out there are written for Windows. And the users are not programmers, they will not care if about binary compatibility ;).\n \n>> My second point is that ... we already have Java. We have .Net managed code\n\nUse java to build a ultra-complex Graphical User Interface to achieve cross platform and avoid recompilation? Thanks, but no.\n\nI have already had enough experience with it. Java is good for server-side applications and client side applications that don't need too much ultra-fancy painting capability. Java VM + bytecode interpretation will make your program slow as turtle (I don't care if people say Java is not slow anymore, my measurement still says Java *is* really slow)\n\n>> Because Windows offer extremely *polished* desktop experience\nMaybe true\n\n"
    author: "fred"
  - subject: "Re: My two cents ... again and again"
    date: 2008-03-30
    body: "> Windows offers *keyboard only* driven GUI (you can virtually do anything without using a mouse) - the thing which is still *not* possible in *any* Linux DE.\n \nA friend of mine has a notebook with a broken touchpad. He's using his KDE 3 just with the keyboard, and that does not slow his workflow down."
    author: "Stefan Majewsky"
  - subject: "Re: My two cents ... again and again"
    date: 2008-04-02
    body: "Do you know why people still prefer to *pay* for buggy, virus prone, registry breakage prone Windows? Because Windows offer extremely *polished* desktop experience. Once set up properly Windows doesn't tend to crash here and here. Windows offers *keyboard only* driven GUI (you can virtually do anything without using a mouse) - the thing which is still *not* possible in *any* Linux DE.\n\nPolished? Windows is anything but polished... Start ms word doc, minimize it, open a second word doc, the other one is maximized - totally crazy. Or try alt-tab - most apps open a 'hidden' extra window, making alt-tab incredibly annoying. Need more examples? Open a bunch of webpages so the taskbar starts to add all windows into 1 button. Now enlarge the tab bar - no matter how much room there is, it won't put em back individually. And have you ever heard of focus stealing prevention? Redmond clearly hasn't (Yeah, it's Lubos, our KWin maintainer who invented it some years ago). And I can go on and on... Linux does ALL of these things perfectly, has done so for years.\n\nAnd you complain about keyboard driven gui - most Windows apps can't change shortcuts, entirely unlike KDE - where you can always change all shortcuts. We're lightyears ahead in that area...\n\nIf there is ONE desktop which is extremely unpolished, it's MS Windows. By far."
    author: "jos poortvliet"
  - subject: "Re: My two cents ... again and again"
    date: 2008-04-05
    body: "> My second point is that ... we already have Java.\n\nHow much market share has Java on the Desktop? Right.\n\n> We have .Net managed code. We already have XML driven user interface. While > KDE4 is a fantastic DE, the world has already moved further to a point where\n> recompilation is not necessary at all.\n\nKDE also has \"XML-driven User interface\" (KXmlGUI). Of course that doesn't cover all your application, as the actual GUI is always only a tiny part of the app. And in Java, the vendor has to maintain binary compatibility as well.\nE.g. you can't add a method to a public interface without breaking BC.\n"
    author: "Frank"
  - subject: "KDE4"
    date: 2008-03-29
    body: "I think Aaron takes the right lesson. For the platform it would have been better to make a gradual transition. Eg. to first port KDE 3.5 to QT4 as a KDE 3.6 and then work on the next big things. Also the Suse kicker is available for a long time. You need a platform with users, then the rest will just emerge."
    author: "andy"
  - subject: "Re: KDE4"
    date: 2008-03-29
    body: "Porting unmaintained KDE 3 code to Qt 4 that is to be dropped and replaced anyway would have been waste of time.\n\n\"the Suse kicker is available for a long time\"\n\nDo you mean Kickoff?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE4"
    date: 2008-03-29
    body: "Hello,\n\nand here I wonder why we keep getting told that KDE 3.5 _is_ still maintained for years? That doesn't seem to fit into your statement.\n\nAnd Plasma team is investing a lot of time into explaining why Plasma can't do anything at all yet.\n\nTo me, it's clear. Kicker and kdesktop are applications. As these they were _not_ within the scope of KDE 4.0.0, which was to provide the libraries for the applications.\n\nAll in all, KDE 4.0.0 was taken hostage by dropping kicker and kdesktop, to the point, that KDE 4.0.0 had to be delays by months and _then_ still was not feature complete in _only_ that, for even the most simple and core desktop tasks. \n\nSaying that investing a few weeks into something would have been a waste of time, means to somehow not recognize the damage that has been done to the project. I can very well imagine it wouldn't have been fun. I can very well understand that nobody did it. But then, did anybody see a chance to do it?\n\nThere were people highly enthusiastical about KDE 4.0.0, like that guy who wanted to do all the linguistic tricks. But now I guess, KMail 4.0 won't be able to spell check different languages per paragraph. Happy that we got some cool gizmos instead? Me not. \n\nProbably I making up options, probably that guy would have droped out anyway, but as somebody who actively tried to run SVN, I can tell you, that it turned me away that there simply was no desktop shell for many months.\n\nI strongly advise KDE to never ever let anybody put anything on a criticial path before he or she has shown the code. Were Plasma really such a great thing to must have it in 4.0.0, we certainly should have had the code first, not?\n\nBut coming back to sanity, hey, how was every decision to be made to be 100% right, or else we get mad at each other? A lot wise things were decided for KDE4. \n\nBesides, not porting the workplace shell is not among them, but then again, the one who now understood how great an idea it is to port to MacOS and Windows, probably will understand as well, why Plasma should be ported. \n\nWell, once people will be able to stop laughing about Plasma as a suggest replacement for anything now functional.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: KDE4"
    date: 2008-03-30
    body: "So all the Plasma developers are all magical super-good PIM programmers? Not all programmers are proficient or even enjoy to code in all areas of programming. Plasma is not the cause of KDEPIM being delayed to KDE 4.1."
    author: "Jonathan Thomas"
  - subject: "Re: KDE4"
    date: 2008-03-30
    body: "No, Plasma is not a cause for delay of 4.1 which was supposed to be the release for applications to catch up. Plasma is the cause for delaying 4.0 and should itself always have been part of 4.1 only.\n\nThe delay is work for KOffice besides, I think they will miss the 4.1 time frame as well or drop almost everything application-wise from their release. A pity.\n\nThe thing that Plasma did, was making the SVN version unusable for a long time, which definitely has turned away people from trying to be a part of it.\n\nLet me put it that way: All the arguments why 4.0.0 needed to be released with Plasma in its nascent form are actually arguments why 4.0.0 needed to be released without Plasma.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KDE4"
    date: 2008-03-30
    body: "As much as I respect the work going into Plasma, I'm afraid I must say I'm inclined to agree. Do you want to know how Kompare was ported to KDE 4? Using a kdelibs4 package on KDE 3.5! To a point where I proofread my changes in... Kompare from kdesdk 3.5 (because that's what was integrated in my desktop)!\n\nThat said, Plasma _is_ improving quickly, 4.0.3 is essentially usable (thanks also to the feature backports; I must say I really don't understand the people complaining about those, they were very much needed!) and I'm no longer worried about shipping that in Fedora 9. (Shipping with 4.0.0 or daring to ship one of those so-called \"release candidates\", however, would have been a disaster.) There _are_ some remaining annoyances though, e.g.:\nhttps://bugzilla.redhat.com/show_bug.cgi?id=439636\nhttp://bugs.kde.org/show_bug.cgi?id=160073\n(Yes, that's the same annoying bug, we asked the reporter to report it upstream too.)"
    author: "Kevin Kofler"
  - subject: "Re: KDE4"
    date: 2008-03-30
    body: "I was talking about the wait for 4.0.0, and how KDEPIM was pushed back to 4.1."
    author: "Jonathan Thomas"
  - subject: "Re: KDE4"
    date: 2008-03-30
    body: "Hello,\n\nexcept for base, games and edu, no applications were supposed to be ported for the release of 4.0.0 if I am not all wrong.\n\nThat release was to get kdelibs in a perfect state and to integrate more techs in the framework that apps would then start using.\n\nI don't recall KDEPIM being delayed. Actually KDEPIM more or less has had an independent release schedule and was focused on stable releases with incremental feature additions, wasn't it?\n\nI don't mind anything being pushed to later. I do mind that Plasma was not pushed back and as a consequence was pushing everything else back.\n\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KDE4"
    date: 2008-04-02
    body: "So you say KDE 4.0 should have been released without a desktop? \n(to clarify, without plasma = without desktop as porting kicker & kdesktop would realistically not have been possible - just read and think about it)"
    author: "jos poortvliet"
  - subject: "Re: KDE4"
    date: 2008-03-29
    body: "Not a bit. The problem then would be that we'd have a \"stable\" KDE that would be continually breaking its API every few months. You think that's better? It's certainly not a recipe for quality & stability.\n "
    author: "T"
  - subject: "twits"
    date: 2008-03-29
    body: "sometimes i wish people would learn to lurk.  just hang out and read, and then read some more, then read a bit more.. before they start posting their 'opinions'.  that or i begin to wish sites would stop providing the 'user feedback' option.  given, user feedback is a driving force that makes opensource software meet users needs better in ways 'corporate' goals don't always address.  too many only understand how to complain without understanding the importance of constructive criticism.  sometimes it seems people think developers of free software should knock on their door, ask how they can make FREE software that works for them, and ignore how others may want to use the same software.\n\nin aaron's keynote speech (at google) he did an excellent job outlining the benifits and potential of the various kde4 frameworks.  there is a huge amount to be excited about.  but people are not patient.  and software development requires patience.  many of the replies to this post say people either failed to read, listen, or understand what kde4 is about.  and the potential it holds.  if a particular freely provided software doesn't make you happy... just go somewhere else.. unless you can add something useful.  but it seems interpretation of what something useful actually is fails to gain understanding.\n\ntransition periods are difficult and i can only hope aaron and others can ignore comments such as those here during this transition period.  from what i've understood of kde4 and its future, the kde developers are trying their best to provide the best solutions they can.  i think they are doing a wonderful job, and i applaud them.  \n\n"
    author: "tooth"
  - subject: "Comments on tone"
    date: 2008-03-29
    body: "To those who complain:\n\nWhat has happened so far is:\n1. Make huge under-the-hood changes\n2. Release a version to put out your API and get testing by early adopters\n3. (in progress) Fix bugs, add missing features\n\nSo step 3 is going to be continuing throughout the rest of the year. Why complain about it? Who is forcing you to use it? You already have a great, free, product to use, why complain about another product that is also given freely and will someday be truly great? And while the developers get their work done, it's not like they're blocking your driveway, or spewing toxic fumes into your living room. In fact, their work should not be impacting you in any negative way whatsoever, since you're free to use whatever software works well for you.\n\nAlso, why complain about the enthusiasm & evangelism the developers show for their work? I think it's wonderful. Free software progresses by attracting developers who care for a product, and without promoting the advantages of the framework that has been developed, it is hard to imagine how others will learn enough about it to become intrigued and join the project.\n\nBottom line: if you don't like it, just wait until it is better. Nothing more is being required of you, and you'll save a lot of saliva by not foaming at the mouth all the time. If you are a patient and constructive person, then you can contribute by providing constructive feedback that acknowledges the value of what is being so freely given.\n"
    author: "T"
  - subject: "Re: Comments on tone"
    date: 2008-03-29
    body: "I cannot disagree with you and I'm really sorry for my complains.\n\nHowever I've always wanted Linux to become a real alternative to Windows and it just cannot happen if we don't have a set of stable APIs.\n\nISV just cannot spend their time and resources compiling their applications even for five most popular Linux distros. They will not spend their resources because Linux distros and API's are maturing at such a speed, so that your *binary* application is not guaranteed to run even in a one year term.\n\nAnd closed source software is here to stay for at least the next 25 years."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-29
    body: "For a couple of years now there is the LSB and basically all distributions do have support for it or are even certified.\n\nThe LSB is also moving at a slower pace and additionally has a long deprecation period.\n\nHowever you don't see ISVs actually using it."
    author: "Kevin Krammer"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "LSB was nice ... in theory.\n\nAlas, in reality LSB is more of a moving target. The target which is moving at a very fast speed. New LSB releases appear almost every year - that being said we are again at the point of unstable APIs and developers who shun writing their applications for Linux."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "It doesn't matter if there are new releases of the LSB, because they add things and at most deprecate older ones, thus staying backwards compatible.\n\nDeploying on more than one platform is alway more work than deploying on just one, but developing for that additional platform, or as you put it \"...writing their applications for Linux...\" is not.\n\nHave been doing this for years and I really can't hear the whining anymore, usually from people who made an unfortunate choice regarding and are now locked into a single platform or would have to resort to \"porting\"."
    author: "Kevin Krammer"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "Even though theoretically most newer Linux distros comply with the latest revision of LSB, the fact is that Linux distros have:\n\n1) Different versions of libraries (and applications are not guaranteed to work flawlessly with different versions of libraries)\n2) Different sets of file and directory structures (I mean menu files hierarchy, system settings, etc.)\n3) Different packaging systems (for now we have at least four: RPM, DEB, TGZ/SlackWare, Sources/emerge/Gentoo). Even RPM implementation greatly differs amongst RedHat, Mandriva and Suse - e.g. the same libraries have different names.\n\nTake a note that a very rich VMWare Co. spends a considerable amount of time just testing how their products work with different versions of Linux distros. Most shareware writers/programmers cannot afford such expenses ever. Thus most shareware and freeware utilities (some of them are very useful and necessary) will never reach the world of Linux/OpenSource.\n\nAnd we (open source users) are thrown ourselves at mercy of those who have a spare time and desire to write something (which may not even work as expected) ... While Windows users enjoy thousands of high quality flourishing software releases because its writes get *paid* and they will not break their heads trying to find out why their piece of code doesn't work in distro X.\n\nWithout stable API Linux will be at the bottom of Desktop for a very very long time.\n\nHave you ever considered why Wine has become so popular and attracts so much attention recently? Because Wine has a power to preserve mostly stable Windows API even in the light of lacklustre Vista reception and possible Microsoft demise."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "> Different versions of libraries (and applications are not guaranteed to work flawlessly with different versions of libraries)\n\nIf this is about libraries not covered by the LSB then it is not likely that a third party vendor will be using the installed one but rather bundle a specific one with their product, just like they do on other platforms.\n\nIf this is about libraries covered by the LSB, can you given an example? Libraries in LSB are specified at symbol level and thus even newer versions should at least provide everything an application expects them to provide.\n\n> Different sets of file and directory structures (I mean menu files hierarchy, system settings, etc.)\n\nI don't know of any desktop environment not using the freedesktop.org menu specification. As far as system settings are concerned, most applications do not anything to do with them anyway. But I am open for a list of applications that do.\n\n> Different packaging systems (for now we have at least four...\n\nNot from the point of view of an LSB application and traditional software vendors are more likely to use an installer anyway and there are plenty of those, proprietary and free ones.\n\n> Take a note that a very rich VMWare Co. spends a considerable amount of time just testing how their products work with different versions of Linux distros.\n\nYes and they are one of very few applications which needs to integrate at kernel level. Most desktop applications don't.\n\n> Without stable API Linux will be at the bottom of Desktop for a very very long time.\n\nAh, nice try. Both GNOME and KDE APIs as well as any API they are building on have been stable for years.\n\nBesides, ISVs do follow changes on other platforms as well, moving to the lastest and greatest of the platform APIs, but somehow do this without complaining.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "\"For a couple of years now there is the LSB and basically all distributions do have support for it or are even certified.\"\n\nThe LSB, and projects like Portland, are failures. Nobody is using it."
    author: "Segedunum"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "I wouldn't say the Portland project is a failure. They have produced xdg-utils, which is really useful, and many packages in Fedora which aren't part of a specific desktop have been configured or patched to use xdg-open to open their files. That way, they will follow the settings of the desktop of your choice. Given that the previous preference was gnome-open, this is a particular improvement for us KDE users."
    author: "Kevin Kofler"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "Alas, Portland project is irrelevant to this topic - though I highly appreciate people's attempts for unifying. I'd even say that such projects can lead some day to a bright future where we have slightly different Linux distros with very similar cores (libraries, configuration, etc.) where they only differ in their artwork, installers and booting process.\n\nI was talking about 100% binary compatibility between distros and it's nowhere to be seen."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "\"I was talking about 100% binary compatibility between distros and it's nowhere to be seen.\"\n\nI'm afraid you've been proved wrong there. Conclusively. Installing between distros is a problem. Binary compatibility? Nope."
    author: "Segedunum"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "> Installing between distros is a problem. \n\nIt's not. You can rpm2cpio rpm package, untar deb package, `tar -zxf TGZ package` and simply copy it into your file system directly.\n\n> Binary compatibility? Nope.\n\nReally, nope. Applications compiled for Qt 4.3.x may or may not run with Qt 4.4.x, the same goes for GTK :-)\n\nI've seen that, I'm not imagining."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-31
    body: "As far as I can download a Mysql rpm for RedHat, use alien to convert it to deb and install it without a flaw, LSB works"
    author: "Vide"
  - subject: "Re: Comments on tone"
    date: 2008-03-29
    body: "> And closed source software is here to stay for at least the next 25 years.\n\nThis is nonsense. Have you tried to run 16-bit-Win-Apps on Vista? Have you tried to use your MS-Office-97-Macros in Office-2007? (Have you ever tried to use it on an \"i18n'ed\" Office?\n\nHave much fun..."
    author: "Andreas"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "> This is nonsense. Have you tried to run 16-bit-Win-Apps on Vista? Have you tried to use your MS-Office-97-Macros in Office-2007?\n\nOffice 97 runs just fine in Windows XP SP3 right now. The former was released in 1996, the latter will be released soon.\n\nSo, as you can see, you can run 12 years old software just fine. *Most* 12 years old open source software will not even *compile* under newer distros.\n\nHave fun either."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "\"Office 97 runs just fine in Windows XP SP3 right now. The former was released in 1996, the latter will be released soon.\"\n\nThat's not what you were asked. XP was released in 2001 (mentioning SP3 is totally irrelevant - I'm sure you didn't do that on purpose;-)), and Office 97 four or five years earlier. Of course it will run. Getting Office 97 to work on Vista is slightly more problematic:\n\nhttp://www.google.co.uk/search?hl=en&q=%22office+97%22+%22vista%22&btnG=Search&meta=\n\nHave you tried running 16-bit Windows applications in Vista? DOSBox provides better support for DOS apps than Windows, and at the rate things are going WINE will provide better backwards support for Windows apps than future versions of Windows. Microsoft have lost control of their own APIs.\n\n\"*Most* 12 years old open source software will not even *compile* under newer distros.\"\n\nTwelve year old open source software doesn't need to compile under newer distros.................because it's open source. There is a far newer version of a piece of open source software available now with more features than what was available twelve years ago. Why would I want to do that?\n\nAs for twelve year old binary software, Motif applications still run, I have 8+ year old games that still run on any Linux distro and provided you have KDE 3 libs, KDE 3 apps will run, as will KDE 2 apps if you have KDE 2 libs installed. As demand increases those libs will continue to be shipped, and see better integration with older applications inheriting new features. On top of that, the ABI stability of our favourite compiler has got infinitely better over the years.\n\nYou painted right over the real problem with Linux desktops and distros today, and that is general software installation, but as for backwards compatibility, things look pretty good indeed."
    author: "Segedunum"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "> Have you tried running 16-bit Windows applications in Vista?\n\nWhy are you so possessed with running 16 bit applications under Vista? After all there's a free DosBox which perfectly runs even under 64bit Vista.\n\n> Twelve year old open source software doesn't need to compile under newer distros\n\nWhy are you so certain about that?! There are plenty useful old Linux applications which do not run in newer Linux distros. And they don't compile either. And you have decided *for me* that I don't need them.\n\nWhat I say is that in Windows (even in Vista) you ***can*** run 12 years 32bit old useful applications and utilities (e.g. screensavers). And programmers who wrote them have changed their profession a long time ago, thus leaving you with *no support* but with *working* applications.\n\nAnd Wine's 16 bit windows API is *very* incomplete - you can believe me.\n\nAnd still Open Source applications work perfectly in Windows."
    author: "Artem S. Tashkinov"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "\"Why are you so possessed with running 16 bit applications under Vista? After all there's a free DosBox which perfectly runs even under 64bit Vista.\"\n\nBecause that's what you were asked. Will Vista run these older Windows and DOS applications out of the box without having to resort to projects like DOSBox and WINE? No. You're spinning I'm afraid.\n\n\"Why are you so certain about that?! There are plenty useful old Linux applications which do not run in newer Linux distros. And they don't compile either. And you have decided *for me* that I don't need them.\"\n\nYou're talking about twelve year old *open source software*. Read that again. Versions move on and latest versions are shipped with distros. I'd be pretty surprised if you could get a lot of Win95 code to compile and run perfectly under XP and Vista (much has changed that requires umpteen compatibility settings). On a Linux system, all you need are the relevant libraries and headers in most cases, and as I'd pointed out, Motif applications will still run as will 8+ year old games - on any distro. You don't have a point here.\n\n\"What I say is that in Windows (even in Vista) you ***can*** run 12 years 32bit old useful applications and utilities (e.g. screensavers).........thus leaving you with *no support* but with *working* applications.\"\n\nThe people trying to get Office 97 to run properly wouldn't agree with you, and the standard response is that it isn't supported now. WINE provides better support for Office 97 than Vista does.\n\n\"And Wine's 16 bit windows API is *very* incomplete - you can believe me.\"\n\nVista and XP's support is almost non-existant, and you'll be very lucky to get a lot of apps working. WINE's support for such apps, and improvements to make them work, is in better shape and is still ongoing.\n\n\"And still Open Source applications work perfectly in Windows.\"\n\nErrrrrrrr, yer, because the source code is there. Even then, there are still a lot of unsupported features and software when moving from Win9x -> NT for a great deal of open source projects. A lot of binaries you might have had running on Win9x won't run on NT, and support is generally dropped."
    author: "Segedunum"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "Ever heard of static linking? That's how most closed source software work, even on windows."
    author: "Patcito"
  - subject: "Vista is binary compatibility misery"
    date: 2008-03-31
    body: "I have direct experience of moving a bunch of 2001-era programs running on Windows 98 SE to Vista.\n\nOutlook 2000 *does not work* on Vista.  Even with address book hacks (Microsoft changed the system address book completely) it crashes.  Fireworks 4 and Dreamweaver 4 do not work properly on Vista.  All three of these programs crash upon exit.  I won't even mention the unsupported hardware and broken drivers.\n\nISVs will not spend time and resources making their old application versions work on new Windows.  They may be willing to sell you a newer version.  Gee, thanks.\n\nI hate closed-source software and am doing my best not to buy any more.  (Though when getting it for free... it's harder to say no.)  And I really regret getting new hardware preloaded with Vista; XP would have worked much better and a Linux distro + WINE would probably have about the same level of incompatibility with 7-year old software that Vista has.\n\nI realize this doesn't contradict your precise points, but Windows stability is less rosy than you portray.  After two or three releases, you're compelled to buy new software, often from new vendors."
    author: "skierpage"
  - subject: "Re: Comments on tone"
    date: 2008-03-29
    body: "While i agree on toning down discussion i cannot understand why you developers take your products  being criticized and your views thwarted. \nOf course you are free to go your way, whatever people say, but that is not the open source community many people have in mind. \nOpen communities should be able to welcome professionals from different fields and simple users, making every effort to understand their needs and complains, so as to be able to exploit such wide diversity of competences. You appear to go the opposite direction. \nOk, such efforts should be be made by both parts, but i have noticed several times on these pages, you seem to close the door to any confrontations. \n\n--\nPol     \n"
    author: "Pol"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "> Open communities should be able to welcome professionals from different fields and simple users, making every effort to understand their needs and complains, so as to be able to exploit such wide diversity of competences.\n\nWhen I work for a client, I listen to his needs and complains, when I work on free software I do it for me only, if you happen to like it, good for you, but don't expect me to listen to your needs. \nThat's free work I'm doing for fun or for my own needs, \"free work\", get it? When was the last time you did something for free? When was the last time you did something for free for others? Hacking can be pretty boring when you're not hacking on things that are of interest to you, that is why free software developers only works on things that answers their own needs, otherwise it gets boring. I hope users could understand that one day. Buy or download yourself a C++ book, start learning and programing, you'll soon understand :)\n\nCheers"
    author: "Patcito"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "While I agree that you're basically right with this statement, you nevertheless should be aware that this attitude will get you absolutely nowhere as soon as you try to adress more than the open source geeks  (making up probably about 0,000001% of all computer users).\n\nKDE cannot aim to be a desktop for the masses if, as soon as there is criticism, key players retreat to the \"leave me alone or code it yourself\" mantra."
    author: "Anon"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "Patcito, despite being right, is not answering anyone. Of course he can code whatever he wants to the way he wants too. So what?\n\nKDE is much more than a collection software made, for free, by guys who are coding for themselves and don't really care about potential users, and that might just happen to suit other people's needs. "
    author: "ad"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "> KDE is much more than a collection software made, for free, by guys who are coding for themselves and don't really care about potential users, and that might just happen to suit other people's needs.\n\nVery few people are being paid to work on KDE, amarok, kate, digikam, konversation to name a few, most plasma developpers etc...\nSo yes, most people working on KDE do it for free and for their own needs, it just happens that KDE attract a lot of people and so many needs and wish gets answers and solutions. Sometime though, no KDE devs have interest/fun/needs in developping something so it just doesn't get done. The good thing though is that as this is Free Software so you can always hack it yourself OR pay someone to hack it for you OR you can use something else. I couldn't care less what you use, use whatever you want and if you don't like it, fix it, pay somebody to fix it or switch to something else, as simple as that."
    author: "Patcito"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "\"So yes, most people working on KDE do it for free\"\n\nSure. \n\n\"and for their own needs\"\n\nBullshit. \n\n\"I couldn't care less what you use, use whatever you want and if you don't like it, fix it, pay somebody to fix it or switch to something else, as simple as that.\"\n\nThat's fine. Just not the attitude of most kde devs though.\nAnd it's actually disrespectful towards many of them to say it is."
    author: "ad"
  - subject: "Re: Comments on tone"
    date: 2008-03-31
    body: "Certainly they work on things that they find interesting but they also answer feature requests from users. If they only did it for themselves they wouldn't bother would they. I imagine the kde devs get a lot of satisfaction from people using something they have created just like someone else. \n\nTherefore having an open attitude helps everyone. Telling someone to go away, I don't want to do it or generally being curt helps nobody. There just wouldn't be a community, nor would this site be here if KDE devs didnt care about users"
    author: "Fool"
  - subject: "Re: Comments on tone"
    date: 2008-03-31
    body: "I still maintain that if a feature present no fun/interest/need for the developer, it won't be implemented unless he's paid for it. It's not about telling the user to go away, just \"sorry but I have no time/interest adding your feature, maybe some day, somebody will do it\". Sorry if you believe that not wanting to implement your pet feature is \"being curt\", that's not very accurate and fair."
    author: "sloppy bobby"
  - subject: "Re: Comments on tone"
    date: 2008-03-30
    body: "I don't imagine their are many shills posting to dot.kde.org so the negative response by some here is surprising.\n\nIf you don't like KDE the code is there for you to change it."
    author: "Tom Callway"
  - subject: "Slightly off topic:Can't wait for Aaron 4.1keynote"
    date: 2008-03-31
    body: "Slightly off topic: Can't wait for Aaron Seigos KDE 4.1 keynote.\n\nI think we can make KDE 4.1 seem even cooler than Steve Jobs' Reality distortion field. At least ours is real. :)\n\nPlease let us know if you need any donations for video equipment or a conference hall to shoot a KDE 4.1 keynote.\nYour last keynote is why KDE 4 has been in the minds of so many college students and professors.\nPlease also make it cool like the last keynote.\n\nDon't forget to include coverage of AMAROK 2.0. It will draw people to KDE, the same way iTunes and the iPod drew people to Mac OS-X. :)\n\n"
    author: "Max"
  - subject: "Sorry Aaron"
    date: 2008-04-01
    body: "Until some key long standing issues in KMail are addressed ( some of which are 4+ years old in bugzilla ), Kontact is not going to be any kind of competitive threat to Outlook.\n\nI am talking about bugs like 86463, which is 4 years old now and has over 700 votes - I know developers want to work on what is fun, and I understand that, but things like preserving HTML formatting are so absolutely critical to any modern organization I don't see how people expect KMail to be taken seriously.\n\nI mean, I work in a hybrid office environment. 1/2 the developers use Linux, 1/2 use Windows - so we are definitely not a windows-centric organization, even though we run Exchange. And yet, I don't know of a single person who uses KMail, because it is simply not up to the task. Even I am forced to run Thunderbird in my KDE desktop.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Sorry Aaron"
    date: 2008-04-02
    body: "Hmm, 86463 was fixed years ago... You must be talking about something else?"
    author: "jos poortvliet"
---
<a href="http://www.siriusit.co.uk">Sirius'</a> Tom Callway <a href="http://www.siriusit.co.uk/myblog/interview-aaron-seigo-kde-project-lead.html">interviews KDE's chief-hugger Aaron Seigo</a>. He talks about KDE 4, communication within the project and what effect the 'new platforms' (Windows and Mac OS) have on KDE development. Seigo also talks about the KDE 4.0 release and how that compares to the 2.0 and 3.0 releases of KDE. About Kontact, he explains, "<em>We are going to see some very interesting developments happening when Kontact is available on all platforms. For instance, finally there will be a groupware solution that looks and behaves exactly the same on all platforms (a support win) that lets you choose your groupware server (a server side win). Kontact represents the client side of the first realistically competitive threat to the Exchange-plus-Outlook hegemony. And that's just one application.</em>"




<!--break-->
