---
title: "openSUSE 11.1 Released with KDE 4.1.3"
date:    2008-12-19
authors:
  - "fgiannaros"
slug:    opensuse-111-released-kde-413
comments:
  - subject: "KDM Theme"
    date: 2008-12-20
    body: "Can sombody tell me why openSuse has to choose the worst made (among all distros) KDM theme as default?\nLook at the GNOME (GDM) theme, isn't it beautiful? Then compare it to the KDM green stripes with fonts that one can't even read - an embarrassment.\nI love openSuse but come on people, you can't be really serious about this."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-20
    body: "Strange comment, I actually _really_ like the look of the login screen, and from what I've seen almost everyone does. The fonts are quite perfectly readable. The green stripes look pretty slick, and the design fits in well with the boot, installer screen, etc. \n\nThere have virtually been no complaints about this -- I think you just have slightly more idiosyncratic tastes. You can very easily change the KDM theme from KDE's system settings, anyway."
    author: "Francis Giannaros"
  - subject: "Re: KDM Theme"
    date: 2008-12-20
    body: "Yes it's possible that I have a weird taste, that I am not cool but I love the GDM theme in contrast to the KDM Theme. I even like the original KDE theme more than the opensUSE one even though it doesn't match the green boot screen stripes. Couldn't they use a background similar to the default wallpaper? Yes I like the wallpaper but I hate green stripes! Apart from that we had those same stripes in 10.3 I think it was. They just did some recycling, no new ideas. Maybe that's why I don't like it."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-21
    body: "\"You can very easily change the KDM theme from KDE's system settings\"\n\nThis is not true unless you are using another openSuse 11.1 with a different KDE 4 than the one that I have installed. \nFirst, as a user there is no possibility of changing the KDM theme on KDE 4.x so one has to log in as root in order to do so. \nNow the theme is changed so one logs out or restart X just to find a fall back theme instead of the one that was chosen. \nAm I missing something?"
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-20
    body: "I like it too. Even if green is not my favourite colour (I prefer Fedora's blue) it is well integrated and very nice: a great improvement in usability respect the silly gdm I was \"forced\" to use in Fedora 8. openSUSE was always (and is) the best looking Linux available."
    author: "RGB"
  - subject: "Re: KDM Theme"
    date: 2008-12-21
    body: "You don't have to like Gnome (I don't either) but if there is one thing that the GTK guys can do then it's designing Themes. Actually it's the only thing that I like about Gnome. \nLook at Ubuntu for example, they use brown. Brown is a really boring colour to me but these guys make it damn beautiful and attractive.\nApart from that GDM has at least one feature that KDM doesn't have: I can use more than one themes at the same time like a diasshow. This isn't possible with KDM AFAIK, it's very easy to chang and install themes on GDM and there are a lot of Themes available for this login manager.\nOf course I prefer KDM but I try to be objective, even as a KDE fan."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-22
    body: "What's your problem? If you do not like the default theme, just change it. "
    author: "matter of taste"
  - subject: "Re: KDM Theme"
    date: 2008-12-22
    body: "Then tell me how to change it using KDM 4. Like I said, it doesn't work."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-28
    body: "System Settings &#8594; Login Manager &#8594; Theme (might be slightly different, I'm on a norwegian locale).\nWasn't so hard, was it?\n(Or maybe your distro is broken? :p)"
    author: "sandsmark"
  - subject: "Re: KDM Theme"
    date: 2008-12-22
    body: "Well you can all preach like Paul an Peter, the truth is that it isn't that easy to install KDM themes, even on KDE 3.5 you have to install a theme manager before this is possible. On KDE 4 it just doesn't work! Well try to install a GDM theme - it's fool proof. I love KDE but there is a lot of room for improvement here."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-23
    body: "Yes I know. They also use their own daft window decorations for no apparent reason whatsoever."
    author: "Segedunum"
  - subject: "Re: KDM Theme"
    date: 2008-12-23
    body: "In all honesty, the window deco is terrible. Look how elegant and beautiful the original Oxygen deco is and look at what openSuse offers. It's the very first thing that I change after I log in for the first time.\nOpenSuse is the best KDE distro out there - no doubt about that and it's installer is presently unbeatable, hats off to the guys who worked on it (if onle the other themes were that beautiful) but since this distro changed from SuSe to openSUse it hadn't designed a boot screen, a KDM theme and a start up screen that makes me say wow.\nAnd for those who think that I don't have taste, take a look at PCLinuxOS. it can be done."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-25
    body: "Ehhh, not for KDE4 they do. They use the Ozone decoration which in fact is the default KDE4 decoration. "
    author: "Morty"
  - subject: "Re: KDM Theme"
    date: 2008-12-26
    body: "But they coloured it with blue using white buttons, which doesn't look good at all. It just doesn't fit, green everything but blue Window deco - terrible. It (the openSuse modification) can't be compared to the original Oxygen decoration."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-26
    body: "It is not a OpenSuse modification, but they should probably do something with the blue. Open the window decoration control panel and check the \"Blend title bar colors with window contents\" option, and you are good to go.\n\nBut it is not Oxygen, it's Ozone the default window decoration for KDE4. Oxygen newer was default, both on account on the problems to distinguish between active and inactive windows on non composite systems and the inability to follow the KDE window decoration standard with ability for the window decoration to follow the color scheme.  "
    author: "Morty"
  - subject: "Re: KDM Theme"
    date: 2008-12-27
    body: "I did that, actually that's one of the first things that I change immediately after the installation.\n\nWhat's the big difference between Ozone and Oxygen? They both look the same to me."
    author: "Bobby"
  - subject: "Re: KDM Theme"
    date: 2008-12-27
    body: "Nothing, expect for Ozones ability to follow the KDE color schemes window decoration color. Adding greater distinction of windows and making it better usability for people with non composite systems, low end displays and vision problems."
    author: "Morty"
  - subject: "Advertisement on the dot?"
    date: 2008-12-20
    body: "This article sounds very much like marketing speach. Something I would expect in an advertisement, but not on the dot.\n\nIt's OK to announce new distribution releases here, but please avoid marketing speech like \"The award-winning release...\". Kubuntu Intrepid Ibex has been released several weeks ago and also kontains KDE 4.1.x. And I am sure that it also has won some awards.\n\nPrimarily the dot should be around KDE, not about distros.\n\nJust my 2c"
    author: "Carsten Schlipf"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "Well, given that PackageKit is gaining more traction, custom stuff like KWin and PowerDevil backports are new stuff that isn't available in most other places, and also regarding the fact that Novell/openSUSE is the most active corporate contributor to KDE (with openSUSE being the most polished KDE distro at the moment), I think there's enough meat and justification to publish such an article.\n\nI for one, as Kubuntu user even, love to read about what openSUSE is doing with KDE, and I'm totally in favor of this article being published here on the Dot. This is nice stuff that I want to know of."
    author: "Jakob Petsovits"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "Where do you get the opinion from that suse is the most active corporate contributor? And the 'most polished' opinion is surely your own and not shared all over.\n\nAnyway, the point is not that a user might want to know about this, but the point is that the dot is not an advertisement platform for certain distro's that happen to have some friends in the dot moderation team."
    author: "Dave"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "If you look at the number of KDE developers employed by Suse, you'll see why they can be considered the most active corporate contributor. At least compared with other distro's. Nokia and KDAB also employ many KDE developers, and there are other parties out there, but Suse ranks among the largest.\n\nAnd about the polish, they use these resources for polish of KDE. And for back porting as well, which has its downsides, but the end result is that I certainly believe suse 11.1 to be the best KDE distro out there.\n\nAnd I have no problem with positive stories about excellent KDE focused distributions on the dot. It's KDE advertisement, and that's what we do here."
    author: "jospoortvliet"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "Well, packagekit came from Fedora/Red Hat and Fedora 10 was (I think, may be wrong) the first distro to use kpackagekit.\n\nPersonally I don't think the dot is the place for distro release announcements - that's distrowatch and the distro websites themselves - but hey, whatever. I don't really care too much one way or another.\n"
    author: "Simon"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "I think it is important, at least when it comes to major distributions."
    author: "Andre"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "Then the dot is serving (some of - you at least) its readers well by publishing this stuff :-) I guess some people didn't care for the story on Qt4.5 beta either (random non core kde example). Although that one was interesting to me."
    author: "Simon"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-22
    body: "the dot is the perfect place for distro's to show off their KDE releases.  And that is what this article is doing, show casing KDE"
    author: "R. J."
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-28
    body: "As others have said, packagekit comes from the redhat guys, and powerdevil (both the app and the backporting) was done by Dario Freddi, which also works as a packager for the KDEmod project on Arch Linux (we also had the kwin backports for quite a while before opensuse shipped with them, for example the Cube backport was done by Mandriva, iirc.)."
    author: "sandsmark"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "\"Kubuntu 8.10 Brings KDE 4 to the Masses\" is not marketing speech?"
    author: "Anonymous"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "It was simply true: Kubuntu 8.10 is the first distribution using KDE 4 as default.\n\nAnd this article had a length of 6 lines. And compare the text style. Truely different: http://dot.kde.org/1225379191/"
    author: "Carsten Schlipf"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "Actually there was a release of openSUSE 11.0 with KDE4 as default[0] back in June, as well as other distributions. So Kubuntu 8.10 was not the first.\n\nAs for \"Award winning\" that is \"Simply true\". It did indeed win an award, as cited in the article.\n\n[0] http://download.opensuse.org/distribution/11.0/iso/cd/openSUSE-11.0-KDE4-LiveCD-x86_64.iso"
    author: "Benjamin Weber"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "Mandriva 2009.0 bring KDE to masses before Kubuntu. They have no advert on the dot and they contribute more to KDE than Kubuntu."
    author: "kinesthetic"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "Assuming there was no rejected dot submission from Mandriva community about its release I see no reason for you to complain."
    author: "Anonymous"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-24
    body: "The dot publishes, in some form, almost all news submissions that relate reasonably well to KDE.  The difference here is that one a few distros submit news about their KDE work."
    author: "Troy Unrau"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "If by \"the masses\" they meant 3 or 4 people, then yes, that's true."
    author: "donald"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "I also have big problems with this post; are there suse backers that have a hand in chosing which articles go on the dot??\n\nThis article surely isn't helping KDE in any way.  A distro saying how great they are for shipping loads of unreleased code from hard working KDE people is not just misplaced on the dot. It is bad enough that suse claims to ship a better-than-kde release with alpha-quality code, to have this on the official KDE publishing channel is really poor practice.\n\nWill the abuse never stop :("
    author: "Dave"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "Get over yourself.  \n\nYou haven't complained when the dot ran release notices for fedora or ubuntu. \n\nthe dot is about what ever they want it to be about.\n\nThe abuse will stop when you stop abusing people in the name of the silly little agenda you are trying to carrying out."
    author: "R.J."
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "I really can't agree at all. You can't claim it's a SuSE conspiracy because articles for other distros are pretty much always published for new versions as well (even rather small distros), unless thats also part of the conspiracy to throw people off the scent... (the devious jerks! ;)\n\nAt least from my reading of it, the only thing that could *qualify* as marketing speak is the single adjective (adjective phrase?)'award-winning' which, given that the vast majority of the article is practically just listing the updates, I don't think it's reasonable to get worked up over.\n\n>Will the abuse never stop :(\nSorry to say, but IMO, the only people being abused here are the OpenSuSE people/article submitter/Dot editors."
    author: "Kit"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "You should do your research.\n(i) distribution releases on the dot are on the whole extremely useful (as a conclusion from the majority of dot readers and from the dot editors), \n(ii) one or two people _always_ complain with every distribution release article, \n(iii) this article _is_ helping KDE as people are finding out the latest ways to have the KDE platform on their computer, \n(iv) the SUSE KDE people are _all_ KDE developers, mostly core developers, \n(v) the decision to backport changes is never taken lightly -- it's curious that the only people who ever complain about this however are those not working on shipping a usable distribution. Releasing a high quality distribution with KDE 4 is not trivial; backporting things with KDE 4 at this current stage is a simple fact, and all popular distributions are doing it. If one particular backport is not working then I would love to hear it, otherwise I'm not sure why you're complaining."
    author: "Francis Giannaros"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-24
    body: "Gotta love that quote: \"Releasing a high quality distribution with KDE 4 is not trivial\". I dare say it has not been done, because it is not yet doable.\n\nThe thing that struck me about the article is that phrase \"brings back KDE-PIM\". \n\nWhen was it gone? Did the last openSUSE really have no Kmail in it? I can barely imagine that. That would be definitely too much blood on that edge for me.\n\nAnd one other thing that strikes me is the implication that distributors should be or even have to be sponsors of the development. Creation and distribution can be done by the same people, but it doesn't have to be that way.\n\nOther than that, I guess since the articles for the releases are written by the respective community members, that they must be expected and allowed to have a spin to them.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "openSUSE really puts out amazing KDE packages.  It may sound like marketing speak (like most distro press releases) but I think the praise is well justified in this case."
    author: "T. J. Brumfield"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-20
    body: "I agree 150% ;-)"
    author: "jospoortvliet"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-22
    body: "Agree.\n\nI'm still using openSUSE 10.3 and both 11.0 and 11.1 are now out. Regardless, KDE support is fantastic! Both, the stable and unstable branches. I just moved from KDE 4.1.3 to KDE 4.2 Beta 2 and am really happy with it. \n"
    author: "Jad"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "I'm sorry you see it that way, but I disagree with your approach on several counts. For one, Ars Technica is IMHO the most respected Linux reviewing website out there; they do not give awards as a whim, and the KDE 4 delivery of openSUSE got a special mention. That this release won an award was only mentioned to highlight the fact that this is a _particularly good openSUSE release_, and in that sense it can be distinguished from say some previous openSUSE releases.\n\nI made no comparison to Kubuntu, did not say that it was an \"award-winning distribution, unlike others\" which you seem to apparently think. I'm not making this into some sort of competition; I would much rather be in an environment where we work together.\n\nSince you think the whole article sounds like marketing speech, I'd be interested in hearing what else is contentious use of language for the dot. I've been reading the dot for several years, am certainly first-and-foremost a KDE user, and yet I'm still amazed by the fact that one or two people always complain when a distribution release is mentioned. The vast majority of Dot readers, and the Dot editors, approve of these stories and see them as quite useful. I wish some people would just get over it."
    author: "Francis Giannaros"
  - subject: "Re: Advertisement on the dot?"
    date: 2008-12-21
    body: "Francis, just ignore it.  It was a great article.  It's just a shame that some people still have hissy fits when their distro isn't mentioned.\n\nThank you for the article Francis."
    author: "R. J."
  - subject: "Impressive"
    date: 2008-12-21
    body: "I installed 11.1 yesterday and am really impressed. KDE 4.1 IS ready for production use - only on Kubuntu one doesn't get that impression...\n\nI've been using Debian or Debian-based distros (Kubuntu, Sidux) for years now and I'm still a bit sceptical about RPM (I can painfully remember dependency nightmares on Suse 6.0 that made me switch to Debian in the first place). But so far, package management works like a charm - adding the Packman repository went smooth, at least.\n\nAnd the desktop experience is way better than on Kubuntu, everything seems to fit together (but I guess you can't blame the Kubuntu folks, they sure don't have the manpower Suse has).\n\nCongratulations for creating the best KDE (4) distro out there!"
    author: "Sepp"
  - subject: "Re: Impressive"
    date: 2008-12-21
    body: "Dependency hell still exists Sepp but one gets used to it - just joking ;)\nI am presently using openSuse 11.1 running KDE 4.2 and you wouldn't believe that this KDE is still Beta, really smooth and stable, only that my little red ball is not working :(\n\nThe only problem that I am having with 11.1 is that I can't install my Digittrade DVB-T Stick. It was no problem on 11 but I keep getting an error 2 (Fehler 2) message while trying to install it on openSuse 11.1.\nIt's really frustrating because I don't want to reinstall openSuse 11.\n\nIs there any coder here who could give me one or two tips on what to change in the install script so that it can be installed on 11.1? It's an openSuse 11 driver so I guess it's because of the new kernel.\n\nKUbuntu isn't as refined as openSuse but it's really not bad, it's coming along slowly but surely.\nWell yes, I have to agree that openSuse is the best KDE distro out there and it keeps getting better:)"
    author: "Bobby"
  - subject: "Re: Impressive"
    date: 2008-12-21
    body: "perhaps you should go see a dr about your little red ball not working ;)\n\nI agree about KDE 4.2  I have been using for a few months now.  It's just coming together so perfectly now, and it is hard to see what will happen with 4.3"
    author: "R. J."
  - subject: "Re: Impressive"
    date: 2008-12-22
    body: "Well, I did that but the doctor said that I should be a bit patient and promised to fix it with the next visit (update), which will make me appreciate it even more ;)\n\nI love KDE 4. Yes there are still a few things to fix but it's coming on very nicely. I can't overemphasize how impressed I am by the industriousness, energy, determination and ambitiousness of Aaron and his crew. Big up!"
    author: "Bobby"
  - subject: "Kubuntu and OpenSuSE."
    date: 2008-12-22
    body: "You are absolutely right about Kubuntu. It makes you hate KDE. I installed KDE 4.2 beta 2, only to burn it 30 minutes later, wishing to return to GNOME, after a conflicting-PowerDevil, no-Akonadi, and unusable session.\n\nAlso, my session revealed that something is dead wrong with my translation (Spanish). It is so wrong, that I'm willing to fix that by myself. Just tell me how can I cooperate with my translation team and I'll improve that translation."
    author: "Alejandro Nova"
  - subject: "Re: Kubuntu and OpenSuSE."
    date: 2008-12-22
    body: "working great here... I see no diff between my suse and my kubuntu running kde 4.2 beta2...\n\nI just prefer debian like distros..."
    author: "gnumdk"
  - subject: "Re: Kubuntu and OpenSuSE."
    date: 2008-12-22
    body: "I also installed KUbuntu on another partition (just to test it) and it's running fine - not so refined like openSuse but it's good to go. There are even a few things that they enhanced that openSuse didn't: for example, I can see my hard drive partitions in Dolphin, which I have to add manually in openSuse. I also like the quick start folder right beside the start menu, it's very convinient.\nI think that the problem with KUbuntu is that it has gained a bad name over the years and now it's hard to get rid of that even though the devs are really trying.\nI am speaking as a fan and user of openSuse."
    author: "Bobby"
  - subject: "Re: Kubuntu and OpenSuSE."
    date: 2008-12-22
    body: "I can see my Hard Drive Partitions, including my vista partition in Dolphin without having to add anything in opensuse 11.1"
    author: "R. J."
  - subject: "Re: Kubuntu and OpenSuSE."
    date: 2008-12-23
    body: "Really? Did you have the partitions at the left side where Home, Network, Root (the red folder) and Wastebin are by default or did you add them at some point?\nI even created a new user to see if it had to do with my previous settings but I have the same result. No partions listed at the side (like in KUbuntu) by default. Of course I can find the partitions but they show up as folders and not with hard drive icons."
    author: "Bobby"
  - subject: "Re: Kubuntu and OpenSuSE."
    date: 2008-12-24
    body: "Then I'm going to give it a go when KDE 4.2 final is released, or when April comes, with Jaunty ready. Let's give time to the Kubuntu crew.\n\nI didn't intend to bash Kubuntu. Maybe I installed it from Ubuntu and not from itself, and that was the problem. Maybe I was running kubuntu-experimental. I will definitely bash my translators, because you can't put something like \"Configuraci\u00f3n avanzada de los efectos\" in one line, in the white sidebar of a preferences dialog: it makes you lose screen state (in KDE, because of the beautiful Qt resolution-aware drawing stack, I get bigger fonts, and less space, so everytime I see a preferences dialog, I curse the translators because the drawing stack makes me lose even MORE space). But I'm not going to bash Ubuntu, or Kubuntu for the matter.\n\nI installed Intrepid Ibex in my tx1000, a certified Linux-unfriendly notebook, designed from ground zero to run solely with Windows Vista. And the Ibex made everything work in no time and with mostly free software (including Broadcom wireless (working with b43 and fwcutter, flawlessly) NVIDIA video (177.82, working, with StR, StD, TV-out), a touchscreen that was a nightmare to configure before the Ibex (working almost flawlessly with evtouch, thumbs up to X developers!), and more hardware hated by all Linux lovers). How could I bash the Ibex! I recommend it! ;)"
    author: "Alejandro Nova."
  - subject: "Re: Kubuntu and OpenSuSE."
    date: 2008-12-26
    body: "The most important thing about KDE4.1.3 on opensuse vs. KDE4.1.3 on Kubuntu is that (I'm told) opensuse contains crucial improvements that have been backported from KDE4.2.  That means they're not in Kubuntu yet, but they will be eventually.  I can name two big improvements that just make all the difference to me. These two improvements are :  \n\n1. The filter bar has been restored to Konqueror, making it more useful as a file manager.\n\n2. The bugs have been fixed in the \"Action Inputs\" interface, allowing working custom keyboard shortcuts in KDE for the first time that I am aware of.\n\nTaken together, these two improvements in particular not only justify my choice to use opensuse instead of Kubuntu, at least for the moment, they have caused me to revise my previous out-of-my-cold-dead-hand adherence to KDE3.  At least for my media box, I'm a KDE4 user now.\n\nUnless you have special software needs, I think you'll find that opensuse is pretty sufficient for resolving dependency issues although it's not always easy getting more obscure software.  With Debian and *buntu, I get a kick out of being able to install fun stuff like, say fvwm-crystal, with a single command.  It's that kind of freedom of choice that will probably have me back using Kubuntu eventually, some time after KDE 4.2 comes out."
    author: "blackbelt_jones"
  - subject: "updating from OpenSUSE 11.0"
    date: 2008-12-22
    body: "Any reports? I remember reading that its possible without burning a disc.\n\nIs wiping / just the best way?"
    author: "Ian Monroe"
  - subject: "Re: updating from OpenSUSE 11.0"
    date: 2008-12-23
    body: "There's a very to upgrade from a previous version to 11.1 without reinstaling or using a CD/DVD, but last I heard it was Very Much Not Recommended Way and (from what I heard), likely to lead to lots of problems (I ran into many dependency issues that I was eventually able to work out).\n\nBasically what I'd do is switch all the sources to being the version that you want to upgrade to, then ask Yast to upgrade everything if there's a newer version available. You'll likely have to work through a lot of dependency issues Yast can't auto-figure out but it worked for me a couple times. It *might* be better to tell Yast to simply update EVERYTHING, even if a newer version isn't available (never tried that way before)."
    author: "Kit"
  - subject: "Re: updating from OpenSUSE 11.0"
    date: 2008-12-23
    body: "add the repositories for 11.1 \n\ngo into terminal and type\n\nzypper dup"
    author: "R. J."
  - subject: "Ugly Pimple"
    date: 2008-12-23
    body: "I see OpenSuse got rid of that ugly cashew in the corner. Very nice. That alone is enough for me to try it out."
    author: "Clifford"
  - subject: "Warning: OpenSUSE's Windows Installler"
    date: 2008-12-24
    body: "I made the Mistake to try to install Opensuse with the Windows-autorun-installer (Instlinux) on the DVD. It changes Windows' own Boot Sequence, inserts itself into the Windows Bootloader and adds an entry to install Opensuse at startup. Problem is it doesn't know how to treat the new Vista Bootloader right and you can't start Vista afterwards.\nThe problem has nothing to do with Grub or the like. I had Opensuse 11.0 installed before, so at startup there's grub which has the entries 1. Opensuse 11.0 (works) and 2. Windows Vista. If you select Windows Vista, there's a new subscreen from Windows, with the entries a) start Windows Vista (doesn't work, does the same as b)) and b) install Opensuse 11.1 (works, starts the 11.1 installer from c:\\openSUSE something). So no way back into Vista.\n\nI spent the next six hours trying to fix the problem without a reinstall. I learned more about the new Vista Boot process (different from XP) than I ever wanted to know. (The installer should finally do that too). So I needed a Vista install DVD, which I didn't have, because you only get useless Recovery DVDs these days (even had to burn them myself). So after getting my hands on a rescue CD and several attempts to repair Vista, I managed to repair the Vista boot store (not a text file anymore) and get rid of the openSUSE installer using trial and error. At the next startup I was greeted with the uninstaller which did finally the right thing.\nAs it seems the installer is part of Opensuse since 10.3. So since 1 1/2 years it must have been trashing Windows installs of curious Vista users, as there are many reports of other people. I wonder how this is still unnoticed respectively why the installer is still there and autorun on the DVD breaking stuff.\n\nAnd just a quick note that KNetworkManager still doesn't work. Had to change to ifupdown to connect to the net. Also you can't change the network with KNM (not the right one set during install), so had to use ifupdown anyway. I wonder what KNM is good for, since it is also known for not working properly since a long time.\n\nAnother problem is that Opensuse 11.0/11.1 doesn't work properly on the processor, an Intel Core 2 Duo T9300. Even when not in powersaving mode it clocks the processor down and all actions like copying files, actions using the CPU, downloads and network or ftp connections come to a halt and begin to timeout/get dropped until you wake the notebook up e.g. by using the touchpad. Found that a problem also on recent Kubuntu and Debian Sid releases, so might be a kernel problem, Vista works right on the CPU.\n\nIf you wan't to make comments like \"nobody needs Vista anyway, what's the problem\", I can tell you it's the OS I had to use because it worked when openSUSE 11.0 didn't. Apart from several minor things (and the processor problem) I couldn't use the KDE desktop at all because it was so unbearable slow (nvidia graphics, but with both standard and nvidia drivers). Entering a folder or opening a file took several seconds because there was a fade animation on the icon which had to be completed first.\n\nWhile I'm at it, many thanks to nvidia (not) for finally packing the new driver for opensuse after 4 months of procrastinating.\nThe driver has been out since early August but only now with the release of the next Opensuse Version it's the right time to fix the previous release (ftp://download.nvidia.com/opensuse/11.0/x86_64/). It was hailed as a breakthrough but not provided to the suffering users. Apparently fixing their stuff twice a year is enough, en passant while they are putting out the drivers for the next release 11.1. Or maybe they were just so kind because it's christmas time, who knows."
    author: "Anon"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2008-12-24
    body: "That's unfortunate that the installer don't like the Vista boot loader, could be a good idea to notify the OpenSuse team(bug report). That way there are hopes the issues can get fixed and they can add some warnings on the OpenSuse website to warn other users.\n\nThat said, I don't think the installer is used much as most prefer to boot from the DVD/CD when installing. Since most machines having Vista also have bootable DVD/CD.\n\nI have the installer a few times when installing on machines without DVD, and old BIOS not able to boot from external USB DVD drive. Only on XP and Win200 machines, but it has worked flawlessly every time."
    author: "Morty"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2008-12-24
    body: "You should be able to boot to the Vista DVD, go into a recovery console and fix the MBR within 10 minutes.\n\nThat being said, once openSUSE is done installing, it installs grub into the MBR and grub can boot into Vista without problems."
    author: "T. J. Brumfield"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2008-12-25
    body: "\"grub can boot into Vista without problems.\"\n\nIn case you didn't read the post you replied to: GRUB loaded Vista's bootloader but the installer screwed up Vista's boot menu so that it couldn't boot Vista."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2008-12-27
    body: "I dual boot, Vista, and OpenSUSE, currently running OpenSUSE 11.1, and have no problems with this.  I suggest you file a bug report and see where it goes.  I have yet to hear of any other vista users having this problem.  And personally, I am running KDE 4.2, and find it to be amazingly fast.  So again, maybe it is unique to something you are doing, or to your machine.  "
    author: "anon"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2009-01-04
    body: "How exactly did you fix this problem then, because i have the same problem i can't boot linux or vista cause the installer is corrupted and the recovery DVD for vista is no use it has the option to fix the problems on start up but it doesn't think anything is wrong. System recovery does nothing apparently except delete files that i can no longer access. I am in risk of losing everything on my computer just because linux installer doesn't work and seemed to be the dominant force that has broken my computer. I have searched for this error and none of them seem to be close to the problem i have. Error 17: File not found then it tells me i have a fictional partition of my hard drive which I don't and it offers me the chance to view the 3 files that are attached to it and edit them in the BASH terminal. However what i would prefer to do is remove this tumour us  problem from my computer and go back about my business. If anyone could spare the time to please find a quick and practical solution to my problem (Taking my hard drive out and copying its contents onto another machine before reformatting isnt considered to be quick or practical) then I would be eternally grateful. \n"
    author: "Man in trouble"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2009-01-13
    body: "Hey, I have the same problem you did. After trying to install opensuse 11.1 with the autorun-boot-loader, my vista partition failed to boot/work. I have tried using the vista recovery cd (from neosmart) but it even fails to recognize my vista partition (since it can't recognize the partition, it doesn't repair it). Could you tell me what exactly you did to get vista to work again? Thanks."
    author: "AGS"
  - subject: "Re: Warning: OpenSUSE's Windows Installler"
    date: 2009-01-13
    body: "Hey, I have the same problem you did. After trying to install opensuse 11.1 with the autorun-boot-loader, my vista partition failed to boot/work. I have tried using the vista recovery cd (from neosmart) but it even fails to recognize my vista partition (since it can't recognize the partition, it doesn't repair it). Could you tell me what exactly you did to get vista to work again? Thanks."
    author: "AGS"
---
openSUSE 11.1 is <a href="http://news.opensuse.org/2008/12/18/opensuse-111-released/">now out</a> (<a href="http://en.opensuse.org/Screenshots/openSUSE_11.1">screenshots</a>), featuring KDE 4.1.3 and a <a href="http://news.opensuse.org/2008/12/18/sneak-peeks-at-opensuse-111-kde-in-opensuse-111/">string of KDE improvements</a>. The release brings back the much-loved KDE-PIM suite, and includes new games, the KSCD CD player, KSystemLog to keep track of system changes, improvements to Dolphin, Konqueror (including Webkit part), Plasma (including auto-hide panel, folder view), Marble integration with OpenStreetMap, and much more. The release is available as an installable live CD, or on a DVD with KDE 3.5.10, GNOME, Xfce, and many more applications.


<!--break-->
<h4>KWin Composite Effects</h4>

Of particular interest are the <a href="http://www.kdedevelopers.org/node/3747">KWin improvements</a> in this release. openSUSE users now have a number of backported KWin effects to enjoy, and show off to their friends. If KWin effects arent your cup of tea, you can use the new Compiz KDE configuration module to enable and handle <a href="http://opensuse.org/Compiz_Fusion">Compiz Fusion</a> on KDE 4.

<div align="center"><a href="http://files.opensuse.org/opensuse/en/thumb/d/d1/Kde4-cube.png/800px-Kde4-cube.png"><img src="http://files.opensuse.org/opensuse/en/c/cc/800px-Kde4-cube-thumb.png" border=0 /></a><a href="http://files.opensuse.org/opensuse/en/thumb/1/10/Cover-switch.png/800px-Cover-switch.png"><img src="http://files.opensuse.org/opensuse/en/9/9e/800px-Cover-switch-thumb.png" border=0 /></a></div>

<h4>PackageKit and Powerdevil</h4>

KDE has now also <a href="http://www.kdedevelopers.org/node/3773">standardised on PackageKit</a> for its backend, which means both desktops are using the same update stack. Furthermore, <a href="http://www.kdedevelopers.org/node/3752">Powerdevil</a> is now included for easier and better power management on your KDE 4 desktop. Just click the battery icon in the system tray.

<div align="center"><a href="http://files.opensuse.org/opensuse/en/thumb/4/40/Powerdevil.png/800px-Powerdevil.png"><img src="http://files.opensuse.org/opensuse/en/9/99/800px-Powerdevil-thumb.png" border=0 /></a></div>

<h4>Other</h4>

The <a href="http://arstechnica.com/articles/culture/ars-awards-2008.ars/7">award-winning</a> release also contains <a href="http://en.opensuse.org/Testing:Features_11.1">230 new features</a>, improvements to YaST, OpenOffice.org 3.0, and more freedom with a <a href="http://en.opensuse.org/OpenSUSE_License">brand new licence</a>, Liberation fonts, and openJDK. This is also the first release built entirely in the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a>, and the last release to contain the full KDE 3 desktop available on the DVD medium for installation.
<br /><br />
Finally remember that for testing purposes, the recently released KDE 4.2 Beta 2 is available in the openSUSE Build Service, via <a href="http://opensuse.org/KDE4">1-click-install</a>. Alternatively, you can try out the release with <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a>. 

