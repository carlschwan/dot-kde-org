---
title: "Kubuntu 8.10 Brings KDE 4 to the Masses"
date:    2008-10-30
authors:
  - "jthomas"
slug:    kubuntu-810-brings-kde-4-masses
comments:
  - subject: "Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "The reason I won't upgrade is because Kubuntu has disabled the ZUI, including keyboard shortcuts. Since I need this feature (even on this Eee PC I am writing this on I have 3 activities), I can't really upgrade.\nWhy was this done? "
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "Maybe cause ZUI is more confusing than anything else at least in 4.1."
    author: "debianuser"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "I don't find it confusing at all. It actually made me become more productive. I even made a video (on YouTube at the moment) showing how it can be used."
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "It is confusing. You've got two similar concepts, multiple desktops and multiple activities, I think they should be somehow merged.\n"
    author: "dandu"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "Actually, there are plans to do so. See the 4.2 Feature Plan on Techbase."
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "10x, \"Activity<->Virtual Desktop Affinity\" sounds nice, I'm eager to see how it's going to be implemented :)\n"
    author: "dandu"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "Is there a doc somewhere explaining how to use all these must-have cant-live-without features? The ZUI is the strangest thing I've seen. You zoom out and now you have a tiny desktop up in the corner. And you can scroll around in a large empty field that has a tiny desktop in the corner. I don't get it."
    author: "David Johnson"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "This image (courtesy of Half-Left on #kde) shows what you can do with activities:\nhttp://img28.picoodle.com/img/img28/4/7/19/f_DesktopZoomm_31fc46d.png\nI made a video showing how to use the ZUI:\nhttp://www.youtube.com/watch?v=EhODrJkoidA\nThere's an activity tutorial on the KDE Forums:\nhttp://forum.kde.org/showthread.php?tid=7671\n\nHope those are enough to clarify."
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-11-02
    body: ">Hope those are enough to clarify.\n\nNo sorry, I've looked at your three links and still doesn't understand what's the difference between the 'activities' and the virtual desktop.\n\nIt looks really redundant.."
    author: "renoX"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "Seriously.  I've been following KDE4 (obviously not very well...) since it was just in the early idea stage, something like three years ago.  This is the first I've heard of a \"ZUI\" or \"Activities\".  I always thought the \"zoom out\" thing was a weird bug; I had no idea it was a reimplementation of multiple desktops.\n\nWhy can't you just check a box to have separate plasmoids for each desktop, just like you can to have separate background images?  Does the ZUI/Activities thing do anything besides creating a separate set of plasmoids and background image?  If not, it seems like a ridiculous way to do things, with the \"eye candy\" factor being the only reason to have bothered with it at all."
    author: "Darshan"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "If you look up aseigo's earlier blog posts, you'll see that there are ideas to make activities as different \"contexts\"."
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "so kubuntu now begins to remove features because they might be confusing some users? at the moment I feel more and more uncomfortable with kubuntu. ZUI is one of the key features of KDE4 and although it is very basic in 4.1, it is very useful, for me at least."
    author: "hias"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "This is nothing new, kubuntu has always removed features to \"improve\" usability. Ironically the disease in kubuntu of removing features because they might confuse some users, has created lots of confusion on it's own caused by missing functionality when experienced users and developers have have tried to help kubuntu users.  \n\n"
    author: "Morty"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "<I>emoved features to \"improve\" usability</I>\n\nSmells like Gnome and this is not a compliment.\n"
    author: "missing functionality"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "The problem is not smell, rather a unsubstansiated belief that users may get confused, and that removal of some features may migitate this and make better usability. Usability are more complex than that, requiring metodious testing and analyzing rather than such hand waving.\n\n"
    author: "Morty"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "This is basically the list of reasons why the ZUI was made inaccessible to users:\n\n* at times it puts activities on top of each other\n* upon crashes it also happens to add activities\n* it's not granny-proof, the zoom of the cashew control is horrible for people who can't navigate all that well using a mouse\n* you can easily get lost if you don't understand the concept of activites (especially because there is no 'bring me home' feature)\n* by default it doesn't add any activities which would explain the actual use of this feature\n* by default it doesn't use wallpapers, using the default transparent background instead, which looks the most horrible\n\nLuckily, most of these issues should be resolved in KDE 4.2"
    author: "Jonathan Thomas"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "there could have been at least an option to switch it on, there are some users who used this feature in hardy and can't do it now."
    author: "hias"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "I see the point of doing so. However, I think that a way to keep those available would have helped, at least to report bugs (like that, no one will since there is no way they can even use the feature)."
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "* at times it puts activities on top of each other\n\nThis NEVER occurred to me in Hardy with KDE 4.1.2 (and earlier).\n\n* upon crashes it also happens to add activities\n\nDo you mean upon crashes of plasma? If so, then this NEVER occurred to me in Hardy either.  So I assume that this and the previous point occur because Kubuntu screwed something up in the packages for Intrepid.\n\n* it's not granny-proof, the zoom of the cashew control is horrible for people who can't navigate all that well using a mouse\n\nTrue.\n\n* you can easily get lost if you don't understand the concept of activites (especially because there is no 'bring me home' feature)\n\nSince in the zoomed state there is only one activity with a cashew that contains a \"Zoom in\" button, it is quite obvious that pushing that button will do the opposite of the \"Zoom out\" button.  In my opinion, there should have been the feature that pressing the Escape button on the keyboard zooms in to the last used activity.\n\n* by default it doesn't add any activities which would explain the actual use of this feature\n\nWrite documentation instead of removing features.\n\n* by default it doesn't use wallpapers, using the default transparent background instead, which looks the most horrible\n \nA small patch would fix this.  As far as I know Kubuntu is not afraid to change the behavior of KDE (after all, you patched out the zoom), so why are you all of a sudden afraid of changing the background in the zoom?\n\nSo from all the arguments above, only one is valid, namely that the zoom is not granny-proof (the other ones can be resolved by not adding bugs and patching stuff).  Since you write yourself on the website of Kubuntu that the user needs to decide for himself/herself if KDE4 is good for him/her (and therefore if Intrepid is good for him/her) and that Hardy is still supported for another year, I see no reason at all to remove features that will only be visible to people who deliberately decide to switch to KDE4 anyway (granny is probably not going to \"risk\" installing Intrepid after reading the announcement).\n"
    author: "g"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "We have gotten several bug reports regarding the first two issues.\nWe ship pure KDE code for Plasma except for the Taskmanager tooltips we backported. There isn't any magical package error that could cause this. In short, the above problems are problems with KDE that will be resolved in the future.\n\nIn case you didn't notice, there is documentation for activities in KHelpcenter in the Plasma Manual. Most people would use the feature and go \"wtf?\" before they found documentation for the feature, except maybe if you took a png of the documentation and used it as a wallpaper. The feature is just too infantile in its current state, and people don't just go and read the manual about such things, even if they should.\n\nMaking the zoomed-out background is also somewhat non-trivial. There isn't any code in place for actually *painting* a background (hence the default transparency-checker background), so it's not a simple matter of switching wallpaper."
    author: "Jonathan Thomas"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "I actually agree with the temporary disabling of this feature, for exactly of the reasons Jonathon cited.  For some users, it can be very confusing in its 4.1.2 state.  I too used the plasma ZUI when using the Hardy and the KDE 4 ppa.    When you know what it is intended to do it can be quite nice.  But there are both bugs and credible usability issues, many of which have been diligently reported (and if you haven't reported bugs, please report them).  It already appears to be improving quite nicely in 4.2 (especially with the activity switcher plasmoid).\n\nI think Hardy with KDE 4.1 from the ppa repository served exactly the purpose it should: Expose the essentially unaltered KDE to the user community so feedback can be provided directly upstream.  Intrepid is a product release and I think the Kubuntu community correctly tried to address the expectations of their users for a released product.  I think this approach is a reasonable balance between providing upstream feedback on the core KDE experience and servicing the expectations of their users.\n\n"
    author: "Jamboarder"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "workaround for activities:\nif you already have activities you can switch them by clicking on the desktop and then pressing CTRL+SHIFT+N for next activity or CTRL+SHIFT+P vor previous activity.\n\nto create an activitiy first quit plasma by starting krunner with ALT+F2 and type \"kquitapp plasma\". maybe you have to do it several times.\nthen open ~/.kde/share/apps/config/plasma-appletsrc and look for [Containments][1] which should look similar to this\n\n[Containments][1]\nbackgroundmode=0\nformfactor=0\ngeometry=1286,0,1280,800\nimmutability=1\nlocation=0\nplugin=desktop\nscreen=0\nzvalue=0\n\ncopy and past it and increase the number from  [Containments][1] until there are no dubicate numbers. you also have to set the position of the new activity. I have a resolution of 1280x800, so I changed \"geometry=0,0,1280,800\" to \"geometry=1286,0,1280,800\". my new activity is on the right from the default one. if you add more activities you have to make sure that they dont overlap. then you have set \"screen=-1\"\nyou should now have something simlar to this.\n\n[Containments][4]\nbackgroundmode=0\nformfactor=0\ngeometry=1286,0,1280,800\nimmutability=1\nlocation=0\nplugin=desktop\nscreen=-1\nzvalue=0\n\nsave the file and start plasma. now you can switch with the shortcuts I mentioned above.\n\nhave fun"
    author: "hias"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-30
    body: "I already have activities on Hardy. Can someone confirm that by upgrading one can still switch back and forth?"
    author: "Luca Beltrame"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "yes, you can. you have to click on the desktop and then CTRL+SHIFT+N or CTRL+SHIFT+P"
    author: "hias"
  - subject: "Re: Why disabling Plasma zoom?"
    date: 2008-10-31
    body: "Thanks for posting this.\n\n@Kubuntu: this is really user-friendly... Kubuntu = linux for human beings...\n\nI am going to wait to upgrade to Intrepid until KDE 4.2 is out and hope that then Kubuntu does not decide to remove other useful features."
    author: "g"
  - subject: "KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "It's true, people don't like change. When you have a truly visionary direction, it's hard to get people on board. KDE devs: you're diluting yourselves if you think for a minute this is the case with KDE4. Right now, KDE4 resembles the bastard offspring of OS X and Vista in terms of appearance and functionality. Except, my OS X system works, and KDE4 doesn't more often than not.\n\nUsually, version upgrades mean more and/or better functionality. What we have is something that can't even do half of the things it has been able to do flawlessly in the 3.5.x branch. \"Oh but eventually it will catch up with or *maybe even surpass 3.x!*\" you say. Meanwhile, all the other desktop platforms are trooping ahead, further empowered by an infusion of development support and constantly swelling userbase thanks to the monumental /faux pas/ called KDE4.\n\nSo far all I've given is negative criticism... so what's to be done? Well, if you don't want to go the way of XFree86, the best way is to actually **listen to the power users in your community**. When we say the menu sucks, it's not because it takes time to get used to it. It's because it takes a *lot* longer to navigate than the old version, which was arguably not broken in the first place. When we say kicker is broken, it's because it has much less functionality than its predecessor... this is a step forward how? When we say Dolphin sucks functionality-wise compared to Konqueror, it's not because we just haven't taken the time to learn its nuances, it's because you've spoiled us with a million cool and useful features in Konqueror, then tried to substitute it with the KDE equivalent of Nautilus. Shame on you.\n\nIn conclusion, if KDE is to succeed on the desktop, it needs to be developed by KDE/Linux fans, not OS X/Vista/Gnome lovers. And listening to users who think Vista is super sweet or Gnome is better \"because its more easier than kde\" is the road to hell. Users that stupid are extremely fickle anyway, and will bounce to whatever's popular in that particular second. Where does that leave you? Where does that leave the real core constituency in the KDE community? In my opinion, it leaves us all up poo poo creek without a plunger."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "i'm pretty confident time will prove all your nonsense wrong.  "
    author: "tooth"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Re: KDE4 as it currently exists has no future.\nby tooth on Thursday 30/Oct/2008, @13:31\ni'm pretty confident time will prove all your nonsense wrong. \n\n~~\n\nWhich part is nonsense exactly, Mr. Blind Fanboy? I'm confident that if anything, time will prove me right. Either KDE will shift its direction to something that makes sense, or it will ultimately wither."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "call me blind from one sentence, after 4 paragraphs of narrow-mindedness.  i'm content to wait and see."
    author: "tooth"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "You are a minority, mister. Most people do like KDE 4 as it is, and with the direction it has taken.\n\nAlso, you completely forget that KDE 4 is not kdebase, is only a part of it."
    author: "suy"
  - subject: "No. The silent majority!"
    date: 2008-11-01
    body: "A Minority? Not at all!\nThe silent majority. \nIn our little office there are 5 developers using kde for years. And none \nof us will ever switch to kde4 as long as there won't be a big change \nregarding the whole direction the kde development had taken.\n\nReading the dot or aseigo's blog - months ago, we simply gave up!\n\nHonestly, its already a common saying '...like KDE4' if you are kidding \nabout crappy software. - sadly\n\n"
    author: "Martijn"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "The guys from the Vista team claims similar things about their products... "
    author: "Romanujan"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "One only has to look at the SVN stats for KDE vs other desktops (assuming he means Free desktops) to see precisely which one is \"trooping ahead\".  This is just another in a long line of posts making the same central mistake: Assuming that things are the way they are now \"by design\" (with the implication that the KDE devs are explicitly ignoring [power] users' requests) rather than as a result of the extremely gruelling port to Qt4 coupled with a complete re-write of the desktop portion.\n\n"
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "I agree with the parent. KDE4 is broken. Not just the codebase, but the whole mentality. It is embarrassing to have a desktop that is so clearly a poor counterfeit of Vista and Leopard, especially since KDE used to be arguably the best desktop environment around.\n\nAnd here's to all the people making excuses for the way things are: YOU'RE NOT HELPING. A healthy open-source effort needs to take negative criticism into account as much or moreso than the positive criticism. You can disagree, maybe to you, KDE4 is your ideal desktop. But excuses just make you look foolish, and they make the KDE team look ignorant and inflexible.\n\nBest,\n-Ben"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "That's /exactly/ what I was trying to say, though much less elegantly (and a lot more angrily, unfortunately). I don't feel like the KDE team listens anymore."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "If you use KDE4 as it comes with SUSE, it looks mostly like KDE 3, not like OS X or Vista.\n\nThe codebase is not broken. Where do you see it broken ?\n\nI think we all (many) agree that KDE 4.0 was basically a beta, and 4.1 can be considered the first real release. We could have given them other names (we discussed that lengthy), this is what came out. \n\nKDE4 is a huge step, and really on the way to be that much better than KDE3 was. I can't really say a lot about Windows and Gnome, I almost never use them. Windows - well, nothing fancy. Vista - looks nice. Gnome - kinda boring. OS X - looks nice, has some nice things, but overall I don't understand why people claim it would be by far the most user friendly OS.\n\nAlex\n"
    author: "Alex"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Well Alex, that probably has something to do with the fact that it is."
    author: "illogic-al"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Here is the great contradiction of your post.  On the one hand you demand the right to express negative criticism, even saying it is a healthy thing (which is probably true if it's constructive as opposed to destructive).  But on the other hand, you accuse \"all\" those who like KDE4 as making excuses for the way things are.  So your viewpoint is correct and all those differing viewpoints are wrong?  Could it be that many people genuinely like KDE4?  Could it be that more people like it than not?  A word of advice for you.  You have every right to offer criticism, especially constructive.  But when you demean others for having a different viewpoint, you come off looking like nothing more than a petty complainer.  And nobody likes complainers, except for other complainers."
    author: "cirehawk"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-27
    body: "Yes, I agree with you.  In contrast to 3.x, KDE4 is nothing but a piece of crap.\n\nI'll still use KDE 3.5(shipped with Fedora 8) in the very future. I'll switch to gnome, or anything else who doesn't like a bastard of Vista and MacOS.\n\nKDE4 will die, I bet."
    author: "kdefans"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "I have just upgraded from Kubuntu 8.04 to 8.10, and I find KDE 4 *very* hard to use. It is not at all intuitive; for most of the things I want to do as a developer, it takes more clicks; I cannot move icons around on my panel (there is surely some way, but it certainly is not obvious to me); the panel documentation talks about clicking on certain \"icons or features\", but since most icons don't have info bubbles and the doc doesn't include icon images, it is very hard to read and understand.\n\nFor now, I am staying with KDE 3.5, which is a great desktop.  If I am forced to upgrade someday, and you keep your current metaphor in KDE 4, I'll reluctantly go back to GNOME.\n\nFor me, your new desktop is much harder to use than KDE 3.5 (my favorite), or Vista or Mac OS X, and it is not as good looking for my taste.\n\nI hope you pay attention to some of the comments I have seen posted here.\n"
    author: "Kern Sibbald"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "It's actually very easy to do, just not completely obvious.  On the right handside of the panel press the plasma button, suddenly the panel opens up.  Now you can do things like resize panel and move icons etc etc.  Press the Red Cross when your done.  \n\nBear in mind that KDE 3.5+ has more functionality than KDE4 but took many years to get there.  KDE4 is a complete rewrite and you will find that in January with KDE 4.2 a MAJORITY of the things you are missing from KDE 3.5 will emerge.  There is a section linked in kubuntu release that lists the HUGE number of things in the pipeline for KDE 4.2\n\nI kind of compare this whole flame war to people upgrading from XP to Vista (not that I like having to use either!).  There is a paradigm shift and it takes a while to get used to it.  \n\nKDE4 has gotten better and better in just the last 6 months.  In 12 months I can't wait to see where it's at.  Just think, people who don't like Vista will have to wait 3-4 years for Windows 7 to see any possible changes.  We get to see radical change happening every few months!\n\nI like some things better in KDE 3...but to be honest it took a good menu (thanks Suse for Kicker) and a lot of my own tweaking to change it from looking cartoonish and dated to something suited to 2008/2009.  KDE4 takes very little of that for me.  Also just wait until the Raptor menu is incorporated into 4.2\nTHAT will knock everything else for dead.\n\nmy 2 cents"
    author: "Andrew"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "\"Also just wait until the Raptor menu is incorporated into 4.2.  THAT will knock everything else for dead.\"\n\nWith all due respect to the Raptor devs, I really wish people would stop hyping it so much.  It spends probably about 90% of the time with little to no development activity, and so far just seems to be Yet Another Menu.  Frankly - if people haven't been wowed by the far more complete Lancelot, yet, then they won't be wowed by Raptor.  "
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "IAWTC\n\n3.5 is unique, mature, highly functional and productive, and perhaps most importantly of all it presents useful defaults but is infinitely supple. In other words I've considered kde3.5 the best DE available (out of any option) because it rewards experimenting with it and makes 'play' easy and fun. This puts discovering the perfect DE experience suited to the individuals need and taste more within reach and more practical. \n\nI know it didn't get to be like this overnight and similarly kde4 will take time to mature. In fact this is the only reasonable counterpoint.\n\neverybody expects this. There is one problem, but it has two parts:\n\n1) we aren't being appraised of exactly where kde4 is in this process. Dont rush people onto a development platform. Linus states that \"if you want people to test tell them it's a release\", but the kernel is already complete and most of these version rev's are including different software. With KDE4 we have libraries and a meager shell. When compared with 3.5 it is shockingly devoid of features. \n\nOk so you want to keep peoples interest as you rebuild? You've got to offer them a vision of what it _will_ be like eventually.\n\n2) is, very simply, when you offer people your vision and they say 'that sucks, this is why' take their words into account. This is the free software way, isn't it? Maintain a bold forward looking vision, but take practical almost cautious steps toward that goal, always checking with the community and getting feedback. Making the process as open to micro-improvement as possible. \n\nBasically I get the sense that kde4 is trying to be revolutionary instead of evolutionary, and is leapfrogging the concerns and interests of people like me in the process.\n\n\n\nSo hey these things might already be happening, but I wouldn't know because I stopped paying attention several months ago. However if they are please give them more organization and spotlight so you can beat me over the head with them.\n\nregards,"
    author: "khaije1"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "should read:\n\nLinus states that \"if you want people to test tell them it's a release\", but the kernel is already complete and most of these version rev's are including different __features__."
    author: "khaije1"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Long before KDE 4.0 I was reading about all this revolution, new wonderful features, better application integration... and at some point, after few disappointing alphas/betas the devs started claiming that \"KDE 4.0\" is not \"KDE 4\" - what is it (\"KDE 3\", \"KDE 5\", ...) - it wasn't really explained. After the release of the \"stable\" code we could easily see that, \"KDE 4.0\" is really \"KDE 4 early beta\", without most of the 3.5 features. To this date every KDE X.0 release was a really nice improvement to the end user (I clearly remembered using 2.0 beta on my workstation - it was such a nice leap forward, that I didn't want to go back to stable 1.x, despite some occasional crashes!). Oh well - it was supposed to be better in KDE 4.1, which was meant to be aimed at normal user.\n\nKDE 4.1 is out. And what? Just the next disappointment, still most of the 3.5 features missing - very configurable Panel, and more; even the printer configuration is not there! Most of the applications are not ready yet, you have to keep either 3.5 or Gnome libraries in your system to be able to use i.e. some stable, modern DVD burning software. Still no \"play\" button appearing on the MP3 file icon after moving a mouse cursor over it, still no good Nepomuk support (to remember, that this file was downloaded form that Internet site), Kopete still crashing, etc. It's not about time needed to change the paradigm - it's about a desktop which is not yet ready for a paradigm change and at the same time inconvenient to work in an old way.\n\nAnd now we are being told to wait for the KDE 4.2... Well, basing on the experiences with 4.0 and 4.1, I am skeptical, you know. I already have one Vista installed on my machine, and I don't really wont to have another one. I will probably eventually switch to KDE 4 (or move to Gnome - the more outdated KDE 3.5 becomes, the more serious this possibility is...), I would just like it to happen because of better usability and new features, not just because KDE 3 is no longer supported and more and more troublesome with new system libraries..."
    author: "R."
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "\"claiming that \"KDE 4.0\" is not \"KDE 4\" - .... it wasn't really explained.\"\n\nWhat you call claiming, is stating of a simple fact. And it has been explained over and over again. KDE 4 are and will be the whole series of releases dubbed KDE 4.x. Obviously KDE 4.0 are not it, it's only the fist bit. It's like saying the first chapter of The Fellowship of the Ring are the The Lord of the Rings. "
    author: "Morty"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "Well, my 2c. I use KDE since 1997 and KDE4 is the worse for all the time. Most people who defend KDE4 are some way connected to its development so I can understand the attitude.  But I am just user and my attitude is obvious. I must switch to Gnome env because KDE4 is not usable for me as it is. I still use Kmail, Konqueror, Kopete and many other programs because I like it. But desktop itself is sorrowful. For instance, keyboard switcher was very nice for multilingual users, especially feature that allows fast switch English/Russian or English/Ukrainian by keyboard. Now I must switch trough all 3 or 4 layouts. Panel is awfully black and I can not change its behavior or even color, KNetworkManager is more stupid then ever... and so on and so on. \n\nTo be honest, I do not understand why all this s..t happened. It's a nuisance that such wonderful project degraded to such a miserable condition."
    author: "Alex Lukin"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-05
    body: "I think the title of this thread is a bit harsh.\n\nI would like to offer my user perspective and say I am very grateful we have a free open source OS in the first place! SO A BIG THANK YOU TO ALL THE HARD WORKING DEVELOPERS!!\n\nI dumped Windoze 18 months ago and started out using Gnome. Had a flawless experience for 6 months and flawless distribution upgrades. I then really liked the look of KDE 4 and tried the 4.0 release. I struggled with that for 3 months - Plasma crashing, system freezing etc. I then moved to KDE 3.5.9 and loved it to bits...although on my system, there was still the occasional crash/freeze and it did appear to run a bit slower than Gnome. And with the Google Gadgets bar I was able to replicate some KDE 4 like features\n\nI have had a look at KDE 4.1 now in Ibex and it looks nice enough but still a bit buggy as well. I wonder if the decision to cut KDE 3 should have been deferred until the 4.2 release???\n\nSo, for now, I have decided to go back to Gnome\n\nIt's great to have an OS that looks nice with 3D effects but the ability to do normal day-to-day stuff without performance issues should be the most important thing.....Vista is the perfect example of good looks but crap performance\n\nI'm sure KDE 4.1 will work well for others, just not good for me right now!\n\nAnd I am looking forward to the 4.2 release!\n\nThanks again for giving us a viable alternative to Windoze!\n\n"
    author: "Raaj"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-06
    body: "I have no connection to the development team at all. I have used KDE from the 2.x days and I am very happy with KDE 4.1.\n\nKDE 4.0 really wasn't \"Ready\" and after trying it briefly I went back to 3.5. KDE 4.1 is a different from 3.x but IMHO it is now good different.\n\nKDE 4.1+ is absolutely the future, great work Devs."
    author: "John NZ"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "I think you need to be patient.\n\nKDE 4.0 was a alpha release and KDE 4.1 is clearly no more than a beta release.\nKDE 4.1.2 shows crashes all the time and lacks important features. And it is obviously slower than KDE 3 and not very responsive (at least if you have a Nvidia card, and even without the desktop effects). Also your laptop battery might loose over 25% of its capacity because of the switch (this is what I measured). Ok... This just means that KDE 4 may not be ready to please you as much as KDE 3.\n\nThen wait! All these problems will be solved in time. Maybe in KDE 4.2 or KDE 4.3.\n\nSometimes I am also a bit worried because the developers do not seem to spend too much time on fixing the bugs and optimizing the code. I think the developers are excited about the new capabilities. Once this first stage is cleared, you will see improvements in your day-to-day experience. I believe KDE 4 has all the technology to be more pleasant and featured than KDE 3."
    author: "Hobbes"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Maybe they should have called KDE4 \"KDE-X alpha alpha\", but they didn't. It was released with a 4.0 stable build number, giving the impression of a stable release. \n\nStill, you've ultimately missed my point. While the bugs, flaws and general broken nature of KDE 4 is somewhat disheartening, the truly scary thing is the direction it's taken. It really wants to be Vista; not even Windows users generally care for Vista. It's also trying to dumb down the KDE experience, which has always had an advantage over Gnome precisisely because it /didn't/ dumb things down, but rather gave the user lots of options should they choose to exercise them. You can't out-vista Vista, and you can't out-gnome Gnome. What kind of sense would that even make anyway? If we wanted Vista, Leopard, or Gnome, we could simply opt to run those."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "As Anon nicely says, you seem to assume that things are \" the way they are now \"by design\" \". There is nothing like a \"broken nature of KDE 4\". It is not its nature. It is its temporary state.\nIn many places, I have seen: \"take care, KDE 4 may not be for you.\" And this warning is still available on Kubuntu website for the 8.10 release...\n\nEven in the limited state in which KDE 4 currently is, it already has many many features. I think KDE policy remains the same. Just wait a bit more (6 month to one year) to see KDE fully back! and actually better than it has ever been."
    author: "Hobbes"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "I really and sincerely hope you're right. I'm the type that can make due with TWM if I have to, but I'll really mourn the loss of KDE if it doesn't find its way back to its powerful roots."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "\"I really and sincerely hope you're right.\"\n\nI'm a recent dev for KDE, and am focusing on restoring some functionality to Konqueror4 that was lost as a result of the port to Qt4, starting off with features that when implemented in Dolphin would automatically benefit Konqueror (my old favourites like previews-in-tooltips, expand-folder-on-hover, etc).  Every dev I've interacted with has invested a great deal of their time and energy in helping me to do this, so from my (admittedly so far limited :)) experience, there is absolutely no conspiracy to \"dumb-down\" KDE.  All that is needed is more people stepping up to help :) "
    author: "SSJ"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "That's honestly the best KDE news I've heard in a while. I hope you succeed!"
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Here here!"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "It is 'hear hear', but SSJ hits the nail on the head with a very good, personal example. That's exactly what's going on: lack of time. 4.2 will be a lot better, just try the upcoming alpha. And further down the road, think 4.3 and 4.4, we'll really start innovating and doing cool, impressive stuff. Sure, it took us 3 years to build the foundations & get back to 3.5 in terms of functionality - but I think the future will prove it was well worth it."
    author: "jospoortvliet"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "\"While the bugs, flaws and general broken nature of KDE 4 is somewhat disheartening, the truly scary thing is the direction it's taken.\"\n\nPlease provide some tangible examples of the \"direction\" KDE has taken and why it's the wrong direction. Seriously. You are so strong with your opinion, that answering this simple question should not be hard at all.\n\nWhat I see is that KDE has added a lot of groundwork and technology that allows KDE to become truly kick-ass system in the future. KDE3 was a dead end. It represented a desktop that was basically Windows95 on steroids."
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "can't stand the truth? the only thing reminding on win95 is the current stability of kde4. \nit's bitter - i know.\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Oh I can stand the truth just fine. And I can understand the fact that KDE3 represented ideology of the static-desktop that was common with Windows 95 and it's contemporaries. KDE3 was an excellent desktop of that type, but fact is that world and technology is moving elsewhere.\n\nAnd you too are complaining about stability of desktop that is 10 months old. KDE3 is currently 6.5 years old. Looking at the longevity of KDE3, KDE4 is literally still taking it's first steps. And we need to remember that move from KDE3 to 4 is a lot bigger more than move from KDE2 to 3 was. If we look at past releases, move from 1 to 2 was about as huge. And KDE2 was buggy, crashy and semi-functional for a long time after release."
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Not sure if you'll really get me. My poor English doesn't suffice for seriously arguments. Sorry.\n\nWhat I am really convinced of, is that the main cause for the KDE4 disaster is to think of a desktop environment as of a matter of ideology. \n\"ideology of the static-desktop\" - i can only hope that you had a grin at your face while writing that ... \nBut honestly, all intents based on inflated ideologies are doomed to failure. \n\nMore in detail now - its easier for me.\nFor daily work i mostly use bash, lets call shell here. If i would start vi with a terribly broken ~/.vimrc vi would crash. That's what you would expect. But if the underlying shell crashes conjointly, I would cry, curse and rant the hell - Everyone used to use a shell would call that a failure by design! But the very same thing happens with KDE4. A stupid single widget crashes not only itself, but the whole underlying desktop shell too! Really! \nSo a gravely failure by design - not the first or last one so far. But the big surprise is, how many people doesn't seem to realize that.\n\nDid you really never wonder about that? \nBack to your win95 comparison. Microsoft wasn't ever able to stabilize that platform - guess why? Because it's not possible to hide a misconception. They finally had to drop that crap and switch to NT. \n\nAnd an ideologically slipslop here only obscures the clear view. \nConsidering that, the critical posters here are much more constructive than the bunch of those, telling the devs will fix it. They didn't in 4.1 and they won't in 4.2 or later, if some fundamental changes won't happen. Even worse, defending the current state and pretending it can be fixed without changing  some fundamentals will only delay the badly needed redesign.\n\nAnd about the devs - you cannot blame those. Most of them working at applications and aren't responsible at all for that kde4 misconception.\n\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "If I recall windows 98 was based on the same codebase as 95. It still was horrible mainly cos it's kernel sucked. But if I recall it was far more stable than 95...\n\nI have had plasma crash, but afterwards it started right up again. Sure it is annoying but hardly the end of the world"
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "... yup and windows me was another weak try to stabilize the faulty design by cosmetics - with the very same result as before - it stayed _unstable_.  I thought the point was clear what I'd meant without having to mention all those stranded works at the surface.\n\nBut you are absolutely right, by introducing that automatically restart it was obvious that the KDE is wanting to go the win95/98/me-way, dealing with the symptoms instead of redesign the real cause.\n\n> Sure it is annoying but hardly the end of the world\nBut for long time kde users like me, in _need_ for a stable platform and now forced to take a glance at enlightment, wm or even gnome again, it comes rather close to that ;-) "
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "> dealing with the symptoms instead of redesign the real cause\n\nActually they are dealing with the real cause (non-sandboxed code) and designed it that it would allow avoiding it (sandboxed widgets, e.g. written in JavaScript)\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "And what about those widgets written in c++? Proof me wrong, but if a little stupid widget can crash the whole desktop shell, then they actually did not dealing with the real cause. And there isn't an excuse for a flaw like that. \nJust coming to my mind - even web browsers - in the past not especially known for security, stability or clean design - currently trying to isolate their different tabs, so one process should never be able to crash others or the whole browser itself."
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "those are the core widgets, and doubtless they will get more stable with time...\n\nPlasma in it's current incarnation is less than a year old"
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "yup, got that already.\nBut then the question is, how the separation is achieved between core (-widgets) and others? The chosen language as an isolating model? Honestly? And that's not only a matter of faulty design but also of security somehow. I'm sure you can imagine what is possible for those nasty guys with some c++ experience ;-) All they have to do is making an really appealing, seemingly harmless little widget..."
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "They could do that already... Make a nasty little plugin for prog X, then uh oh. Why would you use plugin X? Because you know it comes from a trustworthy source. \n\nYou can't download with ghns binary plasma packages, only scripted plasma widgets. Any thing else will either be included in your distro packages (you trust them, right?) or you'll have to compile yourself (in which case you take the risk).\n\nScripted plasma widgets use the scriptengine which serves as a buffer between the applet api (in language X) and the native c++ api."
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "> Make a nasty little plugin for prog X ...\nI did answer that post in the second thread with you, ~10 posts below ...\n\nhttp://dot.kde.org/1225379191/1225397878/1225398986/1225399609/1225574516/1225577095/1225577939/1225583584/1225630926/1225635227/1225636893/1225640312/1225642243/1225645600/1225649024/1225652893/1225668179/1225676239/"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "\"What I am really convinced of, is that the main cause for the KDE4 disaster\"\n\nThere is no \"KDE4 disaster\". There is just software that simply doesn't yet do all the things it's users would like it to do. That's hardly a \"disaster\".\n\n\"But honestly, all intents based on inflated ideologies are doomed to failure. \"\n\nInflated ideologies like \"free software\"?\n\n\"For daily work i mostly use bash\"\n\nMost users don't. If you are a CLI-user, are you really in a valid position to offer critique about a GUI?\n\n\"Back to your win95 comparison. Microsoft wasn't ever able to stabilize that platform - guess why? Because it's not possible to hide a misconception. They finally had to drop that crap and switch to NT.\"\n\nSwitch to NT was about switching the kernel of the OS, it had very little to do with the GUI itself. GUI and the concepts behind it of XP and Win95 (or Win98) is fundamentally the same.\n\n\"Considering that, the critical posters here are much more constructive than the bunch of those, telling the devs will fix it.\"\n\nThey are not constructive at all. Their critique is vague and unspecific. They keep on telling how \"KDE4 is unusable\". That's not constructive at all. If they wanted to be constructive, they would provide tangible examples of what exactly is broken.\n\nThe most tangible examples we have been told in this discussion is basically that \"some functionality that is present in KDE3 is absent in KDE4\". And I agree. That claim is true. But the fact is that that missing functionality is being added back as we speak. This is a question of resources and time. In the migration to new codebase, some functionality was lost (and some was gained). But as time goes on, that functionality is added back.\n\nYou and many others seem to think that KDE4 is about removal of features. It's not. You are complaining about a release that is 10 months old. You are comparing it to a release that has 6.5 years of developement behind it (even more, if we consider KDE2 to be kind of \"pre-KDE3\").\n\nCompare KDE4.0 to 4.1. HUGE amount of improvements happened between those releases, wouldn't you agree? And they did that in 6 months. Sure, you might say that 4.1 isn't good enough yet. But they are on a right track. In just 6 months they fixed large part of the complaints users had about 4.0. It doesn't take a rocket-scientist to see that KDE4 is developing very, very fast.\n\n\"They didn't in 4.1 and they won't in 4.2 or later\"\n\nMany of the requested features were added in 4.1, and more will be added for 4.2. Were ALL of them added back in 4.1? No. There's only so much time and resources to do this stuff. \n\nWhat exactly are YOU doing to help make KDE4 better? Am I correct in assuming that your contributions so far consist on sitting on your arse and complaining?\n\n\"if some fundamental changes won't happen.\"\n\nAnd what would those be? Come on, let's hear some \"constructive criticism\".\n\n\"Even worse, defending the current state and pretending it can be fixed without changing some fundamentals will only delay the badly needed redesign.\"\n\nWhat changes are needed? What kind of \"redesign\" would you want? What exactly is broken in KDE4? If you want to be constructive, now is your chance. Vague comments about \"redesign\" and \"fundamental changes\" are NOT constructive.\n\nAnd are you really in a position to demand for \"fundamental changes\" and/or \"redesign\"?"
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "> Most users don't. ...\n\nYou didn't even read my post! The comparison with bash and vi should  demonstrate, what you would expect of a shell. That also applies to a graphical shell like a desktop environment.\n\n> ... are you really in a valid position to offer critique about a GUI?\n\nSo stop! It's an impertinence not being able to understand (or deliberately to ignore) what i am talking about, but judging about 'valid positions' - every one using a software is in it. Period!\n\nAll your following lines mostly dealing with the missing features in v.4. I didn't ever mentioned those! If you want to make another Obama/McCain drivel here, answering questions i never asked - I'm not the guy for those time wasting nonsense.\n\n> And what would those be? Come on, let's hear some \"constructive criticism\".\nRead the part about what a shell is supposed to be again.\n\n\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "> The comparison with bash and vi should demonstrate, what you would expect of a shell. That also applies to a graphical shell like a desktop environment.\n\nI agree that this is comparable, however one needs to check whether a specific example is comparable.\nIf an application launched by the desktop shell would take it down when it crashes, that would be bad.\n\nFortunately that doesn't happen.\nThe only things that might crash the desktop shell are non-sandboxed plugins, just like non-sandboxed plugins of a shell interpreter can make it crash.\nTherefore one of the desktop shell's design goals was to allow sandboxed plugins, comparable to shell functions and scripts in your analogy.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "OK, yup I've got it.\n\nSo now, is it possible that when I'd write yet another silly analogue clock widget in c++, that widget would be a \"non-sandboxed plugin\"? Is the choice of language the isolating concept? And if so, that you would not call a design flaw?\n\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "The core widgets will be in c++. But in future I expect that they will encourage people to write code that is sandboxed. \n\nIt would be helpful if you described what widgets were bringing plasma down, or maybe heaven forbid, file a bug report?"
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "It's a compromise: keeping widgets in process allows for reduced memory consumption and better visual integration, but native widgets will bring down the Plasma process (just as in KDE3, a dodgy applet would kill your panels).\n\nPick your poison."
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "No, if I remember right - the opened applications and the desktop shell (that was the subject matter) continued to run quiet well. KDE3.x seems to have a far better modular design to achieve such isolating. \n\n\"It's a compromise\" - isn't that the most common phrase when it comes to a defense of design flaws? ;-) where i heard that lately? ... our company last Friday :-D\n\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "He is talking about kicker applets. So kicker goes down.The desktop remains, so what good is that to you beyond being able to click on an icon?\n\nYou are aware the krunner still works if plasma goes down? As do any apps still running and pretty much the rest of kde. What could you possibly be doing on the desktop that means means it is a disaster if it goes down?\n"
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "> ... so what good is that to you ...\nIt simply only had crashed kicker. The applications still in front, i can uninterrupted continuing my work or whatever I'm currently up. Alt+F2, MMB etc. all remaining untouched - That in fact is the advantage a modular design can provide you with. No more, not less, but personally i think there are much smaller things related to the 'desktop experience' and usability that had been widely discussed here...\n> What could you possibly be doing on the desktop that \n> means means it is a disaster if it goes down?\nBesides the clear advantage mentioned above, really it shouldn't be as hard  for example to imagine a widget crashing continuously to more than only disturb your workflow ...\n\nIngenuous, would you want your employees work with that, wasting their time and your money ... ;-)"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "If plasma crashes you can still use alt+F2, and apps remain untouched and kwin still works so you can also Alt+TAB.\n\nAlso note that if a widget crashes plasma, either it won't be added when plasma restores or it will be in a previous stable state (plasma saves its config periodically), or if plasma keeps crashing it gives up after a few tries and lets you start it manually.\n\nBut seriously what distro are you using that has such crashy behaviour? And what did you try when plasma did crash, for you not to know this?"
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "No, you are simply wrong. First, there is a difference if a 'plugin' you've deliberately  installed can crash an application or if a widget (also deliberately installed) can crash the whole desktop shell. Second, a plugin architecture should ever try to isolate its core against those plugins.\n\nFinally, a concept where the chosen language qualifies for privileged 'non-sandboxed' API calls !? Of course you have no doubts about that ;-) \n\nIts always the same when it comes to lousy concepts. First it will be claimed there isn't something like that, and later you can hear that it isn't as bad or others do worse. \nBut what I learned just now is, that there isn't only the automatic restart, nope! plasma also saves its config periodically. (doesn't sound suspicious?) Thats what I'd call cosmetics - with the result cosmetics ever gain, read the posts here or remind yourself on win95/98/me. Btw. even the ones claiming to be happy with the sadly state kde4 is in now, have to admit that it runs not quite stable. A Desktop shell!? 2008 not 1998!?\n\n> But seriously what distro are you using ...\nMostly I'm running debian, sometimes gentoo, but my latest kde4 experiences i've had with an ubuntu notebook. And also seriously: having plasma crashes there wasn't the same as suffering a kicker crash (must be years ago), in terms of effects and what it felt and in terms of frequency! \n\n\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "When running \"stable\" i.e. 4.1 I only ever got one or two crashes. Even when running trunk I have surprisingly few crashes. \n\nSaving its config makes sense even in the most stable of stable apps. Besides I've never claimed it was completely stable, Plasma is still very young (<year) so can hardly expect it to perform flawlessly. You can simply think of plasma applets as core extensions of plasma, anything will be in scripted languages.\n\n\"having plasma crashes there wasn't the same as suffering a kicker crash (must be years ago)\"\n\nYou lose the panel and your wallpaper and whatever applets you have on your desktop. Nothing else. The fact that kicker is extremely mature > ~6 means that it had lots of time to become stable. Unfortunately it's code was pretty unmaintainable and couldn't be ported to Qt4 easily which was the reason they dropped it. \n\nIn my experience if you want a good kde environment you should try either mandriva or suse. "
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "Cause your post only deals with the symptoms, I'll take the absence of reply to the question regarding the subject matter as an answer -\n\nto be said further (though it really wasn't the point):\n\n> Plasma is still very young (<year) ...\n\"KDE launched Plasma in 2005 to revitalize the desktop interface ...\"\n[ http://www.linux.com/feature/114560 ]\n\n> Saving its config makes sense even in the most stable of stable apps\nPeriodically? (Btw. how often?) What stable apps do you mean?\nAnyway, a need for that caused by faulty design isn't just what you would expect after a 'fresh software redesign'. Besides, it would be very illustrative to hear if kicker ever had the same mechanisms ... if not - why? You never wondered? Seemingly others felt the difference between kicker and plasma crashes too. In quality and quantity.\n\nThanks for the distro advices. \nBut i'll stay as long as possible with kde3.5. And then, if there hasn't been a fundamental change in kde4, i have to take a look at old friends and foes ;-) unfortunately. \n\nBut maybe a new QT desktop has evolved in the meanwhile - a professional one in terms of robustness by concept, one to attract developers to base their software on it - \n\n\n"
    author: "lennart"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "Just what exactly is faulty by design? (and don't answer KDE or Plasma, I mean which concept in the algorithmic sense, ie what to fix)\n\nIf your answer is un-sandboxed widgets, then everything is already explained. Base widgets (ie shipped with your distro) can run un-sandboxed, which allows for grater visual integration. Other widgets (the kind you get through ghns) are all scripted and run in a sandbox.\n\nI'm a new KDE user, I couldn't stand 3.5, which always felt off, and am currently running kdesvn on a production box with very little pain considering. "
    author: "David Mills"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "> Just what exactly is faulty by design?\nThought after the longest thread i ever wrote that's pretty clear now. Blaming my English ...\n\n> If your answer is un-sandboxed widgets, ...\nYup, that's an important part of.\n\n> Base widgets (ie shipped with your distro) can run un-sandboxed... \n> Other widgets (the kind you get through ghns) \n> are all scripted and run in a sandbox.\nThat in turn simply isn't just the truth. Every one (not only distros) can write or ship 'base widgets' running un-sandboxed. Naturally not via GHNS, but that - as you surely know - isn't caused by and wouldn't suffice as a security model.\n\n\"... choosing which language to write your applet in. C++ and ECMA script will both be available by default\" \n[ http://plasma.kde.org/cms/1084 ]\n\nAnd don't get me wrong here - I'm not demanding to close the c++ API. In contrary, who would write in say ruby if he could in c++ :-D  \n\nThat's why \"nearly ever plasmoid on KDE-Apps is a C++ plasmoid\" \n[ http://mail.kde.org/pipermail/plasma-devel/2008-July/000078.html ]\n\nProbably the point gets somewhat clearer ...\n\nI will try again: Every shell (even most apps with a certain extent) needs to separate its core from additional functionality it probably wants to provide. So first it has to be decided, what functionality belongs to core and what is extension and second how to achieve a clear separation of the scopes. You ever want to shield the inner / privileged against the outer without loosing the benefit that those are coupled. You may have more scopes than only these two.\n\nAnd in both points kde4 fails deadly!\nAgain, if a silly analogue clock widget can crash the whole shell, than:  \n- either it belongs to a far to central scope \n- or (and) the privilege separation sucks \n\n> I'm a new KDE user\nand I'm a long time one - maybe that's why I'm worried and you apparently don't\n \n\n\n"
    author: "lennart"
  - subject: "what a mess!"
    date: 2008-11-03
    body: "gosh! what a mess :-/\nto be said here in addition: every single widget should be encapsulated not only against the (core)shell but against each other widget - regardless of the language, of course \no man!"
    author: "abramov"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "OK, I re-read your comment. I must admit, that before I just glanced through your comments regarding Bash, Vi etc. But, to address your comments:\n\nYou are concerned about Plasma crashing? crashes are naturally always bad. But doesn't plasma automatically restart itself in case of a crash? And I haven't witnesses any plasma-crashes in 4.1.\n\nThat said, would plasma crashing REALLY be that bad? It would not affect apps at all. Apps are not related to plasma, and neither are app-windows. If plasma crashed, the worst that could happen is that the panel and the widgets disappear. But your app would still be there.\n\nI would be more concerned about Kwin crashing, since that could actually take the app-windows with it (although it would probably just take the windeco with it, leaving the app untouched)\n\nIn some ways plasma i somewhat similar to Explorer.exe (which drawn the desktop) in Windows-land. And I have had Explorer crash on me when I had had bunch of apps open. And after I restarted explorer, my apps were still there.\n\n\"Read the part about what a shell is supposed to be again.\"\n\nIt's supposed to be the area where the user gets his work done. Be it CLI or a desktop. How does KDE4 and/or plasma divert from that definition? If anything, it provides the user more tools to get his work done. Is that a bad thing?\n\nOr is your point that shell should be about running applications, and it should not concern itself with stuff like plasmoids at all? Plasmoids are just an extension of what we have had since the beginning. Plasmoids are just another step in the road that was started by systray, taskbar, Kmenu, icons and the like.\n\nIf you just want a desktop that runs your apps, why not use something like TWM? Fluxbox? What you seem to want is a barebones windowmanager, not a full-blown desktop.\n\nIf you want KDE to move towards barebones WM, then it should be said that KDE is not about that, and it has never been about that."
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "The system I am using at a client company has KDE 3.2 installed. KDE 4.2 is almost upon us, so it's okay to compare the two. KDE4 has much more eye candy, and looks really slick, but when it comes to actual functionality and <gasp> usability, KDE 3.2 edges it out. I see a lot of scaffolding for what will surely be awesome stuff in the future, but in the meantime KDE3 (even the ancient 3.2) is more productive for me."
    author: "David Johnson"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Wow, nice to hear something different than \"kde4-looks-so-nice\" post. I agree with you, and would like to add that with systemsettings it takes a lot longer to navigate compared to kcontrol with its tree view. OTOH, bigger icon everywhere. Progress!!! ;-)"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "System settings are much more organized than they used to be. Kcontrol was fine for experts, not for all users."
    author: "Hobbes"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Right. Cause now we have \"I wish I was OS X\" control panel instead."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Well, if it works better..."
    author: "illogic-al"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Yeah, let's add the expert and basic tabs. Then it will be even more organized. Look, if you create something for stupid users, only the stupid ones will use it. Do you really want that?"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "yeah, there a 2 possiblities : organize Hierachically or by Subtextsearch.\n\nthe old KControl had this. Now i have to guess where my settings are. Great !"
    author: "sfdg"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "I believe someone was working on a KControl look for the systemsettings. Shouldn't be hard to do. Feel free to volunteer..."
    author: "jospoortvliet"
  - subject: "No: System settings is less organized!"
    date: 2008-10-31
    body: "I want to change some keyboard properties.\n\nIt appears in:\n\nGeneral -> Personal -> Country/Language\nGeneral -> System admin. -> Keyboard & mouse\nAdvanced -> Advanced user options -> key combinations\n\nAs a \"beginner\" I don#t know of deadacute keyboard maps!\nI know of the difference between \"\u00e9\" and \"'e\" and I want that I sdwitching desktops is possible using Ctrl+Alt+Left/Right\n\nNow please tell me, how I find these options as a \"new user\"\n\nKcontrol was better for both, beginners and \"professionals\". I agree it is easier to visually find items in a huge icon panel compared with a listview. But the items themselves are badly organized. I bet you agree you were wrong. \n"
    author: "Sebastian"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "> When we say the menu sucks\nThen \"we\" failed to note that the traditional menu is still available.\n\n> When we say kicker is broken\nThen \"we\" failed to realize that it can't be cause there is no kicker any longer.\n\n> When we say Dolphin sucks functionality-wise compared to Konqueror\nThen \"we\" failed to realize that Konqueror is still there.\n\n> Shame on you.\nNope, shame on \"we\" for wasting space in the internet."
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "I think tooth's confidence just paid off and all that nonsense was proven wrong :D \n\n"
    author: "dandu"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Wow, how does it feel to be a cheerleader? Seriously.\n\nBut really, to me his arguments sounded like a bunch of excuses. \"You can work around the stupid design philosophy by doing X or Y, so you are stoopid\", but never actually addressing the stupid design philosophy itself.\n\nCheers,\n-Ben"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "> Wow, how does it feel to be a cheerleader?\nPlease tell us.\n\n> like a bunch of excuses\nAnd yet you never did contribute anything. Is this your first contact with FOSS? or probably even with other humans?\n"
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "## Wow, how does it feel to be a cheerleader?\n#Please tell us.\n\nWhat am I a cheerleader for exactly? I guess I'm just so stupid I miss your point?\n\n## like a bunch of excuses\n#And yet you never did contribute anything. Is this your first contact with FOSS? or probably even with other humans?\n\nNegative comments are a contribution. Want something more specific? Try this: \"Please, please, for the LOVE OF GOD, don't try to make KDE like Vista, or like Leopard, or like Gnome.\" \n\nBecause I don't support KDE blindly doesn't mean I don't know anything about human contact, or about FOSS. I've submitted my share of patches, though admittedly not to KDE. I've also donated to the FSF and a few others. Not that I think it earns me any cred, karma, whatever, but that love me or hate me, I'm a member of the community too. Just because I agree with Bob about KDE4 doesn't mean I am a hater. But I have a special distaste for those who blindly follow something or make excuses for it, especially when we can potentially make it better. Pretending it already IS better is just a hindrance in the long run, not a help."
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "> Just because I agree with Bob about KDE4 doesn't mean I am a hater.\nIt means exactly this cause his comment has exactly nothing to do with critics. The \"shame on you\" part alone is enough to don't cheerlead for him like you did. Don't expect anyone to take him or you serious with such an anti-social message attached.\nOr in short: Don't wonder to have shit in the corner if you shit in the corner.\n"
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: ">It means exactly this cause his comment has exactly nothing to do with critics. The \"shame on you\" part alone is enough to don't cheerlead for him like you did. Don't expect anyone to take him or you serious with such an anti-social message attached.\n\nIt sounds to me like you are arguing with \"Bob's\" delivery primarily content. I don't really care for his approach either, there's a lot of anger, and in this context it's pretty irrational. But facing up to real problems is the way forward, and on that point I agree with him.\n\nBesides this, I get the same vibe from your posts. A lot of anger, a lot of excuses. I don't know you except for the few posts you've made here, but it sounds to me like you're just making excuses for the state of KDE4."
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "> Besides this, I get the same vibe from your posts.\n> A lot of anger, a lot of excuses. \nCorrect. I feel guilty for not being able to do what your parents missed: teach you how to interact with others in a social and constructive way. Depending on your age it may even be to late and this would be hard because staying alone in this world, without real friends, without trust and without love, will destroy you.\n\nHere is my tip: try to get in contact with others. You don't like something withint KDE? Well, try to change it in a *constructive* way. The KDE family welcomes anyone, anybody can participate and its a good feeling to work with others on similar goals. You can do something good rather then flaming your personal frustration out anonymously within a forum. Be constructive, feel good, enjoy your time on earth and start to love and be loved!\n"
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: ">Pretending it already IS better is just a hindrance in the long run, not a help.\n\nAgreed, 100%. It seems like nobody in the core KDE team seems to want to admit that their approach to calling a heavily developed new branch a stable build is a mistake. Admitting it was a mistake and restating the direction would only help.\n\nBeing honest about mistakes is probably the only reason DreamHost has stayed in business as long as they have. Sure they had a lot of undue breakage and downtime, but they were forthright with their customers, and admitted it was their own fault. Why they've lost so much marketshare to the likes of AS and similar is because after admitting their mistakes, they largely failed to remedy them.\n\nNot to say that all KDE devs are paid, that isn't my point. In fact very few are, and most motivations are probably very positive. But if you are really trying to help people, and build a reputation as the best coders out there, pretending KDE4 is on task and everything's shiny is a misstep."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Hello Bob,\n\nit comes down to that: A stable release is one with bug fixes and security fixes being down by the project. Any project obviously will only expand to that effort where it makes sense and KDE found it made sense to release KDE 4.0.0, which was its own decision.\n\nThe quality of 4.0.0 was downright amazingly bad compared to the quality of 3.5 and all earlier releases that I have seen from 2.2 on. The rewrite mistake has been made by the project with regards to kicker (luckily not for kwin), and I am told, it was the only choice.\n\nGiven that the project is usually making decent choices, I am willing to wine at the loss of an alternative reality with a KDE 4.0.0 release that had only bugs, not so much feature removal in the core desktop shell at the same time, but then, probably users would have liked that, but no developers were there to implement it, including myself. I regret it, because I think that was a good opportunity to give back.\n\nSo, at least I am at peace with KDE 4, even though I won't be able to use it before 4.3 or so.\n\nAnd one more thing: No KDE developer owes anybody to work for universal success of KDE against all other desktops. Why would they? KDE is e.g. not there to extinguish Gnome, is it? What would be the purpose of that?\n\n\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Hello Bob,\n\nit comes down to that: A stable release is one with bug fixes and security fixes being down by the project. Any project obviously will only expand to that effort where it makes sense and KDE found it made sense to release KDE 4.0.0, which was its own decision.\n\nThe quality of 4.0.0 was downright amazingly bad compared to the quality of 3.5 and all earlier releases that I have seen from 2.2 on. The rewrite mistake has been made by the project with regards to kicker (luckily not for kwin), and I am told, it was the only choice.\n\nGiven that the project is usually making decent choices, I am willing to wine at the loss of an alternative reality with a KDE 4.0.0 release that had only bugs, not so much feature removal in the core desktop shell at the same time, but then, probably users would have liked that, but no developers were there to implement it, including myself. I regret it, because I think that was a good opportunity to give back.\n\nSo, at least I am at peace with KDE 4, even though I won't be able to use it before 4.3 or so.\n\nAnd one more thing: No KDE developer owes anybody to work for universal success of KDE against all other desktops. Why would they? KDE is e.g. not there to extinguish Gnome, is it? What would be the purpose of that?\n\n\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Not a cheerleader, don't stress yourself too much about it Ben. Joe's comment was just the right reply for all that bs.\n\nI don't like the current state KDE 4 is in either, and that's why I'm not using it, just playing around and reporting bugs when I get some spare time. But I see great potential, and that's why I'm willing to wait until 4.2 or 4.3... \n\nOn the menu thing: I love the new menu (no cheerleading). I've been using it on KDE 3 for more than 1 year, and it really is a great productivity boost for me. I'm not bothering to explain why or how, I know you're not interested."
    author: "dandu"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: ">> When we say the menu sucks\n>Then \"we\" failed to note that the traditional menu is still available.\n\nThe traditional menu is available in a hackish way. The Suse-provided menu is a mistake, pure and simple. Defending the new, shitty menu because the old one is technically still around doesn't make much sense to me.\n\n>> When we say kicker is broken\n>Then \"we\" failed to realize that it can't be cause there is no kicker any longer.\n\nIt's not called \"kicker\", but it's fundamentally the same. If we are going to regress in to arguing semantics, we may as well bark and growl at this point. \n\n>> When we say Dolphin sucks functionality-wise compared to Konqueror\n>Then \"we\" failed to realize that Konqueror is still there.\n\nKonqueror is still there... as the web browser. But which is the default system browser?\n\nYour \"rebuttals\" are exactly the same ilk as all the other blind KDE fanboyism which seems to support the KDE4 team no matter what they do. I love KDE, and I think that they have a lot of the best code practices around, even down to using GIT, which is pretty progressive. But again, my primary complaints are about the direction, not about the specific technology. That's just the icing on the cake."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "\"Defending the new, shitty menu because the old one is technically still around doesn't make much sense to me.\"\n\nMaybe so, but if someone present the thesis that KDE are depriving the users of choice and power users of their preferred menu, then pointing out the old menu is still there and officially sanctioned by the KDE devs (heck - switching to it is as simple as right-clicking on the menu and choosing \"Classic\"!) makes perfect sense.  \n\n\"Konqueror is still there... as the web browser.\"\n\nAnd a file manager.\n\n\"But which is the default system browser?\"\n\nDolphin.  There is of course a GUI option in KDE4.2 to change this which, incidentally, did not exist in KDE3.  "
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "It seems to me you're just making excuses. I agree with the parent that the direction KDE is taking frankly sucks. If you really like Vista you probably disagree, but not everybody thinks like that. In fact, things like Window's new ad campaigns like \"Mojave Project\" go to show that not even MS fans like Vista very much.\n\nCheers,\n-Ben"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "You mean the direction of having more immediate choice in menus by default and adding the option to configure which file manager you want to use out of the two now installed by default?  Sorry, this doesn't make any sense, and I can't imagine how you perceive it as \"making excuses\"."
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "No, I mean the defaults being [very poor] Vista/OSX rippoffs."
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Two options:\n1. Set defaults for power users, and hope that less sophisticated users will figure out how to simplify it to their tastes.\n2. Set defaults for more naive users, and hope that power users will figure out how to tweak it to their tastes.\n\nYou seem to be someone with the ability and interest in tweaking your environment, and once the KDE4 gets fully fleshed out it probably won't take you long to achieve what you want. So, I vote we go with #2.\n"
    author: "T"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Maybe I miss your point, but in the comment you're replying to, I was talking about the look and feel. There's no excusable reason to bootleg the look and feel off Vista or Leopard. I mean, even if they were all that attractive (hint: they're overrated), KDE shouldn't be a clone or emulation of their design philosophies. Should it?"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "I keep seeing claims that KDE4 has the same look and feel as OS X and Vista. I have to admit that I find it difficult to see these claimed similarities. If you are refering to optimizations based on research and proven experience on Human Computer Interaction then I think is better to optimize the experience rather than make it more difficult. Of course one can do without mouse, menus, windows etc because they are \"copying\" technology and concepts first illustrated with the Xerox Alto personal computer back in 1973, but is this what you want?\n\nObviously Apple and MS have the resources to pursue research and mass trials on Human Computer Interaction and one can see the results of these studies as part of their products. KDE and other desktop environments do not have the resources to pursue similar studies so they usually come late in improving their interfaces. Sincerely, I do not see anything wrong with developers learning from their experience with other systems. If humans were not learning from others then we would probably have still been living in caves...\n\n-- John"
    author: "john"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "> doesn't make much sense to me.\nBefore it was all not there and now it's there but does not make sense to be there? Interesting shift in your troll-strategy.\n\n> But which is the default system browser?\nMan, you can set whatever you like to your default. Seems you continue to fail and realize just anything.\n\n> Your \"rebuttals\"\nYet I did prove that ALL of your points are BS and that's your answer? Come on...\n\n> But again, my primary complaints are about the direction\nLet me be very clear here: Your primary problem is your attitude and your social interaction.\n"
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "You still haven't answered the overhanging complaints about a shift towards Vista/Leopard wannabe-ism. In fact, if you were to, it would be a first on any KDE blog or mailing list; at least that I've ever read.\n\n-Ben"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "> You still haven't answered the overhanging complaints about\n> a shift towards Vista/Leopard wannabe-ism.\nYou are really amazing. First you did present that as fact and now it's a question?\nUltimate answer: Either improve your social skills or stop wasting our time.\n"
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "#It means exactly this cause his comment has exactly nothing to do with critics. The \"shame on you\" part alone is enough to don't cheerlead for him like you did. Don't expect anyone to take him or you serious with such an anti-social message attached.\n\nYou might take a little of your own advice friend. Plus, it wasn't even a question. What I said that KDE is starting to look like a Vista/Leopard rippoff, and nobody seems to want to talk about it. You answered by changing the subject... if you were trying to rebut the point, you're going about it backwards. :)\n\n-Ben"
    author: "Ben Hurrr"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: ">> What I said that KDE is starting to look like a Vista/Leopard ripoff, and nobody seems to want to talk about it.\n\nWell instead of running around crying that the sky is falling, what do you propose KDE should look like? I personally feel that one of KDE's greatest strengths is that I can change my icons, colour, window style, splash screen, etc any time I feel like with just a few clicks. And if you're talking less about looks and more about functionality as to how KDE is ripping off everyone else you'll have to provide some examples because I don't see any blatant ripoffs happening. Similarities? Sure. All OS's are going to have some things in common. Hell there are things in Leopard I would love to see in KDE. I'm sure there are going to be things in Windows 7 that are going to be brilliant. I'm also 100% sure that there are and are going to continue to be better than anything else available.\n\n>>the best way is to actually **listen to the power users in your community**\nLike when some users said that they wanted the old menu back and so someone rolled up their sleeves and implemented it? Just because things aren't the default way of doing things doesn't mean you weren't listened to. And just because something has yet to be implemented doesn't mean it won't.\n\n>> A healthy open-source effort needs to take negative criticism into account as much or more so than the positive criticism.\n\nWhen I hear \"negative criticism\" I think of what you are doing, repeating over and over that such and such feature is broken and not providing any real solutions to remedy it. \"Positive criticism\" is saying this doesn't work for me what if we tried this or maybe that would work better. No wonder the developers stop listening. If all I ever heard was \"it sucks, it looks like Vista\" I'd stop listening too. Be constructive, be positive and you might have a better chance of being taken seriously. Otherwise you get labeled a Troll."
    author: "Damiga."
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "The old menu is fundamentally broken. If you install the whole of KDE, it becomes an unusable mess. Make the mistake of changing the font size up too large and it moves into \"reformat and install windows\" territory. If you have any amount of software installed on your machine, for example to satisfy more than one user, it becomes a mess.\n\nI've watched the iterations on the old menu over the 3.x series, trying to make it work. Different ordering schemes, reorganizations, re classifying, etc. It didn't work.\n\nThe only way the old hierarchical menu works is if you severely limit the available selection of software. I find that an insult and travesty. One of the strengths of free software is the available choice.\n\nThe new menu is an attempt to deal with reality. You are seeing the second iteration, with many of the frustrations of the first iteration fixed. There is still room for improvement. But to suggest a hierarchical menu as solution for anything is absurd.\n\nDerek (who recently looked at gnome, and seeing a hierarchical menu, removed it. Life is too short.)"
    author: "dkite"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "And KControl, where is it ? ^^"
    author: "Pliskin"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "https://bugs.kde.org/show_bug.cgi?id=153556"
    author: "Joe User"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "It doesn't seem like KDE people want to take care of this \"bug\"..."
    author: "Pliskin"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "> Then \"we\" failed to realize that Konqueror is still there.\n\nNo, you failed to realize that it uses dolphin part so they are completely the same!\n"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "> Then \"we\" failed to note that the traditional menu is still available.\nThat 'classic' menu lacks a lot of feutures the kde3 one has. It's really an insufficient replacement for it.\n\n> Then \"we\" failed to realize that it can't be cause there is no kicker any longer. \nOk ;-) even worse - no kicker and no coequal replacement in terms of functionality.\n\n> Then \"we\" failed to realize that Konqueror is still there.\nAs you obviously don't know that konqueror uses the file manager part in common with dolphin, you have to realize - i know, it's hard - that those advanced features are lost in KDE4.\n\nYup, it's a shame.\nBut lets hope for a kde 3.5.11 - or a jump to 5.0, simply skiping this KDE4 mistake "
    author: "Roland M."
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "i used to be an angry user who keep moaning about the state of kde, linux etc...., but now i have changed, things are more complex then a simple vision of an angry user,  kde dev are not stupid at all, they are doing all what they can, they know what they want and to be honest they are damn smart on many aspect, but simply life is not fair. \n\ntake the example of plasma team, ask yourself how many of them are paid to do that, or simply how many dev are working on it, you will be surprised by the answer, \n\nkde as any other opensource project need more devs, more artists, more people who can write good and robust documentation, good bug reporter, people who can submit clever and usable ideas with mock-up, people who answer questions on the forum and the mailing list.\n\ncome on man, kde can't grow by itself, so please either help them, or at least be polite in your critisism. \n\n"
    author: "mimoune djouallah"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "You're right, my attitude towards KDE has gradually shifted from helpful to disenfranchised. I don't suppose it would help to say my earlier correspondence was a lot more polite? But it seems like for the most part, KDE has decided \"screw the community, we'll do what we want\" and users like me are left screaming at a wall."
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "I don't think you're a troll---you have demonstrated give-and-take in your posts, so you deserve a serious reply.\n\nI don't think it's so much \"screw the community\" as \"honest, guys, we know it's not perfect from a user perspective yet. But really, there's no way you can evaluate what it will become without actually diving into the code, because there hasn't been time for it to mature into its final form. So, we're not going to go into angst or massively change direction because of your criticisms just yet---we have a vision, and we're going to implement it, and we'll be quite interested in your feedback, but we'll take it seriously only after we've had a chance to show you what we think it can become.\"\n\nOften key, the fundamental advances are in the libraries (think Qt4, but also all of the other underlying technologies developed for KDE4). These changes break a _ton_ of code. It takes a long time for the benefits of these changes to start outweighing the breakages. For code that can be ported, in the short term the best way to get things \"up and running\" is to _disable_ the features that you haven't had time to port over yet; and obviously, brand new code takes time to become feature-complete. It's that disabling/incompleteness that is bothering many people. It's not a deliberate handicapping, it's simply the most practical way to get applications to the point of being quickly useful.\n\nThe fact that it's open source and released periodically should at least be reassuring: you can see for yourself that development is proceeding at a fast pace. It's not a case of someone promising great things but not putting in the work needed to make it happen.\n\nSo mostly, it just needs time.\n\nAnd if you still don't like it a year from now, pick the thing that bothers you most and try to fix it! Even if you can't program, pick a particular feature and  provide very specifically-documented suggestions for improvement. I've seen that the developers are quite responsive to that sort of input, at least if the code is in a position to make those kind of changes. (Often, it's a case of, \"yeah, you're right, but we first have to get X working before we can even begin to think of tackling Y.\" Again, it's a case of time, not \"screw you.\")"
    author: "T"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Thank you. If the replies I got on mailing and bug lists were more like yours, I confess that my attitude wouldn't suck so bad. \n\nStill, you say that KDE will get better in newer versions... To me, this is a fallacious argument. Of course it will. So will Gnome. So will XFCE. So will OS X, and maybe even Windows. There is always a newer build unless a project has outright died. But the state of KDE4 right now is simply broken. It never should have been released with a stable build number...  maybe it shouldn't have even been called KDE at all, but \"Plasma\" or something similar. That of course is moot; it's called KDE4, and it is what it is.\n\nBut this series of mistakes has given Gnome a huge shift in \"market share\", meaning a shift in resources and interest as well. And the fact that nobody seems to be willing to call it a mistake is perhaps the biggest mistake of all. We are a joke in the larger OSS community -- that's right, I don't exclude myself as a member of the KDE community, nor exempt from blame that it sucks -- let alone in the larger sphere of technology. In purist terms this doesn't mean anything. Who cares what people think? But answer me this. Is MS king of the desktop because the technology's any good, or because people perceive it a certain way?"
    author: "Fireproof Bob"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "\"But the state of KDE4 right now is simply broken.\"\n\nCan you elaborate, in detail, what makes it \"broken\"? I'm honestly asking this because I'm using KDE4 even on production machines, and most breakage I see is stuff I caused myself (mixing packages, old plasmoids that I forgot to update...)."
    author: "Luca Beltrame"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "KDE4 was released almost year ago and since then I see enormous amount of eyecandy added, but you still have settings in systemsettings that doesn't work or do anything (dead checkboxes), a problem reported several times in b.k.o. In my opinion, that means it's broken. Only solution I see is that they make 4.3 release a bugfix only release (or b.k.o cleaning release).\n\n"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "To me it only means that some areas need more manpower. Remember people, it's all about scratching an itch."
    author: "Luca Beltrame"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Yes, but if you don't have enough manpower, existing should be redirected to that areas. If not, we won't have a good DE. I know that most people are not paid for their work, but I'm also not paid to give only good comments.\n"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "In a volunteer-drive project, \"redirecting manpower\" is, unfortunately, meaningless - volunteers will work on what they want to work on.  Some kind of incentive is required, and I don't know what form that would take."
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Then volunteers will have to accept the fact that they will not have the best DE around."
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Well the volunteers are already happy with what they are working on. that's why they volunteered. Those who don't volunteer, they get to wait like everyone else. It's not about being the best, it's about doing something that's fun, that you like, that interests you. Everything else is largely secondary. "
    author: "illogic-al"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "\"Yes, but if you don't have enough manpower, existing should be redirected to that areas.\"\n\nSo you are suggesting that the artists are told to hack the code? That would be the same if construction-crew of an apartment-building decided to make the painters work on the electrical-work on the site, because it was running behind schedule. Nevermind the fact that they have no clue about how to install electricity-stuff...."
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Did I mentioned artists? Don't read between the lines. There's just empty space there."
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Basically he is extrapolating because people here apparently can't handle subtlety. What he means everybody's area of expertise is different and you can't just assign people to work on code which they do not understand. \n\nIf they are inclined or wish to work on something then they will take the time to understand, but you can't force anybody seeing as it is voluntary.  "
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "I know, but if devs that wrote all the good stuff in the old days (think kprinter) are now gone, that only confirms the subject of this thread.\n\n"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "Intersting quote at the bottom of this page:\n\n\"If you have an itch not being scratched, you can scratch it, or pay someone to scratch it for you.\" -- Eric Laffoon\n\nIt is true that some things are still not there, but if you don't have to patience to wait for them then you can do as the quotation suggested. Or you can go look at the feature plans on techbase and see that the printer stuff is actually being worked on."
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "If something is on the feature list that doesn't mean it will be in the final version. Printing stuff was planned for 4.0, then postponed for 4.1, then 4.2 and probably will soon be postponed again. \nBut I have patience, I use debian and they are luckily still on 3.5."
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Do you know what I do to try to get bugs fixed? I donate to KDE e.V and earmark money for specific tasks, like KOffice in the hopes that the devs there will fix bugs.\nThat's the cheap way of doing things. I could just hire a devel and make him/her write a patch, but things aren't that important to me.\n\nI think KDE in its current state is quite OK and it does all things I need it to do. It's far from perfect but much better than any of the alternatives (IMHO)."
    author: "Oscar"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "\"KDE4 was released almost year ago and since then I see enormous amount of eyecandy added, but you still have settings in systemsettings that doesn't work or do anything (dead checkboxes)\"\n\nYou do realize that artwork and functionality is handled by different people? adding artwork does not mean that functionality and bugs are not being worked on (or vice versa). Do you suggest that the artists should just twiddle their thumbs and do nothing, if the coders don't happen to be working fast enough for your tastes?"
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "I'm sorry but eyecandy is done in c++ too. Please don't enter the conversation if you cannot understand what I write."
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Really? Icons are C++? Desktop-backgrounds are C++? Themes are C++?\n\nDamn, I used one of my own photographs as a desktop-background. I must be a real C++-wizard!"
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Did I mention icons or backgrounds? Themes (styles) are c++, but i'm not talking about that either. Please stop being so ignorant. Try this:\n\nhttp://www.ereslibre.es/?p=98\nhttp://www.youtube.com/watch?v=M8wgD4gkVwU\n[...]\n"
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "\"Did I mention icons or backgrounds?\"\n\nWe are talking about artwork, are we not?\n\n\"http://www.ereslibre.es/?p=98\"\n\nLooks like a feature to me\n\n\"http://www.youtube.com/watch?v=M8wgD4gkVwU\"\n\nLooks like a feature to me, and not artwork. Artwork would be creating textures on those snowflakes. Code that simulates snowfall on the desktop is not artwork, it's a feature.\n\nDo you even know what \"artwork\" is=. This is artowkr:\n\nhttp://www.nuno-icons.com/\n"
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "No, we are not talking about artwork! I didn't mention the word artwork not once. Stay focused, don't put words in my mouth, reduce your imagination and read the whole thread again."
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "\"No, we are not talking about artwork! I didn't mention the word artwork not once. Stay focused, don't put words in my mouth, reduce your imagination and read the whole thread again.\"\n\nOK, here is the relevant part, and this was said by you:\n\n\"KDE4 was released almost year ago and since then I see enormous amount of eyecandy added, but you still have settings in systemsettings that doesn't work or do anything (dead checkboxes), a problem reported several times in b.k.o. In my opinion, that means it's broken. Only solution I see is that they make 4.3 release a bugfix only release (or b.k.o cleaning release).\"\n\nAre you now going to say that \"eyecandy\" and \"artwork\" are two different things?\n\nMaybe YOU should read the entire thread...."
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "Yes, I'm saying that artwork and eyecandy are two different things. Artwork is subset of eyecandy superset. You can do eyecandy in c++:\n\nhttp://www.youtube.com/watch?v=M8wgD4gkVwU\nhttp://www.youtube.com/watch?v=0FuB4YZfC4s\n\nEOD."
    author: "vf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Really? Icons are C++? Desktop-backgrounds are C++? Themes are C++?\n\nDamn, I used one of my own photographs as a desktop-background. I must be a real C++-wizard!"
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Aaah, sure, Gnome, XFCE, Windows & Mac OS X will get better over time, just like we. But we do have the advantage of an architecture which actually supports what we want to do - they don't. Gnome will have to spend years on re-architecturing before they can efficiently do what KDE is doing without any effort - same goes for all the other GUI's out there.\n\nNow we've lost some time by working on these lower-level things, but it's paying off. You can't seriously argue we're not improving at least 5 times faster than for example Gnome. Just compare KDE 4.0 with 4.1 - then look at the current & previous version of Gnome. And 4.2 is going to differ from 4.1 as much as 4.0 did. Besides, developers have seen the potential of KDE's libraries already - we're having such an influx of new developers we're thinking about changing our whole development model to accomodate the growth!\n\nWe're the joke of the larger OSS community? Look who'll laugh last..."
    author: "jospoortvliet"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "OK Jos, we cannot argue with that. The point is I have a new computer for the family and I'm trying set it up with the new Kubuntu. And... well... it is buggy and incomplete. I cannot force my family to use it. I'm installing Ubuntu with Gnome."
    author: "Flavio"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Rather than buying the *buntu hype, you could use a distribution that provides a stable and complete implementation. OpenSuse or Mandriva have both exelent KDE 4 offerings."
    author: "Morty"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "First of all, I think Kubuntu (and Suse and mandriva) should not have jumped on KDE 4 before 4.2. Second, Kubuntu has a serious lack of resources - esp it's quality process isn't up to the standard of Suse & Mandriva.\n\nSo it's more Kubuntu you have a problem with - complain to Canonical, they're ignoring KDE and are wasting their resources on Gnome. Delaying progress for the Free Desktop at large, as Gnome will never ever come even close to catching up with the proprietary competition. While KDE already has, at least architecturally speaking (and the visible part is just a matter of time)."
    author: "jospoortvliet"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Well, the big difference (as Jos says also) is that KDE4 is starting from a completely reworked framework, and so the number of expected breakages is huge. But on the other hand the progress will be much faster because of the excellence of that framework.\n\nI'm trying to come up with a \"real-life\" analogy, but I'm having a hard time, because this issue of architecture being different from immediate user-experience is perhaps more pronounced with software than anything else.\n\nHere's the best I can do at the moment: imagine a 2000 km race. One guy hops on the best technology available (a bicycle) and starts pedaling. Another guy spends 3 days attaching a motor to the frame of his bicycle, it causes some other problems that then need fixing, and so he starts out the race 4 days behind---and all his supporters are bemoaning the fact that he's going to be beat out by the other guy. But, even though the other guy keeps pedaling (i.e., keep getting better, too), the motorized bike wins in the end because it's so much faster. (I wince over that analogy, because for environmental reasons I try to use bicycles rather than motorized transport wherever possible!)\n\nI know that analogy sounds really fake and dumb, but in software development this is pretty much the main design decision day in and day out: work on improving architecture, or start pedaling? KDE has always focused on getting the architecture right, even if that means it takes longer to get the applications working. It is possible to spend too much time reworking your architecture, but I think so far KDE has shown remarkably good taste in that balance. It's much of the reason why KDE has been so successful despite having smaller resources than other desktops.\n\nIn my estimation, KDE4 has already largely turned the corner: KDE4.1 is usable enough (even if it does have annoyances) that I think we must be entering the high-speed portion of the race."
    author: "T"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "++"
    author: "illogic-al"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "\"But on the other hand the progress will be much faster because of the excellence of that framework.\"\n\nReally? And when will this happen? This decade?\n\n-- kolla, Kubuntu 8.10 - for KDE4 test-bunnies!"
    author: "Kolla"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "If you compare KDE 4.0.x with 4.1.x you'll see that. 6 months and lightning-fast improvements."
    author: "Luca Beltrame"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-03
    body: "yes, agree...\nsince i have tried KDE4.0 and seeing so much angry comments about KDE4, i have decided to take to plunge to help out with KDE development.\nsince now i m on the other side, i can see things differently, there are a lot of work done by the development teams. They are still trying to catch up with the features that are still missing compared to the 3.5.x counterpart.\nanother thing is that, as most of us work on KDE during our free time, it is actually hard to get things done quickly. i had to juggle my free time between work, family and hobby. i am determined to help KDE4 but we just have 24 hours a day and frankly I m not too good in time management either.. :-("
    author: "sonofusion"
  - subject: "agree"
    date: 2008-10-30
    body: "i'm back to fvwm since a month now.\ni've been on kde last years, every day.\n\n4.0 is not 4, is not .0 so why does it exists ? why give it that name ?\nto show off, just that.\n\n\"maybe 4.2 will be ok, maybe 4.3...\" could be 6.9, my problem is that actually the strange 4.1.2 thing is everywhere, kde as changed it's place from 1st choice to.. call it like you want, this version is not stable and is a mistake.\n"
    author: "_alexmyself"
  - subject: "Re: agree"
    date: 2008-10-31
    body: "4.x is not as good as 3.x, so you went back to... fvwm??!"
    author: "Paul Eggleton"
  - subject: "Re: agree"
    date: 2008-10-31
    body: "i agree, that's the part where i couldn't stop laughing myself ;)"
    author: "trapni"
  - subject: "Re: agree"
    date: 2008-11-02
    body: "Fvwm, unlike KDE3, is available on current distributions."
    author: "Kolla"
  - subject: "Re: agree"
    date: 2008-11-02
    body: "plasma have problems,\nkate have problems,\nkwin have problems,\nkonqueror haves problems,\ndolphin have problems,\nkmail have problems,\nk3b is not there yet,\n\ngo back to 3.x is not easy because 4 is the 'normal' version for most distributions, so yes it could be possible, but i don't want to pay attention each times i update my dist. there were no problems while 4 was flagged 'beta'.\n\nso yes i'm back to fvwm and all apps that could replace my usual kde ones.\n\nlaugh if you want, i've laugh too at the beginning, but time and my nerves have said stop to kde.\n\ni really don't understand why this release have been tagged as stable.\n\noh, and fvwm is really great, sure not the same kind of approach at all, just not the same things at all, yes but good in many ways.\nas for emacs,firefox,filezilla,amsn,graveman...\n\nthat said i hope kde, as we want to see it, will come back soon."
    author: "_alexmyself"
  - subject: "Re: agree"
    date: 2008-10-31
    body: "This has been discussed to death. Sure, we should've been more clear in the release announcement about the purpose of 4.0 (release our final, stable foundations & get the code to interested developers & users), but the name was fine."
    author: "jospoortvliet"
  - subject: "disagree"
    date: 2008-10-31
    body: "The name was _NOT_ fine.\n\nIt should have been \"KDE 4.0 Developer Release\"\n\nBecause in its form it was _only_ really useful for developers. \n\nEverybody who thinks otherwise should get the source and try to use it."
    author: "Tom"
  - subject: "Re: agree"
    date: 2008-11-01
    body: "It wasn't fine. The fact that it was called a x.0 release has led almost every distro to start dropping support for the still-maintained 3.5.x branch.\n\n4.0 was a tech preview. 4.1 is beta quality."
    author: "Anonymous"
  - subject: "Re: agree"
    date: 2008-11-03
    body: "That's their fault, not ours. Just like pretty much all .0 releases, KDE 4.0 was still missing a lot and had many issues. Sure, it might have been a pretty rocky .0 release compared to Vista, Gnome 2.0 or Mac OS 10.0, but not by much."
    author: "jospoortvliet"
  - subject: "Dug down"
    date: 2008-10-30
    body: "Wait, where's the mod down feature? Weren't we going to implement comment moderating so comments like this would be buried?"
    author: "kwilliam"
  - subject: "Re: Dug down"
    date: 2008-10-31
    body: "Yes you're right. If we could mod down posts like that, we'd only have to look at things we agree with. That would surely be a utopia of free speech... for the majority.\n\n-Ben"
    author: "Ben Hurrr"
  - subject: "Re: Dug down"
    date: 2008-10-31
    body: "Actually no, but \"poisonous comments\" (not implying yours are, I haven't read the thread in full) have had a huge impact on developer motivation. Moderation is to keep flames and ad hominem attacks out of the way (they *did* happen in a not too distant past)."
    author: "Luca Beltrame"
  - subject: "Re: Dug down"
    date: 2008-10-31
    body: "\"Trolls,\" \"poisonous comments,\" \"flames,\" and \"ad hominem attacks\" are also useful names to call anything you disagree with, to excuse yourself from having to address the potential of merit in any such comment. They are also ways to bury people who have real frustrations, sometimes not worded exactly tactfully, but often valid and honest nonetheless.\n\nThere are two great ways to be able to excuse yourself from weighing comments on merit or writing off others' frustrations WITHOUT having to affect others' interactions and discussions:\n\n1. Don't read threads you don't like\n2. Read them, and grow up and develop a little thicker skin so you don't have to go running to mommy with tears in your eyes.\n\nWhy are these two options necessarily worse than the (practical) forceful silencing of dissent?"
    author: "C. M. Franks"
  - subject: "Re: Dug down"
    date: 2008-10-31
    body: "The problem is that not everyone will grow a thicker skin, especially with arguments that come back *over and over* again, sometimes even coming up with not so nice sentences about the people working on this project. In the end, there will be some damage in terms of developers abandoning the project, or reducing their commitment.\nPoliteness isn't outdated, you know. The problem is a lot of criticism isn't worded properly, and I think it would require 2 minutes to think, before typing, to avoid most of this problems. Since it doesn't look like this is going to happen any time soon, I welcome moderation."
    author: "Luca Beltrame"
  - subject: "Re: Dug down"
    date: 2008-11-01
    body: "\"\"Trolls,\" \"poisonous comments,\" \"flames,\" and \"ad hominem attacks\" are also useful names to call anything you disagree with, to excuse yourself from having to address the potential of merit in any such comment.\"\n\nWell, the thing is that in the discussions here there hasn't really been that much criticism that is useful. Fireproof Bob (for example) spent lots of time and energy telling how KDE4 sucks. But he provided ZERO tangible examples on what actually is broken and how to fix it. He made vague comments about the \"direction\" KDE has taken, but he gave no tangible examples of what that \"direction\" is and why it is wrong. Sure, he said \"it's the same direction Vista has taken\". And that direction would be.... what?\n\nThere is a difference between constructive criticism and just saying \"it sucks\". The latter should be modded to hell, the former should be cherished."
    author: "Janne"
  - subject: "Re: Dug down"
    date: 2008-11-01
    body: "\"\"Trolls,\" \"poisonous comments,\" \"flames,\" and \"ad hominem attacks\" are also useful names to call anything you disagree with, to excuse yourself from having to address the potential of merit in any such comment.\"\n\nWell, the thing is that in the discussions here there hasn't really been that much criticism that is useful. Fireproof Bob (for example) spent lots of time and energy telling how KDE4 sucks. But he provided ZERO tangible examples on what actually is broken and how to fix it. He made vague comments about the \"direction\" KDE has taken, but he gave no tangible examples of what that \"direction\" is and why it is wrong. Sure, he said \"it's the same direction Vista has taken\". And that direction would be.... what?\n\nThere is a difference between constructive criticism and just saying \"it sucks\". The latter should be modded to hell, the former should be cherished."
    author: "Janne"
  - subject: "Re: Dug down"
    date: 2008-11-02
    body: "Personally I have no problem with people that disagree with me, just those that don't add anything valuable to the discussion. And having the ability to to mod down noise would be immensely useful. "
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-30
    body: "Imho, KDE 4.1 still has some minor rough corners, but 4.2 will iron out most of them (network-manager plasmoid, kde4 versions of k3b and digikam will also be available for 4.2) \n\nThe lack of future is more likely to happen for gnome... Since several releases only minor things change, and, worst is, in 2008+, who wants to develop in C? C++/Qt seem like heaven in comparison. It's just a matter of time until distros will turn their back to gnome in favour of kde."
    author: "Yves"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "> who wants to develop in C?\nLinux kernel hackers. GNU guys. Wine team. Me.\n\nActually, I vastly prefer to develop in C than in C++. There are very little really useful improvements in C++, the rest are either unneeded syntactic sugar, or outright bloat magnets that shouldn't be (over)used in the first place. And the (small) benefits of using C++ are easily offset by some really nice enhancements that come with ISO C99, if you only decide to use it.\n\nWhich brings me to my main complaint.\nWHY THE HELL there are no Qt bindings for the C language? Honestly, this is the most used programming language on the un*x platform. There are bindings for every little toy language any maniac likes to use, but not for C. Is it some kind of political matter, or what?\n\nI really respect the technical excellence of the Qt toolkit, which makes me that much more angry at being forced to either use Gtk, or program in a language I don't like. Usually I choose the former, 'cause having to restrict myself to C++ is REALLY annoying."
    author: "slacker"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Well, I don't know why there are no C bindings. Maybe nobody was interested enough ? You could be the one :-)\n\nBut, using C++ doesn't restrict you in any way.\nYou can do and use everything you can do in C also in C++ (and more).\n\n\nAlex\n"
    author: "alex"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Wrong! Wrong! Wrong!\n\nRead the specs. As I said, there are many cool features in C99 that are NOT available in C++ (even as GNU extensions in GCC).\n\nFor example, tell me how to write this nice and readable code example in C++, without sacrificing its' cleanness. This is perfectly valid and correct ISO C99.\n\nvoid fun(int foo, double bar, float complex tab[foo+20*sin(bar)])\n{\n\tfloat complex my_copy[sizeof(tab)/sizeof(float complex)];\n\tmemcpy(my_copy, tab, sizeof(tab));\n\tanother_fun(my_copy, sizeof(my_copy));\n}\n\nAnd if I had time, I would gladly write those damned bindings myself... But it is a s**tload of work, to write and then maintain them... I'm just amazed why each little toy language has some maniacs dedicated enough to write Qt bindings, and the language that is _THE_ most widely used on the primary open source platform doesn't."
    author: "slacker"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Because C people are to lazy to write it :-P ?? If it is the most used then there most be someone to write a binding, why would someone that uses another language make a binding for C people?"
    author: "awaka"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "Anything you write in c can be compiled with a c++ compiler. \nSo just use the c++ libraries"
    author: "ac"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "As I wrote above, WRONG!"
    author: "slacker"
  - subject: "Give it time!"
    date: 2008-10-31
    body: "Just give it time. From a developer perspective, the KDE 4 codebase is much better, they've cleaned up the KDE 3 codebase and ready to introduce new features. It just needs more time for bugfixing.\n\nDid you remember how long did windows xp to become stable enough? Or did you remember when OS X 10.0 first released it was like crap? Or Gnome 2.x which became stable and (a bit) usable after 2.10 or something? KDE 4.x is still less than one year, so give it time! In the meantime, you can freely use your KDE 3.X.\n\nFor me, KDE 4.1 is quite usable even though I miss some little features here and there."
    author: "fred"
  - subject: "Re: Give it time!"
    date: 2008-10-31
    body: "It is very cleaned/fresh and pretty, but lacks some not-so-common features from KDE 3.5, this is why people hate KDE 4.\n\nI think that KDE PIM needs more polishing."
    author: "Miha"
  - subject: "Re: Give it time!"
    date: 2008-11-02
    body: "\"In the meantime, you can freely use your KDE 3.X.\"\n\nNot if the distros for some reason have decided to drop it in favour of KDE4.\n\nAt home I use KDE3 on my gentoo systems, at work there was a decision on using Ubuntu, and as of 8.10 KDE3 is no longer an option. Which pisses me off really, since there are other non-KDE-related issues that are at last fixed with 8.10."
    author: "Kolla"
  - subject: "Re: Give it time!"
    date: 2008-11-02
    body: "\"...at work there was a decision on using Ubuntu, and as of 8.10 KDE3 is no longer an option.\"\n\nThen stick with Hardy. It's going to be supported for another year."
    author: "Leonardo Aidos"
  - subject: "Re: Give it time!"
    date: 2008-11-03
    body: "Didn't you read the rest of that sentance?"
    author: "Kolla"
  - subject: "Re: Give it time!"
    date: 2008-11-02
    body: "That is unfortunate. The best you can do is tell your employer about your issues. Consider if you employer moved to Vista, you wouldn't go complain to microsoft, you would tell your boss your objections."
    author: "txf"
  - subject: "Re: Give it time!"
    date: 2008-11-03
    body: "Why all this dodging of the point? I work in the IT department, I'm part in selecting what we use, and Ubuntu 8.10 fixes alot of this for us, but screws around anyone who are using KDE, who btw are a minority in the first place.\n\nWhy cant Ubuntu come with KDE-3.5.x as well, given that it supposed to be supported for another 18 months?! I find it really remarkable,\n\nAnd just to screw things up even more, ~/.kde is used by both KDE3 and KDE4, so anyone unfortunate to switch between a hardy and intrepid will have a whole lot of mess to deal with. Thank you very much."
    author: "Kolla"
  - subject: "Re: Give it time!"
    date: 2008-11-04
    body: "This is kubuntu's fault. By default kde uses .kde4\n\nUnfortunately kubuntu is really under resourced so they can't support two desktops. Blame Canonical. If you care about the kde users then you should really go for a more DE neutral distro like suse (which btw currently offers kde 3.5 ..though I think they are dropping it in 11.1) or mandriva (dunno if they offer kde 3.5). \n\nWith your position, you could help lobby canonical for better kde support. Or not, after all the kde users are a minority (and who cares about those? :/ )"
    author: "txf"
  - subject: "Re: Give it time!"
    date: 2008-11-04
    body: "No, by default we use .kde for both KDE3 and KDE4. KDE supports the upgrading of config files between major versions. Any failures encountered after the upgrade of config files are KDE bugs, and should be reported as such."
    author: "Jonathan Thomas"
  - subject: "Re: Give it time!"
    date: 2008-11-06
    body: "I meant that vanilla kde uses .kde4 by default "
    author: "txf"
  - subject: "HERE IT IS!  KDE3 repository for Ubuntu 8.10!"
    date: 2008-11-04
    body: "http://apt.pearsoncomputing.net/\n\nPass it on!\n\nI had a problem with KDM, had to disable it to log on, but there have been no other reports of this, and everything else has been beautiful.  I'd recommend that you use it with Ubuntu, not Kubuntu.  I can't vouch for how these KDE3 apps will coexist with KDE4.\n\nContact the administrator with any questions or concerns.\n\nI think what we're going to see is that, as distros drop KDE3, unofficial repositories will come into existence to take up the slack."
    author: "blackbelt_jones"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "It's shocking how much crap open source developers have to deal with. I feel for them. If I ever start contributing code to KDE (or any other OS project), remind me to never read unmoderated public comments, because it'll just destroy my motivation and infuriate me.\n\nOf course, everyone's entitled to their opinions, but dammit, after repeating the same thing 200 times I got sick of it. If you truly think there is no future and hope for KDE4 then don't waste your time hanging around. After all, according to you, it's a lost cause.\n\nI for one see potential in KDE4 and will continue to encourage the KDE developers in a positive way."
    author: "mass"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-05
    body: "And that's why amateur open source is useless. Imagine a MS developer refusing to go to work just because uninformed morons have bashed Vista to death."
    author: "mayhem"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "My opinion. Simply my opinion...\n> When you have a truly visionary direction, it's hard to get people on board.\nSo you understand that the KDE people have a truly visionary direction. Good!\n\n> Meanwhile, all the other desktop platforms are trooping ahead\nList them, please:\n* GNOME: Quite a while and all small changes. Seems in the realm of being on life support.\n* Windows: Seriously dragged down by an awful code-base and a serious case of DRM sickness.\n* OSX: Out of all of them, OSX seems to be the only fast-moving desktop besides KDE.\n"
    author: "Michael \"Opinion\" Howell"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "I basically agree with Fireproof Bob.\nKDE 4.1 is very, very nice to look at, but almost useless to work with. \nI say this with a heavy heart, since I consider myself as a KDE user, not a Linux user. I've even considered using Windows, as long as I could take KDE with me.\n Of course, I haven't given up yet. I will try every new version and hope that it will be functional. Maybe 4.5? (Of course it also depends on whether Amarok will be fixed or not. 2.0 is just awful, and I don't suppose 1.4 will be supported for ever...)"
    author: "tryfan"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "\"KDE 4.1 is very, very nice to look at, but almost useless to work with. \"\n\nPlease give specific examples and things that you think could be improved. I'm using KDE 4 to work and I don't really find it \"useless\"."
    author: "Luca Beltrame"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "I only installed kubuntu on my Laptop for testing and haven't done any work on it yet, but two times out of three,something kde-related crashes on startup. Knotify crashes almost every time I shut down. When it doesn't crash, shuteown doesn't work and I have to kill X manually. Kontact crashed after three clicks. Mac OS menu bars are no longer supported, but it doesn't fall back to normal menus automatically, so right now, I don't have any access to menus whatsoever and no apparent way to get it, except maybe by deleting my entire profile. I could go on, but I think this is already sufficient for calling it unusable"
    author: "zonik"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "1) Amarok 2. That would be enough for me to call KDE4 unusable, but of course I can use Amarok 1.4 - as long as it is being developed. For some people, it may not be important at all.\n2) Dolphin.\n3) The desktop, with those awful \"icon\" thingies. At this point, it doens't work as a regular desktop metaphor at all. The Folder Views might come to something, but they are not there yet.\n4) KickOff. (And Lancelot doesn't seem much better).\n5) The Panel.\n\nOK, everyone says that it will become better. Fair enough, that's why I said I will keep on trying. Alas, at the moment it's so bad that I can't  even use it on a day-to-day basis, which also means that I can't even file bug reports.\nAs of yet, it's no big problem, since I can still use KDE 3.5.x. \nBut in reality, you have to upgrade your distro every now and then, and for how long will you be able to do that, while still keeping 3.5?"
    author: "tryfan"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "Maybe you should tell the people responsible for your distro. However there will probably be third party packages or distros catering to kde3, at least for a while"
    author: "txf"
  - subject: "Windows 7 copying KDE4 not KDE3..."
    date: 2008-10-31
    body: "Strange! KDE4 is a crap, and KDE3 is the best DE ever (you think). But MS is copying large parts of the UI for Windows7 from KDE4/plasma not KDE3! Maybe KDE4 isn't that bad as you think...\n\nI'd say KDE4 is way better than KDE3. It only takes time to show it's full power.\nKDE 4.0 was a release to make the KDE-Developers happy (after 2 years of silent hacking, they needed tho show and use something of their efforts).\nKDE 4.1 is pretty nice. Only a few rough edges and missing ported applications (Amarok, Digikam, KDevelop, KOffice,...).\nKDE 4.2 will be better than KDE3 ever was. That's what I honestly belive."
    author: "Birdy"
  - subject: "Re: KDE 4.2 better than KDE3 ever was?"
    date: 2008-11-02
    body: "I use the SVN. I have the KDE 4.1.71. That is pretty much how KDE 4.2 will look and feel. It will not be \"better\" than KDE 3.5.x, it will not even be close.\n\nI used to like the KDE desktop. Now I (ab)use XFCE4 and a few KDE apps like kate."
    author: "xiando(tm)"
  - subject: "Re: KDE 4.2 better than KDE3 ever was?"
    date: 2008-11-02
    body: "if you don't like it, you don't have to use it.  I to am using \"4.2\" and I consider it to be well in front of 3.5."
    author: "R. J."
  - subject: "Re: KDE 4.2 better than KDE3 ever was?"
    date: 2008-11-03
    body: "I'd love to hear on what points 3.5.x does better than 4.2 as I would have a hard time coming up with things. Konqi is pretty much back, and has several big advantages (not the least speed), dolphin rocks, Gwenview is 10 years ahead, and Plasma kicks kickers's ass anytime, anyplace. KWin - not even in the same league with all the graphical stuff, I love it. Even apps like KWrite got a lot better, as did the file-open dialog. What the heck is it you miss?"
    author: "jospoortvliet"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "KDE4 is a great technology and it helps you to define the future of the desktop. It is true that the transition is not that smooth but progress is taking place in a very rapid way. KDE4 is going to rock."
    author: "Andr\u00e9"
  - subject: "4.0 - Developers; 4.1 -  Distros; 4.2 - User"
    date: 2008-10-31
    body: "I think that KDE 4x has a great future. But, as a lot of people have said, it takes a while to have all the functionality of KDE 3 in the KDE4 branch.\n\nI think that:\n- KDE 4.0 was aimed at developers, in order to get them with something stable to develop for.\n- KDE 4.1 will be used mostly by distributions (see kubuntu, openSuse, etc.). Some of the main KDE programs (amarok, digikam, koffice) will be in beta phase.\n- KDE 4.2, with most of the functionality of KDE 3.5 I think it will become a great source of pride for all the community."
    author: "Alberto"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "While I disagree with most of the post, and the way it was written (too agressive) I just find the image \"don't look back because KDE 3 was better\" a lot of funny... and true [for now] ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "You hit the nail, that's the way I feel to.\n\nStarting for the \"I won't remove the cashew even if you fill bugs and provide patches\" attitude, they are deff, they are full of them self, and they are failing big time.\n\n"
    author: "Robert"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-10-31
    body: "You're totally wrong. KDE4 is growing and it doesn't have full functionality of KDE 3 yet. But, it's far more advanced than Macos or Vista - never met with such great number of very high quality apps in Vista or in Macos of course. Linux with KDE 4 is much faster than them and looks better. I agree that KDE shouldn't go GNOME's way, but it seems that it's going in some part - Amarok 2, Dolphin..."
    author: "Pawel"
  - subject: "Completely agree"
    date: 2008-10-31
    body: "I completely agree that Kickoff is a usability nightmare.  I really believe the reason that some people say they preferred it was due to search functionality.  I believe you can add search without developing a cumbersome menu that is a pain to navigate.\n\nI completely agree that Dolphin is watered down.\n\nI completely agree that in many ways KDE 4 today still lacks functionality I've come to expect, but KDE historically has spoiled me with a fantastic desktop.\n\nHowever, I also completely agree with most of the responders that your post is largely dead wrong, worthless at best, and poisonous at worst.  As a KDE 4 detractor, I can still see many benefits of KDE 4. I can see a future for KDE 4.  I can see tons of progress made since KDE 4.0.  I still disagree with major decisions.  I'm still running KDE 3, and maybe I'll never run a KDE 4 desktop.\n\nHowever, ugly blanket statements that foolishly compare KDE 4 to Vista (seriously!) just take away any credibility you have."
    author: "T. J. Brumfield"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "i completely agree too \nwhen 4.0 was out, it was told that 4.1 will reveal all the promised strength and advantages. Comparisons to 2.0 and 3.0 were drawn ... After 4.1 the very same told to look forward to 4.2. \nAnd now, 4.2 short ahead and kde4 _not_usable_ - i think it should (must) be really obvious for everyone that something seriously goes wrong. \nIs it coincidence or rather misconception to have weather forecasts and realistic looking clocks at the desktop, but a ripped down file manager, cumbersomely KMenu and KControl, an ideological fight about how a user has to use the desktop, much increased line-spacing in konsole, casual desktop crashes and an overall decreased number of config options ... Yes i know ... but really, is it _only_ a problem of man power?\nanyway, for now i can stay with kde3.5 but that won't last forever ... and (sadly) kde4 isn't the alternative\n\n\n"
    author: "abramov"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "\"Yes i know ... but really, is it _only_ a problem of man power?\"\n\nEr ... yes."
    author: "Anon"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Enough! That was the last try i'll give it for the next months.\nThe whole desktop crashes again and again while trying to resize the \"folder containment\" - \nafter automatically(!) desktop restart (seems to be an important feature :-)) the former size is restored ...\nIt's only ridiculous! \nYears ago, i would had brought out in laughter, if someone had told me that he's up to make a desktop environment\nwhere SuperKaramba-like widgets will be able to crash the whole desktop shell. \nI can assure you guys, my laughter now has silenced. \nMisconception. But no one dares to call it by name. Sorry, the original poster somehow does ..."
    author: "Mark Gerstner"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Please, check your system instead of blaming KDE4. I'm not experiencing such crashes in any way."
    author: "Orestes Mas"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Yeah, that's what they used to say those win98 users ... but they weren't right back in time and neither now. \n\nIts a case of design flaw! \n\nDid you really don't got the point? Even if I would misconfigure a widgets behaviour, that _must_not_ crash the whole desktop shell and all other running applications there! \n\nBtw. i barely did anything else with that fresh kde4 installation except for those round about 50 mouse clicks that led to the neverending restart loop.\n\nAs said before - it's ridiculous!\n"
    author: "Mark Gerstner"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "I've yet to have a crash, but I am running OpenSUSE and as far as KDE goes, they do it far better than any others in my view.  And have recently installed the unstable branch for 4.2, it is looking amazing, and rather stable.  Honestly if you have problems try a different distro, as I have found some distro's KDE versions are very, well, lacking.  "
    author: "R. J."
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "Hmmm I think you're a bit off here. KDE 3.5 *is* more stable, but I am running KDE 4.1 on my computer and installed 3.5.9 on a friend's computer. I have to say, even with the quirks of KDE 4, I would prefer to stay with it instead of going back to 3.5. The applications, the look, the framework, ...\n\n4.0 was pretty rough and a bit unsatisfactory. 4.1 is waaaay better... bit buggy sometimes, but usually its a pleasure to work with."
    author: "Darkelve"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "I haven't read the discussion but.... You are right: KDE4 as it is right now has not future. Why? because KDE4 is improving very very fast as we speak. To say that KDE5 as it is right how HAS a future would imply that we would be running the current release of KDE4 for all eternity. Which, of course, is not what will happen. It's just as valid to say that OS X, Windows, GNOME etc. as it is today has no future\".\n\nThat said, I REALLY don't understand the whining about KDE4's features. I would understand it if developement was standing still and/or developers were telling users \"STFU you loser,s we have no interest it fixing these issues!\". Anyone who actually follows the developement of KDE would see that it's progressing fast. Very fast. The functionality that is missing is being added all the time.\n\nKDE4 is 10 months old. 10 months. For the first few major releases, OS X sucked hard as well. Today it's so good that no-one wants to go back to OS 9.\n\n\"Meanwhile, all the other desktop platforms are trooping ahead\"\n\nAre you claiming that KDE is standing still? I'm sorry, but you are utterly clueless. No, you really are."
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "> I haven't read the discussion but....\nmaybe you should read the discussion first?\nmaybe there are users with completely different use cases and experiences than yours?\nmaybe there are users feeling kidded beeing assured that konqueror fm will still be available but finding out it now uses the dolphin parts? \nmaybe those waiting for features but all they get aren't the ones they waited for?\nmaybe pretty much of those care a sh** for how stable macOS was or is?\nmaybe stability has a totally different significance to them?\nmaybe jannee...\n"
    author: "Mark Gerstner"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-01
    body: "\"maybe you should read the discussion first?\"\n\nI did read it afterwards, and I made some additional comments.\n\n\"maybe there are users with completely different use cases and experiences than yours?\"\n\nAnd maybe there are users with different use cases and experiences than yours? So what's your point? That only your use-case is valid?\n\n\"maybe there are users feeling kidded beeing assured that konqueror fm will still be available but finding out it now uses the dolphin parts? \"\n\nSo what? Is the problem that Konqueror in KDE4 does not do everything Konqueror in KDE3 does? \n\n\"maybe those waiting for features but all they get aren't the ones they waited for?\"\n\nLike I said: KDE4 is 10 months old. 10 months. KDE3 is now 6.5 YEARS old. Which of those two has had more time dedicated to it?\n\nWhen KDE2 or KDE3 was 10 months old, it was crashy and buggy as well., and it was lacking a lot of the features we took for granted in the latter releases. \n\nAnd why are they just twiddling their thumbs and waiting? Why aren't they doing anything? WHy are they just sitting on their asses, and making demands to the developers?\n\n\"maybe pretty much of those care a sh** for how stable macOS was or is?\"\n\nMaybe you should try to understand that I was merely using MacOS as an example of a release that at first attracted a lot of complaints, but in the end turned out to be something great?"
    author: "Janne"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "not to mention that kde3 was only an extension of kde 2 meaning that it had a far easier path to stability. Kde4 is brand new. \n\nFrankly I hope all the whiners do move on, so we don't have listen to their moaning. They're always welcome to come back when it suits them..."
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "well there is on doubt tahat KDE 4 at this stage is immature...\n\nhowever i don't understand such voices.... its been said 1000times why They (KDE.dv and developers) decided to work on project 4 in that way... in modern desktop history there are no such example of such a huge technology jump and only open development is a way to attract users and devs to switch and somehow stabilize new platform...\n\n1. I personally and my 23 colleges in our company are working on KDE 3.5.10 and CentOS 5.2 (very good suspend support on laptops).\n2. Our IT team is testing KDE 4.1.2 on F9/F10 Rawhide... especially KDE PIM\n3. in mid of the 2007 we were planing to switch around KDE 4.3.1/2 now it will be probably 4.4.1/2\n\nEvery body has a right to complain cos that's a beauty of FOSS... but it will be nicer if users were more patient and try to understand whole ecosystem...\n\neg. I'm annoyed by the Dolphin/Konqueror case... however I have to accept free will of developers... You cannot changed a nature...\n\nKDE dev keep on going... in next 12-14 months there will be a very good desktop on the market..."
    author: "wilq"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-02
    body: "\"Meanwhile, all the other desktop platforms are trooping ahead, further empowered by an infusion of development support and constantly swelling userbase thanks to the monumental /faux pas/ called KDE4.\"\n\nhttp://cia.vc/stats/project/KDE\nhttp://cia.vc/stats/project/GNOME\nhttp://cia.vc/stats/project/xfce\n"
    author: "Anon"
  - subject: "Here it is!  Unofficial KDE repos for Intrepid!"
    date: 2008-11-04
    body: "http://apt.pearsoncomputing.net/\n\nFireproof Bob:  I love that banner.  I always thought there was a strange conflict between the two slogans \"Be Free\" and \"Don't Look Back\"  My mock KDE 4.1 banner would say:  \"Be free to look back\"!\n\nNobody hates KDE4 more than I do, but why ruin somebody else's fun?  KDE could be a spectacular success, or a spectacular failure, and either way the community could benefit.  If it were to turn into an abortive attempt, however... that would benefit no one.\n\nI say we let KDE4 go wherever it's going. It falls to us who are not seduced by WIDGETS! WIDGETS! WIDGETS! to defend the bridge back to sanity that is KDE3 against those who would burn it down. When they say (as they said of Intrepid) \"you can't run KDE3 on this!\" Run it anyway... and then make sure anybody who wants to can run it as well.\n\nKDE 4 is not the issue.  KDE 4 is a brave and noble experiment.  The issue is the continued viability of KDE 3.  When it comes to KDE 3, I'm gonna fight, for my right, to paaaaaarty!!!\n\n "
    author: "blackbelt_jones"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "I've given my opinions about KDE before. I wasn't listened to. That doesn't mean I should go and cry and whine. If KDE sucks, then it'll be back to the good old days of bb and everything else customized to the T for me. I'm not too worried. KDE was very good while it lasted all those years. If KDE4 is where it begins to suck, then fine, I won't use it anymore. It'll be time to explore other software - (not Gnome... gag, choke, sputter)\n\nHOWEVER\n\nLet the devs make what they want to. Come on. It's their creation. You whiners act as if your opinion so valuable. Well, f0ck that. It's not. If the devs are making a mistake, let them. They can be their own demise. I make music. Many times I couldn't give more than one or two thoughts to someone's opinion about my creations. Because there are those that like them and there are those that don't. If you don't like it, then that's fantastic. It just means that there are people different that any of us. Isn't that great?\n\n(What an RL n00b...)"
    author: "winter"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "What a sensible comment...you don't belong here GTFO"
    author: "txf"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-06
    body: "hahaha... The moderator is annoyed. =)\n\nBTW: http://dot.kde.org/1225379191/1225934313/"
    author: "winter"
  - subject: "Re: KDE4 as it currently exists has no future."
    date: 2008-11-04
    body: "KDE4 is not bad. The problem is, current state of KDE4 should be a RC."
    author: "Laura"
  - subject: "I think you've got it backwards."
    date: 2008-11-04
    body: "I think KDE4 has a brilliant future.  It just doesn't have much of a present.  \n\nAnd that's a problem because apparently some of the major distros have decided to shove it down our throats, although all the data I've seen indicated that KDE3 is what a slight majority of KDE3 users are currently using.  It's not as much of a problem as it could be, because the community will pick up the slack.  Within days of the Kubuntu Insipid Ipex release, (NO KDE3 OPTION!)  There were KDE3 repositories appearing, and now I'm running Intrepid Ipex with KDE3.5.10... and I love it.\n\nKDE4 is the focus of development as long as KDE3 will be supported, and as long as people use it, it will be supported.  \n\nhttp://aseigo.blogspot.com/2008/01/talking-bluntly.html\n\nThere's a lot of myths and misconceptions about that.  People think that KDE3 is about to be yanked out from under them, and it isn't.  I think this is the big problem.  Speaking for myself, KDE4 isn't an issue if I don't have to use it. Right now, using KDE4 would be like moving into a house without a roof because I think the kitched is lovely. \n\nI have my reasons for believing that KDE4 could be KILLER one day, but I see no reason to believe that it's not going to be a long long long rollout.  \n\nI don't expect new features in KDE3.  I'm still discovering awesome new uses for the old features.  All I want is to be able to count on security updates and bugfixes.  And I finally learned that I have always had that right.  But I really had to dig to get that information.  In my opinion, the KDE team has handled everything perfectly down the line EXCEPT to explain to the public what they're doing.  I've said this before:  professional wrestlers probably make better relations people than software developers.\n\nThere's no real reason for anybody to be upset, unless you're upset that KDE3 isn't going to get many new features, and I think that's not reasonable.  The KDE3 team is too talented and too ambitious to focus their full attention on \nfixing something that is just so perfect, in it's limited 20th century way.  They'd probably wind up breaking it.\n\nI can't speak for anyone else but when I complain about KDE4, the subtext is that I fear for KDE3.  But once again, I can see that KDE3 i9s gonna be just fine, Kubuntu be damned.\n\n\n\n"
    author: "blackbelt_jones"
  - subject: "KDE3 is still being maintained!"
    date: 2008-11-04
    body: "So many people don't seem to understand that.  I didn't understand it until I read this.\n\nhttp://aseigo.blogspot.com/2008/01/talking-bluntly.html\n\nSee?  No problemo.  "
    author: "blackbelt_jones"
  - subject: "Just a constructive comment"
    date: 2008-10-30
    body: "I agree that KDE4 is taking to long to be final, but its a big codebase, the devs are trying to rewrite & improve 6 years of work. from  3.0 till 3.5.10, it's an unpleasant status quo situation, but hey, you have 3.5.10 just 2 monts old.\nAs for the features & looks, seems like no one remmembers vanilla kde3 in arch & slack, with the settings chooser at first start, i don't think a large group of people used vanilla kde setup. KDE4's look is about apealing default for most users, and friendly configs. The features will come"
    author: "Maki"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "That's correct, but my distro (Kubuntu) just force fed me an unstable and incomplete KDE4. And no, i cannot stay with Hardy, the previous release. I need many other upgraded non-kde packages from the new release. So now, I, a long time KDE user that installed KDE on so many computers for family and friends, am thinking about installing GNOME, which is now very polished. This is the point."
    author: "Flavio"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "Then chose another distro. Opensuse, for example, ships both."
    author: "Sebastian"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "Ok great! Just what I wanted. Reinstall another distro. In practice you're admitting Kubuntu has failed to deliver a usable distro."
    author: "Flavio"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "Maybe an unusable distro *for you*, not in general. And that failure is quite different from KDE 4 being a failure, isn't it? Go complain with Kubuntu that you prefer to use KDE 3.5.x, but don't complain with KDE that 4 sucks because your distro forces you to use it and you don't want to switch to a distro that does allow you to use it. \nBTW: you can always compile a vanilla KDE 3.5.10 on your Kubuntu if you want. Nobody is stopping you. "
    author: "Andr\u00e9"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "I'm talking about kubuntu. Never said that KDE4 sucks in general, i said that THIS release is not ready and Kubuntu force fed me with it. Is it clear?"
    author: "Flavio"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "So which Kubuntu member forced you to upgrade at gunpoint? He will be severely reprimanded! Hardy will still be supported for another year if you don't want to upgrade."
    author: "Jonathan Thomas"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "Since many find this Kubuntu release usable, this is clearly not the case. He only points out that there are alternatives that would suite *your* needs better.  \n\nSo for you it's a no brainier. According to you Kubuntu don't deliver the product you need, so it's rather obvious that you should change to something else that does. "
    author: "Morty"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "KDE 3.5 is still available on Kubuntu, right?"
    author: "jospoortvliet"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "For 8.10, no - you'd have to compile yourself or get some third-party packages."
    author: "Anon"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "And the problem with that is...? It's really not that hard to compile KDE yourself, it just takes a bit of time."
    author: "Andr\u00e9"
  - subject: "Re: Just a constructive comment"
    date: 2008-11-02
    body: "Time = money. People don't want to waste either. Yes, compilation is automatic, but there are plenty of packages you need to compile, and for that you have to manually unpack sources, configure, make, make install for each. And don't forget that \"./configure\" will sometimes give you errors that some headers were not installed, so you need to supervise all of this, since a script/chained-command-line would simply stop at that point."
    author: "Phd student"
  - subject: "Re: Just a constructive comment"
    date: 2008-10-31
    body: "3.5.10 backports are available for anyone who doesn't want to switch to 4.1.2"
    author: "Anon"
  - subject: "Re: Just a constructive comment"
    date: 2008-11-02
    body: "Isn't that a forwardport then?"
    author: "Stefan Majewsky"
  - subject: "Re: Just a constructive comment"
    date: 2008-11-03
    body: "Where?\n\nI dont see any intrepid in http://archive.ubuntu.com/ubuntu/pool/main/k/kdebase/"
    author: "Kolla"
  - subject: "Re: Just a constructive comment"
    date: 2008-11-03
    body: "# Pearson Computing KDE3.5 Repository\ndeb http://apt.pearsoncomputing.net/ intrepid main\ndeb-src http://apt.pearsoncomputing.net/ intrepid main\n"
    author: "Wouter Deconinck"
  - subject: "Re: Just a constructive comment"
    date: 2008-11-02
    body: "So does Kubuntu not provided the other non kde packages you need without having to update KDE?  I know OpenSUSE do.  \n\nI tried Kubuntu's release, and honestly, compared with the beta 4 released of Opensuse's 11.1, I couldn't believe how more stable opensuse was, considering that it was a beta release, and how far better they had modefied KDE"
    author: "R. J."
  - subject: "YankeeOS?"
    date: 2008-10-31
    body: "I have never seen such a buggy and unpolished distribution like this Kubuntu release. The whole distribution is a terrible mix of languages since your great piece of closed source software called \"Launchpad\" distroyed the work of so many kde translators once again and even worse than ever before. You tell the people the Kubuntu has to fit on one CD to make it possible to use in counties still in development where DVD drives are not so common but you have no problems releasing a distribution which is unusable for people not speaking English... There are regions in the world, where you might have the posibility to use a simple computer but still won't have the chance to learn english. It's a shame what Launchpad does to KDE's translations.\n\n"
    author: "TorresSur"
  - subject: "Re: YankeeOS?"
    date: 2008-10-31
    body: "I can agree that!\n\nIn Slovenian, for instance space is translated to vesolje (universe, which is similar word to space), but in fact space key has to be called 'preslednica'. \n\n"
    author: "Miha"
  - subject: "Re: YankeeOS?"
    date: 2008-10-31
    body: "\"It's a shame what Launchpad does to KDE's translations.\"\n\nYes, this is sadly true. Kubuntu was always one to the worst translated Distris, but in Intrepid this is a pure disaster.\n\nSimply embarrassing!\n\n \n"
    author: "SONA"
  - subject: "en-ca and en-au \"translations\" messed up Amarok"
    date: 2008-10-31
    body: "My grip with launchpad translations: for like 12 or 18 months (at least, I'm pretty sure its fixed now) the status bar in Amarok 1.4 would show HTML code in the Canadian and Australian translations. It was like no one cared or had the ability to do anything about it.\n"
    author: "Ian Monroe"
  - subject: "Translation Central"
    date: 2008-10-31
    body: "openSUSE developed a fantastic build respository allowing any user to build software on their servers, even for other distros.  It really is a great tool available for the whole Linux community.\n\nI've long wondered if the FOSS community needs a central translation repository.  Major projects like KDE have a translation team, but what about small individual apps outside the KDE servers?  What about individual distros?\n\nIf I can commit the strings from my distro or app in a central place where anyone can translate them, I think it would help things a great deal.  In addition, simple single-word translations perhaps could just be plucked from the repository.\n\nInstantly my app could pick up say 70 different languages translating copy and paste because someone already translated those strings for other apps."
    author: "T. J. Brumfield"
  - subject: "KDE4 is unusable"
    date: 2008-10-31
    body: "I update KDE4 once per month and give it a try, each time I end up back to my beloved KDE3. I've never seen a product make so many regressions from one version to a new one, we really set a record there and if Firefox now is on the Guiness book, so should be KDE4 too. \n\nNow I will wait until version 4.2 to give it another try, hopefully I can use my second monitor by then without problems and, read it carefully, do advanced printouts with kprint, like printing only odd (or even) pages! \n\nI also wonder what happened to all those benchmarks showing how faster and with a smaller memory footprint KDE4 was, oh boy, I bought into that!\n\nCertain things are also a waste of time and effort, like the composite effects. They never worked reliably, neither in the long history of KDE3 nor do they now in KDE4. They're hyped to dead, but they don't work and if we never managed to make the simple composite effects of KDE3 work well, can we expect to see them working reliable in the near/middle future? \n\nImagine if QT were 100% controlled by the community, you could be able to only maximize windows, but not minimize or close them until the next release :)\n\nPoor Kubuntu users, all four of them.\n\n\n\n\n\n\n"
    author: "Petar Popara"
  - subject: "Re: KDE4 is unusable"
    date: 2008-10-31
    body: "In my experience the KDE4 desktop effects work way better than the KDE3 ones ever did. The KDE3 ones were horrible. I've never had any trouble with the KDE4 ones, and if you have, KDE/Kubuntu/any KDE distro would much prefer you to report it rather than making vague \"it sucks\" comments."
    author: "Jonathan Thomas"
  - subject: "Re: KDE4 is unusable"
    date: 2008-10-31
    body: "Printing odd pages is for experts only, eyecandy is for everyone!\n"
    author: "vf"
  - subject: "Re: KDE4 is unusable"
    date: 2008-10-31
    body: "Please provide us a link to the desktop environment you have programmed in your spare time so that we can properly compare it with KDE 4.\n"
    author: "Jordi"
  - subject: "Re: KDE4 is unusable"
    date: 2008-10-31
    body: "That's right! Because taking the time to try stuff out and give feedback, most likely indicating that not every whizz-bang feature endlessly blogged about is necessarily advantageous, is clearly a negligible contribution to the effort: how dare such a Lilliputian stand amongst the \u00fcber-hacker giants whose priorities and better judgement are not to be questioned!"
    author: "The Badger"
  - subject: "Re: KDE4 is unusable"
    date: 2008-11-01
    body: "Providing constructive feedback is great. Basically saying \"it sucks\" is NOT constructive feedback."
    author: "Janne"
  - subject: "Yeah, but to be fair, it does suck.  However..."
    date: 2008-11-04
    body: "Okay, here it is.  At the moment, almost every plasmoid is essentially a glamorized applet.  But the plasmoids require a lot more maintanance than the KDE3 applet... adjusting, resizing, moving around, and, inevitably, this never-ending locking and unlocking.  It's the same amount of function, for more work.\n\nBut I do think it shows some promise. There have been a lot of studies lately about how multitasking really isn't as effective as people think.  Now, we all know that your everyday GUI desktop environment is a hotbed of multitasking. Everything has to be up front.  Now Someday, in KDE, we may see the ability for mutiple desktops built for singletasking.  In other words, if I can arrange those plasmoids for writing, or for research, or for blogging, or for any single task, and if I can save those configurations and call them back, I can create a very focused and effective work environment.\n\nhttp://www.npr.org/templates/story/story.php?storyId=95524385\n\nHere's a seven minute NPR story that convinced me that KDE4 may have a future, even if it doesn't have much of a present"
    author: "blackbelt_jones"
  - subject: "Re: Yeah, but to be fair, it does suck.  However..."
    date: 2008-11-04
    body: "> . . . if I can arrange those plasmoids for writing, or for research, or for\n> blogging . . .\n\nI've been following this dicussion out of curiosity. I'm normally a Gnome user who tries out each new version of KDE and plays with it occasionally. I suppose it's shallow of me, but the main selling point of KDE for me has been multiple desktop backgrounds. So cool! And the lack of same in KDE4, in addition to the general confusion and lack of obvious configurability, has been offputting. But your reference to singletasking, and the article link, is extremely interesting. It's the most commonsense, useful commentary to come out of this whole discussion, much of which is b.s. I'm intrigued at the idea of setting up my own desktop in this way. If this is what the developers had in mind, it's a shame that developers are usually better at designing things than explaining them. Or so it seems to me. Thanks for your thoughts. I thought I was being self-indulgent in following the discussion this far - damned multitasking! - and suddenly get something out of it."
    author: "Dale Huckeby"
  - subject: "Re: KDE4 is unusable"
    date: 2008-11-07
    body: "Hello? The guy tries out KDE 4 \"once per month\" (a not inconsiderable investment if he has other things to be getting on with) and points out concrete issues with the experience, and this is the same as just saying \"it sucks\"? Would it be kinder to the developers for the guy to just shut up about it and go and use something else? Typically, a chorus of self-congratulation (\"behold our vision!\") is exactly how a lot of software ends up alienating potential and existing users."
    author: "The Badger"
  - subject: "Re: KDE4 is unusable"
    date: 2008-11-02
    body: "I have to admit that twin monitor setup does make some application-switching effects terribly useless. Also, Plasma messes up when dragging plasmoids from one screen to the other. I haven't reported these as bugs, and I feel ashamed for not doing so. \n\nBut. I think KDE4 rocks. I have been using it since 4.1.1 (until then, it was too unpolished, and I stuck to KDE3) and now at 4.1.2 it is very-very stable, never crashed on me, and lets me do everything that I want to do. It wasn't that easy to get used to it, but it is better.\n\nTrue, KDE4 is missing features - it was a great idea to backport task manager pop-ups, for example. And I have a trillion other things that I miss if you tell me that it is missing. But it's great as it is!"
    author: "Phd student"
  - subject: "KDE4 is coming along nicely"
    date: 2008-10-31
    body: "I switched to KDE4 at 4.1 and will say it's coming along nicely. I really look forward to all the apps being native without the use of kde3 libs (Amarok being the primary! Kaffiene a close second.) But I know this takes time, so please  just keep up the great work!\njetpeach"
    author: "jetpeach"
  - subject: "Re: KDE4 is coming along nicely"
    date: 2008-11-02
    body: "You could download those apps, Amarok 2 is working rather well, I haven't had any problems with it, and there are K3b, Kaffiene builds for KDE4 that work great.  Well, at least they do on OpenSUSE"
    author: "R. J."
  - subject: "Re: KDE4 is coming along nicely"
    date: 2008-11-04
    body: "I can only second that and like the author of this thread said, it's coming along nicely. I use KDE4 about 90% of he time now. I use KDE 3.5.10 sometimes but not without KWIN 4 for example. KDE 3.5x is rock solid but it's beginning to show age just like XP in the Windows world. Right now KDE 4 has everything, not perfect but that will come in due time.\nWhat really gets on top of my nerves are the whiners and waggonists."
    author: "Bobby"
  - subject: "My comment"
    date: 2008-10-31
    body: "I only want to say that i like KDE4, i like the ideas behind it, and i like the future it has. I don't mind if KDE4.0 would be called beta, alfa, or gamma. I know that lacks some features, but i don't mind, because i can live for a while without them, knowing that all of them are now in SVN.\n\nDevs, thanks, and keep on going!!\n\nBye"
    author: "Dienadel"
  - subject: "Re: My comment"
    date: 2008-11-02
    body: "wait until you see 4.2, it is coming along beautifully.  I personally think running it on OpenSUSE that 4.2 is 10 times better than 4.1 :)"
    author: "R. J."
  - subject: "KDE 4.1.2"
    date: 2008-10-31
    body: "is the best KDE ever. I really enjoy working with KDE 4.1.2. As a whole KDE is very stable and looks way better than anything Windows has to offer (and KDE 3.5 btw). I think it was also the right decision to change from Konqueror to Dolphin for the standard file management (less clutter).\n\nThere is only one big downside: The KDE implementation in Kubuntu is by way the worst of all distributions I have used (Mandriva 2009.0, Opensuse 11.0)."
    author: "xhit"
  - subject: "Re: KDE 4.1.2, Mandriva solved the problem better "
    date: 2008-10-31
    body: "I agree. For instance Mandriva 2009.0 with KDE 4.1.2 is totally usable for the exercises I usually did on KDE 3.5. Of course some minor bugs here and there, but this will improve by time as more people use KDE 4 and report the problems.\n\nI ask people to be more patient. Rome wasn't built in a day."
    author: "nagyokos9"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "First, how is this a downside of KDE and second, maybe you should try saying why you don't like Kubuntu. Just attacking it makes no good and you know there are people working hard on it, so why talk about the result of their work like this ?"
    author: "Nikos"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "Ok, here are my two favorites:\n\nGraphic Glitches:\nSee http://forum.kde.org/showthread.php?tid=6511&page=1 for details (problem doesn't exist in opensuse or mandriva)\n\nAdept:\nI highly dislike adept. IMO it is very unintuitive (don't try to be unique for the sole purpose of being unique). Look at synaptic and kpackagekit for more intuitive UI.\n"
    author: "xhit"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "Ok, here are my two favorites:\n\nGraphic Glitches:\nSee http://forum.kde.org/showthread.php?tid=6511&page=1 for details (problem doesn't exist in opensuse or mandriva)\n\nAdept:\nI highly dislike adept. IMO it is very unintuitive (don't try to be unique for the sole purpose of being unique). Look at synaptic and kpackagekit for more intuitive UI.\n"
    author: "xhit"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "I have these graphic glitches as well, using Kubuntu 8.10, 4 years\nold 9200SE ATI (RV200) graphics card, Pentium IV 2.6 GHz.\n\nI am very happy that the graphics effect work at all, although they are\nquite slow (glxgears is at 700 FPS here). Open source driver.\nBut, this is actually the first KDE / X / kernel chain that supports all of my\nhardware!\n\nI have the glitches on my ASUS laptop as well, using ATI RV280.\nThere the effects work smoothly (glxgears  at 2000 FPS). Also open\nsource driver.\n\nI have often a slightly different variant of the glitches, when the\nspace where a menu pops out is completely black before being filled in.\n\nIn addition I have the following:\nWhen I start kde, halfway in the progress shown in the splash screen,\nthe whole screen flashes for half a second, and I can actually for a moment\nsee the contents of a PREVIOUS KDE session! \nSomehow KDE / X / whatever seems to be switching the memory of the graphics card?\n\nAbout the bitching going on here w.r.t. the stability  / \nusefulness of KDE4.1.2: I like the progress that\nis being made, and I think I will switch completely with KDE 4.2.1.\nReason is that I like working with a mature desktop for everyday work.\nUntil then I periodically use KDE 4.1+ on another partition.\n\nI think progress costs time, and you cannot expect the KDE desktop to be\ndeveloped behind closed doors and suddenly to be revealed when completely\nfinished. If Microsoft is incapable of that (Vista!) then surely also the KDE\ndevelopers.\n\nThe pain for actual users is almost 0, I would say there is a period of\nhalf a year between now and Q2 next year where we have problems due to\nKDE3.5 (konqueror, ...) getting outdated and KDE4.x being too immature.\nIn all a very smooth process, it is not like somebody is forcing us to \nuse KDE 4...\n\nTo summarize: in my view the big common factor in many complaints is: \nresistance to change. People do not like changes, and that is in my\nopinion a big reason for all the bitching. Must be something in our\nnature!\n\nI like the progress being made and want to congratulate the developers and\nother contributors!\n"
    author: "John van Spaandonk"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "I think you didn't read my post at all. I wrote \"KDE 4.1.2 is the best KDE ever.\" \nSo, while I like the new KDE very much, IMO the implementation in Kubuntu is not as good as in other distributions - so I want to see an improved KDE implementation in Kubuntu.\n\np.s. You are absolute right about \"resistance to change\""
    author: "xhit"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "(My reply was not related to just your post.)\n\nHistorically, Kubuntu was a bit of an afterthought w.r.t. ubuntu.\nThe whole (k)ubuntu development cycle is still slaved to gnome releases, \nand the Kubuntu team is relatively small.\nThe fact that Kubuntu has glitches definitely will not stop the\n(k)ubuntu release. I think this means we can have a good Kubuntu\nrelease a month orso _after_ the official (k)ubuntu release, they\njust need a little more time to fix/backport.\n\nI think that the Kubuntu team does not have the energy to backport as much as e.g. the OpenSuse folks, which have a KDE centric distribution from the start.\nBut personally I do not like the amount of tailoring and backporting that the OpenSuse people do, I like a \"more pure\" KDE.\n\nSo actually I switched back from Opensuse to Kubuntu :-)\n\n\n"
    author: "John van Spaandonk"
  - subject: "Re: KDE 4.1.2"
    date: 2008-12-01
    body: "\"I have often a slightly different variant of the glitches, when the\nspace where a menu pops out is completely black before being filled in.\nIn addition I have the following:\nWhen I start kde, halfway in the progress shown in the splash screen,\nthe whole screen flashes for half a second, and I can actually for a moment\nsee the contents of a PREVIOUS KDE session! \nSomehow KDE / X / whatever seems to be switching the memory of the graphics card?\"\n\nI have exactly the same problems, I have a ragepro graphics card. Sometimes the menu is completely black before being filled in and some times just streaks of garbage like some random memory. GLX runs slightly under 700 fps, but when I open a menu it slows to below 400. I tried experimenting with xorg.conf. It does force changes but none for the better. It works as well without any xorg.conf. There seems to be no way to tweak graphics. "
    author: "dwhitbeck"
  - subject: "Re: KDE 4.1.2"
    date: 2008-12-02
    body: "I removed the panel, which was located at the bottom of the screen. Then I added a panel which defaulted to the top of the screen. I added back the application launcher menu widget. Now menus open quickly rather than taking a couple of seconds as before. I see no obvious way to locate the panel at the bottom of the screen. I assume if I do, menu painting will be slow as before. Strange."
    author: "dwhitbeck"
  - subject: "Re: KDE 4.1.2"
    date: 2008-12-02
    body: "I put the application launcher menu on the desktop. I can put it where I want it. It is fast, almost immediate. "
    author: "dwhitbeck"
  - subject: "Re: KDE 4.1.2"
    date: 2008-10-31
    body: "Graphics:\nThose graphic glitches occur on other distros for me too. Often it is due to nvidia (cached pixmaps appearing on new windows, glitchy krunner and systray) but then there are others which occur for most gfx cards. \n\nAdept:\nI agree, I wonder what was going through the authors mind when he created that ui, with those irritating expanding descriptions, lack of options. Then again you can always use synaptic with Qt-gtk style. Far more functional and polished. \n\nFor an example of a decent kde package manager gui, I suggest Arch's shaman. I quite like it, probably by the time kde 4.2 comes out I'll be using arch, more work initially, but much more flexibility..."
    author: "txf"
  - subject: "Re: KDE 4.1.2"
    date: 2008-11-21
    body: "I agree, Xhit!\n\nI am running Mandriva 2009 Powerpack which offers KDE 4.1.2.   On my new Sony VAIO VGN FW140E/H laptop it is FAST and stable.  The only program that has given me problems is Skype, but I replaced it with Ekiga.  Did I mention it was also a beautiful DE?\n\nDon't like the new menu?  Right click on the menu button and chose the \"Classic Menu\".\n\nI love Dolphin and how it works with mountable devices.  Plug in a USB stick and it first shows in the \"New Device Notifier\" popup, where you can click on it and chose an option.  Or, you can open Dolphin and select it in the \"Places\" panel.  When you are done you can right mouse on its icon in the \"Places\" panel and select \"Safely Remove\". \n\nI love how the \"Cashew\" icon works on the right end of the icon panel, and dittos for the larger \"Cashew\" icon in the upper right hand corner of the desktop. \n\nI love how the KDE settings have been collected in the \"Configure your Desktop\" icon on the panel.  Want to change the date and time on the panel clock? Look under its \"General\" tab for the \"Regional & Language\" icon. \n\nThe KDE Dev crew will be supporting security and bug patches in KDE 3.5.10 but don't expect new apps or improvements in existing apps built with QT3, because QT3 itself is in a security & bug patch only mode.  That means that if someone or some group wants to continue the development of KDE3.x they will have to support the development of QT3 too.  The first problem they will encounter is trying to create new forms (*.ui) without using the QT3-Designer.  Very few coders can do that.  Using the QT3-Designer forces them into an application development mode that is awkward and clumsy, to say the least.  The QT4-Designer, by contrast, is a breeze to use and allows for the more standard form of C++ application development.  The following website:\nhttp://www.potu.com/man/doc.trolltech.com/4.0/qt4-intro.html\noutlines how Qt4.0 is different from Qt3.  Since then Qt4's development has continued to Qt4.4.  This website outlines the MANY improvements in Qt4.4 over Qt4.3.4:\nhttp://doc.trolltech.com/4.4/qt4-4-intro.html\n\nAs one can plainly see, it won't take long before those who dream of continuing the KDE3.5 development tree will be faced with the daunting task of upgrading the capabilities of Qt3 to do what Qt4.4 can do, or face a future stuck in the KDE3.5 past.  They will be faced with the same choice that Trolltech faced: continue to kludge Qt3, with all the bloat and instability and bugs that would entail, or design QT4 from the ground up, and in a modular way.   If putative KDE3.5 developers choose neither then it would be like one deliberately choosing to run Win95 and wondering why they can never use the new hardware or run the new apps."
    author: "GreyGeek"
  - subject: "Holy cow, there's so much annoying nonsense here"
    date: 2008-10-31
    body: "Plasma is still work in progress, but it's better than kicker at it's current state (and Plasma does a lot more than kicker...).\n\nDolphin is a lot better designed for defualt file manager than Konqueror, period. If you like Konqueror mess you are free to use it.\n\nSystem Settings is a better default than KControl.\n\nThe problem is the KDE 3 heritage, those application are a damn mess. Okular, Dolphin, System Settings, Koffice 2, the new KDE 4 apps are quite good.\n\n\n\n\n"
    author: "Luis"
  - subject: "Re: Holy cow, there's so much annoying nonsense here"
    date: 2008-11-01
    body: "+1 "
    author: "mimoune djouallah"
  - subject: "Re: Holy cow, there's so much annoying nonsense here"
    date: 2008-11-01
    body: "Totally agreed. Personally I find most of the arguments everybody here has made completely invalid. \n\nI think they are basically inflexible Dinosaurs, railing that time has passed them by.\n\nSo certain that their world view is the only valid case and that the whole of kde development revolves around them. If they find change and REAL evolution so distressing maybe they should stick to gnome.\n\nWhilst I want kde to succeed, I would rather it have lesser success with fewer idiots than more success with more idiots like those above..."
    author: "txf"
  - subject: "Re: Holy cow, there's so much annoying nonsense here"
    date: 2008-11-04
    body: "I don't want to put down KDE 3.5x apps because they are rock solid and have paved the way for the 4 era but yes they have done their days. The one app that clearly stands out in improvement for me is KMail. \nThe KDE 3.5x version of KMail was a real mess, a buggy heap. When one compares the KDE 4 version with the older version then you just can't believe that they are related. It's not finished yet and still has rough edges but it's already a joy to use."
    author: "Bobby"
  - subject: "Please change the name from KDE to QDE..."
    date: 2008-10-31
    body: "Seriously, KDE is now so depending of Qt that is not even funny anymore, KDE has become a big commercial for our pro-patent \"friend\" Nokia.\n\nI remember when KDE had is own personality, his own spirit but now is just an extention of Qt.\n\nSo I propose to change the name from KDE to QDE."
    author: "Robert"
  - subject: "Re: Please change the name from KDE to QDE..."
    date: 2008-11-01
    body: "What a load of crap. \n\nHow about some proof or at least anecdotes illustrating your point?"
    author: "txf"
  - subject: "Re: Please change the name from KDE to QDE..."
    date: 2008-11-02
    body: "Nokia files patents just like most big companies.  They fought heavily against open standards being used in HTML 5.  That being said, there is no proof that Nokia is going to hurt QT or KDE that I've seen.  They've been vocal in wanting to attract developers to work on FOSS projects and I thought it was a cool move to give away tablets to KDE devs.\n\nIf Nokia tried to close off QT, people would just fork the free version, and the commercial version would likely die off."
    author: "T. J. Brumfield"
  - subject: "Re: Please change the name from KDE to QDE..."
    date: 2008-11-04
    body: "Looks like there are a lot of crabs around here. You know, crabs only have shit in their heads."
    author: "Bobby"
  - subject: "Re: Please change the name from KDE to QDE..."
    date: 2008-11-03
    body: "So gnome is what \"microsffft-agrements\" pro-patent beast novell friend?"
    author: "c'om"
  - subject: "Kubuntu 8.10 is awsome!"
    date: 2008-10-31
    body: "it's fast, and responsive!!! enabling multi-verse repository in adept installer and installing kubuntu-restricted-extras, enables all java, flash, mp3, etc. support in amarok, xine. of course, software patents are not applicable to my country, India.  also visit www.medibuntu.org.\n\nGood news is that kubuntu 8.10 is just great.. latest xorg, kernel and all, simply smooth and responsive.\n\ninstead of reading the rants and complaints, why not try the very well done experience of kde 4.1.2 (and 4.2 will indeed be greater).\n\nThanks kubuntu team and KDE team for the gift!"
    author: "fast_rizwaan"
  - subject: "Re: Kubuntu 8.10 is awsome!"
    date: 2008-11-04
    body: "I tried it yesterday. It's not as polished as openSuse usually is but it's the best Kubuntu that I have touched thus far. What I like about it is the way it handles codecs. A beginner doesn't have to know what to install in order to view his multimedia contents because it installs everything that you need for music and video automatically. I also like the way that they integrated the hard disk partitions in Dolphin.\nI used to complain about Ubuntu's bastard daughter in the past but it looks like she is growing up to be a sexy lady :) Watch out Suse."
    author: "Bobby"
  - subject: "Still problems"
    date: 2008-10-31
    body: "Still a load of problems, but at least it's usable now. There still seems to be a bunch of missing features that aren't to do with the Qt re-write, Plasma, etc, though - like RSS feeds missing from KTorrent, the ability to change the layout in KNode, and so-on. The new Adept is *really* awkward compared to the previous one. CPU usage seems rather high in general. The System Settings seems to want double-clicks instead of single-clicks. I still don't know why I can't change the colour of panels without having to create a whole new theme.\n\nGiven the number of clocks that were written during the testing of plasma, why can't the default clock put the time *next* to the date instead of trying to put it above it? Some of us don't like huge panels (as you may have gathered after the outcry over the panel size in KDE4.0)\n\nGot to be honest, I just don't *get* plasma - as a rewrite for Kicker it's fine, but what's the point of putting a load of stuff directly on the desktop? The whole point of having windows is that the desktop is usually partially or totally obscured. You've ended up with 2 totally different (and not very compatible) paradigms for partitioning tasks on the desktop.\n\nI've no doubt I'll be classed as a whiner for daring to not like everything about it, but I've tried to be constructive about things that are broken in either or both KDE4.1 and Kubuntu Intrepid, and I say this as a one-time (minor) contributor to parts of KDE."
    author: "Anonymous"
  - subject: "Re: Still problems"
    date: 2008-11-02
    body: "It's fine if you don't get it. Just use it the same way you used kicker. But plasma is less about simply widgets on the desktop. It is more of a Gui constructing framework. \n\nThis essentially means that you can make the shell the way you want. Granted it is currently quite limited, but expect to see later more divergent concepts. The plasma devs are also working on a mid gui using plasma. \n\nRegarding partitioning tasks. It is my personal belief that widgets on the desktop are a more passive rather than active form of interacting with data. Basically they are there to check and not to be used. There is some blending between active and inactive but generally the focus is on data visualisation and with standard windows data interaction. \n\nI don't think you're a whiner. Simply because you have been honest and not spouted vitriol like some of the other posters. It is alright to complain as long as you are clear to what you want exactly. Even better would be to file a  bugreport when requesting a feature to be implemented (like the clock).\n\np.s. I agree about adept...But I prefer to give it some time to see where it goes. You can always use synaptic if you need a gui...  "
    author: "txf"
  - subject: "Re: Still problems"
    date: 2008-11-02
    body: "RSS and Atom Feeds are already functioning in SVN trunk of ktorrent, they will be part of ktorrent 3.2."
    author: "Joris Guisson"
  - subject: "Kde4 is awesome, but still not ready for most user"
    date: 2008-11-01
    body: "Although i do use kde4 in my daily use, i definitly agree with debian guys in not putting kde4 in their next release.\nI do love kde4 it really rocks, but i don't like distros that force you to use it.\nThat's why so many ppl complain about kde4, most of them were forced by distros like fedora 9 with that so many bugs kde4.\nKde4 HAD to be released, but it didn't needed to be used.\nForcing ppl is what is making copies and copies of windows vista stay on the market.\nBut whatever they do or do not, i'll never use a distro that removes the identity of a software by changing it's \"icon\" without truly modifying anything. They can do, but for me that's stealing, a K doesn't look like a hat or a stupid ring with dots around.."
    author: "too early"
  - subject: "Great work!"
    date: 2008-11-01
    body: "I switched to KDE4 right from the beginning, I think this is coolest open source project which can bring Linux desktop to the masses! I am using latest KDE on my production laptop updating it every month and for a half a year I touched my second boot Vista only for hour or two :).\n\nThank you for your work.\nI believe in open source!"
    author: "Boris"
  - subject: "too many bugs"
    date: 2008-11-01
    body: "There are too many bugs or missing feature introduce by kubuntu it's a shame.\nThey disabled plasma zoom without notice.\nThey are no migration done for kmail.\nKmail in the version 4 is buggy and cannot read some imap count.\nUsing Nepomuk with kubuntu is stupid because the backend is old and slow, very very slow. Because of this you can forget to use strigi and all the nepomuk nice stuff as the notation etc.\nI don't understand why did they provide krita2, you cannot open any jpeg file (kubuntu specific bug).\nAnd I probably miss most of them..."
    author: "Albert"
  - subject: "Release party in Berlin"
    date: 2008-11-01
    body: "I am just coming from the Ubuntu release party from the c-base in Berlin and must admit the Kubuntu presentation was \"entsprechend\" horrible. So my question is what can be done to cast a better light on KDE4.x and its new features?"
    author: "Andre"
  - subject: "Re: Release party in Berlin"
    date: 2008-11-02
    body: "to be honest, you want to show people an amazing KDE build, show them OpenSUSE.  OpenSUSE, in my view do a far better KDE than anyone else.  I had a look at kubuntu and it really doesn't even look half as great as what OpenSUSE have in 11.1 "
    author: "R. J."
  - subject: "Re: Release party in Berlin"
    date: 2008-11-02
    body: "i have used this crap . its useable , but full of bugs and slow as hell "
    author: "panke"
  - subject: "Re: Release party in Berlin"
    date: 2008-11-07
    body: "\"its useable , but full of bugs\"\n\nwhat the hell you mean?!"
    author: "freedguy"
  - subject: "I want my KDE 3 BACK !!!!!!"
    date: 2008-11-02
    body: "I tryed to put that thing (KDE4) to work but it was really slooooow, even though I have a Geforce 8500GT, Athlon 2GHz 64bits x2, 2Gb of RAM .... It is really slow!!!! Man, I had to install fluxbox while I install ubuntu-desktop via adept ... that's a shame.\n\nIf just we could choose which version of KDE to use, as in 8.04, but no, in this version it's not enabled (if someone knows how, I'll appreciate).\n\nHow could the people at Kubuntu make us use such a buggy and slow product? I want to choose which Windows Manager to use !!!\n\nOh, and besides all my frustation, the \"K menu\" doesn't show a \"K\", show other icon, as well as the \"Delete icon\" on the plasmoid widgets, that shows an \"Edit icon\" .. :S ..."
    author: "Daniel Wilches"
  - subject: "Re: I want my KDE 3 BACK !!!!!!"
    date: 2008-11-02
    body: "If you like KDE 3.5 it still ships with OpenSUSE 11, and with the upcoming release of OpenSUSE 11.1 both KDE 3.5 and KDE 4.1 will be available for people to decide which they want to use."
    author: "R. J."
  - subject: "Not even ready for unstable!!!"
    date: 2008-11-02
    body: "even the unstable branch would not fit to what i have experienced:\nrepeatedly crashes, deadly slow, the good programs are kde3.5 anyway and an space wasting layout - let alone questions of taste\nPLEASE debian guys! Don't drop kde3.5 in SID in favor for that ... \nfor testing and stable i am conviced no one even only half reasonable will do that, but even for unstable you shouldn't - it is in experimental where it belongs to, if at all!\n"
    author: "Wolfgang Peters"
  - subject: "Reverted to Kubuntu 8.04"
    date: 2008-11-03
    body: "I'm willing to accept that some of the problems with KDE 4.1 in Kubuntu might be down to config choices made by the Kubuntu packagers.  But, this must have done the reputation of KDE no end of damage.  It's clunky, flakey (try accessing a smb:// uri with Dolphin or Konqueror) and generally embarrassingly bad.  "
    author: "Zuccster"
  - subject: "Re: Reverted to Kubuntu 8.04"
    date: 2008-11-03
    body: "why would it damage the reputation of KDE?  KDE is far more than just kubuntu, and from what I have heard a lot of people who weren't happy with the kubuntu release that I know, have shifted to other distro's that have supplied a better KDE build.\n\nI'm on KDE 4.2, well, what will be 4.2 in a few months, on OpenSUSE and it is a beautiful thing.  Don't judge KDE just on what one distro does to it."
    author: "R. J."
  - subject: "If you don't like it..."
    date: 2008-11-03
    body: "Ultimately, with free software you are left with two options when a radically new release comes out:\n\n1. Use it (perhaps even help improve it)\n2. Fork the old code base\n\nIf you really think KDE 3.5 was better than KDE 4, fork the code base. Stop whining about it and take control of the situation.\n\nPersonally, I just don't understand some of the things in KDE4.\n\nThe whole desktop-folder-as-a-widget thing seems to actively interfere with any hope of having a decent looking desktop picture. It seems to imply that -- for now at least -- you can either go with nothing on the desktop except a picture, or you can go with a pattern or solid color that can have clutter on top without disrupting the visual appeal.\n\nThen again, I totally fail to understand the appeal of widgets in the first place. I mean, why? I either want my screen covered with a set of windows, or I want the screen completely clear. What is with the widgets? Why do people think they're good ideas? Why would I want to use them? What problem do they solve? -- I just don't get it.\n\nIt doesn't help that most of the desktop-specific widgets either do something other applications do (a clock or calculator), or they do something that is a total time-sink (like games or comics). It may be that I'm just an old fuddy-duddy for failing to understand the greatness of widgets. In the end, I've no desire to fork the code-base, and I'd consider going back to ratpoison or flwm before I went back to GNOME. (GNOME actively interfered with how I wanted to use my system.) I'll put up with it, and wait for someone to convince me why I really wanted desktop widgets.\n\nI really like the new menu in KDE4. It is different in a really nice way. Most of the time, I don't traverse things all that deep anymore, because for applications I use regularly I can type a few letters and search for them faster than I can look for them. I do wish default menus would become more hierarchical. (I tend to prefer the Debian-style menu organization to the normal one for that reason.) The scroll bars are a huge benefit there. It is really a wonder no one thought of that earlier.\n\nI am interested in the plans for the future of KDE. I'm hoping for more genuinely useful features (like the menu improvement), and fewer features that leave me scratching my head (such as widgets).\n\nCheers,\nSteven"
    author: "Steven Black"
  - subject: "I tried it - mini review"
    date: 2008-11-06
    body: "Kubuntu 8.10.\n\nVery short review:\nLog in screen works but looks unfinished.\nPlasma is slow (openbox is fast and KDE apps open quickly with it).\nLog in the second time and the Kmenu has moved onto the other side of the taskbar and the tray.\nKonqueror has improved.\nKmix crashes occasionally.\nThe restricted drivers app doesn't seem to work without first installing the drivers via apt or Adept.\nAdept looks unfinished but it works.\n\nI'll stress this:\nIf you need a dependable OS, stay with 8.04 until a newer Kubuntu is released. The decision to drop KDE3.5 is not too smart. At least Kubuntu 8.04 is still supported. Though, the Kubuntu folks should really should warn people about Kubuntu 8.10 rough state if they want to be taken seriously, as for some people computing is more than a hobby. ;)"
    author: "winter"
  - subject: "KDE4 sucks :-("
    date: 2008-11-09
    body: "KDE4 sucks unbelievably much.\n\nI have been a kde user since the beginning. I never liked gnome, and I only used enlightenment for a while. \n\nI am kind of desperate right now, as I don't have a desktop to migrate right now. I am staying with kde for now, but only with half a heart, and mainly due to inertia, a bit of hope for the next couple of releases, and lack of alternatives that I like.\n\nKDE4 is: Many new stupid words, many more clicking for the same thing, less functionality and configurability, and easy access to \"usual\" actions.\n\nIf I wanted easy access to \"usual\" actions, I would be using winblows. There is no \"usual\". \n\nAnyways, kde4 is a catastrophe, and only a miracle in the next couple releases will save it. Actually it is so bad, that I have a conspiracy theory that people from MS worked as contributors to kde, to plant bad ui/technological ideas into it and cripple it.\n"
    author: "George"
  - subject: "Re: KDE4 sucks :-("
    date: 2008-11-09
    body: "Sounds like you were even too stupid to keep KDE 3.5 along trying KDE 4. I can see how that sucks for you."
    author: "Anon"
  - subject: "Notice for GNOME users!"
    date: 2008-11-21
    body: "Please put a disclaimer in your anti-KDE4 rant, or delete you posting.   Slandering KDE4 won't make GNOME any better.  GNOME doesn't need your kind of \"help\"."
    author: "GreyGeek"
---
Today sees the <a href="http://www.kubuntu.org/news/8.10-release">release of Kubuntu 8.10</a> featuring the latest KDE 4.1 desktop. The Kubuntu developers have been hard at work, integrating this major new version into a completed desktop.  The settings and artwork have been kept close to KDE's defaults to ensure the best face of our favourite desktop shines through.  Desktop effects have been enabled by default for cards which support it thanks to the wonderful KWin and package management comes via a brand new version of Adept.  Printer Applet and System Config Printer KDE were written to ensure a complete user experience, both are now part of KDE itself. Update Manager, Language Selector and plenty other tools have been reworked for KDE 4.  <a href="https://help.ubuntu.com/community/IntrepidUpgrades/Kubuntu">Upgrade</a>, <a href="http://www.kubuntu.org/getkubuntu/download">download</a> or <a href="https://shipit.kubuntu.org/">request a free CD</a>.


<!--break-->
