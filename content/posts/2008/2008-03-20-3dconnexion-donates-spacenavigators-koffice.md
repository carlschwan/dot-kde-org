---
title: "3DConnexion Donates SpaceNavigators to KOffice"
date:    2008-03-20
authors:
  - "brempt"
slug:    3dconnexion-donates-spacenavigators-koffice
comments:
  - subject: "KDEedu too?"
    date: 2008-03-20
    body: "I'm sure the guys working on Marble and KStars would love to borrow one of these :-)  Perhaps some of the games too?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: KDEedu too?"
    date: 2008-03-20
    body: "We can hit two birds with one stone as it were -- I can propose sending one to Inge Wallin, who's both involved in KOffice and in Marble :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: KDEedu too?"
    date: 2008-03-21
    body: "Yes, please send one to Inge - that should keep him from working on KChart ;-)"
    author: "Torsten Rahn"
  - subject: "Re: KDEedu too?"
    date: 2008-03-21
    body: "Awesome! Thanks a lot to 3DConnexion! Glad to see Krita coming along so nicely!"
    author: "annma"
  - subject: "Re: KDEedu too?"
    date: 2008-03-22
    body: "Haha!  I should threaten to work on other projects than Marble more often.\n\nNow...  A beer at Akademy to the person who suggests the best usage of this little device in kchart. :-)"
    author: "Inge Wallin"
  - subject: "Re: KDEedu too?"
    date: 2008-03-23
    body: "Well, a whole beer crate goes to the person who suggests the best usage of this little device in Marble ;-)"
    author: "Torsten Rahn"
  - subject: "3D for Krita ?"
    date: 2008-03-20
    body: "Pardon my feeble imagination, but how usefull can a 3d controller be for a 2d drawing application ?"
    author: "moltonel"
  - subject: "Re: 3D for Krita ?"
    date: 2008-03-20
    body: "The third dimension is used for zooming. While the first and second is used for panning."
    author: "Cyrille Berger"
  - subject: "Re: 3D for Krita ?"
    date: 2008-03-21
    body: "Could you also use it for drawing? The simplest thing might be to map the 3rd dimension to pressure, in order to re-use the way pressure-sensitive devices are handled. While this might not be the best idea, perhaps someone who is playing with the device can come up with a better one..."
    author: "Martin"
  - subject: "Awesome!"
    date: 2008-03-20
    body: "Thanks 3DConnexion!\nAnd they should get the possibility to advertise that somehow - I'm sure they would appreciate some positive official statement or something."
    author: "liquidat"
  - subject: "you're welcome"
    date: 2008-03-20
    body: "Glad to be of help!  And thanks Hans for the work :)"
    author: "ettore p"
  - subject: "Re: you're welcome"
    date: 2008-03-21
    body: "Thanks again! And welcome to the Dot!"
    author: "Martin"
---
A couple of weeks ago Hans Bakker, who had never touched KOffice code before, started hacking on a Krita plugin for the <a href="http://www.3dconnexion.com/">3DConnexion SpaceNavigator</a>. Within a week or two he had a working plugin for Krita and it quickly became clear how cool these little devices are and how many possibilities for new user interaction paradigms they afford.



<!--break-->
<p>So Hans suggested contacting Ettore Pasquini from 3DConnexion about the possibility of them donating a SpaceNavigator to the KOffice project for testing purposes. Well, that was only last week, and this week a box with three SpaceNavigators already landed on my desk. These are nice, solid feeling, attractive to handle things that come out of the box with drivers for Linux.</p>

<img src="http://static.kdenews.org/jr/koffice-space-navigators.jpg" width="500" height="431" alt="SpaceNavigator photo" />

<p>For an early review of what is possible with the SpaceNavigor on Linux, see this review on Linux.com, <a href="http://www.linux.com/feature/60918">SpaceNavigator: An affordable 3D controller for Linux</a>.</p>

<p>I will be distributing them among KOffice hackers for playing with and testing Hans' code this week, and I am sure that during the development of the KOffice 2 series of releases we will find more and more interesting uses for this unusual and fun interaction tool.</p>

<p>Thanks Ettore and 3DConnexion!</p>


