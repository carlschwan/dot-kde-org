---
title: "Amarok Insider - Twelfth issue released"
date:    2008-04-13
authors:
  - "Ljubomir"
slug:    amarok-insider-twelfth-issue-released
comments:
  - subject: "Sometimes less is more"
    date: 2008-04-13
    body: "I know Amarok is a complex program with a heap of features, but I'm not sure about the screenshots - they seem somewhat cluttered. I like the current (1.4) layout, because it focuses on what Amarok does - play music."
    author: "AndyC"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "What exactly is that you find cluttering the interface? Browsers and Context View can be hidden, and playlist switched to classic mode. Also, each individual service can be disabled from the configuration dialog. Lastly, don't forget that the artwork is still in constant flux, and is heading towards simplicity.\n\nCheers!"
    author: "Ljubomir"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "Actually I find the new fancy playlist a lot *less* cluttered than the classic column-based playlist.\n\nEspecially if you enable more than 5 columns or so in Amarok 1, it starts to resemble Excel."
    author: "Mark Kretschmann"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "Perhaps you wanted to say \"it starts to resemble OOCalc\" ? ;)"
    author: "Capit. Igloo"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-14
    body: "\nThat was problem when user wanted more than 4-5 colums on amarok window without keeping it fullscreen. \n\nBut it still was better in one important way. Showing the info of music.\n1.4.x series playlist could be full of different albums, artist etc and it was short one and very simple to use. But Amarok2 playlist is made for whole albums, not for random songs. When I add 20-30 songs what are mostly, if not all, from different artist and albums, I have very long and hard to control-playlist on my screen. But if I add 5-8 CD from database, it's nice and good because I have only a 5-8 artist tab there what I can hide or show.\n\nThis is one reason why I dont like new Amarok playlist. \n\nOther reason is that playlist is on right edge and database on left edge and I need to drag files from left to right side. On 1.4.x series I can just do small mouse movement to get them dropped easily to that place on playlist I like.\n\nI hope that Middle place (plasmoids) can be removed so Playlist could get bigger again, or at least switch the playlist from right to middle. "
    author: "Fri13"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "I think that the \"curvy background\" on the right pane gets overused and that gives a bit of the impression of clutter. \n\nI'd just use it only once for the navigation on top and stick to plain colors for the other background on the right. \n\nExcept for that the new screenshots rock :-)"
    author: "Torsten Rahn"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "Good artists are a rare resource in open source. We're struggling to find good ones that actually stay with the project. Most of them are never heard of again after a few days.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "Agree"
    author: "ad"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "I think the feel of clutter is due to the high amount of (different and nested) visual borders. See the attached picture were I highlighted plenty (but not all) borders with red."
    author: "Anon"
  - subject: "Easier to forget about borders than to do it right"
    date: 2008-04-13
    body: "> I think the feel of clutter is due to the high amount of (different and nested)\n> visual borders. See the attached picture were I highlighted plenty\n> (but not all) borders with red.\n\nFrom a dev's perspective, it's easier to forget about borders than to make the finishing touches and disable them. Of course, this isn't helped by the fact that widgets have borders around them by default."
    author: "Tray"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-13
    body: "I agree. We've noticed the same thing the other day.\n\nAs a first step, I've removed the bulky and redundant frame around the playlist today. SVN up and check it out :)\n\n\nPS: We're open to further GUI improvement suggestions.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-14
    body: "Hate to be a bother, but can someone give us a screenshot?"
    author: "The James"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-14
    body: "Here's the latest version, with the playlist border already removed:\n\n\nhttp://mark.kollide.net/snapshot63.png\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-14
    body: "Thank-you very much."
    author: "The James"
  - subject: "More suggestions re: color gradients, scrollbar"
    date: 2008-04-15
    body: "On the attached screenshot I've highlighted more suggestions for improving the consistency of Amarok. Specifically, eliminating the subtle yet needless and ugly color gradients between different areas, eliminating the useless separator at the top of the Plasma panel, and eliminating the horizontal scrollbar when in fact nothing is overflowing to the right. \n\nAlso, I think there should be no space between the scrollbar and the box it controls. The Mac OS X style seems to get this right by not inserting a space:\n\nhttp://amarok.kde.org/files/Image/AI/12/full/12-osx2.jpg\n\nbut the Oxygen style is sloppy and puts the space in."
    author: "vlad"
  - subject: "Re: More suggestions re: color gradients, scrollba"
    date: 2008-04-15
    body: "Thanks for your suggestions. Some rare constructive criticism. :)"
    author: "Ian Monroe"
  - subject: "Re: More suggestions re: color gradients, scrollba"
    date: 2008-04-15
    body: "> Thanks for your suggestions. Some rare constructive criticism. :)\n\nYou're welcome, thanks for taking it into consideration :)"
    author: "vlad"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-15
    body: "Nice, I feel much calmer looking at that screenshot already."
    author: "Leo S"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-15
    body: "Hmmm... Perhaps the different parts of the interface should not look like if they were \"stamped in\" (eingestanzt, you know what I mean) but merely stick out. I don't know if that's a theme-specific issue or a general design question."
    author: "blueget"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-16
    body: "\nAmarok2 Really looks great... even there are those few GUI design problems for those who likes 1.4.x series ;-)\n\nAmarok2 looks more slick'n'slide than iTunes and WMP :-D"
    author: "Fri13"
  - subject: "Re: Sometimes less is more"
    date: 2008-04-14
    body: "> it focuses on what Amarok does - play music\n\nAmarok does _a_lot_ more than only play music. That's why it is so popular."
    author: "Birdy"
  - subject: "Heh.."
    date: 2008-04-13
    body: "I tried to switch from amarok to exaile as I figured it'd conserve memory by not loading libraries from two desktop environments.\n\nWhile amarok does more, it also uses less space than exaile, KDE libs and all.\n\nRock on, guys.  Best of everything.\nps. (but I still wish I could rediscover my music by playing it at >1x tempos, you know, *cough* fast forward.)\nGreat for classical.\n<1x great for rap."
    author: "ethana2"
  - subject: "no proxy, no amarok"
    date: 2008-04-13
    body: "no proxy support under windows, so no amarok for me"
    author: "mimoune djouallah"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "Can you elaborate on this? Is this a general KDE issue or an Amarok issue?"
    author: "Mark Kretschmann"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "I believe konqi also suffers from this so I guess general..."
    author: "jospoortvliet"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "hi \n1- proxy is still broken under kde trunk, it is a known bug.\n\n2- kcontrol (systemsettings) is in kde-base workspace, as a result it is not available to windows users. thus no way to configure amarok, kget etc to use proxy.\n\nso we poor windows users we have double pain :-)"
    author: "mimoune djouallah"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "So why did you pollute this thread with it?"
    author: "Dan"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "eah, perhaps this bug need to be fixed, don't you think so ?, i already filled a bug report against systemesetting about this issue, i really don't know if i wrote something wrong that upset you ?  \n\n"
    author: "mimoune djouallah"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "It has nothing to do with Amarok.\n\nThis article is about amarok.\n\nWhining about a bug in a very early port of kde in the comments of an article dedicated to progress in Amarok is rude.  Don't do it."
    author: "Dan"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "please calm down.. you're a bit overreacting here. it's very much a valid point to point out some shortcomings with the (early) windows port of KDE+Amarok. And the style of writing suggests it was shurely not meant as a rude bashing..."
    author: "Thomas"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-13
    body: "It especially doesn't have much to do with Amarok, since I bet phonon-ds9 backend does have proxy support since its using DirectX. So you could play streams fine. (though web services would be b0rked)."
    author: "Ian Monroe"
  - subject: "Re: no proxy, no amarok"
    date: 2008-04-16
    body: "ok you are right it has nothing to do with amarok, proxy in kde is still broken, and under windows to configure proxy is as simple as kcmshell4 proxy.\n\nok i am just a stupid user."
    author: "mimoune djouallah"
  - subject: "The 1 megadollar question"
    date: 2008-04-13
    body: "Can I use it to sync my iPod?"
    author: "John Doe"
  - subject: "Re: The 1 megadollar question"
    date: 2008-04-13
    body: "yes"
    author: "Beat Wolf"
  - subject: "Re: The 1 megadollar question"
    date: 2008-04-13
    body: "no.\n\nnot yet."
    author: "Dan"
  - subject: "Re: The 1 megadollar question"
    date: 2008-04-13
    body: "in the alpha"
    author: "Pascal"
  - subject: "Re: The 1 megadollar question"
    date: 2008-04-13
    body: "Nope, won't be ready by then."
    author: "Dan"
  - subject: "Re: The 1.5  megadollar question"
    date: 2008-04-16
    body: "Will it work with the Ipod Nano (3rd gen)?\n\nEither way...\n\n\n.... Great job guys!!"
    author: "An"
  - subject: "Looks great!"
    date: 2008-04-13
    body: "I am so happy to see those screen shots. The new UI looks nice, easy to use and actually light weight. I am happy to see that the original amarok look & feel is still around in this new version. \n\nKudos to all the team!! As usual, can't wait to try this out."
    author: "Emms007"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "Thanks dude :) We're also very excited about Amarok 2. Most of us devs already use it daily for music playing, and going back to Amarok 1 really feels like stone-age.\n\nI guess it's a good sign that we enjoy using our own product (eating your own dog).\n"
    author: "Mark Kretschmann"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "\"Alarming: Amarok devs to convert to canibalism. News at ten.\" ;-)"
    author: "Daniel Molkentin"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "As far as I know eating dogs doesn't make you a cannibal."
    author: "Narishma"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "Amarok symbol == wolf. The wolf is eating dogs."
    author: "Jonathan Thomas"
  - subject: "Re: Looks great!"
    date: 2008-04-14
    body: "Microsoft's internal mantra is \"eat your own dogfood\", which means if you write code/a program, then you need to use it - you can't expect others to, if you don't.  It's good to know the Amarok team use v2 on a daily basis, let's hope they spot the obscure bugs too and not just the obvious ones ;-)\n\nIs it possible to skin/theme Amarok 2 to make it look like 1.4, just for the old school people?"
    author: "Phil"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "Very exciting reading, and nice screenshots.\n\nTo be honest, I don't really like the current look in SVN. However, it's getting there; the sliders are quite nice-looking. :)\n\nI think it's all the shininess my eyes don't like. As someone said, it looks cluttered. The new ui also lack \"sharpness\", unfortunately I can't really describe what that means. But I'm sure it'll improve over time.\n\nIf I have some free time tonight I might try to make a small mockup. Not a new theme I'm afraid, as I don't know SVG very well.\n\nOh, a final question: are the toolbars configurable? I never use the stop button, and just prev/play/next looks imo much better."
    author: "Hans"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "Nope, the toolbar is fixed. However, maybe we'll decide to remove the stop button, that's still a possibility.\n\nAs always: Proper solution for everyone > customization options. That's the Amarok way (TM).\n"
    author: "Mark Kretschmann"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "\"As always: Proper solution for everyone > customization options.\"\n\nAlthough I like to play around with options, I agree with this philosophy.\n\nAbout the stop button, have you ever considered moving it to the side? WMP does this; even if I don't like the software itself, I think the controls are placed very well. http://www.geekpedia.com/pics/Windows%20Media%20Player%2011/Lost.png\nThis way the symmetry of the three \"important\" buttons is maintained."
    author: "Hans"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "how about three buttons #prev#play/pause#next# and a fading in/out #stop# depending if a song is played or not, like the last.fm buttons"
    author: "Hias"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "That makes no sense at all :) Sorry."
    author: "Mark Kretschmann"
  - subject: "Re: Looks great!"
    date: 2008-04-13
    body: "it would, if you were me a few minutes ago, but now I guess you are right :)"
    author: "Hias"
  - subject: "Collection not shown"
    date: 2008-04-13
    body: "Hey, I'm testing it currently, but my collection is not shown in the TreeView. The collection.db-file seems to be ok (from the size).\nSomeone an idea?\n\nLukas"
    author: "Lukas"
  - subject: "Re: Collection not shown"
    date: 2008-04-13
    body: "Hm, try to delete the collection.db, and then rescan. If that doesn't help, talk to us on IRC or so, and we'll look into it.\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Collection not shown"
    date: 2008-04-13
    body: "Oh, btw will there be a way to reuse my old collection from Amarok 1.4 ?? Would be sad to lose all the stats and stuff. :("
    author: "Jens Uhlenbrock"
  - subject: "Re: Collection not shown"
    date: 2008-04-13
    body: "Yes, in fact our man from down under, Seb Ruiz, is currently working on a converter script that does exactly that. Last I heard the script is almost finished, so you could be one of the first to try it :)\n\nSeb's IRC nick is sebr, try talking to him.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Collection not shown"
    date: 2008-04-13
    body: "Mmh, done, it works now basically, but the problem was, that Amarok2 has some problems with some kind of files (I don't know) and then shows no Tracks in the Collection-Tree. Anyway, another problem is that it doesn't show all files of my collection only a few =(\n\nLukas"
    author: "Lukas"
  - subject: "About theming Amarok"
    date: 2008-04-13
    body: "Hi, I was theming my compiled Amarok (svn from ~2 weeks ago), and I was having some problems with colors..\n\nThe SVGs color\u0092s on the application looked very different from the svg editor (inkscape).\n\nI guess it\u0092s because of that thing of colorizing the theme to match the desktop colors, and if that\u0092s true, it\u0092s possible to disable this? My svgs were very nice (at least for me ;) ) and I was frustrated that I couldn\u0092t have amarok with the same colors I choose..\n\nBesides that, congratulations for the great work! The idea of making the entire window themable with SVGs is wonderful!"
    author: "Paulo Cesar"
  - subject: "Re: About theming Amarok"
    date: 2008-04-13
    body: "Hey, if you've created a good SVG theme, you shouldn't hesitate to share it with us! We're desperately looking for new artists anyway."
    author: "Mark Kretschmann"
  - subject: "Re: About theming Amarok"
    date: 2008-04-13
    body: "We replace only certain colors. \n\nIf you don't use those colors, then they will not get replaced, see SvgTinter.cpp for the colors that are replaced.\n\nHowever, any default theme would need to be tinted, for obvious reasons."
    author: "Dan"
  - subject: "Amarok's UI criticism"
    date: 2008-04-14
    body: "Hi\nFirst, i use Amarok, i love it, new UI looks perfect but:\ni love KDE, because awesome intergration.i love to select one style, and whole my applications look same.Amarok, has now its own UI.SVG based user interface and widgets which are really beautiful.they even match colors.\nbut Amarok now looks different...so, i dont know it is possible or no, but i think if Amarok guys want such look for they app, create a QStyle so all applications could have same look and feel.\n\n(im a user, i dont know if that is possible or no, but i dont like KDE to go the way that windows went: each application with its own theme support, different looks and colors for each application, etc)\n\nsorry for bad English."
    author: "Emil Sedgh"
  - subject: "Re: Amarok's UI criticism"
    date: 2008-04-15
    body: "I share the concern.. i like the fact that there are a lot of new UI possibilities (svg, plasma, ) in KDE4, but i'm like emil also a little concerned that it will make the desktop apps look more alien to eachother.\n\ni like juk for being so 'KDE' if you understand what i mean. (i need amarok for the features though)\n\np.s.: i think the 'Manage Files' is still the biggest feature of amarok giving user full control over their collections. a possibility to put tracks in more than one genre would be my biggest wish -- i hope to submit a patch at some point."
    author: "cies breijs"
  - subject: "cloud and images"
    date: 2008-04-14
    body: "I am using Amarok 2 and I must say it is already working really well.  It also looks great.  I don't think it looks too busy at all, in fact I can't wait for more context views.  I have to admit, though, I have an extremely high threshold for \"busy\" so I may not be representative.  I do have one question and one suggestion.  \n\nFirst, the cloud view looks very cool, but it sounds like it currently only works with online services.  Will it be made to work with user labels at some point?\n\nSecond, in the tree view you have album art for albums, but for artists you only have that generic person image.  Is there any way you can take one of the album art images for each artist and apply it to the artist as well (if there is one)?  It doesn't matter which one you use, it can be the first alphabetically, first in the folder structure, randomly selected (although it should stay consistent between sessions).  I just want something you can use to select an artist visually.  At least for me identifying an image is a lot quicker than reading text."
    author: "TheBlackCat"
  - subject: "Re: cloud and images"
    date: 2008-04-14
    body: "For now, the cloud view is mostly a prototype and it still has some major issues ( Such as skipping all further items when it cannot fit any more in the view. ) That said, it is one of my little personal pet projects, and I have a tendency to not bing able to leave them in an unfinished state for too long... ;-)\n\nI certainly think it would be cool if it worked with local tags, favorite last.fm artists or whatever as well... Most of this is likely post 2.0 though"
    author: "Nikolaj Hald Nielsen"
  - subject: "Amarok for Windows Binaries"
    date: 2008-04-14
    body: "Any chance that we'll see updated Windows binaries?\n\nThe version I get with the KDE for Windows installer is from mid-February. =(\n\nThx."
    author: "ac"
  - subject: "Re: Amarok for Windows Binaries"
    date: 2008-04-14
    body: "Well we need more Windows devs to help us out. Our Windows team is a bit underpowered at the moment.\n\nYou can help by doing some advertising in the Windows community."
    author: "Mark Kretschmann"
  - subject: "Re: Amarok for Windows Binaries"
    date: 2008-04-16
    body: "+1\n\nI'll try my best to recruit Windows programmers."
    author: "AD"
  - subject: "Re: Amarok for Windows Binaries"
    date: 2008-04-16
    body: "+1\n\nI second that. A great new Windows alpha would be great.\n\nI'll let my Windows friends know about it.\nI think the hardest part is that KDE on Windows / Amarok is not advertised AT ALL on windows groups and the windows side. Only Linux friendly people know about it.\n\nWe need to somehow advertise AMAROK on WINDOWS heavier. I think that would create some pull for the entire project. (kind of like the iPod created pull OS-X and the rest of the family of Apple products. Have Amarok be the \"Halo-vehicle\" for KDE on Windows. <-- It's a car industry term.)\n\nMaybe get Cnet involved. A lot of Windows users use Cnet. "
    author: "AD"
  - subject: "Smaller memory footprint than iTunes?"
    date: 2008-04-16
    body: "It's too early for tweaking, but I have a suggestion that might help with adoption:\n\nMake Amarok on Windows have a smaller memory footprint than iTunes. iTunes is a Memory hog!!!!\nI think a lot of people would switch based on that alone.\n\nHopefully it's possible. *crosses fingers*\n\n\n"
    author: "AD"
  - subject: "What about podcasts?"
    date: 2008-04-15
    body: "I really like to hear podcasts. unfortunately amarok makes it to complicated to manage podcasts. They UI is at the bottom of the playlists menu and sometimes podcasts won't get synced with my mp3 player. \n\nI wish a central playe in the amarok GUI to manage my podcasts and chose which episode I want on my mp3 player. "
    author: "Torsten"
  - subject: "Re: What about podcasts?"
    date: 2008-04-16
    body: "+1\n\nI like podcasts as well. Hopefully this will be implemented eventually. Hopefully it'll be easier that podcasts under iTunes."
    author: "Md"
  - subject: "Great job guys!!!"
    date: 2008-04-16
    body: "Amarok 2.0 is the most exiting program with the most buzz EVAAAR!!!!\n\nTons of people are waiting for it. Especially for the Windows version. (Hopefully they'll eventually switch to Linux as well. :) )\n\nQuestion:\n\nWill it work on college campuses and share playlists, the way that iTunes does now? I love that feature that I can play the songs that other iTunes users have on their pc's over the campus network. Will this work with Amarok 2.0 users as well? (will that work no matter what OS Amarok 2.0 is installed on?)\n\n\n.\n___________\n* I'm predicting that Amarok will replace Winamp of days past. (for the young'ins. Winamp was HUGE!!! in the '90's!!!) *crosses fingers*\n\n"
    author: "Md"
---
The next issue of Amarok Insider has been <a href=http://amarok.kde.org/en/node/458>released</a>, along with lots of screenshots. It takes an in-depth look at Amarok's visual theme, Plasma-centric technologies, Web services, and MacOS X integration. It also discusses pending Amarok releases, talks about past and future events, and interviews some crazy Amarok fans. Enjoy!


<!--break-->
