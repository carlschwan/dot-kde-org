---
title: "KDE 4.1 in Slackware Testing"
date:    2008-08-19
authors:
  - "jcollins"
slug:    kde-41-slackware-testing
comments:
  - subject: "slamd64"
    date: 2008-08-19
    body: "Fred, now you've got no excuse not to get this working!"
    author: "Matt Williams"
  - subject: "KDE3 & KDE4 at the same time"
    date: 2008-08-19
    body: "For those who don't want to remove KDE3 (& everything that depends on it), there're packages at <A href=\"http://cardinal.lizella.net/~vbatts/kde4-coexist/kde-4.1.0/\">http://cardinal.lizella.net/~vbatts/</A> which let you run both at the same time.\n\nSee <A href=\"http://www.linuxquestions.org/questions/slackware-14/is-it-possible-to-install-kde-4.1-and-keep-3.5-on-slackware-12.1-661275/\">Is it possible to install KDE 4.1 and keep 3.5 on Slackware 12.1?</A> for more info."
    author: "Jimmy"
  - subject: "Slackware rules :)"
    date: 2008-08-19
    body: "I met many of the slackware team at the 4.0 release event.  They were all extremely friendly towards KDE so we figured it would only be a matter of time before it hit slackware.  (they are rarely \"early adopters\")  It looks like the next version will likely contain KDE 4.x.  Wheee!\n\nNow can someone tell gentoo that the boat is sailing without them? :P\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Slackware rules :)"
    date: 2008-08-19
    body: "I'm running KDE4 on my Gentoo box since 4.0-beta\nAt the moment 4.0 is in portage and 4.1 and SVN ebuilds are in two overlays: what's the problem?"
    author: "Dusdan"
  - subject: "switch"
    date: 2008-08-20
    body: "[off-topic]\nThe fact that you need an overlay? ;) Okay, kinda the same as alternative repositories for KDE4 Factory on openSUSE.\n\nAlso, I see Firefox 3 is still not stable on Gentoo. What's up with that? After 4 years of Gentoo, I'm glad I made the switch last year (first Mandriva, now openSUSE).\n[/off-topic]"
    author: "parena"
  - subject: "Re: Slackware rules :)"
    date: 2008-08-20
    body: "I found a nice feature matrix that you can monitor for the progress of kde 4.1 in gentoo.\n\nhttp://skrypuch.com/kde4/\n\nYou can use kdesvn too on gentoo.\nI did that as well and haven't had any issues with it."
    author: "Mark Hannessen"
  - subject: "KDE 4.1 rocks"
    date: 2008-08-19
    body: "I am deeply impressed with KDE 4.1\n\nMy wish for Windows 7: Package it with KDE 4.x"
    author: "velum"
  - subject: "Please look back"
    date: 2008-08-19
    body: "Patrick, please don't make the mistake Arch Linux did. Some people will need to keep KDE 3.5.9/10 around for a while. Please include KDE4, but keep KDE3 for a bit longer. The two can coexist. Really they can. Laptop users need a decent network/wifi configuration tools, which aren't quite there yet in 4.1. Also there are problems with many video cards that are flaky at present. Other reasons as well."
    author: "David Johnson"
  - subject: "Re: Please look back"
    date: 2008-08-19
    body: "The packages in /testing are named like the kde3 packages with just a different version and they both install to /usr. Personally i think this is a shame especially since i really know myself that you can install KDE3 and KDE4 on Slackware without getting into conflicts (i have KDE3 in /usr + ~/.kde and KDE4 is installed in /opt/kde4 + ~/.kde4).\n\nWell... that is why it is in testing any may become \"stable\" with maybe Slackware 12.2 and/or KDE 4.2 which is OK for me...\n\nDV"
    author: "DarkVision"
  - subject: "Re: Please look back"
    date: 2008-08-19
    body: "Their way of packaging is the clean way, the way you're suggesting is an ugly hack.\n\nSee also my reply to a previous thread: http://dot.kde.org/1218753052/1218821288/1218834292/1218841558/1218848920/1218907032/"
    author: "Kevin Kofler"
  - subject: "Re: Please look back"
    date: 2008-08-20
    body: "I just read your post, and wonder, what about having KDE4 and the KDE3 bits with no available KDE4 versions. Those can coexist in a single $KDEHOME? Is eventually possible to have .kde for KDE4 and left KDE3 environment into a .kde3, then later migrate and remove the later?"
    author: "Shulai"
  - subject: "Re: Please look back"
    date: 2008-08-24
    body: "Until KDE4 is getting \"stable\" that \"ugly hack\" is better then anything else. On three different machines even GNOME behaves better then KDE 4.1 on Slackware. I really wonder why Pat added KDE4 to testing yet... :(\n"
    author: "DarkVision"
  - subject: "Gentoo"
    date: 2008-08-20
    body: "At one time Gentoo use to be considered the bleeding edge distro.\nBut the Gentoo dev's can't even get a ebuild out for 4.1.\nSuch a sad sad thing. We now have rely on overlays via Layman now."
    author: "MrEcho"
  - subject: "Re: Gentoo"
    date: 2008-08-20
    body: "Wrong; the Gentoo developers could have put the ebuilds anytime in the main portage tree, but it's with intention that it's still not there because KDE 4.x still needs to mature. "
    author: "Herbert Eisenbei\u00df"
  - subject: "Re: Gentoo"
    date: 2008-08-20
    body: "Well, couldn't they just put them on some exclude list. I remember when I used to use Gentoo, if there was a package that was in \"testing\" you'd have to include a keyword for it to emerge. Can't they do that? If they don't, then it's a sad day for Gentoo. The Gentoo I used to know was about testing software and developing it. That was the main reason I used it - I could test almost anything and get it working. Other distros had prepackaged crap that you had to recompile anyway to get the feature set you wanted."
    author: "winter"
  - subject: "Re: Gentoo"
    date: 2008-08-22
    body: "kde-base/kde-meta [M](~)4.0.4 [M](~)4.0.5\n4.x IS in the tree, but no 4.1"
    author: "MrEcho"
  - subject: "Upgrade Scripts"
    date: 2008-08-20
    body: "\"Patrick has included scripts to help upgrade from KDE 3.5.x.\"\n\nIs there something like this for KDE in general. I'd really like to keep some settings such as email accounts and bookmarks and contact and calendars. Well, all the info in the PIM. I know I can do it manually, but a script would be nice."
    author: "winter"
---
Official <a href="http://www.slackware.com/">Slackware</a> packages of KDE 4.1 (and dependencies) are now in the "Testing" directory of Slackware-current. See the <a href="http://www.slackware.com/changelog/current.php?cpu=i386">changelog for more information</a>, and read on for a quote from Slackware founder, Patrick Volkerding!

<!--break-->
<p>Slackware creator/maintainter Patrick Volkerding writes this in the Slackware changelog:</p>

<blockquote>
"Added KDE version 4.1 to testing! :-)
Thanks to Robby Workman and Heinz Wiesinger for all the packaging and testing help, and of course to the whole KDE community for helping to bring the Linux desktop to a whole new level of appearance and ease of use. I've installed this on my main email/browsing/general machine and as far as I'm concerned there's just no looking back. It's really a big step forward."
</blockquote>

<p>Patrick has included scripts to help upgrade from KDE 3.5.x. Take advantage of the README's and documentation he's included (and notice that this information is for Slackware "current", not Slackware 12.1)</p>

<p>Kudos to Rob Workman for making KDE 4.x available for Slackware for the last few months.</p>





