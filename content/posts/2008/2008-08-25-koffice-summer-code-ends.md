---
title: "KOffice Summer of Code Ends"
date:    2008-08-25
authors:
  - "brempt"
slug:    koffice-summer-code-ends
comments:
  - subject: "Yay for KOffice!"
    date: 2008-08-25
    body: "I really look forward to release of KOffice 2.0 (or at least the first beta) because I need a simple, yet powerful office and productivity suite :) Hopefully all will be well :)"
    author: "ufoman"
  - subject: "Reactions"
    date: 2008-08-25
    body: "My reactions to the last brush of the last picture while viewing it in fullscreen was \"Computers can do THAT?!\"\n\nIt's beyond impressive! I'm really out of words to congrat its developer!"
    author: "Bruno Laturner"
  - subject: "Wow!"
    date: 2008-08-25
    body: "Sounds amazing! "
    author: "Sebastian"
  - subject: "style"
    date: 2008-08-25
    body: "Boudewijn, could it be that you read a lot of books that are a lot older than you? I love the way you write."
    author: "ben"
  - subject: "Re: style"
    date: 2008-08-26
    body: "Pelham Grenville Wodehouse is my secret :-)"
    author: "boudewijn rempt"
  - subject: "Congrats"
    date: 2008-08-25
    body: "Congrats to all the SoC students.  It looks like you all did a wonderful job.  I hope that the experience will help you in your coding careers, and that you remember to revisit KDE every now and again with a good idea or two.  Those screenshots are slick.\n\nCheers"
    author: "Troy Unrau"
  - subject: "What's up with the GUI fonts in the last screen?"
    date: 2008-08-25
    body: "The panel to the right looks different (and much better imho ;-)) to the main menu bar. \n\nIs that a bug / kOffice feature / doctored screenshot ???"
    author: "Miman"
  - subject: "Re: What's up with the GUI fonts in the last screen?"
    date: 2008-08-26
    body: "It's just that the font for the docker is smaller than from the menu, so wild guess, the effect of antialiasing is more visible and you like to have font with more antialiasing ;)"
    author: "Cyrille Berger"
  - subject: "Re: What's up with the GUI fonts in the last screen?"
    date: 2008-08-26
    body: "Its that smaler fonts do look beter and normaly they are sharper, couse of theyr thikness is smaller than 1 pixel making them look super sharp..."
    author: "nuno Pinheiro"
  - subject: "Thanks SOC Coders!"
    date: 2008-08-26
    body: "Looks like a lot of productive work.  Your efforts are very much appreciated. \n\nDave\n"
    author: "dthacker"
  - subject: "Escher image loading?"
    date: 2008-08-26
    body: "Does it need special code to load images by Maurits Cornelis Escher? Or does .doc work like Escher images?"
    author: "testerus"
  - subject: "Re: Escher image loading?"
    date: 2008-08-26
    body: ":-) No, Escher is a particular way Word has of storing images. The .doc file format has many weird and not so wonderful ways of handling images, and this is a previously unimplemented way."
    author: "boudewijn rempt"
  - subject: "Re: Escher image loading?"
    date: 2008-08-26
    body: "And just a note about the images - since I was adding functionality to WV2, you need the CVS version of WV2 to display any images."
    author: "Benjamin"
  - subject: "Early Adopters"
    date: 2009-06-08
    body: "KOffice, the often overlooked challenger in a productivity suite rivalry championed by Microsoft Office <a href=\"http://www.hostseeq.com \">website hosting  </a> and OpenOffice, rolled out version 2.0 Thursday.\r\n\r\nThe release is intended for developers, testers and early adopters the development team warns, <a href=\"http://www.hostseeq.com/c/internet_marketing.htm \">internet marketing  </a> and shouldn't be used in production. While it adds a sizable pack of improvements to KOffice, it lacks several of the features baked into the previous build, KOffice 1.6 - perhaps quite appropriate for a KDE-oriented major release. The <a href=\"http://www.hostseeq.com/c/domain_registration.htm \">domain registration</a> development team says to expect the missing features to reappear with release versions 2.1 or 2.2.\r\n"
    author: "danielsmth15"
---
This year's Google Summer of Code is drawing inexorably to its close: the first indication that season of mists and mellow fruitfulness is indeed upon us. The <a href="http://koffice.kde.org/">KOffice</a> students are busy tying up the last threads, adding the last flourish of polish that makes all the difference before they will slake the satisfied sigh that goes with work well-done. So - what did we achieve this year? In our very best tradition, read on for an overview!








<!--break-->
<p>This year, KOffice had seven slots. Lorenzo Villani has been working on Kexi web forms, Carlos Licea has worked on making it possible to load KPresenter 1.x (.kpr) documents in KPresenter 2.0. Luká Tvrdý has worked on a Chinese brush simulation for Krita, Fela Winkelmolen on a calligraphy brush for Karbon, Fredy Yanardi on notes and presentation view support for KPresenter, Pierre Ducroquet on ODF support for KWord, and finally Benjamin Cail on converting the .doc to .kwd file import filter to import directly to ODF - and on improving the graphics support in libwv2.</p>

<p>Our choice of projects in 2008 was dictated less by a desire for flashiness - rather we were determined to choose those projects most likely to add solid worth to KOffice. Additionally, life was somewhat easier for our students than last year, when both KOffice and KDE were still under heavy development and every Monday was basically spent on getting the latest binary and source incompatible changes incorporated. This year, only KOffice was a rapidly moving target! And next year, we'll have reached the coding nirvana of feature development against a stable foundation.</p>

<p>Carlos Licea, mentored by Camilla Boemann, is an old hand at KOffice and KPresenter hacking, and was already quite aware of the challenges. His job was to code completely from scratch a new import filter for a rather under-documented file format, namely the old KPresenter .kpr format, which also doesn't map easily onto the OpenDocument presentation format. It's not really something for cool screenshots, but it is amazing how far Carlos has come: the filter is basically ready!</p>

<p>Thanks to Fela Winkelmolen, well-known from KDE games, Karbon now has a really cool and flexible calligraphy brush that can make use of all the features of a high-end tablet, but works just as well with a mouse. The amount of functionality delivered by Fela is amazing, and, fortunately, very visible from screenshots:</p>

<a href="http://static.kdenews.org/dannya/koffice_soc_karbon_calligraphy.png"><img src="http://static.kdenews.org/dannya/koffice_soc_karbon_calligraphy-thumb.png" title="Getting out the broad-nib pen with Karbon" alt="Getting out the broad-nib pen with Karbon"/></a>

<p>Lorenzo Villani, mentored by Jaroslaw Staniek, has been enormously productive: thanks to his work, database applications defined in Kexi are also available through HTTP and HTML: Kexi Web Forms. For the user and application developer this is almost completely transparent: thanks to the KexiDB2 library, the web forms daemon is able to do reverse-engineering on the database and discover the table names, the stored queries and provide the user with an appropriate HTML form to create or update rows.</p>

<a href="http://static.kdenews.org/dannya/koffice_soc_kwebforms.png"><img src="http://static.kdenews.org/dannya/koffice_soc_kwebforms-thumb.png" alt="Browsing a database defined in Kexi" title="Browsing a database defined in Kexi"/></a>

<p>Pierre Ducroquet and his mentor Sebastian Sauer are old hands at their task: like last year, they worked on improving the OpenDocument support of KWord. The focus this year was on proper support for page styles - and Pierre also implemented table-of-contents, saving of lists, and headers/footers. Header and footer support is something that demanded a large amount of work, because the way KWord supported these features still dated from the very first version of KWord designed by Reginald Stadlbauer.</p>

<a href="http://static.kdenews.org/dannya/koffice_soc_kword.png"><img src="http://static.kdenews.org/dannya/koffice_soc_kword-thumb.png"/></a>

<p>Benjamin Cail, mentored by Boudewijn Rempt, but actually helped out most by Werner Trobin and David Faure, had chosen to port the .doc file filter to create OpenDocument files instead of .kwd files. An interesting aspect of this is that his work is actually completely independent of KWord. Indeed, because of the heavy development going on in KWord, Benjamin mostly tested converting documents using <em>koconverter</em> and then loading the .odt file in OpenOffice Writer. But since the ODF libraries of KOffice weren't ready either, Benjamin has spent time on the generic style handling, too. The .doc importer uses the <a href="http://sourceforge.net/projects/wvware">wvware</a> wv2 library, which was originally intended to be shared by Abiword and KWord. But the Abiword developers decided to stick with wv1, and development on wv2 stalled. Benjamin, as part of his work this year, has implemented Escher image loading for wv2. Is it all done? No, not yet. There's plenty left to do, but that's what you get with a humongous file format like .doc. Still, the improvements are very tangible!</p>

<a href="http://static.kdenews.org/dannya/koffice_soc_simple_image.png"><img src="http://static.kdenews.org/dannya/koffice_soc_simple_image-thumb.png"/></a>

<p>Fredy Yanardi, another old Summer of Code hand, mentored by Thorsten Zachmann, had two KPresenter tasks before him: adding support for editing notes with slides, and implementing the presentation view for KPresenter. This means an important stride for KPresenter: together with Carlos' work, KPresenter is really getting ready for release.</p>

<table><tbody><tr><td>
<a href="http://static.kdenews.org/dannya/koffice_soc_presenter_view_main.png"><img src="http://static.kdenews.org/dannya/koffice_soc_presenter_view_main-thumb.png" title="Support for speaker notes in KPresenter" alt="Support for speaker notes in KPresenter"/></a></td><td>
<a href="http://static.kdenews.org/dannya/koffice_soc_presenter_view_slides.png"><img src="http://static.kdenews.org/dannya/koffice_soc_presenter_view_slides-thumb.png" title="Slide overview in KPresenter" alt="Slide overview in KPresenter"/></a></td></tr></tbody></table>

<p>Luká Tvrdý was new to Qt development, KOffice, and Krita. He still managed to implement a complete new brush engine for Krita, designed to simulate Chinese brush painting, but which actually is more or less the basis for any bristly brush engine. Luká went through many implementations, but the end result is very impressive:</p>

<a href="http://static.kdenews.org/dannya/koffice_soc_krita.png"><img src="http://static.kdenews.org/dannya/koffice_soc_krita-thumb.png"/></a>

<p>All this code is already in KOffice 2.0 Alpha 10, which will be released this week for you to play with.</p>

