---
title: "KOffice Sprint 2008 Starting"
date:    2008-11-09
authors:
  - "jpoortvliet"
slug:    koffice-sprint-2008-starting
comments:
  - subject: "Most Important Project"
    date: 2008-11-09
    body: "Hello,\n\nplain good to see that KOffice manages to release as well. I wish you all good and hope that your visions all come true.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Thanks KDAB"
    date: 2008-11-09
    body: "Thanks again to KDAB for hosting this sprint.  They've hosted a number of KDE related hackfests before and they are always welcoming and supportive to the KDE community at large.  Not only that, but it's a wicked marketing move :)\n\nCheers, and happy hacking to the KOffice team."
    author: "Troy Unrau"
  - subject: "Re: Thanks KDAB"
    date: 2008-11-09
    body: "Thanks to KDAB and KDE e.V. also from me for sponsoring again such a great sprint :)\n"
    author: "Sebastian Sauer"
  - subject: "Windows Mac"
    date: 2008-11-10
    body: "When will it be available for Windows and Mac? Unix?"
    author: "Axl"
  - subject: "Re: Windows Mac"
    date: 2008-11-10
    body: "The beta releases are already available for windows and osx."
    author: "Boudewijn Rempt"
  - subject: "Re: Windows Mac"
    date: 2008-11-10
    body: "Will the final Windows release have support for \"My Computer\" and Windows drive letters? "
    author: "Sebastian"
  - subject: "Re: Windows Mac"
    date: 2008-11-10
    body: "When will windows be ready for the desktop?"
    author: "jospoortvliet"
  - subject: "Re: Windows Mac"
    date: 2008-11-10
    body: "Thank you, I needed that laugh today."
    author: "Ken Rice"
  - subject: "Re: Windows Mac"
    date: 2008-11-10
    body: "Sure why not, it works for other KDE applications now."
    author: "Morty"
  - subject: "Re: Windows Mac"
    date: 2008-11-10
    body: "Very well. I tried it a few weeks ago and it did not... Bad user experience at that time."
    author: "Sebastian"
  - subject: "20k"
    date: 2008-11-10
    body: "I like the first sentence: \"Some 20 KOffice developers...\"\n\nu could easily read this as \"Some 20K Office developers...\". And the thought of this is kind of funny. 20k hack0rs at one place. LOL."
    author: "Dr.Schnitzel"
  - subject: "Re: 20k"
    date: 2008-11-10
    body: "All those beards... the smell would be unbelievable :P"
    author: "NabLa"
  - subject: "Re: 20k"
    date: 2008-11-10
    body: ":-) To have enough power to run 20k+n laptops would be probably another problem."
    author: "Sebastian Sauer"
  - subject: "Re: 20k"
    date: 2008-11-10
    body: "nah - just need a methane powered generator and you'll be fine - free energy..."
    author: "Ian"
  - subject: "Re: 20k"
    date: 2008-11-11
    body: "20k batteries, dude! :-)"
    author: "sebas"
  - subject: "Re: 20k"
    date: 2008-11-11
    body: "That would be a very bad thing to happen. Microsoft could send their own Office-hitmans there too and get rid off their competitors very easily...."
    author: "Mikko"
  - subject: "Re: 20k"
    date: 2008-11-12
    body: "Microsoft can not even pull off a decent OS without the countless bugs. How would you think could it pull of SUCH a difficult task as described here???\n\n"
    author: "markus"
  - subject: "Re: 20k"
    date: 2008-11-11
    body: "Except for the fact that K means Kelvin not kilo (which is k). Yes, prefixes ARE case sensitive."
    author: "Erik"
---
Some 20 KOffice developers have gathered at the KDAB offices to discuss and prepare the upcoming release of KOffice 2.0. Read on to learn about what the meeting is for and what is on the agenda to be sorted before the big release.





<!--break-->
<a href="http://static.kdenews.org/jr/koffice-sprint.jpg"><img src="http://static.kdenews.org/jr/koffice-sprint-wee.jpg" align="right" width="400" height="268" style="border: thin solid grey; padding: 1ex; margin: 1ex" /></a>

<p>Saturday morning, 9 o'clock. Some 20 people have gathered at the gate before the KDAB offices to discuss and prepare the upcoming release of KOffice2.0. They are excited. This release will be a big step for the KOffice project. The community has worked on this for over 2 years. Much thought has been put into an architecture ready for the new millennium. After all, computers are becoming more and more important. Expectations are rising. Yet progress seems slow. There has been an almost complete lack of innovation in the Office space between 1995 and 2005. There has been a little movement lately, but nothing incredibly special. KOffice has a clear vision: build an architecture capable of supporting every crazy idea out there. Create the freedom to be creative, and allow it to happen.</p>

<p>
The purpose of this meeting is twofold. First of all, the current state of KOffice will be discussed. The parts of KOffice which will be released should be put under scrutiny. The KOffice developers choose those applications which are mature enough to benefit from feedback and input from the larger community. They might not be entirely ready for the end user, but they must be ready for at least some light use. That way users can give some meaningful input, and steer development in the right direction. The workflow in the application needs to be analysed and optimised. A list of issues needs to be compiled. What are showstopper bugs, what areas are in urgent need of resources?</p>

<p>
The second goal for this meeting is to discuss how to introduce this new release. Recently the website, logos, release announcements and other marketing related topics have been discussed. The team needs to come to conclusions, and start preparing. We need release notes, an announcement, a better website and a more general strategy. Yes, we are growing up: Free Software is thinking about strategy these days.</p>

<p>
Saturday morning, 9 o'clock. The doors open, and we are welcomed by the KDAB employees. Up the elevator, into the offices and on to work. There is a tight schedule and a lot of work ahead of us. Of course, the developers will blog about what happens. And you can expect an article with the first results and some impressions tomorrow!</p>




