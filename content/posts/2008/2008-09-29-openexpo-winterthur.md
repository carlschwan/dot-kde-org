---
title: "OpenExpo Winterthur"
date:    2008-09-29
authors:
  - "aleisse"
slug:    openexpo-winterthur
comments:
  - subject: "KOffice Templates"
    date: 2008-09-29
    body: "I my opinion, one key thing about using an office suite is the availability of a mature collection of templates. Templates in my language (from the users view) for private and business use.\nIt would be too simple to just say, \"use any Ooo-Template you like\". \nWhat we (well, at least I need) need is a good manual, how to create templates and a central, well structured soure for high quality Templates.\n\nKOffice is special, and so highly likely not so easy for everyone, who is used to other software. Good templates help to understand the priciples and increase produktivity.\n\n"
    author: "Frank"
  - subject: "Re: KOffice Templates"
    date: 2008-09-30
    body: "I agree with the parent. Templates are very important for non-techie uses. Hell, I rely on them sometimes as I ain't big on word processing myself."
    author: "NabLa"
  - subject: "Blog"
    date: 2008-09-29
    body: "I've also blogged about the experience:\n\nhttp://amarok.kde.org/blog/archives/803-OpenExpo-2008,-Zurich-Switzerland.html\n\n"
    author: "Mark Kretschmann"
  - subject: "nice slides"
    date: 2008-10-01
    body: "Those slides are really nice and give a good overview of what will be in KOffice2. \n\nAnd congrats to your first (I think?) KDE presentation, Alexandra :)"
    author: "sebas"
  - subject: "Re: nice slides"
    date: 2008-10-09
    body: "Thanks, Sebas! It was the first indeed.\n\nI'm slow I know. (And this rhymes)"
    author: "Alexandra Leisse"
---
The second <a href="http://www.openexpo.ch/">OpenExpo</a> for this year was held in Winterthur on 24th and 25th September. All was perfectly organized by <a href="http://www.ch-open.ch/">/ch/open</a>. Read more about KDE's presence at the show.







<!--break-->
<div style="float:right; border: 1px solid #888; margin:5px 5px 5px 0; padding: 5px; width: 180px"><a href="http://static.kdenews.org/danimo/claudia_openexpo.jpg"><img src="http://static.kdenews.org/danimo/claudia_openexpo_small.jpg" border="0"></a><br /><small>Claudia, our Administrator.</small></div>
<p>KDE had a well visited booth, where Luca Gugelmann, Alexandra Leisse, Claudia Rauch and Eckhart Wörner answered questions, collected feedback and took care of the smaller and bigger sorrows of our users while Mark Kretschmann added some Rock 'n Roll feeling.</p>

<p>On wednesday Alexandra Leisse gave a well received talk about the upcoming release of KOffice 2. The slides (in German) are <a href="http://koffice.kde.org/presentations/troubalex_winterthur.odp">available here</a>. A video of the whole talk will be online later this week.</p>

<p>Thanks to everybody who made this presence a success.</p>

