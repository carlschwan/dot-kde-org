---
title: "Test Latest Builds With KDE4Daily 4.1"
date:    2008-05-26
authors:
  - "SSJ"
slug:    test-latest-builds-kde4daily-41
comments:
  - subject: "great"
    date: 2008-05-26
    body: "Hey this is great service (to encourage backtraces and easy updates), which will mean I dont need to allocate 15gb to enable me to compile the whole tree and compile it every other day.\n\nps: A fast http mirror would be great as I the torrent is very slow :(\n"
    author: "crazydood"
  - subject: "Re: great"
    date: 2008-05-26
    body: "More people downloading it should make it faster, right?"
    author: "Andr\u00e9"
  - subject: "Re: great"
    date: 2008-05-26
    body: "\"ps: A fast http mirror would be great as I the torrent is very slow :(\"\n\nThere's now an HTTP mirror courtesy of Tony Wolf - many thanks to him for this :)\n\n\nhttp://downloads.os-forge.eu/kde4daily_4_1-final-qcow.img.bz2"
    author: "SSJ"
  - subject: "Re: great"
    date: 2008-05-26
    body: "... and an additional mirror has been provided by Kiyoshi Aman here:\n\nhttp://kde4.aerdan.org/kde4daily_4_1-final-qcow.img.bz2\n\nCheers!"
    author: "SSJ"
  - subject: "great idea!"
    date: 2008-05-26
    body: "great idea guys! I'm downloading it right now!!!"
    author: "ANDREA"
  - subject: "A few odds and ends ..."
    date: 2008-05-26
    body: "...  that I forgot to mention in the article.\n\nFirstly, I now have a blog, of sorts:\n\nhttp://ssj-gz.blogspot.com/\n\nMy very first blog entry is a brief analysis of the last KDE4Daily run.\n\nAn update to r812045 is already queued up and tested, and the next is on its way.  After a while, human decisions will be removed from the upload process, and my faithful workhorse will operate on a \"If it compiles - ship it!\" basis, so be warned ;) If you get a dud update, then just wait around for the next one - KDE4Daily should be robust enough to deal with failures like this.\n\nSome future plans:\n\n- Implement a \"mini-Dashboard\" ( a small, KDE4Daily specific version of this:\n\nhttp://ktown.kde.org/~dirk/dashboard/\n\n) that tells you what the KDE4Daily preparer is doing (sitting idle; updating SVN; compiling; processing debug info) and whether any error occurred in the last batch.  This is mainly for my use as I have a full-time job this time around, but others might find it helpful :)\n\n- Allow a full dev environment to be created inside KDE4Daily by running a script.  This will involve grabbing all external dependencies, checking out specific SVN modules (using \"snapshots\" so as to ease the strain on KDE's SVN servers) and updating them to current, and building.  I would have done this last time (and several people asked me for it) but the crazy partitioning scheme meant there wouldn't have been room.  So soon the barrier to hacking on KDE will be lower than ever! \n\nThat's all I can think of for the time being; enjoy!\n"
    author: "SSJ"
  - subject: "Re: A few odds and ends ..."
    date: 2008-05-26
    body: "congrats on getting through your thesis; welcome back to KDE land and thanks for making these images available. rock on =)"
    author: "Aaron Seigo"
  - subject: "VirtualBox Image"
    date: 2008-05-26
    body: "In case you want to test the image with VirtualBox, look at the bottom of this howto:\nhttp://liquidat.wordpress.com/2007/11/23/howto-transform-a-qemu-image-to-a-virtualbox-image/\n\nAfterwards, it can easily be used as a native VirtualBox image."
    author: "liquidat"
  - subject: "Re: VirtualBox Image"
    date: 2008-05-26
    body: "Indeed, thankyou :)\n\nIncidentally, if anyone wants to convert to VirtualBox and upload it somewhere, please do - I'd do it myself, but it takes me hours and hours to upload images, and I'm already seeding the Qemu version :/"
    author: "SSJ"
  - subject: "Re: VirtualBox Image"
    date: 2008-05-30
    body: "If someone can get a virtualbox image torrent going, I'll seed.  For many users, this makes more sense than qemu.  Gentoo users especially are often stuck without qemu, but can easily use vbox."
    author: "vbox user"
  - subject: "Username and password for login"
    date: 2008-05-26
    body: "What's the username and password for login on the live-cd r811964?"
    author: "Andreas"
  - subject: "Re: Username and password for login"
    date: 2008-05-26
    body: "you mean the bugsquad live cd? the debian based one..\n\nUsername: user\nPassword: live\n\n"
    author: "solardeity"
  - subject: "Re: Username and password for login"
    date: 2008-05-26
    body: "That's it, thanks."
    author: "Andreas"
  - subject: "Official kde.org branding?"
    date: 2008-05-26
    body: "Hi,\n\nWould it make sense to put such initiatives under the official kde.org sites? (e.g. kde4daily.kde.org). The same also applies to the EBN and dirk's dashboard."
    author: "Diederik van der Boor"
  - subject: "Thanks"
    date: 2008-05-26
    body: "The excellent KDE4Daily is back -- thanks.\n\nI'll dedicate my 10mbps upstream to seeding this."
    author: "IAnjo"
  - subject: "Re: Thanks"
    date: 2008-05-26
    body: "Nice to see it back again! Very good to get more people to test it and fill out bugs!"
    author: "michael"
  - subject: "Thanks SSJ!"
    date: 2008-05-26
    body: "Thanks SSJ, kde4daily makes testing KDE trunk easy and fun! (Plus, now I can resist learning to compile from SVN until summer, when I actually have time. Aaron's blog has been getting me so excited that I was ready to risk screwing up my computer during finals week, lol.)"
    author: "kwilliam"
  - subject: "Where to report bugs?"
    date: 2008-05-26
    body: "hm, just wondering...what should be the first port-of-call when reporting bugs? To you or the kde bugtracker? \n\nI've already run into one bug...and one that seems unlikely to have been missed by the developers. Nothing has crashed though so there's no backtrace available...but the thing is, whenever I add a plasmoid or a new panel - it isn't saved between sessions and neither are programs left running on shutdown (at least not akregator or kontact). I can, however, create directories and files in /home that are saved and retained as they should (including comments and tags) so I don't think it's as basic as insufficient rights to modify files - and the files I've taken a further look at has sufficient rights at that.\n\nNote though that I have converted the qemu image to a vbox one, although I doubt that is the cause of the problem."
    author: "Jonas"
  - subject: "Re: Where to report bugs?"
    date: 2008-05-27
    body: "I've noticed that logging out seems rather ... abrupt, and I wonder if this is the cause:\n\nhttp://bugs.kde.org/show_bug.cgi?id=162642\n\nThis would probably kill all session-saving, etc.\n\nThe Plasma thing has hit me a few times, and it seems that there is a plasmaappletsrc (can't remember the exact file name at the moment) but no plasmarc.  I don't know enough about the internal workings of Plasma to know if this is the cause of it, but it does seem strange at least.\n\n"
    author: "SSJ"
  - subject: "Re: Where to report bugs?"
    date: 2008-05-28
    body: "If it looks like a general KDE bug, then bugzilla, but search it first to make sure you aren't reporting a duplicate. And make sure to put in that you're using a daily version, and which one it is.\n\n\n\n"
    author: "A. L. Spehr"
  - subject: "Kubuntu?"
    date: 2008-05-27
    body: "If KDE4Daily is Kubuntu-based, are actual .deb packages available for vanilla Kubuntu?\n"
    author: "Riddle"
  - subject: "Re: Kubuntu?"
    date: 2008-05-27
    body: "The KDE4 install isn't provided as .debs, but there is in principle no reason that I can see why users of *buntu Hardy couldn't run KDE4Daily natively.  I'll investigate this more when I have a chance, but I'll try and post an \"anatomy of KDE4Daily\" so that people can see how it's put together starting with a blank server install - a few people have asked for installable/ LiveCD versions, and this will hopefully enable them to easily create them."
    author: "SSJ"
  - subject: "Re: Kubuntu?"
    date: 2008-05-27
    body: "That would be great for all the wonderful and adventurous people on the kubuntu forum.\n\nI am sure some of us would love t help building a live/install cd.\n\nVery much looking forward to this:)) and thank you for all the great work:)"
    author: "Fred"
  - subject: "Re: Kubuntu?"
    date: 2008-05-28
    body: "> I'll investigate this more when I have a chance\nThat's good to hear.\n> but I'll try and post an \"anatomy of KDE4Daily\" so that people can see how it's put together starting with a blank server install - a few people have asked for installable/ LiveCD versions, and this will hopefully enable them to easily create them.\nThat's very good to here.\n\nI'd be willing to help if you need it.\n"
    author: "Riddle"
  - subject: "Re: Kubuntu?"
    date: 2008-06-03
    body: "It's probably missing some essential details, but see here:\n\nhttp://ssj-gz.blogspot.com/2008/06/anatomy-of-kde4daily.html"
    author: "SSJ"
  - subject: "testing"
    date: 2008-05-27
    body: "Hi all,\n\nI did some initial tests, 256mb for qemu mem is waaaay to slow for kde. I felt  with 1gb its slow, but bearable. I can imagine how slow its going to be when generating a backtrace. \n\nI am not sure though why it is so slow, I have had vmware images (kubuntu) run on vmware server with 512mb and its quite responsive.\n\nPlasma theme bug?\ndownloaded themes with khotnewstuff and installed some of those that were voted great in the plasma theme contest? and after installing, selecting them as \"the\" theme, logged back in and the whole desktop is empty (no panel, cashew, nothing)\n\nany ideas?\n"
    author: "crazydood"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "Oops - the Plasma comment here:\n\nhttp://dot.kde.org/1211789022/1211839118/1211869059/\n\nwas directed at crazydood :)"
    author: "SSJ"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "I have another minor nitpick :)\n\nThe default window size is bigger than my 1280x800 display, can you make the height less than 800? it would be great :)\n\nAs for the plasma bug, i deleted my .kde and .config in $HOME seems to be back up and running.\n\nAs for the updates wow 80MB of patches :) \n\nkeep em comming hehe"
    author: "crazydood"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "woops the patches were another 30mb :)\n\nohh and about the window size, dont worry, i had biig task menu bar, so i couldnt see the whole qemu window\n\nohh and thanks for the warning tip in the latest update on how to kill plasma and run again, by running krunner.\n\n:)"
    author: "crazydood"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "I second this. To top it off, the kdm theme doesn't seem to react very good to changing the resolution (buttons get hidden), so you just can't change the resolution on the xorg.conf.\n\nMaybe change the kde theme, and put \"1024x768\" before \"1280x1024\" on the xorg.conf? That way, if you wanted more/less, you could use krandrtray or xrandr to change it."
    author: "IAnjo"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "--Ah forget it. It seems that there is some kind of but when I put both \"1024x768\" and \"1280x1024\" on the xorg config file, very weird."
    author: "IAnjo"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "It's slow because qemu needs the kernel accelerator module to do true virtualisation rather than emulation.\n\nI've converted the KDE4Daily 4.1 image to VirtualBox's format, but I need to confirm that it works before I compress it and send it off to my webserver."
    author: "Kiyoshi Aman"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "Thanks for the tip! It was running way, way slow without it."
    author: "Jonathan Thomas"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "Hi again,\n\nI have compiled the kernel module kqemu for virtualisation, but the performance is the same.\n\nany difference between running it with virtualbox?\nsay 1gb ram allocation\n\nfor me its painfully slow, konsole probably takes around 10 seconds to launch"
    author: "crazydood"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "I noticed with mine that nepomukservices(?) chewed up 100% of the CPU for several minutes.  Have you tried running \"top\" in a terminal (from within KDE4Daily) to see if any processes are slowing everything down?"
    author: "Anon"
  - subject: "Re: testing"
    date: 2008-05-28
    body: "nope, nothing of that sort (nepomuk or any other app taking 100% cpu) here.\n"
    author: "crazydood"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "Hi,\n\na VirtualBox Image would be really nice to have, because it's complicated for me to install qemu3 (it needs gcc3).\nI will seed of course, if there's a torrent.\n\n"
    author: "mk"
  - subject: "Re: testing"
    date: 2008-05-28
    body: "I tried to convert, but my .vdi ended up with a boot failure."
    author: "mk"
  - subject: "Re: testing"
    date: 2008-05-27
    body: "I found it to be very slow also. It would be great if you could convert it, I would do it myself, but it needs 40gigs of free space to do(for the .bin and uncompressed .vdi). \n\nThankyou =] "
    author: "TimS"
  - subject: "Re: testing"
    date: 2008-05-28
    body: "I was not able to make a working VDI VirtualBox image, but that's okay; I converted to VMDK using qemu's qemu-img conversion utility instead and *that* works rather well:\n\nhttp://kde4.aerdan.org/kde4daily_4_1-final.vmdk.bz2\n\nPlease be sure to mention it in your bug reports just in case, as there may be flaws I haven't encountered yet.\n\n[This image should work in VMWare as well, but I haven't tried it because I don't use VMWare.]"
    author: "Kiyoshi Aman"
  - subject: "Re: testing"
    date: 2008-05-28
    body: "I forgot to mention, I *cannot* seed torrents, so unless someone else wants to make a torrent for the image, there will be no torrents available. Sorry."
    author: "Kiyoshi Aman"
  - subject: "Re: testing"
    date: 2008-05-28
    body: "Excellent - thanks very much for this!"
    author: "SSJ"
  - subject: "seeding"
    date: 2008-05-28
    body: "At the moment of writing this, I'm downloading it from two machines (own computer and 100Mibps server), the second will be seeding. If you want, I can provide a http source too."
    author: "Marcin &#321;abanowski"
  - subject: "Virtualbox running"
    date: 2008-05-28
    body: "Well I've got it running in a virtualbox. I am now updating it and will then try to get a torrent out. I will have some seeding problems up until monday though, so best thing will be to get it tonight.\n\nWill post again when I know more."
    author: "Jord"
  - subject: "Re: Virtualbox running"
    date: 2008-05-28
    body: "Just saw there is already one out there... :-) "
    author: "Jord"
  - subject: "Re: Virtualbox running"
    date: 2008-05-29
    body: "I can't get networking to work with the VirtualBox VM running under Windows or Kubuntu. There isn't an /etc/initab file, is that the problem? Any help appreciated..."
    author: "pepe"
  - subject: "Re: Virtualbox running"
    date: 2008-05-30
    body: "I had the same problems (with 1.5.6) . Actually i only now found a workaround. \"sudo su\" to root password is kde4daily\nThen rmmod pcnet32 (for the default virtualbox device network module). modprobe pcnet32.\nwhen you now look at dmesg you'll see something like \"found 1 device. renamed it to eth5\".\n\nthen dhclient eth5"
    author: "mk"
  - subject: "Re: Virtualbox running"
    date: 2008-05-30
    body: "thank you. It works for me with eth1"
    author: "bakc"
  - subject: "Re: Virtualbox running"
    date: 2008-05-30
    body: "Worked for me too! Thanks very much. \n\nNow need to fix screen resolution, it's way too big so I need to scroll in VirtualBox to see everything. Used KR&RTray to change to 800*600 but this just cut out an 800*600 window!\n\n"
    author: "pepe"
  - subject: "Re: Virtualbox running"
    date: 2008-05-31
    body: "you can edit /etc/X11/xorg.conf\nJust search for the \"Screen\" section und remove the resolution \"1280x1024\" for the color depth 24"
    author: "bakc"
  - subject: "Re: Virtualbox running"
    date: 2008-06-01
    body: "I managed to fix resolution issues by simply installing the VirtualBox Guest Additions drivers. By default an ISO of the installation files is mounted as the CD/DVD drive. You have to use apt-get to install make and the linux kernel headers first though. The terminal emulator panel on Dolphin makes this really easy!"
    author: "pepe"
  - subject: "Re: Virtualbox running"
    date: 2008-06-12
    body: "For me eth1 works also. Thank you very much for this description.\n\nBtw. after you follow those steps it is good to edit\n/etc/rc.local\nand copy there those commands. This way each time you boot up your kde4daily the network will be activated."
    author: "Maciej Pilichowski"
  - subject: "VirtualBox VM networking"
    date: 2008-05-29
    body: "I've tried to get the new VirtualBox VM working in Windows and Kubuntu but can't get any network connection for updates etc...\n\nThere isn't an /etc/iftab file. Does that have something to do with it? Any help appreciated...\n"
    author: "pepe"
  - subject: "Re: VirtualBox VM networking"
    date: 2008-06-02
    body: "I can't get that to work either."
    author: "Jeremy LaCroix"
  - subject: "Plasma woes"
    date: 2008-06-02
    body: "\nHaving great time updating and trying out the new theme but the latest update has not fixed a problem I've had: when I log-out something goes wrong with plasma. When I log back in I get grey screen, and this is only fixed by logging in to a console session and deleting the plasmarc and plasma-appletsrc.\n\nSo is this a known bug or specific to kdedaily? Anyone else had this problem?\n\n"
    author: "pepe"
  - subject: "Re: Plasma woes"
    date: 2008-06-02
    body: "http://dot.kde.org/1211789022/1211839118/1211869059/"
    author: "Anon"
  - subject: "Re: Plasma woes"
    date: 2008-06-03
    body: "I've investigated this, and filed a bug report:\n\nhttps://bugs.kde.org/show_bug.cgi?id=163112\n\nI'm not sure if it is KDE4Daily-specific, but there's a chance that the CPU information that Solid uses is not correctly provided by Qemu, or something like that :)"
    author: "SSJ"
---
With the release of 4.1 on the horizon, and initiatives such as <A HREF="http://grundleborg.wordpress.com/2008/05/25/kde-pim-krush-day-is-now/">Krush</A> days</A>, recent call for <A HREF="http://blog.nixternal.com/2008.05.21/kde-41-documentation-needs-your-help/">help with documentation</A>, and the perennial need for <A HREF="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Localisations">localisation</A> it is very useful for end users to be able to easily get their hands on up-to-date builds of KDE4, preferably without having to wait for their chosen distro to provide packages.  As was the case with the run up to <A HREF="http://dot.kde.org/1195829316/">KDE4.0</A>, KDE4Daily VM aims to provide such a service.


<!--break-->
<p>For the uninitiated, KDE4Daily aims to accomplish this goal using <A HREF="http://fabrice.bellard.free.fr/qemu/">Qemu</A> virtualisation technology (although with KDE4Daily 4.0, people kindly stepped up to provide a VirtualBox/ VMWare equivalent).  A self-contained Qemu image with a Kubuntu 8.04 base and a <A HREF="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Included_Modules">comprehensive</A> set of a self-compiled KDE4 modules (all at r810996, initially) is provided, along with an <A HREF="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Updating_KDE4Daily">updater system</A> inside the VM itself.  The updater downloads and installs binary updates provided by me to the full set of modules; these tend to be roughly 20-50MB each (although they will occasionally be larger), take a few minutes to apply, and will hopefully be pushed out daily - hence the name ;). "Bridge" updates will hopefully condense a week or so's worth of updates into one more compact version, so you can update at your own pace without being hit with a massive bandwidth bill :)</p>

<p>Because Qemu is distro-agnostic, you do not need to worry about distros or libraries or dependencies or suchlike; in fact, you can even test out KDE 4 while running Windows! The downside is that eye-candy such as KWin's new Composite-based effects will not be testable as Qemu does not support hardware graphics acceleration, and everything will generally feel a lot more sluggish than is the case with a native install.

<p>KDE4Daily comes with its own backtrace-generation system so hopefully devs can be assured of having useful backtraces in any crash bugs you file, courtesy of DrKonqui.  Do note that this system is currently <A HREF="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Slow_Backtrace">rather slow and resource-intensive</A>, although there are plans to improve it during the run of KDE4Daily 4.1.  If you do somehow manage to crash a KDE app, please be patient while the valuable backtrace is created!

<p>An extensive FAQ is provided at the KDE4Daily homepage, above; please feel free to ask any further questions in the Dot comments section. Also, note that KDE4Daily has not yet had any real testers apart from myself, so please be prepared for "teething trouble" such as botched upgrades and bandwidth issues! Enjoy, and remember that the more people test, the better KDE 4.1 will be.  It's not all work, though; if you just want to try out recent deliciousness such as the <A HREF="http://aseigo.blogspot.com/2008/05/krunner-for-41.html">KRunner</A> and <A HREF="http://www.kdedevelopers.org/node/3475">Marble</A>, then that's fine, too!

