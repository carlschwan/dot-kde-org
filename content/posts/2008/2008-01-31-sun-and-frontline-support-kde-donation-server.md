---
title: "Sun and Frontline Support KDE with Donation of Server"
date:    2008-01-31
authors:
  - "jhall"
slug:    sun-and-frontline-support-kde-donation-server
comments:
  - subject: "Wrong Frontline!"
    date: 2008-01-31
    body: "Hi guys,\n\nWrong Frontline! <a href=\"http://www.frontline.com.au/\">www.frontline.com.au</a>\n\n"
    author: "Amused"
  - subject: "Re: Wrong Frontline!"
    date: 2008-01-31
    body: "Yes, I'm afraid that's entirely my fault - I didn't supply our dot editors with the URLs myself and I guess it's understandable someone from the northern hemisphere might find the wrong Frontline.\n\nUnfortunately as most of the dot editors are in the northern hemisphere we might be waiting a few hours for someone to be awake to fix it.\n\nMy apologies to Frontline for this mistake, and my assurances that it will be amended as soon as I can get a hold of an editor."
    author: "Jes Hall"
  - subject: "Re: Wrong Frontline!"
    date: 2008-01-31
    body: "it's also \"e.V.\" \n\nits not obvious to me, how one is supposed to get in touch with the editors. i probably just need to wake up, but maybe this could be highlighted more..."
    author: "bangert"
  - subject: "Re: Wrong Frontline!"
    date: 2008-01-31
    body: "I believe it is editors@dot.kde.org  or admin@kdenews.org (the last adres can be seen on the bottom of the page)"
    author: "jos poortvliet"
  - subject: "Re: Wrong Frontline!"
    date: 2008-01-31
    body: "I guess you'd like it if any of us never ever use your services. I don't think I've ever seen an IT company spam like this."
    author: "winter"
  - subject: "Re: Wrong Frontline!"
    date: 2008-01-31
    body: "huh? What's wrong with correcting an URL?"
    author: "jos poortvliet"
  - subject: "Thanks"
    date: 2008-01-31
    body: "Great news, especially the bottom-node about anonsvn :) Thanks Sun and Frontline!\n"
    author: "Sebastian Sauer"
  - subject: "Re: Thanks"
    date: 2008-01-31
    body: "indeed, anonsvn is failing now and then for me. it seems that anonsvn syncs later than authenticated servers (which wouldn't be that bad), but it also seems there are at least two servers who themselves often are out of sync :)\nso i get \"updated to revision n\" message, try to get svn log to that revision - and then it claims there is no such revision =)"
    author: "richlv"
  - subject: "Re: Thanks"
    date: 2008-01-31
    body: "There's three right now:\n\nName:   anonsvn.kde.org\nAddress: 78.46.32.117\nName:   anonsvn.kde.org\nAddress: 131.174.31.25\nName:   anonsvn.kde.org\nAddress: 138.246.255.177\n\nThe middle one is the EBN, which doubles as anonsvn. I know it updates every five minutes from the master; I don't know about the others."
    author: "Adriaan de Groot"
  - subject: "OT: 4.0.1 Tagging?"
    date: 2008-01-31
    body: "Wasn't 4.0.1 to be tagged yesterday?"
    author: "blueget"
  - subject: "Re: OT: 4.0.1 Tagging?"
    date: 2008-01-31
    body: "Yes and no - but mostly yes ;)\n\nhttp://lists.kde.org/?l=kde-release-team&m=120174348031330&w=2"
    author: "Anon"
  - subject: "Just a marketing?"
    date: 2008-01-31
    body: "Hmm, isn't that server pretty weak for projects like KDE?\nIt looks to me Sun would just like to hit the headlines, like supporting KDE and stuff. I mean, if they can afford to buy companies for 1 BILLION dollars, they could've been more generous if their intents are honest. Or maybe they just want to get rid of old hardware?\nAnyway, it's always nice to receive a donation, but this is small news and shouldn't be on top of announcements, IMHO."
    author: "Observer"
  - subject: "Re: Just a marketing?"
    date: 2008-01-31
    body: "No, it's not weak. It would be one of the strongest machines in our infrastructure. When I said that KDE as a project runs on surprisingly little hardware, I meant it: we have machines that you wouldn't use as a modern doorstop supporting critical parts of the project. At the same time, we don't need monstrous kit to get things done, so this donation is, just like I said: a nice way of extending our reach. "
    author: "Adriaan de Groot"
  - subject: "Re: Just a marketing?"
    date: 2008-01-31
    body: "eh just a silly question, can we have more information where the server will be placed physically, i mean do u have an office for kde ev !!\njust wondering "
    author: "anon"
  - subject: "Re: Just a marketing?"
    date: 2008-01-31
    body: "This machine will stay in Australia; as stated in the article it will be hosted through Frontline. Probably in one of the big cities. The question whether KDE e.V. has an office is totally separate, but yes, we do: see http://ev.kde.org/contact.php"
    author: "Adriaan de Groot"
  - subject: "This is good news."
    date: 2008-01-31
    body: "It's good to see that Sun is supporting the open source community, I'm sure the hardware that they've donated will help the KDE project out greatly."
    author: "Zachary"
  - subject: "Re: This is good news."
    date: 2008-01-31
    body: "I agree, this is a great move. Thank you Sun!"
    author: "Charley"
  - subject: "Many Thanks Sun !"
    date: 2008-01-31
    body: "Now,\n\nAs a Solaris Express user, could KDE help the port of KDE 4.0 over to Solaris ?\nI would love to see KDE 4.0 running on my Solaris box with the all new Kwin !\n\nMany Thanks to everyone's work,\nEdward."
    author: "evocallaghan"
  - subject: "Re: Many Thanks Sun !"
    date: 2008-01-31
    body: "http://people.fruitsalad.org/adridg/bobulate/index.php?/categories/10-Solaris"
    author: "Anon"
  - subject: "Re: Many Thanks Sun !"
    date: 2008-01-31
    body: "Yes, we are doing just this. In fact, we even have a KDE OpenSolaris project established, so stay tuned!\n\nWe'll be updating the solaris.kde.org site soon with more details..."
    author: "kszwed"
  - subject: "opensolaris and kde"
    date: 2008-01-31
    body: "i read some where that opensolaris will ship kde 4, at leas the applications part (no workspace) is the information still relevant.\n\nfriendly"
    author: "mimoune djouallah"
  - subject: "Donations are always wellcome"
    date: 2008-01-31
    body: "We all appreciate donations!\nCheers mate!\n"
    author: "Tuxero"
  - subject: "Who's next?"
    date: 2008-01-31
    body: "Nokia supporting KDE\nSUN supporting KDE\n\nWho's next? Microsoft?"
    author: "Fred"
  - subject: "Re: Who's next?"
    date: 2008-01-31
    body: "Since Microsoft have little history of supporting Free Software compared to the two companies listed, I would guess \"no\"."
    author: "Anon"
  - subject: "Re: Who's next?"
    date: 2008-01-31
    body: "I'd say Apple, through their work on KHTML/WebKit ;-)"
    author: "jos poortvliet"
  - subject: "Re: Who's next?"
    date: 2008-01-31
    body: "How about google?\n\nThey could take a more active part and become a patron of KDE. \nThat would be great!!\n\nI also wouldn't mind if google writes some applications for plasma. Specifically applications that could sync google calendar, google mail, google news, etc. with KDE applications. (I know KDE programmers could do it, but since google stands to benefit the most from it, maybe they should do it themselves. :-) It would free up time for already busy developers to concentrate on the rest of the KDE experience.)\n\n "
    author: "Max"
  - subject: "Re: Who's next?"
    date: 2008-02-01
    body: "Yeah right. First they should try to make Gmail work in Konqueror as a fully-supported browser."
    author: "Eduardo"
  - subject: "Re: Who's next?"
    date: 2008-02-01
    body: "Well yea.. But the other things are important too.\n\nMaybe Google will have an incentive now that the Microsoft-Yahoo deal will mean more interoperability for Microsoft-Yahoo. I'm sure google will want to cement their stake on the desktop too.\n\nI for one will welcome google with open arms. :)"
    author: "Max"
  - subject: "great!"
    date: 2008-01-31
    body: "Big up Sun and Frontline! ty's from the community!"
    author: "meh"
  - subject: "yay"
    date: 2008-01-31
    body: "way to go Sun Microsystems and Frontline! it's nice to see some appreciation for the KDE teams hard work! \n\n"
    author: "Pedro"
  - subject: "a pair of 73 ???"
    date: 2008-02-01
    body: "\n at least was it new?"
    author: "CMS"
  - subject: "Re: a pair of 73 ???"
    date: 2008-02-01
    body: "73GB is a standard 2.5\" SAS size, yes. Let's not forget that KDE doesn't really have monstrous data requirements; no multi-terabyte storage pools are needed (and when they are, we can ask for an X4500 :) ). \n\nWell, except if we're doing an SVN mirror, because the repo is a little over 60GB right now, but Karol is going to get more disk."
    author: "Adriaan de Groot"
  - subject: "Re: a pair of 73 ???"
    date: 2008-02-01
    body: "It was 34 GB in December. I think you're a bit off in your estimates :-)"
    author: "Thiago Macieira"
---
During a tutorial today on-stage at <a href="http://linux.conf.au/">linux.conf.au</a>, <a href="http://www.sun.com">Sun Microsystems</a> and <a href="http://www.frontline.com.au/">Frontline</a> donated a server to the KDE project, available for shipment within hours. Aaron Seigo, Plasma developer and KDE e.V President, accepted a certificate from Ross Cunningham of Sun Microsystems and David Purdue of Frontline on behalf of the KDE project.









<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 300px;">
<a href="http://static.kdenews.org/jr/sun-server.jpg"><img src="http://static.kdenews.org/jr/sun-server-wee.jpg" width="300" height="194" /></a><br />
Ross Cunningham (left) and David Purdue (right) present the certificate for the server to Aaron Seigo and Karol Szwed
</div>

<p>The server is a SunFire X4200, with two AMD Opteron processors, 4GB of memory and a pair of 73GB SAS disks. This generous gift is part of Sun's demonstrated commitment to supporting Open Source, and thus the KDE project.</p>

<p>Adriaan de Groot, in an email statement made on behalf of KDE's sysadmins, thanked Sun and Frontline for their gift. "<em>Large Free Software projects like KDE can be run on surprisingly little infrastructure, but at the same time there is always more, extra OS platform support, developer support, build farming, quality checking, that we can do with a little more machinery and horsepower. We're happy that Sun can help us do more.</em>"</p>

<p>While the intended purpose of the server has not yet been finalised, there has been mention of possibly using it to increase reliability and robustness of the anonsvn service.</p>







