---
title: "KDE and OpenUsability Offer Summer Stipends for Students"
date:    2008-03-21
authors:
  - "skuegler"
slug:    kde-and-openusability-offer-summer-stipends-students
comments:
  - subject: "I wish OpenUsability all the best..."
    date: 2008-03-21
    body: "it really makes KDE better and better. KDE4 looks really nice by default and this is something new here ;]\n\nI wonder only how many students study usability, user-interface design, and interaction design or related subjects? Here in Poland it is not common. "
    author: "Goro"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-22
    body: "Usability is just an personal opinion."
    author: "reihal"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-22
    body: "\"Usability is just an personal opinion.\"\nIMO it is an added value. Compare Ktorrent out of box and Transmission out of box.  Ktorrent is scary for new user but powerful, yet most people will choose Transmission only because it is easier to use. To learn using it. \n\nAn added value for Ktorrent would be better UI out of box with the same amount of features.\n\nAn added value for KDE would be sane defaults with better technology than competition. So in my opinion usability is not just an personal opinion.\n"
    author: "Goro"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-23
    body: "Not true. (Well, in practise it often is true, and that's the exact problem.)\n\nThis Season of Usability is for those with formal education in this field and that's where it's neither guesswork, nor personal opinion. You could've found out yourself by doing the minimum amount of research and thinking before replying.\n"
    author: "Sebastian Kuegler"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-22
    body: "That's not the case of kmail. The usability study messe it up a lot and no one is happy about it (except developers) :-("
    author: "Murdock"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-23
    body: "Just to make the case: Real usability people would be able to back up such a statement with the following data:\n\n- A group of people with [this and that] background have been observed while using both version of kmail \n- The group of people has been chosen based on the following criteria: <insert list of relevant criteria here>, <insert motivation to choose for those criteria here>\n- The old version took them N seconds to walk through a list of tasks: <insert list of tasks here>\n- In the new version, the same actions took M seconds (where N < M)\n- The same tests with another group, consisting of developers <insert motivation to choose for this group here> finished the tests at an average of X seconds, and with the new version in Y seconds (here X < Y)\n\n(You see, this has much in common with scientific methods in other areas.)\n\nSo thanks Murdock for your ad-hoc assessment, but unfortunately you only prove that you just don't understand the difference between people with a formal education in the field, opposed to those that use 'usability' in much the same way as others use Godwin's law.\n\nWith the Season of Usability, OU and KDE want to attract those people with formal education (\"experts\").\n"
    author: "sebas"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-24
    body: "I've yet to see anything of that sort for Kickoff, just that people insist that Kickoff helps usability and productivity, even though it slows you down and adds steps to the process."
    author: "T. J. Brumfield"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-24
    body: "Then you have not bothered to look.\nhttp://en.opensuse.org/Kickoff"
    author: "Morty"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-24
    body: "Your wonderful 5 pass procedure doesn't explain why kmail became so difficult to use in version 1.8.\nPick up two new (but experienced) users and give one kmail 1.7, kmail 1.8 to the other. Measure how much time they spend to associate a mailing list, account end expire settings (common task) to a folder.\nWhere is the proof that \"- In the new version, the same actions took M seconds (where N < M)\"?"
    author: "Murdock"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-24
    body: "Your mistake is false generalization (about usability) and not being specific enough about your problems with kmail. The pim-devel list is the most suitable place for that."
    author: "sebas"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-24
    body: "Why, isn't it an usability problem? What happens if someone asks the usability team an opinion about a program and its usability?\nI still trust usability team, but sometimes things don't go as they should.\n\nhttp://lists.kde.org/?l=kde-usability&m=116057387701141&w=2\n"
    author: "Murdock"
  - subject: "Re: I wish OpenUsability all the best..."
    date: 2008-03-24
    body: "just to be specific...\n\nhttps://bugs.kde.org/show_bug.cgi?id=115611"
    author: "Murdock"
  - subject: "More KDE related projects:"
    date: 2008-03-22
    body: "http://openusability.org/projects.php\n\nwill there be just these 3 or all listed above?"
    author: "Tom"
  - subject: "Re: More KDE related projects:"
    date: 2008-03-23
    body: "OpenUsability suffers from a lack of manpower, so they cannot satisfy every project looking for advice. The projects mentioned above (HIG, KOffice toolbox) are those that will be mentored if a student for those shows up and wants to work on it. (And all that with high-profile mentors such as seele and \\el).\n\nThe projects listed on the page you link are looking for usability advise, but they're not in the focus of the Season of Usability. OU is also very much a hub to bring usability experts and Free Software developers together, one of their tools is the page you mention."
    author: "sebas"
  - subject: "Re: More KDE related projects:"
    date: 2008-03-24
    body: "There are 9 different projects and 10 student openings. The relative KDE ones are 2 student openings for the KDE4 Human Interface Guidelines and 1 student opening for the KOffice toolbox and palette project."
    author: "seele"
  - subject: "Best wishes!"
    date: 2008-03-22
    body: "Celeste and Ellen, I wish you guys all the best! I don't know anything about usability myself, but find your blog posts about usability inspiring, and I hope that the HIG is soon completed and then implemented! Interface consistancy is probably KDE's greatest advantage that we can leverage."
    author: "kwilliam"
  - subject: "Re: Best wishes!"
    date: 2008-03-23
    body: "LOL, \"guys\"???"
    author: "Kevin Kofler"
  - subject: "Re: Best wishes!"
    date: 2008-03-23
    body: "\"Guys\" seems to be unisex in USA, so with their hegemony ;)  of our language :) it's probably true everywhere now. "
    author: "Gerry"
---
Our friends over at <a href="http://openusability.org/">OpenUsability</a> have just started a call for students of usability, user-interface design, and interaction design or related subjects for the <a href="http://season.openusability.org/index.php/projects/2008">Season of Usability</a>. Season of Usability is a project that offers mentoring students that want to work on usability aspects of various projects, including KDE. Students are offered a stipend worth $US1000. KDE is involved in the Season of Usability with three possible stipends, two for students who want to work on the <a href="http://wiki.openusability.org/guidelines/index.php/Main_Page">KDE 4 Human Interface Guidelines</a>, another project aims for improving the toolbox and palette interaction KOffice.



<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right;">
  <img src="http://static.kdenews.org/jr/season-of-usability-logo.png" alt="logo" width="320" height="101" />
</div>

<h2>KDE 4 Human Interface Guidelines</h2>

<p>Working on the KDE 4 Human Interface Guidelines is actually a very exciting thing to do. You will be able to work with some of the world's most excellent programmers, embedded in the friendly community that is KDE. Your work will be very visible, in multiple applications for millions of users, worldwide. Where else can your work have this kind of impact?</p>

<p>Tasks within this stipend include providing content for the HIG, creating guideline checklists for developers, collecting examples, but also higher-level tasks such as identifying repeating interaction patterns and designing solutions for them.</p>

<p>The KDE 4 HIG students will be mentored by <a href="http://weblog.obso1337.org">Celeste Lyn Paul</a> (seele) and <a href="http://ellen.reitmayr.net">Ellen Reitmayr</a> (\el), KDE's usability gurus.</p>

<h2>KOffice Toolbox and Palette Interaction</h2>

<p>KOffice 2.0 has been designed around a palette pattern where tools would be associated with shapes (e.g. text tool, rectangle tool, etc.). The documents are built up from those shapes and information about the shapes and settings for the tools are shown in palettes. The KOffice team has noticed that this design has not worked out quite satisfactorily and the common interaction elements in the applications need improvement.</p>

<p>This project introduces you into the world that is KOffice, the worlds most comprehensive office suite. KOffice has the necessary degrees of freedom to not copy other products, but to come up with innovative solutions supporting workflows that may be different from those people are squeezed into by traditional office suites. This is your chance to gain experience in the productivity software world, and of course become part of the community that is KDE and KOffice.</p>

<p>Mentors are <a href="http://weblog.obso1337.org">Celeste Lyn Paul</a> (seele) and <a href="http://ellen.reitmayr.net">Ellen Reitmayr</a> (\el) and <a href="http://www.valdyas.org/fading/index.cgi">Boudewijn Rempt</a> (boud), KOffice core developer, maintainer of Krita and generally nice guy.</p>

<h2>Applying</h2>

<p>Applicants are expected to have some experience in interface and interaction design and/or usability analysis methods. Basic understanding of those topics is enough, however, as the project is meant to be a learning experience. The application deadline is set to the 14th of April, projects will be starting in May and run into July, maybe August. When applying, keep in mind that you do not need to be a KDE user. For your work you can use a KDE 4 Live CD or other means such as remote desktop access.  See the <a href="http://season.openusability.org/index.php/projects/2008">Season of Usability webpage</a> for more information.</p>



