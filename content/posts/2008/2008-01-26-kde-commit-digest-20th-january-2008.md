---
title: "KDE Commit-Digest for 20th January 2008"
date:    2008-01-26
authors:
  - "dallen"
slug:    kde-commit-digest-20th-january-2008
comments:
  - subject: "4.1"
    date: 2008-01-26
    body: "It's fantastic to read news already talking about KDE 4.1. Gotta love it."
    author: "winter"
  - subject: "Re: 4.1"
    date: 2008-01-27
    body: "My only question, is that in a few of these digests I read that certain things just missed the 4.0 cut, and are being shelved for 4.1\n\nIf these things just missed the cut, couldn't they be released with 4.0.1 or 4.0.2?\n\nNot impatient, just curious.  (Fragments good. Sentences bad.)"
    author: "T. J. Brumfield"
  - subject: "Re: 4.1"
    date: 2008-01-27
    body: "Hi\nNo usually, because x.y.z releases are in feature freeze i think.\nbut in some special cases, some features get into the releases.For example there are some plans to add Customization options to Plasma in 4.0.1 or some features are going to be added in 3.5.9.but they shouldnt break the string freeze (no change in strings)."
    author: "Emil Sedgh"
  - subject: "Re: 4.1"
    date: 2008-01-28
    body: "If there's something in particular you'd like backported, you can always try contacting your friendly distribution packager(s). :-) I can't speak for the other distributions, but here in Fedora, we'll evaluate such requests on a case by case basis."
    author: "Kevin Kofler"
  - subject: "IRC Client"
    date: 2008-01-26
    body: "is there any plan to reactive the IRC plugin for Kopete?\nor porting konversation?\n\nim just runing Kopete from KDE 3.Amarok pre-2 'plays' music, KMail is almost usable and stable (but its not using Akonadi, right?)\n\nKDE 4 is becoming more and more usable...\n"
    author: "Emil Sedgh"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "Everybody raise his hand that still\na) has an icq account\nb) has friends that also have an icq account, but have no jabber account.\n\nIf the amount of hands is low; thats the reason why nobody has stepped up to fix the ICQ plugin :)"
    author: "Thomas Zander"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "*rises hand*"
    author: "Beat Wolf"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "The parent post was talking about IRC, not ICQ."
    author: "Kevin Kofler"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "and just before anyone starts worrying about the state of icq: I was using icq with kopete's kde4 code back in *2006*, when having a chat window open without crashing was a big thing. ;)"
    author: "Chani"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "The ICQ-plugin probably receives the most attention at the moment - Roman Jarosz is simply amazing. His work will be one of my main reasons to switch to KDE4 once it's ready for my needs.\n\nBy the way, don't judge a protocol's popularity by the popularity in your own country... It varies a lot. From my experience here in southern Germany, nearly everybody uses ICQ, a few lost souls are still on MSN or AIM, and almost nobody uses Yahoo or (alas!) Jabber."
    author: "onety-three"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "He was talking about IRC, anyway it's recent news that AOL recently started an experiment integrating AIM/ICQ in Jabber (so you can login with your AIM account using the XMPP protocol)"
    author: "Vide"
  - subject: "Re: IRC Client"
    date: 2008-01-27
    body: "well personally i use IRC with xchat these days\n\nbut when i was on windows, i was using icq a lot\n\n\ni still hope something like licq, but maybe smaller, or \"embeddable\", can show up ... i want a tiny tiny tray icon without any popups on default"
    author: "she"
  - subject: "Re: IRC Client"
    date: 2008-01-27
    body: "/me raises hand"
    author: "MamiyaOtaru"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "> or porting konversation?\n\nKonversation will definitely be ported. The usual mix of real life distractions have put us a bit behind our desired schedule on this one, but it's coming."
    author: "Eike Hein"
  - subject: "Re: IRC Client"
    date: 2008-01-31
    body: "Good News! Thanks in advance."
    author: "stein"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "I'd like to see an IRC plugin for kopete more than konversation.  However, current IRC implementations in all IM clients seem to suck.  They need to (in order of importance)\n\n* stay logged in to channels without opening windows, as if THEY were the account, and the server connection was just a meta-connection.\n\n* show history from a channel when you open it to chat, just like popping up konversation would, if it had been logged in to a channel.\n\n* auto-identify with nickservs\n\n* be able to ignore specific messages from the server, and bots, so there aren't endless annoying new messages when just logging in.\n\nI don't think there's anything else I'd need actually.  With those few things, I could forget that IRC is \"special\" and use it like any other IM account."
    author: "Lee"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "I remember reading in the developer blogs that there's some new IRC client for Qt4. I can't remember it's name, but someone mentioned that since Konversation wasn't ported it would be a good \"excuse\" to try this new one. (It had the unique feature of keeping the IRC connection open even when you closed all the windows, or something like that.) I simply cannot remember it's name. Anyway, since IRC is an essential means of communicating among developers, I'm sure some sweet IRC solutions will be available for KDE 4.1, whether it's Konversation, Kopete, or something else.\n\nAnd I suggest the guy who replied about ICQ get some sleep, or a new pair of glasses, lol. :-)"
    author: "kwilliam"
  - subject: "Re: IRC Client"
    date: 2008-01-26
    body: "This new IRC client is Quassel, developed by a friend of mine:\n\nhttp://quassel-irc.org/\n"
    author: "Mark Kretschmann"
  - subject: "Re: IRC Client"
    date: 2008-07-25
    body: "This one looks really nice, at least until konversation is ported. Thanks! :)"
    author: "lukrop"
  - subject: "Re: IRC Client"
    date: 2008-07-02
    body: "This is something I really miss in Kopete-kde4 ..."
    author: "PhilippeP"
  - subject: "Naysayers proven wrong"
    date: 2008-01-26
    body: "\"The post-KDE 4.0 commit surge continues this week, with 3043 commits. (...) . There is a real buzz to KDE development right now, an extra edge to what is already a vibrant atmosphere, and it is evident everywhere, from IRC to SVN.\"\n\nThis proves the naysayers (especially the idiotic OSNews.com crew) wrong who complain that the 4.0 release should have been delayed. If 4.0 wasn't released like this, the commit surge wouldn't have happened."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Naysayers proven wrong"
    date: 2008-01-26
    body: "Wrong, it's the opposite. It's proof for a truth behind the critics."
    author: "Chaoswind"
  - subject: "Re: Naysayers proven wrong"
    date: 2008-01-27
    body: "You can't prove the opposite.\nThe reality is: 4.0 released => development activity increases.\nThis is a benefit for the project overall.\n\nYour claim is just pure speculation. "
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Naysayers proven wrong"
    date: 2008-01-27
    body: "Correlation does not imply causation.\n\nYour claim is also pure speculation."
    author: "Anon"
  - subject: "Re: Naysayers proven wrong"
    date: 2008-01-27
    body: "His claim is indeed also speculation, but it is a fact that the amount of commits has increased after 4.0 for WHATEVER reason.\n\nAnd this is good insofar as most commits IMPROVE a situation. "
    author: "she"
  - subject: "Re: Naysayers proven wrong"
    date: 2008-01-27
    body: "Right and Wrong.\nYes, after KDE 4.0 release a ton of commits went to subversion tree.\nBut the reason isn't only because 4.0 brought more developers or made people write faster. Don't forget there was a API/ABI freeze and much of newer feature where developed in branchs or even in the author machine without any commit.\n\nIt's kind of natural to see a surge of commits after a freeze is dropped :)"
    author: "Iuri Fiedoruk"
  - subject: "Delay"
    date: 2008-01-27
    body: "I think they correctly delayed from what would have been a disastrous October release to a promising (if frustrating) January release."
    author: "T. J. Brumfield"
  - subject: "Re: Naysayers proven wrong"
    date: 2008-01-28
    body: "And how many of those commits are to TRUNK and how many are to the 4.0 BRANCH?\n\nThis seems to prove nothing except that, as usual, there is more interest in doing new features than in fixing bugs.  For example, since the release was tagged ( 758631) there have been more than twice as many Plasma commits to TRUNK as to the 4.0 BRANCH.\n\nIMHO, the critics (which are NOT naysayers but, rather, realists) are correct that KDE-4 wasn't ready for the 4.0.0 release.  The release proves it.  The desktop lacks significant functions and there are many serious bugs (e.g. 154595).\n\nNow I hope that these bugs are promptly fixed but I fear that KDE will return to the seriously defective development model where bugs reported in the current release BRANCH are considered to be fixed if they work in TRUNK.  Bugs in the release BRANCH need to be fixed in the BRANCH.  The reason for this should be obvious, but it is also advantageous because the BRANCH is more stable and the bug can be properly fixed.  Note that if the current code base is going to last for years, that bugs need to be properly fixed, not just hacked."
    author: "YAC"
  - subject: "Pluto is a dwarf planet"
    date: 2008-01-26
    body: "\"We have already been deriving Pluto from the KSAsteroid class for practical reasons. With this change, Pluto is now labeled an \"asteroid\" in the details dialog.\"\n\nBut Pluto is not an asteroid, is a dwarf planet: http://en.wikipedia.org/wiki/Dwarf_planet"
    author: "Rub\u00e9n Moreno Montol\u00edu"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-26
    body: "For me Pluto stays a true planet"
    author: "gerd"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-26
    body: "I thought Pluto was the Dog Star."
    author: "reihal"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-26
    body: "Spaceballs ftw! :-D"
    author: "chris.w"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-26
    body: "We Love Pluto!!!!! =)\n\n"
    author: "Richard Lionhard"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-27
    body: "I thought it was a kuiper belt object.\n\nBut as far as I'm concerned there is no standard definition of a planet at the moment, which is pretty silly."
    author: "T. J. Brumfield"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-28
    body: "There is a standard definition of a planet:\n\n(1) A planet is a celestial body that (a) is in orbit around the Sun, (b) has sufficient mass for its self-gravity to overcome rigid body forces so that it assumes a hydrostatic equilibrium (nearly round) shape, and (c) has cleared the neighbourhood around its orbit.\n(2) A dwarf planet is a celestial body that (a) is in orbit around the Sun, (b) has sufficient mass for its self-gravity to overcome rigid body forces so that it assumes a hydrostatic equilibrium (nearly round) shape, (c) has not cleared the neighbourhood around its orbit, and (d) is not a satellite.\n(3) All other objects orbiting the Sun shall be referred to collectively as \u0093Small Solar System Bodies\u0094.\n\nMore info: http://www.answers.com/planet&r=67"
    author: "Rub\u00e9n Moreno Montol\u00edu"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-27
    body: "While I prefer feedback through bugs.kde.org or on our mailing list, you're absolutely right about this.  I'll fix it soon...\n"
    author: "LMCBoy"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-28
    body: "Ok, thanks! Next time I'll use the proper method to notify bugs."
    author: "Rub\u00e9n Moreno Montol\u00edu"
  - subject: "Re: Pluto is a dwarf planet"
    date: 2008-01-28
    body: "FYI, fixed in trunk (can't backport due to new string, \"Dwarf planet\"):\n\nhttp://websvn.kde.org/?view=rev&revision=767475\n\n"
    author: "LMCBoy"
  - subject: "thanks"
    date: 2008-01-26
    body: "A great read as usual.  Thanks Danny!"
    author: "mactalla"
  - subject: "Akondi now?"
    date: 2008-01-26
    body: "So, is Akondi working in 4.0.x now, or is it going to be in 4.1? I seem to remember it and PIM being scheduled for 4.1.\n\nAlso, I love all these backends. It should be great for developers and users alike."
    author: "Soap"
  - subject: "Re: Akondi now?"
    date: 2008-01-26
    body: "Thats planned for 4.1\n\n4.0 has no pim\n\n"
    author: "Emil Sedgh"
  - subject: "Re: Akondi now?"
    date: 2008-01-27
    body: "I imagined that having a contact/pim backend would be crucial to making good use of nepomuk."
    author: "T. J. Brumfield"
  - subject: "Sigh..."
    date: 2008-01-26
    body: "Uhm. Over the years people developed a plethora of KDE-based media players (KPlayer, KMPlayer, Kaffeine, Noatun, and soon even Amarok will have video support), but in KDE 4.1 the \"official\" video player (as it's the only one on kdemultimedia, everything else is in extragear or elsewhere) is going to be a ultra-basic thing that (I quote from the homepage) \"has not yet made a stable release, but it is getting there\"...? What a curious decision :/\n\n(Note: this is not a flame. I just think is curious, there's probably a technical reason for this but I can't see it and I'd like that somebody would tell me)."
    author: "GiacomoL"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "Dragon Player is actually the KDE 4 incarnation of a player that existed in the KDE 3 days as well, namely Codeine, written by one of the Amarok developers. Dragon Player is the KDE 4 rewrite, designed to use Phonon from the outset."
    author: "Eike Hein"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "I like none of the players you mention, except KMPlayer for playing in Konqi. Codeine/Dragonplayer rules them all in terms of just playing movies."
    author: "jospoortvliet"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "+1"
    author: "Mark Kretschmann"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "another +1"
    author: "Emil Sedgh"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "+1\nmoreover, the default KDE multimedia player should be straight, simple, quick to load, and Dragon Player has everything"
    author: "Vide"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "+1"
    author: "Richard Lionhard"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "Absolutely.  I have no need for any of the features in the other video players.  Codeine/Dragon Player does what I need it to do without getting in the way.  Once in a blue moon I fire up VLC for a stubborn video file that xine has issues with or to get at the slow motion feature."
    author: "Leo S"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "Dragon player is one of the best/simple video players, but are they going to keep that name? Come one, Dragon Player, thats more childish then something that starts with K."
    author: "Jeremy"
  - subject: "Rule #43"
    date: 2008-01-27
    body: "No Dot story without someone having a different opinion about some name. :~)"
    author: "sebas"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "Is there an easy way to switch the audio track or turn subtitles on and off?  Is there a way to record whatever internet stream it is playing?  Without those features I would respectfully have to stick with Kaffeine.\n\nIt would then be a case of \"90 percent of people only use 10 percent of the features, therefore we can cut the other 90 percent of the features and still keep 90 percent of the people happy\" backfiring, since the set of features people uses differs from person to person.\n\nI really do miss the days when KDE was about enabling, and giving features, and allowing users the opportunity to ignore them, but at least they *are there* for those who do use them.  If my software is going to ascribe to gnome philosophies, I might as well use Gnome."
    author: "MamiyaOtaru"
  - subject: "Re: Sigh..."
    date: 2008-01-28
    body: "You know, there's a reason it's so common to hear of application \"extensions\" or \"plug-ins\" these days.\n\n"
    author: "spaceboy"
  - subject: "Kaffeine is still a good choice"
    date: 2008-01-28
    body: "You can switch audio tracks and subtitles. I do plan on adding any feature that's that's make or break in being able to watch a movie. Having subtitles for a foreign film would be one of those. :)\n\nRecording streams is actually a (somewhat hidden) feature Codeine had, so who knows I might add it back sometime in the future. \n\nBut really, no one is taking away your Kaffeine. How does having a simple video player mean that omgbbq KDE hates features. Kaffeine is alive and well in KDE extragear and I expect it to be back ready to serve their current users and new users who just expect more features out of their video player."
    author: "Ian Monroe"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "I haven't been able to test Dragon, but I really like Codeine for playing movies from DVD. The no-frills interface makes it nearly perfect for this.\nTo be *really* useful for playing compressed movies, it has to be able to change sound sync on the fly, though.\nI also think you have to be able to skip forward-backwards and a few other things like that.\nBut as I said, I haven't tried Dragon - maye those things are already in in."
    author: "Goran J"
  - subject: "Re: Sigh..."
    date: 2008-01-28
    body: "You can use the slider to skip forward and backward, just use the slider. True for Codeine and Dragon Player. If you mean in DVD chapters, that is a feature that I plan on adding in the future.\n\nI don't get the point about changing sound sync though... I've never had this problem."
    author: "Ian Monroe"
  - subject: "Re: Sigh..."
    date: 2008-02-01
    body: "> You can use the slider to skip forward and backward, just use the slider. \n\nYes, of course, but it's much more precise to use the arrow keys to jump a few seconds backwards and forwards. Especially when you don't use a regular mouse, but a small trackball :)\n\n> I don't get the point about changing sound sync though...\n\nIt's a rather common problem with .avi files. I don't know what causes it, though."
    author: "Goran J"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "I'm quite glad that Dragon Player is in kdemultimedia, but the value of being in a KDE module is often overstated. For instance in KDE3, I guess under your theory Kaboodle was the official KDE video player. The reality is that most KDE-distros shipped Kaffeine as their default player. The KDE modules do serve as a way of raising awareness of KDE apps, especially within the project. But its mostly a form of release management.\n\nAnyways don't knock Dragon Player until you've tried it. It has a simple user-interface, that's a lot different then being ultra-basic."
    author: "Ian Monroe"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "The name sounds really bad."
    author: "gerd"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "name sounds bad? im happy to see a move away from Kthis and Kthat with KDE4. plasma, dolphin, amarok, phonon, dragon player, etc. etc. all show a move in naming, and its great. The Kapplications is not only ugly, also its inconvenient in menus to look through."
    author: "Peter"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "Agreed that it's an improvement to drop the Kthis, Kthat, but \"Dragon player\" sounds like a game not a media application.  Just \"Dragon\" would be fine though. Given there is some talk of applications being listed with descriptions in the menu listings, the extra descriptiveness isn't needed: it is \"just a name\" after all..\n\nIf the menus go that way, we'll have: \"Dragon Player (Movie Player)\" ...which is a bit redundant...\n\nKopete Chatter (IM Client)\nKonqueror Browser (Web Browser)\nKmail Emailer (Email Client)\n\nSee what I mean?\n\nLooks good, name aside."
    author: "Anon"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "I think 'Dragon' itself is a good name, however the name is not that much of a problem. It is that Dragon by itself does not necessarily mean anything in terms of what the application can do, but I do not think a name should do that anyways. Consider how people of a given profession were addressed ever so long ago, a person could be \"titled\" John (Black)smith and while his name would 'John' his profession would be a blacksmith. The same could be applied here; the name is Dragon and the profession-if you will-is a media player so it could be titled Dragon media-player. That way people know what it does while giving it a unique name. Also for instances like the 'Kmenu' only the name would have to be displayed resulting in something like Dragon (Media Player) so no redundancy. :)"
    author: "Woe"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "Because it's really obvious what Okteta is for.  KHexEdit was just too obscure.\n\nWhy K*appname* gets slammed when G*appname, i*appname* and Win*appname* get a relatively free pass is beyond me.  I appreciate a name that tells me at a glance what it does, and for which OS/environment it is created.\n\nI think the move away from such names it the latest mass hysteria to strike KDE, in the same vein as the widespread love at the time for Keramik.  Given time, perhaps this move will be seen as something other than a bright idea, the same way Keramik was tossed into the junk heap."
    author: "MamiyaOtaru"
  - subject: "Re: Sigh..."
    date: 2008-01-28
    body: "+1\n\nin freshmeat, if an app name is unknown to me, then\nif its name starts with a k I give alook at it as it is highly probable that it'll be a kde app that takes benefit of the integrated desktop.\nelse I skip as if it's a graphical app, it won't take benefit of the tight integration, thus: useless.\n"
    author: "olahaye74"
  - subject: "Re: Sigh..."
    date: 2008-01-26
    body: "I disagree. \"gerd\" sounds really silly though."
    author: "Mark Kretschmann"
  - subject: "Re: Sigh..."
    date: 2008-01-27
    body: "KDE has always been about choice.  You seem to suggest that the current state of KDE represents KDE's final goal, which is a falacy.  KDE 4.0 didn't have a video player at all, so you're suggesting officially KDE abandoned video players, and by having 1 video player in KDE 4.1 the official stance is to only have one official player.\n\nBasically, don't read too much into the fact that not all the KDE 3 apps have been ported over.  In a volunteer world, people opt to port over what they opt to do so.  I'm sure most of the KDE 3 apps will get ported over in time."
    author: "T. J. Brumfield"
  - subject: "K3B anyone? "
    date: 2008-01-27
    body: "What happened to K3B? Does it get ported to QT4 and get a sexy new oxygen look or is there something different?"
    author: "Jeremy"
  - subject: "Re: K3B anyone? "
    date: 2008-01-27
    body: "I think there is a very nice work going on it because everytime i update extragear/multimedia, i see that many files are updated from K3B.\nand probably a refactoring to use solid for hardware abstraction...?"
    author: "Emil Sedgh"
  - subject: "Re: K3B anyone? "
    date: 2008-01-27
    body: "There is a blog entry on k3b.org from the 24. January about it:\nhttp://k3b.plainblack.com/k3b-news/blog"
    author: "cheko"
  - subject: "Re: K3B anyone? "
    date: 2008-01-27
    body: "@Danny or @Troy, little suggestion: Could you interview Sebastian Trueg about state of K3b-kde4 and post it in another digest or similar? A lot of people would be interested "
    author: "Koko"
  - subject: "Re: K3B anyone? "
    date: 2008-01-27
    body: "Don't forget also that k3b is one of the most rock-solid apps in all KDE world.\nSo we should not expect a quick-bugged-rushed version soon, they probally won't do the same as KDE team did for 4.0 ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: K3B anyone? "
    date: 2008-01-28
    body: "Aaron and the rest of the team bleeds from the little almost unnoticeable hit."
    author: "Luis"
  - subject: "Re: K3B anyone? "
    date: 2008-01-30
    body: "But so true."
    author: "Johnny Awkward"
  - subject: "MVC Framework"
    date: 2008-01-27
    body: "Pardon my observation but I keep seeing this MVC embrace and have to rant:\n\nI just love how the C++ world and Java world has \"embraced\" this Smalltalk/ObjC/Cocoa MVC world.\n\nYou abhorred it when NeXTStep/Openstep had it as the center of it's entire development paradigm.\n\nSuddenly we see this grand renaissance as if it never happened but within the Java/C++ world.\n\nend of rant."
    author: "Marc Driftmeyer"
  - subject: "Re: MVC Framework"
    date: 2008-01-27
    body: "the point is not in 'who was first' (or 'who's to blame' / 'what to do next'), the point is in the fact that idea exists and in bringing it into life (realization).\n\nThe devil is in the details"
    author: "Nick Shaforostoff"
  - subject: "kopete"
    date: 2008-01-27
    body: "On KDE 4 I was having problems with kopete last night, it wouldn't connect to msn.  So I updated KDE then went to bed, this morning Kopete wont even open.  shows up on the task manager for about 10 seconds then closes.  How do I find what is happening so that I can report it?"
    author: "RJ"
  - subject: "Re: kopete"
    date: 2008-01-28
    body: "I would open it from konsole in order to see some terminal output..."
    author: "Sebastian"
  - subject: "Re: kopete"
    date: 2008-01-28
    body: "Yeah, thats what I want to do, but i've barely used konsole so don't know what to type in Konsole to open it.  I'm on opensuse 10.3, if anyone needs to know that."
    author: "RJ"
  - subject: "Re: kopete"
    date: 2008-01-28
    body: "Just type 'kopete' (minus the quotes), then enter."
    author: "Haakon Nilsen"
  - subject: "Re: kopete"
    date: 2008-01-28
    body: "hi, yeah I have done that, that's why i thought something else had to be written because when I type it nothing happens.  I've tried reinstalling kopete and everything. "
    author: "RJ"
  - subject: "Kget finally starting to catch up?"
    date: 2008-01-28
    body: "Finally, mirror search functionality.  But will it also:\n\n - automatically split downloads into multiple parts\n - download several parts simultaneously from different mirrors\n - measure mirror response time and dynamically adjust which mirrors\n   it uses?\n"
    author: "Yeah Right"
  - subject: "NOKIA aquires Trolltech ! ? ! ? ! ?"
    date: 2008-01-28
    body: "What's going on here? First I read this I was quite shocked, but as the dust is settling, I wonder what are the positive effects?\n"
    author: "Thomas"
  - subject: "Re: NOKIA aquires Trolltech ! ? ! ? ! ?"
    date: 2008-01-28
    body: "I thought the same, WTF!\n\n"
    author: "peter"
  - subject: "Good news"
    date: 2008-01-28
    body: "A positive aspect is that Nokia will apply to become a Patron of KDE. It is announced in an open letter addressed to KDE :-) and the opensource community: http://trolltech.com/28012008/28012008-letter\n\nThis is very good news to have support from such a firm. Nokia will probably rely on Qt for its products, which is very positive for Qt, and therefore for KDE in the end."
    author: "Hobbes"
  - subject: "Re: NOKIA aquires Trolltech ! ? ! ? ! ?"
    date: 2008-01-28
    body: "there are no positive effects.\nin the short they will pretend to support opensource.\nbut in the long term KDE will have to switch to something like openqt.\n"
    author: "asdf"
  - subject: "Re: NOKIA aquires Trolltech ! ? ! ? ! ?"
    date: 2008-01-28
    body: "I don't see this as a positive thing..\n\nEmbrace, extend, extinguish.\n\nWell Nokia isn't that evil. They will just stop bothing with open source updates, or helping the KDE community beyond what's necessary to support mobile phones.\n\nThis is a sad day for open source.\n\n"
    author: "Richard Lionhard"
  - subject: "Nokia & Trolltech"
    date: 2008-01-28
    body: "Ouch, I really don't like this. Trolltech will now be part of a slow-moving, corrupt, pro-software-patent company that is against open standards like ogg.\n\nWhat currently bothers me most (and I haven't even seen anyone asking) is that probably QTopia is going to die. Because I don't see Nokia switching to an opensourced phone architecture. And why would a company that openly rips subsidies and fires people making a plus invest in a second phone framework?\n\nIMO, Nokia sucks both morally and technologically, and it's sad to see Trolltech going there."
    author: "anonymous coward"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-28
    body: "I couldn't agree more"
    author: "asdf"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-28
    body: "In other news, Nokia, a pro-software patent company releases millions of lines of code under both the GPL v2 and the GPL v3 license.\n\nNow that Nokia 'owns' KDE, their actions with regard to Trolltech will make their stance on Free Software very clear.\n\nThis is a very interesting development, potentially harmful, but also potentially very positive for KDE and Free Software in general.\n\nLet's hope that Nokia will not cut resources on the many useful technological developments.\n\n"
    author: "Jos"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-28
    body: "Just curious, but what GPL code has Nokia released?"
    author: "T. J. Brumfield"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-29
    body: "A lot, both kernel code and GTK stuff. They also work on WebKit."
    author: "jos poortvliet"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-29
    body: "> They also work on WebKit.\nno, they don't. They just forked webkit years ago to \ntailor it to a tablet product and that's it.\nPure opportunism. No commitment.\n\n\n"
    author: "julia"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-28
    body: "This is a sad day for open source..\n\nQt will die beyond what's necessary for phones...\n\nWhat, google wasn't available to buy trolltech? C'mon.\nGoogle at least has a policy of \"don't be evil!\"\n\nI was so exited that google jumped on the KDE bandwagon, then this...\n\nWhat is this the dot-opensource? Now it seems that all these old companies are buying up open source projects. What will come of this?\n\nFirst, SuSE, then mysql, then Trolltech, what's next?\n\nQt we will miss you. You had so much going for you in the Qt 4.x version..\n\n-Richard"
    author: "Richard Lionhard"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-29
    body: "The biggest problem I see with this takeover by Nokia is that it will marginalize whatever neutrality TrollTech offerred when dealing with other massive mobile and desktop players. For instance, when KDE4 applications show up on winmac systems the end users would have seen them as almost generic \"open source\" additions to their desktop, now it will tilt more towards \"hey, check out this Nokia stuff running on my winmac desktop\". Same with Qtopia, it was just another open source option available to run on whatever hardware could support it but now it's \"that Nokia stuff\" that may start to find itself actively inhibited in non-Nokia aligned hardware. I can't imagine Steve Jobs ever being happy about Nokia based software running on any Apple based products whereas before it would have been more of a \"we don't care about marginal stuff from TrollTech\". This will be bad for KDE if KDE is seen as an extension, a subsidiary, of Nokia (from other corporate players point of view), or, it could be okay if KDE is forced to establish more of it's own identity divorced from Nokia."
    author: "markc"
  - subject: "Re: Nokia & Trolltech"
    date: 2008-01-29
    body: "i doubt that risk is real, kde isnt viewed as a extension of trolltech right now"
    author: "Beat Wolf"
---
In <a href="http://commit-digest.org/issues/2008-01-20/">this week's KDE Commit-Digest</a>: Taskbar and KMenu functionality from KDE 3.5 returns to the <a href="http://plasma.kde.org/">Plasma</a> panel, and work on clocks in Plasma, with the move of the binary-clock Plasmoid to kdereview. Improvements in annotation handling in <a href="http://okular.org/">Okular</a> (which has been officially capitalised). Essential support for viewing bug contents in the rewrite of KBugBuster. More data export options (CSV, HTML, etc) in <a href="http://edu.kde.org/kalzium/">Kalzium</a>. The CVS implementation in <a href="http://www.kdevelop.org/">KDevelop</a> moves to the Model/View framework. The start of JavaScript functionality in <a href="http://kst.kde.org/">Kst</a> plugins. Usability refinements in <a href="http://konsole.kde.org/">Konsole</a>. <a href="http://www.mailody.net/">Mailody</a> begins to be ported to the <a href="http://pim.kde.org/akonadi/">Akonadi</a> service. A "mirror search" plugin for <a href="http://kget.sourceforge.net/">KGet</a>. IPv6 work in <a href="http://ktorrent.org/">KTorrent</a>. Colour docker improvements across <a href="http://koffice.org/">KOffice</a>. Optimisations in <a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. Various work in KJS and KHTML. Support for the <a href="http://wiki.xmms2.xmms.se/index.php/Media_Player_Interfaces">MPRIS</a> multimedia player interaction specification in <a href="http://dragonplayer.org/">Dragon Player</a>, with Dragon Player moving from playground/multimedia into kdemultimedia for KDE 4.1. The <a href="http://kopete.kde.org/">Kopete</a> Bonjour protocol moves to kdereview. The copy of Qt within KDE SVN is updated to be GPL version 3 compatible. <a href="http://commit-digest.org/issues/2008-01-20/">Read the rest of the Digest here</a>.

<!--break-->
