---
title: "KDE Commit-Digest for 9th November 2008"
date:    2008-12-29
authors:
  - "dallen"
slug:    kde-commit-digest-9th-november-2008
comments:
  - subject: "Danny"
    date: 2008-12-29
    body: "I want to have children with you <3"
    author: "Lisa Engels"
  - subject: "Re: Danny"
    date: 2008-12-29
    body: "I suspect there wouldn't be such activity on Danny's days off. He seems quite busy.\n\nDerek"
    author: "dkite"
  - subject: "Re: Danny"
    date: 2008-12-29
    body: "Danny's turbo is on again :)\n\nI'm still waiting for when he catches up and starts doing digests for future dates. Might even be a nice joke for april fools *hint hint*."
    author: "IAnjo"
  - subject: "Re: Danny"
    date: 2008-12-30
    body: "wow, I'd love to see a commit digest for december 2009 already. Must include lots of cool, interesting stuff.\n\nBTW why refer to april fools? You doubt Danny can do this? If the last few days have shown us anything, it's that Danny can do more than we thought was possible ;-)"
    author: "jospoortvliet"
  - subject: "wow"
    date: 2008-12-29
    body: "wow, commit-digest frenzy again\n\nthankz men."
    author: "rudolph"
  - subject: "Kmail Message list"
    date: 2008-12-29
    body: "I personally do not like the new default display for the message list.  I wish when new features are added that they wouldn't be set as the default, but would leave the current settings as they are.\n\n"
    author: "R. J."
---
In <a href="http://commit-digest.org/issues/2008-11-09/">this week's KDE Commit-Digest</a>: Merge of the <a href="http://kontact.kde.org/kmail/">KMail</a> <a href="http://www.commit-digest.org/issues/2008-06-22/#1">Summer of Code project (including a reworked folder tree and a new message list)</a> to trunk (in time for KDE 4.2). The alternative "QuickSand" interface is integrated into KRunner. Option to enable image support in Klipper, with the start of efforts to convert the utility to KConfigXT. <a href="http://phonon.kde.org/">Phonon</a> intuitively responds to removal of internal (non-hotpluggable) sound devices. First version of scripted services for browsing content from BBC (with basic search ability) and NPR, and beginnings of a SongKick applet in <a href="http://amarok.kde.org/">Amarok</a> 2. Use of <a href="http://solid.kde.org/">Solid</a> in <a href="http://www.digikam.org/">Digikam</a> to identify pictures already downloaded from cameras. RAW image support added to slideshows in kipi-plugins (used by Digikam, etc). Scripted ability to resume paused torrents after a configurable amount of time in <a href="http://ktorrent.org/">KTorrent</a>. Initial support for extraction of EMF files from the Compound File Binary Format (Microsoft OLE file format) in <a href="http://okular.org/">Okular</a>. Practice start dialog with statistics and settings display in <a href="http://edu.kde.org/parley/">Parley</a>. Simple graphs and multiple graphs working in Rocs. A basic GetHotNewStuff implementation in Palapeli. Further work in <a href="http://www.kdevelop.org/">KDevelop</a>. New converter between KPIM and KABC distribution lists in <a href="http://pim.kde.org/">KDE-PIM</a>. Support for import of KnowIt files into KJots. "Eyes" applet moves to kdeplasma-addons, while "kbstateapplet" moves to kdereview. Python bindings for the <a href="http://plasma.kde.org/">Plasma</a> library move to kdebindings. Plasma moves to from kdebase/workspace to kdelibs.<a href="http://commit-digest.org/issues/2008-11-09/">Read the rest of the Digest here</a>.

<!--break-->
