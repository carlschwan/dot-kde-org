---
title: "KDE Commit-Digest for 23rd March 2008"
date:    2008-03-31
authors:
  - "dallen"
slug:    kde-commit-digest-23rd-march-2008
comments:
  - subject: "Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: " "
    author: "Danny Allen"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Another great digest Danny. For me the articles with the screenshots are the most interesting anyway. I enjoyed reading about KOrganizer, WorldClock and KSysguard.   With the worldclock plasmoid, will it also be possible to combine it with the panel clock and calendar view to match the feature in the latest release of GNOME? I must say that this worldclock already looks more detailed and prettier than theirs :) (thanks marble!)"
    author: "Darryl Wheatley"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "And the clouds are actually the real clouds covering the world!"
    author: "Inge Wallin"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Woah - seriously?"
    author: "Anon"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "yeah, afaik these are real clouds provides by nasa..."
    author: "Robin"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "NASA make clouds, now? ;)\n\nAnyway - cool, that's a really neat feature :)"
    author: "Anon"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Yes, but they don't get updated yet (right now you need to delete them manually still. But we'll fix that before 4.1."
    author: "Torsten Rahn"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Just to get this straight: The data that is placed on the server gets updated all the time (we are basically using the same data that XPlanet does which has realtime clouds as well). However right now Marble only fetches it once. So if you want an updated cloudcover you need to delete the cloud data that marble has fetched by hand. We'll add some mechanism that does this automatically later, so you'll be able to have real time clouds that update automatically for KDE 4.1\n"
    author: "Torsten Rahn"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Are the dark and lit regions also displayed correctly wrt. the seasons? In the screenshot it looks like both the north and south pole are lit at the same time...\n"
    author: "random kde user."
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Basically: yes.  But the light zones are a little bigger than the dark zones of the earth due to light dispersion in the atmosphere.\n\nRemember that exactly now is the mid point between winter and summer all over the world.  So it's dawn at the north pole and dusk at the south pole."
    author: "Inge Wallin"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Yes, they are displayed correctly wrt the seasons.\nThe rather \"boring\" situation that you see on the plasmoid screenshot is due to the fact that we just had the equinox where spring started. At that time both poles are somewhat lit by the sun at the same time. But yes, it looks a bit as if both poles are lit a bit too much at the same time. I've just checked it back and it seems to be due to the shading in the textures.\n\nFor a different point of time that might appeal more to you you might want to have a look at the first screenshots here:\n\nhttp://www.kdedevelopers.org/node/3275\n"
    author: "Torsten Rahn"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-04-01
    body: "Neat, though I'm not sure it makes the clock more usable."
    author: "Joergen Ramskov"
  - subject: "Re: Statistics will be uploaded in about an hour."
    date: 2008-03-31
    body: "Good work Danny!!!"
    author: "Fran"
  - subject: "Finally: Process Monitoring, Thanks! :)"
    date: 2008-03-31
    body: "Now the only thing missing, is a button for the window-Decoration, to reach this function faster. And of cource, an option for kwins window-specific settings, to enable an automatic logging *hinthint*"
    author: "Chaoswind"
  - subject: "Re: Finally: Process Monitoring, Thanks! :)"
    date: 2008-04-01
    body: "First add \"above all\" button to Oxygen ;]"
    author: "Koko"
  - subject: "Re: Finally: Process Monitoring, Thanks! :)"
    date: 2008-04-02
    body: "indeed , johnflux ftw ! :)"
    author: "mxttie"
  - subject: "Nice review!"
    date: 2008-03-31
    body: "Thanks Danny :)\n\nFew highlights for me:\n\n- Looks like To-Do module of Kontact will be usable after all, at the moment (3.5.9) it is :/ especially in contrast with the rest of it\n- Gilles again on top with work on digiKam (and it feels testing new features)\n- Konsole and catching of key events - more refinement is :)\n- as always KHTML developers shine :)\n- and final proof we need more mini-kdevelopers: and also the youtube miniature thingy because my daughter says so ;)"
    author: "m."
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "Yes,\n\nI'm again on top commiter this week (:=)))\n\nIn fact both digiKam branches are in active developpement: KDE3 (0.9.4) and KDE4 (0.10.0). There are a huge changes in KDE3 which are backported automatically in KDE4. It's a big work to code, test, and valid...\n\nAbout KDE4, I plan to review all new features implemented by me and Marcel  (and there are a lots (:=))). I will post a new entry in my blog this week...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "Rocking! Digikam is one of my favourite applications. Thanks for getting it where it is. Keep up the good work, i'm already looking forward to the KDE4 port. "
    author: "sebas"
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "Is the digikam version for KDE4 already usable? I mean, can I loose photos or data with it, or is this unlikely? I can live with missing functionality and the occasional crash - you're getting used to this with KDE4 ;))). But it would be great to switch to a native KDE4 app for photo management.\n\nDigikam, Amarok and k3b are my favorite apps, and unfortunately all those are not out for KDE4 yet."
    author: "Anon"
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "We have planed to release 0.10.0 for septemeber, if all is fine. \n\nRegression tests take a while. I wouldn't provide a \"stable\" release full of bug. \n\nLike you can see, porting a Qt3/KDE3 application like digiKam, K3b or Amarok to Qt4/KDE4 is not a simple task. All must be re-tested, some parts re-written, because changes in API are huge. The advantages is to review all old codes and to make factorization, re-writting, simplifications and improvements.\n\ndigiKam still in alpha. Do not use it in production yet, at least until first 0.10.0 release candidate.\n\nThere is also kipi-plugins and all shared libraries where we have working hard  me and Marcel to port in native QT4/KDE4 : \n\n- libkdcraw (including now 16 bits color depth auto-gamma/auto white balance)\n- libexiv2 (including now full XMP metadata support)\n- libkipi (partially re-written and cleaned).\n- 7 kipi-plugins fully ported bu me (SendImages, RAWConverter, JPEGLossLess, FlashExport, TimeAdjust, MetadataEdit, and AcquireImage).\n\nI can post here few screenshots of digiKam for KDE4 in action:\n\nhttp://digikam3rdparty.free.fr/Screenshots/newsearchwindow.png\n\n==> the new Search tool make by Marcel. It's not yet complete, but it's planed to finalize this interface for beta1. Like you can see, a big work have been done to make a clean interface without any database-sql requests. It's more users friendly. Note than Database schema have been re-written and improved to include severals photo metadata informations. For ex, GPS info are now stored in DB: all read-only files can be geo-localized.\n\nhttp://digikam3rdparty.free.fr/Screenshots/thumbbarwithpreviewmode.png\nhttp://digikam3rdparty.free.fr/Screenshots/FullColorThemeSupport/thumbbarineditor.png\n\n==> In digiKam for KDE4, thumbnail kio-slave is diseapears. All thumbs are now generated using multi-threading (another part implemented by Marcel)\nIf you have already played with Showfoto, you have certainly seen a thumbbar. KDE3 implementation use kioslave without memory cache mechanism. It's slow.\nThe new one is more faster and can be included as well everywhere in digiKam without to decrease performance. I have included thumbbar in Image Editor (F4) and in AlbumGUI with preview mode (F3). A same cache is used everywhere. \n\nhttp://digikam3rdparty.free.fr/Screenshots/digikam_for_KDE4_with_XMP_metadata_support.png\nhttp://digikam3rdparty.free.fr/Screenshots/MetadataEditor/\n\n==> in KDE4, XMP is supported everywhere. I have improved the MetadataEditor kipi-plugin in this way and re-writted all dialog pages to be more user friendly, and to be homogenous with others tools available under MAcOS-X or Win32.\n\nhttp://digikam3rdparty.free.fr/Screenshots/digikamKDE4_under_MACOSX.png\n\n==> Gutavo Boiko, have ported several code in digiKam to compile fine in native under MACOS-X. \n\nhttp://digikam3rdparty.free.fr/Screenshots/newkipiimagecollectionselectorwidgetKDE4.png\n\n==> With KDE4, I have fixed libkipi to become a pure image collection interface: no widget, no dialogs, no translation. All gui components must be re-implemented in kipi-host using the right model/view implementation. For digiKam, I have already implemented all. The advantage is really visible here: the treeview used for all phisical/virtual albums in digiKam can be used as well with all kipi-plugins (KDE3 only provide a flat albums list, not really suitable). \n\nhttp://digikam3rdparty.free.fr/Screenshots/gpstracklisteditor.png\n\n==> In KDE4, I have implemented a new tool to edit GPS track list of several images at the same time. This tool use Googlemaps, but there is a plan to use marble if necessary, especialy when users do not have a network access.\n\nhttp://digikam3rdparty.free.fr/Screenshots/digikamKDE4_15.png\n\n==> With KDE4, multiple root album path is supported, including removable medias and network repositories. Now, there is no problem to use digiKam with a NFS server to host your images.\n\nhttp://digikam3rdparty.free.fr/Screenshots/FullColorThemeSupport/digikam0.10.0.png\nhttp://digikam3rdparty.free.fr/Screenshots/FullColorThemeSupport/fullcolortheme6.png\n\n==> Full color theme interface: this is also implemented in KDE3. Now color schemes are applied everywhere in GUI. With black themes (my preferred), digiKam sound like a pro-software (:=)))...\n\nhttp://digikam3rdparty.free.fr/Screenshots/RAW16bitsAutogamma\n\n==> This is a very important feature : auto-gamma and auto-white balance with all RAW file format using 16 bits color depth ! Before, 16 bits color depth support nequire to use color management to have a suitable image in editor. Without CM, you have a black hole image... This is duing a limitation from dcraw which do not provide an homogenous interface between 8 bits and 16 bits color depth workflow. \n\nWith these screenshots, you can compare a same RAW image decoded:\n\n- On the left by dcraw in 8 bits color depth and converted to PNG. Auto-gamma and auto white balance is performed automatically by dcraw.\n\n- On the middle by digiKam in 16 bits color depth using libkdcraw with a dedicaced auto-gamma and auto-WB performed in digiKam core.\n\n- On the right by the LightZone (:=)))... Like you can see, digiKam is not too bad!\n\ndigiKam is now able to play with all RAW images in 16 bits color depth without to use a complex Color Management settings: RAW pictures can be handled like JPEG file in your workflow. This way is used by LightZone for ex, and it's a very productive and fast. Note than it's also implemented in KDE3...\n\nAnd i would remember to all than digiKam have been the first suitable opensource photo-management program which support 16 bits color depth pictures as well ! Gimp, f-spot do not support it ! Cinepaint can do it, but seriously, who will use it to play with pictures ? Of course we have krita now to work with layers... digiKam+krita == the perfect photo suite (:=)))\n\nIt's time now fpr me to close this file and to return in underground hacking-world...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "An obvious question for KDE4:\n\nWhat about Windows port?"
    author: "m."
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "In theory, a Windows port must compile, but personnaly i have not yet tried... I'm too busy with current Linux implementation to finalize.\n\nAll contributions are welcome to report compilation problems under Windows...\n\nGilles Caulier\n\n"
    author: "Gilles Caulier"
  - subject: "Re: Nice review!"
    date: 2008-03-31
    body: "Yep, digikam is great and has improved hugely over the last year or so. Thanks Giles (+ the other developers)"
    author: "Simon"
  - subject: "Re: Nice review!"
    date: 2008-04-01
    body: "Thank you for all your hard work, DigiKam Team, really :)\n\nBtw, maybe you can c/c all that and make a new post in your blog, I think a lot of people are waiting to read that"
    author: "DanaKil"
  - subject: "Looks great!"
    date: 2008-03-31
    body: "kde 4.1 looks very good indeed. The ONLY things that bothers me at the moment is the taskbar, or better, the rendering of the tasks in the taskbar. I think currently it looks a little bit ugly. i would also like a option that the tasks are listed on two rows not only when the panel is full, but from the beginning. In short, i would like to have a taskbar that can be configured that it more or less looks exactly like in kde 3.5.9.\n\nFor the rest, great work!"
    author: "Beat Wolf"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "+1\nThe way it is now is probably the most sane default, but I'd like the option to change it."
    author: "Jonathan Thomas"
  - subject: "Bugzilla"
    date: 2008-03-31
    body: "http://bugs.kde.org/show_bug.cgi?id=160156\n^Vote for the wishlist item, if you'd like. That'll be more... impactful than just sitting here and discussing Plasma improvemnts by ourselves on the Dot."
    author: "Jonathan Thomas"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "You can already have the items in two rows. Unfortunately you need to set the panel to be really thick ~40px in order to get two rows."
    author: "Fool"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "Why would you want smaller buttons? Those provide less space for text, so they're harder to read, and they're also harder to hit with the mouse. KDE3 actually had a problem that those buttons started in the upper row (for bottom panels), so the first set of buttons didn't make good use of Fitt's Law because the buttons didn't touch the screen edges while there was empty space below them ...\n\nThey can be made more beautiful though. But since new artwork has yet to be done for Plasma, I wouldn't try to judge it right now ... "
    author: "sebas"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "Agreed! Why does anyone want to remove the new feature?\n\nThough:\n\nIf someone wants to implement (many users demand it) it I would recommend not no waste the new code with too many new options, but to implement a second taskbar plasmoid with 3.5 looknfeel...."
    author: "Sebastian"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "agreed :D"
    author: "jos poortvliet"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "Agree. This \"bug\" is just the typical \"I'm used to this and I don't want to change even if the new way is better\". Please stop a moment nad think \"Why do I want to have a two rows taskbar even if I have only, say, 3 entries?\".\nThe answer is only one: \"Because I'm used to it, since Windows first and KDE3 then forced it on me.\""
    author: "Vide"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "Or maybe personal aesthetic preference?\nAnd it's a wishlist item, not a bug."
    author: "Jonathan Thomas"
  - subject: "Re: Looks great!"
    date: 2008-03-31
    body: "It's worth trying out new things...\n\nEither way, thank you for improving it!!!\n\nKDE looked stale until 4.0 arrived.\n\nCan't wait to see the final version."
    author: "Max"
  - subject: "Quick Launch"
    date: 2008-03-31
    body: "Is this the same as the good and old kicker applet that placed small (instead of big) application icons in the panel?\n\nIf not, can someone pelase provide a screenshot? :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: Quick Launch"
    date: 2008-03-31
    body: "it seems to be the plasma equivalent"
    author: "hias"
  - subject: "Re: Quick Launch"
    date: 2008-03-31
    body: "That's what I think.\nIf so, one less complain from my part about KDE4 series :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Quick Launch"
    date: 2008-03-31
    body: "Yes, it's the Plasma-equivalent of the good old kicker-quicklaunch =)\nIt will show n items in n rows in the taskbar and the rest in a Plasma::Dialog :)\n\nLukas"
    author: "Lukas Appelhans"
  - subject: "Excitement !"
    date: 2008-03-31
    body: "KSysguard with process monitor support is great. Sysinternals' Process Monitor is always with me when debugging Windows applications.\n\n\nI'm having a feeling drawing is a bit slower with QT 4 on my 1.4Ghz laptop with Intel 855GM video chipset. It's like every move/redraw has slightly more latency than KDE 3.5.9.\nIs that most because of debug-enabled binaries, or the fact that Intel 855GM has bad X.Org driver/performance? \n\nI dont know, but maybe someone else does (?)"
    author: "User"
  - subject: "Re: Excitement !"
    date: 2008-03-31
    body: "I've written a bunch of blogs about it. It seems Qt4 relies more on acceleration for drawing, and as many (if not most) drivers suck, things get slower."
    author: "jos poortvliet"
  - subject: "Re: Excitement !"
    date: 2008-03-31
    body: "(KSysguard maintainer) I've never used the SysInternals process monitor stuff.  I'm very interested to hear from users what they would like in ksysguard / task manager.  What features do you like and use from the SysInternals tool etc?\n\nJohnFlux"
    author: "John Tapsell"
  - subject: "Thoughts on World Clock"
    date: 2008-03-31
    body: "Perhaps there could be a level of Akonadi integration in World Clock, each contact managed by Akonadi could be given a GPS coordinate (or city location) which would make the contact show up as a dot on World Clock.  When the dot is clicked on that persons information pops up and provides a drop down of methods to contact them via Email, voip, IRC etc (through the default application).\n\nYou could then extend this to groups of people (potentially filtered by time) so you can contact them on mass e.g. confrence call amongst developers.\n\nMaybe this is outside the scope of a plasma applet or a world clock but I believe that it might be a useful way to contact people."
    author: "Jared"
  - subject: "The weather plasmoid..."
    date: 2008-03-31
    body: "Looks awesome. Though, do you think, that green on blue is a wise idea?"
    author: "Sebastian"
  - subject: "KDE on Windows"
    date: 2008-03-31
    body: "I don't believe I've ever seen much report in any of the digests about the KDE 4 ports to Mac and Windows.\n\nI tried the KDE 4 Installer for Windows, and the very first time I ran it, it allowed me to download Amarok, except that version of Amarok was quite broken and I couldn't get it to play a single song.  I was hoping there might be a newer version, but the installer can't find any download for Amarok at all anymore.\n\nThe other games and apps I've tried so far seem to work reasonably well, except one thing that is sorely lacking is integration into the Start menu.\n\nI'm curious how the porting is going."
    author: "T. J. Brumfield"
  - subject: "Re: KDE on Windows"
    date: 2008-03-31
    body: "- start menu not yet\n\n-amarok when i tried to build the collection, it still don't work, to be honest even in linux it did not worked for me the last time ( when i was home using my opensuse box, now i am stuck with windows in work for two months).\n\n-can't try konqueror as the option the configure proxy still crash.\n\n-the kde games works perfectly well, \n\n"
    author: "mimoune djouallah"
  - subject: "Re: KDE on Windows"
    date: 2008-03-31
    body: "I am also using openSuse 10.3. I got Amorok to work with the KDE version 4.0.66 which seems to be even more stable and functional than 4.0.2. Amarok is looking really pretty but it isn't as stable as the KDE 3.5 version.\nThe developers are working very fast. At this pace I am beginning to wonder what 4.1 will be like. I am really impressed :)"
    author: "Bobby"
  - subject: "Re: KDE on Windows"
    date: 2008-03-31
    body: "> -can't try konqueror as the option the configure proxy still crash.\nthis is fixed in svn and will be available in the 4.0.68 release\n\n"
    author: "Ralf Habacker"
  - subject: "Re: KDE on Windows"
    date: 2008-03-31
    body: "hihi thanks, \n\nme marking my bug report as fixed, neaaaaaaaaaaaaaaat"
    author: "mimoune djouallah"
  - subject: "Re: KDE on Windows"
    date: 2008-03-31
    body: "\"Amarok was quite broken and I couldn't get it to play a single song\" actually it play sounds, but no way to configure proxy in kde application, bad "
    author: "mimoune djouallah"
  - subject: "Re: KDE on Windows"
    date: 2008-04-01
    body: "I think it is because the first time I opened the app I configured it to look for my music folder.  Later I just opened it trying to get a song to play, but I guess the library function is quite broke, and every time I open the app now it is trying to build the library and failing."
    author: "T. J. Brumfield"
  - subject: "Re: KDE on Windows"
    date: 2008-03-31
    body: "Yes, could we get a detailed digest and updates for KDE on Windows?\n\nPlease!!"
    author: "Max"
  - subject: "Re: KDE on Windows"
    date: 2008-04-02
    body: "Feel free... I'm sure Danny is way to busy already."
    author: "jos poortvliet"
  - subject: "Re: KDE on Windows"
    date: 2008-04-02
    body: "Well, the commit digest is that, a commit digest. Wich mean you should find in it informations related to the SVN commits of the week.\n\nDanny had been adding some 'extras' for some weeks now, but an article about the windows port doesn't have to be in the commit digest. You cousd just have a dot article published about that thought :)"
    author: "kollum"
  - subject: "playground plasma"
    date: 2008-03-31
    body: "Does anybody else have problems building playground/base/plasma from trunk?\n\nI use kdesvn-build and cannot build it because of the webapplet and the worldclock"
    author: "hias"
  - subject: "Re: playground plasma"
    date: 2008-03-31
    body: "Same here. These seem to be broken at the moment. Build issues like these are generally quickly fixed though... :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: playground plasma"
    date: 2008-03-31
    body: "For the webapplet, you need to apply the contents.diff patch to your qt-copy. It won't apply cleanly, but it's relatively easy to squeeze it in.\n\nThe issue with the worldclock has been reported to the author.\n\nIn both cases, you can just comment the offender in CMakeLists.txt in the parent directory and thus exclude them from the build. Note that that's playground where basically nothing is guaranteed to work, or even compile :)"
    author: "sebas"
  - subject: "Re: playground plasma"
    date: 2008-03-31
    body: "Yeah, I know, but playground is so much fun to use :)\nUsually it took only a few hours/days and then I could build it again, but this time ist a little bit more complicated it seems.\n\nI built qt-copy with all the patches, at least I set the apply-qt-patches in kdesvn-build to true. maybe I need to rebuild it. "
    author: "hias"
  - subject: "Re: playground plasma"
    date: 2008-03-31
    body: "Oh, contents.diff is not in qt-copy, but in the webapplet. that's the problem.\nIs there a howto on techbase how to apply the patches, or is it enough to copy the patch to qt-copy? I've never done this manually before."
    author: "hias"
  - subject: "Re: playground plasma"
    date: 2008-03-31
    body: "Basically, to apply a .diff or .patch file, open a console in the target folder (in this case most definitely the root directory of qt-copy; check the file names in the .diff file to be sure) and say `patch < /path/to/my/contents.diff`. The output should list a couple of files being patched.\n\nThese manual changes might get overwritten when qt-copy gets updated. If playground/base fails to build, try to apply the patch again. However, this might cause problems if some files did not get changed, they might be patched twice (which is most certainly not good). The safest way is to delete the checkout of qt-copy and download and build it again (but this takes some time as far as I know)."
    author: "Stefan Majewsky"
  - subject: "Re: playground plasma"
    date: 2008-04-01
    body: "thanks very much, I will try it"
    author: "hias"
  - subject: "forking konqueror !"
    date: 2008-03-31
    body: "i know it is a taboo here, but am i the only who thinks that konqueror is too much, i want from my kde web browser to (hint) browse the web, i am very satisfied with dolphin as file manager, don't you think there is a need for a simple web browser for kde.\n\nthanks\n\nps: unfortunately konqueror under windows still crash when i try to configure proxy, "
    author: "mimoune djouallah"
  - subject: "Re: forking konqueror !"
    date: 2008-03-31
    body: "If you _really_ need a simple browser, use the Qt 4.4 demo browser (WebKit based). It works quite ok in the current Qt snapshots.\n\nBut then, I hear your complaints about missing KWallet integration, missing this, missing that...\n"
    author: "christoph"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-03-31
    body: "What's wrong with Firefox?\n\nThat's an awesome web-browser!!!"
    author: "Max"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-03-31
    body: "Not KDE - looks different, starts slower and consumes more memory (due to not using shared KDE libs), cannot use kio slaves, cannot use kparts, cannot stort passwords in kwallet, doesn't open files with KDE's file associations etc."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-04-01
    body: "It's a piece of bloated shit."
    author: "Dan"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-04-01
    body: "Firefox is awesome in Windows. But Firefox in Linux is simply inferior compared to Firefox in Windows (major complaint: SLOW). I use Firefox in Vista, and in the same machine I use Linux - Debian, the same version of Firefox feels so slow and sluggish. Thats why I always use Konqueror/KHTML."
    author: "fred"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-04-01
    body: "You will be happy to see that Firefox 3 is a *lot* faster, in painting in general, and as well in page rendering.\n\nI switched to the beta some weeks ago, and whenever I accidentally launched firefox2 instead of firefox3, it felt as if my browser had a 100kg steelball towed to it's feet and was crawling on all fours"
    author: "yves"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-04-01
    body: "That would be good news! Because I usually need to have 1 or 2 firefox tabs to open gmail or other google ajax application (sad - even though gmail works with Konqueror user agent spoofing, it has few annoying issues).\n\nBut I already fall in love with Konqueror/KHTML :D"
    author: "fred"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-04-01
    body: "Firefox has branches for all kinds of options and operating systems.   It had a QT branch ages ago, but it was abandoned.  Personally, I'd love to see a QT 4 branch of Firefox that integrates well into KDE."
    author: "T. J. Brumfield"
  - subject: "Re: forking konqueror ! - no need: Firefox"
    date: 2008-04-01
    body: "Maybe a Gecko kpart would be more realistic."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: forking konqueror !"
    date: 2008-03-31
    body: "Firefox is great indeed but not integrated into KDE at all"
    author: "makosol"
  - subject: "Re: forking konqueror !"
    date: 2008-03-31
    body: "Theres not really anything stopping anyone from writing a full web browser based on either QtWebKit or KHTML (or both).\n\nI think it'd be interesting to see what a web browser equivalent of Dolphin would look like. I'd imagine it'd be much easier to maintain than something like Konqueror (which'd make sense, as Konqueror is far more than just a web browser), and fortunately most of the KDE infrastructure like KWallet is quite easy to use (I'd imagine the other side would be harder, i.e. knowing when you need the information and where to put it into the webkit/khtml widget). You could use something like Kross and D-Bus to allow plugins in a variety of languages as well.\n\nI've actually been planning on trying my hand at writing a web browser based on QtWebKit in Qt4.4 (I did some work with Tech Preview 1, but ran into some bugs with QtWebKit in that version, and the Beta1 build for Windows didn't include the binaries needed for QtWebKit and I haven't gotten around to compiling it myself on Windows or Linux). If I /do/ make something thats somewhat decent I'll probably post it to kde-apps.org. And who knows, it may actually attract some attention and eventually be included in KDE 4.some-really-large-number (in which case I'd want to add support for using KHTML as an alternative backend)."
    author: "Sutoka"
  - subject: "A Dolphin-esk web browser"
    date: 2008-04-01
    body: "\"I think it'd be interesting to see what a web browser equivalent of Dolphin would look like.\"\n\nIt's interesting, because Dolphin started out to be a simple, Nautilus-like file manager, but has become more powerful in some respects than Konqueror for file managing. (I like the favorites dropdown in the address bar and the Columns view.) Include tabs, port a few more of the views from KDE 3, and allow more than two folders in \"Split View\", and Dolphin would be completely superior for file managing. If a \"simple\" web browser was started based on WebKit, it might quickly surpass Konqueror in web browsing functionality. Don't get me wrong, I love Konqueror (I use it's split views all the time), but there doesn't seem much advantage to having a web browser and a file manager combined. Aside from forward and back buttons, there's almost nothing in common between web browsing and file browsing."
    author: "kwilliam"
  - subject: "simple web browser Re: A Dolphin-esk web browser"
    date: 2008-04-01
    body: "> If a \"simple\" web browser was started based on WebKit, it might quickly surpass Konqueror in web browsing functionality. \n\nWhy does everyone assume this? \n\na) Read the post farther up saying what KDE-integration functionality you'd lose.\n\nb) Where would all these magic developers come from? Lots of hype, no action.\n\nc) Someone already started a WebKit browser. It doesn't do much."
    author: "Anon"
  - subject: "Re: simple web browser Re: A Dolphin-esk web brows"
    date: 2008-04-03
    body: "If the browser you are referring to is safra in playground then that someone is me. It's a very young project, patience. ;) I'm also waiting for qt-copy to be updated."
    author: "Louai Al-Khanji"
  - subject: "Re: A Dolphin-esk web browser"
    date: 2008-04-01
    body: "What Konqueror has is not specifically web browsing or file management - it can display whatever type of file (directory, html, pdf etc. with kparts) which is accessed via whatever protocol (file:, http:, ftp: etc. with kio slaves). File management (directory via file: or sometimes ftp:, sftp: etc.) and web browsing (html, sometimes pdf, doc, etc. via http: or https:) are special cases of this, but not the only case that someone may want to use."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: A Dolphin-esk web browser"
    date: 2008-04-01
    body: "However, most of the kparts have a standalone application also (kpdf part has kpdf, etc., now file management dolphin part has dolphin) so a standalone app for html browsing wouldn't be a problem, if someone writes it. However, most web browsers are able to display not only html, but pdf and other file types also. That's what Konqueror can do easily and it would be silly not to reuse the kparts technology - and if a browser reuses kparts to display whatever file type, it is essentially the same as Konqueror."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: forking konqueror !"
    date: 2008-04-01
    body: "konqueror is great as a file browser and as a web browser ! I just hope it get Kross one day :)  (maybe a way to do some simple firefox-like extension)"
    author: "DanaKil"
  - subject: "Re: forking konqueror !"
    date: 2008-04-01
    body: "> ... but am i the only who thinks that konqueror is too much\nYup, you are ;-)\n\nKonqui is the best horse in the stable!\nfeels lean and slippy - on the contrary - being able to even broaden konquis functionality with further add-ons would be great\n\n\"No fake - I'm a big fan of konqueror, and I use it for everything.\" -- Linus Torvalds\n \n \n\n\n\n\n"
    author: "Niklas"
  - subject: "Re: forking konqueror !"
    date: 2008-04-01
    body: "I don't understand. Why don't you just stop using it for other things than web browsing then?\n\nI use Konqueror for a lot of things, web browsing is only a small part of it. I browse local files, remote directories, remote shares, source code repositories, zip/rar/tar-archives and web sites. I also view pictures and movies in it, along with pdfs, chm-files and other document types. The fact that it adapts itself to what you are doing is just awesome.\n\nEvery functionality that Konqueror has is actually just KParts. Konqueror loads all these parts on-demand, so if you're not using them, you're not 'paying' for them. What would be the point of removing this?\n"
    author: "SP"
  - subject: "Re: forking konqueror !"
    date: 2008-04-03
    body: "The myth of the _web_browser again.  There is no such thing.  Firefox is NOT a webbrowser; it is just a browser that can browse files in any location.\n\nSo, what do you mean by a webbrowser?  Would it simply be a browser like Firefox that does not include a file manager?  Or, do you want a browser application that is not able to use the protocol: \"file:///\" to browse files on your local system?"
    author: "JRT"
  - subject: "Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "Amarok is my favorite audio app ever. IMO much better than iTunes, not only in features, but also in performance, specially with large music libraries.\n\nThere is one thing that for me has been flaky through all these years (and I've been using Amarok since 0.x), and that's the collection management, specifically safeguarding this data.\n\nSometimes when updating from 1.x to 1.x+1 the collections got wiped out. Or certain scripts can bugger up your collection (mp3fixer does this for me). If you change the partition where you store your music files, they won't appear anymore on the collection, unless you rescan (which sometimes loses all your playcounts and ratings) or hack into the database to modify UUIDs in a few places. There is no easy way of backupping this data (I manually run a script to backup the relevant mysql database and the config file).\n\nSo I'm concerned about the migration from 1.x to 2.x. Will it respect all my ratings? What about the album covers (many of them added manually, being copyleft music you won't find them in Amazon)? Playlists, iPod configuration, Last.fm settings... ? In short, has this process been thought of and fixed already? "
    author: "NabLa"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "It's probably best to ask this on one of the Amarok lists."
    author: "sebas"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "That's true, but I guess he is right to post it here - it simply gets more publicity. ;)\nA more stable collection database managent is one of the few things the great Amarok still needs."
    author: "Anon"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "Aye, otherwise I would have gone to the list or amarok's forum"
    author: "NabLa"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "+1\n\nThis gets read more and gets much more publicity than amarok blog.\n(which is sad... Did the Amarok buzz die off?)"
    author: "Max"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "doesn't this work a lot better if you use MySQL? I haven't seen my DB being wiped out in ages..."
    author: "jos poortvliet"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "Last time it happened to me I was using sqlite; perhaps a year and a half ago (maybe from 1.3 to 1.4? don't remember), backing up the database was a pain because the folder containing it had a dozen different copies, and the last one was not current by at least one month...\n\nI have MySQL on all the time as I do webdev, so it made sense to me to convert over to it so a process that was already running could be used. Incidentally I can back it up pretty easily and if there's something going wrong, I got the expertise to fix it. I think MySQL felt much faster when I switched, but tbh sqlite is pretty fast on its own."
    author: "NabLa"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "Even if it improves Amarok performance, I still think mysql is just overkill for this task and sqlite is the perfect solution."
    author: "Vide"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "The perfect solution is that amarok offers the choice - you get to use sqlite, someone else gets to use mysql.  Everyone is happy :-)"
    author: "Adrian Baugh"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-03-31
    body: "That's kind of a standard answer but no solution, sorry ;).\n\nFor the avarage user Amarok should use SQLite. No need to use MySQL, as it adds additonal database complexity and overhead. I think everyone agrees.\n\nHowever, even the average user might want to use his/her Amarok database collection safely despite of updates, might want to make a backup of it, or might be able to store the database somewhere else than in Amarok's config dir (imagine music collections on removable media).\n\nThat should be adressed some time, however I understand if it's not a priority. At the moment I'm just looking forward to the time I'll be able to use this great music software natively on KDE4 (at the moment, it unfortunately doesn't even scan my local collection running openSUSE factory and KDE 4.0.66)."
    author: "Anon"
  - subject: "Re: Amarok 2 transition from 1.x"
    date: 2008-04-01
    body: "Pardon, the best DEFAULT solution. And the default should be safe through updates (or just another way to say: don't address me to another backend if sqlite has problems)"
    author: "Vide"
  - subject: "Re: Amarok 2 - request: Updates"
    date: 2008-03-31
    body: "I would like to make a request:\n\nCan we have a special dot post in a few weeks about updates on Amarok 2?\n\nI haven't heard much of anything about Amarok 2 lately. even the Amarok blog has been really quiet.\n\nAlso updates for Amarok on Windows would be great!!!!\n\nI'm so tired of iTunes!!!!\n\n\n"
    author: "Max"
  - subject: "Re: Amarok 2 - request: Updates"
    date: 2008-04-01
    body: "+1 !!\n\nWe need new screenshots to drool over.\nMore updates please!"
    author: "Mike H"
  - subject: "Re: Amarok 2 - request: Updates"
    date: 2008-04-02
    body: "You should get your fill of Amarok 2 news very soon! We have a little something coming up very soon :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Amarok 2 - request: Updates"
    date: 2008-04-02
    body: "And I even managed to add a nice little touch of redundancy to that post... "
    author: "Nikolaj Hald Nielsen"
  - subject: "Konqueror and proxies"
    date: 2008-04-01
    body: "When is this issue slated to be resolved? I say issue because its not a 'bug'. I am forced to use firefox (not that its a bad thing) at work because of proxy servers. Otherwise, I am very happy with KDE4 desktop (4.0.2) with a bit of help from KDE3 Apps.\n\nPlease get konqueror talking to proxies asap.\n\nThanks."
    author: "Kanwar"
  - subject: "Re: Konqueror and proxies"
    date: 2008-04-01
    body: "Yes please. That's the most desired change for me...\n\nI had found the patch to correct the issue on the web (silly thing), but I had trouble compiling for KDE4 and I gave up without spending much effort.\n\nNot that firefox-3.0 is bad by any means, but I liked konqueror a lot last time I used it. I want to test how fast it renders pages as well."
    author: "Ioannis Gyftos"
  - subject: "Will phonon support OSS?"
    date: 2008-04-01
    body: "I have a Creatvie X-FI in use, so far only oss drivers support this sound card, but can not use it in KDE 4, it is not seen it systemsettings. I am using Kubuntu Hardy, I don't know if it is something related to phonon or solid...."
    author: "rockmen1"
  - subject: "Re: Will phonon support OSS?"
    date: 2008-04-01
    body: "Phonon does not need to support OSS directly.  Phonon on linux uses either the Xine backend or the gstreamer backend.  So as long as xine or gstreamer support output to OSS phonon should work for you.  "
    author: "Leo S"
  - subject: "Middle click in Konq is already there "
    date: 2008-04-01
    body: "From last weaks digest:\n\"David Faure committed changes in /trunk/KDE/kdebase/apps/konqueror/settings/konqhtml:\nGUI: checkbox for the \"Middle click on a tab closes it\" option (which has existed for a long time, but didn't have a GUI).\"\n\nwoot!"
    author: "Koko"
  - subject: "Re: Middle click in Konq is already there "
    date: 2008-04-02
    body: "cool indeed!"
    author: "mxttie"
  - subject: "Compositioning effects?"
    date: 2008-04-01
    body: "Are we getting more compositioning effects before KDE 4.1 is released?\n\nWe don't even have feature parity for the most popular features with Compiz fusion yet. I'd also be exited about more unique effects that Compiz fusion doesn't have yet.\n\nC'mon it's only a few more months before KDE 4.1 is ready to be released. We need to have these effects ready to create buzz around KDE..\n\n-M"
    author: "Max"
  - subject: "Re: Compositioning effects?"
    date: 2008-04-02
    body: "Port a few Compiz effects to Kwin ;-)\nThey are supposedly very small, often under 100 lines of code..."
    author: "jos poortvliet"
  - subject: "Time to vote for Kubuntu (again) :)"
    date: 2008-04-01
    body: "It's time to bump this to the most recent digest again. :)\n\nhttp://brainstorm.ubuntu.com/idea/478/\n\nIf you haven't done so, please vote to give KDE equal representation in Kubuntu. It deserves it!!! Many people work hard on KDE.\n\nhttp://brainstorm.ubuntu.com/idea/478/image/1/"
    author: "Max"
  - subject: "Re: 619 votes for Kubuntu "
    date: 2008-04-01
    body: "619 votes currently."
    author: "Max"
  - subject: "Re: 619 votes for Kubuntu "
    date: 2008-04-02
    body: "yawn.  it gets boring having a certain distro come in here and ask for votes, is kbuntu that bad you have to spam the net for votes?"
    author: "anon"
  - subject: "Re: Time to vote for Kubuntu (again) :): EEK"
    date: 2008-04-02
    body: "Someone's been voting us down: 618 votes"
    author: "Riddle"
  - subject: "Re: Time to vote for Kubuntu (again) :)"
    date: 2008-05-15
    body: "Related: More \"magic\" for Kubuntu:\nhttps://bugs.launchpad.net/bugs/150333"
    author: "/usr/"
  - subject: "Timezone separators"
    date: 2008-04-02
    body: "Could it be shown equivalent to these?\n\nhttp://aa.usno.navy.mil/faq/docs/world_tzones.php"
    author: "Marc Driftmeyer"
---
In <a href="http://commit-digest.org/issues/2008-03-23/">this week's KDE Commit-Digest</a>: Support for "undo closed windows" in <a href="http://www.konqueror.org/">Konqueror</a>. GetHotNewStuff support for <a href="http://plasma.kde.org/">Plasma</a> themes. <a href="http://konsole.kde.org/">Konsole</a>, Konqueror, and <a href="http://kate-editor.org/">Kate</a> session selection added in Plasma applet form. New Plasmoids: "Generic Folder View", "System Command", KNotify-based "Popups", "Quick Launch", and to display data from <a href="http://edu.kde.org/kalzium/">Kalzium</a>. <a href="http://www.digikam.org/">Digikam</a> now uses <a href="http://phonon.kde.org/">Phonon</a> for video and audio previews, with improved use of Phonon in <a href="http://dragonplayer.org/">Dragon Player</a>. Start of NEPOMUK support in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. A NEPOMUK "Social Query Daemon" for viewing storages across a network, and work on tagging GUI's for NEPOMUK using <a href="http://enzosworld.gmxhome.de/">Dolphin</a>. Work on services and queries, with the removal of the engine system (now using Phonon only) in <a href="http://amarok.kde.org/">Amarok</a> 2. Continued development in <a href="http://konsole.kde.org/">Konsole</a>. Various functional improvements in <a href="http://edu.kde.org/kturtle/">KTurtle</a>. Support for synonyms in <a href="http://edu.kde.org/parley/">Parley</a>. Support for custom themes in <a href="http://games.kde.org/game.php?game=knetwalk">KNetWalk</a>. A system tray application for <a href="http://pim.kde.org/akonadi/">Akonadi</a>. Initial implementation of a remote desktops dock widget for KRDC. Work on the "reports" functionality of <a href="http://www.kexi-project.org/">Kexi</a>. Several long-awaited improvements in KCron. KDiamond moves from kdereview to kdegames. KAgenda moves to playground. Initial import of Palapeli, a jigsaw puzzle game. <a href="http://commit-digest.org/issues/2008-03-23/">Read the rest of the Digest here</a>.

<!--break-->
