---
title: "KDE 4 And Fedora Interview"
date:    2008-02-19
authors:
  - "rsundaram"
slug:    kde-4-and-fedora-interview
comments:
  - subject: "Porr guys"
    date: 2008-02-19
    body: "They will have TONS of work to ship a usable and bug free KDE 4.0 :)\nThis is kind of both good and bad for KDE. On a side they will fix bugs and more people using KDE means more bug reports and more bugs fixed (even if only to stop the SPAM of reports). On the other side a lot of people will get disapointed with KDE4, will run ASAP to gnome and never look back again :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "\"On the other side a lot of people will get disapointed with KDE4, will run ASAP to gnome and never look back again :-P\"\n\nBased on some of the old reviews of GNOME 2.0.0 I've read from the time of its release, you could easily have predicted that everyone would have switched to KDE and stayed there.  And yet, according to widespread online polls, GNOME is currently the more popular of the two DEs.  \n\nThe line of reasoning misses the fact that the Linux desktop is still growing, possibly at an accelerated rate compared to previous years, and that many newcomers first tastes of GNOME and KDE will be GNOME 2.2x vs KDE 4.1 or 4.2.  In fact, only a small proportion of newcomers - just for those joining up this year, let alone all of the droves that will join in years to come - will have to put up with the roughness of KDE 4.0.x or (to a hopefully far lesser degree) 4.1.  Predictions of an irreversible shift based on a single short window of sub-par releases are fairly naive and not supported by historical evidence, IMO."
    author: "Anon"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "> And yet, according to widespread online polls, GNOME is currently the more popular of the two DEs. \n\nOnline polls at GNOME sites I presume? :-P\nSince I'm reading polls where KDE still has a larger market share in the Linux destkop."
    author: "Diederik van der Boor"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "\"Online polls at GNOME sites I presume? :-P\"\n\nhttp://www.desktoplinux.com/news/NS8454912761.html\n\nPublicised widely, including here on the Dot (skewing results towards KDE, if anything).  Over 38000 respondents, checked for uniqueness of IP addresses.\n\n\"Since I'm reading polls where KDE still has a larger market share in the Linux destkop.\"\n\nLinks?"
    author: "Anon"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "It was also publicized on big sites like digg which are infested with Ubuntu users, so it doesn't prove much either way.\n\nYou can't really measure the \"desktop share\" of OSS desktops anyway so why bother."
    author: "Narishma"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "I remember that poll. It basically came down to a shouting match --- \"Hey everyone on #kde! There's a poll about which desktop is the BESTEST! Everyone go and vote for KDE!\" --- and was hence useless.\n\nA better way of gauging use would be to check the package download stats of a distro that installs neither desktop by default. Maybe debian?\n\nAlternatively poll people at a desktop-neutral linux event. Even so that may have bias towards the more hacker-friendly desktop (whichever that is!)"
    author: "Tim"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "The only reliable poll would measure those who switched gnome<->kde and stayed switched.  That way you are talking about individuals who have experienced both and made an informed decision (uninfluenced by biases in the original installation)."
    author: "stats"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "> http://www.desktoplinux.com/news/NS8454912761.html\n\nNever trust statistics where one bar with 14% is taller than another 14% bar and also never ever trust a statistic where the background labeling differs from the values in the statstic bars! ;-)\n\nSCNR\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: Porr guys"
    date: 2008-02-22
    body: "Yeah, that poll makes a good laugh. They must have been drunk when they made those graphs!"
    author: "Erik"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "KDE is better, Gnome is better.  Does it really matter?  That is one of the thngs that nearly put me off the linux community when I was looking to make the switch from windows.  Personally when someone I know wants to move to linux, I don't tell them which to use, I just give them the links to both the live cd's of opensuse gnome and kde and tell them to find which best suits them.  Then I tell them that they should also try out the different distro's until they find which suits them.  I get tired of the which desktop is better, which distro is better and I find it really puts people off linux"
    author: "Richard"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "According to http://www.google.de/trends?q=kde%2Cgnome it's not that big difference. But if you like to compare, then probably try http://www.google.de/trends?q=kde%2Cgnome%2Cwindows%2Cmac :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Poor guys"
    date: 2008-02-19
    body: "The Swiss seem to be the most adventurous OS nation on Earth!   Why are the stereotypically staid swiss the most likely to search for mac, gnome, kde relative to windows I wonder."
    author: "interesting.."
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "look at searches for ubuntu in place of gnome and the results look more positive. However, search results dont translate 1to1 for users."
    author: "mike"
  - subject: "Re: Porr guys"
    date: 2008-02-20
    body: "What is it with this KDE vs. Gnome?\n\nCoke vs. Pepsi\nDemocrats vs. Republican\nAmerican cars vs. Imports\netc.\n\nDoes it always have to be A) or B) in the United States? Ever heard of \"both\" or having choice? Why does it always have to be one, or the other?\n\nIs that a cultural thing we Europeans don't understand?"
    author: "Hi"
  - subject: "Re: Porr guys"
    date: 2008-09-29
    body: "They are just to stupid to see that world is not black and white and that in real life things are a bi more complicated. \nAs one sad: \nPeople like lies, because truth is to complicated to understand and to difficult to accept..."
    author: "kojot"
  - subject: "Re: Porr guys"
    date: 2008-02-19
    body: "I really doubt the validity of the last point. If someone who has never seen, let alone work with, a Linux desktop gets his first taste with KDE 4.0.0 (or 4.0.1) and feels let down, my guess is that he would be far more likely to run back to Windows than to Gnome. Not because Gnome is necessarily better or worse, but because he would judge the entire Linux experience on his KDE 4.0 experience.\n\nGnome is not bad, but personally I find KDE 4.0.0. more stable and useful than the current stable version of Gnome, despite the inevitable temporary regressions a transition of this magnitude involves."
    author: "Master of the known universe"
  - subject: "Re: Porr guys"
    date: 2008-02-20
    body: "I'm actually switching to Fedora when it comes out BECAUSE they're the first to embrace KDE 4.0. That's what I love about Linux. No lock in. Tons of choices. Ever since I put /home on a separate partition, I see no problems in trying them all out.\n\nIf by some miracle SUSE 11 gets delayed 1 month (is that too much to ask? :rolls eyes: ) so it coincides with KDE 4.1 and Qt 4.4 I'll probably switch back to openSuse then.\n\nI pretty much gave up on Kubuntu. Like other posters said. They treat KDE as a red-headed-stepchild, so why bother anymore. Kubuntu will be interesting again to me when they support KDE 4.X straight out of the box, and not just community-supported. Until then, I'm lucky I have CHOICE!!\n"
    author: "Hi"
  - subject: "Re: Porr guys"
    date: 2008-02-20
    body: "We will seriously consider pushing KDE 4.1 as a Fedora 9 update once it is out, so you won't have to switch just to get 4.1. Qt 4.4 might even make it into the release, or it can be pushed as an update too (pushing KDE 4.1 will imply pushing Qt 4.4 too anyway)."
    author: "Kevin Kofler"
  - subject: "enjoyed the interview"
    date: 2008-02-19
    body: "just wanted to say that i enjoyed the interview; i read about it on Rex's blog and ran over here to the dot to see if it was up here already .. .and it was.. yeah! =)\n\nit's great to see KDE and Fedora become better friends, as both have a lot to gain from each other."
    author: "Aaron Seigo"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-19
    body: "You know, out of the 3 big distro-backing companies, Canonical, Novell and Red Hat, Red Hat is the only one who isn't a KDE supporting member.\n\nMaybe KDE e.V should invite them to be a Patron of KDE.\n\nI can't think about anything more friendly and supporting than that."
    author: "Bruno Laturner"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-19
    body: "That's a nice idea, but Red Hat have made their thoughts about KDE abundantly clear over the years.  Such a request would just be embarrassing for both parties, I fear."
    author: "Anon"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-19
    body: "Working on it... :)"
    author: "Rex Dieter"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-19
    body: "Technically, Cannonical isn't either since Mark Shuttleworth became a patron as a private person."
    author: "Erunno"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-20
    body: "Probably because the individual contribution is lower.. I kid, I kid, please no flame. :)"
    author: "Hi"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-20
    body: "Nokia isn't on that list either, neither is Google, I wouldn't worry about it too much.\n\n"
    author: "Hi"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-20
    body: "But Trolltech is and if I recall correctly, Nokia said they would become patrons too.\n\nIn regards to Google - they do support KDE in various ways."
    author: "Joergen Ramskov"
  - subject: "Re: enjoyed the interview"
    date: 2008-02-20
    body: "Note that Red Hat does commercially support KDE in Red Hat Enterprise Linux and to develop, package, QA and support does require a considerably large staff for a huge piece of software stack like KDE.  "
    author: "Rahul Sundaram"
  - subject: "Re: enjoyed the interview"
    date: 2008-03-16
    body: "Huge staff? RH has one guy that compiles it and that's about it."
    author: "jhgjbk"
  - subject: "Re: enjoyed the interview"
    date: 2008-03-30
    body: "No. Red Hat has two full time developers working on KDE and dozens of other people doing QA, support etc. Talk about what you know. "
    author: "jeff"
  - subject: "Fedora.."
    date: 2008-02-21
    body: "I hope they do a good job integrating KDE 4 into Fedora; cause I plan on using Fedora for my distro. :]"
    author: "Jeremy"
---
KDE 4 is seen by many to be the next big step on the free software desktop, while many do not yet advise it for everyday use. Either way, it is an innovative release and in line with Fedora's goal of providing the latest and greatest free software it is set to be the default KDE environment in the next major release of Fedora. We caught up with a few members of the <a href="http://fedoraproject.org/wiki/SIGs/KDE">KDE Special Interest Group</a> for <a href="http://fedoraproject.org/wiki/Interviews/KDE4">an interview about the work they are doing</a> to get it ready for release, their own opinions on the software and what they think about the progress made by Fedora in embracing KDE.





<!--break-->
