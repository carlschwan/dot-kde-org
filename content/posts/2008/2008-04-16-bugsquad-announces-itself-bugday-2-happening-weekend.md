---
title: "BugSquad Announces Itself, BugDay 2 happening This Weekend"
date:    2008-04-16
authors:
  - "aspehr"
slug:    bugsquad-announces-itself-bugday-2-happening-weekend
comments:
  - subject: "Nice :-)"
    date: 2008-04-16
    body: "Can you please coordinate this in some way with http://quality.kde.org/ ?\nMaybe that mailing list could be reused etc. ?\n\nAlex\n"
    author: "alex"
  - subject: "Re: Nice :-)"
    date: 2008-04-16
    body: "The \"quality teams\" are basically dead. quality.kde.org is a relic. There is a sort-of-research team on quality, based around the EBN with Allen Winter and myself, but there's not much to coordinate with us. Actually I'd be really glad if the bug squad would take over quality.kde.org and kde-quality@k.o because those make more sense in the hands of people doing immediate work on quality. But then again, the name may not cover the purpose of the group."
    author: "Adriaan de Groot"
  - subject: "Re: Nice :-)"
    date: 2008-04-16
    body: "> Actually I'd be really glad if the bug squad would take over quality.kde.org\n> and kde-quality@k.o\n\nYes, that was actually what I meant.\nInstead of creating yet another web site/mailing list/etc. just reuse one which already exists.\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Nice :-)"
    date: 2008-04-16
    body: "I'm not sure how much overlap there is right now, but there might be more in the future. So sure, let's talk. /me joins #ebn ;)"
    author: "A. L. Spehr"
  - subject: "\u00bfTriaged?"
    date: 2008-04-16
    body: "Hi, I'm not english native, and I don't know the meaning of \"triaged\". I've searched for it in wordreference and urban dictionary unsuccesfuly. Could someone explain what it means?\n\nI suppose it means something related to confirm or clasify, because the announce says no program skill are needed, so I think people is wanted on IRC channel to test bugs, not to fix them... is it right?\n\nit may be funny... ;-)\n\nThanks in advance for the explanations.\n"
    author: "Git"
  - subject: "Re: \u00bfTriaged?"
    date: 2008-04-16
    body: "Triage means classify or sort - in this case by severity. A more common-day usage is in casualty nursing where you classify patients to make sure the more serious get seen first.\n\nThere is a Wiktionary entry ( http://en.wiktionary.org/wiki/Triage ). From there you might be able to get to your native language ;)"
    author: "Martin Fitzpatrick"
  - subject: "Re: \u00bfTriaged?"
    date: 2008-04-16
    body: "Alas, while it is a fun word, it is not probably actually funny. :D\n\nI was wondering if I needed to explain \"triaged\"... I would have just linked to our documentation, except we haven't written that page yet! So:\n\nWhat is bug triage?\n\nIf you do a google search on the term, you'll find lots of articles about the economic cost of deciding which bugs to fix, and which bugs not to fix. In free software, the cost is our developer's time. They decide what to fix based on a time-severity tradeoff. Many of them have very long TODO lists, and bugs.kde.org is part of that.\n\nBugSquad aims to help developers by keeping what is in b.k.o as useful as possible. In general, bug triage checks incoming bug reports to see if they can be replicated, make sure they aren't duplicates, and see if they give enough information. For BugDays, we do the same, but for a particular component of KDE. And we start by going through all the old reported bugs. \"Does this bug reported in 3.2.1 still exist in 4.0.3?\" In addition, we try to sort out things into categories. We also look for any major bugs that have slipped through, and not been noticed by developers, for example \"Google.com does not display\". This could happen if the bug report was not written very clearly, and the fact that Big Things broke is only mentioned at the very end, and not clear in the title. \n\nIn the end, developers make the call as to what is \"hard to fix\" vs. \"important enough to bother\". So you don't need to know any programming, you just need to have a recent version of KDE4 to join in. We'll be working on Konqueror stuffs, so you'll be able to get away with not using trunk. ;) A recent SVN 4.0 branch would work very nicely. 4.0.3 will also work. 4.0.2 will probably work. 4.0.1 or earlier is too old. We'll also write testcases. (http://konqueror.kde.org/investigatebug/ for a definition of that :)\n\nJoin #kde-bugs and you'll see the links for whatever we're currently working on in the topic. We're friendly! \n\nHa! I guess I just wrote some more documentation. *g*"
    author: "A. L. Spehr"
  - subject: "Re: \u00bfTriaged?"
    date: 2008-04-20
    body: "Thank you very much for your nice answer, all is clear now ;-)\n"
    author: "Git"
  - subject: "Re: \u00bfTriaged?"
    date: 2008-04-16
    body: "Yeah. It means something like going through them, testing them. At least, that's what my limited English knowledge tells me ;-)"
    author: "jospoortvliet"
  - subject: "Add bugsquad list to main KDE mailinglist page"
    date: 2008-04-17
    body: "What good is a spiffy new mailing list if no one can find it? Please add the bugsquad mailinglist to http://kde.org/mailinglists ."
    author: "Tray"
  - subject: "Re: Add bugsquad list to main KDE mailinglist page"
    date: 2008-04-17
    body: "Oh.... Good idea!"
    author: "A. L. Spehr"
  - subject: "Re: Add bugsquad list to main KDE mailinglist page"
    date: 2008-04-17
    body: "Now that you mention this, this is actually the first time I heard of the bug squashing triage too. I never knew that this even existed"
    author: "she"
  - subject: "That is because we are new!"
    date: 2008-04-17
    body: "There have been many dedicated people over the years who have done bug triage. And there were Krush Days before KDE4.0 was released. But then #kde-bugs was silent. Until one day I caught someone joining who actually said something (grundleborg), and we decided we ought to restart bug triage days. And furthermore, that everyone currently doing bug triage should work together, and that there should be formal coordination. And that is BugSquad. \n\nOur first BugDay, we were pleasantly surprised to find that 1/3 of khtml bugs were fixed in kde4. Perhaps more, we're still waiting back to hear from people. \n\nRight now, #kde-bugs has gone from just a few people idling, and never saying anything, to... well, there's 16 on right now, and they've all said something within the past 24hours. Last bugday, we ended up with 40+ in channel. We do a lot of discussion there, moreso than on the mailing list. But I think it fair to call it an active channel at this point.\n\nAfter this BugDay, we'll start to move into things that will probably be harder to check, and can use people willing to write How-to-debug-my-favorite-application documentation.\n\nCome join us! We are fun! *bounce* *bounce*"
    author: "blauzahl"
  - subject: "Coordinate"
    date: 2008-04-17
    body: "I happily chime in, but please could someone coordinate this. \n\nI dont mind if this happens on IRC the designated time, but I would rather like to know that someone is in charge who runs forward with it so I know that my time is not lost either (i.e. by idling in one more channel already). :-)"
    author: "she"
  - subject: "Re: Coordinate"
    date: 2008-04-17
    body: "Don't worry! There will be experienced Bug-Squashers on IRC throughout the day, so you can rest assured that if you ask any questions, someone will answer. You certainly won't find yourself idling (unless you don't say anything of course ;)"
    author: "George"
  - subject: "Re: Coordinate"
    date: 2008-04-17
    body: "In fact, there are people already starting now. It isn't a timed race or anything. There are even people on right now talking! Although, right now we seem to be talking about bugzilla policy stuff and documentation.\n\nWe didn't widely announce the first BugDay, because we weren't too sure how well it would work out. We went through 355 bugs, so I'd say that went pretty well. And the irc channel was extremely active. \n\nThe basic instructions are in the topic. We have a lot more documentation than last time, too! And we're always happy to answer questions."
    author: "blauzahl"
  - subject: "I can has .kdesvn-buildrc plz? :3"
    date: 2008-04-17
    body: "Can anyone please post a recent KDE4 .kdesvn-buildrc please?\n\nI want to start with a clean build for BugDay."
    author: "Nonymous"
  - subject: "Application logs"
    date: 2008-04-19
    body: "What always concerns me is that when you are starting KDE applications from the console you are getting x warnings outputted while it runs. Who keeps track of them and tries to resolve these issues?"
    author: "andy"
  - subject: "Re: Application logs"
    date: 2008-04-20
    body: "Those are debug messages - they tell you what the app is doing so you can track down problems if they occur. Since the most common use case for kde apps is to start them from the k menu or the run command dialog, output on the command line isn't really a big issue.\n\nIn fact, most distributions turn off this debug output throughout kde (there's a compile-time option that turns kDebug() into a no-op). But if you dislike them, and they're not already turned off, you can use kdebugdialog to control the output of messages - take a look in the user guide on docs.kde.org."
    author: "Philip Rodrigues"
  - subject: "Re: Application logs"
    date: 2008-04-20
    body: "I think these problems need to be fixed, not disactivated."
    author: "andy"
  - subject: "Re: Application logs"
    date: 2008-04-22
    body: "If you mean genuine X errors generated by the X server, they're probably occurring at the Qt level, rather than KDE - you could try reporting them to Trolltech I guess, although I don't see much point unless you actually see a problem with the *functionality* of the application you're running.\n\nIf on the other hand you mean the KDE debug output, then it's *debug* output, for solving problems when they occur, so in once sense it can print what it likes.\n\nI would make one proviso though - we often tell people in #kde to run kbuildsycoca, which prints out a lot of warnings, and at least one error, all of which are entirely harmless as far as I know. It would be cool to not have these messages around generating undue concern, but it's a pretty minor point."
    author: "Philip Rodrigues"
---
The <a href="http://techbase.kde.org/index.php?title=Contribute/Bugsquad">KDE BugSquad</a> is pleased to announce itself! Come and learn the fine art of bug triage. How might one do so? Join us for a BugDay on April 20th (0:00 UTC to 23:59 UTC).  The last one was a great success, with 355 bugs triaged, and almost a third of those closed. We would like to finish off more bugs, and could use your help! All you need is a recent version of KDE4, although 3.5.9 could also be useful. That is it! We will provide all the training and support. No programming knowledge is needed. Join #kde-bugs on irc.freenode.net anytime to find out more details.  Also, we have a spiffy new <a href="https://mail.kde.org/mailman/listinfo/bugsquad">mailing list</a>, lots of new documentation on techbase, and are already discussing T-shirts. Surely world domination is next? 





<!--break-->
