---
title: "UserBase Goes Live!"
date:    2008-09-19
authors:
  - "lpintscher"
slug:    userbase-goes-live
comments:
  - subject: "Now the same for news"
    date: 2008-09-19
    body: "This is in my humble opinion a very good decision. MediaWiki is a great tool, and the huge number of people that contribute to Wikimedia Foundation's projects prove that a wiki model is very capable of generating great content.\n\nThis will allow many users to quickly contribute content, fix it, or expand it.\n\nAnd now, also in my humble opinion, it will be great if this way of working could be extended to news sites like this. Look at all the stories in the dot's frontpage: all are written by people who work in other areas of KDE.\n\nWe've read in some blog posts that a change in the software behind the dot is planned, and Drupal is a good candidate. I'm not a Drupal fan, but it has a great feature that could be useful to a site like the Dot. Users of the system can have personal blogs (if allowed) and if an admin wants, can promote it to the front page. A bit like Slashdot's journals.\n\nI think this could be great to lower the entry barrier for people who lurk the development mailinglists or the Planet and might want to promote KDE somehow. Indeed, some developer's posts are very targeted to users (like the ones from the packagers or distro developers). It would be cool a way to highlight them.\n\nWell, maybe I'm rambling here. Sorry. :-)\n\n"
    author: "suy"
  - subject: "Re: Now the same for news"
    date: 2008-09-19
    body: "You can already submit your own stories. I have, and have gotten them posted before. It's not that people can't submit stories, it's just that, apparently, they don't. ;-)"
    author: "Jonathan Thomas"
  - subject: "Re: Now the same for news"
    date: 2008-09-20
    body: "Being on the receiving end of the mailing list that gets the email notifications when someone submits a story: we get one submission from outside the core group every week or two, if that.  If it's good, it's posted.  If the submission isn't that noteworthy, we save it for a collection of quickies.  If the submission needs to be rewritten, it usually gets rewritten first and then posted.  But in reality, so few submissions are received..."
    author: "Troy Unrau"
  - subject: "Why?"
    date: 2008-09-19
    body: "Having more wikis is not the solution to usable information. More choices in some areas is a good thing, but kde.org should not be the first point of contact. IMHO, kde should cordinate with distros and have each one create a set of good documentation on their corresponding wikis. That'll give them a good incentive to make sure that the wiki remains updated, and that it is more geared towards the actual packages. Come on, all distros customize the kde they ship - while there is a scope for generic information, changes are that it is going to confuse the users."
    author: "Anon"
  - subject: "Re: Why?"
    date: 2008-09-19
    body: "I would argue that it makes more sense for KDE to provide the high quality base documentation, and then allow distributions to adapt that based on their own customisations. In reality the differences between one distribution and another are rarely that big and it makes little sense to duplicate so much work for the same end result - although of course we welcome (and need) contributions to flow the other way as well.\n\nIt's also worth noting that this isn't \"another wiki\". In fact, the purpose of userbase is to reduce the amount of duplication of documentation that exists on KDE. Currently information for users is distributed between wiki.kde.org (which itself has become a little haphazard) and individual project wikis. Some information is duplicated, some is out of date, and some is mixed in with developer documentation. All this makes it more confusing for users to get at what they need: clear, simple, descriptions of our software and what it does.\n\nAs userbase is rolled out, w.k.o is being remove. User docs from w.k.o are being moved to userbase and developer docs to techbase where appropriate. Additionally we are working with application developers to move their user documentation from single project wikis to our central location.\n\nWhen you think about it, it really does make a lot more sense! Hope this has helped to clear things up a little.\n\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: Why?"
    date: 2008-09-19
    body: "in addition to what Martin already noted above, i'll address this bit:\n\n\"kde should cordinate with distros and have each one create a set of good documentation on their corresponding wikis.\"\n\nthere are many issues with this idea. here are some of them:\n\n* we'd spend time coordinating the a large number of operating systems, and we simply don't have that kind of manpower. distros would be MORE than welcome to point from userbase to their own wikis, or vice versa, for distro-specific content\n\n* much of the information will be precisely the same between OSes. why should each operating system community duplicate all the effort? this is, in fact, one of the problems we currently have: some questions get answered on one distro wiki, and another question gets answered on a different distro wiki ... and the questions aren't distro specific at all.\n\n* not all of our supported operating systems have such community organs. Windows and Mac users are the obvious targets here, but there are numerous less community minded operating systems or ones that are simply too niche/small to support a vibrant enough community to have a well stocked wiki of their own\n\n* there are many things that we, as the SOURCE of the products, would like to definitively answer. distros often get the answers to certain things wrong, such as \"how long with kde 3.5 be around for?\" putting our hands in third parties for information such as this, which are real concerns for our down stream users, is utter insanity.\n\n* if having information on their wikis is good incentive to make sure it remains updated, then link to userbase from their wikis and let's combined all those community efforts into one\n\nthe balkanization of free software along the lines of distributions is a horrible turn of events for free software. it's been more destructive than any KDE/GNOME split has been (for instance), and i personally will do anything i can to ensure that the distros-are-the-defining-lines trend does not continue to reinforce itself. i realize the important role of distributions in the FOSS supply chain and that there will be differences between them, but they are neither properly incentivated nor have the necessary assets to productively serve as the primary caretakers of the general FOSS using population. they can play a very important role in that area, but shouldn't be the sole actors.\n\nall imho, but then i think the last 10 years bears this out pretty clearly."
    author: "Aaron Seigo"
  - subject: "Re: Why?"
    date: 2008-09-20
    body: "Hello Aaron,\n\nwhat do you mean when you say \"balkanization of free software along the lines of distributions\", where do you see that happening?\n\nI only see a couple of players behind closed doors, where you have to buy them, and only one of them has a free clone. They sort of don't contribute (directly) to the advancing of Linux, do they? Anyway, it's not the matter, is it?\n\nAnd then I see 3 distributions Debian, Fedora, Ubuntu and openSUSE (sorted by age I hope) that simply work on different areas at different times, with different priorities.\n\nBut in the end share almost anything with another sooner or later. Is there anything wrong with that? \n\nI was always thinking of this as a particular strength. \n\nLet me give an example: I seem to already have decided that KDE 4.2 may not be ready for me either (rather I am not ready for it, I may just be too old, too comfortable with what I have now) and you can imagine how happy I will be to be able to use it on Debian Testing, whereas the others have already signaled that they will default to KDE 4 and have KDE 3 only for unported stuff, or as a 2nd class interest.\n\nOn the other hands, I think openSUSE shiped the earliest stable release of 4.0 available to them, providing backports to enhance the user experience. Which must be a god-send for many users. The openSUSE is in a long tradition (from SUSE) of supporting desktop Linux, where others don't seem to be so much for a long time.\n\nI wouldn't want to live in a world with less successful distributions. Somebody (Ubuntu) has to care for Gnome still, not? And somebody (Fedora) must make sure that SELinux actually gets some use, not?\n\nOr else I fear, things will never get ready. Actually I would hope that there will be an increasing number of successful (like in big community) distributions that offer specializations in the targets and goals. \n\nDebian could be the Universal Distribution. But I somehow came to conclusion that it could hardly be the Universal Community.\n\nYours,\nKay\n\n\n"
    author: "Debian User"
  - subject: "Re: Why?"
    date: 2008-09-22
    body: "I wasn't refering to development models (closed doors, e.g.) or even the amount of technology that gets shared (which is near to 100%, really) nor that choice and diversity is bad.\n\nWhat is unfortunate is that there are often rather uncessary differences that give little or no actual benefit to the user but certainly make multipel distros harder to support and ship software for. But even that's not the Worst of the Problems(tm), imho.\n\nAnon suggested that each distribution should host their own information and each distribution's user base should suckle off of that teat. So instead of having a userbase that is N*M large, where N is the number of distros and M is the average userbase size per distro within a given category, we have N communities of M size.\n\nIt results in duplications of efforts and no clear targets for 3rd parties. \"LDN\" from the Linux Foundation is on the scene years and years later than it should've been in any sane environment and even now it's hardly the comprehensive body of information it should've been. But an \"LDN\" is critical for 3rd party involvement. It didn't happen because there are all these separate projects and all these separate distros and having these littel fiefdoms is an acceptable status quo.\n\nIt also makes it harder for new people to get involved because choosing their distribution flavour also means choosing their community to a large extend. If one or the other is a good fit .. too bad.\n\nIt also makes it harer for us to punch at our true weight in the market: instead of seeing all Linux based roll outs as equal and part of the same blob, they get promoted along division lines \"Canonical on Dell\" or \"KDE in Brazil\". Obviously getting credit is important and deserved, but the current mechanism rarely rewards those who actually deserve it anyways.\n\nIt also makes for incoherent messaging: person A will ask group B to prognosticate on group C. They don't ask group C because person A affiliates themself strongly with group B. Misinformation is unintentionally borne out into the community. \"Person A\" is often a blogger or journalist, group B is often a distribution and group C is often an upstream project. (As a further bit of humour, group B will often get pissed should group C *ever* say anything about them.)\n\nThere are benefits to the balkanization as well, but it's really not worth it in the ballance. We can maintain diversity without having these large walls made of wikis and bugzillas and marketing teams keeping everyone apart."
    author: "Aaron Seigo"
  - subject: "Re: Why?"
    date: 2008-09-19
    body: "There are some distros/systems that do not change gratuitously change KDE. Many just brand it a bit with artwork, and a few actually leave it alone, giving you a \"vanilla\" KDE. That is what I prefer. Even with \"customized\" distros, 99% of the KDE documentation will remain the same.\n\nI'm not understanding your concern. It's like saying we shouldn't develop KDE because distros are going to be developing their own customized KDE anyway."
    author: "David Johnson"
  - subject: "Re: Why?"
    date: 2008-09-20
    body: "you must be confused.\n\nfirst you say:\n>[Having more wikis is not the solution to usable information.]\n\nthen you say:\n>[...kde should coordinate with distros and have each one create a set of good documentation on their corresponding wikis.]\n\nare you proposing MORE WIKIS? contradiction! your argument fails.\n\nAlso... could you please outline how MANY distributions could find the labour to maintain all these wikis for each distribution if KDE as a whole already does a poor job. In actual fact you would be taking the people interested in writing documentation and spreading their efforts very thin. end result, it would not work."
    author: "mike"
  - subject: "Not for users..."
    date: 2008-09-19
    body: "Is that USERbase?\n\nDo you espect a user do this?\n\nhttp://userbase.kde.org/Integrating_Spamassassin\n\nIs not friendly at all, it looks like material for the LinuxHater blog."
    author: "David"
  - subject: "Re: Not for users..."
    date: 2008-09-19
    body: "That is sooo outdated, it should be removed right now. Better ask around before taking such an old documentation from somewhere and putting into the new wiki. Really, this is not bashing, it is a suggestion to improve the quality of UserBase wiki."
    author: "Andras Mantia"
  - subject: "Re: Not for users..."
    date: 2008-09-19
    body: "> Do you espect a user do this?\n\nAndras already pointed out that this might no longer be necessary, e.g. have been replaced by something easier to use.\n\nNeverthless I just wanted to point out that there are a lot of very different types of users [*] and information which is very helpful for one type might not be helpful at all for another, independet of whether the available information is too complex or too high-level.\n\nThe only workable solution is to collect different approaches to the same problem and try to group them and annotate them so different types of users can find the documentation of the approach most useful for their purpose.\n\n[*] just as an annectotal example: KDE, being a Free Software desktop project, would most likely define the term \"end user\" as someone interacting directly with computer, while the Linux Foundation, being a industry interest group, defines \"end user\" as someone being a CTO, CIO or similar position of companies or institutions.\n"
    author: "Kevin Krammer"
  - subject: "Re: Not for users..."
    date: 2008-09-21
    body: "Exactly! Putting aside the fact that the tutorial is outdated, it doesn't make it less \"for users\". \"Users\" does not include only beginners or complete newbies to KDE. In fact, it's quite difficult to quantify or definitely qualify what \"for users\" include. It will be much easier to define what it will not include. And what UserBase will not include is content that is oriented towards KDE development. So you will not see pages devoted to compiling KDE or this or that module. But you may see pages that use scripts or commands to make your life as a user more convenient. Of course, you will also see information that's relevant to beginners or casual users as well."
    author: "Jucato"
  - subject: "Re: Not for users..."
    date: 2008-09-20
    body: "> it looks like material for the LinuxHater blog\n\nnah, it looks like an opportunity for you to put on your Solutions and Ideas hat and get involved to make it better.\n\nhere's what my S&I hat murmurs into my ear:\n\n* pages could well do with ratings: skill level, usefulness, etc. wouldn't that be cool and useful?\n\n* pages like this one should probably be updated or dropped (kde 3.*2*?)\n\n* that page needs to be organized in the tree better (e.g. http://userbase.kde.org/Integrating_Spamassassin -> http://userbase.kde.org/Tutorials/Mail/Using_Spamassassin_with_a_Mail_Reader)\n\nwhat improvements can you think of? why don't you try and do one or two of them?"
    author: "Aaron Seigo"
  - subject: "Re: Not for users..."
    date: 2008-09-21
    body: "Someone took the trouble to use the Discussion page attached to the spamassasin page, to tell us about the outdated nature.  That's what Discussion is there for (among other things) and we do monitor what's written and act on it where possible.\n\nIt's very likely that there are other pages where this applies.  Please tell us.\n\nI would love to see skill-ratings attached - but we are mediawiki newbies, so if you know how to do it, contact us either by email (community-wg@kde.org) or on #kde-www"
    author: "Anne Wilson"
  - subject: "thanks"
    date: 2008-09-19
    body: "Thanks a bunch community-wg, userbase has the potential to lowering the entry-barrier for many kde users out there, and helping out those who would like to learn more about kde before trying it out.  It'll also be good as a reference guide to send people to when they have questions about their kde systems (for sysadmins)."
    author: "Vladislav"
  - subject: "Nice"
    date: 2008-09-20
    body: "Really, really nice.  Fantastic progress in such a short timespan.\n\nI'd like to trash the current kontact.kde.org site and do a redirect to the appropriate place in UserBase.  It would be really great if the UserBase community could make sure all the info from kontact.kde.org was transferred and updated.  Let me know if that gets done and I will work with the sysadmins to make the redirect happen. -Allen"
    author: "Allen Winter"
  - subject: "Re: Nice"
    date: 2008-09-21
    body: "We don't mind whether the main content is on project sites quoted from userbase, or on userbase redirected from project sites.  That should be the decision of the devs concerned.\n\nThere are quite a lot of the sites we've accessed for userbase that are very out of date.  I guess the problem is that devs have only so much time, and it has gone on development rather than documentation.  Perhaps we could have some volunteers to help them get the sites up to date?"
    author: "Anne Wilson"
  - subject: "Site down?"
    date: 2008-09-20
    body: "I cannot access UserBase right now :/ Am I the only one?"
    author: "Vide"
  - subject: "Re: Site down?"
    date: 2008-09-20
    body: "Looks like most KDE-related sites (techbase, bugs.kde.org, etc) are down at the moment."
    author: "Anon"
  - subject: "404"
    date: 2008-09-20
    body: "I can't seem to access the site at all..."
    author: "anders"
  - subject: "Re: 404"
    date: 2008-09-21
    body: "Well, both techbase and userbase are down right now. The server is probably down ..."
    author: "KubuntuUser"
  - subject: "Re: 404"
    date: 2008-09-21
    body: "Looks like a <i>News Picks</i> posting at Groklaw.net about the existence of UserBase might have contributed to an overload? Pamela's more powerful than Superwoman! ;)"
    author: "bbaston"
  - subject: "Re: 404"
    date: 2008-09-22
    body: "Could be. Planetkde.org is redirecting to developer.kde.org, so technically it's down too. :-("
    author: "Meme #32398"
  - subject: "OT: present windows"
    date: 2008-09-22
    body: "Take a look at this:\n\nhttp://www.undefinedfire.com/kde/more-present-windows-love/\n\nIt looks soooo good :-) I hope this physics engine makes it into 4.2 and will replace all the linear animations in kwin (minimize, alt-tab switcher etc)"
    author: "Yves"
  - subject: "Re: OT: present windows"
    date: 2008-09-22
    body: "it's already committed to trunk and so it is in 4.2. But we have to port the effects to use this. And currently only 2D is supported. So no new motion for CoverSwitch or Cube yet.\n\nBtw the animations are not linear in 4.1, but it is hardly visible"
    author: "Martin Gr\u00e4\u00dflin"
  - subject: "So, where's the link?"
    date: 2008-09-22
    body: "I looked around but I could not find a link to userbase from kde.org."
    author: "Janne"
  - subject: "hmmm"
    date: 2008-09-22
    body: "Good idea. I looked around and however one thing I would like to say though:\n\nIt is a bit limited right now. There are a few screenshots and descriptions but it is not really \"helpful\". It looks slick but I am missing a bit more information.\n\nMaybe we just have to be patient, but I want to point this out right now. :)"
    author: "markus"
  - subject: "Re: hmmm"
    date: 2008-09-23
    body: "Like you said: Be a little patient.\n\nIn some weeks you should get information about many apps and more."
    author: "Mark Ziegler"
---
The KDE community is pleased to announce <a href="http://userbase.kde.org">UserBase</a>. UserBase is the new end-user wiki for KDE and complements <a href="http://techbase.kde.org">TechBase</a>, the wiki aimed at developers. It will contain tips and tricks, links to where to get more help, as well as an application catalogue giving an overview of the different kinds of programs that KDE offers.




<!--break-->
<p>After weeks of preparation and vivid discussions at Akademy, UserBase is ready to help users with their day-to-day problems and is awaiting contributions from the wider community. <a href="http://userbase.kde.org/Talk:Welcome_to_KDE_UserBase">A Talk page</a> is available, for suggestions and requests for content.</p>

<p>The KDE community hopes to offer information that is better tailored to the needs of the KDE users by offering them a place to share their knowledge.</p>

<p>For questions regarding UserBase please contact the Community Working Group at community-wg@kde.org.</p>



