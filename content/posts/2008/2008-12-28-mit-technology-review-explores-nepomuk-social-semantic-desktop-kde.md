---
title: "MIT Technology Review explores NEPOMUK, the Social Semantic Desktop in KDE"
date:    2008-12-28
authors:
  - "sjarvis"
slug:    mit-technology-review-explores-nepomuk-social-semantic-desktop-kde
comments:
  - subject: "K3b"
    date: 2008-12-28
    body: "Yeah, NEPOMUK is nice and stuff. Too bad that Mandriva made its employee Sebastian Trueg neglect his other project K3b and concentrate on NEPOMUK instead. For some insane reason the people at Mandriva think that tagging files is somehow more important than the possibility to back them up. K3b is dormant since months."
    author: "The Devil"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "I burned a DVD of OpenSuse 11.0 with K3B a few nights ago. *shrug*\n\nNepomuk is also a lot more than \"tagging files\". It also covers things everyone does (work with information on their computer), while K3B covers things a lot of people do (make CDs and DVDs). It's really hard to compare the two in a meaningful way and say \"this one is more important\" since they are both rather important.\n\nSo my question to you (the \"royal you\") is why aren't others working on K3B if it's so important? =)"
    author: "Aaron Seigo"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "I don't know why others don't work on K3b 2.0. Maybe they are too busy working on one of those GNOME CD writing apps (GNOME Baker, X-CD Roast, Brasero, Nautilus' recording feature,...)? ;-)\nLuckily Brasero works fine under KDE 4..."
    author: "The Devil"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "And the KDE3 version of K3b works as flawlessly as ever under KDE4, no need for luck."
    author: "Morty"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "Why should I pollute my system with outdated KDE 3 libraries? I need to have some GNOME stuff installed anyway, so Brasero doesn't result in additionally installed dependencies,"
    author: "The Devil"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "There is a K3B package compiled against KDE 4 available in my distro. It's probably not very stable, but it has worked for me."
    author: "sandsmark"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "yeah, i'm pretty surprised that no one has taken over k3b code till now. I heard months ago that Sebastian don't have time any more to work on this project, i thought that someone will continue it. \n\nIt really is a surprise for me that \"burning pillar\" of KDE isn't ported to KDE4. \nI'm not complaining, because i can't code and I can estimate if it is hard to port k3b to kde4, i'm just surprised. ;]"
    author: "Koko"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "It really doesn't bother me that K3B hasn't been ported to KDE 4. KDE 4 is still not ready for real people, as opposed to developers, so I won't be using it, and I won't be installing it for my clients."
    author: "tracyanne"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "While that is or might be considered true,\nIt does help to have the software out in the real world so that its bugs can be found by end users and it can reach the stability you need to get it rolled out ;)"
    author: "Mark Hannessen"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "even my mom uses kde 4.2beta2 now, as it is stable enough, fulfills her needs pretty well and she can learn to use kdenlive in its kde 4 incarnation. big up to anyone who worked on this, good work folks!"
    author: "Jay"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "even my mom uses kde 4.2beta2 now, as it is stable enough, fulfills her needs pretty well and she can learn to use kdenlive in its kde 4 incarnation. big up to anyone who worked on this, good work folks!"
    author: "Jay"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "> t really doesn't bother me that K3B hasn't been ported to KDE 4.\n> KDE 4 is still not ready for real people, as opposed to developers,\n> so I won't be using it,\n\nWell considering that a port might take a half year to a year to complete, it would be useful if work was already underway :p So at the time KDE 4 Is Ready (TM) a new stable K3B version is also available at that time."
    author: "Diederik van der Boor"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "Since it's already ported that should not be much of an issue. \n\nIt already builds, runs and does it's job. So depending on what the developers know of issues and have planed, releases may not be far off. "
    author: "Morty"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "Unfortunately it's not really ready for developers either.  It was about a year after 4.0 that python-kde4 became available.  I still don't think it supports everything that KDE4's C++ api supports.  Likewise, key KDE4 technologies like Nepomuk and Plasma have been slow to solidify.  Developers haven't ported stuff because the infrastructure wasn't there.  Personally, I gave up after a long love affair with KDE 3.2-3.5, and moved back to GNOME -- even after a long hate affair with the 2.x series.  I'd love to stick with KDE, which is why I'm here looking at the news to see if anything sensible has happened, but the whole idea of releasing alpha-quality (YES, alpha) regressive code in 4.x as the replacement (regardless of the official line) for a very good and stable 3.x is just insane."
    author: "Lee"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "Call us crazy then :-)\n\nSome of the best results started off as crazy ideas. So I think we're in the right track.\n\nIn any case, I don't understand why some people had to remove the system that worked for them... If KDE 3 works, use it."
    author: "Thiago Macieira"
  - subject: "Re: K3b"
    date: 2008-12-30
    body: "Sorry, my initial comment there was a little too harsh, but this is an incredibly frustrating issue, that KDE core devs/release managers seem intransigent about.  I simply don't get why KDE devs would spend years developing a great reputation with KDE 3.2--3.7, and then throw it all away by calling alpha code 4.0, 4.1, or 4.2.\n\nIt's frustrating as a KDE user, because I've recommended KDE to people over GNOME, on the basis of its professionalism and serious feature set, only to see half-baked stuff presented with a stable version number.\n\nI **KNOW** the official line is that early 4.x releases are for developers and early adopters, but there is no sane reason for that policy, since it goes against everything that other projects' releases do.\n\nMore importantly, it's frustrating as a developer.  KDE 4 isn't ready, yet people are using it.  Worse, it's not ready for developers, yet people are expecting user-facing apps to be ready.\n\nMore importantly still... KDE 4 still needs tons of manpower to finish it.  Yet, most of us thought things were going well, until we saw the state of 4.x!  If only the status had been clear at 4.0alpha1, we might have pitched in and gotten the 4.0 release looking good.\n\nAnother issue is that I contributed ideas to KDE 4, years ago, when there was a forum for ideas (on KDE-Artists, I think).  Seemingly, those ideas were taken on board and mentioned in plans, despite the credit for the ideas being dropped along with the forum.  BUT, those ideas have not yet materialised fully.  I might have worked on them myself, if APIs for my chosen language had been developed properly, along side the core C++ stuff (which I was also assured would happen, years ago).\n\nBasically, the whole process has been unprofessional.  I never got why groups like Ubuntu chose to back GNOME instead of KDE's superior, object-oriented, more featureful C++ technology.  Now it's very clear to me though: no matter how good KDE was/is/will be, no organisation in its right mind can back KDE while it's getting fundamental cooperative and planning stuff so wrong.\n\nThis really needs to be sorted out.  I'm not sure KDE will come through 4.x as a desktop much more popular than enlightenment, much less one of the two major desktops.  If 5.x is the same regression from 4.5+ that 4.x was from 3.5+, then I'd bet good money that KDE will die, inasmuch as any free software can ever die."
    author: "Lee"
  - subject: "Re: K3b"
    date: 2008-12-31
    body: "Why not simply call yourself a KDE 3 ecosystem fan and recommend that to your friend and don't drop it for something else? All you wrote is how you're disappointed how slow the KDE 4 ecosystem is getting up to your personal standards, but all you seem to do (according to what you wrote) is drop both KDE 3 and 4. What's the sense in that?"
    author: "Anon"
  - subject: "Re: K3b"
    date: 2009-01-04
    body: "That's a bit like asking, if Windows 95 is so bad, why don't you stick with DOS?  For their times, I liked OS X 10.0, OpenLook, MGR, AmigaOS, BeOS, and others.  Doesn't mean I still want to use them when the world is moving on."
    author: "Lee"
  - subject: "Re: K3b"
    date: 2009-01-01
    body: "Burning pillard of KDE? K3b is \"Burning pillard\" of Linux!\n\nAlmost every computer has CD/DVD burner on these days and what happens... one of the TOP 5 applications gets abandoned because of technology what can't be used at all... yet..."
    author: "Fri13"
  - subject: "Re: K3b"
    date: 2009-01-02
    body: "yeah..\ni really need this K3B too.. i can't live without this...anyway i love kde 4 so much too.. please don't force me to choose.. both are great.."
    author: "jack"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "Now that things have settled down a little bit in the libs it would be nice if some core developers (N.B.: just some of them) would switch their focus for a slice of time (maybe just for a release, e.g. from 4.2 to 4.3, and then just do basic maintenance until a new maintainer steps up) on some user apps that need love.\nI really miss K3B, Kaffeine, KSCD (bad state ATM); and some extra attention could be given to getting several apps more tightly related to KDE, for example WebKam (see kde-apps), KMess, Kile, KGRUBEditor (I heard time ago that Kubuntu wanted to integrate it), KDE Partition Editor (really good it got into kdereview).\nI'm not suggesting a radical change, but from my point of view it would be nice if KDE 4.3 will become the \"focus on integrating applications\" release.\n\nSorry for the OT."
    author: "Diego"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: ">KGRUBEditor (I heard time ago that Kubuntu wanted to integrate it)\n\nIt's usable and lays in kubuntu systemsettings already."
    author: "anon"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "What would be even better is if some new blood would chip in to help - it often baffles me how we can have almost one new contributor being granted SVN access every day (over 300 a year!), yet nobody works on the beloved apps that everyone uses, like K3B, KControl, Konqueror etc.  Very strange indeed."
    author: "Anon"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "This is mostly because those apps have a huge codebase that is quite hard to understand for somebody new. Working on stuff like plasma is much easier for example"
    author: "Beat Wolf"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "Add Kaffeine to the list. I am still forced to use the KDE 3 version on Kaffeine. Yes it works fine but it's still a bit sad that we still have to use an \"old\" version on a new desktop.\nK3B is one of my favorite KDE apps but like Kaffeine only the KDE 3 version works properly. The new version is good for nothing.\n"
    author: "Bobby"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "the problem is that phonon (at least what is written on the kaffeine site) is nowhere near powerful enough for them to make it proper. I've pretty much given up on kaffeine or having a decent phonon based video app any time soon. I just use smplayer (awesome, but lacking kdelibs integration with nice stuff like common shortcuts and kio).\n\nNobody else seems to mind (or even discuss) this situation so I guess it really isn't important enough."
    author: "txf"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "It seems like there are more techies here than normal users, that's why these things are not discussed more often.\nSmplayer is OK but for DVDs kaffeine is still the best so I will continue to use the old version until the new kid grows up. The advantage with these older versions is that they are rock solid and they always work."
    author: "Bobby"
  - subject: "Re: K3b"
    date: 2008-12-30
    body: "true...never was that much of a fan of kaffeine, more likely because of xine's hideous subtitle/osd rendering"
    author: "txf"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "KMess... well, let's wait instead for this. The best MSN client for Linux, aMSN, is switching from TCL/Tk to... QT4!\n\nhttp://geowworld.blogspot.com/2008/06/amsn2-ya-tiene-front-end-qt4.html"
    author: "Alejandro Nova"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "http://drfav.files.wordpress.com/2008/10/amsn2.png\n\nFor the unbeliever, I can see aMSN looking beautiful in Oxygen colors ;)."
    author: "Alejandro Nova"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "Which version of K3B did you use? I couldn't burn the SUSE DDVD with the KDE 4 version.\nNEPOMUK is fine like the others say but why neglect the cow to concentrate 100% on the calf?"
    author: "Bobby"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "I am not The Devil, but I think he has some point. I think K3B is one of the best apps within the KDE world (there are many others too of course), and for me I really do not see any use for Nepomuk as such, whereas I think a graphical tool to burn DVDs is nifty. I dont have a problem with Seb working on something else in the least way, this is his decision of course etc... however I really think the main issue is whether something is important or not.\n\nMaybe in some months we can review and could get a registered user base to vote on issues. I am also not implying that those votes _should_ have an influence, but I am suggesting that these votes could provide a meaningful point what users value. \n\nFor me, I wouldnt vote for Nepomuk, and for you it would probably be that nepomuk is more important. So the interesting thing is how many different people share one's point of view."
    author: "mark"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "Let me elaborate - I dont need Nepomuk because I already have a system where I find everything that I want. (I wrote a pseudo shell in ruby where everything I work with is already identified properly. I dont need tags for any other apps to identify for me because I and \"my shell\" know where things are to be put. In a way it is a content management system already. Just to explain why I have no need for file finders and the like, and I agree that other people could well need this. Just I wont need these things.)"
    author: "mark"
  - subject: "Re: K3b"
    date: 2008-12-30
    body: "Nepumuk is a great idea of course but it's really sad that a very important app like K3b has to suffer for it's development.\nThe KDE applications that I personally use most are Kaffeine, K3b, Amarok and KMail.\nAmarok is coming on fine and Kmail has made some tremendous improvements - thumbs up to these guys. I think that it's also benifiting from Nepomuk though.\nThe other two apps, Kaffeine and K3b are laging far behind. It would at least be good if a new blood would jump in to help with K3b like Anon said. I know it would be a lot of work to go through the code and all that but it would at least move forward."
    author: "Bobby"
  - subject: "application porting status (Re: K3b)"
    date: 2008-12-28
    body: "According to http://techbase.kde.org/Schedules/KDE4/Application_Porting_Status there are quite a few applications that have not been ported yet."
    author: "testerus"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "You're jumping to conclusions.\n\nI don't think Mandriva \"made its employee [...] neglect\" anything. Project such as NEPOMUK work quite differently actually. The EU contract companies to work on specific parts of a research project. Those companies put a verfiable effort in terms of man hours into the project in question, contributing to the project's goals. Often, companies contract or hire people to conduct this work. In the case of Mandriva, I suppose they hired Sebastian because of his knowledge of KDE and probably other qualities to work on NEPOMUK. So Mandriva simply got contracted to put work into NEPOMUK. I'm sure it would be pretty daft to tell the EU auditors \"Well, that guy on the Dot said we should port k3b instead, so we didn't fulfill our contractual obligations\".\n\nThat's where reality strikes: It often happens though that people that are starting a job find less time to put into their hobby projects (which I understand k3b has been for Sebastian).\n\nSo instead of blaming Mandriva for K3b not being ready yet, I'm happy that Sebastian found a way to be paid to work on KDE software, and pretty cool new technology that has the potential to set KDE apart from Windows and Mac, and surpass it in terms of innovative new technology."
    author: "sebas"
  - subject: "Re: K3b"
    date: 2008-12-29
    body: "\"pretty cool new technology that has the potential to set KDE apart from Windows and Mac, and surpass it in terms of innovative new technology.\"\n\nBut you yourself are jumping to conclusions. Noone can say what the future holds. For so many years we heard the propaganda of \"this year will be the linux desktop year\" and hardly anything happened.\n\nNow, KDE is a great project with smart people working on it, and I think he is incorrect in blaming Mandriva, but the fact remains that GREAT APPLICATIONS like K3B is what I believe were the key strengths of KDE. \n\nYears ago i felt that Gnome looked nicer than KDE, and I was using only fluxbox. But I used more and more of the KDE apps and this is why I like KDE more than Gnome today. So what is the point if we do features that \"set KDE apart from Windows and Mac\", but we would lose focus on GOOD applications?\n\nThe whole move to KDE 4 was not without pain. Take cmake - it still is not a 100% replacement for the ancient autoconfigure. Take the move from dcop to dbus - the syntax for the named programs url's really sucks in dbus. Take the switch to plasma - I am sure plasmoids will be awesome and hopefully as lively as kde-apps.org, but right now the activity is really not that high. \n\nOne problem is that with big moves, too many times lose out on the details that make something great. And without attention to the good details, what advantage would I really have? The fancy GUI effects are cool, but I really want to do work faster than before - that would be a critical feature for me."
    author: "mark"
  - subject: "Re: K3b"
    date: 2008-12-28
    body: "I can't see what all the fuss is about\n\nI just used the KDE4 port of K3b, and have been using it for quite some time now.  Works like a charm.\n\nDormant?  Well someone must be working on it, because there are always updates for it on opensuse."
    author: "R. J."
  - subject: "the killer app"
    date: 2008-12-28
    body: "would be the ability to look at a file and have it tell me where I downloaded it from. can it do that yet?"
    author: "illissius"
  - subject: "Re: the killer app"
    date: 2008-12-28
    body: "\"Killer app\" suggests an outstanding feature, nowhere else to be found.\nBeOS supported something like that in the 1990s. Mac OS (X) and Windows (on NTFS partitions) also support that, but yeah.. NEPOMUK would make that possible under Linux, too.\nAs a former BeOS, Windows, and Mac OS X user, I found most those metadata features pretty useless. I'd rather use the final release of K3b 2.0 (see above). ;-)\nWell, different people, different tastes..."
    author: "The Devil"
  - subject: "Re: the killer app"
    date: 2008-12-28
    body: "You repeatedly seem to be missing the point that in a Free Software project, you cannot direct people to one task (not even your pet peeve one), except if you pay them.\n\nI'm personally not really impressed by people trying to push their own agenda or opinions onto someone else (or a group in this case), unless these people actually chip in and put some weight behind their words.\n\nThe fact that you find the metadata storage (which is of course only one of NEPOMUK's features) not so important probably says more about you than anything else.\n\nAnd in case it isn't clear, you're not alone. There are more people preferring to burn a CD yet over pushing the vision of the semantic desktop ahead. Reality though is that most of them just use existing tools without whining and accept other people's choices as they go.\n\nReally, what's the problem with k3b port not being ready for release yet?"
    author: "sebas"
  - subject: "Re: the killer app"
    date: 2008-12-30
    body: "To be fair, you chose the one person who \"gets it\" to pick on. He's one of the few in this thread who /do/ realize that.  Didn't you catch his final sentence?"
    author: "Evan \"JabberWokky\" E."
  - subject: "How do I use it?"
    date: 2008-12-28
    body: "\"NEPOMUK offers a combination of automatically created metadata (such as document modification dates and music file tags) with user-created comments and more complex relationships such as that between an email attachment and the sender and recipients of an email.\"\n\nI have asked this before, but... How do I actually use NEPOMUK? I mean, looking at the quote above: What would I have to do if I wanted to find all files sent as email-attachments by certain person? As the quote above shows, it should be doable. But how do I actually do it?"
    author: "Janne"
  - subject: "Re: How do I use it?"
    date: 2008-12-28
    body: "I don't think that KMail tags any saved files, yet. All involved apps need to support NEPOMUK."
    author: "The Devil"
  - subject: "Re: How do I use it?"
    date: 2009-01-07
    body: "Yep - my understanding (which may be wrong) is that this is more the ultimate aim use case example. There is more nepomuk integration in 4.2 with rating and tagging of files from dolphin for example. As for emails and the quoted example, I think that depends on the abilities of applications like strigi to grab that data and maybe also on kmail etc too.\n\nThe interesting bit imho that MIT picked up on is that the integration in KDE (although still quite early) is being used to help sort out how exactly these things are going to work and how real users are going to interact with it."
    author: "Stuart Jarvis"
  - subject: "Re: How do I use it?"
    date: 2009-01-07
    body: "PS - sorry if the distinction between what nepomuk offers/will offer and what is actually easily accessible in KDE right now was not clear"
    author: "Stuart Jarvis"
  - subject: "Re: How do I use it?"
    date: 2009-01-07
    body: "And see also sebas comment below which has some useful info. Note in my distro (fedora) strigi indexing seems to be disabled by default but can be switched on using the desktop search control module in system-settings advanced panel"
    author: "Stuart Jarvis"
  - subject: "Strigi Reloaded?"
    date: 2008-12-28
    body: "Question; will \"Strigi Reloaded\" be a part of KDE 4.2? For more information about Strigi Reloaded see http://www.kdedevelopers.org/node/3573.\n\nUnfortunately for me Strigi in KDE 4.1.3 is crashing during initial indexing, but more unfortunately for me Strigi can't continue after the crash so it needs to completely restart the indexing.\n\nI'm really looking forward a good search tool in KDE 4.2.\n\nWhich leaves me at one more question; how to use (a working) Strigi and Nepomuk? I can't find a user interface for both. I think indexing and tagging is very useful, but how to extract the information?\n\nThanks!!"
    author: "LB"
  - subject: "Re: Strigi Reloaded?"
    date: 2008-12-28
    body: "Good question, I would like to know also.\n\nI assume it is a plugin for KRunner but I have only got crashes out of it yet so 'SOL' I guess although so many seem to be promoting it I guess I have done something wrong.\n\nAs a stop gap measure both Google's desktop search function (pressing ctrl twice when installed) works well although updates slowly and Gnome's tracker is a delight to use but the only real client is nautilus which I don't like. Beagle was quite good but that just failed to recognise some files. So use Google's desktop as it is the better of two weevils (as a bonus it indexes your emails if you use GMail, Appointments (through Google Calendar) and I think chat conversation if you use their jabber servers and I don't think the rest do unless you use their designated applications but I haven't got a great deal of ram and Gmail is better anyway as I can search through my entire catalogue of emails and if I let Kmail keep my All Folders open then it takes forever to search for new mail (Outlook seems to do that right though - can't think why it works so well), Korganizer doesn't write back to Google Calendar and Kontact doesn't sync with my Google contacts - I could move completely over but I can use the web based equiv's anywhere so 'SOL' again).\n\nSorry to sound negative again but I have pointed out the problems and workable solutions so it can be considered fair.\n\nOooh, If you have ever touched spotlight on the Mac then you'll be left wanting every time you touch any other desktop - V V Good."
    author: "Dave Taylor"
  - subject: "Re: Strigi Reloaded?"
    date: 2008-12-28
    body: "Desktop Search actually works for me in KDE 4.2 trunk, albeit the UI needs work. For example, when using it from KRunner, I often get too many hits, the KRunner UI is not suitable to handle all that. On the one hand, we're working on improving the KRunner UI, but I'm also thinking of a dedicated app or plasmoid specific to desktop search. Developers can use the classes in kdebase/workspace/libs/nepomukquery to integrate search in their apps.\n\nAs to the question \"How do I use it?\", assuming you have your files indexed, you can use the nepomuksearch IO slave (for example nepomuksearch:/hastag:KDE from any app). Enabling the KRunner plugin called \"Desktop Search\" will search directly whatever you type into the KRunner window (but as I said, the UI there needs improvements.)\n\nFor 4.3, we're looking into integrating this functionality into the file open dialog, which is IMO the natural place for it.\n"
    author: "sebas"
  - subject: "Re: Strigi Reloaded?"
    date: 2008-12-28
    body: "KRunner, Plasma, and the File Open dialog are all good places for it, but where I'd really like the ability is in Dolphin with it automatically searching only in the folders below the one I'm presently in. The filter bar in Dolphin now is very limited.\n\nChad"
    author: "Chad"
  - subject: "Re: Strigi Reloaded?"
    date: 2008-12-28
    body: "Besides putting it in file-dialog, how about putting it in dolphin (well, everywhere to be honest)? I would love to see smart-folders in KDE, where you could define a set of criteria (for example: all text-documents modified in the last 30 days) and that folder would then display all files that match that criteria. \n\nOh, and I think the kio-slave should be renamed. \"nepomuksearch\" is a bit on a long side for quick searches :)"
    author: "Janne"
  - subject: "Funding  Stops"
    date: 2008-12-28
    body: "From the article : \"funding of the nepomuk project stops this month\" , so i really hope that we got the manpower and interest in this project to keep it moving forward fast !!!!!\n\nother attempts already nearly died to get semantic desktop to work (strigi...)"
    author: "ghhhu"
  - subject: "Re: Funding  Stops"
    date: 2008-12-28
    body: "Strigi didn't die but is alive and kicking. It's used in KDE4's desktop indexer, for example.\n\nAs to continuation of NEPOMUK, there will be an announcement regarding this in January."
    author: "sebas"
  - subject: "Re: Funding  Stops"
    date: 2008-12-29
    body: "Is Strigi \"alive and kicking\" in KDE Trunk or should it work with current stable version 4.1.3? For me in version 4.1.3 Strigi is kicking, but isn't alive.\n\nI really hope for a good desktop search solution in KDE 4.2. Are you aware of any changes in KDE to go from hope to reality?\n\nAlso do you know anything about \"Strigi Reloaded\"?\n\nThanks!"
    author: "LB"
  - subject: "Re: Funding  Stops"
    date: 2008-12-29
    body: "I've not tested it with 4.1, but with 4.2 it works for me. That's the integrated \"Strigi Reloaded\" solution, AFAIK. It uses strigi's JStream library."
    author: "sebas"
  - subject: "Re: Funding  Stops"
    date: 2008-12-29
    body: "Is there a way to diagnose what is Nepomuk/Strigi doing? On my system I have always to turn both off or I get 100% CPU usage: I thought it was due to the index being built, but when it still stays at 100% after 5 hours, I'm not too sure."
    author: "Luca Beltrame"
---
KDE 4 saw the introduction of NEPOMUK, the foundations for the "Social Semantic Desktop". The idea behind Semantic desktops is to make it possible for computers to identify meaningful relations between files and real-world 
people and relationships. These relations can then be exploited to help the 
user find their data. <a 
href="http://www.technologyreview.com/computing/21840/?a=f">Read the full MIT Technology Review article on the project</a> to learn why it "might be the semantic desktop that 
actually survives" and see how its inclusion in KDE is making it accessible to 
end users during its development process, and read on for a brief overview of how <a href="http://nepomuk.kde.org/">NEPOMUK</a> works.




<!--break-->
<p>NEPOMUK offers a combination of automatically created metadata (such as document modification dates and music file tags) with user-created comments and more complex relationships such as that between an email attachment and the sender and recipients of an email. Files can then be found by using a search combining several items of metadata rather than relying only on keywords. NEPOMUK aims to go a step further than other similar projects by making it easy for members of a community to share their accumulated metadata and to transfer files with the collected metadata intact.</p>


