---
title: "KDevelop Team Meeting Agenda"
date:    2008-04-10
authors:
  - "harryF"
slug:    kdevelop-team-meeting-agenda
comments:
  - subject: "cool the doctor will be there. "
    date: 2008-04-10
    body: "i hope he is not tired by the trip, i guess traveling from Australia is not really fun.  "
    author: "an"
  - subject: "Re: cool the doctor will be there. "
    date: 2008-04-12
    body: "The Tardus should make quick work of it."
    author: "Ian Monroe"
  - subject: "Why does KDevelop refuse to have a terminal view?"
    date: 2008-04-10
    body: "This may not be true any more, but I read at one point that KDevelop was philosophically opposed to putting in one of those command/terminal windows that lots of unix IDE's have.\n\nI can understand why you wouldn't want to force it, but having access to the command prompt of the underlying debugger is a really powerful thing.  If it's not too hard to have that access available on an optional basis, why leave it out?\n\nAt my company, we debug with dbx on AIX at the command line.  It stinks, but there's one really powerful feature that I use all the time.  It's the ability to call code from a breakpoint.  I go so far as to add functions to my libraries for printing out nicely-formatted complex data types just so they can be called from inside a dbx session."
    author: "littlenoodles"
  - subject: "Re: Why does KDevelop refuse to have a terminal view?"
    date: 2008-04-10
    body: "You are mistaken. kdevelop has a terminal view -- specifically a konsole kpart view.\n\nI use it every day."
    author: "hmmm"
  - subject: "Re: Why does KDevelop refuse to have a terminal vi"
    date: 2008-04-15
    body: "Yes, and KDevelop3 has had an integrated shell for the last 5 years.. which makes the OP's point a bit strange. I don't remember about KDevelop2.\n\nMaybe it's referring specifically to the integrated debugger? IIRC, the cli to gdb has been there for years as well."
    author: "teatime"
  - subject: "Re: Why does KDevelop refuse to have a terminal vi"
    date: 2008-04-10
    body: "on the contrary, we love command interfaces. We also prefer the amount of info a UI can display at a time, and the convenience to move views + close them when not needed. Both together is a killer combination.\n\nMy condolences on having to use dbx on AIX, I had to do that once and reverted to printf() :)"
    author: "harryF"
  - subject: "CodeBlocks vs. KDevelop"
    date: 2008-04-10
    body: "I am a big fan of KDevelop, but recently I've found that CodeClocks released a new version with multi-plataform support for Mac and Linux.\nIt's goof to finally have a decent alternative to KDevelop while the KDE4 version isn't out.\n\nPS: I came back to KDE3 beacuse of KDevelop and Quanta+, among others."
    author: "Iuri Fiedoruk"
  - subject: "Nrrg"
    date: 2008-04-11
    body: "It's a shame I don't have time to attend when I could get there in just a few hours by car. %$@#\u00a7.\n\nAnyways, have fun, and make a lot of progress on KDevelop and Quanta!"
    author: "Jakob Petsovits"
  - subject: "Maps..."
    date: 2008-04-11
    body: "OpenStreetMap has better maps of the area - it shows the tram line and where all the stops are.\n\nhttp://www.openstreetmap.org/?lat=48.13843&lon=11.54055&zoom=15&layers=B0FT\n\n(and if someone knows the area, they could add the hotel and the venue to the map as well!)"
    author: "Andy Allan"
---
The KDevelop Team Meeting Organisational Head Committee is happy to announce the agenda for Saturday and Sunday's <a href="http://dot.kde.org/1202500128/">KDevelop event</a> in Munich. While enjoying some <a href="http://labs.trolltech.com/blogs/2008/04/08/liebe-geht-durch-den-magen">breakfast</a> accompanied by a <a href="http://labs.trolltech.com/blogs/2008/04/07/free-usb-hub-with-coffee-cup-holder/">USB cup warmer</a>, you can grill Harald with all your Qt related questions, watch Aleix present a live demo of KDE 4 and finally discuss what KDevelop 4 really should be.  Sunday is reserved for in-depth KDevelop topics. Highlights include Roberto talking about language support, Andras on Quanta, Andreas on project management support, Hamish on editor integration and Vladimir on debugging. And of course there will be plenty of space for discussing your favourite KDevelop module directly with its creator.  All the details, including a visual guide on how to get there, can be found on the <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_Developer_Meeting_2008">KDevelop wiki</a>.




<!--break-->
