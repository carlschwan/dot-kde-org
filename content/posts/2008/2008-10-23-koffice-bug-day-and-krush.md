---
title: "KOffice Bug Day and Krush! "
date:    2008-10-23
authors:
  - "aspehr"
slug:    koffice-bug-day-and-krush
comments:
  - subject: "."
    date: 2008-10-23
    body: "> Krushing is a confusing term that just means looking for new bugs, and then filing bug reports\nErr..."
    author: "hlpr"
  - subject: "missing info!"
    date: 2008-10-23
    body: "Also: \"Bug Squad\" is the confusing name for the team that designs new icon themes for the KDE desktop.\n(Their latest work is the slick j/k theme.)"
    author: "Martin"
  - subject: "Improvements in KWord"
    date: 2008-10-24
    body: "I'm happy. There is a bug (wishlist ) for 4 years (2004-12-21), which is now being solved\n\nhttps://bugs.kde.org/show_bug.cgi?id=95601"
    author: "ac_z01"
  - subject: "openSuse 11 packages?"
    date: 2008-10-25
    body: "> All you need is a copy of the new KOffice Beta from your distribution\n> with the debug packages installed\n\njust tried to install Koffice 2 beta2 in opensuse11, but the only packages I found on download.opensuse.org (http://download.opensuse.org/repositories/home:/CyrilleB/openSUSE_11.0) are still version 1.9.96 (Beta2 should be 1.9.98)\n\nWould be nice to have up-to-date packages for opensuse so that users of this distro can participate in the bug day!\n\n"
    author: "Oliver"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-25
    body: "Try this: http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Extra-Apps/openSUSE_11.0/"
    author: "mactalla"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-26
    body: "mactalla, thanks for the link.\n\nNow I successfully installed Beta 2. Wanted to take part in the bug day testing Krita, but it crashes as soon I want to do something on an image. I'm tired of filing crash reports, and please, can someone tell me if there is one single action in krita (except loading an image) that does not crash it? Thats really a pity, I was hoping that there would be a useful image editor for KDE soon...\n\n"
    author: "Oliver"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-27
    body: "It's the same for me. \nThere is so much crashes in Krita that I don't know where to begin...\nIs really Krita working fine on other computers??\n\nI had the same problem with the Krita 1.6 serie."
    author: "Ro"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-27
    body: "Krita 1.6 is pretty stable, actually. There are some things that make it crash, like bad icc profiles. Now if your camera embeds a profile lcms thinks bad, and you try to use krita for your camera pictures, you will have a lot of crashes, but they should be pretty similar. I have been using Krita 1.6 myself to work on pictures taken with my camera and to draw and sketch without encountering any crashes.\n\nAs for Krita 2.0, it depends very much on what you are doing with it. Plain painting is pretty stable, as is running filters. Other things haven't been debugged well enough. Then there are distribution problems: Kubuntu notoriously has had problems with the way they compiled the jpeg import filter and OpenSUSE uses weird names for the tablet devices, which means Qt cannot pick them up unless you edit your xorg.conf by hand.\n\nThere simply are many problems we as developers never notice because our circumstances are different from yours, which is why it's important that people tell us about crashes (and other bugs) because no matter how obvious it might seem to you, we may never have seen that crash ourselves. If you're unsure whether it's something we'd like a bug report for, you can always contact us at the kimageshop@kde.org mailing list or on irc: #krita or #koffice, and we can investigate together with you.\n\nThis Sunday's bug day was wildly succesful for Krita: we got a much better handle on existing problems. Reporting bugs helps!"
    author: "Boudewijn Rempt"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-29
    body: "Ok!\n\nI'll try the beta 2 asap and and give a feed back on the mailing list.\n\nIn general, Krita was really slow on the beta 1. Is it because of some debug mode?"
    author: "Ro"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-29
    body: "Partly... There were a few unfortunate performance-guzzling bugs in the freehand painting code. Those are gone now, but they are still in beta2, as far as I know. And there is still a lot to improve performance-wise. The good news is, we're right on to it, the bad news is, krita will always be slower than an application that's hard-coded for 4 8-bit channels."
    author: "Boudewijn Rempt"
  - subject: "Re: openSuse 11 packages?"
    date: 2008-10-30
    body: "And automatic crash reporting, does it work well enough? Do you see a need to provide \"talkback\" versions that submit more detailed crash reports?"
    author: "Andre"
---
To help the upcoming final release of KOffice, Bug Squad will be having our first KOffice Day this Sunday. We will triage the old bugs, and look for new ones (krush).  We will be starting at around 07:00 UTC (be aware that summer time ends in various European countries on the 26th). As always we will be gathering in #kde-bugs on freenode IRC.  You do not need any programming knowledge. All you need is a copy of the new KOffice Beta from your distribution with the debug packages installed. This is an easy way to give back to the KDE community, and also an excellent way to get involved. 



<!--break-->
<p>Triaging just means going through the old bug reports and checking to see if they are still valid, and if they can be given new information. Often older bugs are fixed without the developers realising there is a bug report on it. Or a bug report may be poorly written, and so it gets overlooked. Triage helps to clear out and improve the bugs in the Bugzilla database. It seems simple, and it is! But it saves developers' time for coding, and is more important than you might at first think. </p>

<p>Krushing is a confusing term that just means looking for new bugs, and then filing bug reports. Developers will be standing by!</p>

<p>In particular, we will work on KWord, Krita, KChart, Karbon, KPresenter, KSpread, and more. Is one of these your favourite application? Or something you use a lot? Come join us and help make it better!</p>




