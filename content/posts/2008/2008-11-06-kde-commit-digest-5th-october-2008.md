---
title: "KDE Commit-Digest for 5th October 2008"
date:    2008-11-06
authors:
  - "dallen"
slug:    kde-commit-digest-5th-october-2008
comments:
  - subject: "Simply..."
    date: 2008-11-06
    body: "...Thank you, Danny"
    author: "Emil Sedgh"
  - subject: "Re: Simply..."
    date: 2008-11-07
    body: "Hello Danny,\n\njust don't burn out fellow. :-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Simply..."
    date: 2008-11-11
    body: "thanks danny, big appreciation from me :)\n\nvlad"
    author: "Vlad"
  - subject: "yeah-yeah-yeah?"
    date: 2008-11-06
    body: "More like \"Yeah! Yeah! Yeah!\"! A warm \"welcome back\" to Danny and the Digests :)"
    author: "Anon"
  - subject: "One Heck of a Week"
    date: 2008-11-06
    body: "That sounds like one heck of a week for commits."
    author: "T. J. Brumfield"
  - subject: "My favourite commit.."
    date: 2008-11-06
    body: "Thanks Danny!  \n\nAnd my favourite commit this time around is:\n>> Don't load mimetype icons at startup, they decrease the startup time by 6-10x\n\nThanks Harald Sitter for that.  I've always wondered why a simple dialog like kfind sometimes takes ages to load (so long that I sometimes think its not going to happen at all and try to launch it again)."
    author: "Leo S"
  - subject: "thanks"
    date: 2008-11-06
    body: "thanks for the digest"
    author: "R. J."
  - subject: "plasmoids"
    date: 2008-11-06
    body: "> Support for image file previews in the \"FolderView\" Plasmoid\n\nI'm a bit confused about the difference between plasma and regular applications. Can FolderView not be implemented by embedding the dolphin folderview kpart into a plasmoid? From the way things are written, it sounds like plasmoids need to be programmed from scratch and are different to kparts in some way. I'm sure this isn't the case, but I don't understand why plasmoids could not just be implemented using window manager like features e.g. plasmoids could jut be regular apps with the title/menu bar turned off with some transparency. "
    author: "Bob"
  - subject: "Re: plasmoids"
    date: 2008-11-06
    body: "YES, that would be great!\n\nAnd we would get rid of so much trouble we always have with these plasmoids.\nInstability of one would not affect any other. The common themes and colors would apply for those too (if not particularly overwritten) - leading to a badly missed consistently look and feel ... The longer I think about it, the more advantages coming to my mind.\n\nThat really sounds like a CHANCE!"
    author: "Christian K."
  - subject: "Re: plasmoids"
    date: 2008-11-06
    body: "Actually, I thought that FolderView did use Dolphin to view the folder.  It simply needed to hook the UI in.\n\nAs for a rewrite, it won't happen. For one, libplasma has been moved into KDELibs, so binary compatibility needs to be maintained. Secondly, Plasma needs to be able to work with any window manager, and not just with KWin. Thirdly, Plasma needs to work on many use-cases not related to the current desktop. Fourthly, it'd be difficult work with very little benefit."
    author: "Michael \"It does\" Howell"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "\"Actually, I thought that FolderView did use Dolphin to view the folder\"\n\nNope :)"
    author: "Anon"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "Nope, but they do use 'libkonq' for the context menu's."
    author: "Diederik van der Boor"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "> I'm a bit confused about the difference between plasma and regular applications\n\n\"regular\" applications are stand-alone applications with top-level windows of then own and tend to use primarly or only 'native' widgets with most of the UI pretty well hardcoded. plasma uses a canvas based approach and is highly modular allowing for the creation of various designs out of small building blocks; amarok's context view is a non-desktop example of this.\n\n> Can FolderView not be implemented by embedding the dolphin folderview kpart into a plasmoid?\n\nit could, and it would suck in many various ways. from how it is rendered to the interaction mechanics, it's just not \"perfectly\" suited. \n\nin kde1, kfm did the desktop icons (like nautilus does today in gnome); in kde2 (and kde3) it was broken out into its own application called \"kdesktop\" which was basically a specialized, but somewhat limited, file manager for the ~/Desktop folder; in kde4 we have something much closer to an actual file manager, but which goes for a more modular approach over all, where the listing is just one component but still deisgned for the \"on the desktop\" use case, which something like dolphin isn't.\n\nthe good news is they share a lot of the same code, just repurposed for the specific use cases.\n\nin the case of file previews, folderview uses the KFilePreviewGenerator class for that .. the same class that Dolphin, Konqueror and the file dialog uses.\n\nthe real question is getting the code sharing going at the right level, such that we aren't duplicating code, ending up with featureless UIs but also have interfaces that well tuned for their use cases.\n\n\" I don't understand why plasmoids could not just be implemented using window manager like features e.g. plasmoids could jut be regular apps with the title/menu bar turned off with some transparency.\"\n\nyou could. and then you'd have a poorly interacting, difficult to manage, hugely resource consuming system that requires far more code for each component to be written.\n\nit would also make things like coordinating activities, zooming and all that other stuff not only a lot more difficult but *require* a decent compositor and for the controlling app to do all the management. basically, it would require smashing much of plasma into kwin. both are already complex enough as it is.\n\nso, in summary, we would lose visual integrity, developer simplicity and performance with what you suggest.\n\nnot exactly a great idea in my books."
    author: "Aaron Seigo"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "Hello Aaron,\n\nI agree with you, Plasmoids seem preferable with a more optimized UI.\n\nStill, in the absence of such UI, why not view single or tiles of documents inside a special purpose Plasmoid that loads KParts. Like I would have a pile of documents that I currently work on, one that I have to review and one that was released a few days ago.\n\nDoing that with Nepokuk tags and queries, it would be a nice way to organize my work with that. I guess, I may even give up on KMail for mail reading with an interface like that.\n\nHow far are the Python bindings to Plasma with 4.2 anyway? Would I be able to bridge KParts and Plasmoid from scripting now? Can I make my own containment with it too?\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "> How far are the Python bindings to Plasma with 4.2 anyway? \n\ncomplete, working and in kdebase/workspace.\n\n> Would I be able to bridge KParts and Plasmoid from scripting now?\n\nshould be able to, yes, using the full PyKDE bindings and a QGraphicsProxyWidget (which lets you stuff a random QWidget onto the canvas)\n\n> Can I make my own containment with it too?\n\nIf you can make a plasmoid out of it, you can make a containment out of it."
    author: "Aaron Seigo"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "Hello Aaron,\n\nthanks, I shall try it. To merge Plasma, Nepomuk and KParts will be very exciting.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "Not 100% related, but... I feel that the folderview-plasmoid is great in theory, but it doesn't go far enough. I mean that if I open a folder in folderview, it gets opened in Dolphin. when I first did that, it felt \"unsmooth\" and jarring, since I was expecting it to be opened inside folderview itself. Opening it in Dolphin (which requires Dolphin to be launched, and brand-new window appearing on the desktop) causes a delay, and requires me to move my attention from Folderview to Dolphin, disrupting my workflow.\n\nYes, I filed a bugreport(* about it, but not much seems to have happened.\n\n* = https://bugs.kde.org/show_bug.cgi?id=171889"
    author: "Janne"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "I think that's the way it is supposed to work. When you opened a folder on the old desktop (kdesktop) the file manager was opened too. The difference now is that you can have a huge number of  folders shown on the desktop, not only de ~/Desktop. The plasmoid you want is kinda a filemanager plasmoid, wich folderview isn't. (I think it possible to do with plasma too)."
    author: "SVG Crazy"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "\"I think that's the way it is supposed to work.\"\n\nI know, and I feel that it's wrong. Using Dolphin breaks the workflow, and introduces a delay, since it takes some time for Dolphin to launch.\n\n\"The plasmoid you want is kinda a filemanager plasmoid, wich folderview isn't. (I think it possible to do with plasma too).\"\n\nFolderview already kinda is a filemanager. I mean, you can do basic filemanagement with it. I fail to see why opening folder inside folderview is suddenly an \"advanced\" filemanagement-feature that should not be there.\n\nSince Folderview immediately launches Dolphin when you open a folder, then what's the point of having Folderview? Why not bypass it completely and use Dolphin instead? I mean, you would probably end up in Dolphin in any case.\n\nOf course it makes sense that actual _files_ are opened inside relevant apps, but I don't understand why folderview couldn't handle opening of folders.\n\nQuickaccess-plasmoid CAN open folders inside the plasmoid, and it feels very natural way of working. So it could be done. When folderview launches plasmoid, it feels unexpected, unnatural and jarring."
    author: "Janne"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: ">>>Since Folderview immediately launches Dolphin when you open a folder, then what's the point of having Folderview? Why not bypass it completely and use Dolphin instead? I mean, you would probably end up in Dolphin in any case.\n\nThe point of using folderview is basically the same point of having a file on ~/Desktop file on KDE 3, showing files on the desktop.\n\nOn KDE 3, we had a ~/Desktop view on the desktop, now, with folderview, we have this feature extended!! We can have multiple views on the Desktop. This is very useful when you work, for instance, with some files on your $HOME/Project folder and have to upload it on a Network folder. You could have two folderviews (one for the project file, another for the network) and copy the file from one view to another just dragging and dropping the file! This is just one of the possibilities.. I can see many others using Nepomuk, for instance.\n\nFolderview is a new and extended way of getting a view on the Desktop.. on KDe 3.. we could only have ~/Desktop view on desktop.. on KDE 4.2, it will be possible to have this old way of doing things (like kdesktop) and the new way.. using multiple folderviews...\n\nFolderview, IMHO, isn't a filemanager, although it can do some of the things filemanagers do (like copy, move delete files/folders). It is exactly what the name suggests, a view."
    author: "SVG Crazy"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: ">>The point of using folderview is basically the same point of having a file on ~/Desktop file on KDE 3, showing files on the desktop.\n\nJust to make it more complete.. The point of using folderview is basically the same point of having a file on ~/Desktop file on KDE 3, showing files/folders on the desktop."
    author: "SVG Crazy"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "\"The point of using folderview is basically the same point of having a file on ~/Desktop file on KDE 3, showing files/folders on the desktop.\"\n\nIf that was it's point, we would still be usign the old system. But we are not. We are using folderview so we could have something better.\n\nBesides: if folderview could open folders inside folderview, it would still be displaying contents of that folder, would it not?\n\nIt really seems to me that people are arguing against this suggestion because of dogma. That is, they are saying \"folderview should not do that because it's meant to simply display files and folders\". Should we tie our hands because of some narrow definition? No, we should work to make each and every piece of KDE as good as possible. Extending functionality of folderview in this manner would not make it worse, it would make it better. So why shouldn't it be done? Because of dogma that says \"folderview is only supposed to show contents of a folder\"?"
    author: "Janne"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "The idea of Folder View is not to have an Plasmoid what allows you to move inside of folders and so on. It's idea is to open the files (folders are files too) with correct applications.\n\nIt would be stupid to click folder and get it opened on folder view and not on filemanager, because when you open a PDF or text file, you get it opened with correct application.\n\nThe folder view is not only for filemanagement. It is for filtering files and when integrating with nepomuk, it filters just files or stuff what you want. So you dont always see folders with it.\n\nFolder View is \"Window\" to files, it shows what you want it to show. To give you a easy access for wanted files, so you dont need to search and track them first. \n\n\nI like the way that Dolphin gets opened. I hate the Kubuntus quicklaunch because it does not open the wanted folder to dolphin, but I need first click wanted folder and then \"Open\". I want to get filemanager opened fast (If I dont have such already!). "
    author: "Mikko"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "\"The point of using folderview is basically the same point of having a file on ~/Desktop file on KDE 3, showing files on the desktop.\"\n\nThe point of using folderview is to give us a system that is more powerful and flexible than traditional \"files on the desktop\" is.\n\n\"On KDE 3, we had a ~/Desktop view on the desktop, now, with folderview, we have this feature extended!! \"\n\nYes we do. So? Does that mean that further improvements are not needed? And why do you feel that you need to tell me about the greatness of having several folderviews running? What does any of that stuff have to do with my suggestion?\n\n\"Folderview is a new and extended way of getting a view on the Desktop.. on KDe 3.. we could only have ~/Desktop view on desktop.. on KDE 4.2, it will be possible to have this old way of doing things (like kdesktop) and the new way.. using multiple folderviews...\"\n\nThat has nothing to do with my suggestion.... Why are you telling me the greatness of having several folderviews when I suggest that we should be able to open folders inside folderview? It has nothing to do with the subject at hand.\n\n\"Folderview, IMHO, isn't a filemanager\"\n\nIt already does some filemanagement-stuff. Why shouldn't it do some more (that is: open folders)? How exactly would it make folderview WORSE?"
    author: "Janne"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: ">>That has nothing to do with my suggestion.... Why are you telling me the greatness of having several folderviews when I suggest that we should be able to open folders inside folderview? It has nothing to do with the subject at hand.\n\nWell, all that explanation was because you asked what's the point of folderview if you can't browse files.... I just answered you question. \n\n\n>>It already does some filemanagement-stuff. Why shouldn't it do some more (that is: open folders)? How exactly would it make folderview WORSE?\n\n\nMy opinion is that your suggestion would bring some complication to folderview. If you want to open a folder inside itself, there MUST be a way to come back to beginning. Make it automatically doesn't solve the problem because it can really be confusing (how much time to wait to come back to previous folder? and if I don't want to come back to previous folder at all?). It would have to be solved with some navigation widgets (like trees, breadcrumbs, or something like that). But folderview is a view, it is not intended to navigate. \n\nThis is just one of the problems. On 4.2, as I said before, there will be an option to make folderview behave like the old kdestop (making folderview be the desktop itself). Imagine the mess it would be to click a folder on this desktop and not opening the file manager!! Navigate through the desktop itself wait some minutes and come back automatically to the first folder? Where is that file that was where on my desktop some minutes ago? it's gone???? It would be VERY confusing!!!\n\nI think that maybe a file managemer plasmoid could be done.. why not? There's a mini web browser plasmoid already there.. why not a mini file manager?  (In fact, probably it can be done with plasma and dolphins kpart, who knows!).But that's not folder view is about.\n\nThe \"problem\" here is the concept. Folderview is a view, not a mini file manager/browser.\n\nWhat do you expect when you click on a txt file? Kwrites opens... an image? Gwenview opens... an svg file? Inkscape opens... Where is the \"jarring\" about that?... The same thing with folders... Dolphin opens. It is the way things work... just that.\n\n"
    author: "SVG Crazy"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "\"My opinion is that your suggestion would bring some complication to folderview.\"\n\nNot really. It would be one checkbox in settings (\"Open folders inside folderview yes/no\"). In the actual folderview there could either be a button that takes you back to the beginning, or it could be done automatically after few seconds of inactivity.\n\n\"On 4.2, as I said before, there will be an option to make folderview behave like the old kdestop (making folderview be the desktop itself). Imagine the mess it would be to click a folder on this desktop and not opening the file manager!\"\n\nThat's a question of implementation. It could very well be that if the folderview is used as a containment, it would use filemanager instead. Why? Because that makes sense. This is about what makes sense.\n\n\"I think that maybe a file managemer plasmoid could be done.. why not?\"\n\nWhat would be the point if we already have folderview, that does 95% of what is needed? I have no need for a \"filemanager-plasmoid\". I just want an existing plasmoid to behave in a manner that makes sense and would actually make the plasmoid better.\n\nAnd by your logic, we aleady have a filemanager-plasmoid: Quick Access, since quickaccess can handle the hi-end task of opening folders...\n\n\"The \"problem\" here is the concept. Folderview is a view, not a mini file manager/browser.\"\n\nIn other words, the problem is dogmatism. You think that Folderview should not have this feature, not because it wouldn't work or something like that, but because \"folderview is supposed to be a view, not a mini-filemanager\".\n\nHad KDE-project itself behaved like that, we would not have KDE running in Nokia's internet tablet, because KDE's mission-statement (which has since been altered) said \"KDE creates desktop-software for UNIX-like OS'es\", and internet tablet is not a desktop...\n\n\"What do you expect when you click on a txt file? Kwrites opens... an image? Gwenview opens... an svg file? Inkscape opens... Where is the \"jarring\" about that?... The same thing with folders... Dolphin opens. It is the way things work... just that.\"\n\nThere's a difference between files and folders. Folderview is obviously capable of displaying contents of a folder. So when I open a folder in folderview, I obviously expect it to open inside folderview, since that is clearly something folderview can do. Like I said, Quick Access plasmoid does practically same thing as folderview does, and it knows how to open folders inside itself. What's the difference?\n\nWhen user clicks on a file (a .jpg for example) he already expects it to open in an application. When he's inside an app that handles folders (like folderview does) he usually expects them to opened inside that app. This is somewhat similar to opening a folder inside Dolphin, and having Dolphin open a new Dolphin-window for the folder...\n\nI have yet so see any technical reasons why this features should no be in there. It would not make the plasmoid worse in any way. All I hear \"this is the way it works.\" or \"it's just a view\"."
    author: "Janne"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "One could use the same reasoning to demand that Dolphin becomes an all-purpose, KPart'd file viewer/web browser and that it would be \"Dogma\" to say that doing what it does is just the way it works. Dolphin does what it does, Konq can serve the same purpose and more.\n\nSimilarly, Folderview serves a purpose (displaying a folder on your desktop), and a file manager displays folders and more."
    author: "Jonathan Thomas"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "\"How exactly would it make folderview WORSE?\"\n\nIf you open a folder, you would have to go back (several levels if you do file management in the plasmoid) if you want to see the desktop. E. g. sometimes the user opens the Documents folder, sometimes starts an application... if the documents folder was opened right in the plasmoid, they would have to go back whenever they want to start an application from the desktop, while they otherwise might want to have a folder inside Documents open all the time."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "The point of folderview is to give quick access, from the desktop, to (a limited) amount of files(or folders). It's not supposed to be a filemanger or file browser. The limited filemanger capabilitieas are only to preform the task taht comes natural for files on the desktop, like open, delete, rename, copy or move plus the ability to arrange the files. "
    author: "Morty"
  - subject: "Re: plasmoids"
    date: 2008-11-07
    body: "You are tied to dogma. You think that \"because fodlerview is meant to display contents of a folder, it must not actually open folders\". Well, why not? Because of that narrow definition?\n\nQuick Access plasmoid handles folders just fine, why couldn't folderview handle them as well? Is that difference justified because of some dogma that says that folderview must not open folders?\n\nOr is this because of some weird sense of \"competition\", which says that fodlerview must not threaten Dolphin in the filemanagement-area in any shape or form? So we should cripple folderview, so Dolphin gets used more?\n\nIf folderview could handle opening of folders, it would not make it one bit worse in any shape or form. And we should be striving to make each and every component of KDE as good as it could be. Crippling software so people would use some \"fully featured\" version instead is done with proprietary software, not, free software. If we can make folderview more fucntional with no drawbacks, then why shouldn't we do it?\n\n\"The limited filemanger capabilitieas are only to preform the task taht comes natural for files on the desktop, like open, delete, rename, copy or move plus the ability to arrange the files.\"\n\nTo me, opening a folder inside the folderview is very, very natural. The fact that it opens Dolphin instead is unnatural and jarring.\n\nBesides, folderview is supposed to bring us new, improved, flexible and powerful way of handling files on the desktop. And now you tell me that the functionality of the folderview should be limited so it would be more similar to the way traditional desktop handles files? Well, folderview is not \"traditional desktop\", it's something new and better."
    author: "Janne"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: ">>Or is this because of some weird sense of \"competition\", which says that fodlerview must not threaten Dolphin in the filemanagement-area in any shape or form? So we should cripple folderview, so Dolphin gets used more?\n\nC'mon, threaten Dolphin??? File management is much more than navigation through folder. I think nobody on KDE team has this \"weird sense of competition\" you say...\n\nAnyway, I think what you want is a mini file management plasmoid, and I think that it can indeed be done (and would be great). But that's not folderview. \n\nFolderview in concept is a containment that shows files and it must be \"static\" because this containment can be the desktop itself. There's no dogma at all (it is the concept). In fact, plasma is the most \"mutant\" and flexible thing I ever saw on a desktop, a plasmoid can be done with just some lines of code... Well, the explanation and my point of view is explained on the last response I gave you. I'm done here.. \n\npeace and love!"
    author: "SVG Crazy"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "I think your are right, folderview should remain as it is, but it would be interesting to have a plasmoid that also does file managment. We already have alternative panels Why not have alternative folderviews?"
    author: "Raul"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "I agree with you and that\u00b4s what I am trying to explain that a view is something \"less\" than a browser, so folderview is not intended to be a browser. File manager plasmoid would have this responsability (and actually, I think it can be done). "
    author: "SVG Crazy"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "+1\n\nTo me this seems like a very reasonable 'feature'. "
    author: "The Dude"
  - subject: "Re: plasmoids"
    date: 2008-11-08
    body: "I'd also like to see this.  I can't quite put my finger on why it doesn't feel like it's missing in kdesktop, but in KDE4 the behaviour definitely does feel \"jarring\", as you say."
    author: "Anon"
  - subject: "KDE hurts me."
    date: 2008-11-06
    body: "I read about all these cool features and all the goodness that comes and I'm no longer allowed to use KDE at work. It pains me but the exchange connectivity really is a make or break kind of thing for me. And for many others I'm sure.\n\nSadly KDE is no longer my primary desktop. Now I'll have to endure XP instead. KDE will however keep it's place in my home environment.\n\nBut how I'd love an application (KDE-PIM) that could talk MAPI to exchange and free my laptop from Microsoft. Going to XP feels like stepping back in time, as well as in features. :-(\n\nSorry for being such a wet blanket. "
    author: "Oscar"
  - subject: "Re: KDE hurts me."
    date: 2008-11-06
    body: "We're facing the same problem. Although often the community does not see the importance of the issue: Exchange connectivity is *very* important for many companies. At out company it is also blocking the transition to 'Linux-only workstations'.\n\nI sincerely hope that KDE-PIM will get full working Exchange connectivity with the work that is being done on the OpenChange Akonadi back-end. Can someone give a status report?\n\nRegards, Harry"
    author: "Harry"
  - subject: "Re: KDE hurts me."
    date: 2008-11-06
    body: "Thats why I have to use Kontact3.x on a KDE4 desktop at work. But opening attachments always uses KDE3 apps in favor to KDE4 apps (Kubuntu 8.04)\n\nBye\n\n   Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE hurts me."
    date: 2008-11-07
    body: "If you are making the change to linux only work stations, start from the server side. There are two thins I see. Exchange has IMAP support. Outlook can connect to IMAP. So you could either install a nix based IMAP server and have the Outlook zombies connect to that and use publish iCal calendars. Or you could enable IMAP support on your Exchange server and have Kmail connect to it. There are ways to do it that don't require some fancy Exchange replacement/support. It's usually just the IT department is too scared to roll out something new. So they keep paying the MS dues and the boss wonders what is going on. It's like that it almost every IT department I've worked in. When I suggest we enable IMAP support for flexibility reasons, I am met with fear and trembling."
    author: "winter"
  - subject: "Re: KDE hurts me."
    date: 2008-11-07
    body: "100% agree... Still today (sadly) it's way easier to migrate servers than client, and when you migrated serers to not-(so)locked-in-platforms, you can easily migrate clients, if you want. Use Zimbra to achieve almost 99% of Excahnge features, for example, if you want something easy to install, comemrcially supported and paying license fees is not a problem (and it shouldn't, you are using Exchange after all). Users can then just use Outlook as they did or use whatever other client supporting well-known standards"
    author: "Vide"
  - subject: "Re: KDE hurts me."
    date: 2008-11-07
    body: "If it's actually important for the company, then pay someone to work on mapi support for Akonadi. "
    author: "Pascal"
  - subject: "Re: KDE hurts me."
    date: 2008-11-07
    body: "It's not important to the company at all. They've standardized on XP and most people are content with that. I who used KDE on my previous job would love to keep doing that, but alas, I have to conform and use the tool I'm given. I can hardly ask them to modify their infrastructure to fit my needs.\n\nIf however my infrastructure would seamlessly integrate into theirs then it would be a different matter entirely.\n\nAnd I'm not rich enough to pay a developer. I do donate some money to KDE and related projects every now and then, but that's as much as I can do.\n\nBut I keep hearing about exchange connectivity from linux applications and I haven't given up hope."
    author: "Oscar"
  - subject: "Re: KDE hurts me."
    date: 2008-11-07
    body: ">> It pains me but the exchange connectivity really is a make or break kind of thing for me. And for many others I'm sure.\n\nI have the freedom to run Linux at work, and we run an exchange 2007 server.  I used to run Outlook 2007 in VirtualBox, but after CrossOver Office gave away their software for free, I'm now running Outlook 2007 in Linux directly.  Works great!"
    author: "Leo S"
  - subject: "Re: KDE hurts me."
    date: 2008-11-07
    body: "Run Windows in a VirtualBox. I do it and it's great.\n"
    author: "Jos"
  - subject: "Re: KDE hurts me."
    date: 2008-11-12
    body: "Stop using Exchange! Really. Everytime someone sets up an Exchange server got kills a puppy. :(\n\nBy using Exchange corporations are keeping themselves locked into Windows. A better solution is to use Zimbra. Really my organization uses Zimbra with 30,000 clients (a University). It works great and it has even more features then Exchange does, like a wiki."
    author: "Jon"
  - subject: "Music"
    date: 2008-11-06
    body: "\"Support for playing music during a slideshow in kipi-plugins (used by Digikam, etc)\"\n\nWhat? Great I already love it before I have seen it. Somewhere I read about fancy 2D and 3D transitions and now music support. Seems like the kippi using tools will do anything one would expect from a kool presenter tool. Keep up the excellent work.\n\n------------\nDanny, thanks for providing this great news!\n------------\n\nBTW, does anybody know if there is any work done at the grammar and spell check front (Sonnet ...)?"
    author: "Holger F."
  - subject: "Re: Music"
    date: 2008-11-07
    body: "The spell checker is working pretty decently.\nThe grammar checker will depend on some external project being able to do decent work. So far seems that there are no plans for it. So I don't think there will anything there in 4.2 or 4.3\n"
    author: "Jordi"
  - subject: "Re: Music"
    date: 2008-11-07
    body: "To bad. But thanks for the information. If I remember correctly the developer of OOos LanguageTool once used to code for KOffice. To he isn't anylonger."
    author: "Holger F."
  - subject: "konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-06
    body: "*me* wonders.\nSince such a long time I have problems with standard konqi and flash. Sometimes a site works, but on a reload the nspluginviewer crashs.\nOTOH using kmplayer *for me* leads to rock solid flash experience in konqi - also with KDE4 using kmplayer-0.11.rc4.\nWhy is knpplayer(that uses the same Adobe flashlib) not standard? Why is nspluginviewer so unstable?\n\nBye\n\n  Thorsten\n\nReference:\nhttp://mikearthur.co.uk/2007/12/konqueror-with-latest-adobe-flash-howto/\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-07
    body: "This might have to do with your distro. I'm not 100% sure though. Yes, nspluginviewer just not working with KDE4 Konqi for many sites."
    author: "winter"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-07
    body: "Interesting, thanks for the link. Never heard of it and right now I am having a bag of trouble with Flash on Firefox (it crashes my systeem after a few minutes of viewing videos) so if that really works then I will finally have a good reason to switch to Konqueror."
    author: "Bobby"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-07
    body: ">  Why is nspluginviewer so unstable?\n \nGenerally[1]: because the close-sourced flash plugin likes crashing with it. Why? It's impossible to know. Doing anything with flash is basically an exercise in trial-and-error. Why does npplayer work better? I can't answer either; my guess is because it's a Gtk application, and not a Qt one, so it works more like Mozilla in some subtle way.\n\n[1] Though it's possible to break it by doing things like using Qt3 version of gtk-Qt style.\n"
    author: "SadEagle"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-07
    body: "Is the flash-plug-in a Qt application? Are Gtk apps more stable than their Qt counterparts and why?\n\nMy guess is that flash crashes because it's closed source and we can't see the bugs. Konqueror as a KDE Qt-application is definitely more stable than the Gtk-Fox but I use the Fox because I like it more as a browser and it's much faster than Konqueror. "
    author: "Bobby"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-08
    body: "The Linux version of Flash uses GTK."
    author: "Anon"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-08
    body: "Thanks, so actually is't Gtk that's weak link and not Qt ;)"
    author: "Bobby"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-09
    body: "The problem is not the toolkit used, the problem is the fact that the Flash plug-in is closed source and Adobe offers near zero documentation on how it interfaces with the Netscape plug-in API. So right now there's a blackbox Flash plug-in which is only ensured to work with Firefox, while any other browsers supporting the Netscape plug-in API (not only Konqueror but also Opera and the webkit based ones) need to catch up by trial and error, looking which obscure usage of XEmbed or the likes makes a newer version of Adobe Flash crash everywhere except in Firefox.\n\nIt's very shameless of Adobe in this context to claim of having \"open sourced\" the Flash/SWF format while continuing offering its implementation as closed source only."
    author: "Anon"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-09
    body: "Actually I have to disagree with your claim that Flash doesn't crash on Firefox. Right now I am having a hell of a time trying to view a 5 minutes long flash video on Firefox without getting a crash. The problem is that it not only brings down Firefox but my whole system! I get a \"no signal\" on my monitor and a dead keyboard. This is a nightmare, believe me. Even Opera and Konqueror manage to play a longer Flash video on my machine than Firefox. Although I am not sure if it's the Intel GMA chip to be blamed or Adobe."
    author: "Bobby"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-10
    body: "In a protected environment such as a unix system, it should be impossible for any user process to crash the system. So basically, you're hitting a driver or hardware bug (probably the former)."
    author: "slacker"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-11
    body: "That's what I was always told and to be frank, I never experienced something like this since I am using Linux. What's killing me is that I don't know where the problem really is. I was wondering if it's a hardware flaw/defect but if that were the case then the video card shouldn't be working at all. Then I thought that the problem lies with openSuse 11 so I installed KUbuntu and later openSuse 11.1 Beta 4 but I ended up getting the same results after using the flash-plugin.\nI filed a bug report at openSuse but it's closed due to a lack of response so it seems like I am the only one having such problem.\nI was told to adjust the xorg.conf file by adding the EXA line but that actually made matters worst so I had to reverse the changes. \nWhat I have noticed however is that it takes longer to crash if I am not using compositing.\nIf it's a driver problem then how can I get it solved? "
    author: "Bobby"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-11
    body: "Newer Adope Falsh plug-ins offer \"hardware acceleration\" and that seems to be enabled by default, maybe that crashes it for you?"
    author: "Anon"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-12
    body: "I came a step further Anon. It wasn't openSuse, it wasn't Plasma, KDE or Gnome to be blamed. It wasn't even the flash plug-in nor the Intel GMA. The problem was the CPU. The CPU was overheating due to one of the fan pins that wasn't proberly tightened. The technician did the necessary adjustments and now it looks like everthing is OK.\nI would still like to say thanks for all the suggestions and help :)"
    author: "Bobby"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-14
    body: "I see, glad you found and fixed that. =) I find it quite telling though that apparently running a Flash plugin is the most processing intensive activity you seem to have on your system in case you really didn't notice that overheating effect anywhere else."
    author: "Anon"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-14
    body: "For whatever reason, sites I usually go to all have working flash for me. I run trunk, but I guess my flash version is a bit old. When you make bug reports, give your flash version.\n\nSites that don't have working flash are the ones that get bugreported. ;)"
    author: "blauzahl"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-14
    body: "For whatever reason, sites I usually go to all have working flash for me. I run trunk, but I guess my flash version is a bit old. When you make bug reports, give your flash version.\n\nSites that don't have working flash are the ones that get bugreported. ;)"
    author: "blauzahl"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-14
    body: "For whatever reason, sites I usually go to all have working flash for me. I run trunk, but I guess my flash version is a bit old. When you make bug reports, give your flash version.\n\nSites that don't have working flash are the ones that get bugreported. ;)"
    author: "blauzahl"
  - subject: "Re: konqi and flash: nspluginviewer vs knpplayer"
    date: 2008-11-14
    body: "For whatever reason, sites I usually go to all have working flash for me. I run trunk, but I guess my flash version is a bit old. When you make bug reports, give your flash version.\n\nSites that don't have working flash are the ones that get bugreported. ;)"
    author: "blauzahl"
  - subject: "KDE on Windows with X11"
    date: 2008-11-07
    body: "Related to the problem of not being able to use KDE because of the need of Windows:\nWe can hear much about compiling KDE apps (but not the DE) for Windows with Qt/Windows. Is it currently possible to compile KDE including Plasma, KWin, etc. for Windows using Qt/X11 on CygWin and running it in an X server on Windows? With *nix-specific features removed it could be possible."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-07
    body: "No, cygwin is not supported.\nAt least I know nobody who tried, and I also think it doesn't make a lot of sense, since you can run the stuff natively.\n\nAlex\n"
    author: "alex"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-07
    body: "\"you can run the stuff natively\"\n\nIf you run it natively, you have to use the lame Windows window manager, a crap compared to KWin."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-08
    body: "I agree with you 100%. I tried KDE on Windows XP a few weeks ago (out of curiosity) and I also think that it doesn't make any sense if I can't use all the components of KDE (on top of the Windows Kernel) so that I can enjoy all the goodness of KWin and Plasma. That would really breath some life in the aged Windows instead of pouring new wine into an old bottle."
    author: "Bobby"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-10
    body: "Actually, I believe the best way of using windows apps _IS_ pouring new Wine into the old unix bottle..."
    author: "slacker"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-07
    body: "I saw on the Planet someone working on porting Plasma to Windows.  From what I recall Aaron said he wouldn't do it himself, but he wouldn't stop anyone else from doing it."
    author: "T. J. Brumfield"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-08
    body: "that is correct; some people have even gotten the Plasma desktop shell working as a Windows shell replacement."
    author: "Aaron Seigo"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-10
    body: "It is possible to run many applications natively. I tried the packages a few weeks ago.\n\nBUT: ...!\n\nKDE was adjusted for use with UNIX-like systems. You may see that you can not access any harddrive except C:\\, you will see that \"My Computer\" of MS Windows is not accessible. There is no support for Windows file \"links\" (which are special files on Windows)\n\nThat speaking: KWrite/Kate do really work good as long as you edit a file on C:\\\nThe others applications can start, but do not create any benefit."
    author: "Sebastian"
  - subject: "Re: KDE on Windows with X11"
    date: 2008-11-10
    body: "Then the packages you tried must have been broken, or something wrong in your installation.\n\nI just tested and had no problem opening a file from a non c:\\ location and saving it to a networked drive also not c:\\. I also get access to stuff like My Documents, My Recent Documnets, Desktop and whatever else you usually get in a windows file dialog."
    author: "Morty"
  - subject: "What's PIMO"
    date: 2008-11-07
    body: "So what's the PIMO concept?"
    author: "debianuser"
  - subject: "Re: What's PIMO"
    date: 2008-11-07
    body: "http://www.semanticdesktop.org/ontologies/2007/11/01/pimo/\n\nPersonal Information Model (PIMO)\n\nDerek (found it by looking at the code diff)"
    author: "dkite"
  - subject: "Re: What's PIMO"
    date: 2008-11-07
    body: "This is starting to get interesting, essentially PIMO is the class structures for keeping track of what you do, where you are, who you are, the tasks that you are working on.\n\nA file could be tagged with all that.\n\nNeat.\n\nDerek (read the link above. All preliminary of course)"
    author: "dkite"
  - subject: "Re: What's PIMO"
    date: 2008-11-07
    body: "\"keeping track of what you do, where you are, who you are, the tasks that you are working on.\"\n\nScary."
    author: "yman"
  - subject: "Re: What's PIMO"
    date: 2008-11-07
    body: "Maybe if it is public.\n\nBut I keep a diary of all those things during my work day.\n\nThe other day I was working on a spreadsheet, referring to a group of web pages and emailing a contact, all to do with a particular project. The work continues, and will over the next 6 months. In addition to the handwritten notes, drawings etc. All this stuff has in common one word that describes the project. \n\nIt would be very nice to have available a history of everything to do with the project, an easy way to see each discrete element of that history, so that when I sit down next time I don't have to collect the disparate bits all over again.\n\nTo have the whole thing automatically be tracked if I do it in a plasma activity, and readily available when I open that activity would be extremely useful.\n\nPIMO only describes the categories of data that can potentially be stored. How it gets there and in what detail, available to whom are other issues entirely.\n\nDerek"
    author: "dkite"
  - subject: "Re: What's PIMO"
    date: 2008-11-07
    body: "PIMO is your personal vision of the world. \nYou have a Think called YYY who happens to be a person and to have a email address whatever@whoknows. \n\nThis is your vision of the world regarding YYY"
    author: "Jordi"
  - subject: "Buddy icons in Kopete "
    date: 2008-11-07
    body: "hello..I wrote many times about same thing..In yahoo protocol kopete don't display buddy icons, don't work file transfers and don't work webcam no send no receive..I upgrade to 4.1.3 in hope to change something but all is the same...Could tell me somebody if is in plan to repair this defects?or kopete team consider this is normal...all the best!"
    author: "neaghi"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-07
    body: "I must to say, I see it's a lot of work for things who I don't use, even worse..I don't know at least what it's is this things..and some basics who everybody see and use,like buddy icons, file transfers, webcam..nobody care..what to talk about another feature like audio.."
    author: "neaghi"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-07
    body: "I don't use yahoo protocol so I just don't care\nAnyway I remember I used it some time ago and the webcam was working perfectly.\n "
    author: "Jordi"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-07
    body: "Have you filed bug reports?"
    author: "Anon"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-07
    body: "no, I don't filled because I consider is a basic things who everybody see, including devs-people...it's not an extra feature or something.And for those who don't care about, maybe for they is good enouh a text IM, but in my country is most used yahoo..all my friends have acccounts on it, and for us is a progres to see us, to hear, and this is a progres not a regres..In this moment every people I try to switch from Widowz to linux, stop when I tell her \" for moment in linux dont't exist capability of audio-video in yahoo...\" If somebody want to make a change, want something better not worse..or the big majority don't understand the background advantage of linux..and don't care about..im my country most people have Win and programs cracked..so is not a matter of money..:P..In this context missing feature like what I say it's not understandable...and , yes in 3.5 kopete work fine..Anyway if kopete or pidgin..who is faster.:) .implemented audio and video..beside buddy icons&so..I see a great movement from Win to Linux..for big majority..till then people stay with XP and fight with viruses, trojans..& co..So if community is interest to grow home-users not only specialists, must to fullfill needs of them...\nok example..my friend get drunk last night and want to show this everybody ...or buy a new car..:)..what is most quick? send a pic to everybody from list or put a picture in avatar?Or...my friend don't know english  and see an eror message on the screen..I ask him to tell what write here..and he give me the web and i see with my eyes...or write by hand a couple of rows..and posibility to wrong is big enough...and some examples can continue.."
    author: "neaghi"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-07
    body: "\"no, I don't filled because I consider is a basic things who everybody see, including devs-people.\"\n\nWell, that's where you're wrong - what if none of the devs personally use Yahoo? What if they're too busy work on some other protocol to test to see if Yahoo still works?\n\nI'm reminded of the time when everyone was complaining about the fact that Digg was clearly broken in Khtml, and no one actually bothered to report it because it was so \"obviously\" broken.  What they didn't realise, of course, is that none of the Khtml devs (quite sensibly) ever visit Digg! :)"
    author: "Anon"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-14
    body: "Hint: Developers will notice more quickly if you file bug reports. \n\nIf they look like common issues, as these do, they are likely already filed but not recently updated. Several months ago BugSquad did actually go through all the kopete bugs, but not all items could be checked because some required a particular videocamera or something. Lots of things were fixed... Not all. \n\nCheck that you're using the latest version, and then look for an open bugreport and comment on it that you still have the same issue, AND give what version number you are using. \n\nOdds are that someone will look at it again. Incidentally, kopete is in need of developers, so if anyone out there is interested, #kopete and bug reports await you. ;)\n\nAnd I guess I'll try yahoo some. Other things that can be an issue for developers: you have to have someone to actually talk to! If everyone you know is on a different service, you have won't run into problems on some as easily, since you aren't using it. "
    author: "blauzahl"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-07
    body: "File transfer works for me (0.60.1 on KDE 4.1.1). No buddy icons but I don't care about that. I don't have a webcam."
    author: "Simon"
  - subject: "Re: Buddy icons in Kopete "
    date: 2008-11-08
    body: "For yahoo i am using Gyachi 1.51, is working great. Yahoo procotocol on Kopete really sucks."
    author: "Laura"
  - subject: "KJots broken link"
    date: 2008-11-07
    body: "This link\n\nhttp://dot.kde.org/issues/2008-09-28/#2\n\ndoesn't work, it gives a 404"
    author: "Vide"
  - subject: "Re: KJots broken link"
    date: 2008-11-07
    body: "Fixed, thanks.\n\nDanny"
    author: "Danny Allen"
  - subject: "The Headline is wrong"
    date: 2008-11-08
    body: "It should be 5th November instead of 5th October"
    author: "bumbl"
  - subject: "Re: The Headline is wrong"
    date: 2008-11-09
    body: "No, it's correct. Danny built a little backlog to catch up on again."
    author: "Anon"
  - subject: "Re: The Headline is wrong"
    date: 2008-11-09
    body: "No, it is correct.\nI've been busy (and still am!) with other stuff.\n\nDanny"
    author: "Danny Allen"
  - subject: "hint for future Digest"
    date: 2008-11-09
    body: "It looks like kdenlive is nearly ready to release its first stable KDE 4 version. As many users are shouting: \"KDE4... ok, but where are the native apps?\", this is one of the big apps and maybe worth a special look:\nhttp://www.kdenlive.org/\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: hint for future Digest"
    date: 2008-11-09
    body: "Looks like a project i'll have to keep my eye on.\nThanks for the suggestion ;)"
    author: "Mark Hannessen"
  - subject: "Re: hint for future Digest"
    date: 2008-11-11
    body: "I tried using the KDE 3 version, but it would not capture any video where as Kino worked immediately.  I normally look for KDE/GTK apps first to fill my needs, but fighting with Kdenlive for a day, I could not get it to work."
    author: "T. J. Brumfield"
  - subject: "k3b development? kde3 \"killer apps\""
    date: 2008-11-09
    body: "I have not heard anything from k3b in a while. Seems a bit stalled...\nWill we have to stick with the kde3 version of k3b?\ndigikam for kde4 is getting along nicely though (the other still-kde3 app I use frequently)\n"
    author: "Thomas"
  - subject: "Re: k3b development? kde3 \"killer apps\""
    date: 2008-11-09
    body: "I am also forced to use the KDE 3 version of K3b because I can't even burn .iso images with the KDE version. Don't know what's going on. I might file a bug report or something."
    author: "Bobby"
---
In <a href="http://commit-digest.org/issues/2008-10-05/">this week's KDE Commit-Digest</a>: Support for image file previews in the "FolderView" Plasmoid, which are enabled by default. Kross support for making comic providers using scripting languages in the "Comic" <a href="http://plasma.kde.org/">Plasma</a> applet. First fully-working version of the QEdje script engine for Plasma is moved into kdereview, then into kdebase. More progress in the "Weather" Plasmoid, more integration of D-Bus in PowerDevil. A more accessible configuration option to enable "show my bookmarks" as the start page in <a href="http://www.konqueror.org/">Konqueror</a>. Initial implementation of session management in <a href="http://konsole.kde.org/">Konsole</a>. Support for editing multiple languages at the same time with the scripting API of Lokalize. First version of an Enhanced Metafile Format (EMF) generator for <a href="http://okular.org/">Okular</a>. A collection of new data plugins in <a href="http://kst.kde.org/">Kst</a>. Configuration support for thumbnail cache options in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. Support for playing music during a slideshow in kipi-plugins (used by <a href="http://www.digikam.org/">Digikam</a>, etc). Preliminary support for the PIMO concept in <a href="http://nepomuk.kde.org/">NEPOMUK</a> (a key idea of the framework). More refinements to the search bar in <a href="http://kate-editor.org/">Kate</a>. First parts of a "partly-usable" Media Devices applet in <a href="http://amarok.kde.org/">Amarok</a> 2. "Ink" receiving support, and the ability to preview file transfers for the WLM protocol in <a href="http://kopete.kde.org/">Kopete</a>. More work on a "cost breakdown" view in <a href="http://koffice.kde.org/kplato/">KPlato</a>. Work on improving the handling of lists in the "TextShape" of <a href="http://koffice.org/">KOffice</a>. A draft of a TCP-based real-time syncing model in Okteta. Optimisations in <a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Removal of the wizard in <a href="http://edu.kde.org/parley/">Parley</a>. Kapman moves from playground/games to kdereview. Initial import of KJots rewrite, <a href="http://commit-digest.org/issues/2008-09-28/#2">with plans to port to Akonadi, NEPOMUK, and Plasma</a>. <a href="http://commit-digest.org/issues/2008-10-05/">Read the rest of the Digest here</a>.



<!--break-->
