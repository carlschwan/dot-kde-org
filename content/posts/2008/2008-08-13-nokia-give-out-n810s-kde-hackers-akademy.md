---
title: "Nokia Give Out N810s to KDE Hackers at Akademy"
date:    2008-08-13
authors:
  - "jpoortvliet"
slug:    nokia-give-out-n810s-kde-hackers-akademy
comments:
  - subject: "Diversion"
    date: 2008-08-13
    body: "So the KDE developers  will be diverted to code for Nokia instead of KDE?\nAm I that paranoid?"
    author: "reihal"
  - subject: "Re: Diversion"
    date: 2008-08-13
    body: "<i>Am I that paranoid?</i>\n\nApparently so.  They'll still be coding KDE, just helping to make it work better on these small devices.  When the kernel developers made linux work on big iron, were they \"developing for IBM instead of linux\"? Or \"developing for ARM instead of linux\" when they made it work on ARM devices? I don't think so..."
    author: "Adrian Baugh"
  - subject: "Re: Diversion"
    date: 2008-08-13
    body: "But it is right. The Linux Kernel folks are highly sponsored and thus dependent on the companies that profit from server market. This is the reason why up to today the graphical solutions on Linux mostly suck - simply because there is not enough momentum to truly \"outcompete\" the \"games\" and graphical sector.\n\nThey hardly care about the desktop user at all. \n\nThe KDE folks are much better in this regard listening to users. I am just sad that the KDE folks do not write their own kernel. I hate arrogant programmers.  \n\nBut let's not write the names of a few very important GNU folks here whom I think are arrogant and disregarding users ... ;-)"
    author: "tomas"
  - subject: "Re: Diversion"
    date: 2008-08-13
    body: "The claim is always made that kernel hackers don't care at all about desktop/laptop users.  Yet in moving to a newer kernel my laptop now has better suspend/resume (which never used to work reliably), better power management and a better wifi stack.\n\nAs far as graphical solutions, with the notable (and recent) exception of nVidia, linux graphics drivers are better than they have ever been.  There are still plenty of problems, but things like Gallium3D (http://en.wikipedia.org/wiki/Gallium3D) will hopefully solve issues like these.\n\nSo, if the kernel hackers don't care about the hardware I use, I haven't noticed.  My hardware is running better than it ever has and I don't think it's just a coincidence."
    author: "Kyle Cunningham"
  - subject: "Re: Diversion"
    date: 2008-08-13
    body: "Come on, the next non-Microsoft game consoles have to be Linux based."
    author: "velum"
  - subject: "Re: Diversion"
    date: 2008-08-14
    body: "\"This is the reason why up to today the graphical solutions on Linux mostly suck\"\n\nAnd that's the reason why Linux has no foothold in hi-end 3D-animation, CGI etc. etc... no, wait...\n\n\"They hardly care about the desktop user at all.\"\n\nNow there's a load of crap. They (the kernal-hackers) care VERY MUCH about the desktop. they have spent A LOT of time and energy creating schedulers that improve interactivity on the desktop, they struggle with ACPI in order to make usage on laptops smoother etc. etc. Of course they care about the desktop, since about 99% of them use Linux on their dekstop, and they obviously want their desktop-experience to be as good as possible.\n\nThis whole argument started when Con Kolivas started whining when his set of pathches were not accepted to the kernel. Just because his patches were not accepted does not mean that the devels do not care about the desktop. He was not the savior of the desktop that was rejected by the kernel-hackers, he was not entitled to have his patches in the kernel."
    author: "Janne"
  - subject: "Re: Diversion"
    date: 2008-08-14
    body: "I never once say Con Kolivas whine.  Some fans of his work did.  Con was always pretty polite, made tons of modifications to his scheduler, and consistently his benchmarks showed that not only was his scheduler superior to anything anyone else put out, but also his scheduler constantly improved during the three years he worked on it.\n\nLinus said for years that desktop performance wasn't that important, and he argued against \"fair\" scheduling.  When it was painfully apparent that Linus was wrong, he wasn't about to admit that.  He asked his buddy to start writing a brand new scheduler from scratch that heavily borrowed Con's ideas that Linus had been bashing for three years.\n\nThe new scheduler was buggy, untested, and not as fast as Con's.  Linus, who always suggests that politics don't matter and the best code should get it, once again proved himself a hypocrite.  He blatantly then said that Con's code would never be seriously considered because Con as a person was incapable of supporting and standing by his code.\n\nNever mind the mailing list archives that demonstrate how well he had already supported the code for three years.  Linus didn't want to admit he was wrong, so he put inferior code in the kernel, and even worse, drove away a great programmer with senseless personal attacks.\n\nI agree with you that the kernel devs care about the desktop, however I understand how people get the impression that they don't.  Repeatedly, Linus has turned down patches that would improve desktop performance, because he said the kernel shouldn't focus on the desktop.  And by that he means that Linux needs to be able to run in many different environments.  His goal is to design a kernel that can largely can run well in any box, not just a desktop.  When users (not Con) then started asking for a desktop-kernel fork, Linus went off blasting the desktop-user-brigade, which certainly didn't help the impression that he cares about the desktop.  He then repeatedly blamed Con for it, but in reading the LKML for years (as well as Con's mailing lists) never did I see Con push for a fork.  It was uninformed users.  Regardless, Con was the villain.  "
    author: "T. J. Brumfield"
  - subject: "Re: Diversion"
    date: 2008-08-14
    body: "I think you're missing that for it to be supported, the main thing that will happen is that KDE will work better on smaller screens. That will translate well to other small devices, as well as to older smaller CRTs/LCDs."
    author: "blauzahl"
  - subject: "Now 100 KDE Developer use Gnome ;)"
    date: 2008-08-13
    body: "As Gnome runs on the Nokia 810 there are now 100 KDE developers running Gnome. :) "
    author: "Tobias"
  - subject: "Re: Now 100 KDE Developer use Gnome ;)"
    date: 2008-08-13
    body: "I guess you mean \"they use some of the GNOME platform libraries\". The GNOME desktop is something different."
    author: "binner"
  - subject: "Re: Now 100 KDE Developer use Gnome ;)"
    date: 2008-08-13
    body: "Of cause you are right, but that isn't such a nice headline. ;)"
    author: "Tobias"
  - subject: "Re: Now 100 KDE Developer use Gnome ;)"
    date: 2008-08-13
    body: "Perhaps you missed Nokia purchasing Trolltech, and Nokia repeatedly approaching Maemo developers asking them to develop more QT apps for the platform.  Many of the existing developers are ingrained in developing with GTK, and Nokia has been trying to change that.  This move is part of that initiative."
    author: "T. J. Brumfield"
  - subject: "Re: Now 100 KDE Developer use Gnome ;)"
    date: 2008-08-17
    body: "> perhaps you missed \n\nthe smiley from the original poster :-/\n"
    author: "anonymous"
  - subject: "Furthermore"
    date: 2008-08-13
    body: "Getting hardware in the hands of a developer is a good way to make sure it is supported.  It shows a commitment of Nokia, and it is frankly a cool gesture.  I've been lusting after one of these for a while and debating whether or not I should purchase one.  I'm waffling between one of nxxx series, or considering something like the new ASUS mini-laptops, precisely because I know KDE runs well on them.  Knowing that KDE will be supported on the Nokia internet tablets helps me lean back in that direction."
    author: "T. J. Brumfield"
  - subject: "re"
    date: 2008-08-13
    body: "why would it be a gnome device, when gnome is being deprecated for QT/KDE? That is the long term nokia plan most likely, deprecate gnome from the device. If you think they are going to keep the gnome stuff around you've got a surprise coming!"
    author: "jh"
  - subject: "Re: re"
    date: 2008-08-13
    body: "that's wrong, maemo will still be gtk+ based, nothing will change. qt will make it on the nokia devices, so that it's possible to develop qt-based apps, too."
    author: "gnome"
  - subject: "Re: re"
    date: 2008-08-14
    body: "Why did they port Mozilla to Qt for Maemo then?"
    author: "Ian Monroe"
  - subject: "Re: re"
    date: 2008-08-15
    body: "Why did they port WebKit to GTK?"
    author: "Tobias"
  - subject: "At the end ......"
    date: 2008-10-21
    body: "At the end .... everything will be the same. \n\n\nNokia will have cheaper phones (with KDE) but the price for the rest of the people will be upper, sure.\n\nSo, at the end Nokia wins and the rest lose !!!!!!\n\nGreat work made by Nokia Enterprise."
    author: "Antonio J. Martinez"
---
During the <a href="http://emsys.denayer.wenk.be/">Emsys</a>-sponsored <a href="http://akademy.kde.org/events/emmobile.php">Mobile and Embedded day</a> at <a href="http://akademy2008.kde.org/">Akademy 2008</a>, Nokia distributed 100 N810 internet tablets to KDE developers, and gave around 50 more for the project to distribute to worthy sub-projects. Read on for more.


<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/akademy-2008-free-n810.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-free-n810-wee.jpg" width="400" height="267" /></a><br />
Queuing for their free N810's
</div>

<p>Kate Alhola, head of the <a href="http://maemo.org/">Maemo</a> development team, told us: "We would like to have more Open Source applications on the Maemo platform, and giving these devices to the KDE developers will help them write those. Besides, it's great to give to the F/OSS community. We have used F/OSS software and products for a long time and have just begun to contribute back, both in terms of code and sponsorship. We realize our infrastructure depends on F/OSS developers to grow, and we wanted to emphasize our commitment to the F/OSS community. After all, we are the largest mobile devices manufacturer who considers F/OSS a key asset and we're heavily expanding our involvement, focusing more on the Qt development community."</p>

<p>Sebastian Kügler, KDE e.V. board member noted: "Many KDE hackers have been interested in developing applications for the Maemo Platform, and now they can. We welcome the Maemo community to work with us on getting the power of KDE available to Maemo users!"</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/akademy-2008-binner-n810.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-binner-n810-wee.jpg" width="400" height="267" /></a><br />
Stephan ports openSUSE to the N810
</div>

<p>We found another board member, Adriaan de Groot, apparently without a N810. Asked about it, he told us: "I just gave away mine because the other guy had better reasons to have one". KDE developers are playing with their devices everywhere, and it's a testament to the great work of the Akademy organization that on the network everybody is still able to browse the web. We can expect to see many Maemo-related blogs and code pop up in the coming months!</p>

