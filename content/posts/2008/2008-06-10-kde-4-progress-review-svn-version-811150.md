---
title: "KDE 4 Progress - A Review of SVN Version 811150"
date:    2008-06-10
authors:
  - "liquidat"
slug:    kde-4-progress-review-svn-version-811150
comments:
  - subject: "panel configuration"
    date: 2008-06-10
    body: "the panel configuration tool is great, but you can't move applets inside the panel. and if you add the old menu to the panel you really want to put it leftmost. so thats basically why i'm not using kde4 despite the great efforts (looks like i'm very picky)."
    author: "SirDonnerbold"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "If that's the only reason you aren't using it, then there's actually a workaround, of sorts: remove the widget from the panel, open the Add Widgets dialog, and drag the widget from the Add Widgets dialog onto the required position on the panel."
    author: "Anon"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "What's the workaround for placeing application shortcuts? Currently one has to poke around in the configuration file :-(\n"
    author: "Birdy"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "eh really, just in kickoff right click and add to panel, u can even drag and drop links from desktop to panel, but yes moving applet in the panel is still not here, not a big deal for me anyway"
    author: "mimoune djouallah"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "Yes, they can be added to the panel. But I can't move them where I want them to be (placeing).\nAt least with kubuntu rc1 packages."
    author: "Birdy"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "When you drag and drop them to the panel, they should appear where you drop them. At least, they did for me.\n\nIt's kind of a pain, and it's possible you'll have to try a couple times to get them exactly where you want, but at least you only have to do it once."
    author: "dolio"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "for now, it is not possible to move applet inside the panel, there is already a bug report, although u will be astonished to see that in kde4.0.x packages of opensuse and i guess fedora they have fixed this issue why it is not upstream, i don't know ;)"
    author: "mimoune djouallah"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "Replace \"Add Widgets dialog\" with \"Kickoff\" in the other post ;)"
    author: "Anon"
  - subject: "Taskbar"
    date: 2008-06-10
    body: "What I'm really is the possibility to configure the taskbar. I'd like to set it so that it behaves like in KDE3, where I have a great overview because of these \"open programs\" one under the other (and next to the other). Can you implement this please?"
    author: "Anonymous"
  - subject: "Re: Taskbar"
    date: 2008-06-13
    body: "Yes. We want see more taskbar usability. "
    author: "legato"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "openSUSE has a patch that allows moving plasmoids in the panel. (I'm not sure if this works with the 4.1 experimental but it works with the default 4.0.x.)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: panel configuration"
    date: 2008-06-10
    body: "It doesn't work with 4.1."
    author: "Kevin Kofler"
  - subject: "Thanks!"
    date: 2008-06-10
    body: "KDE 4.1 is shaping up reaaaally nicely. Big THANK YOU to all the devs! :)\n\nI think there are only a few annoyances left:  (I'm on beta 1)\n\n- why can't we hide the plasma \"bubble\" in the upper right corner?\n- the folder view applet has problems with unmounted network shares\n- the shutdown dialog is still unnecessary complicated (too many clicks)\n- widgets --> install new widgets --> download from internet --> nothing happens\n- no right click menus for desktop icons and/or folder view\n- why does dolphin create annoying hidden .directory files? krusader doesn't do that...\n- some plasmoids in the panel look messy when making the panel really small\n\nthat's all for now. thanks everyone! :)"
    author: "fish"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "\"- why can't we hide the plasma \"bubble\" in the upper right corner?\"\n\nOpenSuse have a patch for that.  Eventually, the main supported method (as far as I can tell - I'm happy to be corrected, here) for outright removing it will be for someone to write a containment that does not have the Plasma Cashew.  This should be easy to do (in fact, I think the existing Folder View may qualify) and again, as far as I can tell, there will eventually be a GUI option for setting your desktop containment.  It won't happen for 4.1, though, I don't think.\n\n\"- the folder view applet has problems with unmounted network shares\"\n\nThe folder view will likely have many problems as it is very young :) They will be fixed in time, I'm sure.\n\n\n\"- the shutdown dialog is still unnecessary complicated (too many clicks)\"\n\nI haven't seen any plans to change this, I'm afraid :/\n\n\"- widgets --> install new widgets --> download from internet --> nothing happens\"\n\nThis might still be on the TODO list - I'm not sure.\n\n\"- no right click menus for desktop icons and/or folder view\"\n\nThe Folder View will have this for 4.1, IIRC (in fact, it may have been added over the last couple of days).  Ultimately, if you want your desktop background to behave as in KDE3, the Folder View is the only place where you'll need this functionality.\n\n\"- why does dolphin create annoying hidden .directory files? krusader doesn't do that...\"\n\nBecause it uses the same system of storing per-folder settings that Konqueror did in the KDE3 series.  I'd like to see a cleaner system, personally, but I'm not sure how technically difficult this would be.\n\n\"- some plasmoids in the panel look messy when making the panel really small\"\n\nEnsure you have a bleeding edge build (KDE4Daily is great for that:\n\nhttp://etotheipiplusone.com/kde4daily/docs/kde4daily.html )\n\n\nand, if they are still present, file bugs reports, with screenshots.\n\nThanks for the helpful and refreshingly non-vague feedback! Dot commenters - this guy knows how to give constructive and actionable feedback, and it would be great if more people followed his example.  If you don't feel the slightest bit of gratitude for the KDE devs, you can even strip out all the \"Thankyou\"'s and general tone of politeness, if you want - the resulting post will still stand head and shoulders above the usual \"criticism\" we see here every day."
    author: "Anon"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "* Because it uses the same system of storing per-folder settings that Konqueror did in the KDE3 series. I'd like to see a cleaner system, personally, but I'm not sure how technically difficult this would be. *\n\nWouldn't Nepomuk qualify for this particular task?"
    author: "NabLa"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "> Because it uses the same system of storing per-folder settings that Konqueror did in the KDE3 series. I'd like to see a cleaner system, personally, but I'm not sure how technically difficult this would be.\n\nPersonally I'd prefer it if Dolphin used the same setting for all folders by default."
    author: "Greg M"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "> Personally I'd prefer it if Dolphin used the same setting for\n> all folders by default.\n\nJust go into Settings -> Configure Dolphin... -> View Modes and click on \"(x) Use common view properties for all folders\" No .directory files are generated anymore :-)"
    author: "Peter Penz"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "Thaaaaaanks!!!\n\nSo I'll switch to Dolphin right now..."
    author: "fish"
  - subject: "Re: Thanks!"
    date: 2008-06-11
    body: "i know this is not the right location to post questions, but...\n\nis there a reason why dolphin doesn't try to use extended attributes before resorting to an extra file per directory?"
    author: "ac"
  - subject: "Re: Thanks!"
    date: 2008-06-11
    body: "> is there a reason why dolphin doesn't try to use extended attributes before resorting to an extra file per directory?\n\nIt's not portable. The less OS-specific code in any particular application, the better, the easier to work with later, the smaller the application, and the more portable the application."
    author: "Riddle"
  - subject: ".directory file / shutdown/logout comments"
    date: 2008-06-11
    body: "What's wrong with a single file (say dolphin-directoriesrc) in ~/.kde/share/config, to which any directories with altered settings are added along with their settings? The one thing I can think of(for why not) is parsing that text file when it contains thousands of entries might be slow going(?).\n\nPer-directory settings are something I'd like to use, but not at the cost of crufting up every directory I visit with a .directory file.\n\nWhile I'm at it I'll also mention that I can't help but wholeheartedly agree with the previous comment about the Shutdown dialog.\n\nYou hover over the Leave button on the menu and the menu changes to offer you all the regular options (logout, reboot, shutdown), so far so good. But then when you choose one of these options, you are just presented with a dialog containing the exact same options? What's the point of that? There is no point that I can see, apart from being totally redundant.\n\nJust make the Leave button on the menu an actual button with no menu items tied to it. When CLICKing Leave, the menu should stay the same, displaying whatever applications or favourites or whatnot were already displayed at the time. It should only activate the dialog containing the choices of which way the user wants to leave the desktop. Then the user makes a choice and logs out. It's that simple really. Or keep it the way it is and remove the redundant choices from the dialog and just have it be a \"Do you really want to \"logout|shutdown|reboot\"? Yes/No.\n\nAppologies for not remembering the name of the new-fangled KMenu. And for possibly not excercising tact/restraint in my comments. I love KDE!"
    author: "Geoff"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "I'll agree with this one.\n- the shutdown dialog is still unnecessary complicated (too many clicks)\n\nI think what i am going to comment on is also a 3.5 wobbly.  When you've pressed for shutdown or logout, the screen washes out to black which is fine. But a soon as you confirm the action, the screen colour returns and the dialog box disappears.  Depending on whats happening on your PC (i.e. madly processing causing a slow down) you are then presented with what looks like a useable desktop where you could select an icon.  Should the screen stay black or the desktop completely disappear as the first action after a confirmed logout/shutdown?\nThis caught me out late one evening when i was a bit dozy and i thought the Confirmation screen had been cancelled so i tried to select logout/shutdown applet again.\n\nGreat work is being done by all the devs on KDE4 and related apps. Thanks."
    author: "Ian"
  - subject: "Kshutdown"
    date: 2008-06-10
    body: "The shutdown dialogue is much too complicated, but not powerful enough at the same time. I miss the option (under an advanced button) for timed shutdowns. For an idea what I mean have a look at (KDE3-only) kshutdown.\n\nAlso hibernate and suspend do not do anything, I want to at least get an error report if something fails.\n\nBut these are minor annoyances, I have been using KDE4 for three months as main desktop. It is still behind KDE3, but the pain is becoming less intense.... More betas would be appreciated....."
    author: "MoritzMH"
  - subject: "Re: Kshutdown"
    date: 2008-06-12
    body: "What version of KDE are you using, or were you using when you last tried suspend/resume? It should be fixed in 4.0.4 (and anything newer, of course)."
    author: "Kevin Kofler"
  - subject: "Re: Kshutdown"
    date: 2008-06-12
    body: "I mean suspend/hibernate. But yes, resuming afterwards should work too. ;-) But if that doesn't work, it most likely isn't KDE's fault. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "\"- why can't we hide the plasma \"bubble\" in the upper right corner?\"\n\nPut a application window on to of it, and it's \"gone\". Seriously, what's the use of removing it? It does not take away any screen relastate for applications whatsoever, and when it's not covered by applications it's easy accesable wich is intentional. The biggest consecquence of having it present, is that it cover up the corner of the walpaper. If loosing a 20-30 pixel square of the walpaper is a big issue, I think you spend way to much time staring at the walpaper rather than using the computer....\n\n\n\"- the shutdown dialog is still unnecessary complicated (too many clicks)\"\n\nAgreed, use the shutdown panel applet instead. One click and you get the shutdown dialog. \n"
    author: "Morty"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "It's not a huge issue but it is visual noise. I don't change my desktop settings very much so I'd just like one less widget to be there. Should we have a network, sound, graphics etc. settings widget sitting around too in plain sight? I hide my kmix system tray icon in KDE3 for the same reason. \n\nThe more clickable things in sight, the more distracting they are. I also find the cashew expand animation very annoying as I set it off accidentally a lot and it just begs you to click the buttons that just appeared. The subtle glow, say, the close button makes tells you the button is clickable without distracting you if you weren't looking at the close button. I just find the cashew distracting. Yes, it's not that big a deal, but all these small decision choices add up."
    author: "Bob"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "The cashew expand animation now only starts after clicking on the cashes, the only thing it does when hovering over it is getting a color. I used to dislike the cashew as well, but I can live with the current form (at least the kubuntu kde4.1 beta packages).\n\nThere are still a lot of things missing, but it's a nice improvement, and especially the speed improvement of both kwin in composite mode and plasma are welcome."
    author: "terracotta"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: ">> - no right click menus for desktop icons and/or folder view\n\nThis has just been implemented 1 or 2 days ago, so you must build from svn trunk to see this, or wait for the RC :D\n\nI am also very happy to get all file management function with right click in the Folder View! And I guess several other features are planned for Folder View, I'm crossing my fingers...\n\nYay!!!!! Plasma developers are amazing people with amazing patience :D :D"
    author: "fred"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "+1:\n- can't delete files in the folder view\n\nBasically if you want to delete a Desktop item, you must open dolphin and go to your ~/Desktop folder and delete the files, this is not possible from the folder view applet."
    author: "Iuri Fiedoruk"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "Dragging any item from the folder view plasmoid to the trash can plasmoid will also do the trick.. :)"
    author: "Fabio Margarido"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "It is already possible in current trunk, it was not possible in Beta."
    author: "anon"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "> - why does dolphin create annoying hidden .directory files? krusader\n>   doesn't do that...\n\nAs mentioned by Anon already: like Konqueror in KDE 3 this file is used to store folder specific settings. If you don't want that Dolphin stores this information you can turn this off under Settings -> Configure Dolphin... -> View Modes and clicking on \"(x) Use common view properties for all folders\".\n\nOther file managers use a similar approach as this has some benefits in comparison to a mirrored storing of the view properties (which BTW is done already for non-local directories or directories without write access)."
    author: "Peter Penz"
  - subject: "Re: Thanks!"
    date: 2008-06-10
    body: "\"KDE 4.1 is shaping up reaaaally nicely\"\n\nYep, I've just tested the 4.1 packages from Kubuntu and I'm near to do to the switch right now. The 4.0 was just unusable for me and now, everything works better, especially KWin compositing and Plasma of course\n\nThe only thing that prevent me to do so* is maybe that I can't change the desktop with a scrollwheel stroke on the desktop (this is really the best way to manage desktops for me because I only use 2 desktops). I think it's too late for the 4.1, even if this is surely (well... maybe) a very small change :-/\nI'll be glad if I see the Quick Browser applet as a plasmoid too :)\n\nThanks everyone :)\n\n* well, ok... this can't prevent me to switch, I'm too impatient :p"
    author: "DanaKil"
  - subject: "Re: Thanks!"
    date: 2008-06-11
    body: "Maybe in the meantime you could place a somewhat-large pager plasmoid on the desktop and use your scrollwheel on that? That might hold you over until the feature is implemented."
    author: "Jonathan Thomas"
  - subject: "KDE 4.1beta1"
    date: 2008-06-10
    body: "I tried KDE 4.0 and quickly switched back to KDE 3. It simply had too few features at that time to be worth switching. Yesterday I tried 4.1beta1. Muuuuch better, really. Thanks to all involved. But it took me some time to figure some things out (please note that I'm well aware of bugs.kde.org - I don't wanna complain here about specific bugs, just want to give a \"meta-view\" about my first experience with 4.1, what's good, what's bad and how I worked around things that don't work so well right now)\n\n1) I created ~/Desktop4/Graphics, ~/Desktop4/Tools and so on and now use the new folder view applet to show those folders on the desktop in separate boxes. Way better than the cluttered KDE3 desktop. And much better than all those separate boxes behind icons in KDE 4.0. Some things I don't like about this solution: There is no way to get rid of the icons in ~/Desktop for good. So I had to delete them. Now, if I still sometimes want to switch back to KDE 3 there are no icons. But I can live with that. Another \"glitch\" is that I cannot right-click/create icons directly in the folder view. I need to go there in Dolphin. Now, when I create an Icon in Dolphin it has the extension \".desktop\". I need to remove it manually to have a decent Icon label. Perhaps an option to hide extensions in the folder view and a menu entry to open the folder in Dolphin would already solve most of those problems easily.\n\n2) Getting 3D graphics right is a bit tricky, because I couldn't find information on what is recommended. First thing is, you should set effects to \"OpenGL\" in the System Settings. Otherwise, you cannot expect all those great effects to work fast. While experimenting, sometimes KDE4 didnt start at all so I needed KDE3 to repair my system. I thought FGLRX would be better even if I'm sacrificing \"openness\". But RADEON driver seems to have much improved to what I'm used to in recent days. I'm not gaming so I cannot say anything about that. But for KDE 4 desktop I found RADEON to be that much better choice. Even if there were sometimes some artifacts on the screen. With FGLRX sometimes X used 40% cpu for minutes without apparent reason. And I havent been able to play movies properly without heavy flashing and distortions - I tried all combinations of VideoOverlay, OpenGLOverlay, TexturedVideo and what not. With radeon I didn't need a single option. Worked out of the box. So I'm sticking with RADEON and it seems to be that even general performance and CPU usage is better than with the proprietary driver.\n\n3) Watch out: A square applet takes 100% of your CPU continuously. It took me some time to figure out, that the \"Minimize All\" applet made my PC so slow. So I removed it from the panel. I'm working around this right now by using Ctrl+F2 or Ctrl+F12. This is OK. I couldnt move the applet to it's usual spot anyway because you cannot move applets in the panel with KDE 4.1.\n\n4) The only remaining problem I cannot work around right now is the fact, that the width of the places bar inside the open dialog isn't saved and is so large that I need to resize the panel bar everytime I use the open dialog. This is quite annoying because you obviously use the open dialog very often...\n\n5) Finally you can get rid of the ugly black boxes everywhere by installing the \"Glassified\" plasma theme. It even downloads this theme directly from the dialog. Great!\n\n6) After start-up I see the plasma desktop. I can click the K menu and so on. I can start apps like MPlayer from the \"Recent documents\" menu. But it takes extremly long until I can start KDE 4 applications like Dolphin (more than 1 minute) with seemingly no activity (no disk thrashing). I wonder what is it doing there? Well, I can live with the wait at the moment.\n\nOtherwise, KDE4.1beta1 is just great. With radeon driver it all works fast even with 3d effects. I never had that much success with Compiz causing all sorts of problems with different apps and slowing my PC down. User interfaces looks polished and fresh. So, I think 4.1beta1 is finally \"good enough\" for me to use it regularly. Thanks again to all those involved for shipping a great product which will probably be even better if one day those remaining problems are resolved.\n"
    author: "Michael"
  - subject: "Re: KDE 4.1beta1"
    date: 2008-06-10
    body: "You'll be please to hear that 3) was fixed very soon after the release of Beta 1, and also that (I think) the Places bar size issue (4)) is now much better - there was apparently a temporary regression or suchlike that caused the Places bar to be inordinately oversized.  I'm not sure if the size that you have set is actually stored now, though."
    author: "Anon"
  - subject: "Re: KDE 4.1beta1"
    date: 2008-06-10
    body: "for 5) i wonder why it did not worked for me, i have used kde4.0.81 with glassified theme and those ugly black frame around icons are still here, please can u provide a screenshot;) this the most hated bug for me ;)"
    author: "mimoune djouallah"
  - subject: "Re: KDE 4.1beta1"
    date: 2008-06-10
    body: "Here you are..."
    author: "Michael"
  - subject: "Re: KDE 4.1beta1"
    date: 2008-06-10
    body: "I'd like to add: Glassified theme has some problem with different graphics cards. Perhaps it's useful if you temporarily switch to XRender instead of OpenGL just to find out if this is causing the problem."
    author: "Michael"
  - subject: "Re: KDE 4.1beta1"
    date: 2008-06-10
    body: "About the folder view, it would be great if we can choose which type of files to show through its mime-type (but I agree that the actual file mask is quite good in some case). If anybody know how I can show only the directory with the current applet, I'll be glad to hear you :)"
    author: "DanaKil"
  - subject: "Re: KDE 4.1beta1"
    date: 2008-06-11
    body: "Just for the record:\n\nI have solved the very long start-up delay by installing kdm-kde4 instead of kdm."
    author: "Michael"
  - subject: ">Well, I can live with the wait at the moment. NO!"
    date: 2008-06-27
    body: "I agree with the post and appreciate the tone and what you tried to do but I disagree with one thing;\nWell, I can live with the wait at the moment.\n\nNo, we cant.\nIf you told me that this is a beta version of any other software, I would have said yes, run it on your test system but do not run it on your main machines. \nIt is not ready.\n\nI get where we are at in the process and the jump from 4.04 to 4.1 has been enormous but that's just because 4.04 shouldnt have been even considered an alpha release.\n\nIm not agreeing with SJVN all the way about the numbering system but KDE 4 is being ravaged in the specialized media because the 4.0 is to them an indication that the product is ready to ship.\nBut I also understand that version 4 is totally different from version 3 so you couldnt call this version 3.9.\n\nI blame the KDE marketing team for jumping the gun and for each layer after not going out of their way to warn people that 3.5 was the stable version.\nI got that because I follow KDE blogs like Aaron's but I have friends of mine who are less in the loop who had a different take on it.\nIf the message is badly understood, you DONT blame the person who didnt understand it but those who sent the message.\n\nWe see the 4.x as a new paradigm who will disturb the status quo and I think we have isolated ourselves to the point where we think that others dont want to understand. I say we should do a better job of getting the message across.\nAnd tell the PR people to take a layer of BS off the press releases.\n\n- 4.04 was not ready by far. It promised something which it wasnt equipped to give at that point. This was nothing more than a preview.\n- 4.1 is a huge step and you can see the changes in the recent builds but it still is not something that I would tout to non-KDE users as something they should  check out. 4.1 is a beta.\n- 4.2 looks promising. Will it be something we can use to entice new users?\n  I want so much to say yes, but I would rather make sure that everything is wrapped up tightly by 4.3\n\nNot BSing with promises and getting the message out there about what v4 is trying to do different than v3 is very important. Both to the specialized press/web media and to the non free software using public.\n(I turn off the eye candy for personal use but ALWAYS leave it on for my newbie friends/Mac users).\nWe need a spreadfirefox grassroots campaign.\n\nThe message is not getting out there.\n\nAnd there are many childish technologists like SJVN who wont be back to KDE 4 for quite some time because they thought KDE4.04 is something that it wasnt.\n\nDont blame others for this misconception, look inwards.\n\nIf a tree falls in a forrest and no one hears it, does it still make a noise?"
    author: "robbie enderle"
  - subject: "Migrate from 3.5 -> 4.1"
    date: 2008-06-10
    body: "Hi, is there any application that helps mith the migration of settings like shortcuts, Bookmarks, Kontact data (mail, pop/imap/smtp conf., ...), Kwalletmanager data, ... ?"
    author: "ETN"
  - subject: "Re: Migrate from 3.5 -> 4.1"
    date: 2008-06-10
    body: "I think this will happen automatically, since the files are basically the same and content changes are handled through an update script mechanism.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Migrate from 3.5 -> 4.1"
    date: 2008-07-13
    body: "It doesn't, at least with a kubuntu hardy installation with kde3.5 and kde4.1 beta2 coexisting. At least not with bookmarks, kdewallet data or email account settings, though most of my mailboxes do seem to be recognised."
    author: "Adrian Baugh"
  - subject: "Great"
    date: 2008-06-10
    body: "Thanks for the link to the article, I love the articles on PolishLinux."
    author: "Darkelve"
  - subject: "I will be able to change the position of applets?"
    date: 2008-06-10
    body: "I agree with the article in one point: the lack of a option to change the position of the applets in the taskbar.\n\nDoes anyone knows when this will be implemented?\n\nThanks.\n"
    author: "drz_0"
  - subject: "Re: I will be able to change the position of apple"
    date: 2008-06-10
    body: "It will be implemented, but probably not for 4.1 as it is in feature-freeze."
    author: "Anon"
  - subject: "Re: I will be able to change the position of apple"
    date: 2008-06-11
    body: "The main question (apart from what it takes to implement that) is whether moving items on the panel is considered a feature or if this function missing is considered a bug."
    author: "Stefan Majewsky"
  - subject: "Re: I will be able to change the position of apple"
    date: 2008-06-12
    body: "IMHO it is a bug and we're treating it as such in Fedora, but the upstream developers appear to disagree."
    author: "Kevin Kofler"
  - subject: "Re: I will be able to change the position of apple"
    date: 2008-06-20
    body: "Will we have this feature (or bug resolved) ready for KDE 4.1 Final? Will it have to wait until KDE 4.2? Is there a time frame for this? If this is addressed in KDE 4.1 Final, it would be <b>marvellous</b>."
    author: "Alejandro Nova"
  - subject: "Re: I will be able to change the position of apple"
    date: 2008-06-20
    body: "Not upstream, no - it will 99.9% likely be done by 4.2.  Distros will likely patch it in themselves."
    author: "Anon"
  - subject: "Kickoff"
    date: 2008-06-10
    body: "I like pretty much Kickoff, my only annoyance is the keyboard navigation. I can enter to a submenu with Ctrl+Right and go back with Ctrl+Left. Going up or down is not Ctrl+Up or Ctrl+Down resp.), just Up or Down resp.). This is not coherent and it is an obstruction for fast keyboard navigation.\nThanks."
    author: "Zsolt"
  - subject: "Re: Kickoff"
    date: 2008-06-10
    body: "I'd say file a bugreport, this is a genuine issue which should be tracked properly..."
    author: "jospoortvliet"
  - subject: "ksysguard applet?"
    date: 2008-06-10
    body: "Is there any way to get a system monitor in the KDE4 system tray? I use ksysguard to give me a small graph to tell me cpu, memory and network usage. I find it a good way to assure me that e.g. an application is processing something and that something is downloading."
    author: "Bob"
  - subject: "Re: ksysguard applet?"
    date: 2008-06-10
    body: "+1 , i'm also missing this basic feature of kde3.5\n\nmaybe it's not there for a good reason (hint kde eating to much) ;-)"
    author: "Luc"
  - subject: "Re: ksysguard applet?"
    date: 2008-06-11
    body: "Someone is working on something like that, but it won't make it into 4.1 I suppose..."
    author: "jos poortvliet"
  - subject: "TwinView"
    date: 2008-06-10
    body: "I still can't get my TwinView setup to work correctly with KDE4.\nAt first, the two screens were recognised, but the desktops were super-imposed on top of each other on the primary screen. This has changed sometime before 4.1 Beta 1, and now I get a proper desktop on the primary screen, but the second monitor is completely ignored (verified by debug messages during plasma's startup).\nI have been updating the relevant bug report (http://bugs.kde.org/show_bug.cgi?id=153581), and at some point submitted a patch, but so far got no response from the developers."
    author: "Elad Lahav"
  - subject: "Re: TwinView"
    date: 2008-06-10
    body: "unfortunately the patch no longer applies in trunk/ as root widget is long gone at this point. this actually makes it a bit easier in one sense .. but dual head support is not planned for 4.1. it sucks that dual is, and really always has been, a second class citizen as far back as kde2 when compared to single screen or xinerama/twinview/mergefb configurations, but not enough workspace developers run dual head systems. =/\n\nin any case, there are a number of issues to work through for proper dual head support, and it's on the 4.2 roster."
    author: "Aaron Seigo"
  - subject: "Re: TwinView"
    date: 2008-06-10
    body: "I wasn't referring to dual head support, I was talking about TwinView (single Nvidia card, two signals). It works perfectly for me under KDE 3.5.\nYes, I know that the patch no longer applies, I realised it after upgrading to one of the latest snapshots.\nMy current problem is that the startup process does not recognise the two screens, even though a simple Qt4 application run from startx (via .xinitrc) reports the correct number."
    author: "Elad Lahav"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "ah, some of the people in that bug are talking about dual head, some are talking about xinerama/twinview/mergefb style. confusing.. \n\n> My current problem is that the startup process does not recognise the two screens\n\nneat. wonder what could be causing that. until that can be tracked down, there's not much plasma can do.\n\ni do know it works rather fine on xinerama/twin/etc these days as i just went over that support with one of the devs on irc a day or two ago. curious why your qt is getting confused =/"
    author: "Aaron Seigo"
  - subject: "Re: TwinView"
    date: 2008-06-12
    body: "A current SVN snapshot still doesn't work for me. From your reply, I understand that this should be considered a bug, rather than an incomplete feature. Do I need to open a new bug report?"
    author: "Elad Lahav"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "the kde fundation should use their money to buy all core developers a second monitor. Everything smaller than 24\" OR less than 2 monitors is so nintys. And u would be suprised how more effective u get with a second monitor. Meybe we should start a appeal for funds to get this done. The development speed would way increase."
    author: "teezee"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "They don't even use 1 monitor, they use laptops!\nLike girls and suits.\nPfft.\n\nWhen you have started to use Dual Heads, there is no going back."
    author: "reihal"
  - subject: "Re: TwinView"
    date: 2008-06-13
    body: "girls and suits ? ... DH doesn't always stand for Dual Head."
    author: "yogo"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "And what if I just want a \"workable solution\" for the time being instead of \"proper support\"? :)\n\nFor my system the situation is the \"Clone mode\", just like Xinerama except that the screens overlap. The second screen is my big LCD TV and I use it to watch movies and such.\n\nBut running 4.1beta1 still puts 2 desktops on top of each other.\n\nI wouldn't mind hand editing configution file for the time being if I could just tell plasma to ignore the second screen."
    author: "Quintesse"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "> but dual head support is not planned for 4.1\n\nAnd why not? The situation where it shows 2 desktops on top of each other is a blatant bug, so the feature freeze is a lame excuse for not fixing it. This is one of the most-reported KDE 4 bugs in Fedora."
    author: "Kevin Kofler"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "Er ... lack of time, perhaps? Do you think the Plasma developers are all sitting around doing nothing at the moment, going: \n\n\"Hmmm ... we *could* start working on Dual Head support - or we could all go to the Bahamas!\"\n\"Bahamas, Bahamas!\""
    author: "Anon"
  - subject: "Re: TwinView"
    date: 2008-06-11
    body: "There is SOC project aimed to fix that.\nhttp://code.google.com/soc/2008/kde/appinfo.html?csaid=EAB3DBE72F209C37\nAny news on progress on this one ?\n"
    author: "greg"
  - subject: "Gwenview"
    date: 2008-06-10
    body: "Big thanks to all the developers for their efforts! I especially like Marble for its OSM integration and can even imagine some game developers to use it for in-game maps instead of developing their own. Or x-based distro installers could offer it to select the local settings (suse does this, I believe).\nGwenview is coming along nicely, even with all the effects and eye candy it's still quite snappy and fast. Their are two things I would like to see: Gwenview using the Dolphin file-selection style (the little + and - sings) and an ability to export the list of filename of selected images. I already use it daily from within KDE3. (I still wait for Kaffeine and IRkick to be ported)"
    author: "none"
  - subject: "Re: Gwenview"
    date: 2008-06-10
    body: "Try Dragon. Very nice.\n\nAlthough the latest phonon moves and changes broke sound. Oh well, what's bleeding edge if it doesn't hurt from time to time?\n\nDerek"
    author: "Derek"
  - subject: "Re: Gwenview"
    date: 2008-06-11
    body: "Don't know exactly what to say about this.  10 days ago, if you had asked me, I wouldn't have had enough good things to say about Gwenview.  It is still an example of excelant development work that sets a standard for other KDE4 apps to try to equal.\n\nBut (there is always one, isn't there).  It appears that the View menu is now missing:\n\nFit to Page\nFit to Page Width\nFit to Page Height\nZoom\n\n'Fit to Page' appears to have been replaced by \"Zoom to Fit\" (don't like that string much, but \"Page\" wasn't good either) but the other ones have gone missing. :-(  What is the deal?  Did some GNOME developers sneak in when we weren't looking. :-D\n\nIIUC, we are supposed to have a HIG to cover such things.  Has the HIG been finished for KDE4 and if so, where can I find it?  Or did the HIG developers give up and go into hiding.  The Kde-usability-devel mailing list appears to have been abandoned.\n\nA consistent user interface is an important issue which should not be neglected.  It has always been one of the Mac's important features.  There were (still are) major issues with the KDE interfact guidelines not being followed in KDE3.  I hope that the same thing doesn't happen in KDE4.  There is only one way to do this: GUI guidelines need to exist, be clear, and the developers *must* follow them."
    author: "JRT"
  - subject: "KDE4 on FreeBSD"
    date: 2008-06-10
    body: "Anyone out there a FreeBSD user? We can use some help over at the FreeBSD/KDE project. We used to have some magical elves that would leave us wonderful working KDE ports under our doorsteps, but they have gone away. So we can use some more hands if you have them.\n\nWe can use help building and testing experimental ports, rooting out linuxisms in the code base, writing howtos, updating our webpage, etc. Join our mailing list at https://mail.kde.org/mailman/listinfo/kde-freebsd. Thanks!"
    author: "David Johnson"
  - subject: "nepomuksearch"
    date: 2008-06-10
    body: "hi everyone\n\nKDE4.1 is really good, I use it since several weeks and for me it already rocks. sure there are some missing features like the mac style menu bar, but I guess at least with 4.2 all the features will be back and until then there are many new ones to discover.\n\nI have a question regarding the nepomuksearch like it was described in this blog s\nhttp://www.kdedevelopers.org/node/3426\nhttp://www.kdedevelopers.org/node/3443\n\nDoes this work for anyone? If yes, how can I enable it, or which module do I have to compile?\n\nThanks,\nHias"
    author: "hias"
  - subject: "resizing the panel"
    date: 2008-06-11
    body: "Hi\n\nI installed the 4.devel Kubuntu packages on an oldish laptop already running 4.05. It's a great improvement on 4.0x, the desktop effects work well even on a 5 year old Nvidia 64M card. Kudos.\n\nTwo things that did puzzle me:\n1. the position/size configuration tool for the panel - it's bit like the devs have said \"right, you asked, so you can configure everything!\". It's a bit like an ugly pagination tool in a word processor! Would it not be a simpler to have a simple settings dialogue for width, height and position. With position you would just need to have a 3x3 setting (Top, Bottom, Side) + (Left, Right, Centre) with one choice in each, rather then pixel perfect settings. Maybe that's just me ;)\n2. When the panel is placed on the top of the screen, the search field is at the bottom of the menu. Is that able to be put at the top of the menu ?\n\nOther than that, kudos to the developers. Well done, improving all the time.\n\nThanks."
    author: "yogo"
  - subject: "Re: resizing the panel"
    date: 2008-06-11
    body: "> . It's a bit like an ugly pagination tool in a word processor!\n\nneat, you got the reference.\n\n> Would it not be a simpler to have a simple settings dialogue\n\na lot simpler to code, sure. \n\nthe idea is direct manipulation configuration, vs \"airplane cockpit\" configuration. giving you the ability to simple manipulate things on screen till it looks the way want is usually a lot more straight forward than figuring out what a \"500px panel\" means.\n\n(i'm to \"blame\" for the airplane cockpit config dialog in kicker, btw =)\n\nnote that it also lets us handle changes during screen resolutions in a least-surprises manner without the settings appearing to \"jump around\" in a config dialog that exposes exact sizes.\n\n\n> the search field is at the bottom of the menu.\n> Is that able to be put at the top of the menu\n\nthis was actually an intentional change (by wstephenson) to preserve the mouse ergonomics of the menu no matter where it is placed.\n\nwe'll see how it works out in practice in 4.1, but i have to agree with will (and the opensuse team?) that actually works out.\n\nthese are all slightly different ways of approaching familiar things and so there's a moment of \"wait. what just happened?\" if you are really used to the old ways.\n\nbut give them a try and a chance, see if they work out for you. things like the panel config have tested better with people who haven't used previous kde versions than what we had before."
    author: "Aaron Seigo"
  - subject: "Re: resizing the panel"
    date: 2008-06-11
    body: "I just want to add that I completely _love_ the new panel configuration thingy. The moment I saw it pop up I went \"wow!\". It's really easy to figure out, it gives great feedback and it re-uses concepts people are already familiar with. Plus, it looks good."
    author: "Boudewijn Rempt"
  - subject: "Re: resizing the panel"
    date: 2008-06-11
    body: "Indeed ;-)"
    author: "jos poortvliet"
  - subject: "Re: resizing the panel"
    date: 2008-06-11
    body: "I think that you miss the point.  Yes, it is 'gee wiz' and 'way cool' but that is not what is needed.  This seems to be a general problem with Plasma.  Plasma is designed to be different than the rest of KDE.  This in and of itself is a usability issue.  \n\nBut, it is worse since these things that are different are also harder to use which means that there are additional usability issues since people have to relearn how to do things.  Specifically, doing everything by dragging with the mouse is more difficult for many people not to mention the disabled.\n\n> note that it also lets us handle changes during screen resolutions in a \n> least-surprises manner without the settings appearing to \"jump around\" in a  \n> config dialog that exposes exact sizes.\n\nI do hope that your logic is actually better than that.  If this is really a consideration, then isn't it possible to set the dialog's \"exact sizes\" in percent.  We set magnification of documents in percent with a widget and that seems to work fine.  \n\nOTOH, users are used to right clicking a ToolBar and choosing the icon size from a list of choices.  How is it better to have to try to do this by dragging?  Note that you do NOT want to set the Panel to a size that will result in resizing small sizes (<32x32) of icons since this will make the icons somewhat fuzzy.  You want to be able to choose one of the standard icon sizes, something which is much easier with a list than by dragging.\n\nSo, while I do think that this thing looks cool, it fails the test of usability and time is better spent trying to implement the other missing DeskTop features."
    author: "JRT"
  - subject: "Re: resizing the panel"
    date: 2008-07-13
    body: "Existing users will have an amount of re-learning to do, but it isn't exactly rocket science.  The point is, it's a better and more intuitive paradigm for new users (there are more users who will try kde at some point in the future than the current installed base).\n\nYour point about standard icon sizes is bogus anyway, as kde4 uses vector icons that are custom-rendered to the size you choose.  I seem to remember quite a bit of work went into tuning them so they looked okay even when tiny.\n\nAnd for a future where kde may be on anything from a mobile phone to a huge multi-monitor display, with a wide range of screen dots-per-inch, a fixed number of pixels becomes even less meaningful.\n\nI for one welcome our new plasma overlords :-)\n\n(Offtopic: also, the speed issues with nvidia drivers seem greatly reduced with the 173 drivers.)"
    author: "Adrian Baugh"
  - subject: "Re: resizing the panel"
    date: 2008-06-12
    body: "Hi\n\n> this was actually an intentional change (by wstephenson) to preserve the\n> mouse ergonomics of the menu no matter where it is placed.\n\nI have the panel up-top because it suits my tall height and therefore is better ergonomically for me. Important stuff for me is grouped at the top of the screen, so I quite like the idea of having the search field near the menu button - functionally similar items should be visually grouped together on the screen.\nI don't think it is a difficult change either as I can drag and drop toolbars around in KDE apps using the grips.\n\n> the idea is direct manipulation configuration, vs \"airplane cockpit\" configuration\n\nNot sure about the airplane analogy as when you move a stick in the cockpit the plane moves based on your actions (hopefully) but I kind of agree with you on this point..  It could just be done a bit subtler, say with some grips or the like rather than an on-screen ruler/paginator? Consider it a constructive criticism from a long time KDE user but it just seems a bit over the top.\nIf I want to resize an application window there is no ruler to guide me, I just put it where I want. Doesn't this now give two different interfaces to the same operation ? How about matching it to the way other plasmoids are resize and repositioned?\n\nRemember also that things need to be optionally keyboard driven. Sometimes entering numbers into a config dialogue box is easier for those who don't use a mouse.\n\nCheers, Y"
    author: "yogo"
  - subject: "Symantec Logo?"
    date: 2008-06-11
    body: "I've always wondered why the Symantec logo was chosen to represent Plasma...\n\nhttp://images.google.com/images?q=symantec%20logo"
    author: "yutt"
  - subject: "Re: Symantec Logo?"
    date: 2008-06-11
    body: "A striking resamblance.. it's not. "
    author: "Mark Kretschmann"
  - subject: "Re: Symantec Logo?"
    date: 2008-06-11
    body: "Some people really have fantasies. I personally can't see the slightest resemblance between the two.\n"
    author: "Bobby"
  - subject: "Re: Symantec Logo?"
    date: 2008-06-11
    body: "Yes, indeed, they are remarkably similair:\nhttp://techbase.kde.org/images/6/67/Plasma_logo.jpg\nhttp://z.about.com/d/netsecurity/1/0/l/A/symantec_logo.jpg\n\nWhat did you eat last night? Maybe you should be careful licking post stamps..."
    author: "jos poortvliet"
  - subject: "Re: Symantec Logo?"
    date: 2008-06-12
    body: "Hmm...The Symantec logo and the Plasma logo look nothing alike."
    author: "kde4 is awesome"
  - subject: "Re: Symantec Logo?"
    date: 2008-06-12
    body: "I don't see the resemblance.\n\nThe plasma logo is unique.  Symantec's logo is just a modified yin yang symbol that reminds me of the South Korean flag."
    author: "Evil Jay"
---
Another <a href="http://polishlinux.org/kde/kde-4-progress-new-plasmoids-akonadi-krunner-and-more/">review</a> of the upcoming KDE 4.1 was published at polishlinux.org. The review features the Panel's new configuration tool, the folder viewer, Gwenview, Marble, Akonadi and others. As usual it comes along with many screenshots.
<!--break-->
