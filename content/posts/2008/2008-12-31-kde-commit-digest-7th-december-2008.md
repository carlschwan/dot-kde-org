---
title: "KDE Commit-Digest for 7th December 2008 "
date:    2008-12-31
authors:
  - "dallen"
slug:    kde-commit-digest-7th-december-2008
comments:
  - subject: "To end the same comments (well, we can hope... ;))"
    date: 2008-12-31
    body: "http://www.kdedevelopers.org/node/3814\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: To end the same comments (well, we can hope... ;))"
    date: 2008-12-31
    body: "I can't believe people have suggested you step down from your AWARD WINNING post.\n"
    author: "T. J. Brumfield"
  - subject: "Re: To end the same comments (well, we can hope... ;))"
    date: 2008-12-31
    body: "Thank you so much for the digest danny.\nyour work is much appreciated. \nHappy new year"
    author: "anonymous"
  - subject: "Re: To end the same comments (well, we can hope... ;))"
    date: 2008-12-31
    body: "Is there some article about the specific work behind the digests? I ever wondered about this, because most of them looks like things a script does?"
    author: "Chaoswind"
  - subject: "Re: To end the same comments (well, we can hope... ;))"
    date: 2009-01-02
    body: "Danny should probably blog about it. There is some scripting there, but a human (danny, though I sometimes wonder how human he really is) has to go through all the commits by hand and categorize them. Which means going through hundreds, thousands of commits, reading them and deciding if they are feature, bugfix, optimization or shouldn't be put in the digest."
    author: "jospoortvliet"
  - subject: "Thank you very much, Danny"
    date: 2008-12-31
    body: "And please, don't give up. There's a lot of people not usually posting (like me) who appreciatte a lot your effort.\n\nBest regards from Spain.\n\nJuan Manuel (Poldark)"
    author: "Juan Manuel M\u00e1rquez"
  - subject: "Re: Thank you very much, Danny"
    date: 2008-12-31
    body: "Very true, I never do post here but I do want to express my appreciation of your work, and my admiration of your continuous patience with some of the comments here."
    author: "Clovis Gladstone"
  - subject: "Thanks a lot"
    date: 2008-12-31
    body: "Thanks Danny for your untidily work by the Digest. I read it constantly and I hope you continue with it. Wish you good luck and a happy new year 2009.\n\nGreets Ari"
    author: "Ari"
  - subject: "thanks a lot"
    date: 2008-12-31
    body: "Danny, thanks very much for your efforts in the digest.\nI am only a KDE user, not a developer or contributor, but the digest makes me wait eagerly for the next distro-release! And it also makes me aware of all the good features that KDE has and that I would not know of without the digest.\n\nAs Chaoswind suggests, perhaps it would be nice to explain to the community how you generate the digest.\n\nIndeed, the digest is much richer with introductory comments written by developers on their work, but still very useful without them.\n\nKeep on the good work, Danny, and just publish the digest on a regular basis without external contributions, should none arrive in your mailbox.\n\nregards, sulla"
    author: "sulla"
  - subject: "Thanks Danny"
    date: 2008-12-31
    body: "Indeed, thanks for your work Danny. I, for that matters, prefer to have a Digest with minimal introduction than without Digest. So the current solution is perfectly OK for me. \n\nAbout Phonon, I'd be interested by some feedback by other users here : when I compare the visual quality of Dragon Player using Phonon's xine backend, it's obviously lower than the xine output on the same system. It seems deinterlacing is not working. I would have though that Phonon would use the default xine.cfg for settings but it appear not to.\nDoes anyone have the same experience and a potential fix?"
    author: "Richard Van Den Boom"
  - subject: "Re: Thanks Danny"
    date: 2008-12-31
    body: "Bug reports are usually a much better way to report this than completely offtopic discussions here.\n\n\nIn this spirit, Thanks Danny,\n\nand HAPPY NEW YEAR to you all :)\n"
    author: "Mark Kretschmann"
---
In <a href="http://commit-digest.org/issues/2008-12-07/">this week's KDE Commit-Digest</a>: Old-school desktop patterns return as a <a href="http://plasma.kde.org/">Plasma</a> wallpaper, along with an interactive <a href="http://edu.kde.org/marble/">Marble</a>-based wallpaper display. A simple video player Plasmoid based on <a href="http://phonon.kde.org/">Phonon</a>. Continued optimisations in Plasma and <a href="http://www.kdevelop.org/">KDevelop</a>. Various new colour schemes for KDE 4 added to KDE SVN. KSysGuard processes can now be "dragged into a text editor, word processor or spreadsheet program". More progress in the functional refactoring of the Kigo game. Support for compiling sources at runtime with the C# language bindings for KDE. Backup functionality added to KJots. Last.fm service converted to use the KDE system-wide proxy settings, and basic filtering capability added to the service browser in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Further work on the new DeviceSync application. KConfigEditor begins to be ported to KDE 4. KitchenSync and the <a href="http://pim.kde.org/akonadi/">Akonadi</a> OpenSync plugin are disabled for the <a href="http://pim.kde.org/">KDE-PIM</a> 4.2 release. kdekiosk becomes officially unmaintained due to a lack of developer interest. Final preparations for release and the tagging of Amarok 2.0 Final.<a href="http://commit-digest.org/issues/2008-12-07/">Read the rest of the Digest here</a>.


<!--break-->
