---
title: "Kate Developers Meeting"
date:    2008-04-15
authors:
  - "dhaumann"
slug:    kate-developers-meeting
comments:
  - subject: "how about snippets?"
    date: 2008-04-15
    body: "I really missed them from emacs."
    author: "Paul"
  - subject: "Re: how about snippets?"
    date: 2008-04-16
    body: "Try http://ben.kudria.net/code/snippits .  Works system wide, not just in Kate.  Feel free to contact me if you have questions."
    author: "Benjamin Kudria"
  - subject: "Congrats"
    date: 2008-04-15
    body: "That`s great, my enviroment for programming is Kate, or sometimes kwrite, is a excelent app, and any upgrade would be nice :)"
    author: "Pablo Cholaky"
  - subject: "Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "I'm certainly not a GNOME partisan, but when it comes to fitting the needs of the average user, GEdit is by far better than KWrite. Getting rid of all the programming-specific options like \"Code Folding\", \"Switch to Command Line\", \"Highlight range between selected brackets\", different colors for \"Base-N Integer\", \"Modes & Filetypes\", and the \"Select to Matching Bracket\" keyboard shortcut, as was apparently discussed in this meeting, is a great step toward making KWrite a general-purpose simple text editor. But this is going to be hard, because niche settings for programmers are spread everywhere in KWrite's menus and configuration system. \n\nThere is also extreme redundancy and confusion about shortcuts. For example, Settings-> Configure Editor -> Shortcuts lists a whole bunch of keyboard shortcuts (such as \"Select to Matching Bracket\") that don't even appear when the user goes to Settings-> Configure Shortcuts. And vice-versa, shortcuts in Settings-> Configure Shortcuts such as \"Undo\" don't appear in Settings-> Configure Editor -> Shortcuts.\n\nBy the way: does the \"Show Path\" option under the Settings menu do anything in KWrite?"
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "> I'm certainly not a GNOME partisan, but when it comes to fitting the needs of the average user,\n\nWho needs a text editor but isn't a programmer or system admin?  Who regularly edits text files which do not have some form of syntax or grammar designed for software to process?\n\nGedit, Vim, Emacs et. al are called \"text editors\" but I cannot remeber the last time I edited a large body of something that was just plain text.  In other words, something that wasn't a config file, code or other structured data (eg. XML).   The one exception that springs to mind being people who use Emacs as their mail client.\n\nMost of the non-programming text I write is done via embedded editors in my mail client, web browser, IRC client etc.\n\n\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "> Who needs a text editor but isn't a programmer or system admin? Who regularly\n> edits text files which do not have some form of syntax or grammar designed for\n> software to process?\n\nPeople who don't want to wait a minute and a half for OpenOffice to come up (OK, KWord starts faster, but it is still not as fast as KWrite and it has this annoying template selector). I'm a student, and I take notes in class in GEdit: I don't need to insert graphics or do any kind of fancy formatting. Just quick notes that I can save in a text file and skim through later.\n\n> Most of the non-programming text I write is done via embedded editors in my\n> mail client, web browser, IRC client etc.\n\nHas it never happened that you wrote this wonderful reply inside a web browser text field (ie. for the KDE DOT) and by the time you clicked Submit the Internet connection went down? And when you clicked Back the text box was empty?\n\nAnyway, for the programmers and sysadmins there's always Kate which will remain full-featured, so why force regular users to swim through this ocean of irrelevant code-style options in KWrite?"
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "I have never seen any non-geek use a text editor.  Everyone fires up Word when they want to write a document, and their email client when they want to write an email.  \n\n>> I'm a student, and I take notes in class in GEdit\n\nNothing in kwrite will get in your way there.  Start kwrite, write your text, then save the file and close kwrite.  You will never see the options in the menus, so why are you bothered by them?\n\n>> so why force regular users to swim through this ocean of irrelevant code-style options in KWrite?\n\nNobody has to swim through anything.  If you're writing a text file, you don't even need to click on the menu bar, and you will be happily oblivious to all of the options available in Kwrite.  \n\nPerhaps the only criticism I have of kwrite is that there is no easy way to change the default font."
    author: "Leo S"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "> I have never seen any non-geek use a text editor.\n\nOh come one! I know plenty of Windows people who use Notepad to remove HTML-formatting from text copied from web sites. Otherwise, highlighting/copying a section from a web page and pasting directly into Word ends up carrying tables, pictures, and other junk that messes up and bloats the Word document. OpenOffice and KWord also copy over rich text features, so a simple text editor is a very useful intermediator and cleanser of unwanted text formatting.\n\n> If you're writing a text file, you don't even need to click on\n> the menu bar, and you will be happily oblivious to all of the\n> options available in Kwrite.\n\nThere are plenty of rudimentary features like search, replace, print, spell check, find help, etc that require accessing menus and that are crowded out by programming-specific settings. Not to mention trying to figure out how to turn off scripting features like tick marks.\n\nI'm frankly kind of surprised by the self-centeredness of some programmers who don't want to let the vast majority of computer users in this world have a app that's optimized for their needs. You can tweak the code style knobs in Kate to your heart's desire -- let the rest of us have a lean, mean KWrite for everyday use!"
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: ">There are plenty of rudimentary features like search, replace, print, spell\n>check, find help, etc that require accessing menus and that are crowded out by\n>programming-specific settings. \n\nLet see. Search and replace, that's in the edit menu. Nope, nothing programming specific there. Print then, in the file menu and still noting programming related. I skip forward a moment to the help menu, and surprisingly nothing programmingrelated there either. \n\nBack to the one I skipped, spell check, it's in the tools menu. There actually are something programming related here. Of 20 entries, 8 are related to programming. Thats 40%, not exactly what I'd call crowding out.\n\nOther than this you have two entries programming related, tree if you count line numbering a programming feature. The remaining menu items bookmarks and settings has none.\n\nSo I think it's safe to say that accessing the menus does not crowd you in programming related features.\n"
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "You my friend are out of touch with reality. I know lots of people who use Notepad for simple text edits and note taking .( Makes sense .. doesnt it? )\n\nHaving those advanced features in a simple editor is just insane. That is what Kate should be for."
    author: "Tom"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> I have never seen any non-geek use a text editor.\n\nWell, I'm a geek, so I may not qualify, but I keep plenty of non-code stuff in plaintext files. In fact, my other screen is occupied with \"stuff.txt\", which is basically a collection of random notes (I've similar files on various other machines).\n\nThat said, I love KWrite because it's fast, and it's powerful but the features don't get in the way like they can in a full \"document\" editor (e.g. KWord), which makes it *feel* very lean even though it really isn't (but only in a good way :-))."
    author: "Matthew Woehlke"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Hey, why would someone use a text editor for notes while there is such a mind-blowing mighty world-domination like KDE's basKet? http://basket.kde.org/ "
    author: "A KDE advocate"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Well, that's obvious; because plaintext is very nearly the lowest common denominator of computer file formats and is useful on just about anything :-). (And if you forget devices with no basic text editor and the odd EBCDIC-speaking machine, you can pretty well remove the word \"nearly\" from that sentence.)\n\nI should probably try basKet some day :-), but much of my jotting is still done on the Windows machine I have to have for various reasons."
    author: "Matthew Woehlke"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "There are export functions included (HTML in 1.x) and I think it would be pretty easily to add a text export. A who said basKet can't be used on Windows machines? ;) It can import text-files, though."
    author: "A KDE advocate"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> Hey, why would someone use a text editor for notes while there is such a \n> mind-blowing mighty world-domination like KDE's basKet?\n\nBecause, Basekt sucks. It's not easy, it's dosn't scale anywhere, it's to heavy and complicated. IMHO it's a protocase for a good idea, which is detroyd by simple usability sin's. Yes, it's useful, but that's plaintext-files also, and they are also independent from the application and platform, and more flexibel for things like scripting."
    author: "Chaoswind"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "In my opinion, basKet is easy to use and not at all complicated. If you have specific improvements in mind, why don't you at least file bugs or talk to the development mailling list?\n\nThere exists import and export functions (for non-world-dominated platforms) and it certainly could be improved as well. "
    author: "A KDE advocate"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> In my opinion, basKet is easy to use and not at all complicated.\n\nEvery opinion is based on knowledge and preferences ;)\n\n> If you have specific improvements in mind, why don't you at least file bugs or \n> talk to the development mailling list?\n\nWhile baskets development ist dead, afaik. Also, there was an project to improve the usability already, which will (of course) not be realized."
    author: "Chaoswind"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Don't look dead too me.\n\nhttp://basket.kde.org/news.php\nhttp://basket.kde.org/svnaccess.php"
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "> http://basket.kde.org/news.php\n\nNice, it's reanimated, again, in only seven short months.\nWell, we'll see how useful the kde4-version will be."
    author: "Chaoswind"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "I am a programmer and use kate regularily.\nI need all that.\n\nI've never seen non-programmers to use a text editor.\nThey're comfortable with either oowriter or textarea.\n\nSo, please, don't try fixing thing that isn't broken."
    author: "Ilyak"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "The gnome user comments here are very enlightening. They repeat the standard gnome myths, which thank god kde does not subject its users to:\n\n1. the interface is too complex for users (ok, ok, I believe that gnome users can't handle complex interfaces, that's fine, that's what gnome is for)\n\n2. the notion that removing all configurations and options is desirable.\n\nGnome can cater to its distorted view of what users can or can't handle, if they want to keep being second rate, and KDE hopefully will keep trying to simply be the best it can be, configurable, option rich, powerful.\n\nPlease kde devs, keep on producing a full featured, complex gui desktop that allows users like me, who can handle more than a tiny number options, to work efficiently, and not be crippled by crippled interface decisions.\n\nI, like other people commenting, use kate for session based project work, and I use kwrite to work on single files if I don't need a session. If I need something really simply, I use nano to edit directly.\n\nNormally I ignore gnome user comments since it long ago became clear to me that a desktop that aspires to mediocrity is not interesting to me, but since  this is a kde site, I feel that it's ok to not have to listen to such interface luddites espouse their nonsense."
    author: "lizardx"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Have you seen KDE4?  You're too late"
    author: "AS"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Nobody has seen \"KDE4\".  People have seen KDE 4.0.x and for some reason are assuming, against all historical evidence, that the direction of the entire KDE4 series can be judged on the basis of what they've seen so far - rather like proclaiming, on 1/1/1990, that \"OMG teh 90's suck!1\""
    author: "Anon"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> the notion that removing all configurations and options is desirable.\n\nI'm not advocating just the removal of options, but of the entire programming-related functionality because it is outside of KWrite's stated purpose  to be a simple general-purpose text editor.\n\nHaving said that, it seems some programmers and sysadmins here want KWrite just like it is and say that Kate is too much for some of their needs. Fine, move the current version of KWrite to the kdeadmin package but slim down the version of KWrite in kdebase."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "But KWrite already are a simple general-purpose text editor, and when used as such the impact of the entire programming-related functionality is nearly non existing. Keeping on repeating it does not make it so. For instance less than 10% precent of menu entries are programming related, it would be real hard to make up a use case where it actuall could be a problem."
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "> it would be real hard to make up a use case where it actuall could be a problem.\n\nProgramming functionality is outside of KWrite's goal. Average users (gramps, mom, cousin Ann) don't care about programming, and all those coding tools/options get in their way.\n\nAgain, programmers can use Kate, and if Kate doesn't work for them now, they should file wishes in bugzilla for the things they need. Taking over a general-purpose text editor like KWrite is _not_ ok."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: ">Programming functionality is outside of KWrite's goal.\n\nNonsens, it goals is to be a simple editor, witch it is. There is nothing in the goal of beeing a simple editor that demands it to be unfit for programming. As long as the programming functionality does not distroy the ability to use it as a simple editor, witch it it does not, there are no problem.\n\nSo your complaining is just plain nonsens, since KWrite meets it's goal by beeing simple. And to emphasize this, you can't construct one real user case where the programming functionality are a problem. That the average user don't care about programming does not matter since the functionality does not affect them in any way."
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "> As long as the programming functionality does not distroy the ability to use it as a simple editor\n\nWrongs premise: irrelevant options does detract from the use of KWrite because users have to wade through them. Not to mention they are unsightly.\n\nLook, if you want a programmer's version of KWrite, use Kate or fork KWrite and move the copy to kdeadmin."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "No, it's your premise that is wrong. \n\nFirst of there are NO options to wade troug, the programmer related options are a tiny fraction of the options in KWrite. And even more important, they are located so that a user in need of only a editor for pure text does not get hdetracted by them. There are no valid user cases where removing the fetures will make any gain whatsoever. \n\nSeccondly, there are valid user cases where a programmer may want to use KWrite rather than Kate. So by removing them you will case problems for valid use cases, while gaining nothing.\n\nAnd haveing two versions of KWrite are so stupid, that comments are unneccesary.\n"
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "> a programmer may want to use KWrite rather than Kate\n\nSo what are these reasons? I think that if Kate doesn't work for a programmer, he/she should file a wish in bugzilla or use something like KDevelop. KWrite should cover only *basic* text editing *without* niche functionality like progamming.\n\n> And haveing two versions of KWrite are so stupid\n\nWell if people are so attached to the current programming functionalityof KWrite, keep it in kdeadmin. But KWrite in kdebase should have no niche programming functionality."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-18
    body: ">So what are these reasons?\n\nThere are several, like working on single file programs you don't need the multiple file capabilities or sessons of Kate and it's preferable to not mix it with other projects. While working on big projects, where you work on one or more singel file, located in different part of the project. When doing system administrator task, remote or local, you usually work on single files, there are no need for Kates multiple file interface. When cecking out source code unrelated to your project to get a tip/idea how to solv a problem, or checking up on a header file.\n\n\n>I think that if Kate doesn't work for a programmer\n\nKate works very well for programmers, but there are times where you even as a programmer don't need Kate's functionality and KWrite simply are a better tool for the job.\n\n\n>KWrite should cover only *basic* text editing *without* niche functionality like progamming.\n\nWhy? Since you can't provide one single valid use case where the programming functionality in KWrite is a hindrance when using it as a basic text editor, you gain nothing by it.\n\n\n>KWrite in kdebase should have no niche programming functionality.\n\nWhy do you keep insisting that, it's pure nonsens. Its already established that this functionality does not affects KWrite's abillity as a simple editor for non programmers. Is it some kind of fetish you have, that the tool you use for pure text should not be usable for programming by others?\n"
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-18
    body: "> you don't need the multiple file capabilities or sessons of Kate\n\nHow IRONIC! You can't be bothered to use the \"default\" session in Kate because the the ability to have multiple sessions gets in your way, and you complain about \"multiple file capabilities\". And yet you want to stuff your favorite esoteric coding style options and configurations on non-techy users of KWrite for whom those options are completely irrelevant. Because obviously your programming fluff won't bother them, and even if it does those users don't matter anyway.\n\n> you usually work on single files, there are no need for Kates\n> multiple file interface.\n\nYou can very easily make Kate work on a single file by closing the file lister on the left side. After that it's as if you only have one file open.\n\n> Since you can't provide one single valid use case where the\n> programming functionality in KWrite is a hindrance\n\nSure I can: Geekish programming options are tiresome, confusing, and intimidating for non-programmers and get in the way of their doing simple stuff like spell checking, copy/pasting and printing."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-22
    body: ">esoteric coding style options and configurations on non-techy users of KWrite\n>for whom those options are completely irrelevant. Because obviously your\n>programming fluff won't bother them,\n\nExactly, those options you for some irrational reason hate so much, are in the singel digit percentage of menu options. So yes, they are so few they will not  bother the non-techy users. But it's features that have valid user cases and are indeed used by many existing users. And caused by of some kind of personal, and not founded in reality usability theory, you want to remove features used by many existing users. Removing a tool used by many to solve a non-existing problem, since those users don't matter anyway.\n\n\n>You can very easily make Kate work \n\nSo rather than having a simple tool best fitted to the task, you need to preform extra actions to make a more complex tool less so. Thats irony.\n\n\n>Geekish programming options are tiresome, confusing, and intimidating for non-programmers \n>and get in the way of their doing simple stuff like spell checking, copy/pasting and printing.\n\nThis is not even a user case, only theoretical musings. And its even less valid, since it's not applicable to the reality of this case. Since KWrites programming options are so few compared to the non geekish options, they will not get in the way or intimidate the non-programmers doing simple stuff. Of the non-programming users more than 99.99% will simply just ignore the few options they don't have a need for or don't understand.\n "
    author: "Morty"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "Oh, nevermind, I've missed your point entirely.\n\nI don't see why there's kate and there's kwrite. You're prodadly right, however we'll need to ask people who actually use kwrite.\n\nAlso: it would be cool if kwrite could embed into konqueror as textarea."
    author: "Ilyak"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-15
    body: "I use Kate for projects which involve sets of multiple files which do not change rapidly, while KWrite is my choice for editing single files one time to not mess up my Kate defaults. Also, I have different defaults for indentation in Kate and KWrite because of the different use cases.\n\n> Also: it would be cool if kwrite could embed into konqueror as textarea.\n\nYes, please! I hate this Embedded \"Advanced\" Text Editor without any real editing facilities."
    author: "Stefan Majewsky"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Yup, i'm too using both, kwrite and kate, in just the same workflow .\n\nBtw. it would really nice to have a Tree-View file system pane in kate, like quanta has."
    author: "Niklas"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> I hate this Embedded \"Advanced\" Text Editor without any real editing facilities.\nhttp://websvn.kde.org/trunk/KDE/kdebase/apps/konqueror/IDEAS?revision=659583&view=markup"
    author: "Stefan Monov"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "Why not using Kedit? Or another simple Texteditor (kde-apps.org) if you are confused by too much options? Why ripping an excellent program? To achieve the 10th simple one?\n\nIt would be a big mistake with sadly consequences for KDE to follow those false recommendations!\n\nI wonder who could be interested in this ...\n"
    author: "Niklas"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> Why not using Kedit?\n\nBecause the decision was made that KDE4 (specifically the kdebase package) will have only _one_ simple text editor that shared its infrastructure with more advanced editors in order to save memory. KEdit was therefore removed and labeled redundant (which it was), but now KWrite must live up to its goal to be simple.\n\n> Why ripping an excellent program?\n\nBecause KWrite's purpose is to be simple, and because KDE needs a simple editor in its base package order to fill the needs of ordinary, non-programmer users."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "> KEdit was therefore removed and labeled redundant (which it was), \n> but now KWrite must live up to its goal to be simple.\n\nThat is a really paradox! You got this, didn't you? \nKedit, the 'Simple Text Editor' was redundant - but now Kwrite has to change because it is not simple. Either the first or the latter can't be the truth.\n \n\n"
    author: "Niklas"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-16
    body: "There's no paradox.\n\nKEdit was considered redundant because its code duplicated a lot of kdelibs/kate/ and because no one wanted to maintain KEdit's code. The redundancy did not stem from KEdit's mission to be a simple text editor."
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "And you seem to forget that this is a volunteer project, who are mainly programmers and only maintain KDE because THEY find it useful. If you don't like it then get a third party editor that is simpler for you.\n\nOh, and yes there is a paradox, KDE has never been simple, just open KControl and look at the settings for Desktop>Window Behaviour for example, the fact that you are declaring that a highly configurable environment should have a inflexible, *non-configurable* text editor is just bizarre. If that's what you want, then use GNOME, from what I've heard, being \"simple\" is the whole reason GNOME continues to exist so why don't you just use your right to choose and choose that instead?\n\nAlso, to reiterate for emphasis: The only \"programming\" options are indentation management, syntax highlighting and code folding. Everything else IS useful for \"normal\" users, just because YOU don't find it useful doesn't mean no-one else does (Despite most user's complaints about bloat in MS Word, anytime MS removes a feature there are always more user complaints from people who actually did find it useful)."
    author: "What the hell"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "> And you seem to forget that this is a volunteer project, who are mainly\n> programmers and only maintain KDE because THEY find it useful.\n\nDifferent developers have different motivations. Some do it to because they are going to use the product themselves as you mention. Other devs do it so that users *unlike them* (ie. the rest of us) can use the software. Not everyone has selfish motivations :)\n\n> If you don't like it then get a third party editor that is simpler for you.\n\nWith that kind of attitude, KDE will stay the desktop of choice for the couple ~200,000 programmers of the world, while other Free Software systems will be used by the remaining millions. "
    author: "Tray"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-05-27
    body: "The desktop of choice is determined by it's text editor? Please, I think someone's getting more desperate by the minute.\n\nI've just used KWrite, and I've found nothing intrusive. I don't even qualify as a full, \"geek\", as the most advanced programming I know is Javascript, but for simple text editing KWrite hasn't done anything wrong for me.\n\nI searched for text with ease (found it easily in the edit menu without searching about for it. NOT INTRUSIVE)(now, without a separate pop-up box and instead with an integrated search bar. LESS INTRUSIVE).\nThe only somewhat geeky option in the file/edit menus was, \"convert to HTML\". In, \"View\", the least geeky option was at the very top (new window), while little else in there would have been useful (NOT INTRUSIVE - what the bloody-hell else could I be looking in that menu for?).\nIn, \"tools\", all the most usefull options came with pretty icons on the sides, making them clearly visible - LESS INTRUSIVE.\nOne thing I love to use - adding things directly from the menus to the toolbar, *removing the need to, \"sift through\" what little geek-speak there is in the menu time and time again*. LESS INTRUSIVE.\n\nI wouldn't even call this a, \"simple\" text editor, because simple text editors (notepad) don't get this easy to use! It's powerful and, from a none-geek perspective, it does everything I need and quickly. Another bonus, I can quickly make a HTML page in it because of the coder features, *which have no reason to be gotten rid of*.\n\n\nIf it ain't broke, don't fix it."
    author: "Madman"
  - subject: "Re: Model KWrite after GEdit, keep Kate for geeks"
    date: 2008-04-17
    body: "+1"
    author: "KA"
  - subject: "Thank you!"
    date: 2008-04-15
    body: "[...]There are plans to allow combined highlighting, which means mixing e.g. HTML and php syntax highlighting will be even more powerful[...]\n\nThat, along with correct indentation in php, is what I'm missing since 2, 3? years in kate."
    author: "Mariano"
  - subject: "Firefox"
    date: 2008-04-15
    body: "What makes IMHO Firefox great is the fact that they introduce new features which are useful instantly without much learning, installing extensions whatever and which don't clutter the interface or menus at all. In Firefox3 for example I can use the address bar for searching page titles in my history. No new control elements, no scripts, plugins, extensions - whatever. An existing feature made better. I'm sometimes missing this kind of innovation in various applications, including Kate. Some ideas:\n\n* Better look/grouping of open file list. Perhaps with headings for each server, spacing, sorting etc.\n* Intelligent editing functions working in the background like: If I modify a variable name two or three times let there be a non-obstrusive bar at the bottom (like the new find bar) saying: Click here to replace this name in the whole function/Click here to replace this name in the whole document.\n* Additional information popups reminding you of the number/order of parameters in user-defined (all currently open documents) as well as native functions. Option to go to say php.net/strpos for reference or to jump to declaration of user-defined functions\n* Automatically detecting if there are two similar looking documents currently open and being able to start KCompare\n\nNevertheless, Kate is already great without all those features. Big thanks to all the developers!\n"
    author: "Michael"
  - subject: "Re: Firefox"
    date: 2008-04-16
    body: "I think that most of your points require some quite subtle inference to get right, and it's really not in the scope of a text editor to do these kinds of things (I think we all agree that the office helper is very annoying).\n\nThis kind of application is a possible future use of nepomuk, and should really be a separate application. What would be interesting is if we had a \"What Next?\" applet in the panel or somewhere that always highlighted the actions that we're likely to want to do next. It would need to be un-obtrusive, and have some statistical inference/machine learning going on behind the scenes to make sure that it is useful rather than annoying."
    author: "alsuren"
  - subject: "Re: Firefox"
    date: 2008-04-16
    body: "The office helper is only annoying because 99% of the time it guesses wrong what you want to do. And it's annoying because it hides important information on screen. I propose a bar like the popup suppression bar in Firefox which doesnt hide anything and I think that a programming language is much simpler than a real language so there are some reasonable guesses you can make. In fact, text completion is already such a feature and not less obtrusive than what I proposed. Additional, you can always disable a specific feature. The problem with clippy is that it isn't really clear in what situations it will help you at all. You can only enable or disable it completely. And when you enter a question in natural language it cannot find most of the time a good answer because computers can't do that correctly anyway as we all know. One of the worst ideas Microsoft ever had..."
    author: "Michael"
  - subject: "Feed"
    date: 2008-04-15
    body: "Finally full feed for dot.kde.org, thanks!"
    author: "Petteri"
  - subject: "Scripting language"
    date: 2008-04-15
    body: "How about supporting a subset of elisp? Allowing much Emacs code to be used or ported."
    author: "Martin"
  - subject: "Re: Scripting language"
    date: 2008-04-16
    body: "elisp support would require a kross interface first, I assume. If anyone is interested in writing one, talk to Sebastian Sauer or whoever was last to commit something interesting to kross in svn."
    author: "alsuren"
  - subject: "Re: Scripting language"
    date: 2008-04-16
    body: "No, it wouldn't alsuren. Kate doesn't use Kross. KatePart is being ported to use QtScript, and though are plans to add Kross support for the Kate application, they are fairly distant at the moment.\n"
    author: "Paul"
  - subject: "\"* Collaborative editing\""
    date: 2008-04-15
    body: "In short: Oooh yeah ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: \"* Collaborative editing\""
    date: 2008-04-16
    body: "Collaborative editing is already possible in a branch of abiword using telepathy's tubes. If kate wants to do something like this, a good way to start would be to create a \"help editing...\" menu entry that will search for advertised documents on the local session bus. This would be useful for things like MPX[1] or ssh sessions. Getting the bridge working should be something that happens afterwards. (if you ask me, dbus bridging should really be done without the need for telepathy, but I could rant on that for ages)\n\n[1] http://en.wikipedia.org/wiki/MPX"
    author: "alsuren"
  - subject: "Kate, Bidi, and OpenType"
    date: 2008-04-15
    body: "To the Kate developers (or anyone knowledgeable):\n\n1. Are there any plans to complete bidirectionality support for Kate? Both KEdit and gedit have it, what is needed for this to happen to Kate?\n\n2. How does Kate process OpenType? Does it use Pango, or does Qt have its own layout engine?\n\nBest wishes\nKDE Wellwisher"
    author: "KDE Wellwisher"
  - subject: "Re: Kate, Bidi, and OpenType"
    date: 2008-04-16
    body: "1) Bidi already supported\n\n2) Qt has it's own engine"
    author: "maor"
  - subject: "notepad++"
    date: 2008-04-16
    body: "Easily my favorite text editor on any platform.\n\nhttp://notepad-plus.sourceforge.net/uk/site.htm\n\nIt is open source, and damned near perfect.  I'd love to see a straight KDE port of this, or to see Kate operate more like notepad++, or include some notepad++ features.\n\nThe only feature that notepad++ is lacking that I enjoy is easy comparison/diff/merge tools like WinMerge, or UltraEdit/UltraCompare.  Obviously, in Linux you have diff and patch at the command line, but having visual components for these features is pretty nice."
    author: "T. J. Brumfield"
  - subject: "Re: notepad++"
    date: 2008-04-17
    body: "Erm... Kompare (Development module)?\n\nhttp://en.wikipedia.org/wiki/Kompare"
    author: "Me"
  - subject: "Re: notepad++"
    date: 2008-12-10
    body: "sorry winmerge is superior, permit edit of files being written"
    author: "vivo"
  - subject: "Oh, vi mode~~~"
    date: 2008-04-16
    body: "I tried a lot to find the vi mode in Kate... It seemed that there used to be some work but not stable... Hope that one day this can come true. :)"
    author: "horse"
  - subject: "Re: Oh, vi mode~~~"
    date: 2008-04-17
    body: "Wooh, that would be THE ONE killer feature enabling me to get rid of gvim on both Linux and Windows and use kate instead!\n\ngvim's gui (that's all gvim is about in the first place) is somewhat rotten and feature incomplete. Also, it really lacks the possibility to change to the easy input mode in an instand. When co-workers try touch my editor, they're like pissed   after two seconds. With kate, I could just switch the input mode to something sane in an instant :)\n\nWithout missing powerfull keyboard-only tools vi (okay, loads of improvements from vim) offers.\nAND one would find vi input mode all over KDE, wherever KatePart is used!\n\nHellyeah, I bloody WANT THAT! Where is the donate button?!"
    author: "fhd"
  - subject: "Re: Oh, vi mode~~~"
    date: 2008-04-22
    body: "Google pushed the donate button:\nhttp://code.google.com/soc/2008/kde/about.html\n(\"Vi-like, modal editing mode for Kate\")"
    author: "Erlend"
  - subject: "Re: Oh, vi mode~~~"
    date: 2008-04-23
    body: "I feel like kissing this guy, can't wait for the summer (of code...) to end!"
    author: "fhd"
  - subject: "Search and replace in multiple files"
    date: 2008-04-16
    body: "Hello,\n\nDoes Kate have a search and replace for multiple files? I know I can do it in shell:\n\nfind -iname \"*.html\" | xargs grep -l \"hello world\" | xargs perl -pi~ -e 's/hello world/boum/'\n\nBut it's very complicated for what it does and I can't remember the syntax."
    author: "Batiste"
  - subject: "Re: Search and replace in multiple files"
    date: 2008-04-16
    body: "Tip: you can install the \"rpl\" command-line utility. There is a GPL version of it, at least Debian has it.\n\nrpl -R oldstring newstring directory/"
    author: "Benoit Jacob"
  - subject: "Re: Search and replace in multiple files"
    date: 2008-04-17
    body: "I agree. I don't care much about folding or sessions or vi mode (I use vi if I want vi mode!), but \"Find/replace in ALL OPEN files\" the most important nontrivial feature I look for in every programmer's editor. It's just a must if you want to do some serious refactoring (rename class/function) in a larger codebase. \n\nUnfortunately, Kate (and GEdit) don't have it, so they are out for serious programming for me. Usually I end up running MED or Textpad in Wine...\n\nKate developer, can you hear me...? Please, give me an \"[ ] in all open files\" checkbox in the Find/Replace dialog!!!\n\n\n\n"
    author: "Andras"
  - subject: "Re: Search and replace in multiple files"
    date: 2008-08-20
    body: "I strongly second this... and for exactly the same reason."
    author: "Doug"
  - subject: "Re: Search and replace in multiple files"
    date: 2008-11-10
    body: "Thirded!"
    author: "Matt Kantor"
  - subject: "looking forward to Kate+Vim"
    date: 2008-04-16
    body: "I'm a regular user of Gvim, but would love to have a KDE version instead.\nThere was an attempt to create one which fell apart some time ago,\nso I'm very pleased to see plans to pick it up again.\n\nPlease don't dumb down Kwrite !  After all, there is Kedit for beginners.\nKwrite is a nice medium between Vim & eg Leafpad: please leave it as it is.\n\nOtherwise & generally, thanks to the KDE team for a great desktop."
    author: "Philip Webb"
  - subject: "Re: looking forward to Kate+Vim"
    date: 2008-04-16
    body: "\"After all, there is Kedit for beginners\"\n\nNot in KDE4, there isn't :)"
    author: "Anon"
  - subject: "Re: looking forward to Kate+Vim"
    date: 2008-04-17
    body: "It will be, the Oracle told me."
    author: "reihal"
  - subject: "Re: looking forward to Kate+Vim"
    date: 2008-04-24
    body: "I've been looking and hoping for yzis (http://yzis.org) to come to fruitation, but it seems like it has stalled.\n\nYzis seems like \"the right way to do it\" in that it decouples it self from the interface and allows for kparting."
    author: "Ziling Zhao"
  - subject: "A wish"
    date: 2008-04-20
    body: "Can I make a a wish - a filesystem treeview in Kate? ^^;"
    author: "A KDE advocate"
  - subject: "Re: A wish"
    date: 2008-04-28
    body: "A wish I most heartily second!  It would be fantastic to have that for many of my real world use cases for Kate (code, web, song tabs, system notes, etc)."
    author: "Evan \"JabberWokky\" E."
---
A Kate Developer Meeting was held last weekend hosted by <a
href="http://www.basyskom.de/">basysKom GmbH</a> in Darmstadt to great success. Developers interested in improving <a href="http://kate-editor.org/">KDE's advanced text editor</a> met to shape the roadmap of Kate. An impressive nine attendees turned up including several new faces.


<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px;">
<a href="http://static.kdenews.org/jr/kate-meeting.jpg">
<img src="http://static.kdenews.org/jr/kate-meeting-small.jpg" border="0" width="300" height="188" /></a><br />
<small>From left to right: Dominik Haumann, Erlend Hamberg, Christoph Cullmann, Joseph Wenninger, Paul Giannaros, Leo Savernik, Christian Ehrlicher, Anders Lund (photo taken by Tobias Hunger)</small>
</div>

<p>On Saturday we started early and discussed the following agenda</p>

<ul>
<li>Scripting</li>
<li>Indentation</li>
<li>Kate Sessions</li>
<li>Extending the highlighting system</li>
<li>Collaborative editing</li>
<li>Text input modes (vi mode)</li>
<li>Minor topics: Search &amp; replace, text completion</li>
<li>Interface review</li>
<li>Simplifying KWrite</li>
</ul>

<p>In short, scripting support will allow us to extend Kate with little helper and indentation scripts. We rethought Kate's session workflow to better meet the user's needs. There are plans to allow combined highlighting, which means mixing e.g. HTML and php syntax highlighting will be even more powerful. Collaborative features was also a point which is especially interesting with regard to <a href="http://decibel.kde.org/">Decibel</a>. Another hot topic is the support of additional input modes (vi mode) for power users. Other work includes interfaces for e.g. line annotations, which can be used by KDevelop to show svn annotations inside the editor. Besides that, KWrite - the simple version of Kate - was stripped down to not confuse the users. Experts still can turn on the advanced mode to have a full featured KWrite application.
For detailed results please read the <a href="http://www.kate-editor.org/news/development-sprint-results">developer meeting protocol</a>.</p>

<p>Apart from the discussions there were other highlights like Kate running smoothly on Windows or basysKom's coffee machine. We are really pleased with the results of the meeting and plan to repeat it on a yearly basis.</p>

<p>Thanks to <a
href="http://www.basyskom.de/">basysKom GmbH</a>, the <a
href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.jowenn.net">Joseph Wenninger</a> for making this event happen.</p>

