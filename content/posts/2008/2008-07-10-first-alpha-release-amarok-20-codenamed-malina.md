---
title: "First Alpha Release of Amarok 2.0, Codenamed \"Malina\""
date:    2008-07-10
authors:
  - "lpintscher"
slug:    first-alpha-release-amarok-20-codenamed-malina
comments:
  - subject: "Nice work Lydia!"
    date: 2008-07-10
    body: "Let me be the first to congratulate our new release girl on her first release!\n\nNice work Lydia!"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Nice work Lydia!"
    date: 2008-07-10
    body: "Great news!\nNow let's wait for final release and pray for a k3b sometime in the future :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice work Lydia!"
    date: 2008-07-11
    body: "K3B and KAFFEINE. I just can't wait to see a Kaffeine that's based on Qt 4."
    author: "Bobby"
  - subject: "Re: Nice work Lydia!"
    date: 2008-07-12
    body: "I should send my mockup of Kaffeine for devs, I just need to finish it (tryed to do it next 9months, so its now already too late!)"
    author: "Fri13"
  - subject: "Re: Nice work Lydia!"
    date: 2008-07-10
    body: "Let me be the second :)\nLydia, you rock!"
    author: "ljubomir"
  - subject: "Kubuntu Repositiories?"
    date: 2008-07-10
    body: "Will there be Kubuntu repositories for hardy? \nI already have Beta 2 installed from launchpad, so dependencies shouldn't be that large a problem."
    author: "fabiank22"
  - subject: "Re: Kubuntu Repositiories?"
    date: 2008-07-10
    body: "You can use the Neon packages, they're up to date and working great."
    author: "Ljubomir"
  - subject: "Re: Kubuntu Repositiories?"
    date: 2008-07-11
    body: "Yeah, but they seem to ship their own kdelibs... I already have the 4.1 beta2-libs installed, and don't wanna install 250mb of libs just for an amarok nightly."
    author: "fabiank22"
  - subject: "Can't wait to try it"
    date: 2008-07-10
    body: "I don't mind the redesigned playlist that some people are wary about.  I think Amarok is one of the crown jewels of KDE.  I'm really excited about the upcoming 2.0 release."
    author: "T. J. Brumfield"
  - subject: "Interesting codename"
    date: 2008-07-10
    body: "\"Malina\" is raspberry in croatian. :)"
    author: "Dado"
  - subject: "Re: Interesting codename"
    date: 2008-07-10
    body: "Probably devs mean this: http://en.wikipedia.org/wiki/Malina\n\nAnd malina means raspberry in Russian too :-)"
    author: "Artem S. Tashkinov"
  - subject: "Re: Interesting codename"
    date: 2008-07-10
    body: "Also \"Malina\" is raspberry in Bulgarian too :)"
    author: "Plamen Terxiev"
  - subject: "Re: Interesting codename"
    date: 2008-07-10
    body: "Also in Polish :) Even funnier, it's related to http://en.wikipedia.org/wiki/Golden_Raspberry_Awards :)"
    author: "js"
  - subject: "Re: Interesting codename"
    date: 2008-07-10
    body: "Same in Czech ;)"
    author: "Otakar"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "And Slovak.\n"
    author: "Bobby"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "and slovenian"
    author: "toxic"
  - subject: "Re: Interesting codename"
    date: 2008-07-10
    body: "And Slovakia ;-)"
    author: "ojo"
  - subject: "Re: Interesting codename"
    date: 2008-07-10
    body: "And in serbian :DD \nDon't stop everybody, we have a couple more..."
    author: "ljubomir"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "Macedonian, Belarussian, Bosnian, Slovenian and Montenegrin?"
    author: "Kolla"
  - subject: "Re: Interesting codename"
    date: 2008-07-17
    body: "there isn't macedonian language :) or we in Bulgaria must speak in about 20 languages:)\neveryone who know what cockney is know what i am talking about! :)))"
    author: "bobo"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "Oh, and it thus is also connected with RazberrieTart, the official sidux Fangirl ;-)"
    author: "blueget"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "And in Ukrainian ;)"
    author: "Serzholino"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "In Polish too."
    author: "Kornel"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "Raspberry in polish?  does that make wood shine better??   :o)"
    author: "Ian"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "It's nothing in Dutch... ;-)\n\nBut wouldn't it make for a nice name?"
    author: "jos poortvliet"
  - subject: "Re: Interesting codename"
    date: 2008-07-11
    body: "\"Malina\" means raspberry in Serbo-Croatian (Srpskohrvatski/Hrvatskosrpski)\n\nhttp://en.wikipedia.org/wiki/Serbocroatian_language"
    author: "bios"
  - subject: "Debian"
    date: 2008-07-11
    body: "Amarok experimental repositories don't work in Debian :("
    author: "Ram\u00f3n Antonio Parada"
  - subject: "Amarocks"
    date: 2008-07-11
    body: "Thanks for the Neon packages!\n\nIts great, while I haven't understood the no playlist concept (and the alternative list view is a bit buggy) yet the technology looks pretty amazing.\n\nAre there specific reasons why Amarok is not on EBN?\n\n\n\n\n"
    author: "andy"
  - subject: "Re: Amarocks"
    date: 2008-07-11
    body: "http://www.englishbreakfastnetwork.org/krazy/reports/extragear/multimedia/amarok/index.html"
    author: "Mark Kretschmann"
  - subject: "ugly alpha"
    date: 2008-07-13
    body: "It maybe only an alpha, but am I the only one to think the interface is ugly?"
    author: "Todd"
  - subject: "Congratulations"
    date: 2008-07-13
    body: "My sincere congratulations to all of the Amarok team for the best jukebox-audio-player on any platform. \n\nNow prepare for my crash reports on the alpha :)"
    author: "juanjux"
---
The Amarok team have released the very first alpha version of Amarok 2, their upcoming series based on KDE 4. It features a completely redesigned interface, the PopUp Dropper, and the revolutionary "Biased" playlists. The <a href="http://amarok.kde.org/en/node/523">Complete announcement</a> is available on the Amarok Website. The Amarok team kindly asks you to report any problems you might encounter and submit patches to help make Amarok 2.0 a huge success.
<!--break-->
