---
title: "KDE at LinuxTag 2008 in Berlin"
date:    2008-06-07
authors:
  - "awolters"
slug:    kde-linuxtag-2008-berlin
comments:
  - subject: "congratulations!"
    date: 2008-06-07
    body: "congratulations to everyone for representing kde at linuxtag! i wish i had been there but maybe next time!!! i bet the audience was totally impressed when seeing kde 4 in action :-) oh by the way, juding by kde4daily (great idea by the way) you guys are just awesome!!!!! :-D keep it up!!"
    author: "andrea"
  - subject: "cool"
    date: 2008-06-07
    body: "Sounds like it was a blast ;-)"
    author: "jospoortvliet"
  - subject: "How about the slides?"
    date: 2008-06-07
    body: "I never have the opportunity to go to this kind of events but I am still interesting on what you guys are cooking around there, so there is any possibilities to take a look of the presentation slides?\n\nAnd guys, keep rocking!"
    author: "biquillo"
  - subject: "Re: How about the slides?"
    date: 2008-06-07
    body: "I second that :)\n\nI think there will be a video of Aarons keynote online via linuxmagazin.de .. not 100% sure though."
    author: "Tom"
  - subject: "Videos of keynotes now online."
    date: 2008-06-10
    body: "Want to see Aaron? Go to http://streaming.linux-magazin.de/archiv_linuxtag08.htm\n"
    author: "Thomas Zander"
---
KDE was very busy at <a href="http://www.linuxtag.de">LinuxTag</a> this year. We were present with two main booths - <a href="http://amarok.kde.org">Amarok</a> and KDE -
and a whole bunch of talks within our own track.  Additionally, Aaron Seigo gave a well-received keynote on Wednesday, painting a vibrant
vision of the desktop in a mobile world, and the direction KDE is heading in.
Read on for a more detailed coverage of the event.



<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://www.flickr.com/photos/troubalex/2553607954/"><img src="http://static.kdenews.org/jr/linuxtag-2008-akonadi.jpg" width="240" height="160" /></a>
</div>

<p>Till Adam
brought the features of Kontact to the audience on Thursday. On Friday morning Sebastian
Kügler opened the KDE track with an introduction to KDE 4. As Sebastian Trüg could not make it
to Berlin, Aaron jumped in again and talked about the Plasma basics. He was followed by Till
Adam who presented the possibilities of Akonadi's storage concept and the future of KDE's
Personal Information Management suite.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://www.flickr.com/photos/troubalex/2552789871/"><img src="http://static.kdenews.org/jr/linuxtag-2008-ellen.jpg" width="160" height="240" /></a>
</div>

<p>After lunch Ellen Reitmayr talked about usability concepts in KDE and the efforts the team
undertakes to incorporate them into the desktop and the applications. Franz Keferboeck gave
an overview of KOffice's apps and introduced the audience to the new features that will come
with <a href="http://www.koffice.org">KOffice 2</a>. The final applause for the day went to Lydia Pintscher and Harald Sitter for
their talk on KDE multimedia.</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://www.flickr.com/photos/troubalex/2553600744/"><img src="http://static.kdenews.org/jr/linuxtag-2008-aaron.jpg" width="250" height="167" /></a>
</div>
<p>On Saturday KDE was the topic in two more talks: Lydia Pintscher and Sven Krohlas focused on the
new features of Amarok 2, while Holger Schröder gave a presentation on the latest development of KDE
on Windows.</p>

<h4>Booths</h4>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://www.flickr.com/photos/troubalex/2553595164/"><img src="http://static.kdenews.org/jr/linuxtag-2008-stall.jpg" width="250" height="167" /></a>
</div>
<p>Both main
booths were well manned and even better visited. The interested crowd asked zillions of
questions and was very eager to see the latest features, goodies and eyecandy of KDE 4.1 which
we showed on all computers at the booth, on Linux, Mac OS X and Windows. Other visitors came by to
share ideas or suggestions, and altogether they gave wonderful feedback.</p>


<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex">
<a href="http://www.flickr.com/photos/troubalex/2552784679/"><img src="http://static.kdenews.org/jr/linuxtag-2008-stall-2.jpg" width="250" height="167" /></a>
</div>
<p>The booth
staff talked their heads off and tried to answer all questions, to satisfy everyone's curiosity
and sometimes even find a solution to the users' problems that popped up once in a while. The
booth staff were also supported most of the time by some of the developers who did a great job
dealing with bug reports right at the booth.</p>

<p>At the booths, members of the community met some of them for the first time. We welcomed new
faces to the family and formed new and inspiring working relationships with people from the
KDE community and beyond.</p>

<p>Besides these main booths, almost all distributions showed off KDE 3 and KDE 4 desktops, most
notably openSUSE, Kubuntu and also Fedora. In addition the <a href="http://www.bsi.bund.de">German Federal Office for
Information Security</a> displayed computers running Kontact on Windows as well as on Mac OS X.</p>

<p>Many thanks to all who helped and supported us and made this LinuxTag possible.</p>