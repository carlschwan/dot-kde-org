---
title: "KDE Commit-Digest for 10th August 2008"
date:    2008-08-21
authors:
  - "dallen"
slug:    kde-commit-digest-10th-august-2008
comments:
  - subject: "kio_bookmarks, rtl in plasma layouts"
    date: 2008-08-21
    body: "that kio_bookmarks story was awesome; it brings back memories of cutting my own teeth on kde code. thanks for highlighting this one, Danny, and thank you Xavier for the cool kio plugin!\n\nas for Diego noting that \"Plasma needs a layout manager that knows about right-to-left issues.\" that's actually a problem in Qt itself; the layouts are supposed to be aware of the direction, but they aren't. it's missing a few lines of code in the QGraphicsLayout classes to make this happen. so this problem really ought to be addressed in Qt (if it isn't already; i need to look again in the 4.5 branch) because it will affect all apps using QGraphicsLayout. fixing it with extra code in Plasma really isn't enough here.\n\nanyways ... cheers on another commit digest, Danny. your rate of output this last month has been insanely impressive, and massively appreciated =)\n"
    author: "Aaron Seigo"
  - subject: "Re: kio_bookmarks, rtl in plasma layouts"
    date: 2008-08-21
    body: "You know, I've been absent-mindedly typing bookmarks:/ in the minicli for years and I always vaguely wondered why nothing seemed to happen :-)."
    author: "boudewijn rempt"
  - subject: "Re: kio_bookmarks, rtl in plasma layouts"
    date: 2008-08-21
    body: "Yay! So can we now bookmark bookmarks? ;)"
    author: "Kevin Krammer"
  - subject: "Re: kio_bookmarks, rtl in plasma layouts"
    date: 2008-08-21
    body: "Heh, yeah you can.\nBut if you click on your bookmark to bookmarks:/ all the items in the places pane disappear..."
    author: "Jonathan Thomas"
  - subject: "Re: kio_bookmarks, rtl in plasma layouts"
    date: 2008-08-22
    body: "Hello\n\nI can't reproduce that bug. Can you please mail me a detailed way to reproduce it ?\nBy the way, any feedback (especially bad ones) and suggestions are welcome."
    author: "Xavier Vello"
  - subject: "Now that you mention kio..."
    date: 2008-08-22
    body: "Is the kioslave for bluetooth part of the KDE project or is it something separate? If it is part of KDE, have you got any idea of whether is going to be ported to KDE4 anytime soon? The only way I can think of accessing, say, my phone from within KDE4 is, well, using Konqueror 3. Bit of a inconvenience but at least it works, although it's the only KDE3 program I still keep using (save Amarok and k3b, that is)."
    author: "NabLa"
  - subject: "AAaaarggghhh"
    date: 2008-08-21
    body: "can't keep up with speed of digests..."
    author: "Borker"
  - subject: "Keep it up Danny!"
    date: 2008-08-21
    body: "You're almost caught up. :) Thanks for all you do.\n\n..and wow, less than one month into 4.2 development I'm already excited/impatient for the next release."
    author: "Jonathan Thomas"
  - subject: "RTL Problem"
    date: 2008-08-21
    body: "In KDE3, on every textbox, when the line was getting started with a RTL character, it was going RTL.but now the only places i have RTL is Kate and KOffice.casual text boxes do not have rtl support at all for me.I filled a bug report in Qt and the answer was that in Qt4 i should set a config file to set a 'shortcut' for RTL'ing lines, just like windows.I think KDE/Qt 3's RTL support was much better with automatically going RTL."
    author: "Emil Sedgh"
  - subject: "Phew - once Danny has caught up"
    date: 2008-08-21
    body: "get him coding - all the bugs will be killed in a month....       :o)\n\nThanks Danny and what are you taking??? We all need some...."
    author: "Ian"
  - subject: "Plasmoids?"
    date: 2008-08-21
    body: "Is it possible to write plasmoids in something other than C++? Are there any guides availible? I only seem to find one for C++ on kde.org.\n\nThis is where I'm looking.\nhttp://techbase.kde.org/Development/Tutorials#Plasma\n\nShould I look elsewhere?"
    author: "Oscar"
  - subject: "Re: Plasmoids?"
    date: 2008-08-21
    body: "For me, writing up a guide on how write C# and Ruby script engine based applets is a top priority, as I think a lot of people are interested in writing non-C++ plasmoids that can be downloaded from the internet.\n\nThe KDE svn trunk has a number of fixes for the Ruby and C# bindings that will be promoted to the 4.1.1 branch before that is released. In the meantime if you read here for a description of the basics:\n\nhttp://www.kdedevelopers.org/node/3560\n\nAnd you can build the Ruby and C# bindings in the KDE trunk, and don't mind asking on places like #kdebindings or #plasma on irc, or the kdebindings@kde.org mailing list it is possible to develop now."
    author: "Richard Dale"
  - subject: "Re: Plasmoids?"
    date: 2008-08-22
    body: "Thanks. I've had a quick look at your page and it looks like something I can use. Are there other \"script engines\" other than C# and Ruby availible? I know mostly Perl/Python myself."
    author: "Oscar"
  - subject: "Re: Plasmoids?"
    date: 2008-08-22
    body: "I must second this! Python has a big user base, maybe Richard could include it in his guide?"
    author: "Seconded"
  - subject: "Re: Plasmoids?"
    date: 2008-08-22
    body: "The problem isn't the lack of a guide. The python kross engine wasn't ready in time for 4.1 and is broken. :("
    author: "Jonathan Thomas"
  - subject: "Re: Plasmoids?"
    date: 2008-08-22
    body: "Python support for Plasma is being worked on for KDE 4.2.\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: Plasmoids?"
    date: 2008-08-22
    body: "and just for completeness ... the idea of ECMAScript (aka 'javascript') plasmoids is not only still on the table but will likely becomes one of the most useful (as is \"can be used in the widest variety of settings\") sets of bindings due to the overhead/performance and benefits that will come as we lay in the security framework for them."
    author: "Aaron Seigo"
  - subject: "Re: Plasmoids?"
    date: 2008-08-23
    body: "If you are german, you might be interested in the current linux-magazin issue.\n\nhttp://www.linux-user.de/ausgabe/2008/09/\n\nI did not take a look into it, but they have a howto-dev-plasma article in there."
    author: "gttt"
  - subject: "plasma-Bookmarks"
    date: 2008-08-23
    body: "Wouldn't it be good to have the kio_bookmark functionality (which really looks nice btw) as a plasmoid as well? You could place it on the desktop.\n\nIf konqueror was able to contain plasmoids on a startup-plasma-container as startpage, you could include the bookmarks-plasmoid there, too. Or in amarok or elsewhere. Maybe you could place amarok-plasmoids in the startup-plasma-container of konqueror, too? Hmm, just thinking loudly."
    author: "gttt"
  - subject: "Re: plasma-Bookmarks"
    date: 2008-08-24
    body: "actually, if you read the commit, it's alredy planned by the kio_bookmark author."
    author: "Vide"
  - subject: "Re: plasma-Bookmarks"
    date: 2008-08-25
    body: "Oh, indeed, actually I have missed this. Thanks :)"
    author: "gttt"
---
In <a href="http://commit-digest.org/issues/2008-08-10/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> support for Google Gadgets moves into kdebase. "Places" engine gets service support, and a new "Leave Message" Plasmoid for use with the Plasmoids-on-Screensaver project. More work on the "Weather" Plasmoid and "grouping taskbar", and an initial version of a menu applet for small form-factors, and a new applet to visualise the size of an IceCream compilation cluster. Work on the URL and breadcrumb navigator, and the "capacity bar" in <a href="http://dolphin.kde.org/">Dolphin</a>. A new "Sphere" effect in kwin-composite. More work on biased playlists, AFT, and a toolbox menu as a replacement for the applet browser in <a href="http://amarok.kde.org/">Amarok</a> 2.0. A "fully working" Twitter plugin in <a href="http://edu.kde.org/marble/">Marble</a>. Synonym and antonym modes working in <a href="http://edu.kde.org/parley/">Parley</a>. More work on handling RAW images in <a href="http://www.digikam.org/">Digikam</a>. Various developments in <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>, and keyboard shortcuts, colour scheme, and "export to HTML" work in the MessageListView project in <a href="http://kontact.kde.org/kmail/">KMail</a>. Beginnings of master pages support in <a href="http://koffice.kde.org/kword/">KWord</a>. Initial import of KDisplay and kio_bookmarks. Kreative3d renamed to SolidKreator. <a href="http://konversation.kde.org/">Konversation</a> 1.1 is tagged for release. <a href="http://commit-digest.org/issues/2008-08-10/">Read the rest of the Digest here</a>.
<!--break-->
