---
title: "Camp KDE 2009: Call For Presentations Deadline"
date:    2008-11-05
authors:
  - "wolson"
slug:    camp-kde-2009-call-presentations-deadline
---
The <a href="http://dot.kde.org/1224886228/">Call For Presentations</a> deadline for Camp KDE 2009 will be Friday, November 21.  Due to the compressed planning of this event, deliberation will be held over the weekend and selected presentations will be announced on Monday, November 24 so that travel decisions can be made.

<!--break-->
<p>If you are interested in giving a presentation at Camp KDE 2009, please answer the following questions and send the response to campkde-organizers@k<span>d</span>e.org:</p>

<ol>
	<li>Name</li>
	<li>Title</li>
	<li>Organisation</li>
	<li>Presentation Title</li>
	<li>Presentation Abstract</li>
	<li>Does Camp KDE have permission to record your presentation and post for viewing on a KDE website?</li>
	<li>Will you need your own laptop/portable device for your presentation or will a standard laptop capable of displaying ODF and PDF suffice?</li>
	<li>Are you interested in also leading a BoF (Bird-of-a-Feather) meeting on January 19 or 20 on this or another topic?</li>
</ol>

<p>Please do not wait until the deadline to submit a proposal.  The more time that the organizers have to review presentations, the more informed the decision will be.  We want to host the most balanced schedule and have the strongest presentations possible.</p>
