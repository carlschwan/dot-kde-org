---
title: "KDE Commit-Digest for 30th March 2008"
date:    2008-04-07
authors:
  - "dallen"
slug:    kde-commit-digest-30th-march-2008
comments:
  - subject: "Thank you!!"
    date: 2008-04-07
    body: "Hi Danny,\n\nthanks for the digest and congrats for these wonderful digests these last two years. I've read them all and it was worth it every time!\n\nBest regards,\nRonald"
    author: "Ronald"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "I hope you don't get tired of hearing the same message time and time again: Thanks Danny!\nMuch appreciated!"
    author: "peter"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "Yes Danny, your work is very much appreciated. Ever since I discovered the commit digest (about a year ago), I look forward to it every week. Amazing how you can create a summary each week from the absolutely immense amount of commits that are done every time. Thanks!"
    author: "pinda"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "Absolutely. A big thank you to Danny! It is just a pure joy to read Commit Digest every week and getting inspired by all the hard work KDE people do. And happy birthday to CD! It looks like I'm not the only one having a birthday on the same day :)"
    author: "Jure Repinc"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "+1"
    author: "Emil Sedgh"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "+2 - incremental adding :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "Now if I add 3, and the next person adds 4, it will grow like n^2.\n\nSounds like fun."
    author: "alsuren"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "+4 :D"
    author: "jospoortvliet"
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "+5 ;)\nThanks Danny!"
    author: "m."
  - subject: "Re: Thank you!!"
    date: 2008-04-07
    body: "+6 (!)\n\nBeen a pleasure to read"
    author: "R"
  - subject: "Re: Thank you!!"
    date: 2008-04-08
    body: "*7\n\n(n^2 is just too slow, time for n-factorial!)\n\nSeriously, thanks a million, Danny!\n"
    author: "T"
  - subject: "Re: Thank you!!"
    date: 2008-04-09
    body: "Yes, thanks a lot, the digest is always great :-)\n\n+8"
    author: "Thorben"
  - subject: "Re: Thank you!!"
    date: 2008-04-09
    body: "*8\n\nAlways great to read, thanks! :) As we are in the factorial-function now, we should also include all those KDE-Developers who are commiting.."
    author: "onlinelli"
  - subject: "Award"
    date: 2008-04-07
    body: "and the 'SVN Log Message Of The Week' goes to:\n\nJonathan Riddell: die die ktip, bwahahahah\n\nthis should be entered on theDots quotes list to appear in the footer =)"
    author: "Emil Sedgh"
  - subject: "Re: Award"
    date: 2008-04-07
    body: "+1"
    author: "Jonathan Thomas"
  - subject: "Re: Award"
    date: 2008-04-07
    body: "How about this one from Peter Penz:\n(must go for breakfast now, otherwise I'll eat my keyboard...).\n\n\nLove it ;-)"
    author: "jospoortvliet"
  - subject: "Interface redesign for KOffice?"
    date: 2008-04-07
    body: "Is KOffice going to use the ribbon interface like MS Office 2007? because that's what the screenshots make it look like. From what I can tell, the ribbon interface isn't the standard of KDE 4, so if it is used in KOffice, it will cause inconsistency."
    author: "yman"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-07
    body: "So far only Kexi uses that kind of ribbon-like interface, the rest of koffice uses different approach in the UI. Let's hope there will be a student for KOffice usability project and bring a nice UI to KOffice."
    author: "fred"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-07
    body: "There even had been a Koffice UI competition on this web site. There was a winner with an interesting concept. Neither I understand why the selected concept will not be implemented, nor I would agree with yet another student project without public control."
    author: "Sebastian"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-07
    body: "a) The koffice ui competion wasn't a student project\nb) We have implemented much of Martin's ideas -- and Martin himself is a valued KOffice developer now\nc) We have learned a lot in the meantime and have found we could use some expert help\nd) Why should anyone care whether you agree to anything or not?"
    author: "Boudewijn Rempt"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-07
    body: "I am sorry. I was completely mistaken. I got the impression someone suggested to \"hire\" some student who would design his own \"personal\" optimal UI. This was meant by \"public control\" - more eyes see more than just a pair of them meaning a winner of a competition would probably follow more advice than \"some\" student following ust his own plan. I didn't know that the idea origined in the KOffice team to improve the concepts received in the competition. So, please, forget my trolling. "
    author: "Sebastian"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-07
    body: "I was referring to this: http://dot.kde.org/1206119837/ ;)"
    author: "fred"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-08
    body: "Thanks. I missed that one. ;)"
    author: "Sebastian"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-07
    body: "Public control? Who do you think you are to ask the developers to be publically controlled?"
    author: "hans"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-12
    body: "Hello,\n\ndon't you or anybody else note the uselessness of the argument? KDE is Free Software developed in a public repository. Does that ring something?\n\nKDE makes a point about releasing software before it's ready. Does that ring something?\n\nWhy would you even start to think the issue of public vs. non-public exists when KDE is already 100% public all the time?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-14
    body: "Well it's called opensource not opendevelopment. The development ist not open to the public."
    author: "openSOURCE"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-14
    body: "If you don't believe me try to intgerate a patch in the upcoming 2.6.25 kernel. You will soon understand that not everyone is accepted as developer in a project and that is of course for good reason. So again: It's only the source that is open ."
    author: "openSOURCE"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-14
    body: "Well it's called opensource not opendevelopment. The development ist not open to the public."
    author: "openSOURCE"
  - subject: "Re: Interface redesign for KOffice?"
    date: 2008-04-15
    body: "Developement most certainly is open to public view.  \n\nAnyone can download a copy of the latest SVN.  And file bugs against it.\n\nOTOH, if someone develops something outside of SVN, then it is not open to the public.\n\nNote that even more open development would be an improvement in some cases."
    author: "JRT"
  - subject: "Back to 3.X"
    date: 2008-04-07
    body: "After using KDE 4 from 0.0 to 0.3 I returned to KDE3 and I have to say: Thanks god, it was already time!\nNot that KDE4 is horrible, but 4.0 series is so unstable I could not work on it without a Plasma crash, and Plasma was not saving my settings (added plasmoids, geometry, etc), so every time I logged in KDE I had to re-add the plasmoids :-P\n\nSo, two questions:\n- is there a way to force plasma save? I know that in 4.1 series plasma now save geometry often (there was a bug about this that I've read)\n- is there a way to use a dark kicker (much like the one in KDE4) without having to change all colors to deal with a dark background? (I mean having white fonts only in the panel, because black fonts over black is not nice to read).\n\nThanks in advance, and I'll wait for KDE 4.1.. or 4.2 :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "1)probably logging out before it crashes...\n2)yes there is.search for slim glow on kde-look.org, but for plasma themes that change color, you should wait for 4.1\n"
    author: "Emil Sedgh"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: ">1)probably logging out before it crashes...\nYah, and what happens if it crashes IN logout? (that is my case), ehehehhehe."
    author: "Iuri Fiedoruk"
  - subject: "Re: Back to 3.X"
    date: 2008-04-12
    body: "Just don't log out? ;-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: ">1)probably logging out before it crashes...\n- Yah, and what happens if it crashes IN logout? (that is my case), ehehehhehe.\n\n>2)2)yes there is.search for slim glow on kde-look.org, but for plasma themes that change color, you should wait for 4.1\n- I asked about kicker, not plasma panel :-D\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "KDE4 doesn't have Kicker."
    author: "Jonathan Thomas"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "And what is the title of my post? (back to kde 3.x)\nI want to know if there is a way to make kicker look like plasma panel (white fonts on black brackground) without making all windows use this same color scheme."
    author: "Iuri Fiedoruk"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "Configure Panel - Appearance. Not sure you can manage to make it look anything close to Plasma, Kicker was never particularly flexible. "
    author: "Morty"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "Make a plasma theme... Maybe KDE 4.1 will have color settings for plasma."
    author: "jospoortvliet"
  - subject: "Re: Back to 3.X"
    date: 2008-04-08
    body: "Sorry, they are not going to match color with system colors, like Aya theme?"
    author: "Emil Sedgh"
  - subject: "Re: Back to 3.X"
    date: 2008-04-08
    body: "You can fake a BG color for the kicker by specifying an image to use.  Getting its fonts to be white without white fonts everywhere else?  Well, you could probably change a line or two of code in kicker and compile it.\n\nand laffo at everyone assuming you are talking about the plasma panel in KDE4 :D"
    author: "AS"
  - subject: "Re: Back to 3.X"
    date: 2008-04-08
    body: "I had mine set up with a blank panel (semi-transparent) with white fonts on it. You need to play with the settings but as a general rule go through and turn everything 'transparent' then reduce transparency. That's the only way to get rid of the borders around elements on kicker.\n\nGive KDE4 another go when you get the time. It's settling quickly.\n\nThanks."
    author: "Martin Fitzpatrick"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "My kde4 installation occasionally crashes on logout too. When it happens I even lose the previously saved plasma-appletsrc file.\n\nWhen it doesn't crash, it comes back with the right widgets, but the geometry for the taskbar and sytem tray (both in desktop containment, not panel) isn't restored properly."
    author: "Soap"
  - subject: "Re: Back to 3.X"
    date: 2008-04-07
    body: "Just to clarify:\n<<is there a way to use a dark kicker (much like the one in KDE4)\n>>is there a way to use a dark kicker in kde3 (much like the plasma panel in KDE4)"
    author: "Iuri Fiedoruk"
  - subject: "caution, off topic post, save kde-forum"
    date: 2008-04-07
    body: "hi all\n\nplease save kde-forum it is invaded by evil spammers . \n\n\n\nfriendly"
    author: "mimoune djouallah"
  - subject: "Re: caution, off topic post, save kde-forum"
    date: 2008-04-07
    body: "wow that is getting spammed, and moderators seem to be slow in responding to it."
    author: "R. J."
  - subject: "Parsek"
    date: 2008-04-07
    body: "Parsek sounds great. \n\nKonquest is fun for a game or two but it's just too simplistic. I don't expect anything to rival the complexity of Galactic Civilizations II or Master of Orion III (which is actually a pretty good game with current fan patches and mods. Too bad it was a buggy, unplayable monster at release) but a 4X game with a bit more depth would be a great addition for KDE Games. :)"
    author: "ac is people"
  - subject: "Re: Parsek"
    date: 2008-04-07
    body: "I'm glad you like the idea of Parsek. I also don't plan it will be as complex as GalCiv2 aor MOO3, especially not when it comes to graphics. For now I plan it will just stay a nice 2D client. But there is nothing that prevents the games as complex as Stars!. If you have examine Thousand Parsec a bit you can see it is basically just some protocol made especially for space 4X games. So you can create a ruleset (the actual game rules) as complex (or as simple) as the protocol allows (or will allow in future version). So there should be no problem creating a ruleset similar to Konquest, GalCiv2 or MOO3 or Stars! or any other 4X game. Then I just have to worry to represent all the objects and orders and other stuff from the ruleset properly in the client."
    author: "Jure Repinc"
  - subject: "Re: Parsek"
    date: 2008-04-09
    body: "A good strategy game should force the player to decide between a number of trade-offs. If some feature doesn't add trade-offs it doesn't add to depth, it just makes the game more complicated. \nBad example: Weapons in MoO2/3. There was almost always one optimal choice and 20 progressively worse alternatives. Give each type of weapon one niche at which it excels\nGood example: Weapons/Shields vs. Stealth/Scanners in MoO3. Normally scanners and cloaks aren't that important but in MoO3 you could easily end up in situations where one side was unable to detect the other and couldn't even fire back. \nHow they screwed up anyway: The research system meant that normally one side was better in both weapons/shields and stealth/scanners. You had to be careful in fleet composition but a lot of potential was wasted.\n\n\n\nIn a 4X game the major elements should be\n- colonization vs. planetary improvement (i.e. no colony-rush at the beginning of the game where you try to colonize as much as possible and the player who gets the most planets wins. Colonies should be money sinks for some time before becoming profitable and there should be plenty of planetary improvements at the lower tech-levels that can grow your economy just as much)\n\n- tech vs. quantity - kinda related to the first item but in general you should be able to compensate for lack of size by better tech. This means specifically that tech development shouldn't be a linear function of the amount of money you spend. You should be able to make a few choices (perhaps cultural, like the domestic policy sliders in Europa Universalis) a big empire wouldn't wanna make (e.g. less oppressive regime) that compensate for lack of size and even without those there should be diminishing returns on tech spending.\n\n- diplomacy - AI that honors treaties and allies that actually help you in a war\n\n- espionage - You should be able to spend money on establishing a whole organisation in other empires instead of just sending a few assassins. The better your organisation the more you learn about the empire. At the highest level you can see as much information about the other empire as its player. GalCivII had a system like that and it makes espionage actually useful, not to mention much more realistic.\n\n\nNone of this requires much complexity or micromanagement. A Europa Universalis in Space would be nice. =)\nI'm gonna have a look at TP rulesets and how much flexibility they provide."
    author: "ac is people"
  - subject: "KDETV vs. UVC-VIDEO"
    date: 2008-04-07
    body: "Every single time I use Kdetv with my uvc-based webcam Kdetv doesn't want to exit properly and I must kill the application. Does this happens to me only?"
    author: "mark"
  - subject: "Re: KDETV vs. UVC-VIDEO"
    date: 2008-04-12
    body: "Happens for me too. Submit a bug report! (http://bugs.kde.org/)\n\nUVC is great, and ironically it's largely thanks to Microsoft! Now if only there were an equivalent standard for printers and wifi cards!"
    author: "Tim"
  - subject: "Mousegestures?"
    date: 2008-04-07
    body: "The rework of the hotkeys stuff reminds me of the mouse gestures.\nIirc, someone wanted to rework the mouse gesture recognition, and there were also several possible starting points for a new codebase. Is someone still working on it, even in a 4.1 timeframe? would be really cool to have working/renewed mouse gestures and also the \"rocker\" gestures known from Firefox and Opera.. would be really useful to have it for navigation in Konqueror for browsing the web. With that and the latest additions to Konqueror like sessions and restoring closed tabs Konqueror wouldn't need to hide itself from a feature point of view (the rendering engine already stands out), a browser that you could recommend and use without bad conscience.."
    author: "Anon"
  - subject: "Re: Mousegestures?"
    date: 2008-04-07
    body: "I saw someone ask maelcum (Andreas Hartmetz) about this about a month ago: It's currently \"on-hold\", and I think he is quite busy with other things.  It sounds like there is not much to do on the \"core\" part (although it is fairly \"advanced\" work), but there is a lot to do on the front-end part, with UIs for configuring gestures to be written, etc.\n\nI would love to see this work continued - it would even be damn handy in Kate and other apps! :)"
    author: "Anon Reloaded"
  - subject: "Re: Mousegestures?"
    date: 2008-04-08
    body: "Wait, wait, there are no mouse gestures yet in KDE4?  Obviously I haven't been using it, and stuff like this would be why.  I keep reading the commit digest though, waiting for the situation to change :)"
    author: "AS"
  - subject: "Re: Mousegestures?"
    date: 2008-04-10
    body: "Yes.  The gui is there though to give false hope :)"
    author: "JackieBrown"
  - subject: "Re: Mousegestures?"
    date: 2008-04-12
    body: "Can we implement pie menus too?  Qt has a pie menu class but only in the commercial version.  I dont think there is any kde specific code for pie menus, maybe I could commit some lines if I thought the work would be appreciated..."
    author: "Level 1"
  - subject: "Re: Mousegestures?"
    date: 2008-04-12
    body: "Why do you need someone else's confirmation for this? If you want this feature and will use it it's enough to justify working on it, and given the size of the KDE community there will be hundreds at least that will appreciate it once it's done (though I have no idea what pie menus are and if I'll be one of them)."
    author: "Andrei"
  - subject: "Comments on the dot"
    date: 2008-04-08
    body: "To follow up on the blog linked to by Danny: I'd like to suggest that this post be my last, and that the dot stop accepting comments.\n\nI'm not a KDE developer, so it's not like I'm proposing this because I can't take criticism of my work. It's because I can't take callous criticism of OTHER people's work without occasionally stepping in. So please, take mercy on the 98% of your users who love the work of the KDE devs/artists/packagers/translators, who feel compelled to wade through the morass each week just to try to restore some sense of decency and balance to the poisonous tone that has seeped in. Make that 98%'s lives easier by just shutting the dot down for comments.\n\nAll I want is a place to go to hear the latest news about KDE. We don't need comments for that.\n"
    author: "T"
  - subject: "Re: Comments on the dot"
    date: 2008-04-08
    body: "Despite the occasional poisonous comment, I actually really look forward to reading comments on the Dot. I think we would lose something special if we removed comments completely.\n\nWhat I would like to see is anonymous comments disabled, with an account required to post. This would also stop impersonation of posters.\n\nP.S. This is the real Danny ;)"
    author: "Danny Allen"
  - subject: "Re: Comments on the dot"
    date: 2008-04-08
    body: "100% agree. Disabling AC's, and maybe adding a moderation system.\n\nWe might just give Slashcode a try, it does all of that and more.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Comments on the dot"
    date: 2008-04-08
    body: "or do what LWN does and only allow paying subscribers to post comments -- that would help out two ways :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: Comments on LWN.net"
    date: 2008-04-08
    body: "LWN.net _does_ allow comments from non-subscribers as \"guests\". However, if you cannot read an article meant (initially) for subscribers, you cannot comment on it, can you? :-)"
    author: "gek"
  - subject: "Re: Comments on the dot"
    date: 2008-04-08
    body: "+1 Interesting"
    author: "T. J. Brumfield"
  - subject: "Re: Comments on the dot"
    date: 2008-04-08
    body: "Agreed. I posted something to this effect on the linked blog from the Digest. Anonymous posting is only needed when people are going to be \"outing\" something or posting on dangerous/contentious issues. The dot isn't for that... and I think removing the option to hide behind Anonymous postings would make people think a little harder about what they say.\n\nI know it works for me anyway."
    author: "Martin Fitzpatrick"
  - subject: "Re: Comments on the dot"
    date: 2008-04-09
    body: "\"What I would like to see is anonymous comments disabled, with an account required to post. This would also stop impersonation of posters.\"\n+1\n\nand also possibility of removing topic which is totally offtopic and also trolling."
    author: "Anon :)"
  - subject: "Re: Comments on the dot"
    date: 2008-04-09
    body: "Then \"Compositioning effects wishlist =)\" from this digest could be removed, it is just stupid offtopic"
    author: "Anon :)"
  - subject: "Re: Comments on the dot"
    date: 2008-04-08
    body: "The dot did get meaner, but I think it's because KDE was opened to a wider audience.\n\nIt's not too bad yet. I don't think banning comments would be a great idea, as some people, such as me. Rather just post on the dot, and have qualified individuals repost ideas I like on bugreports, etc.\n\nThis way it gets filtered down from a user perspective - the dot, to a programmers perspective - bug reports. Everybody wins..\n\n"
    author: "Max"
  - subject: "Re: Comments on the dot"
    date: 2008-04-09
    body: "your spam-topic \"Compositioning effects wishlist =) \" wasn't funy at all. Useless.\n\nIn bugzilla you have to think for a while and it would be more sane. "
    author: "Koral"
  - subject: "Compositioning effects wishlist =)"
    date: 2008-04-08
    body: "This list is just a wishlist that I've compiled from the last dot, and from suggestions for compositioning effects by others, and mine.\n\nDon't take this as an absolute wishlist. Think of it more as brainstorming that should get some effects programmers exited, and maybe they can come up with others  that aren't mentioned. (Obviously, there is no deadline, so I'm just gratefully if they'll be added to the KDE 4 branch \"eventually\" )\n\nFeel free to add your own ideas to it. (describe them as best you could.)\n\n_____________\n\n (this list includes effects currently under development, or already done.)\n\n*/ Rotate Desktop Cube (both normal and inverse) - Win Fish/KDE gears \"swimming\" in it.\n(neat idea: please add \"wet floor\" and fish swimming outside of the cube. As if the Cube were a fishtank, but strangely the fish swim on the outside. Maybe make some of the fish 3d Penguin \"Tux's\". I think that would be cool.)\n\n*/ A Compiz effect screensaver/\"relaxation effect\" for when people are \"brainstorming\" and would love their desktop cube interact with virtual environments. (neat idea: i.e.: Snowy hill -rolling down, or stuck on the side, floating in the ocean - like a message in a bittle, in a tropical setting - small lonely island with a lonely palmtree. Desktop is perched against the tree.)\n\n*/ A \"paper plane\"/ or other origami effect for minimizing applications or desktops.\n\n*/ An effect similar to \"magic lamp\" on minimizing applications.\n\n*/ A \"wet floor\" effect - Have fun with it, surprise us. (I know it\u0092s overdone, but many people expect to see it in a \u0093modern\u0094 desktop. \u0096 thank Apple, or whomever they first borrowed the idea from for that.. :p )\n\n*/ Expose - better than desktop cube, it allows all desktops to be seen side by side on a nice glossy surface. (aka.: \"wet floor\")\n\n*/ Effects such as \"Fire\" for when programmers/writers are frustrated and need to get rid of some frustrations.\n\n*/ Maybe another effect like the above where a user can throw items (neat idea: such as Toasters, Red swingline staplers, Mice, calculators, notepads, sharpies, and lots of other typical \"office equipment\" at the desktop) This would then leave \u0093dents\u0094 in the desktop background. -- That effect needs to clear easily and quickly, should the muse strike again and I'm ready to do work again.\n\n*/ A \"shuffle\" effect, where windows bump each other out of the way, when moved violently or brought to foreground. (they should have \u0093weight\u0094/mass)\n\n*/ A compositioning effect that reacts to the music that's playing using KDE's Amarok/Phonom engine. (could also be a screensaver) Make it do something cool with the desktop.\n\n*/ Effects that entertain the user - when he asks for them to be loaded. (DON'T BE \"CLIPPY\" and show up without asking)\n\n*/ A random effects generator. - for people who can't decide.\n\n*/ A way for emerald window decorations to work - or a similar easy to use engine for getting additional window decorations, mouse effects, etc.\n\n*/ The wobbly windows effect.                                  \n \n*/ A better \u0093minimize\u0094 effect. IIRC, Compiz calls it the magic lamp or something like that. It provides a better visualization when minimizing windows.\n\n\nDisclaimer: I realize many of these effects are less useful and more \"fun\", but that's what linux and desktop computing should be: FUN!!!!!!\n\nOthers please add effects you would like to see. Describe them as best you can, so creative 3d animators/3d effects programmers can incorporate them.\n\nExample 2:\nThe common \"over the shoulder look\" by interested non Linux users when one is using a Compiz enabled laptop is a VERY, VERY useful marketing tool.\n I've recruited at least 5 people in the last three months to the world of Linux and some to KDE because of \"DESKTOP - EYECANDY\".\n\nComputer users like to be visually stimulated. Open question: Why do you think French food is prepared in such a nice way. The eye enjoys the meal too, so to speak.\nDon't be wasteful with screen real estate either. Notice how big plates are when serving French food, and how little space is used by the actual food, leaving tons of room for \"whitespace\"?\n\n__________________\nLike I said before, please feel free to add your effect suggestions to this list.\n\n\n"
    author: "Max"
  - subject: "Re: Compositioning effects wishlist =)"
    date: 2008-04-08
    body: "> This list is just a wishlist that I've compiled from the last dot, and from suggestions for compositioning effects by others, and mine.\n\nUsing the dot for such stuff is so much wrong.\nPlease do NOT copy&paste them each dot article, it's pretty stupid (other than useless).\n\n> Like I said before, please feel free to add your effect suggestions to this list.\n\nThere's bugs.kde.org or kwin@kde.org for such things, stop flooding the dot."
    author: "Pino Toscano"
  - subject: "Re: Compositioning effects wishlist =)"
    date: 2008-04-08
    body: "*/ Expose - better than desktop cube,\n\nHave been there from the start, it's called Desktop Grid.\n\n*/ A compositioning effect that reacts to the music that's playing using KDE's Amarok/Phonom engine.\n\nGood old Noatun had a plugin like this(not composite, but you get the idea) since the KDE2 days. Not very usefull, but you could try it with wobbly windows to see the funny effect.\n\n"
    author: "Morty"
  - subject: "Re: Compositioning effects wishlist =)"
    date: 2008-04-08
    body: ">Disclaimer: I realize many of these effects are less useful and more \"fun\", but that's what linux and desktop computing should be: FUN!!!!!!\n\nDesktop Computing ist not about fun it's about providing a workspace.\nWork on the other hand might be funny sometimes, but it's not what it's all about either."
    author: "funny..."
  - subject: "Re: Compositioning effects wishlist =)"
    date: 2008-04-11
    body: "Sorry if you see it that way.\n\nI happen to enjoy computing. Anything that can reduce the daily grind and amount of boredom is welcome in my book.\n\nKDE finally managed to make computing \"FUN\" again. Why do you have to be such a pessimist about it?\n\nWhy do you think Apple has chewed away at Microsoft's marketshare? Because the company portrays using Apple products as fun and hip. Even if Apple users aren't hip. They like to think of themselves that way. \n\nWith KDE 4 Linux will finally be \"hip\" as well. It will stop being the foray of geeks and people that live in basements. (yes, I know horrible cliche :( ) Linux will finally be something cool people will WANT to use. \n\nI think that eye candy helps with that. Sorry you see it that way.\n\nI guess computing is supposed to be boring and tedious for you. Just please don't ruin it for the rest of us.\n\n"
    author: "Max . "
  - subject: "Re: Compositioning effects wishlist =)"
    date: 2008-04-14
    body: ">Sorry if you see it that way. [...] Why do you have to be such a pessimist about it? [...] I guess computing is supposed to be boring and tedious for you. Just please don't ruin it for the rest of us.\n\nActually I'm not that pessimistic as I may sound :)\nI don't know if you work for a living but if you do you might have noticed that PCs at work tend to be clean and somewhat boring. Save some artistic and hip industries noone uses apples for that. They are build just to get the work done. Nothing more nothing less. \n"
    author: "at work"
  - subject: "Re: Compositioning effects wishlist =)"
    date: 2008-12-28
    body: "They already have many of those suggestions out now in compiz fusion.\nI know for a fact that the Magic Lamp \"Genie Effect\" exists (I use it) as well as the airplane one.\nAlso, wobbly windows exists in fusion."
    author: "ThEgUrU"
  - subject: "mulit-threaded apps in kde4"
    date: 2008-04-08
    body: "hi everyone,\nhope that somebody does read this. i've some (serious) questions on kde4 (why get the troll-thread such attention?!). so i wondered if kde profites from multicores.\ni've already read about digikam, which is able to make use of several cores. so i wonder if there are more apps in kde4 which are able to use mulicore-cpus. perhaps someone with a broader knowledge can answer my question, thanks :)\n\nand what is the status about adaptive personal responsive? some rumors is going on^^"
    author: "asf"
  - subject: "Re: mulit-threaded apps in kde4"
    date: 2008-04-08
    body: "I believe so, KDE ships with a library called Threadweaver to let developers take advantage of multiple cores easier."
    author: "Skeith"
  - subject: "Re: mulit-threaded apps in kde4"
    date: 2008-04-09
    body: "We're planning multi-threaded support in KStars...stay tuned!\n"
    author: "LMCBoy"
  - subject: "Re: mulit-threaded apps in kde4"
    date: 2008-04-15
    body: "KDE normally has multiple processes running.  So, it would benefit from multicores.\n\nThen there are apps which use a lot of CPU time to accomplish one thing.  Best example is rendering an image.  These can be designed to use multiple parallel threads to do the single job faster.\n\nOther types of processes can be broken down into subroutines and vector operations where the compiler (if it wasn't C++) could generate multiple threads.  I am looking forward to the port to FORTRAN 2008. :-)\n\nSince C++ doesn't have subroutines or vector operations, threads need to be done by hand pending extensions to C++ that will allow creation of multiple instances of classes that can execute asynchronously or in parallel."
    author: "JRT"
  - subject: "Re: mulit-threaded apps in kde4"
    date: 2008-04-22
    body: "> Since C++ doesn't have subroutines or vector operations, threads need to be done by hand pending extensions to C++ that will allow creation of multiple instances of classes that can execute asynchronously or in parallel.\n\nWith Qt 4.4 there is QtConcurrent::run() which runs a function in a separate thread.\n\nhttp://doc.trolltech.com/4.4rc1/qtconcurrentrun.html#run"
    author: "cloose"
  - subject: "Slight error"
    date: 2008-04-08
    body: "Printer-applet (written in Python) replaces KJobViewer\n\n\nUnless I missed something in the k-c-d thread, I don't think Printer-applet has been included yet - it's just been suggested by Jonathan Riddell."
    author: "RandomGuy3"
---
In <a href="http://commit-digest.org/issues/2008-03-30/">this week's KDE Commit-Digest</a>: The menu item styles of the KDE 3 "Classic" menu return to <a href="http://en.opensuse.org/Kickoff">Kickoff</a>. GetHotNewStuff for KDE colour schemes. "Recently Visited" listings in <a href="http://www.konqueror.org/">Konqueror</a>. A new simplified hotkeys configuration module. The ability to print a "cheat sheet" of shortcuts. Automation plugin for scheduling checks, and GetHotNewStuff support in KLinkStatus. Support for Synonyms, Antonyms and False Friends in <a href="http://edu.kde.org/parley/">Parley</a>. Improved online play (through GGZ) in <a href="http://games.kde.org/game.php?game=ksquares">KSquares</a>. "Photocopy" functionality in Kooka. Various scripting improvements and integration in <a href="http://kross.dipe.org/">Kross</a> and <a href="http://plasma.kde.org/">Plasma</a>. Ability to monitor the input and output of processes, and support for pausing and resuming processes in KSysGuard. The ability to scale remote VNC desktops in KRDC. On-the-fly spell checking comes to <a href="http://kile.sourceforge.net/">Kile</a>. Work on a knotify-dbus-plasma bridge. A fullscreen KDE splashscreen, without flicker. Printer-applet (written in Python) replaces KJobViewer. Kubrick and Glimpse move to kdereview. KTip has retired. Various anticipated sanity enhancements to the default settings of a KDE desktop. The bugfix edition KDE 4.0.3 is <a href="http://dot.kde.org/1207154042/">tagged for release</a>. <a href="http://commit-digest.org/issues/2008-03-30/">Read the rest of the Digest here</a>.

<!--break-->
