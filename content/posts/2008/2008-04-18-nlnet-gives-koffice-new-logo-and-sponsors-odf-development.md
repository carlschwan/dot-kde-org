---
title: "NLnet Gives KOffice a New Logo and Sponsors ODF development"
date:    2008-04-18
authors:
  - "brempt"
slug:    nlnet-gives-koffice-new-logo-and-sponsors-odf-development
comments:
  - subject: "Well done"
    date: 2008-04-18
    body: "I have to say the logos look really nice!"
    author: "Harri"
  - subject: "Re: Well done"
    date: 2008-04-20
    body: "++\n\nThose Logos look very nice!\n\nEven more cool though is the fact that they are sponsoring getting proper ODF support in KOffice. \n\nThanks a lot!"
    author: "Joergen Ramskov"
  - subject: "Re: Well done"
    date: 2008-04-21
    body: "They could be paying the oxygen designers instead! (they already have done a great set of koffice icons!!)"
    author: "Autumn Autist"
  - subject: "Re: Well done"
    date: 2008-04-21
    body: "the purple colour is annoying though...."
    author: "she"
  - subject: "Wow"
    date: 2008-04-18
    body: "Those look great. The pink is a bit too saturated for my taste, but that's cool with me, not necessary to criticize small personal taste issues when everything is awesome otherwise. And the ODF sponsoring is just as nice to hear!\n\nI am so looking forward to KOffice 2 :)"
    author: "Jakob Petsovits"
  - subject: "Good job guys!"
    date: 2008-04-18
    body: "Great work guys! :-)\n\nI would prefer less shiny, but that's just my 2 cents :-)"
    author: "Dread Knight"
  - subject: "How would they look at smaller sizes?"
    date: 2008-04-18
    body: "Specially 16x16 and 22x22? There should be enough visible differences at these sizes."
    author: "OC"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-18
    body: "These are logos -- meant to use on websites, t-shirts, business cards and so on -- splash screens if KOffice ever gets slow enough that a splash becomes necessary,  not as application icons. So don't worry about scalability :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-18
    body: "HUH??  They aren't going to be used as application icons??? That doesn't make any sense.  It would make sense to use the koffice logo in the splash screen of all the koffice programs, but i sure hope that these other logos are going to be used as icons too.  The current icons are really sad."
    author: "BATONAC"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-18
    body: "I agree on that."
    author: "NabLa"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-18
    body: "> in the splash screen of all the koffice programs\n\nWe don't have splash-screens cause our apps startup to fast for them ;)\n"
    author: "Sebastian Sauer"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-19
    body: "be sure to keep it that way,\nit's any of the very good aspects of koffice."
    author: "Mark Hannessen"
  - subject: "Oh. Go on."
    date: 2008-04-18
    body: "I think these would make nice application icons actually: it could perhaps take a bit of work to ensure they are recognisable at the smaller sizes, but they're clean and fresh looking. New and shiny is what KDE4 is about :)"
    author: "Martin Fitzpatrick"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-18
    body: "I understand. But making application logo and icon different may confuse..."
    author: "OC"
  - subject: "Re: How would they look at smaller sizes?"
    date: 2008-04-21
    body: "In KDE, the icons are associated with the file type, not the application (this is stated on the Oxygen site). However, when you DO see something associated with the actual application (menu?, titlebar?), it would be a good idea to use the logo. Anything less would be confusing.\n"
    author: "Riddle"
  - subject: "These are logos, not icons!"
    date: 2008-04-18
    body: "I'm sure that some idiots who are not reading the text properly [*] will start to complain that these will not be distinguishable at small sizes. So just once again:\nThese images are designed to be LOGOS. They are not meant to be used as ICONS. So stop mumbling and screaming, and read the text!\n\n[*] Ok, I have to admit that that note is well hidden in the text and should be placed a bit more apparent - I myself first thought \"that will be tough to distinguish these as Icons...\". :)"
    author: "Blurbs"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-18
    body: "I think they'd be great as icons (even if requiring modification). If we're using them to brand the KOffice suite it would make sense to be consistent to avoid confusion.\n\nVery nice work & very professional looking."
    author: "Martin Fitzpatrick"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-20
    body: "Yeah, while I appreciate you can't just shrink these logos to make icons (at least, I think that would not work from a distinctiveness point of view...) I really like the way the consistency brings all the koffice apps together and makes them really look like the integrated office suite they are and i would like to see this reflected in the icons too - maybe the koffice 'K' with something representing the respective app's function?\n\nOh and great news on the ODF in koffice sponsorship :-)"
    author: "Simon"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-18
    body: "Sorry, I missed that part. But would it help if logos and application icons look  different? May be without the text portion of it they can become good application icons.\n\n"
    author: "OC"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-18
    body: "Actually I think it would be very easy to distinguish as smaller icons."
    author: "Ian Monroe"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-18
    body: "What about using only the content as logo? Omitting the circle could save some space."
    author: "Stefan Majewsky"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-19
    body: "That's a very good point actually: you could place the symbol on top of a document 'template' shape. For example, spreadsheet would show a mini page of cells, with the Kspread symbol over it."
    author: "Martin Fitzpatrick"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-19
    body: "We're thinking in the same direction. Must mean it's a good idea, right? ;)\n\nThe actual symbols don't say much, but I really like the idea. As logos, they're fine. However, as application icons it would be hard to distinguish the applications. I think a \"background\" would help very much - maybe show the symbol on top of the mimetype icon?"
    author: "Hans"
  - subject: "Re: These are logos, not icons!"
    date: 2008-04-21
    body: "Funny, I was thinking something similar, too. Omit the circle (and, of course, the name), and place it over the mimetype. Immediately recognizable, and associated with the logo.\n"
    author: "Riddle"
  - subject: "Nice!"
    date: 2008-04-18
    body: "Very nice! This is great news :-)"
    author: "Erlend"
  - subject: "Beautiful"
    date: 2008-04-18
    body: "Wow! Well done, that artist.\n\nI don't understand why the F for KFormula is upside-down, but overall I think these look very professional, very cool, very KDE4 :)"
    author: "NJ Hewitt"
  - subject: "Re: Beautiful"
    date: 2008-04-18
    body: "I think it may be intended to look like an \"=\" sign? Perhaps it would work rotated a little to the right, would look like less than or equal (sort of)."
    author: "Martin Fitzpatrick"
  - subject: "Re: Beautiful"
    date: 2008-04-18
    body: "I think it's a root sign -- see? Art is good when it gets people talking :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: Beautiful"
    date: 2008-04-24
    body: "Probably because of this:\nhttp://shop.fila.com/us/eng/\n\n"
    author: "openpi"
  - subject: "Some suggestions"
    date: 2008-04-18
    body: "I don't like saying this (I realise dot threads are getting ruled by trolls) but the logos are very unrecognisable and really don't communicate anything to me. The Koffice one is OK as a logo (I would prefer some hint that it is an office application if it were used as an icon) but the other ones don't really look like anything but a bunch of strange symbols.\n\nIt would be OK if there was only a couple of extra logos, but there are so many, they all look very similar and they don't communicate even the name of application, let alone what it does. This makes them very forgettable once you figure out what they are too. For instance, the Kugar one doesn't even look like a K or anything to me really; it looks a bit like a rotated fast forward sign with the chunk removed. How am I suppose to work out the difference between the Kugar and Kforumla logo by looking at them or remember which is which without serious effort?\n\nI'm very confused by the logo/icon comments; why would you want a logo for an application that is different to its icon? That would just be confusing and dilutes your branding.\n\nTo improve each logo/icon I would do either or both of these:\n\n* include something that indicates what the app does (e.g. a spreadsheet symbol in the kspread logo), like the openoffice icons.\n\n* incorporate the whole name of the application into the logo, not just one letter or a bit of one letter.\n\nPlease don't flame me. I've explained what I don't like, the reason why and have suggested improvements. Feel free to ignore my comments."
    author: "Bob"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "Actually, the name is part of the logo design, that's why it's colored and styled similar to the symbol."
    author: "Boudewijn Rempt"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "Doh ;) Thanks for the tip."
    author: "Martin Fitzpatrick"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "I think with the colour/etc. it would be recognisable \"enough\" if not for the sheer number of applications. But, I think part of the problem is that I don't have a clear understanding myself of the difference between all the applications and what I would use a particular one for. It's especially the case in the graphics apps (partly also as the names don't give a clue).\n\nMostly ignorance on my part: But is there an \"introduction/startup/use this to do this\" guide with KOffice? Genuine question.\n\n\n\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "I'm sorry to say so but I agree with all you said."
    author: "Thomas Walther"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "I fully agree!\nThe logos are cute, no question. They all obviously belong together fine.\n\nBut the symbolism does not remind me at all of what the applications are about.\n\nFor example\n* a small mathematical symbol (like x\u00b2) in a circle for KFormula\n* a Pencil in a circle for Krita\n* horizontal \"Gantt-like\" bars for KPlato\n* a tiny chart for KChart\n* a hard disk - symbol for Kexi\n* a small table for KSpread\n* a tiny beamer for KPresenter\n* a pen for Kword (o.k. everybody uses a pen... ;-)), curves for Karbon.\nAll that could be doable with the same or similar line styles and thicknesses and coloring, to retain the general look.\n\nAll of that in a circle, of course, and with the name of the application along with it!\n\nProbably this might be a suggestion to use as ICONS, eventually.\n\nWhy are logos and icons different animals? Look at Amarok!"
    author: "sulla"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "Rock on sulla!  I fully agree with this post.  All of it."
    author: "BATONAC"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "I fully agree too. Without something more, the icons don't mean anything to me and I couldn't understand what app I'm supposed to use for a given task."
    author: "framie"
  - subject: "Totally agree with Sulla's suggestions"
    date: 2008-04-19
    body: "Rock on!!! Great suggestions.\n\nCan we do it that way, - pllleeeeeeaaaaassssee!!! :)"
    author: "AD"
  - subject: "Re: Totally agree with Sulla's suggestions"
    date: 2008-04-19
    body: "+1"
    author: "Kerr Avon"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "I completely agree\n\nthe logo's hold no meaning to the applications they are meant to be about.  I do not use Koffice, and if I saw these logo's they would not inspire me to look into it, they are logo's that I would simply just ignore."
    author: "anon"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "The application brings meaning to the icons, not the other way around. The triangular red thingy that PDF has as it's icon isn't immediatly obvious what it is.\n\nPersonally I think the icons are beautiful and think that they should be used wherever possible. Well thought out and nice looking."
    author: "Oscar"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "Don't agree. Not every icon has to look like what the application does. Think about logos. How does the coca-cola logo look like? What does the acrobat-reader icon show? Why does the kde \"start-menu\" have a icon with an \"K\" and not an picture of a small menu?"
    author: "hgj"
  - subject: "Re: Some suggestions"
    date: 2008-04-26
    body: "Actually Coca-Cola is a very bad example in this context because the white on red \"logo\" only stands for _one_ product. All the other products (Fanta, Sprite, ...) by that company are quite different in shape, color etc."
    author: "Dave"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "I think (mentioned elsewhere in the thread) if we take out the middle symbol (from inside the circle) and place it over a recognisable document 'template' they would work great as application icons. Example I used is the KSpread arrows over a page of spreadsheet cells. But would also work putting the KPresenter \"Play\" over a presentation-like page.\n\nThis could work really well."
    author: "Martin Fitzpatrick"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "It bears pointing out that:\n* Krita basically is a pencil on a canvas in a circle,\n* A play button, when talking about office apps, isn't a particularly bad symbol for a presentation program,\n* W is bsaically the symbol used for MS Word, so obviously using that is a recognizable shape (though the fact that it is shared between those two programs might not be good).\n* The KChart icon basically *&#953;s* a small chart, it just has one line on it.\n\nBasically, all the applications seem to evoke evither their function or their name (Kugar, for example, has a pair of whiskers, basically). The only one that is a little weird in that regard is KPlato (as pointed out elsewhere, even KFormula has a square root)."
    author: "Shadowfiend"
  - subject: "Re: Some suggestions"
    date: 2008-04-21
    body: "I'd love this suggestion too!\n\nYes, people have pointed out that most logos don't look their part (Coca-Cola etc). Then again, those companies took years (and/or millions in advertising) to build brand-recognition and for everybody to know what they are about.\n\nAlso, most applications and companies don't have a whole series of icons. They stick to one. There's a difference between remembering 1 logo and 10 logos. Anybody could see one of them and know that it's a KOffice application, but they'd have to wrack their memory to remember which."
    author: "Valerie"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "> I don't like saying this (I realise dot threads are getting ruled by trolls) but the logos are very unrecognisable and really don't communicate anything to me.\n\n> Please don't flame me. I've explained what I don't like, the reason why and have suggested improvements. Feel free to ignore my comments.\n\nYou shouldn't apologise for not liking something.  I know there are some childish people who think the dot is a place that should be filled to the brim with praise and nothing else, and call people names, but ignore them.  Not liking something doesn't make you a troll.\n"
    author: "Jim"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "I second your well thought out comments."
    author: "Plemo Broll"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "\"Please don't flame me. I've explained what I don't like, the reason why and have suggested improvements. Feel free to ignore my comments.\"\n\nYour post was a good example of constructive criticism - something that is all too rare on the Dot, nowadays.  Please don't be afraid to post more :)\n\n"
    author: "Anon Reloaded"
  - subject: "Re: Some suggestions"
    date: 2008-04-18
    body: "These are good suggestions, but I would respectfully disagree with your original point -- that they are unrecognizable and therefore not effective.\n\nI agree that they don't necessarily mean much, and are not recognizable so far, but give it time.  Think about logos in general, and look at all the logos from Microsoft (one of the best marketing teams on the planet imho).  They really don't tell you much about the product.  They become recognizable as the product becomes known.  The only thing they have in common, is that they are unique and look nice.  \n\nSame as the koffice icons.  unique and look nice.  This is important when dealing with brand recognition.  Once you associate the brand with the logo, then you can recognize the logo with the brand.  Think about every logo out there.  How many truly tell you anything about the product?  Some, but not many."
    author: "rat"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "I see your point, but I have to beg to differ:\n\nWord looks like a W - and a sheet of paper\nExcel looks lik a sylized XL on a spreadsheet background\nPowerpoint looks like a Piechart which makes people think presentation\nAccess has a key, which is a symbol for locked/secured data\nOutlook looks like a clock - since it's primarily a calendar\nPublisher has a big P and looks like a Page layout document.\n\nEven Office newbies understand theses symbols.\n\nIcons/Logos should be self explanatory. Only if the company/product is big and well known it can afford abstract (albeit very, very pretty :) ) meaningless symbols.\n\nI also think that Logos and Icons should be RELATED. That helps a lot.\n\nPlease no flame!\n\nThanks for helping out with ODF btw.\n"
    author: "Anon"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "I think we will have to agree to disagree.  \n\nMy point is that if I saw a yellow clock, I don't think I would have considered that it was calendar program (outlook), but now that I have seen Outlook and the yellow clock a thousand times, I can associate the two.\n\n>Only if the company/product is big and well known it can afford abstract (albeit very, very pretty :) )\n\nI agree here.  No company starts out big.  And few companies change their logos significantly (key word) when they do get big.  \n\n>I also think that Logos and Icons should be RELATED. That helps a lot.\n\nDefinitely agree.\n\nHonestly, I don't know if I'm right or not, but imho, I think the koffice logos are very nice, and I certainly respect the position that they should look more relevant to what the program does, but I just don't agree."
    author: "rat"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "> Same as the koffice icons. unique and look nice. This is important when dealing with brand recognition. Once you associate the brand with the logo, then you can recognize the logo with the brand.\n\nI agree the logo doesn't need to look like what the application does; I would argue that it does help a lot when it does though (e.g. a logo that obviously looks like it's got something to do with TV will perk your interest if you're on the look out for TV stuff). This point aside, I don't agree the koffice icons are unique from each other. They look nice, but they all look very very similar. They don't look like object (e.g. a wolf, a dragon, a pie chart) that your brain will recognise and label, but random lines with random coloured bits. I could look at some of them, wait 1 minute and I wouldn't be able to pick it out from a line-up from other koffice icons. Most people are never going to be able to tell them apart. The amarok wolf is unique and memorable for example."
    author: "Bob"
  - subject: "I agree with you."
    date: 2008-04-19
    body: "please don't flame me either, but I agree with the post."
    author: "AD"
  - subject: "Re: I agree with you."
    date: 2008-04-19
    body: "Also the main post."
    author: "AD"
  - subject: "Re: Some suggestions"
    date: 2008-04-19
    body: "One factoid that many people have not taken into consideration is that in KOffice you can start KWord and end up editing a spreadsheet in it with almost the same power as if you would have started the spreadsheet app.\nThis goes for the editing of almost all content in just about all koffice apps.  So we are seeing that the apps themselves are just a guide towards your final output, or just that you feel more comfortable in one app vs. another.\n\nThis means that while the list of applications is long the difference between starting one application over another is not that big. I think that this is reflected in the logos very nicely.\n\nOver time this concept will only grow stronger in the vision of the koffice team. A logo is something that will be around for quite some time, so there is some looking into the future included here.  A logo is surely meant to outlive an icon theme."
    author: "Thomas Zander"
  - subject: "Re: Some suggestions"
    date: 2008-04-21
    body: "Completely aggree with OP."
    author: "Anders E. Andersen"
  - subject: "Cool. Hope they become icons"
    date: 2008-04-18
    body: "They look great.  These logo (which I am sure would look good as icon) follow a specific style which great, the user would able see these applications are part of the office suite."
    author: "Michael Wright"
  - subject: "Logos, icons and brand recognition"
    date: 2008-04-18
    body: "Hm. I actually quite like the logos from an artistic/designers point of view. \n\nBut:\nHow are the application icons supposed to look? If these are specifically *logos* and not *icons* - how are the two going to relate? \n\nNormally, icons and logos are quite similar, which is a Good Thing (tm) from a branding and brand recognition point of view. \n\nMaybe the same overall design, but with a symbol related to the actual application in the center of the circles (i.e. the same colors as the logos for each application, but with e.g. a page of text for kword or a spreadsheet symbol for kspread in the center of the circles instead of these unrelated symbols)?"
    author: "Frando"
  - subject: "Re: Logos, icons and brand recognition"
    date: 2008-04-18
    body: "Yes I agrea totaly, I have no idea what I shoud do with the oxygen koffice icons we did....\nShould I just redo this in icon sizes with some nice outline??? "
    author: "nuno pinheiro"
  - subject: "Re: Logos, icons and brand recognition"
    date: 2008-04-18
    body: "Why don't you try and see what comes out of it? No one better than you for this task, Nuno."
    author: "NabLa"
  - subject: "Re: Logos, icons and brand recognition"
    date: 2008-04-19
    body: "I think the Oxygen icons are great, and I could imagine making sense of them without a key. My vote is to stick with them."
    author: "Anon"
  - subject: "Re: Logos, icons and brand recognition"
    date: 2008-04-21
    body: "With this logo \"icon\" I can't see clearly for what the hell the application for. It has to have an indication of for what is for in the icon..."
    author: "Autumn Autist"
  - subject: "Re: Logos, icons and brand recognition"
    date: 2008-04-21
    body: "With this logo \"icon\" I can't see clearly for what the hell the application for. It has to have an indication of for what is for in the icon..."
    author: "Autumn Autist"
  - subject: "Re: Logos, icons and brand recognition"
    date: 2008-04-23
    body: "Why should you need to do anything with the oxygen icons? They mostly look good and work, so why change. \n\nIf you really want to make changes. Then perhaps including the colours from the logos into the icon some subtle way, to get a bridge from icon to logo."
    author: "Morty"
  - subject: "nice!"
    date: 2008-04-18
    body: "These are fantastic! Well done!"
    author: "p.daniels"
  - subject: "Re: nice!"
    date: 2008-04-19
    body: "very nice logos indeed, a pleasure look at! congratulations"
    author: "peter"
  - subject: "Awesome!"
    date: 2008-04-18
    body: "The new logos are awesome!\n\nVery simple, cool, distinct and recognizable.  Nice! \nThe only thing is... they're not that intuitive. \nAlso, there must be only so many variations available.  What happens in a few years when new apps have been added to the suite?\n\nBut overall, congratulations! \nNow we'll just see how good the applications have become.\n\n"
    author: "Yeah Right"
  - subject: "Pink?"
    date: 2008-04-18
    body: "Pink is really a bit too much for me. I'd prefer sth. a little less obtrusive. Otherwise they look really great though."
    author: "Michael"
  - subject: "Re: Pink?"
    date: 2008-04-19
    body: "That is purple, not pink.  And the purple is dangerously close to magenta territory.  I think we could choose a regal shade of purple."
    author: "T. J. Brumfield"
  - subject: "Re: Pink?"
    date: 2008-04-19
    body: "Mmm. Looks pink on my monitor. Oh, well."
    author: "Michael"
  - subject: "Cluelessness"
    date: 2008-04-18
    body: "I'll go with the crowd.\n\nI have no idea what some of these apps do, and the logos, while cute(and weird), even make me scratch my head trying to figure what are we talking about.\n\nIf you say it's a office suite, okay, I figured it out by reading KOffice, then you have KWord, KSpread, KPresenter, KChart and KFormula, I might have a little idea about what they do, I'm not so sure about the last two. Kivio sounds like Visio, so it might also be a diagramming software.\n[/cluelessuserwithsomeofficeexperience]\n\nOkay, for real, I use Krita all the time, Kexi is the \"database program\", Karbon 14 is the \"vector graphics editor\", and I've learned what KPlato and Kugar are from the news.\n\nI'll not nitpick about the Knaming_scheme, it's been overly done, but where's the marketing team to FLOOD everyone with awareness of what KOffice 2.0 is?\n\nI do believe it can take the 1st place for the best Open-source Office suite. On the other hand there's not much time until July if you want to have a simultaneous release with KDE 4.1.\n\nPS: A little nitpick from my dirty mind: Kugar's logo looks like a dick. (no offense)"
    author: "Bruno Laturner"
  - subject: "Re: Cluelessness"
    date: 2008-04-18
    body: "Aw, a dirty mind is, famously, a joy forever. But I don't think we'll have a simultaneous release with KDE 4.1 -- there remains too much to be done before that."
    author: "Boudewijn Rempt"
  - subject: "Re: Cluelessness"
    date: 2008-04-18
    body: "Heh, how do you like the Krita one, then?  ;-)"
    author: "Inge Wallin"
  - subject: "Naming schemes"
    date: 2008-04-19
    body: "Very few apps have names that immediately spell out what they are.  Photoshop and Word being great examples of the exceptions, but what does Nero do, or Napster?  Everyone knows, yet people insist that the only way people will understand a program's name is if it plainly describes what the software does.\n\nDid people know what Google meant?  Today it is a verb for searching.  Pick a name you like, and then establish the identity of that name with a great app."
    author: "T. J. Brumfield"
  - subject: "Re: Naming schemes"
    date: 2008-04-19
    body: ">what does Nero do\n\nBurns rom(e). :)"
    author: "138"
  - subject: "Re: Naming schemes"
    date: 2008-04-24
    body: "That's always been one of my favorite software puns.  Someone was thinking way too hard when they came up with that name.  And the old Colosseum on Fire icon was excellent too."
    author: "Tim"
  - subject: "Like the Logos"
    date: 2008-04-19
    body: "I like the logos. They're really nice.\n\nNot quite self explanatory, and very similar to each other. But after some getting used to, I think we'll really like them.\n\nCould you guys update the KDE - logo for KDE 4.1 too? I'm curious to see what it would look like. Then if more people like the new logo than the old one, we could switch. Either way I'd like to see a KDE logo in the theme of your logos. :)"
    author: "AD"
  - subject: "Re: Like the Logos"
    date: 2008-04-21
    body: "Agreed, the KDE logo looks a bit old-style, I'd like to see what they could do with it :)"
    author: "Cypher"
  - subject: "Yaaaay: ODF"
    date: 2008-04-19
    body: "I really like the idea of colour-differentiating the apps by their tasks.\n\nBut the main joy I find in this post is the sponsored ODF support! Now *that* is good news!\n\nIf this were icons (which it doesn't seem like that's the case): why not just use a different iconset? ;)"
    author: "Matija \"hook\" Suklje"
  - subject: "New logo: Undesirable icon meanings"
    date: 2008-04-19
    body: "OK, as a designer I feel I need to point out a couple of obvious things that seem to be being overlooked here. The visual style of the new logos is very shiny and attractive. This is a good thing (tm).\n\nGiven that they look like play/stop/ff/rewind buttons on a media player its worth looking at what they suggest and if it is what we want:\n\nK-Office - Looks like the classic \"Go back one chapter/track\" button. Uh, don't we want people to feel they're going *forward* with K-Office?\nk-Word & K-Spread - Quite unique. Good. Especially the word one which clearly looks like the \"W\" in word processor. These work nicely.\nK-Presenter - \"Go forward 1 chapter/track\" could be OK as presentations are often to impress, which is compatible with the \"go forward\" idea.\nKexi - Uh oh. Looks like the \"Quit\" or \"Shut down\" icon. (See the top right corner of any window with Oxygen for reference).\nKarbon - This is the classic \"Eject\" icon! Does any software maker (other than CD-ROM burning) want to be associated with \"Eject\"?\nKrita - I love Krita. I want it to do well. Does an icon that points *down* and *backwards* suggest a great app?\nKivio - See Krita, but going down faster... :(\nK-Plato - No media player references. If anything the square looks like the classic \"photographers 2 finger, 2 thumb rectangle\", suggesting to see clearer. I'd say this works well with project planning ie \"See your project more clearly.\"\nK-Chart - Why not just 3 vertical lines? Yes, I know it doesn't have the \"V\" in it, but it would more clearly represent a pie chart. Form follows function.\nK-Formula - Why not just an = sign? See K-Chart above.\nKugar - Looks like it's trying to mimic a Cougar footprint. Fair enough. A cougar fits with an \"Agressive business\" report concept. Why upside down though?\n\n\nAll in all, it's definately a huge step up for K-Office (yay!) and many of the icons (K-Word, K-Plato) work really well. A number though give a very clear belittling image that I suspect the original designer missed, especially K-Office, Kexi, Karbon 14, Krita and Kivio.I'm hoping with the visual style established and the files available though, that other designers can upgrade these icons, but keep the style before they stick too rigidly. Overall though, the style is great. The 1 colour per set of apps is nice. And it's all definitely a huge improvement. The question is... will the community be allowed to improve them further? :)"
    author: "bugsbane"
  - subject: "Branding"
    date: 2008-04-19
    body: "+1. I agree that while the logos look professional per se, the icons will hopefully be more reflective of the programs' functions. \n\nI'm taking a course in marketing, and was thinking of how a free project like KOffice could improve its branding. So I thought it might be a great idea to rename KOffice to KeyDocs/KeySuite/Keystone Suite/KeyOffice, offering KeyWord, KeyCell or KeyGrid (for kspread), KeySlide (kpresenter), Kexi (leave as is, or maybe KeyBase), Kreska (instead of Karbon - it means \"line\" in Polish, perfectly representing the vector editor nature of that app), Krita (great name already), Kivio (ditto), KeyTeam or KeyPlato (for KPlato), KeyChart, KeyMath (for KFormula). With such consistent branding where the meaningless K is replaced with something concrete that users can idenify with, we could even have a tagline \"unlock your potential\", or we could emphasize that it's free with \"unlock your documents\". \n\nOf course, I realize people may be attached to the old branding and names, or alternatively someone will suggest that people choose programs based on functions rather than catchy names, but if my suggestions were to give anyone ideas that's great. What do people think?"
    author: "Darryl Wheatley"
  - subject: "Re: Branding"
    date: 2008-04-19
    body: "I felt compelled to express that I believe your ideas are phenomenal.\n\nFurthermore, I would like to express my support for the idea that it is confusing to have icons that are different from the logos, and both should somehow represent the function of the program.\n\nThat said, I'm really impressed by the KOffice logo. The rest really just don't make sense to me."
    author: "The James"
  - subject: "Re: Branding"
    date: 2008-04-19
    body: "Very nice ideas. I'm not sure about the using Key on some and not on others, but it is difficult to think up snappy enough names using it.\n\nVery interesting anyway, thanks for the ideas."
    author: "Martin Fitzpatrick"
  - subject: "Re: New logo: Undesirable icon meanings"
    date: 2008-04-19
    body: "Maybe the logos are meant for a RTL audience? ;)\n\nI didn't like them at first, but I think the simplicity rather makes sense."
    author: "Ian Monroe"
  - subject: "Re: New logo: Undesirable icon meanings"
    date: 2008-04-21
    body: "> KOffice - Looks like the classic \"Go back one chapter/track\" button. Uh, don't we want people to feel they're going *forward* with KOffice?\nActually, my first impression was that it looked like a K.\n> Kexi - Uh oh. Looks like the \"Quit\" or \"Shut down\" icon. (See the top right corner of any window with Oxygen for reference).\nActually, again, I think it's meant to look like an X (Kexi).\n> Karbon - This is the classic \"Eject\" icon! Does any software maker (other than CD-ROM burning) want to be associated with \"Eject\"?\nAre you sure it's not an up arrow?\n> Krita - I love Krita. I want it to do well. Does an icon that points *down* and *backwards* suggest a great app?\nIt's a paintbrush.\n> Kivio - See Krita, but going down faster... :(\nObviously a V, not a down arrow.\n"
    author: "Riddle"
  - subject: "Only one logo is needed"
    date: 2008-04-19
    body: "IMO:\n\nKOffice is *a single product* and therefore needs only *one* LOGO.\n\nInside this single product we have *many applications* which all need different ICONS.\n\n--> The logo can be very abstract.\n(but not too abstract since your product is not that well know)\n\n--> The icons should be less abstract.\n(for productivity)\n\nHaving different logos for each app is just plain silly. But if you really have to go there do NOT make the logos and icons different. That would be even more silly (and confusing for users).\n\nThis is the way its done elsewhere. If you don't believe me just take a look at any other multi app products (e.g. OpenOffice) or even single products where the logo is almost always also used as the icon (e.g. Adobe Reader)."
    author: "trg"
  - subject: "Re: Only one logo is needed"
    date: 2008-04-19
    body: "A more apt parallel would be the Adobe Creative Suite, in which all programs have different logos. The Creative Suite is often sold as a full single product, but that doesn't mean the programs aren't individually useful. Especially when people expect things to be divided like this, it useful to do so."
    author: "Shadowfiend"
  - subject: "Re: Only one logo is needed"
    date: 2008-04-20
    body: "Ok. Never used the suite but I bet the logos are also used as app icons."
    author: "trg"
  - subject: "so..."
    date: 2008-04-19
    body: "so if someone sponsors art work does it have to be accepted? i could reiterate all the reasons why these are unsuitable but we've all read the posts above."
    author: "anon"
  - subject: "Re: so..."
    date: 2008-04-19
    body: "i like the branding the the \">\" Sign in every Sub-Product."
    author: "qwq"
  - subject: "Re: so..."
    date: 2008-04-20
    body: "It *has* been accepted by the people that get to decide: the people working on KOffice. "
    author: "Andre"
  - subject: "i don't get them"
    date: 2008-04-19
    body: "Well, they do look kinda nice but i just don't get them. so i think they suck as icons, the one for koffice is good though"
    author: "gunter"
  - subject: "Application icon = file type icon + logo"
    date: 2008-04-20
    body: "I suggest to compose the application icons as follows:\n1. Take the standard icon of the file type that the application works with.\n2. Put (a part of) the application's logotype on top.\n\n(I do not know whether the ODF file type icons are standardized on freedesktop.org or in the ISO standard.)"
    author: "Erik"
  - subject: "Repeated logo..."
    date: 2008-04-20
    body: "I notice that Kformula has the same logo as Kword - except for the color. This is NOT a good idea for the visually impaired. I'm sure it's quite easy to find a close alternative."
    author: "Nuno Pinh\u00e3o"
  - subject: "Re: Repeated logo..."
    date: 2008-04-20
    body: "For someone who is so much into visual design as you are, I am a bit dissapointed that you do not see the difference. They are NOT the same. The difference was immediatly obvious to me."
    author: "Andre"
  - subject: "Re: Repeated logo..."
    date: 2008-04-20
    body: "nuno pinh\u00e3o not pinheiro :) that was not me :P"
    author: "nuno pinheiro"
  - subject: "Re: Repeated logo..."
    date: 2008-04-21
    body: "Hoops! right...\nBut at least they are close enough to fool me at first sight. And I am sure I'm not the only one. \nWhich IMHO is a reason enough to improve.\nAs for the fact that the name is included in the logo, off course that counts also to identify the application, but probably the logo image will be used for icons and quite probably the name will be left out of the icon. \nConsidering the size of an icon and the difference between the two image logos is small, I do thing it would be better to change one of them."
    author: "Nuno Pinh\u00e3o"
  - subject: "Re: Repeated logo..."
    date: 2008-04-21
    body: "\"but probably the logo image will be used for icons and quite probably the name will be left out of the icon.\"\n\nEr, no, probably none of these things will happen."
    author: "Boudewijn Rempt"
  - subject: "Re: Repeated logo..."
    date: 2008-04-20
    body: "Er, no -- apart from the difference in the symbol, there's a much bigger difference: underneath the KWord logo the text reads \"kword\" and underneath the kformula logo it reads \"kformula\" -- the text is part of the logo."
    author: "Boudewijn Rempt"
  - subject: "Bad choice"
    date: 2008-04-20
    body: "This was a bad choice. In MS Office you have your Icons and every one sees \"Ah this is Word or this is outlook\". They are clear and easy to remember. No problem.\n\nBut here? The may be nice, crystal,svg etc etc but there is absolutely no association between the icon and the application - and that is what icons are for.\n\nDim"
    author: "Dimitri"
  - subject: "Re: Bad choice"
    date: 2008-04-20
    body: "Those are logos. The icons may be different, depending on what the oxygen team decides."
    author: "Darryl Wheatley"
  - subject: "Re: Bad choice"
    date: 2008-04-21
    body: "http://kdedevelopers.org/node/3417"
    author: "jstaniek"
  - subject: "Re: Bad choice"
    date: 2008-04-21
    body: "Ok, but why do we (koffice) need three different icons for one application?\nJust imaging mercedes would have a firm logo (the star), then there would be a logo for mercedes dealers and then every car gets his own logo (so one for the C-Class, the E-class and another one for the S-class). Sounds a little crazy doesn't it?\n\nDim"
    author: "Dimitri"
  - subject: "Re: Bad choice"
    date: 2008-04-22
    body: "\"Ok, but why do we (koffice) need three different icons for one application?\"\n\nRead carefully if you can. There's only one icon per app.\n"
    author: "g."
  - subject: "The logos are nice... but they don't \"work\""
    date: 2008-04-20
    body: "The logos/icons (I don't know the real difference between the two words, honestly) are nice. The idea of assigning them a common \"theme\"/\"skinning\"/\"branding\" is brilliant. To repeat the '>' or '>' or '^' or 'v' symbol in each of them emphasizes that idea even more.\n\nSo, I can probably very easily recognize these logos/icons as \"something to do with KOffice\".\n\nHowever, the total set still doesn't \"work\". Not for me, I'm sorry. It will take years for me to learn to differentiate between most of them. (Kexi I can associate with the \"x\", KWord with the \"W\"... but the rest I just don't \"get\").\n\nI assume it would be very difficult to reject this \"donation\", even if some/most members of the KOffice Team felt the same.\n\nHas there ever been such a thing like \"usability testing\" for a set of related icons/logos? If so, I'd be very much interested if testing this set would confirm my personal feelings. However, I don't assume there is any likelyhood such a testing will happen...\n\nOh, well. More important is, that code-wise the progress on KOffice is looking very good and promising.\n\nCongratulations to the small developer team that brings this about. You can be proud to have come so far already. Hopefully, this achievement and the upcoming releases on Windows platforms will also bring you more and new developers joining in over the next two years."
    author: "someone"
  - subject: "Re: The logos are nice... but they don't \"work\""
    date: 2008-04-21
    body: ">  I assume it would be very difficult to reject this \"donation\", even if\n> some/most members of the KOffice Team felt the same.\n \nThey don't feel the same."
    author: "Thomas Zander"
  - subject: "SVG?"
    date: 2008-04-20
    body: "Where can I find the SVGs for these wonderful icons?  I would very much like to learn from such masters...\n"
    author: "Cultural Sublimation"
  - subject: "Um... Kexi's angry face?"
    date: 2008-04-21
    body: "Nice! But... am I the only one who thinks that the Kexi logo looks like an angry smiley face? A bit like >:( ? It's pretty funny, but I doubt that it is the intent..."
    author: "Valerie"
  - subject: "Beautiful!"
    date: 2008-04-25
    body: "These new logos are fantastic, they're very slick and professional! Seeing KDE4 come to life and the gorgeous new revised version of the blue gear logo for KDE, it seems fitting that the office suite also is getting a makeover.\n\nCurrently I'm forced to use Gnumeric for university, but I use KWord, Krita and Karbon14 more now than ever. It's good to see some thought being put into their promotion again. I know it's shallow to choose computer software based on their advertising and how stylish their logos are, but enough people still do. This is a much needed step in the right direction."
    author: "Ruben S."
  - subject: "very similar to each other"
    date: 2008-04-30
    body: "Nice... but I think they are very similar to each other, and users will not run programs that wanted to run.\nThanks and keep working!!\n\nText translated from spanish to english by google translate :P ."
    author: "emonk"
  - subject: "very similar to each other"
    date: 2008-04-30
    body: "Nice... but I think they are very similar to each other, and users will not run programs that wanted to run.\nThanks and keep working!!\n\nText translated from spanish to english by google translate :P ."
    author: "emonk"
---
<a href="http://nlnet.nl">The Dutch NLnet foundation</a> aims to financially support organisations and people that contribute to an open information society. Some time ago they decided to help KOffice in two exciting ways: to sponsor the design of a new logo for KOffice, with matching logo designs for all KOffice applications, and to sponsor Girish Ramakrishnan to improve the ODF support in KWord 2.0. The KOffice team is deeply grateful to NLnet for this support!
















<!--break-->
<p>Girish Ramakrishnan, a former Trolltech employee, has already started on implementing a thorough test suite for ODF text loading. Helping him are Thomas Zander and Thorsten Zachmann, two old-time members of the KOffice team. In his own words:</p>

<p style="font-style: italic;">"I am working on getting ODF support up to speed in KWord, my work being sponsored by NLNet. As the first step, I
have spent my time now automating the ODF testsuite at the OpenDocument Fellowship....</p>

<p style="font-style: italic;">"So far, I have found some basic tests are failing - loading of lists, possibly superfluous spaces/blocks. I have patches coming up."</p>

<p>KOffice has done without a real logo forever: we used to use the application icon of KOShell, a rainbow, but that was hardly a real logo, and besides, <i>everybody</i>, including the primary school your correspondent attended, uses a rainbow. But coming up with a good logo is hard, and we postponed and postponed the task.</p>

<p>But then NLnet proposed to retain the services of designer Michiel van Kleef of <a href="http://www.30.nl">30 Media</a>. Michiel was faced with a very hard brief: to design a logo, not an icon, for KOffice which combines business and creativity in one, integrated package. After consultation with the KOffice team we arrived at the following logo:</p>

<div align="center">
<img src="http://static.kdenews.org/jr/koffice-logo.png" width="216" height="241" />
</div>

<p>This great design suggested to Michiel the possibility of doing variations on it for the individual applications that KOffice consists of. While KOffice itself has got the KDE color blue, the business applications get orange:</p>

<div align="center">
<table border="0">
<tr><td align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kword.png" width="215" height="241" />&nbsp;&nbsp;
</td><td align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kspread.png" width="247" height="241" />&nbsp;&nbsp;
</td></tr>
<tr><td align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kpresenter.png" width="326" height="241" />&nbsp;&nbsp;
</td><td align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kexi.png" width="176" height="241" />
</td></tr>
</table>
</div>

<p>And the graphical applications get purple (and it's good to see that Karbon2 opens the official SVG sources correctly):</p>

<div align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-karbon14.png" width="308" height="241" />&nbsp;&nbsp;
<img src="http://static.kdenews.org/jr/koffice-logo-krita.png" width="176" height="241" />&nbsp;&nbsp;
<img src="http://static.kdenews.org/jr/koffice-logo-kivio.png" width="176" height="241" />
</div>

<p>KPlato, the project planner application that is coming along amazingly well for KOffice 2.0 is the odd one out, and gets red:</p>

<div align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kplato.png" width="210" height="241" />
</div>

<p>The helper applications KChart and KFormula are green:</p>

<div align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kchart.png" width="218" height="241" />&nbsp;&nbsp;
<img src="http://static.kdenews.org/jr/koffice-logo-kformula.png" width="302" height="241" />
</div>

<p>There is also a logo for Kugar, the report writing component in KOffice 1, which will probably be replaced by <a href="http://www.piggz.co.uk/">Adam Pigg's promising new report component in KOffice 2</a>:</p>

<div align="center">
<img src="http://static.kdenews.org/jr/koffice-logo-kugar.png" width="196" height="241" />
</div>

<p>All with a subtle, but apposite variations on the design in the circle.</p>

<p>Now all that remains to be done is updating the <a href="http://www.koffice.org">KOffice website</a> and prepare some t-shirts for Akademy!</p>









