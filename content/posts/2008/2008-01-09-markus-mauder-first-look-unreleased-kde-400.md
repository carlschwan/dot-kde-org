---
title: "Markus Mauder: A First Look at the Unreleased KDE 4.0.0"
date:    2008-01-09
authors:
  - "jjesse"
slug:    markus-mauder-first-look-unreleased-kde-400
comments:
  - subject: "Chinese translation for that blog"
    date: 2008-01-08
    body: "A KDE friend in China had translated it into Chinese:\nhttp://linuxdesktop.cn/2008/01/07/kde4-preview-2"
    author: "Cavendish Qi"
  - subject: "Design"
    date: 2008-01-08
    body: "It is a bit like KDE 3.1. New graphics and blink but lack of coherent \"style\". This will emerge later. \n\n(e.g. the plasma panel look is a bit prototypish, the KDE Icon is just too large, proportions don't fit)\n\nThe concept of the KDE 4 cycle was non-evolutionary. It seems to me the times of these development models are over and KDE 4 is now a solid base for the future."
    author: "bull"
  - subject: "Re: Design"
    date: 2008-01-09
    body: "Wow, like KDE 3.1? Already? Not kde 3.0 ?? So we already have a BIG improvement here! ;-) Just joking, I agree with you about the solid base for the future, but for now: look at how the windeco nicely blends with the style's background! hear the OxySounds! enjoy the OxyIcons! and Feel the OxyFeeling!!\nPeople I showed the KDE 4.0.0 desktop were really surprised and appealed ;-)\n"
    author: "an user"
  - subject: "Polish review"
    date: 2008-01-08
    body: "http://jarzebski.pl/read/kde-4-rev-755000.so"
    author: "newgargamel"
  - subject: "Re: Polish review"
    date: 2008-01-08
    body: "This one is 3000 better ;)\n\nhttp://jarzebski.pl/read/kde-4-rev-758000.so"
    author: "Anon"
  - subject: "Re: Polish review"
    date: 2008-01-08
    body: "Yes, you are right! I linked the old article..."
    author: "newgargamel"
  - subject: "Re: Polish review"
    date: 2008-01-09
    body: "yeap, thanx to Korneliusz from jarzebski.pl KDE users from Poland had almost one solid KDE-svn review per week :)&#8234;"
    author: "Koko"
  - subject: "Re: Polish review"
    date: 2008-01-09
    body: "The article at http://jarzebski.pl/read/kde-4-rev-758000.so is a very fascinating read (and it was cool to hear the log-in and log-out sounds). For those who can't read Polish, the author was very happy with many new aspects of KDE 4.0 (such as Oxygen) but discussed a few things that could especially see more work for later KDE4 releases (I translated the major things in the list, sorry in advance if I got anything wrong):\n*KDM hasn't really been touched\n*More oxygen windeco configuration would be nice\n*No way yet to change the size and position of the bottom panel\n*No ability to change the position of applets on the bottom panel\n*The grid of icons isn't very flexible yet in terms of size and spacing\n*Few config options for Phonon\n*Not enough options in System Settings yet.\n*A lot less effects in KWin than Compiz (my note: but at least KWin is more focussed on useful effects than just plain bling, though a cube would be awesome).\n\nAs you can tell, these were pretty expected and understandable omissions, given that 4.0 is the foundation release and the developers are spread very thin. It'll be cool to gradually see all these things drop off the ToDo list as KDE4 enters later releases. Great work people!"
    author: "pcp"
  - subject: "Re: Polish review"
    date: 2008-01-09
    body: "Sounds like fair complaints. My main one concerning KWin is that I really want to see the group-and-flip plugin ported from Compiz. I can't stand compiz, but that would be very nice to have for KWin."
    author: "Stephan Sokolow"
  - subject: "Re: Polish review"
    date: 2008-01-09
    body: "I think that the the only \"real\" complaints are those about KDM (which is a pity it hasn't been touched, but I understand why) and Plasma issues (which are going to be fixed in the future). the \"not enough options\" on the other hand is a good thing, IMO :P"
    author: "Vide"
  - subject: "Re: Polish review"
    date: 2008-01-09
    body: "not _enough_ of anything is, by definition, a bad thing.  I would assume what you really mean is that fewer unnecessary options cluttering up the windows, which is a good thing."
    author: "ciasa"
  - subject: "Re: Polish review"
    date: 2008-01-10
    body: "Yea I miss much of compiz too.. \n\nHopefully somebody will write a plugin for compiz, or port the rest of the features from compiz over...\n\nKDE 4.0 looks gorgeous for now.. Very cutting edge. and - dare I say - Web 2.0 :D\n\n"
    author: "Max"
  - subject: "Beats MS Windows Vista."
    date: 2008-01-08
    body: "Impressive."
    author: "KDE User"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-08
    body: "Not sure it does now, but we realy want to make it beter than that and all the rest."
    author: "nuno pinheiro"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "It will beat Vista because Vista will be the same in the next 4 - 5 years but this is just the beginning of KDE4. Already KDE 4.0 has quite a few features that Vista can only dream of not to mention it's speed and the small amount of resources that it needs when compared to Vista."
    author: "Bobby"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "Not that I don't love KDE as much as the next guy, but from my experience KDE 4 is way slower than Vista, last time I checked was KDE 4.0 RC2, perhaps things changed. But you are right, this is just the beginning, most of the good stuff is yet to come."
    author: "Terracotta"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "What have you been smoking? I'd really like some of that stuff."
    author: "Anon"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "Not exactly a civil answer (im not the one you were answering BTW)."
    author: "anon"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "I can confirm this. I'm on a Intel Core Duo 2.0 Ghz, 3 Gb RAM and 512MB Nvidia graphics card and the graphics are lacking. \nI'm hoping this is a specific problem with my hardware, because otherwise a lot of people will be suprised. It may also be the kubuntu packages that have problems.\nI tried this with post-RC2 packages. \n\nHowever, I'm sure this issue will be fixed quickly or may allready have been fixed"
    author: "Pascal"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "I don't see that. I got a completely new high-end laptop last week shipped with Vista. Vista is pretty slow, but at least it realised that itself and auto decreased eye-candy. KDE 4 on other hand is completely smooth."
    author: "Allan"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "Perhaps there's something wrong with your setup or the machine is crippled by the shitware bundled by the vendor. My laptop is one year old and Vista runs on it like a dream. Granted, it has 2GB of RAM."
    author: "Dirk"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-10
    body: "Like on my laptop.\n\nBut KDE4 in a chroot environement is much faster than Vista.\n\nIt's great, has some bugs, but it is not yet released (in 2 days ;-)\n\nGreat work !!!"
    author: "mschnide"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "KWin is snappier and smoother on my ATi AIGLX than on my nvidia system -- So I suspect driver issues with nvidia.ko. Once KDE 4.0 is out and becomes more 'mainstream', I hope NVidia will sort those issues out. They have been responive in the past fo far ..."
    author: "sebas"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-10
    body: "If you have an NVidia graphics card, try adding \"export KWIN_NVIDIA_HACK=1\" to your .bash_profile and .bashrc. This should make compositing smoother and faster. Without that KWin with composting enabled felt quite slow on my system (not moving windows, but switching from one window to another or from one tab to another). With this, switching windows or tabs became much quicker."
    author: "Michael Thaler"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "> I'm hoping this is a specific problem with my hardware\n\nI have seen at least a couple of bug reports about poor performance on NVidia hardware.  Apparently the very latest NVidia drivers remedy the problems somewhat.  If you haven't already got them, please try them and see if there is any change."
    author: "Robert Knight"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-09
    body: "perhaps debug compile mode ..."
    author: "Jesus R. Acosta"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-10
    body: "Hope not.. Vista is sloooow....\nEven on faster systems...\n\nHopefully it'll add more color by the final release... KDE 4.0 feels a bit too black and white so far. Guess that's the style these days.. Thanks Apple!\n\nAt least it's cutting edge. :D"
    author: "Max"
  - subject: "Re: Beats MS Windows Vista."
    date: 2008-01-10
    body: "Funny enough I didn't like black or dark themes before but KDE4 does it in a way that it really looks very attractive and it's easy on the eyes."
    author: "Bobby"
  - subject: "Young Skywalker"
    date: 2008-01-09
    body: "You are not a Jedi yet."
    author: "Crabby Crab"
  - subject: "Bummer"
    date: 2008-01-09
    body: "I saw Dragon player and forgot that it was codeine. I was thinking that it was an interesting game."
    author: "a.c."
  - subject: "Re: Bummer"
    date: 2008-01-09
    body: "lame trick ;]"
    author: "Dolphin-fanatic :)"
  - subject: "Oxygen icons : A few mistakes"
    date: 2008-01-09
    body: "Hi there,\n\nI just wanted to highlight a few things that I consider as mistakes in the oxygen icon set, and especially on this screenshot : http://picasaweb.google.com/mmauder/KDE400/photo#5152345685276228690\n\n- The arrow buttons (go back/go forward) don't fit at all with the others icons. They seem to have been taken from a bad GNOME icon set... :/\n\n- The trash is not really usable, since it is just too hard to see if it empty or not. Furthermore, because the colors are exactly the same, it seems to be part of the hard drives.\n\n- Why are the hard drives inclined, and the usb key all vertical ? I mean, they are all volumes, so why not make them look similar ? I would be far more intuitive.\n\n\n\nTo finish with, I would like to focus on this screenshot : http://picasaweb.google.com/mmauder/KDE400/photo#5152342502705462338\n\nSee the splashscreen window ?\n-Why does it have square black edges on the top coming out of the round white outline ?\n\n- Why does the white outline suddenly disappear on the right side but continue on the left side ?\n\n- Why are the bottom edges square while the top edges are round ?\n\n\nThat's all I can say for the moment. The rest of Oxygen is just great :)"
    author: "Maximilien"
  - subject: "Re: Oxygen icons : A few mistakes"
    date: 2008-01-10
    body: "Insightful comments! Try adding them here so that the devs don't overlook them:\n\nhttp://techbase.kde.org/index.php?title=Projects/Oxygen/StyleWinDec"
    author: "Tray"
  - subject: "Re: Oxygen icons : A few mistakes"
    date: 2008-01-10
    body: "That's done :)"
    author: "Maximilien"
  - subject: "hm"
    date: 2008-01-09
    body: "When alt-tab works I'll consider playing with it some more. All I do now is update the new version from subversion and compile it up once every few days, confirm alt-tab still doesn't work.. remove .kde, reload, confirm alt-tab doesn't work again then decide it 'aint worth it."
    author: "James"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "what's not working with alt-tab? It's supposed to change the current window/application, isn't it? That's exactly what it does over here. Is there any further functionality missing?"
    author: "Thomas"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "It's likely kded4, which handles the global shortcuts like alt-tab, is not running for him.  Alt-f4, ctrl-alt-del etc then would not work either.  The output of \"qdbus org.kde.kded\" should include \"/KdedGlobalAccel\".  If the service does not exist or doesn't include that interface, something is wrong. \n\nSince it generally works, I would try logging in with a clean user (or $KDEHOME) as there could be some user config left over from pre 4.0 development versions within $KDEHOME that is causing the problem.\n"
    author: "Bille"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "Nah, it's the same thing here, qdbus org.kde.kded do indeed include /KdedGlobalAccel, and this is with a clean $KDEHOME. No shortcuts work."
    author: "fredde"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "looking at the service settings, it reports it as not loaded, so how do i get it to load the god damn thing? ;)"
    author: "fredde"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "Alt-Tab does not work for me either (most of the times).\n\n- after deletion of \"old\" RC2 install and fresh \"1 Click install\" from OpenSuse last night.\n- kded4 is indeed not running at startup (please - anybody - is there a trick?)\n- after kded4 is started manually from konsole command line, all services are working; however short-cut's are not\n- system configuration->advanced->services correctly report that the global short-cut server is not running; but also the \"start\" button is grayed out\n- I got it to work few times by changing some short-cuts, but not repeatable\n\n--> any suggestion for\n1) get kded4 running from start or finding the kded4 start-up log to debug the problem\n2) \"ungray\" the \"start\" button in system config -> advanced -> services  \n3) activate global short-cut server\n\nOtherwise KDE4 rocks! It is fast, nice, and I working with it and can do \"almost\" everything with it."
    author: "vl"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "No as I mentioned in my post I do rm -rf .kde4 before restarting KDE and it still won't work."
    author: "James"
  - subject: "Re: hm"
    date: 2008-01-09
    body: "Interestingly I have the same problem but I haven't reported yet because I am not sure it's some misconfiguration on my part rather than a bug. Where could I ask for assistance?"
    author: "Luca Beltrame"
  - subject: "The Question of the Question..."
    date: 2008-01-09
    body: "So, after KDE4 ist finally coming in an first version, the only things that bugs me is: where can i get it? I'm using Debian Sid(ux) and officialy, there are only the Experimental-Version, which override KDE3 :-("
    author: "Chaoswind"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-09
    body: "Be pantient. See that banner after the article? ;)"
    author: "Hans"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-09
    body: "So what? Will every person on this planet wait until the KDE-Project puplish KDE 4.0 official, to distribute their work? Hopefully not. I don't wanna fall ins the rushhour ;)"
    author: "Chaoswind"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-09
    body: "Well, you can do as Markus Mauder and build KDE4 from SVN. Find more at http://techbase.kde.org."
    author: "Hans"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-10
    body: "Yeah, it turns out to not be that hard, especially if you're running sid, since that has all the right library versions you need. If you're running something older, I suspect it is more work."
    author: "a. spehr"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-10
    body: "Could take longer than waiting for binarys ;) Also, the Update-support sucks."
    author: "Tempura"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-10
    body: "Alternative is to switch to openSuse ;)"
    author: "Bobby"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-10
    body: "Bad Joke. I Killed OpenSuSE yesterday on my Test-System @work (again), because of the horribly behaviour."
    author: "Chaoswind"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-10
    body: "Did she cheat on you?"
    author: "Bobby"
  - subject: "Re: The Question of the Question..."
    date: 2008-01-11
    body: "She used to much Time at bath, and did'nt spend enough time for my personal needs :-9"
    author: "Chaoswind"
  - subject: "The new \"K\" menu."
    date: 2008-01-09
    body: "Looks cool. However, why is the \"search bar\" on the top of the menu and not at the bottom where is closest to the just-depressed-K-button?"
    author: "NabLa"
  - subject: "Re: The new \"K\" menu."
    date: 2008-01-09
    body: "The search bar receives focus when you open the menu. So it's like \"click on the K-button and start typing\", without having to click again. Also, results appear from top to bottom, just under the search bar."
    author: "an user"
  - subject: "Re: The new \"K\" menu."
    date: 2008-01-09
    body: "> However, why is the \"search bar\" on the top of the menu\n\nThe SuSE team who developed the UI originally found that people did not find the search bar if it was on the bottom. \n\nIts a slightly surprising result, but it shows the value of testing things in the real world.\n\nEither way, the search bar has keyboard focus as soon as you open the menu, and any key presses to the tab bar or menu area which wouldn't have any effect (eg. letters) are directed there."
    author: "Robert Knight"
  - subject: "Re: The new \"K\" menu."
    date: 2008-01-09
    body: "Reminds me of a book I read about how to make efficient presentations : it said that when a slide appears onscreen, people automatically look at the middle, so always put in the middle the real important thing you want people to consider in that particular slide. I've been following this motto since then and it seemed indeed to make my slides easily read by attenders.\nTo come back to the menu, I suppose that the eye is more easily attracted to the top of the window than to the bottom (at least in western cultures where we read from top to bottom), especially if there is an animation making the menu coming up from the taskbar.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: The new \"K\" menu."
    date: 2008-01-09
    body: "And what if we put the bar at the top of the screen instead ?\n(I know it is not now possible with the UI for plasma in 4.0)\nShould then the menu layout be modified in function of its position on the screen\u00a0?\n\nI have had some bad surprises with kicker in the time when I used to put it on the left edge..."
    author: "Aldoo"
  - subject: "Picasa?"
    date: 2008-01-09
    body: "These images are posted on a website that doesn't work in Konqueror."
    author: "Michael"
  - subject: "Re: Picasa?"
    date: 2008-01-09
    body: "Sigh, poor Konqui. People stopped using it when Firefox extensions became too useful to be ignored, and it didn't get much love since it started choking on GMail."
    author: "Giacomo"
  - subject: "Re: Picasa?"
    date: 2008-01-09
    body: "I dunno, as someone who has moved more or less from Firefox to Konq over the past year or so (all I really use firefox for is scrabble on facebook - and only recently while the new proprietary flash is broken in 64 bit konq in kde3) I find the advantages outweigh the problems. There are other sites/services/banks you can use if your current ones can't be bothered to make crossbrowser compatible sites."
    author: "Simon"
  - subject: "Re: Picasa?"
    date: 2008-01-09
    body: "It doesn't work in KDE3 konq but I just checked and they do indeed work in KDE4 konq. I was wondering how long it was going to take for someone to notice.\n\nI guess this points out the general lack of use of konqueror for normal people browsing. Google is by far the biggest offender for bad sites that are somewhat popular since they seem to code for firefox first and current standards second.\n\nI'm sticking with konq until something better (qt or kde) comes along and living with what doesn't work.\n\n\n\n"
    author: "fuegofoto"
  - subject: "Re: Picasa?"
    date: 2008-01-10
    body: "Picasa works for me (as in i can see/browse the album) in Konqui 3.5.8 when setting Browser Ident to Mozilla 1.7.3 on XP"
    author: "Rischwa"
  - subject: "Very happy about this release!"
    date: 2008-01-09
    body: "I truly appreciate all the great work that's under the hood of KDE.  I'm glad there's a desktop alternative that isn't a few separate desktop apps cobbled together with the window manager <i>du jour</i>, that has a consistent design under the hood.  KDE has come a long way over the years, not necessarily having the last bit of spit and polish, but certainly having the most power, and making the most efficient use and reuse of libraries to get it.  \n\nI think KDE4 is where this changes and that sense of design shines through, even at a glance.  This is payoff.  I hope to see KDE go toe to toe with OSX this year for the best, most productive desktop.  I have machines running either of them, and I'll be interested to see how it feels to switch back and forth.\n\nGo devs!  I'd join up and bang on that code too if I could pay the bills doing what you do."
    author: "Daniel Rollings"
  - subject: "Rocking or rock stable."
    date: 2008-01-10
    body: "It may rock, but will it be rock stable?"
    author: "szlam"
  - subject: "Re: Rocking or rock stable."
    date: 2008-01-10
    body: "A *major* break from a very large existing code-base being stable on it's very first release?\n\nIt's never happened before and, while the KDE guys are great, they are not miracle workers ;)"
    author: "Anon"
  - subject: "Re: Rocking or rock stable."
    date: 2008-01-10
    body: "It's not yet full featured, meaning that it's just the beginning of the KDE4 series and quite a few programmes are still not ported but it's already very stable, at least the current RC that I am using. I can't remember the last time that a programme crashed and I use the RC2+ everyday."
    author: "Bobby"
  - subject: "A minor bug that will keep me away from KDE4"
    date: 2008-01-10
    body: "http://bugs.kde.org/show_bug.cgi?id=152676\n\nI've tried it, tried workaround and it's useless. To bad such thing is considered less important to be fixed. Using dualhead is the primary thing I use computer (and Linux) for."
    author: "Robert"
---
Markus Mauder on his blog <a href="http://drowstar.blogspot.com/2008/01/first-look-at-unreleased-kde400-with.html">posts a look at the soon to be released KDE 4.0.0</a> complete with screenshots and a review of some of the significant changes.  "<em>I hope you enjoy this preview and come to share my opinion that KDE 4 is going to rock!</em>"  He also has an <a href="http://picasaweb.google.com/mmauder/KDE400">album of screenshots</a> on Picasa that expand on the ones in the article.  The big release happens on Friday, join us in #kde4-release-party on Freenode to celebrate.











<!--break-->
<img src="http://static.kdenews.org/jr/4.0-release-3-days.png" width="377" height="47" />


