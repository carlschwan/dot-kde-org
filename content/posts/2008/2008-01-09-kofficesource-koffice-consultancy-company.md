---
title: "KOfficeSource: a KOffice Consultancy Company"
date:    2008-01-09
authors:
  - "jriddell"
slug:    kofficesource-koffice-consultancy-company
comments:
  - subject: "Business Ecosystem Flourishes"
    date: 2008-01-09
    body: "It's great to see how the business ecosystem around KDE is flourishing. It makes KDE and KDE applications more attractive to *other* businesses simply by putting the right \"face\" on the community. It also makes it possible to hold on to developer talent better by offering a job at doing KDE hacking. Well, at least I hope Boud continues to do hacking and doesn't get tied up in business administration ;)\n\nOddly enough, I just got a Qt / C++ work-from-home recruitment offer minutes ago before reading this announcement. Things are thriving.\n\n(PS: although I'm on the board of KDE e.V. and sometimes drink coffee with Sebas, this is the first I had heard of KOfficeSource, so above is genuine enthusiasm and not pre-cooked.)"
    author: "Adriaan de Groot"
  - subject: "Re: Business Ecosystem Flourishes"
    date: 2008-01-09
    body: "Hey, you too?"
    author: "Boudewijn Rempt"
  - subject: "Re: Business Ecosystem Flourishes"
    date: 2008-01-09
    body: "So it doesn't matter if *I* get tied up in business administration, huh?"
    author: "Inge Wallin"
  - subject: "Re: Business Ecosystem Flourishes"
    date: 2008-01-10
    body: "nah, we expect you too ;-P\n\ncongrats, guys, this is great news."
    author: "Aaron J. Seigo"
  - subject: "Good luck"
    date: 2008-01-09
    body: "These are great news!\n\nWe all wish you good luck!"
    author: "Daniel"
  - subject: "Very Best of Luck!"
    date: 2008-01-09
    body: "I really admire the KOffice project - it rests on *excellent* technical underpinnings (compared to OO.o's sprawling and frankly horrific codebase) and all the devs seem like nice people, too :) \n\nSeeing some paid development on it would be great!"
    author: "Anon"
  - subject: "almost scared to ask this..."
    date: 2008-01-09
    body: "...but here goes: if a company requested / paid for the addition of support of various MS formats (existing binary formats, new XML wrapped binary formats aka OOXML), what would the answer be? are there moral / legal / community issues about this?\n\nI think this is great that a company is forming around koffice and I'm really wishing you guys all the best. I'm just asking this out of pure curiosity and not trying to start flame war (which i know are usually the last lines written  preceding most flame wars, d'oh)\n\n"
    author: "Borker"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-09
    body: "I see no reason why paid development couldn't be for legacy file formats.\n\nIn a recent dot story the KOffice developers explained that they weren't interested in spending their own time developing it, but that's something completely different to paid development.  It is also not connected to any KOffice developer's opinion if for instance OOXML should be made an ISO standard (it shouldn't) or if the file format itself is badly specified (it is) and impossible to implement fully (it is)."
    author: "Inge Wallin"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-09
    body: "Thats pretty much my take on it also. Its simply neither expedient nor particularly desirable to implement 3rd party proprietary formats. \n\nI guess the issue though is mainly that the aforementioned dot article was held up by RMS as a counterpoint to the somewhat MS-happy approach of other development efforts and there is a big opportunity for future paid for efforts to include particularly OOXML being presented in a negative light and starting all kinds of flame war fun on the usual tech blogs etc."
    author: "Borker"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-09
    body: "Wrongzo...\n\nIt matters a great deal to implement proprietary filters for *import*.\n\nExport is another story."
    author: "Joe"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-10
    body: "Borker, notice that the KOffice community doesn't necessarly need to merge the work done by this company. I bet they will have a special build for their customers; possibly even single builds for special cases."
    author: "blacky"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-10
    body: "Except that at least half their board is composed of major Koffice contributers.   (I don't recognize the names of the other 2 board members, but that doesn't mean they are outside the koffice community, just that I don't know them) While in theory the koffice community could reject their patches, since they are the community this is less likely. \n\nI suppose it is possible they will get paid for an ugly hack that sortof works and meets a customers needs now, but isn't the direction they want koffice to go long term.   \n\nSpecial builds for customers, and single builds are likely.  Sort of like Code Weavers has their own version of Wine, but it only better than Wine as a whole in places where they have ugly hacks that make one things sort of work while making others less likely to work."
    author: "Hank Miller"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-10
    body: "Exactly, it's natural for variants of software to exist for different applications. No need for something unnecessary to be added to the main code base."
    author: "Glen Anthony Kirkup"
  - subject: "Re: almost scared to ask this..."
    date: 2008-01-12
    body: "One thing that I kept in mind while doing plugin-interface design for KOffice2 in general and for text specifically is to make sure companies can add their own workflow features without touching the KOffice codebase.\nIf a company wants to have something like a special variable in text that connects to their internal database (for example), they can write a text-plugin quite easily to do that, again without touching the KOffice codebase. (see the techbase tutorials about koffice plugins)\n\nSince the libraries KOffice provides are LGPL the company may very well choose to make the plugin they want to use closed source. Which obviously means it will never hit the KOffice codebase.\nThere is a nice incentive for companies to go via KOfficeSource instead of doing it in-house not only because of the expertise, but also because of the Qt licenses required to do this.\n\nBottom line; this significantly lowers the trashhold for large rollouts to succeed. Any missing thing can now be added by paying the experts to do exactly that.\n\nExciting times :)"
    author: "Thomas Zander"
  - subject: "Another Good Luck"
    date: 2008-01-09
    body: "a __BIG__ 'Good Luck' from not only me, but whole Community.best wishes for you all."
    author: "Emil Sedgh"
  - subject: "Wow"
    date: 2008-01-09
    body: "This is probably the best KDE related news in months - probably not for long, as the release of something big is imminent - and came completely unexpected for me. I hope the business side of this works out nicely, and Boud and Inge can spend a lot of time on improving KOffice even more than was the case before :P\n\nGreat stuff, and I'd like to wish you the best of luck too :D"
    author: "Jakob Petsovits"
  - subject: "nice to know"
    date: 2008-01-09
    body: "that's really nice 2 know!"
    author: "benneque"
  - subject: "quote of the day"
    date: 2008-01-09
    body: "Just a question. Why is the quote at the end of the page\n\"World domination doesn't come by itself.\" -- Ralf Nolden\nprecisely whent I read this article :)\n\nWish you a lot of funy work, benefiting both your incomes and your user comunitiy."
    author: "kollum"
  - subject: "Best of luck!"
    date: 2008-01-09
    body: "I hope you'll have a lot of succes with this!"
    author: "Joergen Ramskov"
  - subject: "hmm"
    date: 2008-01-09
    body: "ok this sounds quite good, a win-win situation\nmake no mistake about this, i think if this works, it will be GREAT for everyone involved here, and for the users just as well.\n\nBUT there is one thing that I want to voice (or ask), even though others might have voiced that already.... will koffice become dependent on it?\n\nI am asking because right now, the whole linux distributions are influenced by companies just as well, and every line of code comes with a price - someone has to understand it at least enough to maintain it. "
    author: "she"
  - subject: "Re: hmm"
    date: 2008-01-10
    body: "My personal feeling is, that there is a huge difference between \"a company joins a project\" and \"a company forms out of a project\" since at the later case it's more the project which influences the company rather then the other way around :-)"
    author: "Sebastian Sauer"
  - subject: "Re: hmm"
    date: 2008-01-10
    body: "The real divider is whether the company owns the copyright to the program or not.  In this case we don't, which I also address in the announcement, so we could never control the KOffice project.\n\nNow, influence is of course something we hope to have, but that's hardly control. KOffice is jointly owned by the developers and that's how it will remain."
    author: "Inge Wallin"
  - subject: "Re: hmm"
    date: 2008-01-10
    body: "agreed; there's one other ingredient: the people involved.\n\nin fact, it's the people part of the mix that determine whether the legal safeguards are ever brought into play. usually the legal safeguards, which are extremely reassuring and pretty vital imho, are there for the case where things go wrong. even with safeguards in place, nobody ever wants to go there. so the people being Good, Upright, Honest and Trustworthy are the key ingredients.\n\nboth Inge and Boud, along with the rest of the KOffice team, are exactly those kinds of people in my experience having worked along side them in KDE and spent time in purely social circumstances as well. i have no doubts in my mind that with the people who are forming KOfficeSource that a good community relationship will be maintained and that we will likely never end up needing to rely on those legal safeguards.\n\nhaving both legal safeguards as well a good people is a killer combination. i can only see good things coming of this collaboration =)"
    author: "Aaron J. Seigo"
  - subject: "Re: hmm"
    date: 2008-01-10
    body: "I guess that legal safeguards are no protection against bad influence. IMHO what she@hot.com was refering to are fights between different forces within a project that harm the harmony and have an impact on \"how a project feels like\". But that's more a general question not related to companies but to individuals aka the people involved what makes it rather difficult to find another answer then what Aaron wrote in his 3th paragraph already (and agreed there too :)\n"
    author: "Sebastian Sauer"
  - subject: "wait..."
    date: 2008-01-10
    body: "\"If KOffice will ever be able to challenge OpenOffice or the proprietary competition,\"\n\nas long as KOffice stays as simple to use as it is for now, it would be OK to challenge OOo or even MS-Office. Simplicity and ease of use should be the goals for any software. "
    author: "kofficeuser"
  - subject: "Collaboration with IBM?"
    date: 2008-01-10
    body: "A while back someone (I think it was TJ Brumfeld) suggested that IBM could work with KOffice developers to produce a better version of Lotus Symphony that doesn't use OOo's clunky old codebase, that causes Symphony to have the speed of a turtle trapped in toffee. Would KOfficeSource GmbH be able to help in this regard by talking to IBM people about collaboration or does this have only a minuscule chance of success?"
    author: "Darryl Wheatley"
  - subject: "Re: Collaboration with IBM?"
    date: 2008-01-10
    body: "I'd also like to hear about this: IBM have smart engineers, and they should surely see that they could probably get much better progress with hiring the same amount of engineers with KOffice than with OO.o."
    author: "Anon"
  - subject: "Re: Collaboration with IBM?"
    date: 2008-01-10
    body: "Mmmmm toffee turtle"
    author: "Borker"
  - subject: "Re: Collaboration with IBM?"
    date: 2008-01-12
    body: "IBM has made contributions to Linux because they wanted to sell hardware. They use a totally different business model for Lotus. In fact, OpenOffice has never seen a line of code from IBM (they forked while it was permitted to go close source)."
    author: "blacky"
  - subject: "A ton of glad-ness and a grain of unhappy-ness..."
    date: 2008-01-10
    body: "I feel a ton of glad-ness about that bold move on the part of some key KOffice developers. I wish them well and all the best in this ambitious endeavour.\n\n(I also feel a grain of unhappy-ness because they decided to single out OpenOffice.org to name as their main competitor which they intend to tackle. No, guys! Your main target should be MS Office, and OOXML. You should rather ignore OOo instead of making yourself sound envious of OOo's success. Most KDE people nowadays would think it's wrong to make a press release saying KDE4 aims to take market share away from Gnome. And it is not just \"tactics\" to do so. It is to recognize who your *main* adversaries and competitors are: and these are MS and Apple, not The Second Free Desktop...)"
    author: "koffice-fan"
  - subject: "Re: A ton of glad-ness and a grain of unhappy-ness"
    date: 2008-01-10
    body: "Well I'm fine with it, currently I'm an unhappy OO.o user due to various bugs in KOffice. I hope this company will end up improving quality control and reduce the OOo market share by one user. :)"
    author: "Ian Monroe"
  - subject: "Re: A ton of glad-ness and a grain of unhappy-ness..."
    date: 2008-01-10
    body: "You're right, of course."
    author: "Inge Wallin"
---
Announcing itself today is <a href="http://www.kofficesource.com/">KOfficeSource GmbH</a>, a
company that will sell services around KOffice.  The founders comprise a small group of
members of the developer community, as well as outside talent.  They share an interest in furthering KOffice by supporting it
commercially in addition to the non-commercial support that can be
found on the mailing lists and IRC.  As the name suggests
it has been created in Germany but will operate across Europe and further afield.











<!--break-->
<p>"<i>The founding of KOfficeSource GmbH shows the commercial potential of the KOffice suite.</i>", said Sebastian Kügler, KDE e.V. director. "<i>It is great to see a company spinning off the KDE Community. Combining the commercial experience in the field with excellent connections in the community seems to be a recipe for success. And success is also what we wholeheartedly wish to the founders of KOfficeSource.</i></p>

<p>Kügler continues: <i>With the upcoming release of KOffice 2 later this year, this makes for a new contender in the office market space which no doubt will be beneficial for institutional, commercial and home users alike.</i>"</p>


<p>The founders of KOfficeSource GmbH are:</p>

<ul>
<li>Tobias Hintze:   CEO (Gesch&auml;ftsf&uuml;hrer)</li>
<li>Boudewijn Rempt: maintainer of Krita</li>
<li>Inge Wallin:     marketing coordinator and maintainer of KChart</li>
<li>Kalle Mattias Dalheimer: KDE founder and owner of Klar&auml;lvdalens datakonsult.</li>
</ul>

<p><em>From the <a href=" http://www.kofficesource.com/announcements/community-announcement.txt">announcement</a>:</em></p>

<h2>What is it?</h2>

<p>From the start, KOfficeSource GmbH will function as a pure services
company.  We will offer professional services around KOffice such as:</p>

<ul>
<li>Customising, e.g. by adding features or creating new GUIs.</li>
<li>Integration into workflow systems or other products.</li>
<li>Training for users, system administrators and programmers.</li>
<li>Packaging</li>
</ul>

<p>Since KOfficeSource does not own any copyright to the code, we can not
and do not want to steer the development in any particular direction.
We will work with the community, basing our services on what the
developers are already producing</p>

<p>Sometimes we will create new features, new modules or maybe even new
components for KOffice.  We will then offer the new features to the
community to accept into KOffice or decline as it sees fit.  We will
work with the current developer community as closely as possible when
developing new code. This means that we will work in the existing Subversion
repository, under the existing licensing rules and the existing governance customs.</p>

<h2>What Will This Mean for KOffice?</h2>

<p>In the long run, we think the mere existence of a company like this
will make KOffice more interesting for other companies to deploy.  It
gives KOffice yet another piece in the <a href="http://en.wikipedia.org/wiki/Whole_Product">Whole Product</a> puzzle.  If KOffice will ever be
able to challenge OpenOffice or the proprietary competition, we will need all of these pieces and more.
This is the first step in that direction.</p>

<p>On a shorter time scale it will probably not mean much at all.  We
do not expect to become very active until KOffice 2.0 is out, maybe
even 2.1. We do not have any large funds at this time that we could
use to sponsor any development.  We will offer our services when the
chances arrive and in time we will be more active.</p>


<h2>What Will This Mean for the Developer Community?</h2>

<p>Probably nothing, right now. The developers involved in this company
will hack on as usual, In the longer run, over a few years, we
might be able to hire developers part or full time. We have great
hopes, but they will take time to become real.</p>

<h2>More Information</h2>

<p>You can read more on our <a href="http://www.kofficesource.com/">website</a>.











