---
title: "KDE Commit-Digest for 27th April 2008"
date:    2008-05-22
authors:
  - "dallen"
slug:    kde-commit-digest-27th-april-2008
comments:
  - subject: "I'm back!"
    date: 2008-05-22
    body: "I took a break from releasing the Digests for a few weeks while I did some important and vital stuff, like moving location, etc.\n\nBut it wasn't really a break, more of a delay: all the commits from this period were kept and read, and the Digests will still be released shortly.\n\nThe next one may come on Thursday...\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: I'm back!"
    date: 2008-05-22
    body: "Just one word: Sweet!\nOk, one more word: Thanks!"
    author: "Firmo"
  - subject: "Re: I'm back!"
    date: 2008-05-22
    body: "Fine. :-)\n\nSee, guys, there actually was a reason for the delays. But asking for it always resulted in \"don't argue\" or \"if you can do it better, contribute!\""
    author: "Erik"
  - subject: "Re: I'm back!"
    date: 2008-05-22
    body: "Good stuff Danny! the digest has become somewhat of a KDE institution. Your aKademy award last summer was very well deserved! :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: I'm back!"
    date: 2008-05-22
    body: "Huray!\nThanks Danny, the Digest is really cool."
    author: "Dirk"
  - subject: "Re: I'm back!"
    date: 2008-05-22
    body: "That's great, I was concerned about the gap, really good news, thanks a lot!\n"
    author: "Michal"
  - subject: "Re: I'm back!"
    date: 2008-05-22
    body: "Thanks for all your hard work!"
    author: "mactalla"
  - subject: "Re: I'm back!"
    date: 2008-05-23
    body: "Good to see you back, Danny!"
    author: "Riddle"
  - subject: "Re: I'm back!"
    date: 2008-05-23
    body: "Thanks Danny :D"
    author: "Lee"
  - subject: "Re: I'm back!"
    date: 2008-05-24
    body: "why not just ignore all the past months ones, and start with the latest releases."
    author: "anon"
  - subject: "Re: I'm back!"
    date: 2008-06-04
    body: "I'm delayed in my reading :-)\n\nI'm getting the following error:\nFatal error: Call to undefined function: preg_split() in /var/www/commit-digest/404.php on line 7\n\nAny clue what's happening?"
    author: "Anon"
  - subject: "You don't know what you've got till it's gone"
    date: 2008-05-22
    body: "Welcome back, Danny - you (and, of course, the Digest :)) have been missed!"
    author: "Anon"
  - subject: "Re: You don't know what you've got till it's gone"
    date: 2008-05-22
    body: "Thank you Danny, it's always a pleasure :)"
    author: "Thomas"
  - subject: "Re: You don't know what you've got till it's gone"
    date: 2008-05-22
    body: "yeah we missed you sweet danny ;-)"
    author: "mimoune djouallah"
  - subject: "Kontact"
    date: 2008-05-22
    body: "Welcome back Danny, it's great to hear from you again :)\n\nI must say I am very impressed with the professional look of Kontact at the moment  - for a while I had to reluctantly agree that Evolution was a better choice. But now, with a beautiful design and hopefully with MS Exchange support thru Akonadi in 4.2/.3/.4, it should be the best free email program around. One other feature I hope gets implemented is the Outlook 2003/7 three column style, where the message is beside the list of emails, and not below it. But this is a minor niggle, so it matters little.\n\nAlso excited about improvements to Plasma, the \"Incredible Machine\"-like game in development, the work to add a KWin cube, and a million other projects from Akonadi to Phonon. KDE development has never been more interesting!"
    author: "Parminder Ramesh"
  - subject: "Re: Kontact"
    date: 2008-05-22
    body: "Do I really have to wait for 4.2 for Kontact to support Exchange? That makes me a sad panda :("
    author: "SMB"
  - subject: "Re: Kontact"
    date: 2008-05-22
    body: "\"\"\"\nOne other feature I hope gets implemented is the Outlook 2003/7 three column style, where the message is beside the list of emails, and not below it.\n\"\"\"\n\nDo you mean something like this?\n\nMenu:\n Settings\n -> Configure KMail\n -> Appearance\n -> Layout\n -> Message Preview Pane\n -> Show the message preview pane next to the message list"
    author: "nik"
  - subject: "Re: Kontact"
    date: 2008-05-23
    body: "Sounds like it! Thanks so much for pointing it out! :)"
    author: "Parminder Ramesh"
  - subject: "Gentoo"
    date: 2008-05-22
    body: "Yay, thanks Danny - was missing the Digest!\n\nTotally off-topic - under Gentoo, does anyone know what I need to emerge to get the Plasmoids, apart from the few basic ones?  I mean ones like the Comic Strip displays, etc...  I'd rather pull it from Portage than install manually."
    author: "Phil"
  - subject: "Re: Gentoo"
    date: 2008-05-22
    body: "I think it's \"plasma-extragear\"."
    author: "Riddle"
  - subject: "Re: Gentoo"
    date: 2008-05-22
    body: "Nope - I've done a search for plasma, plasmoids, extra and extragear.  There doesn't seem to be anything related."
    author: "Phil"
  - subject: "Re: Gentoo"
    date: 2008-05-22
    body: "Ahhhh.  Looks like it's only in SVN.\nI really love the Gentoo guys, I do - but their attitude to KDE4 really sucks."
    author: "Phil"
  - subject: "Re: Gentoo"
    date: 2008-05-22
    body: "Things will take off after 4.1, when there are more plasmoids out of extragear/playground , besides, it shouldn't be too hard to make a 4.0 playground/extragear plasma ebuilds from the svn versions"
    author: "G2g591"
  - subject: "Re: Gentoo"
    date: 2008-05-23
    body: "There is a great kde live (aka svn) overlay for Gentoo:\n\nhttp://forums.gentoo.org/viewtopic-t-690914.html\n\nYou just have to switch to paludis (if you have not already ;-)"
    author: "JAKuhr"
  - subject: "Re: Gentoo"
    date: 2008-05-23
    body: "There was also an extragear-plasma for 4.0 (4.0.0 and 4.0.1, then it moved on to 4.1 so there were no further 4.0.x releases), I don't know why Gentoo isn't packaging that. (Fedora is.)"
    author: "Kevin Kofler"
  - subject: "Motivation statistics"
    date: 2008-05-22
    body: "Hi there\nIn the motivation statistics seems to be a small mistake.\nThere are 50.5% Volunteer but the graphic beside shows something different.\n\nAnyway, thank you for writing this interesting summery.\n\n"
    author: "XCG"
  - subject: "Re: Motivation statistics"
    date: 2008-05-22
    body: "When you add up the number you get about 103% Motivation. :)"
    author: "AC"
  - subject: "Re: Motivation statistics"
    date: 2008-05-22
    body: "That says a lot about the KDE developers, doesn't it? :)"
    author: "Soon off to bed"
  - subject: "Re: Motivation statistics"
    date: 2008-05-23
    body: ":)"
    author: "Andres"
  - subject: "Re: Motivation statistics"
    date: 2008-05-23
    body: "Yes it says they need to get their math straight"
    author: "ad"
  - subject: "Marble"
    date: 2008-05-23
    body: "Strange question: Would it be possible to create and load old Earth maps into Marble? Like the ones from nasa that show the continents 65m and 250m years ago? "
    author: "Jens"
  - subject: "Re: Marble"
    date: 2008-05-23
    body: "Of course ;-)"
    author: "jospoortvliet"
  - subject: "Re: Marble"
    date: 2008-05-23
    body: "Yes, it would be possible as long as the maps get provided in plate carr\u00e9e projection (also referred to as \"Equirectangular projection\"):\n\nhttp://en.wikipedia.org/wiki/Plate_carr\u00e9e_projection\n\nThere are morphing tools and HOWTOS on the web that can tell you how to morph ancient maps into the desired projection. It doesn't require too many skills. It's just a matter of doing research on the matter, finding out about the copyright and legal terms and sitting a few hours in front of the computer to tweak the map according to your needs.\n\nHave a look here:\n\nhttp://spin3d.com/\n\nThe \"equirectangular image\" that is the result after half of the tutorial is what you need.\n\nOnce you got an appropriate image it's very easy and a matter of less than five minutes to get such a map imported into Marble (and doesn't require a single line of code):\n\nJust have a look here for more information on how Marble deals with maps:\n\nhttp://www.kdedevelopers.org/node/3269\n\nWe'd be very much interested in getting free data for ancient maps and would like to offer them for download via GHNS in Marble!\n\n"
    author: "Torsten Rahn"
  - subject: "Re: Marble"
    date: 2008-05-25
    body: "Ok, thanks. I'll see what I can come up with. Main problem is the low detail level of most prehistoric maps, since they are generated anyway you should expect them to be bigger than 800x500...\n\nMy idea stems from PC-Dinos (1993/94) which was a rather cool program and back then I spend (way too) many hours with it. It was the reason I bought a cd drive, it came on two (or even three) cds and you had to swap them quite a bit when changing continents. Today this will probably fit in just a few megs.\n"
    author: "Jens"
  - subject: "Please, This Is NOT a Flame..."
    date: 2008-05-23
    body: "Hey!\n\nOK, I have to...\n\nI've been using KDE since the pre-1.0 days (whatever shipped with RH-5.0, IIRC).  I love what power and control KDE has given to me all these years.\n\n...\n...\n\nCan I have my Desktop back?  Please?\n\nI fully respect the decision to do Something Different with the desktop, but can I have my Good Ol' Desktop back?\n\nI was playing with the OpenSuSE KDE 4.0.4 Live CD and was drop-jawed.  KDE4 is one of the most amazing and powerful things I think I've even seen my machine do.  Hell ANY machine of mine, for that matter.  I *DO* like the idea of having a \"Desktop\" a \"Launch Pad\", but I just want my Desktop back.  \n\nIs there a way to move it back to a 3.5.* style desktop?  I know there's still the ~/Desktop where I can still put the few things I like having on my Desktop, but I can't *interact* with the Desktop the way I want to / am used to.\n\nIs there a plugin, plasmoid, config setting, that gives me the Old Boring Desktop back?\n\n(I use Gentoo.  As you read above, Gentoo is not really making it easy to use KDE4 < 4.1.  I (frankly) don't want to go through all the BS to get KDE4 on my machines if I can't really use it the way I want to (yet)).\n\nThanx.\nM.\n"
    author: "Xanadu"
  - subject: "Re: Please, This Is NOT a Flame..."
    date: 2008-05-23
    body: "Read Aseigo's blog...\n\nhttp://aseigo.blogspot.com/2008/05/no-more-desktop-icons-in-41.html"
    author: "jospoortvliet"
  - subject: "Re: Please, This Is NOT a Flame..."
    date: 2008-05-23
    body: "Thank you for the link.\n\nYa know, I've read through that exact (I think) page a couple times, and never saw this part:\n\n\"That last bit is important: it means that you can have an Old Skool(tm) desktop with an icon mess if that's what you really, really want.\"\n\nAnd he went on to say that \"support\" for the Old Way will improve as KDE4 gets more polished.\n\nGood News to me!\n\nAgain, thanx for the link.  I'll shut up now... :-)\n\nM."
    author: "Xanadu"
  - subject: "Re: Please, This Is NOT a Flame..."
    date: 2008-05-24
    body: "Chani said in her blog that the folder view can be used as a containment. Is that right? And if yes, does this containment support a background image? If these requirements are met there is a functionally equal replacement for kdesktop."
    author: "Stefan Majewsky"
  - subject: "Re: Please, This Is NOT a Flame..."
    date: 2008-05-25
    body: "from link above:\n\naseigo:\n\"In 4.2 we'll be separating context menus and background rendering from containments so that those functionalities can be easily shared; so while in 4.1 you can set the folder view as your desktop containment ... it's not going to be as pretty as it will be in 4.2. In 4.2 it will be completely seamless, whereas right now you'll get a \"nice\" wallpaper based on the applet background svg. Call it the Model T wallpaper: any colour you want, as long as it's the applet background ;)\"\n\nshort: in 4.1 folders on desktop only on black/one-color background ;]\n\nPS. I love idea ofviewing different folders on desktop. I wanted it for years. Thx!"
    author: "Dog"
  - subject: "Correction"
    date: 2008-05-24
    body: "Hey Danny, I think you mean \"KDE 4.1 BETA 1\" tagged for release.\n"
    author: "tobami"
  - subject: "Re: Correction"
    date: 2008-05-24
    body: "OK, I didn't realize the digest was from a month ago, just before Alpha 1 was released, sorry =)\n\nEager to check out Beta 1!\n"
    author: "tobami"
  - subject: "Re: Correction"
    date: 2008-05-24
    body: "or Beta 1 for that matter.\n\nI thought it was just aplha 1 that was released. \n\n______\n(I only follow the dot occasionally, and the digest posts linked from the dot. <-- all the info I need. :p Anticipation for KDE 4.1 is too great already anyways. I've been looking forward to KDE 4 since Aaron Seigo's video released around Christmas. If it wasn't for that video, I probably wouldn't have even known about KDE 4. - well I would, but I wouldn't be nearly as exited as I am now. - Hopefully there will be a keynote for KDE 4.1.0 when it becomes released. I love the way to visually learn about features and get a quick overview/glimpse of what KDE 4 has achieved and is capable of.)"
    author: "Mike"
  - subject: "Newbie question: Where can I get KDE 4.1 alpha?"
    date: 2008-05-24
    body: "Sorry for the newbie question: \nWhere can I get KDE 4.1 alpha for Kubuntu, (or if that doesn't exist for openSUSE)?\n\nI want to try out and help hunt down problems, but I don't have the skill to compile KDE from scratch. Where would I find \"prepackaged\" KDE 4.1 alpha so I can install it easily and test it. \n\n_____\n(obviously this won't go onto my production system. I have a separate installation of Linux on a computer that I can experiment with. I'm not worried about stability. Quite the contrary: I expect breakages and hangups. I want to help fix them (by finding them - I'm not a programmer to fix them directly.) BEFORE KDE 4.1.0 is released. <-- Don't want to repeat the disaster that is Kubuntu 8.04 initially.)"
    author: "Mike"
  - subject: "Re: Newbie question: Where can I get KDE 4.1 alpha?"
    date: 2008-05-24
    body: "For openSUSE 10.3, add the following repository: http://download.opensuse.org/repositories/KDE:KDE4:UNSTABLE:Desktop/openSUSE_10.3\n\n(Change the last number if you've got another version of openSUSE, but 10.3 is the best base.)"
    author: "Stefan Majewsky"
  - subject: "Re: Newbie question: Where can I get KDE 4.1 alpha?"
    date: 2008-05-24
    body: "> Don't want to repeat the disaster that is Kubuntu 8.04 initially.\n\nI don't know what's happening (or not happening) in Kubuntu lands, but I'm currently using openSUSE 11.0 Beta3+ on my notebook (which I'm using every day in lectures etc.) and the KDE 4.0.4 included there works nearly flawless (the only real bug is that Konqueror crashes when okularpart is loaded)."
    author: "Stefan Majewsky"
  - subject: "Re: Newbie question: Where can I get KDE 4.1 alpha?"
    date: 2008-05-24
    body: "Debian has had 4.1 precompiled packages since long ago in the experimental repository."
    author: "Anonymous Coward"
---
In <a href="http://commit-digest.org/issues/2008-04-27/">this week's KDE Commit-Digest</a>: Rating support, with a <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> backend in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. <a href="http://edu.kde.org/kstars/">KStars</a> gets a conjunctions predictor module. Basic XSLT support and a HTML export GUI in <a href="http://edu.kde.org/parley/">Parley</a>. Work on clouds view integration in <a href="http://edu.kde.org/marble/">Marble</a>. Keyboard navigation support in <a href="http://games.kde.org/game.php?game=knetwalk">KNetWalk</a>. The start of a new dock window layout in <a href="http://techbase.kde.org/Projects/Kooka">Kooka</a>. Work on tabbed interface user interaction in <a href="http://enzosworld.gmxhome.de/">Dolphin</a>. A paste text snippets applet in <a href="http://plasma.kde.org/">Plasma</a>. charselectapplet is deleted, replaced by a Plasma-based equivalent. Welcome/info screen stylings extended from the KDE desktop into <a href="http://pim.kde.org/">KDE-PIM</a> applications and KInfoCenter. Various work, including improvements to the collection and On Screen Display in <a href="http://amarok.kde.org/">Amarok</a> 2. Various small features in <a href="http://ktorrent.org/">KTorrent</a>. Initial work on a <a href="http://www.koffice.org/krita/">Krita</a> module for "WaterStudio". KBlocks moves from kdereview to kdegames. <a href="http://pim.kde.org/akonadi/">Akonadi</a> server and shared components move to kdesupport. "WaterFlow", a library and program to create computational flow chart-based diagrams is imported into KDE SVN. <a href="http://dot.kde.org/1209500572/">KDE 4.1 Alpha 1</a> and <a href="http://dot.kde.org/1210273570/">KOffice 1.9.95.4 (KOffice 2 Alpha 7)</a> are tagged for release. <a href="http://commit-digest.org/issues/2008-04-27/">Read the rest of the Digest here</a>.

<!--break-->
