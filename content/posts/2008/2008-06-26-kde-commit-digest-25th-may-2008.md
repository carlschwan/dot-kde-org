---
title: "KDE Commit-Digest for 25th May 2008"
date:    2008-06-26
authors:
  - "dallen"
slug:    kde-commit-digest-25th-may-2008
comments:
  - subject: "Danny, you complete slacKer!"
    date: 2008-06-25
    body: "I'm going to preempt some comments here...\nI am aware that i'm still a month behind on these Digests, and that my attempts to catch up last weekend didn't fully get the job done.\n\nHowever, i'm in full time employment this summer, which is pretty tiring in itself. And i've had and still have other stuff to do in the tangible world.\n\nBut, this is a matter of priority for me, and i'm trying my best to catch up as soon as possible.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-25
    body: "Hey Danny give Danny a break!  Danny's volunteering his time ya know, Danny.\n\nTime to go take my Lithium now..."
    author: "Mark the anon"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-25
    body: "\"I'm going to preempt some comments here...\"\n\nAin't gonna get nothing but luurve from me, baby!  You go ahead and take your time :)"
    author: "Anon"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "I love the digest, so by all means do whatever it takes to keep yourself from burning out."
    author: "James Spencer"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "Do you need help?"
    author: "Damiga"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "No worries! Good work!"
    author: "winter"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "I like the slacKer with a capital K!\n\nPerhaps a strategy to catch up would be to just do a high level commit digest of just some important bits for the last month.  No commit-level detail, just some general efforts that have been going on in the last month in one digest..."
    author: "Leo S"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "1) Love the slacKer with a kapital K ;)\n2) You could use some help. Don't burn yourself out.\n"
    author: "Riddle"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "I just want to tell you something.\n\nCouple years back I moved to France for 11 months, didn't know the language, anyone in there, and for the first two months my only access to Internet - to keep me at least somewhat sane - was in an Internet caf\u00e9, where I visited about once a week or so. And besides reading my mail (which there never was too much), how did I use that time? By reading carefully about the developments of KDE4 (and 3) from the KDE Commit-Digest...\n\nJust wanted to tell you that you are doing an important work here :-) I understand that it is a taxing, and since I hope you will be making it in to undetermined future, my wish is that you don't wear yourself up with it. So relax :-)"
    author: "outolumo"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "thanks for all your efforts. how can we help you to share the work. is that possible?"
    author: "DITC"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "> i'm trying my best to catch up as soon as possible.\n\nyou're doing great; i'm enjoying them as they arrive, lag or not =)"
    author: "Aaron Seigo"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-26
    body: "Old digests are like wine ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-27
    body: "Do people really read the selected commit messages? They must be a lot of work to select, and while the summary and the interviews are interesting even for non-developers (what I am), the commit messages are far too much to read, and, especially bug fixes, are not interesting. And bugs in which one is interested can be tracked in the bugzilla. Wouldn't it be worth omitting the selected commits part if it took less time that way?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-27
    body: "#2"
    author: "asdasd"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-27
    body: "I read the commit messages.  I skip the projects that I don't use, but read the messages for the ones I do so that I can see what's coming down the pipe.  It's nice to see when bugs get fixed or what features are being worked on."
    author: "mactalla"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-28
    body: "Sure, it would take a lot less time that way, but I wouldn't know what happened in that week then, and so there wouldn't really be a summary to speak of ;)\n\nAnd though I know that not everyone reads the selected commits, it's still nice to have.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-28
    body: "Danny, a suggestion: maybe an option could be added to the digest, so that the comments for individual commits are hidden by default?\n\nThen by clicking a button / link for a particular commit in the list reveals the comment. Thinking this would help keep the overall length of the digest down in the browser window, whilst still allowing people to view the comments for the commits they are interested in.\n\nI know your a busy guy. If you think its  a worthwhile idea I could have a go at implementing this myself - drop me a line at stephen.a.ellwood@gmail.com.\n\nCan I just add my voices to the others on this board - thanks for doing such a superb job with the digest! I read it every week - its excellent! :)\n\n-=+Steve+=-\n\n"
    author: "Stephen Ellwood"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-29
    body: "You are being manipulated. It is a plot. Or at least it was when I did the digest.\n\nThe commit comments contain links to patches. It allows, even encourages readers to look at code, learn to read it. Eventually learn to produce patches.\n\nIt is an evil subliminal message that focusses on the developer and development process, and has the evil potential of motivating people to write code.\n\nI'm not sure that is allowed any more in KDE, but there it is.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-30
    body: "Is this meant to be a joke? ;) I can't imagine that linking to patches is considered _evil_."
    author: "Riddle"
  - subject: "Re: Danny, you complete slacKer!"
    date: 2008-06-30
    body: "\"Is this meant to be a joke? ;)\"\n\nYes; Derek used to write the Digest before Danny took over :)"
    author: "Anon"
  - subject: "MSWord-to-ODF filter for KWord"
    date: 2008-06-26
    body: "I definately need to check this out, especially now that KOffice is going multiplatform.  I'm forced to use OOo on Windows boxes currently."
    author: "T. J. Brumfield"
  - subject: "Re: MSWord-to-ODF filter for KWord"
    date: 2008-06-26
    body: "As I wrote in the commit message, it's not actually enabled yet, but hopefully I'll enable it soon. It's still very much a work-in-progress."
    author: "Benjamin"
  - subject: "PHP-Qt"
    date: 2008-06-26
    body: "The PHP-Qt bindings move from playground/bindings to the kdebindings module. KDE 4.1 Beta 1 is tagged for release.\n\nOK, that made my day!\n\nOn other parts, my impression is that KDE4.X started a little slow, but the train is now gaining power and letting the vapor behind. Overall, I'm starting to get impressed by quality of KDE apps, even plasma is starting to become solid and nice.\n\nJust one question: KDE4 overall seems to use more space in screen than KDE3 (but later betas are diminishing that, I have to say). Is there a plan do deal with small screen devices like EeePC 701, HP 2133, Dell-E, Cellphones and the likes?\nIt would be great if there was an option to use the  minimum resolution as possible in a way windows fit the screen... But I do admit I do not have a good suggestion (mine of using qss was rejected) to use here :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: "As always, thanks Danny :)\n\nTo use KDE4 on a small screen have been thinked. MID will be a plateform where KDE4 will be cool to use. And you can do it now :) :\n- use a small, space saving theme (sorry but oxygen wont do it...)\n- use the plasma ability to scale it-self\n- use smaller font\nAnd voila\nEven, plasma is already ready for touch-screen only device. (You know the hated cashew is there for that)\nSo I dont see any problem to do it now with the lastest beta.\n\nyours,\nSeb"
    author: "Shuss"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: "The oxygen style does not support smaller screens (as it does not support many other cool things), but theme engines such as Skulpture (see kdelook.org) are beautiful, polished and support small resolutions. Eg. margins are adjusted to font sizes... "
    author: "Sebastian"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: "Yes, I can do that, BUT, there are some apps that do not behave well under small resolutions.\nI can tell about kmenuedit (at least in the kde3 version) that simply do not fit screen, using a LOT of white spaces all around. Kcontrol, in the other hand, does a fine job inclosing kcmsheels with scroll bars in the case those do not fit.\nIt is not that hard to think, test and develop for small resolutions, but it must be a global effort of whole KDE packages.\n\nBut I'm not saying that KDE itself must focus on small screen, if you look at examples we have now, we'll see the write-once fit-all is not true:\n- microsoft have windows-ce/mobile and windows xp for small fit job\n- apple developed a system for iPhone that just resembles OSX, but uses much less spacing\n\nIf KDE apps just used QtDesigner ui files we could rearranje the UI elements to fit, I would gladly give a hand, but it do not seem to be the case.\n\nSO, in a general way, do KDE team/developers want help in this case?\nWhat I can do it little, but maybe helpfull:\n- run all KDE apps and try to resize the window to the minimum necessary space that fits all widgets\n- note all apps that when resized to small sizes, add scrollbars to deal with it\n- do all metrics and see what apps need adjusts\n- propose solutions, find places that can use less space where we can remove hardcoded spacing for qtspacers that auto-adjust to windows size\n\nIf you have any interest, and think it's worth the work, I and will be glad to help :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: "Patches to fix those problems are generally welcome, but it depends a lot on the actual changes if the patch can get merged.\n\nDo also think of translation (German makes most texts longer, for example). Furthermore, not all applications are using UI files. The preferred thing to do is replace the hardcoded UI with a designer-generated UI file.\n\nYou could also think of creating a KStyle for small resolutions."
    author: "sebas"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: ">You could also think of creating a KStyle for small resolutions.\nYep, I'm working on that, learning how to code a small (very small) style :)\n\nAgreed about translations and ui being hardcoded (I've already said that in the previous post). But there is still a lof of empty spaces that can be eliminated and can't be dealt only thought a theme.\n\nLook at this:\nhttp://markus.wernig.net/en/it/kmenuedit.png\n\nI think it's a good example. Having a lot of boxes (more clear here: http://markus.wernig.net/en/it/Screenshot.png), just uses space without need.\nWhat I think could be done, is placing run in terminal and run as different user in an \"advanced\" button that would open another small window with those options.\n\nOr, you could always put scrollbars in the window in the case it does not fit resolution. Having windows bigger than the screen is much worse than having to scroll, IMHO."
    author: "Iuri Fiedoruk"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: "The space is a function of the Oxygen theme, not the toolkit or platform, using a different theme reduces the space used.  I'm happily running KDE4 on my EEE and the flexibility of Plasma is brilliant for such devices, just as Aaron intended when he designed it (something most people forget when they moan about the state of plasma).  There's also a GSOC project looking at the wider issues in KDE with Small Form Factor devices, such as dialog sizes, etc.\n\nJohn."
    author: "odysseus"
  - subject: "Re: PHP-Qt"
    date: 2008-06-26
    body: "Good to hear, BUT, not all can be solved with the theme. There are apps that do not take resolution in account and places a lot of dialogs one under other, take kmenuedit for an example.\n\nI do think Plasma is better than kdesktop/kicker for this matter, but I do not know about kde apps. I will measure minimal window sizes this weekend and publish, even that it is useless, ate least I will be able to say if it's better or worse :)"
    author: "Iuri Fiedoruk"
  - subject: "finally.."
    date: 2008-06-26
    body: "..some moderation. I am happy to see that some comments have beeen removed. Really, some posts were starting to eat our brains, distracting our work and producing nothing useful - unless frustration is considered any use.\nthanks for the moderation, i am unhappy that it became necessary, but it is. a reader directed mod up & mod down a la slashdot or digg might fix the situation as well. but i guess if any of us knew how to implent it at the dot, it would have been done already."
    author: "elveo"
  - subject: "Re: finally.."
    date: 2008-06-26
    body: "I find it sad, that ist has come so far.  But it ist really nessesary."
    author: "dirk"
  - subject: "Re: finally.."
    date: 2008-06-26
    body: "Yes, you're right. It is necessary to silence those who ask questions, as asking questions is dangerous because it leads to thought and introspection."
    author: "Joseph Blough"
  - subject: "Re: finally.."
    date: 2008-06-26
    body: "I'll reply to this one, since I'm deleting comments on this post.  It is not to silence those that ask questions, it is to silence those that do not treat others as human beings.  Invoking Godwin's law is pretty much guaranteed to get comments deleted at this point, so is overt rudeness.  KDE is not going to encourage people to stop asking questions, or posting about problems.  We are going to encourage people to do exactly that, but in a civilized fashion.  There will be other changes happening to the KDE community.  \n\nThis whole thing is unfortunate as the dot was one of the last outposts of unmoderated speech on the internet.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "Moderation and censorship are different things. In many moderated message boards posts are not deleted and any user is free to have a \"non-moderated\" experience - best of both worlds. \n\nIt's not unfortunate that moderation seems to be needed. What is unfortunate is that post are being censored. \n\n"
    author: "ad"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "We've decided to put the contributions of members of our community above the privilege for everybody to say whatever, whenever on the Dot."
    author: "sebas"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "Hello,\n\nwell put Sebas and I fully concur. I guess, we have to see it as part of the success and failure of KDE that these things started to happen.\n\nSuccess as in many more people than before are interested in KDE progress.\n\nFailure as in delivering a more clear view of what people can and can not expect from KDE, and how to influence it in a forum like this one.\n\nMy assumption is that way too many people believe that posting about bugs here is somehow the only way to get them fixed. It also seems some people want an interactive forum to talk about concrete bugs. Probably dot.kde.org could feature high profile bugs that don't get fixed proactively in articles, and explain why they are not fixed, while deleting pseudo bug reports in comments to other kinds of articles.\n\nWay too many people just don't understand why small things can become hard to achieve. But when was the last time, any KDE developer took the time to explain it to normal people?\n\nThere are people here that believe insults are appropiate. That is very unfortunate. I personally would ask for the impeachment of the KDE president, but I won't, as I don't see the alternative. But insults are really bad and should be banned. Any ad hominem attack leads to waisted emotions and time by the involved parties.\n\nBut deep down, I think that is because of the lack of explaining how else to influence the process. People do want to influence and talk is cheap. Make a better case of explaining how comments get nothing done if they are insults, but the contrary. Then people may still decide that the alternative involves too much time, but many probably will choose to stop flames, when they get reminded of the effect periodically.\n\nWhere does KDE lay down the ethics of Free Software, and where does it say, what users can expect from it? All I see is how professional, perfect, etc. KDE wants to be, and no pointing out, of shortcomings to be expected. That is a serious mistake. People do not want to be poisonous, not all at least. I think the majority of those that behave this way, just don't understand what is happening with them and their victims, and esp. developers.\n\nWith the source open, what's missing is explanation to the people, what this actually means. And how it's not suddenly making everybody solve their pet problems for them.\n\nAnd talking about fights: Personally, I was rather astounded (and unable to comment so far) to see that with all Plasma - design, and goodies - the good old desktop (icons and background image) won't be possible before 4.2. That was breath taking news. Not because I would use that (in fact I disable kdesktop always, no image, no icons, never look at it), but because I am so surprised and taken by that.\n\nAnd was there any kind of statement that says, how this could even potentially have any reasons that go in the direction of possible error, mistake, inability or unavailablity of anyone? Nah, it was a semi-troll blog posting, that made people that didn't read well beyond the headline and look beyond the screenshot, that desktop icons were removed. And then there were comments answered aggressively and with a bold tone that seemed to imply, that those who failed to understand, were at fault or even poisonous.\n\nWhen was the last time any KDE developer said in public: I made a mistake. I have made a wrong plan. I forgot that. I think a lot less frequent that what I can expect it to have happened.\n\nIn the above case, I would have found one more reason to admire that developer if he had said: \"Look, we are working on it, but we won't finish it. For this reason, backgrounds of widgets can't be images yet. And this is why we didn't think of this as important yet. And probably I am sorry, or I don't care.\" But no nothing like that. All Plasma is perfect plan and reality, from 4.0.0 on, because then we were told we couldn't judge the untested Plasma before the final release, but not via release candidates.\n\nTo me, one more reason to impeach the KDE president, for loosing perspective.\n\nSo there is a tension, that I feel too. I am sort of disappointed for not having any discussion about what went wrong, what can be learned from it, etc. for a long time now.\n\nBut if KDE developers make no mistakes, how are users to understand their role correctly? How can they help anyway, if others know that they themselves know it better in advance?\n\nI sense that the KDE developers and users lost contact. Instead of taking time to explain how KDE 4 is going to be time taking, features were hyped that never came to existance (without mentioning those that came into existance instead).\n\nNow people seem to think that telling everybody how KDE 4.1 will be great is any good. It is not. We KDE should be busy telling everybody how and why KDE 4.1 is not yet great, and why not. And what the plan is to actually get a KDE 4 that is as great as the KDE 3 managed to be.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> I personally would ask for the impeachment of the KDE president, but I won't, as I don't see the alternative.\n\nKDE does not have a president, it doesn't even have any kind of governmental body.\n\nIn case you are referring to Aaron Seigo he is a member of the board and president of the KDE e.V.\nSince I can't remember anyone of the e.V. membership voicing any complain about Aaron being on the board or being the e.V.'s president, I can only assume that you are not a member.\nIn which case you can't know that a huge majority of actual members voted for him to be in this positions and continue to support this descisions.\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Hello,\n\nthat \"impeachment\" part was intended to be a funny. I call Aaron \"KDE president\", and I think he well deserves such a title, although I realize it's only KDE e.V. that has such a role.\n\nLike I said, he should be president for lack of better alternative. \n\nI only wish there was a sense for being _open_ besides where there are pretty screenshots and big achievements, but also in moments of failure to achieve goals. Or in the alternative, provided all goals were met all the time, better communication about the actual goals that will be and have been achieved for releases.\n\nOtherwise users are only treated as cheering crouds, rather than friends. In the case of Plasma, I concur that not all discussion, or not even a reasonable part of the discussion, was contributing to any kind of improval of the situation. I sincerly view this situation as unacceptable, but I only see a key in Aarons hands.\n\nFor lack of cheering crouds, it could be that the feedback became overly detrimental to his motivation. But I want to put to record, that I am entirely convinced, that Plasma was miscommunicated (to users) for the largest part of its planning stage and implementation, and that for this problem, and almost only that, users are confused and in part even (unrightfully so) angry.\n\nPlease improve communication about what can be expected and what not. How to behave and how not. \n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> that \"impeachment\" part was intended to be a funny.\n\nyou really need to work on your delivery, because it really didn't come across as very funny at all.\n\n> I only wish there was a sense for being _open_\n\nthis is a mischaracterization.\n\ncase in point: i find the whole idea of a traditional application menu to be outdated and silly. people harassed me endlessly to make one. (as if i owed them something?)\n\ni stated repeatedly that i didn't really want to write one, that it was perfectly possible for someone else to do so and thus was very low on my priority list.\n\nafter dealing with endless and often extremely bitter responses i finally made it very clear that not only did i not want to write it, that i had finally just removed it from my TODO list altogether.\n\n(hint: i am not your slave, and give to those that i feel like giving to.)\n\nSebastian Sauer stepped up at that point and wrote one, using the Kickoff internals. he posted it for inclusion and go the thumbs up immediately from the team (including myself).\n\nso there's an example of something i not only couldn't care about, but eventually came to completely ignore to the point of stating i wouldn't do it and someone else came along, did the work and it made it into 4.0\n\nthat's probably one of the more extreme examples i can think of.\n\nanother might be the way layouting was done in 4.0. i really, really didn't like the approach, stated so on the mailing list, but agreed to try it as an experiment. turned out i was pretty much right, and in 4.1 we were able to switch to WoC anyways.\n\nthere are many other less extreme examples, of course, that demonstrate the same sort of openness.\n\nthere are also examples where i stick to my guns.\n\nthat's normal and natural. and i'm much less of a prick about my thoughts / ideas than many other maintainers out there in free software land in the process.\n\nas for all the \"they don't take public responsibility\" statements in your earlier posting to this thread, i suggest you go back and read the various advisories from the project about 4.0, go see all the bug reports we've closed with patches on code we've written (don't see us shirking that, do you?) or every time i or someone else has said, \"i was wrong\" on a mailing list.\n\nwhile i don't see the need for constant public grovelling, people do tend to be pretty honest and frank here, both about success and failure. meanwhile, commentators and held to exactly zero standards.\n\ni'd also suggest that in all your text above, you've completely missed the context of people such as myself having to deal with the ability for a handful of people to rant, whinge, do and say hurtful things with no counter reaction for years now (yes, before 4.x even). when you add in everything that happened, it's a rather different picture that gets painted from what you presented."
    author: "Aaron Seigo"
  - subject: "Kickoff"
    date: 2008-06-29
    body: "As someone who goes on the record of blasting Kickoff, I'm not opposed to a new menu.  I'm really looking forward to trying out Raptor.  I just find that the interface slows me down considerably in navigation, plus I can only see a few items at a time.\n\nSearch is nice, but Krunner has search now.\n\nI'd love to see an evolutionary menu, but I think Kickoff isn't quite it."
    author: "T. J. Brumfield"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Hello Aaron,\n\nall \"your\" trouble with that comes from the fact that the user community is uneducated about what it can expect and what not from a developer.\n\nIt's entirely obvious to a lot of people that you are not the person that must implement any specific feature. But why doesn't KDE communicate that fact all the time to its users? Why should individual developers fight this in their blogs and comments here? How are people supposed to know?\n\nIt would be much better to have something abstracted from concrete situations, that explains to users what works and doesn't work with developers. And tells users when to educate their fellow users about wrong attitude, with information to point to. \n\nYours,\nKay\n\nPS: I love to recall your fight about \"Usability\" and the good change it brought to the community very well. Just one of many good things (other than code) on your list. That call for impeachment is funny, because it pretends power of users over developers that luckily doesn't exist.\n\n"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "> Sebastian Sauer stepped up at that point and wrote one, using the Kickoff internals.\n\nNot exactly correct since (at least most of) the work was done by our unique Robert :-) I just did saw the need for it, put it on my todo-list and started to implement it while letting others know about that and someone else fast faster and did a better job there. So, it's also a good sample of coperation and that the best way to achieve something is just to do. Someone said once something very wise; FOSS is about being able to do it your own rather then forcing others to do it.\n"
    author: "Sebastian Sauer"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "\"I only wish there was a sense for being _open_ besides where there are pretty screenshots and big achievements, but also in moments of failure to achieve goals. Or in the alternative, provided all goals were met all the time, better communication about the actual goals that will be and have been achieved for releases. Otherwise users are only treated as cheering crouds, rather than friends.\"\n\nIf a friend fails to achieve something you expected you don't berate them and say \"but you promised me!!! how dare you!!!! you owe me!!\" you say \"shit happens, next time\" and move on.  You trust that they did their best and, given time, will hit the mark. Aaron's mentioned elsewhere in this thread that the developer-user interaction had veered well away from 'best friends' to feeling more like an 'abusive relationship' - where the community made demands and berated the results whatever their form.\n\nThe alternative is not worshipful thanks for every shiny widget but a realistic appreciation of the time effort and goodwill of the developers/contributors for what they produce.\n\nYes, we need openness and friendship - but in both directions."
    author: "Martin Fitzpatrick"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> Or in the alternative, provided all goals were met all the time, better communication about the actual goals that will be and have been achieved for releases.\n\nUnfortunately this has been tried and failed, as we can see on the topic of folderview.\n\nWhen the folderview came around all developer immediately got that this indeed allowed whole new set of usage patterns while still keeping the primitive one-local-folder-fullscreen available as well.\n\nObviously it is easier for developers to understand the implications of something since they tend to think along similar paths, so after some initial misunderstanding that a folderview would always just be a windowed area on the desktop, Aaron showed explicitly that a single folderview could be switched into fullscreen mode however at this point couldn't draw a wallpaper but would be able to once feature freeze had been lifted.\n\nAt this point anyone still claiming that the usual primitive icons-on-desktop should probably have been ignored since they obviously didn't want to do even simple abstractions like \"if it can display a check board pattern now, it will be able to draw an image once this is implemented\".\n\nNow Aaron is a really nice guy and even did a screencast to make it even more obvious and no longer require any kind of intellectual exercise, but since at this point we are already dealing with trolls, it couldn't have any positive impact anymore, just making everybody else feeling frustrated with those few people's extreme ignorance.\n\nIt's a pity that the resulting message to developers is that they are no longer welcome to release early, to no longer involve users in the development process, to eventually restrict dicussions to \"inner circles\".\n\nWhat a shame, I really liked the special relationship between users and developers in Free Software, both as a user and as a developer."
    author: "Kevin Krammer"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Hello,\n\nas it's now impossible to link the actual articles, I will have to rely on my memory, and my perception, both of them limited in quality.\n\nWhile I understood the good and bad news immediately (but not why this was not the initial plan, how else was kdesktop supposed to be replaced?), the topic appeared to me as semi-trolling itself. I believe it said something like \"desktop icons are no more\" and later \"I removed...\" and pardon me, that was not helping people to understand.\n\nI sincerely believe that developers must keep their users in mind in communication and try to avoid such pitfalls. If you try catching the eye with provocative news lines (it's my assumption that was done) may just as well toggle the brains off to some people (also my assumption reading the comments back then).\n\nAnd you know, about that friendship part, friends help friends. I think you need to educate some of your user friends to do the explaining part. I am entirely sure people will defend Aaron on his behalf, if they only know what is acceptable and what not, and where to point to for wrong expectations, wrong facts, etc.\n\nMore I less, why do developers have to defend themselves once they managed to get all the facts out? Why can't users explain things to users too? So far none of the KDE developers invited and encouraged that.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-26
    body: "It is _not_ necessary to silence questions. It _is_ necessary to silence those who try to tell the developers what to do (the developers aren't being paid) or who's statements boil down to the rather useless \"THIS SUCKS!\" (that's just your opinion, and probably contrary to the whole design).\n"
    author: "Riddle"
  - subject: "Re: finally.."
    date: 2008-06-26
    body: "Before you send a load of vitriol my way, I have never, not ever said anything like that to any of the developers or anyone on the Dot. I was asking a simple question and lo and behold, my comments got removed. It was honest inquiry; if you load up Aaron's blog you get a message saying you can't read it unless invited--no explanation, no nothing! What is one to think, especially if they're not subscribed to mailing lists or read the Planet religiously?\n\nI'm being 100% serious here, point out where I was saying \"Aaron sucks! Plasma sucks! You're doing it wrong!\" and I will apologize profusely."
    author: "Joseph Blough"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "As I understand Aaron closed his blog to the world because he finally ran out of energy to constantly deal with the unconstructive nej-sayers and poisonous jerks, that tend to attack everything that's about vision and enthusiasm.\n\nI don't really care if you were part of this problem, but seeing your reaction I guess you're feeling kind of guilty. And you seem to assume a conspiracy against you personally just everywhere, as you insinuate that Aaron closed his blog to exclude you from information or something.\n\n"
    author: "Philipp"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "Hello,\n\nI cherish being anonymous here, but I think it's obvious that Aaron thinks (correctly so I guess) that people will probably be more gentle when they are not as anonymous.\n\nAaron: You probably should re-read that \"desktop icons removed\" blog and consider how it probably was semi-trolling.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "not only was it probably not trolling, it absolutely wasn't trolling. i know because, well, i wrote it and as such i know what i was thinking at the time. if you're curious, here's what i was thinking:\n\n\"man, the icon support in 4.0.x isn't that great. people really want that stuff to be better. thank goodness we have something even better now. this is going to rock!\"\n\nwhen i started writing i was thinking:\n\n\"heh. first i'll note how we totally gutted the 4.0 icon support. i'm sure some will enjoy seeing that one roasted and toasted ... and then i'll show what we've replaced it with! people who hated the 4.0.x icons are going to be so much happier with this. first they'll think, 'what will i do with my icons now?' and then they'll see the screen shot, read what the new folderview can do and think, 'woah, that's really neat.'\"\n\ni was giddy with getting to play with one more piece of the puzzle that had danced in my head for years now. it's pretty intoxicating. a really wonderful experience.\n\nas a result, i was completely over optimistic about people's patience to read an entire article before losing it, about people's ability to enjoy some playfulness and for others to keep those people in check who didn't get it.\n\napparently, there isn't much room granted for an error in judging the self-selected audience who read my blog. so be it.\n\nbut it wasn't trolling. \n\nwhile we're sharing advice, though: the \"accept the blame i envision for you in my head\" game is really not a healthy one."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Hello Aaron,\n\nwhat strikes me, is that indeed, telling your thoughts would have been much better than the article I read. Your playful approach is normally well appreciated, but entirely didn't come across in this case.\n\nWhat didn't come across, is what a _major_ breakthrough that is, in the eyes of kdesktop users, in the eye of design beauty, in the eye of being able to throw out the 4.0 implementation of icons, in the eye of having a clear path to catch up with kdesktop.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "Aaron, as probably thousands others, I really liked your post. Not only that one, but pretty much all others as well - and not only because of the content, but also because of your blogging style. Especially the \"no more icons in 4.1\" post really was an amazing read. It is really a pity that there are so many people out there who are unable to read, unable to get a slight bit of joyful irony, unable to post actual comments instead of useless trolling, i could go on but you get the point.\n\nPlease, reopen your blog, keep blogging as you did before, keep ranting. Your blog was my no. 1 read in my news reader, always. Oh, and I'm just a KDE user, not a developer."
    author: "Daniel Peterson"
  - subject: "Re: finally.."
    date: 2008-07-01
    body: "Seconded. I would love it if Aaron opened up the blog again (comments could be off, to avoid the trolls)."
    author: "AIK"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "You *did* read the note on the comment page asking you to stay ontopic?\n\nWhy not just send Aaron an email? The commit digest has nothing to do with that."
    author: "sebas"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "If the comment was deleted because it was off topic, I must say things didn't work out that well did they... :|"
    author: "ad"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "it wasn't because it was off-topic (though it was that, too). i had the misfortune of seeing the comment before it was deleted; in it i was, again, personally likened to a dictatorship from recent history. *sigh*\n\ngodwin's law seems to have stepped in at that point."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "But you are not as bad as Bush.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "I, for my part, am sorry for the bad joke, didn't expected it hurt your feelings. The joke was so ABSURD I tought no one would take it seriously, but I was mistaken.\nAnd I do not care you told \"fsck\" myself :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: finally.."
    date: 2008-07-01
    body: "Hitler, Tom Green... :D  I guess it comes with being popular. But don't let it get over your head just yet. You'll know you're getting there when people start coming up with cartoons and caricatures instead of just writing it in message boards :)"
    author: "ad"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "I was never blaming you personally. I was stating that \"Aaron sucks! Plasma sucks! You're doing it wrong!\" types of posts are what need to be moderated.\n"
    author: "Riddle"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "Bullshit! It is not necessary to _silence_ anyone at all! Removing comments is a bad thing. On the other hand, having a message board full of crap is also pretty bad. There has to be some balance. \n\nA moderation system in which scores below a (configurable) threshold are not visible by default would do the trick just fine. It would also be good to have a login system, non anonymous messages with initial score above default threshold, and the others below. \n\nFurthermore accounts given to developers could be set so that their initial score is higher than regular accounts. And also so that it never goes below the default threshold or even below its initial score. \n\n"
    author: "ad"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "The mandate to delete is temporary - we're working on replacing the dot software to permit proper moderation.  In the meantime, we either delete or we don't."
    author: "Troy Unrau"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "OK then, that's good to know. Well guess my answer to your other post, which I just submitted, just became obsolete. I would delete it myself if I could ;)"
    author: "ad"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "The slashdot code is open, isn't it?"
    author: "T. J. Brumfield"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "It'll probably be drupal that replaces it - right now our biggest issue is migration of data as the dot was pretty heavily customized when it was first created."
    author: "Troy Unrau"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "Those suggestions make sense. The problem is that you tell us what to do, rather than offering help doing it or just plain putting work into it. That's what we call \"armchair developers\", and this whole problem is partly a consequence of this attitude you're exposing. Get involved, earn your privilege to tell others what to do.\n\nBefore you even consider that, please get rid of your abusive way of diminishing other people's comment in public. Another step in the right direction would be to stand behind your words and not post anonymously."
    author: "sebas"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "\"The problem is that you tell us what to do\"\n\nIMHO this would be a problem only if the poster (ad) had posted the same suggestions over and over again, without offering to help. Posting constructive suggestions *once* should always be allowed, and encouraged, no?\n\nGranted, ad did start his post with the word \"Bullshit\", but then went on to be constructive. That's my impression. Let's all try to read each other's posts favorably!"
    author: "Martin"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "> Posting constructive suggestions *once* should always be allowed, and encouraged, no?\n\nThis makes sense, yes.  Just be very sure that once means one time total, not one time per user.   With a million users using something that a thousand developers created this little distinction makes a lot of sense."
    author: "Thomas Zander"
  - subject: "one suggestion ever in history of mankind"
    date: 2008-06-29
    body: "> Just be very sure that once means one time total, not one time per user\n\nI disagree.  this will never happen and you know and understand why -- we are not a hive mind where everyone automatically knows what has been thought and said so it is not accidentally said again. \n\nI am more than happy to have many people suggest the same thing over and over.  that is great feedback -- here is something that is so important, enough people feel they wish to speak out about it.  \n\nbravo for speaking out.\n\nI am more than happy for one person to say the same thing over and over. and hope that s/he takes it further to (motivated internally or by kde dev encouragement) to detail their thoughts and how it could be implemented, its potential  benefit to KDE and the difficulties it would introduce to implement\n\nbravo for speaking out and being encouraged to become a contributor.\n\nand I, maybe one of the rare few, consider \"users\", by the mere act of using KDE, to be contributors.\n\nbravo to those using KDE.\n\nthose with a bee in their bonnet?   Sadly they haven't been effectively dealt with in this incident. no, I do not now what would have been effective.\n\ndesktop wise, saying \"this is how its going to be\" wasn't the best means of communicating a new way of thinking.\n\njumping down the throat of those who have vision, was overly rude and cowardly.\n\ntaking your ball and going home?  not the best response for someone in a public position.\n\ncalling (some) users poisonous? wait till those with an axe to grind get wind of this - you ain't seen nothing yet.\n\nwe need a time-out, everyone in their separate corners to do some reflection.  \n"
    author: "dave null"
  - subject: "Re: one suggestion ever in history of mankind"
    date: 2008-06-29
    body: "> taking your ball and going home?\n> not the best response for someone in a public position.\n\ntaking your ball and going home is when you don't like something and decide the best way to deal with it is not to deal with it.\n\nhowever when someone walks away from a long term relationship that is repeatedly and without resolution abusive? that's not even close to the same thing.\n\nthanks, though.\n\n> to detail their thoughts and how it could be implemented,\n> its potential benefit to KDE and the difficulties it would\n> introduce to implement\n\nand when that happens, it is indeed welcome. it isn't the issue here, though, because that isn't even remotely close to what was happening that got us here.\n\nand, because some people seem to be getting lost in this aspect of the conversation: it wasn't \"the users\". in that sense i feel for those making a case to cut \"the users\" some slack. see, it was a minority of people who are users, not all of them. but it was also others who are contributors who contributed significantly to the situation.\n\nso while some are busy pointing fingers at individuals, perhaps everyone would be better served by looking at the entire system that causes this to happen.. where \"what happened\" is burning through the good will of the most committed people going.\n\n> you ain't seen nothing yet.\n\nand you haven't been reading my inbox for the last few years, either. ;)\n\n> we need a time-out, everyone in their \n> separate corners to do some reflection. \n\nthat's exactly what i'm trying to do. not sure everyone has arrived at that realization yet, though."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-26
    body: "Unlike you, I'm not happy to see comments removed--especially ones that contained honest questioning. As a long time reader of Aaron's blog I found it curious that all of the sudden that you couldn't even *read* the thing--no explanation, no nothing. *Something* would have been better, even if it was just a lone sentence with comments locked out. Not everyone is privy to what goes on the behind the scenes; quite frankly I had no idea that things had gotten so bad until I started poking around the mailing lists.\n\nIt's sad, but the reason that this kind of thing happens is that the society we have been given is ego-syntonic--everybody only looks out for themselves, and to hell with the rest. And, to me, that's is unbelievably, utterly sad because it's the antithesis of everything that KDE and open source stands for.\n\nI guess what I'm trying to say here is that I hope that what's normal for cultures around the world doesn't creep into the open source arena. I feel bad for Aaron and everyone else who has to put up with a bunch of stuff that they really shouldn't have to. It makes me profoundly to sad to even think about it. :-("
    author: "A longtime KDE user"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "The mailing lists, his blog comments, the dot, even *fricking* bug reports. Being called dictator just once in a public forum can hurt. Having that happen to you day in and out? I honestly don't know how he took it.\n\nI think ervin's explanation on planet is sufficient. "
    author: "blauzahl"
  - subject: "That sucks..."
    date: 2008-06-27
    body: "I noticed it was down just now too.\n\nIt really is appalling the attitude that has been displayed towards someone who has put a hell of a lot of time an effort into KDE. Work on the UI is always controversial because it's the one thing that everyone has to get along with. But that same point is exactly the reason why this work is so important. Without this redesign KDE4 would be a back-end of exciting innovative technologies plastered over with a Win95 era interface.\n\nSome people seriously lack vision. That's not their fault of course but what is their fault is using beta / unrecommended software and expecting it to be finished. You don't move into a half built house and then complain it leaks when it rains. That's plain stupid. What is even more ridiculous is when these same people then complain that they're not being listened to adequately - as if idiots make good counsel.\n\nWe don't know what Plasma will become. We do know that the developers are putting more work into this than any number of armchair experts. We also know that the reality is improving an incredible rate. We also know that worst comes to the worst we realise it's going nowhere and we do something else. Shit happens, especially when you're not paying for it.\n\nAaron: Your hard work and innovation in KDE4 is really appreciated. Look forward to seeing what's still to come.\n\nThanks\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "Perhaps Aaron's move to shut most of his communication with us end-users down for a while will help to calm down the situation. I *really* hope it won't be forever. I, for my part, have always liked to read his blog, read about the vision for plasma etc. even if I didn't always understand all his plans. The whole \"dictator\" stuff is really ridiculous, and perhaps all this was necessary to make this point crystal clear for everyone - even I needed some time to fully understand: It's just completely unrealistic to think that Open Source software will work like this: \"Hello community - Do you want feature A, B, C? Vote yes/no. OK, 60% yes - feature in.\" This doesn't work too well in society and it doesn't work with open source development. So, why are so many people surprised about this fact? Please be honest: If Aaron would have asked right before starting the development of plasma: \"Do you want to have most of the KDE3 features removed for an uncertain amount of time - I want to rearrange things?\" I certainly would have voted \"No\". I'm honest enough to admit this. I'm an end-user, using KDE at home and at work and I for sure wouldn't have liked all the features going away with KDE 4.0. But it was probably necessary, as we can all see right now. KDE 4.0 really was a bit shocking for me. Today, I'm writing this on KDE 4.1 beta 2, and there are not many things I still miss and  - more important - I enjoy the new features a lot, having comic strips, folder views and what not on my desktop. Thanks Aaron for all this!\n\nSo, if non-community decisions are required - is Open Source just as \"bad\" (i.e. closed) as commercial software? I think it's quite important to think about how to answer this question as well, because we should be aware what we really like about Open Source to avoid such unnecessary discussions in the feature. IMHO the answer is: Open Source software is better, because if things go completely in the wrong direction or not moving forward at all, after some time things will auto-correct themselves, because developers are able to fork and/or to write alternative & compatible software. If Aaron wants say the cashew button in, but all other 1000 developers not, what would happen? Those developers wouldn't even need to write a new KDE, not even a new plasma. They would simply replace this single component - or write an alternative one. You can't do that by far as easily with commercial software. Obviously, this didn't happen with the cashew so it's just plain wrong to state that the direction the desktop is taken is determined by 1 person. An analogy to the democracy we live in: Not every single decision is voted upon by the public (Do you want us to raise taxes?). But if taxes are raised by 5000% they probably wouldn't last another election. If there is a king or dictator that wouldn't happen. You could really say a lot about our politicians, but not that it's a dictatorship. Looking at the cashew, even looking close - it just doesn't look like a 5000% tax raise, does it? ;-) So, perhaps everyone should keep his cool and instead enjoy all the new things in KDE 4.1 we get for free, because Aaron is working hard for months on end on them. Thanks again for this and to all the other developers. It's the fault of us very content end-users that we are not more vocal about what we like, leaving the playing field to all the naysayers.\n\n\n\n\n\n\n\n\n\n\n\n\n"
    author: "Michael"
  - subject: "Re: finally.."
    date: 2008-06-27
    body: "second..."
    author: "Thomas"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "+1 Amen my brother "
    author: "anon"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "+1 Amen my brother "
    author: "mimoune djouallah"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Hello,\n\nI totally don't mind that \"he who codes gets to decide\".\n\nWith Plasma, it was Aarons rightful decision as maintainer to not port kicker anymore. I think the mistake was not to start work on Plasma (or a new kicker) until very short before the release after having monopolized the buzz about it, which I think prevented others from doing it instead. Remember plasma.kde.org and then nothing at all, for very long time. Many months I have wondered when work will start.\n\nSo, if there was a plan to have KDE 4.0.0 with a meaningful Plasma, a mistake was made here. Of course, it's not like other things were less important. I am not decided, if the lack of Plasma working prevented others from developing more actively on KDE4 and its applications. I tend to think a damage was done to the project with this.\n\nBut the important mistakes were to pretend that Plasma 4.0.0 would be good, or that Plasma 4.1.0 will be, and to prevent others from working in the area while he was not. \n\nI could imagine that somebody may have ported kicker, if it had not been ruled out so early. And with kdesktop/kicker OR Plasma as a choice, no problem would have existed, other than that Plasma would be developed much slower. That would be a possible damage too, because Plasma would have to compete with the old code without an all or nothing approach. Now I can only use Plasma if I like the new window manager better.\n\nI leave it up to Aaron to decide this. I can only make guesses, and I have to assume (likely correctly so) that he and others know KDE better that me. But I think, the tension around KDE 4 was created artificially, and at the discretion of the KDE president. \n\nIf KDE had wanted, and some parts probably did, a much more graceful migration would have been possible for the core user experience. \n\nBut as I said in other postings, discussion about mistakes has become impossible. I doubt that Aaron believes that I admire him, and would take a signature gladly from him. I doubt that I can think he makes mistakes and still be good. And I doubt he considers he made mistakes. But that limits my admiration though. Probably because I have seen how harmful that can be in other contexts.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> I think the mistake was not to start work on Plasma (or a new kicker) until very short before the release\n\nIt is a common misunderstanding that software engineering results in immediate output of some kind. Good software enigneering will get to coding at a very late state, after (often several iterations of) brainstorming, designing, review and evalution.\n\nEspecially such a huge and novel undertaking as Plasma needs months of planning, concept prototypes, etc. before the first line of real code can be committed.\n\n> I am not decided, if the lack of Plasma working prevented others from developing more actively on KDE4 and its applications.\n\nIt is more the other way around. Plasma developers made a sacrifice in allowing a release with only the basic feature set finished, so that other project contributors wouldn't be delayed any longer.\n\nIt is a pity that the decision which gave us tons of awesome applications as early as January and will give us the first improvement iteration of these apps in July gets interpreted as being harmful to the project.\n\n> and to prevent others from working in the area\n\nNobody was ever prevented from working on features in Plasma or creating KDE4 based versions of a desktop handler and a panel.\n\n>  And with kdesktop/kicker OR Plasma as a choice, no problem would have existed, other than that Plasma would be developed much slower.\n\nPlasma development wouldn't have been impacted at all, its developers would still worked on it. In fact the absence of anyone wanting to try a port or new implementation of Kicker/KDesktop shows nicely that all people interested in this part of the KDE workspace felt that their time would be better sent on working on Plasma.\n\n> If KDE had wanted, and some parts probably did, a much more graceful migration would have been possible for the core user experience.\n\nWe all know that the alternative would have been delaying *all* of KDE4 for in the worst case another year, probably delaying integrative work (like getting Nepomuk support into applications) even more.\n\nFrom my point of view an iterative approach of continuous improvements is the lesser of the two evils."
    author: "Kevin Krammer"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> > I think the mistake was not to start work on Plasma (or a new kicker) until > > very short before the release\n \n> It is a common misunderstanding that software engineering results in immediate > output of some kind. Good software enigneering will get to coding at a very\n> late state, after (often several iterations of) brainstorming, designing,\n> review and evalution.\n \n> Especially such a huge and novel undertaking as Plasma needs months of \n> planning, concept prototypes, etc. before the first line of real code can be\n> committed.\n\nNot quite. It's certainly a folly to think that one can sit down and code things without designing. But it's also a folly to think that one can get a complex design right w/o trying to implement it.\n\n"
    author: "SadEagle"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> But it's also a folly to think that one can get a complex design right w/o trying to implement it.\n\nRight, this is what I meant with \"concept prototypes\".\n"
    author: "Kevin Krammer"
  - subject: "Plasma planning"
    date: 2008-06-29
    body: "\"It is a common misunderstanding that software engineering results in immediate output of some kind. Good software enigneering will get to coding at a very late state, after (often several iterations of) brainstorming, designing, review and evalution.\n\nEspecially such a huge and novel undertaking as Plasma needs months of planning, concept prototypes, etc. before the first line of real code can be committed.\"\n\nThis is what gets me.  The Plasma website goes up and promises a revolution in how people use their desktop.  Years go by in silence, and then Plasma is announced as a scripting framework for widgets.\n\nA good design starts by targeting the user's needs.  What does the software need to be able to do?  Then figure out how you intend to implement it, and then begin coding.\n\nWe have this flexible framework, which admittedly is nice to be flexible, but how much planning was their early on for this vision of Plasma?  You heard early on about the concepts for Nepomuk, Akondi, Solid, etc.  How come no one ever publicly talked about this grand vision for Plasma other than widgets, and even that seemed a little later in the overall development.\n\nOn kde-look.org people were submitting some ideas on how to radically approach the desktop.  Not once did I see developers respond positively or negatively on how these types of concepts might work into the KDE 4 scheme.  My complaint isn't that coding was late because of massive planning, but rather the impression I get (which may be flawed as someone outside the development process) is that Plasma didn't have as much of the planning as the other areas, yet Plasma is the most visible.\n\nI warned for months leading up to the KDE 4 release that there would be a lot of negative reactions.  I didn't say that to be mean, but rather to temper developer expectations.  I read on the Planet that several developers seem offended by user reactions.  I wrote last year that of all the exciting KDE 4 technologies I was excited about, most of them would be overlooked and forgotten.  People would (and did) focus on Plasma.  It was the most hyped, and most visible.  It directly affects the users the most.\n\nThen I saw developers late in the game discuss the concept of icons around icons, an experiment which I guess didn't work.  At least they tried, which is fine, but that concept seemed to appear after plasma was reality in code.  Shouldn't that type of brainstorming largely have occured a few years back when Plasma was first a concept?  The same with folderview now.\n\nIf Plasma had been designed in concept years back more solidly, then many of the usability details could have been hashed out.  Documentation could have been laid out.  Users would have a better idea of the end goal, and expectations might be better structured around working towards that end.\n\nIf there was a grand scheme for the Plasma desktop years back, I'm really curious to hear what it was.  I believe everyone is."
    author: "T. J. Brumfield"
  - subject: "Re: Plasma planning"
    date: 2008-06-29
    body: "> Then I saw developers late in the game discuss the concept of icons around\n> icons, an experiment which I guess didn't work. At least they tried, which is\n> fine, but that concept seemed to appear after plasma was reality in code.\n> Shouldn't that type of brainstorming largely have occured a few years back\n> when Plasma was first a concept?\n\nthis was first prototyped at the Appeal meeting some 3 years ago in Berlin. you can ask the other attendees, and maybe even be able to find the mockup somewhere online still somewhere? anyways, that was where i first pitched on a small whiteboard one evening.\n\nthe trash uses to show its menu arrow on the desktop, dolphin uses the concept for selection ... and i'd be very surprised if we don't see it pop up in more places.\n\nso .. yes, you are right that brainstorming should have happened a few years back. in fact, it did.\n\nthe rest of your comment is similarly accurate. i hope you excuse me not refuting it point by point."
    author: "Aaron Seigo"
  - subject: "Re: Plasma planning"
    date: 2008-06-30
    body: "My very small suggestion:\nnext time, instead of trying to surprise users with features, be more open (again, to users) about features and put them also in the brainstorm.\n\nI know developers do not want users placing ideas before they can implemente theirs first, but I believe much of KDE4 problems could be avoided if users had a small participation in the discussion.\n\nProbably 4.0 would be called 4.0-preview-edition, we would not launch 4.0 before the panel/desktop was not at the 3.X level of features, we would have no top-right plasma icon, etc. \nSome would loose (having their ideas removed or hidden), but overall, I think everyone would be happier.\n\nSo, instead of pointing guilties, and keep complying here is my question:\n- how can I help?\n\nI think, at least I can create some kind of KDE IDEAS site, like the ones from Dell and Ubuntu, in PHP+SQL, if KDE team os OK with it - even if it is not official. It would give voice to USERS that I think, is the root of all recent discussion problems (not being heard).\n\nThanks."
    author: "Iuri Fiedoruk"
  - subject: "Re: Plasma planning"
    date: 2008-06-30
    body: "I think you missed the forest for the trees with my comment.\n\nI suggested that it appears from the outside (which may be a faulty perception) that overall there was not a grand design for the finished Plasma desktop from the beginning.  The fact that someone mentioned the icon-around-icon thing three years ago does not clarify if such an overall design for Plasma exists or not.\n\n\"the rest of your comment is similarly accurate.\"\n\nI wasn't aware questions and opinions were easily quantified as being accurate."
    author: "T. J. Brumfield"
  - subject: "Re: Plasma planning"
    date: 2008-06-30
    body: "> On kde-look.org people were submitting some ideas on how to radically\n> approach the desktop. Not once did I see developers respond positively\n> or negatively on how these types of concepts might work into the KDE 4\n> scheme.\n\nyay. I don't know about you but I've a list of bookmarks of my personal top10-concepts aka KDE4 brainstorm ideas. The funny thing is, if you talk with others everyone has his own top candidates and some of them are very different implementation and usage wise. Well, the good thing is, that you don't need to find a consense there and pick only one of them while others are ignored. Per design it's easy to replace the application-launcher with your favorite one - may it be the classic KDE3-like menu, Lancelot, Raptor or whatever else - as well as the panel, the desktop, etc., etc.\nThat is where things start to rock for me since I am allowed to choose my favorite proposal, implement and use it just like I am able to choose my favorite icon-theme, splash screen, color theme, etc. and nobody does to agree there with my personal taste. That's the freedom and flexibility those architecture does provide to me :-)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Plasma planning"
    date: 2008-06-30
    body: "> This is what gets me. The Plasma website goes up and promises a revolution in how people use their desktop. Years go by in silence, and then Plasma is announced as a scripting framework for widgets.\n\nCorrect me if I'm wrong, but doesn't \"a scripting framework for widgets\" provide most (or all) of what the Plasma home page describes? For example:\n> Plasma will deliver new looks for most traditional panel elements, an extensions system designed for beautiful results and graphic treatments that will firmly cement it as the desktop by which others are measured.\nOkay, I'll give a few examples of how Plasma does this:\n1) SVG theming: Obviously, this allows for people to make things look \"cool\"\n2) an extension system: not only can people create widgets for the panel and desktop, but they can _also_ make new ways to present those widgets (new containments). This allows for a lot of completely different desktop designs without adding them to the Plasma core.\n> In Plasma, the desktop is not a file manager view. Icons will still be able to be placed there, just as you can put application launchers and links to files and locations you are working with, but that will not be the grand purpose of Plasma's desktop. Instead, the desktop layer will be a place to interact with your system and a place for the system to interact with you. This means providing an exensible user interface. It shouldn't be busy, but it should be useful.\nAll of this is true today, including the basic fact: the desktop is not a folder view. The folder view applet is. Why is anyone complaining about what is announced long before.\n\nAnd more importantly, Plasma does allow people to deviate from the designs Plasma comes stock with. The widgets, and the widget's presentational form, are hot-swappable.\n"
    author: "Riddle"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Hello Kevin,\n\nthe plan for Plasma implementation didn't reflect that it will take time. There was no way that Plasma could be finished before release, and every month that went by without code, added to it. In fact, there were months without any public dicussion of Plasma, and without any code being written.\n\nAnd with that release. My memory tells me that KDE 4.0.0 got delayed from October to Dezember, because of Plasma not being ready. The fact that at the time of the last release candidate, Aaron claimed that nobody could judge the final Plasma of 4.0.0 based on that RC, is still present in my memory. At the time, everybody should have been aware that another delay was due, except that it wasn't possible anymore.\n\nAs for the no kicker/kdesktop port, I am not entirely convinced that there was no interest, given that it had been made clear so early, that it's supposed to be dead, nobody dared to speak up. Same as with Plasma on Windows or MacOS X, I am 100% sure, people (will) want it, but have been discouraged.\n\nAnd please tell me, how is kicker vs. Plasma any issue for any other KDE tech in apps? And see above, Plasma caused delays. That's my major critique, that Plasma was put on a critical path for 4.0.0 release, when there was no need to do that. Plasma perfectly could have been ready with 4.1 or 4.2 release, while the rest had progressed unhindered.\n\nAs I see it, the quality of kdelibs 4.0.0 was amazingly good. It's a pity that adoption was not positively enforced with a desktop shell concept that was proven and universally accepted, and implemented. \n\nHow much time would it have taken to port kdesktop and kicker? Having read Aarons blog, I understand that the code was not worth it. But then I (and others) would have been KDE 4 users from the start. \n\nI see a chance that this option of a port is only a fantasy I had, but I also see a chance, that it was needlessly discouraged.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "\"My memory tells me that KDE 4.0.0 got delayed from October to Dezember, because of Plasma not being ready\"\n\nThere were a whole bunch of things that were \"not ready\" in October - KConfig and some other important subsystem (mime-types, IIRC) being just two examples.  Singling out Plasma to be the whipping boy is unfair."
    author: "Anon"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "Hello,\n\nit's not about fairness, is it? But it's rather absurd the say that Plasma \"allowed an release\", isn't it? \n\nMore than that, it prevented release for some time and then it prevented release adoption at any larger scale.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "\"it's not about fairness, is it?\"\n\nI don't want to get in to a discussion about fairness, so I'll put it much more factually: If Plasma had been 100% working, 4.0.0 would still have been delayed.  Core elements like the aforementioned KConfig weren't ready and were blockers.\n\n\"But it's rather absurd the say that Plasma \"allowed an release\", isn't it?\"\n\nSorry, I don't understand what you mean by this, and cannot find the quote you are referring to :)"
    author: "Anon"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> As for the no kicker/kdesktop port, I am not entirely convinced that there was > no interest, given that it had been made clear so early, that it's supposed to > be dead, nobody dared to speak up. Same as with Plasma on Windows or MacOS X, > I am 100% sure, people (will) want it, but have been discouraged.\n\nIt's actually not true that they weren't ported. Kicker was actually somewhat functional and working quite close to 4.0 release. It was however, also very buggy, crashy, etc.; since it made no sense to spend time on fixing something \nthat would be replaced. \n\nKDesktop is a trickier matter, however, since it shared quite some code with konqueror's iconviews, which were (sensibly) removed in favor of sharing things with Dolphin.\n"
    author: "SadEagle"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "Hello,\n\na transition plan would have made sense, but implementing it, would have been no fun, so I can hardly blame anybody for not doing it. I do find it bad that it was actively discouraged from the start. The vaporware (for a long time) Plasma took away all its mindshare at least, and public announcements were made, that sort of stopped the (natural) idea in its bed.\n\nAs it stands now, KDE 4 can't be the default desktop for another while, since standard distributions just can't migrate their users to the old icons, but won't have the new icons before yet another 6 months.\n\nIf you (for a moment in your mind) remove Plasma from the mix, it would be hard to find a reason why KDE 4 adoption shouldn't be as fast as it was for KDE 3. That was as I recall it, immediate. Big distributions were fighting to be the first ones, you won't see that yet.\n\nI'd like to see an open discussion that results in an agreement that makes statements like \"kicker shall not be ported to KDE 4\", or newer \"Plasma shall not be ported to other platforms\", unacceptable, because the thing that hurts our community. Both of these are examples where politics gets in the way of what people want to have/do.\n\nIf there really was no way to have a usable KDE 4.0.0 release, fine. If that kind of statement (and decision process) was part of it, it must be done away with. \n\nThat's the worthwhile lesson of KDE 4 history so far. I can perfectly accept to wait for Plasma to finish, and KDE 4 to be finally adopted, even if later that needed to be. It would just be nice to know that things are not prevented at the discretion of (even prominent) nay-sayers.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "> since standard distributions just can't migrate their users to the old icons, but won't have the new icons before yet another 6 months.\n\nI don't think many people would have a problem with using the folderview in windowed mode, especially since a distribtor could use a Plasma theme with very thin borders.\n\n> that makes statements like \"kicker shall not be ported to KDE 4\", or newer \"Plasma shall not be ported to other platforms\", unacceptable\n\nNobody would ever make such statements in the first place, since this is a Free Software project.\nSome developers might explicitly state that *they* are not going to work on something, which doesn't mean anybody else can.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: finally.."
    date: 2008-07-01
    body: "Hello,\n\nabout the statements: Were a Aaron's persons blog still public, I am sure I would try to cite it. Your words were exactly my thoughts (and comment) then.\n\nAnd you know, if the kicker or later Plasma maintainer says these things, they do have a different weight. You would have to port Plasma to Windows and get into tension with Aaron, you probably has some almost consensus behind him, that people just don't want that politically. \n\nAnd about that folderview in windowed mode, hopefully that will work. I have no basis for my assumption that mass migration will only happen once people can get the \"traditional\" KDE desktop shell just with more options.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-07-01
    body: "> about the statements: Were a Aaron's persons blog still public, I am sure I would try to cite it. Your words were exactly my thoughts (and comment) then.\n\nYou have me confused now.\nSince we both agree that Aaron never made such over generalized statements like \"shall not\" and you even commented on them back then, why are you looking for a process that avoids them, if they do not come up with the current one?\n\n> And you know, if the kicker or later Plasma maintainer says these things, they do have a different weight.\n\nSure, if a subsystem maintainer says he won't do something, other developers usually understand this as \"it would mean tons of unsatisfactory work\".\nOf course of others prefer it over the maintainer's own solution, they will still work on it, see the various projects for alternative launchers (traditional K-Menu, Lancelot, Raptor)\n\nKDE's history has quite some occasions where alternatives got developed with full support of KDE's infrastructure and eventually became the solution most people use (e.g. Amarok).\n\n> You would have to port Plasma to Windows and get into tension with Aaron\n\nActually parts of Plasma, e.g. libplasma, are available for Windows as well, since applications like Amarok are using it.\nRegarding tensions, I don't think so. As long as support for new operating systems does not harm the functionality and stability of the main target (Free Software OS), patches will be accepted.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: finally.."
    date: 2008-07-01
    body: "Hello,\n\nit's simple to lift the confusion: I think Aaron said \"Plasma shall not be ported to Windows or MacOS X\" in some blog.\n\nI remember a time when Aaron e.g. said \"Free Software/KDE should not be ported to Windows, it will hurt\". And later he said the above (or similar in meaning). He hasn't commented on it lately I think, but I think he felt at the time that Plasma should be a killer app and therefore for Linux only, and it would be his decision.\n\nNote: I googled up \"Plasma will not be ported to Windows\" from his blog, but I suppose, Google is going to quickly forget about it. The cache didn't work already. Oh my god, I likely violated the DMCA. ;-) Anyway, I concur, this hardly any proof of anything. That's only a memory now.\n\nI am glad anyway, that if such a thing has been said or not, it's agreed that it's unacceptable. That's all that matters.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "Hi Kay,\n\n> My memory tells me that KDE 4.0.0 got delayed from October to Dezember, because of Plasma not being ready.\n\nThere were more areas still unfinished at that time because several people had less time available than they had originally anticipated.\nOther, e.g. KDE PIM, had the advantage of not being as essential, so it could even skip the 4.0 release.\n\n> That's my major critique, that Plasma was put on a critical path for 4.0.0 release, when there was no need to do that.\n\nWell, of course we could have just released platform and apps without workspace, but since we see how the basic implementation triggered negative responses, can you imagine what having no workspace (desktop, panel, etc) at all would have triggered?\n\nI am not sure a \"KDE apps 4.0\" release would have been understood any better.\n\n>  It's a pity that adoption was not positively enforced with a desktop shell concept that was proven and universally accepted, and implemented. \n\nI am not so sure that making a release which actually depends on certain KDE3 components would have been accepted either.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: finally.."
    date: 2008-06-28
    body: "> even if it was just a lone sentence with comments locked out\n\nas there was no safe haven for a public discussion of these matters, i decided to quite simply withdraw from the public. such a parting message did not seem to be useful in resolving any of the pertinent issues.\n\nnow while it was not my immediate intention to inspire change (i just wanted to be rid of pain and waste of energy, tbh; anything beyond that was mere hoping), it seems that this has started some wheels turning that had been quite stuck in the past.\n\nif these efforts begin to work out, i will consider re-emerging into public life with regards to the KDE project.\n\nmore importantly to KDE than just little old me, if these efforts work out we will retain far more contributors in the future than we have the in past.\n\nwe'll see."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Believe me, I was an am still really unsure to post this comment, especially since you gave me a friendly and helpful answer below. \n\nBut: I have strong doubts if this strategy will work out.\n\nI'm afraid that a non-vanishing part of the users will interpret this as \"Educating them by withdrawal of love\" which, as expierience shows, usually leads to escalation instead of the intended de-escalation. I'm no dev, just a user, but IMHO i'm not sure how much escalation KDE, you, the devs and the users can take before something irreparable breaks. I'm a KDE user since the days before 1.0, but this is the first time I see a situation like this: A real crisis.\n\nSeems to me a bit like KDE is going through the difficult time of puberty and someone has to keep one's nerve. Quit talking seems not an appropriate solution to me.\n\nAnyway, thanks for all the work! Especially yours."
    author: "furanku"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> a non-vanishing part of the users will interpret this\n\nwhile absolutely true, there's nothing i can do about that. i'm not doing this to make a point to other people, however.\n\n> usually leads to escalation instead of the intended de-escalation\n\ni *hope* it results in community repairs. but if it doesn't, that's not something that really enters into it for me anymore.\n\n> can take before something irreparable breaks.\n\nthis statement is months if not years too late. i don't know if you realize just how many people we've lost to the same crap that finally got to me. seen Matthias Ettrich around lately?\n\n> someone has to keep one's nerve.\n\ni'd suggest those remaining as active members in the public community here start with themselves then.\n\n> Quit talking seems not an appropriate solution to me.\n\ni'm still talking, but as an individual.\n\ni'm no longer taking part in any public communication or internal organization  on behalf of the project at this point beyond what i feel compelled to do within plasma and to fulfill my responsibilities on the KDE e.V. board.\n\nif you are looking for people to talk to, though, i'd suggest people like Kay above. they seem to be full of ideas and opinions."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "Probably not helpful at all: I've travelled a lot to attend a Kalle Dalheimer talk about pre 1.0, where he claimed that speech recognition would be a high priority issue for 1.0.\n\nI was not disappointed when that did not happen. Currently I'm switching from kde4daily to native builds, and I'm still hopeful that this all will come to a good end.\n\nBut to be honest: IMHO some stupid, uneccessary things have been done, which diminish my faith in that.\n\nBut maybe I just grew old. "
    author: "furanku"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "all i'd ask is that when we actually do make true on things that you're still around and that you don't forget.\n\nwe're all getting older.\n\nit doesn't mean we have to forget how to dream\nit means we learn how to take responsibility."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Aaron\n\nYou seem to have (inadvertently?) timed it well. Discussions about moderation/guidelines in the past have come back to notions of free speech and avoiding silencing the vocal minority. But the dot (and KDE community) exists for the advancement and improvement of KDE and not as a personal soapbox for the misinformed. Your stepping back has shown what's at stake if we don't fix it. Thanks.\n\nOut of interest don't you find the whole thing a bit bizarre? If your code had an impact on the Middle East peace process I could understand it but the issues are a \"Who's Who?\" of \"Who Cares?\": \"double-line taskbar buttons\", \"cashews\" and \"icons on the desktop\".\n\nI admire your patience because frankly it's a load of toss. Developers (yourself included) have shown themselves to be willing to listen to criticism when presented in an intelligible useful way. I vote for holding the critical minority to the same standards. I doubt they can do it.\n\nQ: Having contributors taking time away from what they enjoy doing to answer questions/feedback/etc. seems like a bad use of time. Are there any KDE teams that act as a go between to get information from developers and present it in understandable way (e.g. for the dot)? I'm sure there are plenty of people out there (I'm one) who enjoys hearing and discussing this stuff enough to do this. As a developer would a gaggle of community monkeys to get your point across be a useful resource - or is writing just down time?\n\nAnyway enough of that.\n\nThanks for all your work so far and looking forward to seeing how much faster you code without distractions (you thought you'd get a rest?! ;)"
    author: "Martin Fitzpatrick"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "> Are there any KDE teams that act as a go between to get information from\n> developers and present it in understandable way\n\nnot currently. but as i made my exit, i have thrown out this precise idea in various appropriate places. people seem to be actually moving on it, which is promising.\n\nthis is something that active and concerned advanced users can do as part of their community involvement that would be immensely useful.\n\ni believe some of them are having a meeting on irc tomorrow or today about this topic. i hope they come up with something good to share with others and start rolling with sooner rather than later.\n"
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Thanks for the heads up, I found it in the nick of time ;)\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "As of late I have been critical of Plasma and KDE 4.  It just means I disagree with things.  When I see people make strong personal attacks against you (calling you Hitler) I can certainly understand why it is a tipping point.\n\nI'm just one guy with an opinion (though I imagine there are many who don't speak up who happen to agree with me, as there are plenty of people who disagree with me) but I want to re-echo my last post to you.\n\nI think a lot of the negativity between users and developers comes from a disparity in perception.  You can disappear for a while and focus on code, which may be a win because a fully realized KDE 4 might occur sooner because of that.  Or you can spend some time on PR and documentation.  You suggested I could help with the Wiki.  Frankly, finding a means in which I can contribute certainly appeals to me.  If I had cash, I'd donate to the eV because I do love me some KDE.  I don't presume that I know more than most on KDE since I don't delve into the code.  I don't know what future plans are.  How can I contribute to the wiki when I wouldn't know what to add?  To an extent, those who know the future plans of KDE are best suited to lay them out.\n\nSome wiki entries could keep you from repeating answers.  Point to the wiki.\n\nDifferences between KDE 3 and 4:\n\nThis is intentionally different.\nThis feature will be added back hopefully around release X.\nThis is something we haven't gotten around to, and I'm not sure when we will.\n\nFeature Y irks me:\n\nThis feature is half implemented, and this is a concept drawing/mock-up of what we're going for in the end.\n\nEtc, etc. etc.\n\nIf there are apps that really need documenting, or areas of the wiki that basically anyone can help out on, then point in a direction and I'll jump in."
    author: "T. J. Brumfield"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "i really don't care about any disparity in perception at this point. last month? sure. today? not really.\n\nyour posts were a few drops in that ocean of hurt that led to me saying, \"yeah, i've had enough.\" if you decide to take something useful out of that is up to you, just as it is up to me to take something useful out of it.\n\nit's cool you're thinking of helping out. i hope you do. KDE can use it.\n\n> Some wiki entries could keep you from repeating answers.\n\nhttp://techbase.kde.org/Projects/Plasma/FAQ\n\nif you go to google and type \"plasma faq\" it's about four links down. if you type \"kde plasma faq\" it's the first thing that comes up. it's really not hard to find this stuff."
    author: "Aaron Seigo"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "I also add that contributions to said FAQ are *very* welcome. I plan on adding things (including the infamous question \"can I move the applets on the panel\"?) but since my time is severely limited nowadays, more input would be welcome.\n\nIt's easy, just use Plasma and report common questions and answers to use patterns. Also, spread the word. The more the FAQ is known, the more people will not ask the same questions over and over."
    author: "Luca Beltrame"
  - subject: "Re: finally.."
    date: 2008-06-29
    body: "Thanks for your efforts Luca, I'll take a look later.\n\nA good way to push the FAQs and make sure they get read is to link them in any posts where we're answering a \"already answered\" question (and if it isn't answered add it ;)."
    author: "Martin Fitzpatrick"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "http://techbase.kde.org/Schedules/Is_KDE_4.1_for_you%3F\n\nGet to it TJ."
    author: "Steve"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "hei, just wanted to say that there are alot of people out there supporting you, even if most of the time you hear them less than the others. But i think since you stopped talking, the ones supporting you  (and the whole kde project) seem to have made a step forward and are support you in a more vocal way.\n\nAnyway, have fun coding (that's what it's all about anyway) :-) and i'm looking forward reading you blog posts again (the technical ones, i hope you won't have to write too many of the others anymore)"
    author: "Beat Wolf"
  - subject: "Re: finally.."
    date: 2008-06-30
    body: "One of the legends about David Packard when he was running HP was that he went around regularly locking the conference rooms. It seems he felt people were spending too much time talking and not enough time doing real engineering work. Some people were upset about not being able to have meetings, but other people (those doing the actual work) prospered.\n\nKDE developers, enjoy a bit more time for coding!\n"
    author: "T"
  - subject: "Any progress on the Mac OS Menu bar?"
    date: 2008-06-28
    body: "Sorry if I ask again for the Mac OS like menu bar. When I asked last for it I got the answer that I could enable it with putting\n\n[KDE]\nmacStyle=true\n\ninto kdeglobals, but it's still buggy. With my current up to date kde4daily image I now don't get any menu bar at all with this setting. When I asked this for the first time, Lubos Lunak generously offered to work on this \"somewhen post-4.0\".\n\nWith respect for the momentary nervous situation: Please see this as a uncomplaining feature request/request for information if this is work in progress.\n"
    author: "furanku"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-06-28
    body: "there's a menubar plasmoid in playground; i think it's reasonable to expect it to be fully ready for 4.2. with folderview also a subject of their work, the developer couldn't move the menubar plasmoid also to completion. but it's there, mostly works well, needs some polishing and can then move on into kdebase or kdeplasmoids.\n\nhth."
    author: "Aaron Seigo"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-06-28
    body: "Fine, thanks, good to hear!\n\nIs it in a state that user based testing and bug reports are already welcome or should I wait after the 4.1 release?"
    author: "furanku"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-06-28
    body: "Things in playground are always open to testing, but you have to compile it yourself and expect bugs, crashes and eating of the children. :)"
    author: "Thomas Zander"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-06-28
    body: "OK, then I'll take this as a reason to switch (back) from precompiled kde4daily binaries in a virtual machine to a native SVN KDE installation.\n\nThanks for the answer. "
    author: "furanku"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-06-29
    body: "KDE now fully integrates with EatBabies.com"
    author: "T. J. Brumfield"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-07-01
    body: "So I guess there is a KDE recipe viewer, then... (yes, I visited eatbabies.com)"
    author: "Riddle"
  - subject: "Re: Any progress on the Mac OS Menu bar?"
    date: 2008-07-02
    body: "OK, statistics so far:\n\nOn the plus side:\n\n 1 installed and working KDE SVN version, including\n 1 compiled and installed menubar plasmoid and\n 0 eaten children\n\nOn the minus side:\n\n 1 not working menubar plasmoid\n\nTried to enable it in the config files, got no menubar at all, and through \"Add Widgets\", got a strange grey box with a number in it and a an empty plasmoid with wrong sized handles around it.\n\nBut I'll guess developers will have better things to do in the short time to the 4.1 release than to help me to get things in trunk/playground working and this isn't a support board at all. I just report it for the sake of completeness. So I'll leave that for now and try it later again.\n\nThanks to both of you for your help again!"
    author: "furanku"
  - subject: "Incorrect title?"
    date: 2008-06-30
    body: "Shouldn't the title of the post be: KDE Commit-Digest for 25th June 2008?"
    author: "Kanwar"
  - subject: "Re: Incorrect title?"
    date: 2008-06-30
    body: "See the first comments on this. Danny has some trouble getting them out in time because it's just a tremendous amount of work to read all commit messages and compile a digest."
    author: "Stefan Majewsky"
  - subject: "Re: Incorrect title?"
    date: 2008-07-01
    body: "So, these changes are as per 25th May or 25th June?"
    author: "Kanwar"
  - subject: "Re: Incorrect title?"
    date: 2008-07-02
    body: "Like Stefan pointed out the first comment says: \"I am aware that i'm still a month behind on these Digests, and that my attempts to catch up last weekend didn't fully get the job done.\""
    author: "Peter Penz"
  - subject: "Enough is enough"
    date: 2008-07-01
    body: "I'm more of a lurker than a commenter, but some of the things here are so downright childish and so silly that it seems impossible not to laugh sardonically.\n\n@ The doomsdayers\n\nPlease get some perspective.  KDE is a huge project involving many people and many applications yet you seem to think that because one feature of one app isn't going to be implemented or one feature of one app is late that KDE is in a crisis?  Not only is that belittling to everyone else in the project but it's simply wrong.\n\nBesides, noone is listening to you anyway.  Noone will listen to insults or unconstructive criticism.  Here is an example of constructive criticism that people will listen to:\n\n\"I think that it would be nice to have a panel that is easy to resize.  The current panel takes up too much of my screen and I don't want to have to totally change the theme to get a different panel.\"  (As an aside please don't respond to this. It isn't a serious criticism, just an example)\n\nNow if the developers say that they would rather this not happen, then that's fair enough because, unless you're paying them, you have no valid say on what they do with their time and if you try to tell them then you're going to get ignored.  You can only ask very *very* nicely.  Don't press the issue.  Don't let this prevent you from making further useful constructive criticism on different topics.\n\nIf they say that they'll get around to it later (even if it's a year from now) then that's fair enough as well, because good things take time and priorities must be made.  Wait, and try not to ask every day because that's just annoying.\n\nIf you're lucky, they'll say that it will be in the next release, but that's their say.\n\nHere is a piece of unconstructive criticism:\n\n\"Plasma sucks.  We should go back to Kicker.  Aaron Siego is an idiot.  KDE is in a crisis.  KDE 4 isn't good enough to make the move over to.  People who disagree with me are idiots.\"\n\nNoone will listen to this.  There are no exceptions.  Anything you say when you say it like this will be disregarded.  If you are not polite, if you try to tell someone what to do, or if you insult someone, then you will get nowhere with them.  Keep your criticism constructive and people may listen to you.\n\n@ The People Fighting Back\n\nDon't do that.  Do unto others and all of that - are you really surprised people keep insulting you when you're insulting them back?\n\n@ Aaron Siego\n\n95% of everything is bunk.  Including what people say about you.  Sometimes people are going to react badly misinterpreting your intention or what you say ... that happens.  Some people won't listen to you, and some people will just be mean to you.  That doesn't say much about you, it just says a lot about them.\n\nAs I said before, I'm a lurker.  However I've read a lot and, since I've been a developer for over a decade now, though not of KDE, I understood all of it and the reasons behind it.  You're good at this.  Extremely good.  Keep it up what will result will be fantastic.  I can't thank you enough for what you have already done.\n\n(and, by the way, I *liked* reading your blog.  It was interesting and you have good ideas.  Is there some way people who want to listen to what you say can get invites to read it?)"
    author: "m"
  - subject: "Re: Enough is enough"
    date: 2008-07-02
    body: "Amen."
    author: "yman"
  - subject: "Re: Enough is enough"
    date: 2008-07-02
    body: "It seems it is much easier for people with some programming/developing knowledge to understand or see the idea behind plasma and what Aaron is heading for.\nIt's fine if you don't know much about programming at all but you want to voice your concerns, but some people are just overly rude and have no sense of decency at all. Still, we don't know their state of mind or their age. Can be it's just some 15year old, who is just in puberty and unhappy with himself. It would be such a pity, if this behaviour is succesful after all and gets committed contributors down.\n"
    author: "Thomas"
  - subject: "Re: Enough is enough"
    date: 2008-07-02
    body: "Personally, I think that what's happened is that people with lots of time on their hands and nothing better to do spent their time attacking plasma and Aaron (i.e., trolls). I found a post by Penguin Pete on what's going on with this dynamic and also why most of the kvetching over interfaces is just so much noise:\n\nhttp://penguinpetes.com/b2evo/index.php?title=interface_obsession_syndrome&more=1&c=1&tb=1&pb=1\n\nhttp://penguinpetes.com/b2evo/index.php?title=the_six_kinds_of_anti_foss_trolls&more=1&c=1&tb=1&pb=1\n\nDefinitely worth a read."
    author: "Anonymous"
  - subject: "Re: Enough is enough"
    date: 2008-07-02
    body: "Excellent links! A very good read. I was thinking of doing something similar with a KDE Troll FAQ, giving details of each type of Troll such as their belief system, ways to spot them, and countermeasures. But I think Penguin Pete has done a much better and funnier job that I could ever hope to do.\n\nI feel one type of troll that we get all the time on KDE Dot News, that Mr Penguin left out, is what I would call the 'Armchair Manager Windbag'. This type will write enormously lengthy posts about how the KDE developers did the wrong thing at the wrong time, which usually contain no actual content whatsoever. They are really bad news because the enormous initial post and the equally tedious responses totally drown out any useful discussion which is happening here. \n\nHow do you identify them? They seem to like initials and won't sign their posts as are 'Geoff' or 'Jane', but as something 'R B Steinkopf' or P.K.R. They also will refer to posts they have made in the first person, as though they are someone who they consider very important. For example, 'Two years ago I told Aaron that 16x16 icons were too big'.\n\nWhat effective counter measures are there against these bores? I genuinely don't know. Although they clearly have excellent language skills they never actually lift a finger to improve the project documentation, translations or anything else. So I think the best tactic is to needle them about how we couldn't care less what they think unless they contribute something useful to the project.\n\nI need to stop now, don't want to be considered an 'Armchair Windbag'..\n\n--Richard"
    author: "Richard Dale"
---
In <a href="http://commit-digest.org/issues/2008-05-25/">this week's KDE Commit-Digest</a>: <a href="http://edu.kde.org/marble/">Marble</a> gets "temperature" and "precipitation" maps, and a "stars" plugin. More work on "fuzzy searches" in <a href="http://www.digikam.org/">Digikam</a>. <a href="http://www.konqueror.org/">Konqueror</a> gets support for crash session recovery and session management. Runners can now be managed using a KPluginSelector-based dialog, and attention-blinking support in <a href="http://plasma.kde.org/">Plasma</a>. Various Plasma applets move around KDE SVN before the KDE 4.1 feature freeze takes effect, with <a href="http://webkit.opendarwin.org/">WebKit</a> applet support moving into kdebase. SVG stuff from WebKit starts to be integrated into <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. More optimisations in KHTML, with KJS/Frostbyte, a version using bytecode and other enhancements, moving back into kdelibs. Start of an implemention of the JavaScript scripting API for PDF documents in <a href="http://okular.org/">Okular</a>, based on KJS. Continued work on KJots integration into Kontact, and creating/editing links between entries in KJots. More work on theming in <a href="http://amarok.kde.org/">Amarok</a> 2. Various improvements in kvpnc. More configuration user interfaces in KNetworkManager. Enhancements in the <a href="http://ktorrent.org/">KTorrent</a> bandwidth scheduler plugin. Support for CUPS printing options in KDE printing dialogs. <a href="http://www.mailody.net/">Mailody</a> moves to kdereview. The "OnlineSync" plugin is merged into <a href="http://akregator.sourceforge.net/">Akregator</a>. Initial commit of a new MSWord-to-ODF filter for <a href="http://koffice.kde.org/kword/">KWord</a>, and a caligraphy tool for <a href="http://koffice.kde.org/karbon/">Karbon</a>. KDevMon is ported to KDE 4. Development of the Shaman2 package manager is moved into KDE SVN (playground/sysadmin). The PHP-Qt bindings move from playground/bindings to the kdebindings module. <a href="http://dot.kde.org/1211898836/">KDE 4.1 Beta 1</a> is tagged for release. <a href="http://commit-digest.org/issues/2008-05-25/">Read the rest of the Digest here</a>.

<!--break-->
