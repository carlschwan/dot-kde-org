---
title: "KDE Commit-Digest for 14th September 2008"
date:    2008-09-19
authors:
  - "dallen"
slug:    kde-commit-digest-14th-september-2008
comments:
  - subject: "My precious..."
    date: 2008-09-19
    body: "My precious commit-digest... It's 3h45 am and I MUST read it NOW ! I don't thank you mister Danny :/\n\nCheers :)"
    author: "DanaKil"
  - subject: "PowerDevil"
    date: 2008-09-19
    body: "Well this sounds VERY cool.  This is something I feel has been missing for a while now.  However, I do have one question:\n\n> And a Plasmoid is coming, thanks to Sebastian K\u00fcgler \n> and Riccardo Iaconelli, which will replace the\n> traditional system tray you were used to.\n\nWhat if I don't like all this plasmoid stuff cluttering up my desktop? (except for a few choice icons which is coming, I know)  Will it be possible to have a little status icon down in the tray like the \"Old School\" desktop?  I don't need or want a \"big\" graphs and all sitting on my desktop all the time, I would rather have a little, out-of-the-way icon indicating power/battery and if on battery, charge remaining.\n\nThanx,\nM."
    author: "Xanadu"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Plasma applets can be much the same as having a tray icon except that it is always at the same location!\n\nSo yes, if you have seen the current device notifier, powerdevil's presence would be similar."
    author: "Kishore"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "executive summary: don't worry, you'll be happy.\n\nlong story:\n\n> What if I don't like all this plasmoid stuff cluttering up my desktop?\n\nwe've done such a great job of making plasmoids pervasive that most people don't realize that your entire desktop is made up of plasmoids. everything you see is a plasmoid. the panel itself is a view on a special kind of plasmoid called a \"Containment\".\n\nok, that aside ....\n\n> Will it be possible to have a little status icon down in the tray\n\nwe all hate the system tray for two reasons:\n\n* the idea of xembeding windows is lame and leads to all kinds of headaches. (go look on bugs.kde.org =), but fd.o forces this upon us (our own fault, KDE was instrumental in creating that spec back in the day)\n\n* applications abuse the tray in various ways, populating it with tons of stupid little icons that we can't effectively manage\n\nwe work around the above by either having boring panels built around xembed or nasty hacks to make it appear to work, by implementing things like icon hiding, etc, etc...  but these are all work arounds\n\nin playground we have a new system tray plasmoid that abstracts away the implementation of systray icons into \"system tray protocols\". right now we have two protocols implemented: fd.o and plasmoid.\n\nthe fd.o protocol is the traditional system tray protocol we all know and love to hate. it lets us supporte legacy formats.\n\nthe plasmoid protocol lets you put any plasmoid into the tray; granted this will have some funky results for some plasmoids since tray icons are usually pretty small, but it's perfect for things like the battery or network configuration. it also means we don't have to choose between system tray or flexible widget: we can have both.\n\nthis is pretty important, too, as the system tray app knetworkmanager is sitting taking 65MB of resident, non-shared main memory. as a plasmoid, it should probably take a couple hundred KB. with the new system tray plasmoid, we could put that plasmoid on the desktop, dashboard, panel and/or system tray area. wicked.\n\nthe system tray will also become the source of notifications (so the notification plasmoid won't be a seperate entity there). as a final bit of happiness, i plan to write a spec before 4.2 and implement a protocol for the new system tray plasmoid before 4.3 for a new system tray icon spec to replace the mad fd.o one.\n"
    author: "Aaron Seigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "* applications abuse the tray in various ways, populating it with tons of stupid little icons that we can't effectively manage\n\nSo what do you think should happen with applications that use systray icons like Amarok? Should they provide a plasmoid thay you can add to the systray?"
    author: "Raul"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "i don't think media players should be in the system tray at all. if you wish to stuff it into you system tray and end up with a silly little icon, go for it. but those who wish to have an interface for our media player that's slightly larger than a pencil eraser (probably most of us, to be honest) should have a more reasonable option.\n\nthe system tray is a very useful place to _display important status information_. \n\nit should not be used for an application control interface: it's too small, too crowded.\nit should not be there to provide a placeholder for an otherwise window'less application, for the same reason.\n\nthat said, i'm really not going to argue with people who feel they know better and love their system tray ... they can abuse it to their heart's content =)"
    author: "Aaron Seigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "<a href=\"http://irukado.org/node/77\">followed up in a blog entry</a>\n"
    author: "Lee"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "I do see a case in which applications that you interact with from time to time and are open all the time need to place an icon in the systray to avoid cluttering the taskbar.\n\nThat said, on my systray only amarok, ktorrent, ksensors, knetworkmanager and guidance-power-manager are allowed."
    author: "NabLa"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "so instead of cluttering the taskbar, we clutter the system tray.\n"
    author: "Aaron Seigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "...where the clutter takes much less space.\nI personally prefer 10 systray icons to 10 taskbar entries...\n\nAnother idea to easily access apps running in the background without cluttering anything?"
    author: "Robin"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "ah, you just wrote something about this issue below..."
    author: "Robin"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "Sounds like you prefer a dock to a panel then."
    author: "Ryan"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "I'm so happy someone in such a key position for KDE desktop development has a so clear and correct idea about the fscking systray :) Thanks, Plasma.. systray sanitizing is worth all the plasma migration pain we had in the past. So everything else is a plus  :)"
    author: "Vide"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "I don't quite understand you, Aaron.\n\nRight now my KDE 3.5.10 systray is populated with\n\nSkype\nPSI\nKMAIL\nKNemo\nKMic\nKlipper\nKDE Keyboard Switch\nKSensors\nStarDict\nKWallet\nRSI Break\nMiranda/Wine\n\nI'm not tempted at all by thinking about all these applications consuming the space of my taskbar since I'm already running over six applications at a time and my display resolution is just 1280x1024 not 2560x1600.\n\nIf we don't have a traditional systray then where all these systray icons will go?"
    author: "Artem S. Tashkinov"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Um, in the traditional systray Aaron mentioned?"
    author: "Heja AIK"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Rule #1 of Dot Commenting:\n\n\"If a developer makes a statement of any kind, assign the absolutely most stupid and self-defeating possible interpretation to it.  Play as loose with the rules of grammar and logic as you want\".\n\ne.g.\n\n\"Plasma will focus heavily on being controllable via the keyboard\"\n\nshould be read as:\n\n\"Plasma Lead: Plasma Will No Longer Support Mouse Input\"."
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "Wow,\n\nyou mean it may become actually useful?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "I think you misunderstood.\n\nParaphrasing, Aaron said \"You shouldn't have to use the systray in the traditional way for media players etc. It should be for status icons only.\", Artem asked \"But I use lots of background-type apps that you say shouldn't go in the systray. Where should they go instead if we aren't supposed to use the systray in the traditional way?\". You replied \"You'll still be able to use the systray.\" which makes no sense..."
    author: "Tim"
  - subject: "Re: PowerDevil"
    date: 2008-09-22
    body: "The logical answer is: application should migrate, when due, from a \"systray solution\" to a more correct solution that could be, for example, a little plasma controller, a classic systray or nothing at all, just the plain windows. Right know lots of KDE apps use systray just because they can."
    author: "Vide"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "i talk about controling media players. you list what is mostly a bunch of status icons.\n\ndo you see the difference?\n\nbut lets talk about your list instead.\n\nas for klipper, i do think the system tray icon is a really silly place for it. you should be able to stuff it in there if you wish, but it should also provide a more friendly interface. and those two things should be the *same* thing, just in differen places.\n\nditto for ksensors: it should be a Plasma::PopupApplet and then you'd get all kinds of nift things like be abel to see the full info panel when placed on the desktop.\n\nditto for knemo.\n\nfor things like kmail, kwallet, skype, kgpg, etc, we really need to find a better way to represent \"Things hanging around in the background\". think about it:\n\nwhen you have a kmail window open (as i do right now) you also have an icon in the system tray. two icons for the same thing, in separate locations in the panel with different interaction systems. i believe the best words to describe that is \"fundamentally broken design\".\n\nwhat we really ought to have is the ability for an application to say \"oh, btw, i have a window but i also show status information when my window isn't around. i also have this control menu here that might be useful.\" when an associated window is shown in the taskbar, that information should be merged with that information (which would normally be in the system tray otherwise). when the window goes away, the taskbar should keep an entry for it as a small entry, probably shoved off to the end of the taskbar.\n\nthat way we don't need one icon for the taskbar and one for the systray, the two can work with each other.\n\nthis is not the same as mac os x's dock, though closer in compactness/concept than what we have right now. there are several limitations in mac's dock that i'd like to avoid, which is why i make that part clear."
    author: "Aaron Seigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "<I> > when placed on the desktop.</I>\n\nThere's an excellent contradiction in your words.\n\nIn fact most (applications) icons in my systray are informational icons, i.e. when I'm running KMail I have its icon in the taskbar (and thus I can switch to this applications with a single mouse click) and at the same time I have its icon in the systray which shows the number of unread messages - and I'm really happy I have both of them. So I don't see anything fundamentally broken here ;-)\n\nWhen you attack Kicker's design I can say that it does what it does, it's easily accessible and it doesn't clutter the screen. Just right click the icon and here it is: You can choose the old buffer entries.\n\nSeeing Ksensors data on the desktop is not really convenient - right now I can always monitor my CPU temperature and some other data just glancing over the systray.\n\nSeeing Skype's icon changing its state is also a nifty feature since I understand that I've received an incoming message and I don't really need any other pop up windows and indicators.\n\nThe best thing about systray is that it's tiny, very functional and <B>always</B> visible. The power of plasma applets is enormous but most of the time we do <B>not</B> see the desktop.\n\nI can even say that I'm really sick and tired by KGet's \"Drop Target\" little window, because it's always visible and unused most of the time.\n\nSo I beg you to leave the standard systray for the future.\n\nThank you."
    author: "Artem S. Tashkinov"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "> ...  and at the same time I have its icon in the systray which shows the number of unread messages - and I'm really happy I have both of them. So I don't see anything fundamentally broken here\n\nThe problem is not the informational area showing interesting mail statistics, it is the way this informational area is implemented.\nIt should not be necessary to have the full application running just to present overview information, it should be a small, maybe even mail application independent, second tool collaborating with the full application but not requiring its permanent presence.\n\n> Seeing Ksensors data on the desktop is not really convenient - right now I can always monitor my CPU temperature and some other data just glancing over the systray.\n\nI can do that, even on my KDE3 desktop, without an application embedding itself into the system tray, but rather using Plasmoid's predecessor technology Kicker applets.\nThe KSysGuard applet is a lot more customizable than any systemtray icon/window could ever be.\n\n> The best thing about systray is that it's tiny, very functional and <B>always</B> visible.\n\nTo be <B>always</B> visible it needs to be part of an <B>always</B> visible panel, which means any Plasma applet on the same panel will, surprise surprise, also <B>always</B> we visible.\n\n> The power of plasma applets is enormous but most of the time we do <B>not</B> see the desktop.\n\nThe power of Plasma applets is so enormous that they are <B>not</B> restricted to the desktop. They can be put at the very same level of visibility as the system tray.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "I don't really care about the implementation.\n\nSo, if plasma applets look like aforementioned applications icons presenting the same information in a similar way, then I'm just happy.\n\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "(quote) \"when the window goes away, the taskbar should keep an entry for it as a small entry, probably shoved off to the end of the taskbar.\"\n\nThanks a lot for thinking in this cases.\n\nI only have 4 icons in the systray right now (in KDE 3, because unfortunately I'm running an old distro here): they are klipper, ksensors, the keyboard chooser, and korganizer's daemon. But I will be very very happy to get rid of them when I upgrade to KDE 4. I also agree with you that the systray is overused.\n\nThe only moment in which I could not think of a desktop without system tray, is when I run JuK, Kopete, or KTorrent. Almost never I need to use it's window, so I either ditch them to a virtual desktop, or I hide them in the tray, because with less windows open in the taskbar, it's easier to use the alt+tab switch.\n\nThat kind of apps probably need a way to be handled that we've never seen yet implemented, and your suggestion to show them at the taskbar, but in a different fashion (even less than minimized... micromized? :P) is totally OK for me, and I hope is also OK to other users that work like me.\n"
    author: "suy"
  - subject: "Re: PowerDevil"
    date: 2008-09-24
    body: "I'm glad to see that there are KDE devs who take issue with the mandatory systray placement of Kicker. For me, the program's primary function has always been keeping track of clipboard entries, even after the applications that issued them had been closed. A service could do this as well and better and for anyone who wants to skip back a few entries, there could still be a tray icon or a global shortcut that pops up the list."
    author: "Cyrus"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "If I want to interface with anything I want that to happen in a regular window that I can manage! Not with anything that has it's place hidden in some containment which is cluttering the desktop and on the other hand resists classical window management and placement because it is a plasmoid!"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "<sigh>  The new system tray Aaron mentioned is, by default, sitting in the same place its always been: the panel.  The panel is a containment.  By your measure, is the panel \"cluttering up the desktop\"?  \n\nNothing in plasma \"resists classical window management\".  If plasmoid developers want to use regular windows for user interaction, nothing in plasma prevents them from doing that.  Try to configure the digital or analog clock plasmoid and tell me how that \"resists classical window management\".\n\nYou know developers can have opinions too.  Just because they have opinions how the system tray should be used, doesn't mean they're preventing anyone from doing what they've always done with the system tray.  In fact, in this case, they're making it more flexible by allowing plasmoids to use the system tray."
    author: "Jamboarder"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "didn't we just do this dance the week, Karl? oh, yes, we did:\n\nhttp://dot.kde.org/1219149384/1219200661/1219232051/1219242639/1219261208/1219291804/\n\ni understand you don't like plasma. that's your perogative. but please:\n\n* try and criticize real faults, not made up ones\n\n* when someone offers information that gives new insight to the problem, read it, comprehend it, remember it for the next time\n\n* don't feel compelled to piss on plasma with essentially the same text every time you sit in front of a keyboard with dot.kde.org loaded\n\nissues with \"regular windows\" and \"hidden\" and \"cluttering the desktop\" have *all* been addressed in the past in response to your postings. so either you aren't reading follow ups to your posts or you're just trolling.\n\nthis will be my last reply to you on this matter, as you are currently demonstrating you do not care to engage in a productive dialog."
    author: "Aaron Seigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-24
    body: "> issues with \"regular windows\" and \"hidden\" and \"cluttering the desktop\" have > *all* been addressed in the past in response to your postings. so either you > aren't reading follow ups to your posts or you're just trolling.\nNeither have these issues been adressed - except for a lot of name calling, something which you again employ to discredit an opinion which doesn't do the \"Aaron is great\"-dance - nor are they any less valid today. My beef still is that any plasmoid which offers interaction with the user is basically misplaced inside such a plasmoid - this is a valid design fault, it's fundamental flaw of the design to make user interaction so easy for plasmoids because there are people developing for KDE which take this opportunity and make wrong design decisions. It's this second layer of user interaction that I am opposed against. Your idea might look nice but IMHO it still is a usability nightmare and it will get worse, a lot worse!\nHave you ever tried to explain something like this to someone not so versed as you are in these matters over the phone? Have you ever tried to explain to someone not so deep into this why there is another level of user interaction going on on the desktop? Have you ever had to interact with something like this after your mouse has failed (cordless mouse anyone)... So don't come with the \"Troll\" card just because I don't agree with you and raise concerns on the direction of the development. KDE should be about giving a consistent and coherent UI. Plasma/plasmoids in their current and projected state are all about appearance but they break with a lot of the things that made KDE3 so appealing - everybody was able to understand how and when to interact with the programs running. "
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "In other words, implementation will change, the results will be better, and if I want a blinking nuisance on my panel, it will still be there. If I don't, it won't.\n\nAll I want is the klipper thingy quickly available. I use it quite regularly (probably due to clipboard inconsistencies(bugs) in kdevelop).\n\nDerek"
    author: "dkite"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "(Thank you for replying, Aaron)\n\n> executive summary: don't worry, you'll be happy.\n\nMy Spidey Sense(tm) tells me there's bad news ahead.  :-)\n\n~~~~~~~~~~~~~~~~~~\n\n> * applications abuse the tray in various ways, populating it with \n> tons of stupid little icons that we can't effectively manage.\n\n\"We\" as in Developers, or \"We\" as in Users?  If \"We\" developers, then I'll have to take your word on that one.  If \"We\" Users, I get what you're saying: A ton of crap down there is VERY annoying.  But:\n\n\n> tray icons are usually pretty small, but it's perfect\n> for things like the battery or network configuration.\n\nThat's where I was going with my comment.  That's the kind of thing I use / prefer / rely on the tray for.  Little blinkies showing Network activity that I can click on for more info.  Little power cords or batteries with a % number by it.  Kmail's little icon showing new messages so I don't have to open the thing up.  Little things that I can just lift kicker back up from hiding, (yes, I know you've added hiding, and my svn build has it in.  I've started reading your blog FAR more frequently since I made an ass out of myself here and you gave me some very well said \"what for\"...) quick scan the tray and decide what to do next.\n\n\n> we could put that plasmoid on the desktop, dashboard, panel \n> and/or system tray area.\n\nNow this \"We\" is devs, I got that one :-) .  Here's the thing, though, if the dev feels (as it seems you do from your other comments in this thread) that the system tray is wicked, bad, naughty Zoot! kinda thing, then is the user left without the option of \"minimizing\" something to the tray completely?  Or does the plasmoid system in general just automagicly take over and still allow it?\n\n\n> the system tray will also become the source of notifications\n\nI'm right with ya there, man.  That's all I use it for, and frankly, all it's good for.  Having active interactions with things in the tray is a awkward at best.  *THAT'S* the kind of thing Plasmoids (and karamba stuff, for that matter) is good for.  But that's just me and clearly I'm in the minority (nothing against you specifically or personally, it just seems most people love all that stuff, I just find it all annoying).\n\n\n\nAgain, thanx!\n\nM.\n"
    author: "Xanadu"
  - subject: "Stupid and unrelated"
    date: 2008-09-19
    body: "This question is stupid and mostly unrelated.  However, all this talk of plasmoids and placements got me wondering.\n\nFrankly, it always bugs me that plasmoids look differently than windows and operate differently than windows.  What I'd love is for everything to at the very least look like it belongs together on the same desktop.\n\nMight we see a Plasma theme that more closely matches the Oxygen window deco, or a window deco that more closely matches the default Plasma theme?  What about a means to place plasmoids in traditional windows, or a means to work with windows with the benefits of plasma?\n\nI understand that plasmoids still need to be segregated for ZUI to an extent, but ideally wouldn't it be best if we can choose what goes where?  What if I want the freedom to put my Amarok controls either in a window, in the systray, or part of the dashboard that I pull up with ZUI?\n\nMind you, I ask this perhaps out of ignorance because I'm still not using KDE 4 except for the occasional experiment from time to time (which often still drives me back to KDE 3).\n\n"
    author: "T. J. Brumfield"
  - subject: "Re: Stupid and unrelated"
    date: 2008-09-20
    body: "Hmmm, I wouldn't like plasmoids to look like regular windows. After all, they're very different. They're generally speaking little widgets that show some information, not applications. I can't drag'n'drop an application on my panel and have it resize properly; I can't have an application on my desktop. And I wouldn't want to.\n\nBut maybe you got a point when a plasmoid is promoted to normal window, it might make a bit more sense to make them look like one as well. Not sure..."
    author: "Jos Poortvliet"
  - subject: "A \"Media Player\" runner with support for Amarok 2?"
    date: 2008-09-19
    body: "\nwill this \"Media Player\" runner be an official replacement for the amarok \"player window\" interface in amarok 1.x? .. where can we get more info on this runner?"
    author: "ink"
  - subject: "Re: A \"Media Player\" runner with support for Amaro"
    date: 2008-09-19
    body: "the Media Player runner uses the org.freedesktop.mediaplayer (aka MPRIS) interface, so it will work with any mediaplayer that offers that, which includes Amarok2. it provides controls to the standard media controls like play, pause, blah blah. i believe the docu for it is here: http://wiki.xmms2.xmms.se/index.php/MPRIS"
    author: "Aaron Seigo"
  - subject: "PowerDevil"
    date: 2008-09-19
    body: "As I deeply believe in Allah, I really feel offended by this application name. Could you please change this? I know about daemons but those are at least not user visible. Please consider this and sorry if this request makes you angry.\n\nPeace out and thanks for KDE."
    author: "Ahmed"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "For me computers and the internet are a religion free zone (because it's a global thing, and everybody has it's religion, so you could not make it right for everybody), so religion should not interfere with it. I mean, Devil is a very generic term, it's not a term only used in one religion or so, so everybody from every religion could be offended ( even i'm not sure why somebody would be offended, but i'm a atheist, so don't count on me for that).\n\n"
    author: "Beat Wolf"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "Hello,\n\nthe fact that everyone could be offended, doesn't make it much better, does it?\n\nAnd while at it, remove kill command. It's a fact that UNIX admistrators frequently go on mass murdering due to their getting used to kill-ing. \n\nDon't get me started about \"chmod 666\" of files and specifically the \"Bourne Again Shell\" that defames Jesus or these many daemons you guys run to do evil things. And why would you need a command \"false\", when all you need is \"true\" ?\n\nFor KDE specifically offending already are IO-Slaves, playing on Slavery.\n\nAnd I bet, Taiwan is on the KDE map not part of China, is it?\n\nYours,\nKay\n\nPS: None of these is my opinion or even orignally my content. Obviously everybody can choose names according to their preferences. If you look at wp:devil you will find \"The Devil commands a force of lesser evil spirits, commonly known as demons.\" which gives the name some credit. \n\nI would still assume though that PowerControl or an oldschool KDE name like KPower would be better. Religion jokes are kind of old, aren't they?"
    author: "Debian User"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "hi ahmed,\n\nif you believe in Allah (fear His judgement) then you have nothing to fear from the devil. that's what i understood from the Qur'an (and various other texts).\n\ni do second that it might be better to change the name, freebsd also changed their logo after many years for the reason that people from some cultures where offended by the old one.\n\npersonally i'm not offended the least bit by it -- and i think that spirituality in essence transcends words and images (symbols). yet for many it doesn't. as much as i would like to encourage them not to attach to words and images, a person can give the good example by not attaching to the name and changing it into something that less likely to offend people.\n\n:)"
    author: "cies"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "I also agree that the name 'PowerDevil' is slightly over the edge. With the knowledge this name is offensive to people - and very visible - it leaves me to wonder how much we'd like to keep this name, or like to play stubborn.\n\nThis name is a bit too obvious - in your face - to be left ignored. (at least, we can have a discussion on the ML about it).\n\nI love your well thought out written response btw :)"
    author: "Diederik van der Boor"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "Forgot to add, when I first read about \"PowerDevil\" I had no idea what is was designed for. The name \"Power\" didn't ring any bells about \"PowerManagement\" because of the combination with \"Devil\".   ..just my &#8364;0.02"
    author: "Diederik van der Boor"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Have no fear. It's not the d**** - it's just a DEVice Interaction Layer ;-)\n"
    author: "Michael"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Pretty much everything is bound to offend someone ... . But if just not having that user visible is enough for you, then you are unlikely to see the application name anywhere anyway. Just like right now you would have a hard time actually finding e.g. 'KWin' or 'Solid' written somewhere in the KDE UI.\n"
    author: "Lubo&#353; Lu&#328;\u00e1k"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Somewhere on the dot or the mailing list archives there is a mail sincerely asking us to rename the KIOSlave framework as the word is offensive to the victims of slavery.  To the first bored research student who digs up the reference: I'll write a credible mail to your supervisor explaining your valuable contribution to the KDE archivists' department."
    author: "Will Stephenson"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "are you refering to this bit of well intentioned by woefully executed drivel perhaps? -> http://www.freesoftwaremagazine.com/columns/why_does_kde_use_slaves\n\n"
    author: "Aaron Seigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "No, that's a new one to me.  Wow.  This thing is building momentum.  I'm preparing the commit to rename them all as KIOServiceExecutives."
    author: "Will Stephenson"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "Yeah, and then they'll want stock shares, free cofee and the weekend off :P"
    author: "fabiank22"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "Hello Aaron,\n\nknowing that you are an intelligent person, would you consider to read \"Current Situation\" in http://en.wikipedia.org/wiki/Slavery and think about the issue.\n\nI understand that from Canada, US and Germany (here) that word \"Slave\" sounds like a perfectly suited one to express a technical relationship and has no bad connections (although Germany has had Slaves only 50 years ago and is still trying to make compensations). \n\nBut the current situation about Slavery should not allow us to attach harmless and positive meanings to the word.\n\nIf you read \"Etymology\" you will learn that it was actually standing for \"Slavic people\", meaning it's probably something I should personally take offense to, because of my family.\n\nSo, this is serious. The world would be better fighting Slavery more actively and KDE would be better without using the word Slave for harmless things that are widely viewed positive. \n\nBut then again, I somehow doubt that Free Software is this adult. I recall e.g personally witnessing a speech of Maddog on Linuxtag praising the Chinese for using Linux. \n\nThis in spite of that they do have forced labor, suppression of several kinds, and certainly not the intention to foster any kind of Freedom with Linux.\n\nI didn't recall anybody speak against that, I was only ashamed. And likely it's only my opinion that Freedom should also exist outside the technical domain.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "I'm sorry but why do you people want to take terms that very few find offensive and try to make them into something.  There is no problem with calling it a slave, that is what it is.  There is a world of difference from calling this a slave and slavery.   Then you get all childish and claim because opensource use the term they are not adult. \n\nIf you want to fight slavery then fight it, but to waste your time on computer terms shows that you really aren't interested in fighting slavery, just in ramming your misguided PC attitude on to people."
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "btw, just to add.  My bestfriend was a child prostitute, held captive by a pimp until he was 16.  I work damn hard to fight against such things.  And it is people like you that make me cringe, because all the good work people do fighting such things is easily destroyed by nuts like yourself who take purely innocent terms and try to make them into something they aren't, and then we are faced with having to deal with the problems we are fighting against, and PC nuts like yourself making all the hard working people fighting against such things appear like PC nuts.  "
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "Hello,\n\njust curious, how would you then feel if one of the most successful KDE technologies was named \"prostitute\"? Is it OK to attach positive, harmless means to words describing harmful relationships?\n\nI didn't say I was particularily outraged against the use of the word Slave, although I wasn't aware so far that it comes from Slavic. I just tried to give emphasis that Slavery is not a thing of the past, but of the current times. \n\nAs KDE comes from developers in countries unaware of how widespread Slavery is these days, I felt that it's too easily dismissed as an issue. Note how I didn't demand that anybody change his position, I only proposed to consider the available information from Wikipedia.\n\nAnd China, well. I have this tradition in my family of fighting for Freedom in society. When I was at Linuxtag and learned how proud people can be of a non-free society preferring Linux over Windows, because it allows them to better maintain their non-freedom, I was ashamed. \n\nAllow me that without calling me nuts.  \n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "Well, now YOU are the one who should reconsider his position: Prostitution is a legal business in most countries nowadays, and is in no means \"describing harmful relationships\". Sure there are people that are being - illegally I might add - forced to work as prostitutes. But the same holds true for nearly any other business. Are saying we should rename \"KitchenSync\" because people are forced to work in kitchens somewhere in the world?"
    author: "fabiank22"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "Just consider from wp:Prostitution the following excerpt: \n\n\"Female prostitutes are at risk of violent crime,[14] as well as possibly at higher risk of occupational mortality than any other group of women ever studied. For example, the homicide rate for female prostitutes was estimated to be 204 per 100,000 (Potterat et al, 2004), which is considerably higher than that for the next riskiest occupations in the United States during a similar period (4 per 100,000 for female liquor store workers and 29 per 100,000 for male taxicab drivers) (Castillo et al., 1994)\"\n\nI think it's legal (where it is legal), because it has been found to be even worse for the women where it is illegal. Where it's illegal, they become exploited (instead of protected) by the police too.\n\nBut I didn't consider that it's not a consensus that prostitution is bad, so well I agree it was a bad example.\n\nAnd the kitchen, no. I am not saying that. But you probably knew that, didn't you?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: PowerDevil"
    date: 2008-09-23
    body: "I'm sorry but that is just crap.\n\nIn my country, New Zealand, prostitution is legal, why because we believe in people having the protection and rights of the law.  It was made legal to give prostitutes all the rights that everyone else has in their jobs.  "
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "Well, I was going steer VERY clear of this whole relegion thread until I read this:\n\n> When I was at Linuxtag and learned how proud people can be of\n> a non-free society preferring Linux over Windows, because it\n> allows them to better maintain their non-freedom, I was ashamed.\n\nWhy?  Why were you ashamed?  That whole thought I quoted from you makes no sense to me.  You feel ashamed that people are proud that they have A CHOICE in a \"non-free society\".  A choice that they used to not have?  The choice to \"join\" a non-demeaning, free-thinking, global community such as the FOSS (generally) is?\n\nI have no idea what you're trying to say.  Perhaps I'm just being dense today.  \n\nM."
    author: "Xanadu"
  - subject: "Re: PowerDevil"
    date: 2008-09-21
    body: "Not the people, the government. \n\nThat's a huge difference in a non-free society, actually the definition of non-free is that the people don't matter.\n\nThink Chinese Firewall, Search Result filtering, more \"secure\" end user systems with harder to defeat measures forced by government.\n\nI was ashamed simply because of that going on, and while Cisco and Yahoo or even Google probably don't care, Maddog might at least not haved cited China as a prime example of governmental adoption.\n\nBut then, how sure is e.g. RMS that the Free Software laptop from China he now recommends doesn't contain parts made with forced (child) labor. I am not saying he isn't 100% sure. I just wonder why these things don't matter as much.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: PowerDevil"
    date: 2008-09-23
    body: "you keep bringing up china, are you racist by chance?  Yes China limits what people see, but heck, they aren't the only, America blocks sites as well, and they also monitor everything people do electronically, be it on the net, on the phone, text messages, faxes, etc. And then you go on about child slave labour, and yet you ignore slave labour in the US, or child prostitution in the US.  If you are american, clean your own country up before you go on about others."
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-23
    body: "I honestly wouldn't give a stuff if a computer technology was named prostitute.  My friend was a child prostitute, commonly named a chicken boy, should we ban chickens too?  There is a huge difference between the name KIOSlave and Slaves. and it is only when people like you proclaim a link to slavery that you establish such a link that does not exist.  And it is when you do that, campaigning on insane PC logistics that you do real harm to people who are doing the real work to end such things, instead of silly little comments against technology that in no way is connected to slavery./\n\nYou bring china into a discussion on freedom, if you are american I suggest before you look at other countries you look at your own.  Because from my country, I few America as the greatest threat to freedom and peace.  "
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-22
    body: "I'm sorry but this is NOT seriuos. Blaming using words when out-of-context is just being politically correct for the sake of being it. I mean, changing the KIOSlave name will NOT help in fighting slavery, it's just a non sense. It's not spreading the concept that human slavery is a good thing... they are simply not related in any way."
    author: "Vide"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "I believe the NAACP also filed complaints about IDE using a Master/Slave relationship as well."
    author: "T. J. Brumfield"
  - subject: "Re: PowerDevil"
    date: 2008-09-28
    body: "The religious right in the USA has complained about the fact that *NIX has Daemons and uses the number 666 to denote file permissions (ignoring the fact that it isn't really Six Hundred Sixty Six because it is Octal).\n\nAnd so it goes. :-D"
    author: "JRT"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "Strange I am muslim, but I never felt offended by the name powerDevil, please for the GOD sake, kde is one of the rare communities where the religion, culture, race  don't count, so please don't start this game here, "
    author: "mimoune djouallah"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "http://jdallen.org/news/coda-by-ray-bradbury/\n\nPlease read.\n\nThanks!"
    author: "T. J. Brumfield"
  - subject: "Re: PowerDevil"
    date: 2008-09-19
    body: "I also believe in God but the Devil serves a very good purpose, otherwise God wouldn't have made him so I am cool with the name\n;)"
    author: "Bobby"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "I for one think PowerDevil deserves a name change.\n\nwhy not PowerSATAN?\n\ni would actually start using the laptop without the iv if i had an animated plasma widget bs with lighting bolts shooting from PowerSATAN's asshole to indicate you're on battery power. that would be great."
    author: "I'm in love with aseigo"
  - subject: "Re: PowerDevil"
    date: 2008-09-20
    body: "why are you offended?  You do realise others will be offended that you want to bring your religion into something that has no rleigious context.  Personally I like the name of the application, but hey, I also like Hell's Pizza as well, google it, they make nice pizza, and hey, they just recently brought a man's soul too.  LOL\n\nLife is too short to give a damn about what offends people.  Oh wow, people were offended by the word slave.  Damn,how about a name change to Power Slave Devil?"
    author: "Anon"
  - subject: "Re: PowerDevil"
    date: 2008-09-22
    body: "reminds me of a iron maiden album :-) power slave. great album"
    author: "Beat Wolf"
  - subject: "Re: PowerDevil"
    date: 2008-09-22
    body: "Well. let's call it... the GIMP!\n\nOk ok sorry. That'd be just too obscene."
    author: "Stefan"
  - subject: "suggestion next year's award"
    date: 2008-09-19
    body: "May I propose the next year's award to be handed out to David Nolden? ;-)\n\nFor a long, long time he's doing amazing work on the KDE development platform and as a developer I deeply regards his work...\n\nHarry"
    author: "Harry"
  - subject: "HowTo: Mac OS Dashboard widgets"
    date: 2008-09-19
    body: "Hi there. Prior to the 4.1 release it was mentioned somewhere that Plasma will support MAc OS Dashboard widgets. I could not help me finding a way to install/add them to my Plasma desktop. Though it is off-topic, I hope this is the place to find the right audience: Did this feature make it into 4.1 and how can I use it? \nThanks!"
    author: "Sebastian"
  - subject: "Re: HowTo: Mac OS Dashboard widgets"
    date: 2008-09-19
    body: "It can be done from Add Widgets->Install New Widgets->Install From File in KDE4.1.\n"
    author: "Michael \"OSX\" Howell"
  - subject: "Naming contest"
    date: 2008-09-19
    body: "p.s. If you don't want the Guinness but just want to enter the naming contest for \"SemNotes\", write your entries in this thread ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Naming contest"
    date: 2008-09-20
    body: "How about \"Cloud Nine\"? (It would have been Kloud Nine in the KDE3 series :P)"
    author: "Darryl Wheatley"
  - subject: "koshell?"
    date: 2008-09-19
    body: "\"Kugar and koshell are removed from KOffice 2\"\n\nWhat does that mean? Kugar and koshell panels are removed?"
    author: "Fri13"
  - subject: "Re: koshell?"
    date: 2008-09-19
    body: "They were applications from KOffice 1.x:\nKugar was a reports generator, and koshell was a kontact-like shell for KOffice applications.\n\nThey are not planned to be part of the KOffice 2 release (and they are not maintained), and so they have been removed from KOffice trunk/.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: koshell?"
    date: 2008-09-20
    body: "And when kexi gets released it will have kick-ass reporting which should replace most uses for kugar ;)"
    author: "Adam Pigg"
  - subject: "Re: koshell?"
    date: 2008-09-20
    body: "how can something in kexi replace kugar? kugar was a general purpose report generator, will the report tool from kexi be usable for data from external soures? such as kaddressbook for example, or a csv file?"
    author: "anders"
  - subject: "few questions"
    date: 2008-09-19
    body: "hi everyone,\n\n2 short questions:\n\n1. What about a compatibilty layer in phonon for pulseaudio? think of network based audio streaming (interop?). I know, lots of people asked about it, but there was never a sufficient answer (at least, i didn't find it satisfying..) :)\n2. is the project with the timeline plasmoid still alive?\nare there anywhere more information? read about it months ago, i liked it and it was said that it should be ready for kde 4.2?!\n\ndamn, i had a further question, but i forgot it :/"
    author: "asdf"
  - subject: "Re: few questions"
    date: 2008-09-19
    body: "You can select PulseAudio as a phonon output device. Well, you might have to enable the showing of advanced devices in the configuration module.\nDunno how well that would work out, though..."
    author: "Jonathan Thomas"
  - subject: "Re: few questions"
    date: 2008-09-19
    body: "If you use the Phonon xine-lib backend with these 2 (hackish) patches:\nhttp://cvs.fedoraproject.org/viewvc/rpms/phonon/devel/phonon-4.2.0-pulseaudio.patch?revision=1.1&view=markup\nhttp://cvs.fedoraproject.org/viewvc/rpms/kdebase-runtime/devel/kdebase-runtime-4.1.0-pulseaudio.patch?revision=1.1&view=markup\nit just works.\n\nI'm currently discussing with some key developers how to best fix Phonon-GStreamer and Phonon in general to make it just work for everyone."
    author: "Kevin Kofler"
  - subject: "Re: few questions"
    date: 2008-09-19
    body: "> 2. is the project with the timeline plasmoid still alive?\n\nyes, it's still in my backpocket. unfortunately i've been detained with more infrastructure work both in plasma and in kdelibs; i figured by now i'd be free to write plasmoids, but apparently my purgatory is not yet over ;)"
    author: "Aaron Seigo"
  - subject: "Re: few questions"
    date: 2008-09-20
    body: "We sympathize with your desire to be out of purgatory, all while appreciating the remarkable things that are possible with improvements in the plumbing!"
    author: "T"
  - subject: "Re: few questions"
    date: 2008-09-19
    body: "Every so often I hear about this and every time I mean to ask about it, but then forget. So ...\n\nWhat is the timeline plasmoid ? What does it do?"
    author: "txf"
  - subject: "Powerdevil Screenshot Links"
    date: 2008-09-19
    body: "The links of the 2 first powerdevil screenshots do lead to the commit-digest article again, instead to screenshots? It's not so important, I just wanted to mention it. (using konqueror 3.5.9 here)"
    author: "gttt"
  - subject: "Re: Powerdevil Screenshot Links"
    date: 2008-09-19
    body: "Fixed.\n\nDanny"
    author: "Danny Allen"
  - subject: "see above"
    date: 2008-09-19
    body: "ah the questions was about a backup tool. are there any prjects going on on this topic? something like that: http://lifehacker.com/software/featured-linux-download/timevault-time-machine-for-linux-275399.php\nhttps://launchpad.net/timevault\nsomething like this with a well integrated (qt-)gui? or this: http://www.howtoforge.com/creating-snapshot-backups-with-flyback-ubuntu-7.10\nhttp://code.google.com/p/flyback/\nbest regards"
    author: "asdf"
  - subject: "powerdevil screenshots"
    date: 2008-09-19
    body: "is something wrong with powerdevil screenshots ? i can't open them in oepra here (some data loads, but i land on the same digest page)."
    author: "richlv"
  - subject: "Re: powerdevil screenshots"
    date: 2008-09-19
    body: "Fixed.\n\nDanny"
    author: "Danny Allen"
  - subject: "Resolution"
    date: 2008-09-19
    body: "I thought that all of KDE was supposed to work decently with only 800x600 resolution, is that no longer the case?"
    author: "ac"
  - subject: "Re: Resolution"
    date: 2008-09-19
    body: "there's a difference between \"supposed to\" and \"what the reality is\" ;) these changes are some steps to bring reality a bit closer to what it is supposed to be. nice that people are actually caring enough to put in the work ... something that wasn't always happening (obviously =)"
    author: "Aaron Seigo"
  - subject: "Re: Resolution"
    date: 2008-09-20
    body: "While 600 is nice it does not realy help all of the 800x480 users.\nIs it possible to scale a window on demand as a feature of the windowmanager? Another solution can be auto scrollbars it a window is bigger than the screen, again as a feature of the windowmager.\nPossible or is this limited by X11?\n\nBye\n\n  Thorsten \n\nBTW\nDoes anybody know a very small window decoration for kde4? "
    author: "Thorsten Schnebeck"
  - subject: "Re: Resolution"
    date: 2008-09-20
    body: "Why scrolling the window when you can just move the whole window to see hidden parts? Remember that you can drag a window just clicking and pushing ALT (or whatever key shortcut you configure)."
    author: "Git"
  - subject: "Re: Resolution"
    date: 2008-09-21
    body: "I talk about small devices like the well known netbook device class. So you have to use Alt+LMB+Touchpad (EEE touchpad keys are quite hard ;-). I see many dialogs where you have config on top of dialog and \"Apply\" on bottom. In this case its not very handy to handle \"Alt\" without a separate mouse.\nBut having a shortcut that fits a window into the screen could be quite usefull as buttons are big enough to use these also in a scaled view.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Resolution"
    date: 2008-09-26
    body: "just use the keyboard shortcuts then;  alt-f3, hit 'm' and use arrow keys."
    author: "Thomas Zander"
  - subject: "thanks fredrik"
    date: 2008-09-19
    body: "your work on folderview applet is highly appreciated, now plasma desktop is even better then kde3.5 one,now if some genieus dev add the ability to assign containement to virtual desktop, it will be the best desktop ever on all platform.\n\nthanks really all plasma dev for your work, and aseigo i'am wondering how have supported all those stupid rant about plasma, you simply rock\n\n\na happy user of kde trunk "
    author: "mimoune djouallah"
  - subject: "SemNotes?"
    date: 2008-09-20
    body: "I'm surprised that no one is talking about SemNotes, which looks really intriguing.  Maybe people didn't read the actual article inside the Digest ... ?\n\nDanny - might it be worth, just before the \"Read the rest of the Digest here\" line, having a line advertising \"Featured Articles: Dario describes his work on new power-management system PowerDevil and Laura Dagan describes her work on Nepomuk-enabled note-taking application SemNote\" or somesuch? The featured articles, to me at least, are at least as important as the main \"digest summary\" paragraph and could probably stand to be brought into the limelight a little more, rather than being hidden being a link that needs to be clicked :)"
    author: "Anon"
  - subject: "Re: SemNotes?"
    date: 2008-09-20
    body: "I agree, SemNotes looks really promising. Overall, this was another one of those \"oh my, what an incredible week\" digests. I had to go make a cup of tea to calm down before getting back to real work after reading it."
    author: "T"
---
In <a href="http://commit-digest.org/issues/2008-09-14/">this week's KDE Commit-Digest</a>: "Shortcut Scheme" support allows creation of shortcut themes (Emacs, etc.) for use in KDE applications. A "Media Player" runner (with support for <a href="http://amarok.kde.org/">Amarok</a> 2), more work on panel hiding, and support for text zoom in the "Web Browser" Plasmoid in <a href="http://plasma.kde.org/">Plasma</a>. The "Weather Station" applet moves to kdereview. More refinements in PowerDevil, in preparation for a move to kdebase. Lots more functionality in Attica, the Open Collaboration Services desktop client. Start of session support in KDevPlatform (the basis of KDevelop 4). A "McCabe cyclomatic complexity metric engine" in <a href="http://www.kdevelop.org/">KDevelop</a> 4. Support for image rating (using KRatingWidget) in the interface of <a href="http://kphotoalbum.org/">KPhotoAlbum</a>. Progress towards real levels in the KPicross game. More work towards Jabber-based network games in <a href="http://home.gna.org/ksirk/">KSirK</a>. A "black screen" presentation feature in <a href="http://okular.org/">Okular</a>. Various work in <a href="http://pim.kde.org/akonadi/">Akonadi</a> and <a href="http://pim.kde.org/">KDE-PIM</a>. Start of the NetworkManager KControl module (for use in System Settings, etc). Incremental scanner support returns to Amarok 2. New plugin to specify the download order of multi-file torrents in <a href="http://ktorrent.org/">KTorrent</a>. Passwords saved per LDAP login (not host) in KRDC, greatly improving the experience for LDAP administrators. An OpenGL demo to demonstrate various parts of <a href="http://eigen.tuxfamily.org/">Eigen</a> 2. Some work to make KDE application dialogs fit into 1024x600 pixels. Merge of improvements to KFontInstaller. Import of QuickSand, an alternative front-end for KRunner. A proof-of-concept "<a href="http://decibel.kde.org/">decibel</a>-kde" library for representing contacts "based on the representation used by Kopete". WLM protocol imported into <a href="http://kopete.kde.org/">Kopete</a>. Asciiquarium screensaver moves from kdereview to kdeartwork. Kugar and koshell are removed from <a href="http://koffice.org/">KOffice</a> 2. <a href="http://commit-digest.org/issues/2008-09-14/">Read the rest of the Digest here</a>.



<!--break-->
