---
title: "In Memory of Uwe Thiem"
date:    2008-07-15
authors:
  - "rmoore"
slug:    memory-uwe-thiem
comments:
  - subject: "I am very sad."
    date: 2008-07-15
    body: "Three times I was close to visiting him: once I didn't have the money, the second time I had passport problems, and the third time I got married.\n\nHe loved scifi, we shared a love for Ursula K. LeGuin, for Linux, for KDE.\n\nWe exchanged a bazillion emails, arguing about anything, from the ideal temperature for human beings (27C, IIRC) to how just puting meat over a fire was the most natural kind of meal, and about how he loved cats but couldn't be around one because of his allergies, and his family, and mine, and ... \n\nDamn, this sucks."
    author: "Roberto Alsina"
  - subject: "Thanks"
    date: 2008-07-15
    body: "Thanks for all your work.  you are back home now, living in peace in spirit.  But you have left behind in this world an amazing gift to the world with the work you have done with KDE."
    author: "R. J."
  - subject: "Rest in peace"
    date: 2008-07-15
    body: "Rest in peace, you are probably in a better place now.\n\n... and thanks for all the great you did for free software and KDE :("
    author: "Diego Viola"
  - subject: "Re: Rest in peace"
    date: 2008-07-15
    body: "I second that...\nhope that he will find peace in Open Source paradise.\n"
    author: "Bobby"
  - subject: "Thank you"
    date: 2008-07-15
    body: "Thank you, Uwe, for making this world a better place. You will be with me as long as I will be able to have memories."
    author: "Cristian Tibirna"
  - subject: "Re: Thank you"
    date: 2008-07-16
    body: "He was a very valuable contributor and a nice person. Condolences to his friends and family."
    author: "Jonathan Singer"
  - subject: "*winkt*"
    date: 2008-07-15
    body: "Gute Reise."
    author: "kdeuser"
  - subject: "RIP"
    date: 2008-07-15
    body: "Thanks for everything. My condolences to his family and friends."
    author: "Joergen Ramskov"
  - subject: "Re: RIP"
    date: 2008-07-15
    body: "++"
    author: "Thorsten Schnebeck"
  - subject: "Re: RIP"
    date: 2008-07-16
    body: "Rest in Peace. My deep condolences to his family and friends. "
    author: "axel"
  - subject: "Thanks"
    date: 2008-07-15
    body: "Thanks for everything."
    author: "Johann Ollivier Lapeyr"
  - subject: "I am a selfish bastard"
    date: 2008-07-15
    body: "All I can think of is that now I will never get to meet him face to face. :'("
    author: "taj"
  - subject: "Danke"
    date: 2008-07-15
    body: "Thank you, Uwe, for everything you did for us and the project in all those years. I will miss your voice of reason, the numerous times you helped us pathing the way and making the right decisions. May your soul rest in peace. "
    author: "Matthias Ettrich"
  - subject: "Does anyone know what happened to Jacob Rideout?"
    date: 2008-07-15
    body: "The sad news of Uwe Thiem's passing makes me think of the unexplained disappearance of some other prominent KDE contributors, including Sonnet developer Jacob Rideout (http://dot.kde.org/1183388210/1183406251). Does anyone know what happened to him?"
    author: "Bill"
  - subject: "Re: Does anyone know what happened to Jacob Rideout?"
    date: 2008-07-17
    body: "Yes, I do know what happened to Jacob, and it is not sad or terrible. Rest easy on that issue; I will respect Jacob's silence on the matter, though."
    author: "Adriaan de Groot"
  - subject: "Re: Does anyone know what happened to Jacob Rideou"
    date: 2008-07-18
    body: "Good to know, thanks!"
    author: "Bill"
  - subject: "Heaven will be a better place"
    date: 2008-07-15
    body: "now you are there, Uwe!"
    author: "Tristan Grimaux"
  - subject: ":-("
    date: 2008-07-15
    body: "Rest in peace!"
    author: "Dread Knight"
  - subject: "Rest in peace"
    date: 2008-07-15
    body: "Uwe Thiem's name was one of the first names I learned to know when I got involved with the KDE project (e.g. Uwe was the creator of KFract and I knew him from the mailing lists where he was very active during the early time of KDE development). And he certainly contributed to the spirit that attracted me to this project.\nI've admired and appreciated his level-headed and insightful mails and like others I feel disappointed that I never learned to know him in person.\nIt's a sad day for KDE and for the world we live in now that we've lost somebody like Uwe. "
    author: "Torsten Rahn"
  - subject: "Re: Rest in peace"
    date: 2008-07-15
    body: "It's interesting Torsten said pretty much what I was thinking. In fact the decision process I had to make when Quanta split early  on was really about the moraility of free software, which I think was in some part a reflection of Uwe's influence. Having worked my way back from a high mortality health condition a few years ago I had time to consider my legacy. Contributing to a project that empowers people around the world is a wonderful thing to leave behind. \n\nMany of us probably don't realize just how much Uwe Thiem influenced those who played a part in programs we use every day. In fact his last article should still be influencing us today... It is indeed a sad day."
    author: "Eric Laffoon"
  - subject: ":("
    date: 2008-07-15
    body: "Even in his last moments, he still contributed to KDE, he defended KDE 4 via an article here: http://www.tectonic.co.za/?p=2552.\n\nMay you rest in peace, we will miss you :'("
    author: "fred"
  - subject: "That's life"
    date: 2008-07-15
    body: "Thank you for beeing a KDE contributor, Uwe! Rest in peace!"
    author: "Me"
  - subject: "Oh Dear"
    date: 2008-07-15
    body: "He was quite active within the Gentoo lists as well, and when I started out with it I found I didn't need to post much because Uwe had already answered quite a bit. Just little nuggets here and there.\n\nUwe's one of those rocks where you think \"What's going to happen now?\" when he's not around."
    author: "Segedunum"
  - subject: "good bye"
    date: 2008-07-15
    body: "R.I.P. Friend and thanks you very much for all that you do for the community."
    author: "draconial"
  - subject: "A great loss"
    date: 2008-07-15
    body: "Uwe contributed so much, no just to KDE but in his community, his loss will be immeasurable.  Two strong voices for Free software lost in one week.\n\nMight I suggest the codename for the final 4.1 release be something appropriate to pay tribute to Uwe?"
    author: "odysseus"
  - subject: "Re: A great loss"
    date: 2008-07-20
    body: "I think this is a suggestion which should be indeed considered by the KDE community."
    author: "James"
  - subject: ":`("
    date: 2008-07-15
    body: "You are in a better place now. Rest in peace.\nYou have made the world a better place, to say the least.\nIt is a sad parting, we all can see.\nFor you to say goodbye to KDE.\n"
    author: "Michael \"at the point of crying\" Howell"
  - subject: "re:"
    date: 2008-07-16
    body: "Sad news. \n\nR.I.P."
    author: "Bojan"
  - subject: ":("
    date: 2008-07-16
    body: "Rest in peace"
    author: "biquillo"
  - subject: "Sorry to hear."
    date: 2008-07-17
    body: "I didn't know the guy, but I'm sorry to hear this.  My condolences go out to the family.\n\nI don't believe the departed can hear us, so I will say this, to the KDE contributors still with us:  Your work is greatly appreciated."
    author: "3vi1"
  - subject: "That's so sad"
    date: 2008-07-17
    body: "My condolences"
    author: "nombray"
  - subject: "Fare thee well"
    date: 2008-07-17
    body: "Condolence to his family and the KDE team.  You all know him better than us.  But through all your works and effort, we (the users) have a glimpse of his passion."
    author: "ryj_cube"
  - subject: "Rest in peace"
    date: 2008-07-19
    body: "I didn't know him, but I'm sure he's a good person :)\n\nRest in peace Uwe.\n\nI know you are in a better place\n\nDescanse em paz e tenha uma boa outra vida."
    author: "Ot\u00e1vio Souza"
  - subject: " In Memory of Uwe Thiem"
    date: 2008-07-25
    body: "he may rest in place."
    author: "john smith"
  - subject: "..."
    date: 2008-07-25
    body: "..."
    author: "Vide"
  - subject: "Thanks for this contribution!"
    date: 2008-07-30
    body: "Thanks to you i use kde desktop enviroment and linux :)."
    author: "mikedomo"
  - subject: "The FFII sends its condolences to Uwe's Family"
    date: 2008-07-30
    body: "FFII as friendly association with the KDE project wants to send tis condolences to the Uwe's Family and to all the KDE team by extension.\n\nRest in peace and thanks for you contribution to the Human development, Uwe,\n\nAlberto Barrionuevo\nPresident FFII"
    author: "Alberto Barrionuevo"
---
<img src="http://www.kde.org/contact/img/uwe.jpg" style="float:right"/>
I'm very sorry to let everyone know that <a href="http://www.uwix.iway.na/uwe.html">Uwe Thiem</a>, a long term contributor to KDE, passed away yesterday at 14:45 of kidney failure. Uwe was one of the longest contributors to the KDE family and was one of the original members of the core development team. He moved on to become the main KDE representative in Africa. Uwe was one of the first people to write a book on KDE development, which helped many people who have become regular contributors today, and was still <a href="http://www.tectonic.co.za/?p=2552">writing about KDE last week</a>. Aaron Seigo spoke for us all when he said <i>"Uwe had a deep love for and belief in Africa and the role that technology can, and should, play on that continent. He put his back into it and was a great advocate for Free software in his area of the world"</i>. Our thoughts go out to his family and friends at this irreplaceable loss, we are all a little less than we were yesterday.

<!--break-->
