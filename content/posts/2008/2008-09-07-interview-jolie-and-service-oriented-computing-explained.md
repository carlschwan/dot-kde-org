---
title: "Interview: JOLIE and Service-Oriented Computing Explained"
date:    2008-09-07
authors:
  - "jpoortvliet"
slug:    interview-jolie-and-service-oriented-computing-explained
comments:
  - subject: "Thanks"
    date: 2008-09-07
    body: "Good interview, thanks."
    author: "Dave Null"
  - subject: "great job and interview"
    date: 2008-09-07
    body: "i haven't look a lot about jolie, but it seems really a good project. And btw.. from the eye of another italian see that sometimes something come out from this country it's really good :)"
    author: "marco"
  - subject: "Share Nepomuk Data"
    date: 2008-09-07
    body: "Can i Share (anonymized) nepomuk data with the community ???\n\nthat would be great !"
    author: "Sahreo"
  - subject: "Re: Share Nepomuk Data"
    date: 2008-09-07
    body: "Once we've got the Plasma<->JOLIE architecture set up, each desktop will be able to expose services and other desktop will be able to interact with those services. So, I'd say yes. =)"
    author: "Fabrizio Montesi"
  - subject: "Good Interview"
    date: 2008-09-07
    body: "I'd heard about JOLIE, Plasma integration and the distributed slideshow demo, but I didn't grasp what it was all about (lots of buzzwords, very little explanation). This interview has straightened that out, so now I'm pretty excited about the possibilities of using and integrating it myself :)\n\n\"So, for example, the \"Time\" engine on one computer can be queried as a JOLIE system from a remote PC.\" - I think you may have been spending too much time around Plasma. Distributed clocks!"
    author: "Chris Warburton"
  - subject: "i think SOAP..."
    date: 2008-09-07
    body: "...is dead in the water!"
    author: "Flavio"
  - subject: "BPEL/WS-CDL"
    date: 2008-09-07
    body: "How does this all compare to BPEL or WS-CDL?"
    author: "panzi"
  - subject: "Re: BPEL/WS-CDL"
    date: 2008-09-07
    body: "JOLIE allows for service orchestration, as BPEL does. The main differences are:\n- JOLIE is independent from the underlying communication technology, whereas BPEL is built specifically for Web Services (though extensions are being made);\n- JOLIE is built upon a formal theory, whereas BPEL semantics are unclear in some cases and different BPEL interpreters sometimes behave differently;\n- JOLIE allows for dynamic fault and compensation handling, whereas BPEL uses a static approach;\n- JOLIE is easy to understand and get started with, BPEL is much more complicated for reaching the same goals;\n- JOLIE syntax is intuitive, whereas in BPEL you need a graphical editor because of the heavy XML-based syntax;\n- BPEL is very well integrated with Web Services specifications, whereas JOLIE implementation still lacks some of the most peculiar features (like WS-Addressing and WS-Security full implementations, but we're working on those);\n- much more... ;)\n\nWS-CDL is about choreography, i.e. the description of the behaviour of an entire service-oriented architecture (not just one service, as orchestration does). We're developing a theoretical framework for choreography, equipped with safety properties and projection. With projection, one can first describe the global behaviour (like \"Client sends to Bank the TransferMoney message\") with our choreography language and ask the projection algorithm to produce the real orchestrator applications (Bank and Client) that will follow that behaviour.\n\nOnce that research about choreography is completed, we'll have another programming language and that will be comparable to WS-CDL. The fact that JOLIE is based on our theoretical work implies that we will be able to project on JOLIE programs, i.e. we will be able to generate JOLIE programs automatically from a global behaviour description."
    author: "Fabrizio Montesi"
  - subject: "Why another programming language?"
    date: 2008-09-07
    body: "@JOLIE developers\n\nI think I understand the problem that JOLIE is trying to tackle, but I don't understand why it is necessary to make another programming language instead of just producing a library/framework for an existing one? Why is JOLIE so special? Also how does JOLIE fit in relation to existing languages? Is it something which you would embed inside a more general language/program?\n\ncheers,\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Why another programming language?"
    date: 2008-09-08
    body: "Good (and big :-)) questions!\n\n> Why a language and not a library?\n\nTwo main factors: ease of use and virtualization.\n\nService-oriented architecture defines a new programming paradigm, one in which your main actions are making communications and compose data. Advancing to more complex service-oriented applications, you would want to make various communications concurrent, or you'd be in a situation where more than one possible message could reach you at any moment (non deterministic input communication).\nTaking care of all of that in current languages is very difficult. JOLIE internals can already be used as a Java library (we're already exploiting it in some standalone client application) in order to get communication abstraction, but you're not getting all the power JOLIE can offer.\n\nThe fact that JOLIE is a language gives us two powerful advantages.\nEase of use: an easy to learn syntax equipped with powerful primitives allows newbie service programmers to approach complex service-oriented problems in much less time than having to learn a complex language (like C/C++, Java, etc.) and then how the JOLIE library works.\nVirtualization: the JOLIE interpreter is purposely studied to follow the semantics of our theoretical framework, so we can apply code verifiers derived from our research. We can check if a JOLIE program has some properties or not.\n\nFinally, perhaps the most important thing is that we saw how service-oriented computing is defining a new paradigm. A new paradigm requires a new language, much like it's been done for the object-oriented one. This is because applying new paradigms to languages oriented to other ones can result in big headaches and in the end doesn't produce the full power that the paradigm could offer.\n\n\n> How does JOLIE related to other languages?\nIn JOLIE everything is a service, and you just communicate with and orchestrate them. For example, the console is seen as a service (Console) to which you can send messages like \"Please print this message\".\nCurrently, you have two choices for making another language interact with JOLIE:\n\n- make a JOLIE JavaService library: this is like defining a library for other languages. You make a .jar containing one or more classes that extends the jolie.runtime.JavaService class. JOLIE will be able to load these classes and make them available as services to the JOLIE programmer. This process is very straightforward (you just extend JavaService and you're pretty done). Using the Java Native Interface, you could use this to bridge JOLIE to other language;\n\n- implement one of JOLIE protocols in another language and then communicate with JOLIE over sockets/file/memory/whatever supported by both. You can use any one of it, but if you're interested in performance you'd opt for SODEP, a fast and efficient binary protocol. This is the path we followed for integrating JOLIE with Plasma.\n\nNo matter what choice you make, the JOLIE program that's gonna use your thing doesn't care and won't need to change if you change idea in the future. This is, for JOLIE, a \"communication detail\" that you just need to configure in the program preamble.\n\n\n> Is it something which you would embed inside a more general language/program?\nThat depends on the scenario.\n- You can embed a JOLIE program in your application to bridge you to and/or manage a service-oriented architecture.\n- You can use your language of choice for making some services and then use JOLIE to orchestrate them.\n\nJOLIE has a lot of places to fit in, and if you're speaking of service-oriented architectures it's most likely the language that has the most general \"view\", because its basic element is a \"service\"."
    author: "Fabrizio Montesi"
  - subject: "italianaSoftware"
    date: 2008-09-07
    body: "Do you think there is a market for (open source+Desktop) service-oriented software solutions? Could you describe a typical customer of yours?"
    author: "testerus"
  - subject: "Re: italianaSoftware"
    date: 2008-09-08
    body: "> Do you think there is a market for (open source+Desktop) service-oriented software solutions?\n\nJOLIE has lowered the time needed for implementing a distributed system, but we still have to build user interfaces for these systems. We have made libraries and frameworks to help us, but having a desktop powered by a truly service-oriented technology is what could make a very big difference in this (UI integration with service-oriented applications). I think the JOLIE+Plasma experience will show the benefits of the approach.\n\n> Could you describe a typical customer of yours?\n- Public Administration.\n- Anyone that needs a distributed application.\n- Anyone that has problems with software integration.\n- Anyone that needs to evolve an old software architecture to a service-oriented one.\n- Anyone that needs a distributed application equipped with some property (like the \"this won't cause my money to fly away\" case in the interview).\n- Others... we're discovering new possibilities as we go."
    author: "Fabrizio Montesi"
  - subject: "CORBA improved?"
    date: 2008-09-08
    body: "Much of the features of JOLIE remember me of CORBA (language -> IDL, independience of technology...), that more or less standard (at least implemented by several parties) very fat piece of middleware. \n\nRunning away from that we got to more technology specific but at least much smaller and faster DCOP and then DBUS.\n\nI would like to know how JOLIE is better than CORBA (I am sure that it is)\n"
    author: "Jordi Polo"
  - subject: "Re: CORBA improved?"
    date: 2008-09-08
    body: "JOLIE ...\n- is lightweight;\n- offers a new programming paradigm, allowing much easier definition of distributed applications and of single services;\n- offers innovative semantics for fault and compensation handling;\n- offers an open-source reference implementation;\n- joins the experiences of theoretical academic research and practical software development;\n- integrates nicely with nowadays web technologies.\n\nWe're currently working on a couple features we want (strong message typing, automatic service crash recovery), but we've already come a long way. Expect detailed information on the new JOLIE website that we're preparing (will be published this fall)."
    author: "Fabrizio Montesi"
  - subject: "Security?"
    date: 2008-09-08
    body: "I'm just kind of worried about the security side of things. I wouldn't want my desktop to be saturated with Windows-RPC-like functionality :)\n\nWould that technology be opt-in? (for example, used only on some non-default plasmoids which I would use if I thought were useful) (or any other kind of reassurance, really :P)"
    author: "Ioannis Gyftos"
  - subject: "Re: Security?"
    date: 2008-09-08
    body: "> Would that technology be opt-in?\nYes.\n\nAbout security, implementing authentication methods in JOLIE is trivial and we're building the support for automatic encryption. Moreover, _you_ (the user) will be able to select what you'd want to be exposed and what not."
    author: "Fabrizio Montesi"
  - subject: "test"
    date: 2008-09-10
    body: "Test"
    author: "danimo"
  - subject: "Re: test"
    date: 2008-09-10
    body: "* log started\n\nTest results for test \"test\" issued on 10/Sep/2008, @07:00\n\n- test 1 (validate subject): passed.\n- test 2 (validate body): passed.\n- test 3 (check html generation): passed.\n- test 4 (check if comment is on topic): FAILED.\n\n3 out of 4 tests passed.\n\n* log fininshed\n\nNice to see dot is up again :)\n"
    author: "christoph"
---
During Akademy 2008, we sat down with Fabrizio Montesi who's working on <a href="http://jolie.sourceforge.net">JOLIE</a> integration in KDE (and Plasma in particular). He explained the mechanics of the technology and what it can do for KDE. Read on for the interview.

<!--break-->
<p><b>Hi Fabrizio! Can you introduce yourself?</b></p>

<p>Hi! My name is Fabrizio Montesi, I'm Italian and I work as a computer professional. Recently I have founded (together with my colleague Claudio Guidi), <a href="http://www.italianasoftware.com/">italianaSoftware s.r.l.</a>, a company that centers its business around service-oriented software solutions made with JOLIE.<br><br></p>


<p><b>At Akademy you gave a talk about JOLIE, the technology you are working on. Can you explain the purpose of JOLIE?</b></p>

<p>Well, <a href="http://jolie.sourceforge.net">JOLIE</a> is a programming language for service-oriented computing. It is mostly about communication between applications. Usually, applications have communication mechanisms within themselves - in Qt this is done with the signal-slot mechanism. What it does is essentially this: suppose you have a button, and you click it. That button then tells a part of the application to start doing "its thing", e.g. display an image. Simple.</p>

<p>Now, you might want to have something happen in <em>another</em> applicaton if you hit that button; that's covered in the software world as well, e.g. on Linux/UNIX by DCOP and D-Bus, on Windows by DCOM. The problem is that these technologies are pretty specific and each one has its own set of limitations (among which the most prominent is that some don't work over networks). JOLIE tries to overcome all these limitations and offer a very simple solution for doing what should indeed be simple: "just send this message to that application in that computer".<br><br></p>


<p><b>So JOLIE is like a network-enabled D-Bus?</b></p>

<p>Well, no, it's more than that. D-Bus is a framework designed to enable application integration in the desktop. JOLIE is a fully-fledged programming language for managing service-oriented architectures and technologies. With JOLIE, you can write flexible service "orchestrators" and compose other services, independently of their technology, in order to gain sophisticated functionality.<br><br></p>


<p><b>Now that sounds interesting, "orchestrators" and composition of services, but what does it mean?</b></p>

<p>Let me explain this by giving an example. Say that you want to write an application which allows you to buy stuff at stores. You already have services for accessing your bank and said stores, but you lack the application that actually composes these services to make the money transfer and make the store order for you. Then you write an "orchestrator" to combine the services, which would coordinate the services in order to do what you wanted. Note that a JOLIE orchestrator is very easy to write and can make use of any store and bank service that are based on a technology supported by JOLIE (like, for instance, Web Services, REST, and so on).</p>

<p>Which is what JOLIE is all about - a generic programming language for programming any kind of service or service-oriented architecture, independent of the underlying protocols (JOLIE abstracts the communication away, e.g. D-Bus apps can communicate with a SOAP-based service through JOLIE). And of course, this is incredibly easy to use. In most other languages you'd find it is very hard to write service-oriented code, but JOLIE is all about services. Of course it also provides the normal flow control functions (like the if-else, if-then, while, for, foreach statements), and it adds some specific and powerful tools to handle distributed workflows. The latter help in compensating for network lag and help the programmer to handle complex asynchronous communications. And finally, JOLIE is very safe while doing this, due to the academic work being done.<br><br></p>


<p><b>So people can quickly write orchestrators to let any number of services work together in any way they want. Now you mentioned academic work, can you tell us a bit more about that? This is actually a research project, right?</b></p>

<p>Yes, it is. Writing distributed apps is very difficult to get right. JOLIE is based on SOCK, a process algebra for service-oriented computing, so you can make mathematical proofs on JOLIE applications. For example, we are currently developing a tool for checking distributed systems for possible deadlocks. Another advantage is that you can be sure your application does what it is supposed to do if something goes wrong in some part of your distributed workflow.</p>

<p>Let me give an example of that last point as well. Say, in the previous example, you're ready to order the bank to transfer the money and receive the store receipt. You want this to go fast, so you do two things at once: start the money transaction and wait for the store receipt. Say that the waiting-for-receipt activity receives an error. In that case, the money transaction must be cancelled: you don't want to lose your money for a product that will not be sent to you, right? But you don't want to cancel it at some unknown point. You want to ensure <em>nothing</em> happened to your money at all. So you add a little "revert when an error comes in" thing to the money transaction code. Now, in case of an error from the store, JOLIE will guarantee three things:<br><ul>
<li>if no money has been transferred yet, the transaction won't begin at all;
<li>if the transfer has already started, JOLIE will allow it to finish, then start the reverting action;
<li>if the transaction has finished already, JOLIE will revert it right away.</ul></p>

<p>All this is based on lots of mathematical work to ensure and prove that this works properly. This is a good example of how SOCK is useful in our development process. When we face a very complicated and general problem, we can first build a mathematical framework for solving that problem in SOCK.</p>

<p>After developing the whole theoretical framework and proving that it works, we transfer the results into JOLIE.<br><br></p>


<p><b>Interesting. So this is an implementation of sound, theoretical work. But why in KDE?</b></p>

<p>Well, I wanted to bring the benefits of service-oriented computing to desktop users. There are many services out there on the web and in user computers (every D-Bus-enabled application can indeed be seen as a service), but they don't communicate with each other. JOLIE can help with this. There are a few commercial frameworks which do comparable things, but nothing free, nor very good and complete. Now for this to work, I needed to find an organization which would be interested to work on it. I needed a real, open community to work with, so Vista and Mac OS X were off the list... Vista wouldn't have been very good from a technological standpoint either. I further needed the community to be flexible, interested and pro-active.</p>

<p>I was following the evolution of open desktop technologies since a long time. KDE has some amazing, cutting-edge technology, and you don't write that kind of stuff without many open discussions. And from reading the blogs I was convinced this community works great, is open and flexible, exactly what I was looking for. For me, GNOME seemed much less flexible, both in terms of people and technology. More importantly, KDE showed with the KDE4 series that the project is not afraid to take a step towards innovation, regardless of the hardships that you can meet along the way.</p>

<p>And with Plasma, I was sold: it felt like a natural choice. JOLIE is based upon a philosophy which emphasizes generic solutions over specific ones in order to create something as flexible and powerful as possible. The same applies to Plasma, and as i've seen in KDE development, it is the vibe you see in pretty much all of KDE.<br><br></p>


<p><b>OK, so you decide to work with us. How did that go?</b></p>

<p>Well, I sent an email to Aaron Seigo and he answered back enthusiastically. He happened to have been thinking on very similar topics, but he bumped into the issues JOLIE is built to solve - it's hard to write, compose and communicate with services. On top of that, there are a lot of different communication mechanisms currently used by services all around the world: how to be able to use all of them? These issues are far from trivial, so Aaron was happy to work with us to take advantage of JOLIE. He invited me to come to Tokamak, the Plasma meeting, which I and Claudio did. There we (JOLIE and Plasma teams) found that we clearly had matching ideas. JOLIE and Plasma looked like the perfect match to offer users a flexible and powerful service-oriented experience. So we started a twofold collaboration, aimed to unite the pragmatic world of KDE and the academic world from which JOLIE comes.<br><br></p>


<p><b>Cool. And now you're at Akademy... How did you end up here, and what do you think about it?</b></p>

<p>Well, Aaron and Kevin Ottens invited me to send a talk proposal for Akademy, which I did. I must say Akademy is very interesting, though also very tiring. You find so many great people to speak with here, and I actually did that for the whole time. I've never had so many well informed questions before, too... before and after the presentation! Actually, I think the majority of questions I had during all the presentations I ever gave were asked at Akademy. Of course, this is because there are so many knowledgeable, interested developers here. Many people here will (or already have) actually put some of these ideas to work, so they have questions about it. Getting so much relevant feedback in an academic conference would be unusual: it is more difficult to reach technological collaboration between many different and separated projects. So this has been really great, i've gathered many ideas and issues i'm going to take with me, think about, and work on. And i've left something to think about for others, too.<br><br></p>


<p><b>From what I understood, there is already some code?</b></p>

<p>Yes, that's true. We are already writing a Plasma::Service layer which acts as a bridge to MetaService (a JOLIE orchestrator) on the JOLIE side. This means you can access all services supported by JOLIE in Plasma. You would have to write an orchestrator to compose services, like in the previous e-commerce example. But as you can read in <a href="http://fmontesi.blogspot.com/">my blog</a>, there are some cool code examples. One of them is <a href="http://fmontesi.blogspot.com/2008/07/vision-distributed-presentation.html">Vision</a>, a tool which can distribute presentations real-time over several computers. That way several people can view the presentation on their screens synchronized with one another. This even works in a peer-to-peer way, where each of the PC's can distribute it even further. Look in my blog for <a href="http://jolie.sourceforge.net/videos/vision-previewer.avi">a screencast</a>!</p>

<p>Something else, not available already but we're working on, is exporting data engines from one system to another. So, for example, the "Time" engine on one computer can be queried as a JOLIE system from a remote PC.
All of this is difficult to do right in terms of security, but opens a huge number of opportunities. Think about how easy it will become to start writing remote control Plasmoids, or getting access to a huge amount of information (that of services on the internet) from your desktop.<br><br></p>


<p><b>Do you have any concluding words for the KDE community?</b></p>

<p>Yes. i'd like to thank (and praise) the KDE community for its openness. I felt at ease from the start, met enthusiasm and innovative ideas, and discovered that the people behind the project are friendly, open-minded and a lot of fun to pass your time with.</p>

<p>During these months I have seen that the KDE project is truly innovation-driven. Open-source and community openness are what make this possible, and KDE is showing that it is capable of handling all this (with the KDE e.V., the meetings, and so on). Keep rocking!<br><br></p>


<p><b>Thank you for the interview!</b></p>

<p>Thank you for interviewing me. And who knows... see you at next Akademy!</p>
