---
title: "Integration at Akademy"
date:    2008-08-13
authors:
  - "jpoortvliet"
slug:    integration-akademy
comments:
  - subject: "I'm loving it"
    date: 2008-08-13
    body: "...no, not mc donald, but kde :)\nIntegration is a really good thing, but what happen if you don't have marble installed? It everything at the kde libs level and \"marble\" just show everything, or to use a map in koffice i'll have to install marble too ?\n\n(a little wish about integration, what about a link between akregator and kopete? now is possible to \"send an article link address by mail\", what about a \"send an article link to...\" and then select a connected (or not) user in kopete. What do you think? the same thing could be do to akregator -> konversation too )"
    author: "xdmx"
  - subject: "Re: I'm loving it"
    date: 2008-08-13
    body: "<quote>(a little wish about integration, what about a link between akregator and kopete? now is possible to \"send an article link address by mail\", what about a \"send an article link to...\" and then select a connected (or not) user in kopete. What do you think? the same thing could be do to akregator -> konversation too )</quote>\n\nWell, I'd think that this sort of thing should be done with Akonadi and/or Decibel. I agree that this sort of integration would be awesome but at the same time this is exactly the sort of integration that people this week at Akademy have been talking about creating. I'd imagine that come KDE 4.2 or 4.3 we'll be seeing really cool thing like this being done."
    author: "Matt Williams"
  - subject: "Re: I'm loving it"
    date: 2008-08-13
    body: "> I'd imagine that come KDE 4.2 or 4.3 we'll be seeing really cool thing like this being done.\n\nI hope so :) For now i'll just wait kde 4.1, gentoo hasn't it yet in portage :(\nbtw... -5 months to kde 4.2... wow :)"
    author: "xdmx"
  - subject: "Re: I'm loving it"
    date: 2008-08-13
    body: "Not really related to the article, but as a matter of fact I'm reading this Akregator on KDE 4.1.0 on Gentoo, just search google for 'kdesvn-overlay'.\n\nI btw also really look forward for this kind of integration. It started when I noticed that kmail showed whether a contact of kopete was on/offline (some long time ago) and I think it was always one of the big KDE things (as opposed to f.e. GNOME with their simplicity goals)."
    author: "Erik van 't Wout"
  - subject: "Re: I'm loving it"
    date: 2008-08-13
    body: "> Not really related to the article, but as a matter of fact I'm reading this Akregator on KDE 4.1.0 on Gentoo, just search google for 'kdesvn-overlay'.\n\nUsing the overlay yes, but there isn't in the official portage (but as i see from their git they're doing it, but i don't know how much time it will take to complete :( ). it's just to have it in the official one instead of an overlay (not that it's bad or not save, but kde 4.1 missing from the official one isn't nice :) )\n\nYes, the kmail -> kopete integration is very useful :)"
    author: "xdmx"
  - subject: "Re: I'm loving it"
    date: 2008-08-13
    body: "I am using kde 4.1 using overlay in gentoo as well.\nAnd appart from the nvidia bugs it works very well.\n\nThis way i'll probably get 4.2 fast as well :D"
    author: "Mark Hannessen"
  - subject: "Excel 98 had a map object"
    date: 2008-08-13
    body: "Excel map object could use tables in the sheet so that it was easy to make maps with different shade of gray depending on the value, much like maps in the Commit Digest.\n\nIt's too bad this feature was taken away some time before Office XP, it was nice. I hope I can do the same with KOffice.\n\nThanks for KDE!\n"
    author: "Anonymous Coward"
  - subject: "foo"
    date: 2008-08-13
    body: "AFAIR, the last time I used M$ Office as my main office suite (it was M$ Office 97, the newest ;)), there WAS a map component possible to integrate in documents. It was very primitive, true, but it existed. I don't know about current borg-office incarnations, as I didn't use them much."
    author: "slacker"
  - subject: "Political maps made easy?"
    date: 2008-08-13
    body: "This kind of integration could be great for political stats junkies. With a spreadsheet directly tied to a map that gets shaded/colored based on the data in the spread sheet (so long as one could link the data to specific geographic localities) all kinds of nice map representations of demographic and political statistics would be easy."
    author: "yokem55"
  - subject: "Misinformation"
    date: 2008-08-13
    body: "Actually Lotus 123 had maps integration too.\n\nMaybe these guys shold do some google before posting this kind of misinformation."
    author: "David"
  - subject: "Re: Misinformation"
    date: 2008-08-13
    body: "I don't believe Lotus 123 will allow you to integrate interactive maps.  Euro Office has map charts, but nothing like Marble."
    author: "T. J. Brumfield"
  - subject: "kdegames in konqueror?"
    date: 2008-08-13
    body: "I always wondered if integration of kdegames in konqueror would be possible... A lot of people stay with proprietary instant-messangers because they can play some games with their contacts. KDE has a bulk of network-ready games that could exchange ip-information, at least over one protocoll, like jabber."
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-13
    body: "Sorry, not konqueror but kopete :/ 5 years of Xfce and just switched to kde, am pretty confused by all the ks ;)\n\nAgain, kdegames in kopete would be just awesome.\n\n\"Con just invited you to a round of KReversi. Click \"OK\" to connect\""
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-13
    body: "> \"Con just invited you to a round of KReversi. Click \"OK\" to connect\"\n\nBut that would be possibile just with kopete <-> kopete (or future clients which will support this), it would be nice something where everyone using everything could use, like kopete, gtalk, msn messenger, trillian, icq, aol, etc etc...\nBut i think that this would be very hard to do, especially which closed clients :(\n\nbtw, games just in kopete<->kopete would be nice to have too :)"
    author: "xdmx"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "could there be a better reason to switch to kopete? esp. for those windows-users :)\n\nWell, maybe this could be developed as a jabber-extension (like jingle, muc, etc.), so every other jabber-client could adopt it and additional games could be added /add themselves to the list :)"
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "mmm... i don't know, personally i use kopete, but i think about a lot of windows-users (most under 18) who use stuff like msn messenger because there are so useless stuffs like animoticons (or how they call them), msn messenger plus which add so many useless things, and stuff like that. They probably won't switch to kopete, also if there are games. Instead who use trillian or apps like it because they have more than one account, would be possible that they look to kopete.\n\nA jabber-extension would be nice, but i think that there are 2 problems... first you must have a jabber account to play with the extensions, right? and the second is that you must have the game installed too, so with the client X integrating that extension should tell the user that other apps/games has to be installed (or by user choice during the setup, or by request when the user try to play or receive a request from someone else)."
    author: "xdmx"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "Well... the jabber-extension could communicate about the installed games. For example both of us communicate via kopete and icq. I make a right click on your account in the roster and choose games. kopete tells me I have no jabber-account listed for you. I ask you \"do you have a jabber account?\" \"Hell no, what's that?\" \"We could play games with it\" \"What do I have to do?\" \"Well... If you have an email-account over gmail, web or freenet, you already have a jabber-account, just configure it in konqueror. Elseway you can just register to a jabber-account inside konqueror\" \"Wow, that was easy. Here's my account-name:xxx@xxx.xx\" \"Hey, we both have wormux installed. Want to play?\" \"WOOO\"\n\nI must say this would be assuming, konqueror would scan your .desktop-files (linux/bsd-only?) and filter it with a whitelist or something like that. Still, Kdegames for the beginning should be easy to detect and install on all systems. My first propose was about integration of kdegames with kopete and now I start talking about a gaming-framework that gets way to complicated :D\nLet's just stay with kdegames<->kopete for now, even if dreaming is nice ;)"
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "again mixed konqueror and kopete. Exchange every \"konqueror\" with \"kopete\", sorry..."
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "Sounds like you want to integrate kggz with kopete.  kggz needs some work, but some of the KDE games already have integration with ggz.  I think this is the way forward."
    author: "Dan"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "no, it's not about ggz, since ggz is abandoned, broken and buggy. Also it has a total different goal. GGZ is for building up a lobby and find people who want to play the same game as you. The integration of kdegames in kopete would be about playing with a special person, the game itself is less important. If jabber would be used as the basis of this, it would still be very much possible to build up lobbys by simply setting up MUCs. \nYou see, the design would have several advantages compared to ggz, as it would be absolutely decentral and there is no need for the actual game to implement anything special but direct connections or an integrated server."
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-15
    body: "Alright con, with you we've found a real expert to speak up here. You should probably check out the latest KGGZ code and have a look at the roadmap which was presented at Akademy 2008. Your definition of 'abandoned' seems to be a bit special..."
    author: "josef"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-15
    body: "Well, I've tried about 7 different main-clients for ggz. I compiled the whole stuff (did you ever do this? it's really a mess) I tried to play several games and most were absolutely broken. That's to the kde-clients as well as the gnome-clients. Some games, like widelands, have discovered ggz is not the right environment for them and stopped using it.\nIt's not like I don't like the ggz idea. I very much like the idea of a better kggz and wish it all luck. But still, kggz is not the answer to the question I started, is it? If there is a comfortable way to integrate kggz into kopete, so it can be used for directly connecting 2 or even more users from the context menu... I'd very much like this, but I don't think it is really necessary to use ggz, as for a lot of games, not supported by ggz, all kopete would need to do is exchanging the IPs."
    author: "con"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-15
    body: "It seems like you did not ever do a multi-player game. If it were only about exchanging IPs, GGZ would not have so much LOC (200000 I believe). If you think GGZ was abandoned, that might be because packagers are rather ignoring it currently. That will change when libkdegames 4.2 includes all the new KGGZ libs.\n\nBy the way, Josef indeed did already compile KGGZ, he is its maintainer. I compiled it too, and once the KGGZ libs do not have to be fetched from another SVN repo (4.2), it will be damn easy."
    author: "Stefan"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-16
    body: "What you're missing is that GGZ allows having several different clients for a given game, so a Window$ user could use a W32 client, a GNOME user a GTK+ client, a KDE user a KDE client and a Mac user a Cocoa client, all working with the same server."
    author: "Kevin Kofler"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-14
    body: "\nKopete does has animated emotions too, you can download them from kde-look.org or from kopete by using \"get new stuff\". On KDE4 those are on System settings panel and it is easy to change shortcuts to emotions too, like if you want =) to be smiley and not :).\n\nThe kopete game play could be easy to do. Just with small HTTP server what would serve a game board for other players browser so other player does not need Kopete/KDE at all, just a web browser."
    author: "Thomas.S"
  - subject: "Re: kdegames in kopete"
    date: 2008-08-15
    body: "> Kopete does has animated emotions too, you can download them from kde-look.org or from kopete by using \"get new stuff\".\n>On KDE4 those are on System settings panel and it is easy to change shortcuts to emotions too, like if you want =) to be smiley and not :).\n\nNo, i didn't mean animated emoticons (i have them already :) the bad thing is that using kopete 3.5.8, other clients don't see them, don't know if it's a bug or not). But i was referring to the others, i remember that with one of the last releases (2 year ago probably) in msn messenger you can send useless stuff to the other client which take all the screen to show an animation+sound, i don't remember how they call them :) iirc one of them was like a hammer which crash the screen, or stuff like that, the problem is that they show on all the screen, and are quite annoying (and useless too :) )\n\n\n> The kopete game play could be easy to do. Just with small HTTP server what would serve a game board for other players browser so other player does not need Kopete/KDE at all, just a web browser.\n\nThis would be very nice :) With 2 possibly problems: firewall and nat.. the first it's quite easy to solve, just add a rule on that port. But what about who is under a nat network? (ok, having it for \"normal connections\" would be very nice, but there are them with a annoying nat too :) )"
    author: "xdmx"
  - subject: "Re: kdegames in konqueror?"
    date: 2008-08-14
    body: "I remember games like max99kbgames for windows,\nthose where really tiny apps who where supposed to\nkompletely be transfered via netwok if the other\ndid not have it.\n\nSo, we have one klick installations under linux.\n\nWe have install on request / first need in linux\n(i remember codec installs in kaffeine)\n\nintegrate all this to achieve kdegames<->kopete.\n\nkudos to all devs,\nRalph."
    author: "Ralph"
  - subject: "so many possibilities"
    date: 2008-08-13
    body: "All this stuff is so exciting, so many possibilities, so many things around the corner. I _love_ marble.\n\nRe. the picture of a trike: that's cool, what even is that? A huge fuel tank? A cess-tank? "
    author: "maninalift"
  - subject: "Re: so many possibilities"
    date: 2008-08-13
    body: "I meant to say I _love_ DigiKam, I haven't used marble that much to be honest"
    author: "maninalift"
  - subject: "I think it's a barbecue cooker"
    date: 2008-08-13
    body: "Or maybe a smoker.  I hope that it's not fueled in part off of exhaust from the engine!  \n\nMmMmmmm . . . carbon monoxide ribs."
    author: "Ed Cates"
  - subject: "KOffice-Plasma ideas"
    date: 2008-08-14
    body: "The integration of all these apps really shows the power of the KDE desktop and I can't image what will be possible in the future.\n\nThings that come in mind:\nWould it be possible to integrate Plasma and KOffice shape technology? It would be cool if Koffice could be used to create Plasmoids and scripted with an integrated KDevelop.\n\nAnother cool thing would be to have something like a Basket plasmoid for KOffice shapes maybe similar to this:  http://koffice.org/competition/gui1results/koffice_nodoc.png\nNepomuk integration would be nice too ;-)\n\nThanks to the developers that make this cooperation between different projects possible and create such amazing results.\n"
    author: "ac"
  - subject: "Re: KOffice-Plasma ideas"
    date: 2008-08-14
    body: "I imagine it might be pretty nice to have a Plasmoid that showed live data from a shared database.  It would be nice if you could shape the data form view in KOffice, and then export a Plasmoid to your desktop."
    author: "T. J. Brumfield"
  - subject: "Re: KOffice-Plasma ideas"
    date: 2008-08-14
    body: "Also, if plasma is integrated to toolbar+menu. This will enable apps to have different ways of displaying the toolbar+menu like office 2007 ribbon or merge them to have mac style toolbars+ single menu and can have all sorts of animations poosible with plasma. This would be really nice"
    author: "pns"
  - subject: "Just because you can doesn't mean you should"
    date: 2008-08-14
    body: "This is a great article about integration in KDE and a lot of the things mentioned can allow new ideas to turn more quickly into real applications, so I'm thrilled about this whole concept.\n\nHowever... The steam(?)motorcycle image and its motto has a point too, and I rather missed this in the article, anyone willing to expand on that?\n\nI have some thoughts on this, I'd love to hear your opinions...\n\nIn a way this type of integration looks a lot like the Unix way of working on the CLI, you can pipe together a complex \"application\" from a few simple tools using the shell. The risk of this are when the application interface changes, in the CLI tools it is the options or the output format, in KDE it would be the API and the functionality of the modules.\n\nWho is responsible for keeping all the interfaces working as is expected? Would this limit the creativity of the module makers? Is there a great temptation to branch off a copy of a module after development has been blocked at the request of integrators, thus creating more bloat in the KDE framework? (This is all hypothetical, of course)\n\nAnd the idea of integrating Marble in KOffice sounds great, but how does this interoperate with other ODF implementations? Is a copy of the map in image form included in the document? Is some sort of generic map-object proposed for inclusion in a future version of the ODF standard?\n\nHaving said all that, I'd still love to see all the wonderful ideas take shape using integration :-)\n\nCheers\n\nSimon"
    author: "Simon Oosthoek"
  - subject: "Re: Just because you can doesn't mean you should"
    date: 2008-08-14
    body: "KDE has always guaranteed api stability for its library for the duration of its major release."
    author: "Ian Monroe"
  - subject: "Re: Just because you can doesn't mean you should"
    date: 2008-08-14
    body: "Perhaps I misunderstand, but I get the impression that this goes beyond libraries? Or is every potential module which is capable of being integrated in other applications by definition a library?\n\nCheers\n\nSimon"
    author: "Simon Oosthoek"
  - subject: "Re: Just because you can doesn't mean you should"
    date: 2008-08-15
    body: "\"And the idea of integrating Marble in KOffice sounds great, but how does this interoperate with other ODF implementations? Is a copy of the map in image form included in the document? Is some sort of generic map-object proposed for inclusion in a future version of the ODF standard?\"\n\nI've also been wondering how this is supposed to work.  Similarly for music notes and other unique stuff that's been added to KOffice 2."
    author: "Yuriy Kozlov"
  - subject: "Re: Just because you can doesn't mean you should"
    date: 2008-08-18
    body: "<quote>And the idea of integrating Marble in KOffice sounds great, but how does this interoperate with other ODF implementations? Is a copy of the map in image form included in the document? Is some sort of generic map-object proposed for inclusion in a future version of the ODF standard?</quote>\n\nODF has a way to embed data of unknown types in it. Basically, an ODF file contains a zip file with a file tree within. Each unknown data type is kept in a different \"file\" in the ODF filetree, and the document itself contains a link to it. There is also a preview bitmap generated by the application that understands the data type.\n\nSo all an application that doesn't understand the datatype in question has to do is to show the bitmap, let the file be and keep the link. The user cannot edit the unknown data, but it can be viewed and printed."
    author: "Inge Wallin"
  - subject: "This sounds like a good plaform for a phone"
    date: 2008-08-14
    body: "Maybe the devs who got a free N810 could thoroughly investigate this integration and produce a fantastic system on it that goes a few steps further than the iPhone interface.\n\nThat would make Nokia a happy bunny"
    author: "Ian"
  - subject: "About the digikam -> marble integration"
    date: 2008-08-14
    body: "Is there also from marble -> digikam ? I mean, if i add a photo on digikam and set the position, then when i'll open marble, will be there the possibility to view a thumb of that photo too ? This would be very nice to have, if there isn't yet :)"
    author: "xdmx"
  - subject: "Re: About the digikam -> marble integration"
    date: 2008-08-14
    body: "\nThat would be nice idea, to have integration in both ways. Then we would have options on applications what would allow choosing what integration we like to have. Example we could choose that Marble does not show what digiKam has marked but digiKam does use Marble.\n\nI like the digiKam's Marble integration, and in future when 0.10.0 comes out, we can do hopefully do all kind more powerfull searched, current SVN version is very promising already, wayt to christmas for next release ;-)"
    author: "Fri13"
  - subject: "Re: About the digikam -> marble integration"
    date: 2008-08-15
    body: "For example something like this (taken from the link posted below by Torsten Rahn :) )\nhttp://techfreaks4u.com/blog/wp-content/uploads/2008/07/amrble_current1.png\nhttp://techfreaks4u.com/blog/wp-content/uploads/2008/07/marble_current.png\n\na plugin for marble which integrates panoramio. ok, that's cool too, but which the digikam private photo collection would be even nicer :) (maybe if the zoom is too out, instead of showing too many photos of the same area it could take just one in a given circular range based on the photo's rank and other stuff (and maybe the possibility to filter the images with tags and ranking). nice, isn't it? :)"
    author: "xdmx"
  - subject: "Re: About the digikam -> marble integration"
    date: 2008-08-16
    body: "In future it might be possible to save advanced search filters. You could set to filters any of metadata what photographs has and then even a area of marble where those metadatas are searched.\n\nThis way you could do it by like, first you filter out images what has taken on time 21-22, direction to sunset and then you drag area on marble sidepanel and you get out photos only on that place what match your other filters.\n\nKDE should support more digiKam because it is currently best photo management software done for KDE."
    author: "Fri13"
  - subject: "Re: About the digikam -> marble integration"
    date: 2008-08-18
    body: "For this to happen, the digikam author has to structure digikam so that parts of it are easily integrated.  To start that development was the whole point of my talk."
    author: "Inge Wallin"
  - subject: "Cloud computing"
    date: 2008-08-14
    body: "I wonder if cloud computing is on the radar. When it comes to integration that is something were we will see many developments."
    author: "velum"
  - subject: "Re: Cloud computing"
    date: 2008-08-19
    body: "I suppose that's what JOLIE is going to cover. Stay tuned for an in-depth article about it!"
    author: "jospoortvliet"
  - subject: "Marble + RSS news on the desktop"
    date: 2008-08-14
    body: "I would love to see a Plasma applet that shows you an interactive Earth globe (with Marble) with little dots on it representing localized news, so if you hover the mouse over a dot you see the headline, and if you click on it you can read more in your browser. The dots could be color-coded, so a red dot would represent bad news, a green one good news, etc.\nThe problem that I can see is that the news source would require some kind of extra metadata.\n\nPS: sorry for my English."
    author: "David"
  - subject: "Re: Marble + RSS news on the desktop"
    date: 2008-08-15
    body: "We'd love to see that, too. Similar to Shashanks Twitter Plugin:\n\nhttp://techfreaks4u.com/blog/?tag=marble-kde"
    author: "Torsten Rahn"
  - subject: "Re: Marble + RSS news on the desktop"
    date: 2008-08-15
    body: "I believe Google News ties into Google maps automatically, except the other day an article about the war in Georgia had a map with the US state of Georgia."
    author: "T. J. Brumfield"
  - subject: "Plato-PIM?"
    date: 2008-08-14
    body: "Integrating Plato and KDE-PIM would be good too. It would be awsome to have a planned project in plato exported into KDE-PIM and see how it would fit together with other projects there.\n\nJust a thought."
    author: "Oscar"
  - subject: "Re: Plato-PIM?"
    date: 2008-08-15
    body: "Brilliant.  I love it."
    author: "T. J. Brumfield"
---
An important topic at <a href="http://akademy2008.kde.org/">Akademy 2008</a> is modularisation and integration. It has been the main topic of one talk, but you will find it comes up in many others. It is clearly on the minds of many developers here. Read on for more of what is discussed here.


<!--break-->
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-integration.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-integration-wee.jpg" width="400" height="320" /></a><br />
Integration: just because you can, doesn't mean you should.
</div>

<p>One of the talks on Saturday morning was about integration. As it was given by <a href="http://behindkde.org/people/ingwa/">Inge Wallin</a> it quickly started to resemble a <a href="http://edu.kde.org/marble">Marble</a> talk - fans of that application must be having a blast, as there's also a 'real' <a href="http://akademy.kde.org/conference/presentation/18.php">talk about Marble by Patrick Spendrin</a>. Marble was used by Inge to show one of the long-time strengths of KDE - integration. We have always had technologies like KIO which allows network transparent file access, and the KParts framework which makes it incredibly easy to embed applications in other applications - think about what Konqueror does, showing pictures using Gwenview, for example. In KDE 4, many new technologies were introduced, some, like the ones just mentioned, low-level like Solid (hardware information) and Phonon (audio & video) but also more complex. Marble is a shining example here - it is a full-blown virtual earth application. But you can integrate it on several levels in many other applications and customise it with powerful additional visualisations. Take KOffice - yes, Marble can integrate in KOffice. You can have a document, be it a spreadsheet, presentation or text document, and have a real, interactive map in there. This clearly shows the power of integration - map applications aren't new, neither are office suites. But as far as we know nobody has combined these to the same level before outside of KDE.</p>

<p>Important here is how you can enable integration in several ways. In KDE 3, we mostly saw two kinds of integration - either low-level in the form of libraries used to enhance the functionality in your application, or in the form of a KPart, embedding the application pretty much as-is into another one. Marble shows that more can be done. It not only offers some low-level libraries and a KPart, but it is also specifically designed to act as a plugin in other applications. For example in KAddressBook, where it could be used to link a person to a location.</p>

<p>So for a developer, there are many options now: you can use Marble's low-level infrastructure to achieve a very high level of integration, you can integrate the Marble interface as a KPart - which ensures there are no dependencies and the integration is easier. Or you can achieve less tight (but much easier) integration by using it as a plugin. If you are new to KDE development, writing a plugin for an application is one of the easiest ways to get involved.</p>

<p>Thomas Zander from KWord fame and now employed by <a href="http://www.nokia.com/">Nokia</a> told us he is seeing a growing trend in this area. Noting how hard it can be to figure out what people are interested in when you want to give a talk at Akademy, he is happy there is a clear vision within the KDE community. We have always focused on integration and modularisation, but this vision has crystalised in KDE 4. Almost every sub-project seems to target architectural modularisation and making re-use of code easier, according to Thomas. Look at KOffice with its "Flake" shape technology, Marble, Plasma, and even Qt with the work going into the ODF writer (allowing every Qt app to easily create ODF files). Fabrizio Montesi also <a href="http://fmontesi.blogspot.com/2008/08/are-we-converging-to-service-oriented.html">wrote about this trend</a> in his blog. The JOLIE framework he is working on could bring some great new capabilities in this area, <a href="http://dot.kde.org/1218388866/">merging</a> the architectural advantages of KDE with the powerful service-oriented technology of <a href="http://portal.acm.org/citation.cfm?id=1332479.1333608&coll=GUIDE&dl=GUIDE&CFID=15151515&CFTOKEN=6184618">JOLIE</a>. Look for an interview about this topic online soon!</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jr/akademy-2008-integration-marble-digikam.jpg"><img src="http://static.kdenews.org/jr/akademy-2008-integration-marble-digikam-wee.jpg" width="400" height="320" /></a><br />
Digikam integrated with Marble
</div>

<p>This flexibility is something more KDE applications are and should be aspiring to. A <a href="http://wadejolson.wordpress.com/2008/08/10/akademy-day-2-morning-notes/">funny coincidence</a> was noted by Wade in his blog, where he wrote how Gilles Caulier of <a href="http://digikam.org/">Digikam</a>, a strong early adopter of many of the new technologies in KDE, was talking about integration of Marble in the Digikam talk at the exact same time Inge mentioned it in his integration talk... we are clearly very synchronised in our thinking about this topic.</p>

<p>During the <a href="http://nepomuk.kde.org">NEPOMUK</a> talk by <a href="http://www.deri.ie/about/team/member/laura_josan/">Laura Josan</a>, more integration ideas came up. Very interesting was the description of how Marble could work with Amarok and its Last.fm integration to show a globe with artists and their locations. You could search for a location and see bands from that area, or search for other bands close to certain artists. These kind of things would be rather easy to develop thanks to the infrastructure being built by Marble. More ideas like these are floating around, and many more could come up in the future.</p>

<p>Easy integration, using either the plugin method or KParts technology allow a developer to quickly assess the value of integrating a particular technology, and the tight integration possible with libraries and widgets they provide gives us the potential for entirely new use cases. If you think about integration, and allow your applications to be integrated into others, or to make use of other applications, you or someone else in KDE may stumble upon something incredibly powerful and innovative. And as our current strategy is focusing on doing new, original, innovative things, more integration offers a very compelling avenue for acheiving those targets.</p>

<p>The "Akonadi Rumble" offered even more examples, talking about integrating Akonadi and Decibel in Kopete. Decibel has to store its contacts and the instant messages in Akonadi, which then uses NEPOMUK and Strigi to search through them. This way, you can connect every bit of information related to one person and show it anywhere. As Akonadi is ignorant of the types of data, you can, for example, view chat logs threaded within your email application. The framework, being generalised, thus brings together information from many sources and makes it much easier to work with it in several ways depending on what you are doing. You can keep a much better overview and thus more easily manage your personal information.</p>

<p>As you can see, Akademy is more about integration and cooperation than ever, and KDE is consolidating its lead in this area in the computing world.</p>
