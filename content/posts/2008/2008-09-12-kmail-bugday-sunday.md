---
title: "KMail BugDay on Sunday"
date:    2008-09-12
authors:
  - "jriddell"
slug:    kmail-bugday-sunday
comments:
  - subject: "KMail BugDay on Sunday"
    date: 2008-09-12
    body: "I _will_ take part. That nasty IMAP inbox filters bug still raises its head from time to time (even in trunk). I wanna see it squashed, it's damn annoying."
    author: "JJ"
  - subject: "I'll be there!"
    date: 2008-09-12
    body: "Finally I'll be able to help in a BugDay ( you always set it on exams time!)\n\nHope I can help"
    author: "Damnshock"
  - subject: "KMail version 4.1"
    date: 2008-09-13
    body: "means KDE version 4.1? Because my kmail version is 1.10.1"
    author: "Stephan"
  - subject: "Re: KMail version 4.1"
    date: 2008-09-13
    body: "Yes, thats correct. It should probably say \"KMail from KDE 4.1\"."
    author: "George Goldberg"
  - subject: "More detail"
    date: 2008-09-13
    body: "There's an excellent blog post by Michael Leupold explaining about Bug Triage and the KDE Bugsquad, so if you think you might be interested or want to know more about what will happen on Sunday, it is well worth reading.\n\nhttp://tinyurl.com/3tnrhu"
    author: "George Goldberg"
  - subject: "Re: More detail"
    date: 2008-09-27
    body: "\"You also need a current release of KDE (4.1 is OK, 4.1.1 is good, compiled from trunk is best) and some patience.\"\n\nWhy is compiled from TRUNK best?\n\nI, from a quality management viewpoint, would suggest that it is not only not best but it is not a good idea.  What is best is to have the latest revision of the current release branch built from source.  Actually, you must have built it from source because otherwise you have no way of knowing if the distro fixed the bug.\n\nBugs that are filed against the release BRANCH should be tested against the release branch because only a release BRANCH is stable enough for bug testing.  It is poor quality management to close a bug because \"it works in TRUNK\".  The fact that it works in TRUNK is probably good information, but the bug should not be considered fixed till it is fixed in the (then) current release BRANCH.  The reason for this is that bugs that are closed because they \"work in TRUNK\" seem to reoccur.  Even when fixed in the release branch, they sometimes come back before the next release.  Sometimes they come back after the next release, in which case they should be clearly identified as regressions since these should have the highest priority to be fixed.\n\nYes, this complicates things, but it is clear that KDE does have quality issues.  Probably the best way to deal with this is issue to add additional status information in BugZilla.\n\nI also have to wonder if reporting bugs against TRUNK is really the best way to deal with them.  Specifically, what action needs to be taken after the TRUNK that they were reported against becomes a release?"
    author: "JRT"
  - subject: "Bughunt..."
    date: 2008-09-14
    body: "Damn I readed this too late. I wish I could read this few days ago so I could get the laptop with me to place where I am now.\n\nOh, I hope someone would implent the Ctrl+M option to Kmail for better usability like on kopete, dolphin, konqueror, ark and many other applications have becaue of it. The menu hiding is bretty important for clean application GUI."
    author: "Fri13"
---
The <a href="http://techbase.kde.org/index.php?title=Contribute/Bugsquad">KDE BugSquad</a> is pleased to announce another BugDay! Come and learn the fine art of bug triage. How might one do so? Join us for a KMail BugDay on Sunday, September 14th (7:00 UTC). All you need is KMail version 4.1 or more recent. That is it! We will provide all the training and support. No programming knowledge is needed. Join #kde-bugs on irc.freenode.net anytime to find out more details. Also, we have a spiffy <a href="https://mail.kde.org/mailman/listinfo/bugsquad">mailing list</a> and lots of new documentation on techbase. See you there!

<!--break-->
