---
title: "Website for the KDE Utilities Launched"
date:    2008-07-26
authors:
  - "fkossebau"
slug:    website-kde-utilities-launched
comments:
  - subject: "Wow..."
    date: 2008-07-26
    body: "That's looking really good. Having a uniform look across all *.kde.org pages I think is really important, and you guys have done a fantastic job with this one.\nThe KDE Utilities site looks far better than most of the sites for the main KDE apps. I'm loving the overuse of \"screenie\" - it helps livening up the pages, and again the with the consistency across the sites. \n\nHopefully this will spark some more development across the other kde web pages. Especially doc.kde.org, that site could really do with a makeover."
    author: "David Edmundson"
  - subject: "Re: Wow..."
    date: 2008-07-26
    body: "Thanks :)\n\nThose, who like David :) want to give their little contribution to makeover  http://docs.kde.org/ a little, please see here for contact information: http://l10n.kde.org/docs/index-script.php"
    author: "Friedrich W. H. Kossebau"
  - subject: "Re: Wow..."
    date: 2008-07-26
    body: "I agree."
    author: "markus"
  - subject: "xD"
    date: 2008-07-26
    body: "Floopy disks anyone? xD"
    author: "Dread Knight"
  - subject: "Python in KDE question"
    date: 2008-07-26
    body: "So, is Python officially a dependency of KDE now? The Printer Applet, and I think J. Riddle wrote a System Settings module in Python recently that's been adopted. I'm planning on writing a System Settings module in the next few months for configuring backups, and I thought I would have to write it in C++ to have a chance of it being included with KDE proper, but if I could write it in Python, it would be so much easier. I know KDE used to insist on being pure Qt/C++ but has that changed now?"
    author: "kwilliam"
  - subject: "Re: Python in KDE question"
    date: 2008-07-26
    body: "It is a 'soft' dependency, meaning that you can build and install the kdeutils without Python support. You just won't get the printer applet. One of the ideas that was floating about while leading up to KDE 4.0 was to have at least one non-trivial application to be in KDE and written in a language other than C++. \n\nI say go for it if you want to write KDE software in Python. If you write something good but people don't want to run it because its in Python/Ruby/whatever, then it is their loss. A dependency on Python/Ruby/etc should just be viewed the same as any other dependency. Pros vs cons, reward vs cost. Language shouldn't be treated as a special case IMHO.\n\nI personally don't think there is a compelling technical reason for any of the stuff in systemsettings not to be written in higher level language such as Python or Ruby. Same goes for much of the stuff in the kdeutils and kdeedu modules. It would certainly make it much easier for more people to contribute to KDE. Not to mention it is much easier to diagnose an error message and stacktrace from a user than a segfault. But that is a different post. =)\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: Python in KDE question"
    date: 2008-07-28
    body: "It's not a hard dependency, as someone else has said. A few years ago I probably would have quibbled with it, but Python has become nearly a ubiquitous standard."
    author: "David Johnson"
  - subject: "KDE 4.1 is going to ROCK everybody"
    date: 2008-07-26
    body: "I know this is off-the-topic, BUT I can't help it !!\n\nI am using KDE 4.0.99 on Arch64 and I must that I am BLOWN AWAY by its beauty in design and plain goodness everywhere. This is going to be a HUGE success guys !!\n\nHUGE THANKS to the devs !!\n"
    author: "Junaid Shahid"
  - subject: "KMid anyone?"
    date: 2008-07-26
    body: "I know that is formally a part of KDE Multimedia, but I want to give credit to a little app that is, IMO, the best MIDI player ever for Linux. KMid has been with us since unknown times, and is STILL alive and kicking, despite the fact the last revision of it was in 1999 (release 1.7 for KDE 1)\n\nPlease, give us some insights about KMid 2 (for KDE 4.1) I have some love screenshots a little more updated than http://perso.wanadoo.es/antlarr/pics/kmid_screenshot1.png ."
    author: "Alejandro Nova"
---
The <a href="http://www.kde.org/family">family</a> of KDE websites has got a new member, the site for the fine utilities applications from the module kdeutils.  Despite being one of the first modules, kdeutils has always been without its own website. No longer. At <a href="http://utils.kde.org">utils.kde.org</a> you can now find a lot of information about the KDE Utilities. See for yourself the details of the current set of programs below.








<!--break-->
<div style="border: thin solid grey; float: right; padding: 1ex; margin: 1ex">
<img src="http://static.kdenews.org/jr/kdeutils-website.jpg" width="532" height="297" />
</div>

<p>
The module kdeutils is one of the modules that has been around since the very beginning of KDE. At the time of <a href="http://www.kde.org/announcements/announce-1.0.php">KDE 1.0</a> the module kdeutils was already well populated and <a href="http://websvn.kde.org/trunk/kdeutils/?pathrev=8800">consisted</a> of KArm (now KTimeTracker), KCalc, KEdit, KFloppy, KHexedit, KJots, KLJetTool, KNotes, KTop (now KSysGuard) and KZip (now Ark). The observant will see that nearly all of these programs are still part of KDE and kicking. Only three of them are no longer in development: the very specialised program KLJetTool (used for configuring HP Laserjet printers) was <a href="http://lists.kde.org/?l=kde-commits&m=101948694220101&w=2">removed</a> from the development branch before KDE 3.1 due to being obsolete, and KEdit and KHexEdit were removed only recently before KDE 4.0, due to being unmaintained and with substitutes (KWrite and Okteta).
</p>

<p>
Over the times a younger module has drawn away some of the programs: KTimeTracker, KNotes, and with the upcoming KDE 4.1 also KJots are now part of the module <a href="http://websvn.kde.org/trunk/KDE/kdepim/?pathrev=837673">kdepim</a>. Any removal has been balanced with completely new programs. The two freshest members of <a href="http://websvn.kde.org/trunk/KDE/kdeutils/?pathrev=837673">kdeutils</a>, which entered it in time for KDE 4.1, are Okteta and the Printer Applet. The latter also makes an interesting premiere, being the first program in the mainline KDE modules which is not written in C/C++, but in Python.
</p>

<ul>
<li><a href="http://utils.kde.org/projects/ark">Ark</a> - Archiving tool</li>
<li><a href="http://utils.kde.org/projects/kcalc">KCalc</a> - Scientific calculator</li>
<li><a href="http://utils.kde.org/projects/kcharselect">KCharSelect</a> - Font character selector</li>
<li><a href="http://utils.kde.org/projects/kdessh">kdessh</a> - Front end to ssh</li>
<li><a href="http://utils.kde.org/projects/kdf">KDiskFree</a> - Free disk space viewer</li>
<li><a href="http://utils.kde.org/projects/kfloppy">KFloppy</a> - Floppy disk formatter</li>
<li><a href="http://utils.kde.org/projects/kgpg">KGpg</a> - GPG frontend</li>
<li><a href="http://utils.kde.org/projects/ktimer">KTimer</a> - Time controlled program starter</li>
<li><a href="http://utils.kde.org/projects/kwalletmanager">KDE Wallet Manager</a> - KDE wallet management tool</li>
<li><a href="http://utils.kde.org/projects/okteta">Okteta</a> - Hex editor</li>
<li><a href="http://utils.kde.org/projects/printer-applet">Printer Applet</a> - Applet to view current print jobs and configure new printers</li>
<li><a href="http://utils.kde.org/projects/superkaramba">SuperKaramba</a> - Version of Karamba with python scripting</li>
<li><a href="http://utils.kde.org/projects/sweeper">Sweeper</a> - Cleaner for unwanted usage traces in the system</li>
</ul>

<p>
Each program has a description page, a page with an overview of the program's development (like activity or progress with translations) and a contact page. Over time more content will be added. If you are interested to help out with that, please get in <a href="http://utils.kde.org/contact.php">contact</a> with us.
</p>

<p>If you have been developing a utility based on the KDE platform outside of the KDE repository and you think it would be a good addition to this module, do not hesitate to <a href="http://utils.kde.org/contact.php">contact</a> us.  If you, contributor or user, are interested in some of these programs, go and find all information at <a href="http://utils.kde.org">http://utils.kde.org/</a>.
Finally: Thanks a lot to our KDE sysadmins David, Dirk and Tom, who did all the work in the background to get the site running.
</p>




