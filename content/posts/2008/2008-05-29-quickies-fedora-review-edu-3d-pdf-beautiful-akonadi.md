---
title: "Quickies: Fedora Review, Edu, 3D PDF, Beautiful Akonadi"
date:    2008-05-29
authors:
  - "jriddell"
slug:    quickies-fedora-review-edu-3d-pdf-beautiful-akonadi
comments:
  - subject: "Akonadi"
    date: 2008-05-29
    body: "Great work on the Akonadi logo. "
    author: "KDE fan"
  - subject: "Re: Akonadi"
    date: 2008-05-29
    body: "Agree. It's gorgeous!"
    author: "tryfan"
  - subject: "Re: Akonadi"
    date: 2008-05-30
    body: "Agree. Well done."
    author: "Ioannis Gyftos"
  - subject: "logo"
    date: 2008-05-29
    body: "Great job on the logo it looks really good and very professional just what the doctor ordered for linux."
    author: "Darwin Reynoso"
  - subject: "kde"
    date: 2008-05-29
    body: "If kde 4.0.4 is very slow and unstable (this was my unfortunate experience), than why would kde 4.1 be any better ? Ok you fix a bunch of bugs, but you also add a lot of new functionality.."
    author: "Daniel"
  - subject: "Re: kde"
    date: 2008-05-30
    body: "learn to think\n\nthe functionality is added much more in non-critical components\n\nso your comparison is error-prone!"
    author: "mark"
  - subject: "Re: kde"
    date: 2008-05-30
    body: "Learn to think?\n\nI am repeatedly disappointed by personal attacks on the dot.\n\nA more considerate approach might be to inquire what problems the poster had, and perhaps point them to useful information.  Many people had issues with composite performance in KDE 4.  The blame either lies with QT, KDE or drivers depending on who you ask.  The odd thing is that Intel, Nvidia, and ATI users can all use composite effects via Compiz without problems, so I'm not sure the issue lies with video drivers.\n\nI could be mistaken."
    author: "T. J. Brumfield"
  - subject: "Re: kde"
    date: 2008-05-30
    body: "\"Learn to think?\n \n I am repeatedly disappointed by personal attacks on the dot.\"\n\nThe statement was directed at the poster's lack of logic/ knowledge: SVN trunk not only contains bug-fixes and features, but also *optimisations*.  \"Learn to think\" may have been undiplomatic, but it remains the case that the poster's logic had a critical flaw that anyone in the know would have picked up on instantly."
    author: "Anon"
  - subject: "Re: kde"
    date: 2008-05-30
    body: "\"I am repeatedly disappointed by personal attacks on the dot.\"\n\nWhat you consider \"personal attacks\" is not necessarily that. You accused me of a \"personal attack\", when I called your complaining in the other discussion \"whining\". I don't consider that to be a \"personal attack\", any more than calling your complaining as \"complaining\" would be.\n\nYes, I think the comment about \"learning to think\" was called for. The person basically said that since he has problems with 4.0.4, 4.1 is goiung to suck as well. There is exactly zero logic behind that statement, and calling the person out for that lack of logic is not a \"personal attack\"."
    author: "Janne"
  - subject: "Re: kde"
    date: 2008-05-30
    body: ">> I am repeatedly disappointed by personal attacks on the dot.\n\nThat is quite clearly your problem and not anyone else's. \n\n>> A more considerate approach might be to inquire what problems the poster had\n\nIf a poster actually raises a specific, valid point, then yes.  If they just complain generally in a negative manner, then it's not worth anyone's effort.\n\n>>I could be mistaken.\n\nIndeed."
    author: "Leo S"
  - subject: "Re: kde"
    date: 2008-05-31
    body: "You're both suggesting that personal attacks don't happen here, and that the poster had no point.\n\nSo I guess absolutely no one had issues with the KDE 4 release."
    author: "T. J. Brumfield"
  - subject: "Re: kde"
    date: 2008-05-31
    body: "Not to speak for anyone else, but it seems to me they were suggesting that your definition of a \"personal attack\" differs from theirs.  Also, with regard to the poster you refer to, it seems his only point was to be negative and critical.  He made no specific complaint for anyone to respond to, therefore there was nothing constructive about it.  If the obvious intent of a poster is to be critical, that poster shouldn't get all defensive when he/she is called out for it."
    author: "cirehawk"
  - subject: "Re: kde"
    date: 2008-06-01
    body: "\"Learn to think\" is analagous to calling someone an idiot.  That is a far cry from saying \"please be more specific so we can help you with your problem\".\n\nCalling someone an idiot is a personal attack, plain and simple.\n\nEven if the poster wasn't specific, the poster wasn't alone in encountering bugs, plasma crashes, slowdowns, etc.  Some people don't feel comfortable debugging issues.\n\nDaniel stated that \"it was unfortunate experience\".  That hardly sounds like a troll.  It came across to me like someone concerned, raising a question of whether or not he would have better luck with 4.1.\n\nOne could respond with the staggering number of bugs closed since the 4.0 launch, or one can respond calling the poster an idiot.  I can't understand defending the latter."
    author: "T. J. Brumfield"
  - subject: "Re: kde"
    date: 2008-06-01
    body: "actually we are on an international site, where the way people word what they type has different meanings.  \n\nTo me, his statement telling him learn to think was not calling him an idiot, that was you who put that meaning into it.  Instead he was trying to get across the point that progression brings improvement.  However you have again T. J. decided to carry something on far past it's used by date.  Would of been over within a few posts but you have to keep it going like a troll"
    author: "anon"
  - subject: "Re: kde"
    date: 2008-06-01
    body: "\"actually we are on an international site, where the way people word what they type has different meanings. \"\n\nI am French and I would take it the same way as he did. Telling someone to learn to think is akin to say he's retarded and needs to be fixed.\n\nI am curious to know in what country someone would take it well to hear from the mouth of a stranger that he needs to learn to think. "
    author: "Anon"
  - subject: "Re: kde"
    date: 2008-06-03
    body: "You're right. Maybe, \"Engage brain before fingers\" is more appropriate."
    author: "Madman"
  - subject: "Re: kde"
    date: 2008-05-30
    body: "KDE 4.1 is way more mature than KDE 4.0.4. I've been running it (4.1 alpha1) since release day, a few crashes while I've configured plasma, but no more crashes since the latest updates. Everything looks stable. The Panel is finally configurable and kwin doesn't crash all the time when compositing is enabled... Oh, and konsole is transparent now - YES!\n\nLet's look at kde4.1/kwin: It's way more stable, compositing is way more reliable, but many new effects were added (e.g. wobby windows). 4.0 was the first public iteration of the core + a basic desktop. 4.0.X were mainly bugfixes. 4.1 is the next iteration of the core plus more addons (too simplified, but you'll get the point). So adding features and fixing core bugs doesn't harm each other.... That's what the attacker meant (although I dislike the posters style).\n\nHope this helps."
    author: "Treffer"
  - subject: "Re: kde"
    date: 2008-05-30
    body: "thankfully we've also worked on stability and performance.\n\none can work on multiple dimensions of software at once, and this community is just amazingly dynamic. so not only do we get more features, we also get more of the other nice things too.\n\nthis is just like in kde3 when we kept getting new features and more speed in each release =)"
    author: "Aaron Seigo"
  - subject: "Re: kde"
    date: 2008-05-31
    body: "I found KDE 4 to be amazingly slow on my computer when it first came out.  I think I even commented to Aaron about this on dot, asking if it would be just as fast on an older computer.  \n\nMy experience has been that with each release KDE 4 has blown away it's competition.\n\nThanks to all who work their butts off and get more complaints than compliments some times.  I appreciate being able to use KDE"
    author: "R. J."
  - subject: "Re: kde"
    date: 2008-05-31
    body: "Yeah, it's just like that KDE3 and its apps that kept on giving the SIGSEGV goodness until the KDE 3.3 release. And even then, I found plenty of ways to crash the apps in 3.4 and 3.5, much more ways than I'd ever see from Gnome.\n\nWhat KDE would be without some SIGSEGV goodness from time to time. "
    author: "Anon"
  - subject: "Re: kde"
    date: 2008-05-31
    body: "Then thankfully Gnome exists for you to use, wouldn't you say?"
    author: "cirehawk"
  - subject: "Re: kde"
    date: 2008-06-03
    body: "I've had that bug at the most 3 times. Please, tell me what you're doing wrong - I've been trying to deliberately crash the whole operating system for ages, even using a virtual machine with Windows XP installed while multi-tasking in KDE.\n\nOf course, you could always use Vista, with it's high incompatibility rate, high cost, low security and lack of freedom to do whatever the hell you want with it. Somehow, I think that's the kind of thing you'd like if you enjoy crashes so much."
    author: "Madman"
  - subject: "Planete Beranger"
    date: 2008-05-30
    body: "Doesn\u0092t anyone else find it strange that a programmer such as this Beranger guy attacks Free Software projects left and right (GNOME, KDE, and XFCE - calling them all crap, insulting the developers by calling them morons, idiots, etc)?\n\nI also find it strange that he has not contributed to ANY Free Software project, yet claims to have been involved with Free Software since 1994, especially since he\u0092s a software developer.\n\nBut then you look at his resume\u0085 and we find what? \u0093Windows Developer\u0094\n\nNow the picture becomes clear: he insults Free Software projects and developers because Free Software is a threat to his job. He\u0092s a Microsoft shill if there ever was one.\n"
    author: "Dan O'Brian"
  - subject: "Re: Planete Beranger"
    date: 2008-05-30
    body: "I dunno, I quite like him - he is very outspoken and abrasive but I think you have to realise he has a bit of a sense of humour :-)\n\nBtw, how's Beranger relevant to this article?"
    author: "Simon"
  - subject: "Re: Planete Beranger"
    date: 2008-05-30
    body: "Don't let Beranger bother you, no one cares what he says anyway. Let him go on an ego trip if that's what makes him happy."
    author: "Aaron"
  - subject: "Fear Opensuse?"
    date: 2008-05-31
    body: "It seems people doing reviews fear OpenSUSE.  They always compare recent releases of K and Ubuntu with old releases of OpenSUSE.  Leads to very boring reading."
    author: "anon"
  - subject: "Re: Fear Opensuse?"
    date: 2008-05-31
    body: "The release candidate for openSUSE 11 didn't come out until yesterday I do believe, and openSUSE beta 3 had some serious show-stopper bugs from what I read.\n\nI've heard the new openSUSE 11 installer is a huge improvement, but I guess we'll discover that when it is released."
    author: "T. J. Brumfield"
  - subject: "Re: Fear Opensuse?"
    date: 2008-05-31
    body: "I'm using 11.0 since Beta 2 on a production system and had no problems, apart from a kernel update after which the files in /boot were not updated properly. That didn't really pose a problem, because I just reinstalled in <20 minutes and got the system back to normal in another 10 minutes."
    author: "Stefan Majewsky"
  - subject: "Re: Fear Opensuse?"
    date: 2008-05-31
    body: "I agree with you, I've been using 11 since the alpha's on a production computer, and have had no problems with it.  \n\nAnd too my knowledge there were no showstoppers, and RC 1 one came out on schedule."
    author: "anon"
  - subject: "Re: Fear Opensuse?"
    date: 2008-06-01
    body: "I didn't say RC 1 wasn't on schedule.  I believe it was set to be released on the 29th, but I couldn't download it until yesterday.  1 day to propogate to a mirror is quite acceptable to me.  I have been following the releases, and checking on the bugs.\n\nhttp://en.opensuse.org/Bugs:Most_Annoying_Bugs_11.0_dev\n\nMind you, this isn't a complete list of bugs (they say something like 500 bugs were closed between Beta 3 and RC1) but the most annoying.\n\nGeneral \nafter installation from LiveCD, it is imperative that you delete the file /.buildenv , otherwise your system won't boot after you upgrade the kernel because of a missing initrd file. See Bug #389177 and Bug #392054. [NB: If you happen not to be able to boot because of this, reboot with the livecd, open a console, mount your root partition (say to /mnt/root) (and the boot partition to /mnt/root/boot if necessary), then do \"mount /sys --bind /mnt/root/sys\", chroot to /mnt/root, delete .buildenv and run mkinitrd to create the missing initrd file.] \nNVIDIA driver doesn't compile. Workaround: check here for a patch. No patching required if 173.08 Beta drivers are used. \nparallel driver grabs IRQ14 preventing legacy SFF ATA controller from working (Bug #375836) \nT41p shutting down due to \"temperature critical\" (Bug #378327) \nBranding not yet complete (Bug #369270) \nPackage selector doesn't show any patterns (Bug #390139) \nSMB based (network) printer cannot be added (Bug #386934) \nnetwork printing via lpd fails (Bug #391848) \nUpgrade process leaves the system with wrong permissions on some files. Run `SuSEconfig` to fix them(Bug #390930) \n[edit]GNOME \nYaST control center does not start under GNOME (Bug #389069) \nNo window decoration, workaround run compiz-manager & or metacity --replace& (Bug #387168) \n[edit]KDE \nkdm looses keyboard when detecting an unknown previous session type (Bug #389098) \nKontact starts with sidebar splitter on right window border (Bug #389141), work-around: move it to left to see components \nopensuseupdater-kde unable to start 'yast2 piwo' (Bug #389765) \n(K)NetworkManager unable to autoidentify the right wpa (Bug #391759) work-around: choose Expert settings and specify manually the WPA version and the specific chiper. If you don't know which use be root and check with: iwlist dev-name scan (i.e. iwlist wlan0 scan) "
    author: "T. J. Brumfield"
  - subject: "Re: Fear Opensuse?"
    date: 2008-06-01
    body: "most annoying bugs, doesn't mean they are show stoppers.  Please provide the list of show stoppers\n\nDownload what?  If you are using the beta's you just upgrade using the factory repositories.  I guess you haven't been using it, or don't really know what you are doing."
    author: "anon"
  - subject: "Re: Fear Opensuse?"
    date: 2008-06-02
    body: "I use Gentoo currently, but am looking for a distro for my work laptop (plan to dual-boot and show off Linux as I had been trying to convince the rest of the IT department to consider it as part of our new desktop rollout, at least to replace our old Mac users), as well as my mother's new computer and was considering openSUSE.  My laptop needs the proprietary Nvidia drivers, so not being able to put those in the kernel is a bit of a show stopper.  Not being able to boot after an update is a show stopper.  Grub having problems with different partitions is a show stopper. Etc, etc. etc.\n\nI've heard very good things about openSUSE's KDE packages specifically, and the few times I've tried it, I've been fairly happy.\n\nI downloaded the install KDE CD, and it wouldn't boot on two different computers I tried, and so I was trying to download the DVD, but couldn't find anyone seeding it for a day.\n\nGiven that I was following the prescribed instructions on openSUSE's website, clearly I don't know what I was doing."
    author: "T. J. Brumfield"
  - subject: "Re: Fear Opensuse?"
    date: 2008-06-02
    body: "http://ftp5.gwdg.de/pub/opensuse/distribution/11.0-RC1/iso/dvd/\n\nYou can download the dvd direct"
    author: "fnord"
  - subject: "Re: Fear Opensuse?"
    date: 2008-06-02
    body: "depends on when you downloaded the opensuse KDE Live CD.  Beta 1 had a problem with the live CD.  The single CD is a live CD, not an install cd, it boots into the live desktop and you install it from there.  But again, I have seen no mention of people having problems with loading into the live desktop since beta one.  Perhaps you can supply the link to the bug report you filed over this?\n\nMy computer has Nvidia drivers, and I have no problem with it, and the Nvidia drivers for OpenSUSE 11 are not released during the alpha, beta, or RC, they are released by Nvidia when 11 is final.\n\nI have no problem rebooting after an update.  But like I said, can you provide links that show these as show stoppers, if not they are just bugs which will be marked and fixed, not show stoppers.  Actually reading the news lists there is no mention by any of the developers of showstoppers.  "
    author: "anon"
  - subject: "Re: Fear Opensuse?"
    date: 2008-06-02
    body: "It looks like the RC1 CD is supposed to be a Live CD as well.  However, on a brand new desktop, and on an older desktop, it would mount the CD, load the kernel, and then try to mount the compressed image, and hang there.  2 minutes later, a reboot.\n\nI verified the MD5, and burned the disc twice.  I used rewritable discs, but I doubt that was an issue."
    author: "T. J. Brumfield"
---
Red Hat Magazine has <a href="http://www.redhatmagazine.com/2008/05/14/fedora-9-and-the-road-to-kde4">a review of KDE 4 on the new Fedora 9</a>. *** Linux Journal <a href="http://www.linuxjournal.com/article/9950">takes a look at Marble</a> which recently <a href="http://kdedevelopers.org/node/3475">gained OpenStreetMap</a> support. *** The Fanatic Attack blog features an article on <a href="http://www.fanaticattack.com/2008/exceptional-linux-programs-for-kids.html">exceptional Linux programs for kids</a> covering a good number of our own <a href="http://edu.kde.org/">KDE Education</a> apps. ***  Another project's loss means we gained one <a href="http://code.google.com/soc/2008/kde/appinfo.html?csaid=56A080529F30BE85">extra summer of code project</a> implementing the 3D part of the PDF specification for Okular. *** The Register <a href="http://www.theregister.co.uk/2008/05/29/kde_4point1_beta_1_released/">takes a look at 4.1 Beta 1</a>. *** SoftVision Blog <a href="http://softvision.wordpress.com/2008/05/18/early-days-with-kde-4-opensuse-103-kubuntu-804-and-fedora-9-compared/">reviews KDE 4 distros</a>, the all new Kubuntu and Fedora releases plus an older openSUSE. *** Akonadi gained a beautiful new logo, thanks to Nuno and his excellent Oxygen team.





<!--break-->
<p><img src="http://static.kdenews.org/jr/akonadi-logo.png" width="400" height="246" /><br />
New Akonadi logo by Nuno and Thomas</p>


