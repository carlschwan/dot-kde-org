---
title: "Akademy 2008 Registration Open"
date:    2008-05-30
authors:
  - "kduffus"
slug:    akademy-2008-registration-open
comments:
  - subject: "First one"
    date: 2008-05-30
    body: "See you all at Akademy!"
    author: "Stefan Majewsky"
  - subject: "Re: Frist one"
    date: 2008-05-30
    body: "Oh, I cannot wait! Code and belgian beer!"
    author: "Jos"
  - subject: "Will you talk about Flash/Silverlight killer?"
    date: 2008-05-30
    body: "Will you talk about Flash/Silverlight killer? Because it is awesome -> http://grundleborg.wordpress.com/2008/05/30/plasmoids-in-a-web-page/\n\nThis one really really rocks. HTML5 + svg plasmoids with script support + phonon media streaming -> endless possibilities, robust web :D :D :D I really like this idea. KDE for the win!"
    author: "Dog"
  - subject: "Re: Will you talk about Flash/Silverlight killer?"
    date: 2008-05-30
    body: "The problem is that a standalone browser plugin for that would be quite heavy. Image Qt, kdelibs, and libplasma packaged into one runtime for Windos users."
    author: "Stefan Majewsky"
  - subject: "Re: Will you talk about Flash/Silverlight killer?"
    date: 2008-05-31
    body: "silverlight is pretty huge too."
    author: "Patcito"
  - subject: "Re: Will you talk about Flash/Silverlight killer?"
    date: 2008-05-31
    body: "But people won't notice its size. Windows Update will just install it for most people, as these are not checking the detailed update list. In contrast, open source software is devoted to transparency."
    author: "Stefan Majewsky"
  - subject: "Re: Will you talk about Flash/Silverlight killer?"
    date: 2008-05-31
    body: "The Silverlight web page lists the download as 1MB, which I guess is possible if .NET is already installed."
    author: "Robert Knight"
  - subject: "Re: Will you talk about Flash/Silverlight killer?"
    date: 2008-06-01
    body: "MS tend to provide net installer, meaning it will download a lot of stuff when you execute it, which makes difficult to evaluate the size."
    author: "Cyrille Berger"
  - subject: "[OT] Will you talk about Flash/Silverlight killer?"
    date: 2008-05-31
    body: "How heavy is heavy? Any numbers? And for those with internet flatrates and close to TB-sized harddisks, what is the only interesting number in time to download and install?"
    author: "Hans"
  - subject: "Re: [OT] Will you talk about Flash/Silverlight killer?"
    date: 2008-05-31
    body: "> How heavy is heavy? Any numbers?\n\nSome very rough estimates based on my KDE4/Release build.  With the following components:\n\n- Qt Core+QUI+Network+Scripting+DBus  [ ~15.4MB ]\n- KDE Core+UI+Plasma+Phonon           [ ~11MB   ]\n- Video components (eg. Xine + selected plugins) [ ~1MB ]\n\nTotals 27MB uncompressed.  The largest library of those, QtGui compresses down from 9.1MB to 4.0MB with gzip.  If we were to assume the same compression ratio for the whole lot, that goes down to 12MB.\n\nThese estimates do not include some libraries found on most Linux desktops (X11, image loading, sound, fonts).  These estimates also do not include any scripting bindings for the various libraries.  Using the current Qt script binding generator they would easily double the size of the output.   If you were to create custom builds of all the components which stripped out many of the classes that might not be needed for a \"Plasma web plugin\" then you could probably reduce that somewhat.\n\nFor comparison, the Flash 10 beta is about 3.7MB for Linux and 1.7MB for Windows.\n\n"
    author: "Robert Knight"
  - subject: "Re: [OT] Will you talk about Flash/Silverlight killer?"
    date: 2008-05-31
    body: "Seems like ~15Mb could still be acceptable with modern broadband connections. And bandwidth of course is going to increase further.\n\nPersonally I'd be OK with a download of this size.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Will you talk about Flash/Silverlight killer?"
    date: 2008-06-01
    body: "I agree, awesome idea. Imagine sth. like this: You go to dot.kde.org and drag the news headlines to your desktop. This would completely dissolve any boundaries between the net and your desktop. I can even imagine more \"crazy\" stuff like: You have a sticky-note plasmoid on your desktop containing to-do notes. You can drag it back on a specially prepared webpage like \"plasmoid-whiteboard.yourcompany.com\" and everyone can see it. If a colleague needs your todo-list as well, s/he can drag it onto his/her desktop. The two notes are automatically kept in sync via Jabber or sth. like that. Imagine this also with parts of your clipboard with soure code snippets and so on. Amazingly groundbreaking stuff possible, now that I come to think about this. And very intuitive from an end-users POV."
    author: "Michael"
  - subject: "OT: JavaScript Engines"
    date: 2008-05-31
    body: "Is it true that a current kde trunk build has 3 (more or less similar) JavaScript engines included in it's libs, like:\n\n- One in khtml\n- One in QtScript\n- One in QtWebKit\n\n?"
    author: "Yves"
  - subject: "Re: OT: JavaScript Engines"
    date: 2008-05-31
    body: "Almost. The QtScript engine isn't really related to the other 2 code-wise[1]. But yeah, there are 3, and 2 of them are related (though have a very different execution code at this stage --- the JS interpreter in QtScript is more similar to 4.0 alpha 1 than to 4.1).\n\n[1] Though it's a very indirect descendant of QSA --- I don't know whether there is any code still left --- and AFAIK, QSA was originally developed by KJS's original author, Harri Porten.\n\n"
    author: "SadEagle"
  - subject: "Re: OT: JavaScript Engines"
    date: 2008-05-31
    body: "err, \" JS interpreter in QtScript\" should have been \" JS interpreter in QtWebKit\"."
    author: "SadEagle"
  - subject: "Toll in Belgium"
    date: 2008-05-31
    body: "I'll be travelling by car. Is there a toll on Belgian roads (esp. on expressways)? It would be nice to include this information on the page, even if there is no toll, as the various sources on the Internet are contradictory."
    author: "Stefan Majewsky"
  - subject: "Re: Toll in Belgium"
    date: 2008-06-01
    body: "There are almost no toll roads in Belgium. In fact, I know just one (Antwerp's Liefkenshoek Tunnel)."
    author: "Andy Goossens"
  - subject: "Re: Toll in Belgium"
    date: 2008-06-01
    body: "If there was, people would basicly ride around belgium...\nIt's not that big :p"
    author: "Mark Hannessen"
  - subject: "Re: Toll in Belgium"
    date: 2008-06-01
    body: "They could make an agreement with Luxemburg and the Netherlands to introduce a toll for freight vehicles only like here in Germany. Brings in lots of money and doesn't make the voters too angry. Especially considering the fact that there is probably an even better ratio of local to foreign freight vehicles there than here in Germany. Considering a diesel price of &#8364; 1,50 currently nobody would like to drive around all three countries I suppose.\n"
    author: "Michael"
  - subject: "Re: Toll in Belgium"
    date: 2008-06-01
    body: "Huh? You can't type a \"&#8364;\" anymore on the dot? Ugh. Please somebody fix that if possible. Thanks!"
    author: "Michael"
  - subject: "Attendees!"
    date: 2008-06-01
    body: "Please update this table if you wish http://techbase.kde.org/Events/Akademy/2008/Attendees"
    author: "j"
---
<a href="http://akademy2008.kde.org/">Akademy 2008</a> is now <a href="https://akademy2008.kde.org/registration/user/register">open for registration</a>.  Akademy is KDE's World Summit, a week long event for all KDE contributors, industry partners and users.  The week starts with a two day conference, and is set to include a tutorial day and a embedded and mobile day.  As always, attendance to Akademy is free of charge, but you must register. Registration should be in by the 15th of June if you want the Akademy Team to book your <a href="http://akademy2008.kde.org/accommodation.php">accommodation</a> for you.  See you in Belgium!
			



<!--break-->
