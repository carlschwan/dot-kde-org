---
title: "Second Alpha Release of Amarok 2.0, Codenamed \"Aulanerk\""
date:    2008-07-27
authors:
  - "lpintscher"
slug:    second-alpha-release-amarok-20-codenamed-aulanerk
comments:
  - subject: "Awesome!"
    date: 2008-07-27
    body: "It's great! I'm already loving it!\n\n\nI find the vertical tabs on the left rather ugly..."
    author: "Dread Knight"
  - subject: "Re: Awesome!"
    date: 2008-07-27
    body: "Yes, Amarok does look fantastic, but I would also like to see something that replaces the vertical tabs.\n\nAnyone from the Oxygen team in da house?"
    author: "Jan Ask"
  - subject: "Re: Awesome!"
    date: 2008-07-27
    body: "Last I heard the art they had for the vertical tabs was still meant as a placeholder for when they get some art meant for it. Not sure if anythings changed since I last heard about that, though."
    author: "Kit"
  - subject: "Re: Awesome!"
    date: 2008-07-27
    body: "That is exactly why it's that ugly :) That way we don't forget ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Awesome!"
    date: 2008-07-27
    body: "Can you simply ge rid of vertical tabs and vertical texts? They are impossible to read, give a cluttered sensation and, to sum it up, are the only negative part of the Amarok experience (which is, a part from this, fantastic)"
    author: "Vide"
  - subject: "Re: Awesome!"
    date: 2008-07-27
    body: "And how would be a better method to access that content?"
    author: "Kit"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "The iTunes way to do it, only one big treeView. Of course, in Amarok we have a second level of horizontal tabs inside of the first level (services, podcast and things like that). But i think that a single treeView would do the job better. Now, I am ok with the current system, but i remember when i first used Amarok (i was on XMMS before and iTunes (4.*) in wine for my iPod), i didn't liked the left tabs stuff but not at all. It was taking so many click to access playlist.\n\nI think that there should be not more than 2 of those tabs. The default one should be the treeview with Playlists, podcast, services, devices and \"see all media\" and the second should be the collection, with local, remote and device in different tabs.\n\nIt sound like iTunes/RhytmBox/Other player, but it is still more usage than always having to click 5-7 time to access a basic feature."
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: Awesome!"
    date: 2008-07-30
    body: "Holy vertical space batman!\n\nWe already tried this sort of thing before, it was really ugly (even worse then our current tabs!) and left little space for the actual browsers due to all the tabs.\n\nThe iTunes model only works because they don't have a playlist per-se, but use the playlist-space to show the collection browser normally. But then people can also make their own playlists somehow. Not sure. Anywho: the point is that iTunes tree view isn't an isolated feature, and adopting it means making Amarok cramped and ugly or becoming a complete iTunes clone."
    author: "Ian Monroe"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "\"And how would be a better method to access that content?\"\n\nAt least provide the [b]option[/b] to use something else than vertical tabs... horizontal tabs, text, ANYTHING but vertical tabs... my neck isn't made to turn 90% (ok, a *little* less) and I'm not used to reading text that's rotated 90 degrees. At least, if you keep the tabs, make the text readable from top to bottom:\n\nR\nE\nA\nD\nA\nB\nL\nE\n\nT\nE\nX\nT\n\nYes yes I know you loose a lot of space this way. It's still better than text that's near impossible to read or icons that you can't tell what they do.\n"
    author: "Darkelve"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "In my opinion, vertical tabs are not a problem.\n\nPersonnally, i don't have any problem to read and to know what they are doing. It's better than Horyzontaly tabs, it takes less area...\nAnd reading from top to bottom is more annoying than 90\u00b0 oriented text.\n\nBut the idea to add an option to configure these tabs, for people who prefer 90\u00b0 oriented text or top to bottom text, is really good. "
    author: "Insomniak"
  - subject: "Just use KDE's vertical tabs, please!"
    date: 2008-07-28
    body: "There exist vertical tabs in many apps like Kate, KDevelop, Konqui... But Amarok uses its very own implementation since the early 1.x releases. The same about the curious selection bar in the playlist they were using. I believe it would already help if they just leave it to the Oxygen (or another) style to draw the tabs... "
    author: "Sebastian"
  - subject: "Re: Just use KDE's vertical tabs, please!"
    date: 2008-07-29
    body: "Our implementation is superior to the one in KDElibs, as it uses the whole vertical available space, which makes it much easier to use the tabs with muscle memory.\n\nWe've in fact been asked to put this code into KDElibs, but due to time restrictions this hasn't happened yet.\n\nAs for the _look_ of the tabs, I can assure you that we're working on improving it muchly. In fact it looks already much better in trunk.\n\nOne word, as a hint: Nuno :)\n"
    author: "Mark Kretschmann"
  - subject: "Awful idea!"
    date: 2008-07-29
    body: "Putting the letters\na\nb\no\nv\ne\neach others would make the interface totally unreadable."
    author: "tryfan"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "What about good old QToolBox (http://doc.trolltech.com/qq/qq07-toolboxes.png)\nOf course something less ugly, but this will keep tab labels horizontal, and wont take up so much space\n"
    author: "greg"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "Oh, nice idea ! \n\nBetter than adding option to configure the tabs, and more pratical."
    author: "Insomniak"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "IMO it's the best solution, at least if we follow the \"tabs\" paradigm, I even did a mockup and posted it on amarok-devel but it was rejected saying that buttons may grow in number and then occupy more real estate. I don't think so, but I'm not a developer so I can't really blame them :)"
    author: "Vide"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "something similar, and I think it is good, take a look at right part of the site http://www.cuil.com/search?q=amarok if this efect would be included in amarok it would be great"
    author: "ireko"
  - subject: "Re: Awesome!"
    date: 2008-07-28
    body: "I don't see what you're referring to.."
    author: "bsander"
  - subject: "Re: Awesome!"
    date: 2008-07-29
    body: "\"explore by category\" box"
    author: "ireko"
  - subject: "Re: Awesome!"
    date: 2008-07-30
    body: "Thats pretty much qtoolbox, fyi. (just minus the ugly)\n\nWe are planning on having something like that for the playlist browser (which uses qtoolbox currently)."
    author: "Ian Monroe"
  - subject: "Re: Awesome!"
    date: 2008-07-30
    body: "Heh yea we did this before exactly. It was horrible. ;) \n\nHere is my documented proof:\nhttp://amarok.kde.org/blog/archives/34-New-sidebar-for-amaroK.html\nAnd users saying they don't like it:\nhttp://amarok.kde.org/forum/index.php/topic,5918.0.html\n\nUnfortunately no screenshots... they were probably censored for the children."
    author: "Ian Monroe"
  - subject: "Release Schedule?"
    date: 2008-07-27
    body: "Hi \n\nIs there a release schedule for 2.0 ? Otherwise, anyone knows when are they shooting to release 2.0 final? Thanks!"
    author: "KubuntuUser"
  - subject: "Re: Release Schedule?"
    date: 2008-07-27
    body: "Final release should be Octoberish, last I heard."
    author: "Jonathan Thomas"
  - subject: "Re: Release Schedule?"
    date: 2008-07-27
    body: "Thanks, Jonathan!\n\nHope we see it in Kubuntu 8.10!"
    author: "KubuntuUser"
  - subject: "Re: Release Schedule?"
    date: 2008-07-28
    body: "We hope so too :) We're working at a feverish pace to make this possible, but there are no guarantees.\n\nIn any case, it will be available for download.\n"
    author: "Mark Kretschmann"
  - subject: "Amarok 1.4 import??"
    date: 2008-07-29
    body: "Is there already a way to import all the \"old\" Amarok 1.4 database info (playcounts, ratings etc.) into the new 2.0 Amarok?\nThis keeps my from switching..."
    author: "Anon"
  - subject: "Re: Amarok 1.4 import??"
    date: 2008-07-29
    body: "Yes, Seb Ruiz has been working on a converter script.\n\nI'm not sure if the script is already usable at this point, but it will definitely be doable in the final release.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok 1.4 import??"
    date: 2008-07-29
    body: "Argh... but I want it NOW ;)))!\n\nAnyway - is the database structure so much different that a converter script is so difficult? I, too, am still kept off from trying Amarok2 because that would mean trashing all my album covers, playcounts, ratings and so on... would be great if a converter could be added before the final version is out!"
    author: "Anders"
  - subject: "Re: Amarok 1.4 import??"
    date: 2008-07-30
    body: "People who are worried about such things should just as well stay away from Amarok for the time being. :)"
    author: "Ian Monroe"
  - subject: "radio tab"
    date: 2008-08-03
    body: "Amarok is nice but it also needs simplicity. The way the integration of radio streams is organised does not look very good. It does not make sense to have lists in the tab window."
    author: "ben"
---
The <a href="http://amarok.kde.org/">Amarok</a> team is proud to present the second Alpha of Amarok 2.0. Development is moving at full speed and a lot of bugs have been fixed since <a href="http://dot.kde.org/1215706226/">Alpha 1</a>, as well as features polished. Thank you to everyone who has already helped by filing bug reports and sending patches. Please keep them coming! Read the <a href="http://amarok.kde.org/en/node/529">release announcement</a> for a list of bugfixes and changes. Get Alpha 2 today and help make Amarok 2.0 rock.


<!--break-->
