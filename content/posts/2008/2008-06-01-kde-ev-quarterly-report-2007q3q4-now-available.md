---
title: "KDE e.V. Quarterly Report 2007Q3/Q4 Now Available"
date:    2008-06-01
authors:
  - "dallen"
slug:    kde-ev-quarterly-report-2007q3q4-now-available
---
The <a href="http://ev.kde.org/">KDE e.V.</a> Quarterly Report is <a href="http://ev.kde.org/reports/ev-quarterly-2007Q3-Q4.pdf">now available for Q3 and Q4 2007</a>, covering July to September, and October to December 2007. This document includes reports of the board and the working groups about the KDE e.V. activities of the last two quarters of 2007, as well as event summaries and future plans. All long term KDE contributors are welcome to <a href="http://ev.kde.org/getinvolved/members.php">join the KDE e.V.</a>

<!--break-->
