---
title: "Plasma Themes Contest"
date:    2008-03-21
authors:
  - "alake"
slug:    plasma-themes-contest
comments:
  - subject: "theme"
    date: 2008-03-21
    body: "this theme contest is a great idea.\ni'd love to see a theme that looks like that: http://www.portefolje.net/div/mockup.jpg"
    author: "moss"
  - subject: "Re: theme"
    date: 2008-03-21
    body: "This is not a window style theme contest, but Plasma theme contest (desktop widgets, panel...)"
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: theme"
    date: 2008-03-21
    body: "yes, but there is also a panel and a notifier in this mock-up. that is what i was talking about."
    author: "moss"
  - subject: "Re: theme"
    date: 2008-03-21
    body: "Right, I just wanted to clarify the potential misunderstanding of the announcement :)"
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: theme"
    date: 2008-03-23
    body: "I would love a desktop with that look, amazing mockup.\n(yes, I know plasma can't and won't do the whole desktop theming, just giving my impression that this ubuntu mockup would be awesome)"
    author: "Iuri Fiedoruk"
  - subject: "Re: theme"
    date: 2008-04-03
    body: "You could create a single theme for the desktop and windows. You'd just have to distribute them separately."
    author: "yman"
  - subject: "Colors"
    date: 2008-03-21
    body: "Hi\nI think it 'is' necessary for all themes to match their colors with system colors.its really bad that all my applications are looking almost white, but the plasma panel and desktop are almost black.\nAya plasma theme does this, but i think there should be something more general so all themes get this automatically, or at least an option to 'match with color themes'."
    author: "Emil Sedgh"
  - subject: "Re: Colors"
    date: 2008-03-21
    body: "\"Match with color themes\" as an option seems better to me (don't know if and how this is possible). I like to have a dark panel/widget theme which fit well with a dark background and to have more bright windows color scheme\n\nThis contest is a great idea btw :)"
    author: "DanaKil"
  - subject: "Re: Colors"
    date: 2008-03-21
    body: "One advantage to the way plasma's doing it now, is that the colour scheme is hinted. This lets the artist control how the colour is applied. Amarok 2 already takes advantage of that kind of theming, where different areas are tonally different, but still match the overal colourscheme."
    author: "Soap"
  - subject: "Re: Colors"
    date: 2008-03-21
    body: "I remember someone posting a patch for peer review on panel-devel which allegedly solved this problem i.e. Plasma elements adopted system colors. The person even provided some screenshots showing the desktop with different system colours and it looked all very nice to me.\n\nThe odd thing was that the review request was practically ignored, nobody ever answered. I still wonder what actually happened there. Did the Plasma maintainers not understand the patch or were they waiting for some reference opinion from Mr. Seigo who missed/forgot the patch?"
    author: "Erunno"
  - subject: "Re: Colors"
    date: 2008-03-21
    body: "Oh no, it wasn't ignored.  It got reviewed and accepted.  It's all in there.  The Aya plasma theme is the first result of that effort."
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: Colors"
    date: 2008-03-22
    body: "Ah, thanks for the hint and the link to the Aya theme in the other comment. Looking again at the screenshots with Plasma widgets using native colour scheme the desktop looks far more coherent to me than the concoction of light and black colours that make up the current desktop in 4.0."
    author: "Erunno"
  - subject: "Judging..."
    date: 2008-03-21
    body: "So how many of the winners are going to be the judges this time round?\n"
    author: "fffadas"
  - subject: "Re: Judging..."
    date: 2008-03-21
    body: "depends on wheter your submission is better than their I guess ..."
    author: "wrt"
  - subject: "Re: Judging..."
    date: 2008-03-21
    body: "Not that I understand the question, but the judges will be Jamboarder, Ruphy, Pinheiro and myself.\n\nIf you're asking how many judges will be winners (and not 'how many of the winners are going to be the judges'), it depends on the participating themes.\n\nIf a judge is going to participate, he will not grade it's own theme.\n\nAs far as I'm concerned, since my theme (slim-glow) was included in extragear since 4.0.1, I will not be participating.\n"
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: Judging..."
    date: 2008-03-21
    body: "Not very related to this theme contest but, after the end of the wallpaper contest for KDE4, it was said that there will be a big pack available with all the wallpapers. Have I missed something ?"
    author: "DanaKil"
  - subject: "Re: Judging..."
    date: 2008-03-21
    body: "Well, you should ask the wallpaper contest organizers about that :)\n\nAs for the themes, since the submission has to be on kde-look.org, all will be publicly available."
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: Judging..."
    date: 2008-03-22
    body: "The reson such a package was not relese is quite simple, and my responsability....\nIt was outside of the scope of the contest... Some of the people that entered the contest requested exacly that... \"In case of not winning, do not release\".\n"
    author: "nuno pinheiro"
  - subject: "Re: Judging..."
    date: 2008-03-21
    body: "I don't care. If, as for the wallpaper contest they are the best, it would have been a waste not to have the judge participate!"
    author: "hmmm"
  - subject: "Re: Judging..."
    date: 2008-03-21
    body: "So why do they announce a contest if it is as you imply ? Keep the good work everyone."
    author: "nikos"
  - subject: "DIGG IT, Slashdot it, spread it, get new artists!"
    date: 2008-03-21
    body: "1) http://digg.com/linux_unix/Plasma_theme_contest\n\nthx in advance\n"
    author: "Koko"
  - subject: "Re: DIGG IT, Slashdot it, spread it, get new artis"
    date: 2008-03-22
    body: "I'm sure the folks at deviantart.com will be the ones you want to challenge the most, not DIGG."
    author: "Marc Driftmeyer"
  - subject: "Re: DIGG IT, Slashdot it, spread it, get new artis"
    date: 2008-03-23
    body: "I don't have an account on deviantart, if you have then please submit news about this competition there :) Thx in advance. "
    author: "Koko"
  - subject: "\"Classic\" theme"
    date: 2008-03-21
    body: "I wonder if there could be somebody who could take the \"Uniqueness. The unique always stand out in the crowd; so should your theme.\" guideline and create a unique theme that would stand out by not standing out at all. Meaning Plasma and KRunner would nicely fit with the rest of the apps, just like Kicker and Minicli in KDE3 do. I'm willing to pay in KWin bugfixes ;) . Seriously.\n"
    author: "Lubo&#353; Lu&#328;\u00e1k"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-21
    body: "Not sure if it is what you had in mind but I'm working on few updates to the Aya theme which blends in a way that you might be suggesting (the current one's at http://kde-look.org/content/show.php/Aya+Plasma+Theme?content=76197).  If Aya doesn't quite do it for you, let me know what visual specifics your looking for and I'll fork Aya into a new theme.  \n\nAs an aside, these themes are really pretty easy to do, so I genuinely hope others will dive in.  If anyone needs any helps, feel free to drop me a note."
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "I'm not sure I'm madly in love with that look, but I must say I commend the effort to have a Plasma theme match the rest of the desktop.\n\nThat should have been part of the initial design from day 1."
    author: "T. J. Brumfield"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "I agree. This theme, still, just uses SVG to make plasma elements look like applications, to some extent. But it cannot be perfect as Plasma devs decided to use SVG everywhere instead of common Qt widgets as applications do. (For example, the logout dialog in KDE 3 uses the same buttons as applications while in KDE 4 it uses different SVG buttons.) I think they favored eye-candy too much over consistency."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "I'm thinking it is possible to use QtWidgets in plasma. See krunner buttons, lists and combobox for an example. Granted to logout buttons example you gave makes your point regarding consistency.  Remember that many of the same primitives that are available with the QPainter facility are the same kind of primitives that are available in an SVG (wouldn't surprise me if the QT SVG renderer uses at least some one to one mapping of these primitives).  Personally, I think SVGs were a great choice, since it opens up the look and to the many of us with arguably better artistic skills than coding skills while still affording us the ability to create a \"well-blended\" theme.\n\nThere argueably isn't a Plasma theme *yet*, that blends really well with the oxygen style to everyone's taste.  But, IMHO, it's all just artwork now.  No bloody coding.  No bloody compiling.  Just grab Inkscape and start fiddling.  With a bit of diligence I bet we could make one that's turns out pretty decent.  Anyone up for the challenge?"
    author: "Andrew Lake"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "Oxygen is not the only style out there... and hopefully there will be new widget styles and window decorations, with time. \n\n"
    author: "Luciano"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "\"SVGs were a great choice\"\n\nSVG is great. The only problem is that in some places they used custom SVGs even where normal Qt widgets could have been used.\n\n\"it's all just artwork now\"\n\nAs Luciano wrote, Oxygen is not the only theme, and some themes also have several settings (e. g. Domino in KDE 3 or QtCurve can look many different ways depending on settings). If we want a Plasma theme consistent with the other parts of the system, we need one that blends well integrates with whatever widget style the user selects. As widget styles are programmed (AFAIK), this is not possible if Plasma themes are just artwork (no code). So we need different Plasma themes corresponding to every widget style but that would be very much pointless redundant work and it is not likely to happen."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "Fair points.\n\nAs a theme author I'll do my best to come up with themes that, while they admittedly may not adequately address the points above, will hopefully work reasonably well different styles.\n\npeace,\nAndrew"
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-26
    body: "The Aya theme is supposed to be \"color sensitive\". Also, the CSS3 \"appearance\" option should allow for perfect intergration (how good is Plasma's CSS?)."
    author: "Riddle"
  - subject: "Re: \"Classic\" theme"
    date: 2008-04-03
    body: "Perhaps the solution is that those who make themes make them in pairs of desktop and window themes? That way, they'd look the same but you can also mix and match."
    author: "yman"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "Another thing which I think is unneccessary inconsistency is the Run command dialog . It's OK that it uses Plasma widgets (which can be Qt widgets with widgets on canvas in 4.1) but why isn't it a normal window managed by the window manager?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "About consistency and plasmoids.... \nPersonal opinion only.....\nIts not about being consistent... if you wnat consistency use qt gui and make an app, plasmoids are not that IMO they are litle pices of somthing else and they are suposed to be like that .... againg if you want consistency for plasmoids ... there would be litle point on having plasma, plasma is about fredom to be difrent and do wahthever you want in a ui.... qt style has huge limitations couse of huge number of rules you must follow, plasma is an anser for that, personaly i heven dont favor the all loking alike plasmoids, (think plasma should send a set of rateher coerent plasmoids) but not much more than that, the rest is up to the user to decide, a kinda infinite fredom via svg tewking chosing on kde-look, website downloads etc. \nmaking them coerent with the style is ...\n1 impossible for an all grouds solution.\n2 very limitating for the creators.\n\n\nPS I wont make enteries to the Contest.   "
    author: "nuno pinheiro"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "Isn't it a design flaw of Plasma if even the default plasmoids, that everyone need, look in a different way from applications? You wrote that Plasma is about freedom, everybody creates their own look - this is exactly what I think is inconsistency. Different parts of the system shouldn't create their own different look, they should use the same look as the whole system."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "I disagree. I don't think its a design flaw. In my opinion the panel doesn't belong to the applications. There are applications running on top and the panel in the background, and its ok for me if they look different, because they _are_ different, at least semantically."
    author: "blubb__"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "To clarify, what I have said are my opinions only.\nAbout the pannel ..... ist very very complicated matter, I dont like the current look of the pannel think it should be redone... Making it look like the widget set in use is one idea, dough Im not sure it will deliver any beter results. \nIm pretty sure it wont btw.\nAgain this are my personal ideas on the subject they dont represent plasma in any way.\n\n\n"
    author: "nuno pinheiro"
  - subject: "Re: \"Classic\" theme"
    date: 2008-04-03
    body: "I like the contrast between windows and Plasmoids. The way it's done allows you to choose whether to have it consistent or different, through mixing and matching. It doesn't have to consistent or inconsistent, because it's up to you to choose."
    author: "yman"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "That looks pretty close. I like that KRunner is a rectangle with it, so as soon as I fix KWin to be able to force decorations on any window KRunner will be again a normal dialog, despite how special Plasma developers may think it is. But the colors still don't look right, the gray is somewhat darker than the rest. I read in the kde-look description that the SVG should be colored from the system colors, so is this a bug? There's also the even darker stripe at the top of KRunner, but I guess that's intentional, without the possibility of KRunner having a decoration.\n\nSimply put, I'd want a theme that would not be a theme at all, i.e. a look like of every other windows/widgets. It may look absurd to actually have to ask that something looks normally, but it almost seems like the goal of KDE4 is to impress people with how shiny it is. There are some people who have been using KDE already for some time and care more about using it than just watching it.\n"
    author: "Lubos Lunak"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "It's not a bug.  The lazy theme author used the same gradient for the panel-background (which was intentionally slightly darker) for the dialogs.  I intend to lighten up the dialog SVGs a bit (the SVG's can 'control' how much colorization is applied).\n\nThe darker stripe at the top of the krunner dialog was intentional (even if admittedly if it's not quite right... yet). Once it takes on the system colors, it starts to look like a window sans window decoration. So it seemed like it needed something to 'anchor' it.\n\nI realized a lot of people want a theme that blends well with, or is just the currently selected colors/style.  This was part of my original motivation for getting involved.  I'll keep working on Aya, or maybe a fork of it, that comes as close as possible to satisfying this part of my motivation."
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-22
    body: "lubos, at first i agreed with you about the run dialog but after using it for a while i don't realy see the issue anymore... How about you?"
    author: "jospoortvliet"
  - subject: "Re: \"Classic\" theme"
    date: 2008-03-27
    body: "====================\nNEWER LINK TO AYA\n====================\nIt looks like Aya has been updated. View this link (it seems to look better to me):\nhttp://kde-look.org/content/show.php/Aya+(for+theme+contest)?content=76197\n"
    author: "Riddle"
  - subject: "please pimp plasma!"
    date: 2008-03-21
    body: "at the moment plasma is soo ugly, the panel has a huge clock by default, has a lot of different icon sizes; the krunner dialog and the options to manipulate plasma stuff on desktop has also a bad layout and usability..\n\nHey folks, please help KDE and improve all this things!\n\nThanks a lot :)"
    author: "Tom"
  - subject: "Re: please pimp plasma!"
    date: 2008-03-21
    body: "Dude, you want pimp?! Check this out:\n\nhttp://vizzzion.org/images/blog/pink.png \n\nhehehehe...\n\nDon't forget to contribute your own pimpin' plasma theme (seriously dude, it's really not hard).\n\npeace"
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: please pimp plasma!"
    date: 2008-03-21
    body: "Pinheiro also had a pink mockup, long time ago.\nhttp://pinheiro-kde.blogspot.com/2007/12/make-every-one-happy.html"
    author: "Leiteiro"
  - subject: "Re: please pimp plasma!"
    date: 2008-03-22
    body: "My mother might switch to Linux just for that theme!"
    author: "T. J. Brumfield"
  - subject: "Let People Vote"
    date: 2008-03-22
    body: "I strongly disagree with those who are judging this being allowed to enter in it and feel that instead of people judging, that all entries should be placed on line for KDE users to vote on, thus leaving it a community decision on who wins.  \n\nThis makes the competition fair, and also allows people to vote for what they feel is the best, instead of being lumped too often with themes that leave many of us running to KDE-look to download a decent theme"
    author: "R. J."
  - subject: "Re: Let People Vote"
    date: 2008-03-22
    body: "Yes, online voting is so fair! No vote stuffing, no calling for votes on forums, only pure artistic impartiality.\n\nBut you know what, it matters not, because it will be on kde-look.org, and thus rated by popularity anyway. And downloadable through GHNS.\n\nAnd the judges being amongst the best artists of the community, it would be really unfortunate if they could not compete."
    author: "hmmm"
  - subject: "Re: Let People Vote"
    date: 2008-03-22
    body: "* people can vote, kde-look has a nice rating mechanism. The rating will be considered while choosing the winners.(1)\n\n* having the judges not to compete would disqualify two very popular themes (see the rating) of Jamboarder (Aya and Heron)\n\n* if we wanted to push our themes into 4.1, we wouldn't make this contest at all\n\n* not everybody will be satisfied by the chosen winners, not everybody CAN be satisfied by the chosen winners. For the restless, there is always kde-look\n\n(1)\n1. Problems with ratings:\n1.1. not every KDE user will vote. In fact, most users will not.\n1.2. it can be tempered with\n1.3. we would have to wait a couple of months to get statistically valid results  "
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: Let People Vote"
    date: 2008-03-22
    body: "Just a clarification: Heron is made by magaden.  No reason to have Heron associated with rif-raf like me. :)"
    author: "Andrew Lake (jamboarder)"
  - subject: "Re: Let People Vote"
    date: 2008-03-22
    body: "Huh, my apologies to magaden.\n\nMy bad, I knew I liked two of your themes, and blindly thought that the second is Heron... :)"
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: Let People Vote"
    date: 2008-03-22
    body: "I think it is OK to have judges to decide as long as they judge not only by personal taste and accept that other people might have different taste, and select different styles that match every people's taste as far as possible. This didn't happen at the wallpaper contest when the judges selected similar photos which are, in my opinion, not suitable for being a wallpaper."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Let People Vote"
    date: 2008-03-22
    body: "why not let kde-look.org votes (or number of downloads?) be \"another member of the jury\" if you don't like handful of people giving ratings.\nI myself am more worried that there won't be enough submission."
    author: "kristjan_"
  - subject: "Please submit some themes for grown ups"
    date: 2008-03-23
    body: "I hope so much to at last find some theme that is intersting for people aged more than 20. In all theme sets for any software, I am so bored to only find black&fire styles, or cheesy fantasy stuff. Please also submit some art for grown ups ok ?"
    author: "Dom"
  - subject: "SVG rendering"
    date: 2008-03-23
    body: "When I helped judge the SVG background contest, there was a problem.  The KDE SVG rendering software wouldn't render many of them including, IIRC, some of the winners so I had to use Squiggle to judge them.\n\nThis problem shouldn't occur with Plasma themes.  Please add to the rules that cool effects (including filters) that won't render with the current KDELibs shouldn't be used.\n\n"
    author: "JRT"
  - subject: "Not so many KDE 4 (.1) themes"
    date: 2008-03-23
    body: "I am just wondering... There are not so many KDE 4(.1) themes around (there are only few of them on kde-look.org). Is it difficult to create themes for KDE 4 or maybe majority of KDE users still use KDE 3?"
    author: "koki"
  - subject: "Re: Not so many KDE 4 (.1) themes"
    date: 2008-03-23
    body: "KDE4 has been around for a little over 2 months; KDE3 has been around for  almost *6 years* :)"
    author: "Anon"
---
The KDE Plasma team is inviting everyone to participate in a contest to create Plasma themes from which a select few will be chosen to be included as a part of the upcoming KDE 4.1 release. This is a great opportunity to contribute to a very visible component of the KDE project, the Plasma desktop.




<!--break-->
<p>A great feature of Plasma is the ability to theme components of the desktop using Scalable Vector Graphics (SVGs). This means there is no need for you to know C++ or any other programming language to create a great looking theme.</p>

<p>You only need to know how to use one of many graphics tools (e.g. Krita, Inkscape, The GIMP, Karbon, etc.) and that is all. Depending on your prowess with such tools, you can create great looking plasma themes in as little as an hour.</p>
<p>
You can find a quick guide to creating Plasma themes in the <a href="http://techbase.kde.org/index.php?title=Development/Tutorials/Plasma/Theme">Creating a Plasma Theme in 7 Easy Steps</a> Techbase article.<p>
<p>
Rules:<p>
<ol>
<li>Submit your work in the <a href="http://kde-look.org/index.php?xcontentmode=76">Plasma Themes section of KDE-Look.org website</a>. Put a notice '(for theme contest)' in the title of the theme.</li>
<li>Submissions will be accepted as a tar.gz file of the theme folder.</li>
<li>Submission deadline is 9th April, 2008 18:00 UTC. Themes submitted after the deadline will not be considered for this release.</li>
<li>Winners will be announced by 18th April, 2008.</li>
<li>All artwork must be original or otherwise meet the requirements for distribution as part of a GPL licensed project. Preferred license is LGPL.</li>
</ol>

<p>Guidelines:</p>
<ol>
<li>Consistency. All theme parts should fit nicely with each other.</li>
<li> Completeness. Although Plasma automatically loads required images from the default theme when they are not present in the active theme, it is suggested that you include SVGs for all theme elements.</li>
<li>Uniqueness. The unique always stand out in the crowd; so should your theme.</li>
</ol>

<p>Have fun! The Plasma Team.</p>


