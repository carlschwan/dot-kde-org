---
title: "Akonadi Sprint Readies for KDE 4.1"
date:    2008-03-27
authors:
  - "talbers"
slug:    akonadi-sprint-readies-kde-41
comments:
  - subject: "Department joke"
    date: 2008-03-27
    body: "I still think it should have been \"marathon-hacking-FTW dept.\""
    author: "Kevin Krammer"
  - subject: "Looking forward to this"
    date: 2008-03-27
    body: "There are currently three apps (or lack of these apps) that prevent me to move my main desktop to KDE4 :\n\n- Kontact\n- K3B\n- Amarok\n\nSo I'm really looking forward to Akonadi and PIM stuff. :-D\nThe whole thing seems very promising. How will it be related to Kolab?"
    author: "Richard Van Den Boom"
  - subject: "Re: Looking forward to this"
    date: 2008-03-27
    body: "Don't worry - they work fine in a KDE4 desktop. Still, it's probably worth waiting for 4.1 as 4.0.2 is still a little light on features, although stability is getting reasonable - reasonable enough for me to run it full time now."
    author: "James Ots"
  - subject: "Re: Looking forward to this"
    date: 2008-03-27
    body: "Everybody knows that KDE3 apps (as well as Gnome apps as well as X apps as well as...) can be run in a KDE4 environment. No need to stress this every time the subject comes up!!\n\nHowever, I see it like the original poster: I like a desktop environment where everything fits in. Currently, KDE3 apps don't seem to be any more at home in KDE4 as e.g. a Gnome app: The filetype associations are taken from a (maybe not even existing) KDE3 desktop setup, same goes for hotplugging of e.g. the media devices in Amarok and so on. Thus, you need a full fledged and seperately configured KDE3 installation to run & set up single KDE3 apps in KDE4, and that's not really what I call downwards compatibility - I can have the same level of compatibility with apps from about any other Unix desktop environment.\n\nAs there seems to be no effort to iseamlessly ntegrate KDE3 apps into a KDE4 environment, it's for me very understandable that some users look forward for killer apps like k3B, amarok and so an natively in KDE4!!!!!!!!!!"
    author: "Anon"
  - subject: "Re: Looking forward to this"
    date: 2008-03-28
    body: "The whole problem started with the combination of \"Desktop\" and \"Environment\". Yes - there can be multiple desktops - but there can be only one \"Environment\".\n"
    author: "nf2"
  - subject: "Re: Looking forward to this"
    date: 2008-03-28
    body: "Yeah, but it would be great if at least KDE3 apps would not just be running in a KDE4 environment, but fit perfectly in: Common Mime-types, common handling of removable devices and so on. There should be some kind of compatibility layer for KDE3 in KDE4, as long as you're officialy encouraged to use KDE3 apps until their KDE4 equivalents are up to date."
    author: "Anon"
  - subject: "Re: Looking forward to this"
    date: 2008-04-01
    body: "Considering things like these are getting fd.o standardised the move from KDE 3 to 4 may well be the last one without any such \"compatibility layer\"."
    author: "Anon"
  - subject: "Re: Looking forward to this"
    date: 2008-03-28
    body: "K3b from extragears (where else) has been working fine for me for the past month."
    author: "JackieBrown"
  - subject: "Re: Looking forward to this"
    date: 2008-03-28
    body: "Regarding Kontact: http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/KDEPIM4.1onKDE4.0/\nIt allows you to use Kontact with a stable KDE 4.0.x release instead of using the UNSTABLE branch for everything.\nIt has a few quirks, but overall I found it to work quite well.\nIf you encounter bugs, post them to https://bugzilla.novell.com/ -- not KDE's Bugzilla because PIM4.1onKDE4.0 is not an official KDE branch. I included \"KDEPIM4.1onKDE4.0\" in the summary line of the bug reports I posted to make them easier to find."
    author: "KAMiKAZOW"
  - subject: "Re: Looking forward to this"
    date: 2008-03-31
    body: "K3b and Amarok would be enough for many people to switch exclusively to Linux/KDE.\n\nIt's crazy how high the buzz around Amarok is. Hope there will be more updates and demo vids as the final version approaches. We need stuff to feed the rumor mill and college bulletin boards with. :)"
    author: "Max"
  - subject: "evolution data server"
    date: 2008-03-27
    body: "\"The Akonadi server still needs to be moved to a desktop neutral position, but that is currently blocked by one issue, which at the end of the meeting was properly documented with a very complex testcase which has been passed onto the appropriate people.\" can you explain more !\n\nis there any plan to work with EDS fame, as it seems they share a lot of functionalities.\n\nthanks"
    author: "mimoune djouallah"
  - subject: "Re: evolution data server"
    date: 2008-03-27
    body: "We are currently looking for a student willing to implement GLib/GObject based client library similar to our libakonadi-kde as a Google Summer of Code project so applications from the GLib software stack can also use Akonadi.\n\nAs a side effect it would be possible to create implementations of the two EDS client libraries, e.g. allowing applications which were designed to use EDS to use Akonadi without requiring any change.\n\nTransitioning from deployed functionality to the next generation implementation of the functionality always takes some time, e.g. the DCOP -> D-Bus transition had to wait for KDE4.\n\nDue to the possibility of implementing an EDS backend for accessing Akonadi, an EDS -> Akonadi transition would theoretically be possible at any time, though providing alternative client library implementations as described above is probably more efficient.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: evolution data server"
    date: 2008-03-27
    body: "hanks really for the answer, it is always a pleasure to hear about collaboration ( at least the intent to do that) between the two linux desktops, reading about glib stack and eds in a dot story show a real change  of mentality, thanks kevin for being sensible to better interoperability of the two desktops. "
    author: "mimoune djouallah"
  - subject: "Thanks!"
    date: 2008-03-28
    body: "> We can easily say that this meeting was one of the most productive\n> Akonadi meetings in the past years and the developers are looking\n> forward to the first public release of their software.\n\nI'm looking forward to it too. A great big thanks for all your hard work!"
    author: "Jeff"
---
Last weekend a group of developers gathered in Berlin at the <a href="http://www.kdab.net/">KDAB office</a> for an <a href="http://pim.kde.org/akonadi/">Akonadi</a> sprint. The goal was simple, getting Akonadi in shape for the first public release of Akonadi when KDE 4.1 is released. In the couple of days they met, they made an amazing amount of 270 modifications to the KDE repository, and worked on average from 10am to 3am to make a big step into reaching that goal.  Read on for details.







<!--break-->
<div style="float: right;  border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px;">
<a href="http://static.kdenews.org/jr/akonadi-meeting-2008.jpg"><img src="http://static.kdenews.org/jr/akonadi-meeting-2008-wee.jpg" width="300" height="225" /></a><br />
From left to right: Kevin Krammer, Tobias Koenig, Volker Krause, Tom Albers, Thomas McGuire and Frode Døving.
</div>

<p>Akonadi is the framework that will be used by the PIM-applications to cache their data. That means that it can contain your address book, e-mails and agenda items, or any other data you want in there. It will be the framework that will be used by most of the KDE PIM applications for the KDE 4.2 release.</p>
 
<p>During the sprint the current problems were identified, discussed in detail and prioritised. There now is a complete list of things that need to be done before the 4.1 release and things that would be nice to have before 4.1, but are not crucial.</p>
 
<p>The developers also moved the KDE specific library to the final destination in the subversion tree. The Akonadi server still needs to be moved to a desktop neutral position, but that is currently blocked by one issue, which at the end of the meeting was properly documented with a very complex testcase which has been passed onto the appropriate people.</p>
 
<p>The third major task completed during the meeting was a review of that library. Each and every part of the Akonadi library has been evaluated to make sure the naming is consistent and logical. The developers identified over a hundred issues, most of them simple renamings, but some more complex changes are required as well.</p>
 
<p>The Mailody developers were around to share the experiences they have had with working with the library. Because that will be the first application that will be available using Akonadi, they could give feedback about the current pitfalls of the library. </p>

<p>During the sprint an amazing amount of work was put into Akonadi and related KDE PIM code. In the time equivalent to 6 person weeks the developers committed about 270 modifications to the repository.</p>

<p>We can easily say that this meeting was one of the most productive Akonadi meetings in the past years and the developers are looking forward to the first public release of their software.</p>
 





