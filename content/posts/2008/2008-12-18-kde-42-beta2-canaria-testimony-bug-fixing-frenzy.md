---
title: "KDE 4.2 Beta2 \"Canaria\" Testimony to the Bug Fixing Frenzy"
date:    2008-12-18
authors:
  - "sk\u00fcgler"
slug:    kde-42-beta2-canaria-testimony-bug-fixing-frenzy
comments:
  - subject: "Wow"
    date: 2008-12-18
    body: "1st. Thank you."
    author: "Max"
  - subject: "Re: Wow"
    date: 2008-12-18
    body: "I agree. Thanks KDE team!"
    author: "thothonegan"
  - subject: "Re: Wow"
    date: 2008-12-18
    body: "Please don't first up the dot. Anyway thanks KDE devs. I'm super stoked about 4.2!"
    author: "xian"
  - subject: "Re: Wow"
    date: 2008-12-19
    body: "Definitely wow!  The stats say a lot, and the length of the changelog is huge! Compliments for the amazing progress and wonderful release announcement! :-)\n\nLet's make those chairs fly ^^"
    author: "Diederik van der Boor"
  - subject: "Eager to test it..."
    date: 2008-12-18
    body: "Does anyone know if (or when) kde4daily will have the beta 2 update?\nIt still says I have the latest update but it was almost a week since it did the last update..."
    author: "AC"
  - subject: "Re: Eager to test it..."
    date: 2008-12-18
    body: "it's there now according to a blog ;-)"
    author: "jospoortvliet"
  - subject: "Re: Eager to test it..."
    date: 2008-12-18
    body: "nice! (and updating)\n\nWhat blog btw?"
    author: "AC"
  - subject: "Is this the same as the KDE4.2 'neon' thing"
    date: 2008-12-18
    body: "I installed a copy of Ubuntu8.10 when I saw an article about KDE4.2 daily 'neon' builds available here:\n\ndeb http://ppa.launchpad.net/project-neon/ubuntu intrepid main\n\nThat was several weeks ago, and it installed fine.  But there have been no updates available since.  Also, there seems to be a virtual box 4.2 tester release too.  Are these all the same thing (or at least different versions from the same folks)?  Or is the 'neon' thing discontinued?\n\nI guess I could wait till I get home tonight and check for an update, but I'm impatient...\n"
    author: "littlenoodles"
  - subject: "Re: Is this the same as the KDE4.2 'neon' thing"
    date: 2008-12-19
    body: "Hi\n\nThere is a bunch of ppl using the Neon-project nightly builds of KDE;\nThe latest update is from the 17th Dec, so not really out-dated. I noticed it gets updated once or twice in a week.\n\nFor some reason I don't know, some parts are not included (ie. kate) but beside that it's pretty good and even includes debugging symboles pckages.\n\n\n"
    author: "emms"
  - subject: "Re: Eager to test it..."
    date: 2008-12-18
    body: "It's been a pretty rough week, compilation-wise - hopefully things will be a bit smoother now.\n\nAnd yes - KDE4Daily now has http://websvn.kde.org/?view=rev&revision=898444, which is < 12 hours old :)"
    author: "SSJ"
  - subject: "Correction"
    date: 2008-12-18
    body: "There's a typo in the first sentence of the second paragraph: \"Since the first beta, which was released less than 4 weeks ago, 1665 bugs new bugs have been opened, and 2243 bugs have been closed.\"\n\ns/1665 bugs new bugs/1665 new bugs/"
    author: "Matt"
  - subject: "Re: Correction"
    date: 2008-12-18
    body: "fixed, thanks."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Correction"
    date: 2008-12-18
    body: "Is this proper English? (English is not my first language, but I'm not sure this sentence is grammatically correct).\n\nFor the 4.2 release, the KDE team has fixed literally thousands of bugs and has implemented dozens of features that were missing until now in KDE 4.2 fixes your issues."
    author: "Peter Plys"
  - subject: "Re: Correction"
    date: 2008-12-18
    body: "Not quite. A better way to say it would be:\n\"For the 4.2 release, the KDE team has fixed literally thousands of bugs and has implemented dozens of features that were missing until now in KDE 4.2, which will fix a lot of your issues.\""
    author: "Jonathan Thomas"
  - subject: "Re: Correction"
    date: 2008-12-18
    body: "american or english english ?"
    author: "asdf"
  - subject: "Re: Correction"
    date: 2008-12-18
    body: "What is English English? Is that British English with 20% more Oxford?"
    author: "Stefan Majewsky"
  - subject: "Re: Correction"
    date: 2008-12-18
    body: "Fixed as well, thanks :)"
    author: "Sebastian K\u00fcgler"
  - subject: "Hmm "
    date: 2008-12-18
    body: "Will this get onto the Kubuntu Jaunty alpha2 LiveCD?"
    author: "ethana2"
  - subject: "Re: Hmm "
    date: 2008-12-18
    body: "Yes. It was uploaded the day before yesterda/yesterday just in time for the alpha 2 CDs."
    author: "Jonathan Thomas"
  - subject: "Re: Hmm "
    date: 2008-12-18
    body: "Btw. I'm thinking about updateing my Kubuntu. Will there be KDE 4.2 final packages for Intrepid, or perhaps even Hardy (would rock even more, as I'd love to have a stable base)? I'd hate to wait for Jaunty ;)"
    author: "Michael"
  - subject: "Re: Hmm "
    date: 2008-12-18
    body: "KDE 4.2.0 is planned to be placed in intrepid-backports once it is released. Hardy will not be seeing any KDE 4.2 releases."
    author: "Jonathan Thomas"
  - subject: "Awesome!"
    date: 2008-12-18
    body: "Downloading and compiling right now. With the debug flag set. ;-)"
    author: "Gentoo User #239421"
  - subject: "bug fix rate"
    date: 2008-12-18
    body: "lets get the bug count up my fellow users , and report as many bugs as we can!\n\n"
    author: "asdf"
  - subject: "Re: bug fix rate"
    date: 2008-12-18
    body: "*ahem* Let's find as many bugs as possible, and check super-duper thoroughly to make sure we don't report duplicates."
    author: "Anon"
  - subject: "Bug no. 1"
    date: 2008-12-19
    body: "No Administrator Button in System Settings. It's over 1 year old now and WE would be very happy if some kind coder out there would give it a little attention."
    author: "Bobby"
  - subject: "Re: Bug no. 1"
    date: 2008-12-19
    body: "\"No Administrator Button in System Settings.\"\n\nand there won't be. running the whole GUI as root is not only slow and resource intensive, it's also not exactly great for security.\n\ninstead, as has been done already for things like the time and date control panel, helper apps, preferably with policyKit integration, should perform the actual final settings applications. and those little, non-gui apps should run with elevated priveleges only."
    author: "Aaron Seigo"
  - subject: "Re: Bug no. 1"
    date: 2008-12-19
    body: "I am not talking about running the whole system as root. As a matter of fact if there is no administrator button in the system settings then I WILL HAVE TO RUN THE SYSTEM AS ROOT in order to make certain changeses like changing the KDM theme which is not possible as a normal user in KDE 4. In KDE 3.5 present version using KDM 3 all I have to do is press the administration button in the system settings which promps me to give my root password and then I can make the changes - no security risks to my system. In KDE 4 I have to log in as root in order to make those changes, which of course I personally hate because then my system is vulnerable."
    author: "Bobby"
  - subject: "Re: Bug no. 1"
    date: 2008-12-20
    body: "You missed what Aaron said. You shouldn't have to be running any windows as root much less an entire session, what should be happening is PolicyKit will let you make certain changes (assuming you have the appopriate permissions from PolicyKit), or you'd supply the root password when applying the settings which'd spawn a process as root to do the actual changes, the application with the x11 session wouldn't have any actual special permissions at any point in time.\n\n>In KDE 3.5 present version using KDM 3 all I have to do is press the\n>administration button in the system settings which promps me to give my root\n>password and then I can make the changes - no security risks to my system.\nThis has a number of issues:\n1.) Speed: Pretty much an entire session must be started for the root user, making it *much* slower to open\n2.) Memory usage: Because all the services have to be started for root, more memory is being used.\n3.) Security: Different X clients can assert a *lot* of control over each other, it's generally not considered safe at all to be running X11 apps as root in a normal user's session (running X as root isn't considered a very good idea either, unfortunately there isn't too much choice with that at the moment)."
    author: "Kit"
  - subject: "Re: Bug no. 1"
    date: 2008-12-20
    body: "So why can't I or a lot of other users make the afore mentioned changes on KDE 4? Why does this bug still exist as valid if the solution is already there?\n"
    author: "Bobby"
  - subject: "Re: Bug no. 1"
    date: 2008-12-20
    body: "PolicyKit is very new (in fact we made an exception for it being merged after the freeze). Apps like System Settings need to pick it up now."
    author: "sebas"
  - subject: "Re: Bug no. 1"
    date: 2008-12-20
    body: "Thanks for the info Sebas. So about when can we start enjoying it? Will I be able to change my login theme as a user by the release of KDE 4.2?  "
    author: "Bobby"
  - subject: "Re: Bug no. 1"
    date: 2008-12-21
    body: "I guess this depends if such changes are seen as bug fixes or as new features. The former would appear in KDE 4.2 sometime, the latter would have to appear in 4.3 earliest."
    author: "Anon"
  - subject: "Re: Bug no. 1"
    date: 2008-12-23
    body: "As far as I understand PolicyKit is quite young and might not be fully functional or be officially implemented in 4.2 although openSuse seems to provide the packages. I am however not sure as to how usable this new implementation is."
    author: "Bobby"
  - subject: "plasma-workspace-4.1.85 fails"
    date: 2008-12-18
    body: "plasma-workspace-4.1.85/plasma/applets/system-monitor/net.h:22:27: error: ui_cpu-config.h: No such file or directory\n\nmaybe...\n\nnet.h:22 #include \"ui_cpu-config.h\"\n\nshould be\n\n#include \"ui_net-config.h\"\n\n?\n\nPs. no bugzilla account, so here, sorry. :-)"
    author: "Anon"
  - subject: "Re: plasma-workspace-4.1.85 fails"
    date: 2008-12-18
    body: "plasma-devel at kde dot org is a better place for such reports; but in this case, it's correct. apparently it's not generating the ui_cpu-config.h from cpu-config.ui?"
    author: "Aaron Seigo"
  - subject: "Re: plasma-workspace-4.1.85 fails"
    date: 2008-12-18
    body: "It is, but just like for the hdd_config.ui or temperatur_config.ui, it's generated for their use only, so all of them need their own .ui file.\n\nthe CMakeLists.txt even has\n\nkde4_add_ui_files(net_SRCS net-config.ui)\n\nso it's generated, but just not included (copy'n'paste bug of cpu.h obviously :-) )."
    author: "Anon"
  - subject: "Re: plasma-workspace-4.1.85 fails"
    date: 2008-12-19
    body: "aah, it's in net.h .. yes, that would only work depending on what cmake decided to do. probably worked for us doing make -jN due to that ... anyways, fixed in svn. =)"
    author: "Aaron Seigo"
  - subject: "Re: plasma-workspace-4.1.85 fails"
    date: 2008-12-19
    body: "you are The Man.\n\nsure it's not that per se, but by you comment hint using -j1 did get the compilation go thru with that \"broken\" include.\n\nI bet it's all about me having an \"-j -l4\" as my make conf. maybe I'm too used to have cmake do it's own configs in parallel (if that what happens with -lN)."
    author: "Anon"
  - subject: "debian packages?"
    date: 2008-12-18
    body: "i'm very sad that debian is not packaging beta2 not even in experimental"
    author: "zvonsu"
  - subject: "Re: debian packages?"
    date: 2008-12-20
    body: "http://kde42.debian.net/"
    author: "Eckhart"
  - subject: "Thanks"
    date: 2008-12-21
    body: "debian packagers RULE!! they put the latest SVN on this server !!! go KDE. Let's debug it!"
    author: "zvonsu"
  - subject: "Much Better"
    date: 2008-12-18
    body: "A lot of bugs disappeared. There are still some plasma and krunner crashes, but the impression is that until final, all bugs will be easily crushed if developers keep the speed of fixes.\n\nCongrats."
    author: "Iuri Fiedoruk"
  - subject: "Great :)"
    date: 2008-12-18
    body: "I need a stable system to work. And yet KDE 4.2 Beta 2 does the trick for me :) If I stick to the main plasmoids (panel and folderview) everything works great. \n\nSome small tips:\n\n- I had to remove the plasma config file for things to work properly. This should be removed when plasma is not running. File is .kde4/share/config/plasma-appletsrc\n\n- When trying plasmoids some would crash on exit. This means you cannot exit plasms properly so they always appear on startup. Again deal with config file. \n\n- MSN is not working in my kopete. Apparently a problem with libmsn which has been fixed in later versions. \n\nUsing openSUSE 10.3 by the way\n"
    author: "Jad"
  - subject: "Re: Great :)"
    date: 2008-12-18
    body: "> I had to remove the plasma config file for things to work properly. \n\nplease report problems you have at bugs.kde.org\n\n> When trying plasmoids some would crash on exit. \n\nbacktraces -> bugs.kde.org =)\n\n> This means you cannot exit plasms properly so they always appear on startup\n\nfixed with r898746"
    author: "Aaron Seigo"
  - subject: "Re: Great :)"
    date: 2008-12-19
    body: "Sure, Aaron will do all that :) \n\nThis was not meant as a report, just as small hints to users who might run into the same, to save them some time. \n\nKeep up the good work!\n"
    author: "Jad"
  - subject: "Thank you!"
    date: 2008-12-18
    body: "Thank you very much for this. Please ignore the \"it's not like KDE3.5\"-morons, but keep on innovating. It's very much appreciated."
    author: "walter"
  - subject: "Re: Thank you!"
    date: 2008-12-18
    body: "Yeah, few more years of \"innovation\" and we'll have a buggy version of gnome."
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-18
    body: "you evidently haven't tried 4.2 then, as your complaint is pretty stupid in light of the achievements in that release. you might even call 4.2 the \"parity release\" in which we have climbed over the \"must have the features of 3.5\" wall.\n\nthere are very, very few things there were available in 3.5 that aren't in 4, most of those things were very, very questionable to begin with, and there's a huge, huge amount of features that never existed in 3.5.\n\ni'm sorry if that ruins your \"clever quip\". though i personally feel  displays of sillyness such as yours deserve everything they have coming to them."
    author: "Aaron Seigo"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "Awww, did the backlash to being called a \"moron\" hurt your little feelings?\n\nIt's one thing to say that you don't get any constructive criticism from your detractors, but it's another thing entirely to call them \"morons\" and \"stupid\" just because they have the audacity to think that [Software X] version 4 should be \"feature parity with [Software X] version 3.5 plus extras.\"\n\nThat's the way it's been through most of history; to call them names because they expect it to work the same in KDE is... disingenuous at best.\n\nThey're not morons. They're not stupid. It's okay for them to be a bit miffed because of their confusion at the misnumbering of KDE 4. It's quite natural.\n\nBe nice to them, and they WILL eventually become productive bug-reporters and evangelists again. Keep calling them \"morons\" and \"stupid\" and they are likely to keep heaping on you the same abuse you keep whining about.\n\nYou have an opportunity to be the bigger man. What you do with such an opportunity speaks more about you than about them. I look forward to seeing a positive response in the future, rather than additional fuel on the flamewar fire."
    author: "CF"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "walter != Aaron Seigo - they spell and pronounce their names differently."
    author: "Anon"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: ">Keep calling them \"morons\" and \"stupid\" and they are likely to keep heaping on you the same abuse you keep whining about.\n\nI missed the part where Aaron called anyone a \"moron\" or \"stupid\"."
    author: "Anon"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "Walter called those who complain about lack of feature parity \"morons.\" Aaron called the grumbling backlash against such namecalling, \"stupid.\" Both are a stark contrast to the Mozilla developers who, upon the releasing of the Netscape Navigator codebase, made a huge campaign of accomplishing two goals:\n\n\"Feature Parity,\" and...\n\n\"Zarro Boogs.\" (For non-native speakers, this is just a fun way to say \"Zero Bugs.\")\n\nThey too understood the free-software value of releasing early and often, with nightly builds and encouraging users to become involved in the development effort, but they just didn't assign a .0 version number to it until they had very nearly achieved these two goals. I think Mozilla's approach was laudable, and the KDE 4 approach, while not necessarily invalid (who can decide such a thing?), was certainly more confusing than Mozilla's approach.\n\nThus, calling the inevitably confused users \"moronic,\" and their complaints \"stupid,\" as Walter and Aaron have done, is not as mature and productive as I would have expected. I hope for better in the future. We shall see.\n\n(By the way, if you want to understand the comparison to GNOME, compare Aaron's continued refusal to make a way to turn off the desktop cashew with the GNOME developers' continued refusal to add configurability options to Epiphany, the GNOME web browser, a few years ago. Do you know anybody who uses Epiphany now, after they've resisted incorporating commonly-requested features? Yeah, I don't either. See, the comparison is not as purely emotional as you think: there are actually some real-life parallels.)"
    author: "CF"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "What would you feel if you give something new to a huge community and some confused users start to say: 'your gift sucks hard!' or 'This is the worst thing ever!'\n\nI really admire Aaron and all the people involved in KDE, because their politeness and respect with this people. Seriously keep on bashing will hurt someone.\n\nWith the cashew thing, plasma developers designed it and on their vision it's a MAIN component. Why would one need to hide it? Do you want an option to hide the context menu too?"
    author: "xbullethammer"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "You said it sucks hard, I just that KDE is becoming a \"buggy version of gnome\".\n"
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "Your first paragraph is a common enough occurrence that there's a term for it with its own wikipedia article:\n\nhttp://en.wikipedia.org/wiki/White_elephant\n\nThat's an especially useful term to know around this time of year, because knowledge of it often influences people to be more thoughtful gift-givers, considerate of the needs and unique circumstances of the recipients."
    author: "CF"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: ">they just didn't assign a .0 version number to it until they had very nearly achieved these two goals.\n\nI don't know if you remember Friefox's 3.0 launch, but it was a lot like KDE's 4.0 launch. It had stable libraries and APIs to develop against, but all the extensions (applications in KDE land) came much later. And some of the features were rough around the edges (like KDE's actual desktop shell).\n\nMaybe \"moronic\" and \"stupid\" are strong language, but I'd certainly agree that those with the complaints are \"uninformed\". A lot of people seem to object that features are being \"removed\" as though the KDE devs began with KDE3 and began ripping features out of it, when in fact they started with Qt4 and are adding features. The idea that the goal of KDE4 is to become gnome is just ignorant."
    author: "Anon"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "Hi, and thanks for answering.\n\nI test KDE4 every week from svn and my complaint is not stupid. I still don't find it usable. A lot of features are missing. From simple, hard-to-spot features to obvious stuff.\nIn the first category I would put this... for example I wanted to attach a screen-shot of my plasma desktop to this post but failed to open it in gwenview/okular (\"could not open file\" popup appeared). As I found out later it had a-r permissions, but file dialog didn't show this like in kde3. Now it's like in gnome.\nSecond category, obvious stuff. No kprinter, I can't print odd/even pages. This makes KDE useless to me for printing. Bug reported for both. Nobody is doing nothing.\n\nBut features are not only problems of KDE4. Do you know that for more than a year you have a tab in systemsettings full of options none of which works? That's embarrassing. Either remove it or fix it.  OK, you can say that you didn't noticed it because systemsetting is hard to navigate as you don't have tree view anymore but that's not excuse. :-) Bug reported, people vote, nobody did nothing.\n\nNow to your field, plasma. I posted a comment with attached image when 4.2b1 was released which showed some easy to notice bugs. None of which are solved in beta2 so I made another image, you can find it attached.\n\nAnother recent disappointment are notifications. For I/O they show less information than window that was used before, use more desktop space (I used one window for all operations) and more CPU.\n\nI'm still forced to have cashew even if I never use it. I want to have something else in the corners. Also, plasmoid handles are really pain to use, I hate them ....\n"
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "I got the same problem, I had the exact same problem with KDE, my desktop was 7/8th gray like in your picture. \n\nThere is a lot of things that need to be fixed in KDE 4.2, but I hope in time it gets there. Then we can work on the small things that will make it so great. :)"
    author: "Jeremy"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "\"No kprinter, I can't print odd/even pages. This makes KDE useless to me for printing. Bug reported for both. Nobody is doing nothing.\"\n\nReally?  So you have an intimate knowledge of what's going on in my local svn, or what Qt have planned?  I'd better check my firewall logs again, you must have slipped in the back door while I wasn't looking...\n\nWe use the Qt print system these days, if you've missing a feature like odd/even pages try asking them politely, if enough users post the same feature request they will allocate more resources to the issue, or pick up my patches when I submit them soon.\n\nAnd I'll mention it again, just one more time, we had to drop KDEprint because it was broken and we had no-one to fix it.  Otherwise printing would have been useless for everyone."
    author: "odysseus"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "My crystal ball is broken at the moment so I don't know what's going in your local svn. I'm not going to ask anyone to implement basic features like this. What will be then the next request for KDE/QT5: adding delete functionality to a text editor? Basic functionality must be implemented before software is even released officially.\n\nI know that KDEprint is broken, and, with that said, I think devs should rather say that KDE lacks non-eyecandy manpower and not that KDE4.2 is so great, best desktop ever."
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "Please calm down, nobody wants to hurt you. Flaming and pathetic comments like yours don't help anyone, and surely won't motivate people to work out possible problems you report -- if you finally manage to bring across valid technical points.\nIf leaving your comfort zone is not yet an option for you, by all means, don't.\n\nAs to the printing options you're missing ... did you check out system-config-printer and printer-applet? If so, what's wrong with those, what functionality were you missing?"
    author: "sebas"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "I'm not flaming nor posting pathetic comments, only a bit sarcastic when I'm provoked. Note that in my comments I always have valid criticism, bug reports, screenshots,  suggestions. You saw my answer to Aaron.\n\n> As to the printing options you're missing ... did you check out system-config-printer and\n> printer-applet? If so, what's wrong with those, what functionality were you missing?\n\nPrint odd/event page sets from okular (its print dialog).\n\nhttp://printing.kde.org/overview/kprinter.php\n\nAnd my favorite:\n\nhttp://printing.kde.org/overview/commandbuilder.php\n"
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "The \"tab in systemsettings full of options none of which works\" is obviously overstated, and by far not specific enough. If you can be more specific, we're happy to address that. Otherwise, keep calm, pathetic bitching won't help you moving your agenda forward, unless that agenda is pathetic bitching (in which case you're not welcome).\n\nHow did you use one window for all operations by the way? I thought that wasn't possible before...?\n\nThe bug in the screenshot you describe is on our radar, and will most likely be gone in 4.2.0 (remember, you're running a beta).\n\nBTW, for printing configurations, install kdebindings so you can run system-config-printer and printer-applet. Sure, reading the announcement is not something anybody has time for. But it sure would've saved all of us some cycles complaining.\n\nAnd it's less embarrassing for you, which probably is why you chose to post anonymously."
    author: "sebas"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "> The \"tab in systemsettings full of options none of which works\" is obviously overstated, and by far \n> not specific enough\n\nIt's not overstated, but just like I said. Funny, you cannot even believe that it's true. I'm starting to understand why you devs think KDE4 is so great. You don't use it much as a whole, just the part you are developing. Those parts of KDE that don't have an active maintainer are really bad quality.  A bug report with enough info is reported more than 13 months ago. It sits there, confirmed, full of votes, untouched... (#152266).\n\n> Otherwise, keep calm, pathetic bitching \n\nI'm calm, didn't even want to comment the release at all, but after someone called group of users a morons, I added added a tiny sarcastic comment. It's stronger than me :-). My agenda is just to show (with valid examples, even screenshots) users and devs that KDE4 is not that all great.\n\n> How did you use one window for all operations by the way? I thought that wasn't possible before...?\n\nAttached. Compare this nice usable window to current plasma notifications.\n\n> The bug in the screenshot you describe is on our radar\n\nAttached image actually shows at least 3 bugs. :-)\n\n>  BTW, for printing configurations install kdebindings so you can run system-config-printer \n> and printer-applet\n\nDon't know nothing about it. I'm talking about print dialog (okular->file->print). Cannot choose even/odd pages for printing.\n\n> And it's less embarrassing for you, which probably is why you chose to post anonymously.\n\nI'm not anonymous, or to say it better, I am as much as anonymous as you are. :-)\n"
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "> I'm not anonymous, or to say it better, I am as much as anonymous as you are. :-)\n\nAhem, the poster you're replying to is the author of this article."
    author: "Stefan Majewsky"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "I know :-) and my statement is still valid.\n"
    author: "vf"
  - subject: "Re: Thank you!"
    date: 2008-12-19
    body: "As one of those maroons, I have to say that 4.2 is finally going to shut a lot of us up. Most of the stuff we have missed is now there. It gives us the familiar classic desktop, while not taking away the new stuff.\n\np.s. I'm only missing a good network manager, but that wasn't an official part of KDE to begin with. wicd is working as a suitable replacement."
    author: "David Johnson"
  - subject: "Re: Thank you!"
    date: 2008-12-20
    body: "And we're working on that, too. We're currently hoping to get it ready in time for inclusion into the spring distro releases."
    author: "sebas"
  - subject: "Re: Thank you!"
    date: 2008-12-22
    body: "I agree. I, personally, have not 'bitched' very much, indeed, I am running KDE4 on all desktops managed by me (3 currently). It *already* rocks, but 4.2 is just jaw-dropping, I love it.\n\nMy only complaints:\n* kwrite occasionally behaves totally erratically: cannot copy-paste, it shows empty text when there is text, etc. I need to \"killall kwrite\" and then all is well. Somehow one badly hanging kwrite process (not showing in Task manager) affects other kwrite processes\n* I use twinview (got 2x1280x1024) and dragging widgets from one to the other messes up widget positioning/drawing. I guess most developers do not use twinview that much, so that's why this bug hasn't been fixed (or  noticed). Not a big deal, btw, but is definitely a bug.\n* nvidia prop. driver messes up the plasma panel. This a is known issue, and is probably not KDE's fault"
    author: "Phd student"
  - subject: "Yay!"
    date: 2008-12-18
    body: "I must say that the amount of bugs resolved is noticeable. I have one small issue (which I have reported) where I can't get the systray in two rows in a horizontal panel, and a crash I have reported (corner case) but other that that this is a solid release, especially for a beta!\n\nGo, go KDE!"
    author: "Jonathan Thomas"
  - subject: "Issues found!"
    date: 2008-12-18
    body: "I found plenty of issues that bothers me with KDE 4:\n\n-Akonadi requires MySQL on startup even for the calendar applet. I don't know whether I have done something wrong during compiling but for what does it require such an overkill SQL database for a desktop ?\n\n- On small size Notebooks (Netbooks) often you can not see the apply or ok buttons. I had this issue in Koffice as well as in the KDE Settings tool. There is no way to access these buttons not even if you resize the fonts to 7 pixels. 1024x600 pixels.\n\n- The KDE Panel usually loses all the position information for launchers that i put there. Everything is arranged right.\n\n- Random crashes when entering URL in konqueror.\n- Random crashes when trying to access the audio settings in KDE Settings tool.\n- Same issues with KDE Bookmarks program. No progress bar when issuing recursive tasks like checking all favicons. Nothing happens when chosing this option. Does it actually do something ? And if yes why are no favicons being updated ? Specially when importing bookmarks from mozilla this option is doing nothing.\n- Same issues with the settings for all KDE apps. While a bunch of new architectual changes happened. The same setting files are dumped the same old way like in KDE 3.\n- Konqueror, when going to the Bookmarks menu you always get the \"open folders in tabs\" \"add bookmarks\" \"bookmark tabs as folders\" \"...\" in every tree folder. This is so annoying and so irritating when trying to access to some specific bookmarks. This was annoying in KDE 3 and still annoying in KDE 4 and no way to turn this off.\n\nAnyways there are plenty advantages and disadvantages within KDE 4. Some stuff definately got better, others worse and plenty of the things stayed as is or leave the impression for simply got \"ported but not improved\". Nonetheless I still am looking forward to KDE 4 knowing that a lot of work still has to go and that everyone is working hard to get everything done. Thanks so far and please have a look towards the one or other rant of mine above. First impressions usually are those that stay."
    author: "Simon"
  - subject: "Re: Issues found!"
    date: 2008-12-18
    body: "Please report those issues to bugs.kde.org (look for dupes first!). As you can see easily from the number of bugs that are being handled on a daily basis, doing bugtracking in Dot comments totally doesn't scale.\n\nAlso, you're talking about \"KDE 4\", which is far too little information. Again, all things that bugs.kde.org provides for."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "Do you really think, anyone is willing to fill such a mass of bugs with the current slow Interface of bugs.kde.org?"
    author: "Chaoswind"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "I recommend using the \"Report bug\" menu item in the application itself. Most stuff is already filled in this way. It saves some pain until the issue itself (bugs.kde.org being slow is addressed. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "That's only a pointer to bugs.kde.org. Dosn't reduce the work at all."
    author: "Chaoswind"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "People do it everyday. Most people only file one or two, and that's fine as it's distributed amongst all the users.\n\nSo, I don't have to think about it, I know that people are willing to do so."
    author: "Aaron Seigo"
  - subject: "Re: Issues found!"
    date: 2008-12-20
    body: "Whilst I usually fill out only one or two bugs at a time, more than once I gave up because either it kept timing out or when I go back and my nice detailed bug report is lost. \n\nThese days I write out a copy in kate first, but really shouldn't have to worry about things like this. It would be kind of nice to have a desktop (plasmoid? *grin*) to do this for advanced bug reporters that don't need step by step instructions. \n\nto me it seems like one of the applications of the ideals of plasma, what with the integration of the desktop and a remote service.\n\nIt would be cool, then if bugs.kde.org was unresponsive it would then queue the report, or you could get updates without opening a browser. Dunno if such a thing is easily achievable with bugzilla though"
    author: "txf"
  - subject: "Re: Issues found!"
    date: 2008-12-20
    body: "There is a downside though as you'd probably have to deal with a ton more of bug reports (hmm...conspiracy theorists might claim bug.kde.org works exactly as it should :P)"
    author: "txf"
  - subject: "Re: Issues found!"
    date: 2008-12-20
    body: "Since the introduction of Bugzilla 3.0 some months ago, it's much faster than it used to be.\n\nTo answer your question, which surely was rhetorical: Yes, but your basic assumption is wrong."
    author: "sebas"
  - subject: "Re: Issues found!"
    date: 2008-12-18
    body: "Yeah... As a fellow user, please file the bug reports, so that 4.2 final will be a smooth experience!\n\nWatching the bugfixes being filed over the last few weeks, the speed and skill has been amazing. Huge thanks to all the coders...! It's looking really good.\n\nbtw, I <3 kde2daily!"
    author: "Luke Chatburn"
  - subject: "Re: Issues found!"
    date: 2008-12-18
    body: "\"what does it require such an overkill SQL database for a desktop\"\n\nyou betray your bias with \"overkill\" ;) it's required to provide reasonable performance for large datasets.\n\nhave you measured the overhead, or are you just assuming it's overwhelmingly bad news? =)\n\n\"On small size Notebooks (Netbooks) often you can not see the apply or ok buttons.\"\n\nyes, an ongoing issue. we're actually better in kde4 than in kde3, but still not good enough.\n\n\"The KDE Panel usually loses all the position information for launchers that i put there.\"\n\nthis should be working in svn.\n"
    author: "Aaron Seigo"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "> we're actually better in kde4 than in kde3, but still not good enough\n\nBig padding and margins of oxygen styled windows with huge empty wasted spaces makes kde4 barely usable on smaller resolutions. OTOH, on my 17\" monitor KDE3 looks just fine. \n\n"
    author: "vf"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "\"Unusable\" is extreme. I can work pretty well with my Eee (well... at least before its SSD died, but that's not KDE's fault) although indeed some dialogs are too large. It's getting there, though."
    author: "Luca Beltrame"
  - subject: "Re: Issues found!"
    date: 2008-12-20
    body: "I said \"barely usable\". It would be nice to have oxygen compact, something like:\n\nhttp://martin.ankerl.com/2007/11/04/clearlooks-compact-gnome-theme/\n"
    author: "vf"
  - subject: "Re: Issues found!"
    date: 2008-12-18
    body: "\"Anyways there are plenty advantages and disadvantages within KDE 4.\"\n\nbtw, it might be nice to list some of the advantages with the disadvantages when trying to motivate those who work on things. it's a nice way to avoid giving the impression you are simply a whiner and to get the developers to empathize with you more.\n\npeople are strange, but it's worth keeping in mind."
    author: "Aaron Seigo"
  - subject: "Re: Issues found!"
    date: 2008-12-18
    body: "I rather find that way Konqueror handles bookmarks one of those small brilliant usability touches that puts Konqueror above other browsers. I find it much more convenient and logical to first browse to the place I want the bookmark stored before saving, rather than the other way around."
    author: "Morty"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "I agree, it's a very nice way to design it."
    author: "T"
  - subject: "Re: Issues found!"
    date: 2008-12-19
    body: "No problems with your way of using KDE but on a small display (e.g. Netbooks) it get's in the way of interaction. E.g. I keep nearly 20 Bookmark entries in a folder. The extra entries that the bookmark handler is adding make them nearly 24 entries - 4 entries more than I can keep on the display. For about a few hundred bookmarks I don't like to start sorting them again. I would appreciate if this can be an optional setting entry somewhere."
    author: "Simon"
  - subject: "Over Stimulation"
    date: 2008-12-18
    body: "I think I am running in over drive.  OpenSuse 11.1 and KDE 4.2 beta 2 released on the same day\n\nThanks for the work that has gone into this."
    author: "R. J."
  - subject: "glx plasma desktopFX"
    date: 2008-12-18
    body: "I'm not sure if this is a issue with the distro/packager or if it's a KDE issue.\n\nMy notebook X setup supports glx right out of the box with. I get direct rendering yes and the gears. It's not an impressive fps ~ 150.\n\nHowever, when enabling desktop FX, I get an error saying something like your setup is incorrect.\n\nI've read about certain drivers being blacklisted, which is all good and fine. Maybe radeon is black listed. I think that is what my notebook is using.\n\nHowever, the only reason I want to enable desktop fx is to speed things up. Window rendering is so slow without it. Is it there some way to fix this? Can there be a fallback mode for older cards and some kind 2D acceleration?"
    author: "frozen"
  - subject: "Re: glx plasma desktopFX"
    date: 2008-12-18
    body: "\"the gears. It's not an impressive fps ~ 150.\"\n\nglxgears is not a benchmarking tool.\n\n\"the only reason I want to enable desktop fx is to speed things up\"\n\ndesktop effects don't actually speed things up. they can make things *feel* faster (and smoother, and more fun, and nicer to look at, and more productive), but the painting doesn't actually speed up. =)\n\ndunno about which cards are blacklisted, though... "
    author: "Aaron Seigo"
  - subject: "Re: glx plasma desktopFX"
    date: 2008-12-21
    body: "Yeah, but if your waiting on the interface to update, it's pretty lame. In that case, a UI speed up = an app speed up.\n\nAgain, I don't even want the effects. I just noticed that turning OpenGL compositing, etc, makes my desktop more responsive. With it off, it's as if I'm using a really old PC. So I thought I'd try the same for my laptop.\n\nAnyway, it's not a big issue. Thanks for the suggestions guys. I'll keep trying to crack this one."
    author: "winter"
  - subject: "Re: glx plasma desktopFX"
    date: 2008-12-18
    body: "Have you tried the XRender mode rather than OpenGL?"
    author: "Med"
  - subject: "Re: glx plasma desktopFX"
    date: 2008-12-21
    body: "Yes, OpenGL and Xrender both don't work."
    author: "winter"
  - subject: "Re: glx plasma desktopFX"
    date: 2008-12-19
    body: "There's a disable capability tickbox for these cases, try that one."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: glx plasma desktopFX"
    date: 2008-12-19
    body: "Disabling desktop effects should speed everything up. However I have run across some annoying Qt bugs with graphics view using OpenGL for rendering. I think the problem is that the driver reports a cability, but it ends up being slower than the software fallback. Unchecking \"check capabilities\" might fix this for you."
    author: "David Johnson"
  - subject: "KDE 4.2 CountDown !"
    date: 2008-12-19
    body: "KDE 4.2 CountDown Banner Created last week. The links of banner is in a text file that can be download here:\n\nhttp://www.fileqube.com/file/JzMnEADMr160252 "
    author: "Hosein-mec"
  - subject: "And what about printing?"
    date: 2008-12-19
    body: "(Sorry to post anonymously, but with spam at an all-time high... *sigh*)\n\nHas any attention been given to printing in KDE 4.2?  I'm using Kubuntu 8.10, which uses KDE 4.1.3  and I cannot get anything to paper... Because I cannot define a printer... right now I'm trying to use the CUPS interface to define my LPR printer and I cannot remember the last time I had to fight to define my trusty old LJ6MP in a *nix install.  Obviously, no success yet.\n\nNot to sound like a whining troll, but will we soon have KDE 2.2/KDEPrint -level ease of use in printing once again?\n\nThere was a time when fighting to get anything useful done in *nix was acceptable.  Maybe I'm older (ok, I am), maybe I've been spoiled by other platforms, but it should no longer be this way.  \n"
    author: "Anonymous Coward"
  - subject: "Re: And what about printing?"
    date: 2008-12-19
    body: "ok, as a reply to my own whining rant, i have finally managed to get cups to define an lpd/lpr printer and got something printed.  some notes/observations (remember, i am using kubuntu 8.10):\n\n* printing was defined via /k-menu/applications/system/printing.  at first, i was looking through the '.../settings/' sub-menu... why are there seperate 'system' and 'settings' sub-menu?  that can be confusing for end-users -- i know i was.  maybe 'system' should be a sub-menu, a category for the 'settings' menu?\n\n* why are there a 'printing' and a '(hp) printer toolbox' menu items?  i do not see why this is the case.\n\n* adding a printer is as easy as it should be.   clicking on the 'new printer' button should bring me right away to a simple question like 'local or remote printer' and then to a 'what sort of connection to printer' sort of question, before asking me for the type of printer (brand / model / etc.).  right now, clicking on the button forces me to wait, wondering if something did not hang in the background.  thank ${deity} there is a way to eventually reach 'cups' to define a printer, though cups is still not newbie-friendly.  ouch, it feels like 1998 again.\n\nanyway, despite my tone and my defective shift key, i hope this can be of some help.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: And what about printing?"
    date: 2008-12-19
    body: "Your problem is not a KDE problem, it's a distribution issue. You can get away with it by choosing a distribution that has more advanced and complete (gui)tools for administration and configuration. No need to force yourself back to '98, when alternatives exist."
    author: "Morty"
  - subject: "Re: And what about printing?"
    date: 2008-12-20
    body: "Oops.  Instead of \"adding a printer is as easy as it should be\" is was supposed to be \"adding a printer is not as easy as it should be\".  The 'not' went missing!\n\nMea culpa!"
    author: "Anonymous Coward"
  - subject: "It's definitly getting there..."
    date: 2008-12-19
    body: "After just using it with OpenSuse inside of Vmware without grahpics drivers installed, I could say I was certainly impressed about the performance increases. \n\nAfter messing around with it, and changing the themes, I ended up with my 7/8th of my desktop being gray(the background color of a desktop application, using the aya theme), and I couldn't find a way to get rid of it. I guess I might file a bug report later. \n\nand after changing themes, the bottom bar at the bottom of the screen(Taskbar? Can't remember.) changed it's width, so I had to change it back. \n\nGood job, especially on the performance, it's not there but its pretty close except for some major bugs I encountered. "
    author: "Jeremy"
  - subject: "Re: It's definitly getting there..."
    date: 2008-12-20
    body: "The background bug is known (see above in the comments), the \"taskbar\" is called \"panel\"."
    author: "Stefan Majewsky"
  - subject: "Printer crash"
    date: 2008-12-19
    body: "Every time I login there's a crash from a printer applet. Is this a known issue?\n"
    author: "Oscar"
  - subject: "Re: Printer crash"
    date: 2008-12-20
    body: "Please check bugs.kde.org if it's a known issue, if not, report it."
    author: "sebas"
  - subject: "Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-19
    body: "I really appreciate the calls to use bugs.kde.org to file reports. It is the right place to do so.\n\nBut I recommend every KDE tester to use wrong (or \"dummy\") email addresses and wrong names. Reasons:\n\no There is no information on how data are used by bugs.kde.org, there is nothing I have to sign in order to allow bugs.kde.org how to deal with my data.\no Filing a bug report means:\n\t(1) You (i.e. your email address and full name) may be included in some mailing lists. When filing a bug report it is not really clear, in which mailing lists you will be included and you can not file one if you do not want to become part of them.\n\t(2) Most mailing lists, particularly lists.kde.org, have public archives. That means: Your email address and full name appear (forever?) in public html archives. No matter, if you were asked or allowed it.\n\t(3) There is no way to remove you from these archives. There is no way to remove a user account on bugs.kde.org\n\nI made the mistake to enter my real name and use my personal email address (I believed KDE is trustable). Googling for my address shows only entries by KDE. And now my email address has been disabled by a few providers since it is used in conjunction with my full name by a SPAM mailer. One even tried to order something on my name. Many thanks.\n\nI contacted the sysadmins - but received only one email without any action.\n\nSince KDE e.V. is registered in Germany I believe German law is applicable. I am no advocat, but to my understanding bugs.kde.org strongly violates German data protection act. \n\nI really hope, that KDE modifies bugs.kde.org and mailinglists software in the very near future! At least the emailadress (and surname) should be invisible in public archives - Why can't it be like ion kde-forums where the email address is totally hidden and one can contact other users by their usernames only? \n\nSincerely\n"
    author: "Sebastian"
  - subject: "I want to add:"
    date: 2008-12-19
    body: "Though my harms are not necessarily related to my appearance in public forums, it is still a not very nice situation if one wants to hide as much publicly available information as possible."
    author: "Sebastian"
  - subject: "Re: I want to add:"
    date: 2008-12-19
    body: "AFAIK the email addresses don't appear plain, i.e. at lists.kde.org my address looks like: <neundorf () kde ! org> , in order to avoid spam.\n\nAbout your name appearing in public: the bug tracker is readable by everyone, so if you enter something, of course your name will be visible by everybody.\n\nEmail addresses should be spam-protected, aren't they ?\n\nAlex\n\n\n"
    author: "Alex"
  - subject: "Re: I want to add:"
    date: 2008-12-19
    body: "I do not know about the capabilities of analyzers apps - but if I look in Google via [Removed by moderator at users request] Google seems to understand what () and ! stand for... I do not want to emphasize the SPAM topic here, however, since my I am concerned by the kind of data usage.\n\n> About your name appearing in public: the bug tracker is readable by everyone\n\no During registration I would like to be informed about this (it's a long time ago, I do not know this has changed)\n\no At least there should be an information page where I can be informed about this fact.\n\no Why does the public bugtracker need to show everything? \n\nShowing a username is sufficient. If I want to contact anybody, I could send him a message using some form on bugs.kde.org which is then sent to his/her email address. Off course, at all time I would never see the real email address and the real name only in the case if the user allowed it. \n\n"
    author: "Sebastian"
  - subject: "Re: I want to add:"
    date: 2008-12-19
    body: "Ups - my bad - hopefully this is moderated and someone can delete the email address I entered above -  sorry! "
    author: "Sebastian"
  - subject: "Re: Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-19
    body: "\"But I recommend every KDE tester to use wrong (or \"dummy\") email addresses and wrong names.\"\n\nThose email addresses are used to send replies back to you from the developers, e.g. asking for more information, clarifiation, testing, etc. So if you're going to use some other email address, please at least make sure it's one you can actually check. Otherwise the mail will bounce nd the value of the bug goes way down since the developer<->reporter communication is broken.\n\nIf you have issues with your email address being in bugs.kde.org's database, don't use it. Very simple.\n\nThis is, btw, how other bugzilla's out there work as well.\n\n\"You (i.e. your email address and full name) may be included in some mailing lists.\"\n\nyes, that's true. the development process involves email communication. the archives are reasonably protected, however.\n\n\"Most mailing lists, particularly lists.kde.org, have public archives. That means: Your email address and full name appear (forever?) in public html archives. No matter, if you were asked or allowed it.\"\n\nit really sounds like you shouldn't be engaging in a public collaboration, then. that's what open source is. if you don't want to be publicly working with others, well ... being involved with open source is not for you.\n\n\"There is no way to remove you from these archives. There is no way to remove a user account on bugs.kde.org\"\n\nSince you seem rather ocerned about this, what would have helped, exactly? we (kde e.v. board) happen to be working on a kde-wide website policy right now, so now is a decent time to provide input.\n\n\"I am no advocat, but to my understanding bugs.kde.org strongly violates German data protection act.\"\n\nI'm no lawyer, either, but publicly claiming someone is breaking the law carries with it some liability where I live. Yay for law."
    author: "Aaron Seigo"
  - subject: "Re: Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-19
    body: "Man, I didn't know people were actually *this* paranoid... _creepy_...\n\nAlso, my name and email (Which is quite like my name) are included... dun dun dun!!!"
    author: "Aaron Nixon"
  - subject: "Re: Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-20
    body: "I am not paranoid but I do not like to get spam. I registered a new e-mail address recently because the old got very much spam. I planned not to publish that address on any website where it could be accessed without login (thus accessible to spam bots). Now I get some spam to the new address. I searched in Google for the new address, and the only hits were mailing list archives originating from bugs.kde.org. So it is very susicious that I get spam because of that."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-20
    body: "\"Since you seem rather ocerned about this, what would have helped, exactly?\"\n\n- At least a warning that the e-mail address might end up in public mailing list archives.\n- (Optionally) forwarding bug reports to mailing lists with dummy sender address. Developers can get the address of the reporter through the bugs.kde.org interface."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-20
    body: "> Since KDE e.V. is registered in Germany I believe German law is applicable. I am no advocat, but to my understanding bugs.kde.org strongly violates German data protection act.\n\nAhem, you entered your email address in the registration form. And the registration form says: \"Due to the open nature of Bugzilla, your email address will be seen by other bugzilla account holders. In addition, emails sent by this Bugzilla installation are archived on public mailing lists and via those archives will be disclosed to anyone reading those archives.\" This paragraph carries the bold title \"PRIVACY NOTICE\".\n\nBy confirming this form, KDE may assume that you accepted the clauses. This step happens in accordance with \u00a74 of the Federal Data Protection Act. You are free to revoke this accordance (see \u00a728), but KDE e.V. cannot revoke any publication on third-party websites, you will have to do this manually."
    author: "Stefan Majewsky"
  - subject: "Re: Data safety and bugs.kde.org - legal issues! "
    date: 2008-12-21
    body: "Ouch! I didn't notice this before, but whoever updated the b.k.o layout really tossed out all the \"security by obscurity\" steps taken by the old templates. I remember having gone through plenty of them for at least making plain email addresses not appear to visitors who are not logged in. This issue apparently wasn't considered by whoever did the new templates, and you are fully right to call KDE e.V. on this.\n\nThe update of the b.k.o templates regressed in plenty other areas as well (navigation and the likes). I wish I could get back into it, I don't really have much time for such these days."
    author: "Datschge"
  - subject: "Still broken as hell"
    date: 2008-12-19
    body: "Still lots of glitches. Reported bugs don't get fixed. Heck, this whole Plasma thing was a mistake - broken by design."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "I reported 3 crashes for Beta2 yesterday. Two were fixed within 8 hours, and 1 had been fixed before I even reported it. One of those was fixed within two hours. True, the issues I reported were corner cases, but the Plasma team has been doing excellent work bugfixing throughout the 4.2 cycle. In my experience I have found no evidence that would seem to confirm your assertion, and since you have given none of your own I will be forced to disagree with you."
    author: "Jonathan Thomas"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "> In my experience I have found no evidence that would seem to confirm your assertion\n\nSorry, I'm not that kind of religious KDE user. Nice to see that you haven't seen any evidences...\n\nI try to find out whether KDE 4 keeps its promises, and in fact, it doesn't. It doesn't provide more usability, it doesn't provide better (intelligent) features. New ideas are good, but the way KDE realizes them, makes everything even more complicated! You may steal ideas from Mac OS as much as you want, but the KDE devs didn't get the point. I'm too lazy to explain what I mean, it's already said more than once. Just open your eyes, dude. Lots of people are crying, not just me, and nobody wants to hear them. All they get is a \"f*** off and use Gnome, MacOS or Windows\".\n\nAll KDE provides is a bunch of buggy, shiny new stuff that can't improve the whole experience in a manner of usability. Hey, it's already 4.2, it's time for a change! Stop telling \"wait for 4.[latest_rc]+1\"!"
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "\"them. All they get is a \"f*** off and use Gnome, MacOS or Windows\"\n\nOr KDE 3 until 4 matures.\n\n4.2 is feature-complete and buggy, perfect beta. (I.e. not feature-complete should be alpha, only-a-few-known-bugs should be rc, no bugs should be stable release.)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "> Or KDE 3 until 4 matures.\n\nPerfect answer, thank you. That's exactly what I was waiting for."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "Well, it's still way better than Gnome, Windows or OS X."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Still broken as hell"
    date: 2008-12-21
    body: "rofl.. if KDE 4 reaches the stability of gnome and it's newer design it's *bang* the best it happened to WM in linux.. if not well.. gnome > than kde 3.5 "
    author: "blabla"
  - subject: "Re: Still broken as hell .. and GNOME halted!"
    date: 2008-12-21
    body: "Well don't expect much from GNOME. The current development is driven by one person. Only Vincent Untz seem to be contributing to all type of core GNOME libraries."
    author: "Sammy"
  - subject: "Re: Still broken as hell"
    date: 2008-12-21
    body: "As with all things Linux-related... repeating it doesn't make it true."
    author: "mayhem"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "If you're too lazy to explain what you mean, then you really should f**k off. \n\nSeriously, the only people who are crying are those so wrapped up in self importance that they demand that their every whim be accounted for. I doubt the GNOME devs would be happy to have such people as their users and as I still quite like gnome so I wouldn't wish that kind of misfortune upon them.\n\nFrankly people like that should really just stop trying to communicate because they obviously suck at it. I used to have to deal with those kinds of people and frankly it just got too tiring to try to acknowledge even the sensible comments in the sea of their trash."
    author: "txf"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "Most people are used very often to treat people with complaints like you did. But, just a moment... either those people keep complaining louder and loader, or they turn away from you.\n\nImagine yourself on the road to hell: A lot of people are shouting at you, \"stop it, please! Think of what you're doing!\"\n\nGuess who you are...?"
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "Imagine yourself in the road to hell: A lot of people are shouting at you, \"Do some contructive criticism! Think in which situation are\"\n\nGuess who will turn away from whom?...\n\nDo you think developers deliberately don't care about users? They will because of people like you though."
    author: "Long H."
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "> Most people are used very often to treat people with complaints like \n> you did. But, just a moment... either those people keep complaining \n> louder and loader, or they turn away from you.\n\nWhich is fine. People that are only complaining louder and louder void of useful information are merely a liability to us, those that vent their complaints in a way that's useful for us (i.e. instead of general statements such as yours reporting bugs.kde.org, showing certain workflows we should support and do not yet, ...) actually improve on things.\n\nSo, if you're not into working with us, the better choice is to silently sit and wait. At least that way you don't keep people from doing their work -- which would be in everyone's interest.\n\nSpreading bullshit doesn't make it any less bullshit by increasing the volume ..."
    author: "sebas"
  - subject: "Re: Still broken as hell"
    date: 2008-12-21
    body: "> those that vent their complaints in a way that's useful for us (i.e. instead of\n> general statements such as yours reporting bugs.kde.org, showing certain\n> workflows we should support and do not yet, ...) actually improve on things.\nThis is simply not true, at least not until the developers in question manage to accept that there are different views on things they care about. For example the plasma developers see plasma and plasmoids as self serving and without context. Looking at them in the context of the desktop as a productivity environment should be higher on the agenda. Otherwise things like the broken design of the cashew wouldn't happen (let me explain):\n- On one hand there is the view that the cashew and it's functionality is so important as to enforce it's visibility without regard for those users that don't want it (or can't - my display botches contrast calibration if the corners and edges aren't black). \n- On the other hand there is the menu that is shown when you click on the cashew but that menu is shown underneath any regular window - although I had chosen to interact with the menu, I can't! \nSo it is so important that every desktop has to have this bloody thing displayed but if I want to use it, it becomes so unimportant that the menu displays in the background!\nI reported this as a bug but it was closed because the developer can't fathom the criticism... Do you think you'd have much incentive to report detailed bugs again if this happens to you?\n"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Still broken as hell"
    date: 2008-12-21
    body: "> Seriously, the only people who are crying are those so wrapped up in self\n> importance that they demand that their every whim be accounted for.\nWell if you took the time to report no less than 20+ bugs over the past weeks with some simply ignored or even closed at the behest of the original developer who happens to not like that you are trying to make KDE work as a desktop... For example the bug that the default setup of the GIMP is resulting in an unworkable mess of windows not being picked up by the diverse window switching ways - some pick up only the image windows some only pick up (with luck, not always - you have to start the GIMP without any images to get to this state) the utility windows as well. If you forget to start the GIMP without an image then the image window will be missing if your image happens to be large (say a full size image from a current DSLR). This mess needs clearing up, not ignoring and dismissing the bug report as \"works for me\" - which for me is an insult as it clearly doesn't work for me (or anyone else using the GIMP default install, the code clearly says so)!\nOr having a bug report closed as being upstream (i.e. Qt bug) without attaching the necessary information to track this bug in the Qt bug database - if they are aware of the issue. The latter part I now doubt as I regularly scan the bugs reports for some areas there as a Qt professional seat holder - amongst which the area where this bug is supposed to be located...\nSo it's not that people like me want to have our whims accounted for, we want to be able to use KDE4 in a productivity area. As someone else already said - and everything some people like Aaron say makes me believe he is spot on: Many developers are limiting their exposure to the area in which they are developing in, they lack the ability to understand that users need certain functionality to have a working desktop.\nIn all honesty: I would take away the ability of the main developer to close or dismiss a bug report without peer review! I currently have numerous bugs that plague my use of plasma but since the developer of that area is so self serving and full of himself that he can't see beyond his own agenda there is no sense in wasting my resources of ever posting anything in that area again - at least until the peer review of the bug reports is implemented!\n\n"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "I'm curious. What distribution are you using?\n\nThe reason I ask is that my setup with a built from svn every day or three is remarkably stable, but the distribution setup that is necessarily a few weeks old isn't.\n\nDerek"
    author: "dkite"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "I'm using Debian, both KDE 4.1.3 and 4.2 SVN are installed on my system. They're separated from each other (installation in a virtual machine exists also). I have one SVN installation linked against Qt from the experimental branch and one SVN linked against qt-copy. I'm doing SVN builds every now and then, trying to get \"good snapshots\" (when no big changes are in progress, I'm fully aware of the SVN difficulty).\n\nThe point is: as long as I \"touch nothing\", everything works quite good. And hell, yes, it looks good! The designers are doing a great job!\n\n...but then: I start to test in a systematic way.\n\nConnect a new monitor. Try to change some deskop related options. Well, actually I try to use some of them. Change themes, effects, options, install plasmoids. Resize this one, remove that one, add another one. Restart, look what has changed. What happens when I disconnect my external monitor (between reboots, I'm not SUCH a bad guy) and use my laptop's native monitor? Plasmoids get messed up, disappear, maybe suddenly are rescaled. Change the digital clock to analog clock. Then change plasma theme. Change order of plasmoids in the tray. Resize the tray again. And so on. Not good, really not! Still SO MUCH side effects, and nothing's going on there. It looks like nobody as a clue how to fix those issues. This is what me makes a sad KDE user.\n\nTry yourself to turture KDE4 just a *little* bit. Maybe compare to 4.1.x and look yourself whether specific things have improved.\n\nBug reports are written, there is one for EACH bug; just take a look at bugzilla. No one can complain like \"you aren't constructive\" at me. I really, really tried hard to do so."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "My Bball is gone again but what the heck, it will come back tomorrow :)\nI have to confirm what you said. The Plasma team is very industrious and alert. I am still having issues with KDE 4 (like my digital camera that it doesn't recognize) but not so much with Plasma. It would be great if all KDE team members could make overtime like the Plasma guys do ;)"
    author: "Bobby"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "I was wondering where all the trolls had gone, but look one came back. awwww"
    author: "Gareth"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "> I was wondering where all the trolls had gone\n\nYes, I'm a troll. Congratulations you've noticed that. And know what? I'm watching almost every commit (honestly!) made since somewhere around KDE 4.0. And what I see is not good: There are really big problems concerning Plasma. Each attempt to fix one issue creates at least 2 new ones. My conclusion is: KDE is on a bad way. Sorry for rubbing salt into the open wound. Broken by design."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "Care to give examples of this brokenness? "
    author: "Gareth"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "systray rendering\nprinter configuration\nkhtml rendering\nwlan/network configuration\nplasma\n...\n\nEnough? No, probably not."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "Ok\n\nSystray icons) - The only icon that still renders badly for me is the kmix one. Compared to earlier 4.x releases that a vast improvement, and from what i understand its not KDE's fault but an X problem (or something like that).\n\nPrinter Configuration) - Works fine for me.\n\nkhtml) don't use it, couldn't comment\n\nwlan/network ) - isn't included in 4.2 because it just missed the deadline for new features. Im sure all distros will include it by default and you can always install youself.\n\nplasma ) - I like how you listed that as an actual example of brokenness when the question was for further clarification of why you think plasma is broken"
    author: "Gareth"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "> Systray icons) - The only icon that still renders badly for me is the kmix one.\n\nIn your case, perhaps. But it surely isn't a kmix issue, isn't it? In my case, all icons are rendered badly. Whenever I expand the systray, it overlaps the clock. Sometimes this \"expander\" thingy isn't shown at all. Sometimes I can't click on any icon because of that. Sometimes it looks simply... ugly. Strange systray look SINCE KDE 4 EXISTS. Broken by design.\n\n> Compared to earlier 4.x releases that a vast improvement, and from what i understand its not KDE's fault but an X problem (or something like that).\n\nMaybe it's not KDE's fault. Maybe not... and maybe KDE relies on some functionality which can't provide a good systray rendering for KDE/Plasma. It looks like it can't get fixed. Bug reports about graphical issues are open SINCE KDE 4 EXISTS. Broken by design.\n\n> Printer Configuration) - Works fine for me.\n\nGood for you. I still can't get my printer configured without the CUPS web user interface. Sorry, I'm just a stupid user.\n\n> plasma ) - I like how you listed that as an actual example of brokenness \n\nTry to enlarge your panel \"a little bit\" too much; background wallpaper disappears, or at least, it turns grey. Either that or the panel's background color changes, or maybe disappears, too. Attached Plasmoids beave suddenly strange. Try to move your panel from the bottom to the top, resize it a bit. Desktop background wallpaper turns grey, needs to restart Plasma. Widget positions aren't saved sometimes, or scaled to small. Or rendered completly wrong. And so on... doesn't seem to get fixed. Same strange things are going on SINCE KDE 4 EXISTS. Broken by design.\n\n> wlan/network ) - isn't included in 4.2 because it just missed the deadline \n\n...same as using systemsettings with root permissions, isn't it? So much for \"wait until 4.x\""
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-19
    body: "The using system settings with root permissions is a bad idea. Aaron explains this in some other posts above.\n\nSure moving my panel to the top edge causes its colour to change, thats because my wallpaper is a different colour at the the top and the panel gets tinted based on the colour of the wallpaper behind it. If you don't like that feature you can change the panel .svg in the system settings.\n\nExpanding my panels height above half the height of the screen does indeed cause the wallpaper to screw up. But hey its a beta, this is why we have betas, and its hardly the worlds most annoying bug.\n\nAs far as the systray thing goes, i'd hardly call refusing to include dirty hacks to get around upstream deficiencies \"broken by design\" infact i'd call it the exact opposite."
    author: "Gareth"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "> The using system settings with root permissions is a bad idea. Aaron explains this in some other posts above.\n\nOkay, I'm eagerly waiting for another way to configure the login screen (and network shares). So this configuration obviously won't take place in system settings(?) ...very interesting, thought that system settings will be the central place for configuration activities. I wonder why the administrator button is still shown on the roadmap to 4.2.\n\n> Sure moving my panel to the top edge causes its colour to change, ...\n\n...it also changes its color when you only resize the panel, without moving it from bottom to top or vice versa. It simply forgets the panel background under some circumstances, gets completely transparent. Sometimes it suddenly uses the opague background SVGZ despite of active desktop effects. The Plasmoid's (default) look don't respect the system's color scheme in any way. Finally, change the Plasma theme and nothing looks like it is expected to look.\n\n> ...But hey its a beta, this is why we have betas, and its \n\nThis one existed a long time. From earlier-than-4.1 to 4.1 beta to latest 4.1.x, from SVN to 4.2 beta1 up to 4.2 beta2. I BET this one will still exist in the so called \"final  release\" - remember my words. And when it does, someone will say, \"this is a x.0 release, and this is why we have x.0 releases\". The important thing is: It's not just the background wallpaper, there are a lot of thing who are treated EXACTLY the same way."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "SystemSettings > Advanced > LoginManager\n\nTo setup a share you right click on the folder and under properties you select share.\n\nIm tired, my bed is more appealing than carrying this one,"
    author: "Gareth"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "> SystemSettings > Advanced > LoginManager\n\nRequires root privileges (*administrator button), only possible using \"...kdesu systemsettings\" which is some kind of hack.\n\n> To setup a share you right click on the folder and under properties you select share.\n\nOkay. This is a good one. I surrender.\n\n> Im tired, my bed is more appealing than carrying this one,\n\nI finally agree with you."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "\n>Strange systray\nIf I recall the AWN people (Avant Window Navigator: the composited osx like dock for gnome) complained about the systray spec long before even kde 4.0 was close to being released. They didn't include the systray by default because it looked and worked too badly (I remember trying to use it...urgh)\n\n>printers\nme, I use yast for printers...seems to work fine. And apparently you managed to get it working too. \n\n> wlan/network\nwhats wrong with network manager? or maybe you could just install wicd? Besides it only missed the feature freeze, and it will be released independently but during the 4.2 timeframe and as a result most distro's will probably include it. And seeing as you're a \"stupid user\" then you'll be waiting for a distro use 4.2 right?\n\n>plasma issues \nconsider writing helpful bug reports to maybe help in fixing a personal bugbear of yours?"
    author: "txf"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "> They didn't include the systray by default because it looked and worked too badly\n\nThank you, this is a very interesting piece of information.\n\n> consider writing helpful bug reports\n\nBelive me, I'm doing this since... a long time... IIRC my first installation was a 1.x release. Not quite sure about that. And I'll keep on doing this.\n\nI'll investigate the network manager thing tomorrow. But nevertheless, I already tried hard to configure WLAN+OpenVPN using a native KDE 4 tool, you know. This is not exotic.\n\nGood night."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "Don't hold your breath about networkmanager in KDE4. It's not ready yet, not even basically usable. We're trying to make it work in time for spring distros to include it.\n\nOtherwise, it's hardly a real problem, as both, knetworkmanager from KDE3 and GNOME's nm-applet work just fine.\n\nExotic or not, it needs implementation, which needs time, and also some patience on the user's side. As many KDE developers put in countless hours, it's reasonable to expect the same from people like you.\n\nBeing Christmas and all, I do wonder what makes you so sad. The list of changes is huge, still you complain about things that aren't done yet. That's normal, software won't ever be perfect. It's disappointing to see people not acknowledging the progress made though.\n\nBe happy :)"
    author: "sebas"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "Well, thank you very much, everyone appreciates it to be informed what's going on, respectively why some things are not going on. I hope the network manager thing will make it at least into the 4.2.1 release. I wonder how the login stuff is going to be solved, without raised privileges, you know.\n\nAsides my \"I behave like an a**hole rants\", I'm going to tell you a little anecdote. This one is exactly happened to me as described.\n\nIn my faculty were exactly 2 people using KDE. My KDE fellow was using 3.5.9 on an OpenSuSE installation, and me running KDE 4.1.2 (at that time) on Debian. There were really lots of people using Ubuntu (this eeePC thing is very popular), there was another \"Debianist\" somewhere around me (I smelled him) and I saw an Arch Linux and a Gentoo guy, both sitting in the middle of the first row of course.\n\nKnow what? From about 50-60 Linux people (~160 people in total) exactly TWO were unable to get their wirless/openVPN network connection configured using GUI only. Now, guess who they were!\n\nI ended up with hacking myself through wpa_supplicant (knetworkmanager was not able to handle our specific openVPN settings, and before I'm going to install this Gnome stuff I'll buy a Mac, I swear). ...and my KDE colleague finally gave up, after 2 or 3 months or so, and installed Vista. Couldn't help him with his WLAN, all I could do was providing a gateway over my Laptop using a cross linked network cable. No joke.\n\nNow I'm the last one holding up the KDE flag, but..... the times are getting harder and harder."
    author: "Sad KDE user"
  - subject: "Re: Still broken as hell"
    date: 2008-12-20
    body: "Why do you insist that KNetwork manager should be part of the main KDE packages in the KDE 4 series. It has never been before, for KDE 3 it also was a 3rd party package(extragear), so nothing has changed there. \n\nAnd I don't even think it makes very much sense to make it a part of the main KDE packages, as it will have several major drawbacks. It need to conform to KDEs rather long feature freeze/release cycles. Making new features come slow to the end user. This will most likely lead to distributions to ship backports/prereleases rather than the stable versions. The ability to ship feature increased releases and bug fixes at a faster pace than the main of KDE, are a very good thing for fast evolving applications. This is shown clearly by successfull applications using this strategy, like KTorrent, Amarok and K3b.\n\nAnd one more important factor making this a bad idea for KNetworkmanger, are the thing that makes it tick in the first place, the Networkmanger backend. And it is not synced with KDE releases in any way. Giving a situation where new features in Networkmanger will not be availble to KDE users until the next major KDE release, with the current scheduling of KDE the the worst case will be up to seven months delay. Where as if they keep it a 3rd party applications, the delay will only depend on how fast the developers work."
    author: "Morty"
  - subject: "KDE4 bad fluidity"
    date: 2008-12-20
    body: "I was a convinced KDE user and never used GNOME more than an a few minutes ; this makes me sad but I'm certainly going to use GNOME from now on... Sad because I found the ideas and concept of KDE4 very promising, and even started to dev in C++ using kdelibs but I can't use KDE4 since 4.0, this is not changing I'm loosing hope this is going to change.\nOf cours there were features missing, lots of crashed, bugs, glitches and it was pretty slow. Now I must admit that I recovered every KDE3.5 features I used and I only have a few bugs and crashes on kde4.2 beta but one thing hasn't change at all since the beginning : painting performance, glitches and the lack of fluidity. Of course oxygen looks beautiful, when you see a screenshot.\nBut damn, painting is so slow, it flickers from everywhere, some actions provoke sudden slowdowns that makes kde4 look very lag-ish. I'm not speaking of KDE4 in general, because applications run very fast, I'm only speaking of drawing and painting performances. This doesn't look like a final product at all.\nMaybe this is because I've got a Nvidia 8400M GT, but I tested different versions of KDE4 on 5 to 10 different configurations on different PCs, with different graphic cards, and the persons who watched the result weren't convinced at all, like me.\nI mean, even without composite, displaying dolphin window when minimized take fractions of second (up to 2 sec) to display its content. In fact, something must be painted, I can see a white or grey zone before something is displayed. This can be very fast (but even here, it looks VERY bad : imagine when ou maximise konqueror and see a fullscreen grey zone before everything it displays), or take up to 2 sec! (rare but it happens).\nShowing menus in applications, displaying different tabs in applications etc. is globally slow. Not slow like 2 sec are needed for everything to be displayed, but slow like you can't feel any fluidity while you're working... Basically, every action that needs things do be repainted takes time.\nThe worst part is when you resize windows ! It seems that kde users are accustomed to this and don't even notice, but when I show it to Windows or Mac users, it strikes them (and still strikes me!) : it is slow as hell, there are grey and black rectangles where the different elements of the window are moved, and it is very choppy.\nSame example when you move the \"sub-windows\" of dolphin, there is a sudden slowdown when you detach it or when you replace it somewhere and the different parts of the window move (I assume for the same reasons of resizing windows slowdowns).\nOh I forgot, the blur effect while logging in is very choppy as well.\nNot to mention KDE's general flickering, for example when you move \"sub-windows\" of dolphin, again : when you replace it, it alternatively displays it and the blue rectangle behind. It also occurs when you resize windows,you can see window decorations and icons flickering... If you try to be observant for a while, you'll notice that KDE flickers a lot and you can see things being redisplayed when this isn't needed. (Like the systray which is redisplayed when you switch desktop). \nTo summarize a bit, KDE4 doesn't look fluid at all because of painting slowdowns and flickering, maybe you can't see it because it's a matter of habit, but use mac or gnome for a bit and you'll see the difference.\nI'm telling this because I'm installing a new system for someone and had to choose a desktop environment, therefore I gave a try to gnome, and I rediscovered what fluidity means. \nGenerally speaking I prefer KDE to GNOME, but I can't bear lag-ish environments anymore, so I'm installing GNOME at the moment I'm typing.\nSeriously, kde devs put efforts in what they're doing and it's a shame that this is all ruined because more than 50% of users can't use it because they don't have hardware that fit kde or qt's needs and makes KDE almost unusable.\nThis is so frustrating.\nNow that KDE4 doesn't lack big features anymore, the priority is to make kde usable and fluid for everybody, like Gnome, if you don't want to lose your userbase because of the kde3->kde4 transition.\nThis is sad but KDE4 didn't work good \"out of the box\" on the 8 machines I tested it, and it was the same for most people around me, while gnome was flying.\n"
    author: "Gopa"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-20
    body: "Sorry for the approximate english sometimes and the few missing linkwords (I should've checked before posting :p)."
    author: "Gopa"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-20
    body: "My experience is same as yours, but I usually don't comment it as it's more of a QT problem than KDE's problem.\n"
    author: "vf"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-20
    body: "the lag you experience is from powermizer. \n\nMe, I've disabled powermizer because it causes lag everywhere, even in GNOME.\n\nHowever the reason that gnome seems faster is because it requires less of your hardware than the new stuff in kde.\n\nAnd this is even before we start mentioning the situation as it was before nvidia fixed their drivers to work better in kde."
    author: "txf"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "Hi\n\nCould you please report it or even better contact me (boemann) on irc (freenode) in #oxygen\n\nObviously we aren't aware of these issues, and it's only pure luck that I read this comment. (comments are a really bad way to report any bugs).\n\nAnd we need much more info than a comment can give. We need to have a person to contact and help us diagnose it. Fixing a problem that we don't see and aren't even aware of is obviously never going to get fixed, so the only way is for you to take an active part in trying to resolve it (this is what opensource is all about).\n\nAs for you choosing gnome over kde if all you problem is just the oxygen style being too slow, then I suggest using a different style. Replacing the entire DE sounds a little extreme to me. But I'd really prefer if you (or anyone else) took the little effort of helping us resolve it. After all we are giving you a lot of our spare time to solve your problem (remember we often don't encounter those problems ourselves or we would have fixed them). The least thing you (any of you) could do is to give back a little effort of your own to help resolve issues.\n"
    author: "Casper Boemann"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "Well, I'm kinda busy now (I mean, this entire year :p) but I'd like to help so if I have some spare time tomorrow or probably later this week I'll try to report it (never done this before).\nHowever, I don't think it's an oxygen-related problem, it's the same for other themes. Actually if I remember well painting is slow as well with pure qt when I'm in a gnome session... I'm convinced it depends on Qt and/or my nvidia drivers (latest beta or stable work the same...).\nWell as for flickerings and frequent repaints, I don't know though...\nAnyway, it's strange to see KDE developers (and I thank them for their efforts) being all enthusiast about how amazing KDE looks and how blazingly fast it is, when I haven't seen with my own eyes one fast KDE4 installation... Do I only have bad hardware ?! (When I say fast I mean in terms of painting performance)\nEven the intel cards I tested are quite slow with KDE4, and nVidia cards as well, despite the work of nVidia engineers. Actually, I'm astonished by the few amount of people complaining about such an important issue, either I have bad luck with hardware, or people have forgotten what fluidity is on other desktop shells or OS... \nIt's strange, the few people around me who used kde3 still cant use kde4 for the same reasons and are either staying with kde3 or switching to gnome.\nI think it's important to have a usable kde4 soon ; most distributions are switching to kde4 with their newer version, and people who want the latest and can't use kde4 may simply use another environment..."
    author: "Gopa"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "So ok I'll try to contact you on IRC tomorrow I hope, but I really think this has little to do with oxygen, obviously this is a more general problem, linked with the drivers or qt."
    author: "Gopa"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "try the new nvidia driver, it's impressive how much faster everything is"
    author: "Beat Wolf"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "Yeah everybody says that but I already have latest beta driver (180.16), of course it is faster but it seems people got used to slowness or forgot how fast it should ! Or am I the only one that have a choppy kde4 with the latest driver ?\nMaybe you should try kde3 again, or macOS or gnome to see the different when you just navigate in menus, minimize, maximize windows, scroll up and down in dolphin, konqueror or firefox... I did and it was a shock to use kde4 again. I mean, not like I have huge slowdowns but everything takes a little time to be painted, there are small lags when scrolling etc. This is not fluent like it should be."
    author: "Gopa"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "I'd like to file a bug report but I don't even know who's responsible ! KDE ? Qt ? nVidia ? X.Org ? I mean it's not a particular bug related to a single application!\nMaybe I should post a video somewhere and show all the choppy things. Maybe people with nVidia cards may have got used to it. (well actually I doubt it a little, how come people can't notice that...)\nOr I'm just an isolated case ? Again, I doubt it, plus kde4 was also choppy on all the other configurations I  tested, and there weren't only nVidia cards (though the worse is with my 8400M GT)."
    author: "Gopa"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "> Maybe I should post a video somewhere and show all the choppy things.\n\nHehehe, I had the same idea. Let's see how much glitches we can show in, say 30 seconds or something. Probably 1 per second or so."
    author: "Sad KDE user"
  - subject: "KDE 4.1.85 is not suitable for production use but"
    date: 2008-12-21
    body: "... but meant to invite feedback and bugreports from the community.\n\nYou've been  missing that sentence!"
    author: "Sammy"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-21
    body: "I can confirm the same as what Gopa is saying.\n\nI have two test rigs with openSUSE 11.1 running KDE 4.1.3.  One of them has an AMD 2400+ with 2GB RAM and an nVidia GEForce FX 5 graphics card running nVidia 173.14.15 driver and the other an AMD 2200+ with 1.25GB RAM and an nVidia GEForce 6600 running nVidia 177.80.\n\nI leave the desktop effects to being enabled.\n\nWhen resizing windows there is a noticable/serious lag, just as Gopa described.  When resizing the Kickoff menu it flashes until stop being resized.\n\nWhen restoring windows from the task manager it doesn't go smooth.  I remember Compiz Fusion in GNOME being loads faster.\n\nOverall, from the KDE 4.1 days it is a lot faster.  However, I think by default SUSE modifies the environment variables initialPixelPlacement and GlyphCache.  If these weren't dealt with then KDE 4 would probably be unusable.\n\nIf desktop/window rendering was as fast as it was in KDe 3.5 then I wouldn't give a damn about the other bugs.  I know that the devs are working hard and I wish them all the best.  KDE and GNOME are great desktop environments and my hats off to those maintaining these software projects.\n\n---\n\nSulligogs"
    author: "sulligogs"
  - subject: "Re: KDE4 bad fluidity"
    date: 2008-12-29
    body: "Hi mates,\n\nUnfortunately, I must also join to the group of users experiencing slow painting performance on KDE4. I'm using ArchLinux with KDE 4.1.3, qt-4.4.3 and Xorg 7.4.\n\nMy PC specs are:\n- Athlon Thunderbird 1200 MHz.\n- 768 MB DDR memory.\n- ATi Radon 8500 LE (R200 engine) 64 RAM\n\nI notice that KDE behaves laggish when maximing o minimzing a window. Besides, when scrolling up or down the screen moves choppy and you can see how the new regions of the screen are painted. In an attempt to shed some light on this issue, I have done a small test I don't know whether it would provide us any useful information: I have a cpu monitor on my desktop and when \"nothing\" is done, it consumes around 3-6% of CPU. However, when I simply maxize a window the CPU usage goes up to 70-80% more or less. This apparently indicates than the graphic card 3D features aren't used but instead the CPU is loaded. I don't get to find a more logical explanation to such an intensive CPU usage.\n\nI'll try to overhaul my xorg.conf file and find out if anything else can be done. I have this settings currently:\n\n....\n        Identifier  \"Card0\"\n        Driver      \"radeon\"\n        BoardName   \"Radeon Radeon 8500 LE [R200 QL]\"\n        BusID       \"PCI:1:0:0\"\n        VendorName  \"ATI Technologies Inc\"\n        # Options recommended on dri.freedesktop.org\n        Option      \"AccelMethod\" \"XAA\"\n        Option      \"AGPMode\" \"1\"\n        Option      \"AGPFastWrite\" \"1\"\n        Option      \"GARTSize\" \"64\"\n        Option      \"EnablePageFlip\" \"1\"\n        Option      \"ColorTiling\" \"1\"\n....\n\nIf you see anything that can be change to improve the painting performance, just let me know.\n\nBefore ending this post, I want to thank the whole KDE team for the great job and just want to encourage them to find a solution this.\n\nBest regards! :-)"
    author: "Musikolo"
  - subject: "Re: KDE4 bad fluidity"
    date: 2009-01-14
    body: "Option \"AGPMode\" \"1\"\n\nsets the video card into 1x mode instead of a possible 4x, I would just remove all those options, xorg and drivers do a pretty good job of configuring themselves nowadays."
    author: "Tony Murray"
  - subject: "Maybe change distro"
    date: 2009-01-08
    body: "I noticed the same problem using KDE 4.1 on Kubuntu.\n\nI installed Mandriva (but others distro may be good as well, I don't know). And the smoothness is back again !\n\nEven with compiz-fusion enabled, I find it really performing well.\n\nAt least try the one live distro: http://www.mandriva.com/fr/produit/mandriva-linux-one\n\nRegards,\n\nGC.\n"
    author: "GCollin"
  - subject: "@all KDE developers: Congratulation"
    date: 2008-12-20
    body: "My first impressions are realy good!\nIt's more responsive than 4.1.3 and i like the notifications? !!!\n"
    author: "plan9"
  - subject: "Will KDE 4.6.0 actually be stable?"
    date: 2008-12-24
    body: "I have been using SVN versions of KDE 4.x since 4.0 and I must say that I am deeply disappointed on the way it is progressing.\n\nIt is still so buggy that I consider it alpha software as of KDE 4.1.85 (KDE 4.2 beta2). Bugs who have been present since 4.0 are still there. New bugs have been introduced. A few bugs have gone away.\n\nI do not recommend using any version of KDE 4.x for anything other than testing purposes. KDE 3.x can be used as a desktop in corporate environments. KDE 4.x? Forget it.\n\nWill KDE 4.2 be usable and \"stable\" enough for even personal use? No. How about KDE 4.3? At this rate.. I doubt it. How about KDE 4.6.0? At this rate, maby.\n\nPerhaps it would be a good idea to get very basic things like proper proxy support, support for two monitors (supposedly works on some configurations, does not work if you you two graphics cards for example) and other _basic_ functionality working before adding new features?"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "KDE4 sucks"
    date: 2008-12-29
    body: "I have been a user of kde from the beginning. I don't like gnome, and I have only managed to use it for a few hours at times. Kde3 was the best linux DE.\n\nKde4 promised a lot and I still pray it will deliver (at least what kde3 already had.) But what we get at this point, is:\na) Everything is missing, and what is left is slow and crashing.\nb) Applets, ie: info/(screen real estate) -limit-> 0\nc) A new theme (that I don't even like)\nd) New names (cause with names like eg. \"kpdf\" you might actually understand what the app does. And we wouldn't want that.)\nd) Stupid UI decisions that offer nothing and actually make things worse. (Ie, I don't know if it just kubuntu 8.10, but when I want to hibernate (and that is 90% of the times I close my computer), \"hibernate\" is not offered in the \"leave\" menu. You have to double click the \"turn off computer\" option to get a sub menu for hibernate. I suppose that this is supposed to make the main menu \"easy\" and de-cluttered. But if you miss the double click, the comp just turns off, and there go the 20 open apps that are running at the moment. Also, when you double click, a counter starts that tells you that the computer will close in 30 secs. Do I actually pay electricity for this sh*t? Well, at least if I suddenly die of, say, a heart attack, at the very least I'll go happily as the computer will close in 30...29...28... and even dead I would have saved the planet. WTF? Everything is stupid.)\n\nNow that I said stupid. WTF is dolphin? Just make konqueror from the beginning, just FUBARed, with stupid defaults about \"usual things users do\"? There is no such thing as \"usual things\". Where is all the f* functionality? You will be at the place you started after a couple of years, and that would be a \"success\"? Or just a present to M$ to have time to improve?\n\nAnd you can't f* save on the desktop? WTF? ...I think I read somewhere that if you install a specific applet then you can. Well. Suggestion for KDE5: \"KDE5: Now you cant even save on the hard disk!!!\" (That will teach them who \"innovates\". Anyhow, we all just use the \"cloud\" nowadays, don't we? So who cares for HDs?\n\nKD4: WTF?! Dude, where the F* is my f*ing desktop?\n\nThe best linux DE just commited suicide. It is sad in general, and very sad in particular for myself, as I am left in a situation that I don't know what DE to use.\n\nI pray to all the flying spaghetti of the world to do something, but I am losing hope.\n\nKde4 is a train wreck gone wrong :-( (And that is not a double negative. It is a train wreck squared).\n"
    author: "George"
  - subject: "Re: KDE4 sucks"
    date: 2008-12-30
    body: "Your posting contains quite some insults. There are people spending lots of work into developing and improving a free desktop solution and you insult them. Please instead join the crowd and help improve things from within.\n\nKDE 4 is different from KDE 3, you have to get used to it. The way to do things in KDE 4 is different, but it is not worse. Its sort of a completely new concept. It has to mature, for sure. Do not compare it with the 3.x version which has a long history and is quite finished. Compare it e.g. with KDE 3.1 to be fair.\n\nYour hibernate problem is rather not KDE related. I also use Kubuntu 8.10 and have hibernate there right in the K menu all the time. Mind you, I use the new style K menu, not the classical one. This point should be discussed more in depths to find out what the problem realy is.\n\nIf you do not like the theme, than switch to an old theme. It is not that 100% straight forward, but most components of KDE 4 can be switched to a mode where they look more like KDE 3.\n\nYou do not like dolphin? Change the default application to Konqueror via system-settings / default components (probably has a different name in English).\n\nKind regards,\nStefan\n"
    author: "StefanT"
  - subject: "Re: KDE4 bad fluidity"
    date: 2009-04-12
    body: "I've just installed Fedora 11 Beta with KDE 4.2.1 - I just had to see if anything new had come along and I am happy to say that there has.\r\n\r\nOriginally, I moaned that desktop rendering was not up to scratch.  Resizing and moving windows can feel awkward with bits of blank overlays taking a couple of seconds at a time to correct themselves.\r\n\r\nHowever, it now actually appears that these annoyances have been rectified.  Browsing in Firefox is extremely smooth, just like Firefox on Windows is.\r\n\r\nSo, my hats off to the KDE devs.  I am sitting here with a graphical desktop on Linux that once more seems to be on a par with KDE 3 in terms of rendering speed.  I like it a lot!\r\n\r\nHere's to the future of desktops.  All the best, KDE devs."
    author: "sulligogs"
---
Two days later than initially scheduled due to yours truly preparing for coming year's desktop summit on Gran Canaria, KDE's release team has made availabe <a href="http://kde.org/announcements/announce-4.2-beta2.php">KDE 4.1.85, a.k.a. KDE 4.2-Beta2</a> to testers and reviewers, codenamed <em>"Canaria"</em>. KDE 4.1.85 is not suitable for production use but meant to invite feedback and bugreports from the community.
The KDE community is in massive bugfixing mode, showing a focus on stability and feature-completeness in the KDE 4.2 series. But behold, 4.1.85 is not a boring release. It brings many visible improvements to KDE 4.1, and will ultimately follow up the KDE 4.1 series as a stable release this coming January. So get your testing gear ready and help us squash those bugs for a 4.2.0 that makes Redmond see flying chairs all over. There's a changelog over at <a href="http://techbase.kde.org/Schedules/KDE4/4.2_Changelog">TechBase</a>, and the brave and bored can go for the more detailed <a href="http://techbase.kde.org/Schedules/KDE4/4.2_Feature_Plan">feature plan</a>. As 4.1.85 is the last release before Christmas, we strongly recommend using <a href="http://wadejolson.wordpress.com/2008/12/14/enough-holiday-wallpapers-to-gag-on/">Wade's Christmas wallpapers</a> and the KWin compositing snow plugin while testing.


<!--break-->
