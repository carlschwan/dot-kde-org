---
title: "KOffice 2.0 Alpha 8"
date:    2008-06-12
authors:
  - "cberger"
slug:    koffice-20-alpha-8
comments:
  - subject: "Go KOffice go"
    date: 2008-06-12
    body: "Girish's work seems to be really impressing (http://labs.trolltech.com/blogs/2008/06/11/testing-typography).\n\nI hope KOffice will gain some more developers soon. And may they be as talented as the current ones.\nA small team gaining great progress - did I mention that I'm impressed? ;-)"
    author: "Birdy"
  - subject: "Bug reporting"
    date: 2008-06-12
    body: "If I want to take a look at KOffice 2, and give some bug feedback, can I use the Windows verison?"
    author: "Axl"
  - subject: "Re: Bug reporting"
    date: 2008-06-12
    body: "Yes, sure."
    author: "Boudewijn Rempt"
  - subject: "Re: Bug reporting"
    date: 2008-06-12
    body: "you are welcome, and if you have any related kde on  ms windows questions, just join #kde-windows  "
    author: "mimoune djouallah"
  - subject: "Shared testing framework"
    date: 2008-06-12
    body: "It's great to see KOffice really supporting and progressing ODF, and test driven development really seems like the way to do that.\n\nIs that something that can be done collaboratively? I mean can KOffice, OpenOffice and others share a testing/compliance suite, or is it kde/qt tied?\n\n\nCould it possibly even have it maintained in OASIS. That could make it far easier for other independant implementations to develop."
    author: "Steve"
  - subject: "Re: Shared testing framework"
    date: 2008-06-12
    body: "There is a testing/compliance suite (can't remember the link). But the work that is done currently on the framework is tied to KOffice, since it's automatic testing, and it basically works by loading a document and checking that the internal state (aka Qt/KOffice structures) is correct, and that part is not sharable."
    author: "Cyrille Berger"
  - subject: "Re: Shared testing framework"
    date: 2008-06-13
    body: "There is a testsuite: http://develop.opendocumentfellowship.com/testsuite/\nThe results are not up to date.\n"
    author: "AC"
  - subject: "Re: Shared testing framework"
    date: 2008-06-13
    body: "The results have not been updated, indeed.  But the OpenOffice results were re-tested just a couple of months ago (at least all the failing ones, to see if they started passing) there were virtually no changes at all. Only one test started passing.  I think the OOo people are not really using the results to find bugs and improve OOo :/"
    author: "Thomas Zander"
  - subject: "Re: Shared testing framework"
    date: 2008-06-15
    body: "Hello,\n\nin all fairness, did you consider that OOo people are probably rightfully busy fixing bugs that their users find annoying?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Screenshots?"
    date: 2008-06-13
    body: "You know I have to ask - screenshots, anyone?"
    author: "Morten Juhl-Johansen Z\u00f6lde-Fej\u00e9r (mjjzf)"
  - subject: "Re: Screenshots?"
    date: 2008-06-14
    body: "\nI like to see screenshots too! If possible, from KDE4.x so it's using oxygen ;)\nWhat I am waiting is the paper sheet has space around it so all sheets are not together in one line but there is 1 inch space around it, like on openoffice or on word. Yes, it's just a small outlook wish but it has great meaning for usability when you can feel that sheet is paper and not just a white area what trye to mimic a paper.\n\nSo if someone has screenshots from kword with first page top edge seeing and the join part of two sheet."
    author: "Thomas.S"
  - subject: "Oh my god!"
    date: 2008-06-14
    body: "KOffice running on Windows is so here.  Although I have a few crashes, the fact that it is running on Windows excites the living pants out of me.\n\nI'm looking so forward to stable KOffice 2.0 on Windows.  This will usher a new era of free software that never foreseen before.  The days of Microsoft Office will be numbered.  Those who continue to pay for Microsoft Office will be left in the dust as the momentum of KOffice builds.  \n\nGo KOffice go!"
    author: "KOffice rulez!"
  - subject: "Re: Oh my god!"
    date: 2008-06-14
    body: "openoffice is way better and it hardly dents ms office."
    author: "oh yeah?"
  - subject: "Re: Oh my god!"
    date: 2008-06-14
    body: "OpenOffice is a clone of MSOffice, their only argument is price. We are working on improving the user experience. Will we succeed, only the future will tell."
    author: "Cyrille Berger"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "> openoffice is way better and it hardly dents ms office.\nOOo is a super-bloated M$-Office mirror image. KOffice is fast, light, and better.\n\nYou cannot be better if you are exactly the same."
    author: "Riddle"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "When I say \"better\", I mean better than M$-Office; OOo does have some advantages over KOffice (better GNOME integration than KOffice, for example)."
    author: "Riddle"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "> OOo does have some advantages over KOffice (better GNOME integration than KOffice, for example).\n\nNow _that's_ a convincing argument.\nListen up KOffice devs: we need better GNOME integration"
    author: "Forrestry"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "It is a convincing argument for GNOME users. However, I've got the feeling even THAT will be fixed without the KOffice developers writing a single line of code for it (http://labs.trolltech.com/page/Projects/Styles/GtkStyle http://labs.trolltech.com/blogs/2008/05/13/introducing-qgtkstyle/).\n"
    author: "Riddle"
  - subject: "Re: Oh my god!"
    date: 2008-06-16
    body: "That and it has less features than OpenOffice too.\nThe only problem left is that there are too many K's and the names have almost no G's. ;-)"
    author: "AC"
  - subject: "Re: Oh my god!"
    date: 2008-06-17
    body: "I don't want to GNOME-ify KDE. I was simply stating, that since GNOME accounts for about half of Linux, GNOME support on OOo was probably part of the reason for OOo's popularity as an Open Source application. I never meant that the KOffice developers should port KOffice to GTK+ (that work is being done by TT ;)."
    author: "Riddle"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "With my LaTeX background, I always relies on heavy use of styles. I cannot live without styles, and OOo make the use of styles a really pleasure. \nThe M$Office styles implementation is painful, you need to go trough many windows that pops-up every time you need to change one small thing.\nBut the point that interest here is that the style implementation on koffice is weak: no page styles, no deal.\nI would love to use an office suite fully integrated with my kde (forgot the guy to talked about gnome integration), but kword on the 1.6 era is unusable for me, and I'm not sure if version 2 will solve my needs.\nThe things that a real work processor needs, IMHO, are:\n- hierarchical paragraph styles\n- hierarchical character styles\n- hierarchical page styles (in this point, even OOo have problems with wonderful page styles that _not_ act hierarchically)\n- fields of any kind, either automatically generated or custom, that gives simple but yet powerful cross-references, variables (both numerical and textual), tables of contents, table of figures, etc.\n- Have I mentioned styles?\nI don't like the gtk-java-monky-tonky stuff that is growing inside OOo, nor the way in which the community problems are handled, but Writer is a TEXT processor based on styles, which is quite different than a WORD processor based on frames.\nPlease, give me strong reasons to believe on kword, OOo looks awful on my kde desktop. Right now, the only koffice app that I use regularly is Krita."
    author: "RGB"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "I've been using OO.org up to this point because of it's better functionality. But  I've recently tried the koffice 2 spreadsheet and liked it; it basically does everything I need. There were some things that didn't work quite right but that's understandable since it's still in alpha.\n\nI'm looking forward to switching to koffice; for me there are several advantages. I'm using KDE and of course koffice has better KDE integration. And as you say it's faster and lighter; oo.org takes 8 seconds to open on my AMD 64 X2 box which is ridiculously slow. I'll be using KDE apps for almost everything so there should be better sharing of libraries. "
    author: "Jeff Strehlow"
  - subject: "Re: Oh my god!"
    date: 2008-06-15
    body: "+1. DIE MS-OFFICE DIE!!!!!!!!!! AND NEVER COME BACK!!!"
    author: "Riddle"
  - subject: "Re: Oh my god!"
    date: 2008-06-17
    body: "pffft... from all the things they did in Redmond, Office is quite o.k. It's working as an office suite. Not the worst peace of software, really. I think, even if Microsoft gives up on the lock-in with stupid file-formats, MS Office would still attract some buyers (but the competition would nevertheless grab its 50% market share)\n\nI would go for KOffice anyway as I like openness in terms of code and content... ;-)"
    author: "Thomas"
  - subject: "Re: Oh my god!"
    date: 2008-06-17
    body: "I also like Open Source code. My main problem with both MS-Office and OOo is that they are big. OOo being big is understandable, since it is cross-platform, but Office is not, so it's Just Plain Bloated."
    author: "Riddle"
  - subject: "Screenshots"
    date: 2008-06-17
    body: "No one has test or have any screenshots? Still waiting.... :-)\nimageshack.com or other place would be nice too to have those... :-)"
    author: "Thomas.S"
  - subject: "Re: Screenshots"
    date: 2008-06-17
    body: "Just grab KDE4Daily and play with KOffice yourself!\n\nhttp://etotheipiplusone.com/kde4daily/docs/kde4daily.html"
    author: "Anon"
  - subject: "Re: Screenshots"
    date: 2008-06-19
    body: "\nSo instead someone who has it, would press print screen and paste few files to image sharing site, they told wanted people to download bigger amont of data and even add cost for share servers what share the KDE4 daily ;-)"
    author: "Thomas.S"
  - subject: "Re: Screenshots"
    date: 2008-06-23
    body: "Yeah come on KOffice devs... you've (presumably) worked so hard on this new release and we want to see what it does. Some screenshots demonstrating what it looks like would be much appreciated. We want to be koffice fans -- we just need some help :)"
    author: "Matthew"
  - subject: "Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-20
    body: "This might not be the place to ask this question but I notice in kde4daily that the latest KWord 2.0 alpha 8 can't open MS Word 2003 .doc files nor produce them.\nAs I'm still unemployed I regularly use OpenOffice to edit application.doc and CV.doc in MS Word .doc format. OpenOffice almost can produce good .doc files but I always have to reboot into Windows Vista to check in MS Word 2003 if it looks okay.\nKWord 1.6 can't read my CV.doc correctly.\nAre there already MS filters for KWord 2.0? Will they ever be written?\nI read somewhere that there is a Google Summer Of Code project:\"Improve KWord MS Word filter\", but is that only to read .doc files or also to produce them?\nI'd like to use KWord only in the future.\n\nHm, most companies demand a MS Word .doc CV if you sollicitate for a job. Would it be good practice if from now on I write my application letter and CV in ODF (.odt) format and tell the company to who I write a sollicitation email that they must install the ODF plugin for MS Word if they want to read my application letter and CV?\nThere appear to be three solutions for an ODF plugin:\n* Sun ODF Plugin for Microsoft Office (claims to be better than the sourceforge plugin)\n* http://odf-converter.sourceforge.net/ : Open XML/ODF Translator Add-ins for Office. Sponsored by Microsoft, the final version is planned for August\n* http://www.microsoft.com/Presspass/press/2008/may08/05-21ExpandedFormatsPR.mspx : Microsoft Office 2007 Service Pack 2 (SP2), scheduled for the first half of 2009, willl provide support for ODF\n\nI think for my next sollicitations I will attach to my emails both .doc AND .odt files with the request to the company that they should install the Sun ODF plugin as the ODF format will be my preferred way of communication. This means a little more work for me but I'm guessing many companies have not heard much yet about ODF.\n\nHow many of you guys use only ODF documents in your communication with companies? Are companies easy to adopt the ODF plugin or are you forced to write a MS Word doc?\n\nSo my question whether KWord can produce .doc files is no longer relevant. But KWord should be able to READ them as long as companies send their documents to me in .doc format."
    author: "vatbier"
  - subject: "Re: Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-20
    body: "With OpenOffice 2.4.0 I converted my CV.doc to CV.odt.\nKWord 2.0 alpha 8 can't open that .odt file correctly: fonts are wrong, page layout is wrong.\nKWord 1.6 also can't open it correctly.\n\nI should ask the mailing-list why an .odt file produced by OpenOffice can't be exactly reproduced by KWord 2.0."
    author: "vatbier"
  - subject: "Re: Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-20
    body: "If it doesn't contain any sensitive information, it would be interesting to use the file as a test case."
    author: "Inge Wallin"
  - subject: "Re: Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-21
    body: "May I suggest that after you're done editing and saving your document in KWord, convert it into a pdf-document. Most companies AFAIK can open a pdf-document. Additionally, you will also be sure that the layout of your document preserved when opened by others."
    author: "KDE rocks"
  - subject: "Re: Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-22
    body: "I was most surprised when recently at a presentation, the computer used with the projector could not be used by most of us as it did not have a way to open pdf files (yes. No acrobat reader!). Of course we could connect our laptops to the projector.\n\nNow this again brings us to \"export to PDF\" not being the same as \"print to PDF\". Would koffice give us a way to create pdf files preserving links etc? or would the pdf printer somehow become smarter to it could manage links etc?"
    author: "Kishore"
  - subject: "Re: Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-22
    body: "I didn't know that there's a difference between exporting vs printing to pdf. Could you elaborate? Besides preserving links, what other functions might be of interest to preserve? "
    author: "KDE rocks"
  - subject: "Re: Is KWord 2.0 able to produce MS Word .doc files?"
    date: 2008-06-22
    body: "The big problem in 1.6 with reading .doc files was tables (they are imported perfectly, but kword cannot handle big tables and certain types of images. KWord 2.0 does not have tables at all, but benjamin call is working on the images problem as part of his summer of code project.\n\nSaving as .doc isn't on the agenda. We do the same thing as Microsoft itself here, when you save a document for another version of word than the one you're using, and save it as rtf but call it .doc."
    author: "Boudewijn Rempt"
---
The KDE Project today announced the <a href="http://www.koffice.org/releases/2.0alpha8-release.php">eighth alpha release of KOffice 2</a>, a technology preview of the upcoming version 2.0. Work continues in the same vein as before, with a strong focus on finishing and polishing our new features that will set KOffice.  This is a work in progress, showing the changes that have been made over the last month by the KOffice developers. Most features that will be part of the final release are present now, and bug reports are welcome for the more stable components.


<!--break-->
<h3>OpenDocument Improvements</h3>
 
<p>One of the highlights of this release is the work on saving and loading Open  Document Format documents, especially for the text shape, thanks to the sponsoring of Girish Ramakrishnan by the <a href="http://www.nlnet.nl">Dutch NLNet organisation</a>. Girish has added scores of tests to check for ODF compliancy. </p>
 
<p>It is also worthy of note that now KOffice is able to load and save images in 
text and presentation documents. Shapes can now be animated and associated with
events such as sounds.</p>
 
<h3>Multiplatform</h3>
 
<p>Importantly, for the first time, KOffice is released simultaneously for the 
three main platforms: Unix/X11, Windows and Mac OSX. KOffice is the only office
suite that is available for all three platforms using a single codebase.</p>
 
<h3>Early testers</h3>
 
<p>While KOffice applications, generally speaking, are not ready for bugs reports,
some applications are more ready than others. The developers of KSpread, Krita, Karbon and the report component of Kexi welcome user feedback. The season is open for bug reporting!</p>

