---
title: "Kopete Bug Triage Marathon - 6th and 20th July."
date:    2008-07-03
authors:
  - "ggoldberg"
slug:    kopete-bug-triage-marathon-6th-and-20th-july
comments:
  - subject: "nightlys"
    date: 2008-07-04
    body: "Rumor has it that the Neon nightlys also have Kopete on them:\n\nhttp://amarok.kde.org/wiki/User:Apachelogger/Project_Neon\n\n"
    author: "blauzahl"
  - subject: "kubuntu"
    date: 2008-07-04
    body: "http://amarok.kde.org/wiki/User:Apachelogger/Project_Neon/KDE\n\nThis is the more useful link. :)"
    author: "blauzahl"
  - subject: "Re: kubuntu"
    date: 2008-07-04
    body: "Judging from the updates, I think they're a little behind on compiling (the last kdebase snapshot is more than a week old). Since this process is probably automated, I think that something does not compile. Is there a way to check for build status like dashboard does in PPAs?"
    author: "Luca Beltrame"
  - subject: "Re: kubuntu"
    date: 2008-07-05
    body: "This is probably what you are looking for:\nhttp://developer.kde.org/~dirk/dashboard/"
    author: "Talavis"
---
On Sunday 6th July, the <a href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> will be holding a Kopete bug triage day. The aim: to  dramatically reduce the number of Kopete bug reports from the current level of approximately 530. As usual, this bug day will be coordinated in the channel <b>#kde-bugs</b> on <b>irc.freenode.net</b>. There will also be a followup bug day two weeks later, on Sunday 20th July, to triage any remaining bugs.


<!--break-->
<p>The <a href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> is a great way to contribute to KDE. You do not need a huge amount of time. Just 30 minutes is enough to learn the craft of bug triage - sorting and checking bug reports in the <a href="http://bugs.kde.org">KDE bug tracker</a>. You do not need any previous experience with programming or contributing to KDE - you just need a computer with KDE running on it and a desire to help out.</p>

<p>The Kopete bug triage marathon will take place on two Sundays, 6th and 20th of July, in your timezone. We will start in the morning in Asia, and continue until the evening in America. There will be expert triagers on hand in the IRC channel (#kde-bugs) to help you get started, and once you have done a few bugs you will be able to continue at any time of any day - whenever you feel like doing a bit more.</p>

