---
title: "Qt to be Supported on Maemo"
date:    2008-04-16
authors:
  - "jriddell"
slug:    qt-be-supported-maemo
comments:
  - subject: "Awesome!"
    date: 2008-04-15
    body: "This is awesome! Finally Qt is supported in maemo. By supporting Qt (and buying Trolltech), Nokia can get a whole bunch of superior Qt and KDE applications: office suite (KOffice), multimedia (KDE multimedia), PIM (KDEPIM), networking (KDE Network), and the list goes on.. It becomes clearer why Nokia wants to buy Trolltech ;)"
    author: "fred"
  - subject: "Re: Awesome!"
    date: 2008-04-15
    body: "Hello,\n\nactually no, it doesn't. All this software was Free before. And it remains Free, so what's the point of owning it? Likely not the ability to ship KDE programs. That was Free before.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Awesome!"
    date: 2008-04-16
    body: "by owning trolltech, nokia can make qt's developpers work on qt's compatibility with maemo.\nQT was free, it IS still free, but now the \"free\" way is lead by nokia ^^.\n"
    author: "because"
  - subject: "Re: Awesome!"
    date: 2008-04-16
    body: "\"QT was free, it IS still free, but now the \"free\" way is lead by nokia ^^.\"\n\nWrong. Qt itself was not free, they always had a commercial license which one had to get when one wanted to use it. (For closed source apps for example)\n\nBack then the dependence on Trolltech was one main reason why Gnome was started (and it is now funny that they wanted to incorporate C#/Mono stuff... probably they get paid)\n\nKDE on the other hand was always free in the sense of \"free\". They even have a clause that when Qt would ever become non-free as such, KDE could continue (and fork the latest non free part too)\n\nJust because Nokia bought Trolltech doesnt mean that Trolltech changes its agenda.\n\nYou cant say this without looking at the FACTS, and so far I believe Nokia has hardly done ANYTHING, they just seemed happy with the acquisition."
    author: "she"
  - subject: "Re: Awesome!"
    date: 2008-04-16
    body: "Sorry, not to \"use\" it. Using it was easy.\nBut to \"develop apps with QT\" one needed a license.\n\nThat sentence earlier was misleading."
    author: "she"
  - subject: "Re: Awesome!"
    date: 2008-04-16
    body: "\"Wrong. Qt itself was not free, they always had a commercial license which one had to get when one wanted to use it.\"\n\nWrong. Since it was also licensed under the GPL, I would say that it most certainly was free. Well, excluding the early history with QPL and the like."
    author: "Janne"
  - subject: "Re: Awesome!"
    date: 2008-04-17
    body: "The QPL is a free software license. It's just not GPL compatible. But it is still free."
    author: "David Johnson"
  - subject: "Re: Awesome!"
    date: 2008-04-16
    body: "> It becomes clearer why Nokia wants to buy Trolltech ;)\n\nNokia bought Trolltech to push Qt to Maemo?!?\n"
    author: "Birdy"
  - subject: "Re: Awesome!"
    date: 2008-04-16
    body: "It is possible that Nokia wants Trolltech because of possible uncertainties in GTK's future (looking at the GTK3 discussions). Qt is a safe way as one can buy it and then control it."
    author: "Stefan Majewsky"
  - subject: "Re: Awesome!"
    date: 2008-04-17
    body: "GTK+ is also poorly supported on Mac and Windows. Not everything Nokia does is embedded Linux."
    author: "David Johnson"
  - subject: "Re: Awesome!"
    date: 2008-04-17
    body: "\"It becomes clearer why Nokia wants to buy Trolltech ;)\"\n\nI hope Nokia is not going to do same thing what Sun did for MySQL. They closed it's source... \n\nThis is big problem when company \"owns\" GPL software and then that company is sold out to other company what can cut all new code to be Closed Source and FOSS side is needed to make own fork."
    author: "Fri13"
  - subject: "Re: Awesome!"
    date: 2008-04-17
    body: "\"I hope Nokia is not going to do same thing what Sun did for MySQL. They closed it's source... \"\n\nNo they didn't - don't believe everything you see in awful slashdot editorials :) Everything that is currently GPL'd in MySQL is staying GPL'd, but further components developed by SUN may or may not be placed under a proprietary or non-GPL license.  It's a net win for MySQL and its users.\n\nPlus, Nokia would gain absolutely nothing by closing the source, and they would lose an awful lot, and they are smart enough to see this."
    author: "Anon Reloaded"
  - subject: "Qt > Symbian"
    date: 2008-04-15
    body: "ah, KDE on Nokia phones.... my precious! \n\nFinally slow and not so nice Symbian will disappear :)"
    author: "Koral"
  - subject: "Re: Qt > Symbian"
    date: 2008-04-16
    body: "No, this is not for the phones but for the Nokia InternetTabletts (770, N800, N810, N810-Wimax). Meamo is not available for Nokia phones."
    author: "Nik"
  - subject: "Re: Qt > Symbian"
    date: 2008-04-16
    body: "Well... Symbian is what's underneath, Qt runs on top of it... So it's kind of like saying KDE > Windows these days ;) While it's true that it's better than any other GUI running on it today, well... it wouldn't be much without the underlying systems, really :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Get KDE devs hired by big companies, spread word!"
    date: 2008-04-15
    body: "I urge everyone who reads this to blog about Nokia hiring people with Qt/C++ \nexperience.\n\nhttp://www.nokia.com/A4126298\nhttp://jaaksi.blogspot.com/2008/04/ctia-and-qt.html\n\nThe KDE community needs to get some if its own into powerful companies like Nokia so that they can hopefully help change the attitude of those companies to favor KDE and Free/Open Source Software."
    author: "Tray"
  - subject: "Re: Get KDE devs hired by big companies, spread word!"
    date: 2008-04-15
    body: "and how do you think the community can force companies to hire KDE people?  A company should always hire only the best, and sorry but although the community might want to think with their ego, a lot of times there are much better options than those who work on KDE"
    author: "anon"
  - subject: "Re: Get KDE devs hired by big companies, spread wo"
    date: 2008-04-15
    body: "> there are much better options than those who work on KDE\n\nNow isn't that a provocative thing to say on a KDE news site? Are you by any chance a sore loser from the \"competition\"?"
    author: "AC"
  - subject: "Re: Get KDE devs hired by big companies, spread wo"
    date: 2008-04-16
    body: "Wow.  Who said anything about forcing?\n\nThe point is, to spread the news among KDE friendly devs who would then apply for the job.  If enough of them do, then there's a pretty good chance one of them will end up being the best match for the job and get hired.\n\n> A company should always hire only the best\n\nOk, we agree there.\n\n> a lot of times there are much better options than those who work on KDE\n\nAnd there you go back to trolling...  Or at least deliberately trying to piss people off.  KDE is like any other large software project, it has many great developers and some marginal ones.  I think the balance there is actually pretty favorable for KDE compared to many projects."
    author: "Matt"
  - subject: "Re: Get KDE devs hired by big companies, spread word!"
    date: 2008-04-16
    body: "> there are much better options than those who work on KDE\nN probably needs experienced Qt developers. KDE has developed an entire DE based on Qt, so they obviously are amoung the best, if not ARE the best, at developing with Qt."
    author: "Riddle"
  - subject: "lol"
    date: 2008-04-16
    body: "I can tell you aren't a KDE developer."
    author: "anon67"
  - subject: "YES!"
    date: 2008-04-15
    body: "Yes, yes, yes!\n\nAs big fan of Nokia Internet Tablets I welcome this move. I'd like to remind that thanks to work of penguinbait KDE is quite well supported on NITs.\n\nhttp://tablethacker.com/kde.html"
    author: "m."
  - subject: "IT Manager"
    date: 2008-04-15
    body: "This is awesome! I have long awaited QT/KDE/KDE-APPS on my Nokia tablet. \n\n"
    author: "N810 User"
  - subject: "Re: IT Manager"
    date: 2008-04-16
    body: "Oh, you could be already running kde on your NIT: http://arstechnica.com/journals/linux.ars/2007/09/11/running-kde-on-the-nokia-n770-and-n800\n:-)\nSearch for \"kde\" in the NIT-Forum: http://www.internettablettalk.com/forums"
    author: "Nik"
  - subject: "Nice and all..."
    date: 2008-04-16
    body: "I'm just wondering how many \"framework\" \"toolkits\" etc etc yada yada yada there are? I remember getting Opie to work on my Ipaq. It still works BTW. We have GPE. We have Trolltech's Qtopia. There's Google's Android.\n\nHow many are there? Way back in the day people said it was a pipe dream. Then everything is reinvented. I guess it would be nice to know why the various projects were started as well."
    author: "Winter"
  - subject: "Re: Nice and all..."
    date: 2008-04-16
    body: "Android is really the only new toolkit to come out in the last few years. It's not really clear why Google decided they had to make their own new technology..."
    author: "Ian Monroe"
  - subject: "Re: Nice and all..."
    date: 2008-04-16
    body: "Really, I would say this is a few years ago:\nhttp://maemo.org/news/announcements/archive.html\n\nNot sure when it was really started though. Perhaps it has to do with companies starting open source projects from scratch. That may be good. I wonder why some of the existing projects were no used. Are there technical problems?"
    author: "Winter"
  - subject: "Re: Nice and all..."
    date: 2008-04-16
    body: "Hi, the maemo platform was made public on May 2005 and since day one a key aspect was the use of existing open source projects filling the gaps either contributing to their improvement or complementing them with own development. Linux kernel, X.org, Debian tools, HAL, Dbus, GStreamer, Telepathy, GTK+...\n\nWhat is more important, Nokia has made all this development quite in sync with the respective upstream projects, hiring developers from there, working with companies from their ecosystem, contributing patches back and etc.\n\nMore at <a href=\"http://maemo.org/development/documentation/maemo-quick-start-guide.pdf\">maemo quickstart guide</a> (PDF).\n"
    author: "Quim"
  - subject: "Re: Nice and all..."
    date: 2008-04-16
    body: "Quim writing in the dot, interesting time to come, i hope by this plan the collaboration between qt and gtk will be pushed even better. everyone win."
    author: "mimoune djouallah"
---
Mobile platform project <a href="http://maemo.org/">Maemo</a> are <a href="http://maemo.org/news/announcements/view/qt_to_be_supported_in_addition_to_gtk.html">reporting that Qt is to be supported</a> on their platform.  Nokia developers are blogging that <a href="http://jaaksi.blogspot.com/2008/04/ctia-and-qt.html">they will be looking to hire Qt developers</a>.  This should make it easier to get KDE apps onto internet tablet devices.


<!--break-->
