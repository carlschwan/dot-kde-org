---
title: "KDE 4.0 Release Event Keynote Now Online"
date:    2008-01-20
authors:
  - "dallen"
slug:    kde-40-release-event-keynote-now-online
comments:
  - subject: "can i have a transcript !!! "
    date: 2008-01-20
    body: "can i have a transcript of the speech, my english skills is very low,  "
    author: "djouallah mimoune"
  - subject: "Re: can i have a transcript !!! "
    date: 2008-01-20
    body: "There is no transcript at the moment, but there is a summary of both the keynote and the other presentations after in the Day 2 event summary: http://dot.kde.org/1200788475/\n\nDanny\n\n"
    author: "Danny Allen"
  - subject: "not for me..."
    date: 2008-01-20
    body: "\"this video is currently not available. please try again later.\"\n\nmaybe google knows I'm really supposed to be studying and won't let me watch the video until I'm done ;)"
    author: "Chani"
  - subject: "Re: not for me..."
    date: 2008-01-20
    body: "waaait a sec.\n\"Currently, the playback feature of Google Video isn't available in your country.\"\n\nok, guess I have to find all google-video hostnames and route them through a proxy :P oh yeah, I just *love* being in china :P"
    author: "Chani"
  - subject: "Re: not for me..."
    date: 2008-01-20
    body: "Hoho, I remember that problem from when I was there. It was strange though, I think I just changed user agent to Internet Explorer/Vista and then it worked. o_O"
    author: "Hans"
  - subject: "Re: not for me..."
    date: 2008-01-20
    body: "oh.\nI can't watch it,either.\nI'm in Shanghai, China.....    :("
    author: "nihui"
  - subject: "Re: not for me..."
    date: 2008-01-20
    body: ":(\nif I had root on any of my proxy servers I'd offer you an account.\n\nmaybe once the oggs are up they'll be on a server you can download from."
    author: "Chani"
  - subject: "Aaron outdoes Steve Jobs and Bill Gates combined"
    date: 2008-01-20
    body: "Not that Aaron should try to imitate either of them; they're both egomaniac control freaks. KDE and Free/Open Source Software are really lucky to have has such an eloquent spokesperson as Aaron. And he's the talented developer in charge of one of the most visible parts of KDE, Plasma. Tip my hat, sir!"
    author: "Tray"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-20
    body: "I don't think he outperforms Steve Jobs, but he is excellent.\n\nit would be nice though if they tried to create a more professional production. the image quality of the video could definitely be better, and it would help if they were filming it from a few angles. also better projectors and sound system (the video he played would have been so much better).\n\nthe video editing was quite good, and so were the speakers. I guess the real question though is what is the target use of this video? because if you want to use it as marketing material for tech enthusiasts, it has to be much better. of course, there may be an issue of limited funds."
    author: "yman"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-20
    body: "All Aaron needs to reach the level of Steve Jobs is add a few \"great, extraordinary, most amazing, tremendous, better and better and better\" and then put his laptop in an envelop.\n\nhttp://www.youtube.com/watch?v=Yz1-cPx0cIk"
    author: "Paul"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-20
    body: "Steve Jobs is a bit more lively, and shows more excitement about what he is talking about. the constant repetition of every point he makes does bother me."
    author: "yman"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-22
    body: "Steve Jobs uses NLP methods a lot. That's why he is a great public speaker, and memorable. (People like Tony Robins use the same method.) \n\nA side effect is, that it stretches a small amount of products to last about an hour's lenght.\n\nAlso keep in mind that people use Steve Jobs' keynote instead of a user manual these days to learn how to use a product.\n\nFor this last presentation Aaron's format was great, and I'm glad somebody has even decided to tape it so we can see the footage \"like we were there\". \n\nI think for a consumer level presentation, it would be great if Aaron would go over all the features of KDE 4.1 (when it comes out for Windows/Mac). \nThat would really help \"sell\" (increase public awareness, publicity, and show users how to use it.) the 4.1 branch and onwards. It works for Steve Jobs, just let Aaron develop his own speaking style for KDE. I'm looking forward to a similar hype about upcoming KDE releases. (don't forget to mark them on digg, stumbleupon, del.ici.ous, etc..)\n\nI wonder if Google is willing to sponsor such an event. "
    author: "Max"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-23
    body: "what's NLP?"
    author: "yman"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-24
    body: "http://en.wikipedia.org/wiki/Neuro-linguistic_programming"
    author: "AC"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combined"
    date: 2008-01-21
    body: "Outdoing Steve would mean saying \"Better, better, better!\" in every sentence. \nI think Aaron was on different level as those two. \n\nBest keynote i have seen in ages."
    author: "Tom"
  - subject: "Re: Aaron outdoes Steve Jobs and Bill Gates combin"
    date: 2008-01-22
    body: "It was a very informative and educational presentation and he presented himself as a surprisingly (for me) professional speaker. I wouldn't compare him with the above mentioned persons because he seems to be honest and more natural. Attributes that Steve and Bill could borrow from him."
    author: "Bobby"
  - subject: "hair cut !!"
    date: 2008-01-20
    body: "ok it is stupid to appreciate someone only upon his look, but don't you think that Seigo need a hair cut. i suppose that is the task of kde marketing team! \n\ni would like to thank you all guys, i am here proudly showing kde app under windows to my coworkers, \n\ncheers from algeria \n\n  "
    author: "djouallah mimoune"
  - subject: "Re: hair cut !!"
    date: 2008-01-20
    body: "Yeah, let's all have short hair and wear black suits and we'll all looks pretty much the same, that sounds like fun :|\n\nIt's all about the freedom of choice man!\n\nYou really sound like a communist xD\n\n\n\n\nAnyway, it was a great speech, I've enjoyed every minute of it, even if I was already aware of the new technology and things going on :)\n\nCheers! ^^"
    author: "Dread Knight"
  - subject: "Re: hair cut !!"
    date: 2008-01-20
    body: "He _has_ had a haircut :-)  And he has a real job too..."
    author: "Odysseus"
  - subject: "Re: hair cut !!"
    date: 2008-01-20
    body: "come on i was just kidding, perhaps i am just jealous of his hair;) "
    author: "djouallah mimoune"
  - subject: "Stop this please!"
    date: 2008-01-21
    body: "Please, can you stop bashing around! Talking about somebody's haircut seems to be quite amusing to some people, particularly if one can do this anonymously without being seen by the target person on the web. \n\nI wonder how insensitive some of the posters within the last days may be that they do not realize that any comment on somebody's hairstyle is meant to offend and discredit the target person. Aaron is one of the currently most committed and active persons of the KDE project. It would be a pity if KDE loses him and his effort due to some stupid comments!\n\nKeep you head up, Aaron! As somebody said, KDE is free in everything - opinions and personal look. Who does not understand this - well.... "
    author: "Sebastian"
  - subject: "Re: Stop this please!"
    date: 2008-01-21
    body: "I think that it would be a good idea to implement some kind of moderation system to dot.kde.org. We ordinary people could mod stupid comments down and Real Heroes like Aaron could just read real comments without this static noise...\n\nWell, I guess I should try to google if there's any good free systems available to replace current dot engine. By the way: is there something going on to replace current dot.?"
    author: "Sami"
  - subject: "Re: Stop this please!"
    date: 2008-01-29
    body: " Well, I'm actually starting a non-profit organization specialized on open source software development, especially kde related, but i can't say it could actually compete / replace dot.kde without some actual editors in the team that can bring original news articles...\n\n The system I'm using is drupal (open source content management system) and it's very robust/scalable. So if any editors and any other people who want to help or just want to find out more, feel free to contact me at my email address.\n\nCheers!"
    author: "Dread Knight"
  - subject: "Digg IT !!!"
    date: 2008-01-20
    body: "http://www.digg.com/linux_unix/KDE_4_0_Release_Event_Keynote_from_Google_Headquarters\nToo cool to not share it !\n"
    author: "an user"
  - subject: "Re: Digg IT !!!"
    date: 2008-01-20
    body: "Done! This was an excellent presentation of KDE 4.0. Don't forget to also share it on other social bookmarking sites like reddit.com, del.icio.us, stumbleupon ..."
    author: "Tsiolkovsky"
  - subject: "Re: Digg IT !!!"
    date: 2008-01-21
    body: "done, please we need your diggs ,as to make it to the front page, "
    author: "djouallah mimoune"
  - subject: "hi def"
    date: 2008-01-20
    body: "thanks! looking forward to the hi def download / torrent :)"
    author: "mxttie"
  - subject: "Re: hi def"
    date: 2008-01-20
    body: "Besides having it in high definition it looks like it would also be helpful to those people where they can't access Google Video because of internet censorship."
    author: "Tsiolkovsky"
  - subject: "Re: hi def"
    date: 2008-01-20
    body: "HD was not in the cards for this.  Franz Keferboeck & his supporters in the booth did an excellent job in getting this together, and should be thanked for his outstanding efforts.\n\nAs far as HD goes, you've got to understand, the truly HD video cameras are in the $40k and up range, and then of course you need the devices to record to, so on...\n\nCare to donate a 24p studio-quality camera? ;)"
    author: "Joseph Gaffney"
  - subject: "Re: hi def"
    date: 2008-01-21
    body: "I think what he actually meant with HD was not 720p or such, but a higher definition video than at googlevideo. You don't need 720p to look decent, normal PAL or NTSC is much better than the streaming video version on googlevideo.\n\nSo no $40k needed for that ;)"
    author: "tobami"
  - subject: "Re: hi def"
    date: 2008-01-22
    body: "Incidentally, the original footage was 720p :)"
    author: "Troy Unrau"
  - subject: "Re: hi def"
    date: 2008-01-22
    body: "Oh!, great. Will videos in that definition be available for download?"
    author: "tobami"
  - subject: "Re: hi def"
    date: 2008-01-22
    body: "\"I think what he actually meant with HD was not 720p or such, but a higher definition video than at googlevideo\"\n\nexactly! thanks ;)\n\ncould somebody remove my email address out of the original post, i didn't know it would be public and notifications don't seem to work anyway....."
    author: "mxttie"
  - subject: "Re: hi def"
    date: 2008-01-22
    body: "Cool, looking forward to seeing this at TV resolution (higher def than google/youtube..)\n\nPlease post when and where the torrent will be available."
    author: "Max"
  - subject: "Excellent and enlightening speech!"
    date: 2008-01-20
    body: "This was a great speech, it really combined all the bits of information about KDE 4 we've been reading before.\n\nThanks for Aaron and the others for a great summary.\n\nKDE 4 sure will be a great software platform - it already is very promising and getting there. Few years ago it used to be just a desktop, later a desktop environment and set of applications. Now we can seriously talk about a software platform.\n\nFor and end user desktop and the applications are the operating system. In that sense KDE will provide a wonderful platform for application providers - both open source as well as proprietary.\n\nWhat I'm dreaming of is a seamless integration of free and closed software on the same platform. Examples of that would be:\n\n- a lot more commercial and proprietary applications based on KDE libraries\n- seamless integration of commercial plugins (Flash, Shockwave, DivX, mp3) in KDE\n- seamless integration of proprietary hardware drivers (Win32 drivers on Unix/Linux, automatic or semi-automatic installation of proprietary Unix/Linux drivers)\n- seamless integration of Windows applications on Unix/Linux and KDE, especially commercial 3D games\n\nI'm an open source fan and a promoter, but I think that commercial closed source software has to be taken into account as well. If we could integrate commercial offerings into this great open environment, we'd definetly be on a path to WORLD DOMINATION ;)\n\nA lot of this is already on it's way. There is a way to run plugins and applications on Wine and KDE, but these solutions are not ready for the end user.\n\nTapio Kautto\n"
    author: "Tapio Kautto"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-20
    body: "No thanks, we're already infected by too much proprietary stuff, see e.g. the use of Flash streaming to deliver these videos."
    author: "Kevin Kofler"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-21
    body: ">a lot more commercial and proprietary applications based on KDE libraries\n\nShould not happen, once KDE is GPL. Too bad, I would like this also. Luckly they can do Qt programs that will look well uintegrated in KDE.\n\nAbout the other commentary, I don't think proprietary technology is bad per-se and we can't like in a aquarium forever, so let's do a reality check:\nDoes most of web sites today use flash? Yes\nDoes a big part of them *require* flash to work properly? Yes\nDo we have a free native qt/kde implementation of flash? No\n\nSure, the ideal would be to have SVG+JavaScript, but not even free platafforms support those very well, so unless we have a better product to offer (see Firefox, XVid and others) using something proprietary is a need :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-21
    body: ">>a lot more commercial and proprietary applications based on KDE libraries\n\n>Should not happe, nonce KDE is GPL\n\nIt will happen, it's designed for this. The KDE libraries are LGPL/BSD to allow this. This has been KDE library policy since forewer. Whatever license KDE applications use(GPL2, GPL3, BSD, MIT, QPL, EPL etc), can not change this."
    author: "Morty"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-22
    body: "So my mistake here, but I *ever* read articles stating that kdelibs where GPL and this was one of the advantages of gnome (lGPL) over kde."
    author: "Iuri Fiedoruk"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-23
    body: "Qt is GPL or commercial. So to develop commercial KDE software one has to purchase an expensive commercial Qt license but one can use KDE libs in the commercial product."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-21
    body: "I can do fine without Flash, thank you very much.\nMoreover, there is Klash (from the Gnash project) now."
    author: "Kevin Kofler"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-22
    body: "I can't because of youtube, charges.com.br and other fun sites.\nI can live without them, but and where all the fun will be? ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-21
    body: "Great idea!\nLet's replace everything in our free and open operating system piece by piece with proprietary software. Why haven't I thought of that.\nThat's just genius."
    author: "nobody"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-22
    body: "ADOBE - if you're reading this:\n\nPlease start working on Adobe Creative suite for KDE 4.X.Y branch. Think about it, you'd be able to write software that seamlessly integrates Windows, Mac, and Linux platforms. \n\nIt would open up a great sales opportunity for you.\n\n\n\nTO OTHERS:\n\nI think the future will be a nice split between proprietory and open source software. It would be great if KDE helps merge these two words seamlessly together and be truly platform independent. Free software will follow. I love open source, but I do acknowledge that there is some closed source software that is great and deserves to also exist under Open source platforms, such as KDE, Linux, and the like.\n\n"
    author: "Max"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-22
    body: "Amen.\n\nPerhaps some people like to troll or just have their head up in their **s, but I'll repeat my opinion anyway.\n\nIt's not realistic to think that all applications and libraries would be open source. I'd really love if they would, but it's not going to happen. Many important applications will be proprietary, and we just have to accept that.\n\nWhy Firefox is popular? It's a great application, there's a need for it but also because it supports proprietary plugins. It offers more complete user experience than it would offer without supporting for them. Many people would just try it and abandon it because they would like to see the evil content that's not open.\n\nWhy OpenOffice.org? Because it supports evil operating systems and evil document formats.\n\nWe need more and more open source implementations, but we also need to take closed apps and plugins into account (I just can't say 'support' because it would feel bad to say so as an open source person)\n\nHaving the best platform will draw attention from commercial entities, this means more companies paying for people to work on KDE. That will benefit as all, those that have aggressive attitude towards proprietary vendors as well as those of us that accept the reality.\n\nTK.\n\n\n"
    author: "Tapio Kautto"
  - subject: "Re: Excellent and enlightening speech!"
    date: 2008-01-22
    body: "Yes.. and change the KDE slogan to:\n\"don't be free...\""
    author: "ofcourse"
  - subject: "Ogg!"
    date: 2008-01-20
    body: "> The keynote is immediately available on Google Video, and will be available\n> for download in other formats soon.\n\nHopefully Ogg Theora+Vorbis will be one of those! Down with proprietary formats!\n\nSee also this great blog post by John \"J5\" Palmieri about this topic: http://www.j5live.com/?p=421"
    author: "Kevin Kofler"
  - subject: "Re: Ogg!"
    date: 2008-01-20
    body: "+1\nI want my theora video please."
    author: "Paul"
  - subject: "Re: Ogg!"
    date: 2008-01-20
    body: "I'm a bit disappointed that KDE choose to release the videos in a closed format first.  I'm also waiting for the open formats (due to flash not being available for me).\n\nOn the positive side; I can't recall the videos being available so shortly after the event, ever.  Thats awsomeness right there :)"
    author: "Thomas Zander"
  - subject: "Re: Ogg!"
    date: 2008-01-21
    body: "We didn't do the recording ourselves, the google pr ppl first had to approve everything, then the google ppl worked very hard to get it online asap (so that became googlevideo) and now we have the source, so I believe franz is working very hard to get ogg's online right now. Won't be long until he blogs about it, I suppose..."
    author: "jospoortvliet"
  - subject: "Download?"
    date: 2008-01-20
    body: "Is there a way to actually download the entire video? I don't like these streams...."
    author: "liquidat"
  - subject: "Re: Download?"
    date: 2008-01-20
    body: "Try this:\nhttp://pastebin.com/f7a71c2a\n"
    author: "Nach"
  - subject: "Re: Download?"
    date: 2008-01-21
    body: "useful comment but it ruins the formatting of the page (the URL line doesn't break in Konqueror)"
    author: "Apple Pie"
  - subject: "Re: Download?"
    date: 2008-01-21
    body: "I second this. Thanks to the preposter"
    author: "Sebastian"
  - subject: "Re: Download?"
    date: 2008-01-22
    body: "thanks for posting this..\n\nQuick question: where exactly does this download to?\n\n"
    author: "Max"
  - subject: "Re: Download?"
    date: 2008-01-22
    body: "It will download it to the current directory."
    author: "Nach"
  - subject: "Re: Download?"
    date: 2008-01-22
    body: "well, unfortunately this download is interrupted for me every time, and wget can't continue it (with the correct flags) for some reason... so i'm still waiting for some decent downloadable format.\nwhy google videos do not offer a downloadable format ? i think one of youtube/gvideo allowed that before..."
    author: "richlv"
  - subject: "Re: Download?"
    date: 2008-01-23
    body: "If you have a flash player then the video will be saved in /tmp, under a name like /tmp/FlashRfFkIG . So you can load the google video page, pause the video, wait for it to be fully downloaded, and then copy that file somewhere else (it gets deleted when you close the browser window or navigate to another page). It's \"flv\" format, which you can play with e.g. mplayer.\n\nThere are other ways, but I found this one to be the most practical and universal (works with all websites)."
    author: "tendays"
  - subject: "What's up with Decibel?"
    date: 2008-01-20
    body: "I read in #kopete that basysKom had only one guy working on decibel and that lately they had kind of dropped the project and that the guy was kind of working on his own initiative now on Decibel :/ \n\nAnybody has news about decibel? It really looks like a great project."
    author: "Patcito"
  - subject: "Two things I wonder about"
    date: 2008-01-20
    body: "The first is: Aaron mentions in his speech that Decibel just had made a release  - however, the last release was long ago, and I do wonder what the stage of Decibel actually is.\n\nThe second thing: where is Sonnet? There is no spell checking around in my  KDE 4.0 installation and I haven't seen any screenshots of it recently. Additionally, there has been no work at it at all as far as I see it from the svn.\n\nBut besides these two the list for KDE 4.1 really is impressive by now, and most things are already almost done - that's almost as exciting as KDE 4.0 itself ;)"
    author: "Strangeness"
  - subject: "Re: Two things I wonder about"
    date: 2008-01-20
    body: "Sonnet is in kdelibs, and it works e.g. for spell checking in forms in konqueror, but... I don't know whether the heck the control center module to enable it went !? On my desktop machine I've configured it before, it still works (though with quite some problems in the highlighting module), but I can't seem to see the kcm on my laptop.\n\nAnd yes, it is largely unmaintained, though Laurent has been fixing up some GUI bits, and IIRC some new guy considered picking it up.\n"
    author: "SadEagle"
  - subject: "Re: Two things I wonder about"
    date: 2008-01-20
    body: "Me too. It's neither in KDE 4.0 nor in my SVN build from earlier this day (checked with `kcmshell4 --list`)."
    author: "Stefan"
  - subject: "breaking new: danny picture"
    date: 2008-01-21
    body: "http://franz.keferboeck.info/gallery/main.php?g2_itemId=632\n\nwho could though that !!!"
    author: "anon"
  - subject: "Re: breaking new: danny picture"
    date: 2008-01-21
    body: "I can't believe that he is so young. he looks like a very pleasant guy though :)"
    author: "Bobby"
  - subject: "Upset with KDE 4.0"
    date: 2008-01-21
    body: "I have been waiting for KDE 4.0 for long time until its release recently. I then decided to install it in my openSUSE 10.3 via 1-click installation (http://en.opensuse.org/KDE4). The installation went just fine then I got a very nice desktop. I was a little surprise to see most icons on my desktop showing as question mark (mostly the default openSUSE icons). I didn't blame that, maybe Oxigen didn't offer such icons.\n\nI then came across with another surprise. I could not switch the default K-Menu to the classic one (as seen on http://www.khmeros.info/drupal/?q=en/node/2542). Another surprise was, I could not move the icons I added to Taskbar. I expected that, there will be a \"Move\" menu item when I right-click on those icons (see the Dolphin icon in my screenshot below).\n\nAnother worst surprise was http://www.flickr.com/photos/37463129@N00/2209623432/. I got this mess desktop when I moved Firefox icon around (move to the bottom area) the desktop. The Firefox icon even disappeared in the taskbar (it was behind the taskbar). And I have no way to resize or move the Taskbar.\n\nLast but not least, the Ctrl+Alt+D didn't work as my expectation either.\n\nSo now what should I suppose to do? Yeah, maybe this is related to openSUSE more than KDE."
    author: "LEANG Chumsoben"
  - subject: "Re: Upset with KDE 4.0"
    date: 2008-01-21
    body: "You can delete the present Kickoff Menu (which is just a Plasmoid) then open the add widget window (right click on desktop) and drag and drop the clasical menu directly from there to the taskbar.\nThe taskbar and Plasma do not have all the configuration possiblities that Kicker and KDesktop had so we are kinda limited for now. The additional features will come soon though.\nTo get back your Firefox icon you have to use this little tool in right corner at the top to zoom out the desktop then you can move it to where it belongs and then zoom in back. I think that that will also get fixed.\nI hope that I helped a bit. "
    author: "Bobby"
  - subject: "Re: Upset with KDE 4.0"
    date: 2008-01-21
    body: "Old menu comes with plasma extragear, I don't know if everybody thinks of installing those before complaining here."
    author: "Richard Van Den Boom"
  - subject: "Re: Upset with KDE 4.0"
    date: 2008-01-22
    body: "I usually mention that but I forgot this time since I take it for granted."
    author: "Bobby"
  - subject: "Re: Upset with KDE 4.0"
    date: 2008-01-22
    body: "The old menu is in kdebase-workspace actually."
    author: "binner"
  - subject: "Hail to the president"
    date: 2008-01-22
    body: "Great presentation, just that.\n\nI never watched any \"web-clip\" longer than 4 minutes, but I watched the whole keynote. Honest, still optimistic, aware of the left and right, still addressing the opportunities. Thanks, Aaron.\n\nMy contribution so far was 160 translated strings in a 3.5 extragear app, think of it as the Planck constant of contribution. But this presentation left me so excited and encouraged, I won't stop there.\n\n\"Great innovations start with great optimism.\" Indeed.\n\nPS: Did I say Thank you?!\n"
    author: "m"
  - subject: "Download torrent?"
    date: 2008-01-23
    body: "Are we still getting a downloadable torrent of the file?\n\nI would love to watch it at the full NTSC 720p resolution, or as close as I can get to it.\n\nThe google vid is too grainy when blown up to full screen.\n\n"
    author: "Steve M"
  - subject: "Re: Download torrent?"
    date: 2008-01-24
    body: "are you in love with aaron? "
    author: "peter"
  - subject: "Re: Download torrent?"
    date: 2008-01-24
    body: "Who isn't?"
    author: "Anon"
  - subject: "Re: Download torrent?"
    date: 2008-01-26
    body: "Seriously!!\n\nNot just him, the whole KDE/Qt/trolltech team!!\n\nThis video is amazing!!"
    author: "Richard Lionhard"
  - subject: "KDE 4.0 Release Event Keynote on YouTube"
    date: 2008-01-25
    body: "Hello.\nDoes anybody can post this video on http://www.youtube.com?\nIn my company http://video.google.com is banned.\n\nP.S. \nWho want to download file from google or youtube, can use http://keepvid.com/ service.\n"
    author: "Sergey B."
  - subject: "how did anyone..."
    date: 2008-01-26
    body: "how did anyone tolerate listening to that kid kyle. wow shut up get off the stage!"
    author: "mike"
  - subject: "Wow!! I didn't know that about KDE"
    date: 2008-01-26
    body: "Wow!! \n\nI always thought KDE was a desktop manager, kind of like gnome, and stuff. I had no idea that KDE/Qt is a whole plattform. This is awesome!!\n\nPeople could finally write plattform independent software, not slow, buggy, cut down (like java), but real applications. (and games!!!) If it weren't for the keynote video on Google, I would have never known. \n\nAll I heard about KDE 4.0 so far that it was an update to the KDE desktop. All the screenshots and reviews on various blogs, just showed off the desktop. Let's face it the desktop itself looks incomplete and not that impressive. The huge taskbar and icons gave me the impression that KDE figured all users are over 70 and practically blind without glasses. Come on, that's so much wasted screen real estate. Now after the video though...\n\nI understand. I'm really impressed.\n\nCan't wait to see what you guys will come up with in July!! Hopefully there will be a similar keynote to this one. \n\nI'm glad google is behind this too. Now we really know this will be big!!!\nHopefully google news, calendar, gmail, etc. will all integrate with the desktop and Koffice. Is this going to run with Android? I would be absolutely blown away if I could synchronize Kpim, or evolution with google online and with my iphone. Heck!! Run KDE apps on my iphone.\n\n\nI think this is the revolution we've all been waiting for. True plattform independence, the exact same apps running on everything from a car stereo,iphone,TV,desktop,and laptop. \n\n\nHow come there is so little buzz about it though? Everybody is only talking about KDE desktop. Other than this keynote I haven't heard squat about KDE/Qt as a plattform."
    author: "Richard Lionhard"
  - subject: "Re: Wow!! I didn't know that about KDE"
    date: 2008-01-27
    body: "you're crazy, no.. krazy."
    author: "mike"
  - subject: "Re: Wow!! I didn't know that about KDE"
    date: 2008-01-28
    body: "Yea probably!!!\n\nThe KDE 4x bug bit me!!!\nBig time.."
    author: "Richard Lionhard"
  - subject: "Hello Guys...please check our"
    date: 2009-06-02
    body: "Hello Guys...please check our the <a href=\"http://www.beautyitems.com/\">hair beauty products</a> also...it's good"
    author: "michalraise"
---
On Friday, January 18th, Aaron Seigo, President of the <a href="http://ev.kde.org/">KDE e.V.</a> gave the keynote at the <a href="http://www.kde.org/kde-4.0-release-event/">KDE 4.0 Release Event in Mountain View, California</a> about KDE 4, presenting KDE to the world and the world to KDE. The keynote was recorded, and is <a href="http://video.google.com/videoplay?docid=6642148224800885420&hl=en">now available for streaming through Google Video</a>. Continue reading to watch the keynote!












<!--break-->
<p>Aaron started with an introduction to KDE 4, beginning with the history of the project and expressing how far we have come in the last 11 years. Then, Aaron explored what KDE is, and what our community is based on - freedom and openness. Freedom to do work, have fun, and connect with others. Further, Aaron moved on to KDE 4, and discussed the near-future plans and ideas. The vision of KDE 4 is based upon three principles: beauty, accessibility, and functionality. He ventured into the many areas that KDE has improved upon, and pointed to our roadmap for the KDE 4 cycle.</p>

<p>The keynote is <a href="http://video.google.com/videoplay?docid=6642148224800885420&amp;hl=en">immediately available on Google Video</a>, and will be available for download in other formats soon. The other talks will also become available tomorrow.</p>

<p><embed style="float:center; width:400px; height:326px;" id="VideoPlayback" type="application/x-shockwave-flash" src="http://video.google.com/googleplayer.swf?docId=6642148224800885420&amp;hl=en" flashvars=""></embed></p>

