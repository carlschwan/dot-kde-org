---
title: "KOffice Releases Ninth Alpha of KOffice 2.0"
date:    2008-07-16
authors:
  - "brempt"
slug:    koffice-releases-ninth-alpha-koffice-20
comments:
  - subject: "Nice"
    date: 2008-07-18
    body: "The more I look at Koffice 2.0, the more I like it.\nCongratulations and thanks to all developers, sponsored, students and sponsors."
    author: "Iuri Fiedoruk"
  - subject: "Re: Nice"
    date: 2008-07-18
    body: "+1\nI Always loved koffice, but these alpha look very promising.\nI'M looking for first beta to try out. (I know I could even test this release, but I've no time to test this currently)\n\nCheers to all KOffice devel & kde community :)\n\n"
    author: "Shuss"
  - subject: "Re: Nice"
    date: 2008-07-19
    body: "the more I look at it, try it, and experience openoffice, the more I am convinced that there will never really be a replacement for microsoft office,"
    author: "anon"
  - subject: "Re: Nice"
    date: 2008-07-19
    body: "Yes, and a hand-full of developers will never really be a replacement for hundreds or thousands of developers. Yet, it is astonishing to see what KOffice devs achieve, and what they do to reinvent office suite interfaces."
    author: "Stefan Majewsky"
  - subject: "Re: Nice"
    date: 2008-07-19
    body: "Well it can't really be a replacement for MS Office because numerous (thousands?) third-party extensions (with Macros and VB-scripts and whatever else) exist for MS Office which try to blow MS Office up to a full ERP-app, a report generator or whatever else.\nWe simply can't supply a plug-in replacement for MS Office with regard to these \"macro-apps\".\n\nPossibly it's not even needed as the big flood of MS Office extensions seems to be over anyway (with 75% of the old macros becoming now defunct on new MS Office releases and no one is taking the burdon to port old VB-Macros...most of them are ugly hacks anyway).\n\nAs most of the old MS Office Macros got used inhouse at e.g. administration offices and are going to be replaced with cleaner intranet-solutions over time the focus is shifting away from one office-suite-that-wants-to-be-everything to more lightweight solutions. KOffice might come in handy than ;-).. At least for me, it would be the ideal solution.\n"
    author: "Thomas"
  - subject: "Which ODF version"
    date: 2008-07-18
    body: "Which ODF version is targeted for KOffice 2.0? Is it 1.1 (OOo 2.x) or is KOffice alreading heading for ODF 1.2 (OOo 3)?"
    author: "Birdy"
  - subject: "Re: Which ODF version"
    date: 2008-07-18
    body: "1.2. Actually, we're driving some of the innovations there."
    author: "Boudewijn Rempt"
  - subject: "Re: Which ODF version"
    date: 2008-07-18
    body: "Yes, especially since 1.2 brings many improvements on clarity of the spec for better interoperrability. That said, all of ODF 1.2 (or 1.1) won't be covered by KOffice, since only of the subset of the features are supported."
    author: "Cyrille Berger"
  - subject: "Re: Which ODF version"
    date: 2008-07-19
    body: "> all of ODF 1.2 (or 1.1) won't be covered by KOffice, since only of the subset of the features are supported\n\nDo you mean KOffice 2.0 by that? I wonder why one should not aim to support the spec totally."
    author: "Stefan Majewsky"
  - subject: "Re: Which ODF version"
    date: 2008-07-19
    body: "Part of it is a 2.0 thing -- text tables, for instance, are unlikely to be in 2.0. Part of it is a platform thing: OLE objects, DDE or embedded Java applets are unlikely to ever get in. And part of it is the sheer task of implementing the references standards, like SMIL."
    author: "Boudewijn Rempt"
  - subject: "Thank you"
    date: 2008-07-19
    body: "You are doing a great job. Thank you so much."
    author: "kdeuser"
  - subject: "Congrats"
    date: 2008-07-19
    body: "Huge congratulation.\n\nIt is so very important that we have a contender that can match with open office.\n\nOpenOffice has numerous small user interface problems, and I just feel that KOffice is more likely to listen to users than OpenOffice ever will (plus, OpenOffice feels rather sluggish)"
    author: "markus"
  - subject: "Save as ..."
    date: 2008-07-19
    body: "Is there going to be a 'Save as (.doc, .xls, ...)' option? If KOffice allows saving as MS Office formats, it will accelerate its use in office environments where OO.o is being used today.\n"
    author: "Kanwar"
  - subject: "Re: Save as ..."
    date: 2008-07-19
    body: "KWord already has a save as .doc option. Of course, what it does is save in the rtf format, but that's okay -- Microsoft seems to do the same when word saves for a different word version. As for KSpread and compatibility with the excel file format, probably not, unless someone steps up and starts coding."
    author: "Boudewijn Rempt"
  - subject: "Thanks!"
    date: 2008-07-19
    body: "I'm really looking forward to Koffice2. Congrats to the many talented developers on their progress. And, thanks!\n"
    author: "T"
  - subject: "So, is it going to be use more than Koffice 1.x ?"
    date: 2008-07-19
    body: "I've  on ocasion used Kword 1.x mainly to print images and acompagnied text with it. It was not the purpose but it dit it well. \nThe rest og the software in Koffice was totally a wast of time and embarissing quality wise. The should not have been anything else than Kword.\nHow are things now? I've tryed at couple of alfa/betas or whatever - last time about 6 months ago - and the same as with 1.x series. Kword was more or less usable - the rest of the apps mainly crap.\nCould the \"leadership\" plese dump most apps and concentate on Kword? At least do the one important app well; in stead of having 10 halv baked apps its better to have one well done.\n\nCheers"
    author: "Jand"
  - subject: "Re: So, is it going to be use more than Koffice 1.x ?"
    date: 2008-07-19
    body: "I'm assuming you are only half-informed about the status of Koffice apps. I for one would say Krita is a lovely comprehensive app. Kpresenter is also really nice and Kexi is showing a lot of promise. Kspread may not be in an ideal state and kword surely lacks behind atm (but is shaping up pretty nicely). I strongly encourage you to dig a bit deeper before posting in a diffuse way. In fact it's a far way from 1.x to Koffice 2, so many things have changed under the hood. And btw.... I think, there is no \"leaderhip\" - whatever you might have in mind; there is no such thing I assume."
    author: "Thomas"
  - subject: "Re: So, is it going to be use more than Koffice 1.x ?"
    date: 2008-07-19
    body: "You know, that's the beautiful thing about having choices.  You are free to use what works for you.  It's obvious from your post that KOffice doesn't work for you.  So be it.  By all means don't use it, because calling other people's work crap (and a waste of time as well) accomplishes nothing.  On second thought, it does make you seem like an ungrateful brat.  I for one am pulling for KOffice.  I will try to use it when 2.0 is released.  If it works for me, great.  If not, I will use something else.  Even then though, I will continue to support and encourage the developers.  And as soon as it works for me I will use it full time.  Keep up the good work KOffice team."
    author: "cirehawk"
  - subject: "Re: So, is it going to be use more than Koffice 1.x ?"
    date: 2008-07-20
    body: "KOffice 2.0 has undergone some major refactorings, so in final 2.0 release, it might not have all intended features and still have tons of bugs (or if you prefer to call it... crap?). But the most important thing is, the framework is now ready to take us to the future (2.1 and beyond), so that it will be easier to add further cool new features. I guess you have to do more homework to know what are the cool new technologies behind KOffice ;-).\n\nI think I've heard that KOffice 1.x was already stuck with few things that are hard to improve due to the old framework and the old Qt 3 (notably the text rendering)"
    author: "fred"
  - subject: "Simultaneous collaboration, like Abiword?"
    date: 2008-07-19
    body: "What's the status of the collaboration feature (listed as \"Nice to Have\" on the roadmap) with regard to KOffice 2.0?\n\n\n"
    author: "D R Evans"
  - subject: "Re: Simultaneous collaboration, like Abiword?"
    date: 2008-07-20
    body: "It still would be nice to have :-). At last year's summer of code, we had a student interested in implementing it, but he never produced any code. And right now, everyone is pulling to get KOffice release-ready, so it's extremely unlikely this feature will be in 2.0."
    author: "boudewijn"
  - subject: "Effects for Kword :-)"
    date: 2008-07-21
    body: "Exelent work guys! You are doing such great office tool for normal user. It might not beat the Microsoft Office because for it has great amount of plugins / scripts etc... but for normal user, student and writer, Koffice is already enough. \n\nWe need to go step by step until we can get bigger user base from windows / Mac OS X side too. First we need to get 2.0 and then 2.1 (you know it ;-)) when more people starts to use it, it will get attension what it reserves and get more coders!\n\nI'm waiting 2.0 but what I'm still waiting, is this kind \"eye candy\" in future versions (sooner than later please ;-)), to give better usability to see the sheets difference itself. Do not mistake the \"fit the page\" zoom slider, it should be then without effects but when effects ON, it would be need to have such amount of space :-)\n\n/me is going to install 2.0 final when it hits out.... "
    author: "Fri13"
  - subject: "Re: Effects for Kword :-)"
    date: 2008-07-21
    body: "Ah, forgot to ask, is there coming fix for the \"kword\" panel on left side?\nIt is always 2 rows when moved and can not be moved away to other panels because it does not change to horisontal size. Need to check is there bug report already, but I hope it can be configured to be vertical or horisontal and then have it even on one row, when it would take the whole vertical space and would not look so \"alone\" then :-)\n\n"
    author: "Fri13"
  - subject: "Re: Effects for Kword :-)"
    date: 2008-07-21
    body: "Yes, a preliminary fix for that is already in alpha9."
    author: "Boudewijn Rempt"
  - subject: "Re: Effects for Kword :-)"
    date: 2008-07-22
    body: "preliminary fix?  I fixed all those issues in SVN and got nobody bringing up any issues.\nWhats preliminary about it? (and why do I have to hear it via the dot)"
    author: "Thomas Zander"
  - subject: "Integrated Apps"
    date: 2008-07-21
    body: "Hi\n\nOne feature of OpenOffice i really like, is the ability to open another document type e.g. spreadsheet, from Writer and visa versa. Does 2.0 allow you to do this, if not, are there any plans to do so?\n\nregards\n\nIan"
    author: "Ian"
  - subject: "What's that theme?"
    date: 2008-07-21
    body: "Hi\n\nWhat theme is being used in Krita? It looks much better and more usable than my default Oxygen. Is is an update of Oxygen or a modification?\n\nCheers"
    author: "My Themey"
  - subject: "Re: What's that theme?"
    date: 2008-07-21
    body: "Nope -- just bog-standard KDE 4.1rc1 Oxygen. I did make the fonts smaller and tweaked the colors and contrast a little. I still it's a pity I cannot make the scrollbars emerald green anymore -- but all in all, I like Oxygen very, very much."
    author: "Boudewijn Rempt"
---
The KOffice team <a
href="http://koffice.org/announcements/announce-2.0alpha9.php">announces
the availability of the ninth alpha release of KOffice 2.0</a>. With
KDE4 becoming more stable by the week, KOffice development
is picking up at a fast pace and developers who previously had
trouble keeping up are now getting active again, leading to a much
increased rate of commits for KOffice. Both the NLnet sponsored
Girish Ramakrisnan, who is working on OpenDocument support, and the <a
href="http://www.valdyas.org/fading/index.cgi/hacking/summer_of_code/2008.comments">KOffice
Google Summer of Code students</a> are delivering solid work.














<!--break-->
<div style="float:right; border:1pt solid grey; margin: 6px; padding: 6px">
<a href="http://static.kdenews.org/danimo/krita_editable_text.png">
<img src="http://static.kdenews.org/danimo/krita_editable_text_small.png" border="0" /><br /><small>Using editable rich text (e.g. in Krita) is now possible.</small></a></div><p>Apart from much invisible, but very important work on improving core
funcationality like ODF support, text handling and other infrastructure,
important visible areas of progress in Alpha 9 are:</p>

<p>KSpread has regained support for printing. It is possible to print
a range of sheets or a selection of cells.</p>

<p>The Kexi report generator can now generate reports in html and ODS,
the OpenDocument spreadsheet file format, which KSpread and OpenOffice
Calc can read.</p>

<p>Vector layers in Krita can contain text and vector shapes. Editable,
rich text in Krita is now a reality. Even nicer, it has become possible
to add filter layers and masks to Krita, delivering live filter effects
on vector shapes.</p>

<p>The text shape object, which is the basis for KWord and provides
editable rich text in all KOffice applications has gained a visual way
of changing the paragraph layout.</p>

<p>KOffice-wide, a new implementation of guides provides snapping to
guides and dragging of guides for all flake shape objects.</p>

<div style="float:right; border:1pt solid grey; margin: 6px; padding: 6px">
<a href="http://static.kdenews.org/danimo/kotext_paragraph_tool.png">
<img src="http://static.kdenews.org/danimo/kotext_paragraph_tool_small.png" border="0" /><br /><small>New for text: visually alter your paragraph spacing. </small></a>
</div>

<p>The Google Summer of Code students have been hard at work: Lukas Tvrdy
has implemented the core of a chinese painting brush engine. Benjamin
Cail has ported most of the .doc import filter to make it convert to odf
instead of the old native KWord file format, cleaning up and improving
our ODF code along the way. ODF support is really being focussed upon
with both Piere Ducroquet and  Carlos Manuel Licea Vázquez putting in a
lot of work. Piere has been implementing document variables and Carlos
has made a lot of progress converting KPresenter to use ODF natively
instead of its own old file format. Lorenzo Villani is very productive
and has already shown off <a href="http://blog.binaryhelix.net/">his Kexi
web forms work on his blog</a>, until his computer started smoking! The
calligraphy tool Fela Winkelmolen is writing for Karbon is coming along
very nicely and is already completely usable. Fredy Yanardi, finally,
is shouldering a lot of development work on Kpresenter, focussing on
the presentation notes feature.</p>

<p>But all KOffice developers have been working really hard
on improving all components of KOffice. Please go to the <a
href="http://koffice.org/announcements/changelog-2.0-alpha9.php">changelog</a>
for more details! Or install KOffice on the operating system of your
choice.</p>

<p>For more screenshots, please visit our <a
href="http://koffice.org/announcements/visual-changelog-2.0-alpha9.php">visual
changelog</a>. The KOffice team would really welcome volunteers to
maintain the visual changelog!</p>

<p>The KOffice team intends to continue delivering montly
alpha releases until we have implemented all features in our <a
href="http://wiki.koffice.org/index.php?title=Schedules/KOffice/2.0/Feature_Plan">feature
plan</a>: thereafter we will deliver beta releases until those features
are stable. Coincidental with the release of Alpha 9, KOffice has entered
the feature freeze stage.</p>







