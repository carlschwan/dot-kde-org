---
title: "Qt 4.5 and Qt Creator Reach Beta Status"
date:    2008-12-19
authors:
  - "aleisse"
slug:    qt-45-and-qt-creator-reach-beta-status
comments:
  - subject: "Trolltech doesn't exist anymore"
    date: 2008-12-18
    body: "Actually article should say that Nokia has released betas of Qt 4.5 and..."
    author: "IA"
  - subject: "Re: Trolltech doesn't exist anymore"
    date: 2008-12-18
    body: "It did say that Qt Software did release it, which is technically correct since Qt Software is a division of Nokia. :) The only mention of Trolltech I saw was the website link."
    author: "Jonathan Thomas"
  - subject: "Re: Trolltech doesn't exist anymore"
    date: 2008-12-19
    body: "They should have kept the name, Trolltech, I just find that much better. Well, I guess I'm just picky on names, it pains me to use KDE, let alone use applications that start with K. \n\nYou don't understand what it does to me. ;)"
    author: "Jeremy"
  - subject: "Re: Trolltech doesn't exist anymore"
    date: 2008-12-19
    body: "Hmmmm, imagine how they feel about it. They went from being the trolls to being cuties. You can't argue with that... ;-)"
    author: "Jos Poortvliet"
  - subject: "The source, the source!"
    date: 2008-12-18
    body: "The source! I've been using Qt Creator on my work mac for quite a few weeks now and really like it. But all that time I wanted to browse the source. (And on my private linux thinkpad, I'm using Kdevelop4, and I'm really liking that, too.)"
    author: "Boudewijn Rempt"
  - subject: "Re: The source, the source!"
    date: 2008-12-20
    body: "Indeed.\n\nThank you Nokia/Qt Software for releasing the source code.\n\nDerek\n"
    author: "dkite"
  - subject: "KDevelop and QtCreator"
    date: 2008-12-18
    body: "I wonder if there are any plans from within Qt Software to cooperate with KDevelop? I've been building KDevelop4 from SVN for some time, and the C++ support is beyond awesome :-) I haven't tried the debugger yet, but I've heard it still has some way to go, while the QtCreator debugger seems to be very far already.\n\nComments?"
    author: "Thorben"
  - subject: "Re: KDevelop and QtCreator"
    date: 2008-12-18
    body: "Oh, and I forgot to say this: A huge thanks to Qt Software for releasing QtCreator under the GPL, we shouldn't forget what great gifts they keep giving us :-)\n\nCheers!"
    author: "Thorben"
  - subject: "Re: KDevelop and QtCreator"
    date: 2008-12-19
    body: "Trolltech should have got with the programme years ago and decided that they needed a fully fledged cross-platform IDE designed for Qt, using Qt. Instead what we got was half-baked measures like using Visual Studio with Qt (people use Qt to get away from MS dev tools), trying some half-baked integration with Eclipse and half-baked, halfway-house applications like Qt Designer. A visual designer, but not an IDE which you needed to get separately and where most sane people would expect a visual designer and an IDE to have s decent degree of integration. The framework is first rate, but it has always been let down by the default development tools, or lack of them.\n\nI think the KDevelop developers are entitled to get a bit miffed that the Qt people have finally seen the need in a dedicated IDE for Qt development, that they didn't get any direction or code or component sharing on this from Trolltech years ago and they've just turned up now."
    author: "Segedunum"
  - subject: "Re: KDevelop and QtCreator"
    date: 2008-12-20
    body: "The KDevelop people are not miffed; they all are ok with it :)  And its not a surprise to them and are some pretty big core components that are shared between the two apps.\n\nIn other words; no need for panic or even annoyance.  Qt Creator doesn't even make it easy to compile kdelibs (no cmake support), its meant for different users.\n\nBottom line is; try it out, see if it works for you and use it, either app.  And they are both open source so improve what you don't like."
    author: "Thomas Zander"
  - subject: "Re: KDevelop and QtCreator"
    date: 2008-12-21
    body: "What are those \"core components\" being shared? Such wasn't mentioned anywhere so far afaik. (And don't answer \"Qt\". =P)"
    author: "Anon"
  - subject: "Re: KDevelop and QtCreator"
    date: 2008-12-22
    body: "The designer part is shared I think."
    author: "JohnFlux"
  - subject: "Re: KDevelop and QtCreator"
    date: 2008-12-23
    body: "But Qt Designer is old hats, isn't it? So is there anything new actively shared after KDevelop 4 and Qt Creator started?\n\nAlso dot's flatforty is broken again, please fix whoever runs this place. =S"
    author: "Anon"
  - subject: "Speed?"
    date: 2008-12-18
    body: "Hello,\n\nIt was said 4.5 will bring many optimizations and speed improvements. Does compilation of 4.2 against 4.5 is faster/lighter?"
    author: "m."
  - subject: "Re: Speed?"
    date: 2008-12-18
    body: "The performance improvements done in 4.5 have nothing to do with the compilation time. In fact, you can expect 4.5 to take 20 to 50% longer to compile than Qt 4.4, which was already 100% slower than 4.3.\n\nSo, it takes some more minutes to compile Qt 4.5, but you get improvements all over the board once it's up and running :-)\n\nIf the \"4.2\" you mentioned was KDE (not Qt), then, yes, there are improvements. I haven't seen any significant memory usage differences, but painting performance should be better. You can also try launching individual applications with the \"-graphicssystem raster\" or \"-graphicssystem opengl\" option and see if it makes a difference. (native, the default, is the slowest of the three graphicssystems supported on X11)"
    author: "Thiago Macieira"
  - subject: "Re: Speed?"
    date: 2008-12-19
    body: "<i>In fact, you can expect 4.5 to take 20 to 50% longer to compile than Qt 4.4, which was already 100% slower than 4.3.</i>\n\nAaaargh! It was already taking 8 hours on my 1.4Ghz iBook!"
    author: "David Johnson"
  - subject: "Re: Speed?"
    date: 2008-12-19
    body: "Could you briefly explain what's the technical difference between \"-graphicssystem opengl\" and \"-graphicssystem raster\"?\n\nThanx in adavance!"
    author: "Matthias"
  - subject: "Re: Speed?"
    date: 2008-12-19
    body: "I'm not a graphics engineer, so I don't know the full details. But a high-level explanation of what the graphics system does: it is the entire paint engine (drawing primitives, etc.), plus backing store and a few more things. The three available graphics systems on X11 are:\n\n - native (X11)\n - raster (software, written entirely inside Qt, also known as \"QImage\")\n - OpenGL (hardware-accelerated, using OpenGL)\n\nThe \"native\" engine is what we've always used. That means a QPixmap is actually an X11 pixmap, which is why you can't use QPixmap from outside the GUI thread. When you draw on a QPixmap or on a QWidget that is using the native engine, Qt will end up eventually making X11 calls to draw the primitives.\n\nNow, the \"raster\" engine itself was already inside Qt: it's QImage. Using it is somewhat similar (but not fully) to calling widget->render(&image), then asking X11 to simply display that image. It's more than that because there's the backing store to be considered...\n\nAnd the OpenGL one is the same, except that instead of using Qt's own drawing primitives, we ask the OpenGL library to do it. But you must note that OpenGL was *not* designed to draw pixel-perfect widgets, but instead 2.5D and 3D, where 1px differences don't really matter. So, in order to accomplish pixel perfection, you need good hardware, because Qt asks for a lot from your OpenGL.\n\nThe interesting thing is that drawing everything in the Qt application then transferring the whole image to the X11 server is faster than asking the X11 server to draw. Go figure...\n\nSee: http://labs.trolltech.com/blogs/2008/10/22/so-long-and-thanks-for-the-blit/"
    author: "Thiago Macieira"
  - subject: "Re: Speed?"
    date: 2008-12-19
    body: "Considering that modern graphics hardware doesn't have any 2D video acceleration (unless you use the 3D hardware to do it), it doesn't surprise me at all that rendering everything in software on the client is faster than using X11.\n\nConsidering how variable (slow) some drivers (NVidia) seem to be on X11, maybe going for all-software drawing is a better idea. KDE 4 actually seems to run faster on my machine using the VESA driver than it does with NVidia's driver, despite not supporting any 2D acceleration at all.\n\nAs far as I understand, that's how things have always worked in Mac OS X - the contents of each window is drawn using software, and sent to the graphics as a big image, which is composited on-screen using the 3D hardware. I believe that Vista works this way too, and Microsoft's newer libraries (like GDI+ or .Net WinForms) do all rendering in software even on Windows XP.\n"
    author: "BlackAura"
  - subject: "Re: Speed?"
    date: 2008-12-20
    body: "If VESA is faster on you system than nvidia.ko, consider updating your nvidia driver to 180.16. The nvidia engineers have done a lot to fix those problems."
    author: "sebas"
  - subject: "Re: Speed?"
    date: 2008-12-20
    body: "\"Considering that modern graphics hardware doesn't have any 2D video acceleration (unless you use the 3D hardware to do it), it doesn't surprise me at all that rendering everything in software on the client is faster than using X11.\"\n\nThat's a silly fallacy. It's exactly like saying you can't run email clients on the CPU, unless you use web browser hardware to do it. \nModern gpus are highly programmable and have no problems accelerating any 2D graphics api.\nOne could claim that the latency of having simple alpha-blits run through the entire pipeline instead of dedicated silicon that shotcuts it is a lot higher, but then someone will point out that modern 2D graphics also has gradients, paths, masks and extended blend modes rendering the argument for dedicated 2D silicon moot (plus optimizations for simple alpha-blends in the general pipeline would obviously profit all cases of straight-forward quad texturing making them a lot more relevant)"
    author: "Zack Rusin"
  - subject: "Re: Speed?"
    date: 2008-12-25
    body: "Fine. Modern graphics cards are nicely programmable, and you can do all kinds of neat 2D stuff with them very quickly. In theory. But this is reality, and things don't work that way unless you write every application in OpenGL.\n\nThe _only_ time I've seen fast 2D on a Linux system in recent years is using an older Intel onboard graphics chip, which still has dedicated 2D hardware. All the 2D operations exposed through X, including XRender, are pretty fast, and the desktop is very fast and responsive. Not only is it faster than NVidia graphics hardware on Linux, but it's faster than NVidia graphics hardware on Windows as well.\n\nHell, that was on an older EeePC, which is about as underpowered as you can get. It runs rings around my Nvidia-powered desktop machine.\n\nThe problem is that the 2D APIs we have (like X11, XRender, or GDI) are not designed to run on top of 3D hardware, which has a completely different programming model than older 2D hardware. Operations involving logical operations, or bitmasks, or colour palettes make absolutely no sense in terms of modern 3D hardware, but that's what our 2D system uses.\n\nIt is possible to implement this on top of a modern 3D card. However, that would take a huge amount of work, and until very recently, the likes of NVidia just haven't bothered. Even on Windows, every new generation of graphics card gets slower and slower at 2D operations, and on Linux it's far worse. These kind of operations, particularly the more advanced XRender stuff, are so slow that a decent software implementation is several times faster.\n\nIn the future, we can use stuff like Cairo, or Qt's OpenGL-based painter to get performance improvements, but right now, I don't have any applications that use OpenGL to do 2D. They all use the old 2D APIs.\n\nAnd the fact remains that I can get far better desktop performance by sticking an ancient GeForce 4 MX in my machine that I could from any new graphics card. It's odd that a bottom-end card from 6 years ago, which is in fact a rebadged GeForce 2 from 2000, can beat pretty much anything modern from Nvidia."
    author: "BlackAura"
  - subject: "Re: Speed?"
    date: 2008-12-28
    body: "Update your NVIDIA driver to version 180.18. The 17x.yz series is too buggy."
    author: "The Devil"
  - subject: "Wrong"
    date: 2008-12-18
    body: "> Today is the day of beta releases\n\nopenSUSE 11.1 is no beta release."
    author: "Anonymous"
  - subject: "Re: Wrong"
    date: 2008-12-18
    body: "That doesn't invalidate this article at all.. but nice attempt to hijack the topic..."
    author: "Dan"
  - subject: "What if ?"
    date: 2008-12-18
    body: "Now that KDE4 is based on QT4, what happens to the KDE project if say next year or something Trolltech releases QT5 ?"
    author: "Kamesh"
  - subject: "Re: What if ?"
    date: 2008-12-18
    body: "That won't happen. Qt 5 is not planned at all."
    author: "Thiago Macieira"
  - subject: "Re: What if ?"
    date: 2008-12-18
    body: "Same thing that happened when they released Qt4.  If it is better, KDE will eventually port to it and release KDE 5.  Of course that's not going to be next year, but more like 5+ years from now.  "
    author: "Leo S"
  - subject: "Re: What if ?"
    date: 2008-12-18
    body: "I hope it takes 20 or 40 years....\nThe transition from 3.X to 4.X shows that KDE needs time to mature the code instead of rebuilding everything again and again often :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: What if ?"
    date: 2008-12-19
    body: "The 3.x to 4.x transition was more than just porting to Qt4, it was also used as an oppertunity to rearchitecture KDE as a whole to be able to continue advancing beyond the limits that were being reached. Unless the KDE project dies out, there will definitely be another major revision within 20 to 40 years... regardless of how forward you think you can't predict how the world of computers will be in even a decade, much less two - four."
    author: "Kit"
  - subject: "Re: What if ?"
    date: 2008-12-19
    body: "If Qt5 were to come anytime soon (which it doesn't seem it will, Qt4 is doing just fine =) it would be more like the Qt2->Qt3 change which would cause something more like the KDE 2.2 -> KDE 3.0 transition than the KDE 1.1.2 -> KDE 2.0 or KDE 3.5 -> KDE 4.0 transitions."
    author: "Aaron Seigo"
  - subject: "Re: What if ?"
    date: 2008-12-19
    body: "I think I read somewhere that Qt5 wouldn't break the Qt4 API or at least not as much Qt4 broke Qt3 API. Is that true?"
    author: "kubunter"
  - subject: "Re: What if ?"
    date: 2008-12-19
    body: "Seeing as how Qt5 is not planned at all, whoever said that was making it up."
    author: "Leo S"
  - subject: "Re: What if ?"
    date: 2008-12-19
    body: "Then Aaron was making things up because it was on his blog:\n\nhttp://aseigo.blogspot.com/2007/10/qt-roadmap.html\n\nquote:\n\n\"If a Qt5 were to happen, it wouldn't be before 2011/2012 and it would not be a major API design break with Qt4 as Qt4 was to Qt3\".\n\nThere. Who's making things up now?"
    author: "kubunter"
  - subject: "Re: What if ?"
    date: 2008-12-19
    body: "Trolltech/Nokia has said there won't be a major API break between Qt4 and Qt5. If you thought the KDE3->KDE4 transistion was painful, imagine what it was like for all Trolltech's paying customers. "
    author: "David Johnson"
  - subject: "QT"
    date: 2008-12-19
    body: "QT is brilliantly designed software, it is a great surprise that still other toolkits are around."
    author: "Andre"
  - subject: "Re: QT"
    date: 2008-12-20
    body: "I think it has to do with some people's dislike of C++.\n\nCould be due to a lack of a vital mineral when growing up :)\n\nDerek"
    author: "dkite"
  - subject: "Re: QT"
    date: 2008-12-24
    body: "It uses the wrong bits of C++. Macro trickery instead of templates. I'm new to it all, but the whole QOBJECT/MOC thing is a bit to get used to after using the STL and Boost.\n\nNot sure if other toolkits do it better, but that bit of QT (admittedly a very small part of it) makes me feel unclean."
    author: "Russ"
  - subject: "Re: QT"
    date: 2008-12-25
    body: "I fully agree!\nBut all this trickeries are based on historical necessity: There wasn't good support for templates in compilators in the time when Qt was establish. I hope that in the future the support of templates in Qt will change, because modern compilators have good support of templates and many \"old stuff\" compilators are planed to beeing unsupported in future Qt versions. But I don't belive that it will be in QT5."
    author: "Jiri"
  - subject: "Re: QT"
    date: 2008-12-26
    body: "QObject is not a workaround for templates, and I don't think the stuff QObject does could be done using templates.\n\nSTL and Boost are unrelated matters, since they don't provide the signal/slot mechanics. In fact, it's one of the things that takes Qt a notch above all others - it's only a slight pain at build time, but pays back generously in practical development (as opposed to e.g. observer pattern that quickly starts sucking when dealing with object ownership, N..N relationships etc). Notably, they don't really complicate debugging at all.\n\nSumma summarum: you'll get over it and start loving it."
    author: "Ville"
  - subject: "Re: QT"
    date: 2008-12-29
    body: "Boost does not provide signal/slot mechanics? Have you EVER heard of Boost.signals. Had been in boost for ages. Very customizable too. And yes, I love it."
    author: "Evg"
  - subject: "Re: QT"
    date: 2009-01-02
    body: "Frankly, I hadn't heard about boost.signals.\n\nThe syntax appears a bit more cumbersome (usage of operator(), than with Qt's metaobject system, but that's just by a quick glance).\n\nCertainly it's something I'd consider for non-gui parts of an application, but mostly because of Qt licensing concerns."
    author: "Ville"
  - subject: "Designer?"
    date: 2008-12-23
    body: "I pulled qt-creator from git, and compiled successfully. It runs fine, except that I have no Designer integration. Clicking on a .ui file only shows the xml. What did I do wrong :)"
    author: "christoph"
  - subject: "Java bindings"
    date: 2008-12-26
    body: "Will Java ever be supported in QT creator ?"
    author: "M3"
---
Today is the day of beta releases: <a href="http://www.trolltech.com">Qt Software</a> has released betas of <a href="http://trolltech.com/about/news/qt-4.5-beta-previews-64-bit-on-mac">Qt 4.5</a> and <a href="http://trolltech.com/developer/qt-creator/qt-creator">Qt Creator</a> &mdash; two Qt releases just in time for the holidays!






<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/qt_creator_visualizeqstring.png"><img style="border:0px;" src="http://static.kdenews.org/danimo/qt_creator_visualizeqstring_mini.png" /></a><p style="font-size:small;"><a href="http://static.kdenews.org/danimo/qt_creator_visualizeqstring.png">Debug: Visualizing QStringList</a></p></div>
<p>The greater news concerns Qt Creator this time: the complete source code is publicly available under the GPL from now on. Everybody interested in the development of the latest addition to Qt's tool family should head over to the <a href="http://labs.trolltech.com/gitweb?p=qt-creator;a=summary">repository</a> and take a look. Qt Creator is intended to make cross-platform development with Qt as easy as possible - especially to those who are new to developing Qt applications.</p>
<p>You can find the complete announcement on <a href="http://trolltech.com/about/news/qt-creator-ide-beta-released">trolltech.com</a> and all additional informations are on the freshly updated <a href="http://trolltech.com/developer/qt-creator/qt-creator">Qt Creator page</a>. And of course, there is an <a href="http://trolltech.com/about/news/qt-4.5-beta-previews-64-bit-on-mac">announcement</a> regarding Qt 4.5 beta, too. It highlights new support for <a href="http://trolltech.com/products/appdev/platform/qt-for-mac">64-bit Qt Apps on Apple's Cocoa Framework</a> this time.</p>




