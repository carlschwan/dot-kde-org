---
title: "Kate Developer Sprint Next Month"
date:    2008-03-25
authors:
  - "dhaumann"
slug:    kate-developer-sprint-next-month
comments:
  - subject: "vi rox!"
    date: 2008-03-24
    body: "Wanna have the best editor as part of KDE? Then think about making some vi-Klone. I hate having to press six quad-bucky combinations to do anything useful... And boy, would it be kool to have a vi capable of using kioslaves and embeddable in other apps as a kpart..."
    author: "bucky hater"
  - subject: "Re: vi rox!"
    date: 2008-03-24
    body: "Well, technically there IS a vi-style KPart in development, and it's called YZis, URL: http://www.yzis.org/ --sadly, its development is rather on the 'stalled' side :/"
    author: "Giuseppe \"Oblomov\" Bilotta"
  - subject: "Good news, everyone!"
    date: 2008-03-24
    body: "Actually I'm going to Darmstadt to the Kate developer sprint to discuss such a possibility. I am a vi(m) lover who have previously worked on Yzis and I'm going to meet the other developers and see how such a feature could be implemented in the best, fastest and easiest way. :)\n\nI am applying for this project for GSoC, so hopefully this isn't too far off."
    author: "Erlend Hamberg"
  - subject: "Re: Good news, everyone!"
    date: 2008-03-25
    body: "Good news, indeed!\nI was tracking the yzis progress and was actually trying to use it two years ago,\nAnd it really really good that it is reviralising again!"
    author: "pilpilon"
  - subject: "Re: Good news, everyone!"
    date: 2008-03-25
    body: "I should make myself a bit more clear. :)\nThis is not Yzis. I talked about implementing a vim-like mode for the Kate kpart.\n\nThe result should be the same, though; a usable, vim-like editor for KDE."
    author: "Erlend Hamberg"
  - subject: "Re: vi rox!"
    date: 2008-03-24
    body: "You're so right!\n\nBtw, I don't understand the Kate \"role\" wery well. I mean: for editing a single file there is Kwrite which also has syntax highlitingh and so on. For editing multiple files at a time (which itself it means you're working on a project) there is Kdevelop. Is there something I am missing?\n\nForgive my bad english, I'm not a native speaker."
    author: "Alessandro Guido"
  - subject: "Re: vi rox!"
    date: 2008-03-24
    body: "I like kate because I use it for a lot of stuff, and lots for languages, many of which kdevelop doesn't support, so it is lighter and easier to use.\n\nBut I *really* loved the way kate worked a few releases back, where when you clicked on a file or opened one on the shell the same kate instance was re-used, instead of opening a new one.\nI don't want to troll, and I know this has been discussed several times in bug reports and on the ML, but just wanted to say, I used it, I loved it, and I miss it :)"
    author: "Ivo Anjo"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "Is it \"usability\"?"
    author: "reihal"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "\"kate -u\" does the job.\nYou can also define an alias such as:\n\nalias kt='kate -u'\n\nand use it like this: \n\nkt *.cpp"
    author: "Anon"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "True, but the default usage should be open the selected file straight away, not have a session choice. Entering \"kate -u\" is not obvious to users who are not technical/*nix literate."
    author: "Ian"
  - subject: "Re: vi rox!"
    date: 2008-03-26
    body: "Yes, I would like to see Kate return to this behavour as well."
    author: "Kerr Avon"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "The fact that you are working with multiple files doers not necessary mean you are working on a project. Or if you do, it's not given that a KDevelop (or Quanta) project is up to the task. Sometime you want a editor able to handle multiple files, without the overhead of project management. For instance when you are working on bigger projects, or parts of such, like KDE, Linux kernel or something like uClinux. Sometimes you are only interested in working on a subset of the files, and very often there are no KDevelop project file."
    author: "Morty"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "Indeed. I use Kate to write the articles during KDE meetings, and it rocks. KDevelop isn't really what I'm looking for, as you can imagine..."
    author: "jospoortvliet"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "Probably that there are lots of sysadmins using it to edit scripts or config files :P I mean, not everyone is a developer"
    author: "Vide"
  - subject: "I use Kate for..."
    date: 2008-03-25
    body: "I use Kate for Fortran development, and small C and Python projects as well.  I have tried many other tools, but I find that I like the simplicity that Kate provides, but still has a nice interface and many features.\n\nAt the university I attend and study mechanical engineering, many of my peers have to develop on the linux clusters for just a few of their projects, so they don't know linux very well.  After I introduce them to Kate and how to start it through an X11 tunnel, they tend to use Kate almost like a universal interface to the cluster.  It has a terminal, it has file editing, and file management too, so it fills all of their needs in one program.  Not what it was intended for, but it still seems to work well for them."
    author: "Kyle Horne"
  - subject: "Re: I use Kate for..."
    date: 2008-03-25
    body: "Same here -- for small Python projects that don't require the full IDE machinery, I love the speed and simplicity of Kate. Plus I can usually count on it being available in a typical Linux system, which is rarely the case for KDevelop."
    author: "Otter"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "I should have mentioned that Kate consists of several parts:\n1. Kate Editor Component, also called Kate Part\n2. Kate Application, also called Kate\nKDevelop uses the Kate Part. The meeting of course also focuses on the Kate Part. All applications using the Kate Part benefit automatically if it improves."
    author: "DominikH"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "I use vi(m) when doing remote system or network administration.\n\nKwrite is my tool of choice when I write plain text documents that I will later copy-and-paste to some web sites or email around or some such.\n\nFully fledged kate is it when I hack on code. I don't want a complete IDE.More often than not, it gets in my way. The minimalistic project management kate offers is exactly what I want. So that is its role from my POV.\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "KDevelop uses a very fix project file/directory structure, not useful in many cases."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: vi rox!"
    date: 2008-03-25
    body: "I think that's where I ran into problems when I tried to use it. I only do PHP development at the moment (websites and plugins for WordPress) and find Kate + Sessions a really simple way to get to what I want quickly - and also switch from one \"project\" to another mid-stream when I get an idea*...\n\n\n\n(* have an episode of poor focus)"
    author: "Martin Fitzpatrick"
  - subject: "Re: vi rox!"
    date: 2008-03-26
    body: "> For editing multiple files at a time (which itself it means you're \n> working on a project) there is Kdevelop\n\nKdevelop sucks. It's just a more complicated version of kate. Of cource, if you're developing with c/c++/qt, it's useful, but for every thinks else there is'nt any advantage. And now, that kdevelop3 has only this stupid spacewasting simple simpleIDEA-UI left, there is'nt any reason to use it.\n\nI'm developing mostly python and pyqt-apps and use all the time kate. Of cource, kate sucks as well, but less. The UI can be minimalistic, and most of the missing features can be substitute with some nice scripts."
    author: "Chaoswind"
  - subject: "Re: vi rox!"
    date: 2008-03-26
    body: "do on localhost:\nvi </path/to/filename>\n\ndo on remote host:\nssh user@remote.host\nvi </path/to/filename>\n\nor:\nsshfs user@remote.host:/home/user ~/mountpoint\nvi ~/mountpoint/path/to/filename\n\nvi(m) is the best text editor. Fuse is the best when you need transparent remote drives for every app (or the new GIO, which use Fuse :P )\n"
    author: "anonymous"
  - subject: "Re: vi rox!"
    date: 2008-03-26
    body: "Yeah yeah, vi is great if you love to work in an archaic piece of sh*t on a commandline. If you're more the point-and-click type like most modern computer users, Kate is the best there is. (disclaimer: I myself use both Vi and Kate)\n\nAnd does GIO use Fuse? I thought it didn't - it only might offer a Fuse interface in the future, like KIO-fuse (which is already old news - and nobody uses it)."
    author: "jos poortvliet"
  - subject: "not much to do..."
    date: 2008-03-24
    body: "make it get the focus to the text edit area when clicking into file selection area.\nannoys me every time kate is in background an i click to raise it and can't type cause the focus is in the wrong part of the window.\n\notherwise it is already the best editor :)"
    author: "anon"
  - subject: "Re: not much to do..."
    date: 2008-03-25
    body: "i don't need the feature myself, but i have heard people requesting find/replace in all open files - http://bugs.kde.org/show_bug.cgi?id=156099"
    author: "richlv"
  - subject: "well. That's easy"
    date: 2008-03-24
    body: "It already is the best text editor on earth.  Save yourself some work and go have a beer.\n\nIs Yziz dead btw?"
    author: "Velvet Elvis"
  - subject: "x-symbol "
    date: 2008-03-24
    body: "I'm still hoping to get X-Symbol like capabilities someday. This would allow to parse tokens and display them as symbols. Some examples of applications are:\n\nLaTeX:  \nParse \\prod and replace it with a product symbol, \\rightarrow with the appropriate arrow, produce super and subscripts, etc etc giving a semi wysiwyg. Would be great for Kile, for instance.\n\nHaskell: \nIn literature type vars are usually greek letters. Syntax could be provided to obtain that effect, say alpha, beta, alpha_1, beta_1. Some other tokens could be shown as pretty symbols, such as ->, =>, ++...\n\nTheorem Proving:\nCoq, Isabelle, et all. All of them use plenty of mathematical notation. \n\nhttp://x-symbol.sourceforge.net/\nhttps://bugs.kde.org/show_bug.cgi?id=58340\n\n\n\n\n \n\n"
    author: "ad"
  - subject: "Re: x-symbol "
    date: 2008-03-25
    body: "Sorry, but why wysiwyg? LaTeX is totally the other way round. If you want it in spite of that, use LyX.\n\nAn interesting feature for Kile would be inline spell checking, though.\n\nCU\nDaniel"
    author: "danielHL"
  - subject: "Re: x-symbol "
    date: 2008-03-25
    body: "It's not wysiwyg. It renders a few tokens as symbols. For instance, while one could use tokens to create accentuated characters, such as \\'a for \u00e0, things become quite unreadable. Most people are not that hardcore and either input isolatin (or unicode) characters directly, or use x-symbol for the same effect while keeping the file in ASCII. X-symbol allows you to go one step further and render other tokens as symbols.\n\nIn any case, personally I don't even use it with LaTeX. But I find it great for use with other systems, such as theorem provers, since you can see proper math symbols in your formulas. And I would love to be able to use it with some programming languages. \n\nLook at the bottom of the screenshot:\nhttp://proofgeneral.inf.ed.ac.uk/images/pg-isa-screenshot.png\n\n\n\n\n"
    author: "ad"
  - subject: "Re: x-symbol "
    date: 2008-03-25
    body: "Inline spell checking is THE most interesting feature for kate I think."
    author: "cruzki"
  - subject: "Re: x-symbol "
    date: 2008-03-26
    body: "It's not wysiwyg, it's giving code written it TeX some possibilities that are just fine in XML. E.g.: if you want to type the pi constant, in TeX you have to type \\pi. In XML you can use &#960;, but if you are using UTF-8, you can also type the real character with the keyboard, or with kcharselect (I can't type it here, because the software that runs the site is very outdated).\n\nIn mathematical formulas this sucks a lot, because not only the code becomes much less readable, but also the lines become a lot longer, and it's very difficult to wrap each line of the code to 80 characters, and still making each line a step of some calculation."
    author: "suy"
  - subject: "Love and question"
    date: 2008-03-25
    body: "I really love kate!! One question though, is it possible to start kate without automatically opening the Documents tab? I'm unable to find this option in the menu/settings. Thanks!"
    author: "LB"
  - subject: "Thank You Kate Devs"
    date: 2008-03-25
    body: "I want all you Kate developers to know that I think that Kate is the best multipurpose text editor I have ever used. It pains me to have to use anything else. Please keep up the excellent work. My favorite features of Kate are its speed, ability to handle and keep organized many documents at once, syntax highlighting for every language I've ever tried to use it for, and excellent customizability. I truly appreciate the time you put into making this editor all that it is today, and I hope you can keep all these strengths as KDE moves to version 4.1 and beyond."
    author: "Andrew Matta"
  - subject: "Re: Thank You Kate Devs"
    date: 2008-03-25
    body: "Same here! Coming into programming out of the blues without any notions of previous editors, Kate is the tool that makes my editoring life easy and smooth. I use it for programming, for writing TODOS, for docbook! I already find Kate for KDE 4 better than the 3 series! \nThanks to the Kate team and I wish you all a very productive and friendly meeting!"
    author: "annma"
  - subject: "Well firstly"
    date: 2008-03-25
    body: "I'd say at least restore the functionality that was available in 3.5 as a number one priority before thinking of new features, there's quite a few things the old version could do that the new one cannot."
    author: "someone1245"
  - subject: "Re: Well firstly"
    date: 2008-03-25
    body: "Such as?\n\nI have found nothing missing in the svn version (aside from the irritating bug where the textedit part eats all shortcut -- even when the konsole part has the focus)."
    author: "hmmm"
  - subject: "A treeview and auto-completion will"
    date: 2008-03-25
    body: "I know that kate is not meant as a development tool but I personnaly I use it this way.\n\nThere is two features that I would love to see in kate:\n\n * a tree view like in textmate\n * a programming interface for auto-completion. The idea is to use something like ipython directly in kate."
    author: "Batiste"
  - subject: "Re: A treeview and auto-completion will"
    date: 2008-03-30
    body: "I strongly second this.. a tree view in Kate would be fantastic."
    author: "Vince W"
  - subject: "Re: A treeview and auto-completion will"
    date: 2008-04-02
    body: "You are completely right. I also long for an autocompletion feature for various programming languages. Would it be possible to import the Code-Completion from kdevelop as a plugin for kate?"
    author: "David"
  - subject: "scripting"
    date: 2008-03-25
    body: "Is it possible to write scripts (or maybe macros) for kate? I did find a post speaking about scripts in kjs, and in the script folder there is even a lua script, but I didn't find any documentation. It would be very handy for small tasks.\n(btw kate in kde4 is easily the best looking editor on the Earth :)"
    author: "plo"
  - subject: "Love"
    date: 2008-03-25
    body: "yep, thanks a lot for this great tool :)\n\nI just love the new search toolbar and I hope to see something like this in all KDE apps that need it (KWord, Konq... hmmmm)\n\nMaybe the search toolbar could be shown and hidden with the same shortcut (ctrl-f) instead of using Esc to close it ? I've done an extension to do this in firefox 2 years ago and it was quite popular :)"
    author: "DanaKil"
  - subject: "Re: Love"
    date: 2008-03-25
    body: "well, forget what I said for the shortcut... unlike in firefox, it's much usefull to give it the focus back when the search bar is already shown... -__-'\n\ncheers"
    author: "DanaKil"
  - subject: "Please"
    date: 2008-03-25
    body: "don't make kate as bloated/useless as kdevelop or quanta. I really love its simplicity and speed."
    author: "begging guy"
  - subject: "Black backgrounds?"
    date: 2008-03-25
    body: "Is there anyone out there who has a set of KATE syntax highlighting settings suitable for a BLACK background?  Old habits die hard."
    author: "joe l"
  - subject: "Re: Black backgrounds?"
    date: 2008-03-27
    body: "Somehow I magically managed to get a great black background color scheme on KDE4 kwrite.  The problem is I have no idea how I did it, or how to change it now.  Works great with syntax highlighting and all."
    author: "Leo S"
  - subject: "Notepad++"
    date: 2008-03-26
    body: "http://notepad-plus.sourceforge.net/uk/site.htm\n\nThat is my favorite text editor on any platform.  I'd love to see it get a KDE port."
    author: "T. J. Brumfield"
  - subject: "Just clone NEdit"
    date: 2008-03-26
    body: "..."
    author: "Sebastian"
  - subject: "A couple of Emacs features missing"
    date: 2008-03-26
    body: "I just tried kate. Code folding option is great! I was also very impressed that kate noticed that some other program had changed the file and offered to display a colored diff of the change.\n\nI still need some Emacs feature before I can make the switch: c-ident-command (tab to autoident code to correct level) and ediff-buffers. I know about kdiff3, but it's not integrated to the editor.\n"
    author: "Emacs User"
  - subject: "KATE is already best on earth!"
    date: 2008-03-28
    body: "Kate is best!\n\nI love it from the time I first saw it.\n\nThese past years helped me do my jobs just-in-time.\n\nWaiting to see it back in kdebase 4.1\n\n"
    author: "Todor"
  - subject: "autobookmark"
    date: 2008-04-02
    body: "I almost hate to bring it up since it seemed to cause some problem, but the old autobookmark plugin feature is something I'd love to see brought in.  While it wouldn't be quite as great, simply having a way to save bookmarks as part of a session might help, although autobookmark updated bookmarks whenever a file was saved."
    author: "Chris"
---
In April there will be a <a href="http://www.kate-editor.org/">Kate</a> Developer Sprint, similar to previous sprints for <a href="http://dot.kde.org/1174669960/">Decibel</a>, <a href="http://dot.kde.org/1203626978/">Akonadi</a>, <a href="http://dot.kde.org/1202500128/">KDevelop</a> and others. This is a great opportunity as developers interested in development of KDE's text editor will discuss what to do to make Kate the <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/text-editor-of-the-year-610219/">best text editor</a> on earth. This also means lots of polishing so that Kate in KDE 4.1 will shine even more!  The meeting will take place from 2008-04-11 till 2008-04-13. The location is hosted by <a href="http://www.basyskom.de">basysKom</a> in Darmstadt, Germany. Many thanks goes especially to the <a href="http://ev.kde.org">KDE e.V.</a> for their financial support.  If you are interested in Kate development, join our <a href="https://mail.kde.org/mailman/listinfo/kwrite-devel">mailing list</a>. More to come, stay tuned!



<!--break-->
