---
title: "KDE Italia will be at Open Mind 2008"
date:    2008-05-06
authors:
  - "gventuri"
slug:    kde-italia-will-be-open-mind-2008
comments:
  - subject: "Great! but what about eth0?"
    date: 2008-05-06
    body: "Great to see the KDE team going to events like these,\n\nWill you also attend the eth0 summer camp?\nMore info at http://www.eth-0.nl"
    author: "Jan"
---
<a href="http://www.kde-it.org">KDE Italia</a> is attending this year's <a href="http://openmind05.it">Open Mind</a> Free Software event from May 8 to May 10, 2008. This event is tailored for all people with an emphasis on young students. Giovanni Venturi and Daniele Costarella will give a presentation on KDE 4 as well as provide further information on KDE applications during their workshop.  Open Mind is located at <a href="http://maps.google.com/?q=Villa+Bruno%2C+San+Giorgio+a+Cremano%2C+Naples%2C+Italy">Villa Bruno - San Giorgio a Cremano - Napoli - Italy</a>. There will be a KDE Italia booth at the event where you can go for more information on the team as well as KDE. Please stop by and say hello to Giovanni Venturi and the rest of KDE Italia.


<!--break-->
