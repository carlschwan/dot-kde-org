---
title: "QtCentre - Providing Answers for Two Years"
date:    2008-01-05
authors:
  - "jthelin"
slug:    qtcentre-providing-answers-two-years
comments:
  - subject: "Greater year"
    date: 2008-01-05
    body: "Technically speaking, 2008 will be a greater year than either 2006 and 2007. By about 0.27% -- it's got one extra day :-)\n\nMany thanks for such a service to the community, Johan & the QtCentre team. Its value has been proven over and over, but I sure hope that 2008 bring even more high quality articles and entries."
    author: "Thiago Macieira"
---
Two years ago the <a href="http://www.qtcentre.org/">QtCentre</a> site was launched. The goal was to provide the leading forum on the internet for encouraging the exchange of Qt experience and to provide a meeting point to the community. The response to this has been beyond our wildest expectations - all thanks to all our visitors and contributors.



<!--break-->
<p>As a part of QtCentre's ambition to be the meeting point of the Qt community a Qt programming contest was held last year. The result was phenomenal with a great list of high quality entries and a matching list of prices. The participants came from all of the earth, reflecting the wealth of the community available at QtCentre.  The centre is one of the few sites actively used by both the open source community and the proprietary users of Qt.</p>

<p>A forum is good for exchanging experience and providing answers. For the more in-depth information QtCentre provides a <a href="http://wiki.qtcentre.org/index.php?title=Main_Page">wiki filled with articles</a>. These articles touch the peculiarities of Qt, provide helpful tips and tricks as well as providing useful general knowledge about Qt.</p>

<p>The QtCentre team would like to thank all visitors and contributors. Thanks to you, the centre has succeeded beyond our wildest expectations. We hope to see you all in 2008 and make that an even greater year!</p>

