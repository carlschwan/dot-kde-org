---
title: "openDesktop.org Launches Job Board"
date:    2008-11-13
authors:
  - "fkarlitschek"
slug:    opendesktoporg-launches-job-board
comments:
  - subject: "Open sources?"
    date: 2008-11-13
    body: "Where can I download the source code? "
    author: "Joe"
  - subject: "Re: Open sources?"
    date: 2008-11-13
    body: "troll, it is totally of topic so you just came here to troll"
    author: "notJoe"
  - subject: "Re: Open sources?"
    date: 2008-11-14
    body: "Yeah he's a bit of a troll, but this news is mostly spam, so trolling here is not out of the general scheme of the news item"
    author: "notJoeEither"
  - subject: "Re: Open sources?"
    date: 2008-11-15
    body: "speaking of which:\n\nhttp://www.jobberbase.com/"
    author: "debianuser"
  - subject: "wow!"
    date: 2008-11-14
    body: "that's quite amazing! no jobs yet in my country though :S ;)"
    author: "mxttie"
  - subject: "Re: wow!"
    date: 2008-11-14
    body: "I\u00b4m glad you like it"
    author: "Frank Karlitschek"
  - subject: "openDesktop.org Launches Job Board"
    date: 2008-11-14
    body: "Hello Frank, \n\ni understand the need for a open source job board. That's why i started one myself, as i am an technical recruiter placing candidates within open source companies. I am considered about the quality of the job board you launched, if anyone can post entries like these:\n- Praktikant Marketing/PR (w/m)\n- Praktikant System Administration / Windows Netzwerk (w/m)\n- Senior Sales Executive for the French Market-OSL0000000F\n\nWhat does this have to do with open source? Please do understand, i want to keep up the quality of an open source job board that is run on community developer sites. I am concerned about companies spamming the job board with unrelated job proposals. Or since when is Windows open source?\n\n\n"
    author: "Lukas Chaplin"
  - subject: "Re: openDesktop.org Launches Job Board"
    date: 2008-11-14
    body: "\"... for open source and IT jobs.\""
    author: "christoph"
  - subject: "Re: openDesktop.org Launches Job Board"
    date: 2008-11-14
    body: "Hi Lukas,\n\nI understand your concern. \n\nI\u00b4m a big fan of openness so i want to keep it as open and free as possible. I don\u00b4t think it would be a good idea if I would moderate the jobs. I can\u00b4t and don\u00b4t want to decide what is interesting for our community.\n\nIf we get too many spam submissions i will implement some kind of voting system. So the users can decide what is good and what isn\u00b4t.\n\nWhat do you think?\n\nCheers\nFrank\n\n\n"
    author: "Frank Karlitschek"
  - subject: "Re: openDesktop.org Launches Job Board"
    date: 2008-11-14
    body: "Open source companies have to have sales and PR too. Not so sure what they're doing with Windows networks, but otoh, if they're doing cross-platform engineering, they'll need to have things like that around."
    author: "reality is"
  - subject: "Grazie"
    date: 2009-04-11
    body: "Fue una gran explicaci\u00f3n, gracias felicitaciones y le deseo \u00e9xito.\r\n<a href=\"http://www.aylak.com\" title=\"Okey oyna\">Okey Oyna</a> | <a href=\"http://sinema.aylak.com\" title=\"Sinema\">Sinema</a> | <a href=\"http://havadurumu.aylak.com\" title=\"Hava durumu\">Hava durumu</a>\r\n\r\n"
    author: "Okeyci"
---
Last week we launched a <a href="http://www.opendesktop.org/jobs">free job board</a> on <a href="http://www.kde-look.org">KDE-Look.org</a>, <a href="http://www.kde-apps.org">KDE-Apps.org</a> and the other websites of the <a href="http://www.opendesktop.org">openDesktop.org</a> network. 

I know quite a few people who found a nice full time or freelance job by showing their work on our websites. I also know a few free software projects and companies who are looking for new projects, members or employees.

So I had the idea to build a job board where companies, projects, developers and artist can get in contact. Specialised for open source and IT jobs.



<!--break-->
<p>Unlike other job boards the openDesktop.org job board is completely free - both to those listing jobs and those looking for jobs. We will finance the hosting with advertising and sponsorships.</p>

<p>The job board is visible on all websites in the network, meaning a job listed on one, is automatically and immediately displayed on all the other sites in the network.   </p>

<p>The main feature of the job board is its simplicity - jobs are listed with a few clicks and jobseekers search the available jobs per country and category. All registered users can publish their CV with two click. </p>

<p>So if you are looking for a nice Linux job, an internship, a new logo for your application or a freelance job as an icon designer post your data. It is all free.</p>





