---
title: "KDE Commit-Digest for 6th July 2008"
date:    2008-08-05
authors:
  - "dallen"
slug:    kde-commit-digest-6th-july-2008
comments:
  - subject: "Danny the machine"
    date: 2008-08-05
    body: "Thanks a lot!\n\nOne little question if I may: If I were to write a an app with KDE LGPL libs could I release it closed source?"
    author: "Tom"
  - subject: "Re: Danny the machine"
    date: 2008-08-05
    body: "IANAL, but I believe the short answer is 'yes'. The longer answer would probably include a debate about probably still requiring a commercial Qt license."
    author: "Kit"
  - subject: "Re: Danny the machine"
    date: 2008-08-05
    body: "well, there's no debate really: you would require a Qt license. KDE asks nothing, but Qt does."
    author: "Aaron Seigo"
  - subject: "Re: Danny the machine"
    date: 2008-08-16
    body: "The only way there could even possibly be a debate would be if you did not use any single Q* class, only K* classes... good luck not using QString! And even then you'd almost certainly lose the debate, see: http://www.gnu.org/licenses/gpl-faq.html#GPLWrapper\nSo a commercial Qt license is definitely needed to write proprietary KDE apps."
    author: "Kevin Kofler"
  - subject: "Re: Danny the machine"
    date: 2008-08-05
    body: "One word: WOW. You are the machine!\n"
    author: "Michael \"Wow\" Howell"
  - subject: "Something about screen resizing"
    date: 2008-08-05
    body: "Hey! There is something im wondering a bit about. I often change screen resolution because i use a laptop and an external monitor, but as it is now, the panel does not resize itself to fit the larger screen (thankfully it does resize itself when going from large screen to small :-) ). \nSecondly, this also means that there is no real \"lower right corner\" of the screen, since applets placed there will eventually end up around the middle when using the larger screen, again, im not sure this is desirable.\nWith regards to the panel, a workaround could be to make the panels be sized in percent of available screen size, however, with regards to plasma applets a better solution would be somehow less easy to device - perhaps the option to make the applets automatically stick to borders (of the screen or each other), the ability to have a kind of borderlayout (known from java awt) or something else. \nIm writing this because im curious if plans for something like the above is in the works. Anyway, i want to emphasize that this is not something thats keeping me up in the night :-) . (like the plasma cashew thing in the upper right corner apparently does to certain people)."
    author: "Millhaven"
  - subject: "Re: Something about screen resizing"
    date: 2008-08-05
    body: "Well, check these out:\n\nhttp://bugs.kde.org/show_bug.cgi?id=161857\n\nAny of those your issue?  A couple seem like they would be:\n\n154045  Plasma panel does not resize after resolution change with KrandrTray\n163676  Plasma desktop/panel does not react properly when the screen size is        enlarged due to higher resolution\n"
    author: "Xanadu"
  - subject: "Re: Something about screen resizing"
    date: 2008-08-05
    body: "Yah, the panel behavior is described in the last bug, 163676. \nThis is great news, if its a bug there is a chance it will get solved in the 4.1.n series :-D (not that its a big deal anyway, i am a very happy plasma user)."
    author: "Millhaven"
  - subject: "Re: Something about screen resizing"
    date: 2008-08-06
    body: "Agreed.  Their brains / fingers are already working on it, it seems.  I'm no coder and thus not part of the KDE Team, but, I suspect you'd see a fix pretty soon. :-)\n\nM.\n"
    author: "Xanadu"
  - subject: "Thanks Again Danny!"
    date: 2008-08-05
    body: "Can't thank you enough for writing these articles regularly (sorta :P)"
    author: "spawn57"
  - subject: "Re: Thanks Again Danny!"
    date: 2008-08-05
    body: "Contrary to most people here, I'm very happy that you got late to publish the digests this year ... It makes more digests during the summer !\n\nThank you & congrats Danny !"
    author: "AAk"
  - subject: "Re: Thanks Again Danny!"
    date: 2008-08-05
    body: "Someone needs to get a life... and I'm not even kidding ;)"
    author: "ad"
  - subject: "Re: Thanks Again Danny!"
    date: 2008-08-05
    body: "same here. thanks Danny."
    author: "Holger F."
  - subject: "Serbia & Montenegro"
    date: 2008-08-05
    body: "I repost the same message as the latest digest as it apparently went unnoticed.\n\nOn the map, Montenegro has a grey shade indicating that it has 2-10% of commits while Serbia has a grey shade indicating it has 0% of commits. Yet, in the list, \"Serbia and Montenegro\" have 4.28%. So, are all KDE contributors from Montenegro or is there a bug?\n\nBy the way, i find a bit strange that the numbers are exactly the same as last week.\n\nThanks Danny!"
    author: "Med"
  - subject: "Re: Serbia & Montenegro"
    date: 2008-08-05
    body: "Though I don't have time for a solution right now, I have looked into the problem. The reason is that the map is outdated, with entities such as .yu being a single country, and with contributors in my database as being from .cs (Serbia and Montenegro) - not as being from sub areas within the former territory.\n\nThis will take a while to fix, but I will look into it \"soon\" (along with fixing some other statistics issues).\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Thank you!"
    date: 2008-08-05
    body: "You da man!"
    author: "Heja AIK"
  - subject: "Systray icon background"
    date: 2008-08-05
    body: "Hi all, and in particular, hi Jason,\n\nI would like to talk about systray icon transparency -- or more precisely, of the mildly unsightly solid background of systray icons in KDE 4. :)\n\nI'm sure it's a vastly more complicated issue than I can possibly imagine; presumably something about the systray spec being somewhat imperfect (which I hear it is?) and ARGB visuals not being well supported in some parts of the X11 world and other complicated things. But might I be so bold as to inquire about the actual technical details? In particular, unless I'm mistaken, this issue was taken care of, or at least hacked around smoothly enough in KDE 3, so I would be led to assume that the old hack no longer works with the KDE 4 way of doing things.\n\nMind you, I have not a single doubt this will be seen to eventually by our fine KDE developpers. :) I am just curious, for now, about the technical details of whatever it is they are going to have to hack around for the enjoyment of our eyes.\n\nThanks,\n\n-- S."
    author: "S."
  - subject: "Re: Systray icon background"
    date: 2008-08-05
    body: "See http://dot.kde.org/1217341401/1217350835/1217380916/"
    author: "christoph"
  - subject: "Re: Systray icon background"
    date: 2008-08-05
    body: "I see! This does indeed answer my question. Darn, if this is indeed a GTK issue it's going to take ages to see this resolved. :/ Ah well."
    author: "S."
  - subject: "Re: Systray icon background"
    date: 2008-08-05
    body: "I was thinking about the issue... would it be possible to have two system trays, one for \"legacy apps\" (KDE 3, GTK+, X11/fdo systray), and one for Qt 4/KDE 4 apps, until the new systray spec is released?"
    author: "christoph"
  - subject: "Re: Systray icon background"
    date: 2008-08-05
    body: "the idea is to have one mixed-mode widget that supports both as a transitional device."
    author: "Aaron Seigo"
  - subject: "Re: Systray icon background"
    date: 2008-08-06
    body: "Aaron. I thought we'd decided to draw the line at awesome and leave pure sterling magic for later. ;)\n\nSeriously, though -- /thanks/. The Plasma team deserves one metric ton of awards for the quality its work."
    author: "S."
  - subject: "Re: Systray icon background"
    date: 2008-08-11
    body: "As Aaron said, the idea is to be able to handle both at the same time. The architecture of my rewrite already supports this, but there's still no spec to plug in. ;)\n\nOn whether a new spec will make 4.2 or not, I don't see it as impossible - at least as far as KDE apps go. For KDE apps, all that needs to be updated is KSystemTrayIcon to try the new spec before falling back to the FDO spec. But then, it'd really be best to have it done for the alpha, which only leaves two months..."
    author: "Jason Stubbs"
  - subject: "Re: Systray icon background"
    date: 2008-08-05
    body: "I want also to thanks Jason Stubbs. His work in systray seems great, and it one of the few pieces of KDE4 that still annoys me."
    author: "Iuri Fiedoruk"
  - subject: "Re: Systray icon background"
    date: 2008-08-11
    body: "Just for the record, Sebastian Sauer is another developer that has put a huge amount of effort into the system tray. So thanks to him also as its one of the hardest parts of plasma to work with."
    author: "Jason Stubbs"
  - subject: "I want whatever Danny is taking...."
    date: 2008-08-05
    body: "as he's getting those digests out exetremely fast.......\n\nCome on, Danny  - cough up - what are you taking?????  :o)\n\nps: great work."
    author: "Ian"
  - subject: "Re: I want whatever Danny is taking...."
    date: 2008-08-05
    body: "Yeah, have you been tested recently?\n\nhttp://wadejolson.files.wordpress.com/2008/07/193923_7430_small.png?w=350&h=286\n\nyou are working at a suspiciously fast rhythm :)"
    author: "Raul"
  - subject: "nice work"
    date: 2008-08-05
    body: "1.- nice work men.\n\n2.- i really like this:\"Experimental work on video capture in Phonon, with a \"snapshot\" function added to the video widget.\", phonom can really make easy multimedia on linux.\n\n3.- kpresenter looks really cool, koffice is turning into a killapp for office.\n\n4.- nice work men, really enjoy commit-digest."
    author: "rudolph"
  - subject: "Re: nice work"
    date: 2008-08-05
    body: "There are several women on the team too "
    author: "JohnFlux"
  - subject: "Re: nice work"
    date: 2008-08-06
    body: "oks ;)\n\nmen == danny"
    author: "rudolph"
  - subject: "Re: nice work"
    date: 2008-08-06
    body: "As far as I know he has no multiple personality disorder, so it would be fine to call him just a 'man' ;-)"
    author: "jospoortvliet"
  - subject: "kweather and plasma applet"
    date: 2008-08-06
    body: "Just curious, I noticed the mention of kweather porting to plasma as well as the lcd display plasma weather applet. Are they the same, or will we get two?"
    author: "xian"
  - subject: "Re: kweather and plasma applet"
    date: 2008-08-06
    body: "That LCD applet (weatherstation) uses the weather dataengine to gather the weather information where-as pino's KWeather port is just a port of the old KWeather to use Plasma. KWeather only provides weather information by using METAR data, the weather dataengine can provide information from different sources depending on which plugins are available.\n\nAs we speak, the weather dataengine does not support METAR, but I'm planning to create METAR ion (as those sub-dataengines are called) to provide that data to the weather applets. Currently there are two applets which use the weather dataengine, weatherstation made by Petri Damsten and weather made by Shawn Starr, and several ions to provide data for example from BBC UK Met, Environment Canada and NOAA.\n\nIf someone has more sources for weather information and/or someone is willing to code support for some source to the weather engine, see http://techbase.kde.org/Projects/Plasma/Weather for the documentation. Possible sources can also be added in that same page for developers to see what could be provided.\n\nTo be short, they are not the same and I don't know whether someone is going to work on KWeather to get it working again. I've placed my bets to weather engine and its applets :-)"
    author: "teprrr"
  - subject: "Plasma cgash with Openoffice"
    date: 2008-08-08
    body: "I dot know if is the KDE 4.1 plasma or nVidia's driver, but plasma and nVidia's GeForce 7600 GS video-card working not correct together! \n\nPlasma-analogiclock, panel down and icons crash with Openoffice. \n"
    author: "Jake"
---
In <a href="http://commit-digest.org/issues/2008-07-06/">this week's KDE Commit-Digest</a>: Support for moving of applets in <a href="http://plasma.kde.org/">Plasma</a> panels. Various work, such as autocompletion and bookmarks (shared with <a href="http://www.konqueror.org/">Konqueror</a>) support in the basic Web Browser Plasmoid. Progress in the "Plasma on new form factors" project. A new "LCD Weather Station" Plasma applet makes an appearance. The Powersave and KWeather utilities are ported to Plasma. More work on the "Cube" KWin-Composite effect, including a configuration dialog and keyboard navigation. Work on the multiple choice mode and internet-based translation in <a href="http://edu.kde.org/parley/">Parley</a>. The new "Message List View" becomes more usable, with work on skinning in <a href="http://kontact.kde.org/kmail/">KMail</a>. Initial "biased playlist" support, work on the new "mass tagging" feature, and extensions to the Last.fm service in <a href="http://amarok.kde.org/">Amarok</a> 2. Experimental work on video capture in <a href="http://phonon.kde.org/">Phonon</a>, with a "snapshot" function added to the video widget. KDevPlatform (the basis of <a href="http://www.kdevelop.org/">KDevelop</a>4) gets early integration with the <a href="http://kross.dipe.org/">Kross</a> scripting engine. Further expansion of the D-Bus interface, and more work on the Plasma applet for <a href="http://ktorrent.org/">KTorrent</a>. More work on guide line manipulation, and a new "Paragraph Tool" for better interaction with larger blocks of text in <a href="http://koffice.org/">KOffice</a>. More work on the "Presenter View" in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. The start of a "Table of Contents" implementation in <a href="http://koffice.kde.org/kword/">KWord</a>, with the first steps towards a multi-page table shape in <a href="http://www.koffice.org/kspread/">KSpread</a>. Initial support for image display, and full support for UTF-8 text in the <a href="http://www.kexi-project.org/">Kexi</a> web forms component. A <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>-based file watch service is moved into kdereview. <a href="http://commit-digest.org/issues/2008-07-06/">Read the rest of the Digest here</a>.

<!--break-->
