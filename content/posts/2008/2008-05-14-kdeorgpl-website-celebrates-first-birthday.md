---
title: "KDE.org.pl Website Celebrates First Birthday"
date:    2008-05-14
authors:
  - "kteam"
slug:    kdeorgpl-website-celebrates-first-birthday
comments:
  - subject: "beautiful "
    date: 2008-05-14
    body: "the site looks really polish ;-)"
    author: "Patcito"
  - subject: "Re: beautiful "
    date: 2008-05-14
    body: "Yes, they didn't repeat some of kde.org's design errors. The side menu on kde.org was a nice design idea, but it always gives me some eye cancer... There is some vertical line/border/contrast/whatever between the menu items and the centered text missing."
    author: "Sebastian"
  - subject: "Re: beautiful "
    date: 2008-05-14
    body: "Yes, I think switching to wikimedia would do a lot of good to kde.org (by the way thanks for ignoring my lame polish joke :p )"
    author: "Patcito"
  - subject: "Re: beautiful "
    date: 2008-05-14
    body: "It was quite good joke ;)\n\n--KDE user from Poland\n\n\nPS. MediaWiki sucks, Miguel de Icaza is right here http://tirania.org/blog/archive/2008/Jan-24-1.html"
    author: "patpi"
  - subject: "Re: beautiful "
    date: 2008-05-14
    body: "Take wikidot, its cool and it just works."
    author: "andy"
  - subject: "Re: beautiful "
    date: 2008-05-14
    body: "MediaWiki allows to switch off the clickable images (what I do in my every we bsite based on it). \"I cannot customize it, I didn't RTFM\" is not a valid point, sorry.\n\nRe Wikidot (based on the work of a guy from Poland too, btw), it's a bloat for many of us: it's so heavily depending on javascript, what is not really necessary for the purpose of wikis.\n"
    author: "js"
  - subject: "Congratulations to the KDE.org.pl Team!"
    date: 2008-05-15
    body: "The site looks really good and it is nice to notice that it is not just a copy of www.kde.org.\n\nI guess a \"thank you\" directed towards OpenOffice Software is in order too."
    author: "EP"
---
It has been a year since launching our <a href="http://kde.org.pl">KDE.org.pl</a> site, which has an aspiration to be a real "gate to the world of KDE" in Poland.  During the last months, our site received 220 pages and articles, most of them are translations of news, articles and interviews from dot.kde.org and kde.org. We have got 480 photos, artworks and screenshots. In order to reach more people intrested in KDE, our goal is to simplify the language and keep the quality.



<!--break-->
<p>We have accompanied Polish KDE users by providing translations of The Road to KDE 4, and now we're keeping on with news on KDE 4. The central items on the agenda are topics like Amarok, KOffice, Education, Akademy or interviews with KDE developers.
</p>

<h2>History and Contributors</h2>

<p>The work, based on the flexible Mediawiki technology, were started a long time before the launch - in early 2007. In February, we have started translations and categorising of the knowledge, sometimes exceeding the contents of English pages.
</p>

<p>Over the past months, more people have been joining the Polish KDE Team. Words of appreciation for hard and thorough job especially goes to those who have belonged to the team from the beginning: Pawe&#322; Szubert (pbm, unquestioned record in number of translations), Bartosz Koz&#322;owski (joker) and &#321;ukasz Strz&#281;pek. Content and language-related corrections are maintained by Jaros&#322;aw Staniek, who has also customised the KDE Oxygen Mediawiki style for the web site.
</p>

<p>As a part of the evolution of the web site, <a href="http://forum.kde.org.pl">forum.kde.org.pl</a> launched in July 2007 in cooperation with JakiLinux.org (<a href="http://polishlinux.org">polishlinux.org</a>) portal, and <a href="http://blog.kde.org.pl">blog.kde.org.pl</a> in November 2007. The latter has been designed in similar style to <a href="http://kdedevelopers.org/">kdedevelopers.org</a> blog, thanks to Bartosz Koz&#322;owski (joker). Hosting for the kde.org.pl web site and our blog is provided by <a href="http://openoffice.com.pl/en">OpenOffice Software, LLC</a>.</p>


