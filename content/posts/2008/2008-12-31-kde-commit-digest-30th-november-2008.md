---
title: "KDE Commit-Digest for 30th November 2008"
date:    2008-12-31
authors:
  - "dallen"
slug:    kde-commit-digest-30th-november-2008
comments:
  - subject: "time to admit"
    date: 2008-12-30
    body: "Danny, those 5 liners commit digest are just not very useful and most information can be obtained by reading the planet anyway. If you don't have time to write commit digest anymore, that's fine and we appreciate your past work, but this is just ridiculous and doesn't contain any article, screenshot, tiny interview or casts as it used to. Just admit you don't have time anymore and let other people work on the digest please. Getting no digest for 2 months and then 10 tiny digest with no detailed article whatsoever is a waste of time for you to write and us to read, it's also the proof you obviously don't have time to do it right. Sorry if I sound harsh but I'm sure I'm not the only one who think that way."
    author: "kubunter"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "Please read the note in issue 133. And please be aware that if Danny wouldn\u0092t do this, it probably wouldn\u0092t be done at all, so lets just be thankful for what we get and look forward to future \u0091full\u0092 digests. It\u0092s not like you would tell someone to stop bloging if he doesn\u0092t blog as much for a period of time. BTW: I read the planet and yet I learn new things in every digest, even in these \u0091small\u0092 ones.\n\nSo: Go Danny, a lot of people really appreciate what you are doing! (And late digests are still much better than no digests at all.)"
    author: "fliegenderfrosch"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "If you read the first Digest I posted out of this recent group of Digests, you would know that this is a temporary arrangement to catch up to the present.\n\nI would post extra content if I had it, but people haven't been putting the effort in to reply to me on time or, better yet, proactively contact me.\n\nIf you don't want to read them, don't.\nYou sound like angry customer, only I don't remember you paying me.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "I said I appreciate your past work, but maybe if you officially give up on the digest commit, the community could organize itself to improve the current situation. Maybe by setting a techbase page where any dev or user could post news there and every sunday or so it would be posted to the dot. The current situation really sucks. It's better to do things in a decentralize way sometimes, especially when the centralized way so obviously doesn't work with all due respect.\n\n> you would know that this is a temporary arrangement to catch up to the present.\n\nThat already happened before. Once again, I know you're not paid for this so there is no blaming you for that. I'm blaming you for not officially admitting and letting the community organize something better on techbase or anything."
    author: "kubunter"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "Please disabuse yourself of this fiction.\n\nThe work to organize even a small group to get something together each week EXCEEDS the work it takes to produce the digest.\n\nNo community designed the digest. It wasn't a group decision. It was one person who saw a need and did it. Two people actually. In sequence.\n\nSo if you think that a techbase project would work better, then get to it. I expect to see one ready by next friday.\n\nOtherwise stuff a sock in it.\n\nDerek"
    author: "dkite"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "> Please disabuse yourself of this fiction.\n\nThanks, I LOLed.\n\n> The work to organize even a small group to get something together each week EXCEEDS the work it takes to produce the digest.\n\nThat's what I thought too, but looking at how Danny can't handle a weekly or even a monthly digest, I tend to think a community thing would be better.\n\n> So if you think that a techbase project would work better, then get to it. I expect to see one ready by next friday.\n\nThe problem is that Danny is not clearly giving up, so people still think he can handle it (even if experience shows that he clearly can't, at least on a weekly basis) and the consequence is that they won't engage in a community effort because they think Danny will do it.\n\n> Otherwise stuff a sock in it.\n\nYou sir speek in mysterious ways."
    author: "kubunter"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "As I said, if you have a very good idea on how to keep people updated on what happens in KDE, go ahead. If you think Danny is monopolizing information, then start reading the commit logs and lists. He covers a very small portion of what is going on.\n\nIt is not my or your decision what Danny does, it is his. I, the editors of the Dot and many others appreciate his work, value it in fact.\n\nIf you decide to produce something, go ahead. I'm sure you will find grateful readers.\n\nJust a warning. You will find it to be an extraordinarily demanding job. What Danny does is remarkable, and few can sustain the effort like he has.\n\nAnd before you shrug me off, just for your information. I produced the Digest for about 3 years.\n\nDerek"
    author: "dkite"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "I know it's a huge amount of work, that's why it would better to share it and do it as a community.\n\n> And before you shrug me off, just for your information. I produced the Digest for about 3 years.\n\nI know, and you did a great job. You were also humble enough to stop doing the digest when you realized you didn't have time anymore and I applaud you for that. Let's hope Danny does the same one day."
    author: "kubunter"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "I will step down when I feel the time is right, and when a system is in place to continue the publication. Naturally, it will probably be me who has to create that system - and that's ok - but i'm busy, and I don't have time at the moment.\n\nI decided that these shorter editions to catch up are the best option out of the couple of less-than-ideal options. It would have been much easier for me to just throw in the towel without spending my last couple of days on these Digests, but I wouldn't do that.\n\nOf course, nothing lasts forever: I am aware that I won't be doing the Digest forever. But when it is time for me to step down (which will be both sad and at the same time, a relief!), I will try my best to leave it in capable hands.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: time to admit"
    date: 2008-12-31
    body: "'humble enough to stop'\n\nWhat an utter fool.\n\nGo write something yourself. I beseech you. Tell me where it is and I will then complain and rag and demean and question your ability and proficiency and honor.\n\nOtherwise shut your mouth.\n\nDerek"
    author: "dkite"
  - subject: "Re: time to admit"
    date: 2008-12-31
    body: "kubunter ... kubunter ... \nPlease... Stop. Be nice to the people who do productive work for kde and your pleasure. \nBut please, stop your mission of destruction and chaos. "
    author: "anonymous"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "Just an off-topic question, would it also be possible to integrate an autogenerated Krazy/EBN weekly report in the Digest to demonstrate the code quality progress?"
    author: "Andre"
  - subject: "Re: time to admit"
    date: 2008-12-31
    body: "Oh, that could be very useful!"
    author: "boudewijn rempt"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "I'll bite.\n\nDanny has already asked for more contributions to those sections of the commit-digest, so if nobody steps up, there's nothing to include.\n\nAlso\n> Just admit you don't have time anymore and let other people work on the digest please.\nI'm betting Danny would thank anyone that wants to help him, and could use the help, I don't think that there are people out there that want to work on the digest but don't because Danny does."
    author: "IAnjo"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "Come on, the appropriate procedure is to praise Danny. \n\nAnd guess what I do praise him. No one prevents you from blogging or the submission of news."
    author: "Andre"
  - subject: "Re: time to admit"
    date: 2008-12-30
    body: "Don't mess with my Danny, you monster!"
    author: "Lisa Engels"
  - subject: "Re: time to admit"
    date: 2009-01-02
    body: "Hmmmm, I can see your point - you clearly prefer the articles over the sorted and cleaned commit messages. Personally, I didn't care much for the articles with screenshots, and I'm a huge fan of the small 5-line extractions (if I have little time) and the full list of selected commits... So I would be completely fine if the commit digest would continue like this forever!"
    author: "jospoortvliet"
  - subject: "Re: time to admit"
    date: 2009-01-02
    body: "i disagree. i read these digests, and i think it is a good idea to catch up with simpler ones. danny, thanks for catching up ;)"
    author: "richlv"
  - subject: "respectisteverything"
    date: 2008-12-30
    body: "R E S P E C T"
    author: "respectdanny"
  - subject: "Re: respectisteverything"
    date: 2008-12-30
    body: "I respect Danny, I don't think respect and criticism are mutually exclusive. Actually, never criticizing something or someone would be a disservice in my opinion."
    author: "kubunter"
  - subject: "Re: respectisteverything"
    date: 2008-12-31
    body: "agree.\n\ntoo often on the dot, people expect you to just be positive and kiss up.  but to do that leads no where.  Only through discussion, only through criticizing can true growth occur where there are problems.  And there have been problems with Danny's digest for at least a year now.  He very rarely is current, something does need to be done about it.  Falling a month, two months, three months behind is not acceptable.  It's a sign that something needs to be done to keep it more current.  Whether people like it or not these digests are good publicity for KDE, when they are constantly late, it ends up being very bad publicity for KDE.\n\nDanny has constantly fallen behind, very rarely is up to date, and something needs to be done about that."
    author: "Anon"
  - subject: "Re: respectisteverything"
    date: 2009-01-02
    body: "send him a private mail offering help."
    author: "jospoortvliet"
  - subject: "A Go app"
    date: 2008-12-30
    body: "A kde application for playing go, that sounds very interesting :)"
    author: "Capit. Igloo"
  - subject: "Re: A Go app"
    date: 2009-01-05
    body: "Indeed. Looking forward to it.\n\nPS. Watch Hikaru no Go if you are interested."
    author: "Ioannis Gyftos"
  - subject: "Very useful !"
    date: 2008-12-30
    body: "*lol* you never can satisfy all. Two days ago, the screamed for short-versions, no it seems they get short versions, but are they happy ?! ;)\n\nCriticism is sometime something useful.\nBut this certain case I have to say: Nobody prevents anybody to do something like a digest, on techbase or in what flavour ever.\nAlso nobody prevents anybody from helping Danny.\nso please remember what's \"open source\" is all about, you want something to change? Do so, you have the possibillity.\n\nBut what is really useful, is the digest, short or not. I don't even have the time to read the full digest, so I can't tell if something is missing (and even when then I guess there are good reasons for). So I really apreciate the short info about the KDE status, it keeps me an KDE-enthusiast :)\nSo I guess, at least for me THE DIGEST IS VERY USEFUL.\n\nSo thank you so much Danny, in this way I was able to follow KDE-Development for a long time :)"
    author: "A happy Reader"
  - subject: "Re: Very useful !"
    date: 2008-12-30
    body: "Yes, I agree. Mostly I don't read the whole digest, but I love to read the short summary here on the dot. Thanks, Danny!"
    author: "Thomas R."
  - subject: "One question about the skype plugin"
    date: 2008-12-30
    body: "First of all thanks danny :)\nDoes the skype plugin for kopete require to have skype open and logged (as it worked for the past plugin and iirc with pidgin) so it can pass the messages by dbus?"
    author: "jackie"
  - subject: "Re: One question about the skype plugin"
    date: 2008-12-31
    body: "Skype plugin for Kopete use original Skype. It only communicate with Skype by dbus. You must have installed original dynamic linked Skype. Kopete can start it, but not logged you in yet. You can turn off message/call event, because Kopete show it too."
    author: "Pali"
  - subject: "Sift Through"
    date: 2008-12-31
    body: "Frankly, it takes time to sift through all the commit messages, compile stats, and put together a digest at all.  Danny doesn't list every commit and every commit message individually.  Even without a lengthy summary up top, there is plenty of information towards the bottom of the digest that takes time to put together.\n\nFrankly, the only reason we are having \"short\" digests is that people complained they NEEDED to be current, but didn't need so enough to contribute directly or indirectly.\n\nDanny responded to criticism and attempted to appease the masses, so in turn people want to criticize him again?\n\nI largely stopped posting on the Dot because people routinely jumped on me and called me a troll if I said I didn't like something.  I've long maintained that differing opinions should be welcome.  Criticism is often necessary.  But in this instance, I think it is unfair to criticize Danny for not doing enough when he obviously just worked his butt off to start catching up.\n\nI highly value the digests.  I don't like KDE 4 in its current state, even with openSUSE's packages which backport KDE 4.2 features.  I worry about the future of KDE 4.  I wonder what desktop I'm going to use in the future, and honestly I may just stay on KDE 3 as long as I can.  These digests give me a glimpse of what is to come with each new release.  And the more and more I read them, the more I'm trying to be optimistic about where KDE is headed.\n\nAnd despite all the criticism and arguments that stem from those who love or abhor the Plasma desktop implementation, I do heavily pepper KDE 4 apps into my KDE 3 desktop and look forward to updates on them."
    author: "T. J. Brumfield"
---
In <a href="http://commit-digest.org/issues/2008-11-30/">this week's KDE Commit-Digest</a>: Various usability improvements and polish in <a href="http://plasma.kde.org/">Plasma</a>, with further work on JOLIE integration. Italic font support in the Plasma "Fuzzy Clock" applet. Full support for use-building on signal/slot connections in <a href="http://www.kdevelop.org/">KDevelop</a>, and the start of a framework for code quality checkers using the KDevelop C++ parser. Addition of VIM-like navigation (HJKL) in <a href="http://okular.org/">Okular</a>. Refactoring in the Kigo game (formerly KGo) with a view to future expansion, including work on undo/redo support. Updated artwork in <a href="http://opensource.bureau-cornavin.com/ktuberling/">KTuberling</a>. Support for hiding toolbars in KTimeTracker. More progress in the Skype text chat protocol in <a href="http://kopete.kde.org/">Kopete</a>. Continuation of the improvements to <a href="http://en.wikipedia.org/wiki/Ark_(computing)">Ark</a> seen in recent weeks.<a href="http://commit-digest.org/issues/2008-11-30/">Read the rest of the Digest here</a>.

<!--break-->
