---
title: "KDE Commit-Digest for 10th February 2008"
date:    2008-02-17
authors:
  - "dallen"
slug:    kde-commit-digest-10th-february-2008
comments:
  - subject: "Thanks for another digest!"
    date: 2008-02-16
    body: "Danny, you rock!"
    author: "Digest Reader"
  - subject: "Re: Thanks for another digest!"
    date: 2008-02-16
    body: "Thanks (that was a quick comment!).\n\nOf course, I planned to release the Digest earlier, but various things prevented that :)\n\nI should probably start on the next one now :D\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Thanks for another digest!"
    date: 2008-02-17
    body: "Thanks for this one.  The digest is a good read every week."
    author: "Leo S"
  - subject: "Re: Thanks for another digest!"
    date: 2008-02-18
    body: "Hi Danny,\n\nJust by curiosity: How long does it take you to write a digest?\n\nGrettings,\nHendric"
    author: "Hendric"
  - subject: "New KMail maintainer"
    date: 2008-02-16
    body: "It would be interesting to know the story behind that."
    author: "Inge Wallin"
  - subject: "Re: New KMail maintainer"
    date: 2008-02-16
    body: "There is no big story behind this.\n\nIngo Kl\u00f6cker, the old maintainer, doesn't have the time anymore to work much on KMail, and therefore asked me if I wanted to be the new maintainer, since I've been the person doing most of the work on the KDE4 version that time.\n\nSo there's nothing like the old maintainer conflicts back in 2002."
    author: "Thomas McGuire"
  - subject: "Re: New KMail maintainer"
    date: 2008-02-18
    body: "wonderful. thanks for stepping up.\ni've tried migrating from thunderbird to kmail several times now, but every time there were some small and not-so-small nuisances that prevented me. i guess i'll try again once i start using kde4 :)"
    author: "richlv"
  - subject: "Re: New KMail maintainer"
    date: 2008-02-18
    body: "I was forced to migrate from Evolution to Kmail. I say forced because Evolution was one of the few Gnome apps that I really loved but there was a problem with storing my password in openSuse 10.1 I think it was so I switched to Kmail. I had the hell of a time getting Kmail to set up properly which was the contrast to Evolution and it had some issues with fetching and sending mails sometimes but the worst thing about it is that it's one of the slowest e-mail clients that I ever used. I still use Kmail with the hope that it will improve a lot in KDE 4. KDE PIM would be perfect if Kmail would work as good as Evolution or Thunderbird."
    author: "Bobby"
  - subject: "Re: New KMail maintainer"
    date: 2008-02-20
    body: "I agree, to an extent.\n\nI definitely wouldn't say I \"loved\" Evolution due to its many bugs but the setup of new accounts was very painless.  \n\nI consider myself an intermediate level Linux user but I could not configure Kmail (on Fedora 8) to access my POP accounts without referring to the help.  Even this was out of date as some of the screens had been renamed.  Dejargonizing and making the configuration of POP accounts much more user friendly will certainly help Kmail adoption.\n\nI am not advocating a \"dumbing down\" of the program (which I know is anathema to many KDE devs) but simply removing UNNECESSARY complexity.  Both Thunderbird and Evolution have this one aspect right, so surely Kmail can be improved to surpass them."
    author: "Kerr Avon"
  - subject: "Re: New KMail maintainer"
    date: 2008-02-18
    body: "Hi,\n\nCan you tell if there is any plan to add the \"reply to html mail\" feature.\nI (and others) have been waiting for years for this IMHO critical feature.\nIndeed, when kmail receives an htnl formatted mail it is impossible to reply without destroying the whole formatting (a first step could be to enhance the html to text processing).\nIn a business environment, kmail is not a common mail client (guess what is the de facto standart...). Nearly every mail is html formatted (with colored text, bold text, itemize,...). \nUsing color, bold and itemize *is* useful especially when the mail is address to a group of participants that choose a different color for replying, or use bold to enhance part of a large mail.   \nIt is rather embarrassing to reply to these mails with kmail. You end up screwing the whole thread and you get a reputation: \"that stupid linux geek is not even able to answer mail correctly, why on earth does he insist on using this\". \nWell because, I'm used to *nix like OS, but yes I agree this problem with kmail is unfortunate. I'm pretty sure it will be addressed, at least I've seen some guy saying this a couple of years ago...\n"
    author: "jms"
  - subject: "Re: New KMail maintainer"
    date: 2008-02-18
    body: ">Can you tell if there is any plan to add the \"reply to html mail\" feature.\nWell, I don't plan adding something like this for 4.1 myself.\nNot because I am against it (I know that proper HTML support is important), but simply because there are lots of other things to do and time&developers are short. For example fixing the tons of regressions the Qt4 port introduced and working on refactoring to make the Akonadi port easier.\n\nHowever, maybe someone else will code it, for example Edwin, who already introduced HTML signatures recently."
    author: "Thomas McGuire"
  - subject: "Mono"
    date: 2008-02-17
    body: "Not particularly thrilled about Mono bindings... but that's okay as long as you won't start rewriting core desktop modules in C# (gnome anybody?)"
    author: "karma"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "> but that's okay as long as you won't start rewriting core desktop modules in C#\n\nUnless I am much mistaken, gnome developers are not doing that and have not announced plans to do so.  The only change that I am aware of is the decision to permit applications, which happen to be written in Mono (Tomboy, F-Spot) to be included in the default gnome desktop.  Personally I like both applications and having read through some of the code for them I understand the decision to use the C# language.  It really is considerably less painful than the combination of plain C and gtk.  Contrary to popular misconceptions, Tomboy at least does not appear to use all that much of the .NET framework outside of the runtime and core IO/container classes.  The GUI for example is created through C# bindings for GTK.\n\nAs far as KDE is concerned, the availability of free software implementations of high level languages gives developers an opportunity to write and publish innovative applications more quickly and with fewer technical pitfalls than they could with C++.  Java / Qt Jambi might be the better choice at present since it is supported by Trolltech, although I'm sure Richard has a better informed answer to that.  One key thing to note is that there is a completely open and free implementation of C# and enough of .NET.  I'm not sure the same can be said of Java at the present moment in time."
    author: "Robert Knight"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "http://icedtea.classpath.org"
    author: "Kevin Kofler"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "I personally don't know why people don't stop blaming Mono/C#/.NET. Is it because of the original inventor?\n\nGET REAL PEOPLE: C# is ISO certified and patent free, the CIL is ISO certified and patent free, and so Qt, KDE and those bindings are!\n\nThere are technical and practical issues where a combination of C# and its framework or Java are superior to C++, more than ever when you forced to not use Qt. Just to mention one: Did one of you blamers ever use (N)Hibernate? I don't know another so flexible object-persistent database framework.\n\n\nRegards\nErik"
    author: "Erik"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "Well, there is Hibernate :)  (and SQLAlchemy...)\n\njust joking.\n\nI like C# more than Java, but its main problem as I see it is that it is currently even less cross platform than Java. After that, well, that's matter of personal taste. Only that."
    author: "edomaur"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "We're not forced to not use Qt, which is why C# doesn't have as much to offer us as it does Glib/C programmers."
    author: "Ian Monroe"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "That's exactly it.  I really don't see a lot of point in using C# or Java over C++ when you're using Qt.  Sure C#/Java benefit from some nicer syntax and better code completion/refactoring support in IDEs, but aside from that I don't see much difference.\n\nThe Qt API is better than the class libraries of those languages, and I barely have to do any memory management at all with Qt, so the garbage collector really doesn't improve things for me.  Cross platform is pretty much a wash as well.  C++ requires recompilation, while Java/C# need a runtime installed.  Security is contentious, but I haven't seen any convincing evidence yet on that front.\n\nI can see that if you're using C/GTK you'd jump at the chance, because C# or Java would be far easier to use, but for C++/Qt I really don't see the argument.  The next step easier is a scripting language."
    author: "Leo S"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "Yes, I pretty much agree with you here, and you are quite correct when you say \"next step easier is a scripting language\", such as Python or Ruby. What is the value added by using QtJambi or Qyoto instead of the C++ api?\n\nHowever, from my point of view, getting C# bindings working with Qt/KDE is a very interesting technical challenge. Currently Arno Rehn is doing great stuff with that. He is making the Smoke library that is used by the Ruby/C# and PHP bindings more modular. That will make it much easier to create Smoke libraries to cover various KDE plugin apis such as Plasma or Akonadi.\n\nThe Qyoto/Qt C# api is quite a bit different from the QtJambi one, although C# and Java are quite closely related. Slot/signals are done quite differently in Qyoto compared with QtJambi, and in Qyoto Qt properties are mapped directly onto C# ones. So if you are a Free Software hacker, the differences are larger than they first appear.\n\nThe is only a GPL version of Qyoto available, whereas QtJambi is dual licensed - the target market is very different. \n\nQyoto/Kimono should be able to cover important KDE apis be KDE 4.1, but there is no work being done on a KDE version of QtJambi as far as I know."
    author: "Richard Dale"
  - subject: "Re: Mono"
    date: 2008-02-17
    body: "It's not the point whether you are forced to use Qt, C++, C# or whatever. My point is: Qt/KDE C# bindings offer you a choice. A choice free to be taken, even to develop core modules with it. Why not develop core modules with Jambi?\n\n\nRegards,\nErik"
    author: "Erik"
  - subject: "Re: Mono"
    date: 2008-02-18
    body: "To cut yet another external dependency?"
    author: "Luciano"
  - subject: "Re: Mono"
    date: 2008-02-18
    body: "I'm not sure what a \"core module\" is, but I know that when I installed Kubuntu, neither Java nor Mono needed to be installed by default. Obviously, depedencies aren't a big deal on laptops and desktops, but when I get around to buying something like an Open Moko and install KDE on it, I'd rather not have Mono and Java taking up space. Also, in my experience (as a user) Java apps tend to be slower.\n\nI think it's awesome that people can write software in Java, C#, Python, or Ruby and tap into kdelibs and Qt goodness! Ideally ANY language could be used to write an app that integrates with the KDE. But preferably, the default install of KDE should not be dependent on all those other languages. (Qt's native implementation is in C++, so naturally KDE has to include support for C++.)"
    author: "kwilliam"
  - subject: "Re: Mono"
    date: 2008-02-18
    body: "It's the extra dependency, slowdown, and lack of real advantage. In GNOME, I can understand why using those languages would make sense (the syntax), but for KDE (better syntax), there is no advantage. It also slows it down, and adds another dependency (the runtime.) I don't mind making bindings to those languages (for example, lets say your a company making a cross-platform app and you only want to compile once), but the core modules should not need it."
    author: "Riddle"
  - subject: "Re: Mono"
    date: 2008-02-18
    body: "According to http://www.ohloh.net/projects/272/analyses/latest 2% of the KDE code base are already C# but I have to admit that I have absolutely no idea how accurate that number is."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Mono"
    date: 2008-02-19
    body: "Those measurements include non-official parts as well. I've compiled the 3 base packages kdelibs, kdepimlibs, kdebase. I've never seen it compile any C#."
    author: "Riddle"
  - subject: "Wow"
    date: 2008-02-17
    body: "I was just talking to my friend a few days ago and saying, \"I can't wait until they implement dragging plasmoids from the desktop to the panel.\"\n\nAmazing that its done!  I really can't wait for 4.1!"
    author: "0xA734"
  - subject: "Re: Wow"
    date: 2008-02-17
    body: "Download this and do it:\nhttp://www.kde-apps.org/content/download.php?content=57117&id=3&tan=81187343\n\n\"KDE Four Live\" 1.0.61 is a version with KDE 4.0.61 trunk snapshot packages,\n KOffice 1.9.95.3 and some extra applications.\n"
    author: "reihal"
  - subject: "Re: Wow"
    date: 2008-02-17
    body: "It has been actually backported for 4.0.2."
    author: "Anonymous"
  - subject: "When I'll Make the Switch"
    date: 2008-02-17
    body: "Right now I'm using GNOME, and I like it. Here's when I'll likely switch to KDE4:\n\n- When the new KDE-PIM and KOffice are released.\n- When I can get Auto-Scrolling turned on, especially in Konqueror.\n- When Konqueror has \"Undo Close Tab\".\n- When Konqueror saves and reopens my tabs, even if all power is suddenly cut off.\n- When I can reorganize the plasmoids in the panel through drag-and-drop.\n\nThis isn't a complaint list, or a wish list, or anything else of the sort. It's a collection of anecdotal information I think someone out there might find interesting. And I do know that at least 4 of the above are either being worked on anyway, or are already ready.\n\nI think that I'll be switching to KDE4 when 4.2 comes out. The reason is that the stuff that is planned for 4.1 probably won't all be ready on time (after all, stuff that was planned for 4.0 was put off till 4.1), and what will be shipped probably won't be feature complete or 100% polished.\n\nI realize that if I wait till KDE4 is \"ready\" then I'll only be able to use the last release to ever come out. I think that 4.2 is a good personal goal for me.\n\nI want to use only KDE4 and it's apps, and nothing else. To me the important apps are Krita, Karbon14, Amarok (never even touched it, but from what I've seen, I think I'll like it), Kaffein (never even seen it), Konqueror, KMail, and Kopete. Then in the realm of uselessly wasting time, I also like Konquest (are the AIs working finally?), and would like to use KPatience, especially if it's harder than AisleRiot and doesn't have the weird limitation of only going through the fountain 3 times.\n\nBTW, may I mention that the KDE4 card games look terrifically awesome?"
    author: "yman"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-17
    body: "- When the new KDE-PIM and KOffice are released.\n     --> beeing worked on\n - When I can get Auto-Scrolling turned on, especially in Konqueror.\n     --> possible for a long time (press Shift + up/down)\n - When Konqueror has \"Undo Close Tab\".\n     --> already available since 4.0.0 ..although I've encountered some crashes :-(\n - When Konqueror saves and reopens my tabs, even if all power is suddenly cut off.\n     --> don't know.. I too really miss this feature.. that's the reason why I still use Opera\n - When I can reorganize the plasmoids in the panel through drag-and-drop.\n     --> beeing worked on"
    author: "Bernhard"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-17
    body: "So you've found crashes with undo closed tabs? Please tell me about the details, send a bug report, I will fix those bugs ;-)."
    author: "Eduardo Robles Elvira"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-17
    body: "I've recently sent a mail about this crash to kde-core-devel@kde.org but it was rejected :-( ... I've also tried reporting a bug about this (at bugs.kde.org) .. but I've encountered an sql error ...\n\nI've cc'ed you the mail I've sent originally which describes the bug.\n\nWith the best regards\n\nBernhard"
    author: "Bernhard"
  - subject: "It's not about features, it is the quality!"
    date: 2008-02-17
    body: "I tried KDE 4.0.1 (openSuse) yesterday. I was impressed by Konqueror. Dolphin is not as much polished as it should be: It does not recognize if your left or right pane is active (and suddenly you delete the wrong files!), I could not figure out how to use the mouse in combination with the keyboard (mouse selection does not work at all, hitting a file always opens it etc.) and a pane with \"actions\", not only \"information\" is required (at least as it is hard to get into the context menu for the current directory as now).\n\nRegarding Plasma and Kwin, one should optimize and stabilize the code instead of implementing all KDE3 features. I was simply not able to run KDE longer than 3 hours in one session. Xorg took 100% CPU power all the time, even when doing nothing and when switching all desktop effects off. \n\nI also do not have any clue about all the options for rendering. Using the latest NVIDIA drivers on my 8400 GT, I can turn \"Vsync to blank\" or \"allow flipping\" on in the driver settings (what does this mean at all?). In the KWin settings I am able to chose the drawing method without knowing which one is best. There seems to be no documentation available yet. (Are there any help files on KDE4 which are at least partially complete?)\n\nMaybe I was mistaken and did some KDE4 newby mistakes, or the whole software must be regarded as alpha/beta stuff. I am really disappointed. Regressions are okay and understandable, but it is not about stability which MUST be fine on release. If you ask me, the quality management of KDE definitely did fail with 4.0"
    author: "Sebastian"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-17
    body: "Off course that's being worked on!\n\nOne of the things that caused Plasma performance to drain were the clipping algorithms in QGraphicsView. Trolltech never expected those would be used at all, but then came plasma. Now there is a complete desktop shell using QGraphicsView clipping all the time :P That's one of the things that got greatly improved in Qt 4.4.\n\nYou also mention NVidia issues. Try turning compositing off, and see how well it performs then. After all, all painting goes through KWin/compositing. If your xorg.conf or kwin settings are bad, it will degrade performance quickly.\n\nYou can improve that though with the settings described at http://dot.kde.org/1200050369/1200126492/:\n\nYour nvidia driver section should contain the following:\n\nOption \"AddARGBVisuals\" \"True\"\nOption \"AddARGBGLXVisuals\" \"True\"\nOption \"DynamicTwinView\" \"false\"\nOption \"RenderAccel\" \"true\"\nOption \"AllowGLXWithComposite\" \"true\"\n\nAnd the extensions section should look somewhat like:\n\nSection \"Extensions\"\n Option \"RENDER\" \"true\"\n Option \"DAMAGE\" \"true\"\n Option \"Composite\" \"Enable\"\nEndSection\n\nGood luck!"
    author: "Diederik van der Boor"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-17
    body: "Oh and to improve the performance even more for nvidia drivers, disable the \"vsync\" and \"direct rendering\" in kwin settings -> \"Desktop effects\" -> \"advanced options\""
    author: "Diederik van der Boor"
  - subject: "Thanks"
    date: 2008-02-17
    body: "The xorg.conf options are not the problem on my machine. Your hint regarding the Qt bug were rather helpful, though. The thing is, that even when all effects were turned off, Xorg took 100% cpu time (even if I just sit in front of a top without user action!). So I turned off all plasma desktop applets except the clock - and now - Xorg is now at constantly 5% which is at least acceptable. The particular applets were the system monitors and cpu frequ. analyzer.\n\nStill, I can't use dolphin. Can I probably turn on the double click somewere again? That would at least help a little. Currently it is just not consequent though through, I think. Don't get used to it and already moved files into the Nirvana..."
    author: "Sebastian"
  - subject: "Re: Thanks"
    date: 2008-02-17
    body: "> Still, I can't use dolphin. Can I probably turn on the\n> double click somewere again?\n\nDouble click can be turned on already by going into System Settings -> Keyboard & Mouse -> Mouse. If there are still some problems: please use bugs.kde.org to inform us developers about such problems so that we can fix it. I regularly read the dot to collect user feedback, but sometimes it's really frustrating that people complain in forums about the quality of KDE and don't give feedback to the developers directly by using bugs.kde.org..."
    author: "Peter Penz"
  - subject: "Re: Thanks"
    date: 2008-02-17
    body: "I am sorry to have left this impression. That was not on my mind. Though, I was really looking forward to the release and spent a whole day, just trying to get used to the new kind of software. A few details behaved different from what I expected. \n\nThis may be the wrong place to start a debate on bugs.kde.org, but I feel personally offended: I filed many many bug reports during the last years. They even receive some ratings by public votes. It is the minority where any KDE developer reacted at all. Some were just closed after a few years, because the feature it was referring to was removed, but not because a developer cared. By saying this, I do not want to blame the majority of developers, KDE is doing a great job, else I would not use bugs.kde.org. I just mention it, though I do not care much - KDE is not a company where I bought an expensive product. But please realize, that this is also not encouraging. So, my personal experiences with bugs.kde.org teached me to apply it only to severe bugs and wishes. Maybe this is the reason why I mentioned this here."
    author: "Sebastian"
  - subject: "Re: Thanks"
    date: 2008-02-17
    body: "> This may be the wrong place to start a debate on bugs.kde.org,\n> but I feel personally offended:\n\nSorry Sebastian, it was not my intention to offend you.\n\n> I filed many many bug reports during the last years.\n\nThanks for this!\n\n> It is the minority where any KDE developer reacted at all\n\nI understand that this is frustrating and for sure we developers have to be more careful for responding to bug-reports.\n\n> But please realize, that this is also not encouraging.\n\nYes, I agree and I'm not sure how this can be improved...\n\nStill the tendency to write about things that don't work on forums and blogs has increased a lot during the last years. It is fully OK to write about things that don't work, but I think the balance of writing about cool things vs. writing about things that don't work has shifted a lot to the \"writing about things that don't work\"-camp. Please note that this is just a general statement and my personal impression, this is not directly related to your comment :-)"
    author: "Peter Penz"
  - subject: "Re: Thanks"
    date: 2008-02-17
    body: "\"but sometimes it's really frustrating that people complain in forums about the quality of KDE and don't give feedback to the developers directly by using bugs.kde.org...\"\nI'm working on instant messenger now and if I would count only on 'direct feedback ' for new ideas our IM would die ;)\n\nPS. keep up the good work. I love dolphin"
    author: "patpi"
  - subject: "Re: Thanks"
    date: 2008-02-19
    body: "the system monitor plasmoid is in playground, not a production module. there's no guarantee anything in playground owrks properly or well. if your distro is packaging and installing plasma's playground code by default with kde4 they are doing you a disservice, imho.\n\n5% is also ... bizarre though. it should be silent as a mouse (are mice really silent, though? ;)"
    author: "Aaron Seigo"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-17
    body: "I can only recommend these settings because they work. I don't even have a modern machine: an ASRock mainboard, 1.8 Ghz AMD Sempron processor with an nVidia GeForce 6600 LE (256 MB RAM) and I recently upgraded to 1.5 gig of memory. \nI am now running KDE 4.0.1 which is quite stable and I can't complain about speed. The only problems that I am having are that my KDE 3.5x programmes crash now and then and I can't get Amarok 2 to run (keeps crashing but that's expected with pre-alpha software). Apart from that i am cool and very impressed with the speed at which bugs are being squashed and missing features are added to KDE4."
    author: "Bobby"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-17
    body: "> Dolphin is not as much polished as it should be: It does not recognize\n> if your left or right pane is active (and suddenly you delete\n> the wrong files!)\n\nI cannot reproduce this. Could you please submit a bug-report how to reproduce this (or mail me directly)? Thanks!\n\n> I could not figure out how to use the mouse in combination with\n> the keyboard (mouse selection does not work at all, hitting a file\n> always opens it etc.)\n\nI'd also need more details about what you mean so that we can fix this. We improved already some minor things in Dolphin for KDE 4.1 (selection area of items is optimized to the real size, selection toggle is available for the single click mode etc.), but to solve such issues we first have to be aware about that there is an issue and need feedback from users. It's OK to post things like this on the dot, but it would be more productive having a detailed issue description in bugs.kde.org ;-)\n\n> and a pane with \"actions\", not only \"information\" is required\n> (at least as it is hard to get into the context menu for the\n> current directory as now).\n\nWhy is it hard getting a context menu for the current directory? If it is because of the small viewport area: this has been fixed for KDE 4.1.\n"
    author: "Peter Penz"
  - subject: "Thanks for replying"
    date: 2008-02-17
    body: "Most of the issues are probably regarded to the viewport. But two things are not:\n\n(1) Is there a way to select a single way be using the mouse? \n(2) When using the keyboard (tab key) to switch panes (I used this after failing using the mouse) I found it 1st a little sad that I had to enter \"tab\" several times and that the tab key got caught by the QTExtEdit in the information panel. Second, if activating the left pane by using the tab key, the rectangle around the left pane becomes blue. But all actions on the toolbar and in the menu still refer to the other pane. "
    author: "Sebastian"
  - subject: "Re: Thanks for replying"
    date: 2008-02-17
    body: "> (1) Is there a way to select a single way be using the mouse? \n\nDo you mean selecting files in the single-click mode? There is a solution for this in Dolphin for KDE 4.1: a kind of \"selection toggle\" is blended in \"plasma-like\" above the item. If you don't have meant this: clicking the mouse on the viewport selects the active view in the split-view mode.\n\nRegarding the tab issue: I was not aware about this and have added this point to my TODO-list."
    author: "Peter Penz"
  - subject: "Re: Thanks for replying"
    date: 2008-02-17
    body: "I am fully satisfied, thanks :)"
    author: "Sebastian"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-18
    body: "Something that's bugging me with dolphin in 4.1 is when selecting a group of files and pressing down ctrl to select another group of files, the first group gets deselected ...i can workaround this by selecting only one file holding ctrl and then using the rubberband selection again :)"
    author: "fredde"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-18
    body: "This is a Qt-issue. I have not verified yet whether it still happens with Qt4.4, but will check it..."
    author: "Peter Penz"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-18
    body: "Ok, and yes, i'm using Qt 4.4 0214 :)"
    author: "fredde"
  - subject: "Re: It's not about features, it is the quality!"
    date: 2008-02-19
    body: "I've noticed this problem in kubuntu+kde4.0"
    author: "Iuri Fiedoruk"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-17
    body: "My wish list:\n\n- global key shortcuts for apps (opening dolphin with win+e) in kmenuedit\n- fix double-click for desktop icons\n- port quanta+ (I am a web developer after all)\n- quick launch plasmoid\n- way to move plasmoids of place in the panel\n- add more alternative plasmoids into KDE like Network Graph and Coremoid\n- better app launcher menu (the suse one.. I don't like)\n\nActually a very short list. I could add some improvements in oxygen, but this will soon be fixed in kde-look by people porting their styles and windows decorations soon ;)\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-18
    body: "- global key shortcuts - no kiddin', this has been in KDE since at least 5 years?!? There seem to be some bugs in there, but those should/could be reported at bugs.kde.org\n- the plasma stuff - yeah, probably all planned but not done yet ;-)\n- quanta needs more developers..."
    author: "jos poortvliet"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-18
    body: "I've read quite some time ago (at least a year or so) that the KDE4 version of Quanta will be based on KDevelop. If that's still true, that means no Quanta before KDevelop 4.0"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-18
    body: "Yes, it is planned for KDE 4.1.\nToo bad because it's one of the best tools (the same goes for kdevelop) that exitsts for KDE."
    author: "Iuri Fiedoruk"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-18
    body: "+++ to quicklaunch."
    author: "NabLa"
  - subject: "Re: When I'll Make the Switch"
    date: 2008-02-19
    body: "- fix double-click for desktop icons\n\nalready fixed. it works just fine here, so either you're waiting on an updated set of packages or.. i dunno. but this works.\n\n- quick launch plasmoid\n\nalt-f2 isn't quick enough? ;) i know what you mean though and there is already a wishlist for it on bugs.k.o. i'm sure it'll end up in at some point.\n\n- way to move plasmoids of place in the panel\n\nalready there, just trickier for the user than it needs to be (will be improved)\n\n- add more alternative plasmoids into KDE like Network Graph and Coremoid\n\ntalk to their authors to talk to us to get them moved into svn.\n\n- better app launcher menu (the suse one.. I don't like)\n\nthere's the traditional menu one if you prefer that."
    author: "Aaron Seigo"
  - subject: "Bluetooth "
    date: 2008-02-17
    body: "Is the new KBluetooth going to be Solid Based?\n"
    author: "Emil Sedgh"
  - subject: "Re: Bluetooth "
    date: 2008-02-19
    body: "This is what was said on the solid planning, but honestly I still don't know if the current implementation of kbluetooth4 uses it or blueZ."
    author: "Iuri Fiedoruk"
  - subject: "Konqueror and Quick Launch enhancement"
    date: 2008-02-17
    body: "Hello all !\n\nThanks a lot for the great job with Kde4. It's amazing and i think than it's a revolution for the Desktop and free sofware. I think that in some months and with the usability increasing with the coming of 4.1 or 4.2 people will more easily switching from windows and i will encourage my friends to do that.\n\nI'm a every day user of KDE 4.0 and for me the mosts annoyings things in working with it is the usability of Konqueror or Dolphin. Which are both promising too however.\n\nSome very useful functionality are missing like cut and paste files (i can cut but can't paste ?!), don't have the \"move to\" action with the popup menu when i right click (and other missing actions like archive functionality) and more annoying when i do the right click the file is automatically open (is it a bug ?).\n\nIn a other way i was thinking of the best way to have a quick launch feature for favorites apps.\n\nIs it possible to have an other panel, dedicated or not for quick launch action (after all it can be use for other functionnality), in the left of the Desktop (a vertical panel). This Panel will appear when the mouse come to the left board of the panel only when we need it to use it, so it is not invasive for the screen.\n\nSo we can have a lot of icons for launch apps whe can reach with just one click.\n\nI think that it would be great to increase the \"user experience\".\n\nBest regards"
    author: "okparanoid"
  - subject: "Re: Konqueror and Quick Launch enhancement"
    date: 2008-02-17
    body: "I think that it would be great to increase the \"user experience\".\n\nI mind :\nI think that THIS would be great to increase the \"user experience\".\n\nSorry for my poor english"
    author: "okparanoid"
  - subject: "Re: Konqueror and Quick Launch enhancement"
    date: 2008-02-17
    body: "> Some very useful functionality are missing like cut and paste\n> files (i can cut but can't paste ?!),\n\nI cannot reproduce this issue (tested with KDE 4.0.0, KDE 4.0.1 and trunk). Could you tell me which distribution packages you are using?\n\n> don't have the \"move to\" action with the popup menu when\n> i right click (and other missing actions like archive functionality)\n\nWe are aware about this and hopefully can enable this again in 4.1 (it is not straight forward to port this from KDE 3.x to KDE 4 and needs some time).\n\n> and more annoying when i do the right click the file\n> is automatically open (is it a bug ?).\n\nThis is also not reproducible, but we got several bug reports about this and it turned out being a distribution specific problem on Kubuntu. I don't know whether this has been fixed already by updated packages."
    author: "Peter Penz"
  - subject: "Re: Konqueror and Quick Launch enhancement"
    date: 2008-02-17
    body: "I'm on Kubuntu too, hardy release(kde 4.0.1).\n\nSorry for the bad report if it's a distribution specific problem :op"
    author: "okparanoid"
  - subject: "Re: Konqueror and Quick Launch enhancement"
    date: 2008-02-19
    body: "> Some very useful functionality are missing like cut and paste\n> files (i can cut but can't paste ?!),\n\nI'm on gentoo and have exactly the same problem. I can drag and drop but cannot cut/copy and then paste. If i click the right mouse button in the same window the paste option is greyed out, and in a different window the paste option is not even shown.\n\nI also have problems with opening/saving new files files with sftp in at least kate & kwrite. If one navigates to the file and tries to open a messages displays saying the file does not exist, ut it's clearly removed the entire path to the file. Very frustrating"
    author: "Rich Birch"
  - subject: "Re: Konqueror and Quick Launch enhancement"
    date: 2008-02-19
    body: "> Is it possible to have an other panel, dedicated or not for quick launch action \n> (after all it can be use for other functionnality), in the left of the Desktop \n> (a vertical panel).\n\nyou can already do this, but it is not exposed in the UI yet.\n\n> This Panel will appear when the mouse come to the left board of the panel only \n> when we need it to use it, so it is not invasive for the screen.\n\nthis is coming."
    author: "Aaron Seigo"
  - subject: "How's KDM?"
    date: 2008-02-17
    body: "One of the KDE components that as yet remains little changed in KDE4 is KDM. I know that GNOME & Ubuntu have great things planned for GDM. For anyone who hasn't seen, their mockup for a new \"face browser\" looks amazing: https://wiki.ubuntu.com/DesktopTeam/Specs/GdmFaceBrowser\nWill it be possible to implement something similar to this in KDM using good ol' Plasma? It would also be cool to have some widgets on your login screen e.g. a clock, liquid weather. Are any such plans in the works?\n\nAlso, I loved the Digest and I was especially interested in the KDiamond story (Plasma article was also great reading). Thanks everybody!"
    author: "Parminder Ramesh"
  - subject: "Re: How's KDM?"
    date: 2008-02-17
    body: "That face browser looks really nice. Too bad it uses opengl which, if I'm not mistaken, needs hardware 3d acceleration."
    author: "Petteri"
  - subject: "Re: How's KDM?"
    date: 2008-02-17
    body: "There's also bullet-proof-x stuff that would be nice to put in kdm"
    author: "John Tapsell"
  - subject: "Re: How's KDM?"
    date: 2008-02-17
    body: "Hmm.. I don't agree.   That looks flashy, but I really don't see the point.  You can already click on a user icon in KDM (and I assume GDM as well) and then just type in the password, so that is nothing new.  Then there is the filtering part, which is really not very useful.  I would hazard a guess that most people have a maximum of about 5 users on the system, so filtering is useless there anyway.\nIf you're running a community machine you might have hundreds of logins, so once again it is useless (not to mention 90% of those won't have images so it won't look nearly as nice as in the mockups. \n\nI'm not against making KDM look nicer and cleaning it up a bit, but I don't see the value in a login manager requiring opengl and having filtering and stuff.  The login manager is there to log in quickly, not to sift through pictures of other people that have accounts on the machine."
    author: "Leo S"
  - subject: "Re: How's KDM?"
    date: 2008-02-17
    body: "not to mention, the usability goes way down when there is that many pictures displayed. give me a break. Like you mentioned, with most people not adding their picture there would be repeated graphics and less savvy users would just get confused."
    author: "mike"
  - subject: "Actually, I disagree"
    date: 2008-02-17
    body: "KDM really does need to be extensable. In particular, for allowing a tv,radio, etc control. It would be useful to have the ability to meld it with mythtv. That allows a terminal to serve as more than just a login terminal."
    author: "a.c."
  - subject: "Re: Actually, I disagree"
    date: 2008-02-17
    body: "If you want to use MythTV, why not just use MythTV?"
    author: "Paul Eggleton"
  - subject: "Re: How's KDM?"
    date: 2008-02-18
    body: "beauty has value of it's own, but why should KDE imitate GNOME? there should be a beautiful login screen, but that doesn't mean it should lose functionality-wise or that users wouldn't have a choice."
    author: "yman"
  - subject: "Responses to above"
    date: 2008-02-18
    body: "@John Tapsell: From what I hear, bulletproof-x will find its way into kubuntu hardy or hardy+1, as it may be integrated into Xorg itself rather than *DM (Bryce Harrington's blog: http://www.bryceharrington.org/drupal/node/38). The only thing  KDE would need is an equivalent to display-config-gtk, which in any case was ported from KDE's guidance tools. So the wait may soon be over!\n\n@Leo S, mike: I guess maybe OpenGL could be overkill, especially if it bogs down the startup time. If there were tens/hundreds/thousands of users, then this screen would probably not fulfil its mission. But KDM development at the moment seems to have flatlined, so maybe if a developer with extra time was reading this (I wish) then it could inspire him/her to implement some innovations.\n\n@yman: Indeed, KDE should not imitate Gnome for imitation's sake, but there's nothing wrong with getting some inspiration from friends.\n\nKDM is obviously not as critically needy of workers as the rapidly developing Plasma or Kwin, but hopefully it will also get some lovin from a bright gearhead. Peace."
    author: "Parminder Ramesh"
  - subject: "In fact"
    date: 2008-02-18
    body: "WHile I am not working on KDM, I started work on a *dm designer. Sadly, I got busy with life, and will have to restart it. But in the end, I would like to see kdm be easily extensible. Being able to use the terminal for more than just a login is useful.\nwho knows, it looks like I am picking up a perm job with a company that designs equipment based on linux. Perhaps, I will be able to find the time to re-do kdm at the same time (though do not count on it)."
    author: "a.c."
  - subject: "Re: In fact"
    date: 2008-02-19
    body: "Good luck with the new job. Yes, time for hobbies is a bit difficult to get, isn't it."
    author: "Parminder Ramesh"
  - subject: "Re: Responses to above"
    date: 2008-02-18
    body: "\"@yman:...\"\nexactly what I mean. don't copy others, learn from them."
    author: "yman"
  - subject: "Re: How's KDM?"
    date: 2008-02-18
    body: "It's my understanding that KDM will become a plasmoid also :-)\n\nThere is a mockup but I don't know what it will look like when it's converted to a plasmoid.\n\nShawn.\n"
    author: "Shawn Starr"
  - subject: "Re: How's KDM?"
    date: 2008-02-18
    body: "Sounds awesome :) Thanks for the info."
    author: "Parminder Ramesh"
  - subject: "Fingerprint reader integration?"
    date: 2008-02-18
    body: "I think first and foremost KDM should remain secure and robust, so I'd dare to say that integrating it with libplasma or merging in flashy openGL effects for the sake of it would be insane ATM.\n\nThat said, I could do with some updates in order to reflect the new use cases. My pet whim is support for fingerprint readers. It's been reported for ages, but no one seems to tackle this bug/wish/whatever. GDM can handle fingerprint logon just fine.\n\nPerhaps the devs don't see it as a must-have feature (since not all computers have embedded sensors). But, to put it on another light: it's not *just* fingerprint readers, but all of the upcoming biometric/smartcard/multi-token security features  of which we'll be soon seing a surge, specially in corporate environments.\n\nAlso, it'd be nice to have KDM auto-unlock KWallet. Single Sign-on, anyone?"
    author: "tecnocratus"
  - subject: "Re: Fingerprint reader integration?"
    date: 2008-02-18
    body: "> Also, it'd be nice to have KDM auto-unlock KWallet. Single Sign-on, anyone?\n\nIf you are satisfied with protection through login, you can always use a password-less wallet."
    author: "Kevin Krammer"
  - subject: "Re: Fingerprint reader integration?"
    date: 2008-02-19
    body: ">>Perhaps the devs don't see it as a must-have feature (since not all computers have embedded sensors). But, to put it on another light: it's not *just* fingerprint readers, but all of the upcoming biometric/smartcard/multi-token security features of which we'll be soon seing a surge, specially in corporate environments.\n\nWell, I've been selling two-factor authentication solutions for almost a decade, mostly RSA's, and I'm still waiting for the surge in corporate environments that everyone has been talking about for years now. Was supposed to happen with the dot-com, then it was supposed to happen with SarBox, and now compliance is the alleged driver. The fish still aren't biting... ;)\n\nHaving said that, it would be nice to see KDM have some sort of hook or mechanism for strong-authentication, without having to resort to  workarounds. Though I'm not sure there is a standard mechanism for this yet that would allow it to be a simple hook.\n\n>>Also, it'd be nice to have KDM auto-unlock KWallet. Single Sign-on, anyone?\n\nThat's not SSO. The point of kwallet is to keep your data secure by requiring authentication even within your user environment, it's a separate mechanism from the PAM authentication that the login requires. If you're not concerned about that, then set your wallet with an empty password, and it will still be restricted to you only."
    author: "elsewhere"
  - subject: "Re: Fingerprint reader integration?"
    date: 2008-02-19
    body: "\"That's not SSO. The point of kwallet is to keep your data secure by requiring authentication even within your user environment, it's a separate mechanism from the PAM authentication that the login requires. If you're not concerned about that, then set your wallet with an empty password, and it will still be restricted to you only.\"\n\nNo, then my data will be stored in clear text, and if my laptop get stolen, we have a problem. And while encripting all the home partition is overkill, having PAM authentication to work with kwallet it's not.\nI hope that when Gnome will have it (maybe they already do, don't know) we'll really start to feel how useful this feature is."
    author: "Vide"
  - subject: "Re: Fingerprint reader integration?"
    date: 2008-04-24
    body: "one good solution is to use an encrypted home and login with pam_mount .. then it's fine to leave kwallet sans password since your passwords will still be encrypted on disk, as will your email, im logs, whatever."
    author: "jcs"
  - subject: "Re: Fingerprint reader integration?"
    date: 2008-02-19
    body: "> I think first and foremost KDM should remain secure and robust, so I'd dare to \n> say that integrating it with libplasma\n\nthem's fightin' words ;) seriously though, if you have security or stability concerns with libplasma, please point them out. no, not some random plasmoid here or there, but libplasma itself.\n\nwith the scripting support we can increase the robustness of add ons and for kdm the real benefits would be things like being able to host and even share useful common components ... like an on screen keyboard that's part of the canvas, or system information that gets published in a corner somewhere."
    author: "Aaron Seigo"
  - subject: "Re: Fingerprint reader integration?"
    date: 2008-02-19
    body: "Or a general part to take care of whatever someone would like to do with fingerprint recognition."
    author: "terracotta"
  - subject: "what's the poin"
    date: 2008-02-17
    body: "what's the point in a kde digest that is 6 days past the release of it, when another update should be available tomorrow.  Perhaps if you cannot get it out in a more timely matter you should get some help.  Seems pointless posting this so long after the commit."
    author: "anon"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "As I am not a person always a'jour with the development of kde4 I find these commit-digests great. I don't really care if they are 6 days \"late\"."
    author: "Limp"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "The Dot KDE comments section have become a haven of tantrum throwers and drama queens of late, but this is perhaps the most grating whinge I've seen for a long time.\n\n\"Perhaps if you cannot get it out in a more timely matter you should get some help.\"\n\nNo shit, sherlock! Unless you're volunteering, you're just stating the obvious and in a most disagreeable fashion.\n\n\"Seems pointless posting this so long after the commit.\"\n\nNot at all - many people are very interested in the weekly progress of KDE - it lets them know what is going on in general, and gives them a forum for discussion of any developments.  I doubt they much care if it's a whopping few days(!) late.\n\nIt's a chart of how things are progressing, not so up-to-the-minute life or death news service.  People interested in the latter are free to subscribe to the raw info here: \n\nhttp://lists.kde.org/?l=kde-commits&r=1&w=2\n\nI and many others prepare to wait for Danny's well-prepared edited highlights, even if they are a tiny amount out of date."
    author: "Anon"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "I so agree with you... I know how much work it is to do these digests (after having seen Danny prepare one at Akademy in Glasgow) and I really respect him for going through that every week. I couldn't care less if he doesn't make it on time. Especially considering how he adds more and more goodies like the introduction of new apps and stuff like that."
    author: "jospoortvliet"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "Hey, I'm just amazed at how timely and frequently these digests get published. There are things (called \"life\") that sometimes cause delays. And I, for one, do not regard it as pointless to publish it late, since I don't follow the development in any other way---it's still news to me, no matter when it comes out.\n\nBut perhaps Danny would be interested in your offer to help out, you could try offering more directly (& politely).\n\nThanks, Danny, for another great Digest!\n"
    author: "T"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "Danny, never mind when the digest is a few days late. It is always a pleasure to read and very appreciated. Thanks Danny!"
    author: "peter"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "Yeah, seriously, its nice to hear about the progress of things. I didn't even notice that last weeks never came out, I thought we had gotten a great treat this week by having the digest on sunday. :-)"
    author: "Ryan"
  - subject: "Re: what's the poin"
    date: 2008-02-17
    body: "perhaps that will have upset you sufficiently that you'll never come back and post another pointless comment.\n\nfantastic, another one bites the dust!"
    author: "mike"
  - subject: "Re: what's the poin"
    date: 2008-02-19
    body: "What is the poin of your comment?\n\nI know by experience how much work the Digest is. Hours per week, probably an hour a day to go through the emails, and maybe 5 hours to prepare. I suspect Danny takes more time than that since the results are far better than anything I produced.\n\nI also took it as a point of pride to have produced the Digest every week. Danny seems to have the same goal.\n\nI think the appropriate comment is Thank-you. Encouragement and thanks really help. Comments like yours take work to ignore.\n\nDerek\n\n"
    author: "D Kite"
  - subject: "Off-Topic - Help with desktop effects"
    date: 2008-02-17
    body: "I know this isn't the right place, but as this problem is kde4 related let's see if someone can help me out :)\nThe problem I have is that I'm using a HP w17e widescreen LCD monitor with a Intel 950 card in my machine. For the resolution to work well (getting widescreen on the native 1400x900 resolution) I have to use krandr. This is no problem exept that KDM gets a lower resolution and when KDE session starts it goes to the better one.\n\nThe problem is that when I enable desktop effects alltogether with krandr, screen isn't updated right. I change options on the dialogs and the old options are shown, some icons and text do not appear, taskbar is always showing the same apps over time, etc.\n\nThis seems to happen in compiz+gnome also, so I was wondering if someone here knows about this problem and have a fix? \nYou know, I got this new PC just for running kde4 with desktop effects :("
    author: "Iuri Fiedoruk"
  - subject: "Re: Off-Topic - Help with desktop effects"
    date: 2008-02-17
    body: "Are you using the Intel-version of the driver for your card?\n\nThis one should autodetect your displayresolution and automagically use the right resolution.\nNote that the old i810 does not do that."
    author: "Yves"
  - subject: "Re: Off-Topic - Help with desktop effects"
    date: 2008-02-17
    body: "Yes, I am using.\nIt does detect my resolution, but without using randr, I get a black border on the left (no widescreen) and virtual screen does not work, in lower resolutions it goes to fullscreen but isn't very nice, fonts get blured.\n\nUsing xrandr fixes all resolution problems, but adds this bug when using glx, parts of windows and programs simply are not updated.\n"
    author: "Iuri Fiedoruk"
  - subject: "Cool"
    date: 2008-02-17
    body: "I'm impressed by the across-the-board improvements. Just to (arbitrarily) pick one of many, backporting the 4.1 Plasma fixes to 4.0 is a generous donation of time from all. Thanks, developers/translators/bug reporters/artists/etc.!\n"
    author: "T"
  - subject: "A side note about KDiamond"
    date: 2008-02-17
    body: "The screenshot shown in the digest is already outdated, a current one is available at: http://www.kde-apps.org/content/show.php/KDiamond?content=74922"
    author: "Stefan Majewsky"
  - subject: "Re: A side note about KDiamond"
    date: 2008-02-19
    body: "Thanks for the great game! It looks wonderful!"
    author: "Darryl Wheatley"
  - subject: "One or two things missing"
    date: 2008-02-17
    body: "Well, contrary to Sebastian above, there are definitely some features I would like to see re-implemented in KDE4.\nI've been using KDE 4.0.62 several hours to do my usual work on it and noticed some stuff I really miss :\n\n- I use quite a lot the \"Copy to\" and \"Move to\" entries in the contextual menus in Konqueror, especially when browsing FTP sites. Having the possibility to browse through my entire tree to select the target is an extremely handy feature IMO. To my dismay, this does not seem to be available in either Dolphin or Konqueror anymore, and this is really a shame, as I find this feature much more useful than KGet.\n- The Google/Wikipedia/etc search menu in Konq has really become for me a must have, and I find it really sad that it's not available yet.\n- Having the possibility to have a specific wallpaper for each virtual desktop is not just an aesthetic feature : it allows you to know all the time on which desktop you are without even thinking. \n- I've seen that the taskbar can be resized in height (good) but I really hope that in the end, it can also be resize in width. With 16:9 screens, I really appreciate to be able to contentrate the taskbar in a reduced width.\n- It seems to me that it is possible to change the default file manager from Dolphin to Konqueror. Unfortunately, I've not found yet how to do that. And that's too bad as after using both, I definitely prefer Konq as a file manager.\n\nOn the plus side, now that I can use composite, I must say that I really like the Expose and Desktop Grid effect : fast and efficient. :-)\n"
    author: "Richard Van Den Boom"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "> I use quite a lot the \"Copy to\" and \"Move to\" entries in the contextual menus in Konqueror\n...\n> To my dismay, this does not seem to be available in either\n> Dolphin or Konqueror anymore, and this is really a shame\n\nWe are aware about this and will do our best to reintroduce this feature again for KDE 4.1. The reason that it was not available anymore in KDE 4.0 was mainly because we've been running out of time to port this from KDE 3.x.\n\n> It seems to me that it is possible to change the default file manager\n> from Dolphin to Konqueror. Unfortunately, I've not found yet how to\n> do that. And that's too bad as after using both, I definitely prefer\n> Konq as a file manager.\n\nYes, we should fix this too for 4.1... As a temporary solution: the Dolphin handbook has not been updated for the KDE 4 version of Dolphin and provides a description for KDE 3 how to make Dolphin the default manager. This should now also be valid the other way round: how to make Konqueror the default file manager. So please open Dolphin, open the Dolphin Handbook (F1), go to \"5. Frequently Asked Questions\" and \"5.2. How do I make Dolphin my default manager\". Hope this helps :-)\n "
    author: "Peter Penz"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "Thanks for the info. The time problem is an issue I perfectly understand. ;-)\nJust to be clear, the reasons I would like to switch back to Konq as a file manager is not to be viewed as a critic of Dolphin, it's just that I use my file manager a lot with explorer-like view, using URLs with many filters (like \"*.jpg\") in it and I felt I was more at ease with Konq. But there are things in Dolphin I really like too (the grouping of different files by type is really nice). I understood I would get them in the KDE4 konq also, so all the best. :-)"
    author: "Richard Van Den Boom"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "> But there are things in Dolphin I really like too\n> (the grouping of different files by type is really nice).\n> I understood I would get them in the KDE4 konq also, so all the best. :-)\n\nThanks :-) The grouping features and all other view-dependent features from Dolphin will be available in KDE 4.0.2 also for Konqueror (David Faure has already backported those things)."
    author: "Peter Penz"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "I would appreciate it a lot if Dolphin also had the File Associations KCM embedded as a configuration dialog page. I use to adapt, tweak and change those associations regularly, and have stumbled over the lack of file associations in Dolphin a few times already, even if I don't (yet) use it as my primary file manager.\n\nIf you don't know that this stuff lies around in System Settings' \"Advanced\" tab then you're completely lost on this matter. Sounds like an easy fix?\n\nOh, and thanks for lingering around on the dot, Peter... I think it really shows in the quality of Dolphin's user interface :)"
    author: "Jakob Petsovits"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "Hm... guess I don't want to belong to the \"reports bugs only on the Dot\" group, so: http://bugs.kde.org/show_bug.cgi?id=151094\n\nThat bug suggests that this is supposed to exist since December at least, but I still can't find any file association configuration in my Dolphin (on Kubuntu's KDE 4.0.1). Hm, strange."
    author: "Jakob Petsovits"
  - subject: "Re: One or two things missing"
    date: 2008-02-18
    body: "I've reopened the bug and added to my TODO-list :-)"
    author: "Peter Penz"
  - subject: "Re: One or two things missing"
    date: 2008-02-19
    body: "btw, you can quickly open this in krunner by typing \"filetype\" or \"file associations\" or just \"association\" (the latter is currently broken in trunk due to my recent fiddlings =)"
    author: "Aaron Seigo"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "> The Google/Wikipedia/etc search menu in Konq has really become for me a must have, and I find it really sad that it's not available yet.\n\nit's in extragear/base. if you want to use it, you have to compile it too. there's also a crashmanager that let you restore tabs after a chrash, but it seems to be broken atm\n\nregarding the defaul file manager. right clickt on a folder -> preferences and then click on the little wrench on the right, in the same line as \"Type: folder\". from there put konqueror at the top and it should work"
    author: "hias"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "Thanks for the info regarding Google search.\n\n> regarding the defaul file manager. right clickt on a folder -> preferences \n> and then click on the little wrench on the right, in the same line as \"Type: \n> folder\". from there put konqueror at the top and it should work\nDamn, I should have thought about that. Thanks!"
    author: "Richard Van Den Boom"
  - subject: "Re: One or two things missing"
    date: 2008-02-17
    body: "Regarding the search bar, I suggest using the keyword shortcuts, or whatever they're called in konqueror.\n\nJust type: \"ctrl+L\" then \"gg: \" and whatever you want to search.\n\"wp:\" does wikipedia, and there are many others.\n\nIt makes for a cleaner interface, and I find it's much faster.\n\nPS. Konqueror developers: it would be nice to right click a search field on a webpage, and choose \"Create Search\" from the context menu. I don't know if that's implemented yet."
    author: "Soap"
  - subject: "Global Key to launching app"
    date: 2008-02-17
    body: "One of the features I liked most in KDE3 was the hability to set a global key shortcut to any app in the menu.\n\nIs there plans to bring this feature back into kde4 or it is out for \"good\"?\nI miss opening dlphin with win+e ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Global Key to launching app"
    date: 2008-02-17
    body: "++ - would also like to see this make a return.  It's a great usability feature!"
    author: "Anon"
  - subject: "Re: Global Key to launching app"
    date: 2008-02-18
    body: "Huh? As far as I know, that ability has been in Kde4 from the beginning, though it didn't work flawlessly pre 4.0.1\nIt's in system-settings under \"advanced\" (or whatever the tab is labeled in english). The dialog is very similar to the one from Kde3. I've set win+d to start Dolphin. Seems more fitting."
    author: "Flitcraft"
  - subject: "Re: Global Key to launching app"
    date: 2008-02-18
    body: "I think they are speaking about KMenuEdit and not SystemSettings."
    author: "NabLa"
  - subject: "Re: Global Key to launching app"
    date: 2008-02-18
    body: "That is correct."
    author: "Anon"
  - subject: "Re: Global Key to launching app"
    date: 2008-02-18
    body: "Yep. In kmenuedit you can set a keyboard shortcut that will run/open an application (like win+f for firefox).\nKDE4 removed those, but left some global keys and removed a bunch (like win+D to show desktop) while adding new ones (mostly for plasma).\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Global Key to launching app"
    date: 2008-02-19
    body: "s,plasma,kwin,"
    author: "Aaron Seigo"
  - subject: "Since Peter is so responsive...."
    date: 2008-02-17
    body: "Can I add my pet request for Dolphin? ;)\n\nWhen I have a directory open in details view and sorted by name, I can just start typing letters to jump to files starting with that letter.  That works, except that the view jumps to file, but the file is not highlighted, so I get confused and have to still search for it in the visible files.  It would be nice if the first match got highlighted, so if I type \"test\" it would jump to the first file starting with \"test\" (as it does) and also highlight that file (which it currently does not, but just about every other file manager does).\n\nI'll report a bug on this if you don't already know about it... I didn't see anything related in the bug database.\n\nOtherwise Dolphin rocks.  Thank you for this great app :)  I really liked konqueror back in KDE3, but it was just way too complex most of the time.  Dolphin is much better suited to the task of file management, without losing the powerful features that made Konq good for me."
    author: "Leo S"
  - subject: "Re: Since Peter is so responsive...."
    date: 2008-02-18
    body: "Please, report that feature request in bugs.kde.org. ;)"
    author: "I\u00f1aki Baz"
  - subject: "Re: Since Peter is so responsive...."
    date: 2008-02-18
    body: "> Can I add my pet request for Dolphin? ;)\n\nSure ;-)\n\n> That works, except that the view jumps to file, but the file is not highlighted\n\nThis issue is on my internal TODO list quite some time and I'll try to fix it during this week.\n\n> Otherwise Dolphin rocks. Thank you for this great app :)\n\nThanks! We are currently also taking care that Konqueror gets back all features it had in KDE 3 (e. g. on the weekend we added a tree-support for the details view)."
    author: "Peter Penz"
  - subject: "Re: Since Peter is so responsive...."
    date: 2008-02-22
    body: "> This issue is on my internal TODO list quite some time and I'll try to fix it during this week.\n\nThis is fixed now on trunk."
    author: "Peter Penz"
  - subject: "Re: Since Peter is so responsive...."
    date: 2008-02-18
    body: "Piggybacking on your request...\n\nWouldn't it be nicer if typing in a few keystrokes automatically highlighted ALL matching files in the current folder/view?\n\nThat way I could, say, type in \"IMG\" and then all of \"IMG0001.jpg\", \"IMG0002.jpg\", etc. would be readily selected to be dragged onto another folder, opened in Krita, or whatever..\n\nThat could be further refined with wildcards or even regular expressions (!).\n\nI could also post this wish to bugs.kde.org as a follow-up on yours, if you like the idea.\n\n"
    author: "tecnocratus"
  - subject: "Re: Since Peter is so responsive...."
    date: 2008-02-18
    body: "Makes it easier to select multiple files with the same prefix. \nMakes it harder to select a single file. \n\nI would rather have the latter."
    author: "Santa Claus"
  - subject: "Re: Since Peter is so responsive...."
    date: 2008-02-18
    body: "Well if you want to see all the files, then I think what you want is the filter function.  Ctrl+i and start typing.  \n\nFor the single highlight, my usecase is to go into a folder looking for a file I know the name of and want to open.  So I know I want to open \"weddingsongs.txt\" and I just type wed in the folder, it jumps me down to where I want (or very close to it) and I hit enter to open it.  After that I'm done with it, and really don't want a filter active in the directory or anything else.  Also most lists allow this type of navigation so it's very consistent.\n\nThanks Peter by the way for your response!  I look forward to future versions of Dolphin."
    author: "Leo S"
  - subject: "Is it just me?"
    date: 2008-02-18
    body: "Is it just me, or do other also think plasmoids are beginning to look more and more like.....normal windows? I mean, if I look at al the stuff that get's added to it, it seem like properties for a normal window app. Resizing, DnD, Widget's, etc. Then why not give up the whole idea of plasmoids en make them little \"normal\" apps with some extra features like noborders, etc? What is the use-case?"
    author: "Fred"
  - subject: "Re: Is it just me?"
    date: 2008-02-18
    body: "Amen to that.\n\nI never saw the need to replace the whole desktop stack just because the old panel (kicker) had a messy codebase and was difficult to maintain. Then again, I never liked the concept of \"widgets\" (not just in KDE, but also Apple's or Vista ones). \n\nTo me, applications are just applications, no matter how heavy or lightweight they are. Artificially calling them \"plasmoids\" and then putting them under a completely different interaction paradigm goes against well-accepted usability laws (i.e. it introduces \"modes\").\n\nI remember having seen a paper on a new concept of \"layered\" window groups. The window manager would keep separate \"layers\" of applications according to their intended usage, eg: one layer for the usual workhorse apps, another one for little accessories (calculators, clocks, weather applets...), another one for background tasks (eg: Amarok). The user would then be able to choose what applications he/she would like to see on which layers, and whole layers could be brought to the front, pushed to the back, hidden, etc.\n\nThat's a cool idea because it would be 100% orthogonal to the concept of virtual desktops. Of course, the resulting interface could turn a bit too complex for most typical usage patterns.\n\nBut then again, the same idea could have been easily implemented just with virtual desktops: Desktop #1 is for work, Desktop #2 is for utils, and so on.\n\nI don't know what the Plasma devs have in mind for the future, so please, don't take this as jab. Though, as it currently stands, the whole Plasma concept seems to me as a bit of a \"me too\" feature, rather than something really needed."
    author: "tecnocratus"
  - subject: "Re: Is it just me?"
    date: 2008-02-18
    body: "I like the enforced MVC Pattern on Plasma, but thats something any normal App Developer should code like :-) \n\nThe second Thing i like, is that everything can be contained in everything,\nwhich make it easy to ajust to any form factor (but every formfactor has to be coded seperatly :-( if its not trivial zooming possible)\n\na third nice thing is the supported animations, but i think QT can do this too?\n\nbut i am with you guys, the same could be done with a normal toolkit and normal windows, and the QT Windows are styleable with CSS, which is better than the possibilities plasma gives...\n\nbut its all about the vision, is it ? :D\n\n"
    author: "anwort"
  - subject: "Re: Is it just me?"
    date: 2008-02-18
    body: "Thats Vision with a capital V.  \n\nBut I don't agree.  You need some sort of framework for little programs to exist on the desktop or panel.  Kicker had applets, and plasma has plasmoids.  It's really not so radically different.  You can't make things like the battery monitor or the clock or the device notifier into standalone apps, because the memory consumption would be through the roof, and getting them to integrate into a panel would be very tricky.  \n\nSo you need some sort of plugin system.  That's what plasmoids basically are.  They just happen to provide way more possibilities and much cleaner design than the old kicker applets.  The new hip thing is to call these plugins widgets/gadgets/wookies whatever but they're still basically the same applets that they used to be, with added flexibility to exist outside a panel.    Sure you could run gkrellm instead of a plasmoid, but then it wouldn't integrate into your desktop, it just kind of sits there.  I never used superkaramba or any other widget system for exactly that reason.  There was no integration.  Now with plasma, there is."
    author: "Leo S"
  - subject: "Re: Is it just me?"
    date: 2008-02-19
    body: "From a users POV you may be right, its just the same.  Which sounds great to me as it means we can make sure that usability problem is avoided.\n\nThe point of Plasma is to allow people (including you) to create something on their desktop themselves.  So even if people don't have a lot of experience coding can do something nice.\n\nI'm sure that a lot of people said the firefox framework was not valuable either until loads of new 3rd party plugins started appearing :)"
    author: "Thomas Zander"
  - subject: "Re: Is it just me?"
    date: 2008-02-19
    body: "> just because the old panel (kicker) had a messy codebase and was difficult to \n> maintain.\n\nthat was what got me started thinking about things, but not the ultimate reasons that i arrived at *due* to thinking about things. please don't frame things incorrectly, as much as i appreciate you creating a mythology around my actions. ;)\n\n> Then again, I never liked the concept of \"widgets\" (not just in KDE, but also \n> Apple's or Vista ones).\n\nso you never liked the taskbar? or the pager? or...? those are just \"widgets\" only insanely cumbersome to write, not very agile (e.g. can't move them about) and were all written in c++ as the only real choice.\n\nyou are confusing \"widget\" with \"toy\" because that's what most widget systems end up creating: little toys, some of which are slightly useful. \n\n> Artificially calling them \"plasmoids\"\n\nthere's nothing artificial about it. they aren't traditional applications in form, nor are they meant to be. they are components designed to work together.\n\n> and then putting them under a completely different interaction paradigm goes\n> against well-accepted usability laws (i.e. it introduces \"modes\").\n\nwhich modes are these?\n\n(and don't even get me started on the \"yes, everything is perfect as it is right now. don't... change.. a ... thing.\" sillyness)\n\n> I remember having seen a paper on a new concept of\n> \"layered\" window groups\n\nomg, it's containments! only with a zui as well! you'd think plasma might actually be useful now! ;-P\n\n> the whole Plasma concept seems to me as a bit of a \"me too\" feature\n\nyes, emphasis on \"seems to me\". i understand you don't grok it, that's cool. eventually you will. it just takes time for us to get all the ideas out there for consumption."
    author: "Aaron Seigo"
  - subject: "It's Just YOU"
    date: 2008-02-18
    body: "Don't fret about how plasmoid creators are creating plasmoids.  That has nothing to do with what plasma actually is.  Plasma is an infrastructure, and it's powerful.  Plasma brings the possibilities of almost infinite user interface customization.  As a general rule, everything you see on a KDE 4.0 desktop is a plasmoid.  Not just the desktop widgets.  The panel is a plasma \"containment\", the desktop is a plasma \"containment\", the plasma applets are plasmoids, the icons on the desktop are plasmids, and finally, the desktop widgets are plasmoids.  Everything is run by plasma. and it's ingenious.\n\nThe reason that I'm so excited about plasma is because it gives the possibilities, by creating new containments and plasmoids, to design complete new desktop interfaces.  Such as the one defined here: ftp://ftp.netlabs.org/pub/voyager/docs/MezzoGreypaper.pdf  I'm about to hike the Appalachian trail, but I think that that's what I'm going to do when I get back.  I'm sure that KDE will be mature enough by then."
    author: "batonac"
  - subject: "Re: It's Just YOU"
    date: 2008-02-18
    body: "Have a good holiday, hopefully you won't forget about the Mezzo interface. I had a read through the paper you linked and seems indeed very interesting. I may try SymphonyOS just for that... or see if Kubuntu's got some packages..."
    author: "NabLa"
  - subject: "Re: It's Just YOU"
    date: 2008-02-18
    body: "Unfortunately, that project is dead.  Although you can download a CD, they never made a complete implementation.  In my opinion, KDE 4.0 is a perfect platform to build such an interface on."
    author: "batonac"
  - subject: "Re: It's Just YOU"
    date: 2008-02-18
    body: "you forgot, its the same as \"everything is a window\", so there is no difference betweens this an \"everything is a plasmo-whatever\", but you start from scratch @ your technology stack"
    author: "same"
  - subject: "Re: Is it just me?"
    date: 2008-02-19
    body: "> Then why not give up the whole idea of plasmoids en make them little \"normal\"\n> apps with some extra features like noborders, etc? What is the use-case?\n\n* the ability to arrange the components together however you want\n\n* the ability to switch quickly between different groups of them\n\n* the ability to have little bits of App A sit next to App B (easing away from monolithic application development)\n\n* scripting components, sharing them easily\n\n* the ability to recreate a primary interface with very little work even for different form factors\n\nsee, the whole point is sets of components with clear interfaces between them. making them little applications completely negates the benefits of that approach.\n\nwhat you might be confused over is that plasmoids can be very useful. so you automatically relate that to what you already know (applications, windows!). but you know what? you *can* run a plasmoid as its own little normal app: plasmoidviewer <name of plugin>. you can't go the opposite way when your item is an application from the start.\n\nand because of that we end up with IM apps that can't drag a contact to the desktop, panel or pretty much anywhere else and have a reasonable representation of it, to give one bleedingly obvious example."
    author: "Aaron Seigo"
  - subject: "Yep, I'm one of them"
    date: 2008-02-18
    body: "Yep, I'm one of them. I don't care if the Digest is a few days late. I prefer it being thorough, rather then timely. \n\nAs long as the digests come out in a regular fashing (weekly to bi-weekly) and bring interesting information that shows great amounts of PROGRESS. I'm fine. Those digests is what I and many people use to get a quick snapshot of KDE development.\n\nI'm sure they also help in keeping interest in the project and keep new programmers involved and exited about it.\n\nOh and to the posting/whining question:\nMost people read the digest and respond to it because its \nA) faster \nB) easier \nto respond to than the complicated bug-report procedure. I can make a post here under 30 seconds. It takes much longer to do the same under bugs.kde (let alone the politics and complicated procedure to even get bug reports accepted and acknowledged. So not worth it (unless it's a REALLY BIG problem) ) Besides more people read the digests than the bug reports. (not to mention the dot.kde viewership :) ) \n\ndot.kde is also read by non-geeks (programers, developers, Linux nerds, etc.) So real users feel much less intimitaded to post her. As well as non-geeks usually avoid bug report lists (as they speak too much \"geek\", and non-geeks do not feel that they can contribute in a useful way anyway.)\n\nSo again: Thank you for the digest, and thank you to the dot.kde."
    author: "Max"
  - subject: "Z order in plasma"
    date: 2008-02-19
    body: "what about the z-order - or layers - for the plasmoids ?\n\nsomething like back to front, send to back and stick on top.\n\nalso to fix the overlaying problem when more than one plasmoids cover the same area .\n\nI think it's already in the TODO , but I just didn't notice it anywhere.\n\nRegards,\nMohamed"
    author: "Mohamed Assar"
  - subject: "Re: Z order in plasma"
    date: 2008-02-19
    body: "http://bugs.kde.org/show_bug.cgi?id=156876\n\nSummary: \"you can now set the zorder by click on the applet handles.\""
    author: "Hans"
  - subject: "Re: Z order in plasma"
    date: 2008-02-19
    body: "i contemplated a more complex set of z-order GUI controls, but opted for a simpler approach of click-to-bring-forward which seems to work rather well for the overwhelming number of cases. i'm very hesitant to add too many controls for this particular feature set right now since that can easily lead to insane numbers of controls and suddenly we have krita and not a desktop ;)\n\nbut at least now you can, indeed, control the z-ordering. weee! =)"
    author: "Aaron Seigo"
---
In <a href="http://commit-digest.org/issues/2008-02-10/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> applets can now be dragged from the desktop to the panel. More internet data sources for the Picture Frame and Comic Plasmoids. Configuration dialogs are added to many Plasmoids. The in-development "WorldClock" Plasmoid supercedes the KWorldClock standalone application. A new Plasma applet: Conway's Game of Life. KRunner becomes completely plugin-based. Support for editing GPS track lists in <a href="http://www.digikam.org/">Digikam</a>. More work on expanding theming capabilities across <a href="http://games.kde.org/">KDE games</a>. A variety of enhancements in <a href="http://korganizer.kde.org/">KOrganizer</a>. Initial work on a web interface to control downloads in <a href="http://kget.sourceforge.net/">KGet</a>. Work on paths and snap guides in <a href="http://koffice.kde.org/karbon/">Karbon</a>. A HTML part plugin in the scripting application creator, Kommander. Mono (C#) KDE bindings reach a usable state. Python support in <a href="http://www.kdevelop.org/">KDevelop</a>4. A <a href="http://basysblog.org/index.php/archives/decibel-status-update">return to development work</a> on <a href="http://decibel.kde.org/">Decibel</a>. <a href="http://kontact.kde.org/kmail/">KMail</a> gets a new maintainer, with already-noticeable improvements. KBluetooth and <a href="http://krecipes.sourceforge.net/">KRecipes</a> begin to be ported to KDE 4. The game Kollision moves from playground/games to kdereview. A new game, KDiamond, is imported into KDE SVN. <a href="http://commit-digest.org/issues/2008-02-10/">Read the rest of the Digest here</a>.

<!--break-->
