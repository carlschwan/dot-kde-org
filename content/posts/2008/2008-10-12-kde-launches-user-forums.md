---
title: "KDE Launches User Forums"
date:    2008-10-12
authors:
  - "skuegler"
slug:    kde-launches-user-forums
comments:
  - subject: "I get..."
    date: 2008-10-12
    body: "... a nice white page. :("
    author: "Jonathan Thomas"
  - subject: "Re: I get..."
    date: 2008-10-12
    body: "...unless I'm logged in. ;)"
    author: "Jonathan Thomas"
  - subject: "Re: I get..."
    date: 2008-10-12
    body: "The admins are working on fixing it."
    author: "sebas"
  - subject: "Re: I get..."
    date: 2008-10-12
    body: "Ah, that's why I didn't get the nice blank page at any moment, I never thought of logging *out*."
    author: "Irina"
  - subject: "Re: I get..."
    date: 2008-10-12
    body: "We've identified and disabled a faulty plugin. If you really miss the nice white page, contact us, and we'll mail it to you. ;)\n\nThanks for reporting.\n\nRob"
    author: "OhReally"
  - subject: "Nice job!"
    date: 2008-10-12
    body: "I've registered and browsed through the forums, and I think this is a very nice idea to get user participation outside mailing lists or distro channels. Add that with the fact that forum software was released under GPL...\n\nGood job KDE team!"
    author: "Luca Beltrame"
  - subject: "Spread the news"
    date: 2008-10-12
    body: "http://digg.com/linux_unix/KDE_Community_Launches_User_Forum\nhttp://www.reddit.com/r/kde/comments/76oo9/kde_community_launches_user_forum/"
    author: "Jure Repinc"
  - subject: "Reuse OpenID/.kde.org account?"
    date: 2008-10-12
    body: "\"The KDE Forum team is working on integrating the new KDE Forum with KDE's existing infrastructure...\"\n\nIt would be nice if I could use my account that I already have for other .kde.org services or just an OpenID account for the forum.\n\nThanks a lot for providing a forum - IMHO a thing the KDE project needed for a long time!"
    author: "eliasp"
  - subject: "Re: Reuse OpenID/.kde.org account?"
    date: 2008-10-12
    body: "We are working on providing an alternative openid login."
    author: "Ingo Malchow"
  - subject: "Re: Reuse OpenID/.kde.org account?"
    date: 2008-10-13
    body: "wonderful. openid is a great way to avoid trillions of usernames/passwords - thanks :)"
    author: "richlv"
  - subject: "Moving all app forums over to forum.kde.org"
    date: 2008-10-12
    body: "Hopefully all the app forums get moved!\n\nhttp://amarok.kde.org/forum/ >>> new Amarok sub-forum on forum.kde.org\nhttp://ktorrent.org/forum/ >>> new KTorrent sub-forum on forum.kde.org\n\netc..."
    author: "fish"
  - subject: "Re: Moving all app forums over to forum.kde.org"
    date: 2008-10-13
    body: "That would be greatindeed!!"
    author: "Whatever Noticed"
  - subject: "Re: Moving all app forums over to forum.kde.org"
    date: 2008-10-13
    body: "The Amarok have already told us they want to keep their own forum, since it's very succesful as it is.\nHowever, development teams are invited/encouraged to contact us; we're willing to discuss hosting application specific forums, if development teams bring in their own moderators.\n\nRob"
    author: "OhReally"
  - subject: "Re: Moving all app forums over to forum.kde.org"
    date: 2008-10-13
    body: "\"The Amarok have already told us they want to keep their own forum, since it's very succesful as it is.\"\n\nThat sucks. Very userunfriendly solution. Then please *remove* the Amarok sub-forum link. It takes me to the amarok forums and I have to register *again* to post something. Pretty stupid I must say. Better just remove the link altogether then..."
    author: "fish"
  - subject: "Re: Moving all app forums over to forum.kde.org"
    date: 2008-10-14
    body: "Cool down dude. The next version of Simple Machines has openid support."
    author: "Ian Monroe"
  - subject: "standard forum look..."
    date: 2008-10-13
    body: "Is there any effort to actually integrate the standard forum look into the KDE site design? The current design is a hodgepodge of some KDE site design with the whysoever popular crowded standard forum look. Bugzilla, wikimedia (or whatever tech- and userbase use, it's not credited) and others are integrated, it should be possible to do the same with MyBB."
    author: "Anon"
  - subject: "Re: standard forum look..."
    date: 2008-10-13
    body: "\"wikimedia (or whatever tech- and userbase use, it's not credited)\"\n\nhttp://techbase.kde.org/Special:Version\nhttp://userbase.kde.org/Special:Version\n\nThat works for all MediaWiki installations."
    author: "Stefan Majewsky"
  - subject: "Horay!"
    date: 2008-10-13
    body: "Glad the guys come up with a forum board for this project.\n\nI'm sure am pretty close to being a mailing lists hater."
    author: "Dread Knight"
  - subject: "mailing lists and forums"
    date: 2008-10-13
    body: "i looks like the forums overlap quite a bit with some mailinglists..\n\ni know ruby has some forum that dubs as a mailinglist, e.g. posts on the list appear in the forum and the other way around.\n\nwhat do you think about the doubling of mailinglists and forums?"
    author: "cies"
  - subject: "Re: mailing lists and forums"
    date: 2008-10-13
    body: "We're currently experimenting with this a bit: the forums in the News category are filled by mailing lists (automated). In the future we may create a plugin that does bi-directonal communication. We do need to find time for this, though."
    author: "OhReally"
---
The KDE Community today launches the new <a href="http://forum.kde.org/">KDE
Forum</a>. The new forum uses the bulletin board software <a
href="http://www.mybboard.net/">MyBB</a> offering users, developers and people
interested in KDE a place to help each other, discuss KDE-related topics and
exchange ideas. The KDE Forum complements
<a href="http://userbase.kde.org">KDE's UserBase</a>, the home for KDE users
as a valuable support resource.







<!--break-->
<h2>The Forums</h2>

<p>The forum offers a number of categories</p>

<ul>
        <li><strong>Forum</strong> lists announcement and has a section for
        providing feedback about the forum.</li>
        <li>In <strong>News Releases</strong> you can discuss the latest and
        greatest KDE news, and even follow commits to the KDE codebase in real
        time.</li>
        <li>The largest category is for <strong>Users</strong>, different KDE apps
        and modules are covered in the sub-forums here.</li>
        <li>The <strong>Operating Systems</strong> category gives those seeking to
        discuss platform-specific issues a place to post to. </li>
        <li>Want to get feedback about your code? Need to get more detailed
        explanations of a certain topic? The  <strong>Developers</strong> section is
        there for you. </li>
        <li>The <strong>Other Languages</strong> has pointers to local KDE Forums.
        These forums are not part of the "KDE Forums" proper, but nevertheless a place
        you might find answers to your questions and like-minded people. </li>
</ul>

<p>
The KDE Forums admins have set the structure up as a starting point. Sub-communities or
interesting parties can request their own subforum. The KDE Forum team is working
on integrating the new KDE Forum with KDE's existing infrastructure by means of
IRC bots that can announce new threads and posts on IRC channels, by briding
forum posts to mailinglist messages and by offering RSS feeds.</p>

<h2>MyBB Released as GPL</h2>

<p>In collaboration with KDE, the developers behind MyBB have
<a href="http://community.mybboard.net/thread-38942.html">decided to release</a>
their forum software under the terms of the GPL Version 3. Chris Boulton, project manager
at the MyBB Group said "<em>The KDE forum guys have come to us with the wish to use our
forum software. In this process, we needed to clear out our licensing to
make it easier for the KDE community to make use of MyBB. As a result of that, we
have now released the MyBB forum under the terms of the GPL. This was very well-received
by the community around MyBB</em>". Entirely in the spirit of Free Software, the KDE
Forum team has already contributed a number of plugins for MyBB, supporting the
decision taken by the MyBB team.</p>

<p>Rob la Lau, initiator and administrator of the KDE Forum adds "<em>We've
chosen MyBB as it stands out head and shoulders above other candidates we have
considered to adopt for the new KDE Forums. We know that a good piece of
software makes a lot of difference to the user, after all we want to create a
nice place for everybody to chat about KDE and related questions.</em>"</p>

<p>Sebastian K&uuml;gler, member of the KDE e.V. Board of Directors explains where the
new KDE Forum fits into KDE communication infrastructure, "<em>We've come to
believe that mailinglist don't cut it for all users. KDE becoming increasingly
widely used should also offer  a place for those that aren't familiar with the use of e-mail
for such discussions. The new KDE Forum complements KDE's new end-user
knowledge base Userbase that was launched last month. At the same time,
we're really happy to see that after our request the MyBB team has decided
to release their forum software under the terms of the GPL</em>".</p>

<p>So get your account and join the discussion now.
</p>





