---
title: "KDE Commit-Digest for 8th June 2008"
date:    2008-07-14
authors:
  - "dallen"
slug:    kde-commit-digest-8th-june-2008
comments:
  - subject: "Gitorious"
    date: 2008-07-14
    body: "I think there was a mix up here, I and a few other guys (Mario Danic, David and Jorge Cuadrado) wrote the Gitorious proposal, not Harald."
    author: "Patcito"
  - subject: "Re: Gitorious"
    date: 2008-07-14
    body: "(Ruphy helped too)."
    author: "Patcito"
  - subject: "Re: Gitorious"
    date: 2008-07-14
    body: "Ah yes, the problem was with the way names are referenced in Digests (Harald was in the previous section).\n\nFixed.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Its a tradition!"
    date: 2008-07-14
    body: "Thanks Danny."
    author: "Emil Sedgh"
  - subject: "Re: Its a tradition!"
    date: 2008-07-14
    body: "Yep, thanks :)"
    author: "DanaKil"
  - subject: "minor typo : 'relates'"
    date: 2008-07-14
    body: "minor typo : \"Digikam-relates libraries\" - should be 'related'"
    author: "richlv"
  - subject: "Re: minor typo : 'relates'"
    date: 2008-07-15
    body: "Fixed, thanks."
    author: "Danny Allen"
  - subject: "current"
    date: 2008-07-14
    body: "can we please have the most recent digest, and catch up with the older ones you've missed latter.  I mean they are all good to read, but people I am sure would rather have the latest digest to know what is going on in the latest commit rather than ones that are a month old.  "
    author: "anon"
  - subject: "Re: current"
    date: 2008-07-14
    body: "I agree."
    author: "Mark Kretschmann"
  - subject: "Re: current"
    date: 2008-07-14
    body: "In the end is up to Danny but personally I think that it's not the right thing to do. After skipping one I wouldn't be that motivated to do it. It's old* news after all and I believe there isn't that many people looking for completeness in the digests; just host new things. This way things are hot as they come out no matter how long were they implemented.\n\nTo Danny: 10 thumbs up.\n\n* 1 internet week = 2 human years (or something like that)"
    author: "Cl\u00e1udio"
  - subject: "Re: current"
    date: 2008-07-14
    body: "Well, sometimes being sure what people want isn't enough.\nI for example really _do_ enjoy reading every single commit digest and I wouldn't want Danny to skip any of those. So please speak for yourself."
    author: "Tom Vollerthun"
  - subject: "Re: current"
    date: 2008-07-14
    body: "I agree. No complaints do Danny, he is doing great job but digest is better when current.\n\nIMO good time would be to scratch 5-6 weeks after 4.1 release. Announce that and let authors of applications take care about tracking progress of their children for this time. In new digest let this notes be included."
    author: "mikmach"
  - subject: "Re: current"
    date: 2008-07-14
    body: "I disagree. If he would skip a few, he would get to the digest around the RC's, and KDE being in freeze is far less interesting. Keep the old ones goin' ;-)"
    author: "jos poortvliet"
  - subject: "Re: current"
    date: 2008-07-14
    body: "What use it is really to you if you have up-to-the-day information about what highlights have been commited? If you really need this, use http://commitfilter.kde.org/\n\nIf asked I am perfectly okay with a continuous series, even if it is just a month late. Heck, there a journals published just every quarter, you think noone needs them? If the information is useful on its self, and not just by being \"new\", \"live\", and other questionable attributes, then some timely distance is no real problem. What is your usecase?\n\nI rather have a problem with the low bus number Danny gives here. All this is done by a single person, in his free time, isn't it! Who could replace this man?\nDanny, thanks for every single digest.\n"
    author: "frinring"
  - subject: "Global keyboard shortcuts for applets"
    date: 2008-07-14
    body: "I hope this is the first step for the return of all global shortcuts.\nI miss launching firefox with winkey+f, showing the whole desktop with winkey+d, opening kopete messages and all such things keyboard allows me to.\n\nPS: It's funny to have an eeePC, in that I run KDE 3.5 and KDE 4.1 in my desktop computer. It's easy to see that for now, the KDE4 desktop (this excludes KDE4 apps, I'm talking about kicker+kdesktop+superkaramba vs. plasma) does nothing more than KDE 3.5.\nWhy it's funny? Because some people say they do not like the \"revolutionary\" plasma. For me, at this point, plasma is a revolution only for developers who have a different framework to use. (and have a lot of potential, but still not reached the 3.5 point).\n\nI'm liking KDE 4.1, but each day I want more to see 4.3 (I think this will be the version where plasma changes will really happen, when it's API is frozen for good)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Global keyboard shortcuts for applets"
    date: 2008-07-14
    body: "> I hope this is the first step for the return of all global shortcuts. I miss launching firefox with winkey+f, showing the whole desktop with winkey+d, opening kopete messages and all such things keyboard allows me to.\n\nThat is totally unrelated. What you mean was the ability to define shortcuts for arbitrary actions. The commit explicitly implements a given set of actions for Plasma applets."
    author: "Stefan Majewsky"
  - subject: "Re: Global keyboard shortcuts for applets"
    date: 2008-07-14
    body: "Yep.  KHotKeys is what the OP is referring to, and mjansen merged the khotkeys-rework branch he was working on into trunk yesterday.  I'm not sure what has changed in that branch, though."
    author: "Anon"
  - subject: "Re: Global keyboard shortcuts for applets"
    date: 2008-07-14
    body: "well, sort of. you did focus only in the first two examples. :)\n\nI've said I missed setting a global key for opening received kopete messages also. It could be that it was kopete fault, but I did not see any \"configure global shortcuts\" in the KDE4 apps I used those in KDE3."
    author: "Iuri Fiedoruk"
  - subject: "Re: Global keyboard shortcuts for applets"
    date: 2008-07-14
    body: "> showing the whole desktop with winkey+d\n\nif you add one of the \"show desktop\" widgets and then add winkey+d to the config file you'll get this right now. in 4.2 we'll have a UI for setting the shortcuts which will make this all a bit easier.\n\n> It's easy to see that for now, the KDE4 desktop [..]\n> does nothing more than KDE 3.5.\n\nit does a lot more but i'll spare everyone the feature parade right now. =) but i think that in a way this is sort of a sign of the success of things:\n\nthe framework underneath is, as you note, very different and, most importantly, has no assumptions or ideas about a \"desktop\" yet we have been able to replicate one very easily.\n\nnote that we *had* to replicate that experience because, as is overwhelmingly evident, people freak out about the smallest changes. a huge instant change would be suicide. so i opted for a slow progression in the defaults and available features (the plan for over 2 years now =)\n\nin any case, i gave my laptop to a friend this weekend to play with things (they are a research scientist (biology); they'd never used a linux laptop before) and when they discovered the ability to create multiple containments and zoom in/out between them, folderview, some of the web service based applets .. they said something to the effect that this would change the way they used the computer (for the positive). \n\nthey also kept asking \"you actually wrote this?\", so maybe i need to work on my \"annoyingly elite hacker\" persona more ;-P\n\nwith 4.1 we're starting to get emails coming in documenting how plasma has improved and changed in fundamental ways how people are using their computers. and best: it's doing so without forcing that change on those who don't want to change.\n\n> but each day I want more to see 4.3\n\nyou and me both...\n\n> (I think this will be the version where plasma changes will really happen\n\nit's safe to say we'll be seeing interesting, and hopefully impressive =), changes happening with each release now.\n\nthe new foundations are laid, and now it's an evolutionary march (run?) to build the tower up one floor at a time =)"
    author: "Aaron Seigo"
  - subject: "Re: Global keyboard shortcuts for applets"
    date: 2008-07-15
    body: "I am glad we agree.\nThe spirit of my comment was actually everthing you said above :D\n\nJust about the \"does nothing more than KDE 3.5\", think of plasma (now) being (almost) the same as kicker+kdestop+superkaramba.\nNow the difference is that Plasma is a zillion times more mature, well designed and built technology, and again, for those who love critic Plasma (me included, heh): Plasma is a foundation. Today it hosts a panel that is still a bit worse than kicker and some toys much like a.m.o.r., sure we all wanted the toys would be the last part implemente, BUT, those are just a part of the house. The future will bring a lot of changes into it.\n\nPS: I'm waiting for PHP bindings to start programming Plasmoids, any chance of those in KDE 4.2? :D"
    author: "Iuri Fiedoruk"
  - subject: "kdevelop4"
    date: 2008-07-14
    body: "I've just had a look at the new kdevelop4 from svn.\n\n                  *WOW*\n\nIt makes a really great impression! Its quite fast and looks great. Try it!"
    author: "anonymous coward"
  - subject: "Re: kdevelop4"
    date: 2008-07-15
    body: "just compiled it from svn and indeed, it looks great.\nThanks to all people involved in this project :)"
    author: "DanaKil"
  - subject: "Almost ready to ship!"
    date: 2008-07-14
    body: "While I still seem to be having severe performance problems on my home system (most likely due to video driver), I updated trunk on my work system and the the improvements in KDE4 over the last month over phenomenal! Congratulations to all the heavy lifters out there and the rest of the developers!"
    author: "David Johnson"
  - subject: "Digikam!"
    date: 2008-07-14
    body: "Woo!  This is *exactly* the kind of thing I'd been hoping to see in Digikam since the development of Marble was announced.  Huge kudos for getting it done!  I'm geotagging my photos these days but it would be really good to have a solid desktop application that could make good use of this data.\n\nAnother useful feature, which I'm sure the developers have already thought of, would be the ability to plot all my photographs as points on a map and then browse around them.  Maybe this is even already possible!\n\nFinally, whilst I'm on the subject, integration with OpenStreetmap for geotagging without using Google Maps would be really cool too for occasions when I don't have a GPS on me. (not that there's anything wrong with google maps, it's just not what I prefer)"
    author: "Mark Williamson"
  - subject: "Re: Digikam!"
    date: 2008-07-14
    body: "Marble now has OpenStreetmap support, so that should be quite possible one would think =)"
    author: "Aaron Seigo"
  - subject: "Git/Gitorious for KDE?"
    date: 2008-07-14
    body: "It'd be interesting to hear more about the reaction to the proposal to switch to Git and what plans the community have so far.\n\nI use Git (with Gitorious) for a KDE applications I've been developing and absolutely love it. It was far easier to setup than SVN had always been for me (pretty much nothing to setup for local developments, which is very nice IMO)."
    author: "Kitsune"
  - subject: "Re: Git/Gitorious for KDE?"
    date: 2008-07-16
    body: "It'll be discussed at Akademy. Not sure if the idea actually is to switch to Git, but many developers already use it so it's not unlikely."
    author: "jos poortvliet"
  - subject: "Amarok tabs"
    date: 2008-07-14
    body: "That Amarok theme is really nice for a default. Only thing I don't like about it are the tabs on the left side.\n\nHmmm... I really should start getting into a bit of development instead of just posting on bugs.kde.org. :)"
    author: "parena"
  - subject: "Re: Amarok tabs"
    date: 2008-07-14
    body: "Yes, we are aware that the tabs aren't exactly a thing of beauty. It's because I messed with them, and I'm not exactly an artist ;)\n\nBut stay tuned, we have a lot of improvements planned in the graphical department :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok tabs"
    date: 2008-07-16
    body: "It's a pity everyone complains about the vertical tabs, yet nobody seems to be able to come up with a good alternative... :("
    author: "jos poortvliet"
  - subject: "Re: Amarok tabs"
    date: 2008-07-16
    body: "How about this? I know the icons are fuzzy (I just grew the ones from the vertical tabs) but the ideas are there. It has okular-style tabs, and the row of buttons moved from the bottom corner to the top right."
    author: "Mockup here"
  - subject: "So much exciting stuff!"
    date: 2008-07-15
    body: "I am very happy as I see all the movement and colour of KDE's contributors. \nI heard a while ago that someone wanted to put a patch into Phonon to show the titles of chapters, but this was rejected as it would break binary compatibility. Has this issue been resolved?\nAlso, I'm really looking forward to the new games being made. In particular the Step-game, as I was a \"The Incredible Machine\" tragic! Also KDiamond, I wonder if it will come with power-ups?\nThanks all"
    author: "Parminder Ramesh"
---
In <a href="http://commit-digest.org/issues/2008-06-08/">this week's KDE Commit-Digest</a>: Global keyboard shortcuts for applets, and an <a href="http://amarok.kde.org/">Amarok</a> and "python expression" runner in <a href="http://plasma.kde.org/">Plasma</a>. A Java test applet and various interaction improvements in Plasma. Simple network and CPU monitors in the system-monitor Plasmoid. Initial import of PeachyDock, a Plasma-based alternative panel. The <a href="http://oxygen-icons.org/">Oxygen</a> window decoration gets the "on-all-desktops" button. Continued development toward Amarok 2.0. <a href="http://www.kdevelop.org/">KDevelop</a> gets a new context browser, and various other improvements. Initial work on SVG theming in <a href="http://edu.kde.org/parley/">Parley</a> and <a href="http://stepcore.sourceforge.net/">Step</a>. Support for OpenGL rendering in Palapeli. Enhancements for KDiamond 4.2. Nonogram switches to its own package format, with the import of a collection of game files in this format. Planned developments start to materialise in KColorEdit. Map-based searches in <a href="http://www.digikam.org/">Digikam</a>. Digikam-related libraries move to kdegraphics for KDE 4.1. Enhanced printing support (selections, zooming) in <a href="http://www.koffice.org/kspread/">KSpread</a>. KThumb, a simple command-line utility for managing freedesktop.org thumbnails. Optimisations in <a href="http://kate-editor.org/">Kate</a>, <a href="http://enzosworld.gmxhome.de/">Dolphin</a>, and kjs-frostbyte. Ruby bindings for various KDE facilities (QtWebKit, <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>, etc). Decibel strips some of its KDE dependencies, and moves to kdesupport. KDiskManager is removed to make way for a replacement. <a href="http://www.mailody.net/">Mailody</a> moves to kdeextragear. <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>, <a href="http://www.kmobiletools.org/">KMobileTools</a>, and the <a href="http://www.kontact.org/">Kontact</a> Planner summary plugin are disabled for the <a href="http://pim.kde.org/">KDE-PIM</a> 4.1 release. <a href="http://commit-digest.org/issues/2008-06-08/">Read the rest of the Digest here</a>.
<!--break-->
