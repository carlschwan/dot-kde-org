---
title: "Closing Day at the Release Event"
date:    2008-01-22
authors:
  - "jriddell"
slug:    closing-day-release-event
comments:
  - subject: "who could guessed !!!"
    date: 2008-01-21
    body: "who could guessed that the guy in the middle of the first picture is danny ;-)   i am not sure but the girl near him isn't celeste paul.\n\nah and the t shirt u have looks cool.\n\n"
    author: "djouallah mimoune"
  - subject: "Re: who could guessed !!!"
    date: 2008-01-21
    body: "It is Celeste."
    author: "Anonymous"
  - subject: "Re: who could guessed !!!"
    date: 2008-01-21
    body: "Nah, thats Terry Gilliam in -68."
    author: "reihal"
  - subject: "Re: who could guessed !!!"
    date: 2008-01-22
    body: "Actually this is Linus Torvald's younger sister"
    author: "she"
  - subject: "SQO-OSS"
    date: 2008-01-21
    body: "There was also a SQO-OSS BoF with Paul and Ade. <http://www.sqo-oss.eu/> The highlight was pretty pictures of KDE metrics."
    author: "David Johnson"
  - subject: "Name error"
    date: 2008-01-21
    body: "s/Andrew Mortan/Andrew Morton"
    author: "egan"
  - subject: "KNetworkManager"
    date: 2008-01-21
    body: "I'm sooo glad development of KNetworkManager is picked up again. It is these kind of applications that need a lot of TLC to make KDE rock for 'non-geek' users. Now I'm hoping the kbluetooth will be next...\n\nKeep up the good, no great work!"
    author: "dirtyharry"
  - subject: "Re: KNetworkManager"
    date: 2008-01-21
    body: "Helmut Schaa at SUSE is actively working on an updated KNetworkManager for the KDE 3 series, compatible with the 0.7 version of NetworkManager.\n"
    author: "Kevin Krammer"
  - subject: "Re: KNetworkManager"
    date: 2008-01-21
    body: "Great to hear. If I understood correctly NM 0.7 brings lot of improvements over the current version, and I can't wait to try it out. "
    author: "dirtyharry"
  - subject: "Re: KNetworkManager"
    date: 2008-01-22
    body: "In case you are interested:\nOpenSuse has some packages for the newest knetworkmanager available:\nhttp://download.opensuse.org/distribution/SL-OSS-factory/inst-source/suse/i586/NetworkManager-kde-0.7r759902-6.i586.rpm\n\nThe current svn directory where knetworkmanager is develop is\nhttp://websvn.kde.org/branches/work/knetworkmanager/knetworkmanager-0.7/\n\nHowever, please keep in mind that NetworkManager 0.7 is not yet stable! Fedora 8 was shipped with it, but there are multiple complaints that it doesn't work as well as NM 0.6.5. Also, the current ToDo still has two major features not completed yet:\nhttp://live.gnome.org/NetworkManagerToDo\n\nLast but not least it looks like NetworkManager will even publish another 0.6.x release:\nhttp://mail.gnome.org/archives/networkmanager-list/2008-January/msg00092.html\n\nWe will have to wait for 0.7 for some more time, I think. Maybe there will be a stalbe-enough version at release date of Fedora or Opensuse."
    author: "liquidat"
  - subject: "Irn Bru?!"
    date: 2008-01-21
    body: "What the heck is Irn Bru doing there? Is this some sort of sick Scottish joke? That stuff shouldn't be legal.\n\nThe VideoLan dev is Jean-Baptiste Kempf."
    author: "Ian Monroe"
  - subject: "Re: Irn Bru?!"
    date: 2008-01-22
    body: "Perhaps because they think the pillars of KDE4 are made of girders and Irn Bru is made BY girders and in the translation across the pond it got mixed up?\n\n"
    author: "me ep meep"
  - subject: "Re: Irn Bru?!"
    date: 2008-01-22
    body: "It was *awesome* that Jonathon Riddell brought some Irn Bru - I hadn't tasted it in about 4 or 5 yrs :D"
    author: "Joseph Gaffney"
  - subject: "It's unfortunate"
    date: 2008-01-22
    body: "It's unfortunate that one can't attend such wonderful event! \n\nOn an unrelated note, is the new KNetworkManager going to reply on Solid for interacting with the daemon or it's going to directly talk to NetworkManager?\n\nThe recent efforts to share common functionality in cross-platform cross-desktop multi-API'ed libraries are really exciting and I could only wish for more of them! I recently starting to think about cross-distro tools for configuration e.g. PackageKit. It's to have the same (API) interface to manage a system regardless of it being debian-, redhat-, gentoo-like or any other one. I even think of iabout whether it's GUI interface could be written as a combination of a scripting language and a GUI descriptor XML file (or any other format) that gets rendered using Qt, GTK or any other toolkit with the appropriate backend; while the scripted part calls that cross-distro system configuration library (over DBus, may be?). \n\nI think that could much consolidate so much effort from every distro and unify our vision while still having the freedom to choose! It's a dream, man!\n "
    author: "A Freedom Advocate!"
  - subject: "Re: It's unfortunate"
    date: 2008-01-23
    body: "sounds a bit crazy but this came to my mind when I read your comment:\n\nwill it be possible to use knetworkmanager on windows, if knetworkmanager is going to talk to solid instead of networkmanager directly (and of course if there's going to be a good windows backend for solid)... would be really great because the windows client for wireless sucks and starting another program (such a fat app like they get shipped with your notebook) for doing so is really lame\n\nWith the best Regards\n\nBernhard\n\nPS: Finally I've managed to get a KDE4 session compiled, running and ready for development.. hope I'll be able to help out with my limited knowledge in KDE4"
    author: "Bernhard"
  - subject: "Annual Conference in Jamaica?"
    date: 2008-01-23
    body: "If Jamaica is chosen for the  annual conference next year then that would be a total joy for me because I am planning to fly to Jamaica next year and I would love to meet these great KDE heroes. Apart from that I think that Jamaica needs a little more exposure to open source. The most people there never heard of linux or KDE."
    author: "Bobby"
  - subject: "VLC"
    date: 2008-01-24
    body: "\"A developer from the VLC (VideoLan) project gave us their experience of porting their cross-platform video application to Qt.\""
    author: "Gentr"
---
After the star studded talks of the main event day, the final day of the KDE 4.0 Release Event returned to the un-conference format of small group talks, demos and discussions.  KDE Dot News listened in to some of the sessions, read on for brief summaries.











<!--break-->
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 250px">
<a href="http://static.kdenews.org/jr/kde4-release-event.jpg"><img src="http://static.kdenews.org/jr/kde4-release-event-wee.jpg" width="250" height="188" /></a><br />
Discussing KDE 4 over some Irn Bru
</div>

<p>Benjamin Reed (RangerRick) started the day with a Q&A session on the Mac port of KDE.  The <a href="http://techbase.kde.org/index.php?title=Projects/KDE_on_Mac_OS_X">Mac builds of KDE</a> are available and work reasonably but there are a number of issues to be sorted yet as well as some bugs that have appeared with Qt using the latest version of OS X so it is not recommended for everyday use.  Some unresolved issues include the duplication of Strigi against Apple's Spotlight and lack of a Solid backend.  Benjamin hopes to have stable builds available for KDE 4.1.</p>

<p>Later in the day Holger Schröder demonstrated <a href="http://windows.kde.org/">KDE on Windows</a>.  Currently the underlying libraries are not stable enough for everyday use and as with the Mac port, the Windows team expect to have a stable release along with KDE 4.1.</p>

<p>A session was held with the plan to turn this event into an annual conference of KDE in the Americas.  Locations being looked at include Mexico and Jamaica.  Following the success of this event, there seems to be a good level of confidence that KDE could support a second annual large conference.</p>

<p>Jason Harris demonstrated KStars, the professional quality astronomy application.  Jason uses it in his job as an astronomer to map his way around the milky-way from his university's telescope in Chile.  KStars has this week proved itself as an excellent demonstration app, in a few movements it is possible to show complex physics in a way understandable to all.</p>

<p>KNetworkManager is currently undergoing some re-development with ports needed to both KDE 4 and the work-in-development NetworkManager 0.7.  A session was held on the usability of the application since there will soon be many more network options supported than currently and the existing user interface will not scale.  A design was mapped out which should be able to make sense of the new functionality.</p>

<p>The technical preview of Amarok 2 was demonstrated to excited masses.  The user interface has been entirely redesigned from the stable release with context view set centre screen and the playlist turned from a grid layout to a more space efficient custom list with album headers.  When asked how the demo went, developer Leo Franchi said, "the audience was wowed into submission by Amarok's coolness".</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 250px">
<a href="http://static.kdenews.org/jr/kde4-release-event-opendocument.jpg"><img src="http://static.kdenews.org/jr/kde4-release-event-opendocument-wee.jpg" width="250" height="188" /></a><br />
Continuing the OpenDocument Discussion out of Hours
</div>

<p>Alexander from OpenOffice talked about some of the possibilities of OpenDocument including dedicated C libraries to process the format which could be shared between apps.  KOffice developers discussed plans for an OpenDocument API in kdelibs to make use of the format available throughout KDE.</p>

<p>At the KPilot BoF, developers Adriaan de Groot (being afraid to touch code at his advanced years) and Jason Kasper started discussing the architecture and integration layers between kernel, USB and underlying libraries.  They attracted the attention of two likely new developers in the progress.</p>

<p>A developer from the VLC (VideoLan) project gave us their experience of porting their cross-platform video application to Qt.  They are considering making a Phonon backend of VLC which would allow for using the same multimedia framework on all platforms.</p>

<p>When the day drew to a close we celebrated with <a href="http://franz.keferboeck.info/gallery/main.php?g2_itemId=525">KDE 4 wine</a> and enjoyed some entertainment at the karaoke bar, if you are brave there are <a href="http://wiki.kde.org/tiki-index.php?page=Release+Event">photos and videos</a> available.</p>

<p>There were also big name visitors from the Linux community including Andrew Morton and developers with NVidia and AMD, as well as many from within our hosts, Google.  This event has not only been a successful celebration of the start of our KDE 4 series, it has also been an excellent opportunity to meet and talk with a section of our community who have been unable to get to our European conferences.  Many thanks to Franz, Jeff, Troy, Wade, and the other organisers.</p>