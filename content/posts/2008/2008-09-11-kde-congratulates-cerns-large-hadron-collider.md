---
title: "KDE Congratulates CERN's Large Hadron Collider"
date:    2008-09-11
authors:
  - "jriddell"
slug:    kde-congratulates-cerns-large-hadron-collider
comments:
  - subject: "Cool!"
    date: 2008-09-11
    body: "> Today was Big Bang Day at CERN as the worlds largest science experiment was turned on.\nAnything to do with Plasma? (pun absolutely intended ;)) I guess, since they are looking at the structure of an atom, that it does have some loose relation to plasma.\n> We are pleased that like all world class physicists the first ever ATLAS results come from KDE.\nCool!\n> Just as good, the world has not yet been sucked into a black hole.\nEven cooler!"
    author: "Michael \"Black Hole\" Howell"
  - subject: "not Gnome"
    date: 2008-09-11
    body: "I'm sooooooo tempted to post that screenshot on the Gnome boards and go 'SEE!!! - Beat That!!'   hahaha.\n\nI kid, I kid.."
    author: "anonymoose"
  - subject: "Re: not Gnome"
    date: 2008-09-11
    body: "Based on the control center photograph, they actually use a mixed infrastructure, using Windows as well as KDE. Based on this knowledge, they may also use GNOME in parts of their infrastructure.\n\nYes, I read your whole post, including the \"I kid, I kid\" part."
    author: "Michael \"I don't use GNOME\" Howell"
  - subject: "Re: not Gnome"
    date: 2008-09-11
    body: "They could at least equal it if they would try. Maybe GNOME 3.0 in 4 years ;)"
    author: "Bobby"
  - subject: "Re: not Gnome"
    date: 2008-09-12
    body: "hah yes !"
    author: "Euraran"
  - subject: "...But on Windows?"
    date: 2008-09-11
    body: "Isn&#8217;t that an IE7 though on the Impressive Control Centre picture?  The tabs look ridiculously much like that thing."
    author: "Henrik Pauli"
  - subject: "Re: ...But on Windows?"
    date: 2008-09-11
    body: "My Library uses IE7 (I feel ill ;), and yes, that's it. I can tell by the buttons as well as the tabs.\n"
    author: "Michael \"Library\" Howell"
  - subject: "if you need a 5min indroduction in LHC"
    date: 2008-09-11
    body: "http://www.youtube.com/watch?v=j50ZssEojtM\n\nHave Fun\n\n  Thorsten"
    author: "schnebeck"
  - subject: "but but"
    date: 2008-09-11
    body: "Note how they aren't using KDE4 though =P"
    author: "Patcito"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "NVIDIA GPUs? :)"
    author: "fhd"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "Apropos KDE, has anyone gotten a 4.2 update recently? My last update was on the 3rd of this month, since then nothing :("
    author: "Bobby"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "You'll get your KDE 4.2 update next January. In between releases, we do need at least *some* time to actually write the code we're releasing :> "
    author: "sebas"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "Well, I was actually referring to the devel-version that I am using - no updates in the Suse repo for a week now. You know how impatient the bleeding edge people are ;)"
    author: "Bobby"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "Hi Bobby\n\nWe're busy getting the features done for openSUSE 11.1 beta1 (and discussing KDE3 support).  We'll be back on normal service next week.\n"
    author: "Will Stephenson"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "Thanks very much for the reply. I just can't wait for the next update, guess I will have sleepless nights until next week ;)\nBTW, you guys are doing an excellent job, even the present development version (- my nVidia card) is quite stable."
    author: "Bobby"
  - subject: "Re: but but"
    date: 2008-09-11
    body: "\"(and discussing KDE3 support)\"\n\nI vote for KDE3. "
    author: "user"
  - subject: "Bigger pictures"
    date: 2008-09-11
    body: "and showing that they use a mixed environment:\nhttp://atlas.ch/photos/atlas_photos/selected-photos/events/lhc_atlas1.jpg\nhttp://atlas.ch/news/images/stories/lhc_first2.jpg\nhttp://atlas.ch/photos/atlas_photos/selected-photos/events/lhc_atlas2.jpg\n\n"
    author: "Andras Mantia"
  - subject: "Famous last words and throughts .."
    date: 2008-09-11
    body: "... before the world comes to an end:\n\n\"LHC uses LINUX and KDE\" \n\n\"I wish would have been Windows\" (In that case the experiment would have worked)\n\nSCNR ;-)"
    author: "Martin"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-11
    body: "why should the world come to an end? in case you trust the biologist who believes he understands physics better than physicists well i can only recommend that you trust your gardener in case you ever need open heart surgery. not willing to do that? then trust physicists ... the biologist's interpretation of general relativity - by the way - has been proven wrong a LONG time ago. in case you were kidding i'm sorry :-) there've been just too many people complaining for no reason so i wanted to make this one clear. \n\nas for the mixed environment: I had a chance to do a semester thesis at cern and yes they use a lot of linux but also a lot of windows (though vista is incredibly unpopular [quote: \"get that piece of s*** off my computer!]) . the linux versions are often outdated when they're the official releases (scientific linux i think) but a lot of people use different distributions ... judging by the start up sound a LOT of ubuntu... oh and of course my kubuntu laptop :-) [not a representative survey, just personal experience, guys!]"
    author: "Andrea"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-11
    body: "LHC-doomsday may still going on. Proof: http://i38.tinypic.com/2q37yh5.jpg\n"
    author: "Gordon Freeman"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-11
    body: "I have nothing against scientific researches and experiments but I am convinced that these over dimensionally \"smart\" guys are going to blow us all into oblivion sooner or later."
    author: "Bobby"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-12
    body: "No, that part is reserved for politicians using devices the physicists built in a rather stupid way."
    author: "gustl"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-14
    body: "http://www.cracked.com/article_16583_5-scientific-experiments-most-likely-end-world.html\n\n&#8220;Experts assure us that based on everything we know about science, the chances of doom are fairly slim. Experts also say LHC will change everything we know about science.&#8221;\n"
    author: "You Don't Know Jack"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-11
    body: "\"I wish would have been Windows\" (In that case the experiment would have worked)\n\nYou mean the BSOD aka Blackholes :D"
    author: "Bobby"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-12
    body: "Why would using Linux and KDE cause the experiment to create a black hole, whereas Windows would not. Technically, the KDE they are using (KDE3.5.x), with Linux, is more stable than Windows, so if Linux causes the world to end in a failed experiment, it won't be because of a crash.\n"
    author: "Michael \"Huh?\" Howell"
  - subject: "Re: Famous last words and throughts .."
    date: 2008-09-18
    body: "The system startup and setup processes requires months. There was no collision experiment. Particles haven't completed the entire trip in the HLC. \n\n:-)\n\nThe important thing is journalists and catastrophists are convinced the experiment has been made without consequences so they stop that ridicolous campaign.\n\nOr the BH has been created and we are landed all in another continuum?\n\n"
    author: "Thempleton"
  - subject: "Mixed environment"
    date: 2008-09-11
    body: "They use a mixed environment.\n\nA guy who were there said that he saw Windows, Linux with VLC client and Firefox on lightweight desktops, Apple laptops, etc.\n"
    author: "David"
  - subject: "Re: Mixed environment"
    date: 2008-09-11
    body: "FYI\nOficially supported systems are Windows Xp/Vista and the flavorur of linux called SLC (Scientific Linux Cern 4.7) based on RHES 4 - see http://linux.web.cern.ch/linux/"
    author: "Karol"
  - subject: "End of world  (probably not)"
    date: 2008-09-11
    body: "\"Just as good, the world has not yet been sucked into a black hole.\"\n\nPlain daft. The people who are informed on this subject believe that there is a very low probability of creating a black hole. Very low isn't quite low enough, as far as I am concerned, but the reason that this concern, and the reassurance coming from an apparent lack of the end of the world, so far, is misguided is that all they have done so far is to run a few protons around in each direction and check the detectors, largely by looking for background events.\n\nNot only have there been no collisions, so far, but all of the testing has been done at low energies and if low energies were going to end the world, something like LEP or Tevatron would have done it years ago.\n\nThis is a significant milestone in project management terms, but it really isn't grounds for reassurance."
    author: "Markw"
  - subject: "Re: End of world  (probably not)"
    date: 2008-09-11
    body: "Who cares if they create some black holes?  By the same theories, they have been created over and over for a few billion years here on Earth via inbound radiation from space... and will continue to occur regardless of what CERN does.  This just lets scientists know where the interaction will be so they can watch it with carefully calibrated instruments.  There's nothing new here that doesn't occur all over the place in this neck of the galaxy, it's just in a nice building so we can know when and where it will happen and watch it closely."
    author: "Evan \"JabberWokky\" E."
  - subject: "So if the world will not end..."
    date: 2008-09-11
    body: "...we have to thank KDE ;-)"
    author: "Henning"
  - subject: "Re: So if the world will not end..."
    date: 2008-09-11
    body: "Ah! That is surely the ultimate world domination :)"
    author: "bluescarni"
  - subject: "my father"
    date: 2008-09-11
    body: "Hi, I m proud of my father, he is working on ALICE detector in LHC.  But he is using Linux scientific with an old KDE.... KDE 3.1 !!"
    author: "idk"
  - subject: "Java ;)"
    date: 2008-09-12
    body: "I'll disappoint you but its not KDE or Gnome. All computers in Control Center run Windows XP...All these applications you have seen are Java applications integrated to a Netbeans-based platform. They look so ugly cause they use the old AWT toolkit instead of Swing. Cern is a Java world ;)\nI love KDE and I'm Kubuntu user :D"
    author: "Pe"
  - subject: "Re: Java ;)"
    date: 2008-09-12
    body: "How does this explain the Keramic windecs ? Moreover other people seemed to identify Kst on a picture."
    author: "ac"
  - subject: "Re: Java ;)"
    date: 2008-09-12
    body: "Sorry I can't help you. All the computers in Cern Control Center (CCC) run Windows XP, but I remember some people working on CERN Scientific Linux via Remote Desktop. I dont know about ATLAS control room, but for CCC this is the reality. I wish they had CERN Scientific Linux to all computers :)"
    author: "Pe"
  - subject: "Re: Java ;)"
    date: 2008-09-12
    body: "Because Windec has nothing to do with the application, they could be using metacity and still be made in java."
    author: "David"
  - subject: "KIOSK and KDE"
    date: 2008-09-12
    body: "Hi There,\n\nThe machines in the ATLAS Control Room are running CERN Scientific Linux 4, and a KDE session. The policy is that the whole ATLAS Control Room runs Linux exclusively.\nThey use KIOSK to trim the desktop to their needs. There are many Java applications, (including the ATLANTIS event display) . The \"slow control\" system (that \"steers\" controls/monitors the detector) is built on top of the commercial, Qt3 (KDE3)-based Scada System, called PVSS (produced by Austrian company called ETM), and can run on Linux and Windows. Certain applications that need to execute on Windows (such as OPC-based drivers for various hardware pieces), run on ... Windows; they are available via Remote Desktop. \n"
    author: "piters"
  - subject: "Re: Java ;)"
    date: 2008-09-12
    body: "Yeah, Windows desktops are being used for the wishy-washy stuff while the heavy duty serious processing is done on Linux."
    author: "Abe"
  - subject: "France too!"
    date: 2008-09-12
    body: "\"the Large Hadron Collider in Switzerland.\"\n\nIts on the border between France and Switzerland. Most of it is in France.\n\nJust being picky."
    author: "Jean"
  - subject: "CERN YaY!"
    date: 2008-09-17
    body: "I actually went to CERN as part of a school trip this year, it was awesome. All the scientists were running a KDE/Gentoo cocktail on their laptops."
    author: "Zack"
  - subject: "KDE"
    date: 2008-09-29
    body: "YUP, I also run a KDE kinda cocktail on my machines here at home.\n\nI even use Kubuntu on one of them the other uses Slackware with KDE 3.5\nand the little EEE uses Mandriva with KDE. Seems that Mandriva works\nright outa de box on the EEE. None other works as good.. Suse on the other hand\nwell do I need to say., It has issues!!!\n"
    author: "Andy Hoffman"
  - subject: "Awesome, so its still"
    date: 2009-06-01
    body: "Awesome, so its still workable then. Perfect going on. Lets see now whats next we have to check <a href=\"http://www.envisionwebhosting.com\">web hosting</a> for this project. Any recommendation?\r\n"
    author: "linda.coult"
  - subject: "Expected result of large hardon collider"
    date: 2009-06-16
    body: "Once the supercollider is up and running, CERN scientists estimate that if the Standard Model is correct, a single Higgs boson may be produced every few hours. At this rate, it may take up to three years to collect enough data to discover the Higgs boson (<a href=\"http://www.1-hit.com\">search engine optimization</a>) unambiguously. Similarly, it may take one year or more before sufficient results concerning supersymmetric particles have been gathered to draw meaningful conclusions.\r\nJohn <a href=\"http://www.1-hit.com/web-hosting.htm\">web hosting</a>\r\n"
    author: "johnsmith.smith"
---
Today was Big Bang Day at CERN as the world's largest science experiment was turned on.  Like all good technology enthusiasts the KDE developers have been keeping up with the progress of the Large Hadron Collider in Switzerland.  We are pleased to see that like all <a href="http://edu.kde.org/step/">world class physicists</a>  the <a href="http://www.bbc.co.uk/radio4/bigbang/gallery.shtml?select=13">first ever ATLAS results</a> come from KDE.  Their <a href="http://www.spiegel.de/fotostrecke/fotostrecke-35141-3.html">impressive control centre</a> is also <a href="http://www.spiegel.de/fotostrecke/fotostrecke-35141-5.html">making excellent use of KNotes</a>.  Just as good, the world has not yet been sucked into a black hole.




<!--break-->
