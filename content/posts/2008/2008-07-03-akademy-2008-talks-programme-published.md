---
title: "Akademy 2008 Talks Programme Published"
date:    2008-07-03
authors:
  - "jriddell"
slug:    akademy-2008-talks-programme-published
comments:
  - subject: "any kde on windows talk"
    date: 2008-07-03
    body: "just wondering, i  don't see any talk about ms windows or mac os port of kde ? am i missing something ;)"
    author: "mimoune djouallah"
  - subject: "Re: any kde on windows talk"
    date: 2008-07-03
    body: "There were introduced at talks last year. I'm sure there will be some BoF sessions of varying degrees of formality."
    author: "Ian Monroe"
  - subject: "Re: any kde on windows talk"
    date: 2008-07-03
    body: "This is a conference for kde developers, much less for kde users.  I'm thinking that that subject would go better at the linuxTags, cebits and other bigger events."
    author: "Thomas Zander"
  - subject: "Re: any kde on windows talk"
    date: 2008-07-04
    body: "No that's not true. KDE/Windows & Mac developemnt, portability, is about developers just like KDE/Solaris is, and embedded. IIRC, organizers were just unable to find free slot this year. There will be at least one BoF devoted to portability though.\n"
    author: "js"
  - subject: "Re: any kde on windows talk"
    date: 2008-07-04
    body: "yes.\n\npersonally, i'm hoping we have less of \"stick the mac devs over there\" and \"keep the windows devs over in that corner\" and more of \"all devs mingling together\". it's how we've handled linux, bsd, solaris and other unixes both currently and in the past, so the maturation of the new platforms will probably result in much the same."
    author: "Aaron Seigo"
  - subject: "Re: any kde on windows talk"
    date: 2008-07-04
    body: "I remember drinking (too much of something) with some of the Windows guys, and most of the issues that affect the Solaris port (gcc-isms, Linux-/proc-isms) affect the Windows port as well, so we have a lot in common. Other groupings would be \"OSsen with ZFS\" or \"people without proprietary drivers\" and such: there's much more in common between the developer and porting groups than there is difference.\n\nI dunno .. I might bring my MacBook to see what happens, but I'll probably be busy with the other porting groups and compiler tools."
    author: "Adriaan de Groot"
---
<a href="http://akademy.kde.org/">Akademy 2008</a> has published the <a href="http://akademy.kde.org/conference/program.php">programme of talks</a>. Track themes include research, applications and community. There are keynotes from Frank Karlitschek of the <a href="http://www.opendesktop.org/">Open Desktop sites</a>, Sebastian Nyström of <a href="http://trolltech.com/company/about/management">Nokia</a>, Cliff Schmidt of KDE users <a href="http://literacybridge.org/">Literacy Bridge</a>.  Lightning talk sessions include a Plasma Frenzy and an Akonadi Rumble with <a href="http://www.kdab.net/">KDAB</a>'s Till Adam.  It is a packed programme to start off the week, the rest of which will be filled with BoFs, Tutorials and an <a href="http://akademy.kde.org/events/emmobile.php">Embedded and Mobile Day</a>.  <a href="https://akademy.kde.org/registration/user/register">Register now</a>.


<!--break-->
