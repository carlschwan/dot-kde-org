---
title: "Qt 4.4 Released"
date:    2008-05-06
authors:
  - "jriddell"
slug:    qt-44-released
comments:
  - subject: "mov?"
    date: 2008-05-06
    body: "Releasing on Qt 4.4 is a great news. This is a great toolkit and now it become ever better. \n\nBut what the hell?! Toolkit intended to work on all current major computer platform have released with a demo video, encoded in \u0093mov\u0094 which require proprietary and unavailable for many platforms browser plugin and player. Really, the name of toolkit is not a reason to use video format of the same name."
    author: "dmiceman"
  - subject: "Re: mov?"
    date: 2008-05-06
    body: "It's the eternal disconnect between engineering and marketing. But at least Quicktime is more portable than Flash, the other common alternative."
    author: "David Johnson"
  - subject: "Re: mov?"
    date: 2008-05-06
    body: "Sorry but the .flv of the flash streams can be downloaded with some firefox extensions and viewer under VLC which is insanely portable. I'd rather have a flash applet than getting the unstable embedded video player to work btw. Embedded video players under linux sucks. "
    author: "Anon"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "Please do not speak solely from your own experience when advocating decisions that affect many people.  There isn't even such a thing as a functional Linux flash player.  There's a binary plugin which happens to run on a few kinds of linux, and some open source attempts which barely run.  MPEG4, DivX, some kinds of MOV, some kinds of AVI, Ogg Theora, a simple slideshow... all would be more portable."
    author: "Lee"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "I don't agree. I've tried Adobe flashplayer on several Linux distributions and it worked fine on every one of them. The only problem I've had was with a 64-bit install. Even with that I've got it working with a plugin (I believe it called nspluginwrapper in Debian) which makes 32-bit flashplayer work with 64-bit firefox.\n\nAnd about a year ago I tried gnash and gave up on it because it wasn't stable enough. But I've recently tried the newest version and it's getting pretty decent; I'm using that now instead of flashplayer.\n"
    author: "Jeff Strehlow"
  - subject: "Re: mov?"
    date: 2008-05-08
    body: "Okay, so you made it run on ONE architecture. Not even on an architecture that is really very similar to x86 -> x86_64\n\nAnd you still call it portable? Please look up the word."
    author: "fhd"
  - subject: "Re: mov?"
    date: 2008-05-08
    body: "You're putting words in my mouth. The only thing I said was that there are 2 flash players to choose from in Linux. "
    author: "Jeff Strehlow"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "Please do not speak solely from your own experience when advocating decisions that affect many people. An Ogg Theora video wouldn't play out of the box on Windows and OSX machines.\n\nSCNR"
    author: "AC"
  - subject: "Re: mov?"
    date: 2008-05-08
    body: "You only have to install a plugin:\nhttp://www.illiminable.com/ogg/ (DirectShow/WMP)\nhttp://xiph.org/quicktime/ (QuickTime)\nso it isn't any harder to get to work than Flash on those broken OSes. And good OSes support Theora out of the box. :-)"
    author: "Kevin Kofler"
  - subject: "Re: mov?"
    date: 2008-05-09
    body: "A mov file won't play out of the box on Windows either.."
    author: "Greg"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "Flash does not work under unapproved systems. That includes FreeBSD, OpenSolaris, and many others. I have not had good luck extracting .flv out of streams. But I've never had a problem with Quicktime on any platform."
    author: "David Johnson"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "I never watch Quicktime movies with a proprietary player.  Install a codec pack and then watch a .mov file in any player you want."
    author: "T. J. Brumfield"
  - subject: "Re: mov?"
    date: 2008-05-10
    body: "Yes, that works OK for me.\n\nHOWEVER, it only works on an x86 system!"
    author: "JRT"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "++1\n\nCan't see the movie on my machine either. ALL codecs installed... Opensuse10.3 btw. Agree with VLC/Flashvideos, AVI and others. Even RealVideo would have been better since it is at least officially supported on Linux platforms."
    author: "Sebastian"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "Please everybody get your facts straight!\nThe video is h264+aac+mov. These formats are partly covered by patents, but they ARE documented. It is absolutely possible to play the video with free software (no extra binary codecs required), mplayer and xine should play it out-of-the-box.\n\nWhile I would have preffered an entirely free codec-combination like ogg, this is way better than flash, real media or windows media. Even the download link is provided...\n\nBtw. kmplayer plays the video fine inside konqueror, if your browser doesnt play embedded video, well..."
    author: "Hannes Hauswedell"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "With all the video formats on the web, the problem is not the \"video format\" part (even though it is a strange twist of fate that FOSS people would ask for flash+flv over h264+aac+mov), it is the \"on the web\" part.\n\nI have yet to see an embeded player (Free or proprietary, any platform) that's really nice to use and works all the time, whereas some desktop players are really good (and most will play any file you throw at them).\n\nConclusion ? Please please please allways provide a download link for the video. For sites like Youtube it's a business decision not to provide one, but for promotional material like this it doesn't make any sense. If the embeded player works fine for some people, that's cool. For all the others (technical problems, slow connection, repeated views, file sharing...), all we need is a download link and we'll be happy.\n"
    author: "moltonel"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "You did realise that there is a download link on the bottom of the page?\n\n;)\n\nLike I said, I use kmplayer embedded in konqueror. I have no w32codecs installed and I've never encountered a video that doesnt play with the free software codecs mplayer has.\nThen again, I did build mplayer with support for \"patented-but-free\" codecs."
    author: "Hannes Hauswedell"
  - subject: "Re: mov?"
    date: 2008-05-07
    body: "dmiceman,\n\nofftopic question: are you the fusecram/fuseiso guy?"
    author: "kt"
  - subject: "It's fantastic!"
    date: 2008-05-06
    body: "I'm thrilled by the release!!!\nDo anyboy know how much the Qt codebase grew after adding all the stuff it's been added since Qt 4.3?? Did it double??\n\nWonderful, Trolltech's unstoppable ;-) They're very inspiring to me!\n"
    author: "superfan!"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "i had to use windoze to view this video and why do i see a mac pc and not linux??"
    author: "Darwin Reynoso"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "Because Henrik (the author of the video) uses Mac as his main platform. Really, Qt is a cross-platform toolkit, he could have shown the demos in any platform.\n\nHave you seen the video on the demos on Embedded Linux and Windows CE?"
    author: "Thiago Macieira"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "so... any youtube link?"
    author: "Leiteiro"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "you can see it on linux with firefox/mplayerplug-in/mplayer"
    author: "Raul"
  - subject: "Re: It's fantastic!"
    date: 2008-05-09
    body: "I think it depends on the Linux distribution. I have all those components on my Debian Lenny install and it doesn't work."
    author: "Jeff Strehlow"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "mplayer plus some codec packages (maybe not all 100% Free though) can play almost any format, including the format used for this video - I watched it. Don't waste your time with the crappy mplayer GUI though :)"
    author: "Andreas"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "Dude, you just said \"windoze\" you are so cool"
    author: "Div"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "Some inexact statistics (not including the file headers):\n 11216 files changed, 1813024 insertions(+), 267102 deletions(-)\n\nBy major top-level dir:\ndoc:  1419 files changed, 60218 insertions(+), 18934 deletions(-)\nmkspecs:  126 files changed, 987 insertions(+), 710 deletions(-)\nqmake:  46 files changed, 781 insertions(+), 149 deletions(-)\ndemos:  234 files changed, 21251 insertions(+), 1515 deletions(-)\nexamples:  1040 files changed, 27239 insertions(+), 7130 deletions(-)\nsrc:  6603 files changed, 1479067 insertions(+), 142459 deletions(-)\ntools:  1615 files changed, 125752 insertions(+), 67467 deletions(-)\n\nBreakdown in src/\n\n3rdparty:  3694 files changed, 1147659 insertions(+), 31439 deletions(-)\ncorelib:  377 files changed, 48677 insertions(+), 16247 deletions(-)\ndbus:  57 files changed, 1324 insertions(+), 861 deletions(-)\ngui:  1011 files changed, 91603 insertions(+), 34369 deletions(-)\nnetwork:  113 files changed, 15753 insertions(+), 972 deletions(-)\nopengl:  35 files changed, 2306 insertions(+), 715 deletions(-)\nplugins:  150 files changed, 7223 insertions(+), 401 deletions(-)\nqt3support:  212 files changed, 1280 insertions(+), 1521 deletions(-)\nscript:  115 files changed, 8705 insertions(+), 2664 deletions(-)\nsql:  52 files changed, 1956 insertions(+), 817 deletions(-)\nsvg:  23 files changed, 860 insertions(+), 594 deletions(-)\ntestlib:  33 files changed, 346 insertions(+), 257 deletions(-)\ntools:  75 files changed, 2725 insertions(+), 1154 deletions(-)\nxml:  16 files changed, 218 insertions(+), 7910 deletions(-)\nxmlpatterns:  543 files changed, 84675 insertions(+), 0 deletions(-)\n\nBreakdown in tools/\n\nassistant:  319 files changed, 32400 insertions(+), 6538 deletions(-)\nconfigure:  6 files changed, 824 insertions(+), 166 deletions(-)\ndesigner:  650 files changed, 39072 insertions(+), 29459 deletions(-)\nlinguist:  106 files changed, 6889 insertions(+), 5584 deletions(-)\npixeltool:  5 files changed, 36 insertions(+), 27 deletions(-)\nporting:  65 files changed, 372 insertions(+), 15 deletions(-)\nqdoc3:  112 files changed, 5370 insertions(+), 2205 deletions(-)\nqtconcurrent:  7 files changed, 760 insertions(+), 0 deletions(-)\nqtconfig:  20 files changed, 447 insertions(+), 110 deletions(-)\nxmlpatterns:  11 files changed, 2379 insertions(+), 0 deletions(-)\n"
    author: "Thiago Macieira"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "To summarize: \"3rd party\" includes Webkit which is a large piece of code."
    author: "Ian Monroe"
  - subject: "Re: It's fantastic!"
    date: 2008-05-08
    body: "Phonon is also in 3rdparty, so those added files are not all WebKit."
    author: "Kevin Kofler"
  - subject: "Re: It's fantastic!"
    date: 2008-05-08
    body: "Phonon is a drop in the bucket compared to WebKit (as far as LOC), let's be honest. :)"
    author: "Ian Monroe"
  - subject: "Re: It's fantastic!"
    date: 2008-05-06
    body: "For those chasing KDE SVN, 4.4.0 is in qt-copy since last night. :)"
    author: "sebas"
  - subject: "One important thing omitted in the list of changes"
    date: 2008-05-06
    body: "...is that Qt 4.4 draws things _faster_ than Qt 4.3.4!\n\nI recently ported Gambas GUI component to Qt 4.3.4. On my Core Duo with Linux (Mandriva 2008.1) and a NVIDIA 8300 GS graphic card, the Gambas IDE is almost unusable. Drawing and resizing are as slow as GTK+!\n\nThen I downloaded Qt 4.4 RC1, and compare the demos and examples: at first glance, drawing is now twice faster, maybe faster than Qt 3. But using OpenGL for drawing does not work at all.\n\nOf course, these are completely subjective tests.\n\nBut now I am happy, I will be able to finally use Qt 4 with Gambas! :-)\n\n"
    author: "Benoit Minisini"
  - subject: "Re: One important thing omitted in the list of changes"
    date: 2008-05-07
    body: "I believe that Qt slowness is derived partially from your nvidia card. People with certain Intel cards did not seem to have problems in Qt 4.3 ...\n\nso basically it means that Qt 4.4 is fast in spite of Nvidia?"
    author: "txf"
  - subject: "Re: One important thing omitted in the list of cha"
    date: 2008-05-07
    body: "My guess is that credit goes to \"Alien\": now Qt applications appear to X as one big widget, so like resizing operations especially should be faster."
    author: "Ian Monroe"
  - subject: "Mozilla dudes with Qt?"
    date: 2008-05-07
    body: "http://blog.vlad1.com/2008/05/06/well-isnt-that-qt/\n\nInteresting, if this really happens, then we will have Qt browser with Gecko engine! I still prefer KHTML though, but some sites really force me to use Firefox."
    author: "fred"
  - subject: "Re: Mozilla dudes with Qt?"
    date: 2008-05-07
    body: "hehe, i was about to post the same link, it is a great news, and it looks like Nokia is pushing it, so with a real browser with qt, now kde is complete ;)as far as i love and use konqueror, i want just a browser not a super browser+file manager+ file viewers.\n\n\nthanks mozilla and nokia "
    author: "mimoune djouallah"
  - subject: "Re: Mozilla dudes with Qt?"
    date: 2008-05-07
    body: "This is really good news: :-)\n\n\"Overall, though, what's on the Qt branch now works pretty well under X11;\"\n\n\"I hope that in the future that the Qt port will be a toplevel supported Gecko port, alongside gtk2, Win32, and OSX\"\n"
    author: "Yves"
  - subject: "Incredible News!"
    date: 2008-05-07
    body: "I can't wait to try and build it!"
    author: "T. J. Brumfield"
  - subject: "Hip hip..."
    date: 2008-05-07
    body: ":)\n\nExcellent! Good job, Trolltech, we are with you!  "
    author: "Whoever"
  - subject: "Dashboard applets?"
    date: 2008-05-07
    body: "Maybe this is slightly off topic but does anybody know when I might expect to see the option to use mac dashboard applets in plasma. I saw it in a blog entry a couple months ago and have been waiting for it since. If I remember correctly, using them relied on some changes in Qt that weren't in qt-copy. Now that the final Qt 4.4 has been released, is that possible?"
    author: "Aaron"
  - subject: "Re: Dashboard applets?"
    date: 2008-05-07
    body: "It's possible now, and will be in KDE 4.1 ;-)"
    author: "jos poortvliet"
  - subject: "digg it!"
    date: 2008-05-07
    body: "It's on digg now: http://digg.com/programming/Qt_4_4_Framework_Broadens_Rich_Application_Development\n!!!\n"
    author: "superfan!"
---
Trolltech have <a href="http://trolltech.com/products/qt/learnmore/whats-new">released Qt 4.4</a>.  This is a major release with many new features including WebKit, KDE's Phonon, Concurrency, Widgets on the Canvas and XQuery.  This <a href="http://trolltech.com/products/qt/learnmore/demo/qt44-features-video">video covers what's new</a>.  Ars Technica has an <a href="http://arstechnica.com/reviews/other/troll-treasure-qt44-in-depth.ars">in depth look</a> while release dude Thiago has <a href="http://labs.trolltech.com/blogs/2008/05/06/qt-440-fully-released/">blogged with the now traditional developers' group photo</a>.  




<!--break-->
