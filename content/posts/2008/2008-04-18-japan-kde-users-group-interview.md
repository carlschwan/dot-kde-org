---
title: "Japan KDE Users Group Interview"
date:    2008-04-18
authors:
  - "jriddell"
slug:    japan-kde-users-group-interview
comments:
  - subject: "KDE in the East"
    date: 2008-04-18
    body: "Because of the language barrier, KDE needs the support of people like these guys if we want to be used as an alternative to windows in non-english speaking countries.\n\nGanbatte Kudasai, KDE Japan! "
    author: "Ask"
  - subject: "Re: KDE in the East"
    date: 2008-04-18
    body: "Absolute true! Greetings and thanks to the Japan users group :)\n\np.s. interesting screenshot that shows the beauty of the japanese typeface combined with the beauty of KDE: http://www.kde.gr.jp/img/snapshots/snapshot03.jpg\n"
    author: "Sebastian Sauer"
  - subject: "Re: KDE in the East"
    date: 2008-04-18
    body: "nice the screenie, but what about a kde4 screenshot in japanese?\n\nthe website http://www.kde.gr.jp also has a really nice retro look!\n\n"
    author: "cies breijs"
  - subject: "Multilingual KDE Community"
    date: 2008-04-18
    body: "What support is there (e.g. on sites like bugs.kde.org and other central stuff) for multilingual use or translation.  It just occurs to me following on from the BugSquad shout out that we're effectively limiting participation to people who can speak English which excludes a lot.\n\nDo we have multilingual support on wikis etc?\n\nJust curious and not finger-pointing: I only speaka da inglis."
    author: "Martin Fitzpatrick"
  - subject: "Re: Multilingual KDE Community"
    date: 2008-04-19
    body: "see upper part  of http://techbase.kde.org"
    author: "Nick Shaforostoff"
  - subject: "Really great to hear..."
    date: 2008-04-18
    body: "It's really great to hear about how open source stands in countries from Asia. I was wondering about it for quite some time, especially about China and Japan.\nI was assuming there are problems because of language support / internationalization, but it seems things are starting to improve now.\n\nGreat! Thanks for posting this!"
    author: "Dread Knight"
  - subject: "Go, snow girl, go"
    date: 2008-04-18
    body: "It's hard to believe that one can talk about the japanese translation without mentioning Yukikosan's work.\n\nCommits in trunk/l10n-kde4/ja/messages: 1249\nYukiko: 665\nscripty: 418\npino: 49\ncoolo: 46\naacid: 27\nokushi: 14\nmlaurent: 11\nilic: 9\ndfaure: 5\nalam: 1\ncconnel: 1\nshaforo: 1\n\nI know. The KDE Community in Japan does more than \"just\" translation work, but talking about a \"translation community\" is a bit exaggerated.\n\nA big arigato gozaimasu to the japanese KDE folks who make a desktop possible I will hopefully use in a year or so. :D\n\ncheers"
    author: "icwiener"
  - subject: "Re: Go, snow girl, go"
    date: 2008-04-18
    body: "Perhaps they also upload for other people that doesn't have svn accounts."
    author: "Pascal"
  - subject: "Re: Go, snow girl, go"
    date: 2008-04-18
    body: "That's actually very often the case also in other languages. Many translators don't have a svn account."
    author: "jospoortvliet"
  - subject: "Re: Go, snow girl, go"
    date: 2008-04-19
    body: "In that list, the real Japanese translators are Jukiko and okushi (if I'm not mistaken).\n\nAll the other people (me included) just do \"service work\" in the translation trees, eg moving translations, removing old ones, etc.\nOh, and there's also Scripty... but it's just a script :)"
    author: "Pino Toscano"
  - subject: "Kudos to the pioneers"
    date: 2008-04-19
    body: "What one shouldn't forget is that these ppl don't have a huge community to build on. \"Standing on the shoulders of giants\" is something that doesn't really account to free software developers in a lot of parts of the world. If you have no developer/user base you are on your own. For development on KDE look at Danny's map for example and you'll see a lot of huge countries still being at 0% of SVN commits.\n\nNow imagine you need to develop a character input system first, before you can even enter your non-alphabetic text, or have to work with a gettext system that knows about plural forms, though your own language might not even have a form for plurals but a lot of other stuff that's actually not covered.\n\nSo, all I can say is kudos to these pioneers and hopefully they can get more people to work with KDE in their county. Only then KDE can get a multicultural system and really stand face-to-face to the other competitors.\n"
    author: "Chris"
  - subject: "Amazing"
    date: 2008-04-19
    body: "Tetraphobia:\n\nhttp://en.wikipedia.org/wiki/Tetraphobia\n\nNever ever heard about this before. Interesting read. This shows what unexpected pitfalls there can be when marketing a product globally. Nevertheless IMO the tolerance to \"strange\" behaviour is quite high if I know someone is from a different country. For instance often at trade fairs I find people from the US starting a conversation with me extremely informally \"Hi, how you're doing...\", standing very close in front of me etc - as if I already knew them. If a fellow German I don't yet know would come to me saying \"Na, wie gehts?\" or sth. like that I would consider this a bit unpolite but I immediately associate this to the different background so after initial confusion I smile about this and don't really care. Probably they are laughing about me as well because I'm so uptight ;-) All those so-called experts warning you about things you shouldnt do are often missing this point IMO. What is rude from a local can be a bit strange but not so bad at all from a foreigner. So, I hope Japanese will not be too angry at possible tourists appearing with a #4 on their shirt."
    author: "Michael"
  - subject: "Re: Amazing"
    date: 2008-04-21
    body: "Yea, US English doesn't get more formal then 'Hi, how are you doing\" without sounding silly.\n\nWell cultural stuff is always fairly easy to figure out in person. Marketing is different, so the 4 question was fair enough."
    author: "Ian Monroe"
---
Despite their prominent position in the world as leaders of technology, we hear from oriental countries quite rarely in the free software world.  To find out what happens to KDE in the East, we asked some questions to <a href="http://www.kde.gr.jp/~daisuke/profile_en.html">Daisuke Kameda</a> (&#20096;&#30000; &#22823;&#36628;) of the <a href="http://www.kde.gr.jp/">KDE Japan Users Group</a>.








<!--break-->
<div style="border:thin solid grey; padding: 1ex; margin: 1ex; float: right;">
<img src="http://static.kdenews.org/jr/daisuke-kameda.jpg" width="169" height="252" /><br />
Daisuke Kameda (&#20096;&#30000; &#22823;&#36628;)
</div>

<h4>How did the Japanese KDE Users Group start?</h4>

<p>It had its beginning when Junji Takagi made a patch for operating multibyte characters such as Japanese, when Qt's version 1.x was released. Qt 1.x couldn't operate mulitibyte characters, so we couldn't use Japanese in KDE 1.x previously.</p>

<p>After the patch was made, The Japan KDE Users' Group was launched for aggregating the information about KDE/Qt in Japan.</p>

<h4>What does the Japanese KDE Users Group do?</h4>

<p>We do some activities such as message translation to Japanese, making patch for multilingualisation, exchanging information about KDE/Qt.</p>

<p>But, the necessity of having multilingualisation patches is less now.</p>

<h4>How many people are part of the group?</h4>

<p>There are 1000 mailing list subscribers. About 15 member is active staff.</p>

<h4>Why do we rarely hear from Japan in the free software world?</h4>

<p>Probably, there are some reason.</p>

<p>First of all, free software is used frequently, but few people participate in development of free software in Japan. For example, many company use free software, but few companies contribute free software.</p>

<p>Language difference is also big reason. Since Japanese population and community are large, if the developer use only Japanese and stay in Japan, he can be perfectly satisfied.</p>

<h4>Does our <a href="http://en.wikipedia.org/wiki/4_(number)#In_other_fields">4 version number</a> cause superstitious problems in Japan?</h4>

<p>It is not necessary to care. Many Japanese software was also released version 4.</p>

<h4>Are there any KDE developers in Japan and how can we get more?</h4>

<p>Unfortunately, there are few active KDE developers.</p>

<p>We are also looking for the developers who are interested in KDE. Possibly, we can use Google's Summer of Code for getting more.</p>

<h4>How active is the translation community for KDE in Japan?</h4>

<p>The translation community is very active now :) Message translation is almost 100% complete. But, we need more translators to translate documentation.</p>

<p>And, increasing translators is always welcomed.</p>
