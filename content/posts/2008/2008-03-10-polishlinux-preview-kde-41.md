---
title: "PolishLinux Preview KDE 4.1"
date:    2008-03-10
authors:
  - "Oscar"
slug:    polishlinux-preview-kde-41
---
The makers of PolishLinux have made a nice <a href="http://polishlinux.org/kde/kde-41-visual-changelog-rev-783000/">preview of the state of KDE 4.1</a>.  "<em>You don't always see this in the official changelogs but the KDE 4 development is progressing in an extraordinary speed.</em>"  They also have <a href="http://polishlinux.org/kde/kde-4-tour-digikam-010/">a look at Digikam's KDE 4 port</a>, including its nifty new timeline and Google maps integration.





<!--break-->
