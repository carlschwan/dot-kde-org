---
title: "KDE Commit-Digest for 1st June 2008"
date:    2008-07-03
authors:
  - "dallen"
slug:    kde-commit-digest-1st-june-2008
comments:
  - subject: "Link wrong"
    date: 2008-07-03
    body: "Danny, clicking on \"this week's KDE Commit-Digest\" takes me straight to the subdirectory, not the HTML page.\n\nCan you please fix?  Thanks, mate."
    author: "Kerr Avon"
  - subject: "Re: Link wrong"
    date: 2008-07-03
    body: "http://commit-digest.org/issues/2008-06-01/index3.php"
    author: "LD"
  - subject: "Re: Link wrong"
    date: 2008-07-03
    body: "They should rename index3.html to index.html."
    author: "yman"
  - subject: "Re: Link wrong"
    date: 2008-07-03
    body: "Fixed, thanks.\n\nThat's what I get for working past midnight!\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Link wrong"
    date: 2008-07-03
    body: "Thread hijacking : this thread is now a \"Thank you Danny thread\"\n\nThank you Danny :)"
    author: "DanaKil"
  - subject: "Amarok video player"
    date: 2008-07-03
    body: "\"Amarok 2 gets basic video playing support\" - Yay! I sure hope this means video organizing and management, since those are features I would really like from a media player, and I really don't know where to find one that provides them."
    author: "yman"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "I don't think that's going to happen. The Amarok blog went into more detail about this new development, and made it pretty clear that Amarok wasn't going to become a video player. http://amarok.kde.org/blog/archives/664-Amarok-2-Gets-Music-Video-Support!.html"
    author: "maniacmusician"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "Aren't you the guy from the Ubuntu Forums Ambassadors project (or whatever it was called). I think Ubuntu Brainstorm made that obsolete.\n\nAnyway, thanks for the link. Can you recommend any KDE media player that would allow me to organize my video library? I looked at the Kaffeine web site, and it doesn't seem to be what I want.\n\nRight now I'm working my butt off for an importer of consumer electronics just so I to buy a laptop on which I'd install KDE4 (yeah, that's how much I crave it). After every day my hands and back hurt like hell. Incidentally, all the products I've seen are officially compatible with Windows, Mac OSX 10.3 or later, and Linux 2.6 or later (there is a row of logos of Windows Mac and Tux where it says it). The only bad thing I've seen is that the PMPs don't support OGG, though they do play MP3, WMA, and WAV."
    author: "yman"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "Yes, I'm one of the people behind the Ubuntu Forum Ambassadors project. And yes, Brainstorm has efficiently accomplished some of the goals that we originally set out with, though not all of them. To be honest, the FA team suffered from a lack of cohesion amongst group members, a passive mindset, and especially a lack of time on the parts of the most active members. A couple of the members have been throwing around ideas to re-focus our goals and re-structure the team, but a lack of time is once again getting in the way. Might happen in the fall [shrug]\n\nAs for video library management applications, sorry; I'm not aware of any competent KDE app that can do this. I've heard and read (but not experienced firsthand) that Banshee and Beep-media-player (the experimental branch) are the ones pioneering this category. If I remember correctly, both of them recently had new releases as well, so unless you're a purist about your DE, they would probably be good to at least look at. You could also try browsing around at kde-apps.org\n\nAnd it's true that most PMPs don't support OGG, but there are options. I'm not sure what kind of price range or storage capacity you're looking for, but I know that at least Cowon has some good Linux-compatible players that support open formats. The A3 comes in 30GB and 80GB (possibly others -- not sure), and the new Q5W has a 40GB model. They're pricey, but I don't think they're too far above what most reliable PMPs cost."
    author: "maniacmusician"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "The Meizu miniplayer (M6) is a nice one as well. It's flash based, comes in 2, 4 and 8GB models if I remember correctly and it supports MP3, OGG and APE amongst others. Also plays video. I got one and really like it. Better than an iPod Nano. :)\n\nThere's also the Sandisk Sansa Clip, which is more of an iPod shuffle competitor. Also plays OGG audio."
    author: "parena"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "Another option might be an iPod with rockbox installed (http://www.rockbox.org), which is what I use.\n\nThe same software also works on a bunch of players made by several other manufacturers.\n"
    author: "Quintesse"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "I personally use a Sansa e200v1 with RockBox. RockBox rocks!"
    author: "Riddle"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "From the Banshee web site:\n\n\"This release brings a much sought after feature for Banshee - Video management and integrated playback! Your Video Library supports searching, playlists, smart playlists, queuing, and even bookmarks, just like your Music Library.\n\nThe Music and Video libraries are separated in the interface to provide a clean separation of content, so video will never be mixed in with music - when browsing or searching.\"\n\nThat sounds like it's going in the right direction for me, but unfortunately the latest version in the Ubuntu repositories is 0.13, and I, as a rule, only install from the repositories in order to keep life simple when it comes to software management. There are exceptions, but this doesn't warrant one.\n\nI don't know where to find Beep-media-player, except if that's BMPx, in which case I already tried it out and it's not what I'm looking for. I'll try looking at kde-apps.org.\n\nRight now I'm using GNOME, so I don't think you can call me a DE purist :-)\n\nWhat I have in mind isn't to different from a music player. For video there should simply not be an artist directory so that albums go directly into the Video/ directory. Example:\nVideo/Harry Potter/1 - Harry Potter and the Order of the Phoenix.ogg\nFor single, the album can be named after the single, like this:\nVideo/Man on Fire/1 - Man on Fire.ogg\nNotice that it includes the # before the name. That's because this idea assumes that a single might become a series.\nOr the single can be dumped into the root of the video directly, like this:\nVideo/Man on Fire.ogg\n\nAlso, there needs to be an option when importing to not use the filename. This way, if an entire TV series has no names for specific episodes, you can just have them numbered. Same for podcasts and other such stuff. This could also help the lazy if they need to manually input a ton of missing names and don't feel like it.\n\nAnother feature, which would require a video manager, is the ability the define multiple video as multiple parts of the same video. This way, if I, for example, watch a long video on YouTube and decide to save it permanently, I can use my video manager to watch all 5 or so parts as one, and never have to be aware of there being multiple parts after the initial import.\n\nAnyway,those are what I think the basic features of a video manager/player would be.\n\nIf someone would be willing to mentor me in implementing this, I'm willing to try in my spare time, although I don't have all that much of it.\n\nAs for PMPs, those are only an aside about my job. I don't need one, and hardly have any music. Video is my preferred form of entertainment, and that doesn't fit too well on a 2.5\" screen that costs more than it's worth."
    author: "yman"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "KPlayer might, maybe, be what I want. I'll have to check it out, but I don't feel like installing KDE4 on my GNOME desktop, so I'll wait until I buy that laptop I'm saving for."
    author: "yman"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "Forgot to add; you might also want to consider managing your video library with the file management tools that come with KDE4. I think a combination of tags and the enhanced search abilities (such as Virtual Folders) ought to get the job done nicely. I haven't put it into practice yet, but that's what I eventually plan on doing for managing my own video library."
    author: "maniacmusician"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "Is there a GUI frontent for strigi in the works? It sounds like the foundations are there, but apart from virtual folders I can't find an easy way to actually search for tags or comments I add."
    author: "Sepp"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "I am glad to hear that. Amarok should be the best music player, nothing more."
    author: "Tiger"
  - subject: "Re: Amarok video player"
    date: 2008-07-04
    body: "I don't think the interface should be redesigned, or the app refactored largely to cater video support, however as it has been mentioned by other people, many video players are just that.  They lack the library functions and rich feature sets that Amarok provides for audio.\n\nIn that regard, I'd happy to Amarok get some video support.  I've also read the developers are not intending to write a fully featured video-player (with bookmarks, DVD menu support, etc).\n\nHere is a crazy question.  How difficult would it be to take another QT/KDE-based video player and reuse a lot of that code?"
    author: "T. J. Brumfield"
  - subject: "Re: Amarok video player"
    date: 2008-07-04
    body: "I would like to do that, if someone would mentor me. I'm already planning how it should work."
    author: "yman"
  - subject: "Re: Amarok video player"
    date: 2008-07-04
    body: "If you feel like you have enough knowledge of the Qt and KDE APIs to get yourself started, then it would definitely be a worthwhile endeavor. \n\nBy \"planning,\" do you mean planning the program structure after taking a look at the Amarok code, or do you mean planning a feature list/roadmap? Of course, it would be the former that would be more useful. \n\nI would check out the source for Amarok and see how abstractable and extendable it is. If you come up with a solid and detailed plan of action (see some of the better GSoC proposals for what that consists of), and started a discussion about it on kde-devel, I think that you could probably find someone willing to mentor the project. \n\nPersonally, if such an app were to come around, the most important thing I'd want from it would be for it to really leverage the new KDE4 \"pillar\" technologies to increase integration at a more basic level. "
    author: "maniacmusician"
  - subject: "Re: Amarok video player"
    date: 2008-07-04
    body: "What I'm planning how I want the application to work, which is more like a feature plan. You must realize I haven't written a single line of code in C++, At least not one that works. I did write a little application in Java, but was never satisfied with the design. Perhaps that's because I never tried to properly design it before starting the implementation.\n\nAt this point I think the best approach is to start from a clean slate and think purely about how video management should work, rather than adapting an audio solution to support video as well. It may be that adapting existing code will work just fine, but I can't know that until I know how a video manager is supposed to be. I don't know if anybody ever made a video manager before, so I don't know if there's anyone to learn from. BTW, I can tell you right now that something like musicbrainz.org will be almost necessary, and that NEPOMUK will also be very important, and work in combination with the online DB. Also, there pretty much has to be a separation between the Library and the Collection.\n\nA video consists of one or more files.\n\nThere are basically 6 video-management actions users perform:\nImport, export, delete, modify attributes, move, and copy.\n\nWhen importing a video, The user might have to supply missing information like Title and Series. It will then search online for matches and offer them. The user can select an existing video or declare it as a new one. You can share your information on a per-video basis. All information about the Collection is stored in a DB, such as hierarchy, information attributed to videos, etc.\n\nThe way videos are organized in the Collection is independent of how they are stored in the Library.\n\nThose are just some of my most initial thoughts.\n\nAmarok2's playlist is nice, but I don't think it will work for this.\n\nI'll take your suggestions seriously and look into it, but I'm such a beginner I wouldn't even know where to start when trying to look at the source code of Amarok."
    author: "yman"
  - subject: "Re: Amarok video player"
    date: 2008-07-07
    body: "I thought about this a bit more, and reached the conclusion that all organizational information and the video files should be stored in a database. The organization:\n\nTimeline:\nA timeline lists other timelines and scenes in chronological order, but there can be multiple timelines and scenes that take place simultaneously. The timeline must specify which scenes play by default, and may specify playing dependencies (because if alternate scenes and deleted scenes get too complicated, it might be that they won't make sense unless a bunch of them are played in one viewing).\n\nScene:\nA scene is either a scene in a movie, or episode in a TV series or suchlike. A scene can have multiple videos linked to it.\n\nVideo:\nA video is a copy of a scene. Videos can differ in dubbing and subtitle language if those are encoded into them. A video can consist of one or more files in chronological order.\n\nFile:\nThe data of a video. Put otherwise, a file is a chunk of a video which can encompass the entire video or less. Sometime, such as on YouTube, a single video is too big, so it's divided into several pieces and served in parts. A file would be a part, and a video would be all of the parts put together, even if they remain separate files."
    author: "yman"
  - subject: "Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-07-03
    body: "Is anyone here using Amarok 2 on KDE 4.1 Beta 2? I can't find it in the openSuse 11 repos."
    author: "Bobby"
  - subject: "Re: Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-07-03
    body: "I found a package kde4-amarok:\n\nhttp://software.opensuse.org/search?q=kde4-amarok&baseproject=openSUSE%3A11.0"
    author: "Stefan Majewsky"
  - subject: "Re: Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-07-04
    body: "Thanks very much. What I realise though, is that it's in the Factory ropo. As a result my installation seems updated to factory. Let's see how it works... "
    author: "Bobby"
  - subject: "Re: Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-07-05
    body: "That is not openSUSE Factory. This is the KDE:KDE4:Factory repo for 11.0 (which is AFAIK the successor to the KDE:KDE4:Unstable repo)."
    author: "Stefan Majewsky"
  - subject: "Re: Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-07-06
    body: "A word of warning to people who want to try out Amarok 2, I installed it and now when KDE 4 starts I get an infinite (computer freezes around 200) number of knotify messages telling me my version of phonon-xine is too old. I used to compile amarok 2 from source and loved it, can't wait to see what it's like when it's released. Thanks for making the best music player on any platform (and giving it away for free)."
    author: "Aaron"
  - subject: "Re: Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-07-04
    body: "I though I heard that Project Neon was going to expand and start providing nightly binaries for openSUSE as well.  I could be mistaken."
    author: "T. J. Brumfield"
  - subject: "Re: Amarok 2 with KDE 4.1 Beta 2"
    date: 2008-08-09
    body: "I know I'm a little late on this, but... I think you want to be asking after *alpha* 2, not beta 2.  What's the difference?  Basically, it means it's way buggier and less feature-complete than you would expect from a beta.  If this weren't an OSS project you wouldn't even see it even if they were doing an open beta.\n\nIt also means you probably don't want to use it as your main player yet, even *if* you like livng on the bleeding edge."
    author: "tbocek"
  - subject: "Re: Amarok video player"
    date: 2008-07-03
    body: "\"Amarok 2 gets basic video playing support, and a connection to Librivox public domain audio books. Major porting to KDE 4 continues in K3b.\"\n\nI just hope those two strong pillars of KDE apps get ported when 4.1 final is released.\nKDE4 is great, but having to mix KDE4 and KDE3 is bad, as having to use some \"alien\" apps as openoffice and firefox was not enought :) [PS: not a critic, no flame, just a wish].\n\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Amarok video player"
    date: 2008-07-04
    body: "I would really like to use Konqueror as my one and only browser, but I want/need the following, else I'll use FF (I like FF, but I'd rather have a homogeneous environment):\n1. Undo Close Tab - Should already be implemented.\n2. Session saving & Loading - Might already be implemented.\n3. Flawless Google experience - I think Konqueror has some problems with Google. Gmail is my main concern, but Google Documents is also important.\n4. The Awesomebar - Not implemented, and I have heard of no plans to implement it in the future. I really can't see myself going back to using a browser that doesn't have something like the Awesomebar. It truly is Awesome.\n\nAs to KOffice, from everything I've seen so far, including from a little dabbling in Krita and Karbon,  It will be greate and suited exactly to my taste.\n\nIt might have been nice if as a stop-gap measure they would have made it so that when running KWin4, KDE3 apps would also use the KWin4 theme. Or maybe not."
    author: "yman"
  - subject: "Konq wishlists"
    date: 2008-07-04
    body: "1 Done: edit ---> undo closed window    or control-z\n\n2 Done, and works very nicely. We love Edulix!\n\n3 gmail is still problematic if you're using the javascript version, Google likes its non-standardisms, feel free to complain to them. Especially since to make it work best, you have to change your UA to firefox, thus, they have no accurate count of people using Konqueror. I suspect that complaints would help.\n\n4 Looks interesting: useful or invasive, you pick. Perhaps someone would like to code it?"
    author: "blauzahl"
  - subject: "Re: Amarok video player"
    date: 2008-07-04
    body: "I don't have a problem with the aliens (Firefox, OpenOffice)since they seems to be much better than the natives. Aside from that the can even be made to look like natives :)\nI do hope that we will have apps like Amarok 2, K3B and KAFFEINE as native KDE 4 apps by 4.1 though."
    author: "Bobby"
  - subject: "Since nobody else did yet..."
    date: 2008-07-03
    body: "Thanks, Danny!  You're doing a great job bridging the hackers and the wider community."
    author: "Bille"
  - subject: "Re: Since nobody else did yet..."
    date: 2008-07-03
    body: "me too, "
    author: "mimoune djouallah"
  - subject: "Re: Since nobody else did yet..."
    date: 2008-07-03
    body: "You are also doing a great job bridging the hackers and the wider community? Kudos for that. Thanks Danny."
    author: "ne..."
  - subject: "Re: Since nobody else did yet..."
    date: 2008-07-07
    body: "+1"
    author: "Michael \"thank you Danny\" Howell"
  - subject: "A question on Eigen"
    date: 2008-07-03
    body: "Many mathemetical (open source) libaries exist which are specialized on certain environments, i.e. parallel processing, distributed processing, processing on graphics cards, and specialized on certain tasks, i.e. small matrices, marge dense matrices, large sparse matrices... Each type requires individual data types and different algorithms. \n\nWrappers exist as well, for example the Trilinos project, Boost or the Mathe Template Library (and Eigen sounds to be something like the MTL). (such wrappers provide a unified interface to other libs) How does Eigen fit into the series of available math packages? Are there any benchmarks? \n\nIs there any focus where Eigen is superior to others with respect to efficiency? The Eigen website does not tell much about used algorithms and aimed usecases... The roughly mentioned OpenGL, games and spreadsheet applications are a wide field and may require a greatly differentiated library.\n\nThanks for answering"
    author: "Sebastian"
  - subject: "Re: A question on Eigen"
    date: 2008-07-03
    body: "First of all, the Eigen website you seem to be referring to is for Eigen1. For Eigen2, which is a rewrite from scratch under development, please see: http://eigen.tuxfamily.org/wiki\nIn what follows I'm always referring to Eigen2.\n\nEigen is more versatile than any existing library that I know about. We made this choice because KDE is a huge meta-project with very diverse needs and no existing library covered them all. Here are concrete examples:\na) some existing libs (TVMET, LTL...) focus on fixed-size (aka \"tiny\") matrices, while most libs do dynamic-size and are thus very inefficient (like 10x slower) when applied to fixed-size matrices. Eigen does both, which already makes it unique. Single source code, single API, still both cases are handled and optimized for. It even allows e.g. the number of rows to be fixed (that is, at compile time) while the number of cols is dynamic. Whenever a quantity is fixed, Eigen uses that to do the optimizations which then become possible (such as loop unrolling when it makes sense).\nb) Some libs (such as Blitz++) focus on array manipulation, some focus on linear algebra algorithms, some focus on geometry. Eigen does all that.\nc) Eigen is one of the few libraries based entirely on expression templates. This gives it an awesome API (like \"matrix.row(i)+=matrix.row(j)\") and much better performance.\nd) Eigen is made for general c++ programmers and its documentation and API are designed for them, while other libs are by and for numerical engineers.\n\nEigen is not at all like the MTL or Boost::uBlas, for the reasons listed above (among many). Trilinos is a huge library doing many things but I don't think they do fixed-size and I don't think they have expression templates. They still are very interesting because of the many sparse algos they implement, and we do plan to add a glue layer to the Sparse module of Eigen to allow it to use other libs like Trilinos as backends; but for most use cases I don't see Trilinos being an alternative to Eigen.\n\nIn all focuses touched by Eigen, it is in the top group with respect to efficiency.\n\nFor fixed-size, since we have both vectorization and expression templates, the only other lib that could perhaps have a chance against Eigen is LTL. I haven't done benchmarks. All that I know is that looking at the generated assembly, I don't see any room for improvement over what Eigen does.\n\nFor dynamic-size, we're in the top group since Gael wrote such an efficient cache-friendly implementation of matrix product, and our expression templates allow to leverage it for many uses. We're faster than most BLAS implementations (e.g. Gael measured that we're significantly faster than ATLAS) but not as fast as Intel's Math Kernel Lib.\n\nFor sparse, let's be realistic, we don't plan to reimplement all algorithms (we don't have the resources). We'll let Eigen use existing libs as backends for algos. However we (thanks to Gael again) have our own sparse storage and basic operations, and they are much, much faster than GMM++ against which we've done benchmarks.\n\nPlease, ask your questions to eigen at lists tuxfamily org, as I might easily have not noticed your question here."
    author: "Benoit Jacob"
  - subject: "Digikam fuzzy search"
    date: 2008-07-03
    body: "Wow, that looks cool. I'm not even sure I personally need it or will use it but it's just pretty amazing"
    author: "Simon"
  - subject: "Skipping old digests?"
    date: 2008-07-03
    body: "Hey Danny,\n\nwouldn't it be better to just skip the old digests, instead of trying to catch up?\n\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Skipping old digests?"
    date: 2008-07-03
    body: "Ideally, that might work -- but, I think the main target audience for the Digest is the group of slightly-more-than-casually interested KDE users who are interested in the current developments of KDE. For a lot of these users, the Digest is probably the only such source of news that they have the knowledge and time to comprehend and keep up with what's happening. i.e., the Planet is probably too active and technical for them.\n\nIn light of that target audience, it would probably be better for the Digest to catch up and be complete rather than be cutting edge with a few gaps. Of course, I could be completely off base about said target audience and correct measures to take against them, in which case, feel free to disregard. :)"
    author: "maniacmusician"
  - subject: "Re: Skipping old digests?"
    date: 2008-07-03
    body: "In a way I agree with this. Though, it could generate comments that waste devs time because back seat drivers missed something. So maybe one big very rough digest to catch up would be nice and then. "
    author: "winter"
  - subject: "the world graph is wrong"
    date: 2008-07-03
    body: "The world picture doesn't match the real values (germany had more than 20%). It is also simple to see that the colors can't add up to 100%-unknown."
    author: "kajak"
  - subject: "A curious \"feature\" - don't know where to ask"
    date: 2008-07-03
    body: "I have finally got round to doing ongoing backup.  I've connected a Western Digital My Book directly vis USB to the motherboard (and not done much else yet) to observe its behaviours. \n\nRandomly or after a long enough period of time (not sure which), on openSUSE, clicking on the geeko for the new style menu, activates the My Book and the screen freezes. (ah, relevance at last...) after a few seconds it all works again.  Nothing else, I've yet observed, causes anything similar.\n\nThis seems like a curious thing that shouldn't happen.  \n\nDoes anyone have a view about whether this is a feature, curiosity, bug or even relevant to KDE 3.5.9, (I'm not using 4.1B2 for D2D)?\n\n\n \n\n \n"
    author: "Gerry"
  - subject: "Re: A curious \"feature\" - don't know where to ask"
    date: 2008-07-03
    body: "Sounds like when the kickoff menu tries to build the list of devices for the Computer menu (using HAL) that wakes the MyBook and does a blocking operation, which takes a while for the MyBook to handle.\n\nYou could report this as a bug..."
    author: "Bille"
  - subject: "Re: A curious \"feature\" - don't know where to ask"
    date: 2008-07-03
    body: "Oh by the way, there's a big thread about MyBooks on the opensuse@opensuse.org list at the moment."
    author: "Bille"
  - subject: "Re: A curious \"feature\" - don't know where to ask"
    date: 2008-07-03
    body: "Grateful for all the information, and I'll take a look"
    author: "Gerry"
  - subject: "Re: A curious \"feature\" - don't know where to ask"
    date: 2008-07-07
    body: "This bug-report belongs on a Bugzilla. If this is OpenSuSE, try the OpenSuSE Bugzilla: bugs.opensuse.org.\n"
    author: "Michael \"put it on Bugzilla\" Howell"
  - subject: "KDevelop link is wrong"
    date: 2008-07-04
    body: "The KDevelop link points to www.koffice.org, last I checked we didn't merge with KOffice :)"
    author: "Andreas"
---
In <a href="http://commit-digest.org/issues/2008-06-01/">this week's KDE Commit-Digest</a>: <a href="http://amarok.kde.org/">Amarok</a> 2 gets basic video playing support, and a connection to Librivox public domain audio books. Major porting to KDE 4 continues in <a href="http://k3b.org/">K3b</a>. More work on "Fuzzy Search" integration in <a href="http://www.digikam.org/">Digikam</a>. The start of support for sound effects in KGoldRunner, and the addition of a sound feature in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. Improvements in the msword-odf and kpr-odf import facilities in <a href="http://koffice.org/">KOffice</a>. Start of QTestLib integration into <a href="http://www.kdevelop.org/">KDevelop</a>. The Ruby development bindings support interaction with <a href="http://pim.kde.org/akonadi/">Akonadi</a>. KMPlayer gets a bookmarks menu and support for displaying <a href="http://phonon.kde.org/">Phonon</a> metadata. <a href="http://kst.kde.org/">Kst</a> replaces the concept of tags with "dynamic names". A <a href="/issues/2008-05-25/#2">new, improved version of KColorEdit</a> is imported into KDE SVN. A new module, kdeplasmoids, is created in KDE SVN to consolidate the various different scattered locations of <a href="http://plasma.kde.org/">Plasma</a> applets. A new Plasma theme for KDE 4.1 is unveiled in KDE SVN. All of <a href="http://eigen.tuxfamily.org/">Eigen</a> no longer depends on Qt. KDevelop 4.0.0 Alpha2, Phonon 4.1.0, KOffice 1.9.95 Alpha 8, and KDE 4.0.5 are tagged for release. Phonon moves to kdesupport. guidance-power-manager moves to extragear/utils to be released with KDE 4.1. <a href="http://commit-digest.org/issues/2008-06-01/">Read the rest of the Digest here</a>.
<!--break-->
