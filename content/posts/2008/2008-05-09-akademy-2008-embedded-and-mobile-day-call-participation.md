---
title: "Akademy 2008 Embedded and Mobile Day - Call for Participation"
date:    2008-05-09
authors:
  - "bcerneels"
slug:    akademy-2008-embedded-and-mobile-day-call-participation
---
<a href=http://emsys.denayer.wenk.be>The EmSys research group</a> is hosting an "Embedded and Mobile Day" at Akademy 2008, this year in Sint-Katelijne-Waver, Belgium at <a href=http://www.denayer.wenk.be/index_eng.htm>Campus De Nayer</a>.  We welcome you to join the presentations and panel discussions about Open Source and Open Desktop technologies in embedded systems and mobile devices on Tuesday 12 August 2008.  



<!--break-->
<p>We are looking for your contribution in the form of presentations, papers or demos about:</p>
<ul>
<li>Embedded Linux</li>
<li>Mobile and integrated GUIs</li>
<li>System Integration</li>
<li>Embedded Development tools and distros</li>
<li>Innovation based on Open Source and Open Desktop technology</li>
</ul>
<p>The deadline for submission is June 16th 2008. Submissions may be send to akademy-emmobile@lists.den<span>a</span>yer.wenk.be</p>

<p>Parallel with the presentations a Device Plugfest will be held in an adjacent room. Everyone is invited to bring their USB, Bluetooth, UPnP, IrDA or other consumer devices to test and improve the interoperability with Linux, HAL and KDE. Take a look at the <a href='http://www.flickr.com/photos/metalmijn/2440884077/'>huge donation</a> from Conceptronic.</p>

<p>More information can be found at the <a href=http://akademy2008.kde.org/events/emmobile.php>Embedded and Mobile page</a> on the <a href=http://akademy2008.kde.org>Akademy 2008 website</a></p>



