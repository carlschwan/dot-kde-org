---
title: "KOffice 2 Alpha 6 Improves OpenDocument Support"
date:    2008-02-20
authors:
  - "jriddell"
slug:    koffice-2-alpha-6-improves-opendocument-support
comments:
  - subject: "Nice"
    date: 2008-02-20
    body: "I did not tested koffice2 yet, but I will install kubuntu packages this night right during the lunar eclipse :)\nI want to see what is new in krita, it is a nice image editor with a much better interface than gimp IMHO, but was lacking some features and had some bugs in it's kde3 incarnation, what is absolutely normal once this software is still in it's infancy."
    author: "Iuri Fiedoruk"
  - subject: "Multiplatform Support"
    date: 2008-02-20
    body: "This is the biggest thing with me.  I have to use Windows boxes at work.  I dual-boot on my main desktop.  I want to use the same suite on both Windows and Linux.\n\nThe moment I can run KOffice on Windows, and import Office 2003 docs reasonably well, I'm switching."
    author: "T. J. Brumfield"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "Apparently, MS has more or less released the specs of their binary formats.\nIs there any plan to improve Koffices filter there (not talking about OXML but previous docs)?"
    author: "Richard Van Den Boom"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "No. The real reasons behind the weakness of KOffice filters is not the lack of spec but the lack of a developer interested in doing the work. At least, for the .doc file format, we allready had the spec (up to 97), but even with that, the filters were not complete. The current focus is full support of ODF, and that is allready quiet some work."
    author: "Cyrille Berger"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "\"but the lack of a developer interested in doing the work.\"\n\nOk I can understand that there is interesting coding work and boring coding work.\n\nBut many people can pay a little bit of money, and in this situation it might be worth to raise money to overcome the problem with the lack of interest. Isn't there some kind of \"bounty\" concept? I reckon that there _are_ enough people that dont mind to throw in a few bucks every now and then to projects they regard as interesting. And this work here is also needed by quite many."
    author: "she"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "Look here: http://dot.kde.org/1199838571/"
    author: "Nik"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "We just need to get Google to pitch in a few bucks, like they did to get Photoshop working in Wine. Although I reckon Google probably uses Google Docs internally. :-(\n\nYeah, I'd pitch in $10 to help hire a Microsoft compatibility guy for KOffice. I don't currently use KOffice, but from the screenshots, I'd like to. OpenOffice is just a Word 2003 clone, whereas KOffice appears to have some fresh ideas.\n\nBut now technically, all that's needed is a perfect .doc to .odf (and vice versa) converter right? Then as long as KOffice supported .odf perfectly*, an existing third party library could be used to translate between .odf and .doc. Such converters (albeit not perfect ones) already exist, right? \n\n*and I know I'm using the word perfectly a little liberally here. Even OpenOffice interprets some ODF incorrectly apparently, according to the standard."
    author: "kwilliam"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "Me too!!\n\nI'd pitch in $10 for full MS-Office compatibility (including Excel Macros)\nand I'll pitch in another $20 for more Compositioning (compiz fusion style Kwin) eye candy. The daily office grind is just too bland without it.\nAnd $50 for full Direct X support for games. Hey a worker bee has to have fun sometime as well? Right?\n\nSo that would be $80 bugs towards open source development right there. :)\nBetter than spending $300 on Vista, or $200 per machine to get back to Windows XP.\n"
    author: "Hello"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "http://www.cofundos.org/\n\nDunno how it works but it seems to be what you're talking about."
    author: "Oscar"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "I think the reason OpenOffice.org still sucks is because they spend too many time and resources chasing after microsoft, and too few cleaning up the codebase, and adding new features to it.\n\nI hope KOffice doesn't go down the same path.\n\nIf you want to import m$ office docs, use openoffice to convert them first to odf (I think koffice 2 even ships with a script that automates this). Why spend more time and money chasing after the same tail lights as OOo?"
    author: "AC"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "I'm hoping myself that Koffice will offer feature parity with openoffice soon. This way we'll have 2 choices against Microsoft Office. If they're fully feature compatible to each other, they'll finally create their own \"office document\" ecosystem.\n\nAll we'll need then are \"economies of scale\". Once University computers in the California State University systems can read and write ODF documents, we'll know we have it made.\n\n(I actually had a professor (that wasn't very computer literate) bring his lecture as an ODF \"Powerpoint\"(1) and get frustrated that the school's computer can't read it. It's only a matter of time before more people will demand ODF funcionality and  will \"pull\" KOffice and openoffice through the channel. :-) )\n\n*(1) I still haven't learned the open source equivalent names of programs like:\nWord\nExcel\nPowerpoint\nAccess\nOutlook\nin a way that people understand (and is software brand independent). Someone help me out here please. Otherwise I don't mind if the above just become \"generic-sized trademarks\", just like \"Kleenex\", Xerox (for copying), \"Post-it's\", \"Q-tip\", etc.\n\nCome to think about it, it would be a great blow to Microsoft. Their cash cow could become a comodity. Go KDE - KOffice, and openoffice team!!!\n\n\n"
    author: "Hello"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "I will take the unfortunate role of the \"Betrayer of Hope\". But it won't happen, because it's not the goal of the project. If you need *all* the features of OOo, then go and use OOo. The goal of KOffice is to be a ligth-weight Office Suite that covers 90% of office use cases, which require between 10-20% of features from OOo and Ms Office.\nThat said, KOffice2 is sufficiently extensible that if you need a feature that is not available in the base package, then it can be added through a plugin."
    author: "Cyrille Berger"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "I think its funny how often you see feature requests that can be summarized as \"be more like this program.\" The parent message actually comes out and says that. Why don't they just use program X if they want it to be like it? \n\nWhat has been stopping me from using KOffice isn't a lack of features but a lack of polish. I'm looking forward to KOffice 2.0. :)"
    author: "Ian Monroe"
  - subject: "Light-weight office suite?"
    date: 2008-02-20
    body: "Gee.  I thought the goal of the project was to use the KDE/QT framework to build an office suite without all the bloat of OOo's custom portable UI framework.\n\nI don't remember reading that the goal was to provide only 'basic' functionality.  I assume that just means don't expect advanced functionality to appear overnight, not don't expect it at all.\n\nIt would seem that plugins would make sense in terms of embedding objects foreign to the basic class of document you're working on.  Or maybe providing a GUI for some custom combination of the basic document features.  But not to flesh out the basic functionality of the word processor, for example.\n\nFirefox has gotten away with pointing users to plug-ins, but never (I think) to enhance what kinds of HTML it can render or to add missing JavaScript functionality.  That would be a lousy design, and a bit of a cop-out.\n\nOf course, I'm not contributing code to KOffice, so who am I to criticize?  But still, KOffice as been bandied about as the great cross-platform office suite of the future often enough that a goal of 'light-weight, 20% feature set' is a bit of a disappointment."
    author: "littlenoodles"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-20
    body: "> so who am I to criticize?\n\nFor me the question is more, what you criticize since I guess you probably missinterpreted the ~20% vs. basic functionality. Nobody was saying that it makes sense to remove the processing part from a word-processor. It's more like it makes no sense to embed Gallery, Mediaplayer, Bibliography Database, Webbrowser, PIM, etc. into a word-processor. If there is really a need for them, go with optional plugins while keeping the base code as small as possible.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: ">Gee. I thought the goal of the project was to use the KDE/QT framework to build >an office suite without all the bloat of OOo's custom portable UI framework.\n\n>I don't remember reading that the goal was to provide only 'basic' functionality. >I assume that just means don't expect advanced functionality to appear overnight, >not don't expect it at all.\n\nYou're not alone. That's how I interpreted it too. \nLike a lightweight version of openoffice. Like with out the \"bloat\" of the compatibility layer, and instead relying on Qt.\n\nI never thought of it as \"missing\" functionality.\n\nopenoffice is to big to run on mobile devices. Koffice would be just right. I still want to be able to use all/or most of he features I'd use on a stationary machine.\n\n\n\n"
    author: "Hello"
  - subject: "Re: Light-weight office suite? <-- Full feature pl"
    date: 2008-02-21
    body: "Me too!! +1\n\nI'm tired of having to give things up just so I can use Linux/Foss.\n\nI understand that it will take a few years to get the same amount of features as MS-Office / Openoffice.\n\nBut, c'mon. I'm so tired of overpromising, underdelivering. That's why I liked KDE 4. Aarons keynote made me finally believe that Foss can be BETTER/MORE INNOVATIVE than the commercial equivalent. (maybe Aaron has a similar \"Steve Jobs reality distortion field\". That would be sad through)\n\nJust because I like to use Linux/Foss/non-proprietary technologies, doesn't mean I have go back 10 years when it comes to available features.\n\n-Max"
    author: "Max"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "+1!!!!!!!\n\nI want feature parity with MS-Office as well!!\nI'm so tired of having to give up features just to be \"different\".\n\nThank God for crossover office!!  Why can't we have the speed of KOffice with the functionality of openoffice?\n\nWho are you to tell me what 10% of features I actually use? I'm sure I don't use all the features, but I want to be able to open adn edit Word,PPT, Excel documents without any crazy formatting errors.\n\nI would like to use Koffice on an Iphone, or Iclone. I thought that was the idea behind QT/QTopia?? I want a full blown suite, not just \"Wordpad\". Safari on an Iphone is also a full browser, not a 10% version."
    author: "Linux"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "I agree and that's why I don't use Konqueror as a web browser (although it's one of the most powerful file browsers available) and Koffice as an office suite. \nI don't wish to discourage the developers who I am sure are giving their best but I would love to see Qt versions of Firefox and openOffice. That would bring me to the zenith of my Linux experience :) Sometimes I ask myself if it wouldn't be more constructive to work with these other projects in order to achieve such a goal instead of duplicating efforts."
    author: "Bobby"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-22
    body: "You couldn't pry Firefox from my cold-dead-hands. EVAAAR!!!!\nI use it on any computer and operating system that will run it.\nIt's the only reason I'm even using Linux on a daily basis. Firefox has 100% feature parity with the Internet. ON ANY OPERATING SYSTEM. \n\nI wouldn't use KDE/Linux if it weren't for Firefox. Konqueror is all nice and neat, but it ain't no *fox.\n\nopenOFFICE, and Koffice won't ever have the adoption rate of Firefox, if they can't deliver 100% feature parity (for most common usage, with plugins for the rest) with MS-Office. You can skip \"clippy\", nobody needs that guy anyways.\n\nDid Microsoft's anouncement yesterday help at all? - News please. How does it affect Linux?\n\n"
    author: "Max"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "OpenOffice.org 2.x does use Qt under KDE. The problem is that it just uses Qt widgets but it is not a KDE application."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "+1\n\n\nYes, we definitely need feature parity.  The mention of lacking a bibliography database concerns me, as this is essential for academic writing, which is a huge potential userbase.  Every tertiary student needs this, so if it could be intergrated like Endnote is into Word, that would be perfect.\n\nWe do not want lightweight tools.  If we wanted lightweight, we would use Abiword!"
    author: "Kerr Avon"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "Well, start coding then. Seriously, you can be concerned as much as you like but until you start committing you're not help to alleviate your concerns."
    author: "Boudewijn Rempt"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "Unfortunately, that just is not realistic as I do not have the time to learn coding as I am already fully committed to work, study and other charities.\n\n\nI have offred feedback in the past however and even talking on this forum and giving you user feedback should be valued as a user offering his concerns back to you."
    author: "Kerr Avon"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "I know it takes a while, but please add features to KOffice, because of Qt it could be waaay faster than OpenOffice and Microsoft Office. I would love to use a full blown Office Suite on a lightweight PC/Car PC/Nokia Phone/Gphone\n\nAt the very least add all features necessary for translation of *.doc and *.xls (w/macros) to KOffice Odf format. I ABSOLUTELY NEED TO have full Excel macro functionality. I can put up with missing Word features. I'd also like great Impress/Presentation features. (wouldn't be great to design a presentation on my mobile phone, and then project it from a built in projector (out later this year) I could finally travel light :) )\n\nI would like to start typing up a document on my desktop, continue on my smartphone (running Linux + KDE/Qt), get into my car with my 7\" touchscreen Car Pc with a wireless keyboard, type it up on my laptop when I get to the Office, and then present straight from my Cell-phone. I can't do it with Windows, but with Linux + KDE 4.1 it would be possible. Mr. Seigo promised.\n\nPlease make KOffice, KDE, and Linux BETTER THAN anything we've ever seen under Windows.\n\nYOU HAVE THE POWER!"
    author: "Mike"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "> YOU HAVE THE POWER!\n\nSo do you, thanks to open source. Go for it."
    author: "T"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "I am a die-hard KDE lover but there are two GTK applications that I will never let  go until the KDE equivalents are at least as good and they are Firefox and OpenOffice. Konqueror as a standalone browser just can't match Firefox and Koffice still have a lot of catching up to do when compared to openOffice. I am only a user with very little knowledge of coding but I think that the end users are the real testers and the one who see the differences :)"
    author: "Bobby"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "Nit-pick : Firefox and OO.o can't really be termed \"GTK\" applications: they use their own widgets throughout the entirety of their huge codebases, and on Linux, the \"last mile\" is handled by GTK.  The dependence on GTK is so slight that, in the case of Firefox, a handful of KDE devs managed to remove all dependence on GTK from the 1.5M loc codebase and replace it with Qt in just *a few days*.\n\nIt always irks me when GNOME fanatics try to hold these two apps up as great examples of GTK apps - the two owe practically none of their success to their use of GTK and, indeed, don't use GTK at all on any platforms but *nix."
    author: "Anon"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-21
    body: "On Linux, Firefox only builds with GTK widgets these days.  The QT branch has been completely deserted.\n\nSomeone could develop a QT4 branch, and I'd love to see it."
    author: "T. J. Brumfield"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-22
    body: "That sux, that it's abandoned. This would be great to work under Qt 4.4 and KDE.\nNOKIA - this might be useful for your mobile phones *hint, hint*\n\nWhen are we getting it? Will it be in the next newsletter?\n\nQt-Firefox, I can see it now. =)\n\n"
    author: "Max"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "Nokia committed to WebKit long ago."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-22
    body: ">The dependence on GTK is so slight that, in the case of Firefox, a handful of KDE devs managed to remove all dependence on GTK from the 1.5M loc codebase and replace it with Qt in just *a few days*.\n\n\nReally? That would be SO COOL!!! Where are the packages?\n\nI would love to use KDE -Firefox. (or Fire-saur  = Firefox + Konqii)\n\nIs it any faster than the current version? Does it use less memory? What are the benefits?\n\n\nCould we just do the same with OpenOffice? \n\n\n"
    author: "Mike"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-22
    body: "> Really? That would be SO COOL!!! Where are the packages?\n\nThis work has been rejected by Mozilla, therefore it is no longer being worked on.\n\nThere is no point in working on something that will not be accepted by the application maintainers unless you intend to fork and forking a browser when you already have one isn't wise either."
    author: "Kevin Krammer"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "Really?\n\nFirefox used to have a QT branch back in the Firefox 1.5 days and they objected to the branch before.  Firefox has tons of config options and branches, and I mean tons.\n\nIf someone wrote the code, then post it.\n\n"
    author: "T. J. Brumfield"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "Another option is to develop the Qt version parallelly with the official branch and merge the changes of every new version. Of course this would need extra maintainence work."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "> Could we just do the same with OpenOffice?\n\nThis seems to be done already. I am running OpenOffice with Qt widgets right now. Gentoo package app-office/openoffice-2.3.1-r1 with USE-flag kde enabled."
    author: "Erik"
  - subject: "Re: Light-weight office suite?"
    date: 2008-02-23
    body: "Does OOo use GTK at you under KDE? OOo as of 2.0 can use Qt (default under KDE), GTK (default under Gnome) and its own widget set (default under other DEs)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "> The goal of KOffice is to be a ligth-weight Office Suite that\n>  covers 90% of office use cases, which require between 10-20% of\n> features from OOo and Ms Office.\n\nThat only works if those \"90% of the people\" all need the same \"20% of the features.\"  I suspect that assumption is faulty.\n\nThe other assumption there is that a \"light-weight\" office suite is valuable.  On my four-year old laptop OpenOffice.org starts up in about 12 seconds from cold and  about 3 seconds warm.  Times for Microsoft Office were about 5 / 2 seconds respectively.  Memory usage, running performance and UI complexity are all with acceptable ranges as well.  Back when KOffice was started in 1999 I can see the logic of that.  Beyond 2002 I'm not so sure.  \n\nIf you're looking at low-end hardware (eg. OLPC) then low memory usage and good performance are important, but you're competing with AbiWord and Gnumeric which are pretty good established solutions."
    author: "Robert Knight"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "> That only works if those \"90% of the people\" all need the same\n> \"20% of the features.\" I suspect that assumption is faulty.\n\nIt depends what this 20% are. Let's look at KWord for example. Each users needs the functionality to 1) load a document, 2) edit that document and 3) save that document. And there you have already the basic functionality each user needs :)\n\nNow let's look how additional functionality is integrated, for example Variables like displaying the current date/time or like the scripting plugin. They are both done as plugins.\n\nWhat you gain from this? A defined small(er) set of basic functionality that just needs to work 100% correct for anybody on the one hand and a very modular architecure on the other. Those does provide then;\n* easy way to add or remove plugins without any recompile\n* a clear API and separation for functionality\n* it's not needed to know about all details to add new functionality\n* things are more reusable, look at the Flake-architecture here as example\n* it's easy to tailor an app to special needs like the kids-office, mobile devices or full-powered desktop's\n* a more cleaner codebase\n* etc.\n\nSomeone already did mention firefox and imho we could also look at amarok or even plasma here. Provide base functionality that is just needed in any use-case and go with plugins for more specialized additional functionality.\n\nWell, that's at least my pov here :)"
    author: "Sebastian Sauer"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "I'm far from needing all the features, but I can't use your program if one or two of the 80 ~ 90% so caled \"never used features\" are not there, and every one has an *diferent* set of unusual features that they need and can't live w/o. You must be realy naive to expect pleasing 90% of the users implementing the 10~20% most used features. I would not expect even 10% of loyal users.\n\nAnother day I needed to view and edit an spreadsheet with some parametrical plots, but not even OpenOffice implemented that feature, let alone Koffice. If I worked with that on daily basis, I would never look another time to these programs, but in my case was only an anoyance that made me run to excel for this file, and an negative point for openoffice and koffice.\n\n>That said, KOffice2 is sufficiently extensible that if you need a feature that is not available in the base package, then it can be added through a plugin.\n\nGiven that I'm an programmer and have enought time and knowledge to implement that feature. Or that I'm rich and can pay some hours of an professional programmer's time for every feature that I want and is not there. How much I would have to pay to have that parametrical plot capability in koffice, for example? I have no idea, but it should not be cheap.\n\nAlso, plugins should be reserved for custom features (macro-like things), or some special things that would add too much bloat for only that feature (ie: video-playing).\n"
    author: "Manabu"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "Probably to look at the other way around makes it more clear; without those 20% of the basic functionality that needs to work 100%, you will have (at least) 80% of your users lost ;) Above I already did mention, loading+editing+saving a document for KWord. As example for KSpread it's tetra-important to always calculate 100% correct (not like MSCalc, heh) and to fullfit that need, a lot of work was done on producing unittests for every single formula (and there are a lot of them). Also a lot of time was spend to improve the performance with huge documents since that was a big issue in KSpread 1.6 afaik. That doesn't mean, that support for parametrical plots may never done. It just means, that it had a lower priority then working e.g. on the formula-front.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "s/MSCalc/MSExcel/ ... so, that wasn't meaned to be ambiguous, but is just an example that I still have problems to deal with all the names MS invented like Excel, Office Open XML, shared source, Standard, etc.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "Of course. I was just alarmed with Berger saying that the goal of the project is only have the 20% basic working and nothing more. It is ok as an short-term goal, but not long-term, especialy for an office suite.\n\nIt is realy amazing the amount of progress that KOffice team has done with so little work force, that is an small fraction of that used by MSOffice or OpenOffice. We can only wonder what could be done with an similar work force. So don't have so modest long-term goals!"
    author: "Manabu"
  - subject: "Re: Multiplatform Support"
    date: 2008-03-20
    body: "Video Playing Should No Longer Add Much Bloat In KDE4, Thanks To Phonon."
    author: "BCook2"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "\"The goal of KOffice is to be a ligth-weight Office Suite\"\n\nAnd that's the advantage of Koffice. If one just wants to write a letter or something, than Koffice  is the right choice. If one wants to do nasty things she can start \"a second OS\" like OOo."
    author: "oxblood"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "Software brand independent names:\nword processor\nspreadsheet\npresentation program\ndatabase (management system)\nE-mail client\n\nODF file types: word processing documents, spreadsheets, presentations, graphics, formulae."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "Yea, never thought of it. thanks\n\nI'm using Powerpoint so often as a generic term that I forgot that it means Presentation\n\n(when I used the term \"impress\" all I got is confused looks from the uninitiated)"
    author: "Hello"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "The big problem is not really the lack of interest, but the lack of time. All KOffice developers are really busy on getting the necessary features done. A bounty won't give us more hours in a day -- unless the bounty were big enough to enable us to stop doing our day jobs. We just need more people who want to develop on KOffice.\n\nIt's an interesting project with many really interesting things to do. Even writing an ms-office file format filter can be really interesting: look for instance at the wv2 library project. There's lots of fun to be had working on that."
    author: "Boudewijn Rempt"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-20
    body: "Read the latest from Joel on that topic:\n\nhttp://www.joelonsoftware.com/items/2008/02/19.html\n\nhe makes some very good points, especially that you can't provide compatibility to MS Office without adapting your whole program to the spec. "
    author: "digital.alterego"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "Use LaTeX. Write on any platform.\n\nUse ODF. Write on any platform.\n\nYou'll forgive me if I don't care that you want Windows support when Microsoft doesn't want Linux support. Hell they don't want Mac support but it makes them too much money and they need all they can get with SONY soon to eat at their just recent profit making XBox Division."
    author: "Marc Driftmeyer"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-21
    body: "I use OpenOffice at work since I'm a SysAdmin and get to pick what I use at work, but I still have to share documents with everyone else using MS Office.\n\nMy wife is a student and must give documents to instructors who will only accept them in MS formats.\n\nI can preach about ODF all day, but most of the world isn't listening yet.  MS Office compatibility is very important, but if you can get some of those users to try OpenOffice on Windows (or KOffice on Windows) they will open their minds about OSS.  Then converting them to Linux is easier."
    author: "T. J. Brumfield"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-22
    body: "Same here. Or I have to OPEN documents my teacher send me. WITH TABLES, MACROS, etc. AND THEY HAVE TO FORMAT CORRECTLY.\n\nI don't care how lightweight an application is, if it can't do basic stuff I need it to do.\n\nCan't we just port openoffice to Qt? Wouldn't that be a smaller application, with a smaller memory footprint?\n"
    author: "Max"
  - subject: "Re: Multiplatform Support"
    date: 2008-02-27
    body: "OpenOffice uses it's own UI toolkit (on Linux, it works with GTK themes). It would be very painful to port it from it's toolkit to make it smaller. Also, KOffice only misses some features because it has less developers than OpenOffice and even without it, it's improving quickly."
    author: "Riddle"
  - subject: "Slightly off topic: Google takes a more active rol"
    date: 2008-02-20
    body: "Google is taking a more active role in supporting Linux & Wine.\n\nhttp://www.tectonic.co.za/?p=2162\n\nI'm guessing it won't be long until they will support the KDE 4.X platform more actively. (I hope!!)\n\nEither way, we are thankful for google's role in the open source community.\n\nGoogle - please become a patron of KDE. \n\nHopefully Google will host the next release party as well. I loved the webcast and Aaron Seigo's keynote. :-)\n\n"
    author: "Hi"
  - subject: "Re: Slightly off topic: Google takes a more active"
    date: 2008-02-20
    body: "I strongly doubt you will see google support Koffice.  They already support OpenOffice, there is no need for them to support another.  Nor do I think you will see them support KDE, they are more likely to support a linux distro, which in a way they do by using gbuntu, google's inhouse version of ubuntu."
    author: "Anon"
  - subject: "Changelog complete?"
    date: 2008-02-20
    body: "Is the changelog complete? The OpenDocument improvements are missing and almost all changes are from the drawing apps, but none from the office apps."
    author: "AC"
  - subject: "open dialog"
    date: 2008-02-20
    body: "There any plan to refresh the confusing open dialog?"
    author: "Tom"
  - subject: "Re: open dialog"
    date: 2008-02-21
    body: "Various cleanups (removal of buttons at center of screen specifically) have been done recently, I believe this release will show this change.\n\nI'm quite excited about the updates and I'm sure that most users will like them.  So check them out and after you did, let us know what you think."
    author: "Thomas Zander"
  - subject: "Release date? "
    date: 2008-02-21
    body: "When is KOffice planned to be released? 4.1 maybe?"
    author: "Jeremy"
  - subject: "Re: Release date? "
    date: 2008-02-21
    body: "http://wiki.koffice.org/index.php?title=KOffice2/Schedule"
    author: "Anonymous"
  - subject: "Still a long way to go"
    date: 2008-02-21
    body: "Tested it a bit. Not even cell colors in spreadsheets is supported yet.\nKoffice per se is getting very nice, but odf on it still have a lot of work ahead. Anyway a very nice version, run very well.\nI just don't like how koffice uses too much screen, those new file dialog are just too big, and Krita panel by default don't even fit in my 1400x900 screen."
    author: "Iuri Fiedoruk"
  - subject: "Re: Still a long way to go"
    date: 2008-02-22
    body: "I keep saying that. \"desktop screen-real-estate\". But no one's listening... :("
    author: "Max"
---
<a href="http://www.koffice.org/announcements/announce-2.0alpha6.php">KOffice 2 Alpha 6</a> has been released.  This preview release improves the  OpenDocument infrastructure, adds snap guidelines to several applications and sees major improvements in Krita, Karbon &amp; KPlato.  See <a href="http://www.koffice.org/announcements/changelog-2.0-alpha6.php">the changelog</a> for more details. <a href="http://www.koffice.org/releases/2.0alpha6-release.php">Download it</a> from source or get the packages for Kubuntu or openSUSE.



<!--break-->
