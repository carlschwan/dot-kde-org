---
title: "Interview with Mark Kretschmann and Amarok Installer for Windows"
date:    2008-02-08
authors:
  - "lpintscher"
slug:    interview-mark-kretschmann-and-amarok-installer-windows
comments:
  - subject: "Liked this one"
    date: 2008-02-08
    body: "<<quote>>\n> If you had a million dollars (that's roughly 175Eur at current exchange rates) - what would you do with it? \nAt first I'd make sure that I would no longer have to deal with basic life issues, paying rent, food, etc. Then of course give some to my parents and so on. (...) But let's be frank. The first three weeks I'd probably spend in a state of constant partying, substances and girls - I'm no saint, you see. I think partying is crucial for loving yourself, and loving yourself is the only way to really love other people. Think about it :)\n\n>So that explains why the Amarok crew parties so hard (see FROSCON) ? \n I personally take pride in the fact that we actually know how to celebrate the moment. You see, at the end of the day, what are you going to remember in 30 years time? The day you fixed some code bug and walked the dog and then talked the day over with your partner, or the day where you totally freaked out, party animal style, dancing until sunrise with people you love, and ending up discussing life philosophies while passing around a joint? You choose.\n<</quote>>\n\nNot that I'm that crazy but there are true gems in there :)"
    author: "Diederik van der Boor"
  - subject: "Re: Liked this one"
    date: 2008-02-08
    body: "Well if your the type of person who likes to party hard...  Personally I'm not that type of person.   \n\nIt takes all types to make this world go round though.   He did come out against drinking and coding, so I have no problems with him."
    author: "Hank Miller"
  - subject: "Re: Liked this one"
    date: 2008-02-08
    body: "What's wrong with hacking and drinking? I always drink when I am writing code. I mean, you get into a really loooong coding session you can easily dehydrate. You better do drink!\n\nAre you that fussy about your keyboard?\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Liked this one"
    date: 2008-02-20
    body: "<<quote>>\nAre you that fussy about your keyboard?\n\nUwe\n<</quote>>\n\nYou have obviously never taken apart a TV remote...  I'm telling you, things with buttons + food and drink results in some of the nastiest (and completely unidentifiable) filth to be found in the average household.\n\nJeff... The IR LED scavenger"
    author: "Jeff"
  - subject: "Re: Liked this one"
    date: 2008-02-09
    body: "Nah, don't take the partying line too serious. I like to go out, but I'm not doing crazy things. On the contrary.. I can be pretty serious about things. The lines I noticed were about being able to love yourself and what you'd remember over 30 years."
    author: "Diederik van der Boor"
  - subject: "Re: Liked this one"
    date: 2008-02-08
    body: "Really fantastic answers. Thanks for the last few minutes of laughing :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Liked this one"
    date: 2008-02-08
    body: "I love the amarok team.\n\nNo wonder it's such a great program. They know how to have fun..\n\nHe should work for google when he \"grows up\" (i.e.: wants to settle down.) They party like crazy as well.. :)"
    author: "Max"
  - subject: "Re: Liked this one"
    date: 2008-02-08
    body: "[quote:\npartying, substances and girls\n/quote]\n\nWhy only girls? Why don\u00b4t you try boys also?\nI am really shure that you will apreciate a lot.\n\n[quote:\nwalked the dog\n/quote]\n\nWhy only walk with the dog?\nYou can try \"have fun\" with the dog.\nWhy not? Enjoy your life, play BSDM with you million.\n\n:-P"
    author: "Jonh the G"
  - subject: "Re: Liked this one"
    date: 2008-02-09
    body: "Thanks for calling me crazy. The first one is free, but after that our legal department is going to sue you.\n\nPS: Appreciated your comment ;)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Liked this one"
    date: 2008-02-09
    body: "ok, thanks!\nand good to know. :)\n\n*calls a lawyer*\n\n;)"
    author: "Diederik van der Boor"
  - subject: "Re: Liked this one"
    date: 2008-02-09
    body: "> If you had a million dollars (that's roughly 175Eur at current exchange rates) - what would you do with it? \n\nWhat did the interviewer mean by this? I guess he means US$ and EURO?\nHow should 1M US$ be 175! EURO?\nIt's about 690000 (or 690260) EURO!\n\nJust a bit nit picking...\n"
    author: "Harald Henkel"
  - subject: "Re: Liked this one"
    date: 2008-02-09
    body: "There's this rare, mythical thing called a joke. A good, basic book on the subject is Hannjost Lixfeld: Witz. Arbeitstexte f\u00fcr den Unterricht. Stuttgart, 1978. Gomperz' article \"Witzjagen om zu leben\" is relevant in this context. Of course, the standard work is still Freud's \"Der Witz und seine Beziehung zum Unbewu\u00dften \" and \"Humor\", while the journal Maledicta, pub. Reinhold Aman, is often very informative on the more down-to-earth side of the subject. Finally, the collected works of T. Pratchett are as valuable a resource in the English-speaking world as the works of G. Bomans and P. Rodenka are in the Dutch-speaking world."
    author: "Boudewijn Rempt"
  - subject: "Re: Liked this one"
    date: 2008-02-11
    body: "And it's a really good one too!!!\n\nVERY, VERY FUNNY!! I rofl't for quite a while."
    author: "Max"
  - subject: "!GWN > GWN"
    date: 2008-02-08
    body: "That \"Not the Gentoo Linux Newsletter\" is great. Sometimes we take ourselves too seriously in open source, so it's good to have stuff like this."
    author: "Ian Monroe"
  - subject: "Re: !GWN > GWN -- Very true!!"
    date: 2008-02-08
    body: "Very true.\n\nThe open source community sometimes has a \"holier than thou attitude\".\n\nCoding should be fun. More fun than in the corporate environment. Geeks are great people to hang out with. Especially the ones that like to party. I know a few of those in Silicon Valley.\n"
    author: "Max"
  - subject: "Ruby FTW"
    date: 2008-02-08
    body: "> For me Ruby programming was an eye opener: it's so smart and wonderful on so many levels, and yet easy to learn. I tend to be vocal about such things, and I openly fight Python (which is of course the antichrist) wherever I can. Give Ruby a try, it's just a work of art, and actually useful. I use it whenever I'm not forced to use C++, and I'm even known for my wilds plans to rewrite part of Amarok in Ruby. \n\nAmen :)"
    author: "Patcito"
  - subject: "Re: Ruby FTW"
    date: 2008-02-08
    body: "Cool.. Glad to hear that.\n\nI heard very little about Ruby so far, but what I hear is overwhelmingly positive.\n"
    author: "Max"
  - subject: "Re: Ruby FTW"
    date: 2008-02-08
    body: "If you don't hear much, I'll add one:\nRuby is said to be sh** at Unicode, even worse than Python. Wonder if anyone can say something more clearly on that, as I always dreamt about switching. Oh, well, quite off topic here. But a small reminder that ppl working with Unicode still suffer from huge amounts of bugs and inconsistencies, when they actually should be producing code..."
    author: "Chris"
  - subject: "Re: Ruby FTW"
    date: 2008-02-09
    body: "I've been using ruby for 3 years and never had a problem with unicode. Plus, unlike Python, ruby is developed by Japanese guys so you can bet they know how to deal with encoding. 1.9 even comes with awesome m17n."
    author: "Patcito"
  - subject: "Re: Ruby FTW"
    date: 2008-02-09
    body: "When I say Unicode I mean seamless integration \u00e0 la Java. But instead \"encoding error\" here, \"module currently doesn't support Unicode\" there.\n\nRuby was said [1] to lack proper Unicode support as the leading Japanese guy doesn't like Unicode (many Japanese ppl seam to dislike Han-Unification). A lot of hearsay here though, sorry.\n\n[1] http://blog.ianbicking.org/why-python-unicode-sucks.html"
    author: "Chris"
  - subject: "Re: Ruby FTW"
    date: 2008-02-11
    body: "Ruby uses a global variable $KCODE to set the default encoding - either ascii, UTF8 or two Japanese/Korean encodings, which is very similar to the 'setdefaultencoding()' function in your python link. \n\nIn QtRuby or Korundum for Qt or KDE programming in Ruby, you can display Unicode strings from UI code you have set up in Qt Designer by setting $KCODE to 'u'. As far as I know that is good enough for a lot of use cases. If you want your code to depend on non-ascii collating sequences and regular expression matching you need to be very careful anyway, whichever language you are using.\n\nA Ruby String is a sequence of bytes with a length, and how your interpret it depends on the encoding you are using. There will never be two sorts of strings as in Python. Java has only one sort of string, and so it might not be as convenient for handling non-UTF strings. There is no right answer - language encoding is a complicated subject."
    author: "Richard Dale"
  - subject: "Re: Ruby FTW"
    date: 2008-02-12
    body: "\"language encoding is a complicated subject.\"\n\nI strongly disagree. It might be now, but hopefully in 10 years we will think somebody is randomly generating characters once we see the string \"ISO 8859-1\".\n\nIn my eyes there is no usage for any other encodings except the Unicode ones, except again there are should be some codes Unicode why so ever would decide to not include. Well, there's still a private area in the code table for own use though.\n\nI hope then the Python ppl will say: F* me, but that was one bad decision 10+ years ago."
    author: "Chris"
  - subject: "Re: Ruby FTW"
    date: 2008-02-08
    body: "That's because it is too useless for anybody except religious ruby zealots to know about it and actually use it..."
    author: "python loving flamebaiter :)"
  - subject: "Re: Ruby FTW"
    date: 2008-02-09
    body: "*kiss*"
    author: "Mark Kretschmann"
  - subject: "Re: Ruby FTW"
    date: 2008-02-09
    body: "0h I love the way Ruby is the answer to all problems.:P I mean really, you shouldn't be writing that much code that the language actually affects what you write. I have seen too many guys spit out lines and lines of code, only to go and delete it all or rewrite much of it because they were going to fast and not thinking things through. Whatever you code, think it though. Then it won't really matter what language you use. Be creative. If you have to reinvent the wheel, better not use an interpreted language anyway."
    author: "winter"
  - subject: "Re: Ruby FTW"
    date: 2008-02-12
    body: "This, and especially the part about making Ruby a hard dependency of Amarok, bothers me hugely. If you need to resort to making something a mandatory dependency for it to gain traction, isn't that a sign your chosen tech may be doing something wrong that you are not aware of?\n\nBesides, doesn't KDE come with the Kross framework, that exists SPECIFICALLY to make each user's respective choice of scripting tech a non-issue?\n\nSeriously, I think this kind of zealotry has become Ruby's number one issue lately. It's not a bad language, but it increasingly looks like its fans are more interested in attacking the competition with underhanded tactics instead of catching up technically and that's very, very wrong. :("
    author: "For chrissake..."
---
Amarok project founder <a href="http://gentooexperimental.org/not-the-gwn/not-the-gwn-06.02.08.html#Interview">Mark Kretschmann was interviewed</a> for "Not the Gentoo Linux Newsletter". He talks about Amarok and what makes this project special, its community, beer, more beer and other things of importance to him.  Amarok headquarters is also pleased to announce Amarok 2 Technology Preview 1 for Windows, complete with an easy to use installer! More juicy details at <a href="http://amarok.kde.org/blog/archives/583-Windows-Binaries-of-Amarok-2-Tech-Preview.html">the Amarok blog<a>.


<!--break-->
