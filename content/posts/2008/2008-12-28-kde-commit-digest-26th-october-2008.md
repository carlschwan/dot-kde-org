---
title: "KDE Commit-Digest for 26th October 2008"
date:    2008-12-28
authors:
  - "dallen"
slug:    kde-commit-digest-26th-october-2008
comments:
  - subject: "Bomber?"
    date: 2008-12-27
    body: "Any screenshot or info about the Bomber game? Is it a Qt bomberman? "
    author: "kubunter"
  - subject: "Re: Bomber?"
    date: 2008-12-27
    body: "No, it's a simple \"bomb buildings before your plane crashes\" type of game; very similar to \"Blitz\" on the Vic 20, but obviously far better looking :)\n\nhttp://shield008.blogspot.com/2008/11/bomber-moves-into-kde-games.html"
    author: "Anon"
  - subject: "Re: Bomber?"
    date: 2008-12-27
    body: "http://www.commit-digest.org/issues/2008-10-12/#2\n\nDanny"
    author: "Danny Allen"
  - subject: "More WebCore-based improvements to KHTML"
    date: 2008-12-27
    body: "This really sounds like work duplication. It would be good if KHTML devs could merge their work into WebKit, WebKit development process is really open now."
    author: "kubunter"
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-27
    body: "or rather : your comment is a duplication of the 100 times this subject has been brought up on the dot, so pray look it up in the search box mmh kay?"
    author: "valentine"
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-28
    body: "Don't worry.  If Konqueror sticks to KHTML, Epiphany will still be going WebKit."
    author: "ethana2"
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-28
    body: "Or someone else will stand up and write a kpart for it. :)\nI still like khtml though..\n"
    author: "Mark Hannessen"
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-29
    body: "I prefer my web code to be consistent across multiple browsers and not have basic positional code still screwed up in KHTML.\n\nThe latest trunk still positions position:relative different than Firefox/Iceweasel 3.0.5, Opera 9.6.3+, Safari3.x+, Chrome, etc.\n\nThe law of diminishing returns is rapidly approaching."
    author: "Marc J. Driftmeyer"
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-29
    body: "> The latest trunk still positions position:relative different than\n> Firefox/Iceweasel 3.0.5, Opera 9.6.3+, Safari3.x+, Chrome, etc.\n\nI call bullshit on that... go ahead, show us the html or at least the bug number! :)\n\nOn the other hand, the latest Webcore svn is still unable to render correctly simple *left/right aligned tables* that render perfectly fine in all other engines including KHTML. \n\nThey behave exactly as floats, which is a poor implementation.\n\n"
    author: "s."
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-29
    body: ">Or someone else will stand up and write a kpart for it. :)\n\nSomeone wrote one for a SoC project, IIRC. Not sure the stability/how feature complete it is, but it does exist. I'm not sure if my memory is at all correct on this, but for some reason I also remember that some distro switched to using the KWebKit part as the default rendering engine for Konqueror (although, it might have been the *very* early version that pre-dated the SoC project).\n\nPersonally, I'm a rather big fan of WebKit (which was a good part of the reason I chose it for Foxkit), but I also really like KHTML and think it would be a shame if the development halted."
    author: "Kit"
  - subject: "Re: More WebCore-based improvements to KHTML"
    date: 2008-12-29
    body: "the webkit kpart that allows you to use webkit right inside of konqueror is located in kde svn under trunk/playground/libs/webkitkde\nI use it quite often and its very nice.  The problem I have with it currently is that only in Qt45 webkit will be much more mature and it will basically be the best browser engine around (together with the other webkit based browsers).\n\nJust installing it will allow you to select the webkit based engine via konquerors view menu.\n\nAnd yes, its still being developed and could surely need some more love to integrate stuff like kwallet."
    author: "Thomas Zander"
  - subject: "thanks!"
    date: 2008-12-27
    body: "Thanks Danny for getting these out.  I don't feel that the introductions are necessary, even though they are really nice :)\n\nkeep up the good work, and I hope the best with whatever busies you"
    author: "Vlad"
  - subject: "Nice Work"
    date: 2008-12-28
    body: "Thanks! Really enjoy these. Keep them coming! If we can catch up, I'm sure they'll get easier to write (And read ;D)"
    author: "Eric"
---
In <a href="http://commit-digest.org/issues/2008-10-26/">this week's KDE Commit-Digest</a>: A collection importer added to <a href="http://amarok.kde.org/">Amarok</a> 2, which allows importing statistics from Amarok 1.4 databases (as well as arbitrary external sources). Ability to disable the fullscreen thumbnail bar in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. Shadow brightness implemented for KWin-Composite effects. Support for animated icons across KDE. Support for output of MathML Presentation Markup in <a href="http://edu.kde.org/kalgebra/">KAlgebra</a>. Python language bindings for the <a href="http://edu.kde.org/marble/">Marble</a> library, whilst creating <a href="http://plasma.kde.org/">Plasma</a> DataEngines in Python reaches an almost-working stage. Option to force the number of rows in the Plasma "Taskbar". Start of work on KScoreManager. Import of a minimal KPart for the Palapeli game. Beginnings of filter matching for the syndication plugin (replacement for RSS plugin) in <a href="http://ktorrent.org/">KTorrent</a>. More WebCore-based improvements to <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Initial commit of an "autobrace" KTextEditor extension. Import of KSendEmail, a command-line tool to send email (using D-Bus connectors). The "Bomber" game moves from playground/games to kdereview, whilst Killbots moves out of kdereview into kdegames for KDE 4.2. Kephal (new screen management library) moves to kdereview. The refactored Plasma "System Tray" and "Taskbar" move into trunk in time for KDE 4.2.<a href="http://commit-digest.org/issues/2008-10-26/">Read the rest of the Digest here</a>.

<!--break-->
