---
title: "KDE Commit-Digest for 3rd August 2008"
date:    2008-08-19
authors:
  - "dallen"
slug:    kde-commit-digest-3rd-august-2008
comments:
  - subject: "Nice!"
    date: 2008-08-19
    body: "I'm excited about the improved Strigi, it seems that a lot of \"corner cases\" are being carefully considered.\n\nAnyone know if the Kicker fixes will make it in to Lenny?\n\nAnd, thanks Danny!\n"
    author: "T"
  - subject: "Re: Nice!"
    date: 2008-08-19
    body: "That would rather be in Sebastian's fork of Strigi, I believe? http://www.kdedevelopers.org/node/3573"
    author: "Martin"
  - subject: "Re: Nice!"
    date: 2008-08-19
    body: "> Anyone know if the Kicker fixes will make it in to Lenny?\n\nKDE 3.5.10 will be tagged this month. It's just a bugfix release, but I do not know if it is sufficient for Lenny which comes next month (hopefully)."
    author: "Stefan Majewsky"
  - subject: "Is that possible?"
    date: 2008-08-19
    body: "Thanks for the report, Danny, always read and always appreciated! :-)\n\nIs there a way to bind Grid usage to a middle button / wheel click, for instance?\nAnd is there a way to get Alt-Tab use a composited way of moving through windows, once it seems set to the old way? Once I requested that circulation was done over all windows in all virtual desktops, I could not set Alt-Tab to have a different behaviour than the old one, without any nice composited effects.\nLast question : where is located the configuration to use NTP now? Cannot find it anymore.\n"
    author: "Richard Van Den Boom"
  - subject: "Any news on the Akademy videos?"
    date: 2008-08-19
    body: "Will those be available soon? At the rate KDE is evolving they probably are only up to date for a few more days ;) \n\nBTW I hope they will be as good as the debconf ones. I find it amazing that volunteer projects produce way better videos than \"commercial\" entities like the Linux Foundation etc.\n"
    author: "Tom"
  - subject: "Really, I love opensource"
    date: 2008-08-19
    body: "Benoit's work on KDE\u00b7 kicker is just so wonderful... upstream devs are devoliping fulltime KDE4 but he's got to use KDE3, has some issues with it, has the knowledge and... patches pop up to fix those issue, because the code is opensurce. How great is that?\n:)"
    author: "Vide"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-19
    body: "Interestingly, the Nvidia driver problem seems to have been fixed just today. I cannot comment on that as my Nvidia card does not make any problems but: http://pinaraf.blogspot.com/2008/08/nvidia-driver-at-last.html"
    author: "Stefan Majewsky"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-20
    body: "hmm. for some cards. And it's beta."
    author: "ac"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-20
    body: "NVidia is a catastrophe. I was thinking that the driver situation is being improved until I bought an IBM Thinkpad R50e (old laptop) with an Intel card and compared it to my much more powerful PC which has an nVidia Geforce 6600 LE video card. The Think Pad has a little more that half of the power that my PC has but it beats it (the PC) by leaps and bounds in performance! I just couldn't believe my eyes. Smooth scrolling, YouTube videos play full screen without the slightest problem and a very fast KDE4. A performance that I can only dream of on my PC. All I have to say is, people don't touch nVidia if you haven't as yet."
    author: "Bobby"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-20
    body: "ATI: open documentation: yes - contributing open source drivers: no\nIntel: open documentation: no - contributing open source drivers: yes\nnVidia: open documentation: no - contributing open source drivers: no\n\nTake your pick. nVidia recently restated their position of providing proprietary drivers only, making the Nouveau reverse-engineering project more than they deserve."
    author: "Martin"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-20
    body: "nope, intel give out docs on 965"
    author: "sds"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-20
    body: "> ATI: open documentation: yes - contributing open source drivers: no\n\nAlso this is not true: they hired Alex Deucher, one of the developers of the open source driver.\nhttp://www.phoronix.com/scan.php?page=news_item&px=NjIwNg"
    author: "Diego"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-20
    body: "Yes, it actually looks like they will adopt the Phoronix driver or something like it at some point, which is great. But presently, the driver that they do support, and which supposedly is developed by their core team, is closed."
    author: "Martin"
  - subject: "Re: Really, I love opensource"
    date: 2008-08-21
    body: "Users don't care a red rat about hardware documentation, all they want is that it works! Take my old Thinkpad for example; I installed openSuse 11 with KDE4 recently, which was over in no time. The system booted and there it was! everything worked like a charm. 3D was activated and I had no need to bore around with drivers. That's what users want and what nVidia and ATI don't want to deliver to Linux so why should I give them my money? \nI am planning to buy a Dell at year's end and I have already decided that it has to have an Intel card."
    author: "Bobby"
  - subject: "Unbelievable work!"
    date: 2008-08-20
    body: "Wow! I can't wait to try Benoit Minisini's improvements on kicker ;-)\nThere are a lot of good improvements for people like me who are stuck on KDE3 for various reasons.\nCompliments guy!\n"
    author: "superfan!"
  - subject: "Kmail"
    date: 2008-08-20
    body: "Does anyone have problems with Kmail?  I write my e-mails, but when I look at what is sent all spaces and lines are removed from the e-mail and it is all just jumbled together.  It's with the new kmail in kde 4.1.  I've just switched to thunderbird just so people can actually read what I write, lol."
    author: "R. J."
  - subject: "Re: Kmail"
    date: 2008-08-20
    body: "I haven't received any complaints yet. ;-)"
    author: "Jonathan Thomas"
  - subject: "Re: Kmail"
    date: 2008-08-20
    body: "It actually sends the mail as HTML. I'd been pissed about it till it got sorted out with the newer factory packages in opensuse.\n\nAnother kmail bug that bugged me bigtime was that it put the cursor in when replying, for top posting. I ended up editing the template and it got sorted out too."
    author: "Mrugesh Karnik"
  - subject: "Re: Kmail"
    date: 2008-08-20
    body: "Why don't you file a bug report?"
    author: "Bobby"
  - subject: "Re: Kmail"
    date: 2008-08-20
    body: "I'm not really sure how to, what information is required etc.  Last time I filed a report on a bug I was told I was missing something, but when I asked they wouldn't tell me what I needed to install to do a proper bug report, so I'm not that sure on how to do one, what debugging stuff I need installed etc."
    author: "R. J."
  - subject: "Eye Candy"
    date: 2008-08-20
    body: "The \"Magic Lamp\" effect is now in KWin.  Whoop-dee-do.  \"Weather\" Plasmoid.  Wow.  (yawn)\n\nHow about some usefull Eye Candy like, oh, I don't know...\n\nauto-hide kicker or a desktop background when \"Old School View\" is enabled???\n\n(sorry, this is just getting on my nerves a bit now...)\n\n"
    author: "Xanadu"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "\"The \"Magic Lamp\" effect is now in KWin. Whoop-dee-do. \"Weather\" Plasmoid. Wow. (yawn)\n \n How about some usefull Eye Candy like, oh, I don't know...\n \n auto-hide kicker or a desktop background when \"Old School View\" is enabled???\"\n\nAs has been stated over and over again, those are both in progress.\n\n\"(sorry, this is just getting on my nerves a bit now...)\"\n\nThat's no excuse for shitting on someone else's work."
    author: "Anon"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "Well said."
    author: "Jan Ask"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "These features are waiting for someone like you to be implemented. Show us the code."
    author: "Vide"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "How many people have been begging for the above mentioned things vs. a \"magic lamp\" effect.  I'm simply saying the priority list needs some serious adjusting...\n"
    author: "Xanadu"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "If you want to know: there was a feature request for Magic Lamp: http://bugs.kde.org/show_bug.cgi?id=167387\n\nI implemented this feature because *I* wanted to have it. Because it looks cool. I used it in Compiz, I know it from MacOS and *I* wanted to have it for KWin.\n\nIf you want to know even more: It took me about three to four hours to code this effect. I don't think that your long missed panel hiding can be implemented in three hours. If it would be that trivial it would have been in 4.0.\n\nAnd just that you understand it. I'm working on KDE in my free time. I'm not paid for it (I do not count my Summer of Code project) and I am not a Plasma dev (yes not all KDE devs are working on Plasma). I think you are not in any position to tell me what I should do in my free time and what I should work on. So don't tell me that I have my priorities the wrong way."
    author: "Martin"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "The Magic lamp implementation is nice but it looks quite different from that of Compiz-Fusion. Was that on purpose or will changes be made?"
    author: "Bobby"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "Does it have to look the same? If you have ideas to improve it please tell us.\n\nI do not know exactly how the effect looks like on MacOS or on Compiz Fusion. I don't have an Apple and Compiz refuses to work properly on my machine."
    author: "Martin"
  - subject: "Re: Eye Candy"
    date: 2008-08-22
    body: "I only made the comparison, not meaning that one is better or worse than the other. The Compiz-Fusion Magic Lamp has a sort of winding effect when minimizing and it kind of sucks in but the KDE4 magic lamp is more like a minimize zoom effect with a magic lamp shape.\n\nIf you could get Compiz-Fusion properly installed then you could use the Compiz-Fusion tray icon to change to Compiz or KDE4 in order to make a comparison."
    author: "Bobby"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "> How about some usefull Eye Candy like, oh, I don't know...\n\nsomeone's work on kwin and another person's work on weather information has nothing to do with the plasma workspace. there are people who like and enjoy what they are doing, and your attitude towards their efforts really stinks.\n\n> auto-hide kicker or \n\nnot written yet; will be there for 4.2.\n\n> a desktop background when \"Old School View\" is enabled???\n\nalready in svn for 4.2.\n\n> (sorry, this is just getting on my nerves a bit now...)\n\nas the person closest to the center of the target you are spitting at, i don't accept your apology. if you were truly sorry, you would take a different approach to it. and indeed: half the things you complained about are already fixed, and your comment was totally off topic in this story.\n\nso you can take your apology back as it has no value here."
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "> half the things you complained about are\n> already fixed, and your comment was totally\n> off topic in this story.\n\nI don't feel that it is off topic.  The topic is about what has been written for the next \"release\" of KDE.  I have been reading your blogs (from time to time - I don't get to check it every single day I'm sad to say) and you have indeed touched on the very things I (scathingly - it seems) brought up.  \n\nAre they in SVN?  Well, I can say as of this just past weekend (Aug 17) they weren't.  I compiled SVN on my Gentoo machine.  There was an issue with the xine-plasma-(something) not compiling that was updated later that afternoon (east coast, USA). That's about the time frame I tried it.  Then I see pretty effects for KWin beating out things us lowly (l)users have been bringing up endlessly for over a year.\n\n(there are some serious issues with the i865G Intel vid chip in this machine and the transparency of the Folder View \"window\" (plasmoid), but this isn't a bug tracker, I understand that.)\n\n\n\n> so you can take your apology back as it has no value here.\n\nPerhaps.  I see your point either way.  I respectfully agree to disagree.  I haven't been using KDE since 0.8 or 0.9 because I don't like it.  I simply don't think eyecandy should beating out functionality on the priority list especially when that functionality has been being begged for.  If that is an \"attack\", well, I can't do anything about that.  This isn't back-pedaling.  This is explaining, nothing more."
    author: "Xanadu"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "I'm kinda at a loss of words for this besides \"Dude! Seriously?\" \n\nWhat kind of reaction do you expect from useless whining? Should Aaron and other plasma devs suddenly go \"Gee, this guy is asking for a hide-able kicker. Why didn't we think of that?!\"\n\nWhat you did was not \"explaining\" whatever you think that means. It was a silly attack with no value added to the discussion. This is a story about a single weeks developments in KDE. You show up and start yelling for random crap that has nothing to do with the topic. Someone in KDE (looks like Shawn Starr and Teemu Rytilahti thanks guys!) thought they would like to make a weather applet work. You show up and start whining and complaining. \n\nIf you feel so strongly about it, start contributing yourself. You may have gotten off on the wrong foot, but I'm sure your patches or contributions would be welcomed. Otherwise it might be best to stop bugging people and making them reply to flame posts on the internet, and get back to coding and wow'ing the rest of us. "
    author: "xian"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "> There was an issue with the xine-plasma-(something)\n\nhm. there is no xine-plasma-<anything> in kdebase or plasma-addons ... perhaps you mean phonon-xine?\n\n> Then I see pretty effects for KWin beating out things us lowly (l)users have been bringing\n> up endlessly for over a year.\n\nbut you see, that's not what's happening. firstly, users have not been requesting some of these things for over a year, such as backgrounds for folderview, as those things haven't existed that long. secondly, feature requests that have been made have been getting attention paid to them. people moaned endlessly about not being able to resize a panel, and rightfully so; we said we'd implement that and we did. that's one example of literally dozens such cases between 4.0 and 4.1. we've typically handled the oldest and most painful issues first, as well.\n\nand we continue to improve things, such as making wallpapers plugins so that you can use wallpaper $FOO with containment $BAR. it has nothing to do with kwin effects, and the progress in plasma is actually even faster than what we're seeing in kwin (which is moving at a nice pace as well).\n\n> there are some serious issues with the i865G Intel vid chip in this machine\n\nthose are issues with the driver, not plasma or kde4. these things are getting sorted out upstream as we actually exercise these features in the drivers and then reporting problems to the appropriate upstreams. other than doing that, there is nothing we can do but be patient. the alternative is to live with a desktop that forever looks and behaves like its 1995 all over again.\n\nthankfully there are many chipsets which work wonderfully already, and many more are being fixed. the latest nvidia beta drivers seem to resolve the issues people were seeing on their hardware, for instance.\n\n>  I simply don't think eyecandy should beating out functionality on the priority list\n\nit isn't.\n\n> If that is an \"attack\"\n\ni don't think that's an attack at all, because you shared your thoughts openly and with thoughtfulness. contrast to the tone and content of your original rant; the fact you felt the need to apologize says it all imho. but with your reply here you showed that you can say the exact same things without being a dick about it, and that goes a long ways to having a rational conversation."
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-22
    body: "> perhaps you mean phonon-xine?\n\nYes.  Still, a working rev was checked in not long after I tried.  I like Open Source. :)\n\n\n\n> (in reference to my rant about the new kicker)..it has nothing to do with kwin\n\nPoint made, Aaron.  I hadn't looked at it that way.\n\n\n\n> those are issues with the driver, not plasma or kde4.\n\nIndeed.  I just (not being a codeist) have a hard time understanding why I can play RTCW or Q3A on the box, but it can't resize/move a transparent window.  I know, I know; that's for me to figure out.  Devs don't \"hand-hold\" the users.\n\n\n\n> i don't think that's an attack at all, because you shared your thoughts \n> openly and with thoughtfulness. contrast to the tone and content of your\n> original rant; the fact you felt the need to apologize says it all imho. but\n> with your reply here you showed that you can say the exact same things\n> without being a dick about it, and that goes a long ways to having a rational\n> conversation\n\nYou are a stand up guy, Aaron. Top-notch."
    author: "Xanadu"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "I'm there with you. \nAs it currently seems there is too much emphasis in the development for new eye candy which really is of limited use it it doesn't outright diminish the value of the whole environment (if regular non-plasmoid-malarchy applications get dropped in favour of this).\nI still don't get the need for new plasmoids especially as they disappear completely behind the used applications and thus are worthless in a non \"boah ey, eye candy\" environment. Useful things like different backgrounds on different virtual desktop screens still are missing sorely so that productivity wise KDE 4.1 still is lightyears away from being a valid challenger to KDE 3.5 in terms of usefulness... \nAnd the more plasmoids take over from formerly useful gadget windows which you could raise above the rest of the other windows the further it moves away from ever getting to this state!\nIf you look at the applications, sometimes I think the developers have lost the plot - period. Take gwenview. Formerly a nice configurable, fast image browser on KDE 3 it now is a mess of reduced options slower than ever before (even after the last efforts to regain a bit of speed). Worse still the image thumbnail browser is now fixed horizontally which takes up so much space vertically where screen estate comes at a premium, especially as wide screen displays are getting widespread.\nI have been using KDE from the very beginning, KDE was the reason for our company to go with Qt for multiplatform development - on my recommendation. Almost 10 years have passed since then and given the state of Qt I am slowly but surely revising my opinion. "
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "*sigh*\n\n\"As it currently seems there is too much emphasis in the development for new eye candy which really is of limited use it it doesn't outright diminish the value of the whole environment\"\n\na) Just because *some* new contributors, who would probably not contribute to KDE at all if they didn't work on what they're working on now, happen to work on eye-candy related stuff, does not mean there's some shadowy conspiracy to abandon functionality in favour of eye-candy.\n\nb) Eye-candy is generally configurable: in fact, the two examples the original poster took issue with are not only configurable but are disabled by default! So I don't see how this diminised the \"value of the whole environment\".\n\n\n\"(if regular non-plasmoid-malarchy applications get dropped in favour of this).\"\n\nAny examples of this happening?\n\n\"I still don't get the need for new plasmoids especially as they disappear completely behind the used applications and thus are worthless in a non \"boah ey, eye candy\" environment.\"\n\nYou don't see the need for menus, panels, system trays, task managers, the Jobs server (which I personally consider rather ugly, but very useful) the folderview, etc? And hint: At least two of the plasmoids I've just mentioned do indeed display over other windows.\n\n\"Useful things like different backgrounds on different virtual desktop screens\"\n\nI'd consider this an absolute prime example of \"useless eyecandy\".  A lesson, here: one man's useless eye-candy is another man's essential feature.\n\nI don't use Gwenview so I won't comment on that.\n\n\"I have been using KDE from the very beginning, KDE was the reason for our company to go with Qt for multiplatform development - on my recommendation. Almost 10 years have passed since then and given the state of Qt I am slowly but surely revising my opinion.\"\n\nAnd what \"state\" is Qt in that's so terrible compared to 10 years ago?"
    author: "Anon"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "you took the words right out of my mouth.\n\nthe idea that wallpaper-per-desktop impacted productivity particularly made me smile."
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "Playing devil's advocate here as I don't really use multiple desktops, but I can see where having different wallpapers on your different desktops would help you differentiate the desktops at a quick glance.  One could argue that it does affect productivity in that sense."
    author: "T. J. Brumfield"
  - subject: "Re: Eye Candy"
    date: 2008-08-22
    body: "And one maximized window remove the usefulness of it, reducing it to nothing more than eye candy. As opposed to quick glance at the desktop pager, which always work. By highlighting the active one, helping you differentiate the desktops."
    author: "Morty"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "> Useful things like different backgrounds on different virtual desktop screens \n\nSo, how is this \"useful\" and not just \"eyecandy\" by your reckoning?\n\n> And the more plasmoids take over from formerly useful gadget windows which \n> you could raise above the rest of the other windows\n\nAre you aware you can use CTRL+F12 to bring the Plasma desktop to the foreground?\n\n> given the state of Qt I am slowly but surely revising my opinion. \n\nWait, are you complaining about Qt or KDE now?"
    author: "Paul Eggleton"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "Not to side with the ranting people, but Ctrl-F12 is hardly an obvious or well-known shortcut! Nor are users told of this shortcut at any point.\n\nThere should be a \"Show Desktop\" button on the panel."
    author: "Tim"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "There is already one you can add in 4.1.\n\n\nand another one for show Dashboard...\n\nyou have the choice :)"
    author: "lmarteau"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "> disappear completely behind the used applications \n\nthe dashboard view (Ctrl+F12), panels and plasmoidviewer (which shows applets in their own window; will eventually be integrated into plasma itself, though i'm not sure when yet as we have other things to do) negates this point.\n\n> the more plasmoids take over from formerly useful gadget windows\n\nthere's something fundamentally missing in this viewpoint, which becomes obvious when you look at amarok2: it uses plasmoids extensively within its window. it's not about \"taking over from formerly useful gadget windows\" at all, but rather providing a better way to create such things that can be widely used. your \"useful gadget windows\" were pretty much destined to remain their own little windows forever; as a plasmoid they can be their own little window, part of the desktop, on a panel or in other apps that care about them.\n\nin that sense, they are a bit like kparts, but with a completely different scope and purpose.\n\n> different backgrounds on different virtual desktop screens\n\nwe'll be doing one better than that: different widget layouts for different virtual desktop screens.\n\nbesides the fact that \"different backgrounds\" as done in every other shell out there doesn't work at all for composited window managers (think of desktop cubes and grids), it would be fairly easy to add this to the Image wallpaper plugin if you so desired.\n\n.. not to mention we're doing a lot of things now with wallpapers that will likely be in 4.2 like Edje driven papers that are generally pretty cool looking. ooops, more eyecandy! ;) this is being done by people who never contributed to KDE before, though, who are doing this as an experiment on using Edje in KDE. we as users get to benefit from that experimentation. nothing in this case is lost (developer time or effort, usability, etc) even though the eye candy factor goes up yet again. in fact, we get more interested developers out of the deal.\n\nso yes, we don't have per-desktop wallpapers, but it's easy to implement and we've done (and are continuing to do) a hell of a lot more than that which makes that feature look really rather trivial.\n\n> productivity wise\n\nthe only connection between desktop wallpapers and productivity i can think of is that you might use it as a visual hint as to what desktop you are on. but that seems like a real stretch, so i call BS on this point.\n\nif you wish to talk productivity ... as opposed to \"different wallpapers per desktop\", apparently most people are interested on working on really useful and needed things like improving the system tray so that it doesn't suck anymore, something that has languished for *years* and which, only because of the approach we're taking in plasma, is finally getting attention and work done on it. that sort of thing can really improve productivity.\n\n> I think the developers have lost the plot - period. Take gwenview.\n\ni couldn't stand the rather arcane interface of gwenview previously; it also didn't fit at all on smaller screens (e.g. the EEE PC). the new gwenview has all the features i ever used in a much nicer package. it's something i'm not only more comfortable using, but also more comfortable giving to others to use as well as it looks far more polished and is easier to figure out how to use now.\n\nso maybe KDE has moved on from your personal needs/viewpoints-on-the-world. that would be unfortunate, but KDE can't make everyone happy all the time. what we can do is make more people more happy, and it's pretty difficult to look at something like gwenview in kde3 and gwenview in kde4 and not come to the conclusion that that is what the developers are doing."
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "I agree in some of your points and I can add a whole list of others.\n\nBut the thing is:\no Trolltech/Nokia pushes KDE to mobile devices, where the priority of a *NIX-like desktop behavior is irrelevant.\no I accept that some innovations may cost features (If KWin-composite further inspires from MacOS, I do not see any room for different wWallpapers on each desktop)  \no people who had a view on the whole in the past nowadays work on different places within KDE (most on Qt at Trolltech from what I've learned)\no developers come and disppear. New generations have different interests and different views on quality management, maybe even different definitions on that. \no KDE can not reliably supply the service that Microsoft and Apple can offer - we can be happy that they do an awesome job on that. \n\nTherefore, I do accept that there are many unpolished things in KDE, though I am very unhappy with this. It had been better in the past.\n\nIn fact, if I consider most new effects in kwin (to come back to the original topic), most of them are nice to look at, but without any use,  for example the window flip effect on \"Alt-Tab\". \nI always used Alt-tab for fast finding windows on a desktop (for example when the taskbar is crowded or if a window title was too long to be displayed in the taskbar). But with the new effect I am slower than before. I can only see the current selection, the other windows are visually transformed or invisible at all. That means: The effect looks nice and shows that KDE programmers have the same skills as Microsoft developers, but the work which has gone into that just adds some kilobytes to the download volume and some extra lines of source (which must be maintained!) Whatever: Send a bug report on that and hope that somebody improves this.\n"
    author: "Sebastian"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: ">In fact, if I consider most new effects in kwin (to come back to the original topic), most of them are nice to look at, but without any use, for example the window flip effect on \"Alt-Tab\". \n> I always used Alt-tab for fast finding windows on a desktop (for example when the taskbar is crowded or if a window title was too long to be displayed in the taskbar). But with the new effect I am slower than before. I can only see the current selection, the other windows are visually transformed or invisible at all. That means: The effect looks nice and shows that KDE programmers have the same skills as Microsoft developers, but the work which has gone into that just adds some kilobytes to the download volume and some extra lines of source (which must be maintained!) Whatever: Send a bug report on that and hope that somebody improves this.\n\nNobody forces you to use these effects. We did not replace the old behaviour. Just disable the effect and you have the normal tabbox. Currently there are four effects for alt+tab in trunk. You can choose which one you want to use or none at all. Where's the problem? Btw. you can turn off the animations in flip and coverswitch effect. So the effect can be as fast as without compositing if you want.\n\nThe argument of some kilobytes to download is IMHO not valid. The effects are just 378 KB all together in 4.1. Also lines of code is just BS. If that's an argument we should stop implementing features as this add new lines of code and needs to be maintained."
    author: "Martin"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "> Nobody forces you to use these effects. We did not replace the old behaviour.\n\nI know. I actually use the \"old\" behaviour.  And I said, I accept it, but I am unhappy about the fact that it has been integrated at an early development state. And as soon as unfinished things are integrated  it sometimes happens that they won't be improved any further. \n\n> Btw. you can turn off the animations in flip and coverswitch effect. So the effect can be as fast as without compositing if you want.\n\nI am not talking about the speed of the animation. The problem is: Using the very old \"menu\" that appeared on KDE 3.5 I obtained more information (!) compared with any existing effect in kwin-composite4.1 (even without any effect the menu contains less information compared with 3.5). That means: \"Using\" the \"alt-tab\" feature is less fast compared with 3.5! \n\n> The argument of some kilobytes to download is IMHO not valid... \n\nThis is nut-picking. When you play this card I ask you why KDE includes features that do not create any value? That's my only point.\n\nSee, I agree that playing around with neat 3d effects is required and even makes fun. People should do that. But from my user's perspective, it would have been better if these effects where done 'right', that is if they do not only present the new abilities of Qt/KDElib/OpenGL/whatever, but if they provide eye-candy AND improved navigation.\n"
    author: "Sebastian"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: ">See, I agree that playing around with neat 3d effects is required and even makes fun. People should do that. But from my user's perspective, it would have been better if these effects where done 'right', that is if they do not only present the new abilities of Qt/KDElib/OpenGL/whatever, but if they provide eye-candy AND improved navigation.\n\nSo please tell us how to do them *right*. I'm always glad for ideas how to improve the effects. If you have an idea how to do the perfect alt+tab window switcher please tell us, so that we can implement it. The right place to do it is http://bugs.kde.org/."
    author: "Martin"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "The flip effect alt-tab switcher *is* improved navigation: by being able to see the contents of each window, it provides more information for the task. And I like how it looks.\n\nAs previous replies have said, it is also only one of a choice of 4 options.\nGood developers do not only take into account their personal preferences and usage when designing new features...\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "I see contents but lose context. Lets see example.\n\nAt the moment I have four windows on desktop, when hitting alt-tab in 3.5 I see:\n\n- Eye Candy - Konqueror\n- hp12c.pdf\n- mikolaj@localhost: ~/kget - Konsole\n- E-Mail - Kontact\n\nWhat this list is giving me?\n\na) I know whole context of my current work (I use one desktop to group all relevant tasks - not rare scenario) \nb) I know that I need to hit Tab two times to get to E-Mail\nc) I can even... *gasp* use mouse to quickly select window I want to get - useful with longer lists"
    author: "m."
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "Please tell me where's the difference to KDE 4? You have exactly the same features if you do not use compositing or even if you use compositing and disable all effects for alt+tab.\n\nIf you use the box switch effect you have exactly the same with one difference: You see a thumbnail of the window instead of the application name. Personally I think that is a big improvement.\n\nSo I do not get your point. If you know the perfect effect for window switching please send us the patch."
    author: "Martin"
  - subject: "Re: Eye Candy"
    date: 2008-08-25
    body: "I agree with parent - the alt-tab switcher did get worse. I like the coverswitch one more than anything, but the box switch lost something. It used to show the titles of the windows horizontally, a huge improvement over what MS did and does. The thumbnails dont replace this as they're too small. It would rock if you could have the vertical layout with thumbnails, best of both worlds."
    author: "jospoortvliet"
  - subject: "Re: Eye Candy"
    date: 2008-08-25
    body: "Hmm, didn't get notice about reply - you probably also don't get it.\n\nNevermind: this is near perfect replacement of old alt+tab:\n\nhttp://www.undefinedfire.com/kde/new-present-windows-layouts/"
    author: "m."
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "> Trolltech/Nokia pushes KDE to mobile devices, where the priority of a *NIX-like \n> desktop behavior is irrelevant.\n\ni've read this theory for a while now, and it's really not particularly true. Nokia is certainly encouraging work in the mobile space, but Trolltech never really did and at least my interest predates the current state of things. *shrug*\n\n> people who had a view on the whole in the past nowadays work on different places within KDE\n\nthere are still people with a view on the whole who work directly on KDE\n\n> New generations have different interests and different views on quality management,\n> maybe even different definitions on that.\n\nthat's quite true.\n\n> KDE can not reliably supply the service that Microsoft and Apple can offer\n\nwell, we have a different scope (both in terms of products and user support); that said, i think we've done pretty well going from nothing less than 12 years ago to where we are now. i don't think either Microsoft or Apple are exactly delivering some mythically great service, either (yes, not even Apple: MobileMe and phones that don't work reliably being two recent examples). *shrug*\n\n> I do accept that there are many unpolished things in KDE, though I am very unhappy with this.\n\nyes, and there always has been. thankfully there is more and more completeness, even though sometimes it means we have to dig deeper into the stack to create tools that didn't exist.\n\n> It had been better in the past.\n\nthere are some regressions being suffered right now, but in many areas things are much better now than they ever have been. the latter dwarfs the former at this point, and the former is getting smaller by the day.\n\ni don't ever remember having as nice a manager for hotplug devices, as nice an image viewer, as good a konsole, as friendly a file manager, a desktop globe, games as pretty and fun to play, a run dialog as useful or more.\n\nthere are still a few things i miss, but those seem to be on their way. and a lot of new and very useful things are also headed down the pipe still, so ... maybe it's a matter of personal persepctive on things, but i'm rather happy with the direction of things, and i find going back to earlier versions of kde a bit awkward now ... just like i used to with every successive release of kde2 and kde3.\n\n> most of them are nice to look at, but without any use,\n\nand yet several of them are very useful.\n\ndesktop grid is a great way to get a fast overview of all the virtual desktops and move things between them ; coverflow switch lets me see the contents of at least three windows, i use it often to just \"peek\" at other windows before returning to the original; the zoom is great for examining teeny pages on the web or graphics (great for accessbility as wel); the mouse marker is a godsend when i can't find my mouse cursor (on a projector screen, in sunlight, or other low contrast situations); ... i could go on.\n\n\n"
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "> i've read this theory for a while now, and it's really not particularly\n> true. Nokia is certainly encouraging work in the mobile space, but Trolltech\n> never really did and at least my interest predates the current state of\n> things. *shrug*\nWell we as paying Qt developers have just had the unfortunate task to fill out a survey comissioned by them which spoke differently. There all the answers possible were pushing into the direction of abandoning the desktop application due to a fractioned market and moving to a more glorious future in the mobile market. "
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "i personally haven't seen that survey, but that's still a rather separate topic from the Nokia<->KDE relationship."
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "I think the discussion is a matter of perspective. There are different kinds of people with different needs and different kind of working styles. \n\nPeople can be infotainment users, students, professional programmers, multimedia workers or KDE application developers. Each of those categories  have different requirements for KDE. Working style is an independent dimension where some prefer few (and often just one) desktops with a lot of small windows, while others have twelve desktops with two top-bottom Konsoles lined up next to each other.\n\nIf we look at the discussions about the KDE4 series the different perspective come into play. The infamous Kicker auto-hide functionality for example: The many-small-windows crowd like to have the panel displayed permanently while it is an absolute show-stopper for the top-bottom-konsole people. Another example is the whole plasma thing: The top-bottom-konsole group couldn't care less while it is a revelation for the others.\n\nSo one group sees KDE4 as a great leap forward in technology and invention while another group sees it as step backwards. And they're both right.\n\nAs I see it the KDE4 developers have failed to recognize that there are other people than themselves with different needs and different workhabits. Please note that I'm not saying that the KDE programmers are obligated to satisfy those people. I'm saying the the KDE programmers fail to recognize the <i>existence</i> of those people and the validity and seriousness of their complaints.\n\nI infer that from how the auto-hide feature have been omitted from KDE4, then delayed to 4.2 and still not even in the repository. This is a hands-down showstopper to many people but even after so much complaining it has got nowhere. It beyond me why some people didn't say \"Hey, this is a very important feature for a lot of people, lets prioritize it\" and have those extra users to evangelize KDE4. It would have been tactically much much smarter than the \"Its not important\" or the \"Your problem doesn't exist\" attitude accompanied with a really flashy demo of the latest plasma whiz-bang thing as salt in the wound. In many peoples mind this becomes: \"KDE3 worked nicely, then this plasma-thing came around and screwed it up\". And they're actually right as seen from their perspective.\n\nAs you may guess, I'm a top-bottom-konsole guy. I'm still using KDE3 and will continue to do so until the showstoppers have been fixed. What worries me though is that the current breed of KDE developers do not seem to pay much attention to the needs of people like me as witnessed by the auto-hide thing. Which is basically ok because thats how the open-source model work, but there is also an obligation between the generations of programmers to not to break the work of their forebearers. This is how it works for almost all open-source projects: What if an Apache 4.0 didn't support PHP ? \"It will be implemented in 4.1\". \"Oh, wait some other features are more important so we push it to 4.2\". \"If it is in the repository ? No, but here is a demonstration of speech synthesized error messages in 3D Dolby stereo. Flashy right ?\"\n\nSomeone, some years ago, took the time and effort to make the auto-hide feature work. You (or someone else) broke his work.\n"
    author: "Claus Rasmussen"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "Ssssssssssshhhhhhhhhhhhh!!!!\n\nYes, you're frustrated that you can't move to KDE4 yet and all the great things that brings. You want your showstoppers fixed. You want the features you see people complaining about being missing to be prioritized. We can see that.\n\nThat doesn't mean you can throw around remarks about breaking the work of previous maintainers (It was probably Aaron who implemented autohide anyway...). Other features have been made higher priority. If they weren't people really would thing plasma is all about clocks. I know you recognize that progress too, which is good.\n\n-- the current breed of KDE developers do not seem to pay much attention to the needs of people like me\n-- I'm saying the the KDE programmers fail to recognize the <i>existence</i> of those people\n-- the \"Its not important\" or the \"Your problem doesn't exist\" attitude accompanied with a really flashy demo of the latest plasma whiz-bang thing as salt in the wound.\n\nThis stuff will only serve to demotivate the people who will work on getting KDE4 to the point that you can use it. Of course KDE developers are considerate of users with various needs. \n\nThe discussion is a matter of perspective indeed. You perceive that you're being over looked, but you're not. You might think that you're being helpful bringing up an issue that you see, but you've got the wrong place. Writing stuff like the above is unhelpful and really misrepresents the the attitude of any KDE devs I've encountered.\n\nThanks, \n\nSteve."
    author: "Steve"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "> that there are other people than themselves with different needs and different workhabits\n\nit's quite the opposite: we've realized that there are lots of people who aren't like us and who have different needs and different workhabits and are making kde more useful for them instead of just keeping it all to ourselves.\n\nthe fact that \"people who don't work like me\" outnumber me by probably a few million to 1 is a big part of the motivator for this.\n\n> I'm still using KDE3 and will continue to do so until the showstoppers have been fixed.\n\nthat's perfectly fine, of course. i'd just make one small adjustment to your rather self-centric statement: you will continue to do so until things that are showstoppers *for you* have been fixed.\n\nremember how you talked about other people with other priorities and workflows? it goes both ways.\n\n> Someone, some years ago, took the time and effort to make the auto-hide feature work.\n\niirc it was Matthias Ettrich who wrote the original autohide code in kicker; either him or Matthias Elter. there was a big comment in the code saying (paraphrased) \"be very careful if you touch this, there's lots of magic here that is easily broken!\"\n\ni know about that comment because i maintained that code afterwards for *years*. that included fixing bugs, improving performance and adding features to that code.\n\n> You (or someone else) broke his work.\n\ni think you're lacking a proper history of the code base when you make such statements. \n\nhere's another one: systray icon hiding. i implemented that in kicker. it's likely coming back in 4.2. so in 4.0 and 4.1 i broke someone else's work! oh wait, that someone else's work was mine.\n\nso step back and think about *why* i might be doing that. it's not because i like regressions; in fact, it's because i never want to have to rewrite this stuff ever again (where \"ever\" means ~10 years). sadly, kicker's design was not well suited for what we needed it to become nor well suited to code re-use. these are things we are consciously addressing. it means some short term pain, but that pain is getting less and less by the week because we do care about it and we are fixing things.\n\neventually there will be none left, feature parity and then some will have been reached and people like yourself will migrate to kde4 joining the rest of the user base.\n\nwhich is to say, i don't see what you're going on about in the least."
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "I like the interesting and polite responses you give - you obviously have a thick skin. Thanks!"
    author: "Impressed"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "if we reply to your comments, we get bashed for not getting features done when you want them.\nif we ignore you and try to get code written (and oh god is there ever a lot to be written), we get bashed for failing to recognise your existence.\n\n*sigh*\n\nI know you mean well, but... seriously, what do you expect here? we're not miracle workers. we can't write every feature all at once. developers can't be reassigned like litte cogs, either. most of us aren't even paid for this. I'm going back to school in september, and it's going to take effort for me to make time for hacking on plasma; you're not making me want to make that effort. and I'm not even expecting to have time for fun plasmoids or \"eye-candy\" myself until next year or later... there are some useful little plasmoids in my head I'd really like to have, but I haven't written them because I'm trying to do stuff that's useful to a wider audience, like keyboard accessibility and making the ZUI work."
    author: "Chani"
  - subject: "Re: Eye Candy"
    date: 2008-08-22
    body: "I think that when dealing with the FOSS world what you need to keep in mind is: \"Don't attribute to malice that which can be adequately explained by lack of resources.\"\n\nRome nor KDE were built in a day. patience.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "Aren't you guys aware that you can just turn off composite if it doesn't suit your needs and the use KDE 4.1 more or less like you used KDE 3.5.X?\nIt's like the guys complaining that you can't run KDE 4.1 on some NVidia cards and thus you have to stick with KDE 3.5. Well, turn composite off, since you won't have one with KDE 3.5.X anyway, and enjoy KDE 4.1 other improvements instead of complaining.\nI find it extremely distasteful that so much work and improvements are discarded just because some small feature is missing here and there, as if it was not possible to endure that for some time, until it's implemented.\nAnd please stop all this \"it was better before\" crap. I remember the KDE 3.0 and 3.1 days and it was not exactly rosy. KDE 4.1 already has tons more features and crashes a lot less than 3.1 did for me.\nYou guys just act as spoiled children."
    author: "Richard Van Den Boom"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "I think the whole point is that you can _not_ use KDE 4.1 like you can KDE 3.5. And no, I will not enumerate the regressions again, but look at the toolbar, panel/desktop, ark, konsole, kio, etc. entries at bugs.kde.org.\n\nKDE 4.2 will be improved, sure, but I doubt that it will fix all the feature regressions. And unless Trolltech fixes the regressions from Qt 3, KDE 4.x will never be the same experience as KDE 3.x.\n\nBy the way, I am using shadows (compositing) with KDE 3.x since years. It may be an openSUSE addition, but it works, at least for transparent windows and shadows.\n\nKDE 4.1 is thousands steps forward, but one step back, and if there are people who are affected by this little step, why are they told they are wrong?"
    author: "christoph"
  - subject: "Re: Eye Candy"
    date: 2008-08-22
    body: "> KDE 4.1 is thousands steps forward, but one step back, and if there are\n> people who are affected by this little step, why are they told they are wrong?\n\nYou're not being told you are wrong. You're being told to wait until the regressions are fixed. If you like KDE 3.5 why not stay with it in the mean time?"
    author: "Paul Eggleton"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "You're a little \"bizarre\", to say the least. First, you sya that plasmoid are useless because they are always covered by other windows. Then, you complain about the lack of a feature like \"every virtual desktop with its own background\". Wait but... isn't the background, just like plasmoids, covered all the time by other windows? So, why do you want different backgrounds (or a background at all)?\n\nBe coherent, please, you won't look like a complete fool as you're doing right now."
    author: "Vide"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "That has to do with the immediate way of recognizing the current desktop screen - most of the time there is a small area of the background not obscured by windows. If you are working with several in parallel for different tasks then having such an immediate recognition is imperative. Taking this feature away (because it doesn't fit into the 3D sluggishness that desktops now encumbers) is diminishing functionality. \nAnd switching ALL plasmoids to the front really is a sorry excuse for not having them properly managed in a window list alongside the \"proper\" windows. So everything that get's implemented as a plasmoid is something that needs reimplementing for a productive environment where the user is able to control the layering (by bringing windows to the front)! \nSo in fact I'd rather have a traditional desktop with all the small tools which I can place and manage in a traditional way instead of having two layers which have disjunct managing interfaces! The latter in fact for me is a pure usability nightmare. Try to explain one new interface to a newbie and you have a hard time, try to explain to him that he needs to master two is calling for trouble."
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "you're not required to use plasmoids...plus if you stick plasmoids on the panel they stay there AND are visible all the time *gasp*"
    author: "txf"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "> because it doesn't fit into the 3D sluggishness that desktops now encumbers\n\nother things were prioritized in front of it because it doesn't fit with modrn window managers, yes. but it can still be implemented.\n\n>  So everything that get's implemented as a plasmoid is something that needs reimplementing\n\nthat's incorrect.\n\nthere's already plasmoidviewer that puts any plasma widgt into a window of its own. so any widget can already be used as a stand-alone application with no extra work.\n\neventually we'll be integrating this directly into plasma so you can detach random widgets into their own windows. this feature is wanted but not yet schduled.\n\n(btw, i already noted this in a comment above ... )\n\n> Try to explain one new interface to a newbie and you have a hard time\n\noh boy. the \"think of the children, er, newbies!\" argument ... i suppose you've done some user testing then, or is this hypothisizing on your part?"
    author: "Aaron Seigo"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "> oh boy. the \"think of the children, er, newbies!\" argument ... i suppose\n> you've done some user testing then, or is this hypothisizing on your part?\nI have been there as I support quite a few friends and relatives on linux desktops. I showed them the eye candy (because they were longing for something looking good with vista looming around the corner) but they outright didn't understand the disparity between plasmoid and normal window. So yes that problem is existing and it has stifled the acceptance of KDE 4.1 for quite a few installations, because they weren't able to use it after some hours of playing around with it - so I reverted to KDE 3.5 on their systems. Those people aren't stupid but they are simply in need of a desktop environment which doesn't get into the way of their daily work.\nIt seems you are so in love with that design, could it be that it is your brain child, which you will defend until your last breath? Because that's all that I see here in numerous threads. Every time someone comes along and criticises the plasma/plasmoid combo you are there on the virtual frontline and you are trying to defend it against all arguments no matter how...\n"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "Sorry but it really looks to me like ill will.\nDesktop widgets don't work on any system like \"normal\" windows, whether it is MacOS, Windows XP, Gnome or Vista. It's just that Plasma offers more freedom about what you can do with widgets and that seems for some reason to piss you off.\nIf you want, you can deal with Plasma exactly the same way you deal with widgets and applets on other systems : install them only as buttons on the taskbar or on a secondary bar. To avoid confusing newbies, just don't talk about anything else you can do with Plasma. And surprise!, they have no issue at all compared to using other desktops.\nAfter some time, they'll learn by themselves that you can do other things with Plasma. Or not, but that doesn't matter, they can still use KDE as a desktop.\nI tend to believe that your own prejudices and misunderstanding are the main reasons for your failure to make people appreicate KDE 4.1. I did not have such an issue myself, and I think that's mainly because I don't have a negative approach to it in the first place."
    author: "Richard Van Den Boom"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "The code is open. Just check it out from SVN, take it to your closest programmer shop. Pay them to fix the features you want. Send the patch back to Aaron and the rest of the plasma team. Presto, problem solved!\n\nEasy, isn't it?\n"
    author: "Oscar"
  - subject: "Re: Eye Candy"
    date: 2008-08-20
    body: "\"sorry, this is just getting on my nerves a bit now..\"\n\nWell you're certainly getting on our nerves with comments like this. Why don't you just go away, and stop using KDE software? You've nothing useful to say."
    author: "Richard Dale"
  - subject: "Re: Eye Candy"
    date: 2008-08-21
    body: "I couldn't agree with you more on the fact you need to apologise.  The rest.  Oh well, we all have bad days.\n\nPersonally, I'm grateful for all the eye candy, even if I don't use it.  Because someone has taken the time to write it, and who knows, maybe their contribution will lead to them becoming active in other areas of KDE or linux.\n\nI am grateful that every day I can turn on my computer and use it for what it is intended, without having to worry about all the nasties that affect other operating systems.  Thank you to everyone who contributes to that."
    author: "R. J."
  - subject: "WebKit kpart"
    date: 2008-08-20
    body: "we are approaching the end of summer. What is webkitpart statue ? Is it usable ? Does it support flash plugin ? "
    author: "Zayed"
  - subject: "Re: WebKit kpart"
    date: 2008-08-20
    body: "it works but it's not comfortable yet. \n\n\"open in new window\" doesn't work, \"open in new tab\" doesn't exist, the scroll bar has repaint issues, plugins (e.g. flash) still don't work but Urs just started working on getting them to work using WebKit from mainline (the current dev version).\n\nso it's not quite there yet, but miles ahead of where it was. and if Urs and Michael continue hacking on it, we'll hopefully have a fully functioning <s>battlestation</s> webkit kpart in the near future."
    author: "Aaron Seigo"
  - subject: "Re: WebKit kpart"
    date: 2008-08-21
    body: "ha, ha! i just discovered that middle clicking works to open in a new tab now, so that's already a bit better. \n\nthe scroll bar repaint issues are fixed with qt 4.4.1 as well, so that's another one down.\n\nand i just noticed the little resize handle on text edits: it lets me make the box i'm typing this in as big or small as i want just by clicking and dragging .. no more am i limited by the default size on the page! woo!\n\nthe webkit part does need more hands (what project doesn't, i suppose?) and it's the kind of thing that is pretty perfect for the coupl-hours-here-and-there hacker."
    author: "Aaron Seigo"
  - subject: "Re: WebKit kpart"
    date: 2008-08-21
    body: "resize handle on textedits?!\nAwesome! :D"
    author: "Chani"
  - subject: "KDE3.5.10?"
    date: 2008-08-20
    body: "Hi,\nDoes someoen know which distribution will ship KDE3.5.10? Debain is already in freeze with KDE3.5.9 and afaik all other major distributions uses already KDE4.\n\nSo what distribution should i pick if i want to have the latest and greates KDE3?\n\nGoing with KDE4 is not an option jet. Not because auf missing features but because of to much \"bugs\". E.g. the icons in the tray from time to time still have white background. It's the small bugs which held me away form KDE4. Im fine with the situation that programs have to be ported and features have to be developed but what is already there should at least work smooth.\n\nBut that's just a side note. The really important question is which distribution will pick up KDE3.5.10?\n\nThanks!"
    author: "pinky"
  - subject: "Re: KDE3.5.10?"
    date: 2008-08-20
    body: "> the icons in the tray from time to time still have white background.\n\nyes, the systray is problematic. unfortunately we're dealing with a crap specification that was designed with the way desktop UIs were done 8+ years ago and we're having to bridge that to what we have today. not pretty.\n\nthe good news is that there's a new systray widget in development (Jason Stubbs is the primary developer working on it) that will be replacing the current one in 4.2 (it's currently about to hit kdereview for review, after which it will move to kdebase). it does two significant things: it handles the current systray icons better and allows us to provide support for non-fd.o-spec icons side by side with them. \n\nit does some other less significant but also useful things like systray icon hiding."
    author: "Aaron Seigo"
  - subject: "Re: KDE3.5.10?"
    date: 2008-08-20
    body: "Maybe Slackware?\nThey have 4.1 in testing, but will probably ship KDE 3 as default.\n\nAnd of course KDEmod on ArchLinux...  :)\n"
    author: "mmp"
  - subject: "Re: KDE3.5.10?"
    date: 2008-08-20
    body: "openSUSE \n\nopenSUSE provides both KDE 3.5 and KDE 4 in their latest release. Their KDE desktops are very polished and they always provide bugfix releases. "
    author: "ad"
  - subject: "Re: KDE3.5.10?"
    date: 2008-08-20
    body: "+1. i've been using debian for over 4 year on the desktop, but it failed on me aspire one. opensuse worked out of the box"
    author: "Nick Shaforostoff"
  - subject: "Bug/request"
    date: 2008-08-20
    body: "Hello!\n\nPlease, sorry for posting this here, but as i have no answer, i don't know what to do.\n\nOn 2008-08-02, i opened this bug/request:\nhttps://bugs.kde.org/show_bug.cgi?id=168070\n\nShould i open a bug/request for each app involved? or leave it as is now?\n\nThanks!\n\n---------------------------------\nHello,\n\nI'm actually playing with KDE 4.1 before the big change from 3.5.9. One of the things that i've changed is the way i use apps. Once the app is properly configurated, and placed the icons that i need, the menu bar is no needed in my day to day work. So, i hide it with Ctrl+M. In my 1280x800 display, all vertical space is wellcome :-) and, why not? at least for me, is visually better :-)\n\nThis, can be done in many apps, like konqueror, kget, dolphin, okular, kopete, gwenvieew, etc... But in other apps, the menubar can't be hide, nor option in menubar, nor Ctrl+M.\n\nA not complete list of apps that can't be hide the menubar: akregator, kwrite, kmail, system monitor, juk, dragon player....\n\nI think that KDE should provide this option to all apps, to get a concordant DE.\n\nI don't know if this request must be created for each apps involved, or this can be sent to all devs in the way \"all KDE apps must have this option, make sure yours have it\". Any information needed or just comments, say them. Thanks for reading it.\n\nHope this helps to make KDE better.\n\nBye\n\nDienadel\n------------------------------"
    author: "Dienadel"
  - subject: "Re: Bug/request"
    date: 2008-08-20
    body: "It may happen that your wish report will be reviewed in a few years for the first time, if it is useful somebody recognizes it earlier. You should spent some voting points on it in order to show the importancy of your whish. "
    author: "Sebastian"
  - subject: "Re: Bug/request"
    date: 2008-08-20
    body: "You should at least try to answer his question else you are nothing but a troll"
    author: "trollspotting"
  - subject: "Re: Bug/request"
    date: 2008-08-20
    body: "Your post is blatant trolling.\n\nTo the op, I think filing it as a wish (like you did) against KDE in general is the best idea. The best solution would probably be something along the lines of adding that as a feature to KXmlGuiWindow (or possibly KMainWindow) that all applications can get pretty much for free (I think most use KXmlGuiWindow now?). Its been a while since I used bugs.kde.org (and I haven't used the newer version) so I might be slightly off on things (especially the implementation details :P)."
    author: "Kit"
  - subject: "Re: Bug/request"
    date: 2008-08-25
    body: "Bugsquad doesn't pay attention to votes when triaging. "
    author: "blauzahl"
  - subject: "Eye candy, smoothness and wakups"
    date: 2008-08-21
    body: "Hi,\n\nThe eyecandy effects need 2 more things to be really nice and to be on par with compiz:\n\nhttp://bugs.kde.org/show_bug.cgi?id=163892:\nIn short, a physics based animator would make the animations feel more natural, and not so choppy, there has been a patch as it looks:\nhttp://lists.kde.org/?t=120649479100005&r=1&w=2\n\n\nhttp://bugs.kde.org/show_bug.cgi?id=155694\nWakeups... :-)\nI do not get why a compositor has to use a static timer. Imagine there is no window open, or some windows are open whose content does not change, why trigger 60 wakeups per second?\n\nCan the compositor not react just when kwin, or the application repaints something?\n\n\nMy 2 cents.\n"
    author: "Yves"
  - subject: "Re: Eye candy, smoothness and wakups"
    date: 2008-08-21
    body: "Most effects already use an EaseInOutCurve. So they are not linear. It's just that this curve seems not to be perfect for the short animations of about 200 ms. If I knew the physics I would implement a better curve ;-)"
    author: "Martin"
---
In <a href="http://commit-digest.org/issues/2008-08-03/">this week's KDE Commit-Digest</a>: The <a href="http://plasma.kde.org/">Plasma</a> "extenders" project is merged into kdebase, with initial integration into the kuiserver applet. Continued work on the systray-refactor, and more work on the "Weather" Plasmoid. A whole load of bugfixes for Kicker 3.5.10. A new "Magic Lamp" minimize effect, and a rework of the "Grid" effect in kwin-composite. Support for extracting artwork from iPod's, tag editing and removing files from MTP devices, and scriptable services (including a "web control" script), and lots of other developments in <a href="http://amarok.kde.org/">Amarok</a> 2.0. An automatic image fetching script/plugin added to <a href="http://edu.kde.org/parley/">Parley</a>. Basic XLIFF support in Lokalize. Support for regular expressions in KSysGuard graphing. Improved support for password protected archives in <a href="http://en.wikipedia.org/wiki/Ark_(computing)">Ark</a>. Support for saving file fonts embedded into a PDF file in <a href="http://okular.org/">Okular</a>. A new, enhanced <a href="http://strigi.sourceforge.net/">Strigi</a> service (using KDE technologies) for interfacing with <a href="http://nepomuk.kde.org/">NEPOMUK</a>. KJots and KTimeTracker can now be deactivated (while <a href="http://kontact.kde.org/kmail/">KMail</a>, <a href="http://korganizer.kde.org/">KOrganizer</a> and KAddressbook cannot) in <a href="http://www.kontact.org/">Kontact</a>. Beginnings of "master pages" support in <a href="http://koffice.kde.org/kword/">KWord</a>. Rocs, a graph algorithm tool, added to playground/edu. "Google Gadgets for Plasma" moved to kdereview, "Timer" Plasmoid moved to kdeplasma-addons. <a href="http://commit-digest.org/issues/2008-08-03/">Read the rest of the Digest here</a>.

<!--break-->
