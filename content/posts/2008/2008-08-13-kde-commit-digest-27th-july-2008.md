---
title: "KDE Commit-Digest for 27th July 2008"
date:    2008-08-13
authors:
  - "dallen"
slug:    kde-commit-digest-27th-july-2008
comments:
  - subject: "Akademy makes all contributors..."
    date: 2008-08-13
    body: "...contribute 5x faster.\n\nDanny, you rock.\n"
    author: "Emil Sedgh"
  - subject: "Re: Akademy makes all contributors..."
    date: 2008-08-14
    body: "Yeah indeed. Great stuff and an very interesting read. :)\n\nAnd thanks for bringing KSt up, never have used it before, let's try it."
    author: "mat69"
  - subject: "\"support for using the native Windows start menu\""
    date: 2008-08-13
    body: "\"support for using the native Windows start menu where appropriate\"\nit is about win32 kde port? I was testing win 32 ( http://windows.kde.org/ ) packages few days ago. It is still bugy but it will rock for sure. \n\nI've already submitted few bugreports for win32 platform but there is a lot to do. \n\nHowever Kopete is already usable ;]    "
    author: "Dog"
  - subject: "Re: \"support for using the native Windows start menu\""
    date: 2008-08-13
    body: "> it is about win32 kde port?\n\nyes.\n\n> I've already submitted few bugreports for win32\n\ncool; testers and developers are needed."
    author: "Aaron Seigo"
  - subject: "Re: \"support for using the native Windows start menu\""
    date: 2008-08-13
    body: "What precisely is needed?\n\nI installed KDE under Vista but the nested menu of Vista is a complete mess. I was very much impressed about the maturity of the port."
    author: "andy"
  - subject: "Re: \"support for using the native Windows start menu\""
    date: 2008-08-13
    body: "I keep testing these releases on my dual-boot box at home, and my dual-boot box at work.  The biggest issue seems to be the Knotify crash.  A quick Google search showed several people already reporting it."
    author: "T. J. Brumfield"
  - subject: "The KST entry"
    date: 2008-08-13
    body: "Why the KST entry was on the digest, and not in a story on its own on the dot? Just curious, because I don't see much relation why the commits that week, isn't it?"
    author: "suy"
  - subject: "Re: The KST entry"
    date: 2008-08-14
    body: "Because the top section of the Digest is a showcase for KDE development, and because Kst had never been featured in the Digest.\n\nThe bottom section is about the commits that week, but the top section can be related to that week or not.\n\nDanny"
    author: "Danny Allen"
  - subject: "GHNS uploader"
    date: 2008-08-13
    body: "Will the new GHNS uploader solve the problem that currently seems to exist with plasmoids where the downloads shown in GHNS don't work because they generally point to source packages rather than whatever the \"Add Widgets\" dialogue is expecting?"
    author: "Adrian Baugh"
  - subject: "So fast Danny"
    date: 2008-08-14
    body: "Danny, you are really catching up. Good work!"
    author: "winter"
  - subject: "Kst looks cool"
    date: 2008-08-15
    body: "Thanks, Danny & Andrew, for introducing Kst---looks very useful."
    author: "T"
---
In <a href="http://commit-digest.org/issues/2008-07-27/">this week's KDE Commit-Digest</a>: Support for hiding/showing system icons in <a href="http://plasma.kde.org/">Plasma</a>, support for using the native Windows start menu where appropriate, with more work in the "Previewer" applet and "TabBar". Better filtering support in the "FolderView" applet. Various work toward <a href="http://amarok.kde.org/">Amarok</a> 2, including visual changes, work on playlists, and initial support for MTP devices. Work on a <a href="http://www.commit-digest.org/issues/2008-06-22/#2">welcome screen</a> in <a href="http://edu.kde.org/parley/">Parley</a>. Initial commit of a "Sky Calendar" tool in <a href="http://edu.kde.org/kstars/">KStars</a>. A Twitter plugin in <a href="http://edu.kde.org/marble/">Marble</a>. Trials with network games in KTank. Keyboard actions for switching tabs in <a href="http://konsole.kde.org/">Konsole</a>. OpenSoundSystem (version 4) support in KMix. Quick extract and batch extract interfaces in <a href="http://en.wikipedia.org/wiki/Ark_(computing)">Ark</a>. "Automatic computer shutdown after downloading" functionality in <a href="http://kget.sourceforge.net/">KGet</a>. Experimental mouse pressure and rotation for sumi-e painting in <a href="http://www.koffice.org/krita/">Krita</a>. Text support for the WMF import filter in <a href="http://koffice.org/">KOffice</a>. KGo is added to playground/games. KDE 4.1.0 is tagged for release. <a href="http://commit-digest.org/issues/2008-07-27/">Read the rest of the Digest here</a>.

<!--break-->
