---
title: "4.1 Release Candidate Out For Testing"
date:    2008-07-15
authors:
  - "skuegler"
slug:    41-release-candidate-out-testing
comments:
  - subject: "Panel Autohide"
    date: 2008-07-16
    body: "Hi,\n   Thanks for the new release.. I am installing / testing it on my asus 900.. Is the panel autohide feature included.. I couldn't find anywhere to set it but was wondering if it was setable by editing the panel config file...\n\nThanks\n  Ruairi\n\nps runs great on the eee 900... looks really good also"
    author: "Ruairi"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "You get autohide hopefully with kde 4.2. At the moment this feature isn't there."
    author: "Manuel Mommertz"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "It's not a feature, it's a necessity."
    author: "reihal"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "It may be as well, but there was feature freeze and of course, developers are humans like everyone else. "
    author: "Luca Beltrame"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Then you write it's code. It's a necessity, you know? Just do it. "
    author: "Commander"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "It can even be done during the 4.1 cycle as a plugin, after all, that's how Plasma is build. Maybe writing a Panel using Python so the user won't even have to compile stuff, or changing the current C++ code and offering it on kde-look."
    author: "jos poortvliet"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "That's pretty simple then. You rewrite the existing panel in Python/Kross (not that I think that the current C++/Qt API is more \"intimidating\" or quirky but if you say so!) , and write autohide as a plugin, upload it on kde-apps.org and give us the URL. And do it quickly please, before the 4.1 is released so you have less than a couple of weeks (don't forget to do extensively test it, my KDE 4.1 RC is fairly stable and I don't it crashing because of quick-and-dirty code) Go ahead and don't forget keep us updated with your progress, as well. "
    author: "Commander"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "... yes master ..."
    author: "Paul Eggleton"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "What a perfect after work bit of humor. Thanks!"
    author: "winter"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "would've been even funnier if reihal wrote it ;-)"
    author: "jospoortvliet"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "I have no sense of humour."
    author: "reihal"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "and I'm not a sensei....\n\nI would like to see two things on 4.1.x... (the second one on 4.1.0!) Panel autohide and multirow panel. I have 1280x1024 19\" inch monitor. I use OpenSUSE 11.0 as distribution and KDE 4.1 RC1. I can get three window open and my panel is full. \n\nSystray widget eats lots of space because you cant have two rows. Three icons in panel (dolphin, konqueror, kickoff) eats lots of space because I cant have those dolphin & konqueror as on two row. My three window panel eats all left space because I have only one row to use... so Please :)"
    author: "Fri13"
  - subject: "Re: Panel Autohide"
    date: 2008-07-26
    body: "Just open more windows and.. ta-daaaah! multi rows appear magically (that is how it is supposed to be.. why do we have to waste space if there are only 3 open apps?)"
    author: "Vide"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Great work, tried kde4live and its a huge step foward from 4.0.5 wooooww!!!\n\nReihal: be kind with developers, I think if they could they would have it included already... and if is a necessity, scratch your itch!! :) Have you coded today? Have you donated today? "
    author: "jorge"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "The developers are great, of course. I just need my screen space.\nWhy so touchy?"
    author: "reihal"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "Hey, reihal... after a recent flood, my basement is cluttered and needs to be sorted.  It is not a request, it is a necessity.\n\nPlease tell me when it is going to be done, as I need it."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "If there's a whole bunch of companies who use your basement to make money with it, maybe they'll fix it. \n\nYeah my reply is kind of stupid and doesn't reflect the actual situation, but neither does yours. I agree some flamers in the forum are annoying with all their stupid negative comments, but it was not the case here. Give the guy a break. There's space in the forum for positive and negative remarks. "
    author: "ad"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "Yes, and if they don't, it doesn't get fixed.  There's no difference between a commercial motivation (actually, my basement houses a bunch of stageshow props and costumes, so the community does use it) and a personal motivation.  Both motivate development.  Somebody random on the internet petulantly demanding that other people do something for them is seldom a motivation.  In fact, just demanding that other people do things to make your computer work better is a pretty lousy tactic.  Money and friendship go a long way -- I just helped a friend move to his new home last night, and I did it for friendship... I care about them because they have been nice to my wife and I.  He could also have hired movers and paid them to move it.\n\nStanding in front of my house (because I've helped people move) or in front of the moving companies building and demanding that we move him is a bad method.  He was asking why people were so touchy.  That's why.\n\nIt's not a matter of \"he has no right to complain\", it's a matter of explaining why he isn't making any headway with his demands.  It is basic social or commerce interaction -- you have to be nice to people or pay them money (or some other kind of trade: a time investment for instance).  Motivation for getting other people to help you is pretty simple.  He made demands coupled with no *reason* to assist him ahead of anybody else with 10,000 different varied request.  That's why people are touchy... those kinds of motivation free demands (\"Do this for me because I need it\") are way too common.  \n\nHe seemed genuinely baffled, which is why I *was* giving the guy a break and explaining why he was getting the reaction he was."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "Well I agree with everything you say, except with the part about him making demands. He just said he needed it, it's just a comment. I agree you were quite polite with him, but not with the explanation of the whole reaction because in my opinion it doesn't really fit what happened in this case. Anyway enough of drama I guess :)"
    author: "ad"
  - subject: "Re: Panel Autohide"
    date: 2008-07-26
    body: "No, reinhal has a long historial of plasma bashing here on the dot, so there are reasons if people is so susceptible with him"
    author: "Vide"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "everything was cool till you spoke of necessity, like a demand, like you are paying someone...\n\nThey will do it, the fastest they can, with the resources they have, I have no doubt about it. Touchy because of the negative spam comments that are flouting around everywhere. Demanding everything today! Because I need it! If you need it stick with 3.5.10 till 4.2 have that need of yours. \n\nBut yes, above is a over reaction to a simple request... and I understand your \"com' on, your being too hard on me \" :-D (not ironic)"
    author: "jorge"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "I see, you are projecting your frustration of others comments on an easy target.\nThats normal, I guess."
    author: "reihal"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "don't be so touchy!! :-D\n\nAll I was saying was you don't have the right to order the developers that rude way, and I was trying to teach you some diplomacy, as other \"users\" like you should learn... \n\nI have my little complains/wish/demands, and I have made it here (where I should have made it a bug report) but I didn't received the same reception as you, that's weird... \n\nI will stop now. \n\nGreat Great release!! Thank you guys!\n   "
    author: "jorge"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "You appear to have misspelled \"monstrosity\" as \"necessity\".... ;-)"
    author: "Adrian Baugh"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "Seconded.\n\nPlease don't implement autohide. It's a necessity."
    author: "Ioannis Gyftos"
  - subject: "Re: Panel Autohide"
    date: 2008-07-20
    body: "???"
    author: "MamiyaOtaru"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Not there?\nthen I'll probably wait for the 4.2 release."
    author: "Adrian Ribao"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "the same with me..."
    author: "Tom"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "And that is perfectly cool. We don't expect everyone to move to KDE4 with this release. For some it's good enough (or better) already others' pet features aren't there yet, those can be using KDE 3.5 just fine. Everybody happy :-)"
    author: "sebas"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "4.1 rocks here on openSUSE! I can live without autohide for a while, there is a ton of new fantastic things here :)"
    author: "Koko"
  - subject: "Re: Panel Autohide"
    date: 2008-07-20
    body: "We don't expect everyone to move to KDE4 with this release\nothers' pet features aren't there yet, \n\nSo much for a user question and for the remarks users make everywhere\n\nUsers have questions for \"pet\" feateures\n\nWithout those users none would have opensuse 11,  they are all waiting for the boxed version.   \nThan the may ask questions because they paid for it.\nOk  we will ask Novell to hide kde4 from every one till there will be a boxed version one can buy.\nNo more \"pet\" feature questions. \n\nSo sleep wel  \n \n"
    author: "susegebr"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "This \"minor\" setting is a Show Stopper for me with the KDE4 series as well. I have never wanted my screen real estate (no matter how big or small that may be) taken up by some static bar full of pictures on the screen.  \n\nIs there a long list of features somewhere on the KDE site that shows a list of what features are planned for the 4.2 (or later, or just whatever) release so I can keep an eye on this?\n\n\nM.\n"
    author: "Xanadu"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "here you go\n\nhttp://techbase.kde.org/Schedules/KDE4/4.2_Feature_Plan"
    author: "hias"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Thanks!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Panel Autohide"
    date: 2008-07-20
    body: "Thank you very much, that's exactly what I was looking for!\n\nM.\n"
    author: "Xanadu"
  - subject: "New features in minor releases"
    date: 2008-07-16
    body: "I have been hearing for a while that many features are now being aimed at 4.2 because they just missed the 4.1 window.  Right after the 4.0 release, we were told that many things that just missed the 4.0 window would have to wait for 4.1, even if the code was in trunk right after 4.0 release.\n\nAfter some griping, some of these features were \"back-ported\" to the 4.0.x releases.\n\nI'm curious.  If the feature does make it to trunk earlier enough, should new features be included in minor releases?  Even 3.5.x releases added new features, and some would suggested that 4.x is missing enough new features that it would seem adding them sooner would be the way to go.\n\nIs there a decision making process on when to hold back a new feature that exists in trunk?"
    author: "T. J. Brumfield"
  - subject: "Re: New features in minor releases"
    date: 2008-07-16
    body: "> Is there a decision making process\n\ngenerally we don't add any new features to patch level releases; in extreme situations we do, but we try and keep those to a \"known stable\" minimum. (some of the 4.0 backports definitely failed on that metric, unfortunately.)\n\nanother approach, which is likely more sane for things like panel hiding, is to provide patches for some of these features that can be applied by downstreams to 4.1.x but don't include them in official kde releases until 4.2."
    author: "Aaron Seigo"
  - subject: "Re: New features in minor releases"
    date: 2008-07-17
    body: "Aaron, wasn't one of the initial ideas of Plasma being release-independent from KDE?\n\nYou know, it would be very, very, very (repeat this a lot) good if we could have plasma releases without having to change kdelibs.\nSure some features or plasmoids that required new changes in the libs would not be possible, but things that already are working in trunk today probally work just fine also with 4.1 libs - because the trunk probally is not *that* different from 4.1 yet.\nFor example, I've saw that tooltips are back.\n\nNow that is not an easy thing, nor I believe *that* much important after KDE changed to 6 month release cycle (I'm loving this model, 4.1 being a GREAT release after 4.0 shows that it is working), but it would be fun :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: New features in minor releases"
    date: 2008-07-17
    body: "In short: Yes. You can write your own auto-hiding panel and install it into KDE. You don't need to rip out Plasma or update kdelibs for that."
    author: "sebas"
  - subject: "Re: New features in minor releases"
    date: 2008-07-17
    body: "So, after the Plasma API is stabilized (meaning only new functions/objects, o changes to break compatibility) it would be wise if plasmoids could be upgraded.\n\nThink about how firefox extensions work. THAT would be awesome for users wanting new features without having to (wait for) upgrade the entire KDE system :)\n\nFeasible or there is no desire of such a thing?\n\nThanks in advance."
    author: "Iuri Fiedoruk"
  - subject: "Re: New features in minor releases"
    date: 2008-07-18
    body: "DXS (or GHNS) would have to have that feature added, but it seems entirely possible."
    author: "Scott Dixon"
  - subject: "Re: New features in minor releases"
    date: 2008-07-16
    body: ">I have been hearing for a while that many features are now being aimed at 4.2 because they just missed the 4.1 window. Right after the 4.0 release, we were told that many things that just missed the 4.0 window would have to wait for 4.1, even if the code was in trunk right after 4.0 release.<\n\nYes, and I guess that's normal in any softwareproject, right? Esp with a more time-based release schedule as we're trying to adopt...\n\n\nAbout the decision making process - it's simple. No features are backported, only bugfixes.\n\nWe did add some features during the 3.5.x series because KDE 4.0 took a while, and we added features to 4.0.x because it wasn't really targeted to those who need a super-stable release anyway. It is not impossible there will be features during the 4.1.x series, but I don't think it is very likely. After all, even though some minor things are missing, for 95% of the users, KDE 4.1 is pretty much on par (and of course in many ways superior) to 3.5. I do miss some features - sure. Drag'n'drop of a icon into a text input field in konqueror opens the file instead of adding the location to the input field as it used to do. And there are more issues like that. But should we sacrifice stability for such rather minor things? If we want to target the larger userbase, we shouldn't. And I think, despite such minor issues, KDE 4.1 is good enough. It will be for me, and I'm rather critical.\n\nBesides, KDE 4.2 will be out in another 6 months - not too long a time to wait, right? I'd rather see the developers focus on that release than backporting & heavily testing features for 4.1. Looking at the issue from the perspective of a slightly larger timeframe (1 year is enough already) would make you agree, I think. For long term progress (which ultimately benefits users most) development of TRUNK is most important. More important even than the perceived quality of the current release for endusers (see the 4.0 controversy) - as long as developers like to work on it. And they do, that's why we welcomed 166 new developers in 6 months ;-)\n\nHey I see your point, I'd rather have those features on my desktop YESTERDAY. But really, would you want to sacrifice future progress for it?"
    author: "jospoortvliet"
  - subject: "Re: New features in minor releases"
    date: 2008-07-16
    body: "A few small features that close the gap between KDE 3 and 5 in some areas would be nice to see earlier, but I understand the philosophy of not adding features in point releases.\n\nThat being said, Aaron's suggestion of splitting some of those things out as patches (and I assume in turn allowing distributions to opt to include those at their discretion) is more than fair."
    author: "T. J. Brumfield"
  - subject: "Re: New features in minor releases"
    date: 2008-07-19
    body: "The problem is that although this will be KDE-4.1.0, the desktop and Plasma are really still in Beta.  So, should be apply the proper release standards to Plasma or consider it to be a Beta (which it actually is) and continue to add features to the Plasma Beta that is included in KDE-4.1?\n\nHave we reached the point where the API is stable enough that Plasma from TRUNK will run on KDE-4.1?  If so, then perhaps we can establish separate releases of Plasma till it reaches the point that it is no longer a Beta."
    author: "JRT"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Um... If you don't like to see the panel, why not just get rid of it? Have the task manager, pager, K menu etc. on the desktop and use CTRL+F12 to get to it.\n\nI've used this setting before and found it quite usable, seeing as my screen isn't the biggest out there (bog-standard 1024x768 screen)."
    author: "Madman"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "That's what I did a couple of months ago. Just to see if I could get used to a desktop without any panels. I have just set that moving the mouse into the upper left corner of screen shows me all the windows from current desktop and moving it into the lower left corner shows me all the desktops. This replaced my taska manager and pager. Of course I can also quickly use the Alt+Tab to switch windows and Ctrl+F1, Ctrl+F2 .. to switch to desktops. the KRunner (Alt+F2) also nicely replaces the need for K menu. And I've put the clock on the Dashboard (Ctrl+F12). The only problem so far is the system tray. I have also have it on Dashboard, but when I bring it up the plasmoid is all black and icons are gone. And today I'm still usong this setup and like it a lot."
    author: "Jure Repinc"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Very interesting idea.  Can you share the steps you used to accomplish this?"
    author: "cirehawk"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "click on the panel cashew (it's on the right hand side, or left in an RTL desktop), and click \"Remove This Panel\". confirm, and voila: no panel."
    author: "Aaron Seigo"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "lol  Well that much I know Aaron.  I was talking more about how he got all the start apps and running apps to pop up in the left corner upon mouse over."
    author: "cirehawk"
  - subject: "Desktop Grid"
    date: 2008-07-17
    body: "I think he is talking about Desktop Grid. If you have kwin effects enabled move your mouse to the top left corner of the desktop and a little more in the same direction."
    author: "Anonymous Coward"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "It requires KWin desktop effects. When they are enabled, move the mouse into the upper-left corner and you get expose-style presentation of the windows. Ctrl-F8 gives you desktop grid.\n"
    author: "Michael \"Web Services\" Howell"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "Ok, and then I just:\nhttp://linux.softpedia.com/get/Documentation/HOW-TO-Kicker-on-KDE4-35973.shtml"
    author: "reihal"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "Awesome idea..."
    author: "Sebastian"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "yeah, i'm so happy that we no longer have a hardcoded panel in plasma like kicker did and that the plasma desktop layer can be actually useful as a panel tools replacement, unlike kdesktop.\n\nw00t for flexibility =)"
    author: "Aaron Seigo"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "good idea\n\nit could be a workaround for autohide if there is a possibility to bring the dashboard to the top by moving the mouse to a corner or the border of the screen."
    author: "hias"
  - subject: "Re: Panel Autohide"
    date: 2008-07-16
    body: "What I miss to install KDE4 on my eeePC are:\n- panel auto-hide\n- personalize systray area (choose witch icons will be hidden or not)\n- global keys like we had in KDE3 (both for launching apps and actions)\n\nSome can be done today by editing configuration files, but seems like all will be there for 4.2. So, I'll wait a bit more, while using KDE4 on my desktop machine :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "Hi Ruari.  \n\nWhen you get it install on the ASUS 900, would you mind providing some feedback as to ho well it runs. I was thinking of buying one of thes sublaptops.\n\nAs this thread will probable go away, would you mind sending me an email.\n\nThanks\nDavid\ndavid.boosalis@gmail.com"
    author: "David"
  - subject: "About Kopete"
    date: 2008-07-17
    body: "yes..a little offtopic...I install KDE4.1 from Kubuntu Intrepid Ibex alpha 2..and i'm very surprizing because see more changes one of them I can move icons in panel..good work guys..\nanyway i want to know what's about kopete..I trying all KDE4.xx versions and I can't see buddy pictures, only a square empty and in RC1 none square only big ball green..don,t work webcam..don't work file transfers..all of them I try in Yahoo protocol..It's normal or I missing to install some packages ?\nmany tanks, all the best!"
    author: "neaghi"
  - subject: "Re: About Kopete"
    date: 2008-07-19
    body: "We (BugSquad) found lots of bugs and devs fixed lots of bugs for Kopete during the Kopete BugDay. I'm not sure if they would have all made it into 4.1, but if you are running from SVN, keep getting new updates! They've been working hard in their limited time. Feel free to join us this Sunday, too. :D\nfreenode, #kde-bugs, you just need a new copy of kopete "
    author: "blauzahl"
  - subject: "Re: About Kopete"
    date: 2008-07-19
    body: "We (BugSquad) found lots of bugs and devs fixed lots of bugs for Kopete during the Kopete BugDay. I'm not sure if they would have all made it into 4.1, but if you are running from SVN, keep getting new updates! They've been working hard in their limited time. Feel free to join us this Sunday, too. :D\nfreenode, #kde-bugs, you just need a new copy of kopete "
    author: "blauzahl"
  - subject: "Re: Panel Autohide"
    date: 2008-07-17
    body: "Yes there are more incomplete features on KDE4 and how can you complete to KDE4 on july,29.\nI try to explain what are incomplete features;\n\n1- no panel auto hide\n2- there is no panel configuration( not flexible like kde 3.5)\n3- where is the align vertically and horizontally for icons on desktop\n4- still no support HD audio and no working.\n5- there is no RDS widgets\n6- there is no preview any window on panel\n7-when run the konqueror and put any web site and try to download flash plug in but still go to \"macromedia web site\", please change it. I can help you flash web site is \"www.adobe.com\"\nlast one is; I fell use like windows because there is no free and no changeable anything.  I want to KDE 3.5....\n"
    author: "Deniz"
  - subject: "Re: Panel Autohide"
    date: 2008-07-22
    body: "Hi, I want to hear what your problem is regarding #2.  What is it about the panel configuration that is not \"flexible\" for you?  What feature did 3.5 have that 4.1 doesn't?\n\nAlso, 4.1 is not the end of the 4.x series.  Panel autohiding (which I agree is taking unexpectedly long) will have to wait till 4.2.  \n\nregarding your other issues:\n\n1- working on it.\n2- ...\n3- I'm not sure what you are refering to... is this a standard feature of most desktops?\n4- thats a hardware configuration issue... we can't say whats wrong about it without more information.\n5- whats rds?\n6- That should be there, maybe you just don't have it enabled?  This requires compositing.\n7- its better to install distro packages for flash than get them from adobe... believe me, you will be much happier.  Its an unfortunate part of the internet that people assume you have windows.  I wish the adobe website would make this more clear rather than just giving people a red hat package that they can't use.\n\nAt the end you made a comment, it was a little hard for me to understand, but what I think you said was that \"KDE 4.1 is like windows in that it is not configurable, I wish it was more like 3.5\".  While I agree that the configuration dialogs feel more spartan these days, I don't notice much missing. Is there something that you feel is gone and shouldn't be?  I can maybe add it back in for you."
    author: "Level 1"
  - subject: "nVidia performance problems"
    date: 2008-07-16
    body: "My system has an old GeForce card which uses the legacy driver, and it doesn't support Compiz and the other X11 bling; is that affected by these issues also?"
    author: "Matthew Smith"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "What I heard the problem only effects GeForce 8000 and newer series. So it should work for you."
    author: "Manuel Mommertz"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "The slowness issue, along with some graphics glitches, is also present on an NVIDIA series 6 card (the one I own). I'm unsure if it's related to the same issues, though."
    author: "Luca Beltrame"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "I also have the performance problems here on GeForce 6800. It is working slower then the poor integrated ATI graphics on my laptop. I hope that nVidia resolvs their driverproblems as soon as possible."
    author: "Kovac"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "I have several performance issue, especially with window resizing and I use an onboard Geforce 6150.\nIf I disable desktop effects resizing and 2d performance is greatly improved. Also, setting \"InitialPixmapPlacement=2\" (http://techbase.kde.org/User:Lemma/GPU-Performance#NVIDIA) make resizing a LOT faster when using desktop effects, but it introduces a \"lag\" when opening or closing windows.\nHope Nvidia comes up with a fixed driver. However, when free ATI drivers are complete and working well, I'll be buying an ATI card for sure."
    author: "Lucianolev"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "I have a 7600 GT, and it certainly affects me."
    author: "T. J. Brumfield"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "Yeah, I have a 6600 and it affects me as well. Not as heavy as some seem to experience, but it's not pretty. Boy, am I looking forward to a fix from NVidia..."
    author: "jospoortvliet"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-17
    body: "I have a quite old geforce4 440mx on which I run compiz on everyday use and it works fine. Sadly I can't say the same for resizing, dragging and zoom out performance of my kde4 installation (using Neon packages). So I think it affects me too. For example, zoom-out with the cashew on the top-right takes about 4-5 seconds (with the screen freezed). I hope this situation will improve but I'm not that confident since all the fixes (for what I know) are going into the current driver and not in the new-legacy one that I use. Any suggestion? Or buying a new card is the only solution?"
    author: "kinto"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-19
    body: "Yes, KDE 4.x runs poorly on my Geforce Go 6800.  Ah well, I think I'll switch back to ATI for the next round.  Looking at a 4850 for my next rig."
    author: "jackflash"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "I have very old GeForce 2 MX 400 and have mixed feelings about performance.\n\nMost kwin effects are working - shadows (with some glitches), expose-like windows list, transparency. These I find really helping the rest I find just eye-candy and not tested properly.\n\nBig problems are with:\n\n-  apps on SVG (games, edu) but am not sure if this is issue of nVidia driver or just my old machine\n-  scrolling: sometimes it works flawlessy, sometimes it is slooow\n-  all Plasma related things: I had to get rid of folder views - one of most promising features, it is not possibly to work with them; krunner hints are very slow to appear and navigate; \n-  not sure how it relates to KDE but mplayer has problems with fluent playing of films, there were no such problems with KDE3.5.9\n-  interface of Kontact is dog slow; no other app has those problems\n\nOverall impression is very good. Visually it is years ahead of KDE3. Apps are coming nicely, although some are still bit unstable - Konq crashes at least one time daily."
    author: "mikmach"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "I also have an old Geforce, from end of 2002, and sometimes I also see these slowness effects (yes, only sometimes).\nI think I built Qt once without support for xrender, and I think it felt faster. \nCan anybody confirm this ?\n\nAlex\n"
    author: "Alex"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "Aaah, that makes sense, it's mostly the XRENDER implementation of NVidia which is slow. With 'acceleration' many actions are slower compared to rendering on the CPU... Pretty bad. I'm really looking forward to a fix from NV for this..."
    author: "jospoortvliet"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-17
    body: "I could only speak for my experience but...with my Geforce4 440mx I have poor resizing,dragging and zooming performance (not only in plasma, in all kde4 applications) and if I turn on kwin desktop effects dragging and resizing improve a bit but zooming stays pretty much the same (4 seconds delay with screen freezed). It seems hard to find a common behaviour! :-("
    author: "kinto"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-16
    body: "GeForce 7xxx has the problem as well, but this RC1 is the most stable I have seen so far with the nvidia drivers. Something very important to nvidia users (at least for me!) was to <b><i>NOT use the Blur plugin</i></b>. If you do that, you get artifacts and other shit all over the place. I turned it off and it reduces the problems big time."
    author: "parena"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-17
    body: "I have a NVIDIA card (07:00.0 VGA compatible controller: nVidia Corporation G72 [GeForce 7300 SE] (rev a1)) and other than a 1 second delay kwin effects or konsole window resizing I don't suffer any issue. My question is: are these all NVIDIA videocard issues?"
    author: "Anonymous"
  - subject: "Re: nVidia performance problems"
    date: 2008-07-22
    body: "8800 GT here: If you have problems with folder view, run the command mentioned in the article:\n\nnvidia-settings -a InitialPixmapPlacement=2 -a GlyphCache=1\n\nThis worked wonders for, but apparently it doesn't work for everyone.  You might have luck with a different value of InitialPixmapPlacement, like 1 or 0 (maybe 3?  who knows...)"
    author: "Level 1"
  - subject: "Bug reports"
    date: 2008-07-16
    body: "Please when you report crashes always join a valid backtrace as explained here:\nhttp://techbase.kde.org/Development/Tutorials/Debugging/How_to_create_useful_crash_reports\nIf you see a long list of\n(no debugging symbols found)\nplease do not paste those lines and install the debug packages for your distribution.\n\nThis will help us greatly to quicker fix bugs. Thanks in advance and also thanks to all potential testers!"
    author: "annma"
  - subject: "Re: Bug reports"
    date: 2008-07-16
    body: "This is taken from http://www.kubuntu.org/news/kde-4.1rc1\n\n\"There will be many bugs with this release and it is recommended that you do not file bug reports against the packages in this release. If you come across any issue which may be packaging related, it is recommended that you join the Kubuntu KDE 4 IRC channel (#kubuntu-kde4) and query those in the channel.\"\n\nThose are the packages I'll be using. I don't think they include the debug packages, but I could be wrong..."
    author: "Joergen Ramskov"
  - subject: "Re: Bug reports"
    date: 2008-07-16
    body: "Kubuntu packages from PPA have \"dbg\" packages of KDE 4.1..."
    author: "xapi"
  - subject: "Re: Bug reports"
    date: 2008-07-17
    body: "I know that KDE is in de-clutterization phase removing all the \"not needed for average joe\" stuff from dialogs, but if you really want useful reports you'll have to add that sentence to the crash dialog. I'm surprised that this was not done earlier?!"
    author: "vf"
  - subject: "Re: Bug reports"
    date: 2008-07-17
    body: "The \"remove everything not necessary for Joe A.\" is not accurate.\n\nFirst, there is not Joe Average. Second, there are three reasons why you might miss a certain feature:\n\n- It's not implemented yet\n- It has moved to a more logical place (a lot of options are not in the direct context of the data they're manipulating, see for example gwenview)\n- The app has become smarter so the option is not needed anymore\n\nAs to the crash dialogue, that makes sense. Can you send a patch for this? It should be rather trivial to do."
    author: "sebas"
  - subject: "Re: Bug reports"
    date: 2008-07-21
    body: "OT: I really like new KDE4 aproach which is IMO \"hide by default to hardcore-pro-users options but still alow them to by turned on or sth.\" Fot ex. Kget4 default toolbar is SO MUCH better. There are available only options whitch are needed \"for average joy' and still pro-user can add more buttons in 4-5 clicks.  "
    author: "Koko"
  - subject: "Nvidia, performance, concise information"
    date: 2008-07-16
    body: "I'm bringing this up because kwin's performance is still a serious issue for people with nvidia cards (those were considered a good choice for a linux desktop some time ago ;)\n\nThere are folks that say \"well, i've nvidia geforce-xxx\" and it's running great. But matter of factly plenty of people with fairly powerful nvidia cards get very very bad fps. So the problem clearly is that there's no concise source of information what one can do to achieve that.\n\nOther than <a href=\"http://techbase.kde.org/Projects/KWin/4.0-release-notes\">this slightly outdated article on techbase</a> I at least couldn't find anything. And those options didn't work out for me, as for many other people I spoke to in forums, no matter what combination, setting or tweak was tried.\n\nSo how about doing an up to date, tested article that clearly gives all the possible tweaks and options to get a decent performance out of a nvidia card? I'd like to help, but I'm afraid I can't  because as said I didn't discover any suitable solution to get more performance."
    author: "Philipp"
  - subject: "Re: Nvidia, performance, concise information"
    date: 2008-07-16
    body: "Ah, just found the new techbase article ... so I'll work through that."
    author: "Philipp"
  - subject: "Re: Nvidia, performance, concise information"
    date: 2008-07-16
    body: "Now that's the spirit ;-)\n\nUltimately NVidia has to fix this in their drivers."
    author: "jos poortvliet"
  - subject: "Re: Nvidia, performance, concise information"
    date: 2008-07-16
    body: "Yea, I know. Please stomp on their feet to do this asap... I heard they generally work relatively well with requests from developers.\n\nAfter all one reason to push out KDE 4.0 was to encourage people to do that kind of work in order for 4.1 to be great. So I hate the fact that they're crashing the party for me. KDE 4.1 looks gorgeous, and the compositing stuff is a vital part of that."
    author: "Philipp"
  - subject: "Re: Nvidia, performance, concise information"
    date: 2008-07-16
    body: "If I'm not mistaken, Aaron Siego blogged about this in the past, post 4.0 release (I don't have a link for it, but I remember reading about this).  He stated before that KDE found performance issues with Nvidia cards and was trying to get them to help out with the issues.  I've got an 8600 series card on my laptop, and when I use the stock nv driver performance is great (but then again I can't play OpenArena or ET:QW this way) and when I enable the Nvidia driver for acceleration performance is crap for me.  Whenever I open the menu or a window, I get a stream of black lines before the actual contents of the menu/window appears.  But, I love this project so I'm sticking with it.  Hopefully Nvidia will do something about their crap drivers though.  I'm considering upgrading my video card on my desktop to an ATI one, as they are kicking butt on the open source drivers."
    author: "Kenny"
  - subject: "Re: Nvidia, performance, concise information"
    date: 2008-07-19
    body: "My performance is fine with my nvidia card **if** I turn off desktop effects.\n\nI try turning it on every couple of months to see progress (and on every new nvidia release.)  Not much luck.\n\n"
    author: "JackieBrown"
  - subject: "KDE 4.1rc1 (4.0.98) Slackware 12.1"
    date: 2008-07-16
    body: "Slackware 12.1 packages are available at:\nhttp://kde4.rlworkman.net/packages/\n\nAnd kudos to KDE team, wonderful progress, seems stable and snappy than all previous releases.\n\nAaron, thanks for your creative work :)"
    author: "fast_rizwaan"
  - subject: "Re: KDE 4.1rc1 (4.0.98) Slackware 12.1"
    date: 2008-07-16
    body: "Thanks Robby for your packages.\n\nI'm not doing KDE Slackware packages anymore as I don't have enough time (2 kids, 1 job, 1 company to manage).\n\nI'm happy to see that someone will take back this job that I did for about 5 years. You should upload them on the ftp.kde.com"
    author: "JC"
  - subject: "KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-16
    body: "All New intel chipsets works wonderfully with kwin-compiz, but video playback is not working with XV video driver when desktop-effects are enabled. the problem is with \"intel\" driver (is blacklisted by compiz-fusion).\n\nThe solution is that one should use \"X11\" vo in mplayer (mplayer -vo x11 movie.avi) and \"XShm\" in xine, so that we can use both kwin compiz and video playback."
    author: "fast_rizwaan"
  - subject: "Re: KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-16
    body: "and with Dragonplayer what should I do?"
    author: "Koko"
  - subject: "Re: KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-16
    body: "I've use an Intel card with dragon and never had to do anything. It worked out of the box. If it doesn't for you, set it in the Phonon back-end (Xine, GStreamer, etc).\n"
    author: "Michael \"nothing\" Howell"
  - subject: "Re: KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-16
    body: "there is no such option to set video driver in dragon player or in Phonon Backend."
    author: "fast_rizwaan"
  - subject: "Re: KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-17
    body: "Use EXA."
    author: "Pepe"
  - subject: "Re: KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-17
    body: "and suffer performance loss."
    author: "fast_rizwaan"
  - subject: "Re: KDE 4.1rc1 kwin compiz and intel 915+ chipsets"
    date: 2008-07-17
    body: "I hate this bug too :(("
    author: "Stephan"
  - subject: "Problem with plasmoids"
    date: 2008-07-16
    body: "Maybe it's just me, but I've installed KDE 4.1 RC1 on my Ubuntu machine yesterday and some plasmoids simply show a small circle (aprox. 10 pixels large) on the desktop and when righ clicked do not show any configuration dialog.\n\nI've deleted my plasma configuration files, restarted KDE, but still have this.\nThe usual ones (most used in panel) as clocks, taskbar and such all work fine, but things as show desktop do not :-(\n\nBesides that problem (that is big because I cannot show my desktop files) KDE 4.1 RC1 is rock solid. I only do not install it on my eeePC because the panel still lacks some usefull options that existed in KDE3 that helps using less space :-)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Problem with plasmoids"
    date: 2008-07-16
    body: "Yeah yeah, we're still trying to get the new plasmoids to be packaged. We're having a bit of trouble with the transitional package to the new plasmoid package. Hopefully we'll get this resolved soon..."
    author: "Jonathan Thomas"
  - subject: "Re: Problem with plasmoids"
    date: 2008-07-16
    body: "Install the kdeplasma-addons package now, and everything should be back to normal."
    author: "Jonathan Thomas"
  - subject: "Re: Problem with plasmoids"
    date: 2008-07-16
    body: "Thanks, I tought it was a problem in the source, not in the package :)\nAlas, there was a problem with oxygen-icons package conflicting a file with libkdepim, I had to use dpkg --force-overwrite to install 4.1 RC1.\n\nThanks for the packages, I'm very grateful :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: Problem with plasmoids"
    date: 2008-07-17
    body: "For some reason, I can't find the folder view plasmoid :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: Problem with plasmoids"
    date: 2008-07-17
    body: "OK, just found it on the kdebase-plasma-kde4 package. Did not undestood why it is separated from the rest..."
    author: "Iuri Fiedoruk"
  - subject: "Re: Problem with plasmoids"
    date: 2008-07-17
    body: "Thanks, the new package sorted it out for me.  I still don't have working plasmoids in amarok 2 alpha though - if I drag applets to the middle pane, nothing happens."
    author: "Adrian Baugh"
  - subject: "About the themes"
    date: 2008-07-16
    body: "I wonder why the (color) themes are so dark. I am not sure what gives this feeling (taskbar only?), but the overall desktop seems dark. It is really different from KDE3 which uses many shiny colors. I think it is obvious, even from the screenshots here: http://www.kde.org/screenshots/.\n\nWhat is the rationale behind this style? It sounds like a trend as Microsoft Windows Vista also has a dark default theme (I do not know about the other themes, if any exists ;-) ).\n\nMaybe displaying dark colors requires less energy. (BTW, does anyone know?) But this is probably not the reason!\n\nI tried to find more colorfull and brighter themes (on kde-look), but I did not succeed. I also tried to change the color of the taskbar, but I could not find the option. Is it possible to change that color and to tune the look of this taskbar?"
    author: "Hobbes"
  - subject: "Re: About the themes"
    date: 2008-07-16
    body: "You may use the aya theme wich adapts automatically to the colors of the window theme (Oxygen is quite clear by default, isn't it?)"
    author: "Sinok"
  - subject: "Re: About the themes"
    date: 2008-07-16
    body: "Also, what is a problem, I think, all the themes included by default are similar dark themes (although there is the Aya theme which follows the system colors) and the user does not have the option to modify the color. In KDE 3, while it follows the system color by default, the user can set any custom background, and not just select from a list of themes, most of which have a black or dark grey color."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: About the themes"
    date: 2008-07-17
    body: "\"In KDE 3, while it follows the system color by default, the user can set any custom background, and not just select from a list of themes\"\n\nDo I understand that right that you are not able to select an arbitrary wallpaper? I can. Right beside the wallpaper selector there should be a button which lets you choose an image file to use as wallpaper."
    author: "Stefan Majewsky"
  - subject: "Re: About the themes"
    date: 2008-07-16
    body: "The widget style has a light and \"airy\" theme (Oxygen) as do the window decorations (Ozone). But the default Plasma theme is dark and dreary."
    author: "David Johnson"
  - subject: "Re: About the themes"
    date: 2008-07-16
    body: "I think black is sexy personally.  I like dark themes.\n\nTo each their own.  I'm pretty sure in time we'll see all kinds of colors for plasma themes.  I believe there is a \"fluffy bunny\" theme right now which is bright pink."
    author: "T. J. Brumfield"
  - subject: "Re: About the themes"
    date: 2008-07-16
    body: "> Maybe displaying dark colors requires less energy. (BTW, does anyone know?)\n\nIt depends on your screen.  CRT and plasma displays both draw more power to light a pixel than not to light it.  TFTs and normal projectors use a light source that is always on, so the power draw for a white screen is the same as for black.\n\nThe amount of power saved (for CRT and plasma) is not huge either, because the device still has to run all of its other circuitry for processing the signal and so on.\n\nYou can be sure that saving power was not on the agenda of anyone creating a darker theme.  If you really want to save power, turn everything off!  :-)\n\n-- Steve"
    author: "Steve"
  - subject: "Re: About the themes"
    date: 2008-07-17
    body: "It doesn't require less energy, unless you are using a CRT monitor.\n\nI use a dark theme as it's just easier on my eyes when indoors."
    author: "NabLa"
  - subject: "Re: About the themes"
    date: 2008-07-17
    body: "Personally I think that new KDE looks really bad. \nIt is a failed attempt to mix crapy Vista and OS X and then call it \"new\".\nThere is nothing original about the new KDE GUI.\nLets hope that 3.5 is supported and updated until KDE hires an artist who can come up with a new KDE look and feel that also makes sense and is original too.\n\n\n\n"
    author: "Responses to "
  - subject: "Re: About the themes"
    date: 2008-07-18
    body: "Are you actually under the impression that KDE is an organization capable of actually hiring someone? That KDE is produced by paid people? Well, you are wrong. All design work on KDE4 is volunteer work -- and personally, I think KDE4 looks really great. But neither my nor your opinion has any worth, you are an anonymous coward so you don't have any credentials at all anyway, and I'm only a developer, so I don't have any design credentials until I prove otherwise."
    author: "Boudewijn Rempt"
  - subject: "Re: About the themes"
    date: 2008-07-18
    body: "Yes, KDE4 takes some stuff from Vista and OSX. Linux desktops always take some ideas from other desktops. Frankly, so do proprietary desktops do too (Windows was originally said to be too Mac-like). However, there is plenty of originality in Plasma.\n\n * The \"everything is a widget\" design: I've used Vista and OSX and cannot place panel stuff on the Dashboard/Sidebar, nor can I place Dashboard/Sidebar widgets on the panel.\n * The ZUI\n * SVG themes\n * The ability to use non-native widgets: Plasma can currently use dashboard widgets, and other widgets are planned.\n * compatibility with touch screens: yes, that's what the cashew is for.\n"
    author: "Michael \"troll feeder\" Howell"
  - subject: "visual style and acceptance"
    date: 2008-07-19
    body: "What I find interesting is that both Vista and KDE4 have followed a dark visual theme, and both seem to generate very intense, negative reactions.  There are certainly valid reasons for some of those reactions, but the level of intensity which is rarely seen against other offerings makes me wonder if there's a common issue there, and if it might not be the appearance."
    author: "Lee"
  - subject: "Dark Color Schemes: just don't open konqueror"
    date: 2008-07-22
    body: "I may post this again on the next big dot announcement (final realease is real soon) to get more attention... I want some feed back.\n\nI like dark color schemes personally, but I find dark color schemes have a big problem when you try to open up certain webpages... the dot, I think, is actually a big offender.  Another bad site in facebook (maybe that will change with the new interface).\n\nThe problem is that the html page is rendered using the same color scheme as the desktop environment... this causes a lot of problems.  My solution was to call setQPallete (i think that was the command) early on in the life of the khtmlpart, and set a generic color scheme that was a standard \"paper\" colored scheme.  There were some complains about this on the k-c-d list, I'll repost them.  But I still think the idea is valid: when rendering a webpage, it shouldn't look different just because someone has an unusual color scheme; thats just asking for trouble.  The whole point of a webpage is for it to render the same everywhere.\n\nThis is something I'm perfectly willing to fix but its more a matter of what my follow users and developers think need to be done.  What are you opinions?"
    author: "Level 1"
  - subject: "Re: Dark Color Schemes: just don't open konqueror"
    date: 2008-07-22
    body: "I think you're placing your suggestion in the wrong place. You need to get onto the right mailing lists and irc channels, figure out who the people are who care about color schemes in KDE (Mattew Woehlke springs to mind) and the people working on khtml and start discussing your ideas directly with them, possibly sweetening them with some patch or prototype. The dot isn't really a place where developers discuss their work, but if you got to the right place, you'll quickly find yourself embraced and extended :-)"
    author: "Boudewijn Rempt"
  - subject: "Vertical Panel not ok"
    date: 2008-07-16
    body: "On wxga resolutions it makes sense to use a vertical panel.\n\nUnfortunately the task manager does not look well, since it does not put the text in vertical direction.\n\nabout:blank\n-\nKonqueror\n\ninstead of\n\nK\no\nn\nq\nu\ne\nr\no\nr\n\nAlso, the main launcher icon does not scale well\n\n:-("
    author: "Yves"
  - subject: "Re: Vertical Panel not ok"
    date: 2008-07-16
    body: "Actually... I think the vertical name as you want it is tricky to read.\n\nOn your screenshot, however, the reason that it is hard to read the horizontal name, is because the buttons are rendering the icon to the left of the text, and not offering a lot of room for the text, which is then faded out when it becomes too wide for the narrow space.\n\nWhat is needed, is a quick patch to the buttons to change the button rendering to icon-above-text when the taskbar is vertical, which ought to fix the problem, more or less..."
    author: "Luke Chatburn"
  - subject: "Re: Vertical Panel not ok"
    date: 2008-07-16
    body: "it's even funnier if you shrink the panel to not use the whole screen from top to buttom. than it uses the default grafics instead of the grafics for the right location. look at the screenshot to see what i mean. the left bar is ok, the right bar looks... not that nice."
    author: "Manuel Mommertz"
  - subject: "Re: Vertical Panel not ok"
    date: 2008-07-16
    body: "File bugs!\n(here's one http://bugs.kde.org/show_bug.cgi?id=157644)"
    author: "Yuriy Kozlov"
  - subject: "Re: Vertical Panel not ok"
    date: 2008-07-17
    body: "https://bugs.kde.org/show_bug.cgi?id=166822"
    author: "Manuel Mommertz"
  - subject: "Re: Vertical Panel not ok"
    date: 2008-07-16
    body: "This is actually a regression (WoC?), because I distinctively remember it working in an older KDE4 version. It's a problem now (it doesn't work anymore on my system either), though.\n\nYes, you should file it as a bug.\n"
    author: "Michael \"nothing\" Howell"
  - subject: "Re: Vertical Panel not ok"
    date: 2008-07-19
    body: "It appears to me that neither of the two possibilities is \"correct\" since if you have a wider vertical Panel you might want horizontal text.\n\nSimple solution is to have the widget configurable for either vertical or horizontal text.  This is needed in any case because you need to be able to choose which one you want with a Task Manager that isn't on a Panel.  Being able to choose icon size and position (just like with a toolbar -- that includes text only or icon only) would also be nice.\n\nHow is the clock on a vertical Panel?  In KDE-3, it only works if the Panel is wide enough.  Is this a similar issue?"
    author: "JRT"
  - subject: "press contact africa"
    date: 2008-07-16
    body: "mentions Uwe as contact."
    author: "elveo"
  - subject: "Re: press contact africa"
    date: 2008-07-16
    body: "which is to be found here, ofc:\nhttp://www.kde.org/announcements/announce-4.1-rc1.php"
    author: "elveo"
  - subject: "Trying it now"
    date: 2008-07-16
    body: "It works. Visually it seems crisper.  \n\nLooking forward to getting Kontact back.\n\nThank you. Just have to learn how to use new features. :)\n"
    author: "Gerry"
  - subject: "Promising"
    date: 2008-07-16
    body: "This is my first attempt at Kde 4.\n\nI must say I am impressed and I'm looking forward to installing it as my base desktop environment. I'm running debian and I'll probalby wait until they move it to testing.\n\nAs for the things that bug me, there are some :(, I will look at he bug reports and file my comments there.\n\nKeep going. At last something new and exciting in the DE world."
    author: "Paul Thomas"
  - subject: "Re: Promising"
    date: 2008-07-16
    body: "Gotta agree, I've been using openSUSE's KDE development branch for a while now (following KDE 4.1's development) and have found it to be quite stable and with very few issues. The only crashes I've really had were with KNotify randomly going down (which is something that happened to me randomly in KDE 3 as well on other machines... KNotify has always seemed like the first thing that'd go down if there was any issuue at all). All in all, KDE 4.1 is extremly nice and rather stable for me, can't wait to see 4.2's path!"
    author: "Kitsune"
  - subject: "Re: Promising"
    date: 2008-07-17
    body: "I am waiting to Debian Lenny to have all of these beauties (eye candy) and new and improved functionalities that have been promised. I cant wait to try it, but I can not give me the lust of trying it in another distro. Keep going, you are getting closer to something bigger than everything seen so far..."
    author: "Hern\u00e1n Lorenzo"
  - subject: "Re: Promising"
    date: 2008-07-17
    body: "I have installed the experimental packages in Lenny and they work quite well."
    author: "AB"
  - subject: "Re: Promising"
    date: 2008-07-17
    body: "I am using KDE 4.1 Beta 2 from Debian experimental as my main desktop environment for a couple of weeks now and I am quite happy with it. It is quite stable and with the tips given on http://techbase.kde.org/User:Lemma/KDE4-NVIDIA compositing works quite well with my NVidia 8600 GT. The only serious bugs I encountered is that KMail crashes if you switch between folders to fast and that some plasma applets do not work. But I hope they get fixed before the release. The first packages for KDE 4.1 RC1 are already in incoming.debian.org."
    author: "Michael Thaler"
  - subject: "Kde 4.0.98"
    date: 2008-07-16
    body: "I am trying to compile it but kdelibs 4.0.98 soon failed and complained about a \"kdesupport\" package.\n\nWhen I asked on IRC I was told that kdesupport is provided by my distro (i have no distro, i run a \"source distro\" on my own....) and that it is in fact not \"one\" package but \"multiple\" packages (how informative....)\n\nIt would really be easier if the last known URL to packages like these would be detailed. After all\nyou guys use cmake which is supposed to be better than autoconf, so why not take advantage of \nit and help the users too. At least with modular xorg you normally get exactly what is missing,\nand although the complexity in modular xorg is annoying, I can at least tackle the problems\none after the other.\n\nHow difficult is it to say that, if you need qimageblitz, to just say so (as example)? Doesnt \neven need to be an URL, i can google on my own, but in this case googling didnt really help nor \ndid the official wiki.\nOr that one needs \"akonadi\"? I saw an email from april about this being in kdesupport.\nSaying that one needs \"kdesupport\" when this is hard to GET or download, is kinda a \nbad way.\n\nCompiling something is normally not a huge,nsurmountable  problem for me, more or less - after a little work things normally will compile, if the author didn't make a bad job in the first place. \n\nI verified that between kdelibs 4.0.98 and 4.0.85 there must have been some change, because kdelibs-4.0.85 did not complain to me about an ominous \"kdesupport\" package and in fact finished creating its cmake-job (i didnt start to compile it because I dont see why i should compile 4.0.85 when 4.0.98 is out)\n\nSo, if one wants to build from source, this URL is not enough at all:\n  http://www.kde.org/info/4.0.98.php\n\nBut the name of the package \"automoc in kdesupport\" demands that users have some dependencies\nWHICH ARE NOT DESCRIBED IN DETAIL to the user who wants to compile kde 4.0.98 from the \nofficial tarballs!\nSo what is a user supposed to do here?\n\nI was told to look at:\nhttp://techbase.kde.org/Getting_Started/Build/KDE4#Required_packages_from_your_distribution\n\nBut this also does not help AT ALL for several reasons. Where is kdesupport?\n\nThe first google result is:\nwebsvn.kde.org/trunk/kdesupport/ \n\nCompiling Kde 3.5.9 is really easy compared to how often kde 4 changes those things, \nI am getting irritated. I have no problem if things change quickly, but I am getting left \nout if there are constant referrals to \"use your distribution\" instead of telling \npeople what package they need to use. Not everyone is a newbie, really ...\n\nThanks for anyone who can help me though. If anything, maybe this post can help to smoothen\nout things for the next release, or for others who will have similar problems."
    author: "markus"
  - subject: "Re: Kde 4.0.98"
    date: 2008-07-16
    body: "Looking around on one of the mirrors this looks like the package you'd want: http://ftp.ussg.iu.edu/kde/unstable/snapshots/kdesupport.tar.bz2\n\nThe page you linked to (http://techbase.kde.org/Getting_Started/Build/KDE4) includes a section about updating kdesupport, although thats unfortunately only if you're using SVN."
    author: "Kitsune"
  - subject: "Re: Kde 4.0.98"
    date: 2008-07-16
    body: "It was somewhat disconcerting when I found the following message a while ago: \"The xxx repository is no longer consistent with KDE trunk. Please install the xxx package from your distribution.\"\n\nBut wait, this is from the kdesupport README file in trunk!"
    author: "David Johnson"
  - subject: "Re: Kde 4.0.98"
    date: 2008-07-18
    body: "> Compiling Kde 3.5.9 is really easy compared to how often kde 4 changes those things,\nI am getting irritated.\n\nKDE 4 isn't even feature complete yet, nor has it had the time of bug fixing KDE 3 had. Nothing to be irritated about."
    author: "Carlo"
  - subject: "Re: Kde 4.0.98"
    date: 2008-07-18
    body: "This always worked for me.\n\nInstall subversion.\n\nThen :\n\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/kdesupport\n\nI am now using trunk version but I think this should work with RC1 tarballs too."
    author: "SVG"
  - subject: "Re: Kde 4.0.98"
    date: 2008-07-18
    body: "We moved the automoc tool out of kdelibs into kdesupport. This makes it possible that cmake based non-KDE applications can also use automoc, e.g. phonon, akonadi and other Qt-only packages.\nWe did this maybe 2 months ago, but it was optional until maybe 3 weeks ago, probably that's why not everything wasn't updated yet.\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Kde 4.0.98"
    date: 2008-07-21
    body: "But still that would be GREAT if you created a tarball of extra apps/libs required to build KDE4.\n\nThings that live only in SVN sometimes do not not even compile ;-) SVN version of kdesupport may have features incompatible with release - since release happens at a defined time, while trunk version keeps changing."
    author: "Artem S. Tashkinov"
  - subject: "Bindings"
    date: 2008-07-16
    body: "Some days ago I saw that PHP-Qt bindings where integrated into KDE, so it occurred me, what bindings KDE currently support?\nAll bindings supported by kdelibs are also supported by plasma?\n- I want to create some nice plasmoids with PHP :-)\n\nThanks in advance."
    author: "Iuri Fiedoruk"
  - subject: "Re: Bindings"
    date: 2008-07-16
    body: "At the moment you can write plasmoids in C++, QtScript, Ruby and C# (with Python to come fairly soon I believe). There are C++, Python, Ruby and C# bindings for kdelibs.\n\nWe hope to get the PHP-Qt bindings up to speed with respect to the Ruby and C# ones and their coverage of the kdelibs apis for KDE 4.2. Before then we hope to have some PHP Plasma bindings working."
    author: "Richard Dale"
  - subject: "Re: Bindings"
    date: 2008-07-16
    body: "Nice to know, great news!\n\nI do not use C++ because I think writing things as plasmoids in a compiled language does not make much sense for being small things, that would be faster developed. C# for patent/litigation/m$/etc reasons is out, and I do not want to learn even one more language (my head will hurt if I pass of 10).\nSo my hopes are in Plasma bindings for PHP, that would allow me to write a plasmoid in no-time and help plasma a bit, once after all the criticism I've made on it, I'm in debt :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Bindings"
    date: 2008-07-16
    body: "Well, it now has (some) support for Mac Dashboard widgets, which are written entirely in HTML, CSS and Javascript. I expect PHP wouldn't be so hard to implement."
    author: "Madman"
  - subject: "Re: Bindings"
    date: 2008-07-17
    body: "Implementing PHP Qt and Plasma bindings support is solving a different problem, than supporting HTML, CSS and Javascript via WebKit. We are not using web based apis like the DOM, but Qt ones like QObjects, QGraphicsView features, slots and signals and so on. It may be possible to integrate PHP with WebKit too, so that you could use it like you use Javascript, I'm not sure."
    author: "Richard Dale"
  - subject: "Re: Bindings"
    date: 2008-07-17
    body: "Also, just my 2C, wouldn't PHP bindings allow you to reuse code from your web services in  Plasmoids?\n"
    author: "Michael \"Web Services\" Howell"
  - subject: "Re: Bindings"
    date: 2008-07-16
    body: "I'm hoping proper Python support comes by KDE 4.2 because I have all sorts of ideas that I would like implemented... (and sadly I only \"speak\" Python)."
    author: "Luca Beltrame"
  - subject: "Re: Bindings"
    date: 2008-07-16
    body: "Python support for Plasma shouldn't be a problem for 4.2. Someone else has already made a good start on the problem and has handed their work over to me to integrate it proper into KDE SVN.\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: Bindings"
    date: 2008-07-17
    body: "Proper Python support for KDE/Qt libs are already present, so it's no problem writing Python applications for KDE. By all accounts the most comprehensive and up to date set of Python bindings for any desktop.\n\nFor Plasma I guess you have to wait some more."
    author: "Morty"
  - subject: "Re: Bindings"
    date: 2008-07-17
    body: "I was actually referring to Plasma Python bindings. I know about PyKDE, actually I'm learning PyQt so I can actually use PyKDE in the end."
    author: "Luca Beltrame"
  - subject: "Re: Bindings"
    date: 2008-07-16
    body: "> so it occurred me, what bindings KDE currently support?\n\nThat info is on the 'base, (and even updated on the weekend. wow!)\n\nhttp://techbase.kde.org/Development/Languages\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "KDE-Look bug (this is not a KDE bug!)"
    date: 2008-07-16
    body: "I have an important bug to report that has nothing to do with KDE itself.\n\nEach time I want to install a new Plasmoid using KHotNewStuff2 (from within KDE itself), KDE says that it installed successfully the Plasmoid, but I can't use it. Later, I went to http://www.kde-look.org, and what I found was: no Plasmoid was in a format usable by the KHotNewStuff installer. All Plasmoids were in a beautiful source-code-state, with instructions as useful as \"cmake -DI_DONT_KNOW_WHAT_THE_HELL_IS_HAPPENING_HERE; make install\", or something like that.\n\nPlease, provide in your KDE-Look website packages usable by KHotNewStuff2. I want to try all Plasmoids out there! ;)\n\nThanks for your amazing release."
    author: "Alejandro Nova"
  - subject: "Re: KDE-Look bug (this is not a KDE bug!)"
    date: 2008-07-17
    body: "kde-look.org is not run by KDE project members. You will have to ping the kde-look administrators for this bug."
    author: "Stefan Majewsky"
  - subject: "Re: KDE-Look bug (this is not a KDE bug!)"
    date: 2008-07-18
    body: "OK ;)"
    author: "Alejandro Nova"
  - subject: "Re: KDE-Look bug (this is not a KDE bug!)"
    date: 2008-07-18
    body: "Hi,\n\nI\u00b4m running KDE-Look.org and I am a member of the KDE project. :-)\n\nThis is a known problem. Most of the plasmoids on KDE-Look.org are C++ applications. GHNS has to download this apps, compile them and install them. This is not possible and, of course, not a good idea.\nThe GHNS system becomes more usefull as soon as the python bindings are stable ad more plasmoids are written in scripting languages\n\nCherrs\nFrank"
    author: "Frank"
  - subject: "Re: KDE-Look bug (this is not a KDE bug!)"
    date: 2008-07-18
    body: "Would it be possible for people uploading plasmoids to KDE-Look to tell KDE-Look what language the program is written in? This would get rid of the problem (and allow for KHotNewStuff to check if the needed interpreter is installed ;)."
    author: "Michael \"a way to fix this\" Howell"
  - subject: "Thank you !"
    date: 2008-07-16
    body: " I just want to say <B>I love you</B> !"
    author: "BogDan Vatra"
  - subject: "Re: Thank you !"
    date: 2008-07-17
    body: "Hello </B>\n\n<B> will be very happy to hear that ;)\n\nSCNR"
    author: "furanku"
  - subject: "A few random things..."
    date: 2008-07-16
    body: "-when moving applets around on the panel, sometimes the \"Task Manager\" takes up the entire space so you can't add anything else to the right of it\n\n-it would be nice if the file transfer dialog would stop popping up in Konqueror (\"Web Browsing\" profile) when your opening links to pictures, text documents, etc\n\n-Konqueror's search bar is gone. Is this avaliable as an extension?\n\n-KGetHotNewStuff dosen't work when trying to download new plasmoids. Wallpapers/themes work however\n\n-KGet's bittorrent plugin dosen't work. The status says \"Stopped\" and you can't do anything about it. Ktorrent works\n\n-KDE people seem to have an obession with clocks but the digital clock placed on the panel looks so ugly. I miss the old one in KDE3 which looked like an LCD\n\nOtherwise I love it. The KDM theme/Bootsplash looks great. I plan on switching to KDE 4.1 when FINAL is released. \n\nAmazing job KDE devs!"
    author: "JS"
  - subject: "Re: A few random things..."
    date: 2008-07-16
    body: "BTW I'm using Kubuntu 8.04.1 incase some of my problems are distro specific."
    author: "JS"
  - subject: "Re: A few random things..."
    date: 2008-07-16
    body: "Train station clock for the win."
    author: "T. J. Brumfield"
  - subject: "Re: A few random things..."
    date: 2008-07-17
    body: "+1. I would work on it myself if I knew where it was.\n"
    author: "Michael \"+1\" Howell"
  - subject: "Re: A few random things..."
    date: 2008-07-18
    body: "I think it is at \n\n(With subversion installed)\n\nsvn co svn://anonsvn.kde.org/home/kde/trunk/playground/base/plasma\n\nor\n\nsvn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdeplasma-addons\n\n\nWell, all plasmoids (that are not officially released yet) are being worked on these paths.\n"
    author: "SVG"
  - subject: "KDE 4.1 SVN"
    date: 2008-07-16
    body: "I recently decided to try out kde4 SVN and i must say i am very much impressed with the kontact suite. everything works as a charm and the new look is very awesome looking.\n\nit looks like the wait between 4.0 and 4.1 without the pim tools was well worth the wait!\n\nthanks guys!!"
    author: "Mark Hannessen"
  - subject: "KDE4 is really fast"
    date: 2008-07-16
    body: "I gave it a try, after all the negative comments I read for a while.\n\nAnd I must say that KDE4 is really impressive and faster than my 3.5.9 version.\nFor sure more work needs to be done on plasma, screen saver, but overall after working on it today, I won't switch back to 3.5\n\nI'm impatient to see what 4.2 and next will bring to my desktop. I have made my coworker(using XP) more jealous than ever today.\n\nGreat job and many thanks to the KDE team !!"
    author: "JC"
  - subject: "Thanks"
    date: 2008-07-16
    body: "thanks for all the hard work everyone has done to bring us this release."
    author: "R. J."
  - subject: "damn ! feels like a new PC!"
    date: 2008-07-17
    body: "I allways used SuSE, but 11.0 is so great, it's so much faster (especially yast-package manager) and kde 4.x feels also much more robust than kde3 in relation to opensuse 10.3.\nI feel like i have a NEW PC ! really! updating packages with yast is so quickly now. i wonder, if my previous installation/updates of SuSE 10.x and earlier were buggy! but all the updates in the past just run fine.\n\nFeatures, look and feel of kde4.x is already more than usable!\nbelieve me, i tried kde4 since 4.0.0, though, you might still need some favorite\nkde3 applications like k3b or amarok. After 4.0.3 i switched to daily/weekly development snapshots. Lately, updates are rare."
    author: "opensuse11.0"
  - subject: "Still far from usable for me"
    date: 2008-07-17
    body: "It still has a way too many (mostly regression) bugs (see bugs.kde.org). \nStill no kprinter and khotkeys. I know that people are working on that but only one person is not enough for these flagship components of KDE. Many of us are using KDE because of it (and not gnome or something else). It's really sad to see that skilled developers are working on useless eyecandy and ignoring this.\n\nAnd btw., plasma looks promising but is pain to use. It is designed for touchscreens, and  majority of us doesn't have a one. Why we are forced to use applet handles? I want to use applets as every other window. Imagine if every regular window had had this handle for move or resize actions, who would use kwin. Alt+LMB for move and Alt+RMB for resize (and so on) would be reasonable defaults."
    author: "vf"
  - subject: "Re: Still far from usable for me"
    date: 2008-07-17
    body: "I forgot to add... can someone update this list:\n\nhttp://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan\n\nso we could know what's finished and what isn't (and move that to 4.2 page)."
    author: "vf"
  - subject: "Re: Still far from usable for me"
    date: 2008-07-17
    body: "The page is up to date as far as I can see. A more condensed list of new features is available at http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals\n\n"
    author: "sebas"
  - subject: "Re: Still far from usable for me"
    date: 2008-07-17
    body: "> It's really sad to see that skilled developers are working on useless eyecandy and ignoring this.\n\nIn a volunteer project, you can't demand people work on one system as opposed to another.  People work on the systems they know and enjoy.  The people developing plasmoids may not know anything about writing printing systems.\n\nYou can't assume/suggest that one is stealing development focus from another."
    author: "T. J. Brumfield"
  - subject: "Re: Still far from usable for me"
    date: 2008-07-17
    body: "Well, I know that it's a volunteer project, but if there's no more people that can work on stuff that was once essential part of KDE, then I'm not sure if they can expect a successful future of their project. :-/"
    author: "vf"
  - subject: "Re: Still far from usable for me"
    date: 2008-07-17
    body: "*sigh* - welcome to every software project on the planet! Can people cut out the Chicken Little-ism, or at least make sure it's well-founded? People have been heralding the death of KDE for years, now, and it's just getting annoying."
    author: "Anon"
  - subject: "Gwenview"
    date: 2008-07-17
    body: "Gwenview seems to have gotten some speedups, its quite fast now. Theres only one major problem: It won't show files without dots. aka \"img12345\" \"img67890\"... Recent digikam (kde3 version) has the same problem. Is this just a wrong setting, intention or a bug? Dolphin and Konqueror work as expected."
    author: "JJ"
  - subject: "Re: Gwenview"
    date: 2008-07-17
    body: "fill a bug @ http://bugs.kde.org/"
    author: "meh..."
  - subject: "Re: Gwenview"
    date: 2008-07-17
    body: "This is not reproducable here. Are you using the latest -rc?"
    author: "sebas"
  - subject: "Re: Gwenview"
    date: 2008-07-18
    body: "I nailed it. It was a nfs issue combined with gwenviews inability to show svg. All the files in the top dir were svgs and the subdirs were not getting mounted.\nDigikam 0.10 from svn 834341 still bugs me. Files are local this time, just to be sure ;-) 0.9.3 gives the same result. All files without a dot \".\" in their names are ignored. In the configuration I have added \"*\" to the list. The same goes for ShowFoto, it displays the thumbnail but claims the image could not be shown. I will clear out my kde4 config and dirs and rebuild all modules to see if this bug stays."
    author: "JJ"
  - subject: "Welcome plasmoid"
    date: 2008-07-17
    body: "I noticed this in the 4.2 Feature Plan.\n\nI'm curious what the plan for this is."
    author: "T. J. Brumfield"
  - subject: "Re: Welcome plasmoid"
    date: 2008-07-17
    body: "long time ago there used to be a early screenshot of this (I guess at Aarons Blog) and it appears to be basically what its says it is. A plasmoid that sits on the desktop at first starts and links to introductions and similar stuff."
    author: "Peter"
  - subject: "Re: Welcome plasmoid"
    date: 2008-07-17
    body: "Yes, and also as a replacement for the First Time Wizard from KDE2-3."
    author: "Morty"
  - subject: "Re: Welcome plasmoid"
    date: 2008-07-17
    body: "For instance, when you install openSUSE and log on to your desktop for the first time, a welcome screen is shown with information about how to get updates, a link to openSUSE etc. It might be something like that."
    author: "Stefan Majewsky"
  - subject: "Communication++"
    date: 2008-07-17
    body: "\"While it is not yet up to the features that people are used to from KDE 3.5, KDE 4.1 provides a significant amount of improvements over KDE 4.0, ...\"\n\nI'm really glad to see this.  Thanks for making it clear where KDE 4.1 stands.\n\nFor the record, I've been using SVN as my main desktop at home for a while now, and 4.1 rocks.  Panels are now mostly-configurable, and folder view is quite nifty.\n\nI'm looking forward to seeing what 4.2 brings."
    author: "CondorDes"
  - subject: "Order plasmoids and folderview"
    date: 2008-07-17
    body: "Most be on the heads of developers but it would be great to be able to order Plasmoids in the desktop. Especially Folderview, and to resize them according to a grid so my desktop don't becomes a mess. Like 3.11 (yes its a joke :-D)\n\nLove this kde 4... thing !!!! Really cool to use!"
    author: "jorge"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-17
    body: "There is a Grid Applet in KDE playground:\n\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/grid\n"
    author: "cloose"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-17
    body: "I agree.  It would be nice to auto-size the folder plasmoid to just show the folder name and to automatically expand when the mouse hovers over it or just click on it.\n\nKDE4 is looking great now.  Hopefully all those \"supposed techies\" that got their knickers in a twist over 4.0 because they could not read/comprehend may now crawl back into their basements."
    author: "Ian"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-18
    body: ">... It would be nice to auto-size the folder plasmoid to just show the folder name and to automatically expand...\n\nA good analogy for that in real life would be suspension files in a filing cabinet and using the tabs at the top of the files to find the appropriate folder before opening it.  I think this would be a sensible extension to the folderview concept, and as it has strong links to a similar workflow in real life it should be pretty easy for people to see how it could be used.\n\nIf the panes collapsed down into tabs like the BeOS window manager grouped windows (or the icons on the bottom of the Kickoff menu, or the tabs in Konqueror) this would reinforce the analogy.\n\nRe: the GP post about Windows 3.1:\n\nUntil you mentioned it I hadn't thought of folderview as being a bit like the groups in program manager.  I thought program manager worked well and missed it when it was dropped for Win95 (although progman.exe was still there if you looked for it).  \n\nThat said, folderview is more featureful than program manager, but your comment has made me think of other ways of using the folderview plasmoid :-)\n\nDoes anyone know if the folderview plasmoid can minimise/collapse down to an icon on the desktop?  (I guess this is conceptually quite similar to the parents suggestion)."
    author: "Matt"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-20
    body: "I have been thinking about that for quite a while now. I wanted to add it to the wish list but somehow didn't. That would really be great and takes up only little space on the desktop, in contrast to the present folder view. Folder view is a wonderful idea but it it doesn't look all that sexy yet."
    author: "Bobby"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-17
    body: "One thing I would like to see is better move/resize.\nFor example, if I cant to increase a plasmoid size that is on the left side of desktop, as I can't move the mouse more to the left (the plasmoid handler is on the left side), I have to move it to the right, then resize, then place it on the right place I want again.\n\nI would like to see plasmoids inside kwin instead of this desktop-think where only folderview is usefull (to me). :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-17
    body: "Yes, I think resizing the folderview plasmoid is a bit problematic. I think most people want to have it fitting for the numbers of item in it. But when you resize it you cant tell when it has the correct size as the current view simply scales and the icons are not reordered during the resizing. So you end up with estimating the size, trying to keep that in mind, resize eth folderview and repeat that process until the size is right. Not quite an elegant solution.\n\nAnother thing is that it's a bit inconsistent as it scales around the center and not around one corner as \"ordinary\" windows do, you have to get used to two different types of resizing."
    author: "furanku"
  - subject: "Re: Order plasmoids and folderview"
    date: 2008-07-17
    body: "I agree that the Plasma desktop needs at least some basic DTP-like layout mode. For example, by introducing helpers, i.e. horizontal/vertical lines which are used to align the plasmoids (\"snip\"). And it should be possible to define a \"reference point\" for zooming/rotating/moving. \nIt bothers me as well that I can not arrange the plasmoids properly on my desktop. \n\nBut anyways. At the moment I like the way how the desktop looks on my machine - it is totally empty. And this is okay for the 4.1 cycle.  "
    author: "Sebastian"
  - subject: "Cashew"
    date: 2008-07-17
    body: "how can I disable the cashew? I don't have a touch screen."
    author: "David"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "Give one valid reason why you need to remove it, and I'll tell you."
    author: "Morty"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "Give me one solid reason why should I have it if I don't need it."
    author: "David"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "Because nuts are good for you."
    author: "Anon"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "It's already there and it's presence does not impede you in any way, removing it are superfluous. "
    author: "Morty"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "Without wanting to give oil into some fire I have to agree with the original poster. As an engineer I follow the philosophy that a technology is optimal only  when you can not remove anything without harming functionality. The cashew is NOT required in most use cases... And it is visually not very appealing. As you see, there is no reason to give crude answers to valid questions. But anyway, I am pretty sure that the KDE developers care and that they will find a way to remove it in upcoming releases... "
    author: "Sebastian"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "As an fellow engineer I have to point out that your application of the philosophy in this case are flawed, since making the cashew removable would add functionality not remove:-)\n\nMy intention was not to be crude in my answer, only to get a reason why he wanted to remove the cashew. Any answer would have to fall in to four categories. Bugs in the implementation needed to be addressed by the developers, valid reasons for the developers to implement removability, missunderstanding or just joining the choir."
    author: "Morty"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "I recommend \"relaxation\". It helps! :)"
    author: "Mark Kretschmann"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "what does not having a touch screen have to do with the cashew? *shakes head*"
    author: "Aaron Seigo"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "What does the Cashew have to do with anything?"
    author: "Mark Hannessen"
  - subject: "Re: Cashew"
    date: 2008-07-17
    body: "from the FAQ:\n\nPlease provide an option to disable the upper right cashew. \nAlthough putting an option to disable the cashew for desktops sounds reasonable, from a coding point of view it would introduce unnecessary complexity and would break the design. What has been suggested is, since the destkop itself (a containment) is handled by plugins, to write a plugin that would draw the desktop without the cashew itself. Currently some work (\"blank desktop\" plugin) is already present in KDE SVN. With containment type switching expected by KDE 4.2, it is not unreasonable to see alternative desktop types developed by then.\n\nand for all those people like me who where wondering what on earth is referred to by the word cashew:\n\nWhat are the \"cashews\"?\nWhat is commonly referred as \"cashew\" is the Plasma logo you can find on the default desktop, on the upper right corner, and on the panel, on the right hand side. By clicking on them, you can access other configuration options, such as panel configuration and the Zooming User Interface (ZUI). Some of these, like the panel cashew, only appear if the widgets aren't locked (see below).\n"
    author: "Mark Hannessen"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "Glad that someone else posted it: at least it means that all the typing I did was for good!"
    author: "Luca Beltrame"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "> from a coding point of view it would introduce unnecessary complexity and would break the design\n\nWhy removing a detail on the screen, like the cashew, breaks KDE design? I think KDE really needs a Software Architect..."
    author: "Julio"
  - subject: "Re: Cashew"
    date: 2008-07-26
    body: "Are you available?! KDE can hire you! You seem sooooo competent!!"
    author: "Vide"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "openSUSE patched their Plasma packages for 11.0 so that the desktop cashew (but not the one in the taskbar) can be at least removed via a configuration file. The patch has been refused by upstream (aka Mr. Seigo) as far as I know."
    author: "Erunno"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "AFAICS, it was refused because it broke the design: rather than introducing an option, someone would have to write a new containment plugin without the cashew."
    author: "Luca Beltrame"
  - subject: "Re: Cashew"
    date: 2008-07-18
    body: "Actually I didn't even try to push that patch upstream as there have been several discussions about (optional) removal of the cashew before..."
    author: "binner"
  - subject: "Re: Cashew"
    date: 2008-07-21
    body: "Yes, please make this abomination in design disappear! It's nothing more distracting than having the background of your screen dynamical just because you happen to touch the area in question while maximizing a window! Whoever came up with that brilliant idea shouldn't ever be allowed near UI design again."
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Cashew"
    date: 2008-10-16
    body: "Well a great lot of discussion going on here i see. \nI too would like to remove the Cashew.. \n\nhere is my reason. \nI am locking down a Kubuntu box for a build i am deploying to multiple users in an environment where the end users are teenagers (read: will try to break the image) so i need to remove the panel, the desktop cashew and the ability to right click. \n\nthis will leave them with a blank desktop (except for the programs i leave launchers for) and once i have disabled a few other things like ctrl + alt + F1 etc. should be secure enough to let them on the pc's. \n\nalso if you guys have any suggestions for locking down kde or ubuntu i would be greatly appreciative. (am already using kiosktool)\n\nThanks.\n"
    author: "Neal"
  - subject: "knetworkmanager plasmoid"
    date: 2008-07-17
    body: "I could not find anything about the network-manager plasmoid :-(\n\nHow is the state of it?\n\nknetworkmanager is the only KDE3-app which I need, and I would like to ditch it for a nice KDE4 version :-)\n"
    author: "Yves"
  - subject: "Re: knetworkmanager plasmoid"
    date: 2008-07-17
    body: "Knetworkmanager is already being ported to KDE4 (by Suse). It's basically the same app, just adapted to new networkmanager version and KDE4 libs. Sadly, a plasmoid using Solid for network detection is not in sight yet.\n"
    author: "Thomas"
  - subject: "Re: knetworkmanager plasmoid"
    date: 2008-07-17
    body: "There's already a networkmanager plasmoid in playground (if I remember correctly)... although I was just able to test it on my tower -- so no wlan .. but the lan connection was shown.. have to test it more thorougly on the weekend\n\nAny news on the Knetworkmanager port? have never read of it again since the first announcements.. haven't found a branch too... would also be interesting to know if it'll make it for opensuse 11.1"
    author: "Bernhard"
  - subject: "Re: knetworkmanager plasmoid"
    date: 2008-07-17
    body: "Originally we were trying to get the network-manager plasmoid into 4.1.  We were unable to complete in time so we're hoping to be able to release it by the end of next month.  Unfortunately, real life has interferred with my plans.\n\nOriginally we plan to support ethernet, wlan, and attached 3G (CDMA/GSM) cards.  BT support will not be available in the initial release."
    author: "Christopher Blauvelt"
  - subject: "Re: knetworkmanager plasmoid"
    date: 2008-09-15
    body: "Wow, that sounds fantastic. Big thanks for your work on knetworkmanager. It constitutes a great contribution to the real-life usability of the linux desktop. Hope it'll make it into openSuse 11.1."
    author: "chris"
  - subject: "About Kopete..."
    date: 2008-07-18
    body: "yes..a little offtopic...I install KDE4.1 from Kubuntu Intrepid Ibex alpha 2..and i'm very surprizing because see more changes one of them I can move icons in panel..good work guys..\nanyway i want to know what's about kopete..I trying all KDE4.xx versions and I can't see buddy pictures, only a square empty and in RC1 none square only big ball green..don,t work webcam..don't work file transfers..all of them I try in Yahoo protocol..It's normal or I missing to install some packages ?\nmany tanks, all the best!"
    author: "neaghi"
  - subject: "Re: About Kopete..."
    date: 2008-07-18
    body: "Kopete for KDE4 just sucks. Where is IRC?\n\nI'm using amsn and gyachy for yahoo protocol. They are more complete, and Konversation from KDE3 for irc."
    author: "Ronnie Mills"
  - subject: "Re: About Kopete..."
    date: 2008-07-18
    body: "file transfers in yahoo were fixed yesterday"
    author: "Bille"
  - subject: "Re: About Kopete..."
    date: 2008-07-19
    body: "Come join the kopete bugday this sunday. ;)\nirc.freenode.net #kde-bugs\nYou just need a new kopete.\n\nI think the yahoo stuff was fixed recently. It will certainly get tested this weekend. :D"
    author: "blauzahl"
  - subject: "Phonon"
    date: 2008-07-18
    body: "Hi, just wondering if that problem with phonon not showing the title's names (it only showed the numbers) was fixed. I recall someone having made a fix, but it being rejected because it broke ABI stability? Has this been resolved?"
    author: "Parminder Ramesh"
  - subject: "incomplete features"
    date: 2008-07-18
    body: "Yes there are more incomplete features on KDE4 and how can you complete to KDE4 on july,29.\nI try to explain what are incomplete features;\n\n1- no panel auto hide\n2- there is no panel configuration( not flexible like kde 3.5)\n3- where is the align vertically and horizontally for icons on desktop\n4- there is no RDS widgets\n5- there is no preview any window on panel\n6- when run the konqueror and put any web site on address box and try to download flash plug in but still go to \"macromedia web site\", please change it.\nlast one is; I fell use like windows OS because there is no flexible and no changeable anything. I like KDE3 more than KDE4...."
    author: "Deniz"
  - subject: "Re: incomplete features"
    date: 2008-07-18
    body: "Then use KDE3 (it's still working wonderfully fine) :) As you mentionned it there is not enough time to complete those features, they would probably need a few monthes to be developed, a few monthes during which you wouldn't be able to use KDE4 for missing features, but also a few monthes during which people who don't need those features won't be able to use allready existing features, which would be a shame."
    author: "Cyrille Berger"
  - subject: "Re: incomplete features"
    date: 2008-07-18
    body: "Same with me. I prefer KDE4 4.1 to be released when the release dudes say it is. I do miss some of the features that the OP misses, but i'd rather work around the missing features and enjoy the splendor of KDE 4.1. If i miss application from KDE3, then i simply use it under the hood of KDE4, which works nicely at least on my openSUSE 11.0 box. Examples include KDE3/Kmail and KDE3/KDevelop, but i'm sure there are tons of others.\n<rant>(And while we're at it: To me, the OP sounded very much like he was paying huge amounts of money for a commercial desktop environment when he was complaining about missing features. Remember: This is open source software given to you for free mostly by developers who sacrifice their private time.)</rant>"
    author: "Planetzappa"
  - subject: "Open Source Development"
    date: 2008-07-18
    body: "A more important point than cost, and this is a point which a lot of people don't seem to realise, is that there is no better way to kill an open source project than for the developers to retreat into the \"hacking cave\" away from the public's eye for years and years on end, only to reemerge once their product has reached perfection. The open source model only works when done in public, and when the software is released, warts and all. It is this communication with the outside world which attracts new developers and feedback from the user base about what they like and need.\n\nThe trick is that the people in the community need to find positive ways of participating in the project which lead to improvements in the software itself. Contrast this to closed source software where the only thing you can do if you don't like something about the software is to complain and threaten to leave for a competitor. Two very different worlds with two very different mentalities.\n\n\n--\nSimon\n\n"
    author: "Simon Edwards"
  - subject: "Re: incomplete features"
    date: 2008-07-19
    body: "1 - That is right, it will most probably come in 4.2.\n2 - I hope that this will also be solved by 4.2.\n3 - This will (again) most probably come in 4.2 when the Folderview becomes more flexible and can be used as desktop without regressions.\n4 - What are RDS widgets?\n5 - Do you mean that you do not get a preview when you drag the cursor over the items on your window bar?\n6 - What?!? Sorry, but I do not understand that. You are viewing a website in Konqueror, and then you try to download the Flash plugin? Shouldn't that be delivered by your distribution?\n\nIt seems like you're not a native English speaker. That is no problem, but you might feel more comfortable in forums in your native language."
    author: "Stefan Majewsky"
  - subject: "Re: incomplete features"
    date: 2008-07-19
    body: "1 - That is right, it will most probably come in 4.2.\n2 - I hope that this will also be solved by 4.2.\n3 - This will (again) most probably come in 4.2 when the Folderview becomes more flexible and can be used as desktop without regressions.\n4 - What are RDS widgets?\n5 - Do you mean that you do not get a preview when you drag the cursor over the items on your window bar?\n6 - What?!? Sorry, but I do not understand that. You are viewing a website in Konqueror, and then you try to download the Flash plugin? Shouldn't that be delivered by your distribution?\n\nIt seems like you're not a native English speaker. That is no problem, but you might feel more comfortable in forums in your native language."
    author: "Stefan Majewsky"
  - subject: "Re: incomplete features"
    date: 2008-07-20
    body: "Thnak you very much for your reply but not enough !!!\n1 - That is right, it will most probably come in 4.2. / why?\n2 - I hope that this will also be solved by 4.2. /why ?\n3 - This will (again) most probably come in 4.2 when the Folderview becomes more flexible and can be used as desktop without regressions./ will wait more 6 months ?\n4 - What are RDS widgets? about news or something ?\n5 - Do you mean that you do not get a preview when you drag the cursor over the items on your window bar?/ exactly/ ?\n6 - What?!? Sorry, but I do not understand that. You are viewing a website in Konqueror, and then you try to download the Flash plugin? Shouldn't that be delivered by your distribution?/ you can try to kubuntu and opensuse and its same...\n\nIt seems like you're not a native English speaker. That is no problem, but you might feel more comfortable in forums in your native language./ yes English not my native language but its problem for you. Could you understand and you can answer to me as easy. whats problem for you???"
    author: "Deniz"
  - subject: "Installation of downloaded plasmoid crashes plasma"
    date: 2008-07-18
    body: "Yesterday i upgraded my beta2 to RC1 successfully and my most hated bug has been fixed, thanks folks! Also the overall experience with KDE4 is very nice, i'm using it as my default desktop since the betas.\n\nThis here is still unfixed: Trying to install a downloaded plasmoid crashes plasma. (Or, more exactly: Click \"add plasmoid (it's 'Miniprogramm' in german, dunno the english term)\" in the cashew on the top right, then choose \"download from internet\" at the bottom, then choose \"Weather Plasmoid\" and click OK.)\n\nThe good thing is that plasma nicely recovers from the crash and restarts itself (this was worse in earlier versions).\n\nIs this a known issue? If not, i'll file a new bug soon, including backtrace debugging information.\n\nI didn't try this with another plasmoid recently, so maybe it's an incompatibility of that specific plasmoid, so maybe i'll have to contact the plasmoid's maintainer?"
    author: "Planetzappa"
  - subject: "A suggestion on colour presets"
    date: 2008-07-18
    body: "Firstly, great work on the release guys, I can't wait to try it out!\n\nSecondly I had a idea I thought I might share, just there was a guy posting previously saying that there are a lot of dark themes/colour schemes.  \n\nWhat I was going to suggest is that KDE ship a set of schemes that by default includes all the primary (RBY) & secondary (GPO) colours and black to white too, with a light and dark version of every scheme. (For a total of ~16-24, correct me if i'm wrong, but as I understand colour schemes are just small bits of text/files so it's not gonna produce much bloat)\n\nCrucially though, it will mean that whatever colour wallpaper you choose there is almost certanly gonna be a colour scheme that looks sweet on it/goes with it, because we'll have covered practically every option, with no need to check on the web etc.  \n\nI just thought this might be a nice way to get a bit extra 'built in' user friendly functionality pretty easliy and without much effort.  It could be expanded to have 'neon' or 'pastel' etc etc classes too\n\nThoughts?\n\nJames C"
    author: "James C"
  - subject: "Re: A suggestion on colour presets"
    date: 2008-07-18
    body: "Sounds like a good idea.\n\nAt the very least you could make them and put them on KDE-look.org, they will be downloadable with the integrated download function from the configure colors screen. If they get high ratings and many downloads you might consider proposing them for inclusion to the artists."
    author: "jos poortvliet"
  - subject: "Re: A suggestion on colour presets"
    date: 2008-07-21
    body: "We could actually just be lazy and use the most popular ones from KDE-Look that fit into each class i.e. the most popular light-red, light-blue etc etc.  Why reinvent the wheel right?  ;-)\n\nJames C"
    author: "James C"
  - subject: "Re: A suggestion on colour presets"
    date: 2008-07-21
    body: "We could actually just be lazy and use the most popular ones from KDE-Look that fit into each class i.e. the most popular light-red, light-blue etc etc.  Why reinvent the wheel right?  ;-)\n\nJames C"
    author: "James C"
  - subject: "Re: A suggestion on colour presets"
    date: 2008-07-19
    body: "I have a spontaneous idea on how to extend this yet further: After the colors KCM has read the color schemes, it could take the most prominent colors to classify the schemes into classes like \"Light red\", \"Dark green\", or even \"Light red-violet mix\". This might be hard to implement, but it would be great to have."
    author: "Stefan Majewsky"
  - subject: "Re: A suggestion on colour presets"
    date: 2008-07-21
    body: "This sounds like a great idea though it might be a bit tricky to pull off.  I suppose if you had a \"foreground scheme\" & a \"background scheme\" or some such division it would work quite nicely"
    author: "James C"
  - subject: "Panel Autohide"
    date: 2008-07-18
    body: "Please implement this feature. I don't think that KDE 4.x\nwill be ready for use without it.\n\nPersonally I don't care about complex visual effects, etc (in fact, I would \nlike KDE to be as lightweighted as possible, while continuing to \nbe as fully functional as 3.5.x was). \n\nBut I think that panel-autohide is important for usability. \n\nAlso I would like to be able to hide the panel I would like to be able to temporally hide it when I don't need it, by clicking at the left/right end of the panel, like it used to be possible in kde 3.x \n \nI've reported this problem in bug #158556 on 2008-02-28 \nand this feature is not implemented yet.\n"
    author: "Pablo"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "What would you rather have? KDE 4.1 without panel hiding now, and 4.2 with panel hiding in January, or no KDE 4.1 now, and KDE 4.1 with panel hiding in January?\n\nIf the latter, download the source in January, search and replace the version numbers and build your very own 4.1 with panel hiding!"
    author: "Boudewijn Rempt"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "Dear Pablo,\n\nit is very welcome that everyone files his needs and wishes at bugs.kde.org, even encouraged, so no idea or problem gets lost. There is just a problem: Someone needs to go for it afterwards.\n\nAnd while many developers are spending their own time or that some employer pays them for, most are even only scratching their own itch or that of the employer, implementing things they need and like, and, most important, if they can pull the time from somewhere (like not going out with the boy-/girlfriend, not surfing the sea or not doing other fun/business).\n\nDo you understand this? Someone needs to work on this. Begging does not please anyone, neither the begger nor the begged one. The developers are no fairies who will and can operate every wish :) World would be a paradise if they were.\n\nAll you can do is: File your wish/bug, then wait, do it yourself or pay someone to do it. That's life. :)"
    author: "Hans"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "I see your point. In my case, I'm continue to use KDE 3.5.9\nright now. I'm happy with it, and I'm not planning to switch \nto 4.x for the moment. \n\nI've just built KDE 4.x from svn in order to help testing and \nreport bugs. I don't currently have the programming skills to\nimplement this feature myself.\n\nHowever, it is not just myself  complaining of a missing feature.\nDon't you see a lot of users complaning about missing features\nlike this? My point is that I think that the current direction\nof development of KDE 4.x seems not to be what many users \nexpect.\n\nI think that the desktop is there just to help users to get their\njob to be done with the least possible trouble. So it would be \nnice if the desktop just looks and feel the way they are accustom\nto see. It is not good to force end-users to do the same things \nin a different way from the one they've already learned\n(I don't speaking about my self here, I'm an experienced user\nand I love the command line interface, and advanced users \nlike myself can switch to any other desktop like Xfce,  Fluxbox or \nGnome if they wish). \n\nBut I obseve that the radical changes in KDE 4.0 might confuse end-users that were used to the KDE 3.5.x look and feel. So I would try to keep \nthis kind of simple usability features like \"panel auto-hidding\" \nthat used to make the desktop experience of end-users much \ncomfortable.\n\n"
    author: "Pablo"
  - subject: "Re: Panel Autohide"
    date: 2008-07-18
    body: "I don't understand how an auto-showing pannel is a usablity improvement.\n\nMaybe you can be very accurate when you move your mouse, but when I want to reach a component that is on the border I will probably go a little too far (or hit screen border if there are no panel there) so if a panel decide to show up there I have to move the cursor away, then move it again slowlier.\n\nBut now, I have wide screens, so I can happilly have a hundred pixels used by panels on the left and right of my screen (I prefer vertical panels)."
    author: "Renaud"
  - subject: "Re: Panel Autohide"
    date: 2008-07-19
    body: "An idea.\n\nWhat if you implement a \"Full Maximization\" switch in KWin or in Plasma? This means: let KWin disobey the screen limits imposed by Plasmoids and let it maximize windows over all the available screen width and height. Later, you can recall Plasmoids via the Dashboard with a key combo, and you'll have a setting that does the job, and is 100x more useful than panel autohiding.\n\nI hate with all my heart having to use panel autohiding in Windows Bosta\u0099 to save screen real estate. Whenever I play Warzone 2100 and I carry my mouse cursor to the right, Warzone freaks out, and I'm in Windows Bosta\u0099 desktop, with my newly enabled panel, coming out its autohiding. Kill that annoyance in KDE, please.\n\nPlease, kill \"panel autohiding\" for good, and implement it the way I'm suggesting. I will thank you a gazillion times if you do. \n\nP.S. \"Bosta\" is a South American word meaning, literally, \"a pile of bullshit\". It's meant to replace \"Vista\", and it is pronounced the same way. Use it! :-)"
    author: "Alejandro Nova"
  - subject: "Re: Panel Autohide"
    date: 2008-07-19
    body: "I think that the full-screen option in kwin (right-click, advanced, full-screen) already does what you want: maximize an application's window so it covers the whole screen. The window decorations disappear, too, so you really get maximum working area. I use it for konsole, for instance."
    author: "Boudewijn Rempt"
  - subject: "shutdown and restart still not working"
    date: 2008-07-18
    body: "When you unselect the 2 logout settings (confirm logout and offer shutdown options) should directly shutdown or restart without ask in the leave tab in kickoff, but still is not fixed... I need to logout and after find the shoutdown button and confirm one of the 2 actions... a lot of steps to only restart or disconnect the computer :-(\n\nin other hand, i love how is progressing KDE4, RC 1 is very stable and now I can see HD videos with my old computer that wasn't possible using KDE 3.5.x (could be that I'm using optimized packages of Kubuntu and the official KDE3 is not optimized) :-O"
    author: "skyxap"
  - subject: "is getting very good, but still has some bugs"
    date: 2008-07-19
    body: "when I get the kopete size of the panel changes\n\nare doing an optimal job, can hardly wait for the following versions"
    author: "Emanuell"
  - subject: "Looking good!"
    date: 2008-07-19
    body: "I've been using the 4.1 beta for a couple of weeks, and am very happy / impressed with it so far.  Still having some graphical glitches (bottom panels disappears a lot, which is pretty annoying) but overall I'm definitely liking it more than Gnome on my OpenSUSE 11 install.  After making a few font and other cosmetic changes, it's so pretty!  Now for Amarok to come out ..."
    author: "jackflash"
  - subject: "NVIDIA ..."
    date: 2008-07-19
    body: "Unfortunately KDE 4.1 is almost unusable with NVIDIA 8800 GPU with NVIDIA binary driver (nv driver is unusabe either). Everything works so slow - I just cannot bear it."
    author: "Artem S. Tashkinov"
  - subject: "Re: NVIDIA ... mail to them then"
    date: 2008-07-21
    body: "mail to them then, and beg them to fix this issue."
    author: "Koko"
  - subject: "Re: NVIDIA ... mail to them then"
    date: 2008-07-22
    body: "For some reason people don't seem to know that it runs perfectly fine if you disable desktop effects."
    author: "TonyM"
  - subject: "Re: NVIDIA ... mail to them then"
    date: 2008-07-22
    body: "Maybe because what you say is wrong ?\nI have an nVidia card (8400M GT) and I can assure you that it is still unusable when desktop effects are disabled...\n(2D painting performance are still slow)"
    author: "Unnamed"
  - subject: "Re: NVIDIA ... mail to them then"
    date: 2008-07-22
    body: "I second that.\n\nAnyway there's a thread on phoronix forums and NVidia own forum: http://www.nvnews.net/vbulletin/showthread.php?t=115916"
    author: "Artem S. Tashkinov"
  - subject: "Re: NVIDIA ... mail to them then"
    date: 2008-07-23
    body: "Huh, ok, I've seen some people report that disabling desktop effects fixes the issue."
    author: "TonyM"
  - subject: "Menubar on top"
    date: 2008-07-26
    body: "Will the menubar on top be available on 4.1?"
    author: "marce"
  - subject: "Re: Menubar on top"
    date: 2008-07-26
    body: "Unfortunately not, or at least, it won't be included in the official release - it seems to be rather harder to accomplish than people had expected.\n\nIt's conceivable that it will appear as a third-party plasmoid on kde-apps.org if someone can get it working, though."
    author: "Anon"
---
Today, <a href="http://www.kde.org/announcements/announce-4.1-rc1.php">we are passing the last milestone</a> on the way to KDE 4.1, a release that will be suitable for a larger audience than 4.0 has been. While it is not yet up to the features that people are used to from KDE 3.5, KDE 4.1 provides a significant amount of improvements over KDE 4.0, which some said was a bit of a bumpy ride. Sources and available packages are linked on the <a href="http://www.kde.org/info/4.0.98.php">release info page</a>.
KDE 4.1-rc1 is the only release candidate for KDE 4.1, which will be released on July 29th.





<!--break-->
<p>The development in trunk/ in Subversion has already been opened for feature development, which is going into KDE 4.2 (to be released in January), but developers are strongly encouraged to concentrate on bugfixing in the 4.1 branch for now. Do give RC1 a spin, file bugreports and fix things, there is only a week left until 4.1 is being tagged. Do have your changes in the 4.1 branch reviewed by your peers, though. Note that some users might still be suffering from performance problems with NVidia graphics chips. There is <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">a page on Techbase</a> that gives some more information about it. Make sure you report bugs via <a href="http://bugs.kde.org">KDE's Bugzilla</a> so they can be addressed and do not get lost.</p>







