---
title: "KDE Commit-Digest for 18th May 2008"
date:    2008-06-14
authors:
  - "dallen"
slug:    kde-commit-digest-18th-may-2008
comments:
  - subject: "Earth is just an atom, from a certain POV/model"
    date: 2008-06-14
    body: "[Initial steps toward a \"satellite layer\" plugin for Marble, with initial support for molecular editing in Kalzium.]\n\nHehe, so we can define electrons as satellites flying around the earth nucleus?\n\nAnyway: Danny, how do you do your incredible rocking? Thanks for it :)"
    author: "Hans"
  - subject: "Re: Earth is just an atom, from a certain POV/model"
    date: 2008-06-14
    body: "Through an intensive three week course.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Earth is just an atom, from a certain POV/model"
    date: 2008-06-18
    body: "My wife, a quantum chemist, just audibly groaned.  You're about 75 years late on the planetary model.  *Orbitals*, however, are pretty."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Earth is just an atom, from a certain POV/model"
    date: 2008-06-19
    body: "A cloud of satalites can be compared to a cloud of electrons..."
    author: "Level 1"
  - subject: "Re: Earth is just an atom, from a certain POV/model"
    date: 2008-06-24
    body: "No, electron's positions aren't as well defined as satellites at that scale."
    author: "Tim"
  - subject: "Networkmanager"
    date: 2008-06-14
    body: "The networkmanager applet was moved from kdereview to where?\n\nDerek"
    author: "Derek"
  - subject: "Re: Networkmanager"
    date: 2008-06-14
    body: "Back to playground.\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/networkmanager/"
    author: "cb"
  - subject: "Re: Networkmanager"
    date: 2008-06-14
    body: "About the NetworkManager, it is said that :\n\"When an interface with a higher priority comes back online the applet will disconnect from the current interface and attempt to connect to the higher priority interface\"\n\n--> I assume there is a notification to the user in order to confirm the reconnection (in case there is running downloads or something...) ?\n\n"
    author: "DanaKil"
  - subject: "Re: Networkmanager"
    date: 2008-06-15
    body: "That statement is actually incorrect now. NetworkManager 0.7 can handle multiple active interfaces and tries to keep all connections marked \"connect automatically\" up at the same time, and just changes the connection used for the default route.  This means if you are using the wifi and then plug in, your download and IRC sessions won't break because NM just switches the default route to your ethernet interface and keeps the wifi up.\n\nChanges like this will still be notified to the user."
    author: "Will Stephenson"
  - subject: "Re: Networkmanager"
    date: 2008-06-15
    body: "Does the applet require nm 0.7? Im on Kubuntu Hardy which ships with nm 0.6.6 and i build kde from SVN. Firt he first time recently the applet built by default but i don't see how to use it.\n\nBut the many configuration options make it look really interesting and i look forward to it. Thanks for the nice work!"
    author: "Kishore"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "In this development stage it does depend on NM 0.7, mainly because I'm using openSUSE 11.0 and Chris Blauvelt is on F9, but I'm sure we'll find someone to implement the configuration parts for NM 0.6.x."
    author: "Will Stephenson"
  - subject: "Re: Networkmanager"
    date: 2008-06-19
    body: "They enable and disable features based on which one you have on your system."
    author: "Level1"
  - subject: "Re: Networkmanager"
    date: 2008-06-15
    body: "That's really nice to know. NM disconnecting me the moment I plugged the ethernet cable in always bugged me, but being able to transition gracefully now should be quite a nice QoL improvement.\n\nThat leaves the only problem with NM for me is it takes quite a while to connect to anything (which might just be an issue with the lower level infrastructure which NM really has no control over)."
    author: "Kitsune"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "Why would the user need to be notified about this? They won't even know what the heck are you talking about, and it will constitute yet another useless annoying notification to ignore.\n\nPlease let's keep notifications to a real minimum. There is no need to notify the user of anything unless it's gone wrong."
    author: "NabLa"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "I disagree.\n\nWhen a user plugs in a wired cable I think he appreciates to see a little message that it from now on will use the wired network. \n\nFor instance, a lot of people are afraid to do their bank (money) stuff though wireless. So they will definitely appreciate the message that they are using wired from now on when they plug the cable in."
    author: "Leaves"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "As long as it is a passive notification, perhaps even something you can switch off, I see no problem with these kinds of notifications."
    author: "Andr\u00e9"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "What's a passive notification?"
    author: "NabLa"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "A passive notification would be, say, one of those passive popups that appear in the corner of the screen, don't steal keyboard focus, don't demand you press any buttons, and will automatically go away on its own (kinda like the in-app notifications amarok uses a lot of the time).\n\nI think this is a prime canidate for a passive popup as when you plug in a cable it generally takes a minute before it's fully activated, and if you've already have say a wireless interface active and you're switching over to wired you probably have a reason, either to save power (disable the wifi), security, or speed. If I'm plugging in the ethernet so that I can download the latest ubuntu/opensuse/fedora/etc much faster than over my congested and aging wifi (which is perfectly fine for casual browsing from across the room from the router). I'd wanna have some sort of notification that I can have KTorrent start so that I don't go through all that effort and then KTorrent is still using the crappy slow wifi.\n\nJust a little example scenario where having the notification would be good. If you think you'll never be in one of those situations you can generally disable the notification or change it to a sound or what have you (gotta love KDE's configurability!)."
    author: "Kitsune"
  - subject: "Re: Networkmanager"
    date: 2008-06-16
    body: "Fair point, I agree with you in this case (wifi -> wireless or the other way around) and as long as it isn't intrusive, for example showing up over a full screen movie, for example (something that Kopete does).\n\nI reaffirm on what I said about notifying about any other random activity though. Very disruptive."
    author: "NabLa"
  - subject: "Plasma panel configuration"
    date: 2008-06-14
    body: "When configuring the panel there are two big buttons on the top right of the config widget. The one is used to add something and the one right next to it is about removing something. I think it's a little confusing that adding and removing is not about the same thing. The left button lets you add widgets while the right one lets you remove the whole panel. When you just have a quick glance you could press the \"remove this panel\" option when actually what you wanted is to remove some widget from your panel.\n\nIf I imagine some newbie removing the panel by accident he/she will probably be quite confused. Do you have to confirm removing the panel? From a usability POV I would probably ask for a confirmation and also realign the buttons somehow.\n\nI'd also love an option to remove that cashew from my panel. I don't care about it so much on my desktop, but it's wasted space on my panel since I just configure it every other month or so."
    author: "Jens Uhlenbrock"
  - subject: "Re: Plasma panel configuration"
    date: 2008-06-14
    body: "\"Do you have to confirm removing the panel?\"\n\nYup - at least,  you do with the latest SVN :)\n"
    author: "SSJ"
  - subject: "Re: Plasma panel configuration"
    date: 2008-06-14
    body: "Choose lock widgets in the top-right cashew menu: your panel cashew will disappear."
    author: "Boudewijn Rempt"
  - subject: "Re: Plasma panel configuration"
    date: 2008-06-15
    body: "Lock Widgets is now also in the panel context menus and in the panel cashew."
    author: "Aaron Seigo"
  - subject: "Notify Plasmoid"
    date: 2008-06-14
    body: "this is a great improvement over the old notifications ! \n\nJust questions : wil it be possible to have different looks for the plasmoid's notifications ?. It's great if all applications can use the same system but maybe the user want to have a special style for all his IM popups, one for Amarok, an others for system notifications...\n\n\nMoreover, it would be cool to have an history of previous notifications (especially usefull for popups with a time-out). Any plan for something like this ?\n\nCheers and many thanks to all people involved in this :)"
    author: "DanaKil"
  - subject: "Re: Notify Plasmoid"
    date: 2008-06-14
    body: "goto bugs.kde.org"
    author: "Nick Shaforostoff"
  - subject: "Re: Notify Plasmoid"
    date: 2008-06-14
    body: "What you meant, of course, is how he should go and file these fine ideas on bugs.kde.org so they don't get lost, as he has some very valid points. Maybe you should point that out next time, just to ensure ppl don't think you meant this in a negative way ;-)"
    author: "jospoortvliet"
  - subject: "Plasma"
    date: 2008-06-16
    body: "Why does all the Plasma stuff look so ugly and disproportionate? Is there any consistent design as with Oxygen or it is all just ad hoc graphical hacking?\n\nI mean just take a look at the Raptor menu and the overlap of the two.\nhttp://commit-digest.org/issues/2008-05-18/files/raptor_mockup.png"
    author: "andre"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "I don't know. Why is your comment so annoying and negative? Maybe the two things have a shared root cause? ;-P\n\nSeriously though, go try 4.1. The art is a lot better than in 4.0 as the art team has started (though probably not 100%) to get caught up with things.\n\nNote that the Raptor menu isn't even into alpha at this point. The last thing we do is make the things pretty; the dev flow goes like:\n\n0. technical design\n1. framework\n2. features\n3. visuals\n\nwith continuous stabilization efforts along the way. With many things in Plasma we are on #2 and/or #3 at this point.\n\nIf you'd like to make #3 happen sooner or quicker, please feel free to join in somehow. Heck, hire an artist and set them up with a machine that has trunk/ compiled on it nightly for them. That'd be awesome."
    author: "Aaron Seigo"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "You are forgetting #-1. Ideas. I have seen a lot of mockups with cool ideas for KDE 4 and Plasma, before a single line of code was written or any kind of technical design was made. I have the idea that these are quite often confused with your point 3. :-)"
    author: "Andr\u00e9"
  - subject: "Re: Plasma"
    date: 2008-06-21
    body: "Make all the releases consistent  al made on the same libkde4\nand not release so ans so on this version of the libkde4 and then on that version.\n\n\nSo release kde4.0.5  consistent on 1 and only 1 version of libkde4\n"
    author: "robert"
  - subject: "Re: Plasma"
    date: 2008-06-22
    body: "i'm sorry, i have absolutely *no* idea what you're talking about?"
    author: "Aaron Seigo"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "I am not a KDE dev.\nBut everyone will have different opinions about looks.\n\nI like that look. If you dont like something, then make your feedback more constructive than \"look so ugly\". "
    author: "markus"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "You are right about that. Of course you need to be constructive and positive. \n\nBut of course the hype also needs reflection. It is obvious now that certain things went wrong. We had a giant hype but the result is embarrasing for most users and technology is not ready.\n\nI am not skilled in product design. But of course you feel when something is klunky.\n\nSo many discussions about Plasma and what was finally carried out looks improvised from a visual perspective. It is a bit like 3.1, shiny and new blinking but it just does not fit proportion wise.\n\nTo give you some ideas I attach the file again.\n\n"
    author: "andre"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "Raptor mock"
    author: "andre"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "<i>I am not skilled in product design.</i>\n\nNeither am I so I just donate to KDE e.V. instead. Do you?\nThere are many ways to contribute."
    author: "Oscar"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "Everyone of us has certain projects where he does not contribute or is not skilled. This is normal. Good design probably requires different skills than most of us can provide.\n\nThe sectarian positivity is contrasted by the fact that a former flag ship is losing ground. positivity did a lot of harm, it creates a bubble of expectations that burst sooner or later.\n\nWill KDE4 be desktop ready with KDE 4.2? I doubt so. "
    author: "andre"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "Actually, as OpenSUSE 11 shows, KDE4.0 is already desktop ready."
    author: "Boudewijn Rempt"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "define a metric for \"desktop ready\" and let's see."
    author: "Aaron Seigo"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "OK:\n\nMy personal metric is when I am able to do everything that I can do with KDE 3.  I am willing to do things in different ways as long as the results are the same.  A secondary measure is whether the configuration is as usable as KDE 3.\n\nCommercial and Government users will probably apply a higher standard.  They will not want to have to do things in a different way because that requires retraining and that costs money.  And there is another secondary measure: is it simple to setup a lot of desktops to be exactly the same."
    author: "JRT"
  - subject: "Re: Plasma"
    date: 2008-06-18
    body: "\"My personal metric is when I am able to do everything that I can do with KDE 3.\"\n\nDoes that include KDE 3.0? Besides, are you saying that anything that has even a bit less functionality than KDE3 is not \"desktop-ready\"? What if the new system is missing some functionality, but has some other functionality that is not present in the older system?\n\n\"Commercial and Government users will probably apply a higher standard. They will not want to have to do things in a different way because that requires retraining and that costs money.\"\n\nAh, the mythical \"companies don't like change\"-argument. Well, yues, they don't like change. But they will do it if it benefits them. Had they kept on using old systems, instead of moving to new system because of the included expense of training etc., they would still be running Windows 3.11. So obviously they DO move to new technologies. Somehow they managed to move between different versions of Windows and between different OS'es as well (like, from Solaris/IRIX to Linux).\n\nCompanies do move to new products and systems. They just plan those changes carefully in advance, but they do them nevertheless."
    author: "Janne"
  - subject: "Re: Plasma"
    date: 2008-06-19
    body: "For this reasons you hardly find ports of applications to KDE 4.\n\nThey wanted to change everything at once and get a KDE 4 that blows you away. But KDE does not follow the APPLE development model. And so many new products fail, think of Vista. We reached a level of complexitiy where design becomes very difficult in so many software projects.\n"
    author: "andre"
  - subject: "Re: Plasma gives you a magic pony"
    date: 2008-06-18
    body: "> Will KDE4 be desktop ready with KDE 4.2? I doubt so. \n\nAre you *serious*? Have you even used it? By 4.2 most of the rest of the apps should be ported, and we'll all have our magic ponys. \n\nI have to say that 4.1 is pretty damn good, though. There's a few things that didn't make it in by feature freeze, but all in all, I use it daily and don't have any problems. If you do, then please file a bug report or even better, a patch.\n\nAnd if you're concerned about usability, join the usability people. \n\n"
    author: "blauzahl"
  - subject: "Re: Plasma gives you a magic pony"
    date: 2008-06-19
    body: "KDE usability is great. Usability should not be a reason to remove functionality. The main problem is always \"reliability\". KDE 4 will be much more solid here than KDe 3.x once its maturated. Problem: Egg-Hen. For maturation you need not only a superiour software design but real world testing. No one will port software if no ones is using the platform and its still instable, you don't know where its heading.\n\nI was using the SVN versions, so I cannot complain about stability."
    author: "andre"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "right.. so you keep attaching the same file to make your point.\n\nexcept that the file you attach is a mockup for a code project that is happening outside of the main plasma project (which is fine) and which is in its absolute infancy.\n\nso you're taking some other work that is infant and comparing it to the core work that is maturing.\n\ndo you see the problem here? if not, maybe, as a friend likes to remind me from time to time, \"if you're looking for a conflict, you're sure to find one.\""
    author: "Aaron Seigo"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "\"Opinions are like arseholes.... Everyone has got one\" "
    author: "Jukka"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "And they can be full of sh*t !   :o)"
    author: "Ian"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "Try the different styles. There is quite a difference available.\n\nDerek"
    author: "Derek"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "I have to say the arrow drop shadows and other non-symmetrical layout does make it look ad-hoc, but then again it is not a general release look.\n\nI agree that if people can't handle brutal honesty towards GUI design then what the hell are you doing working in this arena?"
    author: "Marc Driftmeyer"
  - subject: "Re: Plasma"
    date: 2008-06-17
    body: "We don't exactly have lots of artists.  Are you honestly proposing that we should kick out any artists that don't like 'brutal honesty' ?\n\nPersonally I'd rather people were just a bit more polite and that we kick the users that are 'brutally honest'.  I'd rather lose users than developers/artists any day."
    author: "JohnFlux"
  - subject: "Re: Plasma"
    date: 2008-06-16
    body: "Obviously there is no arguing about matters of taste.\n\nPerhaps your comment would sound a bit better if you started it with 'I think' since some people don't seem to understand that you are simply expressing your opinion.\n\nSo, you think that Plasma stuff looks ugly and disproportionate.  Can you be a bit more specific?  Do you have specific suggestions to improve it?\n\nI would not say that it is ugly.  I think that Plasma stuff looks a bit plain and utilitarian.  To me these are not negative qualities.  OTOH, there is something about it that looks a bit awkward.  But, I don't know what I would change to fix that except that some things need to be better positioned and proportioned."
    author: "JRT"
  - subject: "Status of Kopete in KDE4"
    date: 2008-06-16
    body: "Hi\n\ndon't want to spread FUD but I'm a little.. anxious about the state of Kopete in KDE4x.\nIs it still maintained actively? \nI mean, lately using KDE SVN trunk I fille dbugs against plasma, doolphin, cervisia, kmail.. all resolved in a matter of days or at least assigned/checked.\nI've already filled at least two Kopete's bugs (related to the open jabber protocol, not a tricky closed one) and they are still in \"unassigned\" status, without a single comment. And they are two bugs that cause crashes/disconnections in Kopete, and I, as a user, have no means to debug them.\n\nSo, do I need to worry? Kopete is just crucial to KDE."
    author: "Vide"
  - subject: "Re: Status of Kopete in KDE4"
    date: 2008-06-16
    body: "\"I've already filled at least two Kopete's bugs and they are still in \"unassigned\" status, without a single comment.\"\n\nIn many cases this depends on the developers workflow, not all developers use the bug database the same way. Some use it more actively than others, so the unassigned status does not equal no activity. \n\nAnd Kopete is one of those programs with a hig rate of bugreports, making it difficult for the limited number of developers to keep the pace. And I guess by nature some of the bugs are hard to verify. \n\nLooking at the bug statisticks for the last week, Kopete does not look worse off than other applications(Bugsquad activity on Amarok and Konqueror aside).\nhttp://bugs.kde.org/weekly-bug-summary.cgi?tops=20&days=7\n\n\n\"So, do I need to worry?\"\n\nNo, don't think so. But the bug database for Kopete could need some cleaning up/triage, checking for duplicates and removing no longer valid bugs. I guess the developers would be thankfull for some help in this regards. "
    author: "Morty"
  - subject: "Re: Status of Kopete in KDE4"
    date: 2008-06-18
    body: "It's ported, which is a good start. ;)\n\nDid you include a backtrace? That instantly makes a bugreport more usable, and keeps someone from having to ask you for one:\n\nhttp://techbase.kde.org/Contribute/Bugsquad/How_to_create_useful_crash_reports\n\nPaste it directly into your report, to make it easy to look for duplicates.\n\nIts in the top 5 for open bugs, so I think BugSquad will be hitting it soon. The Amarok BugDay knocked Amarok down at least one, maybe more places... I expect a Kopete one can do the same. But you don't have to wait, if you want to help out now, grab a daily or an SVN copy and join #kde-bugs and say you want to learn how to triage. \n\nIf you can code, I'm sure they would love to have patches sent to whatever mailing list it is they use."
    author: "blauzahl"
  - subject: "Re: Status of Kopete in KDE4"
    date: 2008-06-18
    body: "There's no backtrace... it's a disconnection with a \"malformed packet\" and nothing more. As I commented in the bug, I would be delighted to help giving more info if asked, but simply I don't know how to provide more info in this particular case:\nhttp://bugs.kde.org/show_bug.cgi?id=163264\n(ok, I've just provided more info based on recent snapshots)"
    author: "Vide"
  - subject: "Re: Status of Kopete in KDE4"
    date: 2008-06-19
    body: "Technically that's not a crash... it may be as good as a crash for you since its so serious, but its not a crash because the program didn't halt therefore no back trace.  Crashes may seem serious but they are actually much easier to fix (usually) than other kinds of bugs (Called \"logic errors\") because of the back trace.  A network error like yours is much harder because it involves a computer we don't control (msn or aol server).\n\nThis lesson in computer science was brought to you by the Hamiltonian School of Computer Studies, Ameritech, the Center for Bug Triage, and viewers like you."
    author: "Level 1"
  - subject: "Re: Status of Kopete in KDE4"
    date: 2008-06-19
    body: "Yes I know it's not a crash strictly speaking, although with older revision it did *crash* all Kopete, indeed. Anyway I think that Kopete, if compiled in debug mode, should give more verbose output in case of these logical errors, because maybe the devs are not able to reproduce my error and I cannot send more debug info even if I will (it's in my interest, obviously, I want to send group messages)"
    author: "Vide"
  - subject: "Re: Status of Kopete in KDE4"
    date: 2008-06-23
    body: "(Not a kopete developer but anyway)\nTry getting a network capture with tcpdump or better, wireshark while the problem comes so the developers can see what's going on with the connection and all. That should provide all the info probably ... \n"
    author: "anon"
  - subject: "Question"
    date: 2008-06-16
    body: "Is it only me or does the plasma icon indeed looks similar to the Gnome logo? \n\nI can clearly identify the toe prints and the negative of a foot's sole. \n"
    author: "Sebastian"
  - subject: "Re: Question"
    date: 2008-06-16
    body: "To me, it is quite similar to the logitech logo (I have it in front of me right now). More curved, but recognizable."
    author: "RGB"
  - subject: "Re: Question"
    date: 2008-06-16
    body: "The plasma logo looks like Mercurial's logo [1], just with more colors and less reflections.\n\n[1] http://www.selenic.com/hg-logo/logo-droplets-200.png"
    author: "Stefan Majewsky"
  - subject: "Re: Question"
    date: 2008-06-16
    body: "i think it looks like tom green.\n\nwait ......"
    author: "Aaron Seigo"
  - subject: "Re: Question"
    date: 2008-06-19
    body: "When I met you in person, I thought you didn't look as much like tom green as you did in the photo... I think you were shaved then... it was at the kde 4 release event in mountian view."
    author: "Level 1"
  - subject: "Re: Question"
    date: 2008-06-17
    body: "Yes, we took a set of logos from different project and tried to make one that reflects all the different styles of those projects, but merge it into one.\n\nWe also thought it'd be a good idea to get the message \"desktop to go nuts like shit with\" across the line, so we combined it with a cashew and a turd. The obvious downside is that the Plasma logo is now not 100% like the GNOME logo, but goes only 85% that way (same for cashew and turd, respectively). We hope to be able to theme this piece of artwork so that you can have a donut, a sheep or whatever you like.\n\nThe logo replaces files on the desktop and configurability to the bone in KDE4. Hope you like it."
    author: "sebas"
  - subject: "Re: Question"
    date: 2008-06-19
    body: "heheheh, are you serieus ?"
    author: "mimoune djouallah"
  - subject: "Nepomuk"
    date: 2008-06-17
    body: "...is awesome. It is fantastic to see the creator of the very successful k3b go on to produce yet another winner.\n\nRegarding nepomuk, I'm wondering is it possible to use a shorter kio name in the address bar instead of (or in addition to) \"nepomuksearch:/\" ? \ne.g. \"cat:/\" or \"kat:/\" (in honor of an earlier effort) or \"tag:/\" or \"find:/\"\n\nAnd would you say that Nepomuk achieves similar goals to the now-defunct WinFS and GNOME Storage? I'm trying to explain how it works to an interested friend.\n\nThanks.\n"
    author: "Parminder Ramesh"
  - subject: "Re: Nepomuk"
    date: 2008-06-17
    body: "The tagging system is the first visible to the user part of NEPOMUK. The project really goes much, much further than that though. So right now, you're able to find data by their tags. In the future, we'll also dynamically assign tags (for example tag a file as \"attached to email by personX\", so we would be able to find it in the context of that person as well.\n\nThat's all about the semantic desktop, all about getting the computer to understand how human beings construct, deconstruct, find and store information. In the future I hope to be able to find by the following queries:\n\n- That file I received last week, somewhen in the evening\n- Photos of animals (and finding my cat)\n- Finding email relevant to some trip from people that were with me\n- File's I'm coauthoring with personY\n- Relevant bookmarks that go with some IM chat\n- Emails tagged with some topic\n- Recently used in Plasma activity Z\n- ...\n\nThe key is to make the computer know about the actual content of data and its relationships. Right now, computers know about png being an image and are able to display it. They do not have the concept of content of the image for example, and they certainly couldn't say \"This is a cat, humans keep them as pets and cats can be on photos\" -> \"Here's a photo of sebas' pet cat\".\n\nBut the tagging is already quite nice, and seeing how it integrates with the rest of the desktop is awesome. :)"
    author: "sebas"
  - subject: "Re: Nepomuk"
    date: 2008-06-18
    body: "Thanks for your reply. This whole system sounds quite futuristic :)"
    author: "Parminder Ramesh"
  - subject: "Re: Nepomuk"
    date: 2008-06-19
    body: "It's a wonderful technology yes but it's far from ready. I activated it on the present KDE 4.1 Beta 1 and it almost brought my system to a halt. Programme start up, moving around in Dolphin, even scrolling in the Browser was as slow as a snail. Nepomuk was consuming up to 70% of my CPU!"
    author: "Bobby"
  - subject: "Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-17
    body: "I'm so happy. Firefox 3 is finally out.\n\nParticipate in the download day and set a Guiness World Record of most Firefox 3 downloads in 24 Hours!!!\n\n\n_________________\nNow a side question:\n\nSince there is a Windows Vista-, a Mac-, and an Ubuntu theme, will there be a KDE 4 theme? Something that will make Firefox look and feel native in a KDE 4 environment?\n\n-Max"
    author: "Max"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-17
    body: "This might be of interest for you:\nhttp://blog.vlad1.com/2008/05/06/well-isnt-that-qt/"
    author: "Yves"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-18
    body: "firefox 3 has always looked native to me in OpenSUSE, which I must say, OpenSUSE 11 is amazing"
    author: "anon"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-18
    body: "This one seems to be in an early stage, but looks quite promising:\nhttps://addons.mozilla.org/en-US/firefox/addon/7574\n\nIn the end, the QT-port would be the best solution, let's see how it evolves.\n\nAnd there are projects to build cross-platform QT/webkit browsers as well, but they are all pre-alpha.\n\nLast, but not least it's still possible that Konqueror will catch up, one day. I haven't tried the 4.1 betas so far - are there noticeable improvements in Konqueror as a webbrowser?"
    author: "Sepp"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-19
    body: "Undo Close Tab and session saving should both, I think, be available in 4.1."
    author: "yman"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-19
    body: "It would really be nice if that theme would reach maturity soon. The Tango theme doesn't look that nice on KDE 4 but I guess it does on GNOME.\nI try Konqueror now and then but it's not as good as the Fox - at least not yet. Firefox is very versatile, fast and flexible and it's add-on capability makes it so much more powerful than all the other browsers out there.\nIf Konqueror had all the features that Firefox has as a web browser then I would switch right now ;)"
    author: "Bobby"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-20
    body: "To me it is not about the features, but the stability. I still did not find any need for firefox addons. And I regularly use Konqueror on daily basis. But still: Suddenly the flash plugin does not work in konqui or javascript (including konqueror) hangs due to some scripting errors. Then I have to use Firefox. I am pretty sure that when Firefox could somehow better integrate with KDE it will become my main choice... Of course I see that progress in KJS and KHTML are made, but ... :)"
    author: "Sebastian"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-19
    body: "Are there ubuntu packages?"
    author: "andre"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-20
    body: "Try the default FF3 theme with Gtk-Qt-Engine. There are versions for Qt3 and Qt4. They both even use the KDE icons.\n"
    author: "Riddle"
  - subject: "Re: Firefox 3 is finally out!!!!! :D :) :D"
    date: 2008-06-21
    body: "Thanks for the tip, it even looks like a native app now :)"
    author: "Bobby"
  - subject: "18th may?!"
    date: 2008-06-20
    body: "Am I the only who noticed that the last few commit-digests are ostensibly for \"18th May\"? Just thought I'd give a heads up :)"
    author: "rahux"
  - subject: "KDE4 with OpenGL on Intel 945GM hardware"
    date: 2008-06-21
    body: "sorry for being off-topic. I just found out how to get KDE4's Windowmanager running with decent speed in OpenGL mode on my slow Intel 945GM hardware. Thought I should share this info, but found no place where to put it.... no wiki, no nothing ;-(. It seems a lot of users are struggling to get all the nice desktop effects running due to difficulties how to set up OpenGL rendering in a way it works with KDE4....\n\nAnyway.. if you own a laptop with intel graphics (e.g. 950) try this in your xorg.conf:\nSection \"Device\"\n        Driver      \"intel\"\n        Option \"CacheLines\" \"32768\"\n        Option \"DRI\" \"true\"\n        Option \"AccelMethod\" \"exa\"\n        Option \"MigrationHeuristic\" \"greedy\"\nEndSection\n\n"
    author: "Thomas"
  - subject: "more VideoRam helps too"
    date: 2008-06-21
    body: "Section \"Device\"\n[...]\nVideoRam    65536\nEndSection\n\nbtw... just testing KDE4.1 svn.. dear I was so sceptical, but ... OMG I'm speechless.. this is massive... guys, you're amazing. It's beautiful. It's fast, it's slick... everything else feels so oldfashioned now...\n\nAnybody noticed Operas default theme fits perfectly with KDE4.1 ? ;-)\n\nme getting back to alt+tabbing with cover switch effect... drool..."
    author: "Thomas"
  - subject: "Re: more VideoRam helps too"
    date: 2008-06-21
    body: "And less than 6 months since the release of 4.0.0! At this rate, it won't be long until there is nothing significant missing from KDE4 that was missing in KDE3, and after that - well, with all the new stuff in KDE4 libs and Qt4.4+, the sky is the limit.  Seriously."
    author: "Anon"
  - subject: "Re: more VideoRam helps too"
    date: 2008-06-21
    body: "thanks really for sharing this info, as i am a sad 945GM user, i tried all thing to make composition works but without much success. \ntomas, a stupid question, when you activate composition, did 3d games still works ?\n"
    author: "mimoune djouallah"
  - subject: "Re: more VideoRam helps too"
    date: 2008-06-21
    body: "You'll probably want to disable desktop effects before you play games, since graphics performance can be slower with them on."
    author: "Jonathan Thomas"
  - subject: "OpenSUSE"
    date: 2008-06-22
    body: "I'm sad to see that KDE Digest has not run a story yet about the release of OpenSUSE 11.  I can't help but wonder why?"
    author: "Anon"
  - subject: "Re: OpenSUSE"
    date: 2008-06-22
    body: "Why, I can only imagine that their legions of paid editorial staff are all at the Bahamas!\n\nWhy do you think?"
    author: "Anon"
  - subject: "Re: OpenSUSE"
    date: 2008-06-22
    body: "zOMG! It's a conspiracy!11!"
    author: "Jonathan Thomas"
  - subject: "Re: OpenSUSE"
    date: 2008-06-23
    body: "much quicker in stories about Fedora and Ubuntu.  I would expect considering OpenSUSE is one of the major contributers to KDE, that there would of been a more timely released article\n\n"
    author: "anon"
---
In <a href="http://commit-digest.org/issues/2008-05-18/">this week's KDE Commit-Digest</a>: Improved drag-and-drop of applets, and enhanced usability using the "Panel Controller" in <a href="http://plasma.kde.org/">Plasma</a>. Grouping of notifications in the "Notify" Plasmoid, and continued progress in the "NetworkManager" applet. Animations in the "Pager" applet. <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> integration into Plasma is revived. More work on theming in <a href="http://amarok.kde.org/">Amarok</a> 2.0, with the "Current Track" and "Wikipedia" applets re-enabled. A return to work on the <a href="http://raptor-menu.org/">Raptor</a> menu. Initial steps toward a "satellite layer" plugin for <a href="http://edu.kde.org/marble/">Marble</a>, with initial support for molecular editing in Kalzium</a>. Copy-and-paste of vocabulary entries in <a href="http://edu.kde.org/parley/">Parley</a>. "Singmaster" moves functionality in Kubrick. Support for searching the database by GPS position, and "fuzzy searches" (using a user-drawn sketch) based on the Haar algorithm (from <a href="http://www.imgseek.net/">imgSeek</a>) added to <a href="http://www.digikam.org/">Digikam</a>. A "start page" is added to <a href="http://gwenview.sourceforge.net/">Gwenview</a>. More functionality added to Beagle KIOSlave. A "quick reply" function is added to <a href="http://www.mailody.net/">Mailody</a>. <a href="http://www.kontact.org/">Kontact</a> gets a plugin for KJots. An import dialog added to assist in migrating from the KDE3 to the KDE4 version of <a href="http://ktorrent.org/">KTorrent</a>. Full support for the Windows platform in KTorrent trunk. Optimisations in the next-generation tile system of <a href="http://www.koffice.org/krita/">Krita</a>. Work on loading ODF presentation notes in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. KNewStuff2 moves to Goya for handling and displaying items. Support for AIFF and RIFF audio file formats in <a href="http://developer.kde.org/~wheeler/taglib.html">TagLib</a>. Initial import of Nonogram into playground/games. libkscan replaces libksane in kdegraphics. kdelirc moves from kdeutils to playground/utils. <a href="http://phonon.kde.org/">Phonon</a> moves from kdelibs to kdesupport, "the never-freezing new home of Phonon". <a href="http://commit-digest.org/issues/2008-05-18/">Read the rest of the Digest here</a>.

<!--break-->
