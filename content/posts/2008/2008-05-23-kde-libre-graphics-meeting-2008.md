---
title: "KDE at the Libre Graphics Meeting 2008"
date:    2008-05-23
authors:
  - "brempt"
slug:    kde-libre-graphics-meeting-2008
comments:
  - subject: "one benefit"
    date: 2008-05-23
    body: "I think a huge benefit is the face-to-face meetings between the members of various projects.   I look forward to the collaboration that comes out of these meetings."
    author: "dave null"
  - subject: "Intresting applications"
    date: 2008-05-24
    body: "There seemed to be very intresting applications, my favorites were Inkscape, Hugin and Digikam. KDE should start support more image archive applications so normal people can find their images later, what purpose Digikam seem to be great! And many avarage user wants to make own book or other kind pdf files, so scribus should be got to be more kde application than pure Qt!"
    author: "Thomas.S"
  - subject: "not bad"
    date: 2008-05-25
    body: "I view this as a good event to get feedback on IDEAS early.\nNot only from users, but about \"smart\" people (other devs) face to face as well."
    author: "markus"
---
Two weeks ago, the third edition of the <a href="http://www.libregraphicsmeeting.org/2008/index.php?lang=en">Libre Graphics Meeting</a> was held in at the Wroclaw University of Technology, Wroclaw, Poland. Sponsored by KDE e.V., Boudewijn Rempt, Cyrille Berger and Emanuele Tamponi from the Krita project and Gilles Caullier from the Digikam project attended this yearly conference on free graphics software.











<!--break-->
<div style="border: thin solid grey; float: right; padding: 1ex; margin: 1ex; width: 250px">
<a href="http://static.kdenews.org/jr/libre-graphics-meeting-2008-group-photo.jpg"><img src="http://static.kdenews.org/jr/libre-graphics-meeting-2008-group-photo-wee.jpg" width="250" height="167" /></a><br />
      Libre Graphics Meeting 2008 Group Photo (photo by Alexandre Prokoudine)
      </div>

      <p>The Libre Graphics Meeting brings together developers of free graphics software in the widest sense of the word as well as the users: artists, photographers, designers and publishers. Whether you are interested in fonts, photography, panoramas, digital art, mathematics, colour theory, vector libraries, applications, file formats, interoperability, user interaction, typesetting, text layouting, 3D modeling or animation, or all of those, the Libre Graphics Meeting is <em>the</em> meeting to attend. <a href="http://www.libregraphicsmeeting.org/2008/index.php?lang=en&amp;action=program">This year's program</a> had a particularly good mix of introductory and in-depth presentations. No topic was left unexplored, and the great thing is: everyone was talking to everyone and came home with new ideas, new initiatives for cross-project cooperation and integration.</p>

      <p>The LGM is one conference that proves that free software is capable of innovation: one of the highlights certainly was Emanuele Tamponi's presentation on colour mixing as demonstrated in Krita. While unfortunately not taped, his presentation of the new alpha-sigma colour space for realistic mixing of colours grabbed the interest of people from many other projects. Likewise, Pablo d'Angelo demonstrated the <em>enfuse</em> feature in his panorama stitching application <a href="http://hugin.sf.net">Hugin</a>: the ability to adjust the lighting of a panorama while blending, which is another innovation proprietary applications are only now copying. <a>Raph Levien's</a> <a href="http://libspiro.sourceforge.net/">Spiro curve mathematics</a> was demonstrated in <a href="http://fontforge.sourceforge.net/">Fontforge</a> and <a href="http://www.inkscape.org">Inkscape</a>: drawing beautiful curves the easy way. During the <a href="http://freedesktop.org/wiki/OpenIcc">OpenICC</a> BOF session, Cyrille Berger presented his work on <a href="http://openctl.org">OpenCTL</a>, the free software implementation of the CTL pixel manipulation language. OpenCTL is important for making it possible to paint on images that use HDR color models.</p>

      <p>Boudewijn Rempt presented an overview of natural media simulation: the academic field, the proprietary offerings and the free software efforts. Natural media simulation, going beyond programmable brushes and effects, could bring serendipity back into digital art.</p>

      <p>With Gilles Caullier presenting Digikam for the first time at LGM, the scene was set for a meeting of many projects involved in the digital photography world: <a href="http://www.cybercom.net/~dcoffin/dcraw/">DCRaw</a>'s Dave Coffin was present, as was <a href="http://ufraw.sourceforge.net/">UFRaw's</a> Udi Fuchs, and <a href="http://rawstudio.org/">Raw Studio's</a> Anders, Anders and Anders and relative newcomer <a href="http://photobatch.stani.be/">Phatch</a>.</p>

      <p>Simultaneously with the Text Layout Summit for which <a href="http://freedesktop.org/wiki/Software/HarfBuzz">Harfbuzz</a> developer Behdad Esfahbod was present, the <a href="http://www.scribus.net">Scribus</a> team presented the latest developments. Last year's LGM organizer Louis Desjardins from Montreal took us through the entire process of creating a document to output as a PDF. Here again, free software is gaining features beyond what proprietary applications offer, such as precise support for language rules.</p>

      <a href="http://www.river-valley.tv/conferences/lgm2008/">Most talks were taped</a> by Kaveh Bazargan, whose successful Kerala-based publishing business runs entirely on free software. Still, his use of a Mac and OS X for the recording served to show the need for good quality free video recording and editing applications.</p>

      <p>Next year's edition might well be in Singapore, but no matter where LGM will be, it is the most important event for everyone involved in free graphics software. Everyone working on a graphics application or library, from Qt to Kolourpaint, from Krita to KPhotoAlbum, from Digikam to Karbon, from Quasar to Okular, should consider attending!</p>







