---
title: "KDE Commit-Digest for 9th March 2008"
date:    2008-03-17
authors:
  - "dallen"
slug:    kde-commit-digest-9th-march-2008
comments:
  - subject: "Thx Danny - KOffice?"
    date: 2008-03-17
    body: "Excuse, but, I had curiosity on:\n\nKWord for 2.0? yes o no\nODF lib?\n\nI'm trying to compile koffice, but, still I am a novice...:)\n\nGreetings of Per\u00fa :).\n\n\n"
    author: "Fanito"
  - subject: "Re: Thx Danny - KOffice?"
    date: 2008-03-17
    body: "> KWord for 2.0? yes o no\nWe don't know yet, if it's ready yes, if not no.\n> ODF lib?\nWhat do you want to know about it ?\n"
    author: "Cyrille Berger"
  - subject: "Re: Thx Danny - KOffice?"
    date: 2008-03-17
    body: "Don't hesitate to ask for help on the koffice-devel mailing list or #koffice on freenode (although you'll need to pick your time, since most developers are in the CET timezone) -- getting KOffice to compile and run against KDE4 is still harder than it should be."
    author: "Boudewijn Rempt"
  - subject: "Re: Thx Danny - KOffice?"
    date: 2008-03-17
    body: "As various people have observered; releasing an office suite without a word processor is not something that is smart.  You can imagine me (as someone who put a lot of time in KWord in both KOffice svn and Qt) being a bit disappointed too.\nI'm trying hard to make sure its ready, in various ways, but at the same time it would be bad to delay all the other applications just because the word processor is in too bad a state. So, yes, it might get dropped in the 2.0 release.\n\nAs for the ODF lib;  putting time and effort into making the koffice libraries (which include the ODF one) ready and good is not a grand idea at this time. It would just delay other things.\nFor that reason KOffice has always said it will not provide binary compatibility for the 2.0 release.  The ODF library will directly be affected by this and will only be cleaned up and made *nice* after the 2.0 release.\nIt will happen, though.  But right now getting KOffice 2.0.0 out is more important."
    author: "Thomas Zander"
  - subject: "Re: Thx Danny - KOffice?"
    date: 2008-03-18
    body: "I don't mean to sound like a dissenter, but without good ODF support and without a word processor, do you really have a KOffice release?\n\nKDE 4 ended up bumping back a few months, and then people were still insisting perhaps it was released a little early.  An incomplete release will only feed the trolls and incite negativity.  A late release will provoke less of that.\n\nIn the end, most users aren't going to use it until it is usable, regardless of what you call the release.  Until you have solid ODF support and KWord, does it matter if that working release is called 2.0 or 2.1?\n\nMy suggestion (for what little it is worth) is to release more alpha/beta builds until you can release properly."
    author: "T. J. Brumfield"
  - subject: "Re: Thx Danny - KOffice?"
    date: 2008-03-19
    body: "There are reasons to do a release. And those are rather similar to the ones we had for releasing KDE 4.0 in januari. For one, some apps ARE kind'a ready, and not releasing them hurts them (they get less feedback, new developers and it's not motivating to have no release for a long time). Besides, if for example KPresenter is good and solid, I would love a release so I can get rid of the OO presentation app ;-)\nSame goes for eg Kexi etc."
    author: "jospoortvliet"
  - subject: "Re a new name for KGLLib"
    date: 2008-03-17
    body: "OpenKL?\nKGL?"
    author: "Glorgg"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "No, no, \"KuGLer\" is a good name and fits right into our re-imagining of the brand as the \"Kugler Desktop Environment.\"\n\n[/salt]"
    author: "Adriaan de Groot"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "And of course all that is part of the master plan to sue KDE as soon as it's famous, rich and dominates the world. :-)"
    author: "sebas"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "Kogl? "
    author: "Ian"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "I like KGL, thought about the same idea.\n\nIt depends however how you want to market it: as an OpenGL wrapper (use KGL then), or scene building/compositing framework (camera, textures, models) which happens to use OpenGL as it's backend. In that case I'd use a completely different name that refers to \"world\", \"environment\", \"3d\", \"universe\" or \"scene\"."
    author: "Diederik van der Boor"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "KoolGlance? Kool Graphics Library? \n\nI like the fact that it is LGPL and not (yet?) based on kdelibs."
    author: "Sebastian"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "I wonder if that's gonna make a diff in the uptake outside KDE - it will be perceived as KDE tech, after all. And if KDE grows too big, Sebas might execute his evil plans, and all KDE users will be sued. I'm sure the same thing is keeping strigi and it's infrastructure from being used outside of KDE. Or aRts. I'd say Sebas seems more scary than MS even, considering the uptake of Mono ;-)"
    author: "jospoortvliet"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-18
    body: "This is indeed a problem. To me it seemed that arts and other libraries did not involved their own developer communities since they are K specific. I wonder what would happen, if KGL would be named withut the \"K\" logic and if it would be advertised as a toolkit independent GL wrapper whereby the current maintainers guarantee the Qt/KDE bindings... The same as GStreamer probably, which is widely used nowadays?\n\n"
    author: "Sebastian"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-19
    body: "Unfortunately this is the case for Strigi - yet I haven't seen any interest outside of KDE. But that might be a promotion thing..."
    author: "jospoortvliet"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "OpenInventor\nOpenSceneGraph\nOpenSG\nCoin3D\n..."
    author: "Birdy"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "How about a state of matter or a physics term to keep up with the Phonon, Solid and Plasma names. \n\nOr if it doesn't depend on KDE it should probably use a Q somewhere instead of a K."
    author: "Ian Monroe"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "How about gassy?"
    author: "Christopher Blauvelt"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-17
    body: "How about 'sphere' or the german word for sphere 'Kugel' maybe written as 'KuGeL'?"
    author: "Volker"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-18
    body: "Kugel++"
    author: "Pobrecito hablador"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-18
    body: "Gravity? GLavity?\nFluid?\nVapor?\nCombustion?\nHydrogen?\nNuclear?\nLiquid?\nGas?\nGlass?\n\n(any of the above)3D?\nKDE3D (K3D has already been taken)?\nCute3D (a word play on Qt pronunciation)?\n"
    author: "Riddle"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-18
    body: "I like Cute3D. :)"
    author: "Ian Monroe"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-19
    body: "SpaceTime ;-)"
    author: "me"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-19
    body: "\"Vantage\""
    author: "Anon"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-19
    body: "Glance !"
    author: "shamaz"
  - subject: "Re: Re a new name for KGLLib"
    date: 2008-03-22
    body: "GLove or Glove"
    author: "Andy Goossens"
  - subject: "Still using butt ugly fonts"
    date: 2008-03-17
    body: "I've been following the progress of KDE 4 for a while now. Firstly, congratulations to all the devs/other peeps for all their hard work.\n\nHowever, EVERY screenshot I've seen has had THE most ugly fonts and font rendering.\n\nI know that's not the KDE devs fault directly, but surely there must be a half decent looking font somewhere. \n\nThe default for most screenshots is WAAAAAY too big, not proportioned correctly, and DEFINTELY not rendered correctly.\n\nPlease note this is NOT a troll or anything. I genuinely would like KDE to succeed, but for me at the moment (indeed ever since I started using linux) the fonts issue is still a long way from being solved."
    author: "Gogs"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "Yes. I have the same problem. The default fonts are not antialiased. I have so switch to bitstream Vera sans to get a nice looking interface. I\u00b4m using a self compiled version from yesterdays svn.\nI don\u00b4t understand why this is not default. We have 2008 I think it is time for more modern default settings.\nAnd I don\u00b4t think this is the job of the distributions. KDE should ship a nicely preconfigured version without the need to fine tune everything by hand. "
    author: "Hans"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "As far as I understand it, KDE uses whatever fontconfig proposes as default sans serif font. If you don't like it, you should change it. It's not something KDE can do anything about, but your system integrator can. For most users (including me), the default is actually fine (DejaVu Sans on this system, antialiased and all)."
    author: "sebas"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "The first thing that I always do after installing a Linux distro is change the default font. I also use Bitstream Vera Sans which I think is one of the very best fonts for the Linux DE.\nI don't know why it isn't KDE's default font. Could it be a licensing problem? I notice that openSuse doesn't even install it as one of it's default fonts, one has to install it extra.\nFor people like me it's not a big issue but if KDE wants to appeal to normal mortals in a way that OS X does then these little things have to be taken care of."
    author: "Bobby"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "Check if DejaVu is installed. DejaVu is the fork of Vera, and thus looks exactly the same for Latin, but has much wider script support, and has people working on it. Vera never got changed or fixed after it was first released."
    author: "Ben"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "I'm pretty sure DejaVu is the default on OpenSuSE (though its been quite a while since I've done a fresh install, so I might just be getting confused)."
    author: "Sutoka"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "I think it's Sans Serif or something like that but it's definitely not DejaVu."
    author: "Bobby"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-18
    body: "Whatever. The fonts on openSUSE's KDE always look good for me."
    author: "Stefan Majewsky"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-19
    body: "Who cares? Not trying to pick on you, but the point is that screenshots should look VISUALLY APPEALING to people.\n\nChoosing ugly fonts is not a smart way to do, KDE could have an easy link inside widgets to change default fonts easily and quickly without kcontrol-navigating or similar"
    author: "she"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "Thanks for the info. I really didn't know that. I just tried it and indeed it looks the same :)"
    author: "Bobby"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "> Please note this is NOT a troll or anything. I genuinely would like KDE to succeed, but for me at the moment (indeed ever since I started using linux) the fonts issue is still a long way from being solved.\n\nThe Dot is quite a poor choice for a medium for 'technical feedback that is not trolling' (not that trolling has its place there ...). The best thing you can probably do with your feedback is taking it to a mailinglist (such as kde-devel) and find out why it is that way, and what you can do to change it.\n\nBTW, font rendering on my systems, and on screenshots I see is just fine, I cannot see the issues you're complaining about, so it's at least very subjective.\n\nFor this commit-digest it's way offtopic (it's not about some recent change, is it?). Another reason to choose a mailinglist ..."
    author: "sebas"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "I would disagree. This thread is about progress being made in KDE aka Commit Digest. I would say that poor fonts don't represent progress, therefore it would be legitimate to point that out on this thread."
    author: "Gogs"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "But you can't seriously expect all the KDE developers to follow all the threads on all the Dot articles, especially given the signal:noise ratio. Of course, you're free to point it out here but it's unlikely that anything productive will come of it. The mailing list is the correct place to report issues such as this and Bugzila is the place for most other wishlist/bug reports."
    author: "Matt Williams"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "No I certainly do not, but having being reading the Commit Digest announcements for a while they do certainly appear to attract their fair share of devs..."
    author: "Gogs"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "That's true, I have seen even big shots like Aseigo addressing problems on the dot quite a few times even though they might have been posted on Bugzilla or the mailing list. \nI have also posted a few problems/bugs (haven't got a Bugzilla account yet) here which have been been dealt with, so yes they (the devs) are on the alert even on the dot ;)"
    author: "Bobby"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "That doesn't make it a more suitable medium for doing so. In fact, posting to the right mailinglist helps the developers keeping track of it (so chances increase it gets addressed), increases chances that the right developer even reads it, and it helps developers a great deal by respecting their workflow.\n\nSo while sometimes things get picked up from the Dot, it's by no means a preferred, nor suitable, medium to report bugs."
    author: "sebas"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-17
    body: "I am aware of that sebas and that's why I am going to get registered on Bugzilla today. Of course I don't have a bug right now but I am sure that one will come ;)"
    author: "Bobby"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-22
    body: "> I would disagree. This thread is about progress being made in KDE aka Commit Digest. I would say that poor fonts don't represent progress, therefore it would be legitimate to point that out on this thread.\n\nYou can't hold the devs accountable for something that is the responsibility of the distros.  You may as well complain that KDE doesn't speed up package management.  KDE does what it rightly should, and relies on the defaults set in the environment, unless the packagers have altered those defaults.  If those don't work well, your criticism is misdirected."
    author: "elsewhere"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-18
    body: "just wanted to add that i am also not happy with the kde fonts choice!"
    author: "same"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-20
    body: "This screenshot here\nhttp://vizzzion.org/images/blog/pink.png\ndemonstrates that KDE doesn't always need to have ugly fonts. I think these fonts are really nice."
    author: "Thomas Walther"
  - subject: "Re: Still using butt ugly fonts"
    date: 2008-03-20
    body: "I would say that the rendering of this font is better, but the font itself is still pretty ugly.\n\nIt's not in proportion, and by that I mean its width is greater than its height.\nAs someone pointed out it's all subjective of course, but I have yet to see any fonts in KDE which look nicer than those in Windows.\n\nEven when what IMVHO I would call a nice font is chosen, by default it is too large and/or the rendering/kerning sucks.\n\nIn that screenshot look also at how far the tops of the letters l,k and f extend above the height of any letters which are capitalised. \n\nAlso, please note that the spacing is uneven between the letters on the taskbar. For example the 'K' is touching the 'o' where is says EMail - Kontact, but yet there is a big gap between the 'o' and the 'n'. Reading further along, the spacing is inconsistent.\n\nNo I'm sorry (but again, it's only my humble opinion) font rendering in Linux/KDE still has a LONG way to go  before it's as good as it is in Windows. "
    author: "Gogs"
  - subject: "just two features !  "
    date: 2008-03-17
    body: "i have installed kde 4.0.66 from opensuse build service (thanks opensuse u are doing an amazing job) just two small complain about plasma.\n\n- do you think icons on the desktop need those applet handler thing( rezise and rotate), and the black rectangle behind them is too ugly.\n\n- the panel rock, just lacking the ability to move applet.\n\ni have been running kde 4 from the first alpha release, and i must say the last snapshot is absolutely stable and ready for daily use.\n\nthanks for your work.  "
    author: "djouallah mimoune"
  - subject: "Re: just two features !  "
    date: 2008-03-17
    body: "Cool, thanks for having a look at the *current* version, not the ancient Plasma from 2 months ago :-)\n\nSome points you raise:\n\n- The desktop icons need the black background unless someone implements a nicer way to get enough contrast for the text\n- Moving applets inside panels is under way\n- The applet handle is being worked on and polished, that'll be in 4.1 most likely.\n"
    author: "sebas"
  - subject: "Re: just two features !  "
    date: 2008-03-17
    body: "- The desktop icons need the black background unless someone implements a nicer way to get enough contrast for the text\n\nValid point. But why isnt it possible to have one large black box and put all icons inside? Would look much better."
    author: "Michael"
  - subject: "Re: just two features !  "
    date: 2008-03-17
    body: "One candidate plan for dealing with the whole \"have ~/Desktop represented on your desktop somehow\" is precisely this - an area of the desktop cordoned off especially for having these kind of icons there.  So you might be in luck :)"
    author: "Anon"
  - subject: "Re: just two features !  "
    date: 2008-03-18
    body: "A nice idea would be able to put \"folders\" on the desktop in this way. If we're moving away from using the Desktop as a whole to show files, it might be counterintuitive to continue to encourage file storage in ~/Desktop?\n\nWhat might be nice would be the ability to show a folder (e.g. ~/Home, ~/Documents, ~/Downloads, ~/*recent files*, ~/some-other-dynamic-search-folder) on the desktop. Creating these areas could even be the default action when folders are dragged onto the desktop. When people use the desktop to store working-files, normally this means they're either just downloaded from the net or are being edited recently... \n\nThat way we get away from the desktop as file-dump while also allowing for the convenience of having files available on the desktop (which at 1 click away is always going to be closer than browsing through folders).\n\nJust a thought :)\n\n"
    author: "Martin Fitzpatrick"
  - subject: "Re: just two features !  "
    date: 2008-03-19
    body: "This is an absolutely great idea. I would love to see that feature."
    author: "Michael"
  - subject: "Re: just two features !  "
    date: 2008-03-19
    body: "But why are we moving away from the well-established \"Desktop\" concept? I would like to vote for having a desktop that works as I expect a desktop to work like on any other OS\n\nSurely it's a matter of habit, but I really dislike the way the desktop is handled by KDE4 currently, and I guess I'm not the only one.\n\nWhy was it decided that the \"old\" desktop paradigm is deprecated? Just curious."
    author: "Anon"
  - subject: "Re: just two features !  "
    date: 2008-03-19
    body: "you can look on for example Aaron's blog for answers to that question, but a hint: the desktop with icons was developed for 180K floppydrives with just a few files. We now use filemanagers to manage thousands of files. Surely the desktop can be used better, as it's original use (showing all files and/or applications you have) isn't there anymore?"
    author: "jospoortvliet"
  - subject: "Re: just two features !  "
    date: 2008-03-19
    body: "What I was getting at was intended to maintain the useful functionality of the Desktop: that is quick and easy access to useful/relevant files.\n\nI've not used the current KDE4 desktop yet so I can't really comment but from what I understand it handles individual files as plasma applets?\n\nWhat I would like to see is plasmas for known groups of files \"Recent Downloads\" \"Recently Opened\" \n\nIdeally I would like a Task-Orientated desktop where it was possible to group a bunch of files, bookmarks, etc. in a single package so when I want to do some of \"X\" I just open the \"X\" package and bam. Everything is back where I left it...  a lot of apps in KDE provide similar functions now in Sessions but a wholly coordinated/integrated system would be neat.\n\nYou could then ship with some default \"Tasks\" to start people off.\n\nBut anyway, I've wandered a mile off topic. Short answer: I agree."
    author: "Martin Fitzpatrick"
  - subject: "Re: just two features !  "
    date: 2008-03-19
    body: "Something like that is already underway. You can find it in playground."
    author: "sebas"
  - subject: "Re: just two features !  "
    date: 2008-03-17
    body: "I am also using the latest openSuse updates. Suse is really doing a great job in providing it's users with such excellent service.\nThe packages are quite stable but there are still a few glitches that I will report on Bugzilla later.\n\nThe applet handlers on the icons can be very useful, you can resize individual icons to specific sizes or even turn them upside down. Try it, it's far more useful than it appears at first and you don't see it when you lock the widgets. I agree that the icons background doesn't look nice at all.\n\nWell the panel is improving quite fast but it still has a long way to go before it can match the good old Kicker :)\nIt's a nice feeling though to watch KDE 4 as it evolves.\n\nHave you noticed that Kolor Lines (my wife's favorite game) now has more than one themes? A few gimmicks but nonetheless cool.\nThe games are looking good, oxygen and plasma are also shaping up well. I just can't wait to see 4.1."
    author: "Bobby"
  - subject: "Re: just two features !  "
    date: 2008-03-18
    body: "I couldn't agree more. The black rectangle behind the icons is inredibly ugly. Eww. It's the reason I have stopped using icons with KDE4...sigh. :\\"
    author: "fish"
  - subject: "Re: just two features !  "
    date: 2008-03-18
    body: "I have been giving the icons on desktop some thought and have reached the conclusion that it isn't going to work to treat the icons the same as the other desktop widgets.\n\nI don't think that icons need \"applet handle things\".  A right click context menu is going to have to be sufficient.  IAC, it is not correct to have them since plain icons should all be the same size and don't need to be rotated -- at least the icons should all be rotated by the same amount (this might look cool, but it is going to require smaller steps).\n\nOTOH, those dark blue rectangle things are very useful.  The ones on icons all need to be the same size (but that is a bug).  As with many things, configuration solves many such issues.  It should be possible to set the color and the transparency for these rectangle things (for all desktop widgets) -- there should probably be only one setting which would apply to all desktop widgets.\n\nThat said, I can see a place for an application launcher widget.  Perhaps one for a single and one for multiple applications (Quick Launcher on KDE-3).\n"
    author: "JRT"
  - subject: "Oxygen Progress?"
    date: 2008-03-17
    body: "Does anyone know if the Oxygen theme is still being actively developed? There hasn't been an update in quite a while."
    author: "cb"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-17
    body: "it seems so. I use KDE4 from svn and the last days there were some changes. \nfor example the items on the places panel in dolphin are now hovered when moving over them."
    author: "hias"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-17
    body: "yes we are working :)"
    author: "andre"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-17
    body: "Actually it did receive some updates recently, like a new look for selections. But sure progress isn't very fast."
    author: "jospoortvliet"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-17
    body: "Hey! We are working on it, latly i have been rather busy with other works kde related.\nBut we are still working on the icons, and I hoppe Casper will very soon commit more code into Oxygen. "
    author: "nuno pinheiro"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-17
    body: "The progress is obvious (although a bit slow) but there are still quite a few missing icons. I still have a lot of question marks all over the place."
    author: "Bobby"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "Progress seems good to me.  A style is not like other programs where you want fast rate of change.  All you really want it minor tweaks.  We don't want a complete new look every release.  Oxygen rocks already."
    author: "Leo S"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "I don't think the progress is slow, it takes time and it is obvious from his comments he is working on other projects, 4.1 is a while away so he has time to burn.  But honestly when it is finished how many people will stick with oxygen?  I think most will do what they always do, go to kde-look and use what appeals more to them.  I know although Oxygen is nice and a lot of hard work goes into it, I wont be using it, simply because I like to have my computer look the way I like it :)"
    author: "anon"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "Can't agrea more :)\nI hope we can have the Openoffice icon set done in time for 4.1 and arround 50 new apps icons, pim included.\nFor the style, i hoppe we can have a new much beter divider, some work on the windeco... maybe some work in the butons and the scroolbars/progressbars ofcourse.\n "
    author: "nuno pinheiro"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "I hope someday Oxygen will look closer to the mock-ups you did.  Those are incredible!"
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "I agree to that, those mock-ups showed incredible creativity.\n\nI've got to say that the only thing I don't like about Oxygen though is the windows deco. Too low in contrast (specially trying to identify active/inactive windows at first glance) and I'm not a fan myself of the button looks. On the other hand, the icons are extremely high quality, and the widgets (specially the buttons) are way too cool to be said."
    author: "NabLa"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "Indeed. Is it possible to make the active window a colour like blue, while inactve windows remain gray? That'd be fantastic."
    author: "Agreed"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-19
    body: "Already possible. Look in the configure colors section. We didn't enable it by default as it makes the desktop 'flash' when you switch windows."
    author: "jospoortvliet"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-20
    body: "If this \"flashing\" is more than changing the color of the window becomeing active and the one becoming inactive, it's a serious bug. Or rather it triggers a serius bug, simply changing titlebar color shuld not create aby kind of flashing, uncontrolled repaints or whatever."
    author: "Morty"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "If you are using the compositing effects then you can darken the inactive windows. At least that's how I do it. What I personally don't like are the buttons. They are in need of a little colour. The other things are quite nice though."
    author: "Bobby"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "Yes!  And a completely animated progress bar would be like ice cream with our cake!"
    author: "BATONAC"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-20
    body: "Well, I will stick to oxygen because right now, it is by far the best theme out there, maybe only beaten by Mac OS Leopard which still looks a bit cleaner."
    author: "Thomas Walther"
  - subject: "Re: Oxygen Progress?"
    date: 2008-03-18
    body: "I hope there will be some backports to the 4.0.x branch. There are some glitches that are not really acceptable on my 4.0.2 box (most annoying: the black shadows on Konqui's tabs if there are more tabs opened than space available). By backporting I would see the improvements in the style and I may file bug reports. Currently I am afraid of giving bug reports on already solved issues... \n\nMy greatest care is that Oxygen could lead to the same desaster as the Keramik style. I loved the basic ideas of Keramik. But it was never improved after the first \"final/official\" release. In the end there were so many rough edges visible that I could only accept it as a technology study, switching to Plastik asap. "
    author: "Sebastian"
  - subject: "\"online reader\" syncronization project in Akregato"
    date: 2008-03-17
    body: "\" \"online reader\" syncronization project in Akregator. \"\nw0000000000t!!!!! :)"
    author: "mxttie"
  - subject: "bugs.kde.org"
    date: 2008-03-17
    body: "May I ask whether somebody currently works on making KDE's bugtracking system more usable? Something like Ubuntu's launchpad would be very nice as it'd make contributing bug reports much easier. Currently, bugs.kde.org feels a) old and dusty and is b) ultra-cryptic (try to get a list of all akregator-related bugs - you have to use this complex query form and will then get a large table full of strange abreveations like Sev, Pri, Plt, SuS, Gen, LO etc. and with bug reports from 2004 at its top)."
    author: "gissi"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-17
    body: "Sorry, but in my eyes bugzilla is still much more usable than Launchpad. Searching for bugs in Launchpad is still a pain... For me bugzilla does a great job."
    author: "Rolf"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-17
    body: "If you don't like the advanced search dialog; then don't use it. There is a single line for search on the frontpage; that one works just fine for your example.\n\nNote that its actually useful to have the oldest bugs at the top; you need to fix the oldest first, right? :)"
    author: "Thomas Zander"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-18
    body: "Searching isn't the problem, even though a quicker server response time is always a plus. The KDE bugzilla interface is a PITA, when you want to report bugs, forcing you to go through half a dozen of forms again and again."
    author: "Carlo"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-18
    body: "I would agree that the interface is fairly horrible. We (the Amarok dudes) have been looking into using a different system, but so far we've not found any viable alternatives.\n\nAny suggestions?\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-18
    body: "Bugzilla doesn't have to be a pain in the butt, I found Wine's bugzilla to be nicer/easier than KDE's.\n/opinion"
    author: "Jonathan Thomas"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-23
    body: "Can't you skip through most of them by initiating a bug report directly in a KDE app (Help > Report Bug...) though? "
    author: "Datschge"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-18
    body: "I guess, but would you prefer they spend more time on making it usable, or on fixing the bugs in KDE?  I've personally never had any problems with it."
    author: "anon"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-18
    body: "I have, although I finally got the hang of it. The problem is that for new users it's probably a little intimidating. I wouldn't consider launchpad the epitome of accessibility but they are aiming to make it usable by all Ubuntu users.\n\nOf course, fixing bugs in KDE would be a priority but the quality and quantity of bug reports is dependent on users being able to file them."
    author: "Martin Fitzpatrick"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-18
    body: "What about Jira (http://www.atlassian.com/software/jira/)?\n\nWe use it at work for issue management and all our customers are very fond of it. We even notice an increase in customer satisfaction since we use it.\n\nThey support OSS and provide free licenses for open source projects... However, I do not know if KDE applies for all requirements to receive a free license (http://www.atlassian.com/software/jira/pricing.jsp#nonprofit).\n\nJust my 5 cents...\n\nBest regards,\nRonald"
    author: "Ronald"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-19
    body: "\"Open Source\nAtlassian supports and believes in the Open Source movement - JIRA utilises a number of good Open Source components, and Atlassian developers are committers on a large number of Open Source projects.\n\nTo give back to the open source community (and hopefully improve the quality of those projects!), JIRA is free for any Open Source project to use.\n\nThere are a few requirements for an Open Source license, the main ones being: \n\nEstablished code base \nPublicly available project website \nUsing an approved open source license \nYour JIRA instance will be publicly accessible \"\n\nI suppose KDE falls under the open source category ;-) so it would be possible to get an open source licence.\n"
    author: "Terracotta"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-19
    body: "jira seems to be out of question. I can not find any place that says that it is free software or open source. I can not find any download link either. Maybe it would be a good 1st april joke, like when KDE said it would switch to bitkeeper?"
    author: "Erik"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-19
    body: "Ubuntu's Launchpad is closed source as well afaik..."
    author: "JAK"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-20
    body: "Yes, and that's a big problem.\n\nFedora, on the other hand, uses true Free Software for these purposes, we use Bugzilla for bug tracking, and for translations, we developed our own system, Transifex, which is released as Free Software, and which works together with upstream rather than against it (FYI, many Rosetta translations never end up in the relevant upstream repository).\n\nI don't think it makes sense to switch KDE away from Bugzilla, most big Free Software projects use it, so people are used to it. And it isn't that bad either. Most of the annoyances are common to many bug tracking systems and probably don't have an obvious solution: for example, from a user's point of view, it's very annoying to have to register an account to file bugs, but developers need a way to contact the users and account registration is the most effective way to validate e-mail addresses. I think the disconnect between user and developer needs is the biggest problem with bug tracking systems, and switching to a different BTS won't solve that."
    author: "Kevin Kofler"
  - subject: "Re: bugs.kde.org"
    date: 2008-03-19
    body: "Ok, to make my point a bit clearer:\n\nI did not intend to bash Bugzilla and/or KDE's web team. I only wanted to state that there's a lot of space to improve bugs.kde.org in regard of both usability and style. So as many other parts of KDE were redesigned lately I'd still like to know if somebody is working on that (maybe someone from the Oxygen team).\n\nAs well I did not do any deeper comparism between Launchpad and KDE's Bugzilla, but the very important first impression of Launchpad is _way_ better than bugs.kde.org's. This first impression could be easily improved by simply adopting the Oxygen style for Bugzilla and doing some little facelifting concerning the interface itself."
    author: "gissi"
  - subject: "Kross::ScriptingPlugin"
    date: 2008-03-18
    body: "Kross::ScriptingPlugin is a very logical and useful improvement, thank you Paulo!"
    author: "Nick Shaforostoff"
  - subject: "MacOS-like menus ?"
    date: 2008-03-19
    body: "Still nothing new about Bug #153027: \"No MacOS-like menu on top of screen in KDE 4.0\" ? Is there still somebody working on it, or was the idea dropped into the Pit of Oblivion ?"
    author: "Keiran"
  - subject: "Re: MacOS-like menus ?"
    date: 2008-03-19
    body: "Virtually all work on KWin is done by Lubos, and he is immensely busy.  I gather that he personally likes and uses the feature, though, so it's a matter of \"lack of time\" rather than \"feature has been deliberately dropped.\"\n"
    author: "Anon"
  - subject: "Re: MacOS-like menus ?"
    date: 2008-03-20
    body: "It was almost working until they deleted the config dialog from kde3 the day before KDE 4.0.0 was tagged so it is probably not that hard to restore. It did work fine during most of the alpha cycle."
    author: "Emmanuel Lepage Vall\u00e9e"
---
In <a href="http://commit-digest.org/issues/2008-03-09/">this week's KDE Commit-Digest</a>: Support for alternate direction layouts (vertical, horizontal) in <a href="http://en.opensuse.org/Kickoff">Kickoff</a>. Improvements in the Device Notifier applet, with support for window icons in the Pager Plasmoid in <a href="http://plasma.kde.org/">Plasma</a>, with the "Trash" applet moving into kdebase, the "Luna" applet moving to extragear, and the "Contacts" and "Converter" runners moving into kdereview. Support for online play (using the GGZ network) in the <a href="http://games.kde.org/game.php?game=ksquares">KSquares</a> game. A new default theme for the KSame game. More maintenance work in Kooka. Better interaction between <a href="http://okular.org/">Okular</a> and the KDE text-to-speech system. Full support for colour schemes in the interface of <a href="http://www.digikam.org/">Digikam</a>. Lots of old bugs fixed in the mimetype configuration dialog of <a href="http://www.konqueror.org/">Konqueror</a>. Continued work and optimisations in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. The start of the ability to extend GUI's with scripting actions using <a href="http://kross.dipe.org/">Kross</a>. KRatingPainter becomes a global way to manipulate ratings throughout KDE. A "Konsole" mode and "Web Interface" are added to KTimeTracker. Long-awaited support for inserting signatures at the top of email replies in <a href="http://kontact.kde.org/kmail/">KMail</a>. The ability to store all attachments in a specified directory, and a status bar are added to <a href="http://www.mailody.net/">Mailody</a>. <a href="http://korganizer.kde.org/">KOrganizer</a> gets a new "Month" view. More work on the "online reader" syncronization project in <a href="http://akregator.sourceforge.net/">Akregaror</a>. Work on general Zeroconf support in KDE, and particularly in KRDC. Continued porting of <a href="http://kommander.kdewebdev.org/">Kommander</a> to KDE 4. The start of moves to make <a href="http://phonon.kde.org/">Phonon</a> the only output engine in <a href="http://amarok.kde.org/">Amarok</a> 2. <a href="http://commit-digest.org/issues/2008-03-09/">Read the rest of the Digest here</a>.

<!--break-->
