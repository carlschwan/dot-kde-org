---
title: "KDE Commit-Digest for 6th January 2008"
date:    2008-01-11
authors:
  - "dallen"
slug:    kde-commit-digest-6th-january-2008
comments:
  - subject: "GOOD JOB"
    date: 2008-01-10
    body: "I must say I was impressed with kde400!  It is so cool!\nI will try it on Windows too !"
    author: "zvonsully"
  - subject: "Re: GOOD JOB"
    date: 2008-01-10
    body: "on Windows only kdegames, kdeedu + konquerror works for me. Dolphin has problems with folders on first run (yet it works when lounched from Places). There is no more working apps, so win32 is unusable for now. (however I use Kmines on Windows now :D )\n\nInstallation should have better UI (no need for choosing between mvc and this second compilation for normal user)"
    author: "Dolphin-fanatic :)"
  - subject: "Re: GOOD JOB"
    date: 2008-01-10
    body: "Normal users aren't install kde-win32 yet."
    author: "Ian Monroe"
  - subject: "Re: GOOD JOB"
    date: 2008-01-10
    body: "Windows doesn't like animals. No Penguin, no Dolphin and no Dragons ;) "
    author: "Bobby"
  - subject: "Re: GOOD JOB"
    date: 2008-01-10
    body: "No leopards either"
    author: "Zig"
  - subject: "KHexEdit"
    date: 2008-01-10
    body: "\"KHexEdit moved to the unmaintained module.\"\n\nWhat exactly does that mean (besides what it says... :-) )?  Does that mean there is no hex editor \"built into\" KDE?  I'm aware that it's not like that's the only hex editor out there, but, having one just \"simply there\" sure has been a handy thing for a good number of years now.\n\nJust wondering.\n\nThanx.\nM.\n"
    author: "Xanadu"
  - subject: "Re: KHexEdit"
    date: 2008-01-10
    body: "Okteta is there and is maintained and developed."
    author: "Emil Sedgh"
  - subject: "Re: KHexEdit"
    date: 2008-01-10
    body: "It means KDE 4.0 does not include a hex editor, as the old one is not maintained and not fully ported. For KDE 4.1 Okteta should be ready to jump in, it currently misses only a \"few\" things to match KHexEdit featurewise. Until then you can still run KHexEdit from KDE3 just fine inside a KDE4 environment. Seems like the fate with a .0 version of foundation libraries, as KRegExpEditor and others have similar problems, see http://techbase.kde.org/index.php?title=Schedules/KDE4/4.0_Module_Status.\n\nStill anyone is free to revive KHexEdit if she wants to."
    author: "Frinring"
  - subject: "Re: KHexEdit"
    date: 2008-01-11
    body: "Who is \"she\"?  And why isn't she already working on KHexEdit if it's obvious that she should?"
    author: "me"
  - subject: "Re: KHexEdit"
    date: 2008-01-11
    body: "She is the female of the species (more deadly than the male - Kipling).  Since hex editors allow dangerious things that are really hard to do any other way, hex editors are clearly more suited to female developers.\n\nAs for why she isn't working on it already, it isn't obvious that she should, just that she could.    "
    author: "Hank Miller"
  - subject: "Re: KHexEdit"
    date: 2008-01-11
    body: "Note, I posted the above without reading other threads.  Understand that above was written entirely in humor.   There are serious questions about why more females don't contribute to KDE/software/technial projects as much as men.   That doesn't mean we should lose our sense of humor about the situation."
    author: "Hank Miller"
  - subject: "HUGE Digest..."
    date: 2008-01-10
    body: "Thanks Danny.Youre doing a great job."
    author: "Emil Sedgh"
  - subject: "Re: HUGE Digest..."
    date: 2008-01-11
    body: "Yes, a BIG thank-you, Danny!"
    author: "mactalla"
  - subject: "KTank"
    date: 2008-01-10
    body: "First time I hear about KTank.....\n\nHere is the development website:\n\nhttp://ktank.free.fr\n\nThis (oldest post on the site) is what KTank is about:\n\"Ktank will be a remake of the wii tank play. For the moment being however, it\u0092s just a project. Ktank will work with the last KDE4 gaming api. The game will be playable over the network using the latest GGZ library.\"\n\nSorry if this is already generally known...."
    author: "Michel Vilain"
  - subject: "Re: KTank"
    date: 2008-01-10
    body: "thanks for the info. I didn't know it."
    author: "yman"
  - subject: "Re: KTank"
    date: 2008-01-11
    body: "I hope it will support the wii controller."
    author: "Beat Wolf"
  - subject: "Re: KTank"
    date: 2008-01-13
    body: "KTank Will use OpenGL too!"
    author: "DrIDK"
  - subject: "one more day...."
    date: 2008-01-10
    body: "One more day and KDE 4.0 becomes a reality!!! :)\n\nProbably a few more days after that until there will be Kubuntu packages. :(\n\nAny new KDE based distros coming out between now and Hardy Heron?\n\n\nHopefully somebody will fix the Compiz fusion dilemma, so I can enjoy all my compiz effects withing KDE 4.0...\n\nGood Job on the Release guys... Now let's just crank up the publicity and the marketing!!!\n\n\n-M"
    author: "Max"
  - subject: "Re: one more day...."
    date: 2008-01-10
    body: "A lot of Debian packages are already in experimental, and more in incoming.\n\nCan you Kubuntu guys use those?"
    author: "Debian user"
  - subject: "Re: one more day...."
    date: 2008-01-11
    body: "Don't, you'll break your system.\n\nJust be patient, its only a day or two."
    author: "Level1"
  - subject: "Re: one more day...."
    date: 2008-01-10
    body: "Kubuntu packages are already hitting the mirrors, just not for all packages as yet.  Very exciting seeing the 3.98 roll over to 4.0.0 :-)\n\nJohn."
    author: "Odysseus"
  - subject: "Re: one more day...."
    date: 2008-01-10
    body: "I don't think that I will sleep until the openSuse packages are up.\nA very big thank you to the whole KDE team for the supurb job, for crossing the finishing line in style in spite of the tongue lashings in the past few weeks :)"
    author: "Bobby"
  - subject: "Re: one more day...."
    date: 2008-01-11
    body: "what compiz dilemma? you don't need compiz in kde4; there's composite support built into our dear old kwin now :) it might not have as many cool effects yet, but I doubt it's hard to add them... I saw someone asking questions the other day that made it sound like he was working on wobbly windows :)"
    author: "Chani"
  - subject: "Re: one more day...."
    date: 2008-01-11
    body: "cool..\n\nAs long as I can get the same effects as compiz and more effects I'll be happy...\n\nI hope KDE Developers are working together with Compiz fusion developers. No need to reinvent the wheel here.. Just adapt existing ideas and modernize them. :)"
    author: "Max"
  - subject: "Re: one more day...."
    date: 2008-01-11
    body: "http://sabayonlinux.org will surely release with KDE4 pretty soon."
    author: "Joost Ruis"
  - subject: "We need more women!!!"
    date: 2008-01-10
    body: "I just looked at the statistics of the commit:\n\n88.4% Male\n5.42% Female\n\nSeriously!! We need more female developers. The Linux world shouldn't be an ol'boys club. That's what the old computing world was for (Windows, etc.)\nOpen source should be about equal opportunity.\n\nAlso, I'm sure that KDE would benefit from a female touch. :) We all know that women have a better sense of style (and beauty) than us men ever will.\n\n-M"
    author: "Max"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "it might just have to do with personal interests. no one is stopping women from getting involved, so they probably just don't want to. there are equal opportunities, if women aren't taking what they can have, it's because they don't want it."
    author: "yman"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "sadly, it's not that simple. there are lots of subtle things that can push women away; society in general pushes them towards more 'feminine' things, teaching children that girls are supposed to be one way and boys another (it seems to me that the *boys* who don't fit into this are the ones who suffer most these days - grade school can be such a cruel place). computers are seen as a male thing. geeky male stereotypes are all over the place. women still have more trouble than men in the workplace, which may turn some of them away from comp sci in general.\nI find it quite interesting that in china, the gender ratio in comp sci is actually close to equal (or at least my teacher said so; I haven't checked the numbers myself).\n\nalso, perhaps some girls just don't like being surrounded by guys all the time. bit of a chicken-and-egg problem, that. :)\nthen there's the problem that when there are few girls around, the atmosphere can be kinda... male-centric? 'guy talk' can kinda make girls feel left out (thankfully I don't see kde channels having conversations about pr0n or anything) and sometimes guys tend to communicate differently from girls. in the same way that people from asian cultures can be unintentionally pushed away by the directness of criticism, girls can be pushed away by what people on the inside see as perfectly normal behaviour. most will either adapt to it by acting more like a guy (this isn't really a good thing; co-operation and discussion tend to be feminine traits), or turn away. and then on top of that there's the whole free-software-culture thing to adjust to. I think in this a little flexibility is required from both sides. :)\n\nwhile women on *average* may be less likely to enjoy programming, I'm certain that the difference is far smaller than what we actually see.\n\nI think KDE is really a great community, and I've hardly seen any seen any sexist behaviour (what little there was was unintentional, I'm sure), but people aren't likely to discover kde until they've already got past other potential barriers - in the general free software community, in school, at work, and in society.\n"
    author: "Chani"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: ">> also, perhaps some girls just don't like being surrounded by guys all the time.\n\nNow how did a female friend of mine in engineering put it.. Something like there's quantity but a real lack of quality :)\n\nAlso I have a current female co=worker that started out in engineering but was repelled in her first semester by the masses of socially awkward engineering males.  Not that it's the rule, but engineering/csc certainly has more than its share of those kinds."
    author: "Leo"
  - subject: "Re: We need more women!!!"
    date: 2008-01-12
    body: "> Now how did a female friend of mine in engineering put it.. Something like there's quantity but a real lack of quality :)\n\nOr what the female students at Caltech say: \"The odds are good, but the goods are odd\".\n\n"
    author: "LMCBoy"
  - subject: "Re: We need more women!!!"
    date: 2008-01-15
    body: "Haha yeah that was it.  Your memory is significantly better than mine :)"
    author: "Leo S"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "No doubt. But I don't think any one is pushing the females away. its the difficulty and complexity of such a system that makes it naturally an ol'boys thingy.\n\nBut still, as you said, there are things that we men admit we would never be as good as female in.\n\nSo, in addition to development, females can contribute tremendously to UI (and usability/user experience in general), Quality, and Documentation.\n\nMaybe they are not approached properly? maybe tasks are not clearly classified and then advertised properly?\n\nI leave this to the core kde team to tell about...\n\nat last, all kudos to the great 4-oh-oh !\n\n- Kefah.\n\n"
    author: "Kefah Issa"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "<i>its the difficulty and complexity of such a system that makes it naturally an ol'boys thingy</i>\n\nIf I were a woman, that comment would have pushed me away. "
    author: "random kde user."
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "\nYes, its a known fact that boys like to tinker with more complex things, including when that thing is simply pointless.\n\nFor example, its a known fact that violant games are designed for boys... this why girls (in general) don't like playing them.\n\nIts does not have any thing to do with who is better than the other. \n\nIts important that we understand, that our actions as a community, and our expectations could be the real reason why feminine contribution is minimal.\n\nPlease take what I say in a good well."
    author: "Kefah Issa"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "puh-lease. in the mainstream industry the numbers are something like 30% women in software. in some countries like malaysia the number of women getting management and technical degrees is greater than the number of men.\n\nyes, there are gender biases (which don't always line up with your gonads, btw) but we are far away from the much smaller differentials those biases may create."
    author: "Aaron J. Seigo"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "Then you just proved his point."
    author: "Joe"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "\"its the difficulty and complexity of such a system that makes it naturally an ol'boys thingy.\"\n\n\"So, in addition to development, females can contribute tremendously to UI (and usability/user experience in general), Quality, and Documentation.\"\n\nDid I just timewarp back to the 1950's, or something? These are awfully ignorant comments."
    author: "Anon"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "mmm. What I mean here is we are different! I do fully acknowledge the equality concept. I am not saying one is superior over the other. I am just saying that by nature we are different! \n\nfor example i would say a similar thing if it was brought up : why arabs or africans are not contributing as much. I would say that maybe the system complexity for them presents an obstacle, and maybe they can still help in many other things, (while keeping the door for helping in development open).\n\nPlease don't be hard on me. \n\n"
    author: "Kefah Issa"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "perhaps it would be best if you just STFU."
    author: "attendant"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "Yes, you should."
    author: "Joe"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "I get the impression that English is not your first language? Discussing these things is potentially explosive at the best of times, so I've given you the benefit of the doubt on some of the clumsy phrasing.\n\nIn an attempt to dig you out of your hole, would I be right to assume by \"complexity\" you mean in the \"learning curve\" sense. That is, because those groups you identify as being under-represented have - to date - less experience/track record of working on open software projects, there is a barrier to entry for them to take part? Even if only in terms of exposure, peer contact or community.\n\nThe comments on the UI stuff does smack a little of \"girls play with the pretty things\". That said, they are good areas to contribute for people from non-programming backgrounds, a description that fits more women than men. For now.\n\nHopefully that's you rescued... ...now, go get back to less complex things ;)"
    author: "Martin Fitzpatrick"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "I think he has a point, even though he put it rather crudely. Boys generally enjoy technical and philosophical talk more than girls. There are many exceptions to that rule, of course - and it might very well be a cultural thing. In the Netherlands, more woman study and graduate from medicine schools - the average young surgeon here is woman, and she does a better job than males do... As that used to be a very male profession, I suppose it's kind-of a hint at the cultural basis of such biases."
    author: "jospoortvliet"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "In my (computing graduate) school, the number of women has been on par with the number of men during many years (70s and 80s basically). They were even around 60% some years. But their number began to dwindle in the 90s, to become less than 10%. They are almost 25% now so it's better but it's still unbalanced. (But most of the teachers are still women.) According to some people they began to flee when the socially challenged geek clich\u00e9 became widespread. But still, it means that the situation is far from being unrecoverable.\n\nBut when talking about men/women differences, bear in mind that more often that not, personal differences are greater than gender _average_ difference."
    author: "Thomas"
  - subject: "Re: We need more women!!!"
    date: 2008-01-12
    body: "Women are no more sensitive to public opinion than men are. Jeez, what a bunch of patriarchs in this thread."
    author: "blacky"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "see http://en.wikipedia.org/wiki/Women_in_computing for some of the more famous women who have contributed over the years, also highlights that there are ~2.5 times more men educated in CS at the moment - this may be due to comments like Mr Issa's?"
    author: "anon"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "No... it's because people are different.\n\nThe stats speak for themselves. Women aren't interested in developing for KDE. Just because 3 are, doesn't mean anyone is \"better\" than anyone.\n\nTo each his (or, to put it in KDE's obsessive political correctness, \"his/her\" or \"her\") own task. I write a little bit of code, tear apart cars and work on computers. I'm not a businessman, day trader, gardener, house cleaner, welder, pilot, construction worker, stay at home person, ..."
    author: "anon2"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "I do take your point, just seems such a waste of available talent.  Anyway, I get the feeling these posts are like trying to have an argument with oneself - in lieu of any actual women posters hereabouts I mean :)"
    author: "anon"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "um, that's kinda like saying that women just weren't interested in having jobs or voting before the 19th century."
    author: "Chani"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "Going slightly off topic here, but Wyoming (while still a territory) gave women the right to vote (and hold office!) in 1869, before any other US State or Territory, and before New Zealand, which was the first nation as a whole to do so.\n\nhttp://tinyurl.com/2oj2g4\n\nIt's one of those things that makes me wonder why we get painted as backwards rednecks so often :-/\n\nBack to your regularly scheduled thread!\n"
    author: "MamiyaOtaru"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "88.4% + 5.4% = 93.8% != 100%\n\nSo what are those neither male nor female. They can't all be hybrids, can they?"
    author: "Anonymous"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "They've undeclared, i.e. Danny doesn't know what they are and is far to polite to ask..."
    author: "Odysseus"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "According to the digest, 6.09 % are of unknown gender. That's right. There's more of them than the females."
    author: "Hans"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "maybe some girls don't want to let on that they're girls, since some people get so weird about it...\n\nKDE has been pretty good about this in my experience, though. most people seem quite mature :) I don't even think about gender issues most of the time."
    author: "Chani"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "\"maybe some girls don't want to let on that they're girls, since some people get so weird about it...\"\n\nI agree with that, what we're talking about is geeky stuff and you all have to agree that geeky stuff, and not just geeky computing stuff, is just full of scary weirdos..."
    author: "NabLa"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "Don't make assumptions!  They actually could be :)  But let's stop talking about genders and stereotypes please.  It can only spoil a great KDE 4.0 celebration :)"
    author: "Lee"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "Yea, I didn't add the 6% as they were undeclared. I didn't want to make assumptions. Even if they were all women, the ratio of male to female KDE developers is still very sad. :("
    author: "Max"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "The problem with these \"human\" statistics is that they are \"opt-in\" - it is not a condition of getting an SVN account that this information is provided, and this is just a private initiative by me.\n\nWhere I don't know the gender of a person, I can often guess from their name. However, this is not possible for other ones, such as age and location, and so those are less complete. And of course, there are new accounts created weekly, which makes the data collection an ongoing effort.\n\nPeople with KDE SVN accounts can contribute their data at http://commit-digest.org/data/\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "Yes they have a better sense of style and beauty. That's why they are models but when it comes to technology? Who said that women can't drive cars :D"
    author: "Bobby"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "\"Don't make assumptions! They actually could be :) But let's stop talking about genders and stereotypes please. It can only spoil a great KDE 4.0 celebration :)\"\n\nI fully agree. I am stopping right here. Please dis-regard my previous comments and lets not create a flame out of this."
    author: "Kefah Issa"
  - subject: "Re: We need more women!!!"
    date: 2008-01-10
    body: "Remember that those percentages are based on the *number of commits* in that week, not the number of contributors (even though the actual classification of contributors is likely similar).\n\nI would welcome a respectful debate on this issue (a comment below states that the ratio of male/female Computer Science students is about 3 to 1 - this has not been my personal experience at university, and this is certainly not the case in the KDE project).\n\nOut of the 991 KDE contributors in my database, 969 are male and 22 are female - and this covers all areas of KDE, including documentation and translation.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "Yea, at my old University. Cal State Monterey Bay it was actually a bit more balanced than that. But anywhere from a 60/40 to a 70/30 split (male/female) would be a good goal to set for the KDE 4.X.Y release cycle.\n\nComputing should be more equal opportunity, and if women are welcomed more into the KDE/Linux world (without stereotypes (bad ones, at least) they will join in.\n\nSo instead of perpetuating this stereotype, I suggest that every person that reads this introduces 1-2 female programmers/developers/artists to KDE. Let's see if we can accomplish the goal of an even split by KDE 5.0 :)"
    author: "Max"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "But it is equal opportunity. Females obviously don't want it, or care.\n\nIt's sexist to think otherwise. As long as the result is what is desired, I wouldn't care if the statistics were the other way around - and I don't think we'd be having these sexist posts, either."
    author: "anon2"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "but it *isn't* equal opportunity. you obviously just haven't seen this for yourself. lots of people don't see their own privilege; it's kinda like trying to see the back of your own head.\n\nit's certainly a heck of a lot better than it used to be, but there's always room for improvement. :)"
    author: "Chani"
  - subject: "Re: We need more women!!!"
    date: 2008-01-11
    body: "Give it a start Maxine. Gender reassignment is en vogue, I've been told."
    author: "Carlo"
  - subject: "Thanks Danny."
    date: 2008-01-11
    body: "I know it's not much but I put about $10 to your account as an appreciation for your work with the digest. I hope you get that new laptop you were hoping to buy.\n\nA big thank you for your effort. I enjoy the digest every week."
    author: "Oscar"
  - subject: "Re: Thanks Danny."
    date: 2008-01-11
    body: "Thanks - I got another 2 donations this week, something I really wasn't expecting ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Changelog KDE 4.0 <-> 3.5.x ?"
    date: 2008-01-11
    body: "Is there any fast or small readable changelog what features are included/lost in applications?\nFor example, what features are included in Kmail?\n\nThank you!\n"
    author: "anonymous"
  - subject: "Re: Changelog KDE 4.0 <-> 3.5.x ?"
    date: 2008-01-11
    body: "kdepim isn't part of the KDE 4.0 release. You will have to wait for KDE 4.1."
    author: "cloose"
  - subject: "Re: Changelog KDE 4.0 <-> 3.5.x ?"
    date: 2008-01-11
    body: "In other words, all features where lost for the time being ;-)"
    author: "jospoortvliet"
  - subject: "Hallelujah!"
    date: 2008-01-11
    body: "KDE 4 is here with all it's wonderful sounds and glory! A big congratulation to the KDE Team, well done guys :)"
    author: "Bobby"
  - subject: "Thank you!"
    date: 2008-01-11
    body: "I want to thank everybody involved in advance.\n\n"
    author: "Joost Ruis"
  - subject: "Oxygen sound theme"
    date: 2008-01-11
    body: "> so there's a rhythmic pattern that distinguishes the sounds;\n> as opposed to other solutions out there, that rely on making\n> the user jump off the chair every time something happens.\n\n*grin* and this sounds really interesting! Looking forward to it!"
    author: "Diederik van der Boor"
  - subject: "Future of Commit-Digests"
    date: 2008-01-18
    body: "Will you continue to (ir)regularly publish commit-digests, now that kde4 ist out?\n\nThanks!"
    author: "Hannes Hauswedell"
  - subject: "Re: Future of Commit-Digests"
    date: 2008-01-18
    body: "The commit-digest is one of the great bonuses one gets in free software culture.\n\nBased on the unrestricted availability of the base data (who committed what, when, for what reason) and hard work by restless contributors like Danny, the quite technical and huge amount of base data gets transformed into an easy to digest (pun intended) form, for community members to consume who either do not have the time to wade (I'm getting good at this) through all of the base data or don't have the skills or toolsets necessary for accessing it.\n\nGetting back to your question: the commit-digest is a feature not coupled with any specific event such as a release, it has, does and will continue to allow a broader part of our contributors and followers to \"know what's going on\".\n\n"
    author: "Kevin Krammer"
---
In <a href="http://commit-digest.org/issues/2008-01-06/">this week's KDE Commit-Digest</a>: Final commits for KDE 4.0 Final before the tagging freeze. KDE 4.0 Final tagged for release. Lots of optimisations and bugs fixed across KDE. <a href="http://en.opensuse.org/Kickoff">Kickoff</a> menu items can now be added to the <a href="http://plasma.kde.org/">Plasma</a> desktop or panel. Improved resize and rotate for Plasma applets. Document list sorting in <a href="http://kate-editor.org/">Kate</a>. Various progress in <a href="http://www.kdevelop.org/">KDevelop</a>. <a href="http://www.mailody.net/">Mailody</a> moves towards using <a href="http://pim.kde.org/akonadi/">Akonadi</a> for its IMAP functionality, various improvements in Akonadi. Start of a KHotNewStuff2 implementation in <a href="http://edu.kde.org/kalzium/">Kalzium</a> for downloading molecular files. Experimental IVTV support in the Kalva video player. <a href="http://kget.sourceforge.net/">KGet</a> uses more of the shared implementation of BitTorrent from <a href="http://ktorrent.org/">KTorrent</a>. Printing support for the DVI backend in <a href="http://okular.org/">okular</a>. Improved text handling, support for printing multiple page sizes in a single document, and a much-anticipated Table <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> shape in <a href="http://koffice.org/">KOffice</a> (with further work on the Music Flake shape). Lots of work on colour manipulation for KOffice. <a href="http://kile.sourceforge.net/">Kile</a> begins to use <a href="http://kross.dipe.org/">Kross</a> as its scripting framework. Start of a new KDE game, KTank. GetHotNewStuff support disabled in okular, search runner disabled in Plasma for KDE 4.0. KHexEdit moved to the unmaintained module. The new <a href="http://oxygen-icons.org/">Oxygen</a> wallpapers, splashscreen, and sound theme are imported into KDE SVN for KDE 4.0. <a href="http://commit-digest.org/issues/2008-01-06/">Read the rest of the Digest here</a>.

<!--break-->
