---
title: "PolishLinux Preview KDE 4.1"
date:    2008-03-20
authors:
  - "Oscar"
slug:    polishlinux-preview-kde-41
comments:
  - subject: "Font rendering"
    date: 2008-03-20
    body: "Not related directly to the article, but as we are talking about \"polishing\" KDE 4;):\nOn my setup, KDE3 and KDE4 have the same font settings (including antialiasing). However, while the KDE3 font rendering seems almost as good as on windows, KDE4's fonts look kind of fragile and frayed.\nThe screenshots in this article look definitely better than what I achieve with my KDE4 setup. What are the doing?\nBTW, I'm using Tahoma and Vardana fonts, looking great both with WinXP and KDE3."
    author: "Anon"
  - subject: "Re: Font rendering"
    date: 2008-03-20
    body: "I've noticed different rendering between Qt4 and everything else too, and I've seen it mentioned here and there as well. There's a bug open at trolltech which might be relevant: http://trolltech.com/developer/task-tracker/index_html?id=+195256&method=entry"
    author: "Gusar"
  - subject: "digiKam for KDE 3"
    date: 2008-03-20
    body: "digiKam for KDE 3 also have timeline and Google maps integration."
    author: "m."
  - subject: "Amarok"
    date: 2008-03-20
    body: "Some usability nitpicks about the kickoff menu..\n\nSomething which striked me as odd about the kickoff menu is the \"leave\" menu. When you press something like \"shutdown\" or \"restart\" it pops up a menu which basicly asks you the same thing. thereby invalidating the choice you made before.\n\nAnother thing that might be nice would be to make the menu a little bit bigger by default so that you wouldn't need a scrollbar by default in the application part of the menu. ( scrollbars by default is really really bad. )\n\nI realize that by the time kde 4.1 comes out i can resize it myself. so i suppose it isn't that much of a problem. but it would probably be nice to such things as defaults.\n\nAnother thing which boggled in my mind is that i find it really hard to see the difference between active and inactive windows. I found out that i always relied on the inactive window having a darker window then the active one. but with the new style it is basicly all white. I never really noticed how much i relied on it until now that it is gone. Any chance we could get just a little grey or something back for the inactive windows?\n\nRight now i use a composite effect that basicly darkens the whole window. this works very nice as well, but unfortunately my laptop still has a kinda love/hate relationship with composite, so it would probably still be a nice thing to have.\n\nOther then that i have been enjoying kde 4 quite a lot up to now and believe that kde will continue to be as stunning and impressive as ever."
    author: "Mark Hannessen"
  - subject: "Re: Amarok"
    date: 2008-03-20
    body: "\"When you press something like \"shutdown\" or \"restart\" it pops up a menu which basicly asks you the same thing. thereby invalidating the choice you made before.\"\n\nAs much as I dislike kickoff, I have to defend it here: Other start menu alternative also have this pretty annoying behaviour :(. Problem seems to be somewhere else.\n\nI totally agree concerning the window stuff: Current Oxygen-theme seems to ignore any setting distinguishing active and inactive windows..."
    author: "anon"
  - subject: "Re: Amarok"
    date: 2008-03-20
    body: "\"I totally agree concerning the window stuff: Current Oxygen-theme seems to ignore any setting distinguishing active and inactive windows...\"\n\nI ran into this a while back and went searching for some answers. I ended up at:\n\n  http://bugs.kde.org/show_bug.cgi?id=152030\n\nwhich basically says \"this is the way we intended it to work, and no, you can't change it\". The prevailing opinion seems to be \"install a different theme\"."
    author: "Robert Clark"
  - subject: "Re: Amarok"
    date: 2008-03-20
    body: "Well, in opensuse KDE3 kickoff only shows a confirm dialog when you already choose the option in the kickoff menu. Someone from the opensuse team is working on kickoff in order to add all the features it already had in KDE3 so maybe it's on his to-do list."
    author: "WPosche"
  - subject: "Re: Amarok"
    date: 2008-03-20
    body: "\"Other start menu alternative also have this pretty annoying behaviour\"\n\nThe traditional start menu? It has only one menu entry for terminating the session, as in KDE 3 so it is not a problem that a window pops up asking whether you want to log out or shut down etc. The problem with Kickoff is that it has different menu entries for logout, shutdown etc., but it still asks one more time which do you want."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Amarok"
    date: 2008-03-20
    body: "I'm using openSUSE snapshots, and until recently the traditional start menu had exactly the same bannoying ehaviour as the kickoff menu!\n\nThat's like the habit that \"Switch sessions\" leads to a krunner window with the test \"SESSIONS\" preselected, which has again to be confirmed to start a new X-session. \n\nNo real bugs, but examples of that many minor small annoying things that get on your nerves when trying KDE4. Couldn't all this be easily fixed?"
    author: "anon"
  - subject: "Re: Amarok"
    date: 2008-03-21
    body: "For the kickoff thing\n\nGo to system settings --- advance --- session manager and uncheck confirm logout and offer shutdown options."
    author: "JackieBrown"
  - subject: "Re: Amarok"
    date: 2008-03-21
    body: "Any way to add \"hibernate\" in the kickoff menu ?\nNow, the only way to start hibernation is to use the confirmation dialog, and use the secondary choices on the \"shut down\" button. I think that is way too much convoluted !\n(and that almost never work, btw, although hibernation works when started manually with the s2disk command, or with guidance-power-manager) "
    author: "Aldoo"
  - subject: "Re: Amarok"
    date: 2008-03-24
    body: "Once again, BAD DEFAULT SETTINGS is the thing where KDE *truly* excels... This is the way it's always been and probably always will be :(\n\n\"Go to system settings --- advance --- session manager and uncheck confirm logout and offer shutdown options.\""
    author: "anonymous"
  - subject: "Kickoff"
    date: 2008-03-21
    body: "KDE 4.1 looks great, but Applications Tab in kickoff is really, really bad. As a suggestion, Applications Tab could use something like \"Amarok - Services\" screenshot in review. On the left, we would have what is today Application's first level menu (Development, Education, Games, etc...) On the right side, launch icons. Most menus are two levels, so this design could fit very well. If some menu has more levels, this could be rendered as a tree view or indented lines on the right panel."
    author: "Julio"
  - subject: "Re: Kickoff"
    date: 2008-03-21
    body: "That's an interesting idea, I think. Indeed it seems pointless having to open all those sub-levels manually."
    author: "Mark Kretschmann"
---
The makers of PolishLinux have made a nice <a href="http://polishlinux.org/kde/kde-41-visual-changelog-rev-783000/">preview of the state of KDE 4.1</a>.  "<em>You don't always see this in the official changelogs but the KDE 4 development is progressing in an extraordinary speed.</em>"  They also have <a href="http://polishlinux.org/kde/kde-4-tour-digikam-010/">a look at Digikam's KDE 4 port</a>, including its nifty new timeline and Google maps integration.


<!--break-->
