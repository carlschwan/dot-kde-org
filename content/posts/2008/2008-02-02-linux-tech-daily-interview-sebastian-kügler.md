---
title: "Linux Tech Daily: Interview with Sebastian K\u00fcgler"
date:    2008-02-02
authors:
  - "jriddell"
slug:    linux-tech-daily-interview-sebastian-kügler
comments:
  - subject: "KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "What\u00b4s goning on here? I just read that there will be no akonadi support in KDE 4.1 based on a decision at the kdepim meeting: \"Basically for 4.1.0 the plan is to deliver a stable kdepim. Not based on Akonadi, but a complete port. After the release of 4.1.0 we can start porting the application piece-by-piece to Akonadi and hopefully present that for 4.2.0\""
    author: "just me"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "Sounds like a case of the left hand not knowing what the right hand is doing."
    author: "Anon"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "Hehe. KDE is acting like a real company now.\nThe guy from the marketing working group is promising exciting new 4.1 features to the press.\nAnd the developers are blogging that the timeline is not realistic. And the feature has to postponed to 4.2\n\nFunny :-)"
    author: "KDErocker"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "Same for Decibel, it is listed on techbase as a 4.1 feature even if the project is totally unmaintained and has been so for many months already. It would be great if KDE could stop acting like a coorp and not make promises it can't keep."
    author: "anon"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "Looks like work on Decibel is *just* beginning to resume after a very long lapse:\n\nhttp://cia.vc/stats/project/kde/decibel\n\nSo things are looking a bit more promising, on the Decibel front - thankyou, Tobias :)"
    author: "Anon"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-03
    body: "It would be great if people would actually be realistic about things when making such comments.\n\nIt's not easy to keep all communication 100% up everywhere as long as we remain a distributed, open project. So being like a corp would actually *fix* that.\n\nIt's also pretty cool that developers are still in charge and make decisions like \"akonadi after 4.1\" if that's what makes sense. Again, not very corp like letting the engineers writing the code to set the pace of things so freely.\n\nInstead of trying to see all that as a bad thing, perhaps you might consider seeing it as the result of an open devel process that gets better (not to mention Free) technology to your doorstep.\n\nCheers ... "
    author: "Aaron Seigo"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-03
    body: "I find your faith in corporate communications disturbing.. :-)"
    author: "Chris Samuel"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-03
    body: "Dear Aaron,\n\nyou really do not want to experience corporation communication, I'm afraid. It will definitely not result in 100% communication, unless you mean just babbling without reasoning. Trust me, I have first-hand experience on such things in a large corporation.\n\nFor instance, marketing thinks about this and that, writing system requirements which try to even control which technology should be used instead of focusing on functionality. Marketing also requires conflicting and unresolvable goals to be realized simultaneously and has no problems in communicating this to 100%. Architecture and development then have to see how to make the best out of this. They are communicating back to marketing all the time but the goals set within marketing lead to completely ignoring feedback. Next, development ignores requirements, doing what they think could be great. No more communication then. And so on.\n\nI'm not saying that only one of the groups involved is wrong; there are mistakes in all fields and on all levels. But that is exactly what is corporation communication. And it would be sad if KDE would just end up with this kind of corporation communication. My hope is still that the incentives within the KDE development are differently enough from the usual corporation incentives that the development model including marketing, product management, architecture, development, and system test actually works better than in the ordinary companies."
    author: "Breco Pol"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-04
    body: ":-)\n\nIt's refreshing to see such great enthusiasm for the corporate model Aaron. But from my own experience in rather huge development groups, it's far worse than the communication within KDE.\n\ncu / ...."
    author: "Birger"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-04
    body: "Hey, nobody had any requests for me to update decibel, so I did not do it:-) Porting KCall to it was pretty straight forward, so I am mostly happy with what I got (at least for the few use cases we are supporting so far).\n\nI recently had some patches coming in (which got commited ASAP) and now that KDE4 integration is looming up ahead I really do need to fix the KDE integration issues that is still open. Apart from that I am waiting for feedback on what needs improvements and which features should get added (I am sure there still is much to do!). One of the design goals is to keep the decibel daemon (which is a network-accessible service after all) as simple and compact as possible. So just adding functionality that might be needed is not the way forward.\n\nMy other decibel related tasks are:\n\n* Updating telepathy-qt to the newest telepathy spec. Unfortunately that is basically a rewrite. We need to come up with a better way to keep telepathy-qt up to date!\n\n* Add profile support to tapioca-qt. Once that is done it should be visible in Decibel automatically.\n\n* Write some more KCM modules.\n\n* Blog about the kwallet support added by George Goldberg. I hate blogging.\n\n* Update the decibel web site. I hate that even more than blogging.\n\nPS: Help and patches are very welcome.\n\nBest Regards,\nTobias\n"
    author: "Tobias"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "The interview has been published and thus contacted before we, the KDE PIM developers here at our annual meeting Osnabr\u00fcck, decided to keep the applications at their current data access methods rather than to risk our users PIM data on possible last minute bugs because of difficulties of new code and old code working together.\n\nHowever, based on our today's decision, Akonadi will be released with 4.1 and there is a good chance that new applications will be released simultaniously or shortly after, e.g. Mailody.\n\nWe are also working on options to use Akonadi in the current applications by providing compatability implementations contact and calendar resources, which will allow applications to access Akonadi through the old APIs (KABC and KCal respectively)\n\nYou can consider it as a soft migration path instead of an \"import and never look back\" approach."
    author: "Kevin Krammer"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-02
    body: "As Adrian de Groot pointed out in one of his blog posts, the PIM team has a critical shortage of people working on it. Considering kdepim is broken in 4.0, I much prefer having a working kdepim first. "
    author: "Luca Beltrame"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-03
    body: "I was assuming that KDE-PIM in 4.1 would be Akonadi-based, but from Toma's blog, I learnt better. Unfortunately, after the interview was published. Sorry!"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: KDE-PIM meeting and akonadi"
    date: 2008-02-03
    body: "Don't be sorry, you couldn't do anything about it. And besides, it seems they just decided to include Akonadi after all, but without having all the apps ported already... Things change fast in the FOSS world, if you have to say sorry for every time they do, you'd have a fulltime job making excuses - would be a lot like working at Microsoft ;-)"
    author: "jospoortvliet"
  - subject: "Compositioning effects in Kwin?"
    date: 2008-02-03
    body: "What's the deal with compiz?\n\nWhen are the rest of Compiz fusion effects be ported over to Kwin?"
    author: "Steve M"
  - subject: "Re: Compositioning effects in Kwin?"
    date: 2008-02-03
    body: "No compiz effect was ever ported to kwin. It was/is not possible to use compiz plugins in kwin or port them to use them with kwin. Its all new implemented. Please read commit digest if you need more information about kwin and its effects."
    author: "kde addict"
---
Linux Tech Daily has <a href="http://www.linuxtechdaily.com/2008/02/kde-4-developers-an-interview-with-sebastian-kugler/">an interview with Sebastian Kügler</a>.  The e.V. board member talks about his work on the Marketing Working Group, what was exciting about the release event, the improved release process and what he is looking forward to in KDE 4.1.  On how you can help market KDE he says "<em>It might sound a bit scary,  representing KDE in your local LUG, but it&#8217;s really what KDE is about. Everybody comes from a local community, that is where our grassroots are.</em>"
<!--break-->
