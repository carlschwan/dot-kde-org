---
title: "Akademy 2008: Call for Presentations"
date:    2008-03-03
authors:
  - "cschumacher"
slug:    akademy-2008-call-presentations
comments:
  - subject: "Fruity!"
    date: 2008-03-05
    body: "As long as no kriek is involved, I'm all for it. The main research finding of FOSDEM was that Duchesse du Bourgogne is really really nice to drink, but doesn't resemble beer much (we need to expose an Austrian to this stuff)."
    author: "Adriaan de Groot"
  - subject: "Re: Fruity!"
    date: 2008-03-05
    body: "My Austrian, beer-loving soul shivers at the thought of it ... ;-)"
    author: "chris.w"
  - subject: "Re: Fruity!"
    date: 2008-03-05
    body: "Apparently Liefmans, the Kriek brewery has gone belly-up, just heard that last Friday when I was indeed trying to get a Duchesse de Bourgogne in Nijmegen, and even succeeded.\n\nYes, Akademy in Belgium gives that warm fuzzzzy feeling. :-)"
    author: "Sebastian Kuegler"
  - subject: "Re: Fruity!"
    date: 2008-03-05
    body: "There are other brewers who have their flavor of Kriek (which means: cherry) so you'll still be able to drink it, or other fruity beers if you like gnegne."
    author: "Terracotta"
  - subject: "Tracks"
    date: 2008-03-05
    body: "We hope to have some different kinds of tracks this year -- no necessarily in the main conference programme -- such as a Research track (what are all these studies of the KDE development process telling us? what does SQO-OSS do?) and a students track (university students who come in to KDE development through Kevin, mostly) and a SoC track.\n\nIf you do neat stuff with embedded devices, that's also a good topic for a special track.\n\nNo topic too small! No stone left unturned! Talk about GGZ. Talk about decibel. Talk about 7 simple steps to improving your APIDOX. Talk about giraffes, if need be (they are an interesting target audience for KDE because they can't use the mouse, which means we need to adjust that a little, and our default button order is wrong for giraffe culture, as supported by these user studies -- that would be a *great* talk)."
    author: "Adriaan de Groot"
---
As the new leaves of spring bud in the Low Countries, the organisation of <a href="http://akademy2008.kde.org">Akademy</a> is also growing. This will bear fruit in August as the worldwide KDE community gathers in Sint-Katelijne-Waver, Belgium at the De Nayer Institute to celebrate and consider the post-KDE 4.0 world.  Now that the KDE 4 technology platform is in place, this year's Akademy will focus on bringing the pillars of KDE to applications, research efforts around KDE, and work on non-traditional platforms for the desktop. Your work on KDE is interesting to us, so please submit a talk.  See the complete <a href="http://akademy2008.kde.org/conference/cfp.php">Call for Presentations</a> for more details. The important date is the deadline for submissions to the main conference tracks: May 1st.



<!--break-->
