---
title: "KDE and Amarok Present at the OpenExpo in Bern"
date:    2008-03-24
authors:
  - "aleisse"
slug:    kde-and-amarok-present-openexpo-bern
comments:
  - subject: "Amarok on Windows"
    date: 2008-03-24
    body: "I downloaded a build from the KDE Windows installer a while back, but I can't do anything without it crashing.  I've never even gotten it to play a single song.  That being said, Amarok is my favorite player of its kind.  Are there are newer Windows binaries?\n"
    author: "T. J. Brumfield"
  - subject: "Amarok"
    date: 2008-03-26
    body: "When does Amarok suppose to come out? I don't know how I'm gonna live without a audio player in KDE 4 anymore. :("
    author: "Jeremy"
  - subject: "Re: Amarok"
    date: 2008-03-26
    body: "Why not just use Amarok 1.x in KDE 4 while you're waiting?"
    author: "Paul Eggleton"
  - subject: "Re: Amarok"
    date: 2008-03-27
    body: "Everyone told me that, that was unsupported but I guess I can do that. ;D"
    author: "Jeremy"
---
On the 12th and 13th of March the 4th OpenExpo took place in Bern. /ch/open oranises the fair every 6 months alternating in Bern and Zurich. A wide variety of talks grouped into three different tracks accompanied the fair.  This year more than 30 OpenSource Projects took the opportunity to present themselves and their work to a wide audience. KDE and the Amarok team was among them. The booths attracted many people intersted in the improvements of KDE4 and Amarok2. Most were impressed by the amazing efforts the project took to push KDE into a glorious future.



<!--break-->
<div align="center"><a href="http://www.flickr.com/photos/troubalex/2347744196/" title="OpenExpo 2008 von troubalex bei Flickr" style="border: none;"><img src="http://static.kdenews.org/jr/openexpo-report.jpg" width="500" height="326" alt="OpenExpo 2008" /></a><br />Showing KDE to the Swiss</div>

<p>On the 13th Franz Keferb&ouml;ck gave a well received talk about development and structures of KDE's community and its organisational processes and Nikolaj Hald Nielsen talked about Amarok2 &ndash; in the intersection of Free Software and Free Culture in front of a large interested audience.</p>

<p>Many thanks to the people at the booths - Luca Gugelmann, Mario Fux, Franz Keferb&ouml;ck, Alexandra Leisse and Eckhart W&ouml;rner for KDE and Mark Kretschmann, Nikolaj Hald Nielsen and Myriam Schweingruber for Amarok - and to all those who helped organize the event beforehand as well as the /ch/open for their support in Bern.</p>

<p>Videos of both <a href="http://video.google.com/videoplay?docid=-5223232819178781095" title="google video">Nikolaj's talk on Amarok2</a> and <a href="http://video.google.com/videoplay?docid=-970233305311443939" title="google video">Franz' talk on the KDE community</a> are available online.</p>


