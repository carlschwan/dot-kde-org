---
title: "Interview with Krita developers"
date:    2008-08-30
authors:
  - "jriddell"
slug:    interview-krita-developers
comments:
  - subject: "so.."
    date: 2008-08-29
    body: "Krita is the foundation of a solidly integrated Photoshop killer?"
    author: "ethana2"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "It depends on what on \"Photoshop killer\" is for you. For a lot of people Krita might be good enough or become good enough. There is always someone who needs some feature that only Photoshop has like CMYK, Pantone etc.\n\nWhile it might be technically possible to become such a killer app, the Krita team has no patents, less manpower and less money than Adobe. Still they are creating a remarkable app. From the interview is seems to me like features only get added when a developer is interested in them, so Krita will take a different direction.\n\nIt would be interesting if a Hollywood studio would support Krita in the future like they did with cinepaint."
    author: "ac"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "> There is always someone who needs some feature that only Photoshop has like CMYK\n\nThis doesn't invalidate your general argument but I thought that Krita already has CMYK support and that this was one of its 'selling' points over the Gimp."
    author: "Robert Knight"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "Not just CMYK. Krita's color spaces provides infrastructure to add more color types easily. And I've used Krita so I know it supports CMYK colors.\n\nand yes, this is a selling point over The GIMP, though the fact that it isn't based on Gtk (GIMP Tool Kit, they won't leave it) is also pretty good.\n"
    author: "Michael \"colorful\" Howell"
  - subject: "Re: so.."
    date: 2008-09-08
    body: "CMY/CMYK is a phony issue.  Only professional pre-press shops need it.\n\nIf you are taking things to the local Kinko's or 1 Hour Photo shop to be printer, you should use RGB.  Some confusion occurs because the CMYK color space -- which I presume is the Adobe CMYK color space -- is a very different gamut than sRGB.  \n\nI suppose that the (Adobe) CMYK color space is a good approximation of ink on paper, but saving the file as CMYK is not relevant.\n\nHowever, for a laser color or Fuji photo printer, you should not use CMYK, you should use the RGB profile for the printer being used.  Laser color printers do use CMYK toner, but the driver uses RGB.  With photo printing, CMY (subtractive) and RGB (additive) are equivalent (if you print yourself, you probably use CMY but most commercial machines are RGB).\n\nMore important is the color spaces.  And, for most people, the important feature for color spaces is preview.  Yes, the best previews are with a calibrated monitor or a profile for your monitor, But, a preview that presumes that your monitor is sRGB is still a useful feature since monitors are supposed to be sRGB."
    author: "JRT"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "Well, we got most of the features (like 8-64 bits/channel, colorspaces (all of them, what's missing is easy enough to add), non-destructive effect masks, rich text, vectors, painterly things), but what we miss is polish, stability, performance and workflow things. Polish, stability and performance I intend to address through the beta cycle. Workflow -- things like work views for painting, photo editing and things like that -- that's what's on the table for 2.1. Valerie, a user, has made some great designs for that. We only have to find time to implement it. And yes, someone paying money to speed up development would be great. But chances of that are very slim, and, as Robin Rowe said in a very interesting Linux Format interview, hunting funding may reduce available time for development to the point where the application lags so much it's not interesting to funding parties any more.\n\nIn the long run, it will be interesting to see how applicaitons like gimp, photoshop and corel painter develop vis-a-vis newer applications like Lightroom and Aperture. I think Krita will turn more and more into an application to create images with, both from scratch and from existing images, and less and less an application to massage screenshots and snapshots for inclusion on web pages. Myself, I mostly use digikam to organize and prepare images (photos and scans) and Krita to do the rest. "
    author: "boudewijn rempt"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "\"We only have to find time to implement it. And yes, someone paying money to speed up development would be great.\"\n\nWell, this is kind of an empty gesture and doesn't really mean much coming from an anonymous Dot poster, but if I were a billionaire spaceman, I would be throwing money at KOffice (and KDE in general) - technologically, I believe you are going the right way and are a more promising prospect than any other competing Free office suite, and based on the efforts so far of a tiny band of part-timers, I bet the return on investment would be phenomenal :) "
    author: "Anon"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "> Well, this is kind of an empty gesture and doesn't really mean much coming from an anonymous Dot poster\n\nJust because he forgot to capitalize doesn't mean \"aboudewijn rempt\" is a pseudonym.\n"
    author: "Michael \"anonymous\" Howell"
  - subject: "Re: so.."
    date: 2008-08-30
    body: "I believe he was talking about himself."
    author: "zonk"
  - subject: "Re: so.."
    date: 2008-08-31
    body: "You don't have to be a billionaire to send them a few bucks. Just send them your beer-money for a week/month. That'll do. And it'll improve your health too. ;-)"
    author: "Oscar"
  - subject: "Re: so.."
    date: 2008-08-31
    body: "> Well, we got most of the features\n\nSo you mean Krita is almost on feature parity with Photoshop? And what's missing is easy to add? Why does nobody know about this? Krita should be on every designer desktops..."
    author: "Lisz"
  - subject: "Re: so.."
    date: 2008-08-31
    body: "No: what I said was that any colorspace that is missing is easy to add, and that what we were missing was \"polish, stability, performance and workflow thing\". Especially the latter is something that will take a lot of work to get right."
    author: "Boudewijn Rempt"
  - subject: "Re: so.."
    date: 2008-08-31
    body: "Hello Boudewijn,\n\nwhy not put out an effort to collect money via say Paypal. I am willing to spend 10 Euro, not much, but others may be too. With enough money, you can fund somebody qualified of your choice for full time one year of work.\n\nKDE in general could do that. Collect money on news and support sites until sufficient funding has been accumulated for one project, then turn to the next one and highlight it (Kword?).\n\nIf you allow people to publish a small one line statement along with a list of donators, it should work as it does for Wikipedia.\n\nMy understanding is that KDE e.V. already gives money from funds, right?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: so.."
    date: 2008-08-31
    body: "Someone mentioned to me that if you just add \"KOffice\" to the donation to KDE e.V. then the money will in fact go to KOffice.\n\nI hope that this is correct and that KOffice actually got the few $/\u0080 I sent their way."
    author: "Oscar"
  - subject: "Interesting"
    date: 2008-08-30
    body: "Very interesting interview with lots of good questions and answers. From reading the article and the krita mailing list (btw still called kimageshop,  http://lists.kde.org/?l=kde-kimageshop&r=1&w=2) it seems these guys have written a very nice foundation for the coming generations of Krita... rock on!"
    author: "MK"
  - subject: "Thanks for the interview"
    date: 2008-08-30
    body: "Great one. I specially look forward to see http://create.freedesktop.org/wiki/index.php/OpenRaster succeed to provide us a OASIS OpenDocument standard for raster graphics :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Thanks for the interview"
    date: 2008-08-31
    body: "OpenRaster isn't related to OASIS and OpenDocument."
    author: "Cyrille Berger"
  - subject: "Re: Thanks for the interview"
    date: 2008-08-31
    body: "ah, ok. So, freedesktop.org then. Thanks for that clarification."
    author: "Sebastian Sauer"
  - subject: "features with peaches"
    date: 2008-08-30
    body: "These will be ignored as not popular enough or some other stupid s### but what I would like to see added:\n\n* Automatic checksum hash generation with md5deep for any of the sums supported when you save the file\n\n* Automatic gpg signing when you save the file\n\n* The option to generate a hash AND gpg sign automatically, together, when saving\n\n* The ability to use stego"
    author: "YourName"
  - subject: "Re: features with peaches"
    date: 2008-08-30
    body: "\"These will be ignored as not popular enough or some other stupid s### but what I would like to see added\"\n\nI like how you completely sabotage your own feature request before you even get around to requesting the actual features."
    author: "Anon"
  - subject: "Re: features with peaches"
    date: 2008-08-30
    body: "How much is it worth to you? Attach some money and add it to cofundos.org and perhaps someone will write the features for you. Just because things are free doesn't mean they have to be gratis."
    author: "Oscar"
  - subject: "Re: features with peaches"
    date: 2008-08-31
    body: "Hello,\n\nI think the checksum and signing parts for saved user documents should be something put into KDE libraries save dialog. There certainly are use cases to run jobs every time you save something. \n\nIf it really matters to you, consider using python and the inotify module, and it would be really easy to script it outside of KDE.\n\nFor Steganographics you want a text associated, right?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Performance"
    date: 2008-09-01
    body: "Hopefully Krita gets huge performance leap because currently it is almost impossible to use to edit over 800x600px size images or even over 2.1Mpix photographs with smooth mouse movement. OpenGL does not help, actually makes worse.\n\nKrita has great potential but since Krita started, I have followed it's process and for me it seems that it just gets new features all the time but the work where it is designed, it does not fill it promises. What is very sad :-(\n\nThe GUI needs lots of polishing, it could be good to make different palettes for different usage situation, like for image drawing and photo editing. Because photographer does not need such tools as painter and vice versa.\n\nExample, it is \"stupid\" to allow painter to add to image a photograph metadata, like it would be a photograph and not image.\n\nBut if Krita has great plugin architecture what allows writing filters and other plugins easily, it is great thing for future. But polishing really is needed and lots :-) After that, we need lots of different filters and other plugins to make Krita rock!\n\n"
    author: "Fri13"
  - subject: "Computer Develop...."
    date: 2009-06-09
    body: "<a href=\"http://www.optussolutions.com.au \">Telecommunications </a> Optus Business Solutions: Optus Business Solutions provides a wide range of customized business plans and business services from business phone systems to business mobile plans.\r\n"
    author: "moss"
---
Alexandre Prokoudine has an <a href="http://prokoudine.info/blog/?p=86">interview with Krita developers</a> on his blog.  Taken at the Libre Graphics Meeting he talks to Boud and Cyrille about KDE's painting application.  When asked what are Krita's primary goals the answer is <em>"Krita is a very flexible foundation for all kinds of image processing. Weve got an unparalleled architecture to build raster graphics on and a really flexible system of plug-ins"</em>, which covers pretty much everything.


<!--break-->
