---
title: "KDE4Daily 4.2 Edition"
date:    2008-12-11
authors:
  - "SSJ"
slug:    kde4daily-42-edition
comments:
  - subject: "Virtualbox image?"
    date: 2008-12-10
    body: "Hello,\n\nthank you for making KDE4Daily 4.2 available. Is there a Virtualbox image available yet? You linked to one but I cannot find it in the \"Download\" section of your website.\n\nGreetings,\n\ngoran"
    author: "goran"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-10
    body: "Oops - good catch, that link was leftover from 4.1 (I've removed it now).\n\nNo VirtualBox image yet - if anyone wants to step up and make one, please take a look at the notes here:\n\nhttp://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Why_Qemu\n\nThanks!"
    author: "SSJ"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-10
    body: "I personally run trunk from source, because I know how to do it, but if someone provides VirtualBox images, I think I will end recommending this project even more. Qemu is great, sure, but VirtualBox has a really nice Qt4 interface, which helps a lot users who don't have experience with virtual machines."
    author: "Anon"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-11
    body: "You can install Kubuntu 8.10 and then add the Project Neon ppa source.  They have nightly build.  Standalone or in vbox."
    author: "Xebian"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-11
    body: "But with useful debug traces enabled?\nWhat I tried the other day didn't have them (without traces there's not much point in reporting bugs)"
    author: "AC"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-11
    body: "With the following instructions you should be able to convert the kde4daily image to VirtualBox:\n\nqemu-img convert kde4daily_4_2-qcow-final.img kde4daily.bin\nVBoxManage convertdd kde4daily.bin kde4daily.vdi\nVBoxManage modifyvdi kde4daily.vdi compact\n\nThis proces does eat a couple of gigs though..\n"
    author: "Mark Hannessen"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-11
    body: "Here you have http://linuxtracker.org/download.php?id=f89c884aa83cd20ab3c34b2b2c7da79ca62bdc06&f=kde4daily_4_2-qcow-final-vdi.torrent\n\nBut I think that Mark Hannessen's solution will be faster :)"
    author: "Patxi Rodrigo"
  - subject: "Re: Virtualbox image?"
    date: 2008-12-11
    body: "A good solution (not mine, was suggested by \"kamome\" int the comments of http://liquidat.wordpress.com/2007/11/23/howto-transform-a-qemu-image-to-a-virtualbox-image/) is:\n\nqemu-img convert -f qcow -O vmdk <qcow file> output.vmdk\n\nThis way you convert directly from the qcow to a vmware image, which virtualbox also can use, but you only have to uncompress the bz2 and run this, no extra steps (also the resulting file is smaller, but not by much)."
    author: "IAnjo"
  - subject: "Nice!"
    date: 2008-12-10
    body: "As always, a nice way to test out kde4. I maintain a regular trunk build on my hdd, but it's always a good way to confirm if what I'm seeing can be reproduced on an entirely different system.\n\nDownloading now, and gonna keep seeding this one for a while :)"
    author: "IAnjo"
  - subject: "Run with some of kwin's effects"
    date: 2008-12-11
    body: "I know it's cheesy to reply to myself, but I just remembered this might be interesting to kde4daily users:\n\nhttp://knuxblog.blogspot.com/2008/06/cool-visual-effects-on-linux-without-3d.html\n\nIn a nutshell, you can have kwin with composite (read: cool visual effects) if you use it with XRENDER backend (instead of Opengl).\nDon't forget to turn off shadows though, last time I tried them they didn't work at all."
    author: "IAnjo"
  - subject: "Re: Run with some of kwin's effects"
    date: 2008-12-12
    body: "What i find quite scary is that my virtual desktop does all the effects much faster then my real desktop. ( shadows work here as well )\n"
    author: "Mark Hannessen"
  - subject: "Re: Run with some of kwin's effects"
    date: 2008-12-15
    body: "thanks for the tip! "
    author: "mxttie"
  - subject: "excellent"
    date: 2008-12-10
    body: "I applaud all efforts to bring binary nightlies to end users easily.\n\nI may have to give this a whirl."
    author: "ethana2"
  - subject: "Qt 4.5"
    date: 2008-12-10
    body: "What I would love to see is Qt 4.5 snapshots compiled daily in the updates. On my slow machine I need around 30 hours to compile qt-snapshot + kdesupport + KDE + extragear + koffice + playground."
    author: "christoph"
  - subject: "Do you mean something like this?"
    date: 2008-12-11
    body: "http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_11.1+Qt45/\n\nRegularly updated."
    author: "Will Stephenson"
  - subject: "Re: Do you mean something like this?"
    date: 2008-12-11
    body: "I only found akadoni there, I guess the Qt packages are currently rebuild. And now go back and code knm4 :)"
    author: "christoph"
  - subject: "Re: Do you mean something like this?"
    date: 2008-12-11
    body: "i think you also need to add\n\nhttp://download.opensuse.org/repositories/KDE:/Qt45/\n\ndon't know sorry"
    author: ";)"
  - subject: "Re: Do you mean something like this?"
    date: 2008-12-11
    body: "I think he meant something like this:\n\nhttp://download.opensuse.org/repositories/KDE:/Qt45/openSUSE_11.1/"
    author: "Anon"
  - subject: "Seed!"
    date: 2008-12-11
    body: "Seed!!\n\ntorrent too slow... eager to try it!!!"
    author: "jorge"
  - subject: "Rapidshare links"
    date: 2008-12-11
    body: "As the torrent is slow, here are some rapidshare links. Rejoin with /usr/bin/join.\n\nhttp://rapidshare.com/files/172466215/kde4daily_4_2-qcow-final.img.bz2.aa\nhttp://rapidshare.com/files/172466219/kde4daily_4_2-qcow-final.img.bz2.ab\nhttp://rapidshare.com/files/172467208/kde4daily_4_2-qcow-final.img.bz2.ac\nhttp://rapidshare.com/files/172467210/kde4daily_4_2-qcow-final.img.bz2.ad\nhttp://rapidshare.com/files/172467879/kde4daily_4_2-qcow-final.img.bz2.ae\nhttp://rapidshare.com/files/172467880/kde4daily_4_2-qcow-final.img.bz2.af\nhttp://rapidshare.com/files/172468706/kde4daily_4_2-qcow-final.img.bz2.ag\nhttp://rapidshare.com/files/172468707/kde4daily_4_2-qcow-final.img.bz2.ah\n\nRegards"
    author: "Miguel"
  - subject: "Re: Rapidshare links"
    date: 2008-12-12
    body: "And what's wrong with good old 'cat'?\n\ncat kde4daily_4_2-qcow-final.img.bz2.a* > kde4daily_4_2-qcow-final.img.bz2\n"
    author: "Dario"
  - subject: "Re: Rapidshare links"
    date: 2008-12-12
    body: "Some people just aren't cat persons ;-)"
    author: "kakalto"
  - subject: "out of the box"
    date: 2008-12-12
    body: "since i've got several partitions and grub works ok, couldn't i try this out of Qemu, as a full distro? backing up would take some time, but i got it aplenty until march. how would i do this?"
    author: "bruno neyra"
  - subject: "Re: out of the box"
    date: 2008-12-12
    body: "I'm sure such a thing is possible in principle, but don't really know how.  Incidentally, all development and testing of KDE4Daily 4.2 has been carried out in a chroot this time around (Khrooty4Daily :)) which might be a better way of running it natively.  I've been meaning to release it, but don't know what the security implications are of providing a chroot where the root password is known on every install (KDE4Daily's password is always \"kde4daily\")."
    author: "Anon"
  - subject: "Re: out of the box"
    date: 2008-12-12
    body: "^^ I wrote that comment, BTW :)"
    author: "SSJ"
  - subject: "Already found the first three bugs."
    date: 2008-12-12
    body: "1. Its seems that the plasma widget in the top right corner is rendered behind all the other widgets.\n\nIf you place another widget close to the widget in the top right corner it will become impossible to use the top right corner widget for locking/unlocking because it will be rendered behind the widget that is close to it.\n\n2. It is possible to move widgets on the desktop in locked mode.\n\n3. For some reason the plasma add widget got the stuck on the left side of the panel for some reason and i can't get it back to normal anymore.\n\nBugs 1 and 3 should be clear from this image.\nhttp://www.nperfection.com/other/kde4daily.png\n"
    author: "Mark Hannessen"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "4. From the same screenshot:\nIcon Tray graphics corruption. ( The superkaramba and google gadget icons )\n"
    author: "Mark Hannessen"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-13
    body: "# Icon Tray Graphic Corruption\nhttp://bugs.kde.org/show_bug.cgi?id=177660\n"
    author: "Mark Hannessen"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "Great! If your KDE4Daily install is all up-to-date (I'm sure 3. was fixed recently), please file reports.  Can you check whether 2. occurs for all widgets, or just for Google Gadgets?"
    author: "SSJ"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "I gave it some more testing,\nAnd it seems to only happen to google and superkaramba widgets.\nBut not to the plasma applets. ( it works fine with the new xeyes for example. )\n\nHope that helps"
    author: "Mark Hannessen"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "Cool (and good to hear #3 was fixed) - can you file a bug report and post the bugID here? That would be most helpful :)"
    author: "SSJ"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "There ya go.\n\n# Google/Superkaramba widget overlap:\nhttp://bugs.kde.org/show_bug.cgi?id=177595\n\n# Google/Superkaramba widget locking:\nhttp://bugs.kde.org/show_bug.cgi?id=177596\n"
    author: "Mark Hannessen"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "Much obliged :)"
    author: "SSJ"
  - subject: "Re: Already found the first three bugs."
    date: 2008-12-12
    body: "Bug 3 was indeed fixed after the update.\n\nThanks! :D"
    author: "Mark Hannessen"
  - subject: "slowly closing the gap between code and marketing"
    date: 2008-12-12
    body: "Up to now KDE4 has disappointed me again and again on each SVN updates; i could just see no progress on the issues relevant to my usage patterns. Well, there's still a lot of stuff to do (and a lot of crashes to fix, apparently, at least on QT4.5) but after not tasting KDE svn since more than a month, this was the first time i was pleasantly surprised. It looks much more professional, little inconsistencies left, some defaults sanitized. I still see too many vistaisms and leopardishments and miss a lot of KDE3's advanced power features, but at least i have started seeing hope.\nA tremendous amount of work has gone into this and it finally is starting to show. My guess is that indeed sometime in 2009 all the features that have been overly advertised for so long now actually do work with the amount of stability needed to convince people that KDE is indeed a desktop environment you can trust your data to. And it looks as it'll be fun, too! Tough job still."
    author: "eMPee584"
  - subject: "December 5th -> January 5th for tagin' KDE 4.1.4?"
    date: 2008-12-13
    body: "Anyone want to elaborate on the need for 4 weeks of a push back for a .x.x release?\n\nSome of us actually follow the Release schedule."
    author: "Marc J. Driftmeyer"
  - subject: "Black screen"
    date: 2008-12-13
    body: "I never see kde4.2. I can log in and after the splash-screen disappears I only see a black screen. No desktop. Nothing. I do however see an arrow that I can move around so it's not dead, just not working.\nThis is qemu on windows.\n\nAny ideas?"
    author: "Oscar"
  - subject: "Re: Black screen"
    date: 2008-12-13
    body: "Is there any CPU load during this time? If not, I'm baffled, I'm afraid - I tested on Windows yesterday (in Qemu, with kernel module) and it was fine.  The only thing I can suggest is to check the MD5 sum of your download :/"
    author: "SSJ"
  - subject: "Re: Black screen"
    date: 2008-12-13
    body: "I did install som accelerator (that I thought was optional) and now it's working. Dead slow but working. It looks wonderful. =)"
    author: "Oscar"
  - subject: "Re: Black screen"
    date: 2008-12-13
    body: "It should indeed be optional, so that's a bit strange - anyway, glad you've got it [kind of!] working!"
    author: "SSJ"
  - subject: "awesome!"
    date: 2008-12-15
    body: "I'm d/ling the image now ... 4.2 is the first kde release that's made any sense to me (and I've been using kde since the 1.x days).\n\nAt any rate, does this image have all the developer tools as well?  I can see it as a phenomenal resource to not just run and log bugs, but for those of us that can code well but don't necessarily want to spend the time to compile things on the bleeding edge we can debug things live as they crash.\n\nAny thoughts on this?\n\nThanks again!!  Great job!"
    author: "Jim"
  - subject: "Re: awesome!"
    date: 2008-12-15
    body: "It doesn't come with developer tools or dependencies, but the chroot used to create KDE4Daily does.  See here for a little more info; I'm still not sure (if I release it) how many warnings I should put on it :)\n\nhttp://dot.kde.org/1228930911/1229039665/1229075124/"
    author: "SSJ"
  - subject: "Plasmoids - Ruby"
    date: 2008-12-16
    body: "Actually, that i am able to write plasmoids in ruby was one reason why I was looking forward to kde.\n\nHowever, and please - I hope noone misunderstands this - for me the biggest problem was the new build system. Cmake has nice colours but the build system really has a few problems IMHO. The error messages are often somewhat cryptic and inconcise. The GNU Autoconfigure system was smoother and I am running a self compiled kde 3.5.10 here.\n\nI hope that the cmake build system of kde can be approved to deliver the same quality as the kde 3.5.x based one does offer to people who do not want to stick to distributions. "
    author: "mark"
  - subject: "Re: Plasmoids - Ruby"
    date: 2008-12-17
    body: "Your post is somewhat short on details..."
    author: "Paul Eggleton"
  - subject: "Re: Plasmoids - Ruby"
    date: 2008-12-17
    body: "You could almost call it \"cryptic and inconcise\" :)"
    author: "Anon"
---
KDE4Daily is an attempt to lower the barrier to entry for people who would like to test KDE trunk in the run-up to 4.2, consisting of a <a href="http://fabrice.bellard.free.fr/qemu/">Qemu</a> VM image containing a Kubuntu 8.10 base and a <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Included_Modules">comprehensive</a> set of a self-<a href="http://etotheipiplusone.com/kde4daily/buildprogress.html"/>compiled</A> KDE4 modules from KDE trunk (all at r888587, initially), along with an <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Updating_KDE4Daily">updater system</a> inside the VM itself.  After an unusually <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Bugs_In_KDE4Daily">rocky</a> round of development, it is time to (finally!) announce the release of <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Bugs_In_KDE4Daily">KDE4Daily</a> 4.2!


<!--break-->
<p>The updater downloads and installs binary updates provided by me to the full set of modules: these updates tend to be roughly 20-50MB each (although they will occasionally be larger), take a few minutes to apply, and will hopefully be pushed out daily - hence the name.</p>

<p>Because Qemu is distro-agnostic, you do not need to worry about distros or libraries or dependencies or suchlike; in fact, you can even test out KDE 4 while running Windows! The downside is that eye-candy such as KWin's new Composite-based effects will not be testable as Qemu does not support hardware graphics acceleration, and everything will generally feel a lot more sluggish than is the case with a native install.</p>

<p>KDE4Daily comes with its own backtrace-generation system so hopefully devs can be assured of having useful backtraces in any crash bugs you file, courtesy of DrKonqui.  Do note that this system is still <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#Slow_Backtrace">rather slow and resource-intensive</a>, so if you do somehow manage to crash a KDE app, please be patient while the valuable backtrace is created!</p>

<p>This time around I have included a chunk of the kdebindings module, so you should have everything you need to start writing Plasmoids (or other kinds of KDE apps) in Ruby and Python.</p>

<p>An extensive <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html#FAQ">FAQ</A> is provided at the KDE4Daily homepage, above; please feel free to ask any further questions in the Dot comments section!  Hope you enjoy it!</p>



