---
title: "Amarok Insider - Issue 13 Released"
date:    2008-11-06
authors:
  - "Ljubomir"
slug:    amarok-insider-issue-13-released
comments:
  - subject: "Broken Link"
    date: 2008-11-06
    body: "here is the correct one:\n\nhttp://amarok.kde.org/en/node/565"
    author: "Dexter"
  - subject: "Re: Broken Link"
    date: 2008-11-06
    body: "What's so broken about it?\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Broken Link"
    date: 2008-11-06
    body: "Well, the link works perfectly but you arrive to a page not found at amarok's site, which I suppose wasn't the intention :)"
    author: "Dexter"
  - subject: "Re: Broken Link"
    date: 2008-11-06
    body: "It takes me straight to the report.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Broken Link"
    date: 2008-11-07
    body: "funny, with konqueror it works, but with firefox (3.0.3/opensuse) it doesn't... at least on my system."
    author: "Dexter"
  - subject: "Re: Broken Link"
    date: 2008-11-07
    body: "Same problem here (3.5.10). Works with Konqueror, but not with Firefox 3 (Kubuntu 8.04)."
    author: "stevie"
  - subject: "Re: Broken Link"
    date: 2008-11-07
    body: "Well works here on Firefox2/windows"
    author: "shiny"
  - subject: "Re: Broken Link"
    date: 2008-11-12
    body: "The link does not work with Internet Explorer 7, Opera 9.62 or Seamonkey 1.1.12 under Windows.\nIt ONLY works with Konqueror 4.1.1.\n\nThis means the link _IS_ broken.\n\nTheo"
    author: "Theobald Gl\u00fcck"
  - subject: "Great News! Just needs some polishing to get there"
    date: 2008-11-06
    body: "Good to know that amarok 2.0 is almost there. So far I really think you guys made a great peace of software. But I think the context view needs some polishing. I have trunk version installed and sometimes the context view behaves very strange... some positioning troubles with the apples (for instance, when you maximize and restore, sometimes widgets are a little more on the left, or right). There's some problems with the zooming interface too... sometimes I get all the pages side by side (and arrows don't work well) and sometimes there are 2 pages on the upper side and the other 2 bellow (and arrows work well). Besides this little problems (that I know will be solved until the release) amarok 2.0 is already  the best music player in my opinion.. keep on going guys.. and sorry for my English.. learning...\n\nPS.: I got addicted to all those internet/script services..."
    author: "SVG Crazy"
---
<a href="http://amarok.kde.org/Insider/Issue_13">Issue 13 of Amarok Insider</a>, the official <a href="http://amarok.kde.org/">Amarok</a> newsletter is out. It discusses the evolution of Amarok's interface, reveals the release plans, covers some of the biggest features of the upcoming version 2.0, and much more. Download links for Windows and OS X versions of the Amarok 2.0 beta are included.



<!--break-->
