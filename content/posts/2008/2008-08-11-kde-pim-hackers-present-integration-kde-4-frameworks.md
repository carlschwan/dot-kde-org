---
title: "KDE-PIM Hackers Present Integration of KDE 4 Frameworks"
date:    2008-08-11
authors:
  - "skuegler"
slug:    kde-pim-hackers-present-integration-kde-4-frameworks
comments:
  - subject: "What about MS Exchange support in Kontact"
    date: 2008-08-11
    body: "Does anyone know, when the MS Exchange support will be available in Kontact?"
    author: "Andy"
  - subject: "Re: What about MS Exchange support in Kontact"
    date: 2008-08-11
    body: "The Exchange support in Akonadi is based on the work of the OpenChange project. Both them and us (the Akonadi team) have a good chunk of work left before it will become generally usable. Kontact has not been ported to Akonadi yet, so it'll be a while still before users can really get their hands on this. Hold breath at your discretion. :)"
    author: "Till Adam"
  - subject: "Re: What about MS Exchange support in Kontact"
    date: 2008-08-11
    body: "Fedora seems to be targeting this for kdepim (and evolution) for F10 which I think is due late October: http://fedoraproject.org/wiki/Features/OpenChange although like many things in Fedora it might be a bit on the experimental side when it first pops up (that is meant to be a compliment, it provides good testing, rather than a criticism at Fedora)"
    author: "Simon"
  - subject: "Re: What about MS Exchange support in Kontact"
    date: 2008-08-16
    body: "While we're trying to have OpenChange in the repository in Fedora 10 and we'll see if we can build Akonadi and kdepim against it (we're going to ship 4.1 of course, not trunk; 4.1 checks for libmapi already, but the code might not even compile), that doesn't mean you'll actually be able to use it in Kontact at F10 release time. You may have some luck with Mailody (which is already using Akonadi), but that isn't currently in Fedora (and again, this is all contingent on Akonadi's libmapi support). We'll definitely consider pushing 4.2 as an update (though it will depend on how smooth the Akonadi migration is), and there's also kde-redhat unstable which will almost certainly provide 4.2 snapshots, and 4.2 once it's out if we have to decide against pushing it (at least kdepim 4.2) as an official update (but I hope we will be able to push the update).\n\nWe'll probably have to discuss whether it makes sense to advertise this feature, as it won't be usable for end users when F10 releases (unless something happens on the Evolution front, but I don't think they'll be able to get libmapi integrated any quicker than kdepim)."
    author: "Kevin Kofler"
  - subject: "Re: What about MS Exchange support in Kontact"
    date: 2008-08-11
    body: "I'm hoping for this too. The sad state of affairs is that unless you can connect to MS-Exchange you have no chance in the \"Enterprise\" market. Atleast not in my country. \nI'd love to use Kontact on my new job, but if it can't speak MAPI (or whatever it's called) then I'm afraid it's a no-go.\nDamn Microsoft for being so annoying and fearing competition..."
    author: "Oscar"
  - subject: "Re: What about MS Exchange support in Kontact"
    date: 2008-08-12
    body: "I have to wholeheartedly agree with Oscar here. Unfortunately MS Exchange has the lions share of Small/Medium Businesses and unless the kde-pim people target this then they'll always be on the outer.\n\nOn the other hand, if this was done properly then there would be a huge case for moving away from Outlook. Since Office is MS's bread and butter, this would be a big blow if it worked well."
    author: "Socceroos"
  - subject: "Flow Chart"
    date: 2008-08-11
    body: "Wow, the actual speed of progress is really impressive!\nIt seems, that the long work on the frameworks now yield fruit...\n\nKopete, Decibel, Akonadi, Nepumuk, Strigi, Kmail, ...\nIs there some kind of flow chart available, which illustrates how all this components interact?\n\nRegards,\nThomas"
    author: "Thomas"
  - subject: "Re: Flow Chart"
    date: 2008-08-11
    body: "As long as KDE 3.x is still deployed on most KDE Users machine, I really dont feel the \"innovation\" of KDE 4. They will have to convince people that KDE 4 is in fact a step forward from KDE 3."
    author: "tomas"
  - subject: "Re: Flow Chart"
    date: 2008-08-11
    body: "Er... So you feel you only feel the innovation in KDE4 if lots of people use it, not when you yourself give it a try and start to code something cool and cute for KDE4? That sounds very... weird, I think."
    author: "Boudewijn Rempt"
  - subject: "Re: Flow Chart"
    date: 2008-08-12
    body: "i think what he was saying is that until he uses it, he can't really appreciate the innovation in KDE 4, which is true.\n\nthe part he's missing, which makes it sound weird, is that the innovation that is going on right now is exactly what is motivating people to try KDE 4 .. at which point they start getting it. so the innovation is what brings people over in the first place.\n\nreally, his comment is just more KDE 3 vs 4 pot-stirring, something that i don't think is overly useful to pay attention to. KDE 3 exists for those who want it and is great for the transitionary period. reality is that people are moving to KDE 4 now in larger and larger numbers, i'm even starting to hear of smaller production deployments moving to 4.1 ... it's a natural progression, and people ought to be a lot less defensive about whether they use KDE 3 or 4. use what you want, the cool stuff is happening now in KDE 4 and in the next few years pretty much everyone will be using 4.\n\nand yeah, the akonadi integration with other pillars is just astounding."
    author: "Aaron Seigo"
  - subject: "Sync?"
    date: 2008-08-11
    body: "Something that would be a very nice feature for me would be to sync calendars and other things with web-based services. Perhaps working with drupal to sync kdepim to something within their framework, perhaps sync it with google-calendar? \n"
    author: "Oscar"
  - subject: "Re: Sync?"
    date: 2008-08-12
    body: "i believe the pim people are looking at OpenSync"
    author: "Aaron Seigo"
  - subject: "groupware plugins from KDE3"
    date: 2008-08-11
    body: "Since a couple of people have been asking me about this, it's obvious I didn't get this across correctly.\n\nThis is not about re-using KDE3 plugins in the sense of using KDE3 code, all of the available plugins are Qt4/KDE4 ports.\n\nThe migration facilities I am working on are using KDE4 versions of APIs we developed during KDE3 but any code involved is KDE4."
    author: "Kevin Krammer"
  - subject: "great but will we have simple usability things.."
    date: 2008-08-12
    body: "... like search in filters configuration? Try finding some filter when you have 200 of them. Having to browse through all of them just to notice at the end that you somehow missed the rule you was looking for... and start over."
    author: "x"
  - subject: "Kontact 3.x-4.x with CRM support?"
    date: 2008-08-13
    body: "Any chance that Kontact will have greater support for various CRM solutions ( sugar, vtiger etc)? That will also be a pre-requisite to succeed in the business segment and not just MS-Exchange."
    author: "nindnarne"
  - subject: "Re: Kontact 3.x-4.x with CRM support?"
    date: 2008-08-13
    body: "Alfresco would be nice."
    author: "T. J. Brumfield"
---
In the final presentation of the talk days at KDE's yearly world summit, <a href="http://akademy2008.kde.org/">Akademy 2008</a>, the KDE-PIM hackers surprised the KDE community with a couple of announcements, covering nearly all aspects of PIM-related data handling. After demonstrating the Kontact suite on Windows and Mac OS during this year's LinuxTag, the KDE-PIM team continues to raise the bar for competitors on the enterprise desktop. Read on for more details.
<!--break-->
<p>The Plasmobiff <a href="http://plasma.kde.org/">Plasma</a> applet displays PIM information, making it trivial to show e-mails on your desktop. Bertjan Broeksema talked about how KPilot aims to bring Palm Pilot data into Akonadi and back in KDE 4.2.</p>

<p>In another Summer of Code project, George Goldberg has targeted merging Kopete with Decibel. Kopete will use KDE 4's realtime communications framework Decibel, with Akonadi to store Instant Messaging data, NEPOMUK and Strigi to index, search, and make more sense of your communications. Decibel represents individual people, and merges data from your address book, instant messaging, e-mail, and virtually every bit of data you have on your computer. It allows applications to show the data in the comprehensive concept of a "person".</p>

<p>Kevin Krammer talked about re-using existing groupware plugins from KDE 3 and hooking them up with Akonadi. The immediate benefit will be that the groupware servers (or more general PIM resources) that were supported in KDE 3 will also be available for KDE 4 applications with very few changes. Another benefit is the smooth migration path for your PIM data and applications when you decide to switch to Akonadi. Note that the features and additions mentioned here are not part of KDE's releases yet, but will be in the future.</p>