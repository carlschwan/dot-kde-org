---
title: "KDE Commit-Digest for 13th July 2008"
date:    2008-08-09
authors:
  - "dallen"
slug:    kde-commit-digest-13th-july-2008
comments:
  - subject: "Go danny!"
    date: 2008-08-09
    body: "Thanks! Go Danny!"
    author: "IAnjo"
  - subject: "Danny"
    date: 2008-08-09
    body: "Did he clone himself or how can he work this fast?\nAmazing work!"
    author: "JJ"
  - subject: "Re: Danny"
    date: 2008-08-09
    body: "He's at akademy right now.  I'd almost expect a digest every 6 hours until he collapses :P\n\nI'm not around at Akademy this year to buy you a beer, Danny.  Hopefully you'll find no shortage of beer toting fans to keep you motivated :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Danny"
    date: 2008-08-09
    body: "\"He's at akademy right now. I'd almost expect a digest every 6 hours until he collapses :P\"\n\nI hope he doesn't give himself in-Digest-ion! Boom, boom!"
    author: "SSJ"
  - subject: "Re: Danny"
    date: 2008-08-09
    body: "I'd say that it's more likely that he cloned himself than worked this fast ;).\n\nEither one would be an incredible feat.\n"
    author: "Michael \"haha\" Howell"
  - subject: "Re: Danny"
    date: 2008-08-09
    body: "Wow... first day on Akademy, and dot.kde.org opens with a commit digist. Unbelievable, great work! :)"
    author: "Diederik van der Boor"
  - subject: "Morge bugs closed than opened"
    date: 2008-08-09
    body: "In the last digests I liked to see that more bugs are closed than new bugs were opened. A couple of months ago it was different.\nThanks to the numerous bug fixining KDE developers. Many users thank you quietly for every disapearing little bug."
    author: "mathletic"
  - subject: "Koffice2 tagged?"
    date: 2008-08-09
    body: "\nDoes this mean that Koffice2 Alpha 9 is now released or it is last alpha and beta is next one?"
    author: "Thomas.S"
  - subject: "Re: Koffice2 tagged?"
    date: 2008-08-09
    body: "KOffice 2.0 alpha 9 has been released. There will be at least one other alpha release; we hope to be able to start releasing betas after that, until we feel confident we've reached release candidate status."
    author: "Boudewijn Rempt"
  - subject: "Mozilla QT port"
    date: 2008-08-09
    body: "I was curious if anyone has seen this.\n\nhttp://browser.garage.maemo.org/news/10/\n\nI intend to try and build it from source this weekend and give it a run.  I also intend to benchmark it against GTK builds."
    author: "T. J. Brumfield"
  - subject: "Nice new \"olympic\" theme"
    date: 2008-08-09
    body: "I know this is not directly related to the Commit-Digest, but hey, who cares...\nAnyway: There is a nice Red Flag-Linux olympic theme, shows well what can be done with KDE4\n\nhttp://www.linuxine.com/2008/08/olympic-concept-kde-41-theme-preview-gorgeous.html\n"
    author: "Markus"
  - subject: "Re: Nice new \"olympic\" theme"
    date: 2008-08-09
    body: "Not to shoot down your plane...\n\nbut i think about everything in this theme could have been done with the kde3 series too..\n\nTo be honest.. I don't like the theme that much either.. but that could be taste.\n\nI do believe kde4 has the power to create looking custom desktops.\nBut i think you will have to look for it more in applet space.\n\nsomething like a combination of plasma applets ( like calculator, pim, amarok, etc, funky bar, clock ) all \"merged\" with a background that joins everything together.\n\nnow THAT would be cool. :p\n"
    author: "Mark Hannessen"
  - subject: "Re: Nice new \"olympic\" theme"
    date: 2008-08-10
    body: "I can't remember any themes in KDE 3 going through the entire window, buttons in kicker going outside the kicker space, KDE 3 didn't have the SVG awesomeness that this has.  I think you're being slightly nostalgic there."
    author: "m"
  - subject: "Re: Nice new \"olympic\" theme"
    date: 2008-08-10
    body: "Woah. That's awesome!"
    author: "logixoul"
  - subject: "Re: Nice new \"olympic\" theme"
    date: 2008-08-11
    body: "I like it. It really looks great. The only problem is that it seems to be a mockup. The picture has a corner of a window without the rest of the window, and 2-3 windows are in focus simultaneously. I guess it will be a while before this theme actually becomes available."
    author: "yman"
  - subject: "any screenshot of dekoroom?"
    date: 2008-08-09
    body: "any screenshot of dekoroom?"
    author: "Glok"
  - subject: "Previewer"
    date: 2008-08-10
    body: "I'm not sure if I understand it correctly, but is Previewer a kind of OSX QuickShow for KDE ?\n\nIf the answer is yes, then that would be just great. Quick Show is an awesome tool, and since OSX already copied the Virtual Desktop, putting it into KDE is only fair game :)"
    author: "chimai"
  - subject: "Re: Previewer"
    date: 2008-08-10
    body: "KDE didn't invent Virtual Desktops, so if you extended the QuickView to every Motif/X-Windows DE then you'd have it."
    author: "Marc Driftmeyer"
  - subject: "Re: Previewer"
    date: 2008-08-10
    body: "Previewer is mostly just a shell around KParts, just like how Konqueror is.\n\nI've tried some googling but completely failed to find any info on QuickShow so can't really say how similar they are."
    author: "Kit"
  - subject: "Re: Previewer"
    date: 2008-08-11
    body: "Ooops, my bad, that's because it's named Quick Look ( http://en.wikipedia.org/wiki/Quick_Look ) !\n\nI was pretty close though :p"
    author: "chimai"
---
In <a href="http://commit-digest.org/issues/2008-07-13/">this week's KDE Commit-Digest</a>: Work on themed buttons and a TabBar (with animations) in <a href="http://plasma.kde.org/">Plasma</a>, initial work on a "grouping taskbar", and a "server hotlink" Plasmoid imported. Many KDE games get new sound effects. The start of a graphical user interface for the <a href="http://stepcore.sourceforge.net/">Step</a>Game project. Basic Git, Bazaar, and Mercurial version control system support in KDevPlatform (the basis of <a href="http://www.kdevelop.org/">KDevelop</a>4), with <a href="http://kross.dipe.org/">Kross</a> scripting interfaces. A new reflection architecture in the Kross Falcon bindings. A new "components and libraries" information dialog in <a href="http://www.digikam.org/">Digikam</a>. Minimal proxy support in the HTTP KIOSlave. D-Bus actions for invoking the Klipper context menu. Xinerama support in KSplash. Native "Open With..." dialogs for the Windows platform. Beginnings of a "bias editor" UI, and a rating widget added to the current track applet in <a href="http://amarok.kde.org/">Amarok</a> 2. Added support for saving and removing profiles in the <a href="http://koffice.kde.org/karbon/">Karbon</a> Calligraphy tool. First working KSpread/ODS export for <a href="http://www.kexi-project.org/">Kexi</a> reports. An experimental "cylinder" effect for switching desktops in KWin-Composite. Beginning of a KControl module to configure standard action shortcuts. The start of the PowerDevil (power management) KControl module, with D-Bus support. A new game, "Bomber", is imported into KDE SVN. Initial import of Dekoroom, a "new home designer" (with Ogre + Blender support). The <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> filewatch service moves into kdebase. Initial commits of Kommodity/GIO (Qt/C++ bindings for GIO/GVFS). Results of the "Fusion of Web Services with local services" diploma thesis, making the D-Bus web service proxy widely usable. <a href="http://koffice.org/">KOffice</a> 1.9.95 (Alpha 9) is tagged for release. KDE SVN is branched to prepare for the release of KDE 4.1, with <a href="http://dot.kde.org/1216132899/">KDE 4.1 Release Candidate 1 released</a>. <a href="http://commit-digest.org/issues/2008-07-13/">Read the rest of the Digest here</a>.

<!--break-->
