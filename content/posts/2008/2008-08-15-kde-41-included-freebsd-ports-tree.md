---
title: "KDE 4.1 Included in FreeBSD Ports Tree"
date:    2008-08-15
authors:
  - "rklop"
slug:    kde-41-included-freebsd-ports-tree
comments:
  - subject: "KDE on all major platforms"
    date: 2008-08-15
    body: "It is very exciting seeing KDE reach all these different platforms.\n\nI'm curious how well KDE 4 performs on BSD compared to Linux.  Functionally, is there any difference in how they operate?  How does the differing kernel affect things like Solid and Phonon?"
    author: "T. J. Brumfield"
  - subject: "Re: KDE on all major platforms"
    date: 2008-08-15
    body: "Hi, KDE 3 and before also run on FreeBSD. It performs very well. As BSD also uses the same X-windows (x.org) and the filesystems and networking is very much the same (from an application point of view). Solid and phonon are ported, but I can't tell you how hard it was to make that working."
    author: "Ronald Klop"
  - subject: "Re: KDE on all major platforms"
    date: 2008-08-15
    body: "There was quite a bit of work since January seeking out and stomping all the \"Linuxisms\". But it all seems to be working now. People are having some problems, but they seem to be mostly configuration and dependency issues, as well as the ubiquitous video driver weirdness.\n\nPhonon works great. Xine tends to be flaky on both my Linux and FreeBSD systems, so I enable the gstreamer backend. FreeBSD already has good Hal support, so no problems with Solid.\n\np.s. Last week at LinuxWorld Expo in San Francisco, the only desktop I saw running KDE4 was at the FreeBSD booth (on a PC-BSD system)."
    author: "David Johnson"
  - subject: "Spreading indeed..."
    date: 2008-08-15
    body: "Well, KDE 4.1 is in slackware-current now.\nLooks like the universal move to KDE 4.X is started. :-)"
    author: "Richard Van Den Boom"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-15
    body: "Pah,\n\nI tell you! I am just updating Debian Testing and it seems to bring in kdebase-runtime-bin-kde4, phonon, etc. and that is earth shattering. \n\nIt's not Debian \"Experimental\", it's Debian Testing even in as in \"will be Stable\" frozen form.\n\nRender me surprised. :-)\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-15
    body: "Holy monkey, that's unreal. Didn't they just include 3.0 (three point zero) some months ago? :)"
    author: "fhd"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-15
    body: "The funny part is that gentoo won't have it in the near future. From a distro that has been prized to having all recent packages, its down to just another lame project that releases later than Debian. See https://bugs.gentoo.org/show_bug.cgi?id=234773 for more details."
    author: "Anon"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-15
    body: "Indeed, on gentoo for now one must use the kdesvn-portage overlay. And having to unmask some packages too, a bit messy.\nWhile we are at it, i notice that video playback expecially on vlc slows down a lot when in kde4 compared to kde3. Even disabling kwin's new features. At least on my laptop wich has an intel card, i965 chipset.\n"
    author: "AC"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-22
    body: "Gentoo... oh the sorrow...\n\nFrom bleeding edge to bleeding dredge...\n\nMy Heart mourns this sad, sad time...\n\n  "
    author: "Avram"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-15
    body: "Offering KDE4 will be universal, making it the default is another story. A few distros made it the default (and Arch even removed KDE3), but I don't think most folks will be able to abandon KDE3 until 4.2. KDE3 also works on any video card."
    author: "David Johnson"
  - subject: "Re: Spreading indeed..."
    date: 2008-08-16
    body: "Fedora removed KDE 3 almost 3 months before Arch did."
    author: "Kevin Kofler"
  - subject: "Archlinux"
    date: 2008-08-15
    body: "Archlinux has completely moved to KDE 4.1 the day it was released.  You can't even officially find KDE 3.x packages anymore...\n\n:]"
    author: "Pieter"
  - subject: "Re: Archlinux"
    date: 2008-08-15
    body: "What about the KDEMod repositories?"
    author: "T. J. Brumfield"
  - subject: "Re: Archlinux"
    date: 2008-08-16
    body: "Those aren't official repositories, and you can't directly upgrade to KDEMod from KDE. You have to remove one and then install the other. Or you can edit some configuration files to bypass KDE during updates.\n\nThis whole episode was just a wee bit too radical and unstable for my tastes. I may have blinked, but I swear that KDE3 was pulled before the official 4.1 announcement even hit their smtp server."
    author: "David Johnson"
  - subject: "Re: Archlinux"
    date: 2008-08-16
    body: "This gets me how everyone insisted that KDE 3 would still very much be around and be supported for years to come, and yet Fedora already yanked it, Arch yanked it, and I'm hearing that both openSUSE and Kubuntu intend to yank it with their next releases.\n\nIs it so terrible to provide packages for both for a while?"
    author: "T. J. Brumfield"
  - subject: "Re: Archlinux"
    date: 2008-08-16
    body: "Yes, it's terrible. As we say on: https://fedoraproject.org/wiki/SIGs/KDE/KDE4FAQ\n\n\"True, while theoretically possible to do, it involves what we consider pervasive hacks including (at least) violating the FHS and/or installing into separate prefixes/roots, and additional hacks to make both KDE3/KDE4 environments not stomp on each other. None of these things we would willingly want to even try to fix and support, nor to inflict upon our userbase. \nHave we mentioned that upstream doesn't support it? :)\"\n\nOne of those \"additional hacks\" is the use of separate .kde and .kde4 configuration directories, which breaks upgrade paths in horrible ways: there are 2 \"solutions\":\n* move to .kde4 and stay with it forever - you stay forever with a non-default configuration directory and you break the upgrade path once for everyone.\n* use .kde4 until KDE 3 is dropped, then move back to .kde - that way you punish your early adopters who decided to move to KDE 4 as soon as possible by breaking their configuration not once, but _twice_! That's not an option for a distribution like Fedora which is primarily targeted towards early adopters.\n\nAs for attempting to support KDE 3 and 4 in parallel with just one .kde config directory, that sounds unsupportable to me, to the point where none of the distros offering both in parallel decided to use that setup (at least none of the big ones). KDE doesn't support application downgrades, so using old and new versions of the same application in parallel can cause problems with config files."
    author: "Kevin Kofler"
  - subject: ".kde(4)"
    date: 2008-08-16
    body: "<quote>* move to .kde4 and stay with it forever - you stay forever with a non-default configuration directory and you break the upgrade path once for everyone.</quote>\nI thought it was well recommended by most not to use .kde for upgrading to kde4, as many settings were considerably different, and you didn't want to ruin your existing .kde settings either.\n\nSome people want both installed at once side-by-side.  They obviously will need to configure these desktops independent of each other.  .kde and .kde4 being separate is almost a necessity.\n\nYou suggest that you need .kde as an upgrade path, yet KDE 3 and KDE 4 are so different, that upgrading your existing settings just doesn't make sense.  Conversely, I would take the following approach.\n\nI would work on scripts that move existing .kde settings into .kde4 where those settings are compatible and make sense to do so.  However, not all settings can move over, and the format for some settings may have changed.\n\n<quote>* use .kde4 until KDE 3 is dropped, then move back to .kde - that way you punish your early adopters who decided to move to KDE 4 as soon as possible by breaking their configuration not once, but _twice_! That's not an option for a distribution like Fedora which is primarily targeted towards early adopters.</quote>\n\nThis just doesn't even make sense remotely.  First off, their settings aren't broken either time.  As with above, either they get a fresh .kde4 directory with distro defaults, or you have a tool to migrate some of the .kde settings.  This isn't breaking their configuration at all.\n\nThen if you migrate back to .kde (which is really unnecessary and stops people largely from running KDE 3 and 4 side-by-side if they ever want to) you can do so with a symlink.  You're not ignoring or erasing the .kde4 directory.\n\nRegardless, all of this is circular logic.  You're saying you can't have KDE 3 and KDE 4 together, because eventually you want to drop one, and force people into old folder for settings.  Why force people into one folder for settings?  The only reason that makes sense is if you want to remove the choice of running both side-by-side.\n\nI like testing new things.  I like trying to run bleeding edge. I understand that Fedora is largely a testing grounds.  You can test KDE 4 without abandoning KDE 3.  I see no good reason to do so whatsoever.  Several distros handle both side-by-side without a problem."
    author: "T. J. Brumfield"
  - subject: "Re: .kde(4)"
    date: 2008-08-16
    body: "Please join your efforts with KDE3TO4.\nhttp://www.kde-apps.org/content/show.php?content=85769"
    author: "testerus"
  - subject: "Re: .kde(4)"
    date: 2008-08-16
    body: "> I thought it was well recommended by most not to use .kde for upgrading to\n> kde4, as many settings were considerably different, and you didn't want to\n> ruin your existing .kde settings either.\n\nWe don't want to _ruin_ our existing .kde settings, we want to _keep_ them! Of course, some settings can't be migrated, like all the Kicker stuff, those will just get ignored and do no harm. But the vast majority of application settings are interpreted by the KDE 4 versions just fine, as are several global KDE settings (such as single-click vs. double-click activation in list views).\n\n> Some people want both installed at once side-by-side. They obviously will\n> need to configure these desktops independent of each other. .kde and .kde4\n> being separate is almost a necessity.\n\nThat's exactly what I'm saying! And thus the conclusion that installing both at once side-by-side is impractical, because having .kde and .kde4 is impractical.\n \n> You suggest that you need .kde as an upgrade path, yet KDE 3 and KDE 4 are so\n> different, that upgrading your existing settings just doesn't make sense.\n\nIt perfectly makes sense. Most settings are retained just fine.\n\n> Conversely, I would take the following approach.\n>\n> I would work on scripts that move existing .kde settings into .kde4 where\n> those settings are compatible and make sense to do so. However, not all\n> settings can move over, and the format for some settings may have changed.\n\nThat's an ugly hack, and it requires the user to manually run some script. It's also a one-way street, so the only way you can use that script without losing some configuration is to run it once to migrate .kde to .kde4 (when upgrading to KDE 4) and then never again. But in that case, why not just keep .kde?\n\nKDE already has a tool for when the format of settings changes: kconf_update. But it works by fixing the settings in place, without an extra directory to migrate to. And luckily, it is needed only very rarely. Most settings just work in the newer version.\n\nAnother issue is that of running KDE 3 apps in a KDE 4 session. With a shared .kde, they'll inherit several KDE 4 settings, such as the single-click vs. double-click preference. If you use separate .kde and .kde4, you have to configure everything twice.\n\n> This just doesn't even make sense remotely. First off, their settings aren't\n> broken either time. As with above, either they get a fresh .kde4 directory\n> with distro defaults, or you have a tool to migrate some of the .kde\n> settings. This isn't breaking their configuration at all.\n\nResetting all settings to the defaults is what I call \"breaking their configuration\". It means users lose all their customization, which is pretty much unacceptable. Having to manually run a migration tool like the KDE3TO4 hack is also broken, first of all it requires manual intervention, which is unacceptable for an end user, and in addition, you can only run it once without overwriting some settings you made in KDE 4.\n\n> Then if you migrate back to .kde (which is really unnecessary and stops\n> people largely from running KDE 3 and 4 side-by-side if they ever want to)\n\nI already explained why keeping a separate .kde4 forever sucks. (That was the first of the 2 \"solutions\" I discussed.)\n\n> you can do so with a symlink. You're not ignoring or erasing the .kde4\n> directory.\n\nIf you have 2 separate .kde and .kde4 directories and you want to merge them into a single one, how do you suggest doing that without losing settings from either? They can contain conflicting settings! There's just no way this can be done automatically.\n\n> Regardless, all of this is circular logic. You're saying you can't have KDE 3\n> and KDE 4 together, because eventually you want to drop one, and force people\n> into old folder for settings. Why force people into one folder for settings?\n\nBecause it's the only way people can keep their existing settings without manual intervention, it's the only way settings made in KDE 4 can apply to KDE 3 apps where possible and it's the only way you can migrate applications to KDE 4 versions one at a time without running some hackish script for every single such app (hoping such a script will exist at all - KDE3TO4 doesn't even come close to supporting all apps you may want to migrate settings for). And migrating applications gradually is how things work in Fedora: for example, Fedora 9 has kdepim 3.5, Fedora 10 will have kdepim 4.1; KDevelop is still KDE 3 in Fedora 10 and will probably be a KDE 4 version in Fedora 11; and so on. There is no one point in time where we can \"migrate settings\" from .kde to .kde4 and be done with it.\n\n> The only reason that makes sense is if you want to remove the choice of\n> running both side-by-side.\n\nNo, it makes sense because it makes life a lot easier for the user who _doesn't_ want to run both side-by-side. And that's really the common case. You need to be a KDE geek with no real work to do to want to switch between KDE 3 and 4 all the time. ;-)\n\nThe average user wants to:\n* upgrade KDE itself to KDE 4 at one point in time, keeping all his/her existing settings, and without going back and forth and\n* upgrade his/her KDE applications to KDE 4 versions as they become available, keeping his/her existing settings there too, and there too without downgrading.\nThat's the use case the Fedora packaging is designed for (with the move to KDE 4 having been timed as early as possible because Fedora is targeted primarily at early adopters).\n\n> I like testing new things. I like trying to run bleeding edge. I understand\n> that Fedora is largely a testing grounds. You can test KDE 4 without\n> abandoning KDE 3. I see no good reason to do so whatsoever. Several distros\n> handle both side-by-side without a problem.\n\nI have given several good reasons, you've just ignored them."
    author: "Kevin Kofler"
  - subject: "Re: .kde(4)"
    date: 2008-08-16
    body: "PS: Oh and the difference between kconf_update and hacks like KDE3TO4 is that kconf_update is _automatically_ handled when an application reads its settings. And that's because it's the way configuration updates are intended to be handled in KDE, whereas KDE3TO4 is just a hack.\n\n(And the reason I'm mentioning KDE3TO4 which you haven't mentioned in your post is that, as its author remarked, it's exactly an implementation of the approach you were suggesting.)"
    author: "Kevin Kofler"
  - subject: "Re: .kde(4)"
    date: 2008-08-17
    body: "Additionally, setups which have KDEHOME set to a specific location will end up with the same directory for both versions, so neither applications nor any migration tool can assume that .kde for KDE3 and KDE4 are always different"
    author: "Kevin Krammer"
  - subject: "Re: .kde(4)"
    date: 2008-08-17
    body: "Right, that's why my original parallel-installability patches (which we only used in early prerelease packages which weren't considered usable for Rawhide) also patched things to have a separate KDE4HOME environment setting. AFAICT, none of the distros using the .kde4 setup picked that up, probably because it is a PITA to maintain. (I had to rerun KFileReplace and re-undo all the false positives in that patch at every single release. I was glad to be able to finally drop it in early pre-4.0 times.) I think they're also missing several places in the code where .kde is hardcoded. (I found a lot of those when I made my patches, and those were the biggest PITA for false positives from KFileReplace because \".kde\" also appears in strings like \"www.kde.org\".)"
    author: "Kevin Kofler"
---
KDE 4 is now also available for FreeBSD starting with the marvelous KDE 4.1.  The original commit from the FreeBSD/KDE team says '<em>The KDE FreeBSD team is proud to announce the release of KDE 4.1.0 for FreeBSD</em>'.  KDE 4 will be installed into a custom prefix ${LOCALBASE}/kde4 so KDE 4 and KDE 3 can co-exist.  For sound to work, it is necessary to have dbus and hal enabled
in your system. Please see the respective documentation on how
to enable these.  For more Information see the HEADS UP at ports@ and kde-freebsd@ or our <a href="http://wiki.freebsd.org/KDE4/Install">wiki page</a>.  See the <a href='http://www.freshports.org/commit.php?category=x11&amp;port=kde4&amp;files=yes&amp;message_id=200808091652.m79GqSOg017641@repoman.freebsd.org'>Commit message</a> and 
<a href='http://www.freshports.org/x11/kde4/'>port info</a>.  Have fun!






<!--break-->
