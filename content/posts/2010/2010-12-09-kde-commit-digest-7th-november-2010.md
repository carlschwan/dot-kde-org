---
title: "KDE Commit-Digest for 7th November 2010"
date:    2010-12-09
authors:
  - "dannya"
slug:    kde-commit-digest-7th-november-2010
comments:
  - subject: "Changes"
    date: 2010-12-09
    body: "Thank you, Commit Digest team!\r\n\r\nUsing a list for the \"highlights in this issue\" really helps readability.\r\n\r\n(On that note, I think reading the digests can be hard at times because the context changes frequently and has to be deduced by parsing URLs in your head. How about sorting commits by application name instead of fix/feature/optimization? Using a bunch of regexps should work in most cases, I imagine, and the rest could be collected under \"others\" or something like that.)\r\n\r\nAnd before I forget it: Congratulations on the decision to release digests even without articles. They're always great, but that sort of article can be published on the Planet or as a separate Dot story - whereas the actual commit compilations are unique to the Digest.\r\n\r\nSo, good changes all around. I'm happy :)\r\n\r\n"
    author: "onety-three"
  - subject: "The new feature which shows"
    date: 2010-12-10
    body: "The new feature which shows how many days passed since the bug was reported is very cool.\r\n\r\nCongratulations to Stefan Majewsky for fixing three bugs on Kolf, each of them taking:\r\n2999 days (8 years)\r\n2896 days (8 years)\r\n1556 (4 years)"
    author: "emilsedgh"
  - subject: "Git"
    date: 2010-12-09
    body: "Just a quick question: Are also the applications that are developed in git tracked? I ask because Calligra just moved to Git and I'd hate it to not be part of the commit digest."
    author: "ingwa"
  - subject: "Agreed"
    date: 2010-12-09
    body: "The new list format was the idea (and work) of Dot editor Carl Symons. It does make things much easier to read, I think.\r\n\r\nI would also like to praise Danny for releasing the Digest without an introductory article. Much as I love the articles (and should be helping to write them) the Commit Digest is too important to be delayed for that alone. I hope once the team has caught up we'll be able to reintroduce the articles, at least most of the time."
    author: "Stuart Jarvis"
  - subject: "Thx to the Commit Digest"
    date: 2010-12-09
    body: "Thx to the Commit Digest Team. Was  an interesting read as always\r\n\r\n\r\nI don't think that the current issue of the digest covers the last week.\r\n\r\nThe following line gives an indication: \"The plan is to release a Digest every few days from now until we're caught up to real-time.\"\r\n\r\nI like the idea to release the digest without background articles. The digest has enough of value on its own.\r\n\r\nWhy not extend the statistics (add graphs?) and release all on the Dot instead of another homepage?\r\n\r\nAnother idea would be a timeline with information about freezes and upcoming releases? I imagine a Gant Plot like figure.."
    author: "leuty"
  - subject: "Firstly, this issue only"
    date: 2010-12-10
    body: "Firstly, this issue only covers the week up to 7th November (yeah, running behind :))\r\n\r\nBut most importantly, commits to Git repos are not currently detected (the speed of the switch to Git caught me off guard, and I also wasn't aware that this was going to be a complete switchover, without even syncing of commits to SVN).\r\n\r\nI plan to have Git integrated into Enzyme (the platform that powers the Commit-Digest) tomorrow, so at least we'll be future proof soon!\r\n\r\nDanny"
    author: "dannya"
  - subject: "Also, if you send me details"
    date: 2010-12-10
    body: "Also, if you send me details of the Calligra move to Git (forward the commit mail maybe?), i'll try and ensure it is mentioned on the correct weekly issue.\r\n\r\nDanny"
    author: "dannya"
---
The <a href="http://www.commit-digest.org/contribute/">KDE Commit-Digest Team</a> has been busy again and is pleased to present the <a href="http://commit-digest.org/issues/2010-11-07/">KDE Commit-Digest</a> for 7th November 2010.

Highlights in this issue include: 
<ul><li>Notifications window can be moved around</li>
<li>Support for phone calls using the Contacts runner in <a href="http://plasma.kde.org/">Plasma</a></li>
<li>Caching of generated Lanczos textures results in performance improvements in KWin</li>
<!--break-->
<li>New KLocalizedDate class to make localization of dates "fun"</li>
<li>Lots of work on <a href="http://games.kde.org/game.php?game=kolf">Kolf</a></li>
<li>Support for relative "context present" actions in KNotify</li>
<li>Improvements to the katebuild-plugin of kdesdk</li>
<li>Budget and forecast functionality for accounts in Skrooge</li>
<li>Search dialogs added to various <a href="http://pim.kde.org/">Kontact Mobile</a> interfaces</li>
<li>More work on "List Save Refactoring", and Shape Caching in KOffice</li>
<li>Upload of local images, with HTML updating in Blogilo</li>
<li>Printers can now be manually added in playground/print-manager</li>
<li>Conclusion of big remake effort for mimetype icons in the 256x256 size</li>
<li>Videocatcher moves to playground/network/videocatcher</li>
<li>Klickety moves from kdereview to kdegames, KSame moves from kdegames to tags/unmaintained/4</li>
<li>Krazy2 moves to Git</li>
</ul>

<a href="http://commit-digest.org/issues/2010-11-07/">Read the complete Digest here</a>.