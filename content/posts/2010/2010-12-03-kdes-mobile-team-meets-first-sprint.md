---
title: "KDE's Mobile Team Meets for First Sprint"
date:    2010-12-03
authors:
  - "MoRpHeUz"
slug:    kdes-mobile-team-meets-first-sprint
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/mobiletouch.jpeg" /><br />Plasma Tablet</div><a href="http://www.kdab.com/">KDAB</a>'s Berlin office hosted the first Mobile Sprint for KDE contributors between the 18th and the 21st of November. The objective of the Sprint was to connect various KDE teams whose work in some way involves mobile platforms, so they could share experiences and work on improvements for all mobile projects.
<!--break-->
Most of the participants came straight from the <a href="http://conference2010.meego.com/">MeeGo Conference</a> to the Mobile Sprint with lots of ideas and subjects for discussion. The first day was focused on how to get KDE software into openSUSE Build Service (OBS) to provide easy creation and distribution of packages of KDE software to the MeeGo community. From this came the idea of having a "KDE community space" in OBS. This would make it easier to package KDE software and allow everyone to work together - today, each user has his or her own space. Discussions were started with MeeGo's OBS administrators to provide this.

<h2>Making Packages</h2>

Creating packages using OBS made it easy to deploy KDE software to tablets that were distributed during the MeeGo Conference. Soon we started to see our KDE applications running on MeeGo. The results were recorded, and videos posted: <a href="http://www.youtube.com/watch?v=SsWnfny61oI">Kontact Touch</a>, <a href="http://www.youtube.com/watch?v=UKdLCGCTu8w">Plasma Mobile</a>, <a href="http://www.youtube.com/watch?v=ei4-Q1ZoLQE">Plasma Tablet</a> and <a href="http://www.youtube.com/watch?v=LZxMDlkkJFs">KDE Games</a>.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/mobiletargets.jpeg" /><br />KDE software can target many devices</div>

<h2>Using Qt Quick</h2>

The other days consisted of coding and discussions about different subjects, all regarding KDE software on mobile devices. One of the topics was the use of <a href="http://qt.nokia.com/products/qt-quick/">QtQuick</a> and its declarative language, QML, in KDE projects. There was agreement that QtQuick will make mobile application development easier, and that most of the applications would use this new framework when they come to the mobile world.

After these discussions, two things were clear to the team: first, we will need to provide some KDE conveniences to all applications that want to use QML, and second, we want to provide QML components to ease the development of these applications.

<h2>Keeping KDE Platform Benefits</h2>

Technically speaking, the first conclusion means providing conveniences like KConfig and KIcon as well as bringing more localization and internationalization to QML through our KLocale and internationalization functions. Some of this work has already been tried for Plasma's integration with QML; we decided that this would be a great topic for an upcoming KDE Platform Sprint.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/mobileworktime.jpg" /><br />Getting down to work</div>To address the second conclusion, Plasma was selected as the best place to start with providing QML components. Plasma widgets will be migrated to QML, working with the <a href="http://qt.gitorious.org/qt-components">QtComponents project</a>. After that is complete, we will see what went well and what could be improved. We will then propose something that will benefit other applications. Meanwhile applications would be able to use libplasma if they want those QML widgets or anything else that Plasma could provide.

Parallel to these discussions, work has been started on KDE Games towards a framework that chooses at runtime the kind of widgets it should use: QtWidgets, Plasma widgets or QML elements.

A list of future tasks was created in order to help in future technical aspects of the project. This list can be found in the <a href="http://community.kde.org/KDE_Mobile">community wiki</a> and shows that KDE software on mobile devices can go far. However, to reach its full potential, it will need help of the entire community.

The sprint attendees thank <a href="http://www.kdab.com/">KDAB</a> and <a href="http://ev.kde.org">KDE e.V.</a> for supporting the event. You can help support future events like this by <a href="http://jointhegame.kde.org">Joining the Game</a>.