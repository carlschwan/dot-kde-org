---
title: "KDE Reaches New Audiences in North America"
date:    2010-08-12
authors:
  - "sethkenlon"
slug:    kde-reaches-new-audiences-north-america
comments:
  - subject: "go north america! :)"
    date: 2010-08-12
    body: "it is so amazing and wonderful to see the growth of KDE representation in North America. in the \"old days\" it was a small bunch of the same people doing it all, with some of us hitting large #s of shows each. today, there are several times that number who are active on the scene and we even have people coming over from Europe to help out from time to time.\r\n\r\nwe've long complained to ourselves about our weakness of representation in North America, but it seems we're finally turning that oh-so-important corner, thanks to the efforts of everyone who loves and works towards a better KDE on this continent. :) you all ROCK!\r\n\r\nas for FLOSS LC's Summer Camp, i'm there this Saturday giving 2 presentations ... so if you're in the Ottawa area, definitely come by and check it out. even if you don't get tickets to the event, drop by afterwards if you'd like to meet up .."
    author: "aseigo"
  - subject: "+Mexico"
    date: 2010-08-12
    body: "Well since we are talking about <strong>North America</strong>, I would include that our KDE Mexico members has been promoting KDE in many events around Mexico this year; I personally have been double dipping as I usually do, showing off KDE at LUGS/BUGS in California and in Baja California. I'm currently in Campus Party Mexico (http://www.campus-party.com.mx/2010/Software-Libre.html) a rather large event with more than 6K attendees talking of the many ways people can contribute to our beloved project.\r\n\r\nI do know we need to start updating the KDE Events page with our larger events here. I will make sure it gets done from now on. -_-\r\n\r\nWe can't let the Brazilians win.. I hate those guys ;-P (heh. Just Kidding)"
    author: "gamaral"
  - subject: "Send us a report for the Dot..."
    date: 2010-08-12
    body: "...On Campus Party Mexico and any other big or unusual events you're going to - we'd love to cover more of this stuff on the Dot.\r\n\r\nThat goes for everyone presenting KDE everywhere - let us know (use the contribute a story link above). We can easily fix up issues with spelling/grammar etc so your English doesnt have to be perfect. What we can't do is write interesting articles about events we didn't know were happening ;-)"
    author: "Stuart Jarvis"
  - subject: "It's funny but KDE is what attracted me to Linux and open source"
    date: 2010-08-25
    body: "The first time I saw those slanted folders in KDE 1.X when Leo Laporte was demonstrating Mandrake 5.3 on the old (and best) version of the Screensavers I was hooked. I really miss the exciting days of learning Linux and KDE... not that the excitement is gone! it is just different now. I was discovering a whole new world. \r\n\r\nI always thought KDE was pretty popular in the United States, All I ever install for people who are interested in a safer and simpler experience is a KDE based Distribution. \r\n "
    author: "madpuppy"
---

KDE software has traditionally been strongest in Europe and South America. With the growth of events such as Camp KDE and many key contributors calling North America home, KDE is increasing its presence in this region.

<h2>Ohio Linux Fest</h2>

The <a href="http://ohiolinux.org/">Ohio Linux Fest</a> is one of the largest community-driven Linux conferences in North America attracting over a thousand attendees. Of course, KDE will be returning this year for a visit, showing off the latest Plasma Workspaces, KDE Applications, and the spirit of Free Software. This year's Ohio Linux Fest will be held on September 10-12. It is a community event for Linux and BSD users, programmers, vendors, and newcomers. The KDE booth will have several stars, including podcaster and multimedia artist <b>Klaatu</b>, Ubuntu MOTU <b>Mackenzie Morgan</b>, and local Linux Club founder <b>Geoffrey Fannin</b>, showing KDE software running on different flavors of Linux and BSD.

KDE has been connecting with end users everywhere, and has been represented in 2010 at most major local conferences in North America.  This year KDE has presented at SouthEast Linux Fest and FOSSLC Summer Camp, and has actively participated at the Community Leadership Summit and OSCON in Portland, LinuxCon in Boston, Ohio Linux Fest, Utah Open Source Conference, Ontario Linux Fest, LinuxFest Northwest (Washington), and the Southern California Linux Expo.

<h2>Spreading The Word</h2>

Most importantly, through these efforts many users are experiencing KDE software for the first time. The simplicity and intuitiveness of Plasma Workspaces, the friendliness of Plasma Widgets (with new ones being created every day), and the ease of customization are helping people realize that they can have their KDE workspace any way they want it.

KDE's recent 4.5 Release Day saw the arrival of software improvements such as improved text-to-speech support for better accessibility, a more robust semantic desktop with Nepomuk, polished menu structures, advanced tabbed features in Dolphin and Konsole, better localization support, free-space notifiers, a keyboard layout Plasma Widget, a complete rewrite of the KInfoCenter hardware profiler, better styling and theming support, a new feature-filled activity manager interface, and much more.

<h2>See For Yourself</h2>

The rapid pace of development has positioned KDE Plasma Workspaces as one of the most progressive desktop environments available. New users love playing with our software. KDE is available on all major Linux distributions and BSD derivatives, and even on Windows, so it is easy to try. Why not get out to a conference to see the latest from KDE, meet community members or help spread the word to new users?

For a list of conferences that representatives from the KDE community will be attending, go to the <a href="http://events.kde.org/upcoming.php">KDE Events page</a>.