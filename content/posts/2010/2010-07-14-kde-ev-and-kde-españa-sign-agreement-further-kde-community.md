---
title: "KDE e.V. and KDE Espa\u00f1a Sign Agreement to Further KDE Community"
date:    2010-07-14
authors:
  - "jospoortvliet"
slug:    kde-ev-and-kde-españa-sign-agreement-further-kde-community
comments:
  - subject: "Spanish translation"
    date: 2010-07-16
    body: "You can find the spanish translation at http://kde-espana.es/notasprensa/acuerdokdeev.php"
    author: "tsdgeos"
---
During Akademy 2010, KDE e.V. and KDE España <a href="http://ev.kde.org/announcements/2010-07-14-kde-espania.php">signed an agreement</a> making KDE España the official representative of KDE e.V. in Spain. This will bring the international KDE community and the Spanish KDE community closer, while giving our local friends the authority to act officially as our representatives.

The Spanish KDE community has been growing strongly over the past few years. The Gran Canaria Desktop Summit was accompanied by a big local gathering, and before that the Spanish KDE teams have had numerous sprints and larger conferences like the several GUAdemy (joint KDE and Gnome) conferences (<a href="http://dot.kde.org/2007/03/28/guademy-2007-event-report">starting in 2007</a>) and <a href="http://dot.kde.org/2010/06/02/akademy-es-2010-big-success">Akademy ES</a>. There are local distributions in use by the Spanish government, and many independent bloggers and writers (see the recent <a href="http://dot.kde.org/2010/06/28/successful-spanish-kde-blogger-baltasar-ortega-talks-dot">interview with Baltasar Ortega<a/>).

All in all, like its international cousin, KDE España has been very active. Both organizations share the same goals - spreading Free Software and KDE software in particular; supporting the community by taking care of administrative things; running developer sprints and much more. Both are doing this from a non-profit perspective and have worked together in the past several times already. Both KDE España, registered officially in 2009 under Spanish law, and KDE e.V., since 1997 under German law, have a wide variety of contributors like packagers, artists, translators and software developers.

So it is time to formalize the relationship between both organizations. The agreement means KDE España will be the nearest contact for all organizations and individuals who want to contact KDE representatives regarding any issue related to KDE, including but not limited to claims about KDE copyrights infringements in the Spanish territory. KDE España will provide KDE e.V. with a report at least once a year about its activities (events, collaborations, agreements...) and its members, ensuring both organizations are still on the same page.

We hope this closer and formalized collaboration will help the Spanish KDE community to grow and expand its activities, and help make it more effective in spreading and supporting Free Software. At the same time, this agreement will strengthen the bond between the international and the local community!