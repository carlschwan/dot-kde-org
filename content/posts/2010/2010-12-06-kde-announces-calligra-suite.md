---
title: "KDE Announces the Calligra Suite"
date:    2010-12-06
authors:
  - "ingwa"
slug:    kde-announces-calligra-suite
comments:
  - subject: "So all the (sponsored)work on KOffice logos/icons etc is wasted?"
    date: 2010-12-06
    body: "That does not seem right, although I think all the Knaming(KMyBum) is the plague.\r\nI think in the long run I will probably love Calligra and the simple application names, they are just so unfamiliar at the moment.  "
    author: "kragil"
  - subject: "Kudos."
    date: 2010-12-07
    body: "Nice choices on the names. Apple will be pissed :-)\r\n\r\nDo they have some kind officially sanctioned 'long-form' way to refer to them, though? Saying \"I use Plan to plan my plans\" sounds funny and might encounter parse difficulties; so, \"Calligra Plan\"? \"KDE Plan\"?"
    author: "illissius"
  - subject: "I get it, but..."
    date: 2010-12-07
    body: "I can see why it makes sense to package this as a more general office suite, and I understand where the name is coming from, but... it sounds like the name of a birth control pill.\r\n\r\n\"I recently switched to Calligra...\"\r\n\"My coworkers recommend Calligra...\"\r\n\r\nAnyway, hopefully it'll grow on us :-/"
    author: "keka"
  - subject: "Thanks!"
    date: 2010-12-07
    body: "We invested a lot of brainjuice in the names. So this makes us very happy to hear that you like them :)\r\n"
    author: "Mark Kretschmann"
  - subject: "About the name"
    date: 2010-12-07
    body: "The name \"Calligra\" was actually created as a reference to \"Calligraphy\", the art of writing text in a beautiful and harmonic way.\r\n\r\nWe found that this fits our project well.\r\n\r\n\r\nPS:\r\nI can't really see how this sounds like a birth control pill...\r\n"
    author: "Mark Kretschmann"
  - subject: "Inconsistencies"
    date: 2010-12-07
    body: "I find the names a bit inconsistant. Why is het \"Words\" and \"Tables\" but not \"Stages\", \"Flows\" or \"Plans\"? Why has Kexi not been renamed to something to match?\r\n\r\nAnyway, I wish KOffice^H^HCalligra all the best, and I hope this new start will also lead to a better uptake and more sustained development of all of the applications in the suite. Good luck!"
    author: "andresomers"
  - subject: "Callegra Suite vs KOffice?"
    date: 2010-12-07
    body: "It is not completely clear to me what is meant by \"a continuation of the KOffice project\". Here [1] I read that Callegra Suite is to be understood as a fork of the KOffice project. Also, the sentence \"Currently all applications except Calligra Words will be maintained by their respective KOffice developers\" might be read as KWord/Words now being without maintainer, or that KWord will separately be developed as KWord outside of the Callegra Suite. The remark that \"Nearly everyone in the KOffice community has joined together to make this move\" seems to support that notion. Has there been some shism in the KOffice community? That would be really unfortunate. \r\n\r\nIt would perhaps be good to shed some light on this. \r\n\r\nedit: It seems that we <em>are</em> talking about a break in the KOffice community. This [2] email on the KOffice-devel list confirms that. That is really sad news. I think it is also sad that an article on the Dot does not bring this news in a clearer way. Calligra is now presented as if it is the only continuation of KOffice and a positive change only, and you have to read very carefully to see that something may be amis. \r\n\r\n[1] http://andreascarpino.it/2010/12/the-calligra-suite/\r\n[2] http://lists.kde.org/?l=koffice-devel&m=128782551919625&w=2"
    author: "andresomers"
  - subject: "Calligra"
    date: 2010-12-07
    body: "Calligra is a very beautiful name!\r\n\r\nCalligraphy (from Greek \u03ba\u03ac\u03bb\u03bb\u03bf\u03c2 kallos \"beauty\" + \u03b3\u03c1\u03b1\u03c6\u03ae graph\u1ebd \"writing\") \r\n\r\nThe word has a greek origin but is world wide used and sounds nice\r\nfor the latin languages, like spanish or portuguese!\r\n\r\nI would like to be part of this efforce! Good luck! "
    author: "ulissesleitao"
  - subject: "You *can* be part of it!"
    date: 2010-12-07
    body: "KDE applications are always looking for more people to help out. I am sure that is no different for Calligra, and you'd be more than welcome to help out in any or all applications that are part of the suite. "
    author: "andresomers"
  - subject: "..."
    date: 2010-12-07
    body: "..."
    author: "sebsauer"
  - subject: "1. KWord becomes Calligra"
    date: 2010-12-07
    body: "1. KWord becomes Calligra Words and new maintainers.\r\n2. For us this is a very positive change.\r\n"
    author: "sebsauer"
  - subject: "I am not saying that the"
    date: 2010-12-07
    body: "I am not saying that the change is not positive. I <em>really</em> hope it will be! I just think that the way the news is brought, is not very transparent. I also think it is not very fair to bring it this way. Although differences in views on what should be the focus may be exist, and there may be personal issues, I think it would only be fair to recognize the efforts of <strong>all</strong> involved into bringing KOffice this far.\r\n\r\n It has come to a clash and the conclusion that it would be better to continue in separate teams, as I have gathered now. To me, it is sad news that it has come to that, without placing blame anywhere. I do really hope that it will bring new energy to both ends of the fork, and that it will turn out to be a positive change in the end. We have seen more these events in the FOSS world in the past, like with X11 and more recenty with OpenOffice.org. For the first at least, I think the fork was beneficial. I hope the same will be true for Calligra Suite. \r\n\r\nI would just have hoped that the Dot would have shown both sides of the story, and just be open that it was decided that a split would be the best way forward. The focus of both ends of the split could be shown. We are all adults here, right? "
    author: "andresomers"
  - subject: "No sense in harping on the past"
    date: 2010-12-07
    body: "The past is the past -- we decided not harp on it but leave it behind us in this announcement, figuring it wouldn't be very interesting or exciting. Except for one person, all the KOffice community is moving forward under this new umbrella, all excited to make Calligra the umbrella project for great and innovative productivity and artistic applications, on the desktop, on tablets, on phones -- everywhere. Words is already moving forward really rapidly, and next week there'll be a focused effort by the new maintainer, Camila Boemann, and the Calligra Words hackers to bring stability and performance to the text layout engine.\r\n\r\nThe main reason for choosing this name was actually that it sounds nice and relevant both for the office apps, as for the creative apps, karbon and krita, that were always a bit weirdly placed in the KOffice suite.\r\n\r\nThe main cause that made us think about a new name was an unfortunate situation in our community, and that's something we're really eager to leave behind us. Let's just leave it at that and have fun hacking on Calligra!"
    author: "Boudewijn Rempt"
  - subject: "Oh yes!"
    date: 2010-12-07
    body: "We're trying to be a fun and welcoming community where everyone's contributions is respected and valued -- where beginners can learn a lot and everyone can have fun. So join us on #calligra on irc.freenode.net, or on the mailing list!"
    author: "Boudewijn Rempt"
  - subject: "Can someone explain what actually happened?"
    date: 2010-12-07
    body: "As far as I understand it you fork KOffice because of some disagreements with the maintainer of Kword. But what disagreements are this?\r\n\r\nAnd why is the whole community switching instead of the maintainer just forking kword?\r\n\r\nAnd will that mean that future kword and future words will share a common codebases until a certain level? Will there be a koffice basis for all three of them (New koffice, Calliga, Freoffice)?\r\n\r\nSome Clarification would be nice.\r\n\r\nPS: Who is maintainining the new words now and will there be more development in the future? I really like kword and would love to see it become more and more advanced."
    author: "BurkeOne"
  - subject: "Well..."
    date: 2010-12-07
    body: "Lots of questions... I really would prefer not to go into the details of what happened in the past. It's painful and not terribly useful.\r\n\r\nThe important thing is the way forward. Once we actually arrived at the point where we started thinking about a new name, we noticed we were having fun and that things started making sense, pieces falling into their place.\r\n\r\nSo, FreOffice is and will be based on Calligra; what will happen to the old KOffice codebase isn't certain. I doubt it will ever be possible to install both at the same time, and I expect that distributions will prefer to package Calligra Suite.\r\n\r\nCamila Boemann is the new maintainer for Words. She has worked, together with Sebastian Sauer, Pierre Ducruquet and others for a long time now. And they're all on board. In the past month or so, in the koffice-essen feature branch, KWord^Words has already made big strides, though we still need to spend a lot of work on it."
    author: "Boudewijn Rempt"
  - subject: "More about names"
    date: 2010-12-07
    body: "As illissius and andresomers I would like to hear more about the thoughts behind the names. I realize that you've spent a lot of time and effort on them, and I hope you take this as curiosity rather than criticism. :)\r\n\r\nI like that the names reflect the use of the applications, but on the other hand I find them very generic - \"Words\", \"Tables\", \"Stage\", \"Flow\", \"Plan\". One problem I see is that it makes it hard to Google. And as illissius wrote, it can sound funny in a conversation with someone who doesn't know what Calligra is.\r\n\r\nIt seems to me that you want something similar to the OpenOffice.org naming - for example, I always say \"OpenOffice\", not \"OpenOffice.org Writer\". Do you want people to mostly use the name \"Calligra\", (optionally) the \"subnames\" (Words, Tables, ...) to refer to specific applications and \"Calligra Suite\" for the whole thing? For example, \"I used Calligra to write this report\" or \"I used Calligra Words to write this report\"."
    author: "Hans"
  - subject: "because of some"
    date: 2010-12-07
    body: "<cite>because of some disagreements</cite>\r\n\r\nNot even close to. Please just accept that this is past and we already moved on.\r\n\r\n<cite>And will that mean that future kword and future words will share a common codebases until a certain level?</cite>\r\n\r\nSure. We will see what can be done. I guess that's one of the very next steps.\r\n\r\n<cite>Will there be a koffice basis for all three of them (New koffice, Calliga, Freoffice)?</cite>\r\n\r\nI don't know about \"New koffice\" but Calligra will continue the mission-goals defined during the koffice 2 planing phase (in no particular order and probably incomplete).\r\n* make the calligra-libs reusable very much the same way e.g. kdepimlibs can be used today in non-kdepim applications.\r\n* share even more code between the components/applications\r\n* get things stable, fast, bug-free, easy to maintain, ...\r\n* improve the ODF and Microsoft filters. Just to make it very clear: ODF is the way to go (in my opinion it's just impossible to support all of OOXML but all of ODF can be supported).\r\n"
    author: "sebsauer"
  - subject: "Website theming"
    date: 2010-12-07
    body: "Is there any plans or has it been thought of to start using the same website layout that kde.org and edu.kde.org use? The current one looks off as it is and shared effort is almost always a good thing. \r\n\r\nAbout the news itself, I gotta to say that I like the names and hope the best for the project. "
    author: "teho"
  - subject: "Words?"
    date: 2010-12-07
    body: "Yeah, I see inconsistency there - why Kexi and not Bases? Forms? And use plural form for all names. It actually sounds really great - much more better than KSpread etc. - but is it ok to Words? As it's very similar to one product, everybody knows, by company everybody knows with army of lawyers.\r\n\r\nGood luck guys! And thanks for your work! We already discussed Calligra at our Fedora KDE SIG meeting today - looks like we have winner... "
    author: "jreznik"
  - subject: "Reasons"
    date: 2010-12-07
    body: "The reasons for some of that are historically based: Some of the application (like Krita) have already established a brand. So their maintainers decided against renaming them.\r\n\r\nAbout the other inconsistencies: It was a trade-off. It's very hard to find good names, and we chose what we found fits best. Overall, I am personally very happy with the choices.\r\n\r\nRegarding Google: You are right that googling for \"Words\" will not work, but \"Calligra Words\" is easy to search for. So I think that's a non-issue.\r\n"
    author: "Mark Kretschmann"
  - subject: "fictional Latin"
    date: 2010-12-07
    body: "Drug brand names are usually a fictional Latin word that\u2019s a cognate to an English word synonymous with \"lively\". Don't know how he got birth control in particular, lol.\r\n\r\nCalligra has the advantage of being a real Latin word. :)"
    author: "eean"
  - subject: "I was mostly being facetious"
    date: 2010-12-08
    body: "I was mostly being facetious ;) Like I said, I understand the origin of the name. Just sounded a bit weird to me, that's all. Hope it grows on me. Kudos to all involved."
    author: "keka"
  - subject: "Kexi: How about Cranium -"
    date: 2010-12-08
    body: "Kexi: How about Cranium - shell for your brain?\r\n\r\nThe other names are nice though.\r\n\r\nedit: on second thought, too complex and doesn't fit in. Something simple like 'Data' maybe. \r\n\r\n"
    author: "Rtvik"
  - subject: "no there is no such plan."
    date: 2010-12-08
    body: "no there is no such plan."
    author: "cyrille"
  - subject: "Kexi and Krita sound bad in"
    date: 2010-12-09
    body: "Kexi and Krita sound bad in English, and they and Karbon aren't descriptive in English, which ruins consistency. The new names are better in terms of consistency and descriptiveness in English."
    author: "yman"
  - subject: "install.exe planned?"
    date: 2010-12-12
    body: "Personally I use KDE on Windows when I'm forced to work on this system, but it is far too complicated for non-user of linux. So I've never been able to advise KOffice to Windows users. Is an install.exe planned? I mean a very simple wizard wich install Calligra and only Calligra, with only the questions that are accustomed to Windows users:\r\n- no \"install from internet or from local directory?\" (everything should already be in the big install.exe file)\r\n- no \"end user or developer?\"\r\n- no \"compiler mode?\" (the office user does not even understand what we are talking)\r\n- no \"internet settings\" (again, everything should already be in the big install.exe file)\r\n- no \"wich mirror?\" (idem)\r\n- no \"wich release of KDE?\" (the office user always want the last stable version)\r\n- no \"wich KDE packages?\" (the user just want to install KOffice, and he must choose among dozens of packages that have nothing to do)\r\n- It is less important, but the creation of desktop shortcuts is not available.\r\n\r\nPlease, I wish I could recommend Calligra Suite to Windows users."
    author: "idoric"
---
The KDE community today announces the start of the <a href="http://www.calligra-suite.org/">Calligra Suite</a> project, a continuation of the KOffice project. The new name reflects the wider value of the KOffice technology platform beyond just desktop office applications. With a new name for the Suite and new names for the productivity applications, the Calligra community welcomes a new stage in the development of free productivity and creativity applications for desktop and mobile devices.
<!--break-->
Read more for the details

<h2>Calligra Overview</h2>

The Calligra Suite contains the following applications:

<table cellpadding="5">
<tr><td colspan="2"><strong>Productivity applications:</strong></td>
<tr><td><i>Words</i></td><td>Word processor; new, but evolved from KWord</td></tr>
<tr><td><i>Tables</i></td><td>Spreadsheet program, previously known as KSpread</td></tr>
<tr><td><i>Stage</i></td><td>Presentation program, previously known as KPresenter</td></tr>
<tr><td><i>Flow</i></td><td>Flowchart program, previously known as Kivio (will be released in the next version)</td></tr>
<tr><td><i>Kexi</i></td><td>Database application like Microsoft Access</td></tr>
<tr><td colspan="2"><strong>Management applications:</strong></td></tr>
<tr><td><i>Plan</i></td><td>Project management, previously known as KPlato</td></tr>
<tr><td colspan="2"><strong>Graphics applications:</strong></td></tr>
<tr><td><i>Krita</i></td><td>Drawing application for multi-layered pixel-based images</td></tr>
<tr><td><i>Karbon</i></td><td>Drawing application for multi-layered vector images</td></tr>
</table>

More applications will be added as we get more contributors. Currently all applications except Calligra Words will be maintained by their respective KOffice developers. 

The Calligra Suite introduces the Calligra Office Engine which makes it easy for developers to create new user experiences, target new platforms and create specialized versions for new kinds of users. Currently, there are two main user experiences: the desktop UI with the applications mentioned above, and FreOffice which is the only free mobile office suite in existence.


<h2>Calligra and Users</h2>

It is a goal of the Project that users will see only positive changes.  In addition to new names, we expect that the move to Git will make development speedier and higher quality.  New user experiences will be added and the existing ones will see more usability work. 

The community will release KOffice version 2.3 as planned in approximately 2 weeks, and will maintain it with bugfix releases during the KOffice 2.3 life cycle.  Starting with version 2.4, we will continue development of most of the applications under the new names.

<h2>Calligra and the Past</h2>

Nearly everyone in the KOffice community has joined together to make this move. Leaving the past behind us, we are excited at this opportunity to make our software more innovative and widely-used. At the same time that the Calligra project is created, we will move from Subversion to Git, making it an even better platform for innovation in the free office space.


<h2>Calligra and KDE</h2>

KDE is one of the largest open source communities in the world. KDE software is one of only two projects that has reached <a href="http://blogs.fsfe.org/padams/?p=140">a million commits</a> in their repositories (the other is Apache).

KDE publishes several workspaces (Plasma Desktop, Plasma Netbook and soon Plasma Mobile), a development platform, and many sets of applications like the KDE EDU applications, KDE games, Amarok and K3b. 

KOffice has always been part of the larger KDE community.  Calligra will continue to use the KDE infrastructure such as bugtracker, forums, and community wikis. The Calligra team is very much a part of the KDE community and proud of it. The applications we develop will continue to be KDE offerings.

<h2>Resources</h2>

Find out more about Calligra Suite here:

For users:
User Web Site:                http://www.calligra-suite.org/
User Forum:                   http://forum.kde.org/viewforum.php?f=203
Facebook Page:                http://www.facebook.com/pages/Calligra/161929600510563
Facebook Group:               http://www.facebook.com/home.php?sk=group_176774135672923 

For developers:
Developer Mailing List:       https://mail.kde.org/mailman/listinfo/calligra-devel
Developer Wiki:               http://community.kde.org/Calligra
Source code:                  https://projects.kde.org/projects/calligra/repository

Existing web sites for individual applications like krita.org and kexi-project.org will continue to work as before.

<h2>More Information</h2>

The Calligra marketing contact is:

Inge Wallin
Calligra Marketing Coordinator
email: ingwa@kde.org
phone: +46 707 260 853