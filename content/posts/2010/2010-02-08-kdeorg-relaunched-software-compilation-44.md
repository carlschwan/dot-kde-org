---
title: "KDE.org Relaunched for Software Compilation 4.4"
date:    2010-02-08
authors:
  - "troy"
slug:    kdeorg-relaunched-software-compilation-44
comments:
  - subject: "Great"
    date: 2010-02-08
    body: "Nice, very nice. It was time to refresh the KDE.org homepage and it is just wondurful, grat job guys. :)"
    author: "Julien"
  - subject: "Rough edges"
    date: 2010-02-09
    body: "I'd just like to point out that the web team had a great time working on this site. We know that there are a couple of rough edges that still need improvement, and they'll get fixed over the next few days. In the meantime, please enjoy our hard work.\r\n\r\nCheers"
    author: "troy"
  - subject: "Wow it's fantastic"
    date: 2010-02-09
    body: "Really good job!\r\nI really love the new website. It's 10 times as representative of who we KDE are than the old one.\r\n\r\nThanks!"
    author: "Fyzix"
  - subject: "Documentation"
    date: 2010-02-09
    body: "Great effort guys!\r\n\r\nCouple of quick questions:\r\n\r\nShould we really have the Documentation link so high under the Support menu?  Or even there at all?  Many docbooks are horribly incomplete and out-of-date (the prominently linked User Guide and FAQ are dated 2005, and the Plasma one is dated 2008) and Userbase seems a far better place to send people to for now, at least until the docs site can be cleaned up a bit.\r\n\r\nWill we be changing the look of other sites like the wikis, dot, bko and docs to match?  I must confess I quite like them as they are, but we should probably be consistent?"
    author: "odysseus"
  - subject: "Really nice!"
    date: 2010-02-09
    body: "I like it a lot, both the new website and the new look of KDE desktop. Now the only thing left is to exchange the terrible default sound scheme (the piano sounds)."
    author: "SP"
  - subject: "Wonderful!"
    date: 2010-02-09
    body: "Huh, great Job - this is really well done!! All the awesomeness in one place!!"
    author: "nidi"
  - subject: "Excellent redesign"
    date: 2010-02-09
    body: "And much needed! It looks very good and much better laid out."
    author: "Blackpaw"
  - subject: "Some bugs"
    date: 2010-02-09
    body: "Website does not work good on 800 pixel wide systems (netbooks anyone?) Shrinking the text size did not help, looks like there are fixed size layouts or graphics.\r\n\r\nScrolling seems very sluggish (may be a Konqueror problem, though, this is with JavaScript, animations, and plugins disabled).\r\n\r\nDue to the menu being at the top, the text starts \"very late\" in page, so that you have to scroll a lot (yes, the forums have the same layout, and the same problem), the large paragraph spacing adds to that."
    author: "christoph"
  - subject: "Doin?"
    date: 2010-02-09
    body: "It's Dion Moult :) - not Doin Moult!\r\n\r\nThere are some compatbility issues with some browsers (IE, yes, yes) and hopefully these will be ironed out over the next few days."
    author: "Moult"
  - subject: "nice..."
    date: 2010-02-09
    body: "Beautiful job everyone! Cheers!"
    author: "dubkat"
  - subject: "Amazing"
    date: 2010-02-09
    body: "You guys did an amazing work here, congratulations! Wonderful design!\r\n\r\nCheers!"
    author: "edneymatias"
  - subject: "Roadmap?"
    date: 2010-02-09
    body: "Fancy eye candy. But I couldn't find a link to the SC roadmap (for example a link to http://techbase.kde.org/index.php?title=Schedules/KDE4/4.4_Release_Schedule would currently be helpful) - I think a lot of KDE users come to kde.org to check when the next release / RC / beta is due.\r\n\r\nWhile I was looking for a link to a roadmap, I also noticed that there isn't a search field which could (maybe) have helped me find it.\r\n\r\nHope those two things will be added soon."
    author: "padde"
  - subject: "Free Software"
    date: 2010-02-09
    body: "I'd suggest linking the phrase Free Software (for example on the download page) to Free Software definition.\r\n\r\nAnd/or substitute it for Free/Libre Software.\r\n\r\nCheerio and keep up the good work!\r\n\r\nIvan"
    author: "ivan"
  - subject: "Great work"
    date: 2010-02-09
    body: "Really great work.... I really like the new design.\r\n\r\nThanks for all your time!"
    author: "TheGreatGonzo"
  - subject: "Try webkitpart for konqueror"
    date: 2010-02-09
    body: "Yup it's a bit sluggish with khtml, but it flies with webkitpart.\r\n\r\nHonestly the new site is jaw dropping!"
    author: "bliblibli"
  - subject: "WOW"
    date: 2010-02-09
    body: "I love it !"
    author: "taipan"
  - subject: "It looks fresh, lucid, really"
    date: 2010-02-09
    body: "It looks fresh, lucid, really beautiful. Nice job guys and girls :)"
    author: "Bobby"
  - subject: "fixed"
    date: 2010-02-09
    body: "fixed ;-)"
    author: "jospoortvliet"
  - subject: "Well done KDE community!"
    date: 2010-02-09
    body: "I like the new web site a lot, it is as well designed that I have been navigating through almost all the links to enjoy it!\r\n\r\nBut, there is one BUT ... I have realized that almost all the screeshots of the apps are updated with the new look and feel of KDE SC 4.4, but there are some not updated.\r\n\r\nAnyway, incredible and necessary modification for the KDE webpage!\r\n\r\nCongratulations KDE members!"
    author: "Poldark"
  - subject: "Rough edges"
    date: 2010-02-09
    body: "From clicking around a little, I found a few broken links in the menu. Gives 404s. Do you want such errors reported or do you already have the knowledge, and in such cases where do you want errors/bugs reported?\r\n\r\nAs it is, in the menu Community ->About KDE the folowing entries fail ->KDE e.V. Foundation, ->History and ->Awards"
    author: "Morty"
  - subject: "Looks great indeed! It took"
    date: 2010-02-09
    body: "Looks great indeed! It took me a few seconds to find the popping out navigation, but I guess that one makes sense as well and will not form any usability issue.\r\n\r\nI believe there is a small typo in the main text though:\r\n\"The KDE\u2122 Community is a international technology\"\r\nshould be\r\nAN international technology\r\n\r\n(or my English is off)"
    author: "thijsdetweede"
  - subject: "Opera - Scrolling is sluggish"
    date: 2010-02-09
    body: "Don't know why... but scrolling in Opera (10.10) is sluggish... also some rendering errors occur like the head picture vanishing after scrolling....\r\n"
    author: "thomas"
  - subject: "fixed by someone ;-)"
    date: 2010-02-09
    body: "fixed by someone ;-)"
    author: "jospoortvliet"
  - subject: "Congrats"
    date: 2010-02-09
    body: "The new layout looks great, a huge improvement over the old site. Thanks to the www team!"
    author: "saschpe"
  - subject: "Great Job"
    date: 2010-02-09
    body: "It even works nicely with text based browsers ;)"
    author: "longh"
  - subject: "Look awesome!"
    date: 2010-02-09
    body: "I read about this on facebook last night and when I checked the new site, I thougt I was dreaming... looks awesome!\r\n\r\nHowever, there are some issues:\r\nThe navigation is a bit too nervous.\r\nFor example if you click on community and wait for the page to be loaded, the navigation will slide down as soon as you move the mouse.\r\nAnd the other way round: If you click on \"Community/About KDE\", the menu slides/pops back when the site loads. If you want to access a sub-section of \"About KDE\", you will have to hover the navigation again and wait until it slides down.\r\n\r\nThese two issues create a quite nervous feeling about the site."
    author: "Robin"
  - subject: "Weight"
    date: 2010-02-09
    body: "I think the new design looks really great. However it would not hurt to make the elements a bit lighter. I mean, http://www.kde.org/media/images/footer1.png is 1.3MB! Not everyone is on DSL. Frankly this is way too heavy."
    author: "Med"
  - subject: "being fixed as I type this"
    date: 2010-02-09
    body: "It is being fixed now, we forgot that one :)."
    author: "pinheiro"
  - subject: "Opera and Konqueror both"
    date: 2010-02-09
    body: "Opera and Konqueror both. It is all the text, it has an almost invisible white outline using css text-shadow. CSS text-shadow was never supposed to be used for an entire site, and wasn't optimized for it. Now KHTML has new optimizations for text-shadows in trunk though. \r\n\r\nSafari uses OS X native shadow rendering calls, making it faster. Webkitpart hasn't implemented text-shadows yet, making it blindingly fast by doing nothing ;)"
    author: "carewolf"
  - subject: "Don't forget the header graphic"
    date: 2010-02-09
    body: "That one is just shy of 1 Mb as well."
    author: "chill"
  - subject: "dates on announcements and news"
    date: 2010-02-09
    body: "Like the new site - looks good.\r\n\r\nOne of my pet peeves though is no dates on announcements and news. I hope you can get dates on these items.\r\n\r\ncheers, Jd "
    author: "jdory"
  - subject: "bug reports"
    date: 2010-02-09
    body: "There is a proper website bug reporting section within bugs.kde.org - filing there gives the best exposure (people get emailed, the irc channel gets spammed, etc.)\r\n\r\nBut in this case, I'll add it to my todo list :)\r\n\r\nCheers"
    author: "troy"
  - subject: "\u00a0"
    date: 2010-02-09
    body: "Great\u2026 Takes about 2 minutes to load. Why didn\u2019t you just write it in Silverlight to make it even more unusable?"
    author: "cdauth"
  - subject: "Yes and no"
    date: 2010-02-09
    body: "I like the overall look of the site, but I'm far from convinced by the navigation system. I nudge my mouse over, and it unfolds (filling half the content area on a laptop), then doesn't go away again until I click <i>Toggle Menu</i>.\r\n\r\nSpecifically, I'd suggest that what appears on mousing over should disappear on mousing out, and that the content of the page should stay still, not slide up and down."
    author: "takowl"
  - subject: "good look"
    date: 2010-02-10
    body: "i like the redesign of the page, but as people stated before there are some things, which might get better:\r\n\r\n- the picture with the navigation is far to big, so the text (the interesting information) are only reachable by extensiv scrolling with a netbook\r\n\r\n- the new navigation on the top is not performing well\r\n\r\n- all at all the page is quite slow (using opera right now) - I was always impressed by the old one loading a lot faster than most of the other pages I use out there... sometimes less is more ;)\r\n\r\njust my 2 cents & thanks for your work!\r\n\r\nlotman"
    author: "lotman"
  - subject: "feel free to help"
    date: 2010-02-10
    body: "feel free to send proper screenshots to us! send to kde-www mailinglist (@kde.org) plz."
    author: "jospoortvliet"
  - subject: "i wouldn't say it's slow -"
    date: 2010-02-10
    body: "i wouldn't say it's slow - it's dog slow :)\r\nscrolling in opera, for example, 4.4 release notes visibly redraws screen. first page also is awfully slow. it's pretty much unusable in opera right now... (opera 10.10, latest stable)"
    author: "richlv"
  - subject: "To be brutally honest..."
    date: 2010-02-11
    body: "To be brutally honest, it looks really amateurish. KDE has access to many good designers and UI experts. I do not understand, how did you guys manage to frack up your web so badly and make it look like every other garbage pile (aka blog) out there.\r\nDo you really need 250K png as a front page logo?\r\n"
    author: "Spaces"
---
The KDE web team is pleased to announce a major redesign of the <a href="http://www.kde.org">KDE.org frontpage</a> and <a href="http://buzz.kde.org">buzz.kde.org</a>, just in time for the pending release of our updated Workspace, Application and Development Platform compilation. The redesign is the result of many hours of work by artists, coders, writers and testers. Keep reading to gain some insight into the people and processes behind the retooling.
<!--break-->
<h2>Impetus</h2>

As many people are already aware, KDE recently underwent a shift in marketing and promotion vocabulary. These changes combined with many orphaned and out-of-date pages on the existing KDE.org website were a major topic of discussion at the <a href="http://dot.kde.org/2009/11/20/booth-web-and-marketing-sprint">KDE Marketing Meeting</a> last November in Stuttgart. Once a branding strategy had been agreed upon (KDE is the organization or community, which produces Workspaces, Applications, and a Developer Platform), a site navigation was discussed which can accurately relate this information about the community to new or returning visitors. 

During initial mockups, it was discovered that there was an aborted attempt to update KDE.org already in the KDE source repository from two years earlier - this previous attempt was never completed, but some of the framework was already in place to reuse, so work got underway immediately. Now after several months of work, many mockups lay on the cutting room floor, and the final site is ready, aside from a few tweaks still pending.

<h2>The Web Team</h2>

A major, high traffic website like KDE's should not be left without updates for long periods of time, as the interested web developers get stuck in content maintenance mode, and soon lose interest. Occasionally, a refresh is required simply to revitalise the Web Team. So, briefly and in no particular order, the KDE community would like to acknowledge the current Web Team, many of which have had a few too many short nights since November:

<ul>
<li>Eugene Trounev (artwork)</li>
<li>Ingo Malchow (design, code)</li>
<li>Ben Cooksley (css, content)</li>
<li>Dario Andres (content)</li>
<li>Frank Karlitschek (code)</li>
<li>Troy Unrau (branding)</li>
<li>Daniel Laidig (app pages)</li>
<li>Emil Sedgh (dynamic content)</li>
<li>Dion Moult (design)</li>
<li>Hans Chen (design)</li>
<li>Nuno Pinheiro (artwork)</li>
<li>Rainer Enders (mockups)</li>
<li>Sebas Kügler (content)</li>
<li>Lydia Pintscher (content)</li>
<li>Luca Beltrame (content)</li>
<li>Jos Poortvliet (content)</li>
<li>Franz Keferböck (buzz.kde.org)</li>
</ul>