---
title: "Akademy 2010 Call for Sponsorship"
date:    2010-02-14
authors:
  - "jospoortvliet"
slug:    akademy-2010-call-sponsorship
---
The organizing team of <a href="http://akademy.kde.org">Akademy 2010</a> is looking for sponsors to help make the annual world summit of the KDE community a success. Akademy 2010 will take place in Tampere, Finland from July 3rd to July 10th and is organized by <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.coss.fi/en">COSS</a>, the Finnish Centre for Open Source Solutions.
<!--break-->
By joining the lineup of sponsors for the annual KDE conference, organizations benefit from meeting key contributors of the KDE community and getting first-hand information on the latest technological developments for the Free Software desktop.

Thanks to  the vital support of our sponsors, attendance to Akademy is free, thus giving all KDE contributors the opportunity to meet face to face. This not only fosters social bonds, it provides the opportunity to work on concrete technological issues, gain new ideas, and reinforce the innovative culture in the KDE community.

Sponsors are also invited to provide talks about the interesting technologies they are working on, contributing to the flow of ideas at the conference.

The <a href="http://dot.kde.org/2010/02/01/akademy-2010-our-world-clearly-call-papers">Call for Papers</a> was published in early February.

Akademy features a 2-day conference with talks on the latest KDE technologies followed by 5 days of coding sessions and workshops. We expect over 500 attendees from all over the world including core desktop and Linux platform developers, industry players, government officials and members of the educational community.

If your company would like to sponsor this event or if you know an organization which would benefit from sponsoring Akademy, please contact us at <a href="mailto:akademy-sponsoring@kde.org">akademy-sponsoring</a> or visit <a href="http://akademy.kde.org/sponsors">akademy.kde.org/sponsors</a>.