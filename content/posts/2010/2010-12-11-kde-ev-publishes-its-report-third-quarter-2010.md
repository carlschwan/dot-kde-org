---
title: "KDE e.V. Publishes Its Report for Third Quarter 2010"
date:    2010-12-11
authors:
  - "oriol"
slug:    kde-ev-publishes-its-report-third-quarter-2010
comments:
  - subject: "KDE e.V.'s pdf"
    date: 2010-12-11
    body: "The Layout and the design of the pdf is very pretty, but the contents are rather boring. Why don't you spent this time to make a pdf magazine for every new KDE release? I think it would be very good for marketing. Every new KDE release makes me very happy because of it's rapid evolution, but if I was not following the .dot, The frustratingly minimalistic announcements makes it really difficult fore me to explore all the new and exiting stuff!\r\n\r\np.s. I love kde 4\r\n"
    author: "nickkefi"
  - subject: "OMG p^wmonies"
    date: 2010-12-11
    body: "A net result of > \u20ac50k for one quarter is not too shabby.  Heck that's much better than some companies I know. :-)"
    author: "ingwa"
  - subject: "Thanks"
    date: 2010-12-11
    body: "for a very lively and colorful report. I enjoyed reading it! I like the addition of pictures from kde-look and from Akademy. \r\nIt shows how our community is lively and diverse."
    author: "annnma"
  - subject: "Different Audiences"
    date: 2010-12-12
    body: "I think it depends on the audience.  There was a point in my life when I thought open source was uniquely (primarily) about the software.  That is no longer true (or at least, not in the same way).\r\n\r\nNow, I'm more interested in KDE and open source because of the community it represents.  For that reason, the KDE e.V. publication resonates with me very well.  I want to know what the people of KDE are doing rather than the evolution of the software.\r\n\r\nWith that said, I think that your idea of a PDF magazine (with exceptionally high production values) is very interesting.  If properly tuned and targeted, it could be a powerful tool for showcasing KDE technology (which is awesome) and how it can transform people's lives for the better.\r\n\r\nHowever, it's a huge undertaking to put together that kind of a publication.  I would be happy to help with the design end of things, but to be successful, you would also need an editor.  Maybe you could propose it to the KDE promotions working group?  They might have the resources to put something together."
    author: "RobertSOakes"
  - subject: "Help Us :-)"
    date: 2010-12-12
    body: "We would love to make a much more in depth reports of the new features - and sometimes we do. We'd like to do it again for the 4.6 releases, but need your help - see:\r\nhttp://www.asinen.org/2010/11/feature-guide-for-4-6-releases/\r\nfor more details"
    author: "Stuart Jarvis"
  - subject: "KDE's pdf"
    date: 2010-12-12
    body: "I was kind of fearing a \"please help us\" reply..\r\n\r\nI did make o illuminated guide to kde in greek at 4.0 times when people was very confused about all these innovations. (I really loved these times... my desktop was so innovative, self-destructing and unstable! Like a true rock-star!)\r\n\r\nAnyway, I have the following idea:\r\n\r\nThe developers may have a list of kde enthusiasts that are awful in maths (like me) and assign to them to write a pretty report and take some pretty screenshots of their new pretty awesome staff in their app. Then, the promo-team could take these articles, moderate them and put them in the paper. You could then name us \"The kde reporters\" and make us happy and motivated."
    author: "nickkefi"
  - subject: "I think your point on the"
    date: 2010-12-13
    body: "I think your point on the audience is a very important one.\r\n\r\nI believe that there are people that follow the software and people that follow the community and lastly people that overlap both audiences. I think most users that follow the community would also fall into the software audience but I don't think the same is always true for the software audience.\r\n\r\nI believe marketing, news and reports should take these differences in audience into account and try to keep to an audience per a article or report. \r\n\r\nI enjoyed the little of this report that I have read so far, but this report is from my point of view definitely for a community audience and I believe written and present very well for that audience.\r\n\r\nI think the release report/magazine nickkefi mentioned would fall mostly in the software audience category as it would mostly be promoting new features to current or potentially new users.\r\n"
    author: "NaX"
  - subject: "Happy to oblige :-)"
    date: 2010-12-13
    body: "Yeah, you should know you can't post comments with ideas without someone like me asking you to help :-)\r\n\r\nI think it is a good idea, however I wouldn't necessarily rely on the developers having time to ask you to do particular things (it could happen). However, you can go through the feature list as I linked to it and pick things to play with and write about, but maybe that's not quite what you mean.\r\n\r\nCan you either mail kde-promo or me personally (firstname dot lastname at gmail dot com) to discuss? Thanks"
    author: "Stuart Jarvis"
  - subject: "Absolutely..."
    date: 2010-12-17
    body: "Have to agree here: The PDF document is beautiful and informative.\r\n\r\nGreat work!\r\n"
    author: "Mark Kretschmann"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://ev.kde.org/reports/ev-quarterly-2010Q3.pdf"><img src="http://dot.kde.org/sites/dot.kde.org/files/report_0.jpeg" /></a><br />KDE e.V Third Quarter Report</div><a href="http://ev.kde.org/">KDE e.V.</a>, the non-profit organization that supports the KDE community in legal and financial matters, has just published its <a href="http://ev.kde.org/reports/ev-quarterly-2010Q3.pdf">report for the third quarter of 2010</a>. The report highlights the history and current activities of KDE's Marketing Working Group and summarizes the results to date of KDE's individual supporting membership campaign <a href="http://jointhegame.kde.org/">'Join the Game'</a>. It also reviews the different contributor sprints and events that KDE members have attended in the last three months. These included the KDE Imaging Sprint that took place in Aix-en-Provence (France) in late August and <a href="http://akademy.kde.org/">Akademy 2010</a>, KDE's yearly flagship conference that was held in Tampere, Finland, with much success in early July. The report ends by introducing the new KDE e.V. members and by presenting a brief system administration report and an overview of KDE e.V.'s finances.

Besides all its useful information, this report also features a brand new and beautiful design courtesy of KDE contributors Eugene Trounev and Rob Oakes. User-contributed artwork and images are also featured throughout, making this Quarterly Report a real celebration of community efforts. <a href="http://ev.kde.org/reports/ev-quarterly-2010Q3.pdf">Check it out</a>!

