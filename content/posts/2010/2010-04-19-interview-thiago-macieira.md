---
title: "Interview with Thiago Macieira"
date:    2010-04-19
authors:
  - "giovanni"
slug:    interview-thiago-macieira
---
<p>Hello again for another interview with KDE people. Last time we had <a  href="http://dot.kde.org/2010/03/29/interview-aleix-pol-kde-edu-and-kdevelop">Aleix Pol</a> with us. Now it is time for Thiago Macieira. One of the first KDE developers, he is still working towards world domination at <a href="http://qt.nokia.com/">Qt</a> as he will explain to us in the interview. For the Italian readers here is the <a href="http://kde-it.org/2010/04/17/intervista-a-thiago-macieira/">original interview</a>.</p>

<p><b>Hi Thiago, can you present yourself to the Dot audience?</b><br />
Hello, my name is Thiago Macieira, Brazilian, 28 and I’m currently living in Oslo, Norway. I graduated with a double degree in Engineering in 2004 and I am also holding an MBA. I am now working for Qt Development Frameworks in Nokia and still somewhat involved in KDE activities (but not as much as I used to be).</p>

<p><b>When did you first hear about KDE?</b><br />
Oh, sometime in 1998 or 1999. I don’t remember exactly. I remember being on IRC somewhere and someone pointed me to <a href="http://www.kde.org">www.kde.org</a>, which had that picture of a  computer and was asking if Unix was ready for the desktop. I remember thinking, “yeah, right, this project will never be successful”.</p>

<p><b>In which part of the KDE community and in what development area(s) are you involved?</b><br />
I started with coding in the networking area and that’s pretty much the only coding I’ve done for KDE: sockets, KIO, etc. Besides that, I still compile the KDE Software Compilation more often than most people, so I find compilation errors and fix them. Since 2001, I’ve only run the development version of KDE software (except for the period when Software Compilation 4 wasn’t usable yet), so I’m also finding issues and ranting about them on IRC. And as part of the Qt development team, I help by making Qt the best product it can be and ensuring it continues to meet KDE’s needs.</p>

<p><b>What are the missing features of KDE applications you think we should have to build a strong product set?</b><br />
I don’t know. It’s already a very cool and powerful set of applications. It’s not about features missing – though don’t get me wrong, more features are always nice, provided they don’t clutter the interface. It’s about being stable, functional, beautiful and continuously so. KDE has already created most of that, we just need to keep doing what we're doing.</p>

<p><b>What are the KDE applications that are doing very good job? And the one that need lots of love and help?</b><br />
I think that a couple of very visible applications are doing very well, like Plasma, Dolphin, Amarok and the whole <a href="http://edu.kde.org/">KDE-Edu</a> suite. And I love KStars. But you’re right, a lot of other apps need work, maintainers, people to do the boring work that isn’t flashy or visible. Like the libraries.</p>

<p><b>What do you think of KDE software on Windows and MacOS X? Will we have more KDE users that way?</b><br />
I think it is important to continue those efforts to bring the power of KDE’s apps to others. It’s about freedom of your data and the applications you use, so I completely support those efforts. However, I still think the Unix world is more important, it is where we started and it is where we have to succeed.</p>

<p><b>You are working at Qt/Nokia. Do they pay you to develop for KDE too? What are you involved in at Qt?</b><br />
Yes, I work for <a href="http://qt.nokia.com/">Qt Development Frameworks</a>, paid to work on Qt only. Doing KDE work is not part of my day job, except when it comes to testing Qt with KDE software (but testing is not my work, it just happens that I do testing because I use KDE software with the latest Qt). My title is “Senior Product Manager”, which means I’m not a developer in writing. My main job consists of handling what people want of us, learning what they’d like us to do, then trying to ensure things get done in the best way for everyone, including the Qt developers.</p>

<p><b>What do you see as the future of Qt inside Nokia? Now, since Trolltech was integrated into Nokia what do you think? Can Open Source and Business go on together?</b><br />
The future of Qt inside Nokia is very bright. Qt forms the basis of the entire software strategy; Qt runs on all the mid- and high-end devices Nokia has released in the past years and you’re going to see Qt-powered devices coming out soon. What’s more, Qt is also at the very core of MeeGo, a free Linux distribution for embedded, mobile and similar devices, a partnership between Intel and Nokia. I think we’ve proven that business and Open Source can go together. But that’s not the end of the challenge: we still have more to do.</p>

<p><b>What motivates/keeps you motivated to work on KDE software?</b><br />
I don’t know. I just keep doing it :-) I think it’s the community. I feel so welcome that I don’t want to lose that.</p>

<p><b>If you could choose where to live what place you will choose and why?</b><br />
I did choose already. I like where I live now.</p>

<p><b>Have you ever stayed in Italy? If so, where?</b><br />
No, unfortunately my knowledge of Italy consists of the Rome Fiumicino and Milan Malpensa airports. I’ve always meant to go to Italy and stay more than just a few days, visit Rome, Pompei, Venice, Pisa.</p>

<p><b>What was the last book you read?</b><br />
The last fiction book I read was a Star Trek pocket book. I’m not sure which now, because they’re all stacked together after I finish them. The last non-fiction book was “The Innovator’s Solution”, a management book from the Harvard Business School press.</p>

<p>Thiago, thank you for your time and good luck for your projects,
<em>Giovanni</em></p>