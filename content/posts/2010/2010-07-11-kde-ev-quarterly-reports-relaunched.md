---
title: "KDE e.V. Quarterly Reports Relaunched"
date:    2010-07-11
authors:
  - "jriddell"
slug:    kde-ev-quarterly-reports-relaunched
comments:
  - subject: "Wow, that's a lot of"
    date: 2010-07-11
    body: "Wow, that's a lot of developer sprints! Awesome! :D"
    author: "vdboor"
  - subject: "Thanks for the report"
    date: 2010-07-12
    body: "The images make it a lot easier to take a glance at and see how big KDE is.\r\n\r\nGroup images from sprints are great! :)"
    author: "ArneBab"
  - subject: "Great"
    date: 2010-07-21
    body: "Thats very good. Thanks for share it.\r\nhttp://www.profischnell.com"
    author: "\u00fcbersetzer"
---
<a href="http://ev.kde.org/">KDE e.V.</a> is the legal body which holds our finances and represents the project in a range of issues.  Our Quarterly Reports have restarted with <a href="http://ev.kde.org/reports/ev-quarterly-2009Q2-2010Q1.pdf">a special bumper issue covering 2009 Q2 to 2010 Q1</a>.  It covers the many sprints which e.V. organises for our contributors to get together in person with their KDE teams.  It also covers events e.V. has helped KDE to attend and the working groups it oversees.

All KDE contributors are welcome to join KDE e.V. throught the <a href="http://ev.kde.org/getinvolved/members.php">membership process</a> to help out with e.V.'s activities.  If you or your company is interested in providing financial support to the KDE project on a continuing basis please visit the <a href="http://ev.kde.org/getinvolved/supporting-members.php">Supporting Members</a> pages on the KDE e.V. website. If you would like to financially support KDE as an individual please visit <a href="http://jointhegame.kde.org">Join the Game</a> the individual supporting membership programme site.
