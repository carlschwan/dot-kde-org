---
title: "KDE Ships November Updates: 4.5.3"
date:    2010-11-03
authors:
  - "sebas"
slug:    kde-ships-november-updates-453
comments:
  - subject: "Naming consistency"
    date: 2010-11-03
    body: "I'm wondering if I missed something on the naming convention. Why is there suddenly a new naming convention: Plasma workspaces plus the Platform. Is that supposed to be the same as the software collection ?\r\n\r\nEveryone was starting to get used to SC, I don't think it's a good idea to add new names on top of that, but I might miss the point ?"
    author: "Pom"
  - subject: "Missing..."
    date: 2010-11-03
    body: "The full changelog files are missing."
    author: "odysseus"
  - subject: "Well..."
    date: 2010-11-03
    body: "It hasn't changed, SC was a bit awkward, and wasn't really meant to be used in promo activities, but it's still used when we mean 'Everything that KDE releases at the same time on a 6 month cycle'.  The SC was always made up of the three main components, The 'KDE Development Platform' which is kdelibs and so on, the 'Plasma Workspace' which is our desktop offering, and the 'KDE Applications' which are apps that run anywhere, not just on our Workspcae.  The only real change recently has been deciding to rename the 'KDE Workspace' as the 'Plasma Workspace'."
    author: "odysseus"
  - subject: "I have heard that the duel"
    date: 2010-11-03
    body: "I have heard that the duel head problem in plasma is fix in this release but I cannot se it it the changelog.\r\nhttps://bugs.kde.org/show_bug.cgi?id=156475\r\nUnfortunately I found another problem with kwin on more screens \r\nhttps://bugs.kde.org/show_bug.cgi?id=255906\r\nbut this bug has a workaround .\r\n\r\nSo this might be the first release I can actually use (still waiting for debian package people to package it so I can test it again)"
    author: "beer"
  - subject: "plasma fixes"
    date: 2010-11-03
    body: "i also counted ~19 fixes (that i could find) to various Plasma components in this release as well. the list didn't make it to the release announcement, unfortunately (my fault for not doing the work, i suppose :)."
    author: "aseigo"
  - subject: "Jan. 2010?"
    date: 2010-11-03
    body: "Looks like 4.6 is long over schedule... ;)"
    author: "heini"
  - subject: "the changelog is very very"
    date: 2010-11-03
    body: "the changelog is very very incomplete, so if the developer said that its in 4.5.3, chances are very high it actually is."
    author: "Asraniel"
  - subject: "you can still do a blogpost"
    date: 2010-11-03
    body: "you can still do a blogpost about it ;)"
    author: "Asraniel"
  - subject: "Thanks, fixed ;)."
    date: 2010-11-03
    body: "Thanks, fixed ;)."
    author: "Eike Hein"
  - subject: "This makes me wonder each time"
    date: 2010-11-04
    body: "This makes me wonder each time, why are so many items absent from the changelog/announcement? Isn't it a standard procedure to update a changelog by making the commit?"
    author: "vdboor"
  - subject: "KDE PIM"
    date: 2010-11-08
    body: "Any News from the KDE PIM front?\r\n\r\nIs it still planned to release an Akonadi-enabled KDE PIM along with one of the KDE 4.5.x releases, or rather with KDE SC 4.6?\r\n\r\nI'm really interested in news about this...\r\n\r\nAnyway, keep up the good work you're all doing for us users!"
    author: "Zapp"
  - subject: "Almost"
    date: 2010-11-04
    body: "Actually the 'desktop' offering is Plasma Desktop. We also have a workspace for netbooks, Plasma Netbook. Other workspaces are in development, including Plasma Mobile and Plasma Media Center. Collectively, they are Plasma workspaces.\r\n\r\nThe SC still exists for as long as we find it convenient to release a group of stuff together (at the moment, we don't see and end to that). However, the SC excludes a lot of stuff, including digiKam and Amarok and includes stuff you probably don't use or even have installed.\r\n\r\nSo we're talking more about the stuff that is actual, self-contained blocks, like the (currently two) workspaces, the KDE Platform and the (individual) applications. You need the Platform for any of the others but beyond that you can mix and match - run Gtk apps in Plasma Desktop or KDE apps (whether part of the SC or not) in GNOME, XFCE etc.\r\n\r\nAnd yes, we need to be more consistent, clear and elegant in how we write stuff. Work is ongoing."
    author: "Stuart Jarvis"
  - subject: "if one use that workaround"
    date: 2010-11-04
    body: "if one use that workaround thery will proberbly  expirience some trouple with tab-ing between windows. I have been talking with some kwin devs on irc and they say that, they would proberbly not re-introduce things needed to use kwin in this kind of settup. Hes advice was to use another window manager than kwin. Just as you know if you ran into same problem as me"
    author: "beer"
  - subject: "thanks"
    date: 2010-11-05
    body: "Thanks for your answer.\r\n\r\nJust an idea, but having some kind of graphical representation to explain what all the \"blocks\" are and how they interact might help people. It could be on www.kde.org for example."
    author: "Pom"
  - subject: "Yes"
    date: 2010-11-05
    body: "Good idea\r\n\r\nWe had a 'map' in the original announcement (http://dot.kde.org/2009/11/24/repositioning-kde-brand) however, that could do with a few tweaks - less hierarchy and more things within other things. I was actually working on a new one a couple of days ago. It could fit well in the about KDE page on kde.org"
    author: "Stuart Jarvis"
  - subject: "Really bad looking bug"
    date: 2010-11-08
    body: "https://bugs.kde.org/show_bug.cgi?id=252881\r\n\r\nShould be cool if someone try to fix this horrible bug for 4.5.4..."
    author: "gnumdk"
  - subject: "It's looking like it will be"
    date: 2010-11-08
    body: "It's looking like KDE PIM with Akonadi will be 4.6."
    author: "steveire"
  - subject: "KDE4 PIM"
    date: 2010-11-08
    body: "We've just finished updating the KitchenSync tool last week which is a GUI for OpenSync (currently svn v0.40). I've uplifted the akonadi-sync plugin to work with the current opensync svn code.\r\n\r\nThere are still some \"strange things\" to be sourced out.\r\n\r\nWe expect to have it in official kdepim next, but it will take time, so might be kde4.6 or later will include it as a package or official code.\r\n\r\nif you can not wait that long and want to test it for us and help us improve it, then, please, download, compile, install and test. We'll be glad to hear from you. Testing means making backups and not complaining that something is not working as expected.\r\n\r\nfor the related software, use following links\r\n\r\nhttp://www.opensync.org/\r\nhttp://kde-apps.org/content/show.php/KitchenSync?content=132538\r\n\r\ncheers\r\n"
    author: "deloptes"
  - subject: "see my comment above"
    date: 2010-11-08
    body: "see my comment above"
    author: "deloptes"
  - subject: "In addition, some plasmoids"
    date: 2010-11-08
    body: "In addition, some plasmoids do not show anything.\r\nTry, for example, Ruby Analog Clock and Digital Clock (Date and Time plasmoids).\r\n"
    author: "MisterY"
  - subject: "Huh, this seems to be about"
    date: 2010-11-09
    body: "Huh, this seems to be about kitchensync, but what about KDE PIM itself? e.g. KMail 2?"
    author: "vdboor"
  - subject: "I just got a new cell phone"
    date: 2010-11-09
    body: "I just got a new cell phone and was looking for a good sync app, so a working kitchensync would be great indeed.\r\nMaybe I'll try to compile it later today and test it.\r\nWhat are the requirements? I'm currently running Kubuntu 10.10 - do I need KDE/OpenSync from trunk?\r\n\r\nAnd back to my previous question: Maybe I'll try the KDE PIM beta packages from the kubuntu experimental ppa and try to help with testing."
    author: "Zapp"
  - subject: "Please, adopt Bangarang!"
    date: 2010-11-19
    body: "I hadn't another place to make this comment, but I'll do it here.\r\n\r\nJuK is almost unmaintained. Dragon Player is in maintenance mode. Why don't you adopt Bangarang as their replacement for 4.7? This gives us tremendous benefits.\r\n\r\n1. It erases magically one of the most hated things to do by KDE developers: replace JuK legacy code with KDE 4 code.\r\n2. Bangarang NEPOMUK usage is exemplary. Bangarang can and should be used to benchmark NEPOMUK, optimize it, and tune it for future use cases (I'm thinking in Amarok ;))\r\n3. Bangarang sorely needs manpower. Maybe if we can divert some manpower from Dragon and put it into Bangarang we could have an amazing media player.\r\n4. The most obvious one is the last. Bangarang is one app. JuK and Dragon Player are two apps. This fact, and the fact that Bangarang makes extensive use of the pillars of KDE, makes this solution a lot easier to maintain in the long run.\r\n\r\nNow, how is this related to 4.5.3? Easy: Bangarang HEAD runs beautifully here, in SC 4.5.3, but it has some NEPOMUK hiccups. Maybe if we do this, we can iron those winks and give our KDE desktops a better experience.\r\n\r\nEDIT: 3 days after I wrote this, a new update flowed through Git, and I don't have NEPOMUK hiccups anymore!"
    author: "Alejandro Nova"
---
Today, KDE has made available <A href="http://kde.org/announcements/announce-4.5.3.php">4.5.3</a>, the November updates to the Plasma workspaces, the applications built on top of KDE's platform, and the platform itself. This release, as all x.y.z updates, contains bugfixes, performance improvements and localization updates only. As such, it's a safe upgrade and recommended for everyone running 4.5.2 or earlier. The update contains a number of fixes in Okular, Dolphin and a series of KDE games. Also, the new shared data cache continues to mature.
The rate of changes in the 4.5 series is slowing down while many developers already prepare for the 4.6 release planned for January 2011, which was soft-frozen only days ago.