---
title: "Annual Osnabr\u00fcck PIM Meeting Brings Exciting Announcements and Ambitious Plans"
date:    2010-01-14
authors:
  - "steveire"
slug:    annual-osnabrück-pim-meeting-brings-exciting-announcements-and-ambitious-plans
comments:
  - subject: "Great report!\nLooking forward"
    date: 2010-01-15
    body: "Great report!\r\n\r\nLooking forward to all of this maturing and hittingmy various devices. :)"
    author: "mutlu"
  - subject: "Part of Markus' write up is"
    date: 2010-01-16
    body: "Part of Markus' write up is here in German including more snow fight photos:\r\n\r\nhttp://www.linux-magazin.de/NEWS/Kalte-Kaempfe-bei-8.-KDE-PIM-Treffen\r\n\r\nHe'll be have more content in the next dead-tree version of the magazine.\r\n\r\nPS: Looks like the By: Field is messed up."
    author: "steveire"
  - subject: "Incontro annuale della squadra di KDE PIM a Osnabr\u00fcck"
    date: 2010-01-18
    body: "For the Italian readers here my translation:\r\n\r\n<a href=\"http://kslacky.wordpress.com/2010/01/18/incontro-annuale-della-squadra-di-kde-pim-a-osnabruck-grandi-novita/\">http://kslacky.wordpress.com/2010/01/18/incontro-annuale-della-squadra-di-kde-pim-a-osnabruck-grandi-novita/</a>"
    author: "giovanni"
---
On the second weekend in January the 8th incarnation of the annual KDE PIM meeting in Osnabrück, Northern Germany, took place. This meeting has a longer history than any other regular KDE contributor event. As in all previous years, the meeting was hosted by <a href="http://www.intevation.net/">Intevation GmbH</a>. Up to 20 contributors <a href="http://community.kde.org/KDE_PIM/Meetings/Osnabrueck_8">followed an agenda</a> of discussing design, marketing, attracting new developers, tools to assist and monitor progress, and the present and future of KDE PIM.

Friday saw most people arrive through the course of the day and the start of some technical discussions. This was followed by a dinner for all contributors funded by <a href="http://www.kolab-konsortium.com">Kolab Konsortium</a>, which is made up of Intevation and <a href="http://www.kdab.com">KDAB</a>.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/listening to the master.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/listening to the master_small.jpg" width="300" height="225"/></a><br />Listening to the master</div>

<b>KDE PIM targets deployment on mobile devices</b>

Saturday started with the exciting announcement that KDAB and Intevation would be working on making a functional PIM suite and Kolab Groupware client based on Kontact for mobile platforms. The redesign and porting work to the Akonadi framework in recent years with its clean separation between gui and core pim functionality will make this porting possible. Because all of the core functionality is shared between desktop and mobile clients, this should have in benefits in stability and features for both desktop and mobile applications. It also means that more developers will be joining the ongoing efforts to make the KDE Development Platform work on mobile platforms, reaping real benefits for all of KDE.

The state of KDE software on Windows has also made great strides in the past year. Kolab has made available a single click installer wizard for installing Kontact and all dependencies (including Qt and the KDE Platform) on Windows. More news on the windows platform is an estimate that <a href="http://gpg4win.org/">Gpg4win</a> has been downloaded a million times in the last year. Gpg4win includes the Kleopatra crypto framework, which is built on the KDE Platform, which makes it one of the largest existing deployments of KDE software on Windows.

<b>The battle of Osnabrück</b>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/The Great Battle.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/The Great Battle_small.jpg" width="300" height="225"/></a><br />Onset of the Battle of Osnabrück</div>

The annual contributors' photo in Osnabrück gave opportunity for spending some time in the snow which the Great Glaswegian Warrior Adams recognised as the right time to join forces with Georg the Greve and assert their might upon the rest of the sprint attendees. What followed was a mix of massive flukes and embarrassing misses. After the Turning of the Dutchman, the battle finally ended with the epic Clash of the Irishman and the Glaswegian, spelling the demise of both.

<b>SyncEvolution is Intelligent Design</b>

Device syncing is one of the biggest challenges of modern PIM applications. This year we were lucky to have Patrick Ohly of <a href="http://syncevolution.org/">SyncEvolution</a>, which is used in Moblin, attending the KDE PIM contributor sprint. Patrick gave a talk about the challenges of syncing with multiple devices which each support different subsets of functionality and protocol, and the history of SyncEvolution.

As both SyncEvolution and Akonadi have plugin systems, Patrick was able to make the first steps towards communication between the two systems over the course of the weekend. Sharing that syncing technology and not attempting to reinvent it is clearly the desired solution, and presents some interesting possibilities.

With SyncEvolution there is an interesting alternative to OpenSync available, which has been used as base for the KDE PIM syncing stack in the past. OpenSync saw a recent release freezing the API, so there is some healthy competition, which is promising to create better syncing solutions for KDE PIM than ever.

<b>Reaching a wider audience</b>

Another welcome attendee at this year's Osnabrück meeting was Markus Feilner of the German language Linux Magazine. Markus is writing an article about Akonadi and its role in the KDE Platform, and how developers are making use of it and Nepomuk in future applications. This was a good opportunity to demonstrate where we are going and the possibilities of our new platforms to someone who is not a regular contributor to KDE PIM, as well as show how the open development model works in the KDE community.

<b>Current state and future plans for KDE PIM</b>

Kontact based on Akonadi will start to reach users with the release of KDE Software Compilation 4.4 in January. KAddressBook will be the first of the core Kontact applications ported to the Akonadi framework and has seen a lot of QA work in this cycle, but as usual with such a large change in the platform, there may be some new bugs and feature regressions. Feedback is welcome.

Planning for KDE SC 4.5 has started too with the goals of releasing Akonadi ports of KMail, KOrganizer, KJots, KAlarm and possibly Akregator to users. The Akonadi port, which includes Nepomuk integration, has been done in a branch and is ready to be merged to trunk. The full 4.5. release cycle is focused on stabilizing the port. This will be the base for greatly improving speed, stability and searchability.

With the growing maturity and stability of kdelibs in 4.4, it was decided to base development of kdepimlibs and kdepim 4.5 on the 4.4 version of kdelibs. This means that while trunk development versions of kdepimlibs and kdepim will work with kdelibs trunk, they will also continue to compile and work on kdelibs 4.4. This commitment will make it even easier for new developers to contribute to kdepim because they will be able to use distribution supplied kdelibs, kdesupport and Qt packages. It will also simplify development for existing kdepim developers who will not have to track the moving target with changing dependencies. This is tying in with the same policy which was also used for parts of the KDE 3 development cycle before.

The role of svn in KDE PIM development and a move to git was also discussed at the meeting. There are currently many advantages in using svn for the PIM developers and stakeholders managing many maintained branches. Some existing tools and workflows for svn will not work for git, and changing between the systems will cause a change in momentum which will need to be predictable and managed. Dialogue has been opened with the rest of the community working on git migration to see how the needs of PIM development can be met.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/team picture.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/team picture_small.jpg" width="300" height="225"/></a><br />the team</div>

<b>The role of KDE PIM and Free Software in privacy management</b>

In times where many people are migrating to increasingly hosted services for PIM data, questions arise about where KDE PIM as a smart client (or rich client) fits into the future landscape of PIM applications and how people will use them. Bernhard Reiter of Intevation and the FSFE presented on the benefits of rich clients and tries to answer the question of why we are here. Part of the benefits of Akonadi are aggregation of data from various different services and locations. Being able to spread fragments of data between different hosting providers or storage locations, but have them aggregated on a rich client on a desktop or phone is a far better option for the security or privacy conscious. Aggregation also allows a split of the data between different roles or identities. For example, many people carry two mobile devices, one for work communication, and one for non-work communication. By bringing both together on one multi-identity application, it is possible to maintain the split of where the data is  (your social calendar data does not have to mix with your work meeting calendar data), and have it presented together in the application. The same application can then be used with different data when used in different roles, or different times of day.

<b>Tools and Testing</b>

Volker Krause demonstrated the <a href="http://my.cdash.org">CDash dashboard</a> website hosted by kitware and used to run and analyse nightly builds of KDE modules. CDash can be easily used by any developer who wants to submit results to the system (especially for exotic platforms) and helps keep track of build failures, making sure unit tests pass and cover significant parts of the code, and helps track memory leaks too.

Bernhard Reiter demonstrated the <a href="http://treepkg.wald.intevation.org/">treepkg</a> and <a href="http://saegewerk.wald.intevation.org/">saegewerk</a> tools used to create debian packages of the enterprise version of kontact.

All users are encouraged to upload KDE4 builds to <a href="http://my.cdash.org">my.cdash.org</a> as one aggregation point will produce the most useful data for developers. The Debian package builds run by Intevation will be submitting their results to CDash in their next incarnation. It is quite easy for anyone building a kde module. Just use "ctest -D experimental" instead of "make" next time you are building.

<b>Goals of attraction</b>

Before dinner on Saturday there was a brainstorming session about marketing KDE PIM. We tried to come up with ideas of how we can better convey a message about the cool stuff that is happening in development, how others can help spread the message and how we can attract new contributors to KDE PIM. Ideas ranged from the obvious like blogging and microblogging more and creating a KDE PIM planet, or getting messages to multipliers like journalists and re-tweeters, to the questionable like a <em>Developers of KDE PIM</em> calendar or release songs. Hopefully we will be able to make ourselves more visible through the channels already in place in the KDE community and will work closer with the promo folks to make that happen. Anyone who thinks KDE PIM is doing cool things can help by tweeting, writing for the release announcement, or blogging. Promotion and writing is a great way to contribute to your favourite applications. You can easily get in contact with developers by emailing kde-pim@kde.org for questions. Anyone who writes blogs about KDE can easily be added to planet.kde.org.

The marketing discussion continued on Sunday when we tried to flesh out the ideas and considered the perception of KDE PIM within the broader contributor community and user base. PIM and "productivity applications" do not have the same inherent appeal or bling currently as other notable parts of the KDE SC, but it does not need to remain that way. Kontact as a suite will likely remain something to appeal to the needs of businesses and serious people, but with the present power of the Akonadi and Nepomuk frameworks and the new bling possibilities in Qt 4.6 and beyond it becomes easier to cater for the needs of people who do not have 100,000 emails and a groupware server connection, but do care about Twitter, Facebook and bling. We will be trying to put some examples together of ways to use the new framework which are not divided by communication transport mechanism (email, news, blogs), but allows presenting many ways of communicating with a person in a standalone application. Nepomuk is an obvious enabler of these kinds of new generation of applications, and we hope to attract new developers who can experiment and try out new ways of interacting with people and their data.

<b>Conclusion</b>

The weekend at Osnabrück was productive as usual and enabled many design discussions and face-to-face meetings which do not ordinarily happen. KDE PIM will be even more active in the coming year and hopes to expand its contributor base in all areas from development, testing to promotion, artwork and more. The future looks bright for PIM in KDE SC 4.5, and if you want to be a part of it, just let us know.