---
title: "Desktop Summit 2011 Extends Deadline for Call for Hosts"
date:    2010-05-15
authors:
  - "cschumacher"
slug:    desktop-summit-2011-extends-deadline-call-hosts
---
The KDE and GNOME communities are looking for a host for the Desktop Summit 2011, the prime free desktop software event in 2011. The Desktop Summit is the joining of the annual conferences of <a href="http://www.kde.org">KDE</a> and <a href="http://www.gnome.org">GNOME</a>, following up on the success of the <a href="http://grancanariadesktopsummit.org">Gran Canaria Desktop Summit</a>. To give potential hosts some more time to prepare quality proposals, the boards have decided to extend the submission deadline to June 9th 2010.

For details see the <a href="http://dot.kde.org/2010/03/16/desktop-summit-2011-call-hosts">call for hosts</a>. Please send your proposals for hosting the Desktop Summit 2011 to the boards of KDE e.V. (<a href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>) and the GNOME Foundation (<a href="mailto:board-list@gnome.org">board-list@gnome.org</a>) no later than June 9th. Please also feel free to contact us in case of any questions.