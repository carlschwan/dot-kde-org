---
title: "KDE SC 4.4 Release Candidate 2 Out: Camp KDE"
date:    2010-01-25
authors:
  - "sebas"
slug:    kde-sc-44-release-candidate-2-out-camp-kde
comments:
  - subject: "Good work  and old machines"
    date: 2010-01-25
    body: "First of all, great work guys. The very few annoyances that were left in RC1 seem to be gone, this is going to be an amazing release.\r\n\r\nNow I just really miss  some way to make kde feel lighter and faster. I'm working in a very old machine P4 512MB RAM. Moved to fluxbox now and the speed difference is simply amazing, even though I use the same apps including many KDE ones. \r\n\r\nOf course I understand KDE and fluxbox are very different beasts! That said maybe something like a more advanced version of the old KPersonalizer of KDE3 could help? This would be an application that lets you easily adapt KDE to your machine, turning off certain functionalities to make it lighter. It might be the case that I can already improve it in many ways by tweaking configuration. Maybe it is possible for me to make krunner to show almost instantly instead of taking a few seconds. But it's hard to know what to do and how. A dedicated app or control panel module could very useful. \r\n\r\nAlso if it case that some of optimizations would have to be performed at compile time, maybe it would be worth it to provide alternate packages (kde4light) and let this new KPersonalizer configure the desktop to use normal or light when adequate. \r\n\r\nAnyway again, great RC. \r\nLooking forward to the final :)"
    author: "jadrian"
  - subject: "Wow!"
    date: 2010-01-25
    body: "Thanks very much for the speedy release. Isn't it a day before it is due? I am going to do an update right away :)"
    author: "Bobby"
  - subject: "> Moved to fluxbox now and"
    date: 2010-01-25
    body: "> Moved to fluxbox now and the speed difference is simply amazing, even though I use the same apps including many KDE ones. \r\n\r\nSounds like either:\r\n * you're out of memory, so the system needs to swap all the time (perhaps nepomuk is using too much)\r\n * your graphics driver can't deal with KWin effects well (turn of compositing)\r\n\r\nOther then that, I can't imagine a reason why running the same KDE apps outside the KDE workspace happen to be any faster."
    author: "vdboor"
  - subject: "which options?"
    date: 2010-01-25
    body: "My KDE 4 is speedy on hardware that's a lot slower than yours. It seems to depend on a lot on your video driver. Just for fun, try the VESA driver and see how things fare. (Yes, there are plenty cases where VESA is actually faster than anything else -- sad but true.)\r\n\r\nYou're talking about switching certain options off? Can you identify those that slow down your system? Without that information, it would be just guesswork and probably not help at all."
    author: "sebas"
  - subject: "krunner via DBus"
    date: 2010-01-25
    body: "Does it help to call krunner like this (shell command)? \r\n\r\n<code>    dbus-send --type=method_call --dest=org.kde.krunner /MainApplication org.kde.krunner.App.display\r\n</code>\r\n\r\n(It has to be running for that to work - you have to call \"krunner\" at least once - i.e. at startup) "
    author: "ArneBab"
  - subject: "Heya. \n* Compositing is off"
    date: 2010-01-25
    body: "Heya. \r\n\r\n* Compositing is off and so is nepomuk\r\n\r\n* Memory. Yes that's probably the main issue here, I agree. It is using swap all the time. However even with emacs and konsole and okular being the only apps running it feels quite slow. I'd expect 512MB to be enough for that so I'm not sure. "
    author: "jadrian"
  - subject: "Compositing is off so I"
    date: 2010-01-25
    body: "Compositing is off so I wouldn't expect that to be the case, but have a 2nd hand geforce 6 which waiting for me to have some time to install it, so that might help me a bit. \r\n\r\nAs for identifying what slows it down not yet really, not without spending some time which I can't really afford rigth now. That's why something along the lines of kpersonalizer would be handy. It wouldn't have to automagically guess the optimal solution, but a bunch of settings grouped together concerning options that generally may improve a systems performance, could make it easy in many cases to find out the guilty parts by playing around with a few options. "
    author: "jadrian"
  - subject: "kde is fast on my machine with 512 mb ram"
    date: 2010-01-25
    body: "i have one machine with an amd sempron 3000, 512 mb ram and ati radeon RV280 5960.\r\n\r\nkde 4.3 on opensuse runs about as fast on the machine as on my laptop, which has better specs and 2 gig ram..\r\n"
    author: "whatever noticed"
  - subject: "Beautiful wallpapers"
    date: 2010-01-25
    body: "I was simply amazed today: my I.T teacher is now in love with the Macs at college and forgets how to use other machines (she's used to the multi-touch touchpad, and my Dell netbook obviously doesn't have that...), but today she said, \"I'm going to change the background image, 'cause the one I'm using is a bit boring (basically all black with some pink swirls in the corners).\"\r\nLater, she complained that all the wallpapers for the Mac SUCK. I showed her the full list of KDE wallpapers, and we picked out 5 or 6 that she really liked.\r\n\r\nRemember that design goal in Aaron's talk, oh so long ago? \"We want the user to see a proprietary system side-by-side with KDE, and still say, 'I want that one!'.\" You're getting damn close. Maybe I should get a more attractive laptop... ;)"
    author: "madman"
  - subject: "Turning compositing off isn't"
    date: 2010-01-25
    body: "Turning compositing off isn't enough. When you change your drivers to plain 2D you will get additional speed boost.\r\n\r\nSome programs are written... nonchalantly and are trying to get 3D effects even with compositing off, effects aren't visible or borked but resources are wasted."
    author: "mikmach"
  - subject: "."
    date: 2010-01-25
    body: " ."
    author: "wintersommer"
  - subject: "lol"
    date: 2010-01-25
    body: "by the price of 10\u20ac for 1gb ram , you should just buy more...\r\n\r\ni got 8 gb ram and i dont care how much ram kde takes.\r\nthats 80\u20ac :) ...\r\n\r\ni think the focus shifted when you look @ the ram prices these days...."
    author: "wintersommer"
  - subject: "Some bugs"
    date: 2010-01-25
    body: "Happy that you fixed the plasma panel crash, it feels much more stable :)\r\n\r\nPlease, pay extra attention to Nepomuk/KMail issue nr. 219687, because KMail must not be released unless it get fixed.\r\n\r\nKeep up good work!"
    author: "szotsaki"
  - subject: "KDE SC 4.4. Boo Yah! :D"
    date: 2010-01-25
    body: "Congratulations team KDE! 4.4 is looking like a shiny, sexy beast of a release. Everything is getting nicely polished.\r\n\r\nCan anyone on an OS other than Kubuntu confirm if Dolphin's search box works for you? For some reason searching with nepomuksearch:/whatever in dolphin seems to work, but not using the searchbox. Weird.\r\n\r\nAnyway, I am absolutely *loving* the new automount drives options combined with the lightning speed of Virtuoso combined with krunners in Kickoff. This is the power + integration I always dreamed of in KDE. \r\n\r\nThankyou!"
    author: "Bugsbane"
  - subject: "I hope..."
    date: 2010-01-25
    body: "...If finally the \"1 activity per desktop\" feature is fixed (bugged since KDE 4.3 RC1) because will be a very good KDE SC 4.4.0 release by the number of optimizations and new features."
    author: "xapi"
  - subject: "Yes!"
    date: 2010-01-26
    body: "Nepomuk seach in Dolphin works very well on Archlinux."
    author: "mutlu"
  - subject: "Activities"
    date: 2010-01-26
    body: "I don't know what it would need to be like for you to consider it fixed. It does work a bit better than in 4.3, but a major revamp is planned for 4.5, AFAIK."
    author: "mutlu"
  - subject: "Superb release"
    date: 2010-01-26
    body: "Congratulations to all developers, bug triagers, translators, artists, promoters and usability experts who give us KDE SC! This is a superb release. Apart from a few minor bugs, it feels really stable and certainly much faster than 4.3. It's amazing how much better the software gets with every release.\r\n\r\nCheers!"
    author: "mutlu"
  - subject: "You're not the only one with"
    date: 2010-01-26
    body: "You're not the only one with this problem. I've gotthe same problem (I'm using kubuntu as well)\r\nThere's already a bug report:\r\nhttps://bugs.kde.org/show_bug.cgi?id=222687"
    author: "chris"
  - subject: "If you enable the option"
    date: 2010-01-26
    body: "If you enable the option \"different activities for each desktop\" you will get new extra activities with default elements and/or as addition on every restart the order of activities linked to a desktop is changed using one of those new default activities.\r\n\r\nYou don't see that is needed that it mustn't create extra activities and not swap randomly activities?\r\n\r\nThe annoying bug was reported before the 4.3 release without a fix so this feature only can be used to \"demostrate\" what could do KDE4 activities but not much more.\r\n\r\nTo be exact is this bug report :\r\nhttps://bugs.kde.org/show_bug.cgi?id=199729\r\n\r\nCurrently I can't test it so If someone had verified it if still exist the bug will be helpful..."
    author: "xapi"
  - subject: "Not everyone can add more RAM (Reply to wintersommer)"
    date: 2010-01-26
    body: "I should have foreseen that the login would drop the link to the original comment.\r\n-- -- \r\nSome of the older machines don't support a lot of RAM, and in particular if it is a laptop it can be virtually a non-issue.\r\n\r\nSo either our favourite desktop environment has to run adequately or see itself being replaced. I am using an Lenovo IBM ThinkPad X61s with 1 GB RAM, currently with openSUSE.org 11.1 and KDE 4.1.3 - and I am considering taking Moblin for a spin when it is time to upgrade. I don't expect openSUSE.org 11.2 with KDE 4.4 to be faster than my current setup, because one keeps adding new features and daemons.\r\n\r\nThe road to new users is more bells and whistles with every release, so I don't have an argument with that. The issue is how easy it should be to adapt the new release to an older computer. I suppose ideally one should have a common codebase but two software compilations, a light-weight \"moblin\" version in addition to the full version."
    author: "kjetil-kilhavn"
  - subject: "phonon fix"
    date: 2010-01-26
    body: "Amarok stopped losing the sound device, so I can use it again! Many thanks! "
    author: "ArneBab"
  - subject: "KPersonalizer and old hardware"
    date: 2010-01-26
    body: "From my experience KDE4 is not slow (actually it's fast) on old hardware, but it's occasionally slow on some hardware. When I had some problem it was usually somehow related to the graphic card/drivers (don't ask me details... :-)). I had KDE4 (4.0 actually) run beautifully and smoothly on an old samsung PIII notebook with 512MB ram. The desktops cube worked flawlessly ;-).\r\nOn a very recent Acer Core2Duo 2GB ram with ATI graphics card I cannot use effects, otherwise it crashes every 10 seconds. On a acer notebook of a friend with an ATI card I installed the same distro (OpenSUSE 11.2) and fortunately it was great :-)\r\n\r\nNow, all this is to say that KDE4 is not slow by itself (actually it's really fast for what it does), but, for some reasons unknown to me, its performance on some hardware appears randomly slow.\r\n\r\n\r\nOn another note, I fully agree about KPersonalizer. Why do we need it?\r\n1) because we like a quick way to customize KDE for that particular hardware/usage,\r\n2) because even though ram is cheap, someone doesn't want to buy something \"extra\" as a principle and because we know KDE4 can actually run well on a PIII with 512MB ram (perhaps no effects, no nepomuk, but KDE is greatly useful even without those),\r\n3) because what's wrong with it?\r\n\r\nRegards,\r\nKlaus\r\n\r\n"
    author: "klaus"
  - subject: "Thanks for the tip, I will"
    date: 2010-01-26
    body: "Thanks for the tip, I will try it as soon as I log in to KDE in that computer. "
    author: "jadrian"
  - subject: "or not... sorry."
    date: 2010-01-26
    body: "or not... sorry. "
    author: "ArneBab"
  - subject: "Fixed now"
    date: 2010-01-26
    body: "According to the bug report linked in the entry (of which this bug is a duplicate) it should be fixed for 4.4 final."
    author: "einar"
  - subject: "I agree.  Activities was one"
    date: 2010-01-26
    body: "I agree.  Activities was one of the features of the KDE4 series that was touted by the devs.  For a while I could not see how they differed that much from the standard multiple desktops.  However, once \"per desktop\" activities were introduced, I began to see the benefit and have used them ever since.  Hopefully this will be somewhat stable when final is released, otherwise activities really aren't that useful for me.\r\n\r\nOther than that, SC 4.4 is really nice."
    author: "cirehawk"
  - subject: "Is it really KDE?"
    date: 2010-01-27
    body: "Is it really KDE that's slowing down your system? My brother has an IBM Thinkpad R40e running openSuse 11,2 and the latest stable version of KDE and he doesn't have a speed problem. \r\nI am the one here who is always crying for more speed but believe me, I can feel it in KDE 4.4 SC RC2. It's pretty damn fast! Well my machine is not bad but it's two years old but the latest stable version of KDE is obviously slower than 4.4 RC2 with all the bells and whistles. RC2 is even faster than RC1.\r\nCheck your installation and settings because I am not convinced that KDE is the problem. KDE 4 gets faster with every release on the same machine, at least in my case."
    author: "Bobby"
  - subject: "Not always easy"
    date: 2010-01-27
    body: "It is not always an easy option to upgrade your RAM. Older systems, especially laptops, may be harder and way more expensive to upgrade than modern systems. That is, if the system supports the bigger/more memory modules at all!\r\n\r\nIMHO, memory consumption should continue to be a focus of optimization. That will also make it easier to move applications to the mobile space, where memory is at a premium once more."
    author: "andresomers"
  - subject: "wallpapers"
    date: 2010-01-27
    body: "Thanks, This as been our goal since the beginning of oxygen, there is no reason we cant do better than the rest in that department, maybe we can do better, the trick is not giving up and improving a little bit every day, not all is done but I think overall the path so far is very positive. KDE sc is on a steady polishing path that will deliver an extremely mature and well designed desktop, wont from night to day and most people wont notece it, but as great Galileo said \"E pur si muove\" ( And yet, it moves). "
    author: "pinheiro"
  - subject: "Amen that.\nI sometimes got"
    date: 2010-01-27
    body: "Amen that.\r\n\r\nI sometimes got tired of people saying: new hardware is cheap, just buy a new one.\r\nThey forget some facts:\r\n\r\n- only americans earn money in dollars\r\n- only europeans earn salary in euros\r\n- the world is a big place, with more than just rich people\r\n- sometimes you just want to spend money in something else\r\n- KDE4 is FAAAAAAAAAAAAAAT, no matter that it runs in netbooks, it does not run as well as it could or should\r\n\r\nOK, I HAD to say that, now I'm fine and happy again :D"
    author: "protoman"
  - subject: "I found that using KDE with a"
    date: 2010-01-27
    body: "I found that using KDE with a different window manager than kwin, really sppeds it up. But for the RAM part, I blame Plasma (sorry plasma guy & aargon).\r\nWhile it is nice, have all whistles and bells, cool animations and you can put a lot of useless (sorry plasmoid developers) widgets into your desktop, it takes a LOT of resoures that kdestop+kicker did not.\r\n\r\nI would like if KDE had a kind of slim build also, I would get rid of:\r\n\r\n- Plasmoids (who cares about desktop fancyness when you can barely open 2 apps at same time?)\r\n- Neopomuk (I do not care about it, most people today do not use kmail, just go gmail and google will keep your contacts and data in sync).\r\n- animations (like, dragging a windows never should show it's contents)\r\n- akonadi this is my \"favorite\". It is VERY slow and takes a  disk space to build it's database for something some users just do not care about. Why the hell this is on by default in ANY distribution of KDE? Desktop search should be an optional feature, not mandatory.\r\n- replace kwin for something lighter. and do not blame desktop effects, most people just do not use them, and even with it turned off, kwin is VERY slow nowadays.\r\n\r\nWith those configurations, KDE would be much faster.. but seems like the KDE4 direction is to add more animations, graphic effects and such, not making the beast faster. Just look at the 4.4 release, there is nothing new for the user in the desktop, that are not new plaismoids, graphics, etc... Sad thing :-(\r\n\r\nThat said, I do use KDE4, I do like it, but it makes me sad that all promisse of faster KDE that Qt4 bought is just vanished by all those layers and effects added to KDE."
    author: "protoman"
  - subject: "May be you should stick to KDE 3.5 for a while .."
    date: 2010-01-28
    body: "My machine is 5 years old and I there are some problems with performance, but not in the way you describe. Following the consumption of resources in Systemmonitor shows that the main problems are Kmail and Akregator. Both apps which have not been transformed to the new and shiny KDE4-technologies yet. As I understand it, both will be transfered to Akonadi with KDE4.5. So it is worked on, even though I wished the increasing speed problems in Kmail would be fixed for KDE 4.4.\r\nThe other things you complain about are mostly configurable, even though the problems you describe appear largley as things which hopefully are going to be fixed/ improved. If you continue to use KDE4 then please write bug and wish reports."
    author: "mark"
  - subject: "\"Performance-Analyzer\" for KDE4-Systemsettings"
    date: 2010-01-28
    body: "Hi,\r\n\r\nI am really looking forward to see KDE 4.4 final. I read that much good reports, that I installed some test-system to play with the beta-releases.\r\n\r\nBut I have to admit, that I have performance problems, too. For example the important task \"ALT+F2\" for krunner takes too long. In my office on KDE3, I use it duzend of times each day and I only have to think about \"ALT+F2\" to see it. This is not the case with KDE 4 any more.\r\n\r\nBut reading about performance issues and KDE 4 gives you thousands of hints from hardware to software, which you would have to test until you find the solution for your special hard/software combination.\r\n\r\nTo get once and for all times rid of these comments (you are reading right now ;) ):\r\n\r\n==>\r\n==> What about \"Performance-Check\" in KDE4-Systemsettings ? <==\r\n==>\r\n\r\nEveryone who would post any comment on \"KDE4 is soo slow\", you simply point to the integrated performance check, which gives you only the hints for tuning, which help your spezial hard/software combination.\r\n\r\nI am looking forward for any comments on the idea of an KDE specific performance tool.\r\n\r\nGreetings\r\nJens"
    author: "Jens"
  - subject: "Printing and Dual-head support?"
    date: 2010-01-29
    body: "I know I've already asked the same question when KDE 4.3 RC3 came out, but has printing support been improved in any way in KDE 4.4?\r\n\r\nThe printing system in KDE 4.3 is, in many cases, lacking functionalities when compared to its 3.x counterpart. In some applications, printing is also somewhat broken - I've stopped using Konqueror in favor of Firefox because it was way too often unable to spit out on paper anything remotely similar to what's on screen.\r\n\r\nA second question is about support of dual-head display systems. I'm using a Cintiq graphical tablet that includes a built-in screen, so this is a very important setting for me. KDE4 never worked properly with it - the 2nd screen would stay black with no KWin available. I ended up using another WM/DE on it, but that's merely a work-around. This is disappointing, as multihead worked very well under KDE 3.5.\r\n\r\nHas any progress been done regarding support of such multiple screen settings?"
    author: "Lauwenmark"
  - subject: "multiple screens work"
    date: 2010-01-31
    body: "multiple screens work reasonably well here, so something must be special about your setup. did you make a bugreport?"
    author: "Asraniel"
  - subject: "Release candidate not stable?"
    date: 2010-02-02
    body: "\"KDE SC 4.4 RC2 is not stable software, as such, it is not suitable for everyday or production use.\"\r\n\r\nA release candidate is supposed to be a stable version that would become the release unless there are critical bugs discovered. Ideally the \"release candidate\" and the released version are identical. If it is not suitable for everyday or production use then it is a beta.\r\n\r\n\r\n"
    author: "vinum"
  - subject: "Drivers?"
    date: 2010-02-02
    body: "NVidia drivers?"
    author: "vinum"
  - subject: "This is funny"
    date: 2010-02-02
    body: "I find it funny that devotees are now are complaining about speed, and also dare suggest that plasmoids etc should be optional, and I even see people get positive scores - amazing. Those of us who complained about bloat&slowmess at the time of 4.0, 4.1, 4.2... were ridiculd and told to STFU and wait for 4.n+1. So what changed?\r\n\r\nAs for speed, on my home \"test desktop\" for example, 3D OpenGL applications like googleearth flies along just fine, but moving around plasmoids is a sluggish and lugging affair. I find that just hilarious. I also have kde 4.3.5 on my eeepc-901 (2GB RAM), along with kde 3.5.10 - one is close to unbearable, the other runs excellently, guess which. And googleearth works fine on the eeepc as well (albeit with \"atmospheric effects\" turned off)."
    author: "kolla"
  - subject: "Not yet"
    date: 2010-02-03
    body: "We define a RC as a release in which only critical bugs will be fixed. So it should be stable, sure but with so many applications in the KDE software compilation it is unrealistic to expect no bugs. Besides, we're just being cautious here.\r\n\r\nIf you're just releasing one application, like Firefox, it's not hard to do things the perfect way. What we do is not comparable to Windows 7 or even Mac OS X, as that have far less functionality. And we manage to release a steadily improving product every 6 months. Nobody beats us to that, so please give a little credit even if things don't completely live up to your expectations in terms of used words..."
    author: "jospoortvliet"
  - subject: "really?"
    date: 2010-02-04
    body: "\"hose of us who complained about bloat&slowmess at the time of 4.0, 4.1, 4.2... were ridiculd and told to STFU and wait for 4.n+1. So what changed?\"\r\n\r\npersonally i just got tired of dealing with it and have been busy actually improving things rather than spending time dealing with people complaining about things they have a cursory knowledge of. outside of people stuck with craptastic x.org drivers, it seems to be paying off for most.\r\n\r\n\"I find that just hilarious.\"\r\n\r\nit's hilarious that x.org drivers are bad at accelerating (esp with Qt apps), but OpenGL calls to the hardware are fast? that's not hilarious. it's pretty sad and pretty much the status quo in x.org."
    author: "aseigo"
  - subject: "*sigh*"
    date: 2010-02-04
    body: "you've confused Nepomuk with Akonadi, there have been large numbers of performance improvements, my name is Aaron not Aargon (i am not a nobel gas ;) but the most amazing statement you made is this one:\r\n\r\n\"Just look at the 4.4 release, there is nothing new for the user in the desktop, that are not new plaismoids, graphics, etc... Sad thing\"\r\n\r\nnow go read this:\r\n\r\nhttp://websvn.kde.org/*checkout*/branches/KDE/4.4/kdebase/workspace/plasma/design/CHANGELOG-4.4\r\n\r\nthen come back and repeat what you said."
    author: "aseigo"
  - subject: "So..."
    date: 2010-02-05
    body: "So why is this not a problem with KDE3?\r\nAh, I know - it doesn't _rely_ on the buggers, like KDE4 does.\r\nAnd _that_ is exactly where the problem is."
    author: "kolla"
---
The <a href="http://kde.org/announcements/announce-4.4-rc2.php">last release candidate of KDE SC 4.4</a> is out. Following up the first release candidate, another number of bugs has been searched and destroyed. This releases fixes some problems found during the earlier testing phase, and acts as a last line of defense before KDE SC 4.4.0 will be released on February, 9th 2010. Please give this release another spin, report last-minute showstoppers to us using <a href="http://bugs.kde.org">KDE's Bugzilla</a>.