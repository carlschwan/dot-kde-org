---
title: "Behind KDE: Ingo Malchow, the Guy Behind Many Websites"
date:    2010-08-17
authors:
  - "tomalbers"
slug:    behind-kde-ingo-malchow-guy-behind-many-websites
comments:
  - subject: "Cool to see these interviews"
    date: 2010-08-18
    body: "Cool to see these interviews being started again!\r\nAnd nice work on the website! Looks awesome :D"
    author: "vdboor"
  - subject: "Ingo Rocks!"
    date: 2010-08-18
    body: "Taking the chance to say that Ingo Rocks! he as been doing an insane amount of good work, thanks! "
    author: "pinheiro"
---
You are now witnessing the kickoff interview of a new limited series of <a href="http://www.behindkde.org/">Behind KDE</a>. This series will show you who the KDE Sysadmins are and also introduce you to the people directly around that dedicated team. In the coming weeks you will get to know the somewhat weird creatures that work night and day to provide all the KDE contributors with state of the art infrastructure and make sure the transition to Git will be made possible. First in line is <a href="http://www.behindkde.org/node/85">Ingo Malchow</a>, the guy responsible for Capacity (the framework that powers kde.org) and a key person within the KDE Forum team.

Ingo is already famous among the forum administrators and is one of the latest additions to the KDE sysadmin team. In this interview you will meet the person whose hobby is the same as his day job, the one who adjusts the color of the sky in his dreams by means of a stylesheet and the one who can smell an ugly font from 10 clicks distance. Enjoy <a href="http://www.behindkde.org/node/85">the interview</a>!