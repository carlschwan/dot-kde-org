---
title: "KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.0"
date:    2010-08-10
authors:
  - "sebas"
slug:    kde-releases-development-platform-applications-and-plasma-workspaces-450
comments:
  - subject: "Did I jump the gun?"
    date: 2010-08-10
    body: "Where are kdepim, kdepim-runtime?"
    author: "stumbles"
  - subject: "Cool! "
    date: 2010-08-10
    body: "System is already running at full force to get the new version! \r\n\r\nGot to see the improvements over RC3! \r\n\r\nMany thanks to all KDE hackers (including documenters, translators, PR people and all the others) for getting 4.5 done! "
    author: "ArneBab"
  - subject: "they will come in 4.5.1,"
    date: 2010-08-10
    body: "they will come in 4.5.1, since the change to akonadi isn\u2019t polished enough yet \u2014  and noone wants to risk that KDE users lose their emails\u2026 "
    author: "ArneBab"
  - subject: "Sounds like a good plan."
    date: 2010-08-10
    body: "Sounds like a good plan. Compiling as we speak."
    author: "stumbles"
  - subject: "KDE PIM in 4.4 and 4.5"
    date: 2010-08-10
    body: "Actually, I don't think we'll make 4.5.1 for the Akonadi-based PIM. It all depends on how the beta phase works out. Migrating Kontact to AKonadi is a pretty big step, so we don't want to rush things here.\r\n\r\nKontact 4.4 is still actively maintained and continues to work, so that's what users will use in 4.5. We did not upgrade the version number of KDE PIM, since it's essentially what's in 4.4, anyway.\r\n\r\nThere will be a beta of Akonadi-PIM in the next days, and from there we'll see how it goes. The PIM team pays special attention to migrating your data, and if the new Kontact doesn't work for you yet, it's easy to switch back as config will be retained for both versions, also cached data (such as disconnected IMAP accounts) can be kept if you're not sure you're ready to switch."
    author: "sebas"
  - subject: "kdepim"
    date: 2010-08-10
    body: "KDE PIM was been delayed and was not part of the 4.5.0 release of the Applications modules. the new version of KDE PIM is expected to be made in tandem with the 4.5.1 release.\r\n\r\nfor a summary and links to sources on the matter, see: http://forum.kde.org/viewtopic.php?f=20&t=89366&p=165898"
    author: "aseigo"
  - subject: "After discussing this with"
    date: 2010-08-10
    body: "After discussing this with the PIM team, we're not sure we'll make 4.5.1, though. A release still this year does look realistic, however. See also my comment above."
    author: "sebas"
  - subject: "KSensors"
    date: 2010-08-10
    body: "And there's still one application that holds me back on KDE 3.5.10: KSensors.\r\n\r\nTemperature Sensors from KDE 4.x show <strong>only</strong> temperatures, so, please stop advising them.\r\n\r\nI would also become happier if someone ported KNetStats (yes, I know about KNemo, but the former application is more lightweight).\r\n\r\nAnd I expect KDE developers to start working on optimization and solving old bugs (and there are hundreds of them) for KDE 4.6 and onwards. Most of us need a stable and fast DE, not a shiny bug-ridden and prone to crashes one."
    author: "birdie"
  - subject: "Webkit can't be made default in konqueror!"
    date: 2010-08-10
    body: "can we? or at least for some websites, like browser identification?"
    author: "fast_rizwaan"
  - subject: "KDEpim Akonadi"
    date: 2010-08-10
    body: "Better safe than sorry. :)\r\n\r\nOn the other hand - the message has been \"delay from 4.5 to 4.5.1\" and therefore your message \"still this year looks realistic\" is not a very good signal. \r\n\r\nIt leaves me with the impression that it is quite probable that the release will not be available before 4.6.\r\n\r\nIf they now targets KDE SC 4.5.3/4/5 or even 4.6 I find the \"delay to 4.5.1 message\" to be really awkward. \r\n\r\nIt's the communication bit I'm after - not the potential delay. :)"
    author: "Omaha"
  - subject: "Awesome release"
    date: 2010-08-10
    body: "I have been using 4.5 since its first RC and it has been a breeze. The last remaining annoyances have been fixed in the final version (thanks for the delay). :)\r\n\r\nSo thank you to all KDE developers. You are doing amazing work!\r\n"
    author: "mutlu"
  - subject: "Thank you"
    date: 2010-08-10
    body: "All KDE developers and contributors for this great release.\r\n\r\nWhat is the plan for 4.6 BTW ? :) "
    author: "Amine27"
  - subject: "Congrats!"
    date: 2010-08-10
    body: "It sure is awesome! Can't wait for 4.6 :)"
    author: "bliblibli"
  - subject: "Yes you can, but yup it's a bit unintuitive..."
    date: 2010-08-10
    body: "Settings ---> Configure Konqueror ---> File Management ---> File Associations ---> html ---> goto Embedding tab and there put WebKit (kwebkitpart) on top. Hope that helps :)"
    author: "bliblibli"
  - subject: "Number of bugs"
    date: 2010-08-10
    body: "\"More than 16,000 bugs have been fixed\" \r\nDoes this count the duplicated bugs?"
    author: "zayed"
  - subject: "Wouldn't it make sense just"
    date: 2010-08-10
    body: "Wouldn't it make sense just to finish it properly and to get rid of as many regressions as possible then ship it with 4.6?"
    author: "bliblibli"
  - subject: "Can't you simply use KSensors"
    date: 2010-08-10
    body: "Can't you simply use KSensors from 3.5.10 in KDE 4.5? "
    author: "jadrian"
  - subject: "I want to get rid of Qt3/KDE3"
    date: 2010-08-10
    body: "I want to get rid of Qt3/KDE3 libraries once and for all.\r\n\r\nThat's the only reason of not willing to use any KDE3 applications under KDE4."
    author: "birdie"
  - subject: "Even this seemingly innocuous"
    date: 2010-08-10
    body: "Even this seemingly innocuous message gets modded down.\r\n\r\nI love KDE fanboys. Keep on voting!"
    author: "birdie"
  - subject: "Thanks very much to the KDE"
    date: 2010-08-10
    body: "Thanks very much to the KDE team for it's time dedication and hard work.\r\nThe first thing that I tested was Dolphin, which seems to be good now, stable (after RC3) and fast.\r\nKDE SC 4.5 seems to be quite stable and polish. The only two little problems that I have are the start up time and Bball is still not fixed. Although it's a very responsive Desktop it seems to start up a bit slow. KDE takes longer to start up on my systems than openSuse 11.3 itself."
    author: "Bobby"
  - subject: "I can understand that, just"
    date: 2010-08-10
    body: "I can understand that, just like getting rid of GTK+/Gnome libraries and all that. But needing a particular Qt3, KDE3, GTK, or Gnome application isn't really a reason not to move to KDE4. "
    author: "jadrian"
  - subject: "Gnome is a whole different"
    date: 2010-08-10
    body: "Gnome is a whole different matter, they care about compatibility. GTK applications circa 2003 will still be working in year 2010.\r\n\r\nKDE3/Qt3 are *officially* abandoned, thus it makes zero sense to have them installed once I've upgraded to KDE4."
    author: "birdie"
  - subject: "Google is your friend ;-)"
    date: 2010-08-10
    body: "See the <a href=\"http://techbase.kde.org/Schedules/KDE4/4.5_Feature_Plan\">Feature Plan</a>, but keep in mind it may be incomplete and these are always subject to change."
    author: "Stuart Jarvis"
  - subject: "Gnome is a whole different"
    date: 2010-08-10
    body: "<em>Gnome is a whole different matter, they care about compatibility. GTK applications circa 2003 will still be working in year 2010.</em>\r\n\r\nBut likely not in 2011, or maybe 2012, whenever GTK3 comes out. ;-)\r\n\r\nA program like KSensors will likely still just work, so it's totally not a problem using it. If you need support for it which includes development, you're out of luck of course, but saying that if upstream is not working on it anymore make \"zero sense to have it installed\" seems quite unpractical. Sure, it would be nice to have every single application that ever was available based on KDE3 available in 4, but that's likely not going to happen. There are thousands of other, new applications in its place of course.\r\n\r\nDon't get me wrong, I'm sure you've good reasons for stating the above, it just doesn't seem to be the most ingenious reason for not using some piece of software you otherwise really like.\r\n\r\nOr maybe you should just install the GTK version of KSensors from 2003 for now. ;-)"
    author: "sebas"
  - subject: "maybe it's not that?"
    date: 2010-08-10
    body: "Maybe it's because your comment comes over a bit demanding and at the same time narrow-mminded. It sure is easy blame critique you get for your comments on some fanboys, but maybe there's just this tiny little bit of truth in those reactions telling you that your comments are a bit clueless. Just saying, it's not always someone else who's wrong ..."
    author: "sebas"
  - subject: "Your chain of reasoning just"
    date: 2010-08-10
    body: "Your chain of reasoning just ended up justifying sticking to KDE 3.5.10 with the fact KDE3/Qt3 are officially abandoned. \r\n\r\nI get your point, that said in practice for your case none of that matters. For now KSensors of KDE 3.5.10 works better for you, so use it."
    author: "jadrian"
  - subject: "I think KSysGuard's sensor"
    date: 2010-08-10
    body: "I think KSysGuard's sensor sheets can do everything KSensors can - and more."
    author: "Eike Hein"
  - subject: "Agreed"
    date: 2010-08-10
    body: "I agree. I rather see it released as part of KDE 4.6.\r\n\r\nIMHO, it was a terrible idea to move release of the Akonadi-based KDEPIM suite to 4.5.1. Doing that, it misses all the testing it would have received from the 4.5.0 betas and RCs.\r\n\r\nIt's big enough of a change that it should get this testing. I don't want to lose my e-mail!"
    author: "Samat"
  - subject: "Yeah"
    date: 2010-08-11
    body: "and I like aseigo blog posts talking about future plans ;)"
    author: "Amine27"
  - subject: "Temperatures."
    date: 2010-08-11
    body: "BTW, he said that KDE4 sensors show only \"temperatures\". I see the sensor list in KSysGuard and I can get a full list of status indexes. Really... what am I missing?"
    author: "Alejandro Nova"
  - subject: "Closed bugs"
    date: 2010-08-11
    body: "I don't know, but I would guess it's basically the count of all bug reports that have been closed since 4.4.0. That likely includes many duplicates.\r\n\r\nStill, it's an awe-inspiring number. Even if many of the bug closings represent relatively little in the way of improvements to the code, try just sitting down and counting to 16,000 some afternoon. I bet it would heighten the appreciation for how much work has gone into this release :-). Go KDE team!!\r\n\r\nI also am fine with the idea of KDE-PIM releasing with 4.6. I've always thought it was a bit odd the idea of making a major change at 4.5.1, so in a way I find this more satisfying.\r\n"
    author: "tangent"
  - subject: "klipper?"
    date: 2010-08-11
    body: "Hi, \r\n\r\nthanks for this nice release. I use Kubuntu 4.5-backport packages for Lucid and notice that Klipper does no auto-collapse anymore. Is this a bug or a feature (or Kubuntu ;-)?\r\n\r\nBye\r\n\r\n  Thorsten"
    author: "schnebeck"
  - subject: "Ugh, compatibility?"
    date: 2010-08-11
    body: "> Gnome is a whole different matter, they care about compatibility.\r\n\r\nUgh. implying KDE doesn't?\r\n\r\nAll 3.x releases are compatible.\r\nAll 4.x releases are compatible.\r\n\r\nThat's the point of the first number anyways, for a lot of projects. They only increment the first number to start a whole new series (in our case, the 4.x series). KDE guarantees compatibility between those versions. An application that's compiled for 4.2, will still run when you install kdelibs 4.5.\r\n\r\nSame logic applies to GNOME: their 3.x isn't going to be compatible with 2.x"
    author: "vdboor"
  - subject: "That would be highly"
    date: 2010-08-11
    body: "That would be highly misleading and I hope it's not the case. You can close bugs with WONTFIX, INVALID, WORKSFORME, DUPLICATE, etc. Counting all of those as fixed bugs would be pretty ridiculous ... \"nah I won't fix this one, there you go one more bug fixed!\"."
    author: "jadrian"
  - subject: "Ok here"
    date: 2010-08-11
    body: "Haven't noticed it taking any longer than 4.4 did on Fedora. Certainly nowhere near booting time. Sounds like you have some particular issue."
    author: "Stuart Jarvis"
  - subject: "Abandonned"
    date: 2010-08-11
    body: "KDE3/qt3 are stable and mature. For stable and mature software maintenance is marginal. So even when they are officially abandonned upstream you can still use and run and fix them, like you can run applications with DOS 5.0 as many in industry do."
    author: "vinum"
  - subject: "And how to invoke?"
    date: 2010-08-11
    body: "ctrl-alt-v doesn't bring it up anymore...?"
    author: "reagle"
  - subject: "dbusmenu-qt"
    date: 2010-08-11
    body: "Clearly a bug. Please ask the Fedora developers to upgrade their dbusmenu-qt packages, as this has already been fixed some time ago."
    author: "sebas"
  - subject: "I wasn't really comparing the"
    date: 2010-08-11
    body: "I wasn't really comparing the login time to that of KDE 4.4. I really meant the general login time of KDE 4. OpenSuse 11.3 takes 30 seconds to reach the login screen on my laptop and KDE 4.5 takes 38 seconds to boot the desktop after that. However, if I log out and then log in again then it takes only 10 seconds. It's only the boot up login that seems to be so slow."
    author: "Bobby"
  - subject: "Nope, nor in Kubuntu"
    date: 2010-08-11
    body: "Nope, nor in Kubuntu - https://bugs.edge.launchpad.net/kubuntu-ppa/+bug/616370?comments=all"
    author: "TChiverton"
  - subject: "It took even longer on the"
    date: 2010-08-11
    body: "It took even longer on the computer, over 40 seconds to boot the desktop. OpenSuse only took 20 seconds to reach the login screen."
    author: "Bobby"
  - subject: "I hope that the Activity per virtual desktop option get fixed..."
    date: 2010-08-11
    body: "I hope that the Activity per virtual desktop option get fixed for 4.5.1 or 4.5.2 ... until now it never worked without big bugs :-("
    author: "xapi"
  - subject: "KDE PIM (almost) beta 1!"
    date: 2010-08-12
    body: "I tried KDE PIM 4.4.92 (Beta 2), and I must notify you that it has, depending on your point of view, finally reached beta quality. It qualifies, indeed, for a Beta 1 moniker, but it needs a lot of testing, because several betas should be done and the whole thing isn't expected to be stable until KDE 4.6.0.\r\n\r\nCan you test? I make a pledge to test this, because this is the first of a new wave of features that will surely put the KDE desktop ahead of the curve."
    author: "Alejandro Nova"
  - subject: "It's fixed :)"
    date: 2010-08-12
    body: "Perhaps you are coming from plain 4.4. If it's broken for you, now you can interactively fix this going to the Activities switch board. You'll have likely four activities. Delete all of them, except one.\r\n\r\nThen, recreate your per-desktop setup going right clicking on your Pager, and select \"Pager Preferences\". Select \"Virtual Desktops\" and untick the box that says \"Different widgets for each desktop\". Hit Apply. Tick it again. Hit Apply.\r\n\r\nYou'll see how your activity enlarges, and contains 4 different viewpoints. Now you can design your desktop, and you'll never have trouble with per-desktop activity setting after this. This is also much better than old \"fixes\" (recreating plasmarc, praying for it to not recreate a broken setup)\r\n\r\nThe side effects of this are cool. Now you can have entire SETS of activities, switchable with Meta+Tab (one set spans through all your virtual desktops, Meta is the Windows key name under Linux). "
    author: "Alejandro Nova"
  - subject: "personal review"
    date: 2010-08-12
    body: "First, I am not a programmer, I do wish Linux OS to increase nubmer of its user.\r\nI think KDE uses better tech then GNOME (c vs c++).\r\nThese are my personal comments of the KDE 4.5, downloaded from kde live page.\r\n\r\nbooting: why does it takes so long?\r\nAnd then you have to stare at the boot screen with 5 icons, of which last is unproprtional?\r\nAn easy fix. And fire your designer.\r\nI guess services are starting or something. Why not show desktop, and load them in the background?\r\nSurely most people dont need print server as soon as logged in.\r\n\r\nSo after I login, cd keeps loading and then I get error:\r\nNepomuk service error.\r\n\r\nIf you want to force nepomuk (btw change name or add some desription to it) down the users throat, at least\r\nask them on boot if they want nepomuk enabled or not.\r\n\r\nDefault Theme: Cold theme? Really? That is no way to win users.\r\nPlease change background wallpaper to something warmer. This feels like, welcome to the frozen land.\r\n\r\nAgain look on ubuntu, win7, mac os x? Are they all wrong?\r\n\r\n\r\nMenu: It takes 3 clicks to get to the editor. \r\nClicking is not what user want. Look at Linux mint menu. 2 MOUSEOVERS.\r\nWin xp 3 mouseovers. (first is all programs)\r\nIt seem kde start menu is modeled after win7?\r\nwin 7 3 clicks, or 1 mouseover and 2 clicks. win 7 is not that good, although adding search is nice.\r\n\r\n\r\nAnd back arrow? You have to CLICK on something to go back. Lame. And it looks ugly. Everything is on mouseover, 2 level deep max.\r\nOne is better.\r\nIf you want to keep arrow for back move, at least make it on mouse over, or\r\nreset menu on app click. (Move to root categories)\r\n\r\nWhy not move everything in internet in internet submenu.\r\nApss/Interner/Konqueror (Web browser), not\r\nApss/Interner/Web browser/Web browser\r\nYou have duplication.\r\n\r\n\r\nMost people don't bother changing options.\r\nIf it does not work by default your design failed. If I have to look in the manual/on the internet to \r\nmake desktop act as in every other OS,  design failed.\r\nYou want something special great. Ask the user something like:\r\nHi we have this great new concept of desktop, would you like to test it? yes/no. And make sure there is and easy way back to normal.\r\n\r\nWhat is newspaper activity? Nothing happens when I select it.\r\nWhat is the difference between \"desktop\" and \"plain desktop\"?\r\nWhy does not any of these two show files I have in my desktop folder.\r\nIt seems folder view is the closest to \"desktop\" as used in other oses.\r\n\r\nKde need to get non-geek designer. To get rid of the attitude: look what I can do with my programming skill.\r\nIOS, ubuntu, mac os , even win7 is far away from them.\r\n\r\n\r\nIt seems people at KDE are trying hard to show they are different. In the end they'll be the only one using their software.\r\n\r\n\r\nI am not sure it is kde or open suse, but I cannot mount windows hdd. I get error no device available, in an\r\nugliest message window I've seen.\r\nIts about quarter of the screen big, it has title, then empty part, then message, and after that\r\nsomething sign like two small arows over and under the line(?) and X after that? When I click on X window close after 2 sec?\r\n\r\ndesktop pager icons are too small.\r\nThat 'i' in the circle near the scissors icon is barely noticable. Is it supposed to be like that?\r\n\r\nAdd some bookmarks of the KDE site to konqueror.\r\n\r\nIf I want to listen to some mp3 what do I use? Amarok behemonth? Are you kidding?\r\n\r\nAlos what is kde wallet and why would I want to use it with Amarok?\r\n\r\nWhat are those + and minus when I hover over file? Not tooltip info. Easy fixable.\r\nAdd some icon to unmount removable device in dolphin, like it exists in mac and ubuntu. You again seem to use windows method of systray, multiple clicks. Or, keep both\r\n\r\nwhy the restart counter? For the undecided?\r\n\r\n\r\nThis is my opionion, and if you publish software, you cant just read the good comments.\r\nYou cannot expect all the users to be programmers, so please don't say \"Fix it yourself\".\r\n\r\nIf you dont have designers, or cant hire one, post a contest, where people can send their ideas.\r\nYou ll prove you are listening to users (Is that important to you?) and that you dont have\r\nNot invented here syndrom.\r\n\r\n\r\nEditor, and completing text: nice\r\nicons are better than before"
    author: "dbojan"
  - subject: "Thanks Alejandro!\nThat"
    date: 2010-08-12
    body: "Thanks Alejandro!\r\n\r\nThat solution steps isn't easy to imagine to solve the problem if nobody explain this :)\r\n"
    author: "xapi"
  - subject: " the guy without a joob now :D"
    date: 2010-08-12
    body: "Hey I'm the guy you want to fire. :) it wont be easy :) as I'm a over worked volunteer, like most of KDE :).\r\n\r\nAny way, wont say you don't have a point in many of the remark's you point out, specially on the menu I was never a fan of it (note we do offer other options).\r\n\r\nIn the splash screen... next release we will have a new one, hey this one was not that bad :D.\r\n\r\nIn the wallpapers... again next release a new one, we change them every 2 releases, the wallpaper is kinda important as we do all the branding around it, so it needs defined branding values we can reuse, look at this site as well as other kde sites you will notice how we kinda recycle the wallpaper to create a uniform brand. (but no i'ts not my proudest wallpaper yet).\r\n\r\nThe \"i\" notice that its if it is barely noticeable it as nothing to give you, so maybe the fact that it dims of is a good thing, wen it does have something to show it does brighten up.\r\n\r\nThe plus an minus on dolphin.... use them you will understand fastly enough :), very useful on tablets.\r\n\r\nNot sure you ever done design, but making random questions to general public produces very bad inconsistent results, and the overhead of the process is greatly larger than simply doing it yourself. \r\nDesign needs guidance, good overview and a goal. So some one as to do it.\r\nI do need more help, so more quality designers that can work in a team is highly appreciated, in the future we hope we can start to do some fine grain polishing to apps.  \r\nYou appear to have some nifty ideas join us on #oxygen to make a better desktop tomorrow... (personally I believe that we already come a long way since the 3.x days).\r\n\r\nAny way thanks for your comment's they look genuine and a good insight how some one new to KDE looks at it. I will take them into account...(remember this is the result of many sensitivities it will never be 100% like you want it to be, but with work you can push it to be better for more people)\r\n\r\ncheers the guy without a job ;)       \r\n    \r\n       \r\n   "
    author: "pinheiro"
  - subject: "Maybe wrong"
    date: 2010-08-12
    body: "As far as I know that's not entirely true. Packaging KDE 3 and 4 at the same time (well, just the libs) is not as easy as it may seem, and not all distros did it 100%.\r\n\r\nAt the pre-4.0 times, much work was done in trying to make that not as painful, but still is not as easy as with plain Qt applications."
    author: "suy"
  - subject: "Lion Mail."
    date: 2010-08-12
    body: "In tone with the Akonadi/KMail testing, can you provide us a tarball with Lion Mail? I really want to exploit some of the advantages of the new framework."
    author: "Alejandro Nova"
  - subject: "Hurry"
    date: 2010-08-12
    body: "I'm with you. I don't see why there should be any hurry in releasing it as stable. To have it tested by many users the important part is having it packaged, distributed and promoted. Mplayer was being used by tons of people in its Beta and RC days, long before it was promoted to stable. I remember everyone installing a Beta of KDE 2 that came with some SuSE release a long time ago. \r\n\r\nSo just make it possible to install these Betas along with the latest stable, and get distros to distribute it as an option during the KDE 4.5 life cycle. "
    author: "jadrian"
  - subject: "About bug counts"
    date: 2010-08-13
    body: "Yes, it includes all bugs that get closed, for all projects that track their bugs on the KDE bug tracker, i.e. it also includes koffice, amarok, digikam, kdevelop, etc.\r\n\r\nStill the number is a good indication about how much work the team did, as each of those 16000 bugs is usually closed individually.\r\n\r\nOn the other side there is the number of bugs opened. It is a good indication about the work YOU did :) And comparing those numbers, you did a better job then the KDE team: Around 16000 bugs closed, around 18000 bugs opened in the last 180 days, so you could say that we have 2000 more bugs :)\r\n\r\nYou can use bugs.kde.org search service to find how many bugs really got closed as fixed. For example, in the last 180 days around 4600 bugs have been closed as FIXED. Again, this includes all projects. Plasma alone has 700 of those fixes. Additionally, around 600 feature requests have been closed as FIXED (50 in Plasma).\r\n\r\nBut only mathematicians care about numbers. What really counts is that bugs continue to get reported and continue to get fixed. That's the heartbeat of the KDE project.\r\n\r\nKeep beating!"
    author: "christoph"
  - subject: "You're welcome :)"
    date: 2010-08-13
    body: "Any solution that involves praying isn't a solution. I'm happy that this is finally fixed once and for all."
    author: "Alejandro Nova"
  - subject: "Akonadi/KDE-PIM will be released with SC 4.6."
    date: 2010-08-13
    body: "Face the reality.\r\n\r\nOn the other hand, they will release beta after beta during the KDE 4.5 cycle (with every minor release, a beta, starting with Beta 2/KDE 4.5.0). And if we don't test these betas, then Akonadi/KDE-PIM release is going to be a flop.\r\n\r\nSo, IMHO the message is clear. Download and install Akonadi/KDE-PIM Beta 2. Test it. Report bugs. Hammer it.\r\n\r\n(and also, Sebastian... could you release a tarball of Lion Mail to sweeten my Akonadi/KDE-PIM testing?)"
    author: "Alejandro Nova"
  - subject: "you know..."
    date: 2010-08-13
    body: "Whatever metric is used in the release announcement, it is wrong. A lot of bugs are never reported, yet fixed. So how do we count those? If we would only count reported bugs which have been fixed by a code commit, yes, the number is smaller. No, it is not more accurate. Many bugs are closed because they have been fixed in the mean time - WORKSFORME is often used there if the developer who fixed the bug never saw the actual bugreport...\r\n\r\nSo the marketing team simply uses the number of bugs closed in the period between releases. Works fine - you also credit the work of the bug squad with that number. Even on WONTFIX bugs you have to spend time..."
    author: "jospoortvliet"
  - subject: "The fact that there isn't any"
    date: 2010-08-13
    body: "The fact that there isn't any accurate metric doesn't make this approach acceptable. It's so bad that it's not even funny. \r\n\r\nIf you're going for that number better claim \"bugs reports closed\" instead of \"bugs fixed\". Or else chose only the ones marked as \"fixed\" and use that as a lower bound. "
    author: "jadrian"
  - subject: "OK, sorry if that was a bit"
    date: 2010-08-13
    body: "OK, sorry if that was a bit harsh. And also, reading my text, I see kde write needs spelling checker. :)\r\n\r\nBut those icons, they've been like that since, ... 4.3? I can't believe nobody noticed. \r\nAnyway, why do we need splash screen anyway? Ubuntu/Gnome seems to load much faster. No splash screen there. Also I like their notifications. Simple, not, hm, atacking like. :)\r\n\r\nAbout wallpaper, it is not badly designed, it just seems uninviting, or cold somehow. That's all.\r\n\r\nAbout design, I meant more like polls, on kde site. And feedback in KDE itself. Something like firefox 4 beta has.\r\n\r\n#Oxygen you say? Hm. Interesting.\r\n\r\nBest regards, and thanks for replying. :)\r\n\r\n\r\n\r\n"
    author: "dbojan"
  - subject: "Wallpapers"
    date: 2010-08-14
    body: "On the issue of wallpapers: I just don't understand why we keep using abstract images for our default wallpapers.\r\nThey're usually pretty well done (or even downright awesome), but that doesn't change the fact that even someone as technically minded as me will have a much more positive first impression when seeing some relaxing greenish photo of a meadow instead of blue stripes or circles or whatever.\r\n\r\nIn my eyes, the abstract wallpapers scream \"This is for techies! This doesn't belong into your living room.\"\r\n\r\nActually, I've noticed that I have to make a conscious effort to look at the windows and widgets of a screenshot - if I don't my feelings are dominated by what I think of the wallpaper. It's a little sad, but I don't think I'm alone in this."
    author: "onety-three"
  - subject: "wally"
    date: 2010-08-14
    body: "abstract images? :) I gess same reason most desktops do...\r\nReal life images get boring faster IMO.\r\nPus in teh case of KDE, the branding strategy as been revolving around the wallpaer, as we dont have the money to create a worlwide campain that can create familiarity with the pruduct, we instead try to use our biguest banner (the wallapaper) and use it as a base of branding with recognizable elements that are used in all parts of the external projection of kde. Most obvius example are the main kde websites..."
    author: "pinheiro"
  - subject: "#oxygen"
    date: 2010-08-14
    body: "Any one is invited to be a part of the creation of the design experience in kde. Join us on #oxygen.\r\n\r\nI do many little surveys, (mostly in my blog), specially on the base direction we should take, but all pools need to be taken with a grain of salt, the people that usually like to say something do not represent the average user, it represents a certain type of user that is vocal.\r\n\r\nIn the icon pools we sometimes do, we created tools to help us with that, but to do so kinda reduced the spectrum of possible answers witch might not be the best thing for all design issues, so I tend to trust my experience in judging user feedback on what they really mean wen they say I don't like this or that, because as you know perception and reality are 2 very different things.\r\n\r\nAbout typos... well its my trademark ;).\r\n\r\nAgain thanks for the feedback it is valauble.    "
    author: "pinheiro"
  - subject: "Wow... fast!"
    date: 2010-08-14
    body: "Just upgraded on OpenSuse 11.3 from 4.4 to 4.5 and I must say... I'm impressed. KDE 4.5 is a lot faster!\r\nSadly KRunner seems to have problems with web-shortcuts.\r\n\"gg:\" or any other shortcut refuses to work... nothing happens...\r\n(\"Web shortcuts\" are activated)\r\n"
    author: "thomas"
  - subject: "branding"
    date: 2010-08-14
    body: "Thank you for your answer, Nuno.\r\n\r\nI've just written and then erased a lengthy answer. This really isn't the right place to discuss such things and I don't want to come off as too full of myself or anything like that - I really love your work. Also, there's few facts with these topics and a lot of opinions.\r\n\r\nSo I'll just say that I think that pretty pictures that feel inviting and non-technical might be even more important than branding to lure most ordinary users. The chances of somebody coming into contact with KDE more than once without actively seeking it out are still pretty slim nowadays, at least where I live. I'd be amazed if anybody went \"Oh, I must know this somehow, I've seen those blue circles somewhere before\". And there's quite a few Linux users we lost to other desktops who won't care about the branding, either.\r\n\r\nTo get to business desktops and similar larger enrollments the branding is useful, of course - though even there I think it's more important what the distributions do than what KDE itself does.\r\n\r\nProbably dbojan's suggestion of changing the main color to something warmer is a good idea. Your original oxygen mockups used a lush, dark green if I remember correctly - and they looked awesome. We've started to get rid of the abundant Ks, why not do the same with all that blue? If anybody could do that it would be you :)"
    author: "onety-three"
  - subject: "Thanks "
    date: 2010-08-15
    body: "just to again say thank you for you guys comments, I fully agree with your comments, except on the count of kde not being visible, it might not be were you live but this is not an universal truth, there are several places were kde is doing incredibly well and were allot of people have seen our branding values... \r\nIn the current wallpaper wallpaper I tried to get a sunrise feeling to it, something new and exiting coming up on the horizon, I think I mostly failed :D, Its not that it is terrible bad, but it does come up as a bit cold and uninviting... We will try to get it better next time."
    author: "pinheiro"
  - subject: "seperate package?"
    date: 2010-08-18
    body: "Would it be possible to provide seperate feature packages, so that we can already test it? I don\u2019t want to switch to the full KDE trunk to get PIM 2, but I do want to test KMail 2. \r\n\r\n(I\u2019m on Gentoo packages)"
    author: "ArneBab"
  - subject: "closed reported bugs :)"
    date: 2010-08-18
    body: "what they did was fix bugs that users reported. \r\n\r\nBut why don\u2019t you give them a hand? I\u2019m sure if you can write a simple way to get a more accurate number, they\u2019ll gladly take it :)\r\n(provided you make it as easy to use as the current one)"
    author: "ArneBab"
  - subject: "what do you use to activate"
    date: 2010-08-23
    body: "what do you use to activate webshortcuts? I have a space (so \"gg test\" googles test). Check what it is for you?!?"
    author: "jospoortvliet"
  - subject: "Progress is born here and"
    date: 2010-09-03
    body: "Progress is born here and that I think is most important. That is all free and have access to it is vitally important. People can express themselves and it makes them happy.<a rel=\"dofollow\" href=\"http://www.auto-tip.ro/dezmembrari-auto\" title=\"dezmembrari auto\">dezmembrari auto</a>\r\n\r\n"
    author: "masini"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px"> 
<a href="http://kde.org/announcements/4.5/"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde45-small.png" width="500" height="250" /></a>
</div>

<p align="justify">KDE today celebrates its semi-annual <a href="http://kde.org/announcements/4.5/">release event</a>, making available new releases of the <a href="http://kde.org/announcements/4.5/plasma.php">Plasma Desktop and Netbook workspaces</a>, <a href="http://kde.org/announcements/4.5/platform.php">the KDE Development Platform</a> and a large number of <a href="http://kde.org/announcements/4.5/applications.php">applications available</a> in their 4.5.0 versions.
</p>
<p align="justify">
In this release, the KDE team focused on stability and completeness of the Desktop experience. More than 16,000 bugs have been fixed, and many feature requests have been filled. The result for the user is a system that feels faster, takes less time to "think", and works more reliably.  The large number of bug fixes goes accompanied with many parts that have got an extra portion of tender loving care. <strong>Plasma 4.5.0</strong>'s new notification system is one example here. It is designed to get less in your way, yet to support your workflow as smoothly as possible. Visually, the striking monochromatic icons make for a more consistent look in the notification area. A highlight of the KDE <strong>Applications 4.5.0</strong> is surely Marble, which can now be used for map routing as well as viewing. The Konqueror web browser can now also use the WebKit engine to render its content. The <strong>KDE Development Platform 4.5.0</strong> offers a new generic cache for applications that need high-speed access to certain data, such as icons or other pre-rendered artwork. The new KSharedDataCache speeds up loading of many components, while the new HTTP scheduler is optimized for concurrent access to web servers, and makes loading of pages in Konqueror and other parts using the KIO HTTP mechanism faster.
</p>
<p align="justify">
Today's releases are the first in the 4.5 series and will be followed with updated versions approximately monthly that will focus on fixing bugs. The next feature releases are planned for January 2011. If you would like to test-drive 4.5.0, you can do so by installing the packages that should be made available by your operating system vendor. You can also choose to build 4.5.0 from source, the nitty-gritty of that can be <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">found on TechBase</a>. For the impatient, there is also a <a href="http://home.kde.org/~kdelive">Live CD</a> available.
</p>
<!--break-->
