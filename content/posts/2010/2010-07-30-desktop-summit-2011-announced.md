---
title: "Desktop Summit 2011 Announced"
date:    2010-07-30
authors:
  - "sealne"
slug:    desktop-summit-2011-announced
comments:
  - subject: "German"
    date: 2010-07-31
    body: "I start to wonder if KDE only exists for German people....\r\n"
    author: "bschelst"
  - subject: "I wonder why you feel this"
    date: 2010-07-31
    body: "I wonder why you feel this way.. KDE software is translated into many languages, with English being the primary one. KDE itself is an international team of volunteers, and companies from all over the globe. And working all together!"
    author: "vdboor"
  - subject: "Yes of course..."
    date: 2010-07-31
    body: "and Gnome also... that's why the Desktop Summit will be in Berlin!\r\n\r\nAkademy / Desktop Summit\r\n2003 Nov\u00e9 Hrady, Czech Republic (http://events.kde.org/info/kastle)\r\n2004 Ludwigsburg, Germany (http://conference2004.kde.org)\r\n2005 M\u00e1laga, Spain (http://conference2005.kde.org)\r\n2006 Dublin, Ireland (http://conference2006.kde.org)\r\n2007 Glasgow, Scotland (http://conference2007.kde.org)\r\n2008 Sint-Katelijne-Waver, Belgium (http://conference2008.kde.org)\r\n2009 Gran Canaria, Spain (http://conference2009.kde.org)\r\n2010 Tampere, Finland (http://akademy2010.kde.org/)\r\n2011 Berlin, Germany\r\n\r\nCamp KDE\r\n2009 Jamaica\r\n2010 San Diego, USA\r\n2011 ?\r\n\r\nhttp://dot.kde.org/2009/12/27/kde-community-invited-foss-nigeria-2010\r\n\r\nWhat's so hard to understand? Gnome and KDE are international projects. Really one of the points that makes them so appealing. In a big project it is wise to have one major \"communication language\" to reduce translation troubles. That's english (due to \"laziness\" cause most people already learn it in school), but that does not make KDE or Gnome a project for \"english\" audience solely.\r\n"
    author: "thomas"
  - subject: "german? no, spanish!"
    date: 2010-07-31
    body: "obviously, since there have been two Akademies in Spain (so far the largest number in any single country, next year will even that), Akademy.es has been a great success and KDE e.V. just signed officially with KDE Spain granting them official usage of KDE trademarks, etc. it quite evident that KDE only exists for Spanish people!\r\n\r\n*grin*"
    author: "aseigo"
  - subject: "Not true!!!"
    date: 2010-07-31
    body: "Ha, I disagree. Considering KDE users mostly live in Brazil (over 50 million, as YOU said), KDE is all about Brazil. So!"
    author: "jospoortvliet"
  - subject: "Spain migration"
    date: 2010-08-01
    body: "Yes, as an English early adopter, I can confirm that Spain works quite well. But I am eagerly looking forward to the Spain 3.0 release."
    author: "Richard Dale"
  - subject: "KDE is coming home"
    date: 2010-08-01
    body: "Very nice location for a conference. I hope the others from android, XFCE and LXDE would also join."
    author: "vinum"
  - subject: "welcomed"
    date: 2010-08-01
    body: "people from those projects / communities would be more than welcome to join us, and i'm sure we would all learn new things in the process. :)"
    author: "aseigo"
  - subject: "Re:"
    date: 2010-08-05
    body: "source: official kde website:\r\n\r\nContributor Meetings\r\n\r\nThere have been roughly yearly large-scale contributor meetings for the KDE community since very early in the project. Since the KDE e.V. was created the yearly meeting is a requirement of its bylaws, so we can count on it now.\r\n\r\nAKademy 2011 - Germany\r\nAkademy 2010 - Tampere, Finland\r\nAkademy 2009 - Las Palmas de Gran Canaria, Spain\r\nAkademy 2008 - Sint-Katelijne-Waver, Belgium\r\naKademy 2007 - Glasgow, Scotland\r\naKademy 2006 - Dublin, Ireland\r\naKademy 2005 - M\u00e1laga, Spain\r\naKademy 2004 - Ludwigsburg, Germany\r\nKastle - Nov\u00e9 Hrady, Czech Republic 2003\r\nKDE Three - N\u00fcrnberg, Germany, 2002\r\nKDE Three Beta - Trysil, Norway 2000\r\nKDE Two - Erlangen, Germany 1999\r\nKDE One - Arnsberg, Germany 1997\r\n--\r\n\r\n13 meetings, 5 in Germany.\r\n\r\nSo for an international project, almost 40% of the meetings are in Germany. Some people call this 'weird'.\r\nOn the other side, KDE is open source, and people around the world are developing it. Normal people, which are not paid for it, so they cannot afford such travels. Did you ever think about those people?\r\n\r\naseigo, you also need to learn how to use a calculator.\r\n\r\nps: feel free to give my post negative points. Open source = freedom .. but not free to give your opinion it seems..\r\n"
    author: "bschelst"
  - subject: "> Normal people, which are"
    date: 2010-08-06
    body: "> Normal people, which are not paid for it, so they cannot afford such travels.\r\n> Did you ever think about those people?\r\n\r\nAkademy is always in Europe because of this reason. There are a lot of European developers in KDE. Also, students and people with low income are able to ask for a travel sponsorship from the E.V.\r\n\r\nLately, CampKDE is started in America to get a stronger community over there - and provide an alternative for Americans. There are also local Akademy's these days (spain, brazil). These things start because those local communities are strong.\r\n\r\nMore people == More conferences near that place.\r\n\r\nThe first German meetings are done like that, because they were the _first_ meetings. People got the idea that meeting up would be nice (this was AFAIK uncommon in FOSS those days!) that was the basis of the real Akademy which started in 2004. Not something you can draw conclusions from, as you did.\r\n\r\n\r\n> p.s. feel free to give my post negative points.\r\n> Open source = freedom ..\r\n> but not free to give your opinion it seems..\r\n\r\nBesides giving your opinion, I think you're also trying to imply something. And point fingers somewhere. That is a different thing, and treated differently."
    author: "vdboor"
---
<h2>KDE e.V. and GNOME Foundation to Co-Host 2011 Desktop Summit in Berlin</h2>

<p>GNOME and KDE are teaming up again to host the 2011 <a href="http://www.desktopsummit.org/">Desktop Summit</a> in Berlin, Germany. Due to the success of the 2009 Desktop Summit the projects will co-locate GUADEC and Akademy once again in August, 2011 for the largest free software desktop event ever.</p>

<p>The 2009 Desktop Summit was a fantastic opportunity for the leaders of the free software desktop community to share talks, address common issues, and build relationships between the communities with combined social events.</p>

<p>The 2011 Desktop Summit will build on the first Summit's success. More than 1,000 contributors from more than 50 countries are expected to attend the 2011 event in Berlin. In addition to members of the GNOME and KDE development community, the conference will also attract many participants in the overall FLOSS community from local projects, organizations, and companies.</p>

<p><em>"We are looking forward to the Desktop Summit. GUADEC is our community's opportunity to meet in person and we are glad to be able to have our KDE colleagues as part of our conversations and our celebrations."</em> says Stormy Peters, Executive Director of the GNOME Foundation. <em>"Berlin is a great city with a very strong free software community. C-base, KDE and GNOME will make a great team!"</em></p>

<p>Berlin was one of several locations proposed for the Desktop Summit in 2011. It was chosen due to the excellent facilities at the conference site, the strong presence of KDE and GNOME communities in the area, the strength of the organizing team, and an easily accessible site for those travelling to the Desktop Summit. Both communities are very excited about holding the Desktop Summit 2011 in Berlin!</p>

<p><em>"Berlin is a fantastic city with strong ties to both the KDE and GNOME communities as well as free software in general,"</em> says Cornelius Schumacher, President of KDE e.V. <em>"The KDE community is excited to meet again with the GNOME community to hold the second Desktop Summit there. This is going to be one of the main free software events of the year 2011."</em></p>

<p>The Berlin team consists of Claudia Rauch of KDE e.V., Caspar Clemens Mierau of Ubuntu and c-base, and Mirko Boehm of KDE, and is strongly supported by the city of Berlin. With their ties into both the Gnome and KDE communities, the team well represents the idea of a joint Desktop Summit.</p>

<p><em>"We are honored and proud that our proposal was selected. What we look forward to the most is the inspiration our communities will draw from having the Desktop Summit together again, but also from visiting our bustling, welcoming city. We would like to thank all the supporters of the proposal, and will work hard to make the conference a big success."</em> says Mirko Boehm.</p>

<h2>About KDE and KDE e.V.</h2>

<p>KDE is an international community that creates Free Software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet and web applications, multimedia, entertainment, educational, graphics and software development. KDE software is translated into more than 60 languages and is built with ease of use and modern accessibility principles in mind. KDE4's full-featured applications run natively on Linux, BSD, Solaris, Windows and Mac OS X.</p>

<p>KDE e.V. is the organization that supports the growth of the KDE community.  Its mission statement -- to promote and distribute Free Desktop software -- is provided through legal, financial and organizational support for the KDE community. KDE e.V. organizes the yearly KDE World Summit "Akademy," along with numerous smaller-scale development meetings.</p>

<p>More information about KDE and the KDE e.V. can be found at <a href="http://www.kde.org/">www.kde.org</a> and <a href="http://ev.kde.org/">ev.kde.org</a>.</p>

<h2>About GNOME and the GNOME Foundation</h2>

<p>GNOME is a free-software project whose goal is to develop a complete, accessible and easy to use desktop for Linux and Unix-based operating systems. GNOME also includes a complete development environment to create new applications. It is released twice a year on a regular schedule.</p>

<p>The GNOME desktop is used by millions of people around the world.  GNOME is a standard part of all leading GNU/Linux and Unix distributions, and is popular with both large existing corporate deployments and millions of small business and home users worldwide.</p>

<p>Composed of hundreds of volunteer developers and industry-leading companies, the GNOME Foundation is an organization committed to supporting the advancement of GNOME. The Foundation is a member directed, non-profit organization that provides financial, organizational and legal support to the GNOME project and helps determine its vision and roadmap.</p>

<p>More information about GNOME and the GNOME Foundation can be found at <a href="http://www.gnome.org/">www.gnome.org</a> and <a href="http://foundation.gnome.org/">foundation.gnome.org</a>.</p>