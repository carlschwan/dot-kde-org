---
title: "Successful Spanish KDE Blogger Baltasar Ortega Talks to the Dot"
date:    2010-06-28
authors:
  - "oriol"
slug:    successful-spanish-kde-blogger-baltasar-ortega-talks-dot
comments:
  - subject: "Very good interview!"
    date: 2010-06-29
    body: "I think it's a very good interview, actually the questions are good, which usually is a problem since sometimes questions are not really tailored for the interviee. Congrats to Oriol about it!\r\n\r\nAnd of course congrats to Baltasar!\r\n"
    author: "tsdgeos"
  - subject: ":)"
    date: 2010-06-29
    body: "Great!\r\nOne of the best examples of how it's possible to be a big step forward for KDE without developing.\r\n\r\nThanks Baltasar!\r\nPD: And waiting for more work with KAlgebra ;)"
    author: "apol"
  - subject: "Indeed a great interview"
    date: 2010-06-29
    body: "Guess I should leave more comments from now on :)"
    author: "Gallaecio"
---
On June 1st, 2010, <a href="http://www.kdeblog.com">KDE Blog</a>, one of the foremost KDE-focused blogs in Spanish, celebrated the publication of its 1500th post. The occasion seemed to be the perfect excuse to chat with its author, Baltasar Ortega, and to ask him a few questions about himself, blogging, and how KDE is going to take over the world. Read on for his insightful and passionate answers.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/Baltasar_Ortega_Pic.png" width="200" height="138"/><br />Baltasar Ortega</div><b>Dot</b>: Hi Baltasar! Could you start by introducing yourself to our readers?

<b>Baltasar</b>: First of all, I would like to thank you for the opportunity to participate in KDE.NEWS. My name is Baltasar Ortega Bort. I have a BA in physics and I work as a high school teacher in the San Pedro Apóstol school, in Puerto de Sagunto (Spain). I am also a professor-tutor in UNED (the main Spanish public university for distance education, what in English is often referred to as an 'open university').

Computers have always been my hobby at the user level, although I have always wanted to learn to write code (this is one of the permanent items in my to-do list). I have recently been admitted as a member of <a href="http://www.kde-espana.es">KDE-Spain</a>, by which I feel very honored.

In the virtual world I am known as baltolkien, a nick that combines my first name with that of the writer J.R.R Tolkien. This nickname was thought of by my sister-in-law on one of my birthdays, and I adopted it because I thought it sounded very original.

<b>Dot</b>: When and why did you start using free software? And KDE software?

<b>Baltasar</b>: I started using free software around 2004 or 2005, I do not remember exactly. There were multiple reasons: I was curious about discovering new software, I was not pleased with the lack of stability in Windows when you try to tweak the system, I was fed up with viruses and other malware, I wanted to optimize my slowing-down system, etc.

I came across KDE 3.3 by coincidence, and I was pleasantly surprised by its evolution from previous versions, its high level of configurability, and its great applications. It was not coarse like other free desktop environments or how KDE software had been in the past. But what finally sold me to it was the discovery of applications as powerful as Amarok and Konqueror.

<b>Dot</b>: How did you decide to become a blogger?

<b>Baltasar</b>: It all started one year before my switch to KDE when, as I was searching for a gaming blog, I came across Linuxjuegos.com. I wanted to collaborate with the blog and its webmaster, Daniel Moreno, offered me the chance to maintain the blog Gacetadigital.com. I accepted and thus started blogging, although this was not a Linux blog.

Later on, once I had a good track record as a blogger and some experience, I realized that there was no blog in Spanish which focused on the desktop environment I was using (or if there were any, they were not very active). I though that sharing my knowledge would be a good way of helping others and of learning myself, and so I suggested to the site's webmaster the creation of KDE Blog. My idea was immediately accepted, and he even designed the exclusive look that the blog has nowadays (thank you Daniel)

<b>Dot</b>: You are a very prolific blogger. How do you approach the search for topics? Do you have any goals/rules/guidelines when you start writing a new post, or a blogging routine?

<b>Baltasar</b>: You are right that I am a prolific blogger. This was precisely one of my initial goals: to post at least one entry each day in the blog.

There are several sources of ideas for my posts. I have a long list of blogs and webs indexed in my RSS reader that provide me with a lot of information about applications, projects, how-tos, or official releases. They are my main source of inspiration. On top of that, I browse the web in search of relevant video clips (some times these can be very didactic) or presentations. Moreover, when I have some time I try to write my own articles in the form of tutorials for applications with the goal of going in depth into the great KDE apps, and I also write opinion pieces about KDE, free software, education, etc.

As for the style of my posts, I always try to include an image to make the entry more visually appealing and the source of the information that I am using more easily recognizable.

I usually spend most of my blogging time on weekends, and so during the working week (when I do not have much free time) I tend to mostly post news articles (although I must confess that I spend as much time as I can writing for the blog, even on weekdays).

<b>Dot</b>: Do you have any preferred topics that you like to blog about?

<b>Baltasar</b>: Discovering new applications and sharing them with my readers is one of my main incentives. I like to think that there are people out there who have found a very useful new application thanks to me. Another topic that I particularly enjoy writing about is education, on which we have a lot of work to do in the world. Visually KDE software is very enticing for children. And, evidently, I love writing about KDE and its releases.

<b>Dot</b>: Do you have a sense of who your readers are and where they are from?

<b>Baltasar</b>: I think that most of my readers are Linux users of all levels, although my blog is probably too basic for developers. I like to think that many of my readers became Linux users thanks to one of my articles. Moreover, I have many readers who follow me via RSS.

As for where they are from, it is clear that my area of influence is the Spanish-speaking world, so most of my readers are Spanish and Latin American.

<b>Dot</b>: Do you interact with your readers and, if so, how?

<b>Baltasar</b>: I interact with them as much as I can. Of course, comments in the blog are the main source of interaction, although I do not get too many of them. I try to answer all the questions, which make me grow as a user. In fact, some of these questions end up becoming the main ideas behind new articles.

I am always grateful for comments, even if they are critical (as long as they are not offensive) and I only regret that I do not get more of them.

<b>Dot</b>: What do you think about the presence of KDE in the blogosphere?

<b>Baltasar</b>: I think that there should be more blogs for KDE users, blogs that talk about the writers' experiences with KDE, system tweaks, etc.

<b>Dot</b>: What other blogs do you follow regularly?

<b>Baltasar</b>: Many and of different kinds. Most of them focus on Linux, yet others are about education, new technologies, cinema, comic books, games, etc.

Of course I have some favorites, such as <a href="http://dot.kde.org">the Dot</a>, or <a href="http://espaciokde.blogspot.com/">Espacio KDE</a>. I also follow with special interest the project of my friend maslinux, <a href="http://tuxeamesocial.es">Tuxéame</a>

<b>Dot</b>: Besides your activity as a blogger, do you participate in the KDE community in any other way?

<b>Baltasar</b>: Earlier this year I applied for KDE Spain membership, which was granted to me (thank you all very much!) Lately, I have been getting away from my PC and I started giving talks about KDE or about free software and education. I recently started collaborating with Aleix Pol in trying to improve the integration of KAlgebra into the education system. My goal is to increase my presence in events, both as a presenter and as a listener.

Occasionally, I also write articles in Todo Linux's magazine.

<b>Dot</b>: What do you like best about KDE software, and what are your favorite KDE applications?

<b>Baltasar</b>: As I said before, what I like most about KDE software is that you can configure it completely to suit your own tastes. Moreover, now with KDE SC 4 there are stunning visual effects and, of course, the wonderful world of plasmoids.

As for my favorite applications I would highlight the amazing Amarok, Dolphin, Okular, Digikam, Showfoto, Krename, Klipper, Tellico, etc. I would like to be able to use KOffice, but it is still a little immature. There are many applications and they are getting better and better.

<b>Dot</b>: In what areas do you think KDE software should improve?

<b>Baltasar</b>: Mostly in stability, particularly in its plasmoids. I am also eager to see how Kontact evolves.

<b>Dot</b>: How do you see the evolution of KDE software throughout time, and what do you think its future is going to be like?

<b>Baltasar</b>: The evolution of KDE software has been amazing. In two years it has gone from being something great but not very usable to becoming a very stable desktop for almost all kinds of users. In fact, two friends of mine with very little knowledge of computers use KDE SC 4.3 without any problems.

As for its future, I think it will be shining, although in order to get there a lot of work needs to be done in promotion. The developers are doing an amazing job, but it is often not well-known and gets forgotten. So we need to give more talks about the desktop environment in free software events, start more blogs about KDE, have more presence in schools, etc.

<b>Dot</b>: You have been doing a wonderful job spreading the KDE gospel in the Spanish-speaking world. How do you see the position of KDE in Spain and the Spanish-speaking world as compared to in the English-speaking one?

I think that in Spain there is a high level of participation in KDE by developers who make important contributions to a number of applications. Moreover, the KDE Spain Cultural Association was recently officially founded with the goal of fostering the development and use of KDE software. The last Akademy-es was also very successful, which hopefully will break a path for the success of KDE in our country.

The English-speaking world is obviously more active in KDE, as there are many more people involved. However, thanks to the wonderful work of the translators I think that KDE does not have problems in reaching regions where other languages are spoken.

<b>Dot</b>: What do you think KDE could do to reach even more people?

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/Baltasar_Ortega_Tux_KDE.png" width="128" height="123"/></div><b>Baltasar</b>: As I said before: talks, demonstrations, promotions, participation in events and, more than anything else, we should find ways of introducing ourselves into the education systems.

<b>Dot</b>: What does KDE give you personally?

<b>Baltasar</b>: At first it was just a beautiful and functional desktop environment. Now it is a philosophy that matches perfectly my idea of society: a society that helps the needy, that shares its knowledge with others, that learns and improves at high speeds because it works collaboratively.

KDE has gone from being software to being a community, and I would like to be a part of that community from the very beginning.

<b>Dot</b>: Is there any final message that you would like send to the readers of the dot?

<b>Baltasar</b>: In regards to my blog, I would like to encourage people to write comments, as they are one of the main sources of energy for my work.

As for KDE: spread its word, collaborate in projects, write comments and promote free software because it has become one of the best things that humanity has ever seen.