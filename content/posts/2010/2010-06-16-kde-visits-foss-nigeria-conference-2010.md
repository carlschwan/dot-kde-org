---
title: "KDE Visits FOSS Nigeria Conference 2010"
date:    2010-06-16
authors:
  - "Frederik"
slug:    kde-visits-foss-nigeria-conference-2010
comments:
  - subject: "Happy for KDE, happy for the world"
    date: 2010-06-17
    body: "Hearing about the Nigeria conference, and other international \"reach outs\" both make me happy and proud of KDE and it's ability to be a cross-language cross-culture cross-country free software project. Lets keep doing it!"
    author: "blantonv"
  - subject: "way to go!"
    date: 2010-06-17
    body: "KDE and the KDE ecosystem are perfectly on track!\r\nMakes me feel kinda proud too, though I really don't know why ;-)"
    author: "thomas"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/100-2886.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/100-2886_small.PNG" width="337" height="247"/></a><br />Ibrahim Datsuma, one of our hosts</div>In 2010 the FOSS Nigeria Conference took place for the second time and, like last year, KDE attended the conference with two speakers. There was however a change in the visiting KDE team: Frederik Gladhorn joined Adriaan de Groot to take the place of Jonathan Riddell who could not make it this year. The venue for the conference was in <a href="http://en.wikipedia.org/wiki/Kano">Kano</a> at the well-regarded Mambayya House, the Center for Democratic Research of the Bayero University. The conference date was moved from March to April, which meant that the weather presented a different face -- hot followed by rain instead of the <a href="http://en.wikipedia.org/wiki/Harmattan">harmattan</a>.

Just like last year we were enthusiastically welcomed by Mustapha Abubakar, Ibrahim Datsuma and Jibril Muhammad. We arrived late at night so our first impressions were of higher temperatures, in comparison to last year, and a ride through the hot and dark night to reach the first hotel of our stay.

<h3>Day One</h3>

Friday was the first day of the conference where we had opening speeches by:
<ul>
<li>The representative of the Executive Governor of Kano State: Senior Special Advisor of ICT, Alh. Salisu Abdullahi</li>
<li>Deputy Director at National Information Technology Development Agency, Dr. Ashiru Adamu</li>
<li>Deputy Provost at Sa'adatu Rimi College of Education Kumbotso Kano, Dr. Garba Shehu</li>
</ul>

After the opening ceremonies, Adriaan gave an introduction on what Free Software is and the philosophy behind it. It was already noticeable that the audience showed a lot of interest.The lunch break and prayer provided welcome respite on this hot day (we never had experienced temperatures well over 40°C before). Frederik continued by giving an overview of available Linux distributions.

The questions for these talks already showed the range of knowledge amongst the listeners. They ranged from "<i>What is the difference between a distribution and and operating system?</i>" to advanced ones about what exactly sets Arch and Gentoo apart from each other. We moved on to the "History of Linux" by Adriaan, who gave a broad and fun account. In the same vein Adriaan continued with one of his favorite topics: licenses.

One of the topics we felt important was Source Code Management. After an introduction to the concepts, Frederik presented some examples with Git. The chosen system was Git because we wanted to demonstrate a distributed source code management system. Because of connectivity issues, this is much more apt for the local conditions of many of the attendees than a more centralized approach.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/CIMG8512.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/CIMG8512_small.png" width="333" height="251"/></a><br />Group picture with Hutsoft</div>

One thing that is special about this conference is that questions usually get written on paper and passed to the speakers. That gave us the opportunity to prepare a small slide for each of the questions and a little time to think about how best to answer them. After each talk session we answered questions for about half an hour. This approach also leaves a record of the questions asked for next year, so we can adjust the schedule or the programme accordingly.

<h3>Day Two</h3>

The programme changes each day, too, as the questions influence the topics to be talked about the next day. The somewhat theoretical talks of the first day led to lots of requests for more practical topics, so that's what the programme turned into. The practical side was emphasized in the morning with LPI (Linux Professional Institute) exams administered by the LPI group from Lagos. Lots of attendees sat the exam, while Adriaan and Frederik hung out at the Center for Information Technology with Mr. Auwwal Al-Hassan Tata and chatted about networking. Mr. Tata is one of the few Nigerians who drinks coffee, which is something that the KDE speakers could both appreciate (and drink lots of). The visit to the university was concluded by a short walk of the campus and a visit to the library.

Returning to the conference site, we opened the talks with a shared KDE Introduction by Mustapha and Frederik. We reported about the awesome community that we are. Mustapha could gather some first hand experience with us KDE folks at Akademy 2008 in Mechelen already. Since we had promised to show more uses of how free software can be used practically, of course we couldn't refrain from demonstrating some of our favorite applications and Plasma Desktop.

On this second day we already had many more talks held by local speakers. At last year's conference, localization of KDE software into Hausa started and Murtala Lawan gave some insights into translations in his presentation.

Lots of questions concerned installation of Linux and virtualization was also clearly a favorite, so Adriaan just mixed both together and demonstrated a Kubuntu installation inside a virtual machine. On the way there he touched on the concepts behind virtualization.

The day ended with a demonstration of the KDE Education applications by Frederik.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/cimg8458.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/cimg8458_small.PNG" width="288" height="216"/></a><br />Group picture after a long day of conferencing</div>

<h3>Day Three</h3>

On to day three, great initiatives were shown that have been started here in Kano and other Nigerian cities. First Mr. Tata presented his desktop of choice - GNOME. His delightful presentation cheered up the audience for this day. A serious side of free software was presented by Sadeeq Gambo - "Making Money with FOSS". Ibrahim Dasuma shared his knowledge of Drupal and databases. A presentation about Blender was given by Abubakar Sadeeq. This even included a demonstration of modeling the microphone stand during the talk.

An initiative that just came into life during the conference was the founding of a Python user group, so watch out for PyKano.

On this day we answered many questions that had still not been answered the previous days.

<h3>Roundup</h3>

After three days of an intense, diverse and fun conference, at last we as guests had the honor to receive the titles "Warrior of FOSS" and "Champion of FOSS". With a few more photos and the handing out of certificates, FOSS Nigeria ended.

The days after the conference we spent some more time working on translations and visited another two Universities to promote free software there as well. A special occasion was an audience with the Emir of Kazaure.

Although we had no occasion to tell bed time stories we fell asleep every night with the exciting story of the princess and the warrior (actual title of the series remains a mystery) on the Africa Magic TV channel every night. In the meantime our presentation slides made their way onto the <a href="http://fossnigeria.org/node/29">FOSSng website</a> for everyone to see.