---
title: "KDE Software Compilation 4.4.5 Available"
date:    2010-06-30
authors:
  - "jriddell"
slug:    kde-software-compilation-445-available
comments:
  - subject: "Dolphin bug fix navigation column mode not include again"
    date: 2010-07-01
    body: "Hi,\r\n\r\nThanks for your work. I have just a question about the changlog and included bugs fix.\r\n\r\nWhy this bug fix : http://websvn.kde.org/?revision=1132120&view=revision\r\nwas not include in 4.4.4 and 4.4.5 compilation?\r\n\r\nIn the Subversion repository, this issue was fixed since one month.\r\n\r\nBest regards,\r\nThomas PIERSON\r\n"
    author: "thomasp"
  - subject: "Hi Thomas,\nIt might be that"
    date: 2010-07-09
    body: "Hi Thomas,\r\n\r\nIt might be that the patch requires a string change, or was controversial, or it is just overlooked.... Commenting on the patch itself or mailing the developer's mailinglist has a much higher chance of getting your answer than posting here, btw!"
    author: "jospoortvliet"
  - subject: "Ok sorry for the noise."
    date: 2010-07-23
    body: "Ok sorry for the noise."
    author: "thomasp"
---
Despite all the work going into 4.5, those of you wanting to live a stable life have not been forgotten.  <a href="http://www.kde.org/announcements/announce-4.4.5.php">KDE SC 4.4.5</a> (codenamed "Ceilidh") is the last scheduled update in the 4.4 series and includes bug fixes and translation updates.  Particular love has been given to beasties in Konsole, KAlarm and Okular.  See the <a href="http://www.kde.org/info/4.4.5.php">info page</a> for where to download and the <a href="http://www.kde.org/announcements/changelogs/changelog4_4_4to4_4_5.php">changelog</a> for the details.
