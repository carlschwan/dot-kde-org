---
title: "KPresenter Invites Creative Minds to Template Contest"
date:    2010-07-21
authors:
  - "zagge"
slug:    kpresenter-invites-creative-minds-template-contest
comments:
  - subject: "Very interesting! Good"
    date: 2010-07-22
    body: "Very interesting! Good templates are important."
    author: "Caig"
  - subject: "Shape animations"
    date: 2010-07-23
    body: "I was really impressed by the video on Benjamin's blog about the shape animations. I am particularly excited about the resizing, as it gives a very natural method for \"zooming in\" on a particular region. I wasn't even aware that OO could do some of that stuff (if it can). Great things are coming!"
    author: "tangent"
---
Today, the KOffice team <a href="http://www.koffice.org/kpresenter/template-contest/">presents a contest</a> to create great KPresenter slide templates, offering t-shirts for the winners and of course inclusion in the next KPresenter releases for all good submissions. Read on for information on the contest!
<!--break-->
<a href="http://www.koffice.org/kpresenter/">KPresenter</a> is making great progress on the way to KOffice 2.3. Among other things, there will be hardware accelerated slide transitions -- and Google Summer of Code student Benjamin is now working on <a href="http://blog.ben2367.fr/2010/06/24/gsoc-advancement/">shape animations</a>. Jean-Nicolas is working on a slide sorter, and Thorsten has implemented a feature that imports multiple pictures to multiple slides in one go.

But we are still lacking good templates for creating new presentations. That is where we need your help and that is why we are happy to announce the <a href="http://www.koffice.org/kpresenter/template-contest/"><i>KPresenter template contest</i></a>. Create new templates for cool presentations, be it KDE themed, Free Software themed, business themed or school project themed -- or whatever tickles your fancy, and your work could be part of the next release of KPresenter. So sharpen your digital pens and get started!

The rules for submission:

<ul>
       <li>The template needs to be in ODP or OTP format</li>
       <li>The template should have a descriptive name</li>
       <li>The template needs to be <a href="http://techbase.kde.org/Policies/Licensing_Policy#BSD_License">BSD</a> licensed </li>
      <li>Final submissions must be send to kpresenter-contest@kde.org no later than midnight (UTC) on 15 September 2010</li>
</ul>

So let's be creative and create templates for KPresenter. You can create your template in KPresenter any other application that can create ODP -- like OpenOffice Impress. If you need help transforming your design into an ODP file, Thorsten (zachmann at kde dot org) will be glad to help you with that!

The top 3 entries will receive a hip KOffice t-shirt designed by KDE artist Eugene Trounev. However, all reasonably good submissions will be made part of the upcoming 2.3 release of KPresenter, which will see the light of day in October of this year. Your work will reach millions of desktop and mobile phone users! And all entries will be collected at <a href="http://kde-files.org/index.php?xcontentmode=612">kde-files.org</a>.

The jury consists of the following members:

<ul>
       <li>Aaron Seigo (Plasma maintainer and well-known KDE developer)</li>
       <li>Eugene Trounev (KDE artist)</li>
       <li>Thorsten Zachmann (KPresenter maintainer)</li>
</ul>

Thanks to <a href="http://ev.kde.org">KDE e.V.</a> for sponsoring the t-shirts.