---
title: "Akonadi Demystified"
date:    2010-08-26
authors:
  - "troy"
slug:    akonadi-demystified
comments:
  - subject: "down?"
    date: 2010-08-26
    body: "I can't access the post. Is it me only?"
    author: "zayed"
  - subject: "simple"
    date: 2010-08-26
    body: "As everyone wants to know how, when and if akonadi will eat their mail (microblog posts, contacts, babies, kittens etc, it is multifunctional) they all went to sebas' blog, which responded by going down. Now all future Akonadi users are panicking, afraid as they are that this is a sign of the future to come.\r\n\r\nHastily, everyone is searching the web for more information about what is going on, taking it down server by server. In a few days, the main domain controllers will have to give up, taking down the whole web. The USA and Russia will automatically fire their nukes blowing us all back to the stone age.\r\n\r\nAll because sebas wanted to tell us what was going on in the KDE PIM team.\r\n\r\nMaybe it won't become so bad and we'll survive this. Still I recommend to start storing canned food to get through the nuclear winter."
    author: "jospoortvliet"
  - subject: "status"
    date: 2010-08-27
    body: "sebas said on status.net/identi.ca, that his Blog went down due to load :) \u2192 http://identi.ca/notice/47567910"
    author: "ArneBab"
  - subject: "Recomend to check it out later"
    date: 2010-08-27
    body: "Yes, it's still down.\r\n\r\nFor those who can't read it, I'd really recommend checking out the article a bit later. The article has a good explanation what the benefits of akonadi are, and some examples of what new things it makes possible."
    author: "vdboor"
  - subject: "use google cache in the meantime..."
    date: 2010-08-27
    body: "http://webcache.googleusercontent.com/search?q=cache:http://vizzzion.org/blog/2010/08/demystifying-akonadi/\r\n\r\n(takes rather long to load because google tries to load images from the original blog i believe, which is still down. use \"text only,\" which loads instantly.)\r\n\r\nedit: it's a great read and explains things clearly that have been pretty misty in my mind before!"
    author: "phani00"
  - subject: "where to find kontact2 repos?"
    date: 2010-08-27
    body: "probably i'm just too daft today, but i can't find  kontact2 / kmail2 repos anywhere in the build service. perhaps it's the general shuffling of KDE repos/names, or i just didn't get something. either way, can anybody enlighten me on this?\r\n\r\n--\r\nphani."
    author: "phani00"
  - subject: "To avoid more load, a direct"
    date: 2010-08-27
    body: "To avoid more load, a direct link to the text version:\r\n\r\nhttp://webcache.googleusercontent.com/search?q=cache:http://vizzzion.org/blog/2010/08/demystifying-akonadi/&hl=nl&strip=1"
    author: "vdboor"
  - subject: "moving it ..."
    date: 2010-08-27
    body: "I'm in the process of moving the blog to a beefier machine. Just got the credentials now. Sorry for the inconvenience."
    author: "sebas"
  - subject: "Syncing and N900 Interaction"
    date: 2010-08-27
    body: "Hi!\r\n\r\nAkonadi sounds great in theory - and also looks like a step in the right direction but I'm missing some points which I personally think are VERY important for my PIM data.\r\n\r\n1.) are there any plans to enable syncing your desktop kontact with your kontact-mobile on your N900? and I mean syncing for example in the case of: you're downloading your mails from your POP3 account - sort them into some folders on your desktop - and get exactly the same folderstructure etc. on your mobile kontact after syncing..\r\nsame applies of course to local calendars, ...\r\n\r\n2.) what about the contacts stored on the maemo adressbook - or the calendar - those are completly separate to those from kadressbook/korganizer -mobile? if so - are there any plans to provide an akonadi agent to also incorporate those? the maemo contacts also support instant messaging, places, ...\r\nhave any thoughts been given to those topics?\r\nwill it be possible to start a call from kaddressbook-mobile? or write a text message?\r\n\r\nIf those points would be possible it will no doubt be the best PIM system ever... the dream of every organized person :D"
    author: "friesoft"
  - subject: "KDE PIM 4.5 in the Build Service"
    date: 2010-08-27
    body: "They are here:\r\n\r\n<a href=\"https://build.opensuse.org/project/show?project=KDE:Unstable:SC:kdepim45\">https://build.opensuse.org/project/show?project=KDE:Unstable:SC:kdepim45</a>\r\n\r\nHave a lot of fun..."
    author: "Bille"
  - subject: "thanks a lot..."
    date: 2010-08-27
    body: "...we'll see with the fun :)"
    author: "phani00"
  - subject: "um...re. the fun"
    date: 2010-08-27
    body: "it's fun to see some of the new features in action, like tagging & notes, but there are atm a few too many bugs that are correctly described as \"release blockers\". i looked up the first bunch that struck me, and they're already reported & under review, so i doubt i'd do much good as an additonal beta tester. \r\n\r\nin the meantime i need to use kmail, so i'm downgrading again, hoping for the best..."
    author: "phani00"
  - subject: "Akonadi lead me to XFCE"
    date: 2010-08-27
    body: "Hi\r\nThe last few times I've installed/upgraded KDE (Using Slackware, for well over 10 yrs now), Akonadi has caused endless fits.  Even on a clean install of the latest Slackware, going with all defaults, etc., Akonadi reports errors and refuses to let Kmail run.\r\nThis has lead me to ditch KDE entirely.  Too much baggage, too much brokenness.  I don't want or need to waste time trying to fix something that shouldn't be broken out of the box.  Crap like this is why Linux will never become a serious desktop contender.  And I'm a devoted Linux user since probably 96 or 97!!!!\r\nThanks anyway."
    author: "rkblackwell"
  - subject: "completely fine..."
    date: 2010-08-27
    body: "...nobody says everybody has to use KDE. use what works for you, that helps you getting your work done. \r\n\r\ncalling KDE 'crap,' etc., isn't really fair though. plenty people use it happily, and we still don't call your prefered distro or DE bad names. where's the need for that? why get emotional over something that's not forced on you, for that you didn't pay any money, and can live happily without? that's what i don't understand in the whole KDE/KDE3/KDE4 discussion.\r\n\r\n[side-note: if you want to really try out KDE--even though i don't suppose that's on the cards right now--you should use openSUSE. in my experience it's got the best KDE integration on the planet.]"
    author: "phani00"
  - subject: "1) Sync between deskop and"
    date: 2010-08-28
    body: "1) Sync between deskop and mobile.\r\n\r\nIn pracitce that's possible if you put your data on a Kolab server. In theory it's also possible that it works with other storage solutions, but that is noot well tested. In theory someone could step up to write a connector to communicate directly between the akonadi on your desktop and that on your phone.. In pracitce no one has stepped up to do that yet.\r\n\r\n2) Unified PIM with maemo built-ins\r\n\r\nYes, thought has been given to those topics, but no one has stepped up to do it yet, so it will have to wait until it comes to the top of the TODO list, or until someone steps up to do it.\r\n\r\nThe current aim is working software on the phones, and as few regressions as possible on the desktop. After that the core team can look at the bigger picture, but anyone else can do that at any time."
    author: "steveire"
  - subject: "fixed..."
    date: 2010-08-28
    body: "The bug you're talking about (\"Akonadi shows errors on start\") has been fixed a while ago.\r\n\r\nWhile there were problems with the initial introduction of Akonadi in KDE PIM with the 4.4 release, those have largely been ironed out by now. The next step towards a fully akonadified PIM will hopefully be a lot smoother (see above how and why)."
    author: "sebas"
  - subject: "That is the current state of"
    date: 2010-08-29
    body: "That is the current state of Akonadi/Kontact 4.5.\r\n\r\nOTOH the potential is there. If you try Akonadi/Kontact, a download + compile of Lion Mail is strongly recommended."
    author: "Alejandro Nova"
  - subject: "lion mail"
    date: 2010-08-30
    body: "thanks, forgot about lion mail. that might have made things more usable at this stage. will try again after a while. (no problem to switch up & down, since config. files & data stores are kept separate for both versions--great idea!)"
    author: "phani00"
  - subject: "the blog is accessible again..."
    date: 2010-08-30
    body: "...on a new server, presumably."
    author: "phani00"
  - subject: "Every time I start Kontact,"
    date: 2010-08-30
    body: "Every time I start Kontact, Akonadi starts up in a hail of error messages about a missing outbox folder, notifications about previous Akonadi error logs, password prompts and dialogue boxes. It tries to connect to my Gmail account when there is no network connection and fails with error dialogues. I think I killed about 5 dialogues just to get to my inbox last time.\r\n\r\nNot a very good user experience, in fact it's downright irritating. This is on KDE 4.5 using Kubuntu packages and it's been like this for about 2 releases now.\r\n\r\nHave a look at Choqok for some nicer ways to deal with no network connections (i.e silently).\r\n\r\n\r\n"
    author: "civkati"
  - subject: "Akonadi has many advantages."
    date: 2010-09-03
    body: "Akonadi has many advantages. I think it will grow fairly quickly and will soon include everyone.<a rel=\"dofollow\" href=\"http://www.auto-my.com/auto-parts\" title=\"auto parts store\">auto parts stores</a> \r\n\r\n"
    author: "masini"
---
Over on his <a href="http://vizZzion.org/blog/2010/08/demystifying-akonadi/">blog</a> Sebastian Kügler talks about the status of Akonadi, how the migration of Kontact to Akonadi is going, and in general where the direction of development is leading.

As a status update, sebas notes <em>"Initially, we had planned to release the new Kontact along with the rest of the KDE Applications 4.5. This did not quite work out, so we pushed the release back a bit, and are planning to release it along with one of the 4.5.x updates. The current plan is to release the Akonadi port of Kontact still this year. In contrast to our usual releases, this step is a bit different. We want to avoid hassle for the end-user as much as possible, so we are extending the beta phase until we think the quality is sufficient for the high demands of users, which is especially important in Kontact, where you likely spend a lot of your time."</em>
<!--break-->
The blog post also gives some more insight in how Akonadi works, and where it will be used in the future. He concludes <em>"With all the above in mind, there’s little less than a revolution going on in the groupware area. Akonadi matures further and makes possible a full-fledged groupware client in the form of Kontact, with excellent scalability and extensibility. Akonadi is built with a whole spectrum of target devices in mind, which shows in the Kontact Mobile suite running successfully on a N900. With more applications being available in their Akonadi versions, Akonadi will become a lot more useful, and enhance many other applications in the process. Akonadi also allows for a better user experience around email and calendaring in the primary workspace. Groupware is becoming a generic service on the local client. The upcoming new Kontact groupware suite is only the tip of the iceberg of what’s coming thanks to Akonadi."</em>