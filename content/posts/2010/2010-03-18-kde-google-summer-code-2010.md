---
title: "KDE in the Google Summer of Code 2010"
date:    2010-03-18
authors:
  - "lfranchi"
slug:    kde-google-summer-code-2010
comments:
  - subject: "What about kopete?"
    date: 2010-03-19
    body: "It would be nice if someone takes the time to make kopete work reliable with video-chat, specially over Jabber. "
    author: "trixl"
  - subject: "Unfortunately Kopete devs did"
    date: 2010-03-19
    body: "Unfortunately Kopete devs did not take the time to respond to my queries for ideas. It is unlikely they will get a slot this year therefor. Sorry."
    author: "nightrose"
  - subject: "I'm sorry, but I've got to"
    date: 2010-03-19
    body: "I'm sorry, but I've got to ask: That sounds as if the distribution of slots was tied to a reward system for developers. Shouldn't it be centered around the students' applications?\r\n\r\nAlso, it seems to me that a procedure like that would punish just those projects that would need the fresh blood the most (I'd certainly count Kopete to those, even though I love the application).\r\n\r\nSo, I'd be grateful for a clarification of what you meant."
    author: "onety-three"
  - subject: "true, sounds like if a"
    date: 2010-03-19
    body: "true, sounds like if a project is dying because of lack of developers, it can't get new students to fill that gap. bad"
    author: "Asraniel"
  - subject: "It's not a rewards"
    date: 2010-03-19
    body: "It's not a rewards system...\r\nBut there are not many things we expect from teams. One of them is to reply to requests for ideas, which I made repeatedly over a period of weeks.\r\nWe never have enough slots to make all the cool projects we want reality. And it would be an unfair punishment to those who actually manage to do what they are asked for.\r\nThe other point is: If the team doesn't even manage to find the time and resources to write down at least one idea, how are they going to mentor a student who might need a lot of attention - at the very least a few hours each week?"
    author: "nightrose"
  - subject: "I understand where you're"
    date: 2010-03-19
    body: "I understand where you're coming from and I'm certainly not the guy to redesign the process or hate on those who manage it -\r\n\r\nStill, I disagree with parts of your reasoning. The punishment you're talking about isn't a punishment, it would just be the lack of a reward: No special priority for those projects which manage to enter ideas.\r\n\r\nAlso, I think that projects don't enter ideas not because of a lack of time and resources (really, how long would it take to edit a wiki page? 5 minutes?) but mainly because nobody feels responsible for such janitorial tasks. Projects like Amarok have some lead developers and some active community members like you who manage such stuff thankfully, but the smaller projects lack both organization and community.\r\nIn my opinion a lack of organization doesn't necessarily mean there are no people to mentor students - it just means the mentors need to be assigned explicitly. As far as I understand that happens either way because the students can ask for mentors on mailing lists and the like.\r\n"
    author: "onety-three"
  - subject: "the summer of code involved"
    date: 2010-03-19
    body: "the summer of code involved work on both ends. the mentors and the students both need to be committed to make things work.\r\n\r\nmentoring is work--it takes time, dedication, and a willingness to follow with your student even when she is being recalcitrant. \r\n\r\nignoring repeated requests to put up ideas (and even respond to the request to put up ideas) does not really show your ability as a community or even individual developer to be a mentor. disappearing mentors are even worse than disappearing students---as we are then left with directionless student projects. so it is *not* a right to be a mentor---it is a priviledge.\r\n\r\nwe certainly will welcome kopete project ideas if a student is to submit one, and if there is an interested mentor willing to  mentor it. but the devs (i dont even know if there are any kopete devs actually) of kopete right now do not inspire much confidence as soc mentors right now. and that's something that us admins need to take into account to make the summer of code productive and engaging for the students and the community."
    author: "lfranchi"
  - subject: "Alright, I can be satisfied"
    date: 2010-03-19
    body: "Alright, I can be satisfied with that answer.\r\n\r\nPersonally I would probably give more of the responsibility for selecting the mentors to the students but there's no ideal solution and the reasons for the chosen balance appear sound.\r\nThank you."
    author: "onety-three"
  - subject: "time to read"
    date: 2010-05-11
    body: "Thank you for that information. I have a couple of friends which might be interested in this. Wright after they will finish writing the article for the paper which they were suppose to finish on the last week :(. But I'll let them know as soon as possible."
    author: "shina"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/2010soclogo.jpg" width="300" height="267" /></div>This summer the KDE project will once again participate in Google Summer of Code! Summer of Code will allow KDE to grow and bring in new developers to the community.

KDE will begin accepting student applications on March 29th, and interested students should check out the <a href="http://community.kde.org/GSoC/2010/Ideas">KDE Community wiki</a> for a list of potential ideas. Of course, individual ideas not on that page are encouraged as well, so submitting a proposal based on a new KDE application or feature is great as well.

For a list of success stories from the 2009 edition of the program (including pretty screenshots!), see <a href="http://dot.kde.org/2009/09/26/what-i-did-my-summer-holiday">our GSoC 2009 wrap-up</a>.
<!--break-->
Any questions about the program should be directed to kde-soc-mentor-at-kde.org, and students looking for mentors or wanting feedback on their ideas or proposals should contact the developer mailing list, kde-devel-at-kde.org.

Mentors must sign up on the <a href="http://socghop.appspot.com">Appspot page</a>, and students will be able to start applying there on March 29th. A <a href="http://socghop.appspot.com/document/show/gsoc_program/google/gsoc2010/timeline">full timeline</a> is available as well. All participants (students and mentors alike) are encouraged to join #kde-soc on Freenode.

Good luck to prospective students!