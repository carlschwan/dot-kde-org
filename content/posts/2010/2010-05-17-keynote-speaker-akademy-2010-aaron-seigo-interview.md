---
title: "Keynote Speaker at Akademy 2010: Aaron Seigo Interview"
date:    2010-05-17
authors:
  - "gamaral"
slug:    keynote-speaker-akademy-2010-aaron-seigo-interview
comments:
  - subject: "KDE sings the blues?"
    date: 2010-05-18
    body: "Perhaps we should take the opportunity at Akademy to record a KDE sing-along album to be bundled with all installs of Amarok?\r\n\r\nNice interview. I'm looking forward to Akademy."
    author: "Stuart Jarvis"
  - subject: "lock in"
    date: 2010-05-19
    body: "Aaron mentions how bad it is to use lock-in technology, but isn't he a skype user? I think skype is one of the worst lock-in technology today and should be avoided."
    author: "patcito"
  - subject: "different place"
    date: 2010-05-19
    body: "You've got a point, though calling it the worst lock-in technology is a bit far stretched.\r\n\r\nThe major issue at stake is building a whole new FOSS infrastructure on top of lock-in technology. This can be viewed as a problem, because the proprietor can change the rules, or pull the plug. In the KDE 1.0 days this was the case with Qt being non-free.\r\n\r\nNow others are walking down that path, and making excuses for doing so. In the short term the pragmatic \"why not use it?\" is hard to argument against. In the long term it could give trouble with a very undesirable outcome."
    author: "vdboor"
  - subject: "/me locked in, too"
    date: 2010-05-20
    body: "I use skype, too, though I hate having to do so. \r\n\r\nBut it is the kind of software I need to keep in contact, and I don\u2019t yet know free tools which I can give to my friends \u2014 and which work out of the box on GNU/Linux, MacOSX and Windows. \r\n\r\nThe requirement here is quite high, because if only a single friend can\u2019t install and use it easily, I\u2019m stuck with having to have a skype running along. And for every friend who fails at the installation I effectively harm the cause of making more people use and like free software (one bad experience in this crucial area would turn off those who\u2019d only try it because they like me and not because they believe in free software). "
    author: "ArneBab"
---
In about 6 weeks the biggest yearly gathering of the KDE community starts in Tampere. To give you all a little taste of <a href="http://akademy.kde.org">Akademy 2010</a>, <a href="http://guillermoamaral.com/">Guillermo Amaral</a> interviewed <a href="http://aseigo.blogspot.com/">Aaron Seigo</a> and asked him about his keynote.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/aseigo.jpeg" width="350" height="300"/><br />Aaron Seigo (photo credit: <a href="http://www.flickr.com/photos/8038433@N06/">Big RonW</a>)</div><b>Guillermo</b>: <i>Could you introduce yourself to any poor soul that might not yet know who you are and what you do in KDE?</i>

<b>Aaron</b>: I am, at the most basic, a hacker like anyone else in KDE: I work on Free software projects within the grand KDE community. Within KDE, I tend to wear a few different hats, though. In the (recent) past I've served as a board member and President of our global non-profit foundation, KDE e.V.; since then I've shifted more of my attention back to more of the technical and community issues that challenge and strengthen KDE. These days I am the lead developer and designer for Plasma, which is a set of projects aimed at creating both a component system designed for creating amazing primary user interfaces (those things you see when you log in or first turn on a device) quickly and easily as well as creating a set of such user interfaces for the desktop, netbook, tablet, mobile and eventually media center profiles. Outside of that work, I spend a fair amount of time on community related issues: where are working well together? Where aren't we? Why? The challenge is to find good answers that fit naturally within the culture of KDE.

<b>Guillermo</b>: <i>Is your weakness really kryptonite or is it badly written plasmoids?</i>

<b>Aaron</b>: Well, at least badly written Plasmoids can be fixed. My real weakness, however, are those who would throw freedom under the bus for a quick answer. I find it endlessly frustrating and even discouraging to watch even those in the F/OSS community who ought to know the consequences of their choices embrace lock-in technology and make excuses for why they are doing so.

<b>Guillermo</b>: <i>Can you give us the A.D.D. version of what KDE and KDE S.C. are?</i>

<b>Aaron</b>: KDE is an international community of people who have come together on their own terms to create great Free(dom) software for end users; our ranks consist of software developers, graphic designers, interface designers, writers, translators, system administrators, promoters, managers, people involved in business development, etc. As a community we create hundreds of pieces of software that range across the whole spectrum of end user software. The software compilation, or S.C., is a semiannual release of a set of those software products. This conglomeration of software gives anyone with an operating system to go beneath it the building blocks from which to realize a useful computing experience. It includes both essential workspaces, such as a desktop environment or a netbook interface, as well as dozens of productivity and entertainment titles. The compilation is a convenience for the KDE community and our user base who tend to use many of our software products together. There is even more KDE software, however, that isn't in the software compilation. Usually this is due to being software that is specific to a given audience (and so isn't broadly applicable enough for the software compilation) or which simply follows a different release schedule; these includes products like Amarok, K3B, Digikam, KDE Finance, KOffice and more.

<b>Guillermo</b>: <i>We hear that you will be giving a keynote at Akademy 2010, mind telling us quicky what Akademy is all about and will there be punch and pie?</i>

<b>Aaron</b>: Akademy is the KDE community's annual Big Event. While we have dozens of smaller events throughout the year across the globe, Akademy is the largest KDE specific event each year and is unique in that it is about all of KDE. Most of our events are both smaller and focused on a specific aspect that the KDE community is working on, such as Kontact or KOffice or Digikam or Plasma. Akademy is therefore a great opportunity to build bonds across the community, plumb our common needs and chart new directions as needed for the entire community. It's also where KDE e.V. holds its annual general meetings, which are either far more enjoyable or far drier than one might expect, depending on who you ask. I'm relying on others to organize the punch and pie. (And hopefully I won't be wearing any by the end of my presentation. ;)

<b>Guillermo</b>: <i>What would be the subject of your keynote at Akademy 2010?</i>

<b>Aaron</b>: I have been giving much thought as to what I will be discussing in my keynote. All I'll say for now is that the current plan is to take an exploration of KDE through the unique stylings of koans. We have accomplished so much and yet have much more to accomplish. Being able to hold such juxtaposed concepts simultaneously and act productively as a result is not always easy, and I hope to pull back some of the curtains that have remained draped over the windows for too long in KDE.

<b>Guillermo</b>: <i>Will we need to hire bouncers to stand at the door prior to your keynote to throw out any non-kde-developers, or will your keynote be for the general public?</i>

<b>Aaron</b>: The keynote will definitely be interesting for everyone (or at least, so I hope :), though those who are already involved in KDE, or have been in the past, will hopefully be able to take away some new and perhaps even deep insights into where we are going. I hope to spark new ideas and rejuvenate some that were left by the wayside within our community. So while it should be interesting to anyone with even a passing interest in KDE and/or Free software, I will be talking most directly to my fellow KDE people.

<b>Guillermo</b>: <i>Besides some awesome parties and non stop hacking sessions, can we expect anything else from Akademy 2010; perhaps some big news?</i>

<b>Aaron</b>: Akademy always contains a few surprises, while other things are always predictable. You have to be there to appreciate it, really. We do know there will be fantastic talks, great hallway discussion and terrific hacking going on. If three superlatives in one sentence aren't enough to get your juices flowing in expectation, though, one thing I've noted (and this is on a bit more of a serious tone :) is that upon leaving Akademy few if any are happy to be leaving: the near universal sentiment on the last day of Akademy is that it was too short and that being able to connect with KDE as a community is a powerful and meaningful thing. Even those who are not contributors in KDE come away with that feeling of deep community and purpose.

On a more practical note, I also expect more industry and business related discussion and announcements to be made at this year's Akademy than at past installments of the event.

<b>Guillermo</b>: <i>Everybody wants to know, will you or will you not sing karaoke at any point during the conference? (It has happened before and <a href="http://www.youtube.com/watch?v=TTn98m2d3pc">the proof is on YouTube</a>).</i>

<b>Aaron</b>: It could happen.

<b>Guillermo</b>: <i>A biker loved the version of My Way we did in that lonely karaoke bar in Mountain View, will you, Wade and yours truly ever get to sing together again?</i>

<b>Aaron</b>: Some things are only meant to happen once. If they happen twice, it should be as spontaneous and unexpected as the first time. :)

<h2>More from Guillermo and Aaron</h2>

After this text interview, Guillermo and Aaron did a little podcasting to go deeper into the topics Aaron wants to discuss during his keynote. <a href="http://webbaverse.com/archives/110">Listen to it here</a>.

We hope you enjoyed the interview. Be sure to book on time for <a href="http://akademy.kde.org/">Akademy 2010</a> and reserve a spot for this keynote!