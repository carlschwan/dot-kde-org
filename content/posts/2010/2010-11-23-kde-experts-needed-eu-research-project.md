---
title: "KDE Experts Needed for EU Research Project"
date:    2010-11-23
authors:
  - "cornelius"
slug:    kde-experts-needed-eu-research-project
---
The EU research project, <a href="http://www.alert-project.eu">ALERT</a>, is looking for KDE experts to assist research on free and open source software collaboration processes. The goal of the ALERT project is to develop methods and tools that improve FLOSS coordination by maintaining awareness of community activities through real-time, personalized, context-aware notification. KDE provides one use case for applying and evaluating these methods and tools.

The ALERT project is funded by the European Union as part of the <a href="http://cordis.europa.eu/fp7/home_en.html">Seventh Framework Programme (FP7)</a> research initiative. Partners from seven European countries are participating in the project. KDE is one of the free and open source software communities providing expertise and a real-world use case for testing the validity of the research results. The project will run for two and a half years.

KDE representatives are needed to define the details of the KDE use case, and to evaluate and apply the research results to the KDE community and its collaboration processes. Consequently, we are looking for three KDE community members to act as experts in context of the project. The experts will be paid and must be EU residents. See the <a href="http://ev.kde.org/resources/Announcement_ALERT_looking_for_KDE_experts.pdf">job description for KDE experts for the ALERT project (pdf download)</a> for more details and information on how to apply.

The deadline for applications is <strong>30 November 2010</strong>.