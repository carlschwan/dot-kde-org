---
title: "KDE Software Compilation 4.4.1 Out Now"
date:    2010-03-02
authors:
  - "sebas"
slug:    kde-software-compilation-441-out-now
comments:
  - subject: "Kate"
    date: 2010-03-03
    body: "*sigh*, we Kate devels messed up in the Promo area once again it seems: The changelog doesn't list a single one of our bugfixes...\r\n\r\nDuring the sprint two weeks ago tons of bugs got fixed and the Katepart should be _much_ more stable and mature now!"
    author: "Milian Wolff"
  - subject: "Changelogs"
    date: 2010-03-03
    body: "\"The changelog doesn't list a single one of our bugfixes...\"\r\n\r\nDon't see any reason why you can't just ask to add them, don't think the release team impose a freeze on the changelog.\r\n\r\nAnd it seems like the links to all SVN changes don't work, so it's not possible to browse yoyr changes that way either."
    author: "Morty"
  - subject: "Making a great desktop"
    date: 2010-03-03
    body: "even better. I can feel the changes. It feels smoother and even faster. Nice job guys.\r\nOne little thing though. In 4.3 I noticed that Kontact had a dock icon but it now disappeared. Actually I wanted to ask if it would be possible to configure Kontact so that it docks when i click the close button like Amarok or Kaffeine but the icon is now gone :( "
    author: "Bobby"
  - subject: "The release team doesn't need"
    date: 2010-03-03
    body: "The release team doesn't need to be involved, actually - any developer can (and should!) add their fixes to the changelog. And yes, it's still possible to add fixes after the release :). I've fired off a mail to Milian pointing him at the file."
    author: "Eike Hein"
  - subject: "can do for components"
    date: 2010-03-03
    body: "can be enabled for specific components (kmail, korganizer...). for kmail:\r\n\r\nsettings > configure kmail > appearence > system tray \r\n\r\n[x] enable system tray icon\r\n[o] always show kmail in system tray"
    author: "ti22"
  - subject: "you can dock any application"
    date: 2010-03-03
    body: "you can dock any application in system tray by configuring that option in kmenuedit.\r\n"
    author: "whatever noticed"
  - subject: "Thanks for the great tip. I"
    date: 2010-03-03
    body: "Thanks for the great tip. I really wasn't aware of that. The only little problem that I am having with the Kontact icon now is that it disappears from the system tray when I close the window. I would be happy if it would dock like the KMail icon."
    author: "Bobby"
  - subject: "KMail fast again!"
    date: 2010-03-03
    body: "Yay! \r\n\r\nI just tested it, and my KMail has usable speed again! \r\n\r\nMany thanks! "
    author: "ArneBab"
  - subject: "a workaround is closing"
    date: 2010-03-03
    body: "a workaround is closing kontact by clicking the icon in system tray, in stead of clicking the close button on the title bar.\r\n"
    author: "whatever noticed"
  - subject: "add to changelog"
    date: 2010-03-04
    body: "The \"math\" is simple: Add them to the changelog, and they'll be listed. :-)\r\n\r\nThe release team cannot keep track of this (I've tried in the past, and failed)."
    author: "sebas"
  - subject: "Which file?  I have some to"
    date: 2010-03-05
    body: "Which file?  I have some to add myself :-)"
    author: "odysseus"
  - subject: "I would be happy if it would"
    date: 2010-07-05
    body: "I would be happy if it would dock like the KMail icon.  <a href=\"http://bistromdreviews.org/\">Bistro MD Reviews</a>"
    author: "alexgreend"
---
KDE has <a href="http://kde.org/announcements/announce-4.4.1.php">released</a> an update to the <a href="http://kde.org/announcements/4.4/">4.4</a> series of our Software Compilation. Among other improvements, this update includes a fix for KMail hanging when sending emails that just missed the deadline for 4.4.0 and a number of fixes in many gearheads' favorite terminal emulator, Konsole.

As usual, this maintenance release also brings updated translations, but no new features. KDE recommends this update to everyone running 4.4.0 or earlier. Updated packages for many distros are becoming available as we speak.
<!--break-->
