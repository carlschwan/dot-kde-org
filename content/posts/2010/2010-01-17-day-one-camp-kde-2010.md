---
title: "Day One at Camp KDE 2010"
date:    2010-01-17
authors:
  - "jospoortvliet"
slug:    day-one-camp-kde-2010
comments:
  - subject: "Pubmed and CC?"
    date: 2010-01-18
    body: "Pubmed just indexes scientific works (and a lot of other stuff). Instead, PLoS (The Public Library of Science) releases the papers as CC attribution.\r\n\r\nI hope the keynote video will be available soon. I'm really interested as, although not directly touching Free Software, I have to deal with these topics every day."
    author: "einar"
  - subject: "Pubmed"
    date: 2010-01-18
    body: "I think it was meant to referr to Pubmed Central, which does archive papers and uses a Creative Commons license.\r\n"
    author: "tangent"
  - subject: "any videos of these presentations?"
    date: 2010-01-30
    body: "I would love to hear what Justin Kirby had to say (among others), any chance someone taped these presentations and that we see them on http://www.youtube.com/user/kdepromo one day?\r\n"
    author: "zeke123"
---
The first day of <a href="http://camp.kde.org">Camp KDE</a> is behind us, with the first of the many presentations being well attended. A full summary of the talks lives behind the break, and videos will be online shortly. If you are in the San Diego region, feel free to join us for Camp KDE for talks and training.
<!--break-->
Saturday, the first day of Camp KDE 2010 in San Diego, started with a short introduction by Jeff Mitchell. Jeff, who was the principal organizer of the conference, introduced us to a bit of history about Camp KDE and then went into some statistics about the KDE community. The conclusion was that if we maintain our current rate of growth we'll have about 6 billion developers by 2050. Continuing with this theme, he spoke about current topics in KDE such as the migration to Git and the upcoming release of KDE SC 4.4. Jeff then introduced the talks to follow, including the work on KDE-on-Windows, KOffice and KDE technology on mobile devices. 

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/preparing%20presentations%20at%20the hostel.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/preparing%20presentations%20at the%20hostel_small_0.jpg" width="300" height="225"/></a><br />Preparing presentations at the Hostel</div>

The next year should be an exciting time and will see many improvements to user-visible parts of what we do. Seeing the publicity from several major news outlets lately, like <a href="http://www.itnewstoday.com/?p=1327">IT News Today</a>, CNET and <a href="http://www.linuxjournal.com/issue/190">Linux Journal</a>, it's clear our vision is now being picked up more widely. People are starting to realize where we're going. This will bring in more interested developers who want to be on the cutting edge of innovative technology - and the KDE community is that spot.

At the end, a big thanks went to the sponsors: Collabora, froglogic, Google, KDAB, Nokia Qt Development Frameworks, UCSD, KDE e.V. Also thanked were the other people who helped Jeff organize the event like web developer and camera magician Leo Franchi, artist-ninja Eugene Trounev and UCSD liasion and swiss armyknife Andrew Huyng who functioned as the local team.

<H2>Philip Bourne on Open Data</H2>

Professor Philip Bourne followed with the opening keynote about open data. Philip Bourne is a computational biologist, the 2009 Benjamin Franklin Award winner and a leading advocate of open access to data. He's been around IT for over 13 years, when he became a teacher and researcher in computational biology. Currently his team distributes an amount of scientific data equivalent to a quarter of the Library of Congress each month.

Philip Bourne is a big advocate of openness, both for source code and data. Especially when it comes to data gathered and code written with public money it is simply wrong to let it become proprietary. He believes openness has big advantages for any type of content as it promotes learning and growth and does not discriminate. Because of these ideas he got involved with the Public Library of Science, a project to free scientific data and papers.

Philip strongly underlined something he considers crucial for sustainable free and open development (be it code or data): a good business model. With that he is not talking about a monetary model per se, but at least something which rewards people and gives them a reason to contribute to keep things going. Without such a model, or rather a variety of models surrounding a community working on something, it will not stay the same forever. There is a huge risk of commercial interests taking everything over at some point - as has happened with most scientific communities. Fifty years ago code was freely shared, as was data. Magazines were cheap, and everyone had access to the data used in scientific research. Philip presented us with a graph showing the costs of science journals for universities over the last 50 years, going from almost zero to many hundreds of millions per year. It doesn't make sense if you think about it - the researchers who wrote the articles are paid by taxpayers to write them, and review each others' articles for free as well. Why do we pay so much for this?

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/visit%20from%20dragon.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/visit%20from%20dragon_small_2.jpg" width="300" height="225"/></a><br />Welcoming Dragon</div>

The professor is now working with a variety of organizations and people to improve the way science is disseminated and comprehended. Philip believes that through openness we can increase the number of people interested in and involved with science. This way we can not just increase the amount of available data but also increase our knowledge development. As Phillips put it: "I believe open access, if broadly accepted, could change the way education works."

An important key to openness is licensing. Pubmed uses creative commons, requiring all data to be available freely online, unrestricted and in a variety of formats. Copyright will, however, remain with the author and if you use the data attribution has to be given to the original authors. An important goal behind this is the ability to do mashups. Following the popularity of Youtube, <a href="http://www.scivee.tv">Scivee.tv</a>, a scientific web 2.0 video site has been developed. It provides a mashup between video, papers, and other content. The author talks about his paper, and while he talks the site will show the content being discussed. And it works the other way around - if something in the paper is unclear, you can click it and the video will move to that point.

A more recent topic is the use of semantic tagging and linking, where a paper can become an interface to a huge mountain of related knowledge. Currently Philip is looking for ways of integrating semantic tagging in the process of scientific writing, as well as developing proper incentives to motivate the authors to do this.

Finalizing his talk, he reminded us of his three take-home points. Openness is not dependent on content type, it needs a sustainable business model, and it promotes new ways of learning and comprehension.

<H2>Distributing Free Software</H2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/the%20usual%20suspects.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/the%20usual%20suspects_small_0.jpg" width="300" height="225"/></a><br />The usual public</div>

After the keynote, Helio Chissini de Castro and Will Stephenson took the stage to talk about the work distributions are doing.

The first talk was by Helio and covered the work distributions are doing. Lately co-operation is the big thing in the distro world - each faces the same or similar issues, and talking about that with each other simply solves them faster. A win-win for all involved parties. Distributions have always had a hard time working with upstream, and Helio simply points out that the only way to do it successfully is to be persistent, maybe even annoying at times, and keep the pressure on.

Helio then moved on to his thoughts about the future for distros. Desktops are not the only devices anymore. KDE is very well prepared for this - we've built an infrastructure to quickly build an unique interface - exactly what device manufacturers want. They don't all want the same interface but they can build something unique themselves. KDE gives them the tools to do this effectively, which provides a great opportunity for distributions.

Will - packager and developer at openSUSE - then took over, explaining why free software will be very successful and why we're in the right spot to be part of it.

He started with what distributions do - selecting software, deciding on versions, splitting up, integrating things, preconfiguring software, maintaining and stabilizing applications and finally packaging it all and adding their own infrastructure to manage it including hardware configuration and an installation tool. They triage and fix bugs for upstream.  They also find and fix security issues. Distributions gather input from users and communicate it back to upstream, and work on legal issues and licenses. Last but not least, distributions provide services like multi-year support and maintenance and help with corporate deployments to their customers.

After an exhausting list of challenges you'll bump into while developing distributions, Will began painting on the whiteboard and going through the history of distributions and their work.  He stressed that we have to work together with distributions, take each other's situation into account and work within the social web we belong to. The problem is that we can only do so much with limited resources, and our success up to this point has driven end user expectations higher and higher to the point where we will not be able to keep pace.

There are three potential solutions to this:
<ul>
<li>1. Reduce expectations</li>
<li>2. Stop working on this and surrender to the web</li>
<li>3. Turn more users into contributors</li>
</ul>

Of course we should focus on the third option - get more resources in and simply become better. The demands and skills are there, but we'll have to work on it and do more with them. This conclusion quickly turned into a discussion with the whole room on how we can bring in new contributors.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/relaxed%20presentation%20skills.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/relaxed%20presentation%20skills_small_0.jpg" width="300" height="225"/></a><br />Relaxed presentation skills demonstrated by Will</div>

<H2>Coding with patterns</H2>

Usability specialist Celeste Lyn Paul wanted to help the attendees to learn a bit about designing interfaces. She started to explain how we, as humans, follow and easily recognize patterns and those can be used by developers to make their applications easier to learn and use. A design pattern is a recurring set of widgets, functionality or interactions which are found throughout software and across the environment. Celeste showed a few examples of how we're doing this right, for example with search dialogs in Okular, Konqueror and Dolphin. They are not exactly the same but follow the same pattern, doing what it is supposed to do - helping the user comprehend what is going on and what the possibilities are. Patterns create consistency and we need more of that in our KDE software. Bringing certain patterns, pieces of functionality, in libraries and reusing it all over our software is what Celeste heavily advocates.

Celeste then moved on to giving a few examples of patterns she has been working on with developers. The first example was the Get Hot New Stuff interface, our easy interface to add online content to applications. She showed how the button for Get Hot New Stuff is different among many applications. Most show the GHNS icon and phrase the text in a similar way: "Get New xxxxx". But several omit the icon, and use different wording like "download more files" and things like that.

Celeste has therefore designed a short specification of how this item is supposed to look. That way, application developers can take advantage of the pattern to keep KDE applications consistent.

A second example was with rich information lists. She showed a number of lists found in various applications, went through the items on there and pointed out the differences and inconsistencies. Some have actions (which all look and work differently), font sizes differ, icon and descriptions exist in some cases but not others. Again she provided recommendations to promote consistency, yet allow for all the different requirements for all uses of a rich information list. 

She finished her presentation with a call for volunteers, both to help work on identifying other scenarios where this happens in KDE software and also to work on developing reusable classes so that each new application which uses a similar design pattern (lists, GHNS, etc) does so in a consistent way. 

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/less%20usual%20suspect%202.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/less%20usual%20suspect%202_small_0.jpg" width="225" height="300"/></a><br />KDE's youngest contributor</div>

<H2>Free Software Jobs</H2>

After Celeste's talk Alexandra Leisse and Till Adam (barefoot!) took the stage. Their subject was discussing how participating in open source provides a unique opportunity to try new things and maybe even totally change directions in your career. They went through their own history, explaining the kind of strange places they started out (theater for Alexandra, rock music for Till) - ending up in Free Software. Both lives used to work out - but around 25-30 they decided they wanted something else and ended up at software consultancy (Till) and community management (Alexandra). They admit they never learned what they are doing right now - basically, as Till said, we just fake it... You need certain skills, of course. Like being able to deal with Divas, how to improvise, be disciplined and deliver under pressure. Be willing, eager actually, to learn. Not to be afraid of making mistakes. Be willing to wait and continue working on something until it finally pays off. Interesting thing (also true in this room): there are a lot of musicians in Free Software, even though these skills can be found in many other professions. All these things and more make Free Software contributors stand out of the crowd and be different. What is it that the drones lack? We as FOSS people can bring something to organizations. Things like a strong attention to detail - does this fit, is everyone on the same page here? We're not just willing to deal with but can fanatically embrace uncertainty - be flexible to an incredible extent. We also have a feeling for opportunities, we spot where we can go and what is possible. And finally, we often have pretty good people skills, especially for engineers.

Basically, Till argued that in Free Software you can be whatever you want. No matter how little or how much you know, if you put the effort in, you can learn and become something. It is a great place to grow and become more. When you enter and become a part of the Free Software community, you build up connections with a large number of people which presents an incredible number of opportunities. The whole community can not only help you learn and become what you want, it can help you get in contact with the right people, and provide a platform to present yourself and build up a reputation. You can present your skills and experience - and you should.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/even%20more%20relaxed.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/even%20more%20relaxed_small_0.jpg" width="225" height="300"/></a><br />Till one-ups Will, and takes off his shoes</div>

An important principle to keep in mind is the Halfling-Dragon principle: "If you find yourself in the company of a halfling and an ill-tempered dragon, remember that you do not have to outrun the dragon; you simply have to outrun the halfling."

Oh, and musicians are welcome at KDAB and Nokia!

At 15:30 it was Snack Time. Jeff had organized a variety of snacks, from potato chips and sodas to some nuts and dried fruit. Everyone slowly moved to the tables with food and drinks, while others went for the much needed nicotine.

<H2>Contributing as a non-developer</H2>

Justin Kirby then spoke about contributing if you're not a developer. He's been using KDE for many years but never really figured out how to help, as the typical path to contribution is writing code, which was not something he was interested in. At Akademy in July 2009 he finally became involved in marketing. He gave a quick overview of what areas you can contribute to without coding, and how it helps - from taking care of things a developer would otherwise have to do to simply adding something which would never have been done if you didn't do it.

Justin then went through six different types of jobs that anyone can do for KDE without knowing a single thing about writing code, and then provided specific places to look and people to contact in order to help out.  

He started out with documentation, explaining that anyone who uses KDE software is capable of helping out with adding or updating content in the <a href="http://userbase.kde.org>Userbase</a> wiki. For those uncomfortable with editing wikis, there are Klassroom courses ongoing at the <a href="http://forums.kde.org>KDE Forums</a> that cover the basics. He then talked about how you can help developers out by filing bugs when you find them and by getting involved with bug triage. The easiest way to get started with this is to participate in a <a href="http://forum.kde.org/viewtopic.php?f=4&t=84473>Bug Week</a> run by our KDE Bug Squad. Once you have some experience at this you can also become a dedicated triage person for your favorite application. There is also the Promotion team who always needs help writing articles, keeping people in the loop about important KDE activities, and generally promoting all the great work the community is doing. You can see a list of <a href="http://community.kde.org/Promo/Tasks">current Promo tasks</a> to be done on their wiki page. All of these things are something that any KDE user could easily help with in their spare time.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/get%20involved.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/get%20involved_small_0.jpg" width="300" height="225"/></a><br />Justin espouses contributors, contributors, contributors</div>

He then talked about how people with expertise in areas like graphic design or artwork could <a href="http://www.kde.org/getinvolved/art/">become a KDE Artist</a>. This team helps out regularly with designing logos, icons, themes, and things that help shape the identity of our software. For those who have experience in usability or interface design there is also the <a href="http://techbase.kde.org/Projects/Usability">KDE Usability Project</a> which aims to provide feedback to developers on how to make their applications more usable.

Last but not least, he talked about how all users should help out with supporting their fellow community members. He mentioned that this is often somewhat distribution-specific but that everyone can help out with answering questions on forums, or share tips on using KDE where possible (blogs, microblogging, etc).  He stressed that helping users fix problems is what ensures they continue to be a user of KDE and hopefully a contributor at some point as well.

<h2>Marketing in the KDE community</h2>

Your author presented the KDE marketing efforts, explained what they do and how the team currently works. He proceeded to talk about the many challenges the team faces and the opportunities there are to do more and better. The talk finished off with some discussion and the usual call for getting involved. Many of the plans and ideas can be found on <a href="http://community.kde.org/Promo/Tasks">the wiki</a> - a good starting place for those interested in joining the team.

<h2>Plasmalicious user experience</h2>

Artur Duque de Souza ended the day with his talk about users and what they want from software. The answer to that, as he described it, is that people just want to get things done and they don't care how it happens.  They just want it to be simple to get the job done.  For this reason, developing software is all about user experience - but this experience has to be designed differently when creating KDE Software for netbooks and mobile devices. 

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/Arthur.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Arthur_small_0.jpg" width="225" height="300"/></a><br />Artur in action</div>

The work by the Plasma team on getting the KDE Desktop ready for a variety of user interfaces plays a big role here. Working on the netbook interface the plasma team learned you have to create different UIs for different devices in order to continue to make life easy for the user. Another thing they learned is that some applications simply do not work on a small screen - they have to cram too much information on the screen. If KDE software moves to an even wider range of devices, work has to be put into different UIs for different devices. However, the approach of the team, starting with design and then moving on to coding has been paying off, and Plasma is quickly becoming a key player in the workspace market.

In the end, the big thing we learned is that Qt Everywhere pretty much means KDE Everywhere. But we need to focus on the design differences that arise on all these mediums and create a great user experience on all devices!

This talk concluded the day. Everyone then went off to either get some sleep (it was a tiring day) or have food and drink and talk a bit more. Details of the following days will be posted shortly.