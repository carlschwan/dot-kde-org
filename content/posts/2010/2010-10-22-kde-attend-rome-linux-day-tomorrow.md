---
title: "KDE to Attend Rome Linux Day Tomorrow"
date:    2010-10-22
authors:
  - "giovanni"
slug:    kde-attend-rome-linux-day-tomorrow
comments:
  - subject: "Sadly I missed this by a"
    date: 2010-11-19
    body: "Sadly I missed <a rel=dofollow href=\"http://www.articleblast.com/Home_Improvement/General/Bryce_Vertical_from_Vasco_-_Contemporary_Central_Heating_Radiator_Review/\" style=\"color:inherit;background-color: inherit;background-image: none;font-weight:normal;text-decoration:none;cursor:text\">this</a> by a couple of days.\r\n"
    author: "ChrisCooper"
---
Italian <a href="http://kde-it.org/">KDE blogger Giovanni Venturi</a> will be presenting KDE software and Linux to attendees at the <a href="http://www.linuxdayroma.it/">Rome Linux Day</a> (website in Italian) on Saturday 23 October. If you are near the Italian capital tomorrow do drop by.
  
The event is organised by <i>La Sapienza Linux Users Group</i>, which grew from the "Sapienza" University in Rome. The whole event is sponsored by Novell and to celebrate the 10 years of Italian <i>Linux Day</i> events, there will be an install-fest too for Linux installations on machines users want to migrate to a free operating system.

The KDE presentation will be very interactive and there will be a final surprise for everyone.
<!--break-->
