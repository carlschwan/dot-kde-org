---
title: "December Updates Further Stabilize KDE's 4.5 Series"
date:    2010-12-02
authors:
  - "sebas"
slug:    december-updates-further-stabilize-kdes-45-series
comments:
  - subject: "Bad link"
    date: 2010-12-03
    body: "click changelog, then click all SVN changes; results in not found. This is true for any of the all SVN changes links."
    author: "stumbles"
  - subject: "prerelise"
    date: 2010-12-03
    body: " Sounds good, curious to check it out.\r\n--------------\r\n"
    author: "maxprog"
  - subject: "Desktop Icon Grid?"
    date: 2010-12-04
    body: "The desktop icon grid still isn't fixed?  \r\n\r\nhttp://www.youtube.com/watch?v=swpNzWzq_50\r\n\r\nM.\r\n"
    author: "Xanadu"
---
As of today, the latest release in KDE's 4.5 series is <a href="http://kde.org/announcements/announce-4.5.4.php">4.5.4</a>, which adds a bunch of stabilization and translation updates on top of 4.5. Users in general are encouraged to upgrade to 4.5.4. The <a href="http://www.kde.org/announcements/changelogs/changelog4_5_3to4_5_4.php">changelog</a> has more details about some of the changes that went into this release.

Enjoy upgrading!