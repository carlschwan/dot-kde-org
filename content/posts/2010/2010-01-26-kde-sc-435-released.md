---
title: "KDE SC 4.3.5 Released"
date:    2010-01-26
authors:
  - "sebas"
slug:    kde-sc-435-released
comments:
  - subject: "Kate stability improved too"
    date: 2010-01-27
    body: "I think we managed to push in some quite important bugfixes for Katepart as well, making it hopefully much more robust now. At least most seemingly random crashes when moving the cursor in the document should be gone now..."
    author: "Milian Wolff"
---
KDE's release team is working hard these days as you see. Today, we're bringing to you the <a href="http://kde.org/announcements/announce-4.3.5.php">final update for KDE SC 4.3, 4.3.5</a>. Final because there are currently no more releases planned for the 4.3 branch. The KDE team will instead fully concentrate on SC 4.4, with its first incarnation coming to users across the world on February 9th.

Still, it's worth upgrading to SC 4.3.5: KIO has seen a great number of bugfixes making it a lot more robust, and a lot more "just works". Konqueror has fixed some rendering problems (for example in the statusbar). As usual, you can find a more comprehensive list of changes in the <a href="http://www.kde.org/announcements/changelogs/changelog4_3_4to4_3_5.php">changelog</a>