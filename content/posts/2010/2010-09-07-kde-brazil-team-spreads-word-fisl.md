---
title: "KDE Brazil Team Spreads the Word at FISL"
date:    2010-09-07
authors:
  - "sandroandrade"
slug:    kde-brazil-team-spreads-word-fisl
comments:
  - subject: "Cool"
    date: 2010-09-07
    body: "Sounds like a fun event and the scale (7500 people) is impressive. Brazil seems like a very exciting place for KDE right now."
    author: "Stuart Jarvis"
  - subject: "plasma widgets with no coding knowledge?!"
    date: 2010-09-11
    body: "Say what now?  No technical background required to write a Plasma widget in 20 minutes?  Are there notes from that session?  If it's really that easy I should have a try at it :)"
    author: "Justin Kirby"
---
KDE is very active in South America as any readers of the blogs at <a href="http://liveblue.wordpress.com">Live Blue</a> will know already. The KDE Brazil team attended this year's <a href="http://en.wikipedia.org/wiki/Fisl">FISL</a>, one of the major free software events in South America, meeting up with some new users of KDE software and spreading the word of Konqui.
<!--break-->
<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdebr1.jpeg" /><br />KDE at FISL</div>FISL took place in Porto Alegre from 21-24 July and gathered roughly 7500 attendees from sixteen countries, from students and enthusiasts to entrepreneurs and government representatives. There were hundreds of talks, meetings and courses, over two hundred exhibitors and hundreds of people involved in running the event. FISL always provides a great opportunity to meet new people and widely disseminate what is happening in the KDE world.

KDE Brazil had presentations ranging from geek topics to educational, business-oriented, and government-related talks. There were four KDE-related talks and meetings this year:

<ul>
<li><a href="http://kdelovelace.wordpress.com/"><b>KDE Lovelace</b></a>: free software by women. This was presented by Camila Ayres and Tomaz Canazbra. There were three talks about women in free software, the biggest one – in the same slot of ours – brought together 200 participants. Perhaps next year it would make sense to have one unified presentation.</li>
<li>The <b>Third Brazilian KDE Users and Developers Meeting</b> provided a nice opportunity to spread recent news to a general audience and receive some feedback from users and developers about KDE software. Furthermore, it was a chance to promote the <a href="http://jointhegame.kde.org">Join the Game</a> campaign, to spread the technical advances, and say, “Join us, we are a great and enjoyable family.”</li>
<li><b>Develop your JavaScript Plasma Widget in 20 Minutes</b>. Here we showed that even those with no technical background could find in JavaScript their way to KDE contribution. People were impressed with how easy one could bring their beloved application to life with fancy animations in the Plasma Workspaces.</li>
<li><b>Second Brazilian Google Summer of Code Students Meeting</b>. Although not strictly dedicated to KDE, this was a great opportunity to share experiences, tell people “You can!”, and, of course, talk a little bit about Season of KDE and our achievements during the last years.</li>
</ul>

All in all, we met a lot of cool people and had great fun introducing new people to KDE and our software. We also hope to have recruited a few new guys and girls who will carry word of KDE to their regional groups and help us in our continuing quest for world domination.