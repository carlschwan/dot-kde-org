---
title: "KDE Commit-Digest for 21st November 2010"
date:    2010-12-23
authors:
  - "dannya"
slug:    kde-commit-digest-21st-november-2010
comments:
  - subject: "bullet list"
    date: 2010-12-23
    body: "I like the bullet list that is used to format the single changes in this abstract. It is much more readable than the sentences combined in one paragraph in the main text of the digest. So I would recommend to use this list at the top of the digest, too.\r\n\r\nThanks for the weekly reports. It is always interesting to see what happens in development."
    author: "g111"
  - subject: "Git integration"
    date: 2010-12-23
    body: "First of all: Thanks a lot to the whole commit digest team.\r\n\r\nWill Enzyme be ready to cover Git commits as well as soon as the digest catches up to the point where KOffice and kdepim moved to Git?"
    author: "delta_squared"
  - subject: "Git"
    date: 2010-12-23
    body: "by the way, I'm thinking about archiving my home directory with Git to share it on my computers and having everything easily synchronized and available offline. \r\nHas anyone experienced something like that ?\r\nWill the Git KDE integration help in any way ?"
    author: "lbayle"
  - subject: "Not quite..."
    date: 2011-01-04
    body: "> Will Enzyme be ready to cover Git commits as well as soon as the digest catches up to the point where KOffice and kdepim moved to Git?\r\n\r\n---------------\r\n\r\nNot quite: i've implemented Git integration now, but this was a few weeks after the KOffice split. Also there will probably be some issues to fix regarding this integration, so they're inclusion is on a trial basis for the moment.\r\n\r\nDanny"
    author: "dannya"
---
In <a href="http://commit-digest.org/issues/2010-11-21/">this week's KDE Commit-Digest</a>:

              <ul><li>Start of a <a href="http://plasma.kde.org/">Plasma</a>-based "Welcome Screen" for applications</li>
<li>Support for searching events and todos by date/range in the Events Plasma Runner, and the start of a first Plasmoid ("BusyWidget") written in QML</li>
<li>More work on <a href="http://kontact.kde.org/kmail/">KMail</a> Mobile, including a change to make it start twice as fast</li>
<li>Start of work to give <a href="http://utils.kde.org/projects/okteta/">Okteta</a> a mobile interface</li>
<li>Support for the AqBanking5 standard (with dropped support for AqBanking4) in <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a></li>
<li>Various small features in Skrooge and Muon Package Manager</li>
<li>Work on supporting NetCDF metadata in <a href="http://kst.kde.org/">Kst</a></li>
<li>Scribo service for <a href="http://nepomuk.kde.org/">NEPOMUK</a> which allows access to NLP functionality, useful for non-C++/KDE apps</li>
<li><a href="http://ktorrent.org/">KTorrent</a> and libktorrent move to Git.</li>
</ul>

                <a href="http://commit-digest.org/issues/2010-11-21/">Read the rest of the Digest here</a>.
<!--break-->
