---
title: "Interview With Netbook Master Marco Martin From the KDE Plasma Team"
date:    2010-02-21
authors:
  - "giovanni"
slug:    interview-netbook-master-marco-martin-kde-plasma-team
comments:
  - subject: "At it again..."
    date: 2010-02-23
    body: "I see Marco's at it again with the teasing for features in the next SC: http://www.notmart.org/index.php/Software/A_remote_notification\r\n\r\nI wasn't aware before that Nokia had sponsored the development of the netbook interface - that's cool"
    author: "Stuart Jarvis"
  - subject: "traveling"
    date: 2010-02-24
    body: "It is funny going to all these different places and seeing the same faces. Like just try going to a conf without Jos, I dare you. :)\r\n\r\nAnyways \"KDE is a community\" really is the case, its not just a slogan. Can't agree more."
    author: "eean"
  - subject: "It sounds lke Marco has done"
    date: 2010-03-05
    body: "It sounds lke Marco has done some great work on Plasma - which itslef looks like a very neat little user interface.  More and more I think it is important that their are UIs specificatlly designed for the small-screen applications.  <a href=\"http://www.footballshirtsearch.co.uk/\">RyanJ</a>"
    author: "RyanJ"
---
A while ago, Giovanni Venturi interviewed <a href="http://www.notmart.org/">Marco Martin</a> for <a href="http://kde-it.org/">an Italian KDE blog</a> (so if you read Italian, please see there for the  <a href="http://kde-it.org/2010/02/16/intervista-a-marco-martin-di-kde-plasma/">authentic version</a>). For our Italian-challenged readers, we are pleased to present an English translation below.

Marco "Notmart" Martin is of course well known as one of the primary developers of the new Plasma Netbook workspace and for his habit of teasing us via <a href="http://www.notmart.org/">his blog</a> with news of new features for the X+1 release of our Software Compilation just before version X is released...
<!--break-->
<b>Hello Marco, can you introduce yourself to our readers?</b>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/me.jpg" width="237" height="350"/><br />Marco Martin</div>ok:) My name is Marco Martin, I'm 28 years old, I live near Turin and I finished university about a year ago. I've started contributing to KDE in 2007, when I got fascinated by the potential of Plasma. At the moment I am lucky enough to be sponsored by Nokia to work on KDE and, in particular, on the Plasma Netbook project.

<b>When did you first use KDE software?</b>

I think it was about 2000-2001, I just started using Linux. I considered the environment that was installed by default on the Red Hat version of that time (was about 6.0, 6.2?) really not so pretty (it was a weird mix of GNOME 1.x and Enlightenment) and I was looking for something better. Then I saw some beta of KDE 2.0, and never looked back :) .

<b>What part of KDE development are you involved in?</b>

I mostly work in the Plasma team, on the library and on the main workspaces: Plasma Desktop and Plasma Netbook.

<a href="http://www.kde.org/workspaces/plasmanetbook/">Plasma Netbook</a> is a <em>primary</em> user interface (in simple terms this is what is always visible or always reachable, no matter what you are doing with your device right now) specifically designed for small screen devices, but with traditional input methods such as keyboard, mice and touchpads. So it is ideal for devices like netbooks, but lately we are doing work specifically for touchscreens. This means that we will be able to share some functionality with potential future workspaces: Plasma Mobile, Plasma Tablet or whatever.

We think that a device like a netbook needs a different kind of user interface, because besides having different hardware specifications, the use case is pretty different too. While a normal desktop and laptops are "general purpose" machines, used or both "creating" and "consuming" content (with a strong accent on multitasking), a netbook - or more and more an even smaller mobile device - excels as a viewing tool for web resources, multimedia and so on. This is why the first thing you see is a full screen launcher and there are those "pages" of widgets; they give information, often from online sources, in the fastest way possible. In some cases they can avoid you having to open the web browser to look up some particular information.

I think I've been talking enough on this topic now :) . If you want more, watch this <a href=http://www.youtube.com/watch?v=oYy4OS1sB4A>video on Youtube</a> to get a better idea of the end result .

<b>What are the missing items in KDE software that you think we should develop?</b>

KDE is full of elements that have enormous potential, some of them are coming to fruition just now, some still need work, but there is progress indeed.

I can try to recap some of what I think will be important points...

What will be really important are tools for content creation, for creation of any kind of file, permitting users to do any kind of work with applications using the KDE framework. KOffice applications play a really important role. On a similar level an independent application like KDEnlive (and I hope many others soon) are also very important.

Another really important aspect is how much our tools can become "online". We can express this in a sentence full of buzzwords like "surviving in the web2.0 era". It sounds cheesy but gives the idea.

So not only is it important to have a good web browser (a component that unfortunately lags a bit behind right now) but also tools that can free certain tasks from the web browser, offering a rich user interface far better than anything a web UI can offer right now. An example? <a href="http://blogilo.gnufolks.org/">Blogilo</a>. I would like to see many more applications like that.

<b>What are the KDE applications that are doing a very good job? And what needs lots of love and help?</b>

Perhaps this is a bit of self-praise (but is mostly a praise of the other wonderful team members, really ;)) but I think that the workspace (so Plasma and KWin) is doing a pretty good job. It has been hard, because at the beginning of the 4.x lifetime there was still too much work to do , but now I think it's starting to give results.

Similarly, for <a href="http://pim.kde.org/">KDE PIM</a>, lately it has gotten way better, and will be noticeable especially in SC 4.5, when Akonadi will be really put into use.

What needs help... as I was saying it's vital we will have again a web browser that is <em>the best</em>, KOffice&nbsp;is of vital importance too. Its team is doing a wonderful job and the next months/years will be crucial.

<b>What's been your main effort for Software Compilation 4.4? What's the big news compared to 4.3? Have you got plans for 4.5?</b>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/sal_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/sal.png" width="350" height="210" /></a><br />Search and Launch in Plasma Netbook</div>In SC 4.4 I mostly worked on the various parts of Plasma. What is more visible of course is the new Plasma Netbook workspace (of course made possible with the help of various Plasma team members).

Another thing that I consider really important is the progress on the new specification (and implementation) for the System Tray. It will permit to go beyond the limits the current specification has and will have also possibilities for better integration with applications written in other toolkits (Ubuntu already adopted the spec).

Speaking of 4.5, probably in Plasma there will be less big visible changes, but more refinements.

Some features started in 4.4 will be brought in their full potential (there will be a whole new activity manager for instance). One of the most important things will be refinements in terms of stability and performance.

<b>What do you think of KDE on Windows and MacOS X? Can we have more KDE users in this way?</b>

I think it's really important from a technical standpoint because it helps us to write code that is as platform independent as possible. Even if the Linux platform is my favorite, it is also important from a strategical standpoint - especially in an enterprise environment - for us to provide a homogeneous set of software in a mixed environment.

<b>Are you working with Nokia? Do they pay you to develop for KDE too? What did you do and what are you doing now for Qt?</b>

Yes, I work for Nokia (in particular for Qt Development Frameworks). The job is to work specifically on Plasma technology, in particular the Plasma Netbook workspace. The goal was to develop a GUI optimized for a netbook in the least time possible, and I'm thankful they gave me this opportunity.

For them I've also done some technical demos on the multitouch capabilities of Qt 4.6, like you can watch <a href="http://www.youtube.com/watch?v=b749KvoX3w4">on YouTube</a>.

<b>When people talk about deploying a product on more than one platform they often think of Java, but Qt software has a lot of features and is multiplatform too. Now with the version 4.6 we have Qt for Symbian OS too. Can you give us some differences between them? In which case can you use Qt and which one Java?</b>

Java is a programming language, but also a whole environment with a virtual machine. A Java program is not compiled as a native executable, but in a bytecode that has to be interpreted. So if a program is properly written, it will be able to run on any platform where a Java interpreter is available, but this comes with a price on performance and you're limited to its language (even if some experimental compiler for other languages exist).
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/Dubbione_di_Pinasca1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Dubbione_di_Pinasca.jpg" width="350" height="262" /></a><br />Dubbione di Pinasca, near Turin</div>
Qt is a set of C++ libraries that attempts to expose platform-dependent functionality as little as possible. So a program using Qt (and possibly other multiplatform libraries) can be compiled on all platforms supported by Qt with minimal changes. It also has some good quality bindings to other languages, so you're not limited to C++. For C++ applications it is necessary to recompile the sources on every platform, but you'll have a native executable, so it will be faster and more integrated.

<b>What motivates/keeps you motivated to work on KDE?</b>

One thing is its impressive technology, but this would never be enough by itself. The thing that motivates me more is its community. KDE is full of fantastic people, many of whom I can call friends right now. The sense of community and friendship within KDE is unrivaled.

A simple example can be Camp KDE 2010 which was held in January in San Diego, California. I had never traveled so long before, and if one thinks about it for a second it is very impressive that you can be sure that - even if you are on the other side of the world - you'll find friends. No matter if they belong to a different culture, no matter if you only met them on IRC before, you can be sure they will be people with whom you'll have a certain "feeling" (even if you won't always agree :)) and with whom you'll share an important passion.

<b>If you could choose where you can live what place you will choose and why?</b>

This is a difficult question :) I love where I live now (a little village near Turin) even if I find it quite limited and I would like something different, but in the end home sweet home right? :)

<b>What's been the last book you read?</b>

At the moment I'm reading "An earth gone mad" by Robert Dee, on an old Italian book series called "Urania" (that every Italian geek should know :D), I love old sci-fi stories from 60's or so, especially if forgotten and unknown (and often honestly really bad ;p)

<b>Marco, thank you for your time and good luck with KDE development.</b>