---
title: "Linux Magazine Welcomes Scriptable Plasma Widgets"
date:    2010-07-22
authors:
  - "Stuart Jarvis"
slug:    linux-magazine-welcomes-scriptable-plasma-widgets
comments:
  - subject: "Awesome! Plasma as coverstory!"
    date: 2010-07-22
    body: "Awesome! Having plasma as cover story is pretty cool. :D I hope more plasma exposure will come out of this!"
    author: "vdboor"
  - subject: "Very nice"
    date: 2010-07-23
    body: "I had a sense that Plasma is powerful and aimed to be easy to use, but I hadn't yet looked into it. Even given my high expectations, I was impressed. Nice design, folks!\r\n"
    author: "tangent"
  - subject: "yay"
    date: 2010-07-24
    body: "nothing more to say =)"
    author: "Vamp898"
  - subject: "Nice...what about JS frameworks?"
    date: 2010-07-28
    body: "Since I heard that you can make a plasmoid using JS I wanted to try. this article makes it easier, tanks!\r\n\r\nA question about JS... is there any JS framework available or plans to use them? I'm talking about jQuery, MooTools, etc... any chance of using such frameworks in a plasmoid?\r\n\r\nI guess they should be stripped down or something, because some of the functionality doesn't make sense in a plasmoid enviroment, but effects and that kind of stuff would be usefull to develop plasmoids."
    author: "tunic"
  - subject: "Hmmm, interesting idea.\nAsk"
    date: 2010-07-30
    body: "Hmmm, interesting idea.\r\n\r\nAsk on plasma-devel@kde.org or go to #plasma-devel on IRC to find out more..."
    author: "jospoortvliet"
---
KDE software features on the cover of this month's Linux Magazine as Marcel Gagné <a href="http://www.linux-magazine.com/Issues/2010/116/PLASMA-BABY">explains how easy it is to create your own custom Plasma Desktop experience</a> by crafting Plasma widgets using JavaScript.

Focusing on Plasma Desktop 4.4, Marcel describes the ability to quickly create custom widgets as "the very future of computing, a future you can take part in". He explores the possibilities of network transmitted Plasma widgets and praises the portability of widgets, noting that the same widget can be used on your PC running Plasma Desktop, your notebook running Plasma Netbook and even on your smartphone with Plasma Mobile.

Marcel goes on to give some examples of simple JavaScript based Plasma widgets and introduces PlasMate, the new application that aims to provide a simple integrated development environment for creating Plasma widgets.

In conclusion, Marcel calls the ability to easily script new Plasma widgets "an amazing piece of forward-looking code" inviting inexperienced coders and dabblers in to the world of writing useful widgets for the KDE community.