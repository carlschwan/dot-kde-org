---
title: "KDE Gears Up to a Free Cloud"
date:    2010-01-24
authors:
  - "areichman"
slug:    kde-gears-free-cloud
comments:
  - subject: "wrong link"
    date: 2010-01-24
    body: "The link to the website links to this article instead."
    author: "patcito"
  - subject: "And the website is full of"
    date: 2010-01-24
    body: "And the website is full of spam http://owncloud.org/index.php/Features\r\n\r\nedit: looks like both issues have been fixed, whoever fixed it could at least answer me so I don't get downvoted :p\r\n\r\nedit2: and the spam is back now"
    author: "patcito"
  - subject: "Info from presentation"
    date: 2010-01-24
    body: "I also found the slides from Frank very informative: http://www.socialdesktop.org/kdeandthecloud.pdf"
    author: "vdboor"
  - subject: "Great news"
    date: 2010-01-25
    body: "I think this is the right way to go. I'm really looking forward to this.\r\nIf I will use the cloud, then it has to be open source, so I can decide where and how my data is stored."
    author: "chris"
  - subject: "Yes, great thing! \n*It would"
    date: 2010-01-25
    body: "Yes, great thing! \r\n\r\n*It would be great if it supported web acces to a torrent client on your home machine so you can admin your torrents trough any mobile phone or other.\r\n\r\n*You can install ownCloud on a machine with dynamic Ip and make posible the machine sends to your email the external Ip every time it changes so you don't have to use dyndns to reach your machine."
    author: "lorenzofh"
  - subject: "And that, my friends, is how you save Free Software"
    date: 2010-01-25
    body: "Take back the cloud!"
    author: "Bugsbane"
  - subject: "I'd really love to see this."
    date: 2010-01-25
    body: "I'd really love to see this."
    author: "nethad"
  - subject: "Then get involved"
    date: 2010-01-26
    body: "Then get involved ;-)\r\nSeriously, Frank needs a lot of help to make this happen..."
    author: "jospoortvliet"
  - subject: "help appreciated!"
    date: 2010-01-26
    body: "I'm sure input, either on the wiki or esp with code, will be greatly appreciated... To make this happen the team needs more ppl!"
    author: "jospoortvliet"
  - subject: "I can't be the only one a bit worried..."
    date: 2010-01-26
    body: "I can't be the only one worried by GHNS handling installation of binary packages... can I?\r\n\r\nI mean... shouldn't that be handled by repositories and package managers(KPackageKit)? If stuff on kde-apps.org becomes available in GHNS, wouldn't it allow for another attack vector and add inconsistency/confusion to installing applications?\r\n\r\nOr am I the only one that worries about this? :/"
    author: "madman"
---
<a href="http://dot.kde.org/2010/01/21/camp-kde-2010-continues-more-talks">Day 2</a> of <a href="camp.kde.org">Camp KDE</a> kicked off with a bang when Frank Karlitschek announced the start of a significant new KDE project. The ownCloud initiative will complement the Social Desktop and Get Hot New Stuff efforts which are already dealing with social and collaborative data. Like those, the ownCloud initiative strives to combine the rich desktop interfaces made possible by the Qt and KDE libraries with the large amount of social information and data users are putting online.
<!--break-->
Traditionally, a user has been limited to the data on the device he/she is using. Recently this has changed with the introduction of a variety of online services such as Last.fm, Pandora, Facebook and flickr - a significant portion of data is now in the cloud rather than local hosted. ownCloud will allow users more freedom in choosing and changing services, even in running their own hosted services.

<h2>The cloud</h2>

The advantages of storing data in the cloud are many: ubiquitous access to data from multiple devices, social interaction with millions of others on the web and no extra software to install. However, the data is often owned by several different organizations, which don't easily allow interaction or sharing of data among them. A blog hosted on Blogger is very difficult to connect with pictures hosted by Yahoo; sending an email from Gmail to all Facebook contacts is currently impossible and Hotmail users cannot access a Google Documents file without creating a Google account. Besides these convenience issues, there are also problems with privacy and security as well as the potential for one hardware failure to make the data of thousands of users impossible to access. Taken together, the cloud is not perfect.

<h2>ownCloud</h2>

Having an open platform for cloud services such as social network technologies, online communities, and online storage provides possibilities beyond what current proprietary service providers can offer. ownCloud aims to give everybody a personal cloud, letting them store, share and interact with their data from everywhere. But unlike competing implementations, ownCloud allows the user to easily mash up and connect data from different providers, decide where his or her data is stored and even run their own cloud server to keep things completely under their control.

If and when a serious number of providers join this effort or support the interface, nobody will have to join a myriad of different social networks just because he or she has friends on all these incompatible, separate clouds - they will all be able to connect. If users are unhappy with a provider, they will be able to move on to another provider without losing all of the information and connections they have created with that provider, opening up the market and creating opportunities for new companies and innovative services. As an added benefit, users concerned about their online security and privacy can stay in full control of their own data by running their own cloud server.

<h2>The first building blocks</h2>

The Social Desktop initiative, started a few years ago, is already bringing social capabilities to the Free desktop. For potential data providers, a reference implementation is available and several projects such as Maemo.org and <a href="http://forums.kde.org">the KDE Forums</a> have already announced they will become data providers. The Get Hot New Stuff (GHNS) framework, which has existed since the KDE 3 series and has seen significant improvements in the upcoming KDE SC 4.4 release, provides another building block for a free cloud. Thanks to GHNS it is easy to install, rate, comment on and share content like wallpapers, application extensions and widgets with the world. The GHNS team is currently working with the OpenSuse Buildservice team to make it possible to easily build and distribute applications, bringing easy software deployment to the Free Desktop.

The final building block necessary to complete this vision is the online storage of individual data, sharing of that data and collaborative editing. For this, the Free Desktop does not have a unified solution but the ownCloud team aims to provide one, starting with the storage of personal data. Building upon existing technologies, the ownCloud team is working to integrate with the Free desktop and in time create a cross-platform solution. Future releases will address the issues of data sharing and collaborative editing.

The ownCloud team is tentatively planning to release version 1.0 in the next 2 months, with a web interface as well as WebDAV support. All components of the ownCloud infrastructure will be either released under the GPL, LGPL or AGPL licenses.

<h2>Get involved</h2>

While a significant amount of work has already been done on the foundations of the ownCloud initiative, a vision this large will only be possible if many people share the work. If the prospect of freeing your personal data and being able to combine and share it in new, innovative ways excites you, check out <a href="http://ownCloud.org">the website</a>, contact Frank Karlitschek at karlitschek@kde.org or check out the <a href="http://gitorious.org/owncloud/">git repository</a> and together we can take back the cloud.