---
title: "Behind KDE: Meet Ben Cooksley"
date:    2010-08-27
authors:
  - "tomalbers"
slug:    behind-kde-meet-ben-cooksley
comments:
  - subject: "Finally"
    date: 2010-08-27
    body: "...there is face to the person i work with since years! You are doing a great job, Ben, keep it that way."
    author: "neverendingo"
  - subject: "Cool, a fellow Wellingtonian,"
    date: 2010-08-28
    body: "Cool, a fellow Wellingtonian, great to see another Kiwi about the place :-)  Just a shame I'm London based now or we could finally have had a KDE-NZ!"
    author: "odysseus"
---
<p>In the second episode of the new Behind KDE series of interviews with KDE sysadmins, we meet KDE's "identity expert", Ben Cooksley - the guy behind the new identity.kde.org, which will be launched next week.</p>

<p>Ben is one of the leaders involved in setting up key parts of the new git infrastructure. Normally he's quiet and avoids publicity - another reason to put him in the spotlight. This interview even contains one of the rare pictures that are available of Ben.</p>

<p>Ben is also one of the people behind the KDE Forums - he is the one who isn't scared of terms like LDAP and Gosa and can write bug-free sysadmin shell scripts blindly. He's the only one who does everything with Grace.</p>

<p>Intrigued? Confused? Bored? Educate yourself and liven up your day with this latest interview from <a  
href="http://www.behindkde.org/node/799">Behind KDE</a>.</p>
<!--break-->
