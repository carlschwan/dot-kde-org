---
title: "KDE Commit-Digest for 14th November 2010"
date:    2010-12-13
authors:
  - "dannya"
slug:    kde-commit-digest-14th-november-2010
comments:
  - subject: "I really like the new bullet"
    date: 2010-12-13
    body: "I really like the new bullet point-like style of the abstract on the dot. Great work!"
    author: "mutlu"
  - subject: "Windows 7 like launchers"
    date: 2010-12-14
    body: "Woooohoooooo yeeeeeah! YES! YES!\r\n\r\n.\r\n.\r\n\r\nI am pleased to read that news, keep up the constrictive work. And thank you to the Commit Digest for anouncing this information. \r\n\r\n+1 for the bullet style."
    author: "BurkeOne"
  - subject: "Changes and changes"
    date: 2010-12-15
    body: "Yes! The new \"bullet point-like style\" of the abstract on the dot adds clarity!\r\n\r\nBy the way, the title of the digest is \"KDE Commit-Digest for 14th November 2010\". This seems to be a \"copy and paste and forgot to change\" error. I suggest you to follow a list of fixed steps every time you publish something, to avoid irreparable errors that damage your reputation.\r\n\r\nThanks for keeping us updated!"
    author: "ubu-msux"
  - subject: "What's the error?"
    date: 2010-12-15
    body: "What's the error in \"KDE Commit-Digest for 14th November 2010\"?"
    author: "Stuart Jarvis"
---
In <a href="http://commit-digest.org/issues/2010-11-14/">the latest KDE Commit-Digest</a>:
<ul><li><a href="http://solid.kde.org/">Solid</a> switches to the new KDE Power Management System Policy Agent</li>
<li>"Atmosphere" and other layers, and more work on routing instrunctions in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>Demo Mode in Kajongg</li>
<li>Full KIPI support (print, email, etc of images) added to KSnapshot</li>
<li>An "Export" button added to <a href="http://gwenview.sourceforge.net/">Gwenview</a>, providing quick access to the KIPI export plugins</li>
<!--break-->
<li>Various improvements in <a href="http://techbase.kde.org/Projects/Kooka">Kooka</a></li>
<li>Support for hiding menu icons in KDE applications</li>
<li>Plugin loading support added to KNotify</li>
<li>Support for Powerdevil notifications when a battery's storage ability falls below 50%, or the battery has been recalled from the vendor</li>
<li>Support for Windows 7-like launchers in the Tasks Plasmoid</li>
<li>ACL viewing/editing implemented in <a href="http://kontact.kde.org/kmail/">KMail</a> Mobile</li>
<li>Smb4K now mounts and unmounts shares via a KAuth implementation</li>
<li>Telepathy tubes support in KRFB</li>
<li>NEPOMUKBackupSync, with configuration UI enables automatic and manual backup and syncing of the <a href="http://nepomuk.kde.org/">Nepomuk</a> respository</li>
<li>Improved version of the Locale KControl module</li>
<li>SecurityOrigin class of WebCore (useful for guarding against cross-site scripting attacks) imported into <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a></li>
<li>KBugBuster moved to unmaintained/tags/4</li>
<li><a href="http://kplayer.sourceforge.net/">KPlayer</a> and <a href="http://kile.sourceforge.net/">Kile</a> move to git.kde.org.</li>
</ul>

<a href="http://commit-digest.org/issues/2010-11-14/">Read the rest of the Digest here</a>.