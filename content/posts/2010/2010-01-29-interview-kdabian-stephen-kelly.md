---
title: "Interview with KDABian Stephen Kelly"
date:    2010-01-29
authors:
  - "sebas"
slug:    interview-kdabian-stephen-kelly
comments:
  - subject: "Note taking"
    date: 2010-01-29
    body: "When I hear \"notes\", I immediately think of hand-written notes (think MS OneNotes or MS Journal). Any plans to add stylus support to either KDEPIM or KOffice (non-Krita)?"
    author: "christoph"
  - subject: "for Italian readers"
    date: 2010-01-30
    body: "Here the URL of the Italian translation: http://kde-it.org/2010/01/30/intervista-con-stephen-kelly-di-kdab/"
    author: "giovanni"
  - subject: "Congrats!"
    date: 2010-02-02
    body: "Congratulations to Stephen for winning a Qt 4.6 contribution award! His contributions to the Model/View framework has been greatly appreciated! :)"
    author: "mbm"
---
<a href="http://www.kdab.com" title="The Qt Experts">KDAB</a> have <a href="http://www.kdab.com/index.php?option=com_content&view=article&id=149&Itemid=18">published an interview with Akonadi-hacker Stephen Kelly</a>, who also recently won a Qt contribution award. In the interview, Stephen explains how his contributions to the Model/View infrastructure in Qt make dealing with complex models, such as those used in Akonadi to display emails and other personal data, easier. In the interview, Steve also hints at the future of note-taking in KDE.