---
title: "2010 Plasma Javascript Jam Session Winners Announced"
date:    2010-04-14
authors:
  - "aseigo"
slug:    2010-plasma-javascript-jam-session-winners-announced
comments:
  - subject: "How to install them?"
    date: 2010-04-15
    body: "I downloaded the ShortLog plasmoid and tried to install it in my Kubunto 10.04 KDE SC 4.4.2 but it doesn't work. How can I install it?\r\n\r\nThanks"
    author: "Vide"
  - subject: "Re: How to install them?"
    date: 2010-04-15
    body: "On KDE SC 4.4 you need to install an updated version of the Javascript bindings:\r\nhttp://plasma.kde.org/plasma-scriptengine-javascript-4.4-errata.tar.bz2"
    author: "notmart"
  - subject: "Contact with developers"
    date: 2010-04-15
    body: "I'd like to know how can we (as users) can share some thoughts with the developers. I was testing shortlog and I have some ideas that I'd like to discuss with the developer.\r\n\r\nThanks for organizing this competition and for giving us a way to participate... well, KDE is actually a community! "
    author: "johanner"
  - subject: "in touch with dev"
    date: 2010-04-15
    body: "in the package, there is a metadata.desktop file and in there is an email address for the author.\r\n\r\nyou can also see this by hovering over its entry in the Add Widgets interface. we've also considered adding this information to the configuration dialog so you can get to it easily from a running instance of the plasmoid."
    author: "aseigo"
  - subject: "Can't find Aquaria on get hot"
    date: 2010-04-16
    body: "Can't find Aquaria on get hot new staff, also does not install directly from file - shows error on 4.4.2 on opensuse.\r\n\r\nAny chance to get this working on opensuse? I see the above advice to update javascript engine, but can't find such package on kde4 factory repository and prefer not to install such staff by hands."
    author: "benderamp"
  - subject: "Still does not install"
    date: 2010-04-19
    body: "Installing the plasmoid still fails after the javascript bindings update.\r\n\r\n"
    author: "kallikles2003"
---
<p>Submissions for the <a href="http://plasma.kde.org/cms/1238">Plasma Javascript Jam Session</a> closed at the end of March and the judging commenced, with the community joining the judging panel as a collective "5th member". Every <a href="http://forum.kde.org/viewtopic.php?f=157&t=86964">Plasmoid that was submitted</a> was a success and a joy to try out: congratulations to all of those who participated.</p>


<p>It was a competition with prizes up for grabs, however, so we had to make the required tough calls and declare some winners. Without further ado, here are your 2010 Javascript Jam Session competition crown bearers!</p>

<h3>Top 2010 Javascript Jammer: Ontje Lünsdorf with <a href="http://plasma.kde.org/javascriptjam/2010/aquarium.plasmoid">Aquarium</a></h3>

<p>Combining clean code, good design and a fun idea, Ontje's Aquarium claimed the top spot on the podium. Maybe it was how it drove us to feed our fish hoping to breed the more elusive species or the nice implementation of the Boid algorithm that did the trick, but Ontje will be receiving a brand new Nokia N900, an invite including airfare and lodging to a KDE event and a KDE T-Shirt for his efforts.</p>

<div align="center"><image src="http://plasma.kde.org/javascriptjam/2010/screenshots/aquarium.png" width="292" height="186"></div>

<h3>1st Runner Up: Nikita Melnichenko for <a href="http://plasma.kde.org/javascriptjam/2010/make-progress-0.1.0.plasmoid">Make Progress!</a></h3>

<p>This clever Plasmoid is a technically solid, highly intuitive and very useful creation. The judges found it easy to figure out how to use and the looks were outstanding. Nikita will also be making progress of his own to a KDE event (airfare and lodging included) with his new KDE T-Shirt as a result!</p>

<div align="center"><image src="http://plasma.kde.org/javascriptjam/2010/screenshots/makeprogress.png" width="200" height="200"></div>

<h3>2nd Runner Up: Manuel Unglaub's <a href="http://plasma.kde.org/javascriptjam/2010/shortlog.plasmoid">Shortlog</a></h3>

<p>A simple and fresh approach to storing (and finding!) those little snippets, notes and jewels, Shortlog shot Manuel into the 2nd runner up spot. As with Make Progress!, this Plasmoid captured attention for its easy of use and high utility. Hopefully Manuel, inspired by the KDE T-Shirt he'll be receiving for his efforts, will continue to explore the possibilities of his Plasmoid concept pushing it to new heights.</p>

<div align="center"><image src="http://plasma.kde.org/javascriptjam/2010/screenshots/shortlog.png" width="400" height="173"></div>

<h3>The Bragging Rights Titles</h3>

<p>There were also three titles up for grabs that will entitle the creators of those Plasmoids bragging rights for 2010.</p>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/dice.png" width="100" height="100" align="right">
<p>The <b>Beauty Queen</b> title was reserved for the most stunning Plasmoid in form and function and was claimed by Petri Damstén for his Plasma Dice creation. Petri demonstrated his artistic chops and a command of the animation framework to rise above the rest in this metric.</p>

<p>The first place winner, Aquarium, also earned Ontje the title of <b>Technical Giant</b> while the <b>Creative Genius</b> crown has been placed on Nikita's brow for Make Progress!</p>

<p>All three of the bragging rights titles were close races, with Andrew Stromme's <a href="">Guitar Tuner</a> and Shortlog coming in second more than once!</p>

<h3>Kudos To All, And To All .. Start Thinking of Next Year?</h3>


<p>Each and every one of the contestents is a winner for participating and sharing the fruits of their passion and creativity with all of us. We have a number of excellent new tools and toys to keep us busy and amuse us and the <a href="http://techbase.kde.org/Development/Tutorials/Plasma#Plasma_Programming_with_JavaScript">Plasma Javascript</a> support has grown and improved as a result as well.</p>

<p>This year's event was so successful that we are already seriously considering running another such competition next year, perhaps even extending it to runner plugins and DataEngines written in Javascript. So keep your thinking caps nearby, those fingers dripping with Plasma and Javascript and hopefully we'll see even more excellent submissions in 2011!</p>