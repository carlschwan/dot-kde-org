---
title: "KDE PIM Stabilization Sprint"
date:    2010-06-03
authors:
  - "steveire"
slug:    kde-pim-stabilization-sprint
---
<p>Recently the KDE PIM team had one of their regular face-to-face sprints. 15 developers met in the <a href="http://www.kdab.com">KDAB</a> offices in Berlin for discussions, API review, development, and of course, community building (free dinner \o/!). The <a href="http://community.kde.org/KDE_PIM/Meetings/Akonadi-2010-05#Schedule_2"> schedule</a> shows the breadth of topics covered at the meeting, ranging from Nepomuk widgets to bugfixing. The focus in this meeting was stabilising and planning for the upcoming 4.5 release cycle. Apart from the regular members of the KDE PIM team, the meeting was also attended by two members of the MeeGo development team who are already using KCal and wish to work upstream in KDE with some modifications and improvements.</p>

<h3>Releasing the Akonadi based KDE PIM stack</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/pimsprint.jpg"><img src="http://dot.kde.org//sites/dot.kde.org/files/pimsprint-wee.jpg" width="400" height="267"/></a></div>

<p>Akonadi is a server and library providing access and generic APIs for  accessing PIM data, such as emails, addresses and events. It is designed to provide benefits to applications such as better performance and flexibility, lower resource consumption, portability, and the ability to easily create resources for accessing data in multiple locations, such as cloud services.</p>

<p>The port of KDE PIM applications to Akonadi is expected to bring these benefits, as well as providing room for future experiments in user interfaces and trends of user interaction. The separation of the data access interfaces from UI layers of applications serves to enable the quick development of new user interfaces with the same reliable codebase. Conversely, the porting of KDE  PIM applications benefits Akonadi and the new platform by consuming the new technology, uncovering bugs and validating the design and hard work put into developing the platform.</p>

<p>The KDE PIM team is eager to release the Akonadi based applications to start getting user feedback and attract new developers to the team. However, the port is a huge job, so to minimise the risk for users, the KDE PIM module will skip the KDE SC 4.5.0 release to allow extended bugfixing. It will be introduced in the KDE Software Compilation version 4.5.1. KMail maintainer Thomas McGuire <a href="http://thomasmcguire.wordpress.com/2010/05/14/akonadi-meeting-and-the-kde-sc-4-5-release/">detailed the decision and its consequences on his blog</a>.</p>

<h3>MeeGo work on KCal to be upstreamed to KDE repositories</h3>

<p>KCal has been used for some time as part of the PIM stack on the Maemo/MeeGo platform and has been extended in various ways to meet Maemo specific needs. In the best tradition of Free Software, that work was started as a branch of KCal, but work on integrating the changes to the KDE repositories has now started. That integration will involve the creation of the new  libraries KCalCore containing the most useful central parts of KCal and KCalExtended with the MeeGo specific extensions.</p>

<h3>New developments</h3>

<p>Apart from reviews and discussions, the weekend also provided ample time for developement. KMail, KAddressBook and KOrganizer all received a lot of attention, as well as an innovative AccountWizard. Tom Albers posted a <a href="http://www.omat.nl/2010/05/13/accountwizard/">blog and <a href="http://www.omat.nl/2010/05/17/accountwizard-demo/">video</a> about the scope and use of AccountWizard. There was also a lot of work on mail filtering, migration tooling for settings and data from KMail 4.4 to 4.5 and of course unit testing and stabilisation.</p>

<h3>Conclusion</h3>

<p>The KDE PIM team meets regularly 3 times a year for stabilisation and  planning.</p>

<p>Face to face meetings like this help strengthen community bonds and allow progress and discussions not possible with IRC or email. For more information, be sure to check out the quotes section of the meeting page. We look forward to the next meeting and to seeing the new direction our platform will take!</p>