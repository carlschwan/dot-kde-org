---
title: "Plasma Javascript Jam Session Contest"
date:    2010-02-13
authors:
  - "aseigo"
slug:    plasma-javascript-jam-session-contest
comments:
  - subject: "Clarification of the competition rules?"
    date: 2010-02-13
    body: "<p>This is probably nit picking, but should the start of the second paragraph read:</p>\r\n<blockquote>\"Anyone (except <b><i>named</i></b> members of the judging panel) may participate in this open challenge ...\"</blockquote>\r\n<p>Otherwise I read this as implying that because the KDE community are listed as part of the judging panel they are ineligible for the contest - which I doubt is the case! :-)</p>"
    author: "matt"
  - subject: "Pretty awesome"
    date: 2010-02-13
    body: "Wow.. just have to say. This is a pretty awesome competition, with some neat details included :D"
    author: "vdboor"
  - subject: "Hi! I downloaded an article"
    date: 2010-06-15
    body: "Hi! I downloaded an article from torrents <a href=http://www.torrentbasket.com> http://www.torrentbasket.com </a> I've come to the following conclusions on reading it. I would like to see interactive desktop with widgets. Like fast way to add a notes (not just paste text to desktop when desktop is unlocked!) or mindmaps. Something similar what Basket has (or if someone would get basket 2.0 started someday it would help many other. It just is not about Plasma and javascript!) but for desktop. It would be great to just pop-up the dashboard and click somewhere and start typing. Then drag a nice lines around all typed notes and get a mindmap from it. "
    author: "Brigitte777"
---
We are pleased to announce the <a href="http://plasma.kde.org">Plasma Javascript Jam Session</a>. This friendly competition will reward creators of the most original, interesting and beautiful Plasma widgets (Plasmoids) written in Javascript with some great prizes and community recognition.
<!--break-->
Anyone (except members of the judging panel) may participate in this open challenge that starts on Friday February 12th, 2010. The rules are simple:

<ul>
<li>Only Plasmoids written using the <a href="http://techbase.kde.org/Development/Tutorials/Plasma/JavaScript/API">Simplified Javascript Plasmoid API</a> may be entered.</li>
<li>All submissions must be released under a Free software license in compliance with the <a href="http://techbase.kde.org/Policies/Licensing_Policy">KDE Licensing Policy</a>.</li>
<li>All submissions must be the original work of the contestants. Third party Javascript libraries, DataEngines, etc. may be used, but the actual Plasmoid itself must be the work of the contestant.</li>
<li>Each contestant may submit one, and only one, Plasmoid for judging. Contestants may work in teams (an artist and a programmer is a common pairing in Plasmoid development, for instance) but only one prize per submission will be offered regardless of team size and contestants may not be a member of more than one submitting team.</li>
<li>Final submissions must be in the form of an installable .plasmoid file submitted to javascriptjam@kde.org by midnight (UTC) on March 31<sup>st</sup> 2010.</li>
</ul>

Plasmoids will be judged based on the following criteria:

<ul>
<li>Usefulness / Entertainment Quality (40%): accounting for a full 40% of the final score, this metric reflects how indispensable, fun and "recommend it to my friends"-worthy the Plasmoid is.</li>
<li>Originality (20%): the more unique the Plasmoid, the better it will do in this category.</li>
<li>Beauty (20%): for Plasmoids that inspire desire, these points go higher!</li>
<li>Technical (20%): code poetry and Plasmoids that expose the full power of Plasma will rack up technical proficiency points.</li>
</ul>

The prizes up for grabs are really exciting:

<ul>
<li>Grand Prize: A brand new Nokia N900, a trip to join us at a KDE developer event, such as Akademy or Camp KDE, and a KDE t-shirt</li>
<li>1st Runner Up Prize: A trip to join us at a KDE developer event, such as Akademy or Camp KDE, and a KDE t-shirt</li>
<li>3 Honorable Mention Prizes: A KDE t-shirt</li>
</ul>

In addition to these over-all prizes, three bragging-rights titles are up for grabs:

<ul>
<li>Beauty Queen: this crown is reserved for the most stunning Plasmoid in form and function</li>
<li>Technical Giant: the Plasmoid that embodies the peak of technical excellence will walk away with this badge of honor</li>
<li>Creative Genius: the Plasmoid with the most interesting and original concept will claim this title</li>
</ul>

Additionally, everyone who submits a working Javascript Plasmoid that meets the contest requirements will receive a personalized certificate of participation by email. All submissions will be published for download on <a href="http://kde-look.org">kde-look.org</a> after the results are announced on April 9th.

Contestants will also be able to take advantage of training and support from the KDE Plasma team! Training sessions will be held on Friday the 12th, Saturday the 13th and Sunday the 14th of February at 16:00 UTC on irc.freenode.net in the #plasma-training channel. Each session, led by Plasma developers, will cover the Simplified Javascript Plasmoid API in detail along with Plasmoid development tips and tricks.

In addition, contestants are welcome to ask questions and solicit development advice on #plasma on irc.freenode.net and plasma-devel@kde.org, the official Plasma development mailing list, during the competition. We won't write your Plasmoid for you, but each contestant will have access to the same level of Q&A support that all Plasmoid developers normally have. Helpful reference materials can also be found in the <a href="http://techbase.kde.org/index.php?title=Development/Tutorials/Plasma">Plasma tutorials section on Techbase</a> as well as in the KDE Examples module.

The judging panel will be comprised of:

<ul>
<li>Aaron Seigo, Persona Plasma</li>
<li>Marco Martin, Plasma Zen Master</li>
<li>Nuno Pinheiro, Graphics Design Machine</li>
<li>Richard Moore, Javascript Bindings Artiste</li>
<li>KDE Community: Yes, that's <i>you</i>! A poll hosted on the <a href="http://forum.kde.org">KDE Community Forums</a> will allow everyone in the KDE community to have a say in who wins.</li>
</ul>

Panel members will rate each entry individually in each of the four categories. The scores from the five judges will then be added up to create the final results.

The contest timeline is as follows:

<ul>
<li>February 12<sup>th</sup>: The Javascript Jam begins!</li>
<li>February 12<sup>th</sup> and 13<sup>th</sup>: Online training sessions at 18:00 UTC in #plasma-training on irc.freenode.net</li>
<li>March 24<sup>th</sup>: Contest entries can be submitted for inclusion by sending them to javascriptjam@kde.org</li>
<li>March 31<sup>st</sup>: The Javascript Jam is finished, no more entries may be made after this date!</li>
<li>April 2<sup>nd</sup>: All valid entries are published on the <a href="http://plasma.kde.org">contest website</a> and on <a href="http://kde-look.org">KDE-Look.org</a></li>
<li>April 9<sup>th</sup>: Winners announced<sup></sup></li>
</ul>

Finally: a huge round of "Thank-You!"s to Nokia for donating the N900, KDE e.V. whose support makes this event possible and Nuno Pinheiro and Sean Wilson for artwork.

More information along with the official rules can be found on the <a href="http://plasma.kde.org">Plasma website</a> which is is devoted to the Javascript Jam for the duration of the contest. After the Javascript Jam concludes, the Plasma website will relaunch with new content and an updated design.