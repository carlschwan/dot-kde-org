---
title: "KDE and GNOME Desktop Summit 2011 from 6 to 12 August"
date:    2010-10-06
authors:
  - "jospoortvliet"
slug:    kde-and-gnome-desktop-summit-2011-6-12-august
comments:
  - subject: "Yay :-)"
    date: 2010-10-07
    body: "Not much more to say really... Do they still have those tasty doughnuts there?\r\n\r\nLooking forward to meeting some of the GNOME peeps this time too."
    author: "Stuart Jarvis"
  - subject: "What happened to CampKDE?"
    date: 2010-10-08
    body: "As excited as I am about the next Desktop Summit I haven't heard anything yet about CampKDE? Did I miss all the news? Is it not being held this year?"
    author: "areichman"
  - subject: "doughnuts?"
    date: 2010-10-08
    body: "not quite typical for berlin. you should try the original stuff invented in berlin, such as http://en.wikipedia.org/wiki/Doner_kebab or the even more famous http://en.wikipedia.org/wiki/Currywurst\r\n\r\nguten appetit"
    author: "chhtm"
  - subject: "."
    date: 2010-10-08
    body: "Hehe! There could be interesting games, football matches and so on between the two communities ;=)\r\n"
    author: "djszapi"
  - subject: "Great location"
    date: 2010-10-09
    body: "That is a great location for a conference."
    author: "vinum"
---
The Desktop Summit is a co-located event which features the yearly contributor conferences of the GNOME and KDE communities, GUADEC and Akademy. Next year the conference will take place from 6 to 12 August, 2011  in the city center of Berlin at the Humboldt University, Unter den Linden. The event will feature keynotes, talks, workshops and team building events.

Find the official announcement of KDE e.V. and the GNOME Foundation <a href="http://ev.kde.org/news.php#itemDESKTOPSUMMIT2011from6to12AugustinBerlin">here</a> and the Desktop Summit website <a href="http://www.desktopsummit.org">here</a>.

<strong>Desktop Summit</strong>

The GNOME and KDE communities develop the majority of Free Software desktop technology. Increasingly, they cooperate on underlying infrastructure. By holding their annual developer flagship events in the same location, the two projects hope to foster collaboration and discussion between their developer communities. Furthermore, KDE and GNOME aim to work more closely with the rest of the desktop and mobile open source community. The summit presents a unique opportunity for all of us to work together and improve the free and open source desktop for all.

At the Desktop Summit 2011 the organizing team expects well over a thousand core contributors, prominent open source technology leaders, government, education and corporate representatives and hundreds of open source desktop engineers, usability experts and designers. Local and international IT industry are invited to learn about and join in developing the latest innovative desktop technology. The event is locally supported by the TSB Innovation Agency Berlin GmbH and the Berlin Senate. 

<strong>Location</strong>

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/panorama_mit_museumsinsel.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/panorama_mit_museumsinsel_wee.jpeg" width="350" height="232"/></a><br />Berlin Mitte with the University</div>

The conference will take place at the 200 year old <a href="http://www.hu-berlin.de/standardseite-en">Humboldt University</a> in the district of <a href="http://www.unterdenlinden.de/?sid=2">Under den Linden</a>, part of the city center of Berlin. This area has plenty of good hotels and hostels, great restaurants and shopping areas and  good public transport - all that in walking distance. Cultural needs can be met by the near-by Deutsches Historisches Museum, the City Library of Berlin and many other prominent buildings and institutions. A must-see for visitors of Berlin, the Brandenburger Tor, can be found on the street the university is on.

The registration for the Desktop Summit will open on 1 February 2011 and a call for papers will be issued. Talk submissions will be due on 15th of March and one month later the conference program will be announced. Saturday 6 August kicks off 3 days of joint keynotes, talks and social events followed by four days of collaborative workshops. The conference officially ends on Friday, 12 August.

<strong>Important dates</strong>

<ul><li>Febuary: registration for Desktop Summit will open; Call for Papers issued.</li>
<li>15 March: Deadline for travel & hotel reimbursement requests. Registration remains open until the conference.</li>
<li>15 April: Feedback on travel and hotel reimbursement requests.</li>
<li>6 - 8 August: Kickoff and joint keynotes, talks and social events</li>
<li>9 August: General Assembly of KDE e.V.; Gnome Foundation meeting</li>
<li>9-12 August: Collaborative workshops</li></ul>

<strong>Website up</strong>

Meanwhile the website went online on <a href="http://www.desktopsummit.org/">DesktopSummit.org</a> where you can read a bit of information on the conference, its history, the upcoming desktop summit and the location. More content will be added over the coming months. You can also subscribe for news on the conference so you will be notified whenever there is something going on.