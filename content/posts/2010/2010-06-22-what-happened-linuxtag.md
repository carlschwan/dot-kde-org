---
title: "What Happened at LinuxTag"
date:    2010-06-22
authors:
  - "jospoortvliet"
slug:    what-happened-linuxtag
comments:
  - subject: ":D"
    date: 2010-06-23
    body: "aaaaand as usual, points for Eckhart who did the organisation part together with Torsten (who also deserves a hug or two)!\r\n\r\nhas been a very fruitful LinuxTag, let's hope the next one will be at least half as good :D"
    author: "jospoortvliet"
---
Last weekend a team of KDE volunteers (wo)manned a booth at LinuxTag. As hopefully many of you have already read about (and maybe already joined) on Wednesday the <a href="http://dot.kde.org/2010/06/09/announcing-kde-ev-supporting-membership">new Supporting Membership program</a> was launched. There is some more content upcoming, but for now we'd like to give you all a quick taste of what those four days were like.

On Tuesday afternoon, Eckhart Woerner and Frederik Gladhorn arrived at the KDE e.V. office in Berlin, not only to say hello and have quick look at the office but also to equip the boothbox. Then everybody took off to the fairground venue in order to set up the booth.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/800px-Linuxtag.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/350px-Linuxtag.jpg"></a><br/>Enjoying LinuxTag</div>Wednesday saw Frank Karlitschek and Jos Poortvliet presenting the Join the Game program, followed by an interview with Radio Tux. Unfortunately Wednesday morning at 10 was not the best time as there were few members of the public around at that point. The show was however recorded and streamed live by <a href="http://blog.radiotux.de/2010/05/25/sendeplan-linuxtag-2010/">Radio Tux</a>. At the booth we had more luck approaching people; quite a few decided to sign up during the week.

Meanwhile several <a href="http://dot.kde.org/2010/06/07/join-kde-game-linuxtag-2010">KDE talks were given</a> and in the evening we were invited to the Qt community dinner where we chatted with the Cuties and a bunch of other people like the openSUSE team. Afterwards some went for (more) beer or cola; others went to bed.

Come Thursday, it was busier at the booth, which consequently gave the booth team less time to wander around and talk to people at the other stands. Still, we had the occasional visit of a Troll or two (ok, these days <a href="http://qt.nokia.com">Cuties</a>, yes), we managed to sign up some Ubuntu people to our supporting membership and lured in a few openSUSE booth members as well. The free lunch given by the LinuxTag people was enjoyed with great pleasure by those who attended (your writer managed to <i>forget</i> it) and there were talks given and attended. At the end of the day the lucky four winners were drawn for the <a hrefr="http://dot.kde.org/2010/06/15/dinner-winners-supporting-membership-draw-linuxtag">Supporting Membership Dinner</a>.

During the conference, Radio Tux interviewed Frederik Gladhorn about the <a href="http://blog.radiotux.de/2010/06/11/lt10-frederik-gladhorn-kde-netbook-desktop/">KDE Plasma Netbook interface</a> (interview in German) and Frank Karlitschek participated in the discussion around <a href="http://blog.radiotux.de/2010/06/11/lt10-ingo-ebel-torsten-grote-und-frank-karlitschek-diskussion-rund-um-cloud-computing/">cloud computing</a> (also in German). Frank also did an interview with Vincent Unz and Stormy Peters from GNOME.

The evening had a nice 'beach party' - there was sand at 'Box at the Beach' and beach chairs, but no sea. However, there was beer and a barbecue and a party afterwards. Plenty of time to mingle and talk to people from other teams and admire the usual weird collection of t-shirts.

Some of us were a little late on Friday, but it was a very productive day nonetheless. It was pretty busy and we reined in quite a few new supporting members. Unfortunately they didn't have a chance to attend the dinner, which was to take place that night. Saturday was by far the busiest day and thus mostly spent on talking to visitors. In the evening Ubuntu sponsored a barbecue at <a href="http://www.c-base.org/">C-Base</a> and most of the team attended.

Overall, the event was a very positive experience as usual and hopefully will be remembered by many people as the moment they <a href="http://jointhegame.kde.org/">Joined the Game</a>.