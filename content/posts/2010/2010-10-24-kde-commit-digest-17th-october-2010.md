---
title: "KDE Commit-Digest for 17th October 2010"
date:    2010-10-24
authors:
  - "dannya"
slug:    kde-commit-digest-17th-october-2010
comments:
  - subject: "Obligatory..."
    date: 2010-10-25
    body: "Thanks Danny!"
    author: "odysseus"
  - subject: "Phew..."
    date: 2010-10-26
    body: "Phew... I was starting to think nobody had read it! :)\r\n\r\nDanny"
    author: "dannya"
  - subject: "Digest"
    date: 2010-10-26
    body: "Then let me also say, it's wonderful having the digest back! It is my favorite \"article\" on the dot.\r\n"
    author: "tangent"
  - subject: "Thank you Danny!"
    date: 2010-10-26
    body: "Thank you very much Danny.\r\n\r\nWe were a lot of people expecting the commit digest since you posted about a possible return of it.\r\n\r\nNow, sundays will be better :D"
    author: "Poldark"
  - subject: "Thanks"
    date: 2010-10-26
    body: "Since nobody else seems to think that far I'll go ahead and say thanks not only to Danny but also to Jonathan Thomas, Marta Rybczynska, Dominik Tritscher, Marco Krohn and Xavier Vello. Thank you!\r\n\r\nOf course at the moment Danny's contribution towers above the rest because he created the whole platform and leads the effort, but it couldn't/wouldn't be done without the others so I'm just as grateful to them.\r\n\r\nIn that vein it might be a good idea to put the list of contributors somewhere more prominent on the page - and, above all, not in a vertically scrolling niche where a few of the names are hidden by default. The current situation is a bit unfortunate in that regard."
    author: "onety-three"
  - subject: "Agreed"
    date: 2010-10-26
    body: "<cite>In that vein it might be a good idea to put the list of contributors somewhere more prominent on the page - and, above all, not in a vertically scrolling niche where a few of the names are hidden by default.</cite>\r\n\r\nI agree that it's a bit hidden now, and the scrolling is bad (doesn't look good either). Why not add the names under a section at the top, similar to e.g. the <a href=\"http://www.archlinux.org/static/magazine/2010/ALM-2010-Feb.html\">Arch Linux Magazine</a>.\r\n\r\nLastly, thanks all contributors for this digest!"
    author: "Hans"
  - subject: "Krita"
    date: 2010-10-26
    body: "Those who liked the Krita part, don't forget to <a href=\"http://www.youtube.com/watch?v=Jhet6qOvWx4\">check out the speedpaint</a> created by NDee (as a non-artist I still find it very cool) and other amazing works in the <a href=\"http://forum.kde.org/viewforum.php?f=138\">Krita gallery</a>."
    author: "Hans"
  - subject: "When you left, dot.kde.org"
    date: 2010-10-27
    body: "When you left, dot.kde.org kind of died without the digest (and KDE4.0 initial drop in users). It never recovered, lets hope with some advertising and the digest, the engine will start turning again."
    author: "Elv13"
  - subject: "kind of..."
    date: 2010-10-28
    body: "I certainly agree that the digest is a big draw for the dot (count me amongst those thrilled to have it back) but I think the causes that you state for a decline in dot commenter activity is a bit off. \r\n\r\nIn the last few years we've had the arrival and promotion of the forums and a huge expansion of the planet. These are often much better places for directed discussion than dot articles and it seems to me at least that that is where a lot of the discussion has moved to. "
    author: "borker"
  - subject: "Another factor is that"
    date: 2010-10-30
    body: "Another factor is that comments started to require an account for quite a while."
    author: "Eike Hein"
  - subject: "Dot = awesome."
    date: 2010-11-01
    body: "Well, sometimes it becomes a bit repetitie to keep saying \"Awesome stuff!\" each time. A lot of times that's what I think after reading it :D\r\n\r\nJust posting that everytime with 0 comments yet made is a bit weird perhaps. Having each dot posting start with the first comment by \"vdboor\" saying \"Awesome, keep on going!\". I've once proposed a \"Like/Dislike\" rating for stories, I hope this makes it easier to give quick feedback. :)"
    author: "vdboor"
  - subject: "true too"
    date: 2010-11-01
    body: "I know that there are people with a strong desire to have anon. posting but I must admit I personally don't really miss all the fly-by trolling that associated account-less commenting. It got so any dot story that had the words 'amarok' or 'plasma' in the title got trolled to hell and any article that wasn't about either of those two subjects got turned into one... and then trolled to hell."
    author: "borker"
---
In <a href="http://commit-digest.org/issues/2010-10-17/">this week's KDE Commit-Digest</a>: Start of <a href="http://koffice.org/">KOffice</a> UI Abstraction project, to enable implementing various UIs on top of KOffice without in-depth knowledge of KOffice MVC APIs. Support for picture bullets in the .docx format filter in KOffice. "Reply Without Quoting" in <a href="http://kontact.kde.org/kmail/">KMail</a> (kdepim/mobile). Enable dropping of raw data (like one could do in KDE3) in <a href="http://dolphin.kde.org/">Dolphin</a>. Configurable routing profiles in <a href="http://edu.kde.org/marble/">Marble</a>. First approach toward a version of <a href="http://edu.kde.org/kalgebra/">KAlgebra</a> for mobile devices. "Save Plot" function now saves to a SVG file in <a href="http://edu.kde.org/kalzium/">Kalzium</a>. New contextual menu in HTML pages for export as PDF, ODT, image, and copy, in <a href="http://skrooge.org/">Skrooge</a>. "Postpone Break" feature in <a href="http://www.rsibreak.org/">RSIBreak</a>. Initial support of Geolocation in <a href="http://choqok.gnufolks.org/">Choqok</a>. A configuration dialog for the Muon Updater. upower backend for PowerDevil 2. New StartupFeedback effect in KWin. Minimal implementation of a Windows CE backend in <a href="http://solid.kde.org/">Solid</a>. Support for defining "helper protocols" (e.g. "mailto URLs should launch the executable kmailservice") using .desktop files. Various optimisations across KDE-PIM and KOffice. Initial version of a Git plugin for Dolphin. More new mimetype icons in <a href="http://oxygen-icons.org/">Oxygen</a>. More intuitive keyboard shortcuts for rotation in <a href="http://okular.org/">Okular</a>. A global way to allow KDialogs being embedded in graphics views. libplasma can now be built without KIO.

<a href="http://commit-digest.org/issues/2010-10-17/">Read the rest of the Digest here</a>.
<!--break-->
