---
title: "Camp KDE 2011 Call for Locations Posted"
date:    2010-05-24
authors:
  - "jefferai"
slug:    camp-kde-2011-call-locations-posted
---
The <a href="http://camp.kde.org/call_for_locations.html">Call for Locations for Camp KDE 2011</a> has been posted. If you are interested in Camp KDE 2011 being in your city, check out that page and send the requested information to the Camp KDE Organisers at campkde-organizers(at)kde.org. We look forward to seeing you in January!
