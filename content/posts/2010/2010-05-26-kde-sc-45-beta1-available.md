---
title: "KDE SC 4.5 Beta1 Available"
date:    2010-05-26
authors:
  - "sebas"
slug:    kde-sc-45-beta1-available
comments:
  - subject: "Gratulations"
    date: 2010-05-26
    body: "Gratulations to everybody who who contributed to this release. KDE SC 4.5 will be a very polished and nice release. Great work friends! :-)"
    author: "FrankKarlitschek"
  - subject: "Many many thanks!"
    date: 2010-05-26
    body: "I just started my emerge to test it as soon as possible. \r\n\r\nThank you for all your relentless work! "
    author: "ArneBab"
  - subject: "Kubuntu .debs?"
    date: 2010-05-27
    body: "I'm not too comfortable compiling things from source but  would really love to try this out! Are there any .deb files available to install instead? And if so where?\r\n\r\nThanks KDE keep up the amazing work!! :D"
    author: "connelhooley"
  - subject: "new system tray"
    date: 2010-05-27
    body: "Has the new system tray been copied over from Windows 7 (http://kde.org/announcements/announce-4.5-beta1.png)? I've always wondered how they could have done such an awfully looking thing? And now KDE has cloned it? Oh why? I hope I'm wrong."
    author: "atrox"
  - subject: "err, the Windows 7 systray"
    date: 2010-05-27
    body: "err, the Windows 7 systray looks like this: http://www.netzwelt.de/gallery/21116-system-tray.html\r\n\r\nIf you don't like the default plasma theme, install another one, there are quite a lot."
    author: "Zapp"
  - subject: "Yeah.. and you don't see the"
    date: 2010-05-27
    body: "Yeah.. and you don't see the similarity? :D"
    author: "atrox"
  - subject: "Ah yes, another thing: hidden"
    date: 2010-05-27
    body: "Ah yes, another thing: hidden icons in new system tray will open in popup, just like in Windows 7 :)\r\n\r\nI hope I'm wrong about it too."
    author: "atrox"
  - subject: "Wow..."
    date: 2010-05-27
    body: "I was set to miss 4.5 till openSUSE included it officially (11.4 or whatever; just hoping for more stability), but the moment I saw the screen-shot I've changed my mind; though I'll stick to 4.4.3 till rc1\r\n\r\nKeep it up guys!!!\r\n\r\n[Did not find where to post but the image on the announcement page links to http://www.kde.org/announcements/4.4/images/general-desktop.jpg]"
    author: "Yogi"
  - subject: "KDE was planning for a new"
    date: 2010-05-27
    body: "KDE was planning for a new system tray for a long time already. Plasma's whole concept for example, was presented by Aaron Seigo in 2005.\r\n\r\nThat KDE4 now looks like copying Windows 7 is unfortunate. Someones, yes an idea is just better, and copying takes place. At other times, a certain solution is just the most logical thing to do. So both parties invent the same thing (the new expand action in the system tray is an example of this). Please be aware Windows 7 also took good idea's from MacOS and KDE 4 while they were still in the inception phase. And when KDE is late to finish the game, it now looks like KDE was copying.\r\n\r\nThe design goal for the new system tray was not to make it look white (or ugly as you state), but to give plasma more control over it. The mouse over effects work consistently now, and integration/mesh-up of the taskbar and system tray is nowadays possible (like the MacOS dock does). The white looks a bit weird to me too, but OTOH I can really appreciate the monochrome icons in the MacOS system tray. It takes a while to get used to, but it works much nicer in the long run."
    author: "vdboor"
  - subject: "Any kubuntu packages"
    date: 2010-05-27
    body: "Please .... and congrats."
    author: "molecularbiophysics"
  - subject: "Funny, funny, funny. Some"
    date: 2010-05-27
    body: "Funny, funny, funny. Some months ago I was reading all over the net how Win 7 copied KDE 4's panel and system tray. Now someone seems to be awake from his winter sleep and sees it the other way around.\r\nWell, if it works and looks good then it shouldn't matter who copied whom."
    author: "Bobby"
  - subject: "This is it!"
    date: 2010-05-27
    body: "Congratulations KDE guys and girls. I have been testing Beta 1 for a few hours now and I have to say that it's very hard to believe that it's a beta. It's very responsive, stable and looks good on top of that. I can't imagine you topping this for the final release ;)"
    author: "Bobby"
  - subject: "Actually I don't care who"
    date: 2010-05-28
    body: "Actually I don't care who copies who - if it's for good, it's totally OK by me. The problem in this case was purely in these white icons and system tray expanding by popup. If icons are changeable to current-fashion (theme) easily and expanding will be configurable, these new defaults are OK."
    author: "atrox"
  - subject: "I don't care who copies who."
    date: 2010-05-28
    body: "I don't care who copies who. But if something is changed to worse (to my mind), then I start to wonder why this change is done. If Windows 7 didn't have these icons, I would still complain about them :P"
    author: "atrox"
  - subject: "openSUSE Packages of KDE SC 4.5"
    date: 2010-05-28
    body: "We're about to make it even easier to test an unstable KDE release... stay tuned."
    author: "Bille"
  - subject: "new KMail"
    date: 2010-05-28
    body: "\"The new version of KMail will be delivered as part of one of KDE's monthly bugfix updates.\"\r\n\r\nDoes this mean that KMail2 will be within updates in 4.5.1 or smth? So instead of fixing bugs (within a bugfix-release) it will introduce new major features and likely bugs along with them?"
    author: "atrox"
  - subject: "Even easier?"
    date: 2010-05-28
    body: "I thought that KDE:KDE4:UNSTABLE was already very easy for testing of KDE packages (although it installs them system-wide). Now I'm really curious..."
    author: "einar"
  - subject: "KDE PIM will do beta/RC cycles in the mean time"
    date: 2010-05-28
    body: "It's not like it'll be dropped on users without warning. Although separate from the SC release, KDE PIM will make alpha/beta and RC releases for testing prior to the official release."
    author: "einar"
  - subject: "But KDE PIM still IS a part"
    date: 2010-05-28
    body: "But KDE PIM still IS a part of KDE SC, isn't it? How will these alpha/beta/RC releases reach interested end-users?"
    author: "atrox"
  - subject: "A little review."
    date: 2010-05-29
    body: "KDE is, like always, doing things so right, that it almost fails because of it. That's because we had this release days after a Fedora 13 with KDE 4.4, and I have to notify that KDE 4.5 Beta, save the brand new features, is, like always since KDE 4.0, more stable and faster than the past release.\r\n\r\nThis is going to be my own review, with a lot of little things that I care about, but that are so little that they pass unnoticed by most. Forget about the new blur plugin. It's cool, but I disabled it, because once you get used to a solid 60 FPS framerate with a poor IGP that suffered with Aero, you can't leave it. 4.5 is faster and fixes a lot of KDE 4.4 annoyances.\r\n\r\n1. The annoyance number one for everyone using Plasma: plasmoids don't remember you, don't remember where they are and, if you remove them, you have to reconfigure them. Same thing for activities. Enabling more than one activity with KDE 4.4 led to leftover plasmoids, a corrupt desktop, and a Plasma Desktop disoriented. This ENDS HERE. Plasma under KDE 4.5 is SOLID AS NEVER BEFORE. Plasmoids remember you. And if you disable the \"Dashboard-like\" behaviour and later enable it, all of your plasmoids are there, waiting for you. The pain with ZUI ENDS HERE. The painting issues with Plasma END HERE. This alone makes an upgrade from 4.4 \"stable\" a neccessity, and, once more, makes one question if 4.4 even deserves the label of stable. Every KDE beta release has made this with the stable release before it. And this is no exception.\r\n\r\n2. Strigi/Nepomuk. It isn't really faster. This time the recipe is: index less, index better, and hammer less the system. And it worked. Before, we all wasted our processors with continous reindexings. Reindexings are OVER, guys. Before, Strigi hammered our GPUs and Plasma because it had the bad idea of update its status at 120 fps. That's OVER, and Strigi eats a LOT less CPU than before. This is the second reason why an advanced user SHOULD UPGRADE NOW.\r\n\r\n3. Kudos for the guy that redid the Device Manager icon. The old one was annonying and weird. This one is beautiful and clearly means USB. And the monochrome icons are beautiful, with a pen effect that amazed me. The Strigi icon impressed me the most. It's the Nepomuk logo, stylized, I suppose. But when I see it I now see a script S for Strigi, indexing. I can associate it to something. That's beautiful. It's sad to see third party apps, and apps like KGet or Amarok, with colourful icons, now. They deserve their monochrome icons too!\r\n\r\nWhat's missing?\r\n\r\n1. A full review of the keyboard behaviour. The keyboard navigation is riddled with bugs, everywhere. It seems that nobody tested that. I can test. \r\n\r\n2. A delay. We need more time to kill more bugs. We need more betas, more testing and more time, because we are releasing in the worst time of the distro cycle. Fedora has released Goddard days before this beta. Ubuntu is shipping with 4.4.2. Mandriva won't ship 4.5 in 2010.1. OpenSUSE will be released with 4.4. And I can keep going. If we must delay KDE-PIM, why don't just delay the whole thing, put the SC in a bug-murdering-mode, release a Beta 3 and a Beta 4 if neccessary, and release a KDE SC 4.5 as stable as KDE 3.5.10?\r\n\r\n3. The ability to configure KDM. Please, use KAuth!"
    author: "Alejandro Nova"
  - subject: "Kubuntu .debs"
    date: 2010-05-29
    body: "In the past, KDE SC Beta/RC/Final Preview versions were released within a special PPA some days after the official release. So you only had to add the special repository and to update via package manager. I hope, that will be done in future, too. So watch the news area on kubuntu.org - QT4.7beta is already announced and I think KDE SC 4.5 will follow the next days. All steps to install are shown there - it's very easy."
    author: "patrick2901"
  - subject: "3 is implemented"
    date: 2010-05-29
    body: "I think 3. snuck in after the tagging. I remember seeing the commit."
    author: "einar"
  - subject: "Aha.. packaging magic :)\nFor"
    date: 2010-05-29
    body: "Aha.. packaging magic :)\r\n\r\nFor OpenSuse: \"KDEPIM  packages within the UNSTABLE repo where renamed to kdepim4-44 since KDE 4.5.0 SC will ship the KDE SC 4.4 version of KDEPIM. Current snapshots of the KDE SC 4.5 KDEPIM are placed in a sub-repo KDE:KDE4:UNSTABLE:Desktop:kdepim45 and updated daily by tittiatcoke\""
    author: "atrox"
  - subject: "nice review!"
    date: 2010-05-30
    body: "Nice review, Alejandro. I haven't tested plasma in 4.5 yet but thanks to your review I look forward to the stable release even more ;-)\r\n\r\nI'm not really in for a longer testing period, as the x.y.Z releases coming each month can fix whatever is wrong and in the mean time I can enjoy 4.5 thanks to the rolling release schedule in arch :D"
    author: "jospoortvliet"
  - subject: "Issues "
    date: 2010-05-31
    body: "Xine backend is broken.\r\nRight click on some tray icons doesnt work for e.g. kalarm, kopete.\r\nKDM greeter doesn't show list of desktop environments.\r\nThe add new widget strip is misplaced with compiz, {20% of it goes below bottom edge of screen, if panel is on auto hide}\r\n\r\nThis is on kubuntu packages."
    author: "vikrant82"
  - subject: "I had same idea of list"
    date: 2010-06-01
    body: "I had same idea of list (icons on left, text on right) to be used in systray when not even Vista was out. Would you say that I would be only one outside of Microsoft who did not think such? I bet there are thousands of peoples who have had same idea long before Microsoft got such or until such was implented as well to  Plasma Desktop. \r\n\r\nAnd the Windows 7 style is a pop-up, but it is bad one. It only show icons and it set icons in a same line. Like they would be a second systray top of the existing one. And when you drag something from it, it gets hidden.\r\n\r\nOnly same idea in both of these is that other widgets in the panel do not get resized everytime when user opens hidden icons from systray. Otherwise, KDE did better job implenting such idea (what I remember being heard as well from KDE few years ago...).\r\n\r\n"
    author: "Fri13"
  - subject: "From Windows 7? Even Mac OS X"
    date: 2010-06-01
    body: "From Windows 7? Even Mac OS X has have that kind many years now!\r\nCanonical made own style to the GNOME with same idea. Many has used same kind themes for KDE3 in last 7 years!\r\n\r\nSeems you have bitter taste in your mouth because MS did not invent nothing what you say."
    author: "Fri13"
  - subject: "Every step, a step forward."
    date: 2010-06-03
    body: "Congratulations. \r\n\r\nI'm using kde 4.5 beta1 from ppa:kubuntu-ppa/experimental and I have some questions:\r\n- where I can choose to switch Konqueror to use WebKit as rendering engine for web sites?\r\n- about blur: for example main panel/krunner have the blur effect, folderview/transparent konsole doesn't have blur effect, what is the difference between trasparence?\r\n\r\nRegards"
    author: "joethefox"
  - subject: "Same"
    date: 2010-06-04
    body: "Noticed the same issues on the system tray.  I looked at the bug database, but don't see anything relevant to the problem.\r\n\r\nI wish I knew if non-Kubuntu users were seeing the issue too."
    author: "3vi1"
  - subject: "folder view \u2018look into me\u2019 icon"
    date: 2010-06-04
    body: "And Folder view no longer pops up randomly, but has that nice \u2018look into me\u2019 upwards arrow, which is just great! \r\n\r\n(just wanted to add it here :) )\r\n\r\nAnd when we\u2019 at schedules: Couldn\u2019t the 4.x.4 release be timed with the stable distros und the 4.x.0 release with testing distros or such? (I use Gentoo, though, so I don\u2019t really care about release cycles. For myself I rather want projects to have their own schedules, so I don\u2019t have to recompile everything at the same time). "
    author: "ArneBab"
  - subject: "blur"
    date: 2010-06-04
    body: "one small comment: while blur is cool to make transparent windows better readable (effectwise), please always keep the options to turn it off. I regularly use transparency for painting fun, making the MyPaint window 80% transparent and putting it on top of a picture, so I can retrace parts of it very conveniently. "
    author: "ArneBab"
  - subject: "I dunno if there is some"
    date: 2010-06-05
    body: "I dunno if there is some configuration option right now, but if there isn't, you can change this in the file associations (I used that to test it in Konqi 4.4).\r\n\r\nStart konqi and go to settings -> file management -> file associations (or look in systemsettings under file associations). There you can search for html and set WebKit KPart under 'Embedding' as default (on top)."
    author: "jospoortvliet"
  - subject: "no blur ;)"
    date: 2010-06-06
    body: "Right click on a titlebar, select \"Configure window behaviour...\". You'll get a dialog with the first option, \"Desktop Effects\", selected. Select the second tab, \"All Effects\". In the first group, \"Appearance\", you'll find the Blur effect. Disable it :)"
    author: "Alejandro Nova"
---
KDE has released a first test version of the <a href="http://kde.org/announcements/announce-4.5-beta1.php">released</a> that will be out this summer, in August. KDE SC 4.5.0 is targeted at testers and those that would like to have an early look at what's coming to their desktops and netbooks this summer. KDE is now firmly in beta mode, meaning that the primary focus is on fixing bugs and preparing the stable release of the software compilation this summer.
<!--break-->
</p>
<p>
KDE SC 4.5 sports many improvements, among which are:
</p>
<p>
<ul>
    <li>
    A <strong>reworked notification</strong> area. Thanks to the new, D-Bus-based protocol that replaces the old "system tray", a <em>uniform look and consistent interaction</em> scheme can now be guaranteed across applications and toolkits.
    </li>
    <li>
    <strong>KWin-Tiling</strong> makes it possible to automatically place windows next to each other, employing the <em>window management paradigm</em> also found in window managers such as Ion. Advanced graphical effects, such as blurring the background of translucent windows make for a more pleasurable and usable experience.
    </li>
    <li>
    Users that prefer <strong>WebKit</strong> above the KHTML rendering engine currently used in Konqueror, KDE's web browser now can install the WebKit component and switch Konqueror to use WebKit as <em>rendering engine for web sites</em>.  The WebKit component for Konqueror is available from KDE's Extragear repository, is based on the popular KPart component technology and fully integrates with password storage, content-blocking and other features users already know and love in Konqueror.
    </li>
    <li>
    A special focus of this release cycle is the <strong>stability</strong> of the software delivered with KDE SC 4.5. While there are many exciting new features, developers have spent considerable amounts of time finishing off features and polishing those that haven't come to full bloom yet.
    </li>
</ul>
</p>
<p>
Initially planned for KDE SC 4.5.0, the KDE PIM team have decided to delay the released of the Akonadi-based KMail for one month. The new version of KMail will be delivered as part of one of KDE's monthly bugfix updates. In the meantime, the stable version of KMail from KDE SC 4.4 will be maintained. Akonadi will centralize syncing and caching of PIM data, deliver wider support for groupware servers and makes handling PIM data, such as contacts, calendaring and email more efficient by sharing it across applications.
</p>
<p>
Please test this release, and inform us of issues with the software through <a href="http://bugs.kde.org">bugs.kde.org</a>.
</p>