---
title: "Akademy 2010: \"Our World, Clearly\" - Call for Papers"
date:    2010-02-01
authors:
  - "nightrose"
slug:    akademy-2010-our-world-clearly-call-papers
comments:
  - subject: "So excited for Akademy this"
    date: 2010-02-02
    body: "So excited for Akademy this year, as it'll likely be the first time in a while that I return to it. Hopefully this time I don't get attacked by terrorists on my way :P"
    author: "troy"
  - subject: "for Italian readers"
    date: 2010-02-02
    body: "Here a translation for Italian readers: http://kde-it.org/2010/02/02/akademy-2010-call-for-papers/\r\n\r\n@troy: do you mean in Scottland? I was there and I remember the scenes on the tv screens and when you told me you were blocked for hours at the airport :("
    author: "giovanni"
---
<img src="http://static.kdenews.org/jr/akademy-logo.png" align="right" border="10" /><a href="http://akademy.kde.org/">Akademy</a> is the annual conference of the KDE community and open to all who share an interest in the KDE community and its goals. This conference brings together artists, designers, programmers, translators, users, writers and other contributors to celebrate the achievements of the past year and helps define the vision for the next year. In its 7th year, we invite all contributors and users to participate in Akademy in Tampere, Finland from July 3 to 10 2010.
<!--break-->
Recent developments in the KDE technologies make it possible for users to connect with their data and with other users in new ways. In order to build upon this work, the main topics for this year's Akademy will be:

<p style="padding-left:15px;"><b>Expanding Our World: KDE Beyond The Linux Desktop</b><br>KDE technologies have become increasingly available on mobile devices such as netbooks and mobile phones. This brings new challenges to the development of the KDE applications as well as new oportunities to greatly increase the visibility of the KDE community.<br>For this topic we encourage talks addressing the challenges of porting the KDE platform and applications to different operating systems and mobile devices, such as mobile phones and netbooks.</p>

<p style="padding-left:15px;"><b>Connecting Our World: Social Desktop</b><br>Social desktop technology is allowing users of KDE applications to connect with each other and to have stronger links with the developer community. This technology brings a new and unique element to the KDE user experience.<br>For this topic we are requesting talks on the integration of KDE applications with social networks such as Facebook, identi.ca, Twitter or ownCloud. Talks regarding underlying services such as Akonadi and Telepathy are also encouraged.</p>

We also welcome submissions on any other KDE-related topic. This year we are requesting fewer, longer presentations in order to ensure the highest quality of content. Submissions can be made for either a 30 minute presentation or, following last year's success, a 45 minute presentation with an associated technical paper to be published during the conference. Papers are to be submitted in the <a href="http://www.acm.org/sigs/publications/proceedings-templates">ACM format</a> and should be between 4 and 6 pages long. Abstracts should be included in the initial submission.

We are also accepting proposals for BoF and Workshop sessions that can last from one hour to all day. Topics can include anything relevant or of interest to the KDE community.

Submissions may be made by email to akademy-talks@kde.org. They should include your name, small photo, talk/session title and an abstract of no more than 400 words. It should also indicate whether a 30 minute talk or a 45 minutes talk with a paper is being submitted.

The deadline for *all* submissions is Friday, 23 April 2010.

The program committee for Akademy 2010 is:
<ul>
<li>Anne Wilson</li>
<li>Bertjan Broeksema</li>
<li>Celeste Lyn Paul</li>
<li>Lydia Pintscher</li>
<li>Marijn Kruisselbrink</li>
<li>Paul Adams</li>
</ul>

We are looking forward to your submissions.