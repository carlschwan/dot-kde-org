---
title: "KDE Telepathy Sprint"
date:    2010-10-17
authors:
  - "wstephenson"
slug:    kde-telepathy-sprint
comments:
  - subject: "MS OCS/LCS Support"
    date: 2010-10-18
    body: "Does telepathy have support for MS Communicator (sipe) support? If yes, if and when can we see it implemented in KTelepathy?"
    author: "kanwar.plaha"
  - subject: "Not a simple"
    date: 2010-10-18
    body: "Not a simple answer:\r\n\r\nTelepathy does support SIPE via libpurple. However at present a brief search of the internet seems to imply that only basic text IM is currently supported.\r\n\r\nIf telepathy supports it, then KTelepathy will support everything. The only thing we have to do on a per-protocol basis is to create a configuration dialog for configuring the account - though currently no-one is tasked with this.\r\n"
    author: "David_Edmundson"
  - subject: "Design?"
    date: 2010-10-23
    body: "How is this going to be designed, considering Nepomuk/Akonadi? Would it be possible to create a Telepathy Contact Akonadi plug-in, then simply build the application on top of Akonadi, or is it planned that Telepathy be implemented independantly?"
    author: "madman"
---
On a sunny September weekend in Cambridge, England, ten KDE and Telepathy developers met in the Collabora office to plan the future of Instant Messaging in KDE software. Once everyone had arrived, our host George Goldberg gave us an overview of the current state of the codebase, which parts are usable, which parts still need writing, and which parts were written years ago and need revision. This turned into a project management session to determine the order for getting things done, and a discussion about a release schedule that will make the project visible without tying it prematurely into compatibility guarantees that slow down development.
<!--break-->
<h2>The Team</h2>

The sprint attendees are shown below. Many have also written personal blog entries about the sprint - click on their names to read those.

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -231px; width: 464px; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDETelepathy.jpg" /> </a></div>

<p style="font-style:italic; text-align: center;"><b>Back Row:</b>. Olli Salli (oggis), Dominik Schmidt (domme), David Edmundson (david_edmundson), <a href="http://trueg.wordpress.com/2010/09/20/what-happens-if-mr-nepomuk-meets-a-bunch-of-telepathyans/">Sebastian Trueg (trueg)</a>, Will Stephenson (wstephenson)
<b>Front Row:</b> <a href="http://grundleborg.wordpress.com/2010/09/16/telepathy-kde-sprint-day-1/">George Goldberg (grundleborg)</a>, <a href="http://gkiagia.wordpress.com/2010/09/20/telepathy-kde-sprint/">George Kiagiadakis (gkiagia)</a>, <a href="http://drfav.wordpress.com/2010/09/23/kde-telepathy-sprint-its-a-wrap-up/">Dario Freddi (drf)</a>, <a href="http://blogs.fsfe.org/drdanz/?p=311">Daniele Domenichelli (drdanz)</a>,  <a href="http://andrunko.blogspot.com/">Andre Magalhes (andrunko)</a></p>

<h2>Creativity</h2>

<div style="float: right; border: 1px solid grey; padding: 1ex; width: 249px; margin: 1ex">See a preview of some of the results from the sprint in the screenshots below<br /><a href="http://dot.kde.org/sites/dot.kde.org/files/1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/small1b.jpg" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/2.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/small2b.jpg" /></a>
<a href="http://dot.kde.org/sites/dot.kde.org/files/3.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/small3b.jpg" /></a>
<a href="http://dot.kde.org/sites/dot.kde.org/files/4.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/small4b.jpg" /></a>
</div>
We paid a visit to a local hostelry to get the creative juices flowing with some real ale and typical English pub food like bubble & squeak, faggots in gravy and sausages & mash.  One of our contributors, who will remain nameless, protested initially that he did not really like beer, but by the end of the evening was returning frequently to the bar for more flat, warm, malted barley juice.

Back in the office, the Telepathy-Qt4 authors Olli Salli and Andre Magalhaes were there to advise.  Olli gave a detailed talk on the concepts of the Telepathy-Qt4 library, new features that make it easier to code with, and the basic Telepathy components that are most important to client authors.  They then sat with the KDE UI hackers to identify useful features in their code that can be be pushed down into the library.  

<h2>Hard Work</h2>

Instant Messaging UI designs are like opinions - everybody has one.  The modular nature of KDE Telepathy will allow any component to be replaced, for example with wildly skinnable contact lists or infinitely configurable chat windows, yet everyone agreed that it is important to have a functional yet elegant default user interface.  Andre, Will Stephenson, and Daniele Domenchielli formed a UI Love breakout group and commandeered the whiteboard in Collabora's kitchen.  They set about sketching some concepts for a contact list, chat window, voice/video call widget and configuration UI that combine the best of current technology with the experience of Kopete and other clients.  These are being mocked up using Qt Quick.

<h2>Library Hacking</h2>

Meanwhile, our host George Goldberg worked on the KTelepathy library that is the immediate foundation for all the other components.  This is responsible for uniting the individual contacts that Telepathy deals with into Persons, equivalent to Kopete's metacontacts.  It makes heavy use of Nepomuk, KDE's central store of metadata and interrelationships between objects.  Master of Semantics, Sebastian Trueg attended the meeting to provide on-the-spot advice in writing optimal Nepomuk queries, updates to the semantic ontologies that describe how Instant Messaging data fits together. And he provided instant fixes when we ran into problems.

George Kiagiadakis, coming to KDE Telepathy from KCall, worked on the call UI while David Edmundson took on the chat window, assisted by Dominik Schmidt in the Webkit-based implementation of Adium theme support.  Dario Freddi, meanwhile, hacked on adapting the contact list to the current Telepathy APIs, while Daniele Domenchielli planned API designs and helped with the UIs.

<h2>Future Plans</h2>

The project team plans to make an initial release outside the KDE release cycle in January and to explore including Telepathy in KDE Platform 4.7 in July 2011.  Additional contributors are welcome in #kde-telepathy on Freenode or on the kde-telepathy @ kde.org mailing list. There are plenty interesting tasks left to be picked up!

This sprint was sponsored by <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.collabora.co.uk/">Collabora</a>.