---
title: "Kolab Systems Aims to Bring Kolab Groupware to New Level"
date:    2010-03-01
authors:
  - "jospoortvliet"
slug:    kolab-systems-aims-bring-kolab-groupware-new-level
comments:
  - subject: "Bingo?"
    date: 2010-03-01
    body: "Anyone has Bingo yet?"
    author: "jospoortvliet"
  - subject: "Usability work"
    date: 2010-03-01
    body: "Do any of those buzzwords mean that there will finally be some usability work going into KMail?"
    author: "KAMiKAZOW"
  - subject: "Why?"
    date: 2010-03-02
    body: "Sorry, but I don't really get the why.\r\n\r\nGuessing [1] from the \"AG\" you created a JSC, how that helps you fullfill your mission more than being a LLC e.g. is beyond my understanding unless you plan to do an IPO.\r\n\r\nI wish you good luck, but I think that the why mentioned here is a little shallow.\r\n\r\n[1] Did not find an investors relations link or something like that"
    author: "mat69"
  - subject: "\"Why\" explained"
    date: 2010-03-02
    body: "If you build on Kolab, what you probably want is an initial migration, possibly custom development. Those can be done as a project (i.e. limited time and scope), and that is what the companies in the Kolab consortium have been providing until now. (The article says that the work was mostly project-based).\r\n\r\nNow take packaging (or support in general) for example. For this, you don't want a project-based approach, but services, and probably a service level agreement (SLA) in order to be sure to get continuous support, for example in the form of updated packages when a security problem arises. Kolab Systems provides this and is built around the typical needs of such customers.\r\n\r\nI hope this makes the \"Why?\" a little clearer."
    author: "sebas"
  - subject: "Icon survey?"
    date: 2010-03-02
    body: "You seem to be suggesting that there hasn't gone any usability work into KMail, which is plain wrong. Take the icon survey that has been done lately for example. The results of that have already started hitting KMail in 4.4, as there are many new icons, and those are already being perceived as improvements.\r\nNote that I, too, think that usability work on KMail wouldn't hurt, but putting it as \"nothing's being done\" is simply not true."
    author: "sebas"
  - subject: "This explains partially [1]"
    date: 2010-03-02
    body: "This explains partially [1] why to create a new company in the first place, but not its legal form.\r\n\r\n[1] Nothing is forcing the old companies not to offer SLAs. But I understand that in this case it might be better to have a new one."
    author: "mat69"
  - subject: "details, details, ... ;)"
    date: 2010-03-02
    body: "The Swiss AG is actually equivalent to a \"Limited\". There are differences to a German AG, if that's what you are referencing in your initial comment. Feel free to contact us at contact@kolabsys.com to discuss this in more detail, if you wish. \r\n\r\nTill (KDAB's representative on the board of Kolab Systems and long time Kolab/KDEPIM person)"
    author: "till"
---
Today a new Free Software business was launched with the goal of strengthening the Kolab groupware solution ecosystem. The former <a href="http://www.kolab-konsortium.com/">Kolab Konsortium</a> now has a new charter and thus a new name: <a href="http://kolabsys.com/">Kolab Systems AG</a>. Offering services, packaging and quality assurance based on a partnership model, Kolab Systems builds its business on the proven Kolab groupware solution. This leads to a continued focus on supporting and deploying KDE personal information management (PIM) applications and server infrastructure on a variety of platforms.
<!--break-->
<b>The Why</b>

Kolab has long been supported by the Kolab Konsortium but the cooperating companies <a href="http://kdab.com">KDAB</a> and <a href="http://intevation.de/index.en.html">Intevation</a> were strongly project-focused. This development focus did not fully provide the service model many of the companies in the Kolab ecosystem require. In the ten years the Kolab groupware software has existed, there have been many large and small deployments, but the community believed the full potential of Kolab has not yet been reached.

<b>The how</b>

After discussions within and beyond the community of companies, users and developers, KDAB and Intevation decided it was time for a new model and thus created Kolab Systems AG. At FOSDEM the initiative was already partly introduced, and the KDE PIM developers were briefed at a recent meeting. After talking to many other involved parties most of the ecosystem is now aware of this new player. The goal of Kolab Systems is decidedly not to compete with the existing companies providing Kolab support, doing deployments or offering consultancy and development services, but to complement them. Kolab Systems works with partners, providing first, second or third level support where needed, bringing companies in contact with each other and helping them service the needs of the customer.

Kolab Systems will also serve as a central point of contact for end users or companies interested in providing services around Kolab. For the KDE community, the appearance of Kolab Systems means a renewed focus on KDE PIM as a product. The public perception around Kolab is often that it is mainly server-oriented, but in reality most of the magic is in the client. And that client is Kontact, making it a central component of the Kolab offering.

<b>The future</b>

Recent projects like the work going on to bring KDE PIM applications to mobile platforms, Windows and Mac create a huge opportunity. Syncing problems between different devices and platforms like home and work desktops and mobile phones can be alleviated if the underlying architecture makes use of a full-featured platform like Kolab instead of having to rely on mixing and matching data from different providers like GMail and Outlook. Kolab Systems is in a better position to seize these opportunities than the previous less formal collaborations between KDAB, Intevation and their partners. This is because Kolab Systems, as a business, has a focused interest in the product Kolab itself, its sustained development and health, as well as its economic long term viability.

So, this new company is not just another service company starting up around the KDE community, but an effort to bring an already strong ecosystem to a new level.

<b>The quotes</b>

To quote Georg Greve, CEO, from the announcement: <i>"We feel that nurturing the surrounding business ecosystem is essential if Kolab is to fully realize its technical potential for all its users. Kolab Systems AG is the consequence of this and the answer to feedback that we received from partners, customers and the community alike." </i>

And Dr. Paul Adams, COO: <i>"The new business will be focused on a strong partnership model to provide users with a service chain that combines fast 1st level support out of a single hand with Kolab Systems AG as a strong 2nd and 3rd level partner that focuses on testing, QA, packaging and continuous development and maintenance of the technical basis."</i>

<i>Managed by the founding president of FSFE and a senior Free Software expert and researcher with strong ties to the KDE community, the new business will continue to work closely with the Free Software community and ensure the future of the Kolab Groupware Solution as an independent and secure choice for all users.</i>

Read more, including contact information, in the official announcement on the new <a href="http://kolabsys.com">Kolab Systems</a> website and check the <a href="http://kolab.org">kolab community site</a>.