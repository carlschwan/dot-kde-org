---
title: "Qt's Knut Yrvin in Norway's Got Talent Final"
date:    2010-05-21
authors:
  - "jriddell"
slug:    qts-knut-yrvin-norways-got-talent-final
comments:
  - subject: "support him!"
    date: 2010-05-21
    body: "FYI there is a facebook group. Go ahead and join, ignore the Norwegians talking their own silly language there and be part of Knut's quickly growing fanbase ;-)\r\n\r\nOh, and next time you meet Knut, see if you can make him do his thing. Secretly film it and send it to me, I'll put it on Youtube as KDE promo video ;-)"
    author: "jospoortvliet"
  - subject: "Rock on, Knut!"
    date: 2010-05-21
    body: "Fingers crossed!"
    author: "msoos"
  - subject: "I liked Knut's perfomance."
    date: 2010-06-04
    body: "I liked Knut's perfomance. Saddly he wasn't first.\r\n----------\r\n<a href=\"http://www.mec.com.ua/services/contour/\">\u0434\u0438\u0441\u043f\u043e\u0440\u0442</a>"
    author: "huhonasu"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org//sites/dot.kde.org/files/knut.jpg" width="300" height="225"/><br />Knut's got talent</div>Over the last few weeks <a href="http://blog.qt.nokia.com/2010/03/08/qt-knuts-got-talent/">a phenomenon</a> has been sweeping the cultural headlines of Norway. Qt Community Manager and friend of KDE Knut Yrvin has been amazing the judges of Norske Talenter (<a href="http://en.wikipedia.org/wiki/Got_Talent_series#Norway">Norway's Got Talent</a>) with his robotic moves (<a href="http://www.youtube.com/watch?v=R8fNfv-K55A">YouTube video</a>).

Tonight is the final and Knut has made it to the last few contestants. The streets of Oslo will be silent as everyone will be eagerly watching the Norwegian <a href="http://en.wikipedia.org/wiki/Ant_and_dec">Ant and Dec</a> introduce the acts. Will Knut triumph or will the judges buzz him out? KDE will be backing him all the way.

<strong>Update:</strong> unfortunately he failed to make first place, the Norwegian Simon Cowell must have just as poor judgement as the original.  Knut will always be a winner with KDE and we look forward to a repeat performance at Akademy.  Coverage on <a href="http://www.youtube.com/watch?v=d6z8kI021ks">YouTube</a>.
<!--break-->
