---
title: "Solid Sprint Enhances Key KDE Platform Technologies"
date:    2010-10-13
authors:
  - "a(toscalix)"
slug:    solid-sprint-enhances-key-kde-platform-technologies
comments:
  - subject: "Nice job! alot of usefull"
    date: 2010-10-14
    body: "Nice job! alot of usefull information, thank a lot! "
    author: "Huan"
---

<a href="http://solid.kde.org/">Solid</a> is the part of the KDE Platform that handles interaction with hardware, making it easy for application developers to deal with things like network availability by abstracting underlying libraries within a familiar KDE-style API. As such it is an essential component across all kinds of KDE software. It is getting clear that Solid is becoming a well defined team within KDE and everybody is exited about the idea of attracting more developers interested in hardware support to the desktop, mobile devices, netbooks, media center and beyond. There are now quite a few developers working on Solid so it was a good time to get them all together for a sprint in <a href="http://en.wikipedia.org/wiki/Madrid">Madrid</a>, Spain.

<h2>Combining Events</h2>

It has been a busy few weeks for KDE events in Spain. Sebastian Kügler, Release Manager and KDE e.V. Board Member, combined his visit to the Solid Sprint with meetings with local representatives from Madrid City Council and the central government IT Department. Sebas, Rafael Fernandez (KDE Spain Vice President) and members of <a href="http://www.asolif.es/?page=asolif_english">ASOLIF</a> (the federation of free software small and medium enterprise associations), introduced KDE and KDE Spain, discussing legal, economic and management aspects of free software communities. As a result of these conversations, KDE Spain attended <a href="http://www.ifema.es/web/ferias/simonetwork/default.html">SIMO Networks</a>, the largest IT event in Madrid.

<h2>Solid Sprint</h2>

After these meetings, the KDE developers involved in the Solid Team had lunch together before the start of the Solid Sprint and found some time for a group photo:

<div style="width:100%;"><img src="http://dot.kde.org/sites/dot.kde.org/files/IMG_4156.jpg" style="left: 50%; position: relative; margin-left: -300px; border: grey 1px solid; padding: 2px;" /></div>

<p style="text-align:center; font-style: italic;"><b>Top row, left to right:</b> Dario Freddi (PowerDevil), Will Stephenson (Network Management, openSUSE), Kevin Ottens (Solid Maintainer, KDE Mobile Platform dude), Lamarque Souza (network management mobile broadband), Javier Llorente (openSUSE dude)<br /><b>Bottom row, left to right:</b> Alex Fiestas (Bluedevil, upower backend), Rafael "ereslibre" Fernando Lopez (KCategorizedItemView, BlueDevil), Albert ‘tsdgeos’ Astals Cid (KDE-ES president, localization expert), Sebastian Kügler (Power & Network Management, KDE e.V.)</p>

<h3>Day 1</h3>

The first day of the sprint was divided in two different parts. During the first part, some discussions took place in order to coordinate efforts and discuss the state of the art of the different Solid components, going through the major issues that would be accomplished in the following months. During the second part of the session, a lot of hacking took place on the major tasks decided in previous discussions. As usually happens in KDE sprints, the hacking session finished late at night.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/IMG_4159.jpg" /><br />Hackers hacking</div>While the hacking was going on at full pace, Agustín Benito Bethencourt took care of communication tasks, freeing up time for other developers to fully concentrate on code, architecture and design.

<h3>Day 2</h3>

'Day two' of the sprint began early when the Solid hackers returned from dinner in downtown Madrid. Even late at night, almost everybody was still active until the early morning hours. Great things were achieved. Dario Freddi and Sebastian Kügler got large parts of the renewed PowerDevil core and configuration working, while Lamarque Souza made strides towards support for setting up mobile broadband connections using the network management framework in Solid. Kevin "ervin" Ottens pushed the new backend for storage devices based on udisks towards completion, while Alex Fiestas restructured bug reports for the new BlueDevil framework to make user support easier. Rafael"ereslibre" Fernández López took care of the new Udev backend, a provider of information for generic types of devices. Will Stephenson put his muscle into researching how Bluetooth and networking interact. This led to invaluable information for implementing tethering (connecting your laptop through your phone to the Internet) in KDE's Plasma components, using NetworkManager and BlueDevil.

Most people took a couple of hours off in the early morning to catch some badly needed sleep, though apparently not everybody decided that this would be time well spent.

<h3>Wrapping Up</h3>

The KDE Solid team can look back at a huge pile of work done. Some Solid components have moved to a more flexible, scalable, usable and energy-friendly architecture, while others have received love in usability, stability and feature-completeness. Janitor-like tasks like deprecating HAL in KDE software have been moved forward, and are expected to be completed with the next feature release of the KDE Platform in January.

The sprint was a great success. The Solid team would like to thank the sponsors who made it all possible: <a href="http://www.ufocoders.com">UFOCoders</a>, <a href="http://www.interdominios.com">Interdominios</a> (who hosted the event) and, as always, <a href="http://ev.kde.org">KDE e.V.</a> Of course, KDE e.V.'s support for such events depends in turn on donations from companies and individual supporters like you. So if you can, <a href="http://jointhegame.kde.org/">Join the Game</a> and help to make events like this possible.