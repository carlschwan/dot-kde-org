---
title: "Camp KDE 2010 Continues with More Talks"
date:    2010-01-21
authors:
  - "jospoortvliet"
slug:    camp-kde-2010-continues-more-talks
comments:
  - subject: "We cannot and will not (...) starting a political movement"
    date: 2010-01-21
    body: "\"We cannot and will not (...) starting a political movement\"\r\n\r\nWhy not? The Free Software Movement is a political one. Why shouldn't KDE promote AGPLed server technologies?"
    author: "KAMiKAZOW"
  - subject: "sure"
    date: 2010-01-22
    body: "Sure, the FOSS movement at large is certainly a political one, and we are part of that. However, the KDE community's role in this is a largely technical one, and while our community members by and large support the FOSS philosophy, not all of us are equally happy with what the FSF does, for example. It is not what binds us together, it is not what we primarily do - we write code. As you can see from the ownCloud, social desktop and other initiatives, our preference for C++ and dislike for mono and from many other things - we in general support FOSS heavily. But we won't step out of our technology boundaries. Other organisations are far better at that."
    author: "jospoortvliet"
---
The second day of Camp KDE was filled with many more interesting talks. This day's talks were of the more technical nature versus the first day, and the KDE team took notes. As usual, the talks were recorded and videos will be available soon. A detailed rundown of the second day of talks are behind the link.
<!--break-->
The second day started with a keynote by Frank Karlitschek about KDE vs. The Cloud(TM), looking ahead a couple of years into the future. Frank pointed to the trend to use cloud based applications instead of desktop applications. The question this raised was: can KDE could provide a superior experience if we properly combine the advantages of cloud based technology with the freedom of open source and the power of desktop applications?

He began by defining "The Cloud". Chrome OS and Google's huge variety of cloud based services in general are the most pervasive examples, though it has also become more common in multimedia thanks to services like Flickr, Pandora or Last.fm. Frank went on to present the results of a survey he did on the advantages of the cloud. The most important advantages of cloud apps are the social features, the easy access to apps and the ability to access your data from everywhere and share stuff with others.

Is the growing reliance on web applications and online services a good trend from the FOSS perspective? And where will KDE software be in 10 years if it continues like this?

Frank points to the things Google has been doing with and on the web, and the dangers such a successful monoculture presents. Google is definitely behaving itself right now, but things might be very different in 5 or 10 years.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/two_dragons.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/two_dragons_small.jpg" width="300" height="225"/></a><br />And we had a visit from another dragon!</div>

Frank then presented the results of a survey on disadvantages on the cloud. The biggest problems for users are possible privacy and data protection problems and the dependency on third party infrastructure.

The big question of course is: What can be done about it? How can we take back the web? As the KDE community is a technical bunch, we are naturally focused on technology. We cannot and will not fight Google by sending lawyers at them or starting a political movement - we must create something cooler and better. So how can we get the best of both worlds and create a compelling alternative? He outlined the following requirements to compete:

<ul>
<li>provide a rich desktop with rich applications instead of web apps</li>
<li>be social</li>
<li>ease deployments</li>
<li>ensure data you own accessible everywhere</li>
<li>facilitate mashups</li>
</ul>

So for this we need 3 building block:
<ul>
<li>social services</li>
<li>online data sharing</li>
<li>easy deployment of applications.</li>
</ul>
<iframe width="640" height="360" src="//www.youtube.com/embed/5IdMWxtMMB8?rel=0" frameborder="0" allowfullscreen></iframe>
Frank now introduces the first part of a solution, the Social Desktop and libattica. Libattica is an easy to use Qt-only library which provides social and transparent data access for any application with build-in authentication, error handling and support for different data providers. Libattica is currently used in the KDE SC 4.4 desktop by a variety of applications like the social desktop widgets. This makes adding of social features to apps really easy. 

Frank also released a reference server implementation so that other online services can easily integrate with the KDE Social Desktop. The goal for this is to not only support the OpenDesktop provider as social network but also other unrelated networks like Facebook. The social desktop can then combine all the networks in a single experience, and in time even help in getting our data back.

A second technology is GetHotNewStuff which has recently been heavily reworked and updated. In time, the GHNS framework can also help in the deployment - both for data but, with the integration with the openSUSE Build Service which is in the works, also for applications. The future even holds integration with Qt Creator, where a developer writes an application, does a few mouse clicks to upload it with GHNS where the openSUSE BuildService (or some local install of the Build Service software) takes over and creates packages which then anyone can easily install.

The third needed building block is a technology to make storage, access, revision control and sharing of documents really easy. Frank announced the ownCloud project which is a personal cloud storage solution to manage all you personal data. ownCloud will use the AGPL license so everybody can install his/her own cloud storage on a server, desktop and anywhere else. Everybody has control over his/her own data but is still able to access it from all devices, have revision control, automated backups, sharing with others and data encryption. 

So let's make this happen - there is a <a href="http://owncloud.org">wiki page</a> set up and a <a href="http://gitorious.org.owncloud">gitorious repository</a> if you want to help. They are still in an embryonic stage but the future of the social desktop integration into the KDE projects is bright.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/Ian_on_Telepathy.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Ian_on_Telepathy_small.jpg" width="300" height="225"/></a><br />Ian talking about Telepathy</div>

Once Frank was done, Kris Moore took over to talk about KDE on PC-BSD, a desktop distribution of FreeBSD. Kris goes through some recent developments in PC-BSD, including the development of a software management tool and a variety of small tools building upon KDE technology. He explains the PBI package management, which works based on self-contained binary installation packages which still integrate with KDE.

He explains why PC-BSD makes use of KDE applications and technology. The short answer is they love it - the long answer would involve the professional look and feel, the great technology and the continuous innovation coming from the KDE community. Of course there are issues the developers bump into - something Kris does not skip on. The many new dependencies, in part introduced due to the cross-desktop cooperation, make the work of packagers harder. So do the 'Linux-isms' developers introduce while working on KDE software.

The questions revolved around the different and unique way the PC-BSD team works, including: how they keep applications in their own folder at all times; and how each package can be so self-contained that pretty much the whole OS can be upgraded, yet the apps keep on working as they were. This has side effects such as increased RAM and harddrive storage usage. The team is looking for solutions for this and are really creative in their efforts to find a solution. An interesting point which came up is that due to the similarity between Mac and BSD, the people working on Mac benefit from the PC-BSD work and vice versa. All in all, the PC-BSD team and their work is something to keep an eye upon.

Kris was followed up by Ian, who talked about Telepathy. He explained why Telepathy (an Open Source real time communication framework) is needed, what it aims to do and how it works. Based on D-BUS it can easily be used from within KDE, and Ian gave a quite detailed description of how Telepathy works.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/romain_demoing_windows.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/romain_demoing_windows_small.jpg" width="300" height="225"/></a><br />Romain on Windows</div>

After lunch, Romain Pokrzywka told what he referred to as "the horror story of KDE on Windows." According to him it is alive and well - and yes, he runs Windows 7 with KDE Applications on it. He demoed a few of the applications, including a few crashes and error messages. The mixing of Windows and Plasma widgets was pretty impressive as most of the things do indeed work pretty well. After the demo Romain spoke about the history of the KDE-on-Windows efforts to give us some background on where it came from before talking about where it is going. We were introduced to the heroes of KDE-on-Windows and Romain explained how the development work currently goes. A few of the things Romain demoed are pretty new and impressive - Nepomuk and Akonadi, for example. The KDE PIM demonstration results in input from Till Adam who makes everyone anxious for KDE SC 4.5 - while 4.4 isn't even released.

After the second demonstration Romain moves to what the team is currently working on. Solid is heavily under development, and Strigi development still has to be started. Furthermore, a few things are not planned at all. Among those are KWin and the Plasma taskbar and systemtray. KWin isn't even a technical possible, and the taskbar and systemtray would be possible but are very difficult and are simply low priority. Windows has a desktop shell, and while Plasma is very nice, it's not crucial for a good experience for KDE applications on Windows. Romain himself does like to use Plasma, purely as a desktop replacement. Several of the widgets really offer interesting functionality, especially folderview.

Romain then lifted the lid on a few future plans like support for more platforms (64-bit and WinCE), more applications, better integration and per-application installation packages. On the integration front, it was concluded that applying a random style to each application when it starts would make KDE applications feel more at home in Windows, fitting in with the huge variety of styles on the platform. One day, the KDE-on-Windows team would hope to play a role in the world-domination plans of the wider KDE community. 

Marcus Hanwell was next with a talk about CMake and improving the development process. After a short introduction of Kitware, the developers behind CMake and their other technologies, he showed lots of colorful and pretty pictures of the scientific work he has been involved in. After this Marcus went into the details of the development process Kitware has set up using CMake, CDash, CTest and the other tools they use. By using extensive automated testing, the development process can become faster and leaner, thus allowing developers to focus on writing features instead of fixing bugs. CMake makes it possible to work much easier with cross-platform C++. The adoption by KDE has made a huge difference to the spread of CMake, and these days it is really pervasively used. Marcus detailed some technical features of CMake and then moved on to giving tips, tricks and examples for how to use CMake. After the extensive CMake introduction Marcus continued with in-depth CTest information, revealing the brand-new and still experimental git support. Finally, there is a call for interested developers: Kitware is hiring, looking for Qt knowledge!

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/leo_and_too_much_music.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/leo_and_too_much_music_small.jpg" width="300" height="225"/></a><br /></div>

Leo Franchi then gave a talk about Amarok. He started by showing a variety of media players, showing how all the applications seem to focus on sexy and good looks, copying iTunes-like functionality, showing big covers etc. He points out how all these applications try to be intelligent but often fail at it. And of course the applications almost all try to push as much paid content to you as possible. While Web 2.0 can be can be cool, somewhere money has to be made. Having all your music streamed to you for free - that's not gonna happen in the long term. Integration of offline content and online is key here, as you can see with the business models of pretty much all other music services out there.

So what is the problem a good media player should solve? First of all, there is a lot of music in the world. So what does the user want to hear? There are services out there which have been working on this, like Last.fm, and there are a lot of ways to visualize and calculate similarity, and some of these smarts should be put into the apps. The dynamic playlists in Amarok are a way to let the user take care of most of their music tastes themselves, but the team is working to introduce a variety of 'intelligent' biases using data from Last.fm and other services. The information is out there - the challenge is to find and integrate it. Other desktop players fail at this and it is something the Amarok team wants to get right. 

Leo introduces Playdar, a server tool which runs on the background and can get you the music you want - finding it from wherever it can. It will play the song from your collection if you have it; play it from a friend which has shared his or her files if they have it; or find it on the world wide web, say from the streaming service you're subscribed to. Work is being put in integrating this technology as well. Leo's topic solicited a lot of responses and discussion, which kept the team busy for a while longer.

After the snack break Caio Marcelo, a developer from INdT gave an introduction to QML, a new language to write user interfaces for Qt. QML is simply a way to describe trees of objects and properties which together describe the user interface. QML allows you to embed ECMAscript (javascript in Qt) as well to perform simple transformations. QML is heavily inspired by Qedje, the user interface language developed by the Enlightenment developers. QML allows you to use animations and effects and describe regions around objects which respond to mouse input. Caio demonstrated a variety of examples based on QML and shows how easy it is to create dynamic interfaces using this declarative language.

To explain the concept of the declarative UI, Caio gave a few analogies:
<ul>
<li>EFL: "QML is a kind of Edje for Qt"</li>
<li>Web: "QML is a kind of HTML+CSS"</li>
<li>Qt: "QML is a kind of .ui file from QtDesigner"</li>
</ul>

You can use QML in canvas-based applications like Plasma and mobile interfaces. You can even write small applications like Plasma widgets entirely in QML. It would also be a great candidate for a new UI for KDM as it can give themes a lot of flexibility. The power of QML is to allow a sort of advanced themability - themes can change the interface entirely so you can have a single application with several QML 'themes' for different devices from large screens to mobile devices. Graphics, usability and interaction designers can work much more closely with developers, changing interfaces directly. Declarative UIs can bring a variety of new features like easier network transparency to UIs, and the whole range of possibilities is still unclear.

Marco Martin proceeded with a talk about the Plasma Netbook interface. He listed 'the competition', explaining how they work and what advantages and disadvantages their approaches had. Introducing a number of insights, Marco showed how deeply the netbook team has thought about their user interface. The requirements basically boil down to creating an interface with as few elements as possible to get the task the user wants to do done as easily and quickly as possible. However the team had more in mind than just creating this interface - it was also important to make as few assumptions as possible about the form factor, creating a very flexible solution. The goal was to push as much functionality as possible in the underlying infrastructure, to assure future interfaces like for mobile or multimedia devices would be even easier and faster to develop.

Marco started to present the various parts of the netbook interface like the Search and Launch interface and the Newspaper Activity, describing how they work and what assumptions the are based upon. While going through these items he shows how they look and work. 

Looking at the future, it is clear that what KDE SC 4.4 brings is just step one. The look and behavior can still change, and there are many new widgets needed to complete the experience we want to offer: email is coming but appointments and other web services are still needed. Furthermore it is a challenge to scale up and down for widgets, and this is something which still needs investigation. But more and more tiny devices are coming on the market, and the work on this will prepare the KDE technology and community to provide an innovative and creative alternative UI for them.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/chani.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/chani_small.jpg" width="300" height="225"/></a><br />Chani on stage</div>

Chani Armitage took the stage with the last presentation of the day, with the work of the Plasma team on Activities as main topic. She started out by demoing a few of the Activities she uses - which is a lot. So what is an Activity? An Activity is what you are actually doing. If you work on a math task for university, you have a bunch of windows like a PDF file with the assignment, a calculator and some other things. These things belong together but there is currently no organization: we have to manage them ourselves. Chani wants to organize this in a better way. We now have implemented the Desktop Activities, but she is trying to associate windows to a desktop as well. You should be able to save activities and open them on demand. And this not only to make them easy to manage but it also saves resources. More importantly, applications should be able to react to your activities - KMail should show email folders relevant to your math course, the file open dialogs should have relevant shortcuts etc.

This is going to be a challenge. The current infrastructure does not support these things - changes will be needed in many places, including the KDE Applications. For example saving sessions and opening them will be hard to get working in all situations - say an application is running in two Activities? Hopefully a new cross-desktop standard can emerge from this. It is not all grim, however. Nepomuk already has experimental functionality to tell applications what activity is currently running so they can customize their content.  

Chani has already implemented what she refers to as 'a horribly ugly hack' which allows closing and re-opening activities in Plasma but this is just a very first step.

The third day will have more talks but those will be even more technical. All in all, the author is enjoying the conference. As usual it is good to meet old friends and catch up with them, meet new friends and learn what cool stuff those are involved with and to generally hang out and have good conversations.