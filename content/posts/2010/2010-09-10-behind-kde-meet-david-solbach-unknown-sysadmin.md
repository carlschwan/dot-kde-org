---
title: "Behind KDE: Meet David Solbach, the \"Unknown SysAdmin\""
date:    2010-09-10
authors:
  - "tomalbers"
slug:    behind-kde-meet-david-solbach-unknown-sysadmin
comments:
  - subject: "Bringing work home"
    date: 2010-09-10
    body: "In other words, his day job involves blood and his volunteer work involves plasma."
    author: "Luigiwalser"
  - subject: "to me, he'll always be Goliath ;)"
    date: 2010-09-10
    body: "i first met David on irc where he masquerades as Goliat23; he has been simply terrific to work with as a sysadmin and has been critical to the Plasma team having the tools we require to get our work done. outside of the serious stuff, he's also fun to chat with when the time is there. so as usual, another round of hats off to this person who is Behind KDE. :)"
    author: "aseigo"
---
In this week's <a href="http://behindkde.org">Behind KDE</a> interview, we talk with one of the unknown powers behind the sysadmin team, David Solbach. David is the maintainer of <a href="http://reviewboard.kde.org">reviewboard.kde.org</a>. However, there's much more to David than code review and contributing to KDE. He can also design and develop diagnostic blood analyzers and tell you what it was like to take a hit from the bursting of the <a href="http://en.wikipedia.org/wiki/Dot-com_bubble">dot-com bubble</a>.

Enjoy the entertaining and interesting <a href="http://www.behindkde.org/node/835">interview with David</a>!
<!--break-->
