---
title: "Introducing Your KDE Software Labels"
date:    2010-06-21
authors:
  - "Stuart Jarvis"
slug:    introducing-your-kde-software-labels
comments:
  - subject: "one thing..."
    date: 2010-06-21
    body: "I'd like to mention Stu has done far more than post a few articles - he's done the pushy and managementy stuff to get this done. Very good work, Stu!\r\n\r\nGive a few cheers to him :D"
    author: "jospoortvliet"
  - subject: "Another thing ..."
    date: 2010-06-22
    body: "... is that KDE aficionados should stop devoting too much time to KDE core itself, which is IMO matured enough and people should start caring much more about KDE based applications.\r\n\r\nKarbon has almost died, Kivio and KFormula have already been largely abandoned and now both are unmaintained and will likely be excluded from KOffice.\r\n\r\nI know that KOffice and KDE developers teams are mostly unconnected but that doesn't diminish the fact that they are only very few complex and useful applications beyond the core KDE stack, marked by the shiny \"Powered by KDE\" logos.\r\n\r\nOh, and I haven't ranted enough: new KDE versions get released every six or nine months, whereas our primary competitor updates its shell only once per three years.\r\n\r\nP.S. I'm not a spammer!"
    author: "birdie"
  - subject: "KOffice apps"
    date: 2010-06-22
    body: "Hey, hey!\r\n\r\nLet's not paint the devil on the wall just yet!  (Does that expression even exist in English?)\r\n\r\nKivio has not been abandonded, quite the opposite.  After a long time without a maintainer, there is now a group of new people working to revive it.  It's not impossible that it will make a reappearance in KOffice 2.3.\r\n\r\nKFormula, the application, is indeed abandoned, but the formulas live on as an embeddable shape, the formula shape.  It's true that it could use more work, and the KOffice group would welcome new people to work on it, but it will most certainly not be excluded from KOffice.\r\n\r\nKarbon did indeed lose its maintainer recently, but that is one of the most mature applications in KOffice right now and will live on as it is until a new maintainer steps up and adds new features to it.  But we have to remember that Karbon is also the application in KOffice that uses the libraries and plugins the most and adds the least application specific code.  So all the work that goes into the libs and plugins also immediately adds features to Karbon.\r\n\r\nAll this said, now is a perfect time to join the KOffice group.  So much is happening right now, and there are many places for less experienced people to start working.  Please come to #koffice on IRC and we can help you out.  The formula shape, for example, should offer some challenges and also some simple things to do."
    author: "ingwa"
  - subject: "Disappointed - how about Entry 9?"
    date: 2010-06-26
    body: "While the winning entry, Entry 1, is a good design, I'm nevertheless disappointed that Entry 9 didn't win. It should be noted that Entry 9 actually received the MOST community votes (186) vs only 149 votes for the winning entry. I realize, of course, that a panel of judges was involved, but it would have been nice to see some incorporation of Entry 9. Both Entry 1 and Entry 9 share similar designs, but I feel that Entry 9 is more representative of the modern KDE desktop as it incorporates an \"Air\" look which is consistent with the current KDE theming. Entry 1 feels more like KDE 3.x branding, where as Entry 9 felt more modern and relevant to KDE 4.x. Just my 2 cents. "
    author: "irrdev"
  - subject: "an Italian translation"
    date: 2010-06-28
    body: "For the Italian readers:\r\n\r\nhttp://kde-it.org/2010/06/26/introduzione-alle-proprie-etichette-del-software-kde/"
    author: "giovanni"
---

A while ago, the KDE promo team organized a <a href="http://dot.kde.org/2010/06/05/voting-opens-kde-software-label-designs">competition to choose a design</a> for labels that producers of software within our community can use to show that they are part of KDE. Today we are happy to announce the winning designs:

<img style="position: relative; left: 50%; margin-left: -285px;" src="http://dot.kde.org/sites/dot.kde.org/files/labels_0.png" />

These images are now available to <a href="http://www.kde.org/stuff/clipart.php">download from the KDE Clipart page</a> so that you can use them on your project website or within your application or documentation.

<h3>The labels</h3>

For giving application developers - and in particular third party developers - a way to identify themselves with KDE and to label their software as such, we wanted to provide a standard set of KDE software labels. These can be used to communicate that developers feel themselves to be part of the KDE community or that they use the KDE platform. They can be used for example on application websites, in the about dialog of applications or in general communication about releases. They can be particularly useful in cases where it is not obvious that an application is part of KDE or making use of KDE technology.

To be able to express different aspects of the relationship to KDE and provide some variety for application authors we provide a choice of three labels, which are recommended for use with KDE software:

<b>"Powered by KDE"</b>

This label encompasses the dual meanings of software that derives its strength from our fantastic community and from the KDE development platform. This label is recommended for all free software applications whose developers feel part of the KDE community or which make use of KDE technology.

<b>"Built on the KDE Platform"</b>

This label has been chosen for the simplicity of the message it provides: this application uses the KDE platform. It is recommended for all application authors who want to stress their use of KDE technology.

<b>"Part of the KDE family"</b>

We recommend this label for application authors who in particular see themselves as part of the KDE community - and we hope anyone producing free software with KDE technology sees themselves this way.

<h3>The winner</h3>

The winning design scored consistently highly with the four panel judges from the KDE promo team and in the community voting poll on the KDE Forums. It was designed by <a href="http://vanbittern.com/"><b>Philipp Schmieder</b></a>, a freelance web and software developer from Germany. Philipp is familiar with the Qt framework (as demonstrated by his work on the free software video downloader ClipGrab) and has used KDE software since 2004. He comments that <i>"I haven't contributed to KDE, only reported some bugs etc, so the software label contest was a great possibility to finally do that"</i>.

Among the many other great designs we received, a special mention should also go to <b>Carmine De Rosa</b>, whose design edged out Philipp's in the community poll and was only just beaten in the combined community and panel scores.

<h3>The voting</h3>

In many ways, the software label contest has been successful. We have ended up with an excellent design, received many first time submissions from new contributors and captured the interest of the wider community with over 5000 views of the design poll.

The labels were voted on by the community, using the KDE brainstorm infrastructure on the KDE forums, and by a panel of judges from the KDE promotion team. With the experience we gained during this vote we'll improve the process for future votes.

<h3>Get the labels now</h3>

The software labels are now available to <a href="http://www.kde.org/stuff/clipart.php">download and use</a>. We would like, once again, to thank Philipp and all the other participants for their efforts and hope to see further contributions from them in the future.