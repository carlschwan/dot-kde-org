---
title: "KOffice 2.2 Beta 1 Released"
date:    2010-03-21
authors:
  - "ingwa"
slug:    koffice-22-beta-1-released
comments:
  - subject: "Very exciting!"
    date: 2010-03-22
    body: "Glad to see more external interest in KOffice. Will try out the beta and report bugs as soon as it's in the Kubuntu PPA - though they do sometimes bork up the packaging, which doesn't help... :/"
    author: "madman"
  - subject: "hope"
    date: 2010-03-22
    body: "i hope kivio will coming back soon"
    author: "collinm"
  - subject: "It's looking promising,"
    date: 2010-03-22
    body: "It's looking promising, there's a number of new developers who have started looking at it."
    author: "odysseus"
  - subject: "Thanks for sharing"
    date: 2010-07-03
    body: "Really very great for making short notes.\r\nAlways great to see.\r\nThanks a lot to your team."
    author: "Freshers123"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kexi-2.2-alpha1-form-datasource-tags.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kexi-2.2-alpha1-form-datasource-tags-350.png" /></a></div>The <a href="http://www.koffice.org/">KOffice</a> team is happy to announce the first beta of the upcoming 2.2 release of KOffice. This release brings back <a href="http://www.koffice.org/kexi/">Kexi</a>, the data management application similar to MS Access. The new beta also offers many new features and improvements, for example improved support for Microsoft file formats with the addition of import filters for MS OOXML, and bug fixes.
<!--break-->
Release 2.2 will be the first release where external companies and organizations have made significant contributions to development. Nokia has sponsored much work on the general improvements and the converters for importing MS file formats for their <a href="http://dot.kde.org/2010/01/21/koffice-based-office-viewer-launched-nokia-n900">Maemo office viewers</a>. <a href="http://www.nlnet.nl/">NLnet</a> has sponsored the work on RDF metadata and change tracking. <a href="http://www.kogmbh.com/">KO GmbH</a> as well as NLnet have sent participants to the <a href="http://plugtest.opendocsociety.org/doku.php?id=plugfests:200911_orvieto:info">ODF plugfest</a> and <a href="http://conference.services.openoffice.org/index.php/ooocon/2009">OpenOffice.org conference</a> in Orvieto, Italy.

Read the <a href="http://www.koffice.org/news/koffice-2-2-beta-1/">full announcement</a> for more information on this beta and look in the <a href="http://www.koffice.org/changelogs/koffice-2-2-beta-1-changelog/">changelog</a> for details.