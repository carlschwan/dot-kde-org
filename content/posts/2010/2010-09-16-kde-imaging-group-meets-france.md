---
title: "KDE Imaging Group Meets in France"
date:    2010-09-16
authors:
  - "mck182"
slug:    kde-imaging-group-meets-france
comments:
  - subject: "Very cool"
    date: 2010-09-17
    body: "\"face detection and recognition ... geotagging features ... non-destructive image editing ... image versioning\"\r\n\r\nWow. I want digiKam 2.0 :-)"
    author: "Stuart Jarvis"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/4620818767_5fb7450bc0_m.jpg" style="float:right;" />The team concentrated their efforts on refining and building upon this year's Google Summer of Code projects. digiKam had three GSoC projects this year: face detection and recognition by Aditya Bhatt, geotagging features by Gabriel Voicu and non-destructive image editing, and image versioning by Martin Klapetek. The results of this work will be included in the upcoming digiKam 2.0 version along with Kunal Ghosh's <a href="http://blog.lydiapintscher.de/2010/04/26/gsoc-and-season-of-kde-2010/">Season of KDE</a> work—scripting support for digiKam. Of course, none of this work would have succeeded without the great mentors and digiKam developers, with whom the students could work side-by-side at the sprint: Marcel Wiesweg, Michael G. Hansen and Gilles Caulier.

There were many other important attendees, including Andreas Huggel (to whom we can be thankful for the Exiv2 library and tools for all kinds of metadata handling) and Laurent Espitallier, who works on import tools from other photo management applications such as Picasa or Nikon Project.

Overall, the sprint was a really great and fruitful experience for all of us, especially finally getting to know people in person after working with them through the Internet. If you would like to find out more about what we got up to, please read the <a href="http://www.digikam.org/drupal/node/538">much more detailed report</a> posted on digiKam's website. And you can see some <a href="http://www.flickr.com/groups/kde-imaging-codingsprint/">nice pictures from the sprint</a> on flickr.