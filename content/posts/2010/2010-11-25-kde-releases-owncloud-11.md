---
title: "KDE releases ownCloud 1.1"
date:    2010-11-25
authors:
  - "kallecarl"
slug:    kde-releases-owncloud-11
comments:
  - subject: "Great progress"
    date: 2010-11-29
    body: "Thanks for the article, I would have missed this release if it wasn't posted here.\r\n\r\nFrank does great things, regardless of where he's from ;)"
    author: "samtuke"
  - subject: "Very important project"
    date: 2010-11-29
    body: "Perhaps one of the most important for free software, these days.\r\n\r\nAlso one where FOSS can shine: no network effect to keep barriers of entry high: basically it doesn't matter that all your friends use dropbox, owncloud is just as useful.\r\n\r\nI find it sad however that this project receives so little publicity:  I have to lurk on the git web interface to follow advances! I hope a distrib or other will do the right thing and do a nice packaged owncloud installation. With control centre integration and a (dedicated) kioslave, this would be brilliant.\r\n\r\nIn any case well done guys, you rock."
    author: "hmmm"
---
Frank Karlitschek has just released ownCloud version 1.1, which contains many new features, bugfixes and improvements. It is suitable for everyday use. ownCloud is a web-based storage application similar to Google Docs, Dropbox or Ubuntu One with one major difference - all your data is under your own control. Read <a href="http://blog.karlitschek.de/2010/11/owncloud-11-released.html">Frank's blogpost</a> for more information about this active project and its growing development team.
