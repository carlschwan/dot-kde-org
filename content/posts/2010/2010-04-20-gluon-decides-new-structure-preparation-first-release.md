---
title: "Gluon Decides on New Structure in Preparation for First Release"
date:    2010-04-20
authors:
  - "leinir"
slug:    gluon-decides-new-structure-preparation-first-release
comments:
  - subject: "Great Project"
    date: 2010-04-22
    body: "Hi Gluons, firstly, congratz on all the work so far.\r\n\r\nQuick question: what types of games are likely targets for being gluon clients? Would it be useful for RTS or RPG type games, or is it mostly beneficial to arcade style gaming?\r\n\r\n\r\n"
    author: "borker"
  - subject: "I am not a gluon dev but have"
    date: 2010-04-23
    body: "I am not a gluon dev but have followed it a while. As far as I know has one target: 2d games. Target is not there yet. If (or maybe when) they fullfill the goal it should be possible to create both RTS and RPG type of games, but just in 2d "
    author: "beer"
  - subject: "Hey, thanks! It's been hard,"
    date: 2010-04-24
    body: "Hey, thanks! It's been hard, but really good work so far :)\r\n\r\nAs beer says, we're not aiming for any particular type of game, but are focusing on 2D for the time being. We may later branch into 3D (since the engine is entirely capable of handling it - the game world is 3D already, it's just the camera which is currently orthographic), but for now we are focusing on 2D so we don't have to worry about that part of it while we get the rest of the engine up and running as it should :)\r\n\r\nIf you have any specific suggestions for making it easier to create certain types of games, please drop by the irc channel and we can take it from there :)"
    author: "leinir"
  - subject: "Sounds nice that you plan to"
    date: 2010-04-26
    body: "Sounds nice that you plan to include 3d but I think it is a bid early for that"
    author: "beer"
  - subject: "I would like someone could"
    date: 2010-04-26
    body: "I would like someone could make a nice looking building demolition / bridge building games. \r\nThis is a newer version of old 2D DOS game. Made with Flash. 3D version of it would be even nicer! :-D http://www.physicsgames.net/game/Demolition_City.html\r\n\r\nI played the DOS game about 12 years ago (Win 98 just came out) and it was damn cool. "
    author: "Fri13"
  - subject: "yes"
    date: 2010-04-30
    body: "I love to play <a href=\"http://www.yazarokur.com/sudoku/\">sudoku</a> and <a href=\"http://www.yazarokur.com/sinema/\">sinema</a>"
    author: "sinemasever"
  - subject: "I am not a gluon dev but have"
    date: 2010-05-07
    body: "I am not a gluon dev but have followed it a while. As far as I know has one target: 2d games. Target is not there yet. If (or maybe when) they fullfill the goal it should be possible to create both RTS and RPG type of games, but just in 2d <a href=\"http://www.edhardyone.com\">Ed Hardy </a> <a href=\"http://www.uklondons.com\">links of london</a>"
    author: "linkslondon"
  - subject: "Gluon"
    date: 2010-05-20
    body: "Gluon Creator has been created. As seen in the screenshot above, the application is made up of a lot of dockable viewports, in the vein of Amarok 2's powerful Lego style interface layouting, with a default layout which it is hoped will work for most users.\r\n\r\n\r\n\r\n\r\n\r\n<a href=\"http://www.hattoss.com/\">Top Online Universities</a>"
    author: "rockydutt"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 350px;"><a border="none" href="http://dot.kde.org/sites/dot.kde.org/files/Gluon-flow_2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-flow.png" width="350" height="108" alt="Gluon empowers the creators of games and the players of games" /></a><br />Gluon empowers everybody: From the creators of games to the players of games, and back again</div>

<p>Those who have been following Gluon closely over the last months know that at the <a href="http://dot.kde.org/2009/10/28/gluon-sprint-wrap">developer sprint in Munich in October 2009</a> the foundations were laid down for the Gluon vision. Put simply, Gluon provides a new way for game creators to make games and distribute them to the players of games. This was all written down in what is now <a href="http://gluon.gamingfreedom.org/node/7">The Gluon Vision</a> on the Gluon wiki.</p>

<p>Those who have not been following it so closely may remember <a href="http://dridk.blogspot.com/2009/10/gluon-first-technical-preview.html">the technology preview</a> which was released. This happened before the vision was created, and as such it contains the three libraries KCL, KGL and KAL, as well as the demo game Blok. Since then, a lot has changed:</p>

<ul>
<li>The three libraries are now called GluonInput, GluonGraphics and GluonAudio.
<li>Very heavy refactoring has happened to make GluonGraphics focus more on graphics, so that it no longer handles physics as well
<li>GluonInput has essentially been rewritten from scratch, as it was at the time tied very tightly to evdev, a library which only exists on Linux/X11.
<li>Blok has not yet been updated to the new code, and as such does not work in this alpha release. Rest assured, however, that this highly entertaining game will return, stronger and better ;)
</ul>

<p>The libraries are now in a soft freeze, and thus we release the first alpha: Version 0.70, codename &quot;X-Ray&quot;. What this means is that any changes to the public API need to be done in a branch (or clone) of the Gluon repository, and not merged in until the changes have been discussed with the rest of the team. This is done to avoid Gluon becoming too much of a moving target. Note, however, that this is a soft freeze: We do not yet guarantee binary compatibility; that will not happen before 1.0. This style of development basically follows Qt's own model.</p>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 350px;"><a border="none" href="http://dot.kde.org/sites/dot.kde.org/files/gluon-creator-invaders.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-creator-invaders-thumb.jpg" width="350" height="212" alt="The first GluonEngine based game, Invaders, being edited in Gluon Creator" /></a><br />The first GluonEngine based game, Invaders, being edited in Gluon Creator</div>
<h2>So, why the many changes?</h2>
<p>The simple answer: The vision required it of us. To be able to support the vision of letting the makers of games push their games all the way to the players of games, we created a new library, GluonEngine. This is based around the concept that to allow people to create games, Gluon must reduce the amount of boiler plate work required of the makers of games to an absolute minimum. GluonEngine thus provides a system by which scenes are created by hierarchies of game objects, which are simply positions in the world, and which can be extended with new functionality by adding components to them.</p>

<p>To assist people in creating this without having to open a text editor and write the scenes by hand, the application Gluon Creator has been created. As seen in the screenshot above, the application is made up of a lot of dockable viewports, in the vein of Amarok 2's powerful Lego style interface layouting, with a default layout which it is hoped will work for most users.</p>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 350px;"><a border="none" href="http://dot.kde.org/sites/dot.kde.org/files/gluon-player-invaders.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-player-invaders-thumb.jpg" width="350" height="273" alt="The simple Gluon Player used to play Invaders" /></a><br />The simple Gluon Player being used to play Invaders</div>

<p>The game you see in the screenshot of Gluon Creator above is a simple Space Invaders clone, tentatively named Invaders. The game has been slowly developing over the duration of the last couple of months, and is the handiwork of Arjen &quot;ahiemstra&quot; Hiemstra from the Gluon team, who has been using it as a test bed for Creator itself.</p>

<p>Finally, the first, very simple Gluon Player application was created. There will be a Gluon Player for each supported platform. The first Gluon Player application simply loads up a GluonEngine based game, and starts the game. It was created in a very short period of time and in a total of 142 lines with copyright messages and everything, showing just how powerful GluonEngine system really is.</p>

<h2>Where do we go from here?</h2>

<p>The road ahead is clear: while GluonEngine and the three base libraries are already looking good, there is a multitude of functionality missing. So, the following is a round up of what will need to happen before the next release:</p>

<ul>
<li>A proper editor viewport in Gluon Creator needs to be created, to allow for the more artistically inclined among us to position elements in the scenes. This is a vital element for Gluon Creator's success: If the visual artists are not able to work in the way they are used to, and forced to set values in the property view, Gluon Creator is unlikely to ever be used outside of the relatively narrow world of geeks.</li>
<li>Refactoring for GluonAudio and the GDL handling code needs doing. During the last few months, the code in these two systems have proven to be powerful, but slightly clunky to work with. As such, based on the experiences, some refactoring will need to be done.</li>
<li>GluonGraphics needs a proper system to handle materials and shaders, so as to allow for the connection of these items in a more pleasant manner than is currently the case.</li>
<li>Sandboxed Smoke based QtScript bindings for Gluon and Qt for the scriptable component, so as to allow the makers of games the use of the entire Qt API, but at the same time not simply give them the full API, which would include file access and network access. The reason this cannot be allowed is simple: The distribution method proposed for Gluon is based on a website of the same style as KDE-Look.org and such, and as such the scripting abilities need to be throttled in such a way as to not allow for simply accessing everything on the machines of the players of games.</li>
<li>Players. Not the physical kind, though players of games will, of course, be important to us, as much as makers. No, what this is, is the applications which will be used to fetch and play games on desktops and various mobile devices. A Plasma Widget for KDE SC 4's powerful Plasma desktop is in the planning stages, as is a client for Maemo. However, more are needed to cover the wide range of potential devices: MacOS X and Windows are not covered by the Plasma Widget, and neither is GNOME, even though otherwise capable of running GluonEngine based games. So: More Gluon Players!</li>
<li>Components. Lots and lots of components. The method by which the functionality of the game objects is extended, components, is such that while it is possible to write just about anything you can think of, Gluon should provide for all the base cases without the makers of games needing to write the same components over and over. So, amongst others, components for Physics are needed, whether based on Box2D or other libraries.</li>
</ul>

<h2>How can you help?</h2>

<p>As you can tell from the section above, there is plenty of work to be done, and Gluon is by no means finished. As such, the Gluon team would like to invite you to drop by the <a href="irc://chat.freenode.net/#gluon">Gluon IRC channel</a> if you feel like you would like to help us with any of the points above.</p>

<p>Of course, you would likely wish to download this release as well. So, head on over to <a href="http://gluon.gamingfreedom.org/node/3">the Getting Gluon page</a> and grab yourself a copy, while they're still warm!</p>

<h2>Finishing Thoughts</h2>

<p>The freedom of gamers as it is now is limited by the game industry's old fashioned insistence on limiting the distribution of games. Gluon aims at breaking this by providing both the makers of games and the players of games with a new platform for sharing the experience of playing games, and for providing feedback to each other - both in the form of comments and ratings, but also through a donation based payment system. So: There's a revolution coming where the <a href="http://www.gamingfreedom.org/">freedom of gaming</a> is at the center.</p>
