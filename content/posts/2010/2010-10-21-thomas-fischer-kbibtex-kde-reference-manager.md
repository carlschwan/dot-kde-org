---
title: "Thomas Fischer on KBibTeX, the KDE Reference Manager"
date:    2010-10-21
authors:
  - "einar"
slug:    thomas-fischer-kbibtex-kde-reference-manager
comments:
  - subject: "Good"
    date: 2010-10-21
    body: "Thanks for all your work. Great that some of the kpapers proposal ideas are being taken into consideration. From previous discussions here, I've found Zotero and Mendeley. Both seem pretty good. But KDE now seems to provide the technology (nepomuk, strigi...) to put together a really nice Academic Reference Management application. Hope it happens. Best luck with Kbibtex!"
    author: "jadrian"
  - subject: "Thanks for the work indeed!"
    date: 2010-10-22
    body: "Thanks for the work indeed! I'm using the pre-alpha version of KBibtex4 (with backups, as it may eat my data), and conceptually it is shaping up very nicely! \r\nThe most serious thing I'd like to ask for now is a bit more of a community within KDE - working with the KDE bug tracker, forums, mailinglists... as gna is nice i guess, but yet another site to track/log in to."
    author: "thijsdetweede"
  - subject: "KBibTeX for KOffice?"
    date: 2010-10-23
    body: "Hi Thomas!  Really nice reading this.  Do you think it would be interesting for you to interface KBibTeX for KOffice?  I think a good bibliography manager would be an awesome addition to KOffice, and would make it more valuable for the scientific world. \r\n\r\nAnd since you seem to be not very far from me, perhaps we could even have a mini-sprint to get a small group working on it.  Both I and Cyrille Berger are in Link\u00f6ping, so it would be easy to meet. :-)"
    author: "ingwa"
  - subject: "cb2bib/mendeley"
    date: 2010-10-23
    body: "Hi Thomas...\r\nI am glad you mentioned cb2bib and some others mentioned mendeley... which in my opinion are the two apps that a viable kbibtex should superseed.\r\ncb2bib in my opinion was intended as a matching experience for bibdesk in linux. Interfacing kbibtex with mendeley's online part may be a goal.\r\n\r\nAlin"
    author: "Alin"
  - subject: "Not yet"
    date: 2010-10-26
    body: "So far, KBibTeX is an independent project but this may change in the future if both me (and whoever develops KBibTeX, too) and the KDE team agree that moving KBibTeX under the KDE umbrella is a good idea.\r\n\r\nRight now it can be argued that the independence of KBibTeX demonstrates that the KDE framework is used outside the KDE SC, too ..."
    author: "t-fischer"
  - subject: "Sure"
    date: 2010-10-26
    body: "I have to admit I rarely use KWord, LaTex is still my preferred tool ...\r\n\r\nThe most obvious integration would using KBibTeX's KPart. A more refined approach could use editing components available via a dedicated library.\r\nWhat kind of bibliography format uses KOffice/KWord?"
    author: "t-fischer"
  - subject: "Bug reports"
    date: 2010-10-26
    body: "I am getting some good ideas on what features can be integrated. In addition to post them in some forum (here or kde-apps), write a bug report on the <a href=\"https://gna.org/bugs/?group=kbibtex\">GNA page</a>. This way I can better track which request exist and prioritize them. Please write individual reports for different request."
    author: "t-fischer"
  - subject: "KDE Extragear"
    date: 2010-10-27
    body: "Personally I think that it would be a nice companion to Kile in Extragear."
    author: "einar"
---
While they are not busy doing (crazy) research, most scientists do a lot of technical writing: papers, presentations, posters, reports. Such writing is usually accompanied by large number of references; managing them by hand can be tedious and long. That is why scientists use bibliography managers. In the FOSS world, a popular choice is BibTeX, a complement to the LaTeX typesetting system. Along with BibTeX, front-ends are used to simplify addition, modification and removal of bibliography. 

One of the best known KDE BibTeX frontends is <a href="http://www.unix-ag.uni-kl.de/~fischer/kbibtex/">KBibTeX</a>, developed by <b>Thomas Fischer</b>. KBibTeX sports many interesting features, including handy integration with <a href="http://kile.sourceforge.net/">Kile</a>, an integrated editor for LaTeX built on the KDE Platform. KBibTeX is built on KDE 3.5; a port to KDE Platform 4 is in progress.

<b>Luca Beltrame</b> took the opportunity to ask Thomas about KBibTeX, the KDE Platform, science and KDE. Read on for his thoughts and to find out what the KDE Science team can do for you.
<!--break-->
<h2>Thomas Fischer on KBibTeX</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/thomas.jpg" /><br />Thomas Fischer</div><b>Luca:</b> Thanks for participating in the interview, Thomas! Can you tell us a bit about yourself?

<b>Thomas:</b> Hello! My name is Thomas Fischer and I am the main developer of KBibTeX. I studied computer science in Darmstadt, Germany, where I received my MSc. I did my PhD in Kaiserslautern, Germany, which I finished two years ago. Soon after, I moved to Sweden for personal reasons and got a job as a computer science lecturer and researcher at a small university situated between Gothenburg and Stockholm. Besides spending most of my awake time in front of a computer, I enjoy hiking, cycling, and horse riding in the beautiful Swedish countryside. Whenever possible, I combine these open-air activities with geocaching or mapping streets for the OpenStreetMap project.

<b>Luca:</b> Can you tell us how KBibTeX was born?

<b>Thomas:</b> During my PhD studies, I was facing the problem of organizing a growing collection of papers and literature references. Besides knowing how to organize printed papers, you have to have a proper literature database to correctly cite them in your own publications. As LaTeX is the natural choice when writing scientific publications, BibTeX is the literature database most people use. Although it is possible to maintain BibTeX files with a plain text editor, it is much more comfortable with a proper editor that takes care of the necessary details and also has some advanced features such as publication search and data extraction.

Sometime in summer 2004, I realized that there was no good (by my requirements) BibTeX editor available, which is why I started my own project using KDE software, my favorite desktop environment then and today. Since I hadn't much experience with Qt or KDE programming, it took some time before I got things right. On December 10, 2004, I made my first commit into SVN which later evolved into KBibTeX for KDE3. 

<b>Luca:</b> Are there any other people working with you on KBibTeX?

<b>Thomas:</b> So far, there have been only a few code submissions, mostly small bug fixes. There are also some users who translate the user interface to different languages, which is quite helpful. The most important contributions come from (long-time) users who are very good at spotting bugs, almost like a magnet attracting iron. KBibTeX would not be so stable and feature-rich if I had not gotten all those bug reports. So, thank you all!

<b>Luca:</b> How can one help KBibTeX? What needs the most attention?

<b>Thomas:</b> The lack of documentation (i.e. handbook) is the biggest issue with KBibTeX for KDE3. For KDE Platform 4, I started from scratch to implement new ideas such as a (new) user interface that draws inspiration from the <a href="http://kde-apps.org/content/show.php/Kpapers:+idea+looks+for+developers!?content=58990">Kpapers proposal on KDE-Apps</a>. As the new version matures, it needs patient testers just like the KDE3 version. Digital artists and other creative people as well as user interface experts are welcome with their skills. Once the user interface gets more stable, the project will need people who can translate the UI and write detailed documentation. Writing good documentation is a difficult task and can be as demanding as writing good code.

<b>Luca:</b> KBibTeX's current stable version is for KDE 3, with a port to Platform 4 port in progress. What were the biggest difficulties encountered when porting?

<b>Thomas:</b> Much of the low-level code of loading and saving BibTeX files (and other formats) has been ported directly. I've encountered some minor problems due to API changes in Qt and the KDE Platform, but nothing serious. Rewriting the GUI has been a bit more challenging. For example, the Model/View system in Qt4 was new to me and required some re-thinking. So far, the biggest difficulty was the downtime of api.kde.org a few weeks ago. ;-)

<b>Luca:</b> What are your future plans for KBibTeX?

<b>Thomas:</b> My main focus is on developing KBibTeX on KDE Platform 4; the old KDE3 version will only receive bugfixes if necessary. For the Platform 4 version, I plan to have a more or less stable version with only a basic set of features initially. Long-term plans include adding all essential features of the KDE3 version, an improved user interface (constructive feedback welcome), and evolving KBibTeX into a full bibliography manager which can, for instance, manage PDF files linked to BibTeX entries. I would also like KBibTeX to support more bibliographic file formats such as biblatex, Biber, more online search engines, etc.

<b>Luca:</b> Aside from KBibTeX, what other KDE software do you use?

<b>Thomas:</b> I use a full KDE desktop: Konqueror is my primary browser (Firefox or Opera as fall-backs), Konsole is always running as I am still a command-line fan, and KMail/Kontact is my mail client. I am not a big fan of Plasma widgets on the desktop, as I usually run all applications maximized and never see my desktop. ;-)

<b>Luca:</b> Are you involved in other Free Software projects?

<b>Thomas:</b> I am not an official member in other free software projects, but for the sofware I use I submit bug reports whenever possible, and sometimes even patches. I work also on some other smaller projects such as the KParts browser plugin which wraps KDE's KPart technology into Netscape-compatible browser plugins. That way you can use, for example, Okular's PDF viewer inside Firefox, Opera, or Chrome. KDE-wise, one of my earliest code contributions was the new-document wizard in Kile, my most recent contribution is the improved AdBlock in Konqueror 4.5 (automatic updates of filter lists). My favourite Linux distribution is Gentoo Linux, so I try to stay involved by discussing bugs in the bug tracker and contributing ebuilds from time to time.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kbibtex.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kbibtexsml.png" /></a><br />KBibTeX in action <a href="http://dot.kde.org/sites/dot.kde.org/files/kbibtex.png">(click for larger image)</a></div>

<b>Luca:</b> What do you think about the use of KDE software in science? Where does it need improvement?

<b>Thomas:</b> Well, there are several aspects. First, more people need to hear about it, so a little 'advertising' can't hurt. For example, scientists can tell their fellow researchers about the advantages of KDE software. Better communication channels for science users would also be good, such as mailing lists and IRC channels. Also, many researchers (have to) use Windows, which is why more effort should be put into making KDE software as stable on Windows as it runs on Unix or Linux. This would make scientific KDE software such as KBibTeX available under Windows, too, thus reaching more users. Last but not least, software developers need to know that KDE provides an excellent platform to build upon. For example, the integration of Okular's PDF viewer in KBibTeX with just a few lines of code was a big plus for me.

<b>Luca:</b> Do you work together with the Kile developers or people writing other LaTeX editing software?

<b>Thomas:</b> No, not really with the Kile developers. When I receive bug reports on using KBibTeX in Kile, I may have dig into the problem (i.e. Kile's source code) and rarely file a bug report in Kile's bugzilla, but apart from that KBibTeX is pretty much stand-alone. But I've been in contact with the author of bibutils. This tool is a set of command-line programs to convert between different bibliography file formats; KBibTeX can use bibutils to import formats that it does not understand natively. cb2bib is an interesting program, too, although the user interface is unusual. For future KBibTeX versions, I plan to contact cb2bib's maintainer to see how it can be integrated into KBibTeX.

<b>Luca:</b> Thanks for taking the time to talk to us and good luck.

<h2>What can the KDE Science Team do for you?</h2>

You'll never find out unless you ask...

In addition to writing articles for the Dot, you'll find KDE scientists in the <a href="http://forum.kde.org/viewforum.php?f=199">KDE Science forum</a> established for discussing specific KDE applications for science, the use of KDE Workspaces for science, and promotion of KDE software and our community among scientists. If you would like to help raise awareness of KDE's science applications, join our <a href="https://mail.kde.org/mailman/listinfo/kde-science">KDE-science mailing list</a>.

The mailing list is also a great place for application teams to let us know about events they will be attending and to ask for feedback and help - so if you need help promoting your application, want advice on its direction or just want some help at an event, <a href="https://mail.kde.org/mailman/listinfo/kde-science">please ask</a>.