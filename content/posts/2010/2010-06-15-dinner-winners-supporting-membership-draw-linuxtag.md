---
title: "Dinner With Winners of Supporting Membership Draw at LinuxTag"
date:    2010-06-15
authors:
  - "jospoortvliet"
slug:    dinner-winners-supporting-membership-draw-linuxtag
comments:
  - subject: "How many?"
    date: 2010-06-17
    body: "It would be interesting to know how many members the programme got."
    author: "ingwa"
---
At LinuxTag, everyone who <a href="http://jointhegame.kde.org">Joined the Game</a> as a new Supporting Member on Wednesday or Thursday gained the chance to win a place at the Join the Game dinner. So on Thursday evening, four lucky winners were drawn from the new members and invited to the Funkturm (Berlin Radio Tower) Restaurant to dine with some KDE celebrities.
<!--break-->
<div style="float:right; padding: 1ex; margin: 1ex; border: none;"><a href="http://dot.kde.org/sites/dot.kde.org/files/tower.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/tower_small.PNG" width="192" height="256"/></a></div>The four lucky supporting members were:
<ul>
<li><b>Thomas Fricke</b>, long time KDE software user and FOSS consultant working on "Android, Java and Server stuff"</li>
<li><b>Yvonne Kapler</b>, Kubuntu volunteer</li>
<li><b>Marcus Asshauer</b>, Kubuntu fanatic</li>
<li><b>Ingo Ebel</b> from Radio Tux, Germany's biggest Linux Podcast</li>
</ul>

The winners joined well known members of KDE and the wider free software community at the dinner: <b>Georg Greve</b> (who initiated the <a href="http://fsfe.org">FSFE</a>), <b>Paul Adams</b> (our statistics dude), <b>Frank Karlitschek</b> (vice-president of KDE e.V., initiater of the Social Desktop and OwnCloud initiatives), <b>Jos Poortvliet</b> (that would be your humble writer & KDE marketing dude), <b>Matthias Ettrich</b> (started a few little FOSS things such as <a href="http://en.wikipedia.org/wiki/LyX">LyX</a> and KDE), <b>Torsten Thelke</b> (intern at our Berlin office) and <b>Till Adam</b> (who's just cool).

The dinner started with a short "who's who" and was otherwise full of food and conversation. Afterwards a little over half of us went back to our hotels or home (in the case of Torsten), the others spent a few more hours drinking beer and continuing some discussions about the Free Desktop.

As has been described before, the <a href="http://jointhegame.kde.org">Join the Game</a> campaign aims to close the gap between the wider KDE community and KDE e.V. while providing the latter also with a more predictable source of funding and greater financial independence from large sponsors. Many people have signed up already, however we certainly hope for more supporters still to join. So if you want to support the KDE community and can spare the money,  <a href="http://jointhegame.kde.org">Join the Game</a>!