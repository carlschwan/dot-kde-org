---
title: "Ever wanted your ownCloud?"
date:    2010-07-20
authors:
  - "Stuart Jarvis"
slug:    ever-wanted-your-owncloud
comments:
  - subject: "compared to dropbox"
    date: 2010-07-20
    body: "There was the question how it compares to facebook, google docs etc, but what i'm really interested in is, how is it compared to dropbox? can i have a folder that is synced with the server?"
    author: "Asraniel"
  - subject: "WebOdf is not KOffice"
    date: 2010-07-20
    body: "Actually WebOdf has nothing to do with KOffice.  It's just a couple of the same people involved in it."
    author: "ingwa"
  - subject: "Re: compared to dropbox"
    date: 2010-07-20
    body: "Not yet, but work is being done on a syncing client that would give it dropbox-like abilities."
    author: "Icewind"
  - subject: "Ok, thanks for clarifying"
    date: 2010-07-21
    body: "Ok, thanks for clarifying :-)\r\n\r\nRelated to that, I should point out that this was a face to face interview at Akademy, written up from my notes. Frank has had a quick check to see that it more or less matches what he said, but there are likely some other subtleties in the interpretation of his answers where I may not have been entirely accurate."
    author: "Stuart Jarvis"
  - subject: "Cloud computing"
    date: 2010-07-21
    body: "ownCloud is not about \"cloud computing\" - see http://en.wikipedia.org/wiki/Cloud_computing"
    author: "ivan"
  - subject: "Good point"
    date: 2010-07-22
    body: "This is why I love writing stuff for the Dot :-) (really)\r\n\r\nYou're right of course. It's more an alternative to cloud computing that brings some of the benefits and avoids most of the disadvantages."
    author: "Stuart Jarvis"
---
Akademy is a great time to meet people and understand some of the exciting new projects and buzzwords in KDE. One project that has been generating a lot of interest recently is <a href="http://owncloud.org/">ownCloud</a>, the KDE cloud computing project launched by Frank Karlitschek. We caught up with Frank to understand ownCloud better, find out about the current status, and plans for the future.
<!--break-->
<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/owncloud.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/owncloud_0.png" /></a><br />ownCloud's web interface</div><b>Hi Frank, thanks for taking the time to talk to us. So, ownCloud had its 1.0 release a few weeks ago - what's the current status?</b>
At the moment ownCloud offers a storage server so that you can access your files from anywhere. We use WebDAV and SSL so you can securely access files from any computer (Linux, Windows, Mac). There is also a web interface. You can easily back up everything on a second server to keep your data safe too.  If you use Linux with KDE software or Gnome, then you can use KIO or FUSE to make accessing files on your ownCloud server completely seamless. We are using <a href="http://freedesktop.org/wiki/Specifications/open-collaboration-services">Open Collaboration Services (OCS)</a>, the freedesktop.org standard, to bring notifications about activity in your ownCloud right into your KDE workspace - you just have to go to KDE System Settings and add ownCloud as an information provider.

<b>You have talked about the need for users to control their own data and how it is important to use Free Software in the cloud. How does ownCloud relate to the likes of Facebook, Google Docs and Diaspora?</b>
Facebook and Diaspora are not really ownCloud interests at the moment, although perhaps someone might use ownCloud as a base for social networking tools in the future. Our real focus is the user's own data, so we offer a place to store your files, like Google Docs. We are also working on online editing of documents. This is not ready yet, but we are working with KOffice on webODF - an online editor so that you can edit your documents stored in ownCloud either on your PC, with KOffice, or via a web interface.

<b>So is ownCloud web based or desktop based?</b>
Both :-) KDE has some great desktop applications. When they are available, they are nicer to use than any web interface. However, sometimes you may need to use another PC, say your friend's, partner's or in an internet cafe, so we have a web interface option you can reach from anywhere. It goes beyond documents too - you can upload your music collection and then use your smartphone to play that track you were telling your friend about.

<b>Is ownCloud for end users or a platform for others to build on?</b>
End users, absolutely. Of course, we are only at version 1.0 so the things you can do are still quite limited. We hope that other people will want to join us to push development, but as ownCloud is free software, other people are able to use it in their projects too. As we chose <a href="http://en.wikipedia.org/wiki/Affero_General_Public_License">AGPL</a> for the license, everyone can benefit from any work that someone builds on top of ownCloud.

<b>How much interest has there been in ownCloud from KDE and the wider Free Software community?</b>
The response has been great. We have done many interviews with radio stations and magazines, and there has been lots of talk about ownCloud on blogs and microblogging networks. We had two BoFs here at Akademy in addition to my talk. So yes, lots of people are interested. Of course, we already have a collaboration with the people from KOffice to build webODF. All <a href="http://gitorious.org/owncloud">the code is on Gitorious</a> so it is easy for anyone to get involved. We always need more people - ideas too - so suggestions for use cases are just as welcome as code.

<b>What's the connection between ownCloud and the openDesktop network?</b>
There isn't one ;-) They really solve different issues. openDesktop is all about public data, sharing apps and graphics and so on, whereas ownCloud is all about your own private data. So there is not an overlap. Of course, we can share some technologies like OCS, but that is just two projects using an open standard.

<b>Is anyone providing ownCloud as a service, so that the user does not have to run their own installation?</b>
Not yet. But of course it is possible for anyone to set one up, as the software is open. It would be good to find some hosting partners in the future. However, part of the point of ownCloud is that users can host it themselves and have absolute control over their data. This has become even easier with Cornelius Schumacher <a href="http://blog.cornelius-schumacher.de/2010/07/owncloud-in-box.html">announcing ownCloud in a box</a>, just the kind of third party work that is easy with Free Software.

<b>What's next for ownCloud?</b>
Version 1.1 is expected to be released in August or September and will bring some nice additions: easy sharing of files, plugins for a web-based picture gallery and a music server. After 1.1 we will probably go straight for 2.0 which will bring in file versioning (likely using Git).

<b>Thanks very much Frank. Good luck with ownCloud and your other projects.</b>