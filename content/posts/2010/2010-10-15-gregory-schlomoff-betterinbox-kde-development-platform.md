---
title: "Gregory Schlomoff - BetterInbox & KDE Development Platform"
date:    2010-10-15
authors:
  - "steveire"
slug:    gregory-schlomoff-betterinbox-kde-development-platform
comments:
  - subject: "KDE releases KDE, built on KDE"
    date: 2010-10-15
    body: "\"I didn\u2019t understand that KDE also provided a set of libraries. To me, you took the full desktop environment or nothing.\"\r\n\r\nThis, ladies and gentlemen, is why we needed to adjust the KDE brands and give the different things different names. Of course, that alone is not enough, but it's a prerequisite for changing perceptions like this.\r\n\r\nIt will be fascinating to see what BetterInbox comes up with and how their work will help to improve the KDE Platform."
    author: "Stuart Jarvis"
  - subject: "I agree that our messaging"
    date: 2010-10-15
    body: "I agree that our messaging could still be improved, and has come a long way lately, especially with the introduction of separate release announcements.\r\n\r\nAnother thing we need is to reach out to the Qt developers who are not KDE developers and tell them what we have. From speaking to some of them at DevDays I can tell you that many of them still think KDE is a monolithic desktop environment.\r\n\r\nI'm working on it though.\r\n\r\nOf course technically we could still do much better in terms of modularization in kdelibs, but I know that many others in the KDE community want that too, especially in mobile-land."
    author: "steveire"
  - subject: "static libs and LGPL"
    date: 2010-10-15
    body: "\"like the ability to build the libraries statically.\"\r\n\r\nis he aware of the fact that LGPL does not allow a program to link libs statically  ?"
    author: "mkoller"
  - subject: "incorrect"
    date: 2010-10-15
    body: "\"is he aware of the fact that LGPL does not allow a program to link libs statically ?\"\r\n\r\nIn which version? LGPL version 2.1 does, see section 6a\r\n"
    author: "Anda Skoa"
  - subject: "Exchange?"
    date: 2010-10-15
    body: "Are there any plans to get MS exchange integration going too?"
    author: "kanwar.plaha"
  - subject: "Taming information overload with KDE"
    date: 2010-10-16
    body: "Looking forward to seeing BetterInbox enhance the capabilities of the KDE semantic desktop. Good luck!"
    author: "arkub"
---
The KDE Development Platform is an attractive base for developers of Qt applications. At its core, the KDE libraries provide job APIs for asynchronous processing, transparent network access, caching and more. The KDE PIM Development Platform also provides libraries for common transport and storage standards such as POP3, IMAP, vCard, iCal, MIME messages (email) and more. With Akonadi, the KDE 4 Development Platform is a complete framework for creating full-featured PIM applications with modern modular design, extensibility and scalability.

The Kontact PIM suite is a popular Free Software set of applications based on the KDE PIM Development platform, but it is not the only one. In the last few weeks Gregory Schlomoff has been working on an external, third party email solution based on the KDE PIM Development platform.

The Dot checked in with Gregory to see how it is going.
<!--break-->
<b>Hi Gregory, would you introduce yourself to our readers?</b>

Hi, I’m your average hardcore startup geek - passionate about programming, design, and building things. I’m 25, recently graduated from the HEC Paris Business School, born in Brazil from French parents, and currently bootstrapping a startup in Vietnam.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/BetterInboxTeam2010.jpg" /><br/> left to right - Adrien Chey, Gregory Schlomoff, Christophe Laveault<br/> Pho noodle soup just a few steps away!</div><b>How do you like Vietnam? Have you traveled in other countries too?</b>

Vietnam is the third country I've lived in, after Brazil and France, and it’s simply amazing. I came here with no expectations. It was just a cheap and sunny place on the world to do a startup. But every day I spend here, I’m amazed by how kind the Vietnamese are. You walk on the streets and people you never saw or met before smile at you and invite you over for a coffee. It’s incredible.

<b>You are currently developing an email application. Can you tell us something about it?</b>

The project is called BetterInbox. Our goal is to develop a smart email client that departs from the traditional UI paradigm. Traditional email clients are centered on items and folders. We are trying a new approach, based on projects and tasks, which better reflects the way people deal with information.

<b>What is your motivation to create BetterInbox? Are you working alone or in a team?</b>

I always wanted to start my startup. This is something that has been on my mind since 5 or 6 years ago. I’ve worked for three startups previously, and in December 2009, I decided that it was time to start my own. I had no specific idea, so I glanced through <a href="http://ycombinator.com/ideas.html">Paul Graham’s “Startup Ideas We'd Like to Fund”</a>, and reading #28 - Fixing email overload, thoughts began to flow in my mind. I spent the next 8 weeks brainstorming, testing stuff, doing some research, until I had a clear vision of what BetterInbox could be and decided to take the plunge.  I recruited two developers, Christophe and Adrien, and the three of us moved to Vietnam in June.

<b> Why go to Vietnam?</b>

I chose Vietnam by chance, because a friend of mine was already here. I was just looking for a new place in the world to discover. But going abroad actually has several advantages for a startup. One of them is price: all included, my living costs here are lower than what my rent alone would cost me in Paris.

Another great thing about going abroad is that it hedges your risk as a startup founder. Failure is the norm rather than the exception in the startup world. By traveling to a new country, you know that even if your project eventually fails, you will still have had a fantastic time discovering a new country.

<b>What do you think are the main challenges for creating modern PIM applications? Are there many libraries and frameworks available to developers?</b>

It seems quite easy, until you actually start writing some code. Implementing IMAP correctly means reading through hundreds of pages of RFCs. Cache is also very hard to get right. Many desktop IMAP implementations simply avoid the problem by fetching everything they can from the server and working locally. They treat IMAP as a sophisticated form of POP. It worked well in the past, but it simply doesn’t scale today, with the trend started by GMail to keep everything on the server. Add to that the ability to handle offline-mode, and you get an idea of the complexity of the problem.

So a standards-compliant IMAP implementation and a good 2-way cache are the central pieces of any modern PIM application. That’s hard to implement, and I can’t think of any good open-source, cross-platform libraries that provide that, aside from Akonadi and kdepimlibs.

<b>Why did you decide to use Qt as the basis for your application instead of the competition?</b>

Well, cross-platform was a requirement from the very beginning. When I started looking at the different GUI toolkits available, the choice was a no-brainer: Qt is by far the most comprehensive framework available. It adds so many features to the language that sometimes you wonder if you’re still writing C++. But I wouldn’t have been able to choose it if Nokia hadn't decided to make it available as LGPL. Thanks, Nokia!

<b>How did you discover the KDE platform? What parts of it are most interesting to your needs?</b>

I have known KDE since 2003, when I tried a Knoppix live-CD for the first time. Since then, KDE has usually provided my desktop environment of choice on Linux. But I really got interested in KDE a few weeks ago, when I started looking for a good C++ IMAP library, preferably Qt-based.

I stumbled upon a post from Kévin Ottens announcing the beginning of KIMAP <a href="http://ervin.ipsquad.net/2009/10/22/libkimap-the-genesis/">(libkimap – the genesis)</a>. The job-based approach seemed very elegant, and the LGPL license was compatible with my project, so I decided to give it a try.

<b>How do the KDE PIM libraries and Akonadi help you to develop BetterInbox? Do they provide enough features or more than you need?</b>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/akonadi.png" /><br />"Akonadi could be a great addition to BetterInbox."</div>The KDE PIM libraries are extensive, covering many needs of PIM applications. Currently, we are using KIMAP to communicate with GMail’s IMAP servers. KIMAP has a well-written and well-tested API, it’s very pleasant to use. In the future, we will probably be using KCal and KABC as well. As for the caching mechanism, we are still using our own SQLite-based system. We are considering migrating to Akonadi, but that’s certainly not easy, as our code is highly coupled to our cache.

<b>What was your impression of KDE software and the KDE community before you started your work?</b>

At first, I didn’t understand that KDE also provided a set of libraries. To me, you took the full desktop environment or nothing. And I had no idea of how helpful the KDE community could be, so I didn’t ask anyone and started hacking KIMAP to remove all the KDE dependencies.

Then I realized that Akonadi could also be a great addition to BetterInbox, so this time I asked how hard it would be to use Akonadi on a Qt-based application. It took many patient replies from Kevin Krammer, Will Stephenson, and others until I finally realized that I could use KDE's set of libraries, and that wouldn’t prevent BetterInbox from running on other platforms.

<b>Were you able to use the KDE development platform as it is packaged and delivered or have you made any modifications?</b>

I had to make a few modifications. Right now, there is no way to build only parts of the KDE libraries. For example, if all you want to use is KIMAP, you only need KMime and kdecore, but you have to build the whole kdelibs and kdepimlibs packages, with all the dependencies. That’s a LOT of code to build, it takes a few hours to compile, and at any given time, there is a very high chance that something will fail on Windows or Mac.

On top of that, there were several features that weren’t provided by the build system, like the ability to build the libraries statically. So I started repackaging the KDE libraries to get rid of everything we didn’t need. Right now, we have a private git repository with a simple configure script that allow us to build kdecore, kdeui, kio, akonadi, and most of kdepimlibs, along with minimal dependencies (Boost, SQLite, cyrus-sasl, libical, and zlib) all in one command. It took a long time to get it right, but it’s worth it. Our changes are managed as a patch branch on top of the master, and I’m committing back to the trunk the ones likely to be useful to everyone.

<b>What ways do you think the KDE libraries could be improved to be more suitable to application developers like yourself?</b>

I’d say there are probably three areas of improvement:
<ol>
<li>Branding: I guess many developers would be reluctant to use KDE libraries at first because they think this would tie their program to the KDE desktop. Some effort has to go into persuading developers that they can use the KDE libraries in their application as any other library, such as Boost or SQLite, especially if they are already using Qt.</li>
<li>Documentation: yes, this is complicated. Not many developers enjoy writing documentation as much as they enjoy coding, and great documentation probably requires a dedicated documentation team, something that is hard to get for an open-source effort. Still, there are many small things that could be done to improve what we already have. The Google rankings of the documentation pages, for example, aren’t that good. Try searching for “KIMAP ListJob” or even “KIMAP documentation” on Google. The results are not very relevant.</li>
<li>Modularity: Having to download and build 600Mb of source code just to try some module and see if it fits your needs can turn many developers away. We must find a way to make the KDE libraries more modular, so that developers that aren’t targeting a KDE workspace can pick and choose exactly what they need.</li>
</ol>

<b>Making an already large set of libraries more modular and less interdependent is a lot of work. Will you help?</b>

Of course! The thing is, I still don’t have enough vision of the KDE Platform as a whole to make helpful suggestions. But I already committed a few patches here and there, and if there really is a will from the KDE community to make the libraries more modular, I’ll definitely take part in the discussions and help with what I can.

<b>KDE prides itself on being a functional, helpful community and welcoming to new contributors. Can you feel the love?</b>

I can certainly feel the love! I wasn’t expecting such a welcoming attitude. You guys are like the Vietnamese; you invite people you never saw before to your house and make them feel like they are a family member. I love it :)

<b>Have you contributed to any other Free Software communities, or is this your first interaction with Free Software?</b>

This is my first interaction with Free Software, and it's incredibly rewarding. I was actually meaning to write a blog post about how every coder should contribute to OSS. Not necessarily for idealistic reasons, but for very selfish reasons: it just feels good to be part of it, and to work with so many talented coders.

<b>Do you use any KDE applications in your daily or working environment?</b>

No, I’m not using any KDE application at the moment. Actually, I am not using many applications at all. My main environment is Windows 7, and I spend most of my time on Qt Creator, Google Chrome, TortoiseHg, Photoshop, and Quassel IRC, plus a few other tools. And of course, BetterInbox.

<b>Thank you for your time, Gregory, and good luck with your plans for KDE and BetterInbox!</b>

Thank you to all the people that help make KDE what it is.