---
title: "KOffice Based Office Viewer Launched for Nokia N900"
date:    2010-01-21
authors:
  - "jriddell"
slug:    koffice-based-office-viewer-launched-nokia-n900
comments:
  - subject: "Great stuff"
    date: 2010-01-21
    body: "Just downloaded this on my N900, managed to look at some .docx documents. I'm sure it will come in very useful :-) I was just thinking that I might have to buy the Documents To Go app, when along comes KOffice and saves the day. It even loads quickly. Very good stuff, and it's still only an alpha!\r\n\r\nA big thanks and thumbs-up for this one!"
    author: "bralkein"
  - subject: "Symbian"
    date: 2010-01-22
    body: " So this is written in Qt isn't it ? And since Qt supports Symbian now will we see of port of this to Symbian ? Let's say an N97 port. \r\n\r\n I would demonstrate that Qt apps really are cross platform and that Nokia's plan of having Qt apps run on both Maemo and Symbian will work."
    author: "mcirsta"
  - subject: "portrait-mode and kinetic scrolling"
    date: 2010-01-22
    body: "for the correct maemo mobile feeling I miss support for portrait-mode and kinetic scrolling. For an alpha-version it feels ok. Quite fast compared to OpenOffice:\r\nhttp://www.youtube.com/watch?v=Iro0lJ9aa44\r\n\r\nBye\r\n\r\n  Thorsten"
    author: "schnebeck"
  - subject: "KDE apps are usually not pure"
    date: 2010-01-22
    body: "KDE apps are usually not pure Qt apps. Likely there are several dependencies that are hard or even impossible to provide on Symbian."
    author: "KAMiKAZOW"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/office-viewer.png" width="350" height="210" align="right" />

An alpha version of Office Viewer has been <a href="http://maemo.org/packages/view/freoffice/">uploaded to the repositories for the N900</a>.  Users of Nokia's smart phone can install the KOffice based app to view word processing documents, spreadsheets and presentation.  The application can also be used to give presentations.   "<em>This shows both how portable and lean on resources KOffice is</em>" says Inge Wallin, the marketing coordinator of KOffice, "<em>we hope and believe that this is only the first port of KOffice to other mobile devices</em>.

Maemo, the system used on the N900 and other devices, is in the process of moving to Qt.  The Office Viewer marks the start of a steady stream of Qt based applications in the future.

The Office Viewer uses a user interface that is tailored to the device and uses the touch screen extensively. Nokia has made public the source code to the adapted user interface and it is now <a href="http://websvn.kde.org/trunk/koffice/tools/f-office/">integrated into the main KOffice source code</a>.  It is hoped this project will grow as part of the rest of the KOffice community.
<!--break-->
Install instructions are available on <a href="http://www.kdedevelopers.org/node/4143">Jos' blog</a>.