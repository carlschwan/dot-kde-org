---
title: "Voting Opens for KDE Software Label Designs"
date:    2010-06-05
authors:
  - "Stuart Jarvis"
slug:    voting-opens-kde-software-label-designs
comments:
  - subject: "Entry 9"
    date: 2010-06-05
    body: "While I like Entry 9 the most, I hope the typos will be fixed in case that design wins (missing \"the\", \"Family\" and \"Platform\" not upper case)"
    author: "KAMiKAZOW"
  - subject: "I think that 1, 6 und 9 are"
    date: 2010-06-05
    body: "I think that 1, 6 und 9 are all really good, and I can\u2019t really decide. \r\n\r\nin 6 the versions with text to the right are nicer, while in 1 the versions with text below looks nicer. \r\n\r\nAnd 9 is what I would like the most to include in a professional-look program (once I get to writing one :) ). \r\n\r\nAnd for funny apps, 4 would be a really good fit. \r\n\r\nSo that\u2019s a kinda hard decision, and I\u2019d like most to use them all :)"
    author: "ArneBab"
  - subject: "They will be tweaked afterwards"
    date: 2010-06-06
    body: "Yes, minor things like that will be fixed. "
    author: "Stuart Jarvis"
  - subject: "You can vote for more than one"
    date: 2010-06-06
    body: "You can vote up more than one if you can't decide.\r\n\r\nUltimately, no one is going to stop anyone using any design they like, but to make these most useful it is good for us to have one design that we recommend so that it becomes more easily recognizable."
    author: "Stuart Jarvis"
  - subject: "Wow"
    date: 2010-06-06
    body: "What a mess, can you hire professional designers? Most of the contestants haven't proposed anything that looks even remotely modern and professional and people's votes mean ... nothing - only professional designers should have been allowed to vote. IMO only designs 9 and 10 look less or more acceptable.\r\n\r\nAnd speaking frankly the K logo <b>asks</b> to be redesigned.\r\n\r\nP.S. <b>Drupal antispam module is set up incorrectly here: my every post to kde.news is considered SPAM.</b>"
    author: "birdie"
  - subject: "Feel free to donate a few"
    date: 2010-06-06
    body: "Feel free to donate a few thousand \u20ac to let us hire some professional designers, find some who do it for free or do it yourself if you can...\r\n\r\n<i>Drupal antispam module is set up incorrectly here: my every post to kde.news is considered SPAM.</i>\r\nAnd why do you think that is... ;-)"
    author: "jospoortvliet"
  - subject: "entry 10 is the best imho,"
    date: 2010-06-08
    body: "entry 10 is the best imho, although i also like number 7"
    author: "tintifaks"
  - subject: "Can't vote ..."
    date: 2010-06-08
    body: "Hi Stuart,\r\n\r\nShould I login to vote, right ?\r\nI created an account but no activation e-mail was sent. I asked for a re-sending but I got no new mail."
    author: "sandroandrade"
  - subject: "I'll check"
    date: 2010-06-09
    body: "Yeah, you need to log in. Weird about not getting the activation email. I'll ask if the forum people have some ideas\r\n\r\nUpdate: Your account should be activated now, courtesy of your friendly forum admins :-)"
    author: "Stuart Jarvis"
  - subject: "Spambox.."
    date: 2010-06-09
    body: "It often drops in the spam box for some reason."
    author: "sayakb"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/designs.jpg" width="300" height="249"/><br />Vote for your favorite</div>As part of our our efforts to better use our brands, we <a href="http://dot.kde.org/2010/03/11/come-out-part-kde">announced a set of labels</a> for use by people creating KDE software. We chose three options: <b><i>Powered by KDE</i></b>, <b><i>Built on the KDE Platform</i></b> and <b><i>Part of the KDE Family</i></b> and <a href="http://dot.kde.org/2010/05/12/being-kde">asked for artwork</a> for badges and banners carrying these labels so that they could be used on websites or within applications.

<h3>Vote now</h3>

We've received a number of excellent designs and now it is time to pick the best. The judging panel will include four members of the KDE promotion team and <strong>you</strong>.

To have your say, cast your vote <a href="http://forum.kde.org/viewtopic.php?f=157&t=88231">over on the KDE Forums</a> by 13 June. The winning design will be unveiled on the Dot.