---
title: "Akademy 2010 Conference Program Available"
date:    2010-05-10
authors:
  - "seele"
slug:    akademy-2010-conference-program-available
comments:
  - subject: "!!!"
    date: 2010-05-10
    body: "Notice that those who look forward to beer can come as well. After all: \"and be excited about what is coming\" certainly includes beer. And wine. Coke too, if that's your thing, or orange juice. Spiced up with good company. Pizza, probably, too. And Indian food, often lots of it. I learned to appreciate Indian food at KDE meetings, and these days I regularly cook it myself!\r\n\r\nYes, the cultural improvements one makes to him/herself by going to a KDE conference. Unimaginable...\r\n\r\nWish it was time to catch the plane :D\r\n\r\nEveryone who ever has done something for KDE (be it answering a question on Twitter or create some artwork) - and everyone who ever thought about probably doing something - just go and we'll convince you to do more :D\r\n\r\nNo we won't force anyone to do something he/she doesn't want. At least, once we're done with you, you'll WANT to believe 5 fingers is 4 fingers, and we've always been at war with Oceania!"
    author: "jospoortvliet"
  - subject: "Pretty slick schedule!"
    date: 2010-05-11
    body: "Reading through the schedule proves it surely is going to become an interesting week :) Looking forward to be there!"
    author: "vdboor"
---
This summer, from the 3rd to 11th of July, the KDE community will gather in Tampere, Finland for our yearly summit. We're happy to announce that the <a href="http://akademy.kde.org/program">full program</a> for <a href="http://akademy.kde.org/">Akademy 2010</a> has been decided upon. Check it out and see what to expect!

Akademy is the annual conference of the KDE community and open to all who share an interest in what we do and have accomplished. This conference brings together artists, designers, programmers, translators, users, writers and other contributors to celebrate the achievements of the past year. Moreover, at Akademy visions for the next year are defined in mutual discussions between the participants. Of course we will also work on our technology, enjoy good company and entertainment and check out Tampere. The conference provides a great opportunity to meet likeminded people, to discuss and share ideas, learn about the latest desktop technology and be excited about what is coming.

This year's Akademy <a href="http://akademy.kde.org/program/conference">conference program</a> includes many exciting talks regarding the KDE community, development, applications, platforms, mobile computing, and cloud computing. The conference will open on day 1 with a keynote from Valtteri Halla (Nokia) who will speak about MeeGo redefining the Linux desktop landscape. On day 2, long-time KDE contributor and visionary Aaron Seigo will give the midday keynote and discuss the future of Free Software and the KDE community.

<a href="http://akademy.kde.org/user/register">Registration</a> for Akademy is also open and we urge you to register as soon as possible! Please see the <a href="http://akademy.kde.org/hotelandtravel">hotel and travel</a> page for more information about the conference venue, traveling to Tampere, Finland, and finding hotel accommodations.

Help us promote Akademy by linking the official "I'm going to Akademy 2010" badge on your website or blog:

<center><a href="http://akademy.kde.org"><img src="http://dot.kde.org/sites/dot.kde.org/files/igta2010.png"></a></center>