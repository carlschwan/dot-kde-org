---
title: "Frank Karlitschek Introduces Bretzn"
date:    2010-11-02
authors:
  - "jospoortvliet"
slug:    frank-karlitschek-introduces-bretzn
---
<div style="border: 1px solid grey; padding: 1ex; margin: 1ex; float: right;"><img src="http://dot.kde.org/sites/dot.kde.org/files/shot1_wee.png" /><br />Bretzn - simple and quick tied together</div>At the recent <a href="http://en.opensuse.org/Portal:Conference">openSUSE Conference</a>, Frank Karlitschek introduced a new KDE initiative intended to bring developers and users closer together, <em>Bretzn</em>. He admitted that they came up with the name Bretzn (a kind of German pretzel) only the day before the Saturday keynote, illustrating the speed and ease that are appropriate to the initiative.
<!--break-->
<h2>The Issue</h2>
As a developer, you want to create cool Free Software and get the result out to millions of users. Unfortunately, to get the code to users you need to do more than just the fun stuff.

After you have written your application, you have to compile and package it for all operating systems you want to support. There is a wide variety of Linux distributions, and, of course, Windows and Mac too. Once you've built and packaged your software, you have to create a web site with information about the application, along with features such as commenting and bug reporting. Promotion comes next with social networking and conferences. Then you try to get it into distributions. Once the distributions ship your application, users turn up and start giving feedback, resulting in changes to the application. A new version is released, and you have to start all over again with the boring stuff.

<h2>The ambition: introducing Bretzn</h2>
Frank wants to reduce this whole cycle to 10 minutes of work (well, except for the fun part: coding). Meet project Bretzn. The goals of this project are to:
<ol>
	<li>Make it easy for developers to release an application.</li>
	<li>Make it easy for developers to market and for users to learn about that application.</li>
	<li>Make it easy for users to install the application.</li>
</ol>

Project Bretzn consists of three parts: <strong>building</strong> the package, <strong>publishing</strong> the application, <strong>marketing</strong> to users and <strong>providing feedback</strong> to the developer. The building part consists of plugins for IDEs and integration with Build Services and using <a href="http://kde-apps.org/content/show.php/kde-obs-generator?content=121094">Lubos Lunak's OBS (openSUSE Build Service) Generator</a> to generate cross-platform build files automatically. Publishing sends the app to a variety of App Stores, and markets it with announcements on Facebook, Status.net compatible services such as Identi.ca, Twitter, an RSS feed and the <a href="http://socialdesktop.org">Social Desktop</a>. Users provide feedback by rating the application and commenting on it, by writing bug reports and sending messages to mailing lists or forums. All with one click (well, maybe a few more).

<h2>Status</h2>
<div style="border: 1px solid grey; padding: 1ex; margin: 1ex; float: right; width: 252px; text-align: center;"><a href="http://dot.kde.org/sites/dot.kde.org/files/shot5.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/shot5_wee.png" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/shot3.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/shot3_wee.png" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/meego.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/meego_wee.png" /></a><br />Some current AppStore clients, from top to bottom: KDE's GHNS interface, a Qt interface and MeeGo Garage. Click images to enlarge</div>Sebastian Kugler, Dan Leinir Turthra Jensen and Frank himself have been working on Bretzn for about three months. The plans are older, however, as are some elements such as the Open Collaboration Services (OCS) AppStore.

The API is completely open, based on the Open Collaboration Services at freedesktop.org. There are free server and client implementations that support uploading and downloading of applications, abstracting Build Services and including OCS social features. Currently the API's are used by Meego, KDE and GNOME.

There are already several clients for the <a href="http://www.open-collaboration-services.org/ocs/">Open Collaboration Services</a> AppStore:
<ul>
	<li>The openDesktop series of websites (kde-apps.org, gtk-apps.org, qt-apps.org, gnome-look.org etc)</li>
	<li>A Qt client (supporting Windows, Mac and Linux)</li>
	<li>The MeeGo Garage</li>
	<li>The KDE GHNS app</li>
	<li>A GNOME library (no GUI yet)</li>
	<li>Ovi, AppUp and more in development</li>
</ul>

Current features of the API include categories, screen shots, change logs, commenting, rating, search and update notification. Furthermore, applications can be either free or paid; payment goes directly to the developer. Not all AppStore clients include all features right now. The KDE GHNS (Get Hot New Stuff) client is probably the most complete as it has been around the longest.

Social features include providing notifications directly to the desktop using the Social Desktop API. This includes categories such as "what my friends like", "what my friends develop" and Knowledge Base integration.

The project is 3/4 complete, and the team intends to ship in December. They are working with other openSUSE developers to make a proof-of-concept openSUSE AppStore that they want to ship in the upcoming openSUSE 11.4 release.

<h2>Plans for the future</h2>
Future plans are showing commits of friends, linking to bug trackers so users can file bugs, allowing users to test newer versions so developers can gather feedback, and providing the ability to donate to developers.

The code will soon be available on Gitorious for all to see. Of course anyone interested in helping out is more than welcome. The team is looking for integration in other development environments such as Eclipse, NetBeans, Visual Studio, MonoDevelop and KDevelop. Also work on more App Stores, further social media integration and code cleanup is very welcome!