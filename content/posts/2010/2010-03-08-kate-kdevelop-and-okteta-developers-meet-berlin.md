---
title: "Kate, KDevelop and Okteta Developers Meet in Berlin"
date:    2010-03-08
authors:
  - "dhaumann"
slug:    kate-kdevelop-and-okteta-developers-meet-berlin
comments:
  - subject: "My wishes"
    date: 2010-03-09
    body: "Kate:\r\nI know people that uses gedit with kde because it\u00b4s possible to configure like textmate (me too).\r\n\r\nPlease make some changes to:\r\n\r\n-Possibility to write plugins in python (with this, the gedit community group up very fast).\r\n\r\n-Make a page or wiki where people can put their plugins.\r\nhttp://live.gnome.org/Gedit/Plugins\r\n\r\n-Support to external themes, like:\r\nhttp://live.gnome.org/GtkSourceView/StyleSchemes\r\n\r\nKdevelop:\r\n\r\n-Add support to python and django.\r\n\r\n\r\nThanks for your great work."
    author: "juliusray"
  - subject: "Wonderful work + feature request"
    date: 2010-03-09
    body: "I really love where KDevelop and Kate are going towards.\r\n\r\nAs one wish-to-look-at: could you please take care of https://bugs.kde.org/show_bug.cgi?id=126954 ? Kate still opens new files in a new window, making it a multi-document editor which opens each file in a new window. Almost ironic if you think of it. :-p\r\n\r\n(my workflow consists of leaving a file manager open, and clicking at the files I like to edit. I don't use the file-open dialog)."
    author: "vdboor"
  - subject: "Guess after kdevelop 4.0"
    date: 2010-03-09
    body: "> -Add support to python and django.\r\n\r\nFYI, I believe the KDevelop guys want to launch KDevelop 4.0 with a fixed set of languages supported, the remaining could happen after the release."
    author: "vdboor"
  - subject: "Thanks for sharing "
    date: 2010-04-21
    body: "Thanks for sharing knowledge between the KDevelop and Kate developers turned out to be very beneficial <a href=\"http://www.teendriverca.com/\">-</a> Happy to hear that you have quickly sorted out quite a few issues<a href=\"http://www.florida-firsttimedriverscourse.com/\">.</a>"
    author: "joeanderson"
---
Berlin, probably one of the most frequented KDE hacking locations in the world, saw another hack sprint from 13th to 21st of February. This time four of the KDevelop and five of the Kate developers shared a week of very productive programming. Additionally team members from Okteta and KDE on Windows joined the meeting.
<!--break-->
<h3>Attendees</h3>

Many of the attendees found time for a quick group photo:

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:558px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/meeting_1.jpg" width="558" height="528" /><br />Group photo in front of the Brandenburger Tor</div>
</div>

In the group photo are, from left to right: Alexander Dymo (KDevelop), Joseph Wenninger (Kate), Aleix Pol (KDevelop), Niko Sams (KDevelop), Bernhard Beschow (Kate), Milian Wolff (Kate & KDevelop), Christoph Cullmann (Kate), Friedrich Kossebau (Okteta), Dominik Haumann (Kate). Erlend Hamberg (Kate) and Patrick Spendrin (KDE on Windows) also attended.

As usual the meeting was great fun and additionally quite some important work on the projects could get done. As also mentioned <a href="http://dhaumann.blogspot.com/2010/02/power-of-developer-meetings.html">in
this blog</a> developer meetings are really beneficial in several ways:
<ul>
<li>Social aspect: You get to know the other developers involved in the project in real life, which is a great motivation factor. This also happens at KDE's annual conference <a href="http://akademy.kde.org/">Akademy</a>, although there are a lot more people.</li>
<li>Productivity: Since you are sitting next to each other, discussions about how to do what are very focused. It's amazing how quickly a project can evolve this way.</li>
<li>Knowledge Transfer: Since participants are experts in different areas, discussions lead to knowledge transfer. This is essential, as sometimes developers have very few free time to contributes to a project. Spreading the knowledge helps a lot to keep the project alive.</li>
<li>Steady Contributions: We are always open for new contributors. Just send a patch, get commit access and join development. Experience shows that participants of a developer meeting usually contribute for years to come.</li>
</ul>

<h3>Kate</h3>

With eight days this sprint was the longest the Kate developers ever did. A huge amount of time was spent on bug triaging (checking) and fixing. A total of <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=kate&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=CLOSED&amp;emailassigned_to1=1&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2010-02-13&amp;chfieldto=2010-02-21&amp;chfieldvalue=&amp;cmdtype=doit&amp;order=Reuse+same+sort+as+last+time&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0="> 150 bugs</a> got triaged, especially by Dominik and Christoph, who also set a new record by fixing a bug in only 56 seconds. Granted, he'd be even faster if Milian would have told him the bug number earlier ;-)

But not only were bugs triaged, they also got closed. Together with the new features a total of 159 commits got done during the sprint: many memory leaks and crashes got fixed. Moreover, besides many specific speed improvements, the overall performance was increased. JavaScript support and indenters got new features and became more stable. Internals were refactored (esp. the search by Bernhard) and the Snippet plugin got some polish by Joseph. Erlend continued work on the VI mode. By cooperating with Bernhard, the search integration should be better in the future. Immediate results are that the "search for next/previous occurrence of word under cursor" commands now work. An effort to support vim's visual block mode was also begun.

All in all one can say that the sprint results in a faster, more stable and more mature Kate. The good thing of course is that many of the speed and stability improvements were backported and hence users can take advantage of them starting with <a href="http://dot.kde.org/2010/03/02/kde-software-compilation-441-out-now">Kate 3.4.1, part of the recently released KDE Software Compilation 4.4.1</a>.

On a technical note, <a href="http://gitorious.org/kate">Kate development moved to gitorious</a>. This move is motivated by the fact that building only KTextEditor, Kate Part, KWrite and Kate is much faster and especially easier compared to building the KDE modules kdesupport, kdelibs, kdepimlibs, kdebase and kdesdk. This lowers the barrier for new developers interested in Kate development. For Kate in KDE nothing changes, though: all changes on gitorious will be merged back to the main KDE development line and vice versa. Moving upstream development to git also brought new, fresh air into the discussion about all of KDE development moving to git. So stay tuned!

<h3>KDevelop</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/Kdevelop_ruby1_0.png">
<img src="http://dot.kde.org/sites/dot.kde.org/files/Kdevelop_ruby1.png" width"350" height"283" /></a><br />KDevelop With Ruby On Rails Project</div>Even though the numbers are not so spectacular for KDevelop, the developers did not stand idly by: since a first stable release of KDevelop 4 is on the horizon, the developers concentrated on stability and polishing. Especially with the help of the Kate developers on the spot some of the more evasive bugs could be solved in the Kate Part. Overall a total of <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=kdevelop&amp;product=kdevplatform&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;resolution=FIXED&amp;emailassigned_to1=1&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2010-02-13&amp;chfieldto=2010-02-21&amp;chfieldvalue=&amp;cmdtype=doit&amp;order=Reuse+same+sort+as+last+time&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">15 Bugs got closed</a> during the sprint.

Alexander Dymo managed to decrease the memory consumption of the C++-language plugin for certain cases. He also reviewed parts of the UI together with Niko and created a list of things that will hopefully get sorted out before the launch of KDevelop 4. Finally he also put some work into the experimental Ruby language plugin, finally making it useful for himself and hopefully others who are Ruby on Rails developers.

Niko Sams put some more work into the debugger integration, which now makes it possible to <a href="http://nikosams.blogspot.com/2010/02/using-kdevelop-for-debugging-only.html">open any arbitrary binary inside the embedded debugger</a> in KDevelop. Furthermore he released the first beta version of the XDebug plugin, which makes it possible to debug PHP applications in KDevelop. Additionally, <a href="http://nikosams.blogspot.com/2010/02/css-language-support-update.html">his experimental CSS support plugin</a> saw more work. And last but not least managed Niko to greatly increase the performance of auto completion in the PHP plugin, resulting in a much nicer experience.

VCS integration got some love by Aleix Pol. Especially the still experimental Git plugin was improved and can hopefully be made stable in time for the move of KDE development to Git.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/Kdevelop_ruby5_0.png">
<img src="http://dot.kde.org/sites/dot.kde.org/files/Kdevelop_ruby5.png" width="350" height="283" /></a><br />Quick Open Rails Views</div>
Milian Wolff continued to improve the Snippet plugin for KDevelop, integrating the GHNS framework and making it generally more usable. Together with Niko he also continued to polish and improve the PHP plugin for KDevelop.

Friedrich Kossebau, while attending the sprint to work on the integration of Okteta in KDevelop, also used the occasion to get first-hand support for the initial work on a plugin for the management of strings written for the UI. These will be shown in a separate browser toolview as well as being additionally used for the auto completion, along with help on semantic markup and context tags. This plugin had a blazing start and will hopefully see more work in the upcoming months.

<h3>KDevelop 4 now in stabilization phase</h3>

In related KDevelop news, <strong>Andreas Pakulat</strong> has announced that the last Beta release of KDevelop 4.0 is now available. This release marks the end of all feature development and the beginning of the stabilization phase. Finally KDevelop 4.0 is on the last meters towards its first Platform 4 stable release. More details on the exact dates please are available in the <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/4.0_Release_Schedule">release  
schedule</a>.

In addition to new features coming from the developer meeting, the majority of changes are bugfixes, this ranges from supporting git-type diffs in the patch-review, over improvements for the launch-configurations to various crash fixes related to parsing.

You can find the source packages on the <a href="http://download.kde.org/unstable/kdevelop/3.9.99/src">KDE mirrors</a> and there are <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/compiling">instructions for compiling</a>. You can also check out a <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/KDev3_KDev4_comparison_table">comparison of KDevelop 3 and 4</a>.

<h3>Okteta</h3><div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/okteta-plugin-for-kdevelop_0.png">
<img src="http://dot.kde.org/sites/dot.kde.org/files/okteta-plugin-for-kdevelop.png" width="350" height="221" /></a><br />Okteta plug in for KDevelop</div>

With the attendance of Okteta author Friedrich Kossebau, the complete team of everybody's favorite KDE hex editor was present. He managed to properly <a href="http://frinring.wordpress.com/2010/02/16/okteta-going-for-kdevelop-and-kate/">integrate Okteta into KDevelop</a>, by creating a dedicated new KDevelop plug in. This will make it possible to view and edit the raw data of any file from inside KDevelop. The only show-stopper left is missing support for opening files in the hex-editor mode. The basic work was completed in just three days and proved the strong and flexible architecture of KDevelop as well as the possibilities enabled by the modular construction of Okteta. Despite this work Okteta will also continue to be available as a stand-alone program. Besides this exciting feature, Friedrich worked on a variety of other things and improvements, including the earlier mentioned string management plugin.

<h3>Conclusion</h3>

As usual, putting smart minds together at a KDE developer meeting turned out to be a tremendous success and the attendees unanimously concurred that this experience should be repeated. Especially sharing knowledge between the KDevelop and Kate developers turned out to be very beneficial and made it possible to quickly sort out quite a few issues and discuss fundamental design plans.