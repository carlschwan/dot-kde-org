---
title: "KMyMoney Team Announces First Platform 4 Release"
date:    2010-08-16
authors:
  - "asoliverez"
slug:    kmymoney-team-announces-first-platform-4-release
comments:
  - subject: "Congrats"
    date: 2010-08-16
    body: "Congrats to the KMyMoney team :) I know you guys worked really hard on porting to KDE Platform 4, so this is a great day for the KDE Finance group ! \r\n\r\nAnd I'm also confident on alkimia's future, so let's have fun :D\r\n\r\nCheers,\r\n\r\nGuillaume"
    author: "gdebure"
  - subject: "KMyMoney or Skrooge?"
    date: 2010-08-17
    body: "Are there different use cases for KMyMoney and Skrooge or is it just a case of personal preference for which to use? I have the impression that Skrooge is perhaps a bit simpler, KMyMoney with more powerful accounting-style options, but maybe I'm wrong ;-)\r\n\r\n(I know the apps share/will share some things and the developers cooperate, so I'm not suggesting two apps is a waste, just wondering about differences).\r\n\r\nAnd... congratulations, of course :-)"
    author: "Stuart Jarvis"
  - subject: "either one :)"
    date: 2010-08-18
    body: "You're right, they are mostly a matter of preferences, as the philosophy is a bit different. Try both, and pick your choice ! There is not much risk in choosing one then switching, because Skrooge can import and export KMyMoney files :)"
    author: "gdebure"
  - subject: "double-entry accounting..."
    date: 2010-08-18
    body: "well if you have to manage money in more than one currency, skrooge is pretty much no-go from the start. Double-entry accounting (supported by kmymoney)  is absolutely necessary in these cases.\r\nSkrooge seems simpler, more modern and kde4-ish though :)"
    author: "cygnus"
  - subject: "Agreed!"
    date: 2010-08-18
    body: "Agreed!\r\n\r\nThis is one of those times when hard work, dedication, and belief in what you are doing and working on really pays off.\r\n\r\nI can't wait to install this.\r\n\r\nJimmy"
    author: "jimmyxyz"
  - subject: "KMyMoney rocks!"
    date: 2010-08-25
    body: "Hey guys,\r\n\r\nYou all are doing an immense good work. I use KMyMoney almost everyday and I think I'd miss it a lot if I didn't have it. Besides, I want to let you know that your effort is pretty well-valued for people that like me use such a great program frequently.\r\n\r\nBest regards! ;-)"
    author: "Musikolo"
---
The <a href="http://kmymoney.org">KMyMoney</a> team is pleased to announce the release of the first stable version built on KDE Platform 4. With over 15 months of development, this is the starting point for a series of KMyMoney versions leveraging the stellar features offered by the new Platform.
<!--break-->
<h2>Keeping Your Financial Data Safe</h2>

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/scheduleview.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/scheduleviewsml.png" /></a><br />KMyMoney's Schedule View</div>Throughout this effort, the team's focus has been to maintain feature-parity with previous versions, maintaining and improving the high level of quality that your personal financial data deserves. This version has gone through an intensive testing process to find and fix many bugs and to improve the usability of the application.

Along the way, we managed to add some new features too:
<ul>
<li>Better documentation</li>
<li>Support for the latest version of AqBanking</li>
<li>Improved usability of the online banking features</li>
<li>KWallet integration to store online banking passwords</li>
<li>Consistency checks that run automatically before saving data, checking for a wider range of problems, automatically correcting many of them</li>
<li>Support for all operating systems supported by the KDE Platform</li>
</ul>

<h2>About KMyMoney</h2>

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/summary_0.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/summarysml.png" /></a><br />View a summary of your finances</div>You don't know where your money is going? Trying to get a hold on your household budget? Have an investment portfolio and don't know how it has performed? You have money in different currencies and it's hard to figure out how much you actually have? KMyMoney can help you; it is intended to be the easiest open source personal finance manager to use, especially for the non-technical user.

Based on tried and tested double entry accounting principles, KMyMoney ensures that your finances are maintained properly. Although many of its features can be found in other similar applications, KMyMoney strives to present an individual and unique view of your finances. Following KDE's philosophy of sensible defaults with powerful configuration options, KMyMoney offers a default configuration and templates to start managing your finances with minimal hassle, with the possibility to customize it to your liking.

If you are new to KMyMoney, you can enter your account information and keep track of your income and expenses, synchronize with your online banking and have an accurate view of your current finance situation. You can also keep an eye on your investments, tracking their prices through online quotes. When you are ready to really take hold of your finances, you can create a budget, compare it to actual expenses, and take a peek into the future with the forecast feature. This new version of KMyMoney can do all this and more.

<h2>Future Plans</h2>

For the last year, our team has been committed to releasing this version. Now, we will continue our efforts to make KMyMoney the best personal finance manager by leveraging more features provided by the KDE Platform, and by improving the integration with other applications via <a href="http://techbase.kde.org/Projects/KdeFinance/Alkimia">Alkimia</a>, the nascent framework of the KDE Finance applications group. Expect more exciting features coming your way soon!