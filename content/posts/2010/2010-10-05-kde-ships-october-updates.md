---
title: "KDE Ships October Updates"
date:    2010-10-05
authors:
  - "sebas"
slug:    kde-ships-october-updates
comments:
  - subject: "No Kubuntu-packages for Lucid :-("
    date: 2010-10-05
    body: "It is really a shame that there will be no Packages for Kubuntu Lucid:\r\n\r\nhttp://www.kubuntu.org/news/kde-sc-4.5.2\r\n\r\nApart from the face that the RC-CD does not even boot on my system (yes, bug is reported some days ago) from my experiences it takes usally 2-3 month to have a stable system after a new version of (K)ubuntu is released. I do not blame the Kubuntu team for this, because the problems are mostly Ubuntu generic problems. But then it is a really bad decision to cut Kubuntu Lucid users of from KDE-updates, especially since Lucid was meant to be a LTS version."
    author: "mark"
  - subject: "Kubuntu instructions - use wisely"
    date: 2010-10-05
    body: "For Kubuntu packages, check out Softpedia's article on how to add a repository with updated packages:\r\nhttp://news.softpedia.com/news/KDE-Software-Compilation-4-5-2-Released-159514.shtml\r\n\r\nAn important factor to remember when updating packages from new repositories is that these updates can sometimes affect stability and/or usability of the computer.  These issues are generally rare (even more so with minor updates) and are usually minor and easy to fix.  As well, if you do have a problem, it should be easy to roll-back the updates.\r\n\r\nI only bring up the point because you mention that you're using a LTS (long-term stability?) release."
    author: "vivek"
  - subject: "No Kubuntu-packages for Lucid"
    date: 2010-10-05
    body: "It takes a lot of time to package a whole KDE SC release and with our 10.10 release due this week I'd prefer to spend that time making sure 10.10 is in as good a shape as possible.\r\n\r\nOf course we're a community distribution so we'd welcome anyone helping to make backported packages.\r\n"
    author: "jriddell"
  - subject: "Re Kubuntu instructions - use wisely"
    date: 2010-10-05
    body: "Please ignore the above linked softpedia article.  It is factually incorrect.\r\n"
    author: "jriddell"
  - subject: "Hi Sebastian, my friend and I"
    date: 2010-10-06
    body: "Hi Sebastian, my friend and I have been expecting this update, thanks a lot for your fantastic work<em class=\"timeEntry_control\"><a href=\"http://www.itu-eta.com/\" id=\"clean-url\" class=\"install\">Online Casino</a></em>"
    author: "Sam"
  - subject: "Packages for 64bits"
    date: 2010-10-06
    body: "Any chance that this time, there will be fully supported 64 bits packages available?\r\n\r\nI really appreciate all the work you put in getting KDE packaged for Kubuntu! "
    author: "andresomers"
  - subject: "Lucid is, also for Kubuntu a"
    date: 2010-10-06
    body: "Lucid is, also for Kubuntu a Long Term Support version. To promote KDE, and Kubuntu, it seems important to provide backported  packages to this LTS version. Kde's version of Lucid is a quite bugged so, provide bugfixes to a LTS will be nice and strategic (a lot of users will not upgrade to 10.10 and will wait next LTS or switch to other gui)!!"
    author: "alxju"
  - subject: "Packages for 64bits"
    date: 2010-10-06
    body: "Yes, as always we have packages for i386 and amd64\r\n"
    author: "jriddell"
  - subject: "Lucid LTS cont.   :-((("
    date: 2010-10-06
    body: "I cannot believe Lucid is not going to have 4.5.2! This is a Kubuntu *LTS* release,\r\nwe are just 20% into the promised support cycle, now we are stuck at 4.5.1, dead end for KDE on Lucid?\r\n\r\nThere are many folks out there that are using Kubuntu to get real work done and \r\ndo not want to upgrade every 6 months. Lucid is a fine distro, despite having some hiccups\r\nyou can really work with it. KDE is the reason why we use Kubuntu, and cutting all Lucid users off from further enjoying improvements/fixes in 4.5+ is frustrating, disappointing at least.\r\n\r\nThere is always hype and momentum about the next *buntu release, but\r\nthis time you really hurt Kubuntu as a solid LTS distro with stable support, one that is \r\nalmost where it should be. The *buntu release strategy needs a serious overhaul, this is 'releasemania'.\r\n\r\nPlease, take the time after 10.10 is released and package 4.5.2  \r\nfor lucid, there is a reason people were choosing LTS. It is a bug fix release\r\nafter all, addressing problems many of us have in Lucid.\r\n\r\nBTW. Interesting to see blogs about (non-existent) upgrade instructions to 4.5.2 for Lucid that , seemed a no-brainer for many."
    author: "od"
  - subject: "Dolphin fixed but..."
    date: 2010-10-07
    body: "First of all thanks to the KDE team for the long awaited bug fixes. The scroll and crashing problems that Dolphin had seems to be fixed but the desktop start up is still too slow. I am using openSuse 11.3 and the operating system itself starts up much faster than the desktop. If this performance problem could be fixed then I would have a system up and running in 10 seconds."
    author: "Bobby"
  - subject: "PCLinuxOS is a good alternative with up-to-date KDE"
    date: 2010-10-07
    body: "Hi,\r\n\r\nI enjoyed Kubuntu/Ubuntu for some years, but got tired of waiting for updated software, needing to upgrade full distro every 6 months. For me, the network manager never worked on laptops.\r\n\r\nWith PCLinuxOS I found desktop performance (smooth video play!) and up-to-date software on all major software packages. You will get latest versions of major multimedia apps within 1-2 weeks.\r\n\r\nPCLinuxOS' network manager is flawless compared to Kubuntu.. (my experience)\r\n\r\nLatest example: KDE 4.5.2 were ready to be updated on PCLinuxOS's repository a few days before KDE made it public (I of course have donated money to the project, so I have access to updates a few days earlier in another repo). \r\n\r\nBy concept, PCLinuxOS is continually upgrading packages, and releasing a snapshot every once in a while to the public..\r\n\r\nI have found my Linux desktop harbour."
    author: "gstrube@gmail.com"
  - subject: "KDE4-4.5.2: mega bug in the Help Window"
    date: 2010-10-10
    body: "The Help Window of any KDE4 application displays correctly only the first page; the following pages show an error message.\r\n\r\nAs a workaround, you can select the help of an application by clicking \"Applications Manual\" on the left side of the same Help Window."
    author: "MisterY"
---
KDE has released <a href="http://www.kde.org/announcements/announce-4.5.2.php">version 4.5.2</a> of the desktop and netbook Workspaces, the applications and the software development platform. These releases are the second monthly updates to the 4.5 series and contain nothing but bugfixes, translation updates and performance improvements. This release further stabilizes the newly (in 4.5.0) introduced KSharedDataCache and fixes a number of visual glitches, seen for example in Dolphin. Another area of stabilization focus has been KWin, further improving support for desktop effects. More changes can be found in the <a href="http://www.kde.org/announcements/changelogs/changelog4_5_1to4_5_2.php">changelog</a> (which might, as usual, not be complete).

The 4.5.2 updates are making their way into your <a href="http://www.kde.org/info/4.5.2.php#binary">preferred distribution</a> as we speak, but of course you can also <a href="http://www.kde.org/info/4.5.2.php">get the source</a> from KDE directly. Happy compiling and updating!