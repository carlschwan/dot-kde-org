---
title: "First Release Candidate for SC 4.5 Ready to Go"
date:    2010-06-27
authors:
  - "jospoortvliet"
slug:    first-release-candidate-sc-45-ready-go
comments:
  - subject: "kde 4.5 goodies"
    date: 2010-06-28
    body: "I really like the \"kde control center\" i.e., \"systemsettings\" new icon layout, very neat and very well organized. kudos to the devs.\r\n\r\nThe blur effect in plasma (widgets), is just awesome, looks very nice. Feels like it should be part of kwin, toolbars for a really slick looking kde!\r\n\r\nlancelot looks good with the blur effect. perhaps kickoff could use \"full\" widget blur...\r\n\r\nThe new system tray with white icons has pleasant appearance.\r\n\r\nplease disable \"sliding-popups\" as default, at least for *kickoff* menu  which is distracting/'delaying' the text-reading and so reminiscent of windows 98. Fade effect is fast, easy to read visually  than sliding-popups (for kickoff) where text/icons are scrolled, one has to wait till the effect is over as one can not follow/read the moving text upwards.\r\n\r\nthanks and I really appreciate the good work :)"
    author: "fast_rizwaan"
  - subject: "Plasma fail :("
    date: 2010-06-28
    body: "Rapid test, as bad than beta2 :(\r\n\r\nFirst computer:\r\n- Plasma start with my panel active (i can see plasmoids launched in plasmoid adder) but not present on my screen :(\r\n- If i clear plasma config file, on fist launch, moving plasmoid is really buggy, on second launch it's working..\r\n\r\nSecond computer dual screen:\r\n- Second screen is black\r\n- Clear plasma conf\r\n- Second screen is present again\r\n- Logout\r\n- Login\r\n- Second screen is black"
    author: "gnumdk"
  - subject: "Wrong download link in the announcement"
    date: 2010-06-28
    body: "Again :)\r\n\r\nP.S. I'm not a spammer."
    author: "birdie"
  - subject: "New development request"
    date: 2010-06-28
    body: "I do not want this to sound clich\u00e9, but guys - your work is just amazing and entire community does recognise it.\r\nI have one feature request for upcomming revision thought. Could you please concider re-introducing option to have application menu centrally on top al\u00e1 MacOS?"
    author: "atarian"
  - subject: "Being worked on, afaik,"
    date: 2010-06-28
    body: "Being worked on, afaik, Ubuntu is also on this so it'll even work with Gnome apps :D"
    author: "jospoortvliet"
  - subject: "*HUGE* performance improvement!"
    date: 2010-06-28
    body: "This is really amazing! WOWWWWWWWWW\r\n\r\nIt's like using a different laptop. Kwin now is *usable* and *fast*. Finally! I was blaming the Intel drivers but it seems it was not their fault...\r\n\r\nI'm serious: THANKS guys for the hard work!\r\n\r\nI just have to wait for the Akonadi versions of the KDEPIM apps and my system will be perfect :D\r\n\r\nReally nice work guys\r\n\r\nRegards\r\n"
    author: "Damnshock"
  - subject: "good :)"
    date: 2010-06-28
    body: "Great! The performance improvements are always good news. I hope also the use of RAM will be reduced. On my laptop I have 1,2 GByte of RAM but using KMail and KTorrent together the system can also get 500 MByte of swap memory.\r\n\r\nAn Italian translation for the announcement can be found here: http://kde-it.org/2010/06/28/rilasciato-kde-software-compilation-4-5-rc1-nome-in-codice-coppu/"
    author: "giovanni"
  - subject: "been running beta 2 ever since it came out ...."
    date: 2010-06-28
    body: "now running rc1\r\n\r\nits rock solid\r\n\r\nthank you\r\n\r\n"
    author: "somekool"
  - subject: "bug report?"
    date: 2010-06-29
    body: "Can you file a bugreport and work with the Plasma developers to get this problem analysed and fixed?\r\n\r\nIt'd be good if you installed the debug packages, and also posted the output when starting plasma-desktop from a command line (kquitapp plasma-desktop && plasma-desktop).\r\nPlease also include information about your system, your plasma config files (in ~/.kde/share/config/plasma* -- possibly replace .kde with .kde4 ).\r\n\r\nThanks.."
    author: "sebas"
  - subject: "Wrong links for compiling again."
    date: 2010-06-30
    body: "On the page:\r\n\r\nhttp://www.kde.org/announcements/announce-4.5-rc1.php\r\n\r\nIt says:\r\n\r\nThe complete source code for KDE SC 4.5 RC1 may be freely downloaded. Instructions on compiling and installing KDE SC 4.5 RC1 are available from the KDE SC 4.5 RC1 Info Page.\r\n\r\nThe link points to:\r\n\r\nBinary packages\r\n\r\nMoving up the page to:\r\n\r\nBuild instructions. Build instructions are separately maintained.\r\n\r\nthere is a link which points to the instructions to build KDE-4 SVN TRUNK for development.  Not what most users want."
    author: "JRT256"
  - subject: "No changes to the beta, except\u2026"
    date: 2010-06-30
    body: "\u2026that it\u2019s a hell of a lot less resource hungry, and it doesn\u2019t crash! \r\n\r\nMany thanks for your great work! \r\n\r\nPS: If you want to test KDE betas and still use your machine for production work, the emacs daemon is a lifesaver :) "
    author: "ArneBab"
  - subject: "performance"
    date: 2010-06-30
    body: "hey, i made the same experiences. I first installed the 4.5 beta and the desktop runs much smoother on my laptop. Kudos to the developers"
    author: "mannequinZOD"
  - subject: "Screenshot :)"
    date: 2010-07-01
    body: "I just added a screenshot of my current KDE 4.5 rc1 plasma setup with german explanations how to set it that way :) (including links to nonstandard-resources). \r\n\r\n\u2192 http://draketo.de/licht/freie-software/kde/kde-45-rc1"
    author: "ArneBab"
---
In the month since the <a href="http://dot.kde.org/2010/06/09/second-beta-kde-sc-45-available-testing">second beta</a> the KDE community has fixed hundreds of bugs. Development of features has been frozen for a while now and the Software Compilation is at the point where it needs a good testing to shake out the last issues.

So if you can spare some time and can help us identify problems or (in)validate other bug reports we'd be more than happy. Moreover, if you want to check out what's coming in terms of features and improvements to our workspaces, applications and development platform, this RC is a good choice for a test run.

Read <a href="http://kde.org/announcements/announce-4.5-rc1.php">the announcement</a> for more information including links to where to get it.

And as usual the disclaimer: this release is NOT production ready and only for testing!