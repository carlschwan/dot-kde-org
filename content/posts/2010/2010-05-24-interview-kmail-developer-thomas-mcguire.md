---
title: "Interview with KMail Developer Thomas McGuire"
date:    2010-05-24
authors:
  - "giovanni"
slug:    interview-kmail-developer-thomas-mcguire
comments:
  - subject: "Funny prospect..."
    date: 2010-05-26
    body: "Exchanging PGP-encrypted messages over Facebook, world's biggest anti-privacy institution."
    author: "majewsky"
  - subject: "Yay NZ!"
    date: 2010-05-26
    body: "Nice to hear you like NZ so much, you'd be welcome to move down here anytime :-)"
    author: "odysseus"
  - subject: "Kontact -- Kdesk"
    date: 2010-05-28
    body: "Are there plans to separately release Kontact now as a product ALA KOffice? Something like KDesk or similar. With Kmail now not being the pinnacle of the KDE desktop and with all of the data stored and dished out by integration, I actually see a parallel from KOffice to KDesk / Kontact. Withdrawing these related products from SVN and having a separate organization handle them on a separate release schedule with a name change might help with quality control on the larger KDE codebase, as well as helping with integration and separation of responsibilities and expectations. KDE has lost a little focus with development spread thin, and with a lot of huge integration projects going on a lot of bugs are easier to spot in smaller projects. I wonder if such a change would open the door to Akonadi and semantic storage between KOffice and Kontact? Otherwise importance of kdepim-kdepimlibs and possibly a refining of code from and to kde and kdesk.\r\n\r\nXDG might even benefit from a cross-desktop PIM directory.\r\n\r\nI'm almost laughing over Amiga's Workbench and how times have changed for GNU/Linux and how times are changing for KDE. "
    author: "enderwiggin99"
  - subject: "Good post"
    date: 2010-06-10
    body: "That should make it clear that when it comes to job creation policy, we don't just need incremental reform, we need a revolution. The industrial revolution raised production tenfold; we need an analogous revolution for <a href=\"http://jobsfor14yearoldsguide.info/\" style=\"color:#000000; text-decoration:none\">jobs for 15 year olds</a>, this one focused on optimizing labor. New taxes and massive new stimulus spending are out of the question for political and fiscal reasons, as are massive tax cuts for business. Besides, we've tried all these and the track record shows they don't generate jobs on the scale we need."
    author: "davidzh"
---

Welcome back again to the KDE Interview series. Last time we spoke with <a href="http://dot.kde.org/2010/04/19/interview-thiago-macieira/">Thiago Macieira</a>, one of the old timers in KDE development.

Today we feature <a href="http://thomasmcguire.wordpress.com/">Thomas McGuire</a>, the <a href="http://userbase.kde.org/Kmail">KMail</a> maintainer. Italian readers may prefer the <a href="http://kde-it.org/2010/05/11/intervista-a-thomas-mcguire-di-kmail/">original interview</a>.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org//sites/dot.kde.org/files/thomas.jpeg" width="300" height="300"/><br />Thomas McGuire</div><b>Hello Thomas, can you present yourself to our readers?</b>
Hi, my name is Thomas McGuire. I am a 24 year old German (with Irish origins, if you wonder about my surname). I'm the KMail maintainer and I'm studying a combination of IT/EE at the University of Siegen.

<b>When and how did you get in contact with the KDE community?</b>
I started programming by creating small games for Windows. One of them was using the SDL library, which is cross platform. I was curious to see if my game would work on Linux, so I installed SuSE to compile it there. The desktop happened to be KDE software, and I stayed with Linux and KDE applications since then.

<b>In which part of the KDE community and in what development area(s) are you involved?</b>
I mainly focus on KMail and KDEPIM. Occasionally I also commit something to kdelibs, when it is relevant for Kmail.

<b>How did you become KMail maintainer?</b>
Well, I started working on KMail, and continued working on it. When I started, the Platform 4 port was in progress, so it was easy to get in and help with the port.

<b>Is it possible to get better integration with GMail?</b>
We hope that the move to the Akonadi architecture will also bring improved IMAP support to KMail. One improvement that is already implemented is push email (IDLE) support. GMail's IMAP is actually a bit special, like the use of labels and the "all mail" folder, I could see room for improvement there. KMail could deal with these kind of things in a nicer way.

<b>What are the difficulties to develop this kind of application? I tried many times to start coding for KMail but I get lost in all its code. What do you suggest to people that want to help with KMail/KDE PIM?</b>
Indeed, the codebase of KMail was big and scary. We're now porting KMail to the Akonadi architecture, and things got a lot better: instead of having all of the code in KMail, we now have smaller dedicated libraries and components. This code separation makes it easier to develop, and easier to find the code. If you still can't find the right place, simply ask us on IRC.

<b>What application do you use to develop KMail?</b>
I am torn between KDevelop and QtCreator. I used the KDE3 version of KDevelop for a long time, but have now switched to QtCreator. Some things in KDevelop are just better though, especially the Kate-based editor and the C++ support. I might eventually switch to KDevelop 4 again. It is good to see this kind of competition, we now have two great IDEs. For debugging, I still use GDB with the CGDB frontend, somehow the integrated debugger in IDEs never worked right for me. Git-SVN is a great tool that lets you use Git for SVN repositories like we use in KDE. I recently started using it and can recommend it to everyone, really good for productivity.

<b>What is the knowledge needed to develop an application like KMail?</b>
For developing KMail, you don't need much knowledge. When I started, I only knew C++ and a tiny bit of Qt, and picked up the rest on the way. There are plenty people in the #kontact and #akonadi channels that will help you along.

<b>How is the coding work on KMail organized?</b>
Well, it is organized just like any other KDE team, we communicate over mailing list, IRC and have regular developer meetings. There is no one who organizes who works on what, people just work on what they like. But of course we try to coordinate our efforts. Right now, we're all focused on the Akonadi port of KMail and Kontact.

<b>Any possibilities to have Facebook messages appear in KMail or does Facebook not have an API to access them?</b>
KMail 2 will be based on Akonadi (hey I think I'm repeating myself), which is able to aggregate mails and other data from different sources, with so-called resources. What we need is a Facebook resource for Akonadi. The good news is that a Facebook resource already exists in the playground module in SVN, although it is currently defunct. Right now it only supports contacts, but the Facebook API documentation indicates that there is beta support for accessing messages, so seeing Facebook messages in KMail would be possible.
It would be really cool if someone would start working on the Facebook resource again and add messages support, it should be fairly easy.

<b>I always prefer a desktop application to do things. What do you think of all these integrated web applications such as Facebook and Google Apps?</b>
Web applications certainly have their advantages, like being accessible from everywhere without the need to install software. Yet, I prefer desktop software. Take email as an example: I have multiple email accounts, and I can access them all with a unified and powerful interface in KMail. On the web, I need to visit different websites to check all my mail, and I need to be online for that, while KMail has offline support. Another reason is privacy: It is simply not possible to encrypt or sign your mails using webmail, without giving away your private key. And of course using Google services with all your data is a privacy concern in itself, this single company is in control of a big heap of data. Desktop applications still have their place, and are preferable to web applications in some cases. Web applications also have their place. I think we'll see a shift from desktop applications to applications on mobile phones in the future.

<b>What are the missing features within KDE applications you think we should have to build a strong product set?</b>
I'd really like to see Nepomuk-related features expand in the future, this has a huge potential. We need good user interfaces to search and access the data, and integration of our new technologies in many more applications.

<b>What are the KDE applications that are doing a very good job? And which ones need lots of love and help?</b>
Some of my favorite apps for productivity are Yakuake and KJots. With Yakuake, I can quickly access my terminal, which I need frequently. I have a shortcut to KJots in my panel, and enter everything that comes to my mind into KJots. For example, I keep notes about bugs or ideas about refactoring there. There are so many other great applications that I really can't list them all here, like Konversation, KMyMoney and Digikam. Kopete needs some love. The Jabber support there is buggy, especially for groupchats. In the end, I switched to a non-KDE chat client :(

<b>What’s your favourite KDE application?</b>
Apart from Kontact? I don't really have a favorite application. I like the integration between all KDE applications, the desktop as a whole feels very nice to use.

<b>What are your plans for KMail in the next KDE SC release?</b>
Right now the big thing is finishing the Akonadi port of KMail. I think I already mentioned that the Akonadi framework will bring many advantages, we can finally get rid of the old, crusted and crashy storage layer of KMail 1. KMail 2 is work in progress: it starts and can display mail, but there is still a long way to go until we have it ready for use. I am not sure if we make it in time for KDE SC 4.5, but I'd rather spent time on ensuring that KMail 2 is ready than to experience something like the KDE SC 4.0 release again. We'll see how it goes. Porting help is welcome!

<b>Are you paid to work on KDE software?</b>
Yes, I am paid to work with KDE, I work part time at KDAB, a company offering services around Qt. One of KDAB's project is the involvement in the Kolab project, which is a free software groupware solution. Kontact is the official Kolab client, and therefore KDAB works on Kontact and KDEPIM. I got hired by KDAB after working on KMail for some time, a fate  shared by many other KDEPIM hackers. My main work area right now is the so-called "enterprise35" branch of KDEPIM, which is the rock solid version used in production. It is still based on KDE3, so I'm one of the last people doing KDE3 development, along with Allen and Sergio. Anyone remember usermake? We of course make sure that each and every commit to the enterprise branch gets merged to trunk, even though that is a lot of work. There will also be an enterprise5 branch eventually, based on Akonadi. I still spent some of my free time working on KMail, see the next question :)

<b>What motivates/keeps you motivated to work on KDE software?</b>
First and foremost, programming is fun! And having a community of other like-minded people around you makes it even more fun, especially with developer meetings and with Akademy. It is also nice to see that other people use our software and that the work we do benefits them.

<b>If you could choose where you can live what place will you choose and why?</b>
There are many nice places on earth! The best thing one can do is to travel and see as many countries as possible. If I would be forced to chose, I'd pick New Zealand. I have visited NZ for several months once, and really enjoyed my stay there, lovely nature and people.

<b>Have you ever been in Italy? If so, where?</b>
I have not yet been in Italy, but would love to go there one day.

<b>Do you like reading? What was the last book you read?</b>
I am a big book fan. The last non-fiction book I read was "Why Programs Fail: A Guide to Systematic Debugging". The last fiction book was "Drop City" from T.C. Boyle, I already ordered more books from this great author.

<b>Thomas, thank you for your time and good luck with your projects!</b>