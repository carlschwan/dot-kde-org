---
title: "Camp KDE Videos Now Available, Get Them While They're Hot!"
date:    2010-03-03
authors:
  - "jospoortvliet"
slug:    camp-kde-videos-now-available-get-them-while-theyre-hot
comments:
  - subject: "Any plans to upload them"
    date: 2010-03-04
    body: "Any plans to upload them somewhere using ogv or similar? or at least without flash :( ?"
    author: "afiestas"
  - subject: "HTML5 available"
    date: 2010-03-04
    body: "The videos (at least the ones I checked) are offered as HTML5 embedded video, no flash required. Is that different for you?"
    author: "sebas"
  - subject: "HTML5 embedded video on"
    date: 2010-03-04
    body: "HTML5 embedded video on YouTube serves H.264 video files, which is a proprietary and patent-encumbered video codec. It's no better than Flash, really."
    author: "Eike Hein"
  - subject: "This account is suspended. "
    date: 2010-03-06
    body: "\"This account is suspended.\" that's all i get when i try to watch the vidio's."
    author: "Ejdesgaard"
  - subject: "OGV and RSS"
    date: 2010-03-07
    body: "Thanks for filming and uploading the presentations. Uploading them to Youtube is a good promotion for KDE but I would also love to see them uploaded in ogv and be able to use a podcast client to watch them. What about using something like blip.tv?"
    author: "zanoi"
  - subject: "Jeff tried to upload them to"
    date: 2010-03-08
    body: "Jeff tried to upload them to blip.tv (I think) but the files were simply too big."
    author: "nightrose"
  - subject: "Damn"
    date: 2010-03-10
    body: "I guess blip.tv doesnt support long HD videos yet :("
    author: "zanoi"
  - subject: "Torrents"
    date: 2010-03-10
    body: "Hy\r\n\r\nIs there any hope to get a torrent with all the videos in HD ogg ?\r\n\r\nI have a prety unreliable connection, it is too slow to whatch the presentations without it stopping every few minutes, and if I pause to let the cache grow enought, I get disconected before the end and I have to restart the download from scratch.\r\n\r\nI realy long for a .torrent, even if it takes ages to download, at least it downloads relyably.\r\n\r\nThanks"
    author: "kollum"
  - subject: "OGG Torrent"
    date: 2010-03-13
    body: "I would love to see an OGG torrent :-)"
    author: "zanoi"
  - subject: "cheap Poster Printing"
    date: 2010-03-19
    body: "<a href=\"http://www.usaprintingtrade.com/\">cheap Poster Printing</a>:Online Cheap Printing Company Offers You Poster printing, Flyer printing, Brochure and cheap postcards printing services, Banners,Cheap Business Cards, Greeting cards bookmarks printing, calendars printing, magazine, envelopes, letterhead printing etc servies "
    author: "jenniferroy"
  - subject: "Thanks for the video. Let me"
    date: 2010-03-19
    body: "Thanks for the video. Let me download them now. <a href=\"http://www.womenclothingforall.com/\" rel=\"dofollow\">women's clothing</a>"
    author: "Amila"
  - subject: "thanks"
    date: 2010-04-26
    body: "Thanks for filming and uploading the presentations. Great work!\r\n\r\n<a href=\"http://phonesexwow.com\">Jenny</a>"
    author: "Jenny"
  - subject: "good job"
    date: 2010-05-07
    body: "I have watched Youtube and listened all the talks on Amaricas eventsin San Diego. Actually I must admit that you had a great efforts on it and I got lots of useful stuff from your site. I was totally unfamiliar with many of subjects, but now I feel pretty confident in it.\r\n<a href=http://loaninterestformula.com>loan interest formula</a>"
    author: "channel01"
  - subject: "Excellent Info"
    date: 2010-05-07
    body: "Nice article and very informative from the talk by the KDE community on yearly Americas event in San Diego. Thanks for providing a useful video in this article.\r\n<a href=http://constructiongames.net/>construction games</a>"
    author: "lacome01"
  - subject: "I really love to read"
    date: 2010-05-17
    body: "I really love to read articles that have good information and ideas to share to each reader. I hope to read more from you guys and continue that good work that is really inspiring to us. <a href=\"http://www.erectz.com\">male enhancement</a>"
    author: "faraz786g"
  - subject: "Amazing! I've been in search"
    date: 2010-05-20
    body: "Amazing! I've been in search of this particular information for three nights. Thank you considerably for the article. <a href=\"http://www.cleanerreview.com\">registry cleaner reviews</a>"
    author: "swatbolish"
  - subject: "That's a nice video. Can you"
    date: 2010-05-26
    body: "That's a nice video. Can you tell me where do you get all those videos in your site? Thanks.\r\n<a href='http://www.bargainsavenue.com/Ben_Davis_Clothing_s/76.htm'> Ben Davis Shirts </a>"
    author: "bell22"
  - subject: "A little over a month ago,"
    date: 2010-05-30
    body: "A little over a month ago, the KDE community had its yearly Americas event in San Diego. In the final article we promised you all the videos of the talks and now the KDE promo channel on Youtube features a series of Camp KDE talks. Check it out!\r\n<a href='http://www.trinityreversemortgage.com'> reverse mortgages </a>"
    author: "bell22"
  - subject: "great accommodation"
    date: 2010-06-01
    body: "KDE events usually choose a hostel for primary accommodations as it provides close social interaction at relatively small expense. The hostel chosen this year was Banana Bungalow, on the beach of the Pacific Coast, with a few people staying at the nearby Best Western. The location was good for watching the waves, and reasonably cheap\r\n<a href=\"http://www.southhonda.com\">miami pre owned cars honda</a>"
    author: "ayliana"
  - subject: "Considerably, the article is"
    date: 2010-06-08
    body: "Considerably, the article is in reality the greatest on this noteworthy topic. I agree with your conclusions and will eagerly look forward to your next updates. Saying thanks will not just be sufficient, for the wonderful clarity in your writing. I will immediately grab your rss feed to stay privy of any updates. Pleasant work and much success in your business dealings! <a href=\"http://www.theblingking.co.uk/Bling_bling_bracelets/gold_plated\u00a0\">gold bracelet</a>"
    author: "faraz786g"
  - subject: "There are a lot of videos,"
    date: 2010-06-17
    body: "There are a lot of videos, great job .\r\n<a href=\"http://www.enfermedades-sexuales.com/el-granuloma-inguinal-o-donovanosis\">El Granuloma Inguinal Donovanosis</a> "
    author: "manolo33"
  - subject: "I agree with your conclusions"
    date: 2010-06-24
    body: "I agree with your conclusions and will eagerly look forward to your next updates. Saying thanks will not just be sufficient, for the wonderful clarity in your writing.\r\n\r\n<a href=\"http://www.erectz.com/\">male enhancement</a>\r\n"
    author: "alamaneek"
---
A little over a month ago, the KDE community had its <a href="http://dot.kde.org/2010/01/25/camp-kde-2010-wrapup">yearly Americas event</a> in San Diego. In the final article we promised you all the videos of the talks and now the <a href="http://www.youtube.com/user/kdepromo">KDE promo channel</a> on Youtube features <a href="http://www.youtube.com/user/kdepromo#g/u">a series of Camp KDE talks</a>. Check it out!