---
title: "KDE Attends Relaunched SoLiSC Event in Brazil"
date:    2010-11-10
authors:
  - "melissawm"
slug:    kde-attends-relaunched-solisc-event-brazil
comments:
  - subject: "Mendeley "
    date: 2010-11-11
    body: "\"He also introduced Mendeley, a Qt application (though not yet open source) for managing PDF files, particularly scientific articles.\"\r\n\r\nNot _yet_ opensource? Are there any plans to opensource it? That could be fantastic. \r\n\r\nJ.A."
    author: "jadrian"
  - subject: "Indeed. I would love to see"
    date: 2010-11-12
    body: "Indeed. I would love to see it open sourced. I tried it once to see what it offers and I really liked it. However, I am not going to lock my professional future into a closed application. I know you can export stuff, but that really isn't the same.\r\n\r\nI am not unhappy with Zotero, though. :)\r\n"
    author: "mutlu"
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/logo_solisc.png" /><br /></div>KDE recently attended <a href="http://www.solisc.org.br">SoLiSC</a> (website in Portuguese), a local free software meeting based in <a href="http://en.wikipedia.org/wiki/Florian%C3%B3polis">Florianópolis</a>, Brazil. SoLiSC had been inactive since 2005, but in 2009 the Free Software Association of Santa Catarina (<a href="http://associacao.solisc.org.br">Associação Software Livre Santa Catarina</a>) was created to revive it and succeeded in doing so in October this year. The aim of the Association is to create a permanent forum for the discussion of free software in the state of Santa Catarina.
<!--break-->
The SoLiSC motto is Free Thought, Free Knowledge, Free Software (Pensamento Livre, Conhecimento Livre, Software Livre, in Portuguese). The main goals of this group are to educate people, to encourage the use of Free and Open Source Software (FOSS), to promote economic and social development through FOSS, and to create opportunities for people who otherwise would not have access to technology. This includes supporting the SoLiSC event, the <a href="http://flisol.solisc.org.br">local edition</a> of the <a href="http://www.installfest.net/FLISOL2010/Brasil">FLISOL</a> event, Software Freedom Day and other events in the area that promote FOSS education and use.

<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/solisc02.jpg" /><br />Marlei, Melissa and Tomaz from the SoLiSC and KDE teams</div>This year, marking the 5th edition of SoLiSC, we set for ourselves the modest objective of organizing a relatively small event (thus the name SoLiSC.gz) in order to put SoLiSC in motion again without incurring too much cost for everyone involved. We were pleasantly surprised when the event turned out to be bigger than expected! It was a two-day event which took place on October 22nd and 23rd. We had about 300 people (including the speakers) showing up for the talks and hanging out with other FOSS enthusiasts and experts.

In addition to the talks, there was a user group space where attendees were able to meet each other and mingle. There was also, for the first time, a business-focused meeting that took place during the event. It was supported by the local enterprise support organization (<a href="http://www.sebrae-sc.com.br/">SEBRAE-SC</a>), and served as a bridge between local business and FOSS experts to work towards the implementation of free alternatives in small business.

In addition to the participation of local FOSS advocates, we had the pleasure of hosting talks by several Brazilian experts from other states, including well known KDE contributor Tomaz Canabrava, who talked about KDE's scientific applications. As he himself <a href="http://liveblue.wordpress.com/2010/10/28/and-solisc-is-over/">explained in a blog entry</a>, Tomaz talked about several KDE applications including Kile, Cantor and Rocs. He also introduced <a href="http://www.mendeley.com/">Mendeley</a>, a Qt application (though not yet open source) for managing PDF files, particularly scientific articles.

Tomaz's presentation was well received and it was great to see so many people interested in the KDE software and the KDE community. We hope to welcome KDE back next year to a bigger and better SoLiSC.