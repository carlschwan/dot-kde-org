---
title: "Second Beta for KDE SC 4.5 Available for Testing"
date:    2010-06-09
authors:
  - "sebas"
slug:    second-beta-kde-sc-45-available-testing
comments:
  - subject: "Wrong download link in the announcement"
    date: 2010-06-09
    body: "It's now ?url=/stable/4.4.85/\r\n\r\nand it must be\r\n\r\n?url=/unstable/4.4.85/\r\n\r\n<b>I'm not a SPAMMER!</b>"
    author: "birdie"
  - subject: "fixed"
    date: 2010-06-09
    body: "Fixed, thanks!"
    author: "sebas"
  - subject: "Blur effect"
    date: 2010-06-10
    body: "Blur effect make kde 4.5 really slow even on non transparent Windows...\r\n\r\nI'm sure it take some time to compiz dev to make blur usable... Maybe it's not a good idea to activate this by default..."
    author: "gnumdk"
  - subject: "systray icons ..."
    date: 2010-06-10
    body: "Will kmail, kopete & co systray icons be fixed before 4.5 ?\r\n\r\nNew icons looks cool but having mixed style icons looks bad ..."
    author: "gnumdk"
  - subject: "I agree and second that"
    date: 2010-06-10
    body: "I agree and second that proposal. Futhermore, I would love to see matching \"K\" main menu logo in KDE SC 4.5, current one does not fit well in new layout. Honestly, it does not match at all IMHO."
    author: "atarian"
  - subject: "Sigh. Looks like many many"
    date: 2010-06-10
    body: "Sigh. Looks like many many old bugs like https://bugs.kde.org/show_bug.cgi?id=233145 are still not fixed."
    author: "Tom Chiverton"
  - subject: "Bball is still not fixed,"
    date: 2010-06-10
    body: "Bball is still not fixed, still locks with widgets. I have been begging for it to be fixed for such a long time now but nothing :(\r\nThe good is that it seems to be very stable and faster than 4.4."
    author: "Bobby"
  - subject: "Yeah, and this really old"
    date: 2010-06-10
    body: "Yeah, and this really old bug, too: https://bugs.kde.org/show_bug.cgi?id=153973 which makes Konqueror and other KDE web connecting applications/plasmoids mostly unusable in my office environment on laptops.\r\n\r\nEdit:\r\nAnd will KHotKeys work properly with evdev Xorg input driver for keyboard? It's another bug in every KDE 4.x releases that gives me many troubles with multiseat at home. I have to disable KHotKeys to use my keyboards properly. It's making arrows unusable (not always but one time they work another don't), caps lock gets locked etc. - tested on few different keyboards, few PCs, both USB and PS2 keyboards. When KHotKeys is disabled keyboards works properly all the time on the evdev driver.\r\n"
    author: "xeros"
  - subject: "Beautiful"
    date: 2010-06-10
    body: "Installing the beta and starting KDE pops up a beautiful desktop! Amazing work! :)"
    author: "mutlu"
  - subject: "Qt"
    date: 2010-06-11
    body: "that's an issue in Qt.\r\n\r\nthe proxy issue noted in one of the replies is actually of concern, the bball issue is one of those \"there are slightly more important things to worry about\" things. particularly since \"lock\" means \"don't move\" and the bball is a play-thing that requires movement. ;)"
    author: "aseigo"
  - subject: "I can't emphasize enough that"
    date: 2010-06-11
    body: "I can't emphasize enough that you guys have done a superb job with KDE 4. I can't find any real issues apart from my red ball. I am just a big boy that needs a little play ;)\r\nWhat could I possible do to convince you guys that it's a very important bug?"
    author: "Bobby"
  - subject: "I second that "
    date: 2010-06-12
    body: "I think personaley that it's all the little things you should sort out, along with speed optimisations and with my 173xx drivers some the effects look horrible (probably Nvidias fault). If a new user comes on and their menu freezes randomly or plasma a feels like haveing a segfault every 12 times you log in and every other time you login the panels are both half black. Then the bouncy ball locking up is the last straw. I can't live without kde4, I love it. These are the most annoying things though and if we fix all the little niggles then to the end user the desktop will seem a whole lot more polished. I think one release needs to focus on polishing and getting rid of all the stuff kde4 is known for.\r\n   I haven't tried the beta because my laptop moniter (or graphics card, don't know which) has gone and now it won't get past the BIOS so can someone tell me whether kde4.5 so far is an improvement? Does it feel a lot more stable? because to me 4.3.4 felt more stable than 4.4.4 feels now.\r\n\r\nkeep up the great work anyway guys,\r\n\r\nbananaoomarang"
    author: "bananaoomarang"
  - subject: "R. GABRIEL from Bucharest/RO"
    date: 2010-06-15
    body: "Hi.\r\nPlease help me to update my KDE environment to the new 4.5 Beta 2, in my Kubuntu 10.04 Lucid.\r\nThank you very much\r\nAll the best,\r\n\r\n             GABRIEL."
    author: "phenomFX"
  - subject: "Kubuntu packages"
    date: 2010-06-17
    body: "Hi, you can usually find packages on Kubuntu's News site quickly after releases. There are also beta2 packages for you to test.\r\n\r\nhttp://www.kubuntu.org/news\r\n\r\nNote that those are not stable packages, we don't recommend to run them in a production environment."
    author: "sebas"
---
KDE today announced the immediate availability of <a href="http://kde.org/announcements/announce-4.5-beta2.php">KDE SC 4.5 Beta2</a>. Quoting the announcement:
<em>
Over the last two weeks, roughly since the first beta, 1459 new bugs have been reported, and 1643 bugs have been closed, so we're witnessing a lot of stabilization activity right now. More testing is in place, however, while the restless developers continue to create a rock-stable 4.5.0.
</em>
<br />
Well, that looks promising, but more of that testing (and helping us fix issues!) is needed. So here is another snapshot for you to grind your teeth in.<br />
The usual boilerplate applies as well: This release does not meet production quality requirements and should be regarded as testing ground.