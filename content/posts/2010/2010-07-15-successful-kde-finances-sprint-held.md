---
title: "Successful KDE Finances Sprint Held"
date:    2010-07-15
authors:
  - "gdebure"
slug:    successful-kde-finances-sprint-held
---
On April 23rd, developers from various finance-related KDE applications gathered in Eschborn, near Frankfurt, Germany for the first ever KDE Finance Sprint. The fellowship was composed of developers from <a href=http://kmymoney2.sourceforge.net>KMyMoney</a>, <a href=http://kraft.sourceforge.net/>Kraft</a> and <a href=http://skrooge.org>Skrooge</a>. This was a week after the ash cloud stopped all flights over Europe. Until the last minute, it was not clear whether those who were coming by plane would be able to make it. Fortunately, the airports opened just in time.  Read on for a report for the meeting.
<!--break-->
<h2>Day 1, Meet &amp; Greet</h2>

On Friday 23rd, after traveling from our various places, we met in SyroCon's office, located in Eschborn, facing the building that will soon become the new home of the Frankfurt Stock Exchange. Now, ain't that the perfect place for a KDE Finance Sprint?

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/d2jj87d_6dfddrs9z_b.jpg" width="320" height="240" /><br/> Eschborn</div>

We took some time to introduce ourselves and the events that brought us that day into that room, discussing that highly improbable topic: writing KDE Applications dedicated to financial matters.

Teams presented their respective applications, what they currently do, and what they might become in the future.

<h3>KMyMoney</h3>

KMyMoney is a long-time player in FOSS applications for personal finances management. Its origins go back as far as KDE 1! It is probably the most renowned application in the KDE Finance Ecosystem.

The current stable is still based on KDE 3, but the team is working full steam on porting it to KDE Platform 4 in <font color=#e20800><a href=http://extragear.kde.org id=h6xp title="KDE Extragear">KDE Extragear</a></font>. Its philosophy is to have the rigorous methodology of the <font color=#e20700><a href=http://en.wikipedia.org/wiki/Double-entry_bookkeeping_system id=c448 title="double entry system">double entry system</a></font>, while hiding its complexity from the user. It is suitable for a range of users from individuals to small business organizations.

<h3>Skrooge</h3>

In the same area of personal finances management, Skrooge can be seen as the little brother of KMyMoney. Unlike the latter, Skrooge does not rely on the double entry system, and is only suitable for individuals. The project started in 2008, based on Platform 4, and is also part of KDE Extragear.

The Skrooge philosophy is to have a simple interface while providing powerful functions such as advanced reporting or automatic processing of operations.

<h3>Kraft</h3>

Kraft plays on a different field. It is a tender and invoice management application that can be used by small businesses without the hassle of deploying a full blown <font color=#e20700><a href=http://en.wikipedia.org/wiki/Enterprise_resource_planning id=ybqt title=ERP>ERP</a></font>. Kraft allows them to create their products catalog, manage the <font color=#e20800><a href=http://userbase.kde.org/KAddressBook id=x:dn title=KAdressBook>KAddressBook</a></font> database, and administer tenders and invoices using a templating system.

Kraft's latest version is based on KDE Platform 4.

<h2>Day 2, We Have a Plan</h2>

With a clear vision of applications and their respective use cases, it was time to start producing work that could benefit the whole Ecosystem. There were two main topics that we wanted to discuss as we had built the agenda in the preceding weeks.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/d2jj87d_3fg9gt3dw_b.jpg" width="320" height="240" /><br/> Planning world domination</div>

<h3>Oxygen Icons</h3>

When KMyMoney and Skrooge started to talk in 2008, one of the first possible common tasks was requesting nicer icons from the <a href="http://www.oxygen-icons.org/">Oxygen Team</a>. As both applications were moving closer to KDE, it made sense that the visual relationship needed reinforcement. But with both teams knee deep in development, the topic advanced rather slowly.

This face to face meeting was the perfect time to pick it up again, and extend the offer to Kraft developers. So we focused on producing a list of icons that could benefit all three applications. The list can be found <a href=http://websvn.kde.org/trunk/playground/artwork/Oxygen/docs/kdefinancial-icons.ods id=luuj title=here>here</a>.

Nuno added it to his To Do list, so we are confident these icons will see the light, sooner or later.

<h3>KDE Finance Stack</h3>

The KDE Finance Stack is an idea from Thomas Baumgart and Klaas, who envisioned broader interactions between KDE Financial applications, and also between KDE Finance and the rest of KDE. We started by listing a few use cases to define more precisely what we have in mind, like this one:

<blockquote>
    Fred purchases the album of his favorite band in an online music store and pays $5.99 for it. The commitment is done in <a href=http://amarok.kde.org/>Amarok</a>. The next time Fred starts KMyMoney or Skrooge, he is notified of this payment and can put it in the right account, if he chooses.
</blockquote>

Read <a href=http://techbase.kde.org/Projects/KdeFinance/Alkimia/Usecases id=ee4v title="other Use cases">additional use cases</a>.

A good part of the discussion was about finding a name for this component. We had a short  brainstorming session where we put some keywords about this thing (exchange, money, transfer...), and came up with <b>Alkimia</b>, the Arabic source for the word <i>Alchemy</i>. We found it pretty good so we stuck with it ;).

<h2>Day 3, Focusing on Alkimia</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/d2jj87d_4c2n8swc7_b.jpg" width="320" height="240" /><br/> Formulating ideas</div>Once we had a set of use cases, we worked on a preliminary architecture for this framework. <a href="http://techbase.kde.org/Development/Tutorials/D-Bus/Introduction">DBus</a>, <a href="http://pim.kde.org/akonadi/">Akonadi</a> and <a href="http://www.sqlite.org/">SQLite</a> were discussed. An initial architecture came to light, along with a blueprint for a prototype that would allow us to testdrive the concepts. This blueprint has become the source for a Season of KDE (SoK) project that is being worked on at the moment. We also discussed more possibilities for Alkimia, including handling some tasks currently done separately by each application, such as online quotes and file import/export. Afterwards, we wrapped up the discussion. Everyone left for home, exhausted, but extremely happy with the results.

<h2>Special Thanks</h2>
This may not be the Oscars, but still we thank all those who made the event possible. That means SyroCon for supplying the conference room, coffee, beer and an internet connection; the KDE e.V. board for supporting the event financially by paying for travel expenses; and special thanks to Claudia who did a fantastic job arranging the venue, taking care of visas, and generally making everything happen smoothly. You rock!

<h2>History and Future of the Group</h2>

The KDE Finance group is fairly recent. It started with discussions about application integration between the developers of Kraft and KMyMoney. Later on, when Skrooge became known, they were invited. The discussions lingered during 2008 and early 2009. Then a mailing list was created and there was a proposal for a sprint, which was delayed until KMyMoney and Kraft were ported to Platform 4. During January and February of this year, a date for the sprint was decided, more application developers were invited into the group, and clear goals were set. The sprint and the joint work later during the SoK have strengthened the group. Today, it is a lively group.

Even as everyone continues to work hard on their own applications, the KDE Finance group is evolving into a team and results are starting to show. <a href="http://techbase.kde.org/Projects/KdeFinance/Alkimia">Alkimia</a> is making its appearance in playground, taking shape as a promising future pillar of KDE.

<img style="position: relative; left: 50%; margin-left: -320px; padding: 1ex; border: thin solid grey;" src="http://dot.kde.org/sites/dot.kde.org/files/d2jj87d_5g6txqdg9_b_0.jpg" />

<div id=p07l style=TEXT-ALIGN:center>
<i>From left to right: Cristian Onet, Thomas Richard, Guillaume de Bure (sitting), Klaas Freitag, Alvaro Soliverez (sitting), Thomas Baumgart</i>
</div>

Thomas Baumgart's company <a href=http://www.syrocon.de id=s:_c title=SyroCon>SyroCon</a> kindly hosted the event.

Arnaud Dupuis, developer of <a href=http://www.associationsubscribersmanager.org/>Assuma</a>, was also supposed to join from France, but could not make it for personal reasons.