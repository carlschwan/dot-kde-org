---
title: "KDE's New Releases Make a Splash"
date:    2010-08-14
authors:
  - "Stuart Jarvis"
slug:    kdes-new-releases-make-splash
comments:
  - subject: "Tiling"
    date: 2010-08-14
    body: "Is tiling usable already?\r\n\r\nWhile I was an avid ion user some time ago, and love the idea of tiling. I now tried tiling with kde 4.5 but, having too many problems, ended up disabling it. (I will make bug reports in a while when I get the time - I am first reporting other bugs that are more critical to me)\r\n\r\nThree main problems: 1) it's unusable with focus-follows-mouse if you're having floating windows because tiled windows tend to pop to front as soon as they gain focus, hiding floating windows. (BTW I'm having <a href=\"https://bugs.kde.org/show_bug.cgi?id=241109\">other problems</a> with focus-follows-mouse. I suppose developers tend to use another focus policy?)\r\n\r\n2) I haven't found a way to resize a tiled window. I'd expect that, when resizing a tiled window, it would resize neighbouring windows to preserve the pattern.\r\n\r\n3) Floating windows seem to pop between maximised and non-maximised state erratically. I haven't investigated this very far, as that was about when I decided I had to disable tiling altogether\r\n\r\nApart from that, I agree it's a nice release and congratulations to the developers."
    author: "tendays"
  - subject: "Voting down?"
    date: 2010-08-15
    body: "I really don't get the people (or person? no clue at which figure it starts) down rating the last comment. If not everything is shiny you should also be able to mention that, otherwise bko.org should be \"buried\" into vacancy.\r\n\r\nIf none gives feedback -- other than praising -- then the devs have a real hard task. They have to come up with new ideas and improvements all on their own.\r\n\r\nImo the rating system should be about \"punishing\" trolls -- not the nokia ones ;) -- and rewarding good posts."
    author: "mat69"
  - subject: "Voting system"
    date: 2010-08-15
    body: "Slashdot-like voting systems don't work, because people don't use them right. I brougnt this up a long time ago (on the old dot), but this system got introduced anyway.\r\nThere must be another way to fight trolls.\r\nPeople will always vote comments down, if their opinion differs from the one of the poster and that makes the whole voting-system useless."
    author: "Zapp"
  - subject: "At least I don't do that.\nIn"
    date: 2010-08-15
    body: "At least I don't do that.\r\n\r\nIn general I rarely vote at all and then >95% up. So I use these tools for positive moderation, showing what I like instead of showing what I don't like. Yet you are right I have seen many other places where this system does not really work."
    author: "mat69"
  - subject: "re-tweet"
    date: 2010-08-15
    body: "Are you sure the authors just didn't rewrite the announce? Didn't check all links, but I couldn't find any testing results."
    author: "koos"
  - subject: "It depends on you"
    date: 2010-08-15
    body: "It depends on the likes of you voting up things that you think have unfairly been voted down.\r\n\r\nIn theory, since everyone has the same chance to vote, the voting should reflect the views of the readers and they get to hide whatever they, on average, don't like for whatever reason.\r\n\r\nIMHO, criticism should not be voted down, unless off-topic or abusive. But that's just my view - I vote according to those views, but I accept other people may have different preferences."
    author: "Stuart Jarvis"
  - subject: "A lot of tech news is like that"
    date: 2010-08-15
    body: "Unfortunately, reporting by re-writing press announcements is quite common. Ryan's actually used it (Ars - you can see that from 'segphault' in the notification screenshots), but not sure about the others.\r\n\r\nThe point is really to show who has picked up on the news of the releases. There will be proper reviews out there too - perhaps someone would like to go through those and put together an article to get a better reflection of how the new versions of our software have been received?"
    author: "Stuart Jarvis"
  - subject: "Why? Because it is off topic, that's why."
    date: 2010-08-15
    body: "Very simple IMHO.\r\n\r\nBecause the comment is off topic. The article is about media attention for the 4.5 release. And what is the first comment about? Feature X that is not working for user Y. Again. How is that relevant to the topic of the article? Is it related to the question of how the KDE 4.5 release was picked up in the media?\r\n\r\nI am not saying that bugreports are not valuable; the are! But comments like this on the dot get voted down because the dot is not bugs.kde.org. A comment like this may have been on-topic with the article on the release of KDE 4.5 itself, but not here. There is a place to discuss missing features, bugs and other quality issues, and there is a place to discuss other aspects that are relevant to KDE. This is the latter. IMHO."
    author: "andresomers"
  - subject: "Off topic?"
    date: 2010-08-15
    body: "That is an article about the kde 4.5 release, and one of the key new features of this release is window tiling. That's why it didn't feel *that* off-topic (to me) to ask if people find said key feature usable/stable already, is it worth upgrading for (that was my reason for upgrading right away), etc.\r\n\r\nIOW my expectations when posting that comment was to see if people had a similar experience to mine regarding this release, I didn't post that in hope for the problem to get known/fixed quicker (I post bug reports regularly and am planning to do so for problems related to this particular feature as well).\r\n\r\nI agree on the fact that this would have been more on-topic on an article about the 4.5 release itself, but the relevant article is many days old already and I didn't expect I'd get replies there. Anyway I didn't get a response to my question here either, so I guess the right place for that discussion would have been neither dot.kde.org, nor bugs.kde.org, but forum.kde.org?"
    author: "tendays"
  - subject: "Tiling glitchs"
    date: 2010-08-15
    body: "It would be \"awesome\" to see Awesome functionality implemented in KDE SC ;-D"
    author: "martosurf"
  - subject: "Where to ask"
    date: 2010-08-16
    body: "\"That is an article about the kde 4.5 release\"\r\nWell, not really. It is an article about the media uptake of the KDE 4.5 release. Not quite the same.\r\n\r\nPersonally, I think I would have gone to IRC on #kde, or to a KDE mailinglist, since I am not really a forum-kind-of-guy. If you prefer forums, I think forum.kde.org would have been the place to ask, yes. Here, perhaps: \r\nhttp://forum.kde.org/viewforum.php?f=77 ?\r\n"
    author: "andresomers"
  - subject: "Any voting system with negative voting does that"
    date: 2010-08-18
    body: "As soon as you can vote down, you get people trying to censor stuff they don\u2019t like. \r\n\r\nThe only solution to that I know is to only allow positive votes and to set an individual threshold how many positive votes something has to have for you to notice it \u2014 with a seperate threshold for unregistered people. \r\n\r\n* If noone likes it only those who want to see everything see it. \r\n* If a few people like it, all the unregistered people see it. \r\n* Everyone can say \u201dI only want to see what many people like\u201c. \r\n\r\nBetter still would be to set a threshold per commenter. \r\n\r\nSince here you see things with too few votes as stub, the pure positive voting would be even more effective, since at first all would be only a stub and everything which some people like gets visible. \r\n\r\nTo implement that: Just cut out the bury link and show the messages only when they get at least 1 or 2 votes. We already have the mark as spam link, so organized spammers can\u2019t game the system. If need be, add a report abuse link, but i think answering an abusive poster is more efficient most of the time. "
    author: "ArneBab"
  - subject: "Let them rewrite"
    date: 2010-08-18
    body: "Let them rewrite the announce. They give KDE publicity, and that\u2019s a good thing for us :) \r\n\r\nIf every newspaper around the world just rewrote the KDE announcement, that would even better news, because it would massively increase the reach of the announcement. \r\n\r\n"
    author: "ArneBab"
  - subject: "I tested this version with"
    date: 2010-08-21
    body: "I tested this version with Opensuse based livecd.I have been using KDE 4.4.4 already, still the refinement was visible.\r\n\r\nBut using from Livecd, I observed some definite points of concern.When I tried to run in Virtualbox,K Desktop refused to come even after long time and in repeated attempts with varying changes to settings.\r\n\r\nEven otherwise, KDE 4.5 LiveCD takes considerably more time to reach usable stage than the Opensuse LiveCD based on KDE 4.4.4.Almost double the time.\r\n\r\nI logged the time and see that Login stage is achieved in exactly same time with both the LiveCDs, but after that, KDE takes time to load.\r\n\r\nAs I could not run a virtual instance at all,I could not test how the timing would change if installed on hard-disk."
    author: "peebhat@gmail.com"
---

The new software from KDE's <a href="http://dot.kde.org/2010/08/10/kde-releases-development-platform-applications-and-plasma-workspaces-450">recent 4.5 Release Day</a> has been well received by the technical media with widespread positive reviews and recognition of the focus on quality for this set of releases.

<h2>Features <em>and</em> Stability</h2>

<a href="http://arstechnica.com/open-source/reviews/2010/08/hands-on-kde-45-launches-with-tiling-new-notifications.ars">Ars Technica</a> highlights new features of KDE's Plasma workspaces with window tiling and improved notifications. The extensive article also welcomes Marble's route planning and the increasing use of dynamic languages among KDE developers with Javascript Plasma Widgets and the addition of the Python coded Kajongg to the KDE Games family.

<a href="http://www.zdnet.com/blog/hardware/kde-45-released-1723-new-features-16022-bug-fixes/9276">ZDNet</a> recognizes the combination of new features and increased polish with improved notifications and the new Activity Manager. The piece leads with impressive bug fix and new feature statistics that demonstrate the high level of KDE activity.

<h2>WebKit</h2>

UK tech website <a href="http://www.theinquirer.net/inquirer/news/1727626/kde-desktop-integrates-webkit">The Inquirer</a> leads with the news of improved WebKit integration in the KDE Platform, a feature highlight also recognized by <a href="http://www.osnews.com/story/23668/KDE_Software_Compilation_4_5_Released">OS News</a>.

<a href="http://www.h-online.com/open/news/item/KDE-SC-4-5-final-arrives-1053469.html">The H</a> describes performance improvements in the KDE Platform and also notes the improved workspace configuration in System Settings.

<a href="http://www.techworld.com.au/article/355771/kde_4_5_release_ups_stability_adds_webkit_browser">Techworld</a> also highlights the integration of WebKit and increased stability in the latest releases. In addition, it has <a href="http://www.techworld.com.au/article/356373/kde_innovation_still_brewing_amid_stable_4_5_release">an in-depth interview</a> with our very own Sebastian Kügler explaining the ongoing emphasis on innovation along with commitments to stability and quality.

The new KDE software releases even make it into the <a href="http://au.ibtimes.com/articles/42646/20100811/kde-updates-plasma-workspaces-for-desktop-and-netbook.htm">International Business Times</a>, a leading mainstream provider of international business news. The article presents improvements to and new features of the Plasma workspaces.