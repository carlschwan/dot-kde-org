---
title: "Albert Astals Cid: KDE Edu, Okular, Akademy and Life"
date:    2010-07-19
authors:
  - "giovanni"
slug:    albert-astals-cid-kde-edu-okular-akademy-and-life
comments:
  - subject: "Okular"
    date: 2010-07-19
    body: "I like Okular. It is a great application. Thanks. I face a problem with large chm files. Okular can\u2019t handle them.\r\n\r\nBy the way, does Nokia plan to use it in Meego?"
    author: "zayed"
  - subject: "Chm support"
    date: 2010-07-20
    body: "You are right about that, if you have a look at http://bugs.kde.org there are already some bugs about it. The problem is basically a design problem so it's not really straighforward to fix without some thinking so unless someone willing to spend some time working on it appears i doubt it'll be fixed anytime soon."
    author: "tsdgeos"
  - subject: "Thanks"
    date: 2010-07-20
    body: "Okular is a great application. I use it every day, it does its job very nicely and never gets in the way. Thank you very much!"
    author: "infopipe"
  - subject: "Cry me a river :P"
    date: 2010-07-20
    body: "First of all thank you for your work in Okular. It's probably the KDE application I use the most, and it's really damned good. Well done.\r\n\r\nRegarding the developers quitting because of some annoying vocal users. Sorry but that doesn't sound right. Are they new to the internet or something? Honestly, and with all due respect, sounds more like an excuse than anything else. And maybe they believe their excuse themselves, we trick our own minds all the time to make ourselves feel better. But seriously, quitting a project because of some annoying guys on the tubes?... that makes no sense. "
    author: "jadrian"
  - subject: "Why do people contribute?"
    date: 2010-07-20
    body: "You have to ask yourself why people contribute to KDE app development. Here are my guesses:\r\n\r\n1. They like coding\r\n2. They have fun working within their team\r\n3. They like to feel they are doing something positive and making a difference\r\n\r\nYou can do 1 anywhere. Many people in KDE work in several different areas, so you can change the apps you contribute to and still work with a lot of the same people (or other cool people). I'm sure GNOME is a pretty cool community to work in too... With regard to 3, if all you see is abuse then it's quite likely you're going to feel that what you are doing is not appreciated and you'd be better off concentrating on another area - or just decide that you'd like to work on another app where you can keep your head down.\r\n\r\nRead through some of the Okular bugs. Some of the things that people say to the developers there are, frankly, shocking. I wouldn't put up with it.\r\n\r\nI'm surprised we still have anyone working on Okular and thank Albert and the other devs for sticking with it. Like you, I agree it's an awesome app and I use it every day."
    author: "Stuart Jarvis"
  - subject: "I've seen the abuse, and I"
    date: 2010-07-20
    body: "I've seen the abuse, and I can't say I blame the devs in question, not only was the abuse totally unreasonable, it was also totally misdirected, i.e. they were blaming Pino for printing issues when they should have been blaming me or Qt.  This wasn't just one or two isolated incidents, its was extended and concerted abuse and we've lost good devs because if it.  We do this for the enjoyment in what little free time we have, having people constantly abusing you for your efforts does make you less inclined to do so.  There are other fun things we can do in life where people don't yell at us for giving something away for free."
    author: "odysseus"
  - subject: "jadrian: You chose the wrong side"
    date: 2010-07-20
    body: "So you are siding up with people doing abuse instead of with those being abused, bad choice.\r\n\r\nThis is an attempt to answer to jadrian, just failed to use the webpage properly."
    author: "tsdgeos"
  - subject: "True, but so what..."
    date: 2010-07-20
    body: "It's bound to happen once your userbase grows. Ask the Amarok team. Or Mr. Aaron Aseigo. Man he's a pacient dude :D\r\n\r\nIt all boils down to, \"there were some assholes on the internet\". Who cares."
    author: "jadrian"
  - subject: "Chances are any popular kde"
    date: 2010-07-20
    body: "Chances are any popular kde application will have their fair share of userbase assholes. If all they see is abuse they should learn to ignore the trolls. It's a basic skill for those with an online presence. Some decent moderation and blocking features on the KDE bug track system could be handy too - just like they made their way into the dot. \r\n\r\nLet me put it this way, if the future of amazing applications used by many thousands of users everyday can be put at stake by just a handful of assholes writting comments in the internet, this community has a big problem ahead of it. Honestly I don't think that is the case though. \r\n"
    author: "jadrian"
  - subject: "\"... because there are only"
    date: 2010-07-20
    body: "\"... because there are only two sides. And if you're not with us you're with the terrororists. Think about it jadrian... think about it.\" "
    author: "jadrian"
  - subject: "Yes, there are only two"
    date: 2010-07-21
    body: "Yes, there are only two sides. The one that receives the insult and the one that emits it. Simple."
    author: "tsdgeos"
  - subject: "right"
    date: 2010-07-21
    body: "P1: Someone insulted a developer and he kicked a puppy.\r\nP2: I don't think that's a good reason to kick a puppy.\r\nP1: So you're on the side of the the insulting user!\r\nP2: ..."
    author: "jadrian"
  - subject: "Okular, again"
    date: 2010-07-21
    body: "Albert, I, too, would like to thank you for keeping to work on Okular. It is an amazing application and part of my everyday workflow. In fact, it might very well be my favorite Linux app.\r\n\r\nCheers to you!"
    author: "mutlu"
  - subject: "tsdgeos: I think you've missed the point."
    date: 2010-07-21
    body: "jadrian isn't \"taking sides,\" or defending the trolls. He's just pointing out that trolls are a fact of life for everyone that deals with the public via the internet and that the devolopers of okular, or any project for that matter, should be aware of that. I'm sure we can all agree that those hurling the insults were out of line, but you cannot play a public role on the internet without understanding that you will have to deal with this stuff sometimes."
    author: "Pennyless"
  - subject: "Okular"
    date: 2010-07-21
    body: "I, too, greatly appreciate the work that's been done on Okular. I get to freely use this and so many other FLOSS products. How dare I tear down those who have so generously built it up. "
    author: "gregghb"
  - subject: "ODP support"
    date: 2010-07-21
    body: "For everyone interested in viewing odp, ppt or pptx in Okular I added a new generator to KOffice:\r\nhttp://slangkamp.wordpress.com/2010/07/20/odp-support-for-okular/"
    author: "slangkamp"
  - subject: "Great app"
    date: 2010-07-22
    body: "Used it today in a document heavy meeting where I was the only Linux user.  Okular handled everything without intervention, other more widely used software was having trouble with ZIP archives.\r\n\r\nAs for people suggesting devs are being softies if they can't handle the abuse, yes there's a lot of of it out there, yes it's very wearing to be on the end of it. (I think there should be an abuse filter, prior to sending, on email clients - giving a flame score and inviting the sender to consider redrafting - not that I could write it)  \r\n\r\nEveryone has a limit, might not be your limit or my limit. \r\n\r\nI get to use great software FOC, if I don't like it I could always by an iPad..., no need to abuse anyone.\r\n\r\n   "
    author: "Gerry"
  - subject: "more complicated"
    date: 2010-07-30
    body: "It should come from two sides. A handful of assholes and nobody stepping up for the developers - I can understand the devs would leave. And then you can blame the assholes but also those who didn't step in...\r\n\r\nAnyway.  Yes you can say this is the web, ppl will be negative. But if there is a flamefest any time you do something and people call you stupid and an idiot, why would you work on something if they take away the fun? I don't see how it's strange that devs would stop working on an app if their users don't thank them for what they do but call them names and behave like idiots.\r\n\r\nHaving ppl grateful for what you do is a big motivation. Having them call you names is not."
    author: "jospoortvliet"
  - subject: "Sure. But that's no reason"
    date: 2010-07-30
    body: "Sure. But that's no reason not to call the trolls a bunch of losers and idiots cuz they made the devs go away.\r\n\r\nBesides, yes it is a fact of life, so what? Does that mean the devs should put up with the abuse? If you think that, go check yourself out.\r\n\r\n\"Gosh, if you walk around in Irak, chances are you get shot. So don't complain or try to do something about it, just don't walk around there.\"\r\n\r\nSilly argument.\r\n\r\nA bunch of idiots threated the devs like shit. Now the devs don't work on Okular anymore. We all hate the idiots who basically made Okular worse than it could have been. Nothing more to it. And most certainly no blame for the developers who left. It's their choice: they decided to try and help out, in their free time, on an app they cared about. They got flamed to sinders. They had enough of the abuse and left. And that's their right."
    author: "jospoortvliet"
---
Last time in the KDE contributor interview series, <a href="http://dot.kde.org/2010/06/04/interview-stephen-kelly">we talked with the KDE developer Stephen Kelly</a> from <a href="http://pim.kde.org/">KDE PIM</a>. We've been digging around in the KDE interview vaults and found this interesting discussion we had with Albert Astals Cid on 12 May 2010. Albert is well known in KDE from his work with <a href="http://www.kde-espana.es/">KDE España</a>, as maintainer of <a href="http://okular.kde.org/">Okular</a> and the <a href="http://edu.kde.org/">KDE Edu</a> applications. The original interview in Italian is <a href="http://kde-it.org/2010/06/22/intervista-ad-albert-astals-cid-di-kde-spagna/">also available</a>.
<!--break-->
<img src="http://dot.kde.org/sites/dot.kde.org/files/albert.jpg" style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;" /><b>Hello Albert, can you present yourself to our readers?</b>
Hi, my name is Albert Astals Cid, I was born in L'Hospitalet de Llobregat, Barcelona in 1982 and lived in Barcelona for most of my live, though recently I moved to Dublin. I have a Computer Science degree by the Technical University of Catalonia and I am single at the moment. (Hi girls!)

<b>When did you first hear about KDE?</b>
Don't really remember, probably some time around 1999, the first time I remember using KDE software though is in the Mandrake 7.0 release around 2000.

<b>How do you contribute to KDE?</b><br/> I help in lots of places in KDE, I am the coordinator of the <a href="http://l10n.kde.org/">KDE localization project</a>, also maintain Okular, <a href="http://edu.kde.org/blinken/">Blinken</a> and <a href="http://edu.kde.org/kgeography/">KGeography</a>, keep an eye on programs going to <a href="http://websvn.kde.org/trunk/kdereview/">kdereview</a> and do some occasional kdelibs patches. Not strictly related to KDE software, I also maintain <a href="http://poppler.freedesktop.org/">Poppler</a>, the free PDF rendering library.

<b>Can you talk to us about Okular?</b>
Okular is KDE's multiformat document reader. It brought KPDF code into a Platform 4 app by adding a host of new supported formats. Sadly, lately we have been getting quite some abuse from a few very vocal users and that has resulted in some of our developers quitting.

<b>That is sad to hear. Is it without a maintainer now?</b>
No, I currently maintain Okular. Of course, as with every project out there it needs people helping so if you want to help in a very interesting project, you are more than welcome to join :-)

<b>Does it need new features?</b>
Well, all programs need new features, though probably what it really needs more is a bit of polishing to finish some features and fix some corner case bugs and crashes that are hitting some of our users.

<b>How is the support for the <a href="http://en.wikipedia.org/wiki/OpenDocument">Open Document Format</a> in it?</b>
The OpenDocument support is limited to some features of ODT files, if you want a full blown ODT program you should use <a href="http://www.koffice.org/kword/">KWord</a> or the other solutions out there. Some time ago it was said that <a href="http://www.koffice.org/">KOffice</a> would provide a library we could use to render ODT more correctly but that has not happened yet.

<b>What can you tell us about your involvement with the KDE-Edu team?</b>
The KDE-Edu team develops a set of applications for educational purposes targeting very small kids to grown-ups. The applications range from simple vocabulary learning games to physics simulation applications. In the next release the KDE-Edu project will focus in adding features and polish to our current application set.

<b>How much of your time do you dedicate programming for KDE?</b>
Sadly I do not program much lately and spent most of the 2 or 3 hours I dedicate daily to KDE doing coordination, helping others and answering mails.

<b>What application do you use to develop your KDE applications?</b>
I use Kate and Konsole, not forgetting KMail, Konversation and some vim.

<b>What are the most prominent features and capabilities you miss in KDE applications?</b>
KDE already provides a robust workspace you can use daily, in fact, there are lots of people that do use it daily. Of course there are always things to improve, I would sincerely appreciate someone doing a serious job on profiling KDE software so we have real figures on whether the KDE apps use more or less memory than other apps on other platforms and the same for CPU usage. There are lots of so called benchmarks over the internet but sadly they mostly do a poor job.

<b>What’s your favourite KDE application?</b>
<a href="http://konqueror.kde.org/">Konqueror!</a>

<b>What are your plans for the next set of KDE software releases?</b>
I don't really have much plans for the next release. I do want to help Luigi Toscano remove the embedded docbook XML and XSL copies from kdelibs. Another is automating the generation of translated documentation, but I am not sure if I will really have time to finish this as it seems some other things (like this interview) keep getting on my way ;-)

<b>Are you paid to work on KDE software?</b>
Not in money, but I get appreciation by some users and fellow developers and also my KDE coding skills helped me find a good job, so you can think that's somehow a way of getting money from being part of KDE :D

<b>What motivates/keeps you motivated to work on KDE software?</b>
The community is real fun to work with and it also helps me improve my skills.

<h2>About Akademy</h2>

<b>What is <a href="http://akademy.kde.org/">Akademy</a>?</b>
Akademy is the yearly meeting for KDE users and developers. It has historically always been in political Europe (the Canary islands are physically Africa) and usually is somewhere in July/August/September. This year it is at Tampere, Finland from the 3rd to 10th of July. I suggest everyone to attend this kind of event because is a cool experience. You can meet a lot of people, you can give a face to the one you met on IRC and on KDE mailing lists, you can visit a lot of wonderful cities in Europe. Try to be at (at least) one Akademy in you life ;) .

<b>What is it like to organize Akademy? How many people are required? How does it work with the proposal?</b>
I can't really answer this because never organized one, but talking to some people that organized Akademy in the past I can tell that is both a very stressful and rewarding experience :D .

<b>How is the location of Akademy chosen?</b>
The KDE e.V. board has the final say on the Akademy location but it accepts input from KDE e.V. members.

<b>What is <a href="http://www.kde-espana.es/akademy-es2010/doku.php?id=start">Akademy-es</a>?</b>
Akademy-es is the Spanish sister of the Akademy. It is also shorter, usually only running for a weekend instead a whole week like the general Akademy.

<b>How many KDE contributors are in <a href="http://www.kde-espana.es/">KDE España</a>?</b>
KDE España has currently around 20 members of which around 10 are or have been contributing code to KDE programs, the rest are translators and promotion people.

<b>Do you prefer coding or giving talks into at Akademy or another Free Software event?</b>
I am more of a coder but I also enjoy talking about what I'm passionate about so I think the answer is both :-)

<b>What Linux distribution do you use? Have you ever suggested your friends or family to use Linux and KDE software?</b>
At the moment I am using Kubuntu, and no, I do not do much proselytism about KDE software, of course I talk and give some guidance to people I know but I prefer them to freely choose what they like and not install KDE software or Linux because I told them.

<b>If you could choose where you can live what place would you choose and why?</b>
My favorite place for living is Barcelona, which is quite ironic if you realize I recently moved from Barcelona to Dublin :D I really like Barcelona because it has everything you would want, both beach and mountains are close and there is a relatively good transportation system for those of us that don't like driving and the weather is quite good too!

<b>Have you ever stayed in Italy? If so, where?</b>
I went into Venice when I was 5, but I don't really remember much of it. Two years ago I did a two week cycling tour with some friends in the north of Sardinia and I have to say that it is a really beautiful island to visit and the fact that the Italian language is very similar to Catalan helps a lot too ;-)

<b>Do you like reading? What was the last book you read?</b>
I love reading, the last book I read was “<a href="http://en.wikipedia.org/wiki/American_Gods">American Gods</a>” by Neil Gaiman and I'm currently reading “The Folklore of Discworld” by Terry Pratchett and Jacqueline Simpson.

<b>Albert, thank you for your time and good luck for your projects!</b>