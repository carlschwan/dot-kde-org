---
title: "RC3 is Out: Codename: \"Canny\""
date:    2010-07-26
authors:
  - "sebas"
slug:    rc3-out-codename-canny
comments:
  - subject: "Many bugs :("
    date: 2010-07-27
    body: "- Plasma fail on dual screen:\r\nhttps://bugs.kde.org/show_bug.cgi?id=244007\r\n\r\n- Dolphin popup conf fail:\r\nhttps://bugs.kde.org/show_bug.cgi?id=241281\r\n\r\n- Dolphin hangs when opening files:\r\nhttps://bugs.kde.org/show_bug.cgi?id=232054"
    author: "gnumdk"
  - subject: "This dual/triple bug still"
    date: 2010-07-27
    body: "This dual/triple bug still exist:\r\nhttps://bugs.kde.org/show_bug.cgi?id=156475\r\nand that is a bid older than those you link to. This bug is still keeping my at kde 3.5. Aaron has promised me that he would look at it at some point (hopefully before 3.5 is complete dead)"
    author: "beer"
  - subject: "Sigh"
    date: 2010-07-27
    body: "There are still a ton of bugs out there, from a serious information disclosure issue (screen saver not on while resumes from hibernate, then does come on) to major graphical glitches with core widgets (CPU / temp gauges show garbage). This have been around through all the other RCs, and logged in the tracker.\r\n\r\nNot fixed, despite confirms from other users. \r\nCheck https://bugs.kde.org/show_bug.cgi?id=243886 (info leak), https://bugs.kde.org/show_bug.cgi?id=236490 (which causes power button issues on Ubuntu) and things like https://bugs.kde.org/show_bug.cgi?id=219873 and https://bugs.kde.org/show_bug.cgi?id=224517 yourself !\r\nPlease everyone, get voting !\r\n\r\nNot happy; are the KDE developers even eating their own RC dog food ? These bugs are obvious and look like easy fixes to me ! If this release is meant to be about polish, why are these glaring glitches not being fixed ?"
    author: "Tom Chiverton"
  - subject: "Blur."
    date: 2010-07-27
    body: "It's me, or the blur plugin it's a lot faster in RC3 than it was in RC1? I can use blur comfortably in my GeForce 6150 powered laptop now!\r\n\r\nEdit: It's not like so. It looks like... a massive bugfixing in the \"Direct Rendering\" mode. With KDE SC 4.0 to 4.3, when I tried to use Direct Rendering, KWin CPU usage hovered all time around 40%. KDE 4.4 slashed some of that, and in KDE 4.5 Direct Rendering, for NVIDIA users, is FASTER, MUCH FASTER, than Indirect Rendering (60% X CPU usage watching Flash video on Indirect Rendering versus 7% + 18% [KWin] on Direct Rendering).\r\n\r\nSo, if you have been experiencing slowdowns with NVIDIA or simply want a faster blur, this is the one. "
    author: "Alejandro Nova"
  - subject: "Some pushback...\n> These bugs"
    date: 2010-07-27
    body: "Some pushback...\r\n\r\n> These bugs are obvious and look like easy fixes to me ! \r\nObviously if you have the _same_ machine, and _same_ configuration.\r\n\r\nThe screensaver-off-on-suspend doesn't happen for me on openSUSE for example. Actually all 4.5-RC* releases have worked really decent for me.\r\n\r\nThings which are \"obviously to see at first sight\" are not always 100% the same at every system.\r\n\r\nAt regarding https://bugs.kde.org/show_bug.cgi?id=219873 in the bug report you can read it's a simple glitch, but _really hard to fix_. Don't think it won't happen, but perhaps not within one release cycle. Bummer. Get over it. 1000+ other bugs did get fixed. We all like everything to be insanely better, but this happens gracefully in cycles of 6 months.\r\n\r\n> Not happy; are the KDE developers even eating their own RC dog food ?\r\nAgreed, not all are. This is imho where we need _different personalities_ not more management pushing \"you might use the RC\".\r\n\r\n- You've got people who get development up speed, by always being at the edge.\r\n- And there are people who don't have this desire, but like to make things perfect.\r\n\r\nKDE could use more people from the second group, and that's something we can all work on. Making KDE attractive to different types of people. :-)"
    author: "vdboor"
  - subject: "Yeah, put me in the second"
    date: 2010-07-27
    body: "Yeah, put me in the second group - I'm not going to run daily/weekly builds as I need my machine to work.\r\nBut RC should mean that - a release that is as good as it can be made (obviously, time and resource allowing).\r\nLetting simple graphical things slide makes the experience just as bad (worse ?) as the things like the power button being screwed up or the screen saver not playing well with hibernate."
    author: "Tom Chiverton"
  - subject: "Live CD"
    date: 2010-07-27
    body: "It is a pity that no Live CD are generated for the latest builds. "
    author: "vinum"
  - subject: "HTML5 video tag support?"
    date: 2010-07-27
    body: "As flash is more or less broken in konqueror with both engine kparts (rekonq too). I understood that khtml should support something, but wikimedia, youtube and vimeo all are broken with all the possible phonon configurations.\r\n \r\n4.4 was supposed to have something good so is 4.5 finally gonna have something that works? At least at the moment nothing seems to work even with the latest phonon-vlc shizzle :'(\r\n\r\nOther that that 4.5 looks awesome :) even though the activity and virtual desktop stuff is a bit confusing especially when kwin only seems to care virtual desktops with it's effects."
    author: "bliblibli"
  - subject: "Screenlock issues"
    date: 2010-07-27
    body: "Is this[1] the problem you're seeing with screen lock?  If so, it looks like it's actually just a graphical glitch rather than (much of) a security problem.\r\n\r\n[1]: http://lists.kde.org/?t=127832207900001&r=1&w=2"
    author: "ffejery"
  - subject: "kubuntu users"
    date: 2010-07-28
    body: "I'm waiting kubuntu packages ... there are news for us, Kubuntu users, that would like to test this RC3? I need to install openSUSE?"
    author: "joethefox"
  - subject: "It seems to be a bit"
    date: 2010-07-28
    body: "It seems to be a bit unstable, especially Dolphin which is crashing a lot on my system. Scrolling in Dolphin is also a big problem."
    author: "Bobby"
  - subject: "HTML5 video tag support is"
    date: 2010-07-28
    body: "HTML5 video tag support is implemented in QtWebkit."
    author: "philacorns"
  - subject: "fewer than last release"
    date: 2010-07-29
    body: "first, let me say what an awesome first comment that was! meh.\r\n\r\nthe plasma issue looks driver related (see the bug report).\r\n\r\nthe popup issue in dolphin is pretty minor, tbh. developer is aware of it and replied in the positive on the report.\r\n\r\nthe third issue has seen quite a bit of developer investigation along with commits. in fact, it could be resolved with the commit in comment #30.\r\n\r\nthat said, a huge number of bugs were fixed, in addition to new features being added. measure the whole effort."
    author: "aseigo"
  - subject: "or..."
    date: 2010-07-29
    body: "or someone who has a dual head setup could do some work on it. it's very odd that everyone sits waiting for a person without a multi-screen system to fix these kinds of bugs. :)\r\n\r\nand no, i won't be getting one in the near future as i'm planning on moving continents early 2011 and don't need more stuff to pack and ship :)\r\n\r\ni do have a couple of ideas for how to fix that issue (which is actually an annoying limitation around the dbus session and x.org sessions), ranging from hopefully not traumatic (launching a non-uniqueapp version of the shell in such situations on the secondary screens; not very pretty, but at least pretty trivial), changing the application name based on the screen name so as not to trigger the unique app problem in the first place (possibly the cleanest, but need to see if it will actually work) ...\r\n\r\nstill, that \"no multiple screens with which to test\" is a bugger, and no, i'm completely uninterested in running \"fake xinerama\" setups (not that it would help in this particular case anyways :)"
    author: "aseigo"
  - subject: "dogfooding"
    date: 2010-07-29
    body: "i run trunk on my machines for every day work. i even do presentations in front of crowds with that setup (sometimes a bit exciting! ;).\r\n\r\nthe \"need my system to work\" thing is an excuse. in the rare cases something catastrophic might happen, fall back to the installed distro packages."
    author: "aseigo"
  - subject: "on br's"
    date: 2010-07-29
    body: "\"(CPU / temp gauges show garbage)\"\r\n\r\nwhich BR# is this?\r\n\r\n\"These bugs are obvious and look like easy fixes to me !\"\r\n\r\npatches are very welcome. they can join the hundreds of commits made every day to KDE's code.\r\n\r\ni do thin, though, that saying that and then linking to https://bugs.kde.org/show_bug.cgi?id=219873 given comment #44 is a little disingenuous. there you have a Nokia Qt developer on the bug report saying he's working on it, but that it isn't trivial.\r\n\r\n\"are the KDE developers even eating their own RC dog food ?\"\r\n\r\nnot everyone runs every piece of KDE software from trunk, but many of us run primarily or even 100% trunk installations. so.. yes.\r\n\r\nyou may have noticed that some bugs are more critical than others, and so get prioritized higher. given infinite time or more people working on individual bugs, more things would get fixed yesterday. the rate of bug closure per release is pretty amazing, even if it doesn't mean every single bug ever reported gets fixed in release+1. we get to as much as we can. those who are on the receiving end of those bug fixes are pretty happy about that."
    author: "aseigo"
  - subject: "And automatic proxy settings doesn't work, too."
    date: 2010-07-29
    body: "The problem with automatic proxy settings in Konqueror doesn't work, too:\r\nhttps://bugs.kde.org/show_bug.cgi?id=153973\r\n\r\nWhy this bug has still status 'unconfirmed' while it's confirmed by at least 4 people.\r\n"
    author: "xeros"
  - subject: "I am one of the few that have"
    date: 2010-07-29
    body: "I am one of the few that have not imegrated to kde 4 from kde 3.5 and that is becouse of this bug. \r\nThe problem with fixing it myself is that I dont know what to do. That is why I have sat up a bounty on 150 doller on it (and another person has added another 50 doller so you can earn 200 doller if youfix it).\r\n"
    author: "beer"
  - subject: "Sorry"
    date: 2010-07-29
    body: "But none of this bug are fixed for me...\r\n\r\nthe plasma issue isn't driver related (see the bug report)\r\nThe popup issue is minor, i know but it's an issue\r\nThe third issue is really a problem, here on intel hardware, all is unusable, downgrading to 4.4 fix it :("
    author: "gnumdk"
  - subject: "Implemented in KHTML as well"
    date: 2010-07-29
    body: "Well HTML5 is also implemented in KHTML, though scripting of media-objects is still uncommitted. HTML5 in webkitpart doesn't work either though, because QtWebKit depends on Qt's Phonon, and refuses to acknowledge the independent Phonon typically shipped with KDE. Since the KDE Phonon works better with Linux, it is usually shipped by all distros, and thus HTML5 doesn't work in webkitpart. *sigh*"
    author: "carewolf"
  - subject: "none..."
    date: 2010-07-29
    body: "... or you just aren't aware of fixes that do address issues you've either unknowingly run into or haven't run into yet and won't because they were fixed before you did.\r\n\r\nclassic \"half full\" / \"half empty\" world view clash ;)"
    author: "aseigo"
  - subject: "oh, and..."
    date: 2010-07-29
    body: "... i've seen another flurry of bug fixing activity for issues visible in Dolphin (which may mean the fix is actually in kdelibs, e.g.) with a few new review requests coming in from Penz and Trueg as well as straight-to-svn commits. it's a WIP, but it's getting there. \r\n\r\nthe alternative, of course, would be to say \"you can't have this for another N years\" while we try to work out every bug. that doesn't work at all, though, as our dependencies (and thus our defect profile) changes over time even if we don't release, it's exponentially harder to identify issues without a large test base and if we don't release we lose users (understandably) due to not having anything recent for them to use (particularly an issue as the infrastructure beneath us moves around, e.g. HAL deprecation, network management twists, Qt improvements, etc, etc)\r\n\r\nso we are on a journey. and i think it's pretty evident that each milestone along the journey (aka a release :) has been better than the last.\r\n\r\nnot unlike the journey to 3.5 or 2.2 or 1.1.2 :)"
    author: "aseigo"
  - subject: "kwin devs"
    date: 2010-07-29
    body: "the kwin devs have been doing fantastic things, and they aren't standing still. already they are working on improvements for the January 2011 release. i'm really, really impressed with the work they are doing, even with relatively few hands. so yeah, kudos to the kwinners!"
    author: "aseigo"
  - subject: "Bug status"
    date: 2010-07-29
    body: "You should ignore the bug status. Bugs in the KDE bug tracker can start either as NEW or as UNCONFIRMED, depending on who reported it.\r\n\r\nUsually, when a KDE developer itself reports a bug, it automatically gets the NEW status. But this does not mean the bug is confirmed.\r\n\r\nA bug which cannot be confirmed to be a valid bug will usually be resolved as WORKSFORME, INVALID, UPSTREAM, or DOWNSTREAM quickly, so you can take it as a confirmation when bug stays open for a longer time.\r\n\r\nPlease don't assume that developers ignore bug reports just because they are still marked as UNCONFIRMED. If they had more time they would rather spent it in solving the bug then just pleasing you by changing the status to NEW. Sorry if this sounds harsh."
    author: "christoph"
  - subject: "I didn't know that. Thanks"
    date: 2010-07-30
    body: "I didn't know that. Thanks for clarify.\r\n\r\nI'm used to bug reporting on Launchpad where I as a user can change status to inform (K)Ubuntu developers that bug does really exist and issue is reproductable for at least few people.\r\n"
    author: "xeros"
  - subject: "I think this is a perception"
    date: 2010-07-30
    body: "I think this is a perception problem. I totally get why the bugs remain UNCONFIRMED but to me that's like a sign constantly saying \"we don't care\". No matter how hard we convince users we do, the sign is still there.\r\n\r\nWhen a bug is identified, putting it at CONFIRMED at least acknowledges the problem for the user - letting them know a dev understands their problem. :)"
    author: "vdboor"
  - subject: "Sounds good. Where can we"
    date: 2010-07-30
    body: "Sounds good. Where can we found info about that? And as you say you don't know what to do - I'm pretty sure aseigo would be more than willing to help you fix it!"
    author: "jospoortvliet"
  - subject: "KDE releases sourcecode, not"
    date: 2010-07-30
    body: "KDE releases sourcecode, not a whole distribution. However, yes, it would be nice if distro's would provide livecd's and let the users use them to test. I might be able to make that happen in the future ;-)"
    author: "jospoortvliet"
  - subject: "Please send in crash reports!"
    date: 2010-07-30
    body: "Please send in crash reports! Those are high prio and should be fixed!"
    author: "jospoortvliet"
  - subject: "that is something the"
    date: 2010-07-30
    body: "that is something the bugsquad can (and does) do. Join them if you want to help ;-)\r\n\r\n(this is also a reply the the user initially talking about this - you can help. The bugsquad work ain't hard and it makes a difference!)"
    author: "jospoortvliet"
  - subject: "Thanks for mentioning this,"
    date: 2010-07-30
    body: "Thanks for mentioning this, great idea!\r\n\r\nI've got something else planned already.. you'll see the blogs when I'm ready for it. ;)"
    author: "vdboor"
  - subject: "After talking with him at"
    date: 2010-07-30
    body: "After talking with him at Akademy it sounds like he would like to fix it \"soon\" (soon like http://www.wowwiki.com/Soon )\r\nThe problem is to complex for me to hanlde (I have tried to look into the code)"
    author: "beer"
  - subject: "Re: Please send in crash reports!"
    date: 2010-08-01
    body: "It's very sad, KDE 4.5 looks really great, but the Dolphin crashes are a showstopper for me. It crashes every few minutes and right now I have counted 25 bug reports for RC2 and RC3 when searching for \"dolphin crash\". Don't know how many there are for RC1 and earlier.\r\n\r\nA working filemanager is one of the most important applications and Dolphin has been best in class so far. Please, please fix it."
    author: "sflecken"
  - subject: "Sounds like it would be"
    date: 2010-08-02
    body: "Sounds like it would be useful to be able to simulate having multiple screens on a single screen."
    author: "yman"
  - subject: "nothing... I cannot tell you"
    date: 2010-08-03
    body: "nothing... I cannot tell you about problems with this release without compiling from sources or changing distribution... are kubuntu people your friends?"
    author: "joethefox"
  - subject: "Great"
    date: 2010-08-03
    body: "Great, I think the availability of early ISO images helps a lot with testing. Usually you find most errors even when you run the cd image from a virtual box."
    author: "vinum"
  - subject: "Delay!"
    date: 2010-08-04
    body: "I said that KDE 4.5 deserved a delay, and today, it got it. While we are in the delay, I'll ask some questions.\r\n\r\n1. Is the beta 2 of KDE PIM delayed also? I'm anxiously waiting for it because it contains the fix for a bug that actually bans me from properly testing (Akonadi mail engine doesn't work if you have huge attachments in your inbox, so, I can't download all of my mail, that's on beta 1). That bug was reported by me, of course ;). \r\n\r\n2. Can you release right now a \"RC4\" of KDE SC? It would help to test all of the last minute changes and have a solid 4.5 release."
    author: "Alejandro Nova"
  - subject: "I<3U all!"
    date: 2010-08-04
    body: "Great to see that Dolphin bug fixed! :) All in all 4.5 is going to be best free software desktop the world has yet seen :P"
    author: "bliblibli"
  - subject: "Damn"
    date: 2010-08-04
    body: "What an unfortunate mess. Hopefully it'll get sorted out soon since Chromium is not nearly as awesome as Konqueror :)"
    author: "bliblibli"
  - subject: "build server"
    date: 2010-08-07
    body: "Yeah, but we are in the age of build servers, how do you know that your source builds? I expect KDE distributors to do Tinderbox-ing. For a Live-CD you use automated scripts and make a daily build. With the daily build you can run automated gui tests."
    author: "vinum"
  - subject: "Reply: Preliminary builds of 4.5."
    date: 2010-08-08
    body: "I got some preliminary builds through KDE Unstable Red-Hat repository, and there are some interesting remarks.\r\n\r\n1. What have you done to the framerate for screen animations, direct-rendering mode? Whatever you've done, it's awesome. There're more frames running, animations get almost as smooth as with indirect rendering, and videos run better than they did... on RC3! Yes, there are PERFORMANCE INCREASES between RC3 and final. This is more than what I've expected.\r\n\r\n2. This spells the end for most Dolphin crashes, but not for all of them. Some are caused by the infamous DBus race condition, and that will have to wait until DBus 1.4.0. That's TOO long..."
    author: "Alejandro Nova"
  - subject: "works for me with nvidia"
    date: 2010-08-09
    body: "I am using 2 screens with nvidia prop. driver - the screens are in vertical orientation in a dell desktop DVI connection. \r\n\r\nAlso dual screen with dell e4300 laptop VGA out is fine.\r\n\r\nActually, I did not know such a bug existed. I have been using this dual setup for over 1 year with kde 4.x .\r\n\r\ncongrats on this new release. (if you need any more info please ask me)"
    author: "molecularbiophysics"
  - subject: "SC 4.5.0 now available with zypper"
    date: 2010-08-10
    body: "and very nice it is too, everything a bit snappier, particularly Konqi with KHTML\r\n\r\nThank you"
    author: "Gerry"
---
Just a very quick update: KDE's release team has <a href="http://kde.org/announcements/announce-4.5-rc3.php">made available</a> the last release candidate before 4.5.0 comes out next week. Please give it another whirl and <a href="http://bugs.kde.org">tell us</a> about problems with this release. Tagging will happen in the next days, so be quick doing that!