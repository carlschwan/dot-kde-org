---
title: "KDevelop 4.1 Brings Git Integration"
date:    2010-10-26
authors:
  - "Milian Wolff"
slug:    kdevelop-41-brings-git-integration
comments:
  - subject: "Cool"
    date: 2010-10-26
    body: "I really like that project import stuff.  It should make it painless post move-to-Git for KDE 4.7 development to try using KDev instead of my usual Kate + Konsole environment.  Thanks guys!"
    author: "odysseus"
  - subject: "A great IDE gets better"
    date: 2010-10-26
    body: "I am using KDevelop for several years and really like it. The kdevelop 4.x series is a big leap forward. Its understanding for C++ is superb and programming with such support is a pleasure.\r\n\r\nIn particular, I like that KDevelop's parser understands the meaning of the underlying objects. Auto-completion just works (which saves a lot of time), refactoring is extremely helpful (and a real time-saver, too) and the syntax highlighting is very well.\r\n\r\nThe only problem I had with the 4.0.x series were random crashes here and there. However, using the 4.1 betas these seemed to be solved. \r\n\r\nThanks a lot for your hard work and dedication--KDevelop rocks!"
    author: "mkrohn5"
  - subject: "Completely agree! I think"
    date: 2010-10-28
    body: "Completely agree! I think it's one of the most brilliant integration features in KDevelop I've seen! :-)\r\n\r\nI think KDevelop 4 is an awesome IDE, and something we can all be proud of! I love to see more of it coming to KDE! :D"
    author: "vdboor"
  - subject: "It's a pity that I cannot try"
    date: 2010-10-29
    body: "It's a pity that I cannot try it in KDE 4.4"
    author: "annulen"
  - subject: "4.1 supports KDE 4.4"
    date: 2010-11-07
    body: "Why? KDevelop 4.1 does not require KDE 4.5, you can use it just fine!"
    author: "Milian Wolff"
  - subject: "You're right, it works for"
    date: 2010-11-18
    body: "You're right, it works for me!\r\n\r\nWhat is the status of QMake plugin? I'm trying to compile it with KDevelop 4.1 and KDevPlatform 1.1.0, but get error about missing \"project/abstractfilemanagerplugin.h\""
    author: "annulen"
  - subject: "KDevelop compilation"
    date: 2010-11-19
    body: "I have had the same problem, I will try to find what I did."
    author: "francis77"
  - subject: "What is the status of QMake"
    date: 2010-11-21
    body: "What is the status of QMake plugin? I'm trying to compile it with KDevelop 4.1 and KDevPlatform 1.1.0, but get error about missing \"project/abstractfilemanagerplugin.h\"\r\n\r\n\r\n<a href=\"http://www.optimusbt.com/delegate_product\">AD user management</a>"
    author: "larsen"
---
Roughly half a year and over a thousand commits after the first stable release, the KDevelop hackers are proud and happy to announce the release of KDevelop 4.1, the first of hopefully many feature releases. As with the previous bugfix releases, we also make available updated versions of the KDevelop PHP plugins.

KDevelop 4.1 is more stable and polished than 4.0; we suggest everyone should update to this new version.
<!--break-->
Here are some statistics to show you how active the last months were for us:

<table style="width: 100%;">
<tr>
<th>Package</th>
<th>Commits since 4.0</th>
<th>Diffstat</th>
</tr>
<tr>
<td>KDevplatform</td>
<td>752</td>
<td>438 files changed, 14208 insertions(+), 3775 deletions(-)</td>
</tr>
<tr>
<td>KDevelop</td>
<td>371</td>
<td>227 files changed, 7342 insertions(+), 4664 deletions(-)</td>
</tr>
<tr>
<td>KDev-PHP</td>
<td>117</td>
<td>75 files changed, 2467 insertions(+), 901 deletions(-) (excluding generated files)</td>
</tr>
<tr>
<td>KDev-PHP-Docs
</td>
<td>11</td>
<td>12 files changed, 78 insertions(+), 43 deletions(-)</td>
</tr>
</table>

<h2>New Features</h2>

KDevelop 4.1 comes with a list of new plugins and feature additions, which explains the growth of the KDevplatform codebase.

<h3>Git</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kdevelop-git3.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdevelop-git3-sml.png" /></a><br />Git integration (click image for larger version)</div>In addition to the SVN and CVS integration that was already shipped with KDevelop 4.0, KDevelop 4.1 finally supports Git natively.

That means that we have support for the basic features for management of a VCS-controlled project, like moving, adding and removing files inside the project.  Additionally we integrate the basic VCS features like comparing and reviewing local changes, sending our changes back to the server, updating the local checkout and annotating files.

Additionally, there are two features that are unique to the Git support, which are Branch Management and Stash Management. The former lets the user switch branches easily, while the latter can be used to stash the current project changes in case there's something else to work on first. (For more information, check "git branch" and "git stash" help pages).

Screenshots and more information can be found on <a href="http://www.proli.net/2010/09/01/kdevelop-git-support/">Aleix Pol's website</a>. 

<h3>Patch Exporting</h3>

Whenever you're reviewing your changes, e.g. by comparing to a remote VCS server, you are now able to ask KDevelop to publish your changes using different methods like e-mail, pastebin or ReviewBoard. This can be considered the first social feature in KDevelop and we hope to see more of these in the future.

<h3>External Scripts</h3>

If you used to run scripts through an external console, KDevelop 4.1 now has a plugin just for you: The external scripts plugin lets you run arbitrary shell commands or external tools directly from inside the IDE. You can pipe editor contents into those tools and/or let your editor contents be replaced by the outputs of the external command. Alternatively, you can just see the output in a toolview.

This is a useful addition to <a href="http://docs.kde.org/stable/en/kdesdk/kate/advanced-editing-tools-scripting.html">Kate scripts</a>, and makes it simple to compile simple "Hello World"-style applications, support arbitrary build scripts or run external helpers like linters, beautifiers. Or you could use it to paste selected text to a pastebin server. As you see, this plugin should increase the versatility of KDevelop greatly and help users of software that is not (yet?) integrated into KDevelop natively.

Screenshots and more information can be found on <a href="http://milianw.de/blog/kdevelop-externalscript-plugin">Milian Wolff's website</a>.

<h3>Project Fetching</h3>

With KDevelop 4.1, you are now able to import remote projects and start working on them directly. You can transparently create a local checkout of a SVN or CVS repository or clone a Git repository for example. For KDE projects, there is even a special provider that simplifies it even more to checkout a module to start working on it.

More information and screenshots can be found on <a href="http://www.proli.net/2010/07/07/gsoc-progress/">Aleix's website</a>.

<h3>Hex Editor</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/okteta.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/okteta-sml.png" /></a><br />Hex editing in KDevelop (click image for larger version)</div>Developers who work on input and output of files can now view and edit all files as raw data directly in KDevelop simply by selecting "Open as Byte Array" in the context menu of all file listing sidebar tools, thanks to the new Okteta plugin for KDevelop. It reuses the components of the Okteta program and in this first version already makes many of them available, including the great Structures tool.

<h3>PHP 5.3 Support</h3>

The PHP plugin for KDevelop has seen some work to support PHP 5.3 features. It now supports new syntax like goto, late static binding, closures, nowdoc and quoted heredocs, and namespaces. The latter is not yet perfectly supported, but this should improve over time.

<h2>More</h2>

There have been many more internal changes to the KDevelop codebase making it more robust and stable than ever. The features you found in <a href="http://dot.kde.org/2010/05/01/kdevelop-40-stable-released-wild">KDevelop 4.0</a> have been polished and are better than ever.

Here is a list of some noteworthy bug fixes, but keep in mind that <a href="https://bugs.kde.org/buglist.cgi?product=kdevelop&product=kdevplatform&resolution=FIXED&chfieldfrom=2010-05-01&chfieldto=Now&chfield=resolution&chfieldvalue=FIXED">over 150 bugs have been fixed since the 4.0.0 release</a>!

<ul>
<li>Running/debugging applications that produce lots of output does not hang the UI anymore</li>
<li>Project management is more robust</li>
<li>Improved CMake support: notably increased performance and the project tree not collapsed on CMakeLists.txt edits</li>
<li>Fixed a common crash on shutdown (which never resulted in data losses but was very annoying)</li>
<li>You can now select messages from the Build, Execute and Debug toolviews and copy them to the clipboard</li>
</ul>

<h2>Get It!</h2>

We invite everybody to get a copy of the source code from KDE mirrors or install packages from distributions. We wish you all many happy hacking hours. If you run into problems or have questions, do not hesitate to contact us either in #kdevelop on freenode, via <a href="http://bugs.kde.org/">Bugzilla</a> or on our mailing lists.
