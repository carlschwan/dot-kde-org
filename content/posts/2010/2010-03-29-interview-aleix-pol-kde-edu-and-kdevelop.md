---
title: "Interview with Aleix Pol of KDE Edu and KDevelop"
date:    2010-03-29
authors:
  - "giovanni"
slug:    interview-aleix-pol-kde-edu-and-kdevelop
comments:
  - subject: "Qt Creator"
    date: 2010-03-29
    body: "When I first heard about Qt Creator, I was also against it. But now I understand why Nokia developed it and even like that KDE devs now have a broader choice of IDEs.\r\nNokia wanted an IDE to develop code for Qt that also runs well on Windows and Mac OS X. kdelibs sorta work on those platforms, but \"pure Qt\" works better. It was also clear from the beginning that Nokia will obviously target Symbian and Meamo at some point, while KDevelop's goal is -- needless to say -- the KDE Platform that doesn't even work on Symbian."
    author: "KAMiKAZOW"
---
Hello again to all the KDE people. We are here with a third interview. Last  time we had <a href="http://dot.kde.org/2010/03/07/interview-kdepim-contributor-tobias-k%C3%B6nig">Tobias König</a>, a KDE PIM developer. Today it’s the turn of Aleix Pol, a contributor to KDE EDU and KDevelop. As usual, there is also the <a href="http://kde-it.org/2010/03/19/intervista-con-aleix-pol/">original interview in Italian</a> if you prefer to read that.
<!--break-->
<b>Hello Aleix, can you introduce yourself to our readers?</b>
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/aleix.jpeg" width="200" height="266" /><br />Aleix Pol</div>Hi! I'm Aleix Pol Gonzàlez and I'm a 23 year old computer science student in the UPC/FIB university in Barcelona. Right now I'm in Paris in an exchange program (<a href="http://en.wikipedia.org/wiki/Erasmus_programme">Erasmus</a>) trying to learn from some different points of view. KDE-wise, I work in <a href="http://edu.kde.org/kalgebra/">KAlgebra</a> and <a href="http://www.kdevelop.org/">KDevelop</a> mainly. Also I'm working on <a href="http://kamoso.wordpress.com/">Kamoso</a>, an application to retrieve pictures from webcams that we started about a year ago with a friend, and Talk which is a presentation software I've been using for presenting already, but which is still in its early stages. (If anyone is interested, check <a href="http://gitorious.org/talk">http://gitorious.org/talk</a> or just ask me).

<b>When did you first hear about KDE?</b>
I first used KDE software in a Corel Linux installation I managed to make work on my (and my father's) computer quite some years ago, I didn't realize it was KDE software until I started to use GNU/Linux actively around 6 years ago. Since I moved to Mandrake as a distribution, I wouldn't leave KDE in any serious attempt.

<b>In which part of KDE do you give your help?</b>
I think most of my contribution in KDE is inside KDevelop, mostly because of the freedom the <a href="http://code.google.com/soc/">GSoC</a> program provides to the student in terms of making it really possible to spend as much time as you need with some stuff and make it rock for real.

<b>How about KAlgebra?</b>
I started with KAlgebra though, and I still keep maintaining and developing it. It's like my monster, like Frankenstein's and it's a piece of code I love (sometimes I wonder if that's healthy). Right now, I'm trying to push it forward with some improvements in the language and from time to time I get to know some people who are interested in it, so long life to KAlgebra!

<b>And Kamoso?</b>
Kamoso was a project I started with Álex Fiestas. We talked about doing such an application many times and at some point we decided to lock ourselves in some room and get it done. A curious story about it is that the first day we started coding Kamoso we didn't have any webcam at all, so we just created the interface. In the next morning we got a webcam of course. Now, since Software Compilation 4.4 is out, we will release some new version soon. Changes we made in the kipi plugin architecture back in the digiKam meeting allow us to better integrate Kamoso in the KDE ecosystem. We will integrate Kamoso with Kipi so to have the possibility to upload the captured movie to YouTube and Facebook.

<b>Will there be a possibility to have a <a href="http://phonon.kde.org/">Phonon</a> or <a href="http://solid.kde.org/">Solid</a> backend for webcams in Kamoso or does it use its own solution?</b>
Solid backend for webcams is already working and we're using it to let the user choose what webcam he needs. Just great, kudos to Solid! About Phonon this is something really different. Mainstream Phonon doesn't have any input facility, there's some experimental work but we won't rely on that. Plus Nokia is pushing a new Multimedia framework in parallel which does input but we're not really sure about that either. Right now we're using some <a href="http://www.videolan.org/vlc/">VLC</a> library which doesn't even ensure the minimal API compatibility between versions. Even if it's troublesome, we would like to stick to VLC for the moment until we have a good solution for that.

<b>When will we see a stable release of KDevelop 4? (the first version built on KDE Platform 4)</b>
KDevelop 4.0 will be released soon. We're discussing right now the dates on the mailing list. It's not fixed yet but should be before SC 4.5 anyway.

<b>What are the difficulties in developing this kind of application? </b>
The problem about how to create a good IDE is one of the nicest I've found when thinking how to design software. You have to let the user be creative by himself, not to stop him to do anything he wants. You have to elegantly, seamlessly embed the tools that the project is using. You have to provide the needed knowledge to the user from what we have in the computer. It's really challenging and fruitful.

<b>What is your specific role in the team? How many developers are there?</b>
In KDevelop I've mostly maintained CMake support and worked together with the rest of the team to create a good IDE platform (Run configurations, documentation support, project integration, version control, etc). As a KDevelop user I usually try to fix any problem I find during my work in KDevelop or outside (which is not so often anymore since we're quite stable now :). In the KDevelop team we are 6 or 7 people plus some of them who come and go from time to time. During my last talk at <a href="http://fosdem.org/2010/schedule/tracks/kde">FOSDEM</a> I was asked how can we compete with projects like Eclipse which have much more people working on it. I think that we just have to take a look at the result, which I think is pretty amazing. Of course we would do better with more people though, so if anyone is interested, just drop by on the list or IRC and say hi. Of course, we should count also the Kate and KDELibs developers, that would make a better number.

<b>What programming languages are now supported in KDevelop</b>?
Short answer: Officially right now we just provide full language support for C++. Long answer: we have some half baked language support in playground for PHP (that will likely go official soon), Python, Java, C#, etc. If anyone is interested in having one of these in KDevelop just take a look at it and contribute anything you feel like.

<b>Could QtCreator replace KDevelop in the next years? Or do these two IDEs have different target markets?</b>
Yes, <a href="http://qt.nokia.com/products/developer-tools">QtCreator</a> could take some KDevelop users. But of course a free software project dies when its codebase dies. And KDevelop is really alive right now, we're happy about it and we still have lots of ideas to push it forward. It's sad Nokia decided to break the community like that but I'm sure we all will find our place in the whole user range. If someone has a need for an IDE, we would like KDevelop to fill it for sure. We created a platform for that, which will be the base for Quanta too, once it's ready. It's a really nice base, provides features to create IDE's with abstractions like projects, UI, languages etc that make it easily extendable to any purpose related to the subject. We can create anything we need over that platform.

<b>Now that KDE software is available also on Mac OS and Windows could this thing give more resources to the development or make more difficult to have an application working on all platforms?</b>
KDevelop should work on Windows and MacOS X too, but it's not really official since we don't have users on these platforms. I have seen KDevelop running in MacOS X and Windows so making it as useful as in Linux is just a matter of time.

<b>What parts of the KDE Software Compilation could be improved?</b>
KDE SC has evolved a lot since 4.0. Now it's really mature and flexible and probably would fit much of the needs for anyone. I think we will have to get better at integrating remote data services, because it's were the whole industry is going and KDE has a really privileged place for that because of its freedom and openness. This should cause some discussion about what services should be adapted or not so that KDE keeps being free (as in freedom). Another area that is really missing is the multimedia input (as I mentioned before). There is plenty of libraries and stuff to make it work but we don't have a 'KDE way' yet, that makes any choice hard to make and less portable.

<b>Which KDE applications are doing very good job? Are there any that need lots of love and help?</b>
KDE as a project I think is doing a great job, looks like we're all moving together and that's great. Most prominent projects are Amarok and Plasma right now I guess, at least they are the ones with more active people (and some of them paid, which always helps). There are plenty of areas that need more love. First of all Kopete, it's a project that it's really nice and needed but I think it could do better and I'd really like to see it improved. I try to find some time for it but sadly, days just have 24h. Another area that I'd like to see improving is KDE-Edu in the marketing area. We have a unique suite of free applications for schools, teachers and students to use but we have too much problems to reach them.

<b>Did your contributions feature in SC 4.4? Have you got plans for KDE SC 4.5?</b>
Most of my work has been focused on KAlgebra which got some work in form of a <a href="http://edu.kde.org/cantor/">Cantor</a> frontend and some language improvements, as I said I'd like to make from the KAlgebra language something useful and some stuff got in already. In KDevelop there were some improvements and bugfixing but now that KDevelop is in <a href="http://websvn.kde.org/trunk/extragear/sdk/kdevelop/">extragear</a> I guess this doesn't count anymore as contributions in KDE SC, right?

<b>Are you paid to contribute to KDE?</b>
Not now. I've been paid in the past through GSoC though.

<b>What motivates/keeps you motivated to work on KDE?</b>
I think that the great advantage of being part of KDE is that you can reach the end user far easier, every contribution I make it's because I enjoy to contribute to the project itself or because I think that my contribution will make life easier in some way. I have to admit that whenever I receive some e-mail asking about KAlgebra and saying they like it, it makes my day.

<b>If you could choose where you can live what place you will choose and why?</b>
I'm not sure I can say that right now. I think my life will change quite a lot once my studies are over so I won't close any door. Of course somewhere with nice weather would help me get out, which is not always easy, and that would affect my productivity in KDE, of course. I guess somewhere around the Mediterranean will feel always like home to me, I can't help it.

<b>Have you ever stayed in Italy? If so, where?</b>
I've been in Italy a couple of times. The first time I came with some family and we visited some areas in the north of the country (Venezia, Lago di Como), really beautiful. The second time I came with some university friends to Rome. I actually really like the country (oh come on! I couldn't say else!), but I still remember it always for the great food.

<b>What's been the last book you read?</b>
The last book I read was a book about Programming Language design from Michael L. Scott. Other than technical books, the last one I read was coincidentally La Repubblica (“La República” in Catalan) by Machiavelli, which is very related to your country and I enjoyed by the way.

<b>Aleix, thank you for your time and good luck for your projects.</b>