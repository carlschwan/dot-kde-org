---
title: "KDE Software Compilation 4.4.0 Introduces Netbook Interface, Window Tabbing and Authentication Framework"
date:    2010-02-09
authors:
  - "sebas"
slug:    kde-software-compilation-440-introduces-netbook-interface-window-tabbing-and
comments:
  - subject: "Yezzzzz"
    date: 2010-02-09
    body: "Way cool, and about time, I couldn't take the waiting anymore! Congrats everyone :D"
    author: "jospoortvliet"
  - subject: "Rocking!"
    date: 2010-02-09
    body: "Congratulations everyone! Six hard months of rocking now out!"
    author: "einar"
  - subject: "Very good"
    date: 2010-02-09
    body: "I think this is the best KDE release so far!\r\nImproved performance, improved artwork, improved usability, more features, less bugs,..  you get the idea. :-)"
    author: "FrankKarlitschek"
  - subject: "Still..."
    date: 2010-02-09
    body: "Congrats on the new release!\r\n\r\n- Plasma still far from usable. I get this when I try to configure it:\r\n\r\nhttp://vedranf.net/tmp/plasma4.4.png\r\n\r\nAnd, what's even worse, in my bug report Aaron said everything is behaving as intended: https://bugs.kde.org/show_bug.cgi?id=221473\r\nWell, if that's intended, then yes, I was correct before - plasma (still) sucks :(\r\n\r\n- I still don't see a way to print odd/even pages in print dialog :-(\r\n- No global multikey shortcuts yet :-(\r\n- Tons of new eyecandy\r\n- Window tabbing, really nice :-)\r\n\r\nRegards,\r\nVedran\r\n"
    author: "vedranf3"
  - subject: "not convincing"
    date: 2010-02-09
    body: "come on the tarballs are live only minutes and you start complainig about stability? not a very convincing troll..."
    author: "jospoortvliet"
  - subject: "Not to mention..."
    date: 2010-02-09
    body: "that post was there before the tarballs were...\r\nCome on people... don't moan at devs on the news page."
    author: "ffejery"
  - subject: "Sorry, but svn is always"
    date: 2010-02-09
    body: "Sorry, but svn is always accessible. You can call me a troll, but image doesn't lie, nor the developer's response on bugs.kde.org.\r\n\r\nRegards,\r\nVedran\r\n"
    author: "vedranf3"
  - subject: "IMO"
    date: 2010-02-09
    body: "The best KDE EVER :D"
    author: "pinheiro"
  - subject: "The KDE SC is developing at a"
    date: 2010-02-09
    body: "The KDE SC is developing at a pace nowadays, it is ridiculous. This is one hell of a release!"
    author: "d2kx"
  - subject: "printing odd/even pages is there"
    date: 2010-02-09
    body: "I don't have any issues with global shortcuts, and clearly printing odd/even pages is also supported:\r\n\r\nhttp://www.kde.org/announcements/4.4/screenshots/44_okular_printing.jpg\r\n"
    author: "sebas"
  - subject: "Multikey global shortcuts do"
    date: 2010-02-09
    body: "Multikey global shortcuts do not work. That's confirmed for a long time:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=161009\r\n\r\nAbout printing. It is possible that I did something wrong during compile, but that dialog is not what I got few days ago when i compiled kdelibs/kdebase. Maybe I need to rm -rf ~/.kde.\r\n\r\nRegards,\r\nVedran"
    author: "vedranf3"
  - subject: "Release Kudos"
    date: 2010-02-09
    body: "With every release, KDE just keeps on surpassing itself. The 4.4 release looks awesome with new cool features like window tabbing, improvements in many areas (maybe I'll finally enable Nepomuk) and overall polish. Excellent.\r\n\r\nCongratulations and thanks everyone!\r\n\r\nOh, and don't forget that you can post release kudos over at the forums: http://forum.kde.org/viewtopic.php?f=77&t=85699 (registering not needed)"
    author: "Hans"
  - subject: "You need to be running CUPS"
    date: 2010-02-09
    body: "You need to be running CUPS locally at runtime for Odd/Even to be enabled.  Do you also have the other CUPS features like n-up and banner pages showing?"
    author: "odysseus"
  - subject: "Wallpapers"
    date: 2010-02-09
    body: "Now that 4.4 has been released, is there a place (besides trunk) where I can easily get the new wallpapers? They look awesome."
    author: "Krul"
  - subject: "missing proper duelhead"
    date: 2010-02-09
    body: "Just a shame that kde 4 still have problem with duel head\r\nhttps://bugs.kde.org/show_bug.cgi?id=156475\r\nthis make it unusable for me.\r\nBut as soon as \"there is no dbus session bus that matches the x.org\r\ndisplay and so all dbus calls fail and therefore the unique apps also fail\"- problem has gone I will upgrade asap. KDe 3.5 as beginning to age"
    author: "beer"
  - subject: "CUPSd? Nope, only client,"
    date: 2010-02-09
    body: "CUPSd? Nope, only client, CUPSd server is on another machine. Could that be the problem? For the second question I'll check later as I'm not near that machine now.\r\n\r\nRegards,\r\nVedran\r\n\r\n"
    author: "vedranf3"
  - subject: "Plasma Netbook Interface"
    date: 2010-02-09
    body: "What is the status of the Plasma Netbook Interface? I read before that it was going to be a technological preview, but i haven't find this information in the release announcement"
    author: "Krul"
  - subject: "info of the plasma netbook? here"
    date: 2010-02-09
    body: "http://www.kde.org/workspaces/plasmanetbook/"
    author: "xapi"
  - subject: "it's released, not a preview."
    date: 2010-02-09
    body: "It's not a technical preview. It's new, a first release, but it is stable and usable - or should be ;-)"
    author: "jospoortvliet"
  - subject: "To elaborate on the post below"
    date: 2010-02-09
    body: "...it is classified as Tech Preview in the sense that is good enough to be released, but not necessarily \"done\" in that the developers still have big plans for it.  This sort of thing might also come with a disclaimer, but I haven't actually seen one in this case.  That said, I used Kubuntu's backport of it several months ago (which is old code now), and it was quite functional and stable, if still a bit rough around the edges at that point.  I would expect that it would be perfectly usable now, and I'd encourage you to try it.\r\n\r\nEDIT: sorry, my mistake.  Last I heard they were still calling it a Tech Preview, but apparently not anymore.  Sorry for the confusion."
    author: "ffejery"
  - subject: "DUAL HEAD"
    date: 2010-02-09
    body: "Beer, I know that you are going to dump me for this as always but I will not stop until you get it. It's not duel (a challenge), it's dual (double) head.\r\nI know that you are not English speaking so I am just trying to help a bit ;)"
    author: "Bobby"
  - subject: "zui"
    date: 2010-02-09
    body: "As I said in that bug report, we are aware that the ZUI for activities is not workable. The reasons are multiple and include: \r\n\r\n* not being able to do nice transitions, so the zooming is really just a change from one mode to another which makes it harder to grasp what just happened when \"zooming out\"\r\n\r\n* when there is per-virtual-desktop activities it gets tricky since you can see all the activities for all desktops there\r\n\r\n* when there is more than one monitor, a similar thing arises\r\n\r\n* to make sense of the ZUI, the user needs an internal mental model of activities that they can then map to a spatial representation; this is not as common as hoped for :)\r\n\r\nas a results, in trunk (which will become 4.5 eventually) the ZUI for activities has been completely removed.\r\n\r\nall of that said, to call plasma unusable due to that one feature is hyperbole."
    author: "aseigo"
  - subject: "Ok, so now for the usual question..."
    date: 2010-02-10
    body: "Which distribution should I choose to enjoy a decent KDE4 implementation?\r\n\r\nI have always been a KDE fan, I started using it regularly in the KDE2 days. Since the advent of KDE4 finding a good KDE distribution seems really impossible. Each time a new release of KDE comes out, I start the usual distro hopping madness looking for a reasonable KDE user experience, but I always end up going back to Mint (and Gnome). \r\n\r\nI'm convinced the problem is not in KDE itself, which I greatly prefer over Gnome, but in distro implementations. The last KDE distro I tried was Opensuse 11.1, but after about a week I had to surrender and go back to Mint - again. \r\nThe ease of use, consistency, stability, polish and most of all SPEED of Mint/Gnome is vastly superior to Opensuse/KDE and Mandriva/KDE. Oh, and don't get me started on Kubuntu, those people seem to be actively boycotting KDE for some reason. And unfortunately Mint/KDE builds on Kubuntu, so it basically is a (less ugly) pile of crap.\r\n\r\nSo please help me out: I want to use KDE, but I also want a distro that simply works out of the box and manages to stay out of my way while I work (in other words, I need Mint. With KDE). \r\n\r\nDoes anything like that exist?"
    author: "il_marcello"
  - subject: "zui"
    date: 2010-02-10
    body: "Aaron, since zui for activities is not working (and is due to be removed in 4.5), how do you manage activities (i.e. add, remove, set activity per desktop, etc.) in 4.4?"
    author: "cirehawk"
  - subject: "And the same answer"
    date: 2010-02-10
    body: "Since your last try was some while ago and OpenSUSE 11.1 was KDE 4.1.3 based, so lots have improved since then. \r\n\r\nAs for recommendations, I'd say Mandriva 2010 or OpenSUSE 11.2. Perhaps Fedora 12 or even Slackware 13.0."
    author: "Morty"
  - subject: "opensuse or mandriva"
    date: 2010-02-10
    body: "Can't you just install kde on mint?\r\n\r\ni'm still happy with opensuse and kde4: quite fast, well integrated and mature.\r\n\r\nmandriva and kde4 also works very nice on my netbook, but opensuse's version is further developed in some areas."
    author: "whatever noticed"
  - subject: "Not quite"
    date: 2010-02-10
    body: "He say it's not workable, not that it's not working. \r\n\r\nBy that it seems he means that it works in a no-optimal and cumbersome way, with several issues and bad handling of corner cases. Like the parents dual screen setup. In short it does not fulfill the Plasma teams vision for activities and the handling of them, and they need to develop a different approach."
    author: "Morty"
  - subject: "Recommendation"
    date: 2010-02-10
    body: "My distro of preference is PCLinuxOS.  Since they pride themselves on the principle of \"it just works\", they held off on implementing KDE4 until they deemed it stable enough.  In the meantime, I used Mandriva and it has performed very well.  However, a new release of PCLinuxOS is eminent and it will use KDE4.4 and it looks impressive (I've played with the testing packages for a while), so I'm about to switch back to PCLOS.  I'd say give those 2 a try."
    author: "cirehawk"
  - subject: "ZUI"
    date: 2010-02-10
    body: "I understand your point, but for the purposes of my question, the difference in \"not working\" and \"not workable\" is only semantics.  If zui works in a cumbersome way, my question still remains if there is another way to manage activities in 4.4?  That question is not meant to point out a flaw, but rather the opposite.  i like \"per desktop\" activities and am looking for the best way to manage them at this point."
    author: "cirehawk"
  - subject: "two alternatives in 4.4"
    date: 2010-02-10
    body: "in 4.4, you can either use the Activity Bar Plasmoid, or you can go to the Activity Settings (right click or click on the desktop toolbox -> Activity Settings) and in the Mouse Actions panel you can select the Switch Activity plugin and then associate it with what trigger you wish (mouse scroll wheel, right click, etc)"
    author: "aseigo"
  - subject: "LFS"
    date: 2010-02-10
    body: "I would recommend Linux From Scratch.\r\n\r\nThe build instructions are here:\r\n\r\nhttp://techbase.kde.org/Getting_Started/Build/KDE4.x\r\n\r\nThat way you don't have to worry about anyone screwing up KDE."
    author: "JRT256"
  - subject: "My mistake, my last try was"
    date: 2010-02-10
    body: "My mistake, my last try was with Opensuse 11.2 (KDE 4.3.4 IIRC). Then I switched to Mint8, which had more or less just been released.\r\n\r\nOn another forum people suggested I try Arch or Pardus. Any thoughts?"
    author: "il_marcello"
  - subject: "Mint/KDE builds on Kubuntu,"
    date: 2010-02-10
    body: "Mint/KDE builds on Kubuntu, so it's a no-go. I think that not even the brilliant Mint developers could obtain anything usable from Kubuntu... "
    author: "il_marcello"
  - subject: "Hehe, if I had the time I'd"
    date: 2010-02-10
    body: "Hehe, if I had the time I'd probably try that. But if Openuse / Mandriva developers which are way more intelligent than myself can't do a satisfactory job, I doubt I could.\r\n\r\nYour suggestion was ironic, right?"
    author: "il_marcello"
  - subject: "Replacement?"
    date: 2010-02-10
    body: "I always want to use Activities (as in different screens with different widgets) .. but atm it is just too many clicks so I don't.\r\n\r\nWill 4.5 have something simpler? Easier to understand. More like gnome-shell. You press one button and you see your activities and you can put widgets on them and delete one or add a new one.\r\nI would really appreciate that. \r\n\r\nBut 4.4 is a great release no matter what, so keep up the great work guys!"
    author: "kragil"
  - subject: "OpenSuse has the best"
    date: 2010-02-10
    body: "OpenSuse has the best implementation of KDE 4 and the most polished one IMO. It also integrates the GTK applications very nicely in the KDE4/Qt environment. \r\nI haven't tried Mandriva for a while now but it was also very good that last time I tried it. I would also say that these two are the best KDE distros."
    author: "Bobby"
  - subject: "Yes, our cups detection"
    date: 2010-02-10
    body: "Yes, our cups detection routine is very primitive, it detects cups by trying to connect to localhost:631, so remote servers are missed.  But I guess I could improve that now by copying the Qt detection routine, now that it's LGPL.  Stay tuned :-)"
    author: "odysseus"
  - subject: "I don't know about others,"
    date: 2010-02-10
    body: "I don't know about others, but for me there's nothing which is hard to grasp about activities. I said unusable because every time I tried to configure it, I've got what you see on that screenshot. Only way to get working plasma configuration with activities was to edit plasma-desktop-appletsrc by hand and chmod it a-w.\r\n"
    author: "vedranf3"
  - subject: "OK, thanks. Easy fix for"
    date: 2010-02-10
    body: "OK, thanks. Easy fix for 4.4.1 would be to detect cupsd by connecting not to localhost but to whichever server is present in the ServerName directive of the /etc/cups/client.conf."
    author: "vedranf3"
  - subject: "mandriva"
    date: 2010-02-10
    body: "mandriva is great, but doesn't offer bleeding edge kde.\r\ni read somewhere that kde 4.4. won't arrive to mandriva, until kde 4.4.1."
    author: "whatever noticed"
  - subject: "websvn?"
    date: 2010-02-10
    body: "You can download them directly using websvn. What more would you need?\r\n\r\nThe best way of course is to install 4.4.0, so you get all the other goodness \"around\" it as well :)"
    author: "sebas"
  - subject: "dualhead working fine here"
    date: 2010-02-10
    body: "I might add that many people use dualhead in Xinerama mode, and that this seems to work just fine. I've personally been using Plasma on my dual-screen setup since before 4.0, and while there were problems in the early stages, I've been very happy with its dualhead support.\r\n\r\nI've multiple panels (one on each screen that shows only the windows active on that particular screen, KWin behaves well and performance with compositing is just fine.\r\n\r\nNothing to complain about dual-head from my side, YMMV."
    author: "sebas"
  - subject: "Oh, please"
    date: 2010-02-10
    body: "Come now. To start with the same codebase that everyone else uses and then produce an unmitigated pile of junk would require genius of the highest order.\r\n\r\nI've used both Kubuntu 9.10 and OpenSUSE 11.2, and the differences are nothing like you describe. (Anti-disclaimer: I am _not_ a Kubuntu developer.) Bugs are frequently reproducible across distros. That's one good reason to be excited about 4.4 (which I haven't yet tried myself).\r\n"
    author: "tangent"
  - subject: "Normal I have one x session"
    date: 2010-02-10
    body: "Normal I have one x session on each screen and I would choce that solution over Xinerama. \r\nI have 4 reasons to do that:\r\nWhen I start a program I expect that program to start on that screen where I started it and not where I closed it last time.\r\nWhen I tab I would only see the program I am working with eg those program I have on the middle screen and not on my screen to the left and the right.\r\nA few games have problem with running on full screen in Xinerama mode. (they use all 3 screen instead of 1). There might be a solution for this.\r\nLast time I tried to play around with Xinerama mode I could not set it up becosue I have 1 graphic cards to take care of scren to the right and left and another card to take care of the screen in the middle and I could not get help online.\r\n\r\nI plan to stay with kde 3.5 on my desktop to either I get enough knowledge to settup kde in another way that I like, that black screen problem with kde 4 is solved or I am forced to find something else. By forced I mean that squzed is released I cannot use kde 4 by then\r\n"
    author: "beer"
  - subject: "OpenSuse, ArchLinux with KDEMOD and Mandriva"
    date: 2010-02-10
    body: "are the best choices from my experiences.\r\n\r\nARCH is indeed very slick. it comes with decent KDE packages and you can also install KDEMOD (which is a \"pimped\" version of KDE)\r\n\r\nOpenSUSE is also very good. The Suse people did a splendid job to provide a great KDE feeling. Even Firefox and Openoffice have KDE dialogs, Yast is based on Qt. This is really polished. But, it might be the most heavyweight distro of all three...\r\n\r\nMandriva is the good bet in the middle. Also very polished, not so heavyweight, but not so slick like Arch...\r\n\r\n"
    author: "thomas"
  - subject: "mandriva packages available"
    date: 2010-02-10
    body: "Turns out that mandriva decided to release kde 4.4 unofficially on ftp.kde.org:\r\n\r\nhttp://forum.mandriva.com/viewtopic.php?p=779176#779176"
    author: "whatever noticed"
  - subject: "Thanks to the KDE people."
    date: 2010-02-10
    body: "After 10 years of running linux, KDE4 4.4 has blown me completely of my feet. I saw Jos' Poortvliets demo on Linuxworld in Utrecht, I dreamt, followed openSUSE Factory packages, saw it mature and now it's there. Take a couple of days, get the feel, configure and use Nepomuk, the Launch and Search activity, woooooow.\r\n\r\nCheers to all who made this possible"
    author: "knurpht"
  - subject: "Small donation again"
    date: 2010-02-11
    body: "I wish I could use KDE at work but alas, not possible yet. \r\nI do use it at home and I do want to send some gratitude to all of you who work on this.\r\nThanks for the new release."
    author: "osh"
  - subject: "Hmm, that could be a good"
    date: 2010-02-11
    body: "Hmm, that could be a good way, would just need to look through the full list of possible locations for the file and parse the file correctly.  The full Qt method requires building against cups and calls to cups directly to check a dozen different things work, which we obviously can't add in a bugfix release.  It would just be easier if Qt exposed what print engine it was using through the api."
    author: "odysseus"
  - subject: "Actually, it's easier than"
    date: 2010-02-11
    body: "Actually, it's easier than that :-)  I've just remembered a particular behavior of the Qt printing system that differs between cups and lpr which I can query to see which mode it is running in.  Easy peasy."
    author: "odysseus"
  - subject: "A distro to test KDE 4.4"
    date: 2010-02-11
    body: "Following my experience, the distro list are as follows. Obviously, AVOID KUBUNTU AT ALL COSTS. I'm thinking in review that advice when Lucid is out, because Kubuntu started to do the right things with Project Timelord, but Karmic is a no go, and that also includes Mint.\r\n\r\n1. Mandriva. 2010 Spring promises a lot of DRAMATIC enhancements, like they did in 2010 as a technical preview. This time, they are going serious. And Cooker promises. In the meantime, visit aapgorilla repo.\r\n\r\n2. Fedora. Nepomuk in KDE 4.3, with Fedora, was a disaster, because the Fedora KDE team refused to package Sesame 2. Now, with Virtuoso, things are changing. If you tried Fedora with Nepomuk before, you'll see a 30x speed increase (yes, 30 TIMES) with Nepomuk under KDE 4.4. Utilities are OK, KPackageKit is OK, and the KAuth framework needs some polishment. But at least we don't have any major distro-specific bugs, like the load of Kubuntu-specific bugs.\r\n\r\n3. Chakra. Keep an eye on that one. They NEED more manpower. And if they get it, they may be _the_Ubuntu_killer_. Arch is a MUCH better distro to be based on, and if you really want to work in a distro, let Chakra be that one.\r\n\r\n4. Sabayon and Gentoo. They are doing the right things. Particularly Sabayon; try that one, and be in the dev channel ;). If you can't, with Gentoo you'll have a good system.\r\n\r\nI avoid OpenSuSE like the plague. Not because I like childish boycotts against Novell, but because a) YaST is a LOT slower than any competing package management system, b) if I really need a total system configuration panel, Mandriva Control Center wins hands down, and c) OpenSuSE has been the only distro to fail miserably the suspend to disk, suspend to RAM, and resume tests in my notebook. Try to install NVidia drivers in OpenSuSE and you turn it into a OS less stable than Windows ME. I'm not kidding. And I haven't been able to replicate that behaviour with any distro but OpenSuSE."
    author: "Alejandro Nova"
  - subject: "\"Will 4.5 have something"
    date: 2010-02-12
    body: "\"Will 4.5 have something simpler? Easier to understand. More like gnome-shell. You press one button and you see your activities and you can put widgets on them and delete one or add a new one.\r\n I would really appreciate that.\"\r\n\r\nNow you can see activities using the activities widget. When you click the Cashwe, you place widgets to them and delete new activities or remove current ones.\r\n\r\nSo all what you need to do, is to now add new panel to side, place it autohide and add a activities widget there. Then just use them.\r\n\r\nThe Gnome-Shell has same kind idea as ZUI is."
    author: "Fri13"
  - subject: "mandriva"
    date: 2010-02-13
    body: "I have to agree with you and Mandriva, I don't use other Distros so I cant compare, but apart from some bumps (mostly coused by my bad use), it is rock stable, my only grippe with it, is the defaults it ships, iora theme is obviously not my favorite :)\r\n\r\nStill i wish all the luck in the world to all distros shipping KDE, I know theyr try their best.  "
    author: "pinheiro"
  - subject: "Thanks"
    date: 2010-02-13
    body: "That sounds a bit better. I just tried it on my Kubuntu netbook and somehow I can't add new activities when I zoom out. That is probably bug in the Kubuntu packages, but that shows that the functionality is not widely used and even with those tweaks (whcih you have to know) it isn't really straight forward. \r\n\r\nEdit: I just tried again and it works when widgets are unlocked .. hmmm .. I don't like the unlocked look of plasma and always unlocking then adding and then locking again is .. again A LOT of clicks.\r\n\r\nEdit2: I thought about some more and I think you should always be able to add panels and activities. Locking widgets (Miniprogramme in German) is OK and logical, but locking activities makes no sense to me and looks like a bug."
    author: "kragil"
  - subject: "fun and a question"
    date: 2010-02-16
    body: "fun:\r\nwhere's the live wallpaper option!!??\r\n\r\nI have that on my nexus one! I want them on kde! \r\n\r\n\r\nps: yes, I know about \"virus\" and other \"live\" wallpapers. guys, you should patent your ideas :)\r\n\r\n--\r\n\r\nthe question: \r\n\r\ndoes anyone have their printer applet open on login? mine does open on login and I don't know why. is this normal behavior? can it be changed? I would like the printer applet only run when a print job is in the queue.\r\n\r\n"
    author: "nbensa"
  - subject: "Fedora"
    date: 2010-02-19
    body: "Fedora's policy is to not ship java binaries so thats why sesame isn't there. There're now virtuoso-opensource 6.1.0 packages so Nepomuks should work fine.\r\n\r\nBTW Fedora 12 and 11 will have 4.4 soon as an official update and it's already in the updates-testing repo. I have personally been using 4.4 since beta and I'm loving it! It's an amazing software compilation indeed :)"
    author: "bliblibli"
---
<div class="taxonomy-images">
<a href="/category/dot-catgories/kde-official-news" class="taxonomy-image-links"><img src="http://dot.kde.org/sites/dot.kde.org/files/category_pictures/kde.png" alt="KDE Official News" title="KDE Official News"  width="48" height="48" /></a></div><div style="float: right; padding: 1ex; margin: 1ex; border: 0px">

<a href="http://kde.org/announcements/4.4/"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde44.png"/></a>

</div>


Today KDE <a href="http://www.kde.org/announcements/4.4/">announces</a> the immediate availability of the KDE Software Compilation 4.4, "Caikaku", bringing an innovative collection of applications to Free Software users. Major new technologies have been introduced, including social networking and online collaboration features, a new netbook-oriented interface and infrastructural innovations such as the KAuth authentication framework. According to KDE's bug-tracking system, 7293 bugs have been fixed and 1433 new feature requests were implemented. The KDE community would like to thank everybody who has helped to make this release possible.

Read the <a href="http://www.kde.org/announcements/4.4/">Official KDE SC 4.4.0 Announcement</a> and the <a href="http://www.kde.org/announcements/4.4/guide.php">Visual Guide To KDE Software Compilation 4.4</a> for more details on the improvements in 4.4.
<!--break-->
