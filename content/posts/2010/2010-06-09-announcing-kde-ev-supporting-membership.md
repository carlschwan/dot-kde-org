---
title: "Announcing the KDE e.V. Supporting Membership"
date:    2010-06-09
authors:
  - "jospoortvliet"
slug:    announcing-kde-ev-supporting-membership
comments:
  - subject: "Nice initiative"
    date: 2010-06-09
    body: "This is a very nice initiative and I hope that many people will participate. It somehow makes me feel even more part of this whole world of KDE."
    author: "olingerc"
  - subject: "Banner?"
    date: 2010-06-09
    body: "Hey!  How can they be first when I joined before it was even announced?  Cheating!\r\n\r\nBut my real question is: Will there be a nice designed web banner that we can attach to our blogs and websites and mails to show that we are supporting KDE?"
    author: "ingwa"
  - subject: "Nice initiative"
    date: 2010-06-09
    body: "This is a really nice idea (and execution). I had on my to-do list joining as a supporter, and this will be a nice perk for finally doing it, because right now, the only difference was joining to the mailing list, which last time I checked had almost no messages received.\r\n\r\nPS: Unfortunately, the new website doesn't work with Konqueror 3.5. Not an issue for most people, I suppose, but unfortunately still for me. :)"
    author: "suy"
  - subject: "Nice idea but ..."
    date: 2010-06-09
    body: "I tried registering and it just keeps repeating its steps. I sort of expected to go to the paypal site.\r\nDisappointing ..."
    author: "scobiej"
  - subject: "fax form"
    date: 2010-06-09
    body: "For now, please fax the form on the website to us. We're working on easier online payment solutions, but didn't get this done in time (but still wanted to launch the campaign).\r\n\r\nSorry for the inconvenience..."
    author: "sebas"
  - subject: "Done"
    date: 2010-06-10
    body: "I'm waiting my membership card :). "
    author: "zayed"
  - subject: "Excellent idea"
    date: 2010-06-10
    body: "You are among the first (along with a handful of others) who joined years ago - that's why we say 'new' supporting members; and why we're talking about those who 'joined the game' first as this is a new programme to get ppl to sign up, the idea of supporting membership ain't new (but completely unadvertised).\r\n\r\nAnyway, your idea about a web banner is a good one, will see if I can get the artists to make one."
    author: "jospoortvliet"
  - subject: "swag"
    date: 2010-06-13
    body: "Hi,\r\n\r\nI think it would be great if users could support KDE by buying swag: mug, tshirt, mousepad, etc\r\n\r\nI am sure it would be popular AND good marketing!\r\n\r\nis this somewhere on the roadmap? :)"
    author: "mxttie"
  - subject: "Yes, it is ;-)"
    date: 2010-06-13
    body: "Yes, it is ;-)"
    author: "jospoortvliet"
---
This morning KDE e.V., the legal organization backing the KDE community, launched its Supporting Membership programme under the slogan "Join the Game". The <a href="http://jointhegame.kde.org/">Join the Game</a> programme strengthens the bonds between KDE e.V. and the wider community and provides a more sustainable and independent source of income for KDE activties. By becoming part of KDE e.V. as a Supporting Member you can <strong>help to keep the KDE servers running, fund developer meetings, let developers organize and attend conferences and trade shows and protect the legal interests of the KDE community</strong> -- this is all handled by the KDE e.V. in support of the KDE community.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: none;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-ev-logo.png" width="395" height="253"/></div><h2>KDE e.V.</h2>

KDE has been growing immensely over the last few years and so has the non-profit organisation behind it. KDE e.V. is an association under German law. It provides infrastructure, legal support and channels financial and hardware donations to the community. KDE e.V. has increased both in size and scope, funding tens of KDE contributor sprints each year. Our annual main conference <a href="http://akademy.kde.org/">Akademy</a> has grown counterparts in the USA, Spain and Brazil and the e.V. has become increasingly active in the area of marketing and communication with corporate parties. KDE e.V. employs Claudia Rauch full time to support the Board and wider community in their work and recently Torsten Thelke has joined her for an internship.

Meanwhile, the e.V. depends on donations and volunteer work. Donations come from the many corporate supporters we have and, to a more limited extent, from individual donations. While KDE e.V. does not have a direct shortage of cash at present, neither does it lack ambitions. We would like to further expand our operations, help fund more and better meetings, conferences and marketing efforts. To do this while protecting our independence, we would rather grow a steady and dependable stream of support from the community than rely increasingly on money from a few large companies (although we are of course very grateful to our many corporate supporters).

Additionally, KDE e.V. aims to expand the scope of its membership and engage more with the wider community. While currently the majority of core KDE contributors are members of KDE e.V., many other active community members are not. The work of the e.V. is sometimes not very visible or well understood and this is something we want to improve.

<h2>Join the Game</h2>

That is why we would like to present to you the <a href="http://jointhegame.kde.org/">KDE e.V. Individual Supporting Membership</a> programme. Through this programme anyone who wants to help further the aims of the KDE community but is unable to do so in time and effort, who is not a core KDE contributor, or who can spare the money can now help out by becoming a Supporting Member of KDE e.V..

The goal of this programme is to get people more involved in the work of the e.V. - we will send supporting members quarterly reports, ask their opinions and in general keep them informed of our activities. They will also be able to attend the General Assembly of the e.V. membership at Akademy and follow the discussions (voting will remain a privilege of the core membership). Meanwhile, the money we receive will help us have a more sustainable financial basis and make us less dependent on a few generous  supporters. We will be able to fund more meetings, improve our infrastructure and put more effort in representing our community and helping it to make the best software available.

<h2>Join NOW</h2>

For those at Linuxtag who become Supporting Members there is a special treat. The four lucky winners of a lottery among the subscribers will have a nice dinner with a some key KDE contributors on Friday night.

Moreover, all new Supporting Members will receive a nice surprise gift at home as a token of our appreciation. We will engage them through a variety of channels and we have a few other  plans coming up. Every year all supporting members will be able to join the e.V. membership at the General Assembly at Akademy and we will probably (if there are enough members) organize a special evening for them.

Even if you can't make it to Linuxtag, you can sign up on the <a href="http://jointhegame.kde.org/">Join the Game website</a> and share the warm and fuzzy feeling of being Part of KDE...

<H2>First members Join the Game</H2>

<b>Georg Greve</b>, founder of the FSFE and holder of the German Federal Cross of Merit (Bundesverdienstkreuz) was first to Join the Game. Our second new member is <b>Vincent Untz</b>, release manager and Board member of the GNOME Foundation! We hope many readers will consider following them in <a href="http://jointhegame.kde.org">Joining the Game</a> as a Supporting Member and help KDE continue to grow in strength and improve the software you use everyday.