---
title: "You Be The Judge: Plasma Javascript Jam Session"
date:    2010-04-04
authors:
  - "aseigo"
slug:    you-be-judge-plasma-javascript-jam-session
comments:
  - subject: "Guitar Tuner plasmoid"
    date: 2010-04-04
    body: "Hey, is there any way for me to update the blurb about the Guitar Tuner Plasmoid? It's really more than it appears to be, you have the choice of a ton of preset tunings or you can create your own. I wanted to keep the visual interface really simple, so the access to tuning settings is in the settings. You can also switch between real guitar sounds (for standard tuning) and generated tones (for any tuning).\r\n\r\nKDE Look.org entry:\r\nhttp://kde-look.org/content/show.php/Guitar+Tuner?content=120637\r\n\r\nConfig Screenshot:\r\nhttp://kde-look.org/CONTENT/content-pre2/120637-2.png\r\n\r\nThanks,\r\n\r\nAndrew Stromme"
    author: "astromme"
  - subject: "Make progress! documentation"
    date: 2010-04-05
    body: "Hi,\r\n\r\nalmost all features of my plasmoid are described on the homepage:\r\nhttp://nikita.melnichenko.name/projects/plasmoid-make-progress/index.php?lang=en\r\n\r\nTake a look! There are screenshots and examples for controlling plasmoid via DBus. I would love to receive your suggestions for further improvement!\r\n"
    author: "nikita-melnichenko"
  - subject: "Guitar Plasmoid: An Offer and a Hint :)"
    date: 2010-04-05
    body: "Hey Andrew,\r\n\r\nI've been looking for something like this. Thanks for creating it! Let me know if you could use some banjo samples to add to the guitar one.\r\n\r\nOf course the next step for this already awesome plasmoid would be taking input from a microphone (Phonon FTW!) to tell you what note you're currently tuned to... Hint! Hint! :P\r\n\r\nGood luck!"
    author: "Bugsbane"
  - subject: "Banjos? Sure"
    date: 2010-04-05
    body: "I would love to have more samples. I'm not sure if I will include them in the default package or if I can have some kind of fetching (similar to GHNS) built in, but I'll surely use them in some form.\r\n\r\nThe microphone tuner is a wonderful hint, and wouldn't be too difficult but I don't think it's possible in javascript (currently). I'm thinking of working on a similar plasmoid for simple sound analysis, and your suggestion would fit right in with this idea. I could possibly make it a data engine so that the javascript plasmoid could in fact use it.\r\n\r\nAnyways, thanks for the ideas and offer. Feel free to email me, we can figure out how to get those banjo samples."
    author: "astromme"
  - subject: "Shortlog and GuitarTuner"
    date: 2010-04-06
    body: "\r\nAs for the choices above, Shortlog and GuitarTuner look useful and will definitely give them a look this week.\r\n\r\nKudos on the prizes and for the 5th voter idea.\r\nBoth are nice touches that people will appreciate."
    author: "papa chango"
---
<p>The <a href="http://plasma.kde.org/cms/1238">Plasma Javascript Jam Session</a> is a friendly competition that aims to reward creators of original, interesting and beautiful Plasma widgets written in Javascript with some great prizes and community recognition. The competion concluded on March 31st with 11 successful submissions making the deadline. Judging has commenced, and it will not be easy: many excellent submissions were sent in ranging from the entertaining to the highly useful. Ranging in size from a few dozen lines to a few thousand lines of Javascript code, the submissions showcase a variety of ideas and possibilities.</p>

<h3>.. and the nominees are</h3>

Here, along with a description by the authors, are the competing Plasmoids in alphabetical order:

<h4><a href="http://plasma.kde.org/javascriptjam/2010/alarmclock.plasmoid">Alarm Clock</a>, by Drake Justice</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/alarmclock.png" width="224" height="129">

<p>An alarm clock widget with configurable alarms and a snooze function.</p>

<h4><a href="http://plasma.kde.org/javascriptjam/2010/aquarium.plasmoid">Aquarium</a>, by Ontje Lünsdorf</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/aquarium.png" width="292" height="186">

<p>This applet tries to simulate the behavior of fish swarms. It's based on the Boid algorithm from Craig Reynolds. The algorithm has been enhanced with feeding and mating behaviors. A very simple form of evolution is also modelled. So feed your fishes properly and see what the sea holds in store for you! Who knows, maybe you will even spot the famous jaguar shark!</p>

<h4><a href="http://plasma.kde.org/javascriptjam/2010/dice.plasmoid">Dice</a>, by Petri Damstén</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/dice.png" width="100" height="100">

<p>A Plasmoid that lets you roll dice, toss a coin, etc. Basically any simple random thing you can make an SVG for.</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/filtered-r.plasmoid">Filtered Reddit</a>, by Petras Zdanavičius</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/rredit.png" width="400" height="323">

<p>A Plasmoid for Reddit lurkers. It lets you re-filer Reddits even more:
<ul>
<li>Listing Order</li>
<li>Min score required for link</li>
<li>One plasmoid for each reddit</li>
<li>To show or not to show self.reddit kind of links</li>
</ul></p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/googlyeyes.plasmoid">Googly Eyes</a>, by Jaldhar H. Vyas</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/googlyeyes.png" width="142" height="103">

<p>This is a port of the eyesapplet which was in kdetoys in KDE 3.x but sadly never made it to 4.x. You can change the color of the eyeballs.</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/guitartuner-1.3.3.plasmoid">Guitar Tuner</a>, by Andrew Stromme</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/guitartuner.png" width="317" height="94">

<p>A Plasmoid to tune your guitar with!</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/hon-stats.plasmoid">"Heroes of Newerth" Stats</a>, by Henning Fleddermann</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/honstats.png" width="598" height="139">

<p>A Plasmoid that retrieves and displays statistics of <a href="http://www.heroesofnewerth.com">Heroes of Newerth</a> players, including the online-status. Its purpose is to always know when your (HoN-)friends are online and ready for a little game, and to show you yours and you friend's statistics, so you know who's the best, what you need to improve on, and so on.</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/instant-csi.plasmoid">Instant CSI</a>, by Matej Repinc</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/instantcsi.png" width="150" height="150">

<p>The Instant CSI Plasmoid: because sometimes, you just want to go out with a bang. It's not too complicated, but it shows the power of Plasma's Javascript API, and how awesome things you can do with a little amount of code.</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/screenshots/keystate.png">Keystate</a>, by Martin</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/keystate.png" width="171" height="134">

<p>A plasmoid that displays the status of some keys (for example 'Num Lock' and/or 'Meta pressed'). It is highly configurable:
<ul>
<li>you can choose between 4 different layouts</li>
<li>it can display the status of 9 keys</li>
<li>you can configure a color for each key</li>
<li>you can set the font for the text</li>
<li>you can enable the advanced layout settings which give you even more control over the layout of the plasmoid</li>
</ul>

The code is quite heavy as the total lines count is at 1379 (in 19 files - which makes ~73 lines per file)</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/make-progress-0.1.0.plasmoid">Make Progress!</a>, by Nikita Melnichenko</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/makeprogress.png" width="200" height="200">

<p>Track your progress in various tasks with this powerful and flexible Plasmoid. The <a href="http://plasma.kde.org/javascriptjam/2010/make-progress-0.1.0.engine">Make Progress DataEngine</a>, written in Python, is an optional though recommended add-on for this Plasmoid.</p>


<h4><a href="http://plasma.kde.org/javascriptjam/2010/shortlog.plasmoid">Shortlog</a>, by Manuel Unglaub</h4>

<img src="http://plasma.kde.org/javascriptjam/2010/screenshots/shortlog.png" width="400" height="173">

<p>Shortlog saves small information snippets for quick retrieval later on. The information snippets or notes get a timestamp to make it easier to find them again. The idea is to store information you have no good place for or might not need again in a simple way. But if you need them again, you can simply look them up.

How to use it:
<ul>
<li>Add a new note: insert it and hit "enter" or click "OK"</li>
<li>Search for a note: the first character needs to be "/", then just input your search pattern (regex possible) and click "OK"</li>
<li>Delete something: search for it and then click "Delete Selected" to delete all search results</li>
</ul>
</p>

<h3>More Than Just A Competition</h3>

<p>The Plasma Javascript Jam Session has been more than just a simple competition: it has also been instrumental in helping to discover the strengths and weaknesses in the Javascript Plasmoid system and introduce new faces to the KDE community. Each of the Jam Session participants became real-life test cases for Javascript Plasmoid creation, and via the IRC channel and the Plasma development mailing list they were able to offer extremely valuable input over the course of the Jam Session. This in turn resulted in a number of bug fixes, documentation improvements, feature additions and even helped to shape the feature goals for version 2 of the Javascript Plasmoid API which will be released this summer as part of the KDE  
Software Compilation version 4.5.</p>

<p>The Jam Session was also a great way for several people who had never had the opportunity to participate in KDE at the code level to do just that. The low barrier to entry of Javascript Plasmoids and the excitement of some very cool prizes both helped. We hope that this is just the start of an energetic, lively Javascript development community within KDE that can grow and flourish in the months and years to come. This is only the beginning, not the end!</p>

<p>So a big "thank-you" and "congratulations on your efforts" to <b>all</b> of the submitting authors for making the (first?) Plasma Javascript Jam Session a great success and an immensley enjoyable experience for the Plasma development team as well!</p>

<h3>You Be The Judge</h3>

<p>There is one last detail to sort out: the competition part of the Plasma Javascript Jam Session. With the submissions in the queue, it is now time to crown the stand-out submissions. One lucky winner will receive a brand new Nokia N900. The grand prize winner and the first runner up will both receive an invitation including transportation and lodging, to a KDE event, while the top three submissions will also earn a KDE t-shirt. We have a panel of <a href="http://plasma.kde.org/cms/1243">four judges</a> who will scoring all of the submissions based on usefulness or entertainment value, orginality, beauty and technical merit. In addition to the overal winners, the judges will be handing out the Beauty Queen, Technical Giant and Creative Genius bragging-rights awards</p>

<p>But wait! There's room for one more on that panel: <b>you</b>! To participate as a member of the "Fifth Judge" and help select the winners, head on over to the <a href="http://forum.kde.org/viewtopic.php?f=157&t=86964">Javascript Jam</a> thread on the <a href="http://forum.kde.org">KDE Forums</a> to try out the Plasmoids (instructions are included in the first post on the thread) and vote up your favourites. You have until April 8th to register your preferences!</p>