---
title: "KDE Celebrates Ada Lovelace Day"
date:    2010-03-24
authors:
  - "nightrose"
slug:    kde-celebrates-ada-lovelace-day
comments:
  - subject: "Awesome"
    date: 2010-03-24
    body: "\"Yay \\o/\" for Ada - the first programmer. She programmed the Babbage machine before it even existed, how awesome is that? \r\n\r\nAnd of course a \"Yay \\o/\" for the female KDE contributors who are extremely awesome, too!\r\nOh, and btw, that's a nice video you've created there.\r\n\r\nP.S.: Some of the male KDE contributors are awesomeish, too :)\r\n\r\n"
    author: "TunaTom"
  - subject: "Haha of course they are - all"
    date: 2010-03-24
    body: "Haha of course they are - all of them! :D"
    author: "nightrose"
  - subject: "Wikid."
    date: 2010-03-25
    body: "Happy to see KDE recognising some of our finest, against the odds. Would have loved to see everyone (well, at least more of everyone) make it into the video. Won't complain though as I didn't make *any* video. Congrats to whoever did and to the women of open source everywhere.\r\n\r\nHere's to many more. :)"
    author: "Bugsbane"
---
Today is <a href="http://findingada.com/">Ada Lovelace Day</a> - a day to celebrate the achievements of women in science and technology. KDE has always been very welcoming and diverse with a comparatively large number of female contributors among free software communities. For today, let's focus on the great work women do every day throughout KDE to help make it awesome.

<center>
<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/AuLdqbXzID0&hl=en_US&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/AuLdqbXzID0&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>
</center>
Unfortunately not in the video but still very awesome:  <a href="http://blauzahl.livejournal.com/">Alex</a>, <a href="http://www.wouwlabs.com/blogs/anniec/">Ana Cecília</a>, <a href="http://ekaia.org/blog/">Ana</a>, <a href="http://chani.wordpress.com/">Chani</a>, <a href="http://twitter.com/Frankfurtine">Claudia</a>, <a href="http://eng.i-iter.org/blogs/sabine-eller">Sabine</a>, <a href="http://twitter.com/messymother">Valerie</a>, <a href="http://identi.ca/valoriez">Valorie</a>, Vera and many more.


<a href="http://kde.org/community/getinvolved/">Will you be in next year's video?</a>