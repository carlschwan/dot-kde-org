---
title: "KDE Software Compilation 4.4 Release Candidate 1 Available"
date:    2010-01-08
authors:
  - "sebas"
slug:    kde-software-compilation-44-release-candidate-1-available
comments:
  - subject: "4.4 Release Parties"
    date: 2010-01-08
    body: "Don't forget to start planing for visiting/organising <a href=\"http://jussi01.com/?p=74\">4.4 release parties</a>."
    author: "JLP"
  - subject: "This looks very exciting!"
    date: 2010-01-08
    body: "This looks very exciting! There's a lot of new features I've been waiting for! :-D"
    author: "Mange"
  - subject: "anxiously watching the compile"
    date: 2010-01-08
    body: "The changes sound quite neat to me, especially the stronger nepomuk integration. But mostly I hope for a stability and speed improvement over the beta (I already use the beta as production system, and at time that hurts :) ). \r\n\r\nAlso I like having Blogilo officially included! \r\n\r\nI'm already anxiously watching my Gentoo compile - just 7 packages left to build. \r\n\r\nPS: For Gentoo users: just get the kde overlay via layman: \"emerge layman; layman -a kde\" - the rest is just unmasking and keywording, and the overlay gives pregenerated files for that in kde-testing/Documentation/\r\n"
    author: "ArneBab"
  - subject: "Congrats"
    date: 2010-01-08
    body: "To you hard working guys. I was using the Beta upto about 30 minutes ago - crash, crash, crash! It was very unstable but the RC is looking really good.\r\nOne question, are we going to get a new Theme?\r\n"
    author: "Bobby"
  - subject: "Air has been polished further"
    date: 2010-01-08
    body: "Air has been polished further and has a new \"fingerprint\", there's also a new default wallpaper.\r\n\r\nNo completely new theme though, but of course you can go wild and install something you like via gethotnewstuff."
    author: "sebas"
  - subject: "More speed won't hurt"
    date: 2010-01-08
    body: "I agree with you. I don't even want any new feature at this stage. KDE 4 is feature rich already. What I need is a click-and-not-wait Desktop that starts up very fast and of course doesn't crash."
    author: "Bobby"
  - subject: "Okay, Air is good enough. I"
    date: 2010-01-08
    body: "Okay, Air is good enough. I like the theme very much. With a little more transparence it would be perfect. I am good with that, won't put the guys under pressure ;)\r\n"
    author: "Bobby"
  - subject: "Kaffeine for KDE 4"
    date: 2010-01-08
    body: "I know that the KDE team is not directly responsible for the development of Kaffeine but maybe somebody here has a little knowledge and can give me a bit more insight on this subject. \r\nAll KDE 4 apps seem to be developing at the speed of light. Even K3b is very stable and functional in the meantime but Kaffeine, yes my favorite Multimedia app seems to be just crawling along. What is wrong? Can the KDE team give the responsible persons a helping hand?"
    author: "Bobby"
  - subject: "I am wondering if there is"
    date: 2010-01-08
    body: "I am wondering if there is duel/triple head suport in kde 4 by now. \r\nIt should be partly fixed but does it work perfectly? See comments 46 and onward:\r\nhttps://bugs.kde.org/show_bug.cgi?id=156475"
    author: "beer"
  - subject: "I guess you mean dual head"
    date: 2010-01-08
    body: "I guess you mean dual head and not duel head?"
    author: "Bobby"
  - subject: "How?"
    date: 2010-01-08
    body: "I didn't want to disturb my ARCH/KDE 4.3.4 setup, so after seeing that there where binary packages for openSUSE I decided to install it into a different partition. However after opening the file, YaST tells me that it can't resolve over a few dozen dependencies. Does anyone have a better idea for testing this?"
    author: "reub2000"
  - subject: "crashes all the time for me"
    date: 2010-01-08
    body: "I just upgraded from 4.3.4 and I can't do anything at all without the plasma desktop shell crashing and getting a black screen. For me it is completely unstable and horrible. Wish I hadn't upgraded. will probably need to completely re-install or use Gnome instead... when will I learn not to trust betas/rc's. this really sucks.... "
    author: "bjb1959"
  - subject: "Leftover pieces?"
    date: 2010-01-09
    body: "Most of the times this happens if you are mixing stuff from 4.4 and 4.3.x. Are you sure you've fully upgraded?"
    author: "einar"
  - subject: "If you have the repositories"
    date: 2010-01-09
    body: "If you have the repositories installed then use the command zypper dup in the Terminal (as root) to upgrade. That might resolve the most dependencies for you."
    author: "Bobby"
  - subject: "Try by upgrading all"
    date: 2010-01-09
    body: "Try by upgrading all packages, not only the KDE ones. RC1 is very stable. I haven't had one crash after using it for hours yesterday."
    author: "Bobby"
  - subject: "Okular (Note for LaTeX users)"
    date: 2010-01-09
    body: "Whenever a document is reloaded Okular jumps back to the first page. This is pretty bad for us LaTeX users who keep recompiling documents. This has been bug reported but not fixed yet. So LaTeX users might want to keep Okular from KDE 4.3.\r\n\r\nOther than that, so far so good. \r\nGood stuff. Well done guys! :)\r\n\r\n"
    author: "jadrian"
  - subject: "Fast!"
    date: 2010-01-09
    body: "I just upgraded my Arch install and I have to say: Wow! Plasma feels lightning fast and everything is really smooth. I have been running 4.4 now and then since before Beta 1 and both the changes from 4.3 to 4.4 and 4.4 pre-beta to RC1 are amazing. As a special note I's like to thank the oxygen huys for using the animation framework in 4.6 in a great way. I love the small animations everywhere and it really sets 4.4 apart for me."
    author: "ahiemstra"
  - subject: "Do you know the workaround?"
    date: 2010-01-09
    body: "Press \"Ctrl + F, Esc\" after the document has reloaded."
    author: "majewsky"
  - subject: "Yeap"
    date: 2010-01-09
    body: "Yes I read it on the bug report. Not so great. \r\n\r\nWhen typesetting LaTeX it's usual to keep compiling and looking at the result. I definitely want to see the update almost immediately and resume typing. Having to go back and forth in my document each time just doesn't work for me.\r\n\r\n\r\n(it's bug n. 199184 if anyone's interested). "
    author: "jadrian"
  - subject: "Kwallet and PAM ??"
    date: 2010-01-09
    body: "There was plans for PAM in kwallet. I saw that in Feature Plan. Does that figure out in kde 4.4 ? It a handy feature to have."
    author: "jithin1987"
  - subject: "UPDATE"
    date: 2010-01-09
    body: "I deleted my .kde folder and started over and now it seems fine, although I can't get dpms to stay off but that is minor."
    author: "bjb1959"
  - subject: "Thanks"
    date: 2010-01-09
    body: "Thank you so much, on behalf of the oxygen team thanks, there was great concern making it an mostly non-obstructive experience, that enables the possibility of having smooth transitions, between states without undermining the speed you can do stuff.\r\nFor 4.5 we will continue to try to improve the experience.    "
    author: "pinheiro"
  - subject: "Dual-head support has been"
    date: 2010-01-09
    body: "Dual-head support has been just fine since 4.3."
    author: "reub2000"
  - subject: "Fast and lovely"
    date: 2010-01-09
    body: "I agree that those animations rock. I always set the kwin animations' speed to instant, but those oxygen animations I really do appreciate."
    author: "lucke"
  - subject: "It looks like we two got"
    date: 2010-01-09
    body: "It looks like we two got caught up in a duel. Let's see how often you can bury me. It's now two to nil ;)"
    author: "Bobby"
  - subject: "Debian packages?"
    date: 2010-01-09
    body: "Do you know any server where I can find the kde 4.4 SC RC1 packages for debian?\r\n\r\nThanks in advance!"
    author: "Damnshock"
  - subject: "Wired. If it works how cn I"
    date: 2010-01-10
    body: "Wired. If it works how cn I get 2 black screesn (2 out of 3) using debisn sid with kde 4.3.4 and another user says he have the same problem with opensuse 11.2 with kde 4.4 b2 \r\nMr  Aaron J. Seigo says it is a \"there is no dbus session bus that matches the x.org  display and so all dbus calls fail and therefore the unique apps also fail\" kind of bug\r\n "
    author: "beer"
  - subject: "maybe powerdevil is getting in the way?"
    date: 2010-01-10
    body: "DPMS can also be managed by PowerDevil, check your Power Management settings in System Settings."
    author: "sebas"
  - subject: "Just a warning to passers-by,"
    date: 2010-01-10
    body: "Just a warning to passers-by, deleting .kde or .kde4 is not recommended as you may lose valuable data, e.g. your email archive, password wallet, bookmarks, etc.  Instead rename it as something like .kde4-2010-01-05 so you can recover the required files later.  Better yet, just delete the plasma*.rc files first to see if that works."
    author: "odysseus"
  - subject: "First off, the animated"
    date: 2010-01-11
    body: "First off, the animated opening of the application launcher seems jerky. And most of the animations just come off as tacky."
    author: "reub2000"
  - subject: "not a oxygen animation"
    date: 2010-01-11
    body: "That is not an oxygen animation. The oxygen animations are just the ones that go on inside the window and the shadow transition wen changing windows.\r\n\r\nKwin animations depend on the a lot more variables than the oxygen ones, so they do not act 100% the same in all systems and Beauvoir changes a lot depending on your hardware and drivers.  "
    author: "pinheiro"
  - subject: "Works pretty well here - the"
    date: 2010-01-11
    body: "Works pretty well here - the updates to KWin are especially awesome."
    author: "barkholt"
  - subject: "Activities"
    date: 2010-01-11
    body: "I just upgraded to RC1 on Mandriva Cooker and it seems to have screwed up my activities.  Let me explain better what I mean.  For a while now I have had my system set up to use an activity per desktop (I use 4 desktops).  This way I could have a separate setup (wallpaper, etc.) per desktop.  This was all retained when using the previous betas of SC 4.4.  However, once I upgraded to RC1 and logged in, all my wallpapers and activity types were changed.  I decided to rename my plasmarc file and configure from scratch, but here is the issue I'm having with that.  Previously, to add or select an activity I would click the kidney and select \"zoom out\".  From there I could add/delete/select an activity.  I could also change the activity settings (assign activity per desktop) there.  Now, when I zoom out I cannot select an activity (no control handles).  So my question is this.  Has activity configuration moved to another location?  Because even though it's never been that smooth, as of now the zooming function seems to be less stable than ever."
    author: "cirehawk"
  - subject: "notifications"
    date: 2010-01-12
    body: "I [X]\r\nhate [X]\r\nthe notification [X]\r\nsystem [X]\r\n\r\nPlease revert to beta2 or 4.3.x. What you're trying to do in 4.4 beta1 and rc1 is absolutely annoying.\r\n\r\n"
    author: "nbensa"
  - subject: "Could you please elaborate a"
    date: 2010-01-12
    body: "Could you please elaborate a bit? Being a 4.3.4 user, I'm curious about what was changed and why it's bad."
    author: "Hans"
  - subject: "Those contain data as well, sometimes ..."
    date: 2010-01-13
    body: "The Plasma config files contain data as well (for example the text of the notes on the desktop). So also here goes: move it out of the way rather than just deleting it."
    author: "sebas"
  - subject: "It's crawling because Phonon is crawling."
    date: 2010-01-13
    body: "Kaffeine decided to use Phonon from the start and at the moment Phonon still has only 2 backends both which aren't in any way configurable (been like that for 2 years now unfortunately). That's why devel kaffeine for kde4 feels more crippled compared to the kde3 version because of Phonon you can't configure any video/audio settings in kaffeine at the moment.\r\n\r\nKind of hoped they've ported it old fashioned way and used Phonon only when it's ready for such an advanced app."
    author: "bliblibli"
  - subject: "I believe"
    date: 2010-01-13
    body: "I believe he means the new groupping for notifications. The different app notifications are groupped to own tabs what is bottom of all notifications. So in the end, you can not see all notifications at once but you need to go trought \"tabs\" to close them all. \r\n\r\nIt was terrible on beta2 but now on RC1 I have not even noticed such bad thing, even that I saw it is still there."
    author: "Fri13"
  - subject: "I believe"
    date: 2010-01-13
    body: "I believe he means the new groupping for notifications. The different app notifications are groupped to own tabs what is bottom of all notifications. So in the end, you can not see all notifications at once but you need to go trought \"tabs\" to close them all. \r\n\r\nIt was terrible on beta2 but now on RC1 I have not even noticed such bad thing, even that I saw it is still there."
    author: "Fri13"
  - subject: "Not convinced"
    date: 2010-01-13
    body: "Firstly, the pre-release of Kaffeine built on Platform 4 works pretty well for me (I think it's the default in Fedora now). I does have less config options, like many apps when first ported to Platform 4. You can change aspect ratio at least so it's not true that you can't configure any settings.\r\n\r\nRegarding only two backends for Phonon - well, Kaffeine only really had the one backend in KDE 3 (Xine - was there also a Gstreamer backend option in theory? I can't remember).\r\n\r\nI guess the question I'd like to ask is what's missing - other than a final release?"
    author: "Stuart Jarvis"
  - subject: "If you are building from source"
    date: 2010-01-19
    body: "First, I suggest that rather than downloading the tarballs that you check out from SVN with -r1071347.  This is the same code and you will them be able to update without discarding everything and starting over.\r\n\r\nAgain, I point out that the link on this page:\r\n\r\nhttp://www.kde.org/info/4.3.90.php\r\n\r\npointing to instructions for building from source is for building TRUNK.  The correct URL for building 4.x releases is:\r\n\r\nhttp://techbase.kde.org/Getting_Started/Build/KDE4.x\r\n\r\nUnfortunately, I have to say that the instructions for building TRUNK are not maintained and are somewhat outdated.  However, you may find them useful in regard to finding the needed dependencies.\r\n\r\nI have found some issues with dependencies.  You will need to install KDESupport from TRUNK since there isn't a tagged version yet.  There is an issue with Redland.  Soprano currently requires Redland <= 1.0.8.  If you use a newer version, part of Redland RDF support in Soprano will not be built.  Redland uses two supporting libraries.  You should install the latest version of Raptor which is currently 1.4.20 because 'TriG' is required.  You can use the latest version of Rasqal which is 0.9.17.  \r\n\r\nThe Wiki page is a work in progress and I will be updating it with the above and any other issues which I find as I build and install 4.4."
    author: "JRT256"
---
KDE has made the first release candidate of the KDE Software Compilation 4.4 available. The new features of KDE SC 4.4.0 can be found in the <a href="http://www.kde.org/announcements/announce-4.4-rc1.php">official release announcement</a>. A more detailed list is available on <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Release_Goals">TechBase</a>. Please report bugs you find in this release to <a href="http://bugs.kde.org">KDE's Bugzilla</a>.<br />
The first KDE SC (KDE SC, <a href="http://dot.kde.org/2009/11/24/repositioning-kde-brand">what's that?</a>) release (albeit not a stable one) is the first release candidate for the upcoming KDE Software Compilation 4.4.0 which will be released on February 9th. In between, there is a second release candidate planned already, RC2, on 20th January. In the 4.4 beta cycle, we have reduced the time it takes us to package and deliver the test releases, so our testers get the freshest tarballs ever, leading to shorter test-and-fix cycles. KDE SC 4.4.0 is also the first release to be based on Qt 4.6 which became available in December. We have therefore decided to insert another release candidate to make sure we deliver the best KDE Software Compilation to date with KDE SC 4.4.0.
<!--break-->
As usual, the source tarballs can be downloaded from various mirrors, spread across the world. Packages for a number of distros will become available shortly, as the packages are still very fresh, and not everybody has had the chance to fully build them yet.<br />
This release candidate is named after Cornelius Schumacher, in order to worship his continued awesomeness and his contributions to KDE as a whole.<br />