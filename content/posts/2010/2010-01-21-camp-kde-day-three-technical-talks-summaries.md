---
title: "Camp KDE Day Three Technical Talks Summaries"
date:    2010-01-21
authors:
  - "troy"
slug:    camp-kde-day-three-technical-talks-summaries
---
The third day of talks at Camp KDE was somewhat shorter, due to the afternoon Cmake training provided by Marcus Hanwell of Kitware. However, in order to provide complete coverage of the talks for the readers of the dot, summaries of the third day's technical talks are provided within.
<!--break-->
<h2>Stephen Kelly: Akonadi Models and Proxies: Tips Tricks and Hacks</h2>

First talk of the day was by KDE PIM developer Stephen Kelly. Akonadi is a generic interface to access your notes, email, and other PIM data. Stephen walked us through the model view concept, the features of the Akonadi model-view, some challenges the developers faced during development  and how the rest of the KDE community could take advantage of Akonadi.

A simple way to share PIM information between applications is to simply code the apps to directly communicate with one another, as was done in KDE 3 times. For KDE SC 4, the Akonadi framework was introduced as a central, pluggable storage interface. It ensures that there are no conflicts (for example, two applications trying to save data simultaneously to the same file or online service), and allows access to the data without having to start the applications.

To present the data within the applications, Akonadi provides a QAbstractItemModel which can be directly linked to a view within an application such as KMail. Stephen demonstrated a number of models and views, including the EntityTreeModel, which represents all data provided by Akonadi. Applications like KMail will want to restrict the displayed information to provide a good user interface, rather than a dump of all the information available.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/qttraining.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/qttraining_small.jpg" width="300" height="225"/></a><br />Qt training in progress</div>

Stephen then walked us through a number of KDE classes which extend the functionality of Qt to provide an easy way to select and filter for the desired information. He gave examples from KMail and KOrganizer. He demonstrated the advantage of the model-view architecture by showing that email could be displayed using the category view from SystemSettings. Lastly, he demonstrated using Nepomuk and Akonadi in conjunction to sort PIM information by tags. 

Finally, he walked through a number of the test applications and processes in place to ensure that everything functions as expected. There are hundreds of such tests, and there is also a dynamically scripted test application to observe the behavior of the proxy models.

<h2>Will Stephenson: Spreading KDE with the openSUSE Build Service</h2>

Will returned to the stage for a second talk this week to discuss the openSUSE Build Service. His key message was: the build service is for everyone. It is composed entirely of free software using GPL and similar licenses and supports a myriad of distributions. Since the openSUSE hosted version only allows free software to be built with the service, other companies have been downloading and installing a local instance of the software for their own build farms.

The build service is more than just a build farm – it is a software repository that builds for numerous architectures and distributions, complete with dependency management and package signing. It can publish the results in distribution repositories to make downloading and installing seamless to their users. It is used not just meant to build single packages, but can be used to build entire distributions. During the openSUSE release deadlines, it is rebuilding the entire distro nearly every twenty-four hours.

Will introduced us to a number of clients for using the build service. There are web clients, python based command line tools (similar to Subversion) called OSC (openSUSE Source Commander), and a mono-based user interface. Will gave some examples, showing how to build KDE SC 4.4 RC2 for Fedora 12.

Will then presented a prototype help tool called "kde-obs-generator". It takes a tarball as input, scans it, determines if it is a Plasma Applet, a KDE Application, a wallpaper, and so forth. The output includes the required RPM spec files, or Debian control files, and can be very quickly submitted to the build service. He demonstrated this for the application kvkbd.

Recently, connections have been created directly for kde-look.org and kde-apps.org. These packages are now being automatically created giving access to pre-built packages for users of several distributions.

<h2>Jeff Trull: Qt + Salesforce</h2>

Jeff Trull talked about Salesforce.com and his work on integrating their services into Qt and KDE. In principle, the site is a typical modern web 2.0 website, consisting of a bunch of tables, queries and generated displays. They provide SOAP and REST APIs to the outside world.

For using SOAP from free software, the common options are Apache Axis2, QtSoap, and gSoap. Salesforce examples were provided only in Java and Objective C. gSoap provided mixed results, but those were sufficient to get to the next step: providing Salesforce data as a Qt Table model. This included lazy data transfer, handling of untyped data, and provide a reasonable fit for the existing data.

QAbstractTableModel was the first consideration, but QSqlTableModel ended up being the best match. It understands tables and authentication, lazy fetching and varying data types. Fortunately, Salesforce data could be accessed through a SQL query. Finally, QSqlQueryModel was chosen, and Jeff created a new SQL driver based on the SQLLite driver.

Jeff showed an example of a table view that pulled contact information on the fly from Salesforce. The next step was to create an Akonadi resource so that the contacts could be directly available within KDE. He showed the contacts showing directly from KAddressBook.

In the end, the discussion centered around creating or fixing Akonadi resources for other websites and services.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/feedinghackers.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/feedinghackers_small.jpg" width="300" height="225"/></a><br />Developer food</div>

<h2>David Wonderly: Documentation</h2>

After breaking for lunch, the conference continued with a short talk about the current state of KDE documentation by David Wonderly. He had this comment about documentation: “It has no use if it doesn't answer the questions that people ask.” He uses this to show that the docs should be mirroring the questions being asked in email, IRC, or other KDE support channels.

He provides a number of tips. Excessive documentation is worse that bad documentation – keep it simple, avoid tl;dr. This helps to maintain the document over time as the software is changing. Documentation needs to be well outlined, and clearly flowing. Avoid processes that take more than ten steps, and if it becomes more involved, break it into smaller walkthroughs. And lastly, “avoid fluff.” A long and complicated sentence is difficult to maintain and translate. He gave a few examples of sentences that can be rewritten for additional clarity and ease of translation.

<h2>Adenilson Cavalcanti: Plasma New Animation Classes</h2>

Adenilson of INdT provided an overview of the animation framework in place in plasma libraries, integrated into Software Compilation 4.4. This was made possible by the introduction of the Kinetic classes in Qt 4.6, which allow for more realistic animations.

In one month of work, proof of concepts were developed for the Kinetic scrolling list, drop shadows, stacked rotations, fades, and a pulse effect. Now we're four months, 180 commits and several refactorings by a group of five KDE Plasma hackers later.

After having been distracted by pretty photos of Brazil, Adenilson found his second set of slides, and continued with some live demonstrations of a number of animations. These included examples of the animation classes running on phones and netbooks, showing that these animations can easily run on devices low on resources.

He showed a number of code snippets and described how to add the new animation classes in a few lines. This was followed by examples on how to update current plasma widgets to take advantage of the new animation capabilities, with the dictionary widget being the featured example. He handed around a phone to show off the animation examples.

<h2>Adenilson Cavalcanti: KDE-PIM Google Sync</h2>

In an Adenilson double feature, he returned immediately to talk about implementation of KDE-PIM support for Google Calendar and Contacts using Akonadi in KDE 4.3. This is related to the previous talk given by Jeff and is one of the multitude of data providers being created for Akonadi.

He showed a video of contacts being added in KAddressBook just like a locally stored contact, then he logged into Google Mail to find his contacts there, including details and pictures. This was followed by another video showing a calender item being created which was seamlessly synchronized with Google Calendar. The total of this code base was over 700 commits, and 7000 lines of code. Before implementation, this was one of the most highly voted feature requests on the KDE <a href="http://bugs.kde.org/">bugtracker</a>.

The backend is implemented using Google's REST + atom (HTTP + XML) APIs. Authentication requires HTTPS post to get an authorization token and session id. Examples of the protocol were demonstrated using screenshots from Wireshark, a network tool used to inspect traffic.

He then went through the design of the Akonadi plugins required to get Google synchronization, including issues with determining what has changed on the Google servers, and reverse engineering of the protocol.

This concluded the third day's technical talks at Camp KDE, although scheduled training sessions for CMake, Qt and Qt/Embedded happened later in the event. These trainings were of particular high quality according to the comments from many attendants, and will continue for the remainder of the week (with time for coding in between).