---
title: "Sentinella Waits So You Don't Have To"
date:    2010-10-26
authors:
  - "Stuart Jarvis"
slug:    sentinella-waits-so-you-dont-have
comments:
  - subject: "KDevelop improvements?"
    date: 2010-10-26
    body: "Hey there,\r\n\r\nif you want to help us out come and contact us :) You could start by making the \"select process\" dialog you also use a proper KDELibs feature.\r\n\r\nBye"
    author: "Milian Wolff"
  - subject: "Reminds"
    date: 2010-11-05
    body: "This reminds the great functionality in the Ktorrent to shutdown/suspend/hibernate the computer after specific torrents have been downloaded or finished the seeding.\r\n\r\nThe ktorrent has a easy way to make it. And same thing would be here if just would be possible just point and click the task what should be finished and after that then sleep the computer."
    author: "Fri13"
  - subject: "kshutdown"
    date: 2010-11-08
    body: "seems like an interesting app, although kshutdown seemed to fulfill all my needs up until now :) (it also supports different kind of triggers)"
    author: "mxttie"
---
Have you ever started an update, download or file transfer only to find that you need to head off, so you leave your computer running to complete that task? Wouldn't it be great if there was a simple application that could monitor your computer and shut it down, suspend or hibernate it at the right time while you were not there?

<a href="http://sentinella.sourceforge.net/">Sentinella</a> can do all that and more. Built on the KDE Platform, it integrates perfectly into a Plasma Workspace and is probably already available in your distro repositories.

<b>Ryan Rix</b> interviewed Sentinella's author, <b>Carlos Olmedo Escobar</b>, to find out more about the application, future plans and the reasons for building Sentinella on the KDE Platform.

<h2>Carlos on Sentinella</h2>

<div style="text-align:center; float: right; border: 1px solid grey; padding: 1ex; width: 303px; margin: 1ex"><a href="http://dot.kde.org/sites/dot.kde.org/files/senShot1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/senShot1-sml.png" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/senShot2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/senShot2-sml.png" /></a>
<a href="http://dot.kde.org/sites/dot.kde.org/files/senShot3.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/senShot3-sml.png" /></a><br />Sentinella in action - click on the screenshots for enlarged views</div>
<b>Ryan: </b>What is Sentinella and what does it do?

<b>Carlos: </b>Sentinella is a desktop application that lets you choose a condition of the system's activity and an associated action that should be triggered when that condition is met. It can measure the activity of your processor, memory, hard disks, network interfaces. Whenever it reaches some limit it will execute a chosen action, such as shutdown, reboot or hibernate your computer or trigger an alarm.

<b>Ryan: </b>Is there any other application similar to Sentinella? If so, what makes Sentinella different?

<b>Carlos: </b>The main reason I did this project is because there not another app like Sentinella. In fact, with any other similar app, you are limited to shutdown or reboot your system at a specific time. There is not another app that lets you react based on so many conditions and with so many actions. Sentinella is unique! ;)

<b>Ryan: </b>Why did you choose to base the software on the KDE Platform?

<b>Carlos: </b>When I first thought about Sentinella, I did it as a programmer and as a user. I wanted it to integrate perfectly with my KDE desktop. Also I find the KDE Platform very accessible as a programmer, and the community is really helpful. That's all that I can ask for!

<b>Ryan: </b>Does the KDE Platform work well for you or are there some things that should be improved?

<b>Carlos: </b>Well, I see that all the pieces of the Platform are in very good shape but I would like to see some improvements in KDevelop - in my opinion it still needs more plugins and stability.

<b>Ryan: </b>What motivated you to write Sentinella?

<b>Carlos: </b>I was searching for a project that people wanted to use so I can give something back to the community. I always missed an app like this one and it was an excuse to do something my way.

<b>Ryan: </b>How long have you been involved in KDE software development?

<b>Carlos: </b>I have been around for many years but I decided to write this app one and a half years ago.

<b>Ryan: </b>What features do you plan for the next Sentinella release?

<b>Carlos: </b>The next release will bring more translations and also the "What is this?" feature so it will be easier to use for beginning users. Also I'm working on a big release of <a href="http://sourceforge.net/projects/libsysactivity/">libsysactivity</a> which is the library that Sentinella uses for getting activity stats from the system.

<b>Ryan: </b>What do you do in your day job and spare time?

<b>Carlos: </b>I work as a programmer but I like to use my spare time in traveling (as much as I can), sports and whatever activity that I can enjoy with my friends!

<b>Ryan: </b>Thanks very much for the interview and good luck with Sentinella.
