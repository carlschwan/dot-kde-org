---
title: "Anthony Kolasny Explains How KDE Software is Used At Johns Hopkins University"
date:    2010-09-21
authors:
  - "Justin Kirby"
slug:    anthony-kolasny-explains-how-kde-software-used-johns-hopkins-university
comments:
  - subject: "Yay"
    date: 2010-09-21
    body: "Neat! I'm happy to see KDevelop being used in such deployments!\r\n\r\nGood luck!"
    author: "apol"
  - subject: "What's on the left?"
    date: 2010-09-22
    body: "What's the name of the application on the left screen? Picture - \"Plasma Desktop, KDevelop and Konsole at JHU\""
    author: "Pawlerson"
  - subject: "Brainworks"
    date: 2010-09-22
    body: "The application is called BrainWorks.  It was developed by the Center for Imaging Science at Johns Hopkins University.  BrainWorks provides functionality for visualization, analysis, and editing of image data in 3D volume, surface, and landmark format.\r\n\r\nThe photo shows the user setting up to segment scan data into different tissue types by clicking peaks in the histogram near around which he believes the grayscale values of the given tissue type cluster.  The program automatically fits gaussian curves to the histogram and tags each voxel in the image with a tissue type designator."
    author: "M. Bowers"
  - subject: "You can read about it"
    date: 2010-09-27
    body: "You can read about it here:\r\nhttp://cis.jhu.edu/software/brainworks/\r\n\r\nIt has little/nothing to do with KDE ofcourse."
    author: "kolla"
---
While it is easy to focus on many other strong points of KDE software, one aspect that deserves a closer look is the ability for it to support science. Back in July, you may have caught the <a href="http://dot.kde.org/2010/07/02/kde-and-science">Dot story on "KDE-Science"</a> discussing the background and initial call for engaging the scientific community. Today we would like to highlight some of the advances that have occurred since then and present a real world example of how KDE software is already helping to support research.
<!--break-->
<h2>KDE and Science</h2>

Since the publication of that first article, several practical steps have been taken. Our first follow up article, an <a href="http://dot.kde.org/2010/09/13/kde-science-thomas-friedrichsmeier-rkward-toolkits-and-kde-platform">interview with RKWard developer Thomas Friedrichsmeier</a> was published just last week. There has been a <a href="http://forum.kde.org/viewforum.php?f=199">new forum</a> established for discussing specific KDE applications for science, the use of KDE Workspaces for science, and promotion of KDE software and our community among scientists. There has also been a revival of the <a href="https://mail.kde.org/mailman/listinfo/kde-science">KDE-science mailing list</a>. If you have interests in these subjects, please consider joining the conversations.

<h2>KDE Software in the Lab</h2>

While it may seem our efforts are just getting (re)started, there are already users in the scientific community who are leveraging KDE Workspaces in their daily research.  Below is an interview  that highlights one example of how KDE software has already found its way into the lab.  Anthony Kolasny is an IT Architect at the Center for Imaging Science at <a href="http://www.jhu.edu/">Johns Hopkins University</a> (JHU) in Maryland, USA. 

<strong>Justin:</strong> Thanks for taking the time to chat with us Anthony.  So what is it exactly that you do as an IT architect at JHU?

<strong>Anthony:</strong> Essentially, my job is to help provide a stable hardware and software environment so that users can be most productive. I also help manage the various research projects to ensure that milestones and goals stay on target.

<strong>Justin:</strong> Would you tell us a little more about your lab and what kind of work your team does?

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/cis_2009-big.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/cis_2009.jpeg" /></a><br/>Staff at JHU's Center for Imaging Science</div><strong>Anthony:</strong> The Center for Imaging Science is part of the Department of Biomedical Engineering under the Whiting School of Engineering at Johns Hopkins University. Centers at the University are solely research driven. They do not have teaching responsibilities.

Our major focus is exploring various aspects of medical imaging. We use the term "Computational Anatomy" for this field of study. It includes developing algorithms in order to register, segment and quantify shape within anatomical structures. This provides the basis for statistical analysis within populations. (Reference: http://portal.acm.org/citation.cfm?id=309089)

<strong>Justin:</strong> How is your team currently using KDE software?

<strong>Anthony:</strong> Much of our code development involves the algorithmic development of Large Deformation Diffeomorphic Metric Mapping (LDDMM) and GUI plugin development using <a href="http://www.paraview.org/">Paraview</a>. Programmers mainly use C++ with common utilities and libraries like CMake, the Visualization ToolKit (VTK) and Insight Toolkit (ITK). KDevelop is a natural environment to do this development.

<strong>Justin:</strong> How was the decision made to adopt KDE software in your workplace?  Were there any significant challenges in doing so?

<strong>Anthony:</strong> It was a gradual evolution in our environment. Around 2000, our lab was dominated by RedHat Enterprise Linux and Microsoft Windows on the desktop. Desktops and servers migrated to CentOS. Later, users requested Ubuntu due to the need for more current applications in order to support their research. We had some minor annoyances with users switching between GNOME on CentOS and Ubuntu machines. Kubuntu and KDE resolved the issues. Users became accustomed to this environment and started exploring its features. KDevelop has been a useful tool. The KDE workspace best met the needs of the users.

<strong>Justin:</strong> What are the major advantages to using KDE software in your office?

<strong>Anthony:</strong> Users enjoy the KDE software environment. They like the rich collection of applications. I ensure a common core of non-standard applications on all desktops, which provides consistency and familiarity within our environment.

<strong>Justin:</strong> What does your team like about KDevelop that gave it an edge over other options that were available?

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/IMG_3900-big.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/IMG_3900.jpg" /></a><br/>Plasma Desktop, KDevelop and Konsole at JHU</div><strong>Anthony:</strong> The majority of software that we utilize is open source. It's natural to use open source development tools as well. KDevelop provides good functionality for our needs and has dedicated developers who are constantly improving the product. The integration with CMake is very useful for our Paraview development.

The advantage of free development tools is that as our development team expands or we interact with other groups, the development environment does not incur large costs in order to collaborate. We try to avoid expensive development tools.

However, not all of our development is done with free and open source software. For computationally intense algorithms, we utilize Intel compilers and Math Kernel Library (MKL), which allows for more highly optimized code on multicore systems. Our lab tries to be practical in the development of quality code that takes advantage of high end hardware.

<strong>Justin:</strong> Has your group committed any code upstream?  If not, do you expect that you might in the future?

<strong>Anthony:</strong> We have committed code to Paraview and ITK. As our applications mature, we foresee greater use of Qt development. If KDE tools assist in that development, they will be utilized. I see our group pursuing Qt Webkit development in order to glue together the web service of our medical imaging database and the visualization tool based on Paraview.

<strong>Justin:</strong> Ok, and one last question.  What would you advise aspiring developers interested in science, or more specifically imaging, about meaningful projects to pursue?  Is there some major gap in existing software that you wish someone would fill?

<strong>Anthony:</strong> Pursue your interests. There is plenty of research to explore. A simple email to a research group may be all that is needed. We have sponsored several students from around the country to participate in summer projects. Some were paid while others came to learn and to see if this would be a desired career path. These summer projects may develop into research grant proposals which may lead to useful applications that one day may turn into companies. Good ideas usually start with an interesting problem and a desire to solve it.  If anyone is interested in extending Paraview for medical imaging applications next summer, <a href="http://www.cis.jhu.edu/~akolasny/">send me an email</a>.