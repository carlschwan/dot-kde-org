---
title: "Grantlee Version 0.1.0 Out"
date:    2010-04-12
authors:
  - "steveire"
slug:    grantlee-version-010-out
comments:
  - subject: "Interesting!"
    date: 2010-04-12
    body: "I'd love to see Grantlee be used more often, it seems like a neat (and consistent) template solution. Especially because it uses Django syntax for the templates.\r\n\r\nI would love to see a nice homepage to make it spread beyond KDE. (i.e. examples, FAQ, and info about compatibility with Django) "
    author: "vdboor"
  - subject: "I'd like it to have a nice"
    date: 2010-04-12
    body: "I'd like it to have a nice webpage too, but I don't have much talent with that stuff.\r\n\r\nIf you do and you would like to help, drop an email to kde-pim@kde.org."
    author: "steveire"
  - subject: "Thanks for the offer!"
    date: 2010-04-13
    body: "Wish I could help you soon, unfortunately I already have another site on my list to do first.\r\n\r\nI'm glad though to read it's not an oversight, but rather a not-gotten-to-it-yet kind of issue. Perhaps someone else could offer you a hand sooner then I can?"
    author: "vdboor"
---

<a href="http://downloads.grantlee.org">Grantlee version 0.1.0</a> has been released by the Grantlee team. For those not in the know, Django is a high-level Python Web framework. It can be used to develop websites, and offers numerous features to make such a task easier.

<a href="http://docs.djangoproject.com/en/dev/topics/templates/">Django's template system</a> is designed to separate content and presentation with a clean and easy yet powerful syntax. Grantlee is a string template library based on the syntax and design of this template system and is written in Qt. It provides the possibility of <a href="http://steveire.wordpress.com/2009/08/11/email-rendering-with-grantlee">user extensible theming</a> to Qt and KDE based applications, and provides tools for other template based document creation including <a href="http://steveire.wordpress.com/2010/04/06/using-grantlee-for-code-generation">code generation</a> and mail-merge.

<div style="float: center; padding: 1ex; margin: 1ex; border: thin solid grey; width: 700px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/mailapp-07theme.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/mailapp-07theme_small.png" width="350" height="211"/></a><a href="http://dot.kde.org/sites/dot.kde.org/files/mailapp-09theme.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/mailapp-09theme_small.png" width="350" height="211"/></a><br />Grantlee makes it easier for developers and designers to create flexible and attractive theming.</div>

As it is based on Django, there is already a community of designers familiar with the syntax, and a community of developers implementing logical fixes and writing new extensions.

This 0.1.0 release of Grantlee will initially be used by <a href="http://dot.kde.org/2010/02/17/kjots-takes-advantage-innovations-kde-development-platform">KJots</a> in the 4.5 release, and by the <a href="http://mupuf.org/project/arduide/">Qt based Arduino IDE</a> for theming and code generation. It will soon be making more appearances in KDE PIM applications for theming support. A Google Summer of Code project is already proposed to bring Grantlee and GetHotNewStuff support to KMail for user defined themes.

Visit <a href="http://grantlee.org">grantlee.org</a> for more information and downloads of Grantlee! You're also more than welcome to join the Grantlee team in developing Grantlee. Merge requests can be send in through gitorious.