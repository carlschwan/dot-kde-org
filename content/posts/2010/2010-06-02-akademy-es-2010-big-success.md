---
title: "Akademy-es 2010 Big Success"
date:    2010-06-02
authors:
  - "acid"
slug:    akademy-es-2010-big-success
---
Every year, the KDE community in Spain organizes a local Akademy event: Akademy-es. <a href="http://www.kde-espana.es/akademy-es2010">This year's event</a> was held in <a href="http://en.wikipedia.org/wiki/Bilbao">Bilbao</a> from 7th to 9th of May. The event gathered around 80 KDE contributors, users and Free Software enthusiasts from all over Spain and even people from other countries such as France and Ireland.

The event started on Friday afternoon with a talk by <a href="http://www.danitxu.com">Dani Gutiérrez</a> (professor from the University of Basque Country) about KDE on Windows and how it can help Windows to Linux desktop migrations. After that, <a href="http://www.afiestas.org/">Alex Fiestas</a> of KBluetooth fame gave an overview talk on KDE libraries, explaining where we come from and where are we headed in the KDE Development Platform.

The event moved to the social side with a boat trip over the <a href="http://en.wikipedia.org/wiki/Nervion">Nervión river</a> followed by a viewing of the <a href="http://patentabsurdity.com/">Patent Absurdity</a> documentary. While most of the attendees agreed it was interesting to watch the documentary it was also agreed that it was probably not the best thing to do a Friday at 23:00. After that, <a href="http://tsdgeos.blogspot.com/">Albert Astals</a>, <a href="http://www.alfredobeaumont.org/">Alfredo Beaumont</a>, Alex Fiestas and <a href="http://www.ereslibre.es">Rafael Fernández</a> played a Jeopardy-like game, Alfredo being the winner at the end.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:500px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/4587882654_749e5487cf.jpg"><br />
Photo by <a href="http://www.flickr.com/photos/palazio">Gorka Palazio</a></div>

More serious matters returned on Saturday with <a href="http://www.proli.net/">Aleix Pol</a> from the KDevelop team giving a beginners' talk about Qt. The topic then shifted to next generation graphic interfaces with talks about Clutter (Alfredo Beaumont) and Qt Quick (Alex Fiestas) showing that Clutter is today probably a more mature technology but that Qt Quick seems to have a brighter future.

Then we had the first Lightning Talks sessions with talks about <a href="http://gitorious.org/talk">Talk</a> (Presentation software), <a href="http://gitorious.org/kanban">Kanban</a> (keep track of TODO's), <a href="http://www.ereslibre.es/projects/ideal/">Ideal Library</a> (Qt-wanabee library), <a href="http://www.ereslibre.es/projects/bb/">Bugbuster</a> (distributed bug tracking), an overview of the <a href="http://l10n.kde.org/">KDE l10n project</a>, and finally a talk on the state of translation of KDE applications to Basque. 

That was it for the morning. After lunch, which was also used to hold the <a href="http://www.kde-espana.es">KDE España</a> yearly assembly and the <a href="http://itsas.ehu.es">itsas</a> founding assembly, focus shifted to development tools. This started with a talk about our beloved <a href="http://valgrind.org">Valgrind</a> (Albert Astals) and continued with a talk on how KDevelop can help you understand the cosmos and make you happy while coding C++ (Aleix Pol). Next we heard about <a href="http://gitorious.org/spokify">Spokify</a> (Rafael Fernández) which resulted in a nice discussion on whether it is good to make Free Software that depends on closed services or not. Following Rafael, we had a sneak peek at <a href="http://edulix.wordpress.com/">Eduardo Robles</a> project to make point-to-point encryption in webpages using KHTML, and concluded it's a pretty amazing piece of software. The afternoon talks ended with another Lightning Talks sessions with talks about <a href="http://www.gitorious.org/bluedevil/">BlueDevil</a> (KBluetooth successor), <a href="http://www.gitorious.org/kamoso">Kamoso</a> (Webcam application), <a href="http://edu.kde.org/kalgebra/">Kalgebra</a> (advanced calculator), <a href="http://okular.kde.org/">Okular</a> (document viewer) and plugins for Okular.

When the last talk ended, we all headed for a group dinner at HikaAteneo followed by a nice stroll in the Bilbao old city.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:500px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/4587261925_f88cd3dc6b.jpg"><br />
Photo by <a href="http://www.flickr.com/photos/palazio">Gorka Palazio</a></div>


The last day of Akademy-es 2010 was started by Xavier González who presented <a href="http://www.desktop4all.eu">Desktop4All</a>, an opensource VDI solution which uses KDE. When he was done <a href="http://abenitobethencourt.blogspot.com/">Agustín Benito</a> from GCDS fame spoke about his ideas to get Free Software in general and KDE in particular to be more prominent in Spanish companies and governments. A talk from <a href="http://www.kdeblog.com/">Baltasar Ortega</a> about the new features of Software Compilation 4.4 led to the final presentation of the event, explaining what KDE España is doing and where its associates think it should go.

Finally all that was left was a group photo and another trip to the old city to enjoy some <a href="http://en.wikipedia.org/wiki/Pintxos">pintxos</a>.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width: 490px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Images_05_490.png"></div>


All slides can be downloaded from the <a href="http://www.kde-espana.es/akademy-es2010/doku.php?id=materiales">resources</a> page and we hope videos to be available soon.

KDE España wants to thank the local organizers <a href="http://itsas.ehu.es">itsas</a> for such a great job putting the event together as well as our hosts, the <a href="http://www.ingeniaritza-bilbao.ehu.es">Engineering School of UPV</a> and our sponsors and associates, <a href="http://www.cenatic.es/">Cenatic</a>, <a href="http://www.bilbao.net">the Bilbao City</a>, <a href="http://www.tic.ehu.es">the IT deparment of UPV</a>, <a href="http://www.gozatueuskaltel.com">Euskaltel</a>, <a href="http://www.kultura.ejgv.euskadi.net">the Culture Deparment of Basque Goverment</a>, <a href="http://qt.nokia.com">Qt/Nokia</a>, <a href="http://www.cast-info.es">castinfo</a>, <a href="http://www.coiie.org/">EIIEO/COIIE</a>, <a href="http://www.irontec.com">irontec</a> and <a href="http://www.esle-elkartea.org">esle</a>.

See you at Akademy-es 2011!