---
title: "Software Compilation 4.4 now available for Windows"
date:    2010-02-20
authors:
  - "Stuart Jarvis"
slug:    software-compilation-44-now-available-windows
comments:
  - subject: "True names"
    date: 2010-02-21
    body: "Call me a heretic, but I'm going to continue to call it \"KDE\"."
    author: "Brandybuck"
  - subject: "KDE is the community"
    date: 2010-02-21
    body: "Feel free to call it how you want, but the whole effort of rebranding wasn't because some of us woke up in the morning and thought about changing everything: it was to give more visibility to the applications themselves (you don't need the workspace for them), and because now those also run on other platforms (but they don't replace the current shell, hence it's improper to call it \"desktop environment\"). \r\n\r\nIn short, it was done to get rid of the notion \"KDE is a desktop environment\". Because it's much more than that nowadays."
    author: "einar"
  - subject: "\"Software Compilation\" is not"
    date: 2010-02-21
    body: "\"Software Compilation\" is not a name, it's a generic description. Dropping the KDE name is like dropping \"Ford\" in favor of \"Motorized Conveyance\"."
    author: "Brandybuck"
  - subject: "Yes..."
    date: 2010-02-22
    body: "That's why we often use \"KDE Software Compilation\". However, on *KDE* Dot News, talking about the *KDE* Windows team I think we can take it for granted we're talking about KDE software. The question is which software: SC 4.4 and some latest versions of some non-SC apps.\r\n\r\nTo borrow your analogy, it's a bit like going to your local Ford dealer and saying \"I'd like to buy a 5 door family hatchback\" not \"I'd like to buy a Ford\" since maybe they guessed that already."
    author: "Stuart Jarvis"
  - subject: "Speaking of SC 4.4..."
    date: 2010-02-22
    body: "Look, I'm full aware that The Dot isn't the totally correct place to ask questions (PLEASE point me in the right direction as long as that isn't to an IRC chan):\r\n\r\nHow do I stop KDE (SC...) 4.4 from auto-sorting my desktop icons?\r\n\r\nRight-click Desktop --> Sort Icons: Nothing is enabled/checked\r\n\r\nCachew -->  Folder View Activity Settings --> Sorting: Unsorted\r\n(same place) Arrangement: AH!  There is no option for \"Never Arrange\".  The only options are for them being along one of the four screen edges.\r\n\r\nI can put my icons somewhere, but if I create something new on my Desktop, all my icons get lined up along the left side with Folders being first (even though Folders first is *DIS*-abled) and alphabetically after that (again, disabled).\r\n\r\nPlease, is there some way to stop KDE (SC) 4.4 from moving my Desktop icons?  Locking Icons doesn't work, they still get lined up along the left side.\r\n\r\nThank you.\r\nM.\r\n\r\n\r\nP.S.\r\nJust to say it, I've been using KDE since around 0.8 or 0.9.  In addition, I can get \"under the hood\" and play with rc files if needed.  I've already looked at plasma-desktoprc and plasma-desktop-appletsrc.  I see where it's being set as the config GUIs say they should be, but is there something that can be typed in?  Ya know, like having close buttona ON Konq's tabs.  That can be typed in."
    author: "Xanadu"
  - subject: "Try the forums at"
    date: 2010-02-22
    body: "Try the forums at http://forum.kde.org/"
    author: "odysseus"
  - subject: "Thank you."
    date: 2010-02-23
    body: "Thank you.  I've now done exactly that.\r\n\r\nM.\r\n"
    author: "Xanadu"
  - subject: "How to install offline?"
    date: 2010-02-23
    body: "Hi.\r\nI'm using Windows at work on an offline corporate network. Is it possible to download all files and later install offline?"
    author: "andreak"
  - subject: "Should work"
    date: 2010-02-23
    body: "The installer has option for download only, and option to install from local files. So it should work. "
    author: "Morty"
  - subject: "From non-windows clients?"
    date: 2010-02-23
    body: "My computer which has access to the Internet runs Linux, are you proposing to use Wine to execute the client and choose \"download only\"? Why not a tar-file or something which wraps the whole installer-thing?"
    author: "andreak"
  - subject: "KDE for friends"
    date: 2010-02-23
    body: "Does that mean I can now tell Windows using friends of mine that they can just download this great multi-protocol chat program (kopete) or the powerful editor (kate) and they will run for them? \r\n\r\n(same for many other great apps, kopete and kate are chosen semi-randomly :) )"
    author: "ArneBab"
  - subject: "Depends on the apps"
    date: 2010-02-23
    body: "You can browse the files at http://www.winkde.org/pub/kde/ports/win32/repository-4.4/ (or similar on one of the mirrors). However, you likely don't need everything, which is why the installer is good to get just what you want. A Linux front-end for your use case could be nice though, I guess..."
    author: "Stuart Jarvis"
  - subject: "In principle"
    date: 2010-02-23
    body: "Perhaps ;-)\r\n\r\nNot everything is working perfectly yet and some apps are much better than others - just depends if there is Linux specific stuff in that app that hasn't been noticed yet. I found Kopete worked pretty well for me in 4.3, though I think it was lacking XMPP support at the time (I might be wrong).\r\n\r\nBest would be to test them yourself (if you have access to Windows) or suggest them to your friends as something to try - but explain that this is early work and things might not be perfect, to manage their expectations and not put them off KDE software if they have one bad experience :-)"
    author: "Stuart Jarvis"
  - subject: "Yes,"
    date: 2010-02-23
    body: "you can :)"
    author: "Mark Kretschmann"
  - subject: "Yes, but be aware it's not"
    date: 2010-02-23
    body: "Yes, but be aware it's not like installing normal Windows software, I'd agree with trying the installer for yourself and see how it all works, and decide if that's suitable for your friends.  The installer is still a bit techie, but they are working on that."
    author: "odysseus"
  - subject: "crashes"
    date: 2010-02-24
    body: "hi there,\r\n\r\ni know this is not the right place to report bugs, but kde on windows crashes with my install from today everywhere.\r\n\r\nseems like seom central component - i dont know where to file a bug there as it seems its windows related."
    author: "wintersommer"
  - subject: "Bugzilla"
    date: 2010-02-27
    body: "You can use the genera KDE Bugzilla (bugs.kde.org) - just choose MS Windows binaries for the Distribution Method when filling in the form. Or you can ask on the kde-windows mailing list if you're not sure (https://mail.kde.org/mailman/listinfo/kde-windows- bug reports get copied there anyway, I think) or IRC: kde-windows on freenode"
    author: "Stuart Jarvis"
  - subject: "no crash dumps on windows"
    date: 2010-02-28
    body: "am i right that there is no way having crash dumps automatically sent like in linux ?"
    author: "wintersommer"
  - subject: "Different types of accounting"
    date: 2010-06-02
    body: "Different types of <a href=\"http://www.accountssoftware.me/\">accounting software</a> are available for the Self Employed and some of this software has been specifically designed to cater for the precise size and requirements of the self employed business. There are basic Accounting Software packages available for the self employed business that is not vat registered and have no employees. Standard accounting software packages for the self employed business that is vat registered. The vat threshold limit at which businesses are liable for vat is \u00a361,000 up to April 2007 and subject to possible changes after that date. Advanced and more sophisticated Accounting Software for the self employed who also employ staff are available with integrated payroll software included in the Accounting Software packages.\r\n"
    author: "1stukgent1111"
  - subject: "Nicely presented information"
    date: 2010-06-04
    body: "Nicely presented information in this post, I prefer to read this kind of stuff. The quality of content is fine and the conclusion is good. Thanks for the post.\r\n\r\n<a href=\"http://www.jrbtech.com\">OpenSource Software</a>"
    author: "Ossoftware"
  - subject: "The KDE modules allowing easy"
    date: 2010-06-05
    body: "The KDE modules allowing easy installation of just what you want to use. The KDE Windows developers are also introducing metapackages that make it possible to easily install groups of popular applications from for example KDE Graphics, the KDE Software Development Kit and KDE Games. Metapackages are not yet functional in the installer but are expected in a future release.,..Thank you,..<a href=\"http://www.igud.com.my\">alarm system malaysia</a>\r\n"
    author: "harsh7"
  - subject: "I've been using KDE since"
    date: 2010-06-13
    body: "I've been using KDE since around 0.8 or 0.9. In addition, I can get \"under the hood\" and play with rc files if needed.\r\n<a href=\"http://paknbox.com/\">San Diego Moving Boxes</a>\r\n\r\n"
    author: "williamcalo2"
  - subject: "http://www.youtube.com/watch?"
    date: 2010-06-22
    body: "http://www.youtube.com/watch?v=wQm3sY-NiWU\r\n\r\nLink above is for a short featurette/tutorial of Plasma in KDE 4.1 \r\n\r\nThe most built up features of Plasma, that being the heavy integration, never made it into the beta. I don't know if it has been included yet. \r\n\r\n\r\nLWilliams\r\n\r\n\r\n\r\n------------------------------\r\n<a href=\"http://www.geminicomputers.com/\"target=\"_blank\">peachtree</a> for all your accounting suite needs"
    author: "Lwilliams"
  - subject: "You can't change it now..."
    date: 2010-07-03
    body: "I agree Brandybuck, you can't change it now. It would be like suddenly calling a <a href=\"http://www.netcalculator.com.au/tax-calculator.htm\">\r\nTax Calculator</a> a revenue share splitter."
    author: "calculator"
  - subject: "This is good job and I loved"
    date: 2010-07-06
    body: "This is good job and I loved the reading. It is happening in all countries .. thanks for sharing this informtion <a href=\"http://www.kingofgames.net/kids-games/\">Kids Games</a>\r\n."
    author: "shamslive"
  - subject: "Which is the best Pos"
    date: 2010-07-07
    body: "Which is the best <a href=\"http://www.epospos.com/\">Pos Software</a> for designing menu cards for my restaurant?\r\n"
    author: "1stukgent133"
---
Continuing their impressive record of quick delivery of fresh KDE software, the <a href="http://windows.kde.org/">KDE Windows team</a> is pleased to announce the immediate availability of packages with the latest KDE release for the Windows platform.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/showfotosml.png" width="350" height="262"/><br />showFoto 1.1 on Windows</div>

The installer provides not only most of the applications from <a href="http://www.kde.org/announcements/4.4/">Software Compilation 4.4</a>, but also the latest versions of a number of independently released KDE applications. Highlights include the latest stable version of <a href="http://www.digikam.org">digiKam</a> (and the KIPI plugins) and <a href="http://konversation.kde.org/">Konversation</a>. Although Amarok is not yet included in the installer there is <a href="http://amarok.kde.org/blog/archives/1161-Amarok-on-Windows-Getting-there.html">progress being made</a>.

Packages have been split from the KDE modules allowing easy installation of just what you want to use. The KDE Windows developers are also introducing metapackages that make it possible to easily install groups of popular applications from for example KDE Graphics, the KDE Software Development Kit and KDE Games. Metapackages are not yet functional in the installer but are expected in a future release.

As always, you can install KDE applications on your Windows platform using the <a href="http://www.winkde.org/pub/kde/ports/win32/installer/kdewin-installer-gui-latest.exe">latest installer</a> (Windows executable). Please remember that KDE software on Windows is still a work in progress, so not all applications are suitable for day to day use at the moment.