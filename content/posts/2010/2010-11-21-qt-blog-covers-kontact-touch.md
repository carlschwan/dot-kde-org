---
title: "Qt Blog covers Kontact Touch"
date:    2010-11-21
authors:
  - "steveire"
slug:    qt-blog-covers-kontact-touch
---
The <a href="http://blog.qt.nokia.com/">Qt Blog</a> covers topics relevant to Qt developers relating to innovative developments and applications using the latest Qt technology. They have noticed the ongoing development of <a href="http://userbase.kde.org/Kontact_Mobile">Kontact Mobile</a> from the KDE community by <a href="http://www.kdab.com">KDAB</a>, and partners <a href="http://intevation.de/index.en.html">Intevation</a> and <a href="http://g10code.com/">g10code</a>. The blog has a nice <a href="http://blog.qt.nokia.com/2010/11/17/kdab-and-partners-build-kde-based-mobile-app-suite-using-qt-4-7/">article about Kontact Mobile from a product perspective</a>.
<!--break-->