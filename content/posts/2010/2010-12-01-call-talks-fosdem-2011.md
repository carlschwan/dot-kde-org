---
title: "Call for Talks at FOSDEM 2011"
date:    2010-12-01
authors:
  - "jriddell"
slug:    call-talks-fosdem-2011
comments:
  - subject: "Wrong date"
    date: 2010-12-01
    body: "Always the fosdem is the same weekend as my birthday. I can't go even though its pretty close by. I would love to have it a weekend sooner or later."
    author: "acemo"
  - subject: "No problem"
    date: 2010-12-01
    body: "Acemo,\r\n\r\nWe understand your pain, so this year we will just all come to your house and camp out during the Fosdem weekend in your living room.\r\n\r\nPlease make sure the internet is up to speed as we are many ...\r\n\r\nThanks,\r\n\r\n"
    author: "ninja1"
---
<a href="http://www.fosdem.org">FOSDEM</a> is one of the largest gatherings of Free Software contributors in the world and happens each February in Brussels. We are now inviting proposals for talks on KDE, KDE software and general desktop topics to take place in the Cross Desktop devroom. This is a unique opportunity to show the novel ideas of KDE to a wide audience of developers.
<!--break-->
Talks can be on a very specific topic such as developing mobile applications with Qt Quick or a very general topic  such as predictions for KDE and the free desktop over the next 5 years. Topics that are of interest to the users and developers of all desktop environments are especially welcome. Our <a href="http://community.kde.org/Promo/Events/FOSDEM/2010">FOSDEM 2010 wiki page</a> provides details of last year's event and might give you some inspiration.

Please include the following information when submitting a proposal: your name, the title of your talk (please be descriptive, as titles will be listed with around 250 from other projects) and a short abstract of one or two paragraphs. The deadline for submissions is Wednesday 22nd December 2010. FOSDEM will be held on the weekend of 5-6 February 2011. Please submit your proposals to: crossdeskt<span>op@li</span>sts.fosdem.org

If you plan to come to FOSDEM, do add your details to <a href="http://community.kde.org/Promo/Events/FOSDEM/2011">our FOSDEM wiki page</a>.