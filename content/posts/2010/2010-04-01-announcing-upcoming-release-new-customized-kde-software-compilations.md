---
title: "Announcing the Upcoming Release of New Customized KDE Software Compilations"
date:    2010-04-01
authors:
  - "Justin Kirby"
slug:    announcing-upcoming-release-new-customized-kde-software-compilations
comments:
  - subject: "great"
    date: 2010-04-01
    body: "i always wanted a rotation button in kate:) but does it rotate the whole text or just a word? that would be very handy to see if a word is a palindrome :)"
    author: "Asraniel"
  - subject: "My good sir, that is KWrite"
    date: 2010-04-01
    body: "My good sir, that is KWrite :) It rotates the whole text but if you've got a word selected it will only rotate that word. All text is rendered as a raster bitmap format, unless you select \"Use Scalable format\" in the system settings under Advanced->Programs->KWrite->Configuration->Advanced->Text Rendering->More options."
    author: "Moult"
  - subject: "?"
    date: 2010-04-01
    body: "Advanced->\r\n  Configuration->\r\n    1.april->\r\n      Rotate your head :) \r\n\r\n"
    author: "Liso"
  - subject: "correction and alternative"
    date: 2010-04-01
    body: "It's actually Advanced->Programs->KWrite->Configuration->Advanced->Text Rendering->More options->Even more options->Propellerheads->Details->Are you kidding me?->Yes->Configuration\r\n\r\nOr just open the kconf editor, enter \"dbd46848b31f34fe4fc0503d5ab28a98\" and set the value to 23."
    author: "sebas"
  - subject: "You clearly have never tried"
    date: 2010-04-01
    body: "You clearly have never tried to find the option. After careful deliberation, it has been decided to put this on the toolbar. After all, it's a poweruser version, and all options should be on the toolbar.\r\n\r\nOf course, this poweruser version of Kate is only usable on 1920x1080 screens as smaller screens would only show the toolbar with the options (an over 2500 pixels wide screen is recommended to actually USE kate)."
    author: "jospoortvliet"
  - subject: "Congrats"
    date: 2010-04-01
    body: "Congratulations! That's a very good april fools day joke!"
    author: "Nece228"
  - subject: "Step in the right direction"
    date: 2010-04-01
    body: "I've used kde for years, and seeing the direction change into the KDE 4 release, and now branching off like this, I can honestly say that this is a step in the right direction. Customization is one of the major reasons why KDE is my ultimate DE. I've used GNOME, Fluxbox, XMonad, and XFCE. Going back to its roots reaffirms my belief that KDE will always be superior for a productive environment. \r\n\r\nI like this new direction. Congrats KDE team!\r\n\r\nEDIT: Oh god please don't let this be an april fools joke... do you know how awesome this would be? o_<"
    author: "PeonDevelopments"
  - subject: "Why wait for SC 5"
    date: 2010-04-01
    body: "Why not make it possible for me to get KDE SC UFE with the upcoming release? Since the release of 4.0 I have really been missing this ultimate freedom. For example, I would like to configure the panel to look like the MAC Dock with zoom icons and all that. It's a step in the right direction though ;)"
    author: "Bobby"
  - subject: "Closed Source? What?"
    date: 2010-04-02
    body: "This is an April Fools joke, right? You're not going to make this project, made by COMMUNITY MEMBERS closed source, right?\r\nGood, I'm glad we've got that cleared up. Now with \"every feature\", I'm hoping that will mean we get our blur back. It'll be nice to have usable transparent windows."
    author: "correnos"
  - subject: "Re: Step in the right direction"
    date: 2010-04-02
    body: "There is certainly a market for a midget themed desktop with limited settings and lordly-appointed defaults. But I am afraid it would be a tough market, with entrenced competition."
    author: "carewolf"
  - subject: "Then why don't ya configure"
    date: 2010-04-03
    body: "Then why don't ya configure the panel to have zoom icons ;-)\r\n\r\nThere is a bunch of widgets which let you do that, try daisy or fancy-tasks..."
    author: "jospoortvliet"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdedwarf.png" width="248" height="219"/><br />Dialog for the Dwarfed Freedom Edition</div>KDE has enjoyed great success over the years and today marks another important step in the evolution of our growing community. Many years ago when KDE was just beginning we had a small user base and similar expectations of how the software should work. But with growth and success also comes new users and new expectations. As an effort to meet the growing demands of our user community KDE has identified 3 key areas in which we would like to better cater to users' needs. In order to achieve this it has been determined that there will be, going forward, 3 separate releases of each Software Compilation tailored to these areas.
<!--break-->
<b>Ultimate Freedom Edition</b>
The first focus area is geared towards users who prefer the ultimate in customization. KDE has long been unmatched in its ability to give the user full control of their software and we would like to take that to the next level.  Starting with KDE SC 5 we will be creating a KDE SC "Ultimate Freedom Edition". This compilation will include one hundred percent customization of every option, including full maintenance of backports of every feature ever supported by KDE back to v3.0. For quick access, all menu options will be provided on the toolbar and the settings screens have been enhanced with double, tripple and quadruple tabbars to make it contain more settings.

<b>Dwarfed Freedom Edition</b>

The second focus area is aimed at users who enjoy the freedom of open source, but prefer a simple set of default behavior from their software so that they are not bombarded with constant decision making and choices when setting up their software.  This version of the KDE SC has been code named "Dwarfed Freedom Edition". This compilation will bring all of the normal KDE software you've come to love, but with scaled back numbers of options in all desktop environment and application menus so as to make life a little bit simpler for our users.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/fools.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/fools_small.png" width="341" height="264"/></a><br />Initial modifications in KWrite to enable more customization.</div>

<b>Basic Freedom Edition</b>

Last but not least, we would like to reach out to new users transitioning to KDE from outside the world of open source. We know that many people in this category could probably care less about messing with any options at all and want their computer to "just work". To better meet this group's needs we will be releasing KDE SC "Basic Freedom Edition". This compilation will focus on providing rock solid functionality without any customization that might impair their ability to operate the computer. For your protection this compilation will be closed source, not allow you to access root level functionality, and will fully enforce security through obscurity thanks to the "black box" development processes that will be used in creating this edition.

<b>Conclusion</b>

We are very excited to bring to you these great new compilations in the coming months and will continue to bring up to the minute coverage here on the Dot as the KDE community continues to drive software development into the future!

Meanwhile, IT-Wire has the scoop on another great move by the KDE community: <a href="http://www.itwire.com/opinion-and-analysis/open-sauce/38037-kde-picks-kim-kardashian-to-promote-next-release">KDE picks Kim Kardashian to promote next release</a>. The Dot editors would like to publicly express their unhappiness about the fact they did not have the insider perspective here. Haven't we faithfully licked the right boots over the past few years?