---
title: "Cornelius and Vincent Join Different Games"
date:    2010-07-01
authors:
  - "philthane"
slug:    cornelius-and-vincent-join-different-games
comments:
  - subject: "Great stuff"
    date: 2010-07-02
    body: "Cross community building and a nice little stunt for a bit of publicity. There are of course many links between our communities and we can learn a lot from each other, not just about software but also about some of the 'softer' things like marketing and community management.\r\n\r\nNice interview too :-)"
    author: "Stuart Jarvis"
---

KDE has recently launched the <a href="http://jointhegame.kde.org">“Supporting Membership Program”</a> to raise funds for Developer Sprints, Akademy conferences and other activities of KDE e.V, the legal and financial representatives of KDE. President of KDE e.V. is Cornelius Schumacher and at the launch of Join the Game he made a pact with GNOME release manager and board member Vincent Untz that they would each join their rival's support programme. And talk about it to the Dot. And so...

<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org//sites/dot.kde.org/files/pawn.png" width="203" height="330"/></div><b>The Dot</b> Tell us about your involvement in KDE, Cornelius.

Cornelius</b> Sometimes it feels like I'm around in the KDE community forever. I recently looked it up, I wrote my first patch in May 1999. Now, a couple of lines and a few years later, I'm writing less patches, but I'm still there, currently as president of KDE e.V.

<b>The Dot</b> And Vincent, when did you get involved with Gnome?

<b>Vincent</b> I've been involved in GNOME since 2002, when I started writing patches for a dead branch of GNOME and adding useless comments on closed bugs in Bugzilla. Somehow, some people found out how to make me contribute usefully to the project! I'm currently release manager and the president of the GNOME Foundation. I didn't run in the recent elections though so I will soon be the 'past president'.

I'm also a member of the GNOME team in openSUSE, where we make sure to have a rocking GNOME on our distribution; yes, openSUSE is not just a KDE distribution!

<b>The Dot</b> So who came up with the idea of working together like this?

<b>Vincent</b> I'm innocent: I blame the KDE e.V. folks! They wanted to find fun ways to promote the new supporting member status of the KDE e.V. I discussed this quickly with the GNOME Board, and everybody liked the idea. So we did it!

<b>Cornelius</b> At our last KDE e.V. board meeting in Berlin, we discussed the campaign for our individual supporting membership program, and threw around ideas who we might be able to convince to join the game as a supporting member. I don't remember who exactly brought it up, but somebody said that it would be cool to have somebody from GNOME join us. We are working together increasingly well after all and there are lots of good relationships between the two communities. GNOME and KDE share so much in terms of goals, philosophy, and technology, that it makes a lot of sense to stay together, and address the challenges for free software on the client.

So after a carefully crafted scientific selection process to find the coolest guy in GNOME, we came up with the idea to ask Vincent to join the game, and the other way around for me to become a friend of GNOME.

<b>The Dot</b> So Vincent, what did you think of our website, or did you sign up with the form?

<b>Vincent</b> I used the paper form, since I was at LinuxTag when this was announced, and it was the simplest way to apply. And I really wanted to be one of the first supporting members.

I had to steal a second form to get the information for the money transfer, but I'm assuming I should have taken the first form home and then sent it back. Oh well. I'm never good with paperwork! It's really a straightforward process.

I just took a look at the website too. I find it interesting that you have a tooltip on the right to explain to visitors how to use the website! I like that it's all on one page: it really gives the visitor the flow of information in a good way - not too much at once, and in the right order.

<b>The Dot</b> Cornelius, tell us, how do the Gnomies handle this Friend of Gnome thing? Did you get a hug?

<b>Cornelius</b> I signed up on www.gnome.org/friends. It's a nice web page, which makes it very straightforward to join and donate to GNOME. It also reports about how the money is and was spent to rock the free desktop world.

I'm told I'll get a postcard from my favourite GNOME hacker. That's a neat idea. I'm looking forward to my postcard from Vincent. Hugs are not included, but I'm sure this is just an oversight, and we can make up for that at the next desktop summit.

<b>Vincent</b> Oh I did get hugs at LinuxTag. But I prefer to not talk about this publicly...

<b>The Dot</b> Vincent, can you tell us a bit more about the Friends of Gnome program, why you have it and how you organise it?

<b>Vincent</b> The Friends of GNOME program is a way for individuals to contribute to the GNOME project. There are many people who would love to contribute to GNOME, but who feel that they don't have the technical knowledge to do so, which is actually wrong: everybody can help! or who just don't have time to contribute. With the Friends of GNOME program, they can use their money to contribute to the project, and we make sure to use the money for concrete tasks that have a direct impact on GNOME: we organize hackfests, sponsor people to go to events, and our latest use of the Friends of GNOME money will be the hire of a sysadmin for the project!

Donors can choose between a one-time donation and a monthly subscription; there are gifts for all donations (stickers, mouse pad, t-shirts), but the best thing is really the personalized postcard you get from a hacker if you choose the monthly subscription! There's nothing more valuable than a personal "thank you" note.

<b>The Dot</b> And Cornelius, do you things see in the same way?

<b>Cornelius</b> Yes, lots of people are spending their free time to work on KDE software and make the world a better place through free software. But not everybody is able or  willing to do this. We still want to provide a way for those people to participate. So the individual supporting membership is a great way to do that, supporting the KDE community financially. This enables KDE e.V. to support the community by holding developer sprints, providing legal and organizational representation, running Akademy, and much more.

For more details have a look at our shiny new website at <a href="http://jointhegame.kde.org/">jointhegame.kde.org</a>.

<b>The Dot</b> Does either group have anything cool in the pipeline for members/friends?

<b>Cornelius</b> We are preparing to send out our first supporting membership packages. These come with a little surprise. You will love it.

<b>Vincent</b> Yep, we're working on something we hope we'll be able to announce during the summer for the monthly subscribers, but it's still top secret.

<b>Cornelius</b> The primary benefit will of course be great free desktop software, though.

<b>The Dot</b> Of course! Thanks to both of you for agreeing to the interview, it's good to see KDE and GNOME people working together like this. Don't drop the rivalry completely though, it spurs both groups to do better.