---
title: "Akademy 2010: Almost Like Being There"
date:    2010-07-07
authors:
  - "Stuart Jarvis"
slug:    akademy-2010-almost-being-there
comments:
  - subject: "Really nice."
    date: 2010-07-07
    body: "It's good to be able to see all the presentations in video. Thanks a lot, guys."
    author: "Gallaecio"
  - subject: "Videos are great."
    date: 2010-07-08
    body: "Yes, the videos are really nice. Many thanks! Been watching some of them at work, a few minutes each time, between restarts :-)."
    author: "m4ktub"
---
Not everyone can make it to Akademy, although given the record number of people captured in the group photo, it probably seems like everyone did.

<a href="http://byte.kde.org/~duffus/akademy/2010/groupphoto/"><img style="position: relative; left: 50%; margin-left: -300px; padding: 1ex; border: thin solid grey;" src="http://dot.kde.org/sites/dot.kde.org/files/akademy-2010-group-photo.jpeg" /></a>

Clicking on the above photograph allows you to search for yourself or other attendees by name (courtesy of Jonathan Riddell, original image by <a href="http://www.linux-magazin.de/content/search?SearchText=Feilner">Markus Feilner</a>).  Following the fun hacker atmosphere here at Akademy a special easter egg was inserted into the crowd.

For those of you not able to be here, the Akademy team has been making great efforts to video the talks, capture the atmosphere through <a href="http://www.flickr.com/search/?q=akademy2010">photographs</a> and pick up some of the Akademy chatter through informal interviews with community members.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 265px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/valteri-richardlarsson.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/valteri-richardlarsson.jpeg" width="265" height="350" ></a><br/>Valtteri Halla, Keynote speaker (by <a href="http://rlemacs.dyndns.org/">Richard Larsson</a>)</div><h2>Akademy Talks</h2>

The local team was busy over the weekend capturing the presentations on video. These have now all been processed and uploaded together with the presentation slides, thanks to the hard work of Kenny Duffus. To view them follow the links on the <a href="http://akademy2010.kde.org/program/conference">Akademy conference programme</a>.

<h2>Rapid Interviews</h2>

Justin Kirby has been busy over the past few days patrolling Akademy with a video camera searching for interviewees. He caught up with Sebastian Kügler to discover <a href="http://www.youtube.com/watch?v=1RS9OGXT7YY">how KDE software can provide a better experience for the web</a>.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey; width: 261px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/sebasint.jpeg" width="261" height="344" ><br/>Sebastian discussing web integration (by Justin Kirby)</div>You can also find out about <a href="http://www.youtube.com/watch?v=bQ2sDp3fkIE">how you can prepare your KDE application for Windows</a> (Casper van Donderen) and get Thomas Thym's take on the <a href="http://www.youtube.com/watch?v=NKdNF7tkJ50">7 principles of successful open source communities</a> and find out about <a href="http://www.youtube.com/watch?v=LtOrMRkDuMY">Telepathy as a real time communication service with George Goldberg</a>.

If you ever wondered how the KDE Forums became so user friendly and beautiful, you may find a few answers in Justin's conversation with Nils Adermann from <a href="http://www.phpbb.com/">phpBB</a> about <a href="http://www.youtube.com/watch?v=G1W4nMjrIvs">KDE Forums and reaching out to the community</a>. The forums are a success story for both KDE and phpBB with collaboration between the two communities to make phpBB better fit our needs, resulting in software that works for KDE and is, of course, also available for other communities.

Continuing to <a href="http://www.youtube.com/watch?v=adceyQL7gF4">uncloud ownCloud</a>, Frank Karlitschek explained the benefits of open cloud computing and Aaron Seigo explored how exactly KDE can <a href="http://www.youtube.com/watch?v=t2RouLmV6lY">reach for greatness</a>.

The full set of Justin's interviews can be found on the <a href="http://www.youtube.com/user/kdepromo#g/c/3F38BF428EFB4017">Akademy YouTube page</a>.