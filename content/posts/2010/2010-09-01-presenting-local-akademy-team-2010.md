---
title: "Presenting the Local Akademy Team 2010"
date:    2010-09-01
authors:
  - "jospoortvliet"
slug:    presenting-local-akademy-team-2010
comments:
  - subject: "Great job"
    date: 2010-09-07
    body: "Great job all round (and nice interview too). Thanks to Ilkka, Matti, Sanna, the rest of the team and of course the KDE organisers too.\r\n\r\nThat was my first Akademy, but it won't be my last. It was smoother than many of the academic conferences I've been to that cost a few hundred Euros to attend and have massive organisation teams."
    author: "Stuart Jarvis"
---
It is a while now since <a href="http://akademy2010.kde.org">Akademy 2010</a>, KDE's annual conference, <a href="http://dot.kde.org/2010/07/16/kdes-flagship-conference-akademy-concludes-pushing-elegance-and-mobile-space">came to a close</a>. There were a huge number of blogs and articles about what happened and it is safe to say that the latest conference was a success. Many attendees noted how smoothly everything ran, thanks to the KDE organizers and the local team. The local team did an awesome job, not only during the conference itself but also during the many months of thought and hard work before Akademy. The Dot managed to catch up with some key players in the local team to get their take on the KDE invasion of Tampere and find out what it is like to organize such a large event.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 240px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/award.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/award_wee.jpg" width="240" height="180"></a>The team receiving an Akademy Award! (by <a href="http://www.flickr.com/photos/thinkfat/">thinkfat</a>)</div>The local team taking care of the Akademy participants consisted of about 12 local members in addition to the KDE people helping out. Half of them were volunteering, the others work at <a href="http://www.hermia.fi/">Hermia Ltd</a>, which operates <a href="http://www.coss.fi/en/">COSS - The Finnish Centre for Open Source Solutions</a>. During one of the last few days, the Dot sat down with Ilkka (ilectrick) Lehtinen, Matti (smoinen) Saastamoinen and Sanna (SaneMind) Heiskanen who were the three central local organizers of Akademy 2010. Torsten Thelke, an intern for KDE e.V., was also present.

Jos & Torsten asked the three to introduce each other. We quickly figured out this would take too much time spent on making fun of each other, so we asked them to talk about themselves instead. Ilkka, claiming to have 'the boss factor', started. We first wanted to hear about the nice orange t-shirts showing their support for your writer's homeland in the World Cup finals.

<b>Illka</b>: Hmmm, yes, the t-shirts. Well, orange wasn't our favorite color but as long as it ain't yellow (the color of Sweden, Finland's biggest rival) it is fine. There simply wasn't that much choice.

I am 46 years old and have been working in IT for the last 24 years after starting my studies with computer stuff in the military. I attended the University at the same time as Linus Torvalds who was 2 years younger than I am. Ok, he still is, he didn't catch up yet. Then again, he might, they are inventing new stuff all the time, you never know. In 1996 I started my own company which I kept around for 6 years until I sold it during the dot-com boom. I stayed at that company until 2003, working in Texas. After that I became CEO for a small ISP here in Tampere until 2008. After that was sold off, I moved to COSS. I knew the former executive and it seemed like an interesting challenge. On a personal level, I'm married, have 2 animals and 2 dogs (12 and 17 year old boys, the dogs are 7 months and 12 years). My hobbies can be summarized as nature - hunting, walking with dogs, and I play golf occasionally (used to be pretty decent). Otherwise I have a typical boring, middle-age life.

My first encounter with FOSS was at the local ISP - like most of those, it leaned heavily on open source software. Its datacenter ran SUSE Linux, but there were other flavors too, like a few Debian and Red Hat systems. Personally I first installed Linux at home in 1997 and was pretty impressed with what that young fool from my University had done. Jobwise I had to use Windows, but at home Linux did great. And from 2003 I started using it on my work laptops.

<b>Matti</b>: my first encounter with computers was due to my brother getting a beautiful Commodore PC in the 80's. I only played games, my brother impressed me with his programming skills. After the army, I decided to follow my brother who went for computer science.

<b>Illka</b>: What Matti neglects to say is that he was born in the middle of the wilderness in Finland. He's like Tarzan, probably raised by wolves...

<b>Matti</b>: I refuse to comment on that, but I do bite sometimes... Anyway, computers were my new love. It took me a while but I majored in computer science and started out as a programmer. My first Linux experience was in 1999. It influenced my major - I graduated in the area of open source licensing because I had found out that many Finnish IT companies were afraid of the licensing issue. Now, for the last 4 years I have worked at COSS and haven't been programming anymore. I've grown about 10 years older in those years due to Illka chasing me all the time. But like him, my professional life has turned into a personal passion.

<b>Sanna</b>: I'm the non-technical person in this team, meaning I have to tell them what to do all the time. I studied business adminstration and before coming to COSS I hated computers. They transformed me into a nerd.

<b>Illka</b>: Aah, and she's fanatical about Linux now... Still her main thing is event management and organizing, this is the 4th year we're doing that - MindTrek for example, with 800 visitors last year.

<b>Sanna</b>: Sure thing, boss. In my free time I'm an active member of JCI, an organization for young adults (under 40) interested in leadership and networking. Kind of a junior Chamber of Commerce. On a more personal level, I like movies, and am a huge Twilight fan.

<b>The Dot</b>: So how did this Akademy thing happen?

<b>Ilkka</b>: We inherited it. Petri Räsänen tried to get a big international developer meeting here. In the past we had business meetings, but the developer circle is relatively small in Finland, so he came up with idea to boost local development and to give back to the international network. We thought it was great idea, and COSS liked it too so they supported us. We tried to explain that Tampere isn't that dark and cold to get you here, and it did work out.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 240px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/outside.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/outside_wee.jpg" width="240" height="180"></a><br/>Some team members outside (by <a href="http://www.flickr.com/photos/93706655@N00/">smoinen (Matti)</a>)</div>

<b>Sanna</b>: First we applied to host last year's Desktop Summit; but it was Finland vs Gran Canaria, so...

<b>Illka</b>: So it turned out to be a good thing; I was a bit afraid about hosting a Desktop Summit. Demola would be too small, and we would suffer trying to get it all organized. The University could handle it, but hacker space would be more difficult. Gnome wanted their own separate conference, and Nokia (which is involved in COSS) hinted they would love to get Akademy here, so we sent in a proposal. Last year in Gran Canaria after the Gnome meeting, I found out that there would be split this year. So I suggested to Claudia that we should apply to do Akademy. Sanna edited the proposal from last year and we submitted it.

<b>Torsten</b>: That turned out very well, I think. Akademy isn't over yet, but one could fairly say that this has been organized incredibly well. We pointed this out earlier, but we can't say it enough, not just to you three but to the other volunteers as well. Looking at the whole process - what turned out to be most interesting or funny?

<b>Illka</b>: What amused us was the process of working with our KDE counterparts. Because it's business vs community. We make decisions like *snap*. With community, things can take a bit longer.

<b>Matti</b>: Yes, people sometimes took a long time to respond, or they would start voting and discussing things for a long time instead of making the decision. They are always looking for consensus.

<b>Sanna</b>: It's a whole different way of working and thinking.

<b>The Dot</b>: How was that for you?

<b>Illka</b>: It was irritating at first, but when we realized this is how a community works, the way to keep everybody happy, we got used to it.

So to make things more efficient, I would advise putting one or two people in charge to minimize the number of votes - get fewer people involved but have them dedicate more time. Of course this can be quite hard to do with volunteers. Yes, some things need discussion and consensus, but I thought that those leaders would know what needs to be discussed.

I think the KDE organization did a great job on the program. Sanna knows about that, the difficulty of choosing speakers and making a schedule. It was a great relief for us that KDE took care of those. These processes could possibly be applied in other areas. When running day to day tasks, decisions are important, to get them done early enough, precise enough. A committee would be more efficient. They should of course be accepted by the community. If it works, it would help the local organizers a lot.

<b>The Dot</b>: It sounds like you all have spend a lot of time on organizing and planning. Do you have an guesstimate of the number of (wo)man hours put in on Akademy?

<b>Illka</b>: Last year I went to Gran Canaria Desktop Summit to check it out and present a proposal. Otherwise, about 2 months effort over the last 6 months. The last 4 weeks have been pretty much fulltime.

<b>Matti</b>: Approximately the same - especially the last 6 weeks, I did pretty much only planning and organizing for Akademy all day.

<b>Sanna</b>: Same here. It wouldn't have been possible if it wasn't our job.

<b>Illka</b>: And of course there is this week itself, and the work of the volunteers who run around like crazy for a full week.

<b>Torsten</b>: On the KDE side, Claudia, Kenny and others have been on this mostly full-time for the last month or two, I helped out sometimes as well.

<b>Matti</b>: Yes, and the little things you do beforehand also add up. Over the last 6 months, it was something every day. Of course COSS supported it, it wouldn't have happened otherwise.

<b>The Dot</b>: So, should we hire a company next time to do this?

<b>Illka</b>: Ha, count on something like €400,000 at least to have professionals handle this. It's very expensive.

A tempting option for the future, especially if you do a Destkop Summit with the Gnomies too: check out the EU Commission programs. They promote open source strongly and openly, and might be willing to help out. Last year in the Open World Forum in Paris, one of the main subjects was how the public sector can work with communities. Something they don't know very well. So next year, try to get support from the government or EU. Together you guys and girls can do this. They are more than willing to take up this kind of event.

<b>The Dot</b>: Thanks for the tip! Now we only need someone with contacts to set this up ;-)
So what did you think about the conference itself?

<b>Sanna</b>: Really great, very relaxed people. It was totally different from the average business conference.

<b>Illka</b>: Were there any cute guys? She is single, you know...

<b>Sanna</b>: Maybe a few, but I'm very picky ;-)

<b>Matti</b>: Next year we'll visit the new Desktop Summit and check out how it goes. Of course we'll complain and criticize ;-)

<b>Torsten</b>: Sounds like we should invite you three to come over and organize this.

<b>The Dot</b>: He, yeah, every year there are a few people laughing a lot - the organizers of last year's Akademy. But seriously, it's a real burnout thing, such a conference. Actually something we're really worried about. You three still look reasonably fresh, it seems there's not too much damage?

<b>Sanna</b>: Let me say it this way: give me a hug, my taxi is leaving for my holiday RIGHT NOW and I need it ;-)

<b>Illka</b>: I can't imagine doing this as a volunteer, next to a full-time job, but the way we did it - it's doable. And frankly, the KDE community has been amazingly helpful. Last night we had to clean up the hacker space at Demola. They said it would be so much work - I said, 'No problem, these aren't business people, they're a cool community!' So we just asked, and in 5 minutes everything was done. I felt like being in the Army again - ordering people around. It made a difference!

<b>The Dot</b>: I knew that, of course, but it's good to hear we rock!
So now, you've told us about yourself and Akademy, but there are 10 more orange t-shirts walking around. Tell us about them!

<b>Ilkka & Matti</b>: Well, first we have Sanna Salo, she did marvelous job with catering for the day-trip on Thursday - feeding 200 people ain't easy. Then we have Riku Itäpuro, providing a helping hand on camera stuff and other things during the whole conference. And Uwe Geuder, Philippe De Swert, Tero Heino and Paavo Hartikainen acting as camera men and helping in other ways during the whole week as well. Elias Aarnio, a former COSS employee helped everywhere, especially as TOAS (youth hostel rented entirely for the KDE visitors) host - the man inside Tux :)

From Hermia Ltd, we got additional help from Tiia Koskinen, Sanna Eklund, Sini Eronen, Pia Berg, Karen Thorburn and Taru Söderlund during registration for the conference and at TOAS. They also helped dragging things around, telling people where to go, cleaning, stuff like that. And finally, Heikki Ilvespakka and Antto Perttola were our technical wizards and in general were asked to do the impossible things.

So that was the whole team. Everybody worked exceptionally hard, and we're proud of them.

<b>The Dot</b>: Well, we're very grateful for all the team has done. Organizing a 400 person conference isn't easy to begin with. And it's probably even harder if the whole conference consists of such workaholic, crazy-running-around, computer-savvy and generally weird nutcracks as we are ;-)

<b>Illka</b>: Well, some of you were surprisingly normal...
<b>Sanna</b>: ... but others were definitely 'different'...
<b>Matti</b>: ...and in the end it turned out everyone was awesome ;-)