---
title: "KDE Commit-Digest for 24th October 2010"
date:    2010-11-25
authors:
  - "dannya"
slug:    kde-commit-digest-24th-october-2010
comments:
  - subject: "Thank you"
    date: 2010-11-25
    body: "The Digest is fantastic. It is what makes me enjoy KDE developments. I would appreciate an addition of Krazy/EBN statistics to the digest."
    author: "vinum"
  - subject: "Thanks!"
    date: 2010-11-26
    body: "Thanks a lot for this!\r\n\r\nThe editorial intro is usually a very interesting read but the digest is of great value even without it. Why don't you release anyway if there's no introductory content?"
    author: "delta_squared"
  - subject: "I've thought about it..."
    date: 2010-11-27
    body: "Mostly personal preference: I feel that it isn't a \"proper\" publication if it doesn't have original introduction content.\r\n\r\nSo i'm trying to give it one more try.\r\n\r\nOf course, lack of contributions may in future mean it has to be released most weeks with no introduction content, but this would be a sad day for me!\r\n\r\nDanny"
    author: "dannya"
---
In <a href="http://commit-digest.org/issues/2010-10-24/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> starts experimenting with declarative QML, and gets Activity templates to enable quick setup and possible sharing of Activities. Refresh of many mimetype icons used across the KDE workspace. Work on over/under exposure interface functionality, face detection, and image history in <a href="http://www.digikam.org/">Digikam</a>. <a href="http://edu.kde.org/kstars/">KStars</a> merges the OpenGL branch, bringing this functionality to users with the next release. Various work on KLocale/KCalendarSystem in kdelibs. Various work across <a href="http://pim.kde.org/">KDE-PIM</a>, especially to the SMTP mail transport. Progress committed from the <a href="http://koffice.kde.org/">KOffice</a> Essen meetup. Migrating the WYSIWYG Editor to WebKit in Blogilo. Twitter Lists support in <a href="http://choqok.gnufolks.org/">Choqok</a>. More work on KAccessible. <a href="http://ktorrent.org/">KTorrent</a> gains support for the Magnet protocol.  Work in KRFB to allow more than one RFB server run at once. Better Valgrind 3.6.0 compatibility in KCacheGrind. Important progress on the Perl KDE bindings.

<a href="http://commit-digest.org/issues/2010-10-24/">Read the rest of the Digest here</a>.
<!--break-->
