---
title: "KDE Software Compilation 4.4.4 Out"
date:    2010-06-01
authors:
  - "sebas"
slug:    kde-software-compilation-444-out
comments:
  - subject: "meanwhile"
    date: 2010-06-02
    body: "...could someone please fix the planet?\r\nhttps://bugs.kde.org/show_bug.cgi?id=233779\r\n\r\nIt hurts my eyes since months :("
    author: "Robin"
  - subject: "Many thanks for the thorough"
    date: 2010-06-04
    body: "Many thanks for the thorough work! \r\n\r\nMy work-computer will be quite happy with it (theugh this one already runs 4.5 :) )"
    author: "ArneBab"
---
KDE has <a href="http://www.kde.org/announcements/announce-4.4.4.php">issued</a> another update to the 4.4 desktop, applications and development libraries. KDE SC 4.4.4 brings, in addition to its funny version number, mainly small bugfixes that further polish the user experience. Most notable are probably sorting fixes for natural sorting in Dolphin, our nice file manager.

The KDE release team also decided to do another release in the 4.4 series, 4.4.5, to come out next month, before fully concentrating on the 4.5 tree. SC 4.4.4 though is a stable update that is recommended to everyone running 4.4.3 or earlier. Packages from your favorite OS vendor will become available soon.
<!--break-->