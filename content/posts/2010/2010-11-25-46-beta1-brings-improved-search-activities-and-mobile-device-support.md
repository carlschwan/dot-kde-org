---
title: "4.6 Beta1 Brings Improved Search, Activities and Mobile Device Support"
date:    2010-11-25
authors:
  - "sebas"
slug:    46-beta1-brings-improved-search-activities-and-mobile-device-support
comments:
  - subject: "Info-Page not available"
    date: 2010-11-25
    body: "\"Not Found\r\n\r\nThe requested URL /info/4.5.80.php was not found on this server.\""
    author: "mark"
  - subject: "fixed already."
    date: 2010-11-25
    body: "See subject :)"
    author: "sebas"
---
The KDE community has <a href="http://kde.org/announcements/announce-4.6-beta1.php">made available</a> first beta packages of the 4.6 series of the Plasma Workspaces, KDE Applications and Frameworks. All components add exciting new features making the stack more suitable for use on mobile devices, leveraging Nepomuk as the search infrastructure in the UI, and moving away from HAL as hardware abstraction layer.
Source packages are available on ftp.kde.org, but most likely your distributor of choice will have created easy-to-install packages by now.
Be aware that these releases do not meet production quality requirements. Being first betas, they might eat your baby or pet, or even cause serious damage to your beloved data. This is especially true since these releases contain the new Kontact based on Akonadi, bringing KMail and its sister apps to a whole new level of Groupware.
As this release comes packed with new features, the KDE community encourages everybody to test 4.6 Beta1 thoroughly and report issues back to us via <a href="http://bugs.kde.org">KDE's Bugzilla</a>.