---
title: "KDE Science: Thomas Friedrichsmeier on RKWard, Toolkits and the KDE Platform"
date:    2010-09-13
authors:
  - "einar"
slug:    kde-science-thomas-friedrichsmeier-rkward-toolkits-and-kde-platform
---

As reported a few months ago, <a href="http://dot.kde.org/2010/07/02/kde-and-science">KDE software has plenty of use among scientists</a>, who may use Plasma workspaces for their daily jobs or develop applications on top of the KDE Platform.

One application that builds on the KDE Platform is <a href="http://rkward.sourceforge.net/">RKWard</a>. RKWard is an integrated development environment (IDE) for the <a href="http://www.r-project.org">R programming language</a>, a free implementation of the statistics and math oriented S-plus. Due to the wealth of statistical and mathematical functions and the large number of add-on libraries, R is often the tool of choice for people doing statistics and mathematics in many different fields of science. As R does not come with a GUI on Linux (unlike its Windows counterpart), RKWard provides one and adds a lot of features to ease the job, going beyond the role of a simple front-end.

We interviewed the lead developer of RKWard, Thomas Friedrichsmeier, about his experiences, KDE, and the role of KDE software in science.

<b>Hello Thomas, and thanks for taking part in this interview. Can you tell us a bit about yourself?</b>

Hi, my name is Thomas Friedrichsmeier. I'm 33 years old, and live in Bochum, Germany. I have a degree in psychology. Regarding computers and programming, I'm mostly self-taught.

<h3>All About RKWard</h3>

<b>One of your major contributions to Free Software is RKWard, an IDE for the R programming language. How did you start working on it?</b>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/foto_reduziert.JPG"><br/>Thomas Friedrichsmeier</div>During my studies of psychology, I was doing an internship at a small social research firm. As one of their projects, they were evaluating the impact of a poster campaign on road accidents. Being the intern, it was my job to merge a bunch of different road accident statistics from different provinces into a single data file. I don't really remember the details, but several steps of this task were quite repetitive and I would have loved to automate them. But, as I had to find out, it just wasn't possible to program this in SPSS (the tool we were using) at the time.

So I searched the Internet, and found R. It was obvious that it had all the programmability that I was looking for. But after spending an hour or two on the manual, I realized that I still knew next to nothing, and that I would probably need days to get up to speed with R. I didn't have that time.

So, after I had wasted the morning, I spent my afternoon merging the data by hand, which had to be redone after I realized that I had made a mistake in one of the first steps. By the evening, the basic idea had formed. Why not combine the best of both worlds? Create a GUI that allows a person to get started with the most basic tasks quickly and intuitively, and base it on a powerful language that provides all the flexibility and programmability one could ever want.

It took me another year to realize that if really I wanted this idea to materialize, I should better make a start. The 0.1.0 version was released in November, 2002.

<b>What were the biggest satisfactions and obstacles you faced when developing RKWard?</b>

The biggest challenge in developing an ambitious project like this is to keep up the motivation. Naturally, the technical side is always full of obstacles of various sizes, but in general I find working on such problems to be rewarding in itself. Yet at times, when I get stuck on certain issues, when unanswered support requests and bug reports seem to be pouring in faster than I can even read them, when I can't find the time to work on the project, and when development of new features takes ages - at those times, the project just seems to grow over my head.

More than once, what got me out of a motivational low was a small reminder from the community that I am not the only person on the planet working on the project. Receiving an unexpected patch from a person that I have never heard of before is just cool. Finding out that somebody took care of an outdated section in the wiki is wonderful. Realizing that a contributor has taken a long-term interest in RKWard is just great.

<b>Who else, aside from you, is working on RKWard? How large is the RKWard development team?</b>

Many people have contributed to the project in different ways. Since the time we have available to work on the project varies for all of us, it's hard to give a definite answer about who is "currently" working on the project. Prasenjit Kapat, Meik Michalke, and Stefan Rödiger are certainly active members of the current team, and have been working on RKWard for years. However many more people are participating actively by providing translations, packaging, updates to the wiki, feedback and testing. I should also mention Pierre Ecochard, who was a major driving force behind RKWard between 2004 and 2007, but is too busy to participate in the project actively these days.

<b>What are the major advantages of RKWard compared to other R GUIs, such as Rgui?</b>

Well, we have a number of really nice features that you wouldn't want to miss, such as "run again" links to repeat a GUI-driven analysis, plot preview windows, and easy export of graphs to different file formats, along with several others. However, I think the key advantage of RKWard is that it is deliberately broad in scope, providing features for R novices and R experts alike. This may sound like a lack of focus, but in reality the needs of these groups have considerable overlap. Even a seasoned R enthusiast will sometimes have a hard time figuring out the steps required to perform a certain analysis in R. Having easy to use GUI dialogs can be very helpful here, particularly when exploring your data with a number of different statistical procedures that you do not use on a daily basis. Similarly, even an R novice may well be able to automate some tasks by copying and pasting snippets of code which were generated from GUI dialogs for the respective analyses. And as the needs for automation or customization become more advanced, there is no artificial barrier anywhere. RKWard will remain a useful tool all along the learning curve. Finally, many features, such as a decent data editor, integrated help browser, or an integrated output window for the documentation of results, are always useful, to both R novices and R experts.

Looking at some of the competitors, Rgui is a rather minimalistic, MS Windows-only interface, providing only a basic set of features. <a href="http://www.rforge.net/JGR/">JGR</a> is a more serious contender written in Java. It provides some features similar to the ones in RKWard, for example an object browser and an editor with syntax highlighting (but not as nice as our editor based on the KatePart), but its scope is pretty limited compared to RKWard, and it does not really attempt to lower the entry barriers to R.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/rkward_dot.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/rkward_dot_sml.png"></a><br/>RKWard, a KDE R environment</div>In contrast, <a href="http://socserv.mcmaster.ca/jfox/Misc/Rcmdr/">R Commander</a> offers GUI dialogs for a lot of statistical functionality, and in fact it is far ahead of RKWard in that department. However, the primary purpose of R Commander is to be a learning tool to teach R, not a tool designed to work productively with R. This results in some fundamental limitations such as working with only one data.frame (basically a table of data) or one regression model at a time. For the same reason, R Commander lacks features like a decent data editor, script editor, or object browser. <a href="http://www.deducer.org/pmwiki/pmwiki.php?n=Main.DeducerManual">Deducer</a> seems somewhat similar in spirit to R Commander but comes with a more modern UI based on Java. Considering its young age, it offers a remarkably useful set of features, but last time I tried it, it also suffered from a sizeable number of issues.

Perhaps <a href="http://www.sciviews.org/SciViews-K/index.html">SciViews-K</a> is most similar to RKWard in spirit, in combining an IDE with dialog boxes. However, the latest stable version seems to offer little more than an R Console integrated into a text editor (and that can also be had by running R in a Kate terminal window).

Arguably, some of these tools can be used in conjunction to arrive at a similar set of features as provided by RKWard. However, that comes at the cost of learning the idiosyncrasies of each tool, and means a severe lack of consistency and integration. But don't take my word for it. Try the different tools, and see which best works for you.

And of course there are <a href="http://www.sciviews.org/_rgui/">more tools</a>. Notably, perhaps, <a href="http://edu.kde.org/cantor/">Cantor</a> is also based on the KDE Platform, but as a worksheet-based GUI, it follows a rather different approach.

<b>Are you being paid to work on RKWard?</b>

No, but if you have an offer, I'm all ears!

<b>Currently RKWard supports the standard R library and a number of other packages. Are there plans for adding support to Bioconductor, which is widely used among bioinformaticians?</b>

Absolutely. But, to my knowledge, no one on the active development team uses the Bioconductor packages for their own work. So you better not wait for this to happen magically. Rather, why don't you make a start, and provide a GUI for the functions you use most? It's easier than you may think. One important design aspect of RKWard is that much of its functionality is provided by <a href="http://rkward.sourceforge.net/documents/devel/plugins/index.html">plugins</a>, so that it can be extended without much programming knowledge. Users have already contributed a nice number of plugins for functionality that I had never heard of before.

Plugins are grouped into "plugin maps", which means it is possible to enable/disable groups of plugins easily (we still need to improve the UI, but the basics are in place). This means we can be inclusive about the functionality we provide in plugins. Don't forget to bring the kitchen sink. (However, for the more exotic plugins, we plan to provide Get-Hot-New-Stuff integration instead of including them in the official distribution).

<b>What does RKWard need most at the moment? How can people help?</b>

Well, we always have a long list of TODO items, in particular for C++ developers. But the two most important areas where we need help do not require existing programming skills:

First, we are still in need of a lot of basic functionality for data transformations, statistical tests, plots, etc. Most of this can be implemented as plugins, and the learning curve to get started on writing plugins is not all too steep (see above). The second area where we could really need some hands is writing and updating documentation, both for the help-system within RKWard, and for our wiki.

If you have questions on how to start or where, just contact us on the mailing list or forums. We don't bite!

<h3>Choosing the KDE Platform</h3>

<b>Why did you choose to build your application on the KDE Platform, rather than pure Qt or another framework?</b>

To be honest, originally this was pure chance. My desktop happened to be KDE, and KDevelop offered a nice application template, so I just started programming to see what I could come up with.

However, I did have ample opportunity to reconsider this choice. And in fact, after the release of version 0.1.0, I canned RKWard. Together with two other people, I started working on <a herf="http://obversive.sf.net">Obversive</a>, which was similar in spirit, but based on the <a href="http://www.fox-
toolkit.org/">FOX toolkit</a>. Working in a team had been my primary motivation to start from scratch with Obversive, but, eventually I found myself to be the only active developer left on the project. By that time, Obversive was a good bit more advanced than RKWard, and it even ran on Windows and MacOS, while RKWard was still Linux only. However, after considering this and further options, I simply felt that KDE/Qt was a superior toolkit, which would allow me to make progress much faster, and so I came back to RKWard. Well, I suppose we're all biased towards the tools we work with, but I'm still convinced that Qt is just the best GUI toolkit out there.

When the port to KDE Platform 4/Qt4 meant a huge effort of adjusting code, I considered switching to a Qt-only solution, for a second. However, the KDE libraries really provide a lot of excellent functionality on top of Qt, which we would have sorely missed. To name just a few features which are central to RKWard: our script editor is based on KatePart, and that gives us a truly advanced and feature-rich integrated editor for just a few lines of code. KHTMLpart provided us with a full-features HTML viewer long before QtWebKit came around. And I don't even want to think about how we could manage the complexity of and IDE user interface without the KXMLGUI framework.

<b>What do you think about using KDE for science? What are the major roadblocks to a greater adoption in your view?</b>

Well, there is a wide range of science applications, and a lot of overlap towards educational or office application. My answer does not apply to all of that range. However, when thinking about RKWard, I don't think the desktop integration is much of a selling point. Surely, any application should have an intuitive and consistent look-and-feel, but I don't think it's terribly important to our userbase, whether that look-and-feel is KDE's or that of Tcl/Tk (although the thought makes me shudder). Unsurprisingly, many of our users are running RKWard on GNOME, or elsewhere outside of a KDE session.

From that point of view, what it comes down to is whether the KDE libraries are a good platform to base scientific applications on. Again, I really think it is. The most important roadblock that I see to greater adoption of KDE science applications is better portability across Windows, Linux, and MacOS. To many of our users, particularly in the educational context, it is important that they can use the same software on at least two platforms, or even on all three. RKWard is easily installable on the Windows platform these days, and can be used productively despite some platform specific quirks. Getting there was a considerable effort. On MacOS X, we finally managed to get RKWard to work not too long ago, but installing RKWard on the Mac literally means days of compiling all the dependencies. These and other issues remain, and I think we need to admit that (lack of) portability is currently a weak spot for KDE when compared to competing frameworks.

The KDE on Windows and KDE on Mac teams have put a lot of hard work into making KDE available on the respective platforms, but if we want to attract more developers to the KDE libraries, and more users to our applications, we really need to make it a whole lot easier to install KDE-based applications on Windows and Mac. One thing that I would really like to see is re-distributable single-download binary snapshots of kdebase on Windows and Mac, so developers can easily deploy their KDE applications on these platforms.

<b>Thanks very much for your time Thomas, and good luck!</b>

<h2>Want more?</h2>

Are you interested in more interviews? Are you using KDE for science and want to be interviewed? Let us know in the comments or get in touch on the <a href="https://mail.kde.org/mailman/listinfo/kde-science">KDE Science</a> mailing list.

Want to discuss science and KDE science apps? Check out the <a href="http://forum.kde.org/viewforum.php?f=199">new science area</a> on the KDE Forums.