---
title: "KDE's Flagship Conference Akademy Concludes: Pushing for Elegance and the Mobile Space"
date:    2010-07-16
authors:
  - "sebas"
slug:    kdes-flagship-conference-akademy-concludes-pushing-elegance-and-mobile-space
---
KDE met for its yearly flagship conference, Akademy, in Tampere, Finland. The event was kindly hosted by COSS, the Finnish Centre for Open Source Solutions. Akademy started last weekend with a two-day conference attended by more than 400 visitors from all over the world, which then blended into several days of designing, programming, discussing and working on the future of the Free Desktops. Important topics included mobile devices, community topics and many others. Read <a href="http://ev.kde.org/announcements/2010-07-16-akademy2010-concludes.php">the full press release</a> for more information.
<!--break-->
