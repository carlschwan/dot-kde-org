---
title: "Last Days at Akademy 2010"
date:    2010-07-11
authors:
  - "jospoortvliet"
slug:    last-days-akademy-2010
---

Unfortunately, all good things come to an end. Friday was the last day at Akademy and by the evening a few people had already gone. Others were sitting at Demola, scattered around the place or chatting in small groups. Others decided to stay at TOAS, the larger accommodation for many community members and use the public spaces there for hacking and talking. As usual, everyone was tired from a week of hard work and hard play.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 240px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/yfgm row row row your boat.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/yfgm row row row your boat-wee.jpg" width="240" height="180" ></a><br/>Row, row, row your boat... (by <a href="http://www.flickr.com/photos/jriddell/">YFGM</a>)</div>

<h2>Day Trip</h2>
On Thursday, we enjoyed another of those great Akademy traditions - the day trip. For those who don't know, each year Akademy visitors are taken by the local team on a trip for some relaxation and a taste of local culture. This year two buses took us 15 kilometers outside of Tampere - to a place in the woods. After a little walk, we ended up at a beautiful lake where we found a chalet with a few volunteers cooking food for us. A second chalet housed a traditional wood-fired sauna. There was also a camp fire so we could prepare our own food.

Illka gave us a short introduction to the local culture:
<ul>
<li>Sauna - Hot</li>
<li>Lake - Wet</li>
<li>Wood - few Bears this year</li>
</ul>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey;"><embed src="http://blip.tv/play/hIsige2QZwA" type="application/x-shockwave-flash" width="320" height="270" allowscriptaccess="always" allowfullscreen="true"></embed></div>

<h3>Activities</h3>

There were canoes and we could throw frisbees (points for every tree you hit). We were warned not to jump from the pier into the water as it's not deep and dead hackers would constitute pollution of the rather nice nature. We were urged to RTFM for the eco-toilets, which were indeed a tad complicated. There were bows and arrows for those into archery (we lost a few arrows, but apparently nobody got hit). And there were darts too. All this embedded in the woods, looking over the beautiful lake, which was quickly splashing with hackers. Apparently geeks are bad at physics with logs. There was a huge one in the water, tied up with a few ropes. There were constant attempts to get up on it, but there was a lot more falling than standing.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 180px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/ivan_foodmeister.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ivan_foodmeister-wee.jpg" width="180" height="240" ></a><br/>FoodMeister (by <a href="http://www.flickr.com/photos/ivan-cukic/">Ivan Čukić</a>)</div>The sauna was well visited, and a constant stream of people going from the sauna to the lake and back. The water in the lake was amazingly nice so there was a lot of swimming. The 6 canoes were in use most of the time. Some gearheads went for a walk in the forest - which was beautiful but also full of micro-vampires swarming all over you. We now have a bite-covered Chani and she's not alone in that regard.

Ade shared a canoe tip (mostly relevant if you join him in one): <i>"In a Canadian canoe you have to know how to roll over with it and come back up on the other side..."</i> Rest assured most canoers here in Finland did not have to know how to come back up.

<h3>Play and Work and Play</h3>

Meanwhile the food was excellent. We stayed until about 7 in the evening before being called back to the buses. All day the weather was incredibly pleasant. It was a bit more cloudy than the previous days, making it not too hot but just perfect. The relaxed atmosphere was of course very conducive to chatting with fellow hackers. The Plasma team worked on their usual world domination plans, as did some edu and promo people.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey;"><embed src="http://blip.tv/play/hIsige2QcQA" type="application/x-shockwave-flash" width="320" height="270" allowscriptaccess="always" allowfullscreen="true"></embed></div>

At night there was a bunch of people looking for a salsa party, others went to a gothic night, rock bar and two different karaoke places. Despite being tired from the day trip, many didn't return to bed very early - explaining the quiet atmosphere at Demola on Friday. Still, code got written, and people spent a lot of time working or discussing things. However, the BOFs were not that busy. In general, there was far less running around compared to earlier in the week.

<h2>Results</h2>

Concluding all that happened at this Akademy in a few sentences is obviously impossible - even the mail, twitter, blogpost, flickr and dot story stream gives only a shallow impression of what happened. However, a few things are worth noting especially.

<h3>Bigger Than Ever Before</h3>
First of all, Akademy was bigger than ever. Around 400 visitors came, followed the talks and joined the BOFs and hacking sessions. Moreover, we connected more to local communities than ever. We never had so many people from India; the Brazilian team was super active too. We were proud to have them here and we all expect that they will become enthusiastic ambassadors for the Free Desktop and KDE in their home countries!

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 180px;"><a href="http://dot.kde.org/sites/dot.kde.org/files/archery.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/archery-wee.jpg" width="180" height="240" ></a><br/>Archery (the next Hood) (by <a href="http://www.flickr.com/photos/ivan-cukic/">Ivan Čukić</a>)</div><h3>New Directions</h3>
KDE's chief motivator Aaron Seigo proposed Elegance as a common direction for KDE development to take. Elegance combines intuitive and beautiful user interfaces with technologically outstanding solutions. In the subsequent hacking sessions, many developers started thinking about how to make their software more elegant, and thus more attractive to use. Besides this general direction, KDE developers from all the different teams gathered and discussed the future of their products. There was discussion between teams as well. The Plasma and KDE Edu people discussed what to do for the kids in Brazil and elsewhere. The companies and non-KDE developers at the conference connected in various ways as well. <a href="http://www.midgard-project.org/">Midgard</a>, for example, was integrated as a proof-of-concept with Plasma by a 50-line Python shell providing access to Midgard's data storage technology.

<h3>Mobile KDE Software</h3>
Using KDE software on mobile devices was another big subject for discussion and coding. The KDE community is very interested in providing their software for mobile platforms like MeeGo. During this Akademy, work continued on making Akonadi (and by extension, the Kontact Groupware Suite) and the Plasma user interface library work on MeeGo. Like the KDE community, MeeGo aims to support a full spectrum of devices in terms of form factor and performance. Kontact Mobile provides the most scalable and powerful groupware client currently available for mobile devices, while the Plasma universal canvas provides the most mature high-level, extensible and brandable toolkit for mobile devices that are using Qt.

<h2>The End</h2>
On Friday evening your author gathered a few volunteers, did some shopping, and cooked dinner for about 50 people at the TOAS hostel. It turned out reasonably well, thanks to the great help from the hungry hackers. After food, people again went out for sauna or some beer and dancing. Others departed that evening or sometime early on Saturday. While this is being written, there are about 6 people left at TOAS, and Akademy 2010 is officially over. Your author can confidently speak for everyone when saying this has been an amazing Akademy!