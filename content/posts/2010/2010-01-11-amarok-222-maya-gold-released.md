---
title: "Amarok 2.2.2 \"Maya Gold\" Released"
date:    2010-01-11
authors:
  - "jbauer"
slug:    amarok-222-maya-gold-released
---
The Amarok team is proud to announce that Amarok 2.2.2 has been released. After 8 weeks of development and a beta release in mid-December, the source code and packages for some distributions are now available <a href="http://amarok.kde.org/wiki/Download">on the download page.</a>

The main focus of the developers for this release, besides new features, was to improve the UI and fix a lot of bugs. New features are the reappearance of custom labels, <a href="http://amarok.kde.org/wiki/Moodbar"> Moodbar support,</a> easier lyrics editing, much improved podcast handling and a lot of other stuff.

Amarok now depends on KDE 4.3.x and Qt 4.5 as minimum versions. Look forward to some nice upcoming things in Amarok that take advantage of the features which are provided by those new versions.

While you can enjoy the new release, the team is already deep in development for Amarok 2.2.3 which is scheduled to be ready at the beginning of March.

The official release announcement is available on <a href="http://amarok.kde.org/en/releases/2.2.2">the Amarok home page</a>.

Enjoy!