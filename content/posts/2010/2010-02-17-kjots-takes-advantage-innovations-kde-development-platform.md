---
title: "KJots Takes Advantage of Innovations in the KDE Development Platform"
date:    2010-02-17
authors:
  - "steveire"
slug:    kjots-takes-advantage-innovations-kde-development-platform
comments:
  - subject: "No KNotes maintainer?"
    date: 2010-02-20
    body: "We should mention this more someplace - it's a pretty simple application for someone to get their feet wet with.\r\n\r\nNice work, by the way, I'll probably use it for my thesis indeed :)\r\n\r\nCheers"
    author: "troy"
  - subject: "Great!"
    date: 2010-02-23
    body: "I like KJots a lot, and it's great to see it progressing!\r\n\r\nIf you want to see where you still can take \u201csuch a simple app\u201d, you can take a glance at MacJournal. That's what I used when I still was a MacUser, and KJots filled it's role for me. \r\n\r\n- http://www.marinersoftware.com/sitepage.php?page=85\r\n\r\nOne thing it offers (that I miss in kjots) is automatic export to multiple formats. For example I'd need an exporter to plain text or Markdown syntax. That way it would integrate into my workflow (version tracking my works and keeping almost everything in a simple, text-based, program independent format). \r\n\r\nBut anyway: KJots is great for quickly starting articles and just beginning to write instead of first thinking of where to write and to save. \r\n\r\nI'm anxious to try the new version myself!"
    author: "ArneBab"
  - subject: "Thanks Kjots"
    date: 2010-06-30
    body: "I really like using Kjots.    \r\nIt fits exactly the way I work. \r\nThat is, I use it all the time to make notes and reminders etc,\r\nwhile making notes for my site <a href=\"http://www.cinebasti.com\">India Movies Online</a> and <a href=\"http://www.onepakistan.com\">Pakistan News Portal</a> i can use it.\r\n"
    author: "joy123"
---
Work on porting KJots to Akonadi started a long time ago (around summer  2008!), and that effort is reaching a milestone this week. The ported version  of KJots has been merged into trunk for the next release of the KDE Software  Compilation where work will continue on it to refine features and fix bugs.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kjots-plasmoid_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kjots-plasmoid.png" width="350" height="262"/></a><br />KJots Plasma Widget</div>
Since development started on the 4th series of the KDE Software Compilation, many developers started to investigate the newest frameworks in KDE and how they could be used to integrate better and to use the new features they promised. KJots was no exception.

<ul>
<li><a href="http://plasma.kde.org">Plasma</a> promised a simplified pop-up version of KJots on the desktop, so that notes in the structure could be accessed in it just as easily as Klipper.</li>
<li><a href="http://nepomuk.kde.org">Nepomuk</a> promised the possibility of organizing notes by tag and semantic analysis of the note contents.</li>
<li><a href="http://pim.kde.org/akonadi">Akonadi</a> promised a way to access notes stored in another location, such as a remote FTP server, or a mobile phone, and enables the simultaneous access by the KJots main application and the plasmoid.</li>
</ul>

There was some <a href="http://commit-digest.org/issues/2008-09-28/">early progress</a>, but eventually it seemed that Akonadi didn't yet have all of the features needed to use it for KJots. After more development and several new releases, the KDE PIM Development platform has reached a state where it is ready to be used as the basis for KJots. The main result of that has been additions <a href="http://steveire.wordpress.com/2009/10/01/akonadi-its-all-sausage/">in the area of model view to Akonadi</a>, benefiting not just KJots, but all KDE PIM applications.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kjots-tagtree_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kjots-tagtree.png" width="350" height="262"/></a><br />The tag tree in KJots</div>A nice feature which comes for free now with Akonadi is Nepomuk integration. Organizing notes by tag is now trivial. There are missing pieces though, such as a UI in KJots for adding and removing tags.

Note that it is possible, and in some cases sensible, to have one note appear multiple times in the application in different tags. Of course, because the data of the note is shared, the content appears to the user to be synchronized.

It will be an interesting exercise to figure out how to get the best of both worlds in organizing notes in a traditional tree structure, as is currently possible, and organizing them by tag. Possibly in the same application, possibly not. With Akonadi, because so much of the code can be shared, it matters less if someone wants to use a different application than KJots to manage Akonadi notes. Of course, another application would have to be written, but it and KJots would share the same notes in the same way that plasmoids can share the same notes with KJots.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kjots-plaintheme_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kjots-plaintheme.png" width="350" height="262"/></a><br />KJots with a basic plain theme</div>
With the integration of theming using <a href="http://www.grantlee.org">Grantlee</a>, the appearance of KJots becomes much more customizable. Users and theme creators can have full control over the appearance of rendered books in KJots. In the example below, the theme is updated with a summary from each page and a little bit of extra colour. Artistic abilities notwithstanding, it does show how dynamic the displayed content can be.

Because the storage format for notes in Akonadi is identical to email, it may be possible in the future to use features typically used for email handling, and apply those features to notes. For example, a note could be encrypted just like an email and stored on a remote Kolab server. Multiple users with a shared encryption key could potentially collaborate securely on the same notes in almost real-time.

With the release of the KDE Development Platform 4.4, much of the shared libraries making an Akonadi port of KJots possible are in place. When version 4.5 the KDE Application Suite is released in July 2010, KJots will depend on Akonadi and reap all of the benefits described above and some of the features. There may of course be some feature regressions and new bugs appearing when old ones are closed, but that is natural when porting an application to such new and innovative technologies as Akonadi, Nepomuk and Plasma.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kjots-summarytheme_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kjots-summarytheme.png" width="350" height="262"/></a><br />KJots showing a summary view</div>KJots has been a part of KDE since before the 1.0 release over 10 years ago and has always changed with the times. That such a simple application can still integrate many of the new technologies available indicates the growing maturity of the KDE Platform.

Existing data stored in KJots will be migrated over to Akonadi automatically when the application starts. Existing data from KNotes will also be migrated to Akonadi, and will be instantly available in KJots. As KNotes no longer has a maintainer, that application will likely not be released in KDE SC 4.5. It will be replaced with Plasma-Akonadi notes and KJots. However, if you like using the KNotes plugin in Kontact and want to keep that interface to your notes, it would not be difficult to create a replacement KNotes using Plasma. All of the code needed to create Plasma post-it style notes already exists and could be used to implement a modern KNotes replacement. If you're interested in developing with some of the most innovative parts of the KDE frameworks, get in touch on kde-pim@kde.org or #akonadi on freenode.

The features described above are still in heavy development and are not suitable for testing in trunk yet. They should however be available in KJots 4.5.