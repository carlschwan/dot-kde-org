---
title: "An Interview with KDEPIM Contributor Tobias K\u00f6nig"
date:    2010-03-07
authors:
  - "giovanni"
slug:    interview-kdepim-contributor-tobias-könig
comments:
  - subject: "Nice to see these"
    date: 2010-03-08
    body: "I'm glad to see some KDE contributor interviews again - it had been a long time since the most recent ones from People Behind KDE.\r\n\r\nAkonadi seems one of those things that is just starting to fall in to place but we're not yet quite at the point where amazing stuff starts to take advantage of the new possibilities. I'm looking to forward to SC 4.5 and beyond."
    author: "Stuart Jarvis"
  - subject: "Printing is desperately missing..."
    date: 2010-03-08
    body: "I totally agree that proper printing features are missing. \r\n<ul>\r\n<li> E.g. with Okular I have never figured out how to print landscape A4 slides in 2-up mode (long-edge duplex). The slides are always size-reduced 50% (A7 instead of A5).\r\n<li> The print dialog always forgets all its settings!\r\n<li> Also, booklet printing is important to me.\r\n<li> Plus, Konqueror's printing seems to be utterly broken (mostly I only get blank pages with one or two words or boxes).\r\n<li> And finally, the duplex setting in kde's print dialog seems to mess up some cups setting, so that e.g. acroread or firefox are no longer able to properly set the duplex mode..\r\n</ul>"
    author: "reinhold"
  - subject: "Akregator"
    date: 2010-03-08
    body: "What about Akregator? Is it going to be ported to Akonadi by the release of KDE SC 4.5?"
    author: "Krul"
  - subject: "Kontact - Kolab or OpenXchange?"
    date: 2010-03-08
    body: "Some 5 years ago we started to use kontact in our office for email and calendar. NFS provides the email and ics files.\r\nWe want to take it to the next level with a \"real\" groupware backend.\r\nWe have not decided yet wether to choose Kolab or OpenXchange.\r\n\r\nAnybody with some experiences around? What plays best with Kontact?\r\nCan we sync the calendar to our mobile phones? How?\r\n\r\n\r\n"
    author: "thomas"
  - subject: "Print current page"
    date: 2010-03-09
    body: "I miss simple things like a \"print current page\" option. Yes the workaround is trivial, but having to check what is the current page number and define a degenerate page range each time feels kind of silly."
    author: "jadrian"
  - subject: "You'll be happy to know that"
    date: 2010-03-09
    body: "You'll be happy to know that Current Page has made it into Qt 4.7.  Once 4.7 is out each app will have to turn it on individually."
    author: "odysseus"
  - subject: "Yeah, those are a mixture of"
    date: 2010-03-09
    body: "Yeah, those are a mixture of missing features from Qt, app bugs, and bugs in Qt.  There's noises coming out from Qt that they want to sort some of these issues out in 4.8, and I've finally managed to start getting some of my feature code included into Qt, so we're moving slowly on that front as well.\r\n\r\n* Okular n-up, not sure where that problem lies, could be a CUPS thing, bug report please :-)  Does it work fine for portrait n-up?  Does printing landscape 1-up work ok?\r\n* Save Settings, well known issue, needs fixing in Qt itself, have a few proposals there\r\n* Booklet printing is a tough one, there's a few options, but they involve major work either in Qt or in CUPS\r\n* Konqi printing, nothing to do with the Qt print system or the dialog, it's the app code and it's actually a hard one for them to solve.\r\n* Duplex, sounds like a Qt bug\r\n"
    author: "odysseus"
  - subject: ":)"
    date: 2010-03-18
    body: "Ah, great! Thanks for letting me know."
    author: "jadrian"
---
Welcome to another interview with a KDE contributor. Last time we <a href="http://dot.kde.org/2010/02/21/interview-netbook-master-marco-martin-kde-plasma-team">interviewed Marco Martin</a> from KDE Plasma team, this time we talked with Tobias König from KDEPIM about Akonadi and the TV series <em>Lost</em> ;)

The original interview in Italian is available in <a href="http://kde-it.org/2010/03/06/intervista-su-kde-con-tobias-konig-sviluppatore-di-kde-pim/">Giovanni Venturi's KDE blog</a>.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/Tobias_Koenig_CLT-2008.jpg" width="350" height="262"/><br />Tobias König</div><b>Hello Tobias. Can you introduce yourself to our readers?</b>
I’m a 27 years old computer science student, living in Dresden, Germany. In my spare time I develop free software and try to advocate Linux and KDE software.

<b>When did you hear about KDE?</b>
The first time I read about KDE it was about version 1.0 Beta 4 in the German PC Magazine. After having read the article multiple times I decided to wanted to be part of KDE. Unfortunately I had neither a Linux installation nor knowledge of C++ development, only a Windows 95 PC and basic Pascal experience. So I walked to the local book store, bought a RedHat distribution and a ‘Learn C++ in 21 hours’ book and that’s where the journey started... :)

<b>How do you contribute to KDE?</b>
Most of the time I work on the KDEPIM project as maintainer of KAddressBook, however during the last months and years <a href="http://pim.kde.org/akonadi/">Akonadi</a>, the desktop PIM service is my favorite project.

<b>What is missing in KDE software?</b>
During the port from KDE 3 to Platform 4 we sacrificed the KPrinter framework in favor of using QPrinter, however the latter is missing most of the useful features that are necessary to make KDE software compelling for PC pools at universities or in companies. This is really something that needs to be fixed!

<b>What are the KDE applications that are doing a very good job? And any that perhaps need some love and extra help?</b>
I regularly use Konsole, Konqueror and <a href="http://okular.kde.org/">Okular</a> and they do a very good job IMHO :) To the applications that need love and help I’d count most of the KDEPIM applications, because we are still lacking developers in this area. Especially after porting to Qt4 and Akonadi, there are many construction sites left where help is needed.

<b>What do you think of KDE software on Windows and Mac OSX? Can the KDE community gain new KDE users and contributors this way?</b>
My full respect to the KDE on Windows and MacOS X guys, they are really doing a great and important job. We are now (or at least, very soon ;)) able to provide a complete office suite and PIM suite on all three major platforms, including mobile devices as fourth. That’s a killer feature when deploying software in companies or governmental institutions and this will strengthen the position of KDE software in the desktop world, be it on free or on non-free platforms.

<b>Will Akonadi become a Free Desktop standard? Will it be used by other projects?</b>
We tried to apply to be a Free Desktop standard some months ago, however the Free Desktop project had (or still has?) some internal problems with communication and decision making processes, therefor our application got stuck somewhere and we concentrated on hacking and improving the software instead. Of course we hope that other projects (like e.g. Mozilla Thunderbird, Gnome's Evolution) will make use of Akonadi as well, the major blocker so far is the missing Glib-Binding for the Akonadi access library. If somebody with C and Glib knowledge wants to start on this project, we’ll provide as much support as possible :)

<b>What are your plans for the applications in the upcoming KDE PIM 4.5 release?</b>
For 4.5 we plan to have an Akonadi based KMail, KOrganizer and <a href="http://userbase.kde.org/KJots">KJots</a> (maybe we'll manage to get more PIM applications ported). The basic porting is already done in SVN trunk, however we still need some time to iron out all the porting bugs and make use of the new possibilities that Akonadi provides us. Next to the local address book, calendar and mail store we’ll have support for the <a href="http://www.kolab.org/">Kolab</a> and <a href="http://en.wikipedia.org/wiki/Open-Xchange">Open-Xchange</a> groupware servers and all other groupware servers that support the <a href="http://en.wikipedia.org/wiki/CalDAV">CalDAV</a> or <a href="http://en.wikipedia.org/wiki/GroupDAV">GroupDAV</a> protocols. My personal plans are to bring back some functionality in <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a> that people really missed in the 4.4 release.

<b>What are the new features in Software Compilation 4 that you like and that you use every day?</b>
I really like the <a href="http://www.youtube.com/watch?v=dv1Nu4425g8">cover switch effect</a> when switching between windows with Alt+Tab ;)

<b>What is your favorite KDE application?</b>
There are several, however the KDE application that provides functionality that no other free software application provides in that way is definitely Okular.

<b>What Linux distribution do you use?</b>
Debian unstable, that allows me to avoid compiling all the 3rd party libraries myself but still having up-to-date packages.

<b>Are you being paid to develop for KDE?</b>
Partly, I did some small things for <a href="http://www.credativ.de/">credativ GmbH</a> but 99.9% of my contributions were done in my spare time.

<b>What motivates/keeps you motivated to work on KDE?</b>
Definitely the other contributors! It’s great to talk with people that share the same interests and working together with them towards the same goal. Unfortunately during the last years we have more and more users that seem not to understand how Free Software works... they send us irreverent emails out of frustration about issues they have with KDE applications. They think we are some kind of paid support desk and don’t understand that we do all the work for free in our spare time and therefor are not required to do whatever is so important in their opinion... this is sad and very discouraging :(

<b>If you could choose where to live what place you will choose and why?</b>
Some place in Scandinavia, Sweden or Norway, next to the sea. Don’t get me wrong, Dresden is a really nice city and it will always be part of my life ;) but the mentality and pragmatism in Scandinavia fits my understanding of a society much better than the current climate in Germany.

<b>Have you ever stayed in Italy? If so, where?</b>
I have been in northern Italy for one day during a holiday in Austria several years ago (15 or so), unfortunately I can’t remember what the name of the place was...

<b>What’s the last book you read?</b>
The Leopard, a Norwegian crime novel of my favorite author Jo Nesbø.

<b>Have you ever followed the <a href="http://abc.go.com/shows/lost">TV series Lost</a>?</b>
No, just recently asked a friend about the plot :)

<b>What do you think about it? How will it end?</b>
As far as I understood it is about an island that acts as a time machine... so my guess is that the people living there will enter a time paradox and the creator of the island will die before he is born which will cause the series to never have existed in our parallel universe ;)

<b>Thank you for your time Tobias and good luck with your many projects!</b>