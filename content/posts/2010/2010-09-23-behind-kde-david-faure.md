---
title: "Behind KDE: David Faure"
date:    2010-09-23
authors:
  - "tomalbers"
slug:    behind-kde-david-faure
comments:
  - subject: "Cool guy"
    date: 2010-09-25
    body: "Nice to read an interview with David. I have read many mails and commit messages by him and he seems to be one of the hearts and minds at the core of KDE. Great that there are people like him.\r\n"
    author: "mutlu"
  - subject: "One missing question"
    date: 2010-09-26
    body: "How do you achieve this amazing productivity?\r\n\r\nI always wondered how one man can be that productive. And that in just 20 hours a week. David must have found the holy grail of software development."
    author: "slangkamp"
  - subject: "My educated guess would be:"
    date: 2010-09-26
    body: "My educated guess would be: focus.\r\n\r\nDo one thing, do it well. No email checking every 15 minutes (or push email notifications), random phone calls, or twitter updates. That, combined with skill: so you don't need much time searching documentation. Just have one hour of focused work, rinse, repeat."
    author: "vdboor"
  - subject: "Charm time tracker"
    date: 2010-09-28
    body: "How does charm compare to Ktimetracker?"
    author: "testerus"
  - subject: "Productivity"
    date: 2010-10-15
    body: "That productivity is just a myth :)\r\nPeter Penz (dolphin) is way more productive than I am, in terms of commits for instance. I spend quite some time helping people on IRC these days (which is fine, and probably even more useful than simple coding).\r\nAnd I pick some hard bugs so sometimes I spend 2 days debugging (like right now, in Qt's drag-n-drop code, to fix a KDE bug).\r\n\r\nSearch also techbase for \"productivity\", I made a wiki page with some tips on saving time doing some repetitive tasks (mostly about how to set things up for KDE development).\r\n\r\nAnyway, the best asset for productivity is definitely knowledge/experience, indeed; if you know how to do something, you don't need to spend time searching how to do it. Or if how to debug something efficiently, you don't pick the slow way of doing it...\r\n"
    author: "dfaure"
---
This week the Behind KDE interview is with a well known old-timer. He is the guru of kdelibs, but do you know what he does for KDE Sysadmin? Or should I say 'did'? Leave a comment if you get his old timer joke. Meet the one who moved files manually in the CVS era. Meet the guy who developed for KDE before some of you were even born. See how Charm and jazz play an important role in his life. <a href="http://www.behindkde.org/node/793">Meet David Faure!</a>