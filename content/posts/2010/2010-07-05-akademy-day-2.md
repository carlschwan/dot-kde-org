---
title: "Akademy Day 2"
date:    2010-07-05
authors:
  - "Stuart Jarvis"
slug:    akademy-day-2
comments:
  - subject: "woot, videos."
    date: 2010-07-06
    body: "To those at Akademy, please give the video crew a big, warm hug - it's great to be able to see the talks already!"
    author: "Eike Hein"
  - subject: "Absolutely"
    date: 2010-07-08
    body: "It's been a great effort from everyone in the local team to get each talk videoed (and with the general great running of everything). However, Kenny Dufus is the real hero here, he barely stopped for a couple of days getting videos processed and them and the slides uploaded."
    author: "Stuart Jarvis"
---

After a <a href="http://dot.kde.org/2010/07/04/impression-first-day-akademy-2010">successful first day of talks and the Akademy party</a>, hundreds of KDE contributors returned to the University of Tampere for the second day of the conference. Already, the slides and videos are starting to be uploaded on the <a href="http://akademy.kde.org/program/conference">Akademy website</a>, for those of you who couldn't make it to Akademy or are here but couldn't attend two presentations at once.

<h3>The Talks</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1000814_0.JPG" width="262" height="350" /><br/>Lukas paints with Krita (by <a href="http://www.flickr.com/photos/45211080@N07/">Stu</a>)</div>If there were any sore heads from the previous night's party, they were kept well hidden as the sessions kicked off with lively talks from Lukas Tvrdy about Krita and Park Shinjo about localization. Lukas' talk even included a live demonstration of his artistic skills as he showed off some of Krita's brushes.

The lightning talks started straight after. These short presentations of ten minutes each give contributors a chance to introduce a topic or raise interest for workshops later in the week. This session included a highlight of the conference, as Frederik Gladhorn introduced <a href="http://fluffy.jussi01.com/">Fluffy</a> (a Linux distribution combining KDE software, pinkness, bunnies, unicorns, magic and - of course - lots of fluff) to the wider KDE community.

After a break, we got into the serious business of learning about KDE software performance (and how to measure it) with Lubos, while Frank continued to uncloud the ownCloud project. We learned about the KDE Educational applications and KDevelop 4, with the KDevelop speakers requesting greater involvement from the wider community of developers who benefit from this great IDE.

<h3>Elegance</h3>

After lunch, Aaron Seigo led with his keynote talk, "Reaching for Greatness", in which he reflected on the many achievements of KDE, but also the problems and opportunities in coordinating a growing community and becoming more active on different platforms.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/aaronbythinkfat.jpg" width="262" height="350" ><br/>Aaron's keynote (by <a href="http://www.flickr.com/photos/thinkfat/">thinkfat</a>)</div>Aaron noted some of KDE's many successes: 52 million school children in Brazil using KDE software with over a thousand more in universities; 400,000 laptops in 2009 and another 300,000 in 2010 being deployed in Portugal; a million deployments in Venezuela and over 11,000 German government systems utilizing KDE apps too. Given this backdrop, he urged the KDE community to remember what we do well and not dwell on the negatives - nothing is ever perfect, but in reality success is possible when things are good enough.

This is an exciting time for KDE, but also a time of challenges. Qt is now fully open - in its development as well as in its licensing - and KDE developers should get more involved, submitting patches and influencing the project.

KDE has also expanded globally. Clearly this is a success but it makes coordination more difficult - we need consensus in what we do but should not mistake that for requiring unanimous agreement. Everyone needs to respect the consensus in a team for the good of the community even when they may not agree with it. Decisions and  the reasoning behind them should be better documented to save time repeating discussions that have already come to a conclusion.

Most of all, Aaron sought elegance in KDE. Focusing on elegance can help us 'reach for greatnesss' and add the polish to our software that will make it the best out there. Achieving elegance will be an iterative process. It can happen at a community wide level or be applied to even the minor details of an individual product. He suggested having a focus on a few key issues for each Software Compilation release cycle so that over time everything will become elegant.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/icecreambythinkfat.jpg" width="400" height="268" ><br/>Free ice cream tastes better (by <a href="http://www.flickr.com/photos/thinkfat/">thinkfat</a>)</div>Slides and the video of Aaron's talk are already online on <a href="http://akademy.kde.org/program/conference">the Akademy site</a> as is the video from Saturday's keynote. More slides and videos are being added all the time.

<h3>Outside the conference rooms</h3>

As always, there was at least as much chat and sharing of ideas outside the talks as in them. A <a href="http://www.basyskom.de/index.pl/enhome">Basyskom</a> sponsored ice cream cart outside the main entrance hosted an impromptu discussion of the problems of getting KDE applications on to Windows in a form that is convenient and familiar to Windows users. We managed to catch up with KDE-Windows developer Casper van Donderen for a quick interview, one of many that you can view in the <a href="http://www.youtube.com/user/kdepromo#grid/user/3F38BF428EFB4017">Akademy Playlist</a> on YouTube.

Just like day one, the seating outside of the conference rooms was busy with the buzz of chatter and the sound of rapid typing of code and IRC discussion. Productivity was further increased by the lack of any inconveniently scheduled football matches to distract those attendees that still have an interest in the World Cup results.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1000875.JPG" width="400" height="223" ><br/>Thanking the organizers (by <a href="http://www.flickr.com/photos/45211080@N07/">Stu</a>)</div>The morning session was rounded off by the Akademy group photo. This year the process was quite quick and painless with the attendees barely having time for two sweeps of a Mexican wave and a few words with their neighbors before it was all over and time to go for lunch.

<h3>Akademy Awards</h3>

The penultimate session of the conference was made up of our <a href="http://akademy.kde.org/sponsors/">sponsor</a> presentations - the take-home message from that was that if you can code in Qt then most of our sponsors have plenty of great job opportunities for you, so just check out their websites for more details.

Wrapping up the conference part of Akademy, we had the Akademy awards to recognize those who have made great achievements over the past year (and longer). First, the organizing team was thanked for all their hard work that has made the event possible and they received a standing ovation from everyone in the room.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-akademy-award-2010-winners.jpeg" width="371" height="270" /><br/>Award winners Aurélien, Anne and Cornelius collecting for Burkhard</div>

Every year at Akademy, three awards are given to community members who have made an outstanding contribution during the past year. The winners of two of these (the awards for best application and non-technical contributions) are chosen by the previous year's recipient, while the third is decided in a jury vote.

Last year's application winner, Peter Penz of Dolphin fame, chose <b>Aurélien Gâteau</b> for his work on Gwenview. This image viewer and editor is a shining beacon of usability and powerful features, so Aurélien's award is well deserved.

Last year's non-technical contribution award winner, usability expert Celeste Lyn Paul, chose <b>Anne Wilson</b> to receive this year's award. Anne is well known throughout the community for her work in user support and everyone agreed afterwards that she was an excellent choice for this award. Anne coordinates a lot of the work on Userbase and the KDE Forums and plays an important role in reminding developers to take users' needs into account. She is a friendly face to many users and we greatly appreciate that!

Finally, the Jury's award went to <b>Burkhard Lück</b> for his fantastic work on improving the state of KDE documentation. Unfortunately, Burkhard could not attend, so Cornelius accepted the award on his behalf and joined Aurélien and Anne in the winners' photo.

After the awards, the conference part of Akademy was closed, although of course the Akademy event continues all week with meetings and hacking sessions. There probably won't be an article tomorrow, as many attendees were busy in the e.V. meeting. For non-e.V. members there was also plenty to do, with hacking facilities at 'Demola, a <a href="http://akademy.kde.org/program/meetnokiaqt">meet Qt</a> session and the opportunity to take the exam for the Nokia Certified Qt Developer for free!