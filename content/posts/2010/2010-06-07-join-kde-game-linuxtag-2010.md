---
title: "Join the KDE Game at Linuxtag 2010"
date:    2010-06-07
authors:
  - "sebas"
slug:    join-kde-game-linuxtag-2010
---
This year, like every year, KDE will be present at Linuxtag, which is held from 9th to 12th in Berlin. As usual, visitors will be able to meet KDE contributors, chat about KDE and KDE-related topics, get a demonstration of the newest hotness that will be released as part of KDE SC 4.5.0 this summer, learn about the background of the KDE community and the technology it creates and of course just catch up with what's going on in KDE-land -- and that's a lot!
<!--break-->
Of course, there are also a lot of KDE related talks in the presentation tracks:

<ul>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=262">What Is Special About the QML Declarative Language?</a> by Daniel Molkentin, Thursday 10:30, Europa I
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=223">Beyond Groupware: Thinking Differently About KDE PIM And Kolab</a> by Paul Adams, Thursday, 11:00, Berlin I
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=264">KDevelop 4 - Schneller C++ Programmieren</a> by Milian Wolff, Thursday, 11:30 in Europa I
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=230">Koffice 2</a> by Sebastian Sauer, Thursday, 16:30 in Berlin I
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=323">Mobile Development with Qt and Qt Creator</a> by Daniel Molkentin (again), Friday, 11:00, Berlin I
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=445">On being Free</a> by Jos Poortvliet, Saturday, 12:00 in Berlin II
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=450">The Multimedia Aficionado's Guide to KDE SC and Friends</a> by Nikolaj Hald Nielsen, Saturday, 16:30 in Berlin II
    </li>
    <li>
    <a href="http://www.linuxtag.org/2010/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=451">Freeing the web from the browser</a> by Frank Karlitschek, Saturday, 17:00, Berlin II
    </li>
</ul>

<h2>The Game</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org//sites/dot.kde.org/files/pawn.png" width="203" height="330"/></div>This year is a bit special because of something unrelated to the talks. <a href="http://ev.kde.org/">KDE e.V.</a> (the supporting foundation for the KDE community) will announce the launch of the Individual Supporting Membership Programme. This new programme is made available for those who would like to support KDE, but do not have enough time to do so by contributing directly. The Supporting Membership Programme comes with a membership fee of €25 quarterly. Besides the warm, fuzzy feeling of doing something to support the continued development of the Free Desktop, all supporting members are also in for some surprise presents!

The Supporting Membership programme will officially be launched at the Radio Tux stage, on the first day of Linuxtag, Wednesday, 9th, at 11.00 o'clock. <a href="http://en.wikipedia.org/wiki/Georg_C._F._Greve">Georg Greve</a>, the founder of the <a href="http://www.fsfe.org/">FSFE</a> and the holder of the German <em><a href="http://en.wikipedia.org/wiki/Federal_Cross_of_Merit">Bundesverdienstkreuz</a></em> will be the first to Join the Game. He explains why he has chosen to become a supporting member:

<blockquote style="font-weight: bold;">"KDE has awesome technology developed by an incredible community that has had my support for years, so it's an honor to join the game!"</blockquote>

Additionally, 4 of the first people to sign up as supporting members during the first two days will be invited to a cozy dinner with some prominent KDE people (KDE's founder for example). Sounds exciting? Sign up! Naturally, you'll also be able to become a supporting member if you're not attending Linuxtag. Details will become available online in tandem with the physical announcement. So stay tuned to join the game!