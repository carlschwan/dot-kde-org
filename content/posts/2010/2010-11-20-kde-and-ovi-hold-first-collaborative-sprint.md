---
title: "KDE and Ovi Hold First Collaborative Sprint"
date:    2010-11-20
authors:
  - "leinir"
slug:    kde-and-ovi-hold-first-collaborative-sprint
comments:
  - subject: "Ovi share?"
    date: 2010-11-20
    body: "There is also Ovi share: <a href=\"http://share.ovi.com\">http://share.ovi.com</a> which is the single service of Ovi I find useful.\r\n\r\nI would love to see a way to manage pictures more easily from KDE. The Web interface is quite bad to upload pictures."
    author: "Ikipou"
  - subject: "Contributions :)"
    date: 2010-11-21
    body: "Good to hear about that too, and yes that would indeed be great :) Feel free to join in the writings of the document, it is open to everybody - and just to catch that one, don't worry about your language skills, i know a lot of people are all scared about how they may not be sufficiently skilled in English to help with such things. That is why we're helping each other, others will catch that :)"
    author: "leinir"
  - subject: "A couple of points."
    date: 2010-11-22
    body: "Hi ya,\r\n\r\nJust wondering if the kdepim and Marble guys had input to this?\r\n\r\nI'm looking into the geolocation and mapping api at the moment, and the big issue I see is the Ovi terms of service, i.e. only non-commercial use in non-competing apps with no modifications or caching.  Any plans to change that, otherwise it's a killer afaik.  As noted, while the mapping api is ok, it is tied only to Ovi, there's no OSM or other free data plugins as yet.\r\n\r\nAlso, shouldn't the wiki page be on community.kde.org under an appropriate category, rather than a top-level techbase page?\r\n\r\nJohn."
    author: "odysseus"
  - subject: "is there any chance to have"
    date: 2010-11-22
    body: "is there any chance to have Ovi Suite on kde? A tool for manage mobile phone and its software update (like update maps of a mobile phone, or its firmware, etc)."
    author: "loverdrive"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/ovi-sprint-1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ovi-sprint-1-sml.jpeg" /></a><br />KDE and Ovi Sprint attendees</div>The weekend before <a href="http://qt.nokia.com/qtdevdays2010/">Qt Developer Days 2010</a> in Munich, Nokia invited thirteen members of the KDE community to get together in their offices in that same city for a developer sprint. The topic was <a href="http://www.ovi.com/services/">Ovi</a> and KDE, specifically how they can work together to widen adoption for both communities, both from a software development perspective and from a purely collaborative effort perspective. The developers were also invited to the main Qt Developer Days (DevDays) event too.
<!--break-->
<h2>Ovi? What's that?</h2>

In addition to being the Finnish word for "door", Ovi is Nokia's service offering. Thus, rather than just being an app store or a mapping tool, it encompasses all of the services which Nokia offer. Depending on the country, you will find any number of services:

<ul>
<li>Contacts: A storage solution for contact information. In Nokia terms, this is a way of backing up your phone’s contacts, and sharing them between multiple phones.</li>
<li>Mail: An email system provided by Nokia, in the style of Hotmail and GMail.</li>
<li>Calendar: As the name implies, a calendar system - store events and other such details, and access to them through the web or on a phone that supports the services.</li>
<li>Maps: A Web and application system for viewing and using maps of the world - road maps and the like. This includes navigation support.</li>
<li>Store: The piece of Ovi most people know, the application store is a way of purchasing applications for Nokia’s devices.</li>
<li>Music: The newest kid on the block, Ovi Music provides access to over 11 million items of DRM-free downloadable music.</li>
</ul>

The Ovi umbrella also includes the gaming platform N-Gage, though that is not yet pushed as hard as the other services.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><embed src="http://blip.tv/play/AYKJszIC" type="application/x-shockwave-flash" width="480" height="300" allowscriptaccess="always" allowfullscreen="true"></embed><br />Ovi and KDE Sprint Wrapup (<a href="http://blip.tv/file/get/Swjarvis-KDEOviSprintDiscussion657.ogv">Ogg Theora version</a>)</div><h2>What happens at a Sprint?</h2>

In contrast to popular belief, a developer sprint is not a place where a bunch of programmers sit down and get lots of code written; that is a hack-fest or a code sprint. A developer sprint is very different; the main focus is on discussions. The Ovi and KDE sprint is perhaps one of the most extreme examples of this to date, as not a single line of sprint-related code was written. The days were focused entirely on discussions and planning.

Three big pieces of paper were put up on the wall, titled in turn: "What can Ovi do for KDE?", "What can KDE do for Ovi?", and "What features could be added to Ovi?". With pens hanging from a piece of string beside the papers, people were invited to add items when they popped into their heads, without any hesitation or interference.

Later, another piece of paper was added to the wall, dedicated to figuring out how specifically KDE might be able to help with the now discontinued Ovi Files service. This was filled with technical arguments on why the <a href="http://owncloud.org.">ownCloud project</a> would be a suitable technology platform for a replacement service, as well as why it would make business sense to go that way.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ovi-sprint-2-sml.jpeg" /><br />Passionate conversations</div>Each of the sprint participants selected the areas they were most interested in. This allowed the sprint to break out into smaller groups based on certain topics. At the end of the second day, the members got back together in the meeting room to present what they had done over the course of the sprint. A video of this roundtable is above.

<h2>Sprinting Together</h2>

One of the things about developer sprints that needs highlighting at every opportunity is that they allow developers and other contributors to get together in person and work with each other in ways that are simply not possible on IRC and mailing lists. This sprint was no different.

Rune mentioned that he needed arguments for using Qt rather than Android at UNF.dk (a mobile development camp run by the Danish Youth Association of Science). Pradeepto offered to put him in contact with Jarmo Rintamaki, the manager at Forum Nokia responsible for contact with universities and other educational institutions. This finally happened at DevDays. Networking is the name of the game.

Another good bit of networking came out of DevDays. Some of the participants encountered a company that wanted more from Ovi, including rich clients. Thanks to the Open Collaboration Services and libAttica, this is one of the KDE Platform’s core strengths. The company, Orange (specifically their London-based research and development division Orange Labs), offered to help where they could.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ovi-sprint-4-sml.jpeg" /><br />Sprint attendees sharing a tasty meal</div><h2>Gifts and Promises</h2>

Knut Yrvin, well-known community manager of Qt Frameworks Division, and Norwegian national hero, helped set up the KDE-Ovi sprint, and brought three N900s and five N97 Minis to be handed out to sprint participants. The N900s were given to <a href="http://saidinesh5.wordpress.com/2010/10/17/my-first-kde-sprint-and-the-qt-dev-days/">Sai Dinesh</a> of the SyncEvolution project, Mark Kretschmann of Amarok and Arjen Hiemstra from the Gluon project. Arjen got straight to work getting GluonGraphics ported. Two days after getting home, he presented us with <a href="http://www.youtube.com/watch?v=pgYtn8Tc7ss">a video of the Invaders example game running on Maemo5</a> - laggy, but running. The N97 Minis were handed out to others who needed devices for development and experimentation - including Rune who, as mentioned above, needed arguments to convince others that Qt is a suitable platform for teaching youngsters how to program for mobile devices.

<h2>Production Begins</h2>

The results of a code sprint are easy to measure: certain pieces of functionality implemented, APIs reviewed and so on. However, for a developer sprint, in particular one such as this, the achievements are not as easily quantified. So, to describe the value of this sprint, please consider that before it started, the participants were not very aware of what Ovi and KDE might be able to do for each other. The primary accomplishment of this sprint is the scope and precision of what was written on those pieces of paper mentioned earlier. The raw data was gathered on <a href="http://techbase.kde.org/DiscussionNotes">a wikipage on TechBase</a> and a document was begun, putting into a more solid, presentable form the ideas which were born at the sprint.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ovi-sprint-5-sml.jpg" /><br />Working on different devices</div>The single most important request that came out of the sprint was for Ovi to embrace open standards generally and begin implementing web APIs for their services. For example, they could use the Open Collaboration Services for Ovi Store, and the OpenStreepMap APIs for Ovi Maps. This should be something that appeals to Ovi, since they already supply open APIs for some of their service offerings such as the PIM solutions.

<h2>The Next Beginning</h2>

What lies ahead now is:
<ul>
<li>expand and refine the document mentioned above,</li>
<li>implement specific recommendations, and</li>
<li>use Ovi services with open APIs in software produced by the KDE community.</li>
</ul> 
This is a large undertaking; one that is not limited to the participants of this sprint. We invite anybody who has an opinion or an interest in the Ovi services to join in. We want Nokia to understand how Ovi and KDE can work together and why it would benefit both.