---
title: "KDE Italia Announces New Website"
date:    2010-01-14
authors:
  - "opensourcecat"
slug:    kde-italia-announces-new-website
---
<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/logo-kde-it.png" width="150" height="135"/></div>
The admins and editors behind the Italian KDE Portal and forum are proud to announce the availability of the <a href=" http://www.kdeitalia.it">new KDE Italia site</a>.

By announcing the availability of both the new site and the forum (which is on the <a href="http://forum.kde.org/viewforum.php?f=140">official kde.org forums</a> now), the KDE Italia team invites everyone to visit and have a look. There's not much there for now, but content is growing every hour thanks to the dedicated team working on it!
<!--break-->
The new address is <a href=" http://www.kdeitalia.it">http://www.kdeitalia.it</a>, but the old address will still work for a while.

Creating the new site, the KDE Italia team focused more on helping new users to get started and discover the KDE community and its many products. A few how-to articles are being worked on and a section on the site provides links where you can try KDE software using the best KDE friendly distros. Even a KDE Italia customised openSUSE Live cd is being developed!

By making it easier to submit stories, news and ideas to the team, we aim to make contributing to the new site easier.

If you want to help the KDE Italia team please use the <a href="http://www.kdeitalia.it/contact">"Contatti" menu button</a> on kdeitalia.it.

We hope you all, and every visiting potential new user, will like the site. If you have any comments, critisism, ideas or other input, please <a href="http://www.kdeitalia.it/contact">contact us</a>!