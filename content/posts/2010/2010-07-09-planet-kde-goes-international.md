---
title: "Planet KDE Goes International"
date:    2010-07-09
authors:
  - "ungethym"
slug:    planet-kde-goes-international
---
<p>During Akademy the promo team discussed the idea to have localized Planets in different languages. The same day, Planet Master <a href="http://jriddell.org/">Jonathan Riddell</a> jumped on it right away, and already the Planet in Spanish is online.

Blogs from various Spanish speaking bloggers and KDE contributors are aggregated there. Everyone interested in what is going on in the Spanish language KDE community can <a href="http://planetkde.org/es/">visit the site</a> where you can also find an RSS-feed for easy syndication.

Meanwhile the system admins and the promo team are looking to add more local communities to the Planet. If you have a suggestion or want to help out, let us know!</p> <p>If you write KDE related posts in Spanish you can add your blog by following the instructions on the top of Planet KDE ES.</p> <div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 793px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Planet-es.png" width="793" height="641" ><br/>Our new Planet ES</div>