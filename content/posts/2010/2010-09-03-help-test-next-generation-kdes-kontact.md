---
title: "Help Test the Next Generation of KDE's Kontact"
date:    2010-09-03
authors:
  - "sebas"
slug:    help-test-next-generation-kdes-kontact
comments:
  - subject: "Where is the source branch in svn?"
    date: 2010-09-03
    body: "Can somebody point to correct SVN branch where I can get source code to build from?"
    author: "lure"
  - subject: "I am willing to test it, but"
    date: 2010-09-03
    body: "I am willing to test it, but don't want to build it. Are there kubuntu or opensuse packages available somewhere?"
    author: "gg-lechner"
  - subject: "Lionmail and KDEPIM 4.4.93?"
    date: 2010-09-03
    body: "Hey Sebas, thank you for the enlightening \"Demystifying Akonadi\" article. It was a great read and made me try the beta. :)\r\n\r\nSo I installed KDEPIM 4.4.93 and am now trying to compile Lionmail. Unfortunately, it does not compile with the recently released KDEPIM beta. It seems that it needs akonadiconsole from trunk, which was ported to KViewStateMaintainer with revision 1160389. However, I cannot just install the trunk version of it since some PIM parts were moved into other parts of KDEPIM. I wonder, though, whether akonadiconsole 4.4.93 (just released) is older than August 8 (the date of the port)?\r\n\r\nIn general, it would be great if there was a Lionmail that works with the KDEPIM betas.\r\n\r\nThanks,\r\n\r\nmutlu\r\n\r\nP.S. Here is the build error:\r\n\r\n[ 54%] Building CXX object emailnotifier/CMakeFiles/plasma_applet_emailnotifier.dir/emailnotifier.o                             \r\n/var/abs/local/lionmail-svn/src/lionmail-build/emailnotifier/emailnotifier.cpp:39:39: fatal error: akonadi/etmviewstatesaver.h: No such file or directory\r\ncompilation terminated.\r\nmake[2]: *** [emailnotifier/CMakeFiles/plasma_applet_emailnotifier.dir/emailnotifier.o] Error 1\r\nmake[1]: *** [emailnotifier/CMakeFiles/plasma_applet_emailnotifier.dir/all] Error 2\r\nmake: *** [all] Error 2\r\n"
    author: "mutlu"
  - subject: "Hmm... it should be"
    date: 2010-09-03
    body: "Hmm... it should be this:\r\n\r\nsvn://anonsvn.kde.org/home/kde/trunk/KDE/kdepim\r\n"
    author: "mutlu"
  - subject: "openSUSE"
    date: 2010-09-03
    body: "openSUSE has packages: https://build.opensuse.org/project/show?project=KDE:Unstable:SC:kdepim45"
    author: "jospoortvliet"
  - subject: "Fedora"
    date: 2010-09-03
    body: "I'm installing prebuilt packages for Fedora13 from the kde-unstable repo using yum at this very moment.\r\n\r\n"
    author: "awinterz"
  - subject: "Re: Hmm... it should be"
    date: 2010-09-03
    body: "No, that's trunk.\r\n\r\n4.5beta3 source is at tags/kdepim/4.4.93/kdepim\r\n\r\n4.5 in progress is at branches/KDE/4.5/kdepim\r\n\r\n"
    author: "awinterz"
  - subject: "Migrations"
    date: 2010-09-03
    body: "For me migration is probably the most significant thing that concerns me so I'll step up and offer some time for testing.  Thanks for raising the issue, Seb.\r\n"
    author: "esdaniel"
  - subject: "too involved"
    date: 2010-09-06
    body: "I'd need to copy the relevant classes into the Lion Mail sources in order to achieve that. These copied classes play a pretty important role in setting up the widget, so they cannot just be commented out. This time can IMO better be spend on finishing Lion Mail for its first release.\r\n\r\nIf you want to run it, you can either copy those classes yourself, or install kdepimlibs and kdelibs from trunk.\r\n\r\n"
    author: "sebas"
  - subject: "Arch Linux beta3"
    date: 2010-09-06
    body: "I tested the beta3 and still needed to return to the stable one. As everything else worked fine but it just was slow and every email what I downloaded were empty. Just showing from, to who and subject. \r\n\r\nNeed to fill bug reports to BKO."
    author: "Fri13"
  - subject: "Thank you for your response,"
    date: 2010-09-06
    body: "Thank you for your response, Sebas. Surely, you want your plasmoid to use all the available goodies of 4.6 since it won't be released earlier anyway. I think it is perfectly understandable that you made it depend on classes from trunk. I doubt I will actually install kdepimlibs and kdelibs from trunk since this is a production machine. Maybe I will have a go at copying the classes.\r\n\r\nI any case, I am very excited about your work in this area. I have always been annoyed by the limitations of systray applets for GMail and other mail providers, and your work promises to overcome all this in the wonderful \"silky\" manner. You have real vision. Thanks for that.\r\n\r\nCheers!"
    author: "mutlu"
  - subject: "Similar problems"
    date: 2010-09-07
    body: "I tested the beta3 in Arch Linux, too, with similar problems. The whole Desktop became *extremely* slow and unresponsive. Seemed to me that it was mainly a problem with very large local mail folders (several folders with >1000 mails), because with an empty set of the \"local folders\", it worked quite well.\r\n\r\nMoreover, migration did non complete because of one of my imap-servers not supporting the UIDPLUS extension. Without a workaround found at bugs.kde.org I would not have been able to start kmail at all.\r\n\r\nAt least, I did not have empty emails - all mails showed up properly if you had enough time to wait...\r\n\r\nIf I find enough time for it, I will generate a test account in order to file some bug reports."
    author: "ChrDr"
  - subject: "UIDPLUS"
    date: 2010-09-07
    body: "Also hit the UIDPLUS problem (on the university's Exchange server).\r\n\r\nThe bug report you refer to is:\r\nhttps://bugs.kde.org/show_bug.cgi?id=236109 ?\r\n\r\nSo you can start KMail by removing the account, but then you can presumably not add that account without the fix in trunk?\r\n\r\nStill, this is why we have betas... If the trunk fix doesn't make it into a released KDEPIM 4.5 then we'll have to make this issue very clear in the release info\r\n\r\n"
    author: "Stuart Jarvis"
  - subject: "Ubuntu specific bug"
    date: 2010-09-09
    body: "I tried this release from Fedora and Ubuntu repos. The Ubuntu release (running on Maverick Meerkat) fails spectacularly with GMail IMAP accounts. OTOH, Fedora packages work great with the same GMail IMAP accounts (it does have bugs related to content display, but those bugs are evident, so they should be hammered by now)\r\n\r\nI'm giving this warning because there's the possibility of massive bug reports against Beta 3 for a Ubuntu specific bug.\r\n\r\nEdit: Also, if you are not running Ubuntu, you'll surely miss the count of how many times KMail/Akonadi is faster than the old KMail while handling IMAP accounts. To say \"hundreds of times faster\" is to underestimate the difference: with my GMail IMAP account, the sync time went from days (no kidding, it often took 3 or 4 days for KMail to get all the mail) to ~20 minutes.\r\n"
    author: "Alejandro Nova"
  - subject: "It's not a bug, but a feature."
    date: 2010-09-09
    body: "I experienced the same slowdown, because Strigi attempted to index all of my mail and integrate it to Nepomuk. As a side effect, KMail/Akonadi became unresponsive.\r\n\r\nWhen the indexing finished (I gave it an hour or two for ~20,000 mails), KMail/Akonadi and my system performance came back to normal. There's some optimization work that could be done."
    author: "Alejandro Nova"
  - subject: "openSUSE repos dependancy issues"
    date: 2010-10-01
    body: "I just added the repos from that link and am getting an error saying that there are dependencies on kdebase-runtime >= 4.5.2 I can't find any repo with that version anywhere, I have 4.5.1 in kde-factory repos, and 4.5.72+svn or something to that effect in the kde-unstable repos. Any suggestions so I can test just the new kdepim betas?"
    author: "linuxdave"
---
The KDE PIM team has <a href="ftp://ftp.kde.org/pub/kde/unstable/kdepim/4.4.93">made available</a> a beta version of the next-generation groupware client suite Kontact. The new Kontact is built on the Akonadi framework, sharing infrastructure for syncing with online services across applications. While the first bits of Akonadi integration already made their entry in KAddressBook as delivered with 4.4, this beta includes Akonadi versions of most of the other Kontact components, including email using the new KMail2.

As the Akonadi-based Kontact marks a major step in the evolution of KDE's groupware apps, the KDE PIM developers decided to apply a feature-based release schedule to this port. It will be released when it's ready, which is realistically still this year. In order to achieve this, we rely heavily on community feedback, so please give this beta a run and get back to us with your feedback, preferably in the form of bug reports on <a href="http://bugs.kde.org">bugs.kde.org</a>.

KDE PIM Beta3 is not suitable for production use. For those who rely on stable groupware applications, the right choice is the KDE PIM we have delivered with KDE Applications 4.4 (latest version is 4.4.5). This version remains fully supported and the recommended choice for everyone but testers and those interested in the next-gen Kontact.

For more detailed information about this beta version, for Akonadi in general and for migration scenarios to and from Kontact-based Akonadi, please refer to <a href="http://vizzzion.org/blog/2010/08/demystifying-akonadi/">this blogentry</a>, or the <a href="http://www.akonadi-project.org">Akonadi Project page</a>.