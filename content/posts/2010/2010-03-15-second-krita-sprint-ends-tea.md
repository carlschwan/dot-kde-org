---
title: "Second Krita Sprint Ends With Tea"
date:    2010-03-15
authors:
  - "Boudewijn Rempt"
slug:    second-krita-sprint-ends-tea
comments:
  - subject: "Wow."
    date: 2010-03-16
    body: "I leave you guys 'n' gal alone for a couple of weeks and you go and revolutionise everything?! Faster, more focused, more memory efficient, better usability, a number of ideas implemented I'd been thinking but hadn't even suggested yet (eg scratchpad). Korel *ahem* Corel Painter had better watch out! ;)\r\n\r\nAlso, four words that nearly make me weep with joy:\r\n\"middle click now pans\" \\o/\r\n\r\nHere's to a strong, creative vision and busting our butts to get there! Now I've *really* got to make the next sprint!"
    author: "Bugsbane"
  - subject: "Really I would like"
    date: 2010-04-22
    body: "Really I would like to know more on this group and your achievements <a href=\"http://defensivedrivingcourse.jimdo.com/\">.</a> <a href=\"http://allprodriving.livejournal.com/\">.</a> <a href=\"http://defensivedrivingcourse.wordpress.com/\">.</a> "
    author: "joeanderson"
---
It's Sunday now in <a href="http://en.wikipedia.org/wiki/Deventer">Deventer</a> and, except for Lukas Tvrdy, all Krita hackers have gone home -- or, in the case of your author, stayed home. Time for tea and writing a recap of the whole sprint and hackfest!
<!--break-->
<h3>Arriving and setting goals</h3>

The first Krita hackers started arriving on Thursday 25th, with the rest filtering in during the Friday. Thanks to KDE e.V. sponsorship, six Krita developers (every single one from a different country) were able to come as well as <a href="http://www.mmiworks.net/eng/publications/blog.html">interaction designer</a> -- Peter Sikking, of Gimp fame. The seventh Krita hacker was already in place!

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/krita-team.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/krita-team-small.jpg" width="300" height="200"/></a><br />The Krita team at the meeting: Lukáš, Vera, 
Sven, Peter, Dmitry, Boudewijn, Cyrille and Adam.</div>

On Friday morning, disaster struck. A large fuse in front of the hacking house blew up, leaving the developers without electricity and thus also without internet access! Funnily enough, the neighbors weren't affected. The early arrivals went into the church's cellars to hack, hack and hack until the whole crowd had assembled. Cyrille Berger has already <a href="http://blog.cberger.net/2010/02/26/krita-meeting-2010-–-day-1/">blogged about what happened</a>.

The most important part of the actual sprint was Saturday morning: Peter had offered to come to Deventer and moderate a discussion on the team's vision for Krita. <a href="http://www.mmiworks.net/eng/publications/2010/03/working-on-vision-with.html">Peter's blog</a> is very informative and both <a href="http://blog.cberger.net/2010/02/27/krita-meeting-2010-–-day-1-2/">Cyrille</a> and <a href="http://www.valdyas.org/fading/index.cgi/hacking/lastweekend.html">yours truly</a> have also written about the discussions. However, the topic deserves some deeper discussion, here on the Dot.

To fully appreciate the situation and the decisions, it is necessary to be aware of Krita's history.

<h3>A Brief History of Krita</h3>

Krita started, as KImageshop, in 1998. As that name clearly indicates, Krita was meant to be a Photoshop (5.0) clone. The reason KImageshop was part of KOffice did not have anything to do with having a vision: it was mainly because there was already an image component in KOffice and because KOffice offered some easy to use foundations for handling windows, views and documents.

Some work was being done, but then the project stagnated, was renamed to Kray, and someone else -- John Califf -- showed up, did some more work, and then went away. The project stagnated again for a while, got renamed to Krita, and then was picked up by Patrick Julien, who did a lot of refactoring, making the code much faster and cleaner -- but disabling a lot of functionality in the process, such as painting.

In 2003, your author joined the project after obtaining a graphics tablet and wanting to sketch and paint. Krita looked like a nice project to get involved, with plenty to do, like implementing a brush tool. As early as <a href="http://dot.kde.org/2004/08/07/kde-cvs-digest-august-6-2004">2004</a> a desire for a change of focus was apparent: "I want Krita to occupy the niche Corel Painter has in the Windows world, not Photoshop."

Still, it's fair to say that the Krita team never really nailed down exactly what Krita should be. All sorts of functionality and features crept in that made Krita an application inconsistent with itself. The team never really addressed what the position of or reason for being a painting application within an office suite was. The hackers might, at times, have felt vaguely uncomfortable, or have been blissfully unaware -- but there was never active work towards making Krita useful specifically within KOffice.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/krita-preset-palette1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/krita-preset-palette1-small.png" width="300" height="225"/></a><br />The new preset palette.</div>

In the meantime, the porting of Krita to KDE Platform 4 and KOffice 2 had started. That was quite a struggle, because Qt was shifting, the KDE libraries were shifting and KOffice itself was shifting. Real life issues played a part too. Technically, the ideas were challenging. The team had an enormous list of cool things they wanted to get in. In short, Krita 2.0 took a very long time -- and when the team stopped <a href="http://lists.kde.org/?l=kde-kimageshop&m=125294942417927&w=2">to look at where they had arrived at by 2009</a>, there still wasn't a very clear idea of the priorities or ultimate purpose.

<h3>What (and who) is Krita for?</h3>

With this decade of development history behind us, Peter sat down with the seven Krita developers present at the sprint (representing the majority among perhaps ten active developers). Some were new to the project, some were old hands. Peter started straight away by asking the hard questions, never stopping until he got a straight answer. It wasn't actually a very difficult discussion, but it was hard work and it took a lot of time. So, here we have the new, official, Krita vision:

<ul>
<li>Krita is a KDE program for sketching and painting, offering an end–to–end solution for creating digital painting files from scratch by masters.</li>
<li>Fields of painting that Krita explicitly supports are concept art, creation of comics and textures for rendering.</li>
<li>Modelled on existing real-world painting materials and workflows, Krita supports creative working by getting out of the way and with snappy response.</li>
</ul>

As developers, the team is still finding its way towards working within this vision. This is illustrated by <a href="http://lists.kde.org/?l=kde-kimageshop&m=126751510631899&w=2">the discussion</a> on the Krita mailing list of whether a particular set of filters should be retired to extensions.krita.org or kept in the default set. We are not even totally certain what really is useful only for photography, and what can be used by artists to work faster and do things that would be impossible otherwise.

<h3>A clear vision leads to rapid progress</h3>

From that Saturday morning onwards, development was extremely brisk! With Peter Sikking present to help out with pressing questions, and despite being hampered by bugs in the <a href="http://lukast.mediablog.sk/log/?p=203">graphics tablet support</a>, the team rapidly reworked the quick-access popup palette and the brush settings/presets palette and added a useful scratchpad area (with infinite canvas). Thanks to Justin's help, Krita got a new combination slider/textedit/spinbox widget that gives visual feedback about how much of the available range is used and which direction means what. The brush outline cursor was fixed. Many bugs were squashed, from more than 100 to 70, even while new bugs were being reported!. The team improved <a href="http://lukast.mediablog.sk/log/?p=209">the performance in key areas</a> and reduced memory consumption a lot. Of course, there is still a lot left to do if the Krita team want this autumn's release, 2.3, to be the first of the "user-ready" releases.

By Thursday the remaining Krita hackers certainly needed a break! This was provided with a visit to the <a href="http://blender.org">Blender Studio</a> in Amsterdam. The visit to the Blender Studio was exciting enough in itself: the hackers got treated to a weekly meeting of the artists and coders, all showing off last week's work on <a href="http://durian.blender.org">the Project Durian movie</a>. There was also the chance to hear the artists' own views about their needs, and Blender's coders about their ideas, as well as learn from Ton about growing a free software project.

In summary: a week and (almost) a half of dedicated hacking -- it was both exhausting and energizing!