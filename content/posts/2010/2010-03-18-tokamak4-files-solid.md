---
title: "The Tokamak4 Files: Solid"
date:    2010-03-18
authors:
  - "sebas"
slug:    tokamak4-files-solid
comments:
  - subject: "Network management and bluetooth"
    date: 2010-03-18
    body: "Are there any plans to integrate other network management tools into KDE? I ask only because of a fondness for wicd and seems to work rather well for me and is (afaik) a little lighter in footprint than networkmanager.\r\n\r\nI really look forward to decent integration of bluetooth particularly headphones into solid. Right now I have been using blueman instead of kbluetooth because of the headphone need. Though, it is not a totally smooth ride with it.\r\n\r\nThe Upnp integration sounds like a great idea. I recently had reason to start fiddling with that type of thing since switching to AT&T U-verse and their STB supports it. "
    author: "stumbles"
  - subject: "Seems it is too early in the"
    date: 2010-03-18
    body: "Seems it is too early in the morning and I cannot read. Looks like there are plans for wicd... super.\r\n\r\n"
    author: "stumbles"
  - subject: "Yes, I'm hoping for better"
    date: 2010-03-18
    body: "Yes, I'm hoping for better bluetooth integration as well. Bluetooth thetering is on my wish list too, though I understand it might require some work to get there."
    author: "vdboor"
  - subject: "Bluetooth tethering"
    date: 2010-03-18
    body: "Yes, I think that Bluetooth tethering would be very nice to have. Let's wait a bit until the Bluetooth guys have submitted their work, then we can have a look at what's needed to make this work nicely.\r\n\r\nI've recently upgraded my mobile plan to also includes a data package, so I can actually try this and likely help implementing a nice workflow for this."
    author: "sebas"
  - subject: "thanks"
    date: 2010-03-19
    body: "thanks"
    author: "yuri.debura"
  - subject: "Awesomeness."
    date: 2010-03-19
    body: "Great to see so many improvements and usability enhancements hitting such a core part of KDE (especially network management).\r\n\r\nThanks for all your work people!\r\n\r\nPS \"Blue Devil\" is a very funky (if slightly obscure) name. Love it!"
    author: "Bugsbane"
  - subject: "Available wireless networks"
    date: 2010-03-19
    body: "Does the list of currently available wireless network tell which ones use encryption? (I'm not sure what the red/green symbols mean in that screenshot). If no, is there any plan to include that?\r\n\r\nI know I can do an iwlist scan in a terminal but it's nicer if I can see that directly in the gui...\r\n\r\nThanks"
    author: "tendays"
  - subject: "Wireless Encryption"
    date: 2010-03-19
    body: "Yes, they do. The icon on the right indicates the encryption, red for no encryption, yellow for weak (usually WEP) encryption and green for secure encryption (WPA2 for example). Hovering over these icons shows a tooltip giving some details."
    author: "sebas"
  - subject: "Re"
    date: 2010-03-19
    body: "Ah, great. Thanks."
    author: "tendays"
  - subject: "Good quality tutorial and guide to networking"
    date: 2010-06-29
    body: "Good quality tutorial and guide to networking has helped us a lot; thanks for sharing."
    author: "ccexolip"
---
This is the first article in a series of articles that wrap up achievements, work in progress and some background of what happened during Tokamak4, the Oxygen, KWin and Plasma sprint. Tokamak4 took place from 19th to 26th February 2010 in Nürnberg, Germany in the openSUSE premises and was kindly made possible by Novell and KDE e.V. During the sprint, 26 hackers gathered to work on various aspects of the KDE user experience. The combined sprint of the workspace and window manager teams and the Oxygen artwork team made cross-collaboration possible across these KDE software components.

In this article, we actually start off with a side-track of Tokamak4 which was hardware integration. It turned out that many people working on this were actually participating in Tokamak4, so the team took the opportunity to sit together and hack some on different aspects of hardware integration in KDE SC and specifically Plasma.
<!--break-->
<h2>Solid Plans for the Future</h2>

During Tokamak 4 in Nürnberg, several developers of Solid, the KDE Platform component that offers integration of hardware and networking functionality to KDE applications in a platform-independent way, sat together and discussed their <a href="http://mail.kde.org/pipermail/kde-hardware-devel/2010-February/000701.html">plans for the future</a>. One important change is that multiple Solid backends can now be loaded at the same time, providing information from different sources in parallel. A first implementation of this change has been done by Kevin Ottens during Tokamak 4. This change allows for more flexibility within Solid, especially for the support of new middleware such as upower, udisk and the like.

The Solid hackers, or <em>Metalworkers</em> as Kevin "ervin" Ottens started to call the crew, have also decided to expand the scope of Solid a bit to virtual devices and devices on the network. One important addition that is planned, and already in part implemented by Friedrich Kossebau is support for UPnP.

<h2>UPnP</h2>

More and more devices that support UPnP are popping up in your author's network. The media center and NAS offer their content via UPnP to the network, the phone and mediacenter are able to play those files directly over the network. With KDE targeting a wide device spectrum, UPnP cannot be missed out any longer. Friedrich Kossebau has now landed the first bits of UPnP support into Solid, based on the work by Bart Cerneels. The code uses the Coherence server as underlying middleware and already allows detecting UPnP media servers on the local network.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:510px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/upnp-in-solid-now-with-hierarchie.png"/><br />Solid's hardware browser showing UPnP devices</div>

For interested students, there are proposals for possible projects taking part in the <a href="http://community.kde.org/GSoC/2010/Ideas#Project:_Amarok_.26_KDE_UPnP_integration">Summer of Code</a> to bring this work to further fruition.

<h2>Network Management</h2>

The Network Management team, Will Stephenson, Alexander Naumov and Sebastian Kügler sat together to work on functionality to easily connect the computer to wired and wireless and mobile broadband networks and to VPNs. The Network Management Plasmoid which has the focus of the developers now uses a service which can "talk to" different underlying network management systems. Currently, there is a backend for NetworkManager and another one for WICD is in the works. This allows the developers to build a UI suitable for a given device rather than having to duplicate work for the underlying layers as well.

Some distributions already ship KDE 4 network management in the form of KNetworkmanager, a small application that sits in your notification area and offers network connections using a context menu. During the meeting, several UI improvements have been worked out which combined will create a natural workflow in a simple but very powerful user interface. Many of the changes that were discussed have already been implemented. The Network Management team has also gained some fresh blood, in the form of Kubuntu-hacker Aurelien Gateau, who has already committed an impressive number of bugfixes and UI improvments and with Shantanu Tushar Jha, who has improved the visual feedback for unplugged network cables. You can find more information on Sebastian Kügler's blog, <a href="http://vizzzion.org/blog/2010/02/tokamak-iv-network-management/">here</a> and <a href="http://vizzzion.org/blog/2010/02/tokamak-finished/">here</a>.
<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:720px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/network-management-plasmoid.png"/><br />The new Network Management Plasma Widget</div>

In the meantime, many of the design choices the team has made are already implemented in the network management plasmoid. Many interaction bugs have been fixed, VPN support for the plasmoid has been implemented and the first test results of this new piece of hardware interaction UI are promising.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:585px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/network-management-settings.png"/><br />Setting up network connections</div>

<h2>Bluetooth</h2>

Independently from Tokamak, but closely related to all the work that is going into Solid these days. Alex Fiestas, Rafael Fernandez Lopez and Eduardo Robles Elvira have decided to give lots of love to the bluetooth functionality in KDE SC. The system the Spanish developers are working on is called BlueDevil and aims to provide bluetooth service discovery, pairing, file browsing and transfer. The system will be built of a kded module which receives and multiplexes bluetooth events via DBus and a KIO virtual file system slave for file management. The bluetooth System Settings module will make it easy to manage your bluetooth devices. Alex Fiestas regularly updates us on his blog at <a href="http://www.afiestas.org/as-promised-kbluetooth-0-4-2/">afiestas.org</a> about the current state of things