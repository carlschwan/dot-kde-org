---
title: "A week of post-beta bug squashing!"
date:    2010-11-26
authors:
  - "mleupold"
slug:    week-post-beta-bug-squashing
comments:
  - subject: "KDEPIM & kmail2"
    date: 2010-12-01
    body: "I'm trying to file some bugs after trying out beta1.\r\nSo far I'm very satisfied with the desktop itself.\r\n\r\nKmail2 ist kinda problematic in its current state I think. I don't know how to file some helpful bug reports. I have about 25.000 mails in my inbox. Old kmail was stable and quick enough to deal with this. Searching was fast, displaying the message list was fast.\r\n\r\nKmail2/Akonadi does not really crash. it simply hogs up all the available memory and grinds the system to a halt while it tries to index 5GB of emails...\r\n\r\nSecond, my $home is on NFS... So Akonadi runs a Mysql instance locally while the db-file is on a remote fs... ;-( With \"Maildir\" it was o.k.\r\n\r\nI think I need some expert advice. I'd really like to use Akonadi/kmail2, but how to do it the right way?\r\n"
    author: "thomas"
  - subject: "Is there any point ? Serious"
    date: 2010-12-02
    body: "Is there any point ? Serious bugs have been found, and confirmed, but persist in multiple subsequent releases.\r\nFor instance:\r\nPower button shuts down unconditionally instead of hibernating because the acpi-scripts are broken wrt KDE4 (https://bugs.launchpad.net/ubuntu/+bug/553557) - because KDE always generates a defunct process (https://bugs.kde.org/show_bug.cgi?id=236490).\r\nThere are also serious visual glitches that remain uncorrected : https://bugs.kde.org/show_bug.cgi?id=243886\r\n\r\n<sigh>\r\n\r\n"
    author: "Tom Chiverton"
  - subject: "Bug fixing"
    date: 2010-12-03
    body: "Have any of your reports led to fixes? If so, keep at it; you'll likely get some success.  If not, and they've languished for > 6 months, I can understand some frustration, especially if you've gone to the trouble to write really good, detailed bug reports. It's probably worth remembering (if you don't already) that different bits of code are maintained by different people. Some developers will get to your report quickly, and some not.\r\n\r\nDeveloper time is just scarce. I've written some really nice bug reports (spent hours on them) that have languished more than a year with nary a peep. Others get looked at in a couple of hours. KDE seems to have about the same spectrum of response times as any other project I've reported bugs to. And those that get fixed, get fixed in someone's spare time, which is also worth remembering!\r\n"
    author: "tangent"
  - subject: "It's not that as such.\nIt's"
    date: 2010-12-03
    body: "It's not that as such.\r\nIt's that the bugs are so obvious, they shouldn't even be in a shipping product.\r\n\r\nThe power-button data-loss bug is obvious to anyone who uses KDE4 on Ubuntu. How many people get frustrated and don't post ? Why isn't it noticed by the maintainers ?\r\n\r\nThe less serious but experience ruining graphical glitches likewise."
    author: "Tom Chiverton"
  - subject: "Obvious bugs"
    date: 2010-12-04
    body: "Sometimes they are obvious and should have been fixed. But I'd guess the much more common scenario is that for some reason they are more obvious to you than the developer(s). I didn't take time to look into your specific bugs, but differences in habits (especially when there are two or more ways to achieve the same thing) can mask some bugs---the developers may simply never do things in the way you do, and so they don't see the problem. And differences in hardware can account for most of the rest. Little things like monitors of different resolutions, for example. I recently switched to a larger external monitor and uncovered all sorts of graphical glitches (probably in the ATI driver, not KDE) that simply weren't there when I was using my laptop's native resolution (Kubuntu Lucid; probably many are fixed in Maverick, but I haven't yet upgraded out of fear of pulseaudio).\r\n\r\nI'd bet that the average active developer's KDE is a bit smoother than the average user's KDE, simply because when they discover one of those problems they can fix it for their use case.\r\n\r\nOne of the things I see frequently is offers to buy hardware for the developers. I can't tell whether that's a helpful offer or not. On one hand it would make testing a lot easier and more direct, and result in faster bugfixes. On the other hand, if I were a developer, the last thing I'd want is 25 laptops littering my apartment and the obligation to build, install, and test myself on all of those. Testing on a wide array of hardware is sort of what the community is for, and seems like a fair exchange for the privilege of freedom.\r\n\r\nDo any developers want to speak out on this point?\r\n"
    author: "tangent"
  - subject: "I'm very happy to test it for"
    date: 2010-12-04
    body: "I'm very happy to test it for them, and use a fair amount of  beta software at home and work - for which I log bugs.\r\nI don't see why a developer needs to have a great variety of hardware - as you say, that's what we're for.\r\nBut I do expect serious bugs (data loss because of shut down instead of hibernate, security because screen saver isn't active on resume from hibernate) to be addressed rapidly, and if deemed as bad as they look, hold back a release for. Those two I mention occur on all hardware.\r\nSerious bugs shouldn't languish *forever* in confirmed-by-multiple-comments-from-different-people-but-not-officially-confirmed hell."
    author: "Tom Chiverton"
  - subject: "Bugs"
    date: 2010-12-06
    body: "> Serious bugs shouldn't languish *forever* in confirmed-by-multiple-\r\n> comments-from-different-people-but-not-officially-confirmed hell.\r\n\r\nIndeed.\r\n"
    author: "tangent"
---

In January the KDE community is set to ship major new versions of the Plasma workspaces, the KDE applications and the KDE development frameworks. As usual, a series of beta versions and release candidates is released in the months leading up to the release day, with the purpose of letting users test the changes in the upcoming versions and help us find and fix bugs and regressions.

The first of these pre-releases, <a href="http://kde.org/announcements/announce-4.6-beta1.php">4.6 Beta 1</a>, is out now, and you can help the KDE team ensure the upcoming final release is stable and solid: <a href="http://techbase.kde.org/Contribute/Bugsquad">the KDE BugSquad</a> team plans on having a Beta 1 Bug Squashing week, with the aim of having as many bugs as possible reported and to triage them together.

<b>When will it happen?</b>
The Bug Squashing week will take place between Saturday, November 27th and Saturday, December 4th.

<b>How can you help?</b>
You can help the KDE BugSquad team in two ways:
<ul>
<li>By trying out Beta 1 (see below on how to do that) and reporting all the bugs you find in it.</li>
<li>By helping us triage the incoming bug reports to make sure they are valid, detailed and there are no duplicates.</li>
</ul>

You do not need to be a developer or have specific skills to help.

<b>What needs to be installed?</b> 
You should preferably be running either 4.6 Beta 1 or a checkout of KDE trunk. It is possible to use a <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/Beta46Week1#LiveCD">Live CD</a> with 4.6 Beta 1, and some distributions even provide <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/Beta46Week1#Packages">packages</a> for the beta release. For instructions on running KDE trunk, please see <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">the guide on the TechBase wiki</a>. You can also help with a recent stable version, such as 4.5.3, at this point.

<b>Where are people going to meet?</b> 
The Bug Squashing activities will be coordinated on IRC, in the <a href="irc://irc.freenode.net/kde-bugs">#kde-bugs</a> channel on <a href="http://www.freenode.net/">Freenode</a>. Further instructions and lists of bugs being triaged are available on <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/Beta46Week1">TechBase</a> as well.

Bug triaging is a very nice way of getting involved with the KDE community. Join the BugSquad team &mdash; you'll have a lot of fun and will be contributing to make a more successful KDE release day!