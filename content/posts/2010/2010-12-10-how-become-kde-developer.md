---
title: "How to Become a KDE Developer"
date:    2010-12-10
authors:
  - "oriol"
slug:    how-become-kde-developer
comments:
  - subject: "How to become a KDE contributor"
    date: 2010-12-11
    body: "Where can one find the procedure for becoming a contributor?\r\nWhat qualifications are needed?\r\nHow can one tell if one is qualified or not?"
    author: "kdelvr"
  - subject: "There are links to"
    date: 2010-12-11
    body: "There are links to information about getting involved in various ways at http://www.kde.org/community/getinvolved/\r\n\r\nYou do not need any formal qualifications. It's very likely that you will have the skills to contribute in one of the areas and otherwise it is possible to develop them.\r\n\r\nYou can tell if you have the right skills by giving something a try, listening to and learning from the feedback and improving those skills. I think that applies to all of us."
    author: "Stuart Jarvis"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Antonis.jpg" /><br />Antonis Tsiapaliokas</div>In late 2009, Antonis Tsiapaliokas had his first contact with Linux. At that time, he had just started learning the C programming language and he was mostly unfamiliar with KDE software. Less than two years later, he just made his first code contributions to KDE software. 

Seasoned KDE developer Tomaz Canabrava, who helped Antonis develop his programming skills, asked him why he chose to contribute to KDE. Read on to find out more on how Antonis became a KDE developer.
<!--break-->
<b>Tomaz:</b> Hi Antonis. Could you please start by introducing yourself to our readers?

<b>Antonis:</b> Hi! My name is Antonis Tsiapaliokas and I am 19 years old. I am a computer science student at the <a href="http://www.teilar.gr/index_en.php">Technological Educational Institutes</a> (T.E.I.) of Larissa, Greece,  but my hometown is Katerini-Katalonia.

<b>Tomaz:</b> How did you first become interested in Linux?

<b>Antonis:</b> Everything started in September of 2009, when I began taking classes at the T.E.I. I knew about Linux since my high school years, but back then I never considered switching to a new operating system because I was able to do everything I wanted to do with Windows. Moreover, I thought that learning a new operating system would be boring. But when I moved to Larissa I suddenly had a lot of free time and I thought to myself 'let's give it a try', and I installed Slackware in my system for the first time. After that, I tried to find out more about Linux through web searches, and I also learned quite a lot from LinuxTeam, a group in the T.E.I. of Larissa which tries to spread knowledge about Linux and its benefits to the student body.

<b>Tomaz:</b> How did you start using KDE software?

<b>Antonis:</b> In the Christmas of 2009 I removed the Slackware installation from my computer, which was using Xfce 4, and I installed Debian Testing with KDE's Plasma Desktop 4.3. I had installed Kubuntu and Slackware with Plasma Desktop 4.1 and 4.2 in the past but they were too buggy, so I ended up using Xfce 4 instead. I then realized that the Plasma Desktop was very stable and flexible in Debian Testing, and so within a few days I replaced Xfce 4 with Plasma Desktop permanently.

<b>Tomaz:</b> Why did you decide to contribute to KDE?

<b>Antonis</b>: After I started using Plasma Desktop regularly, I became very impressed with the work that KDE developers are doing and I decided that I wanted to help them. I first tried to contribute as a Debian packager of KDE software, but I very soon realized that packaging is not for me. At around that time I took a class at the T.E.I. that focused on C programming, which quickly became my favorite course. I then went to KDE's Wikipedia page to find out what programming language was most used in KDE software. When I saw that it was C++ I thought to myself "great! KDE software is written in a language closely related to my favorite one!" I had come across other languages in the past, such as PHP, MySQL, Javascript or Python, but they all seemed to me either very simple or very difficult without even offering any advanced features.

<b>Tomaz:</b> So how did you take your first steps to becoming a KDE developer?

<b>Antonis:</b> At that point, I started researching the requirements to become a contributor to KDE, and I decided that I should learn C++ and Qt. I wrote a post to the KDE developers mailing list asking for advice, and you kindly offered to help. Back then my programming knowledge was limited. For instance, I knew what functions and variables were and how to use them, but I had to go through a lot of pain and trouble until I was able to write a program using pointers. Thanks to your help, I managed to advance in my understanding of C++ and object-oriented programming and, later on, of Qt, to the point that I feel comfortable using them to write serious code today.

<b>Tomaz:</b> Have you been able to make any code contributions to KDE software?

<b>Antonis:</b> Yes! I have submitted some patches for <a href="http://games.kde.org/game.php?game=kmahjongg">KMahjongg</a> and I am also preparing a new patch for KMail 2.

<b>Tomaz:</b> Thank you for your time and good luck with your future contributions to KDE!

<b>Antonis:</b> Thank you!