---
title: "KDE's Marble Team Holds First Contributor Sprint"
date:    2010-11-15
authors:
  - "tackat"
slug:    kdes-marble-team-holds-first-contributor-sprint
---

The first Developer Conference for the <a href="http://www.marble-globe.org">Free Software Virtual Globe <em>Marble</em></a> ended successfully on November 7. Almost a dozen developers from the Marble community (the "Marbleheads") met for a weekend sprint at the <a href="http://www.basyskom.de/index.pl/enhome">basysKom</a> office in Nuremberg.

<div style="width: 502px; float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/marble_sprintSmaller.jpg" /><br/>From left to right: Bernhard Beschow, Niko Sams, Dennis Nienhüser, Thibaut Gridel, Torsten Rahn, Sebastian Wiedenroth, John Layt <br/>Not shown, but present at sprint: Jens-Michael Hoffmann</div>Marble 1.0 will be released in January 2011. In preparation, the sprint attendees analyzed the current state of the Marble client and its Qt map widget library (<em>libMarble</em>). The presentations covered innovative features such as the new worldwide Online and Offline Routing capabilities in Marble 1.0 (videos - <a href="http://blip.tv/file/4348928">Alternative Routes</a>, <a href="http://blip.tv/file/4348942">Offline Routing</a>, and <a href="http://blip.tv/file/4348946">Route Guidance</a>). Dennis Nienhüser demoed the new and upcoming navigation features.

Bernhard Beschow presented the first results of the <a href="http://shentey.wordpress.com/2010/11/10/marble-meets-opengl/">OpenGL development</a>. Inclusion of OpenGL into Marble is currently scheduled for Marble 1.2; the proper integration of this capability into the Marble library was an important topic (video: <a href="http://blip.tv/file/4354030">Prototype OpenGL</a> - the Ovimaps mode is not part of Marble and will not be distributed). Other developer sessions covered <em>libmarble</em> Qt Quick Bindings (videos: <a href="http://blip.tv/file/3628925">search</a>; <a href="http://blip.tv/file/3628969">Where is that?</a>), Marble's internal <a href="http://www.opengeospatial.org/standards/kml/">OGC KML</a> based data structures, the Mac port and improved support for Digital Elevation Models. 

The current Marble 0.10.0 was the first release that brought official support for mobile platforms. So the Marble MeeGo version and the current state of Qt Mobility were at the center of discussions. The new changes to the Marble user interface for the mobile and desktop version are also important. Several proposals for user interface changes were made and lots of promising solutions were found.

During the last day, the group covered topics such as community outreach, improved marketing and incubation with <a href="http://www.osgeo.org">the OSGeo foundation</a>. The Marble Team is looking for new volunteers to help with marketing, evangelism and documentation. The Sprint ended with a hacking session where bugfixes and newly discussed features were developed. 

The Marble team thanks the two sponsors of the meeting: <a href="http://ev.kde.org">KDE e.V</a> and <a href="http://www.basyskom.de/index.pl/enhome">basysKom GmbH</a>. It was a great experience to meet the other Marble developers face by face, which made the event extremely successful and productive. The Marble Team is looking forward to the next Marble conference which is planned for sometime during the next year.
 
Some meeting slides and other resources are <a href="http://techbase.kde.org/Projects/Marble/MarbleMeeting20101107">available on KDE TechBase</a>.