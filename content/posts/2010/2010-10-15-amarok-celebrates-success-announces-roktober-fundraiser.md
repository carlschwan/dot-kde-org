---
title: "Amarok Celebrates Success, Announces Roktober Fundraiser"
date:    2010-10-15
authors:
  - "nightrose"
slug:    amarok-celebrates-success-announces-roktober-fundraiser
comments:
  - subject: "Without a doubt the best"
    date: 2010-10-16
    body: "Without a doubt the best media player around, and an awesome & helpful dev team too. Got my donation in."
    author: "Sentynel"
  - subject: "My favorite software, ever"
    date: 2010-10-16
    body: "Amarok is my favorite app ever, on any platform. The developers are wonderful human beings, and love to make their users happy. I support them 100%, and have made my contribution. \r\n\r\nBy the way, thanks for adding Google Checkout. It took me less than a minute to send off my contribution.\r\n\r\nAwesome!"
    author: "valoriez"
  - subject: "Thanks a lot for Amarok, but care to give some more details? :)"
    date: 2010-10-16
    body: "Hi Amarok Devs!\r\n\r\nThanks for a great piece of software! I hope you can put my donation to good use. :)\r\n\r\nNow that this is -- except for the occasional bug report -- the only time of the year when I get to interact with you here's a question: Could you give me (us) some update about the stuff that you apparently won't get around to finish for 2.4? Some of this stuff has been around for ages, but even though I try to follow the dev blogs I'm uncertain what the current status is.\r\n\r\nSpecifically:\r\n\r\n1) What happened to CoverBling (http://amarok.kde.org/wiki/CoverBling_overview)? Is there any plan to have any kind of visually nice way to browse ones albums in Amarok?\r\n\r\n2) What happened to the visualizations in Amarok? I think there was a related GSoC project back in 2009 that was supposed to add the required library support (http://api.kde.org/pykde-4.2-api/phonon/Phonon.Experimental.Visualization.html), but what happened with that afterwards?\r\n\r\nFurther (and this is more like a minor thing):\r\n\r\n3) Will there be any way to have lots of context plasmoids in the context part of the main window? Right now, the number is limited by the width of the taskbar-like construct at the bottom of the window. So given the screen size of my laptop, I can't reasonably have more than seven plasmoids even if I accept the fact that the names are shortened with (ugly) dots. Any plans to support *lots* of plasmoids in the UI?\r\n\r\nSo again, thanks for your work, thanks for possibly giving me (us) an update wrt. the stuff above. :)\r\n\r\n    Georg"
    author: "gwittenburg"
  - subject: "Some Answers"
    date: 2010-10-16
    body: "First of all, many thanks for your donation!\r\n\r\nAs for your questions, let me answer them in order:\r\n\r\n\r\n1) CoverBling was removed for two reasons:\r\n\r\nFirst of all, Apple has patented their \"CoverFlow\" technology. Lately Apple itself had trouble with this patent (a different company claimed it), but this doesn't change the fact that it's patented technology, and CoverBling could be accused of copying CoverFlow, which we cannot risk.\r\n\r\nAnd then, CoverBling simply had some technical issues that we could not resolve. On some systems it was very slow.\r\n\r\n\r\n2) Visualizations:\r\n\r\nThis is closely related to \"Spectrum Analyzer\", which you see on the list of planned features for 2.4. Basically, one is a form of the other. So we will very likely introduce visualizations in the 2.4 series. What kept us from doing it so far was missing features in Phonon, which have now been added by Martin Sandsmark.\r\n\r\n\r\n3) More Applets in the Context View:\r\n\r\nSure, we would like to do that somehow. We just need a good idea for implementing it with good usability. If you have one, please contact us, and then we can see :)\r\n\r\n\r\n\r\nCheers, \r\n\r\n<em>Mark Kretschmann (Amarok Developer)</em>\r\n"
    author: "Mark Kretschmann"
  - subject: "The Context Pane"
    date: 2010-10-16
    body: "To me, it seems like the Content Pane has the most room for improvement in the entire program. What I would love, personally, is more things to actually want to stick in it. Right now, file management is weak in the media sources list, since to do something like rate a song, you have to be playing it or right click, find the Statistics menu, and from there click on your rating.\r\n\r\nRight now, Media sources is only useful for loading the Playlist. The playlist is only useful for playing music, since adding these playlists to a device is excessively complicated. \r\n\r\nTo answer these problems, I propose these (possible) solutions.\r\n\r\nI would like to see the context menu gain an advanced file manager with ratings, tag visibility, and tabs for different devices, not showing them all at once, since the Media Sources does this already. This would make it easy to drag them to different devices in Media sources, which could do a similar overlay to the Context Pane (like when things are dragged over it). Or, the other way could work, and dragged files over the context pane could have the additional option \"Copy to Device\" in some fashion.\r\n\r\nIn my opinion, the Playlist menu is fabulous except it needs maybe just a button for \"Add to device.\" Ideal would be an easier option to open playlists, but that is secondary to my above concerns.\r\n\r\nThank you for your time!\r\n\r\nEdit: To answer the first comment as well, it seems easiest in my experience to have the Playlist and Media Sources panes tabbed, since it is easy to add things to the playlist by double clicking what you have highlighted. This would give lots of space for more applets. I use this when using Amarok half-screen on my 1080p monitor."
    author: "Luke.mcd"
  - subject: "The Context Pane"
    date: 2010-10-17
    body: "Agreed, Luke, the context pane to me is what makes Amarok special; head and shoulders above anything else. And I also agree that improvements need to continue. I do as you do and tab the Media Sources and Context panes, with the Context usually on top. And I make it nice and wide so I can read everything well. \r\n\r\nPlaylists can be used to burn to a CD with the use of a script. We do hope to have them transferable to devices soon also. \r\n\r\nWhat I propose to you is that you join the Amarok list, Amarok-devel list, or post on our Forum with your ideas. If you want to talk to the developers directly, come into IRC (Freenode, #amarok). Ideas are welcome! Publicity is welcome! And money too this month, of course.\r\n\r\nOh, and in the playlist you CAN rate tracks which aren't playing if you have the Verbose layout so the stars are visible, then s l o w l y double-click, then click the stars. It works, but you have to be slow. :-)\r\n\r\nAll the best,\r\n\r\nValorie"
    author: "valoriez"
  - subject: "Thanks for your answers! :)"
    date: 2010-10-17
    body: "Hi Mark,\r\n\r\nthanks for taking the time to answer my questions. I appreciate it.\r\n\r\n   Georg"
    author: "gwittenburg"
  - subject: "Context View \"Taskbar\""
    date: 2010-10-18
    body: "> 3) More Applets in the Context View:\r\n>\r\n> Sure, we would like to do that somehow. We just need a good idea for\r\n> implementing it with good usability. If you have one, please contact\r\n> us, and then we can see :)\r\n\r\nHow about having icons for the plasmoids instead of text?\r\n"
    author: "gwittenburg"
  - subject: "Hmmm..."
    date: 2010-10-18
    body: "The idea makes sense, but in reality it's very hard to find suitable icons that actually make sense for each applet.\r\n\r\nE.g., what could possibly symbolize \"Current Track\", or \"Similar Artists\"?\r\n\r\nThis comes with huge usability drawbacks, I'm afraid.\r\n"
    author: "Mark Kretschmann"
  - subject: "Hmmm, the Plasma Desktop plasmoid browser has icons..."
    date: 2010-10-18
    body: "Not meaning to be a pain (I'm aware that in the end, I don't write the code), but people managed to come up with icons for the plasmoid browser of the Plasma Desktop as well. And in Amarok itself, icons have been created for the basic file name assembly in the \"Move/Copy to Collection\" dialog and other nifty things.\r\n\r\nYour call... :)"
    author: "gwittenburg"
  - subject: "Amarok is my favorite app"
    date: 2010-10-26
    body: "Amarok is my favorite app ever, on any platform. The developers are wonderful human beings, and love to make their users happy. I support them 100%, and have made my contribution. \r\n<a href=\"http://www.downloadtorrents.org\">torrent</a>"
    author: "copyme"
  - subject: "PostgreSQL support?"
    date: 2010-11-02
    body: "I wish PostgreSQL support back :)"
    author: "xtian0009"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: solid grey 1px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/rocktober-wee.jpg" width="350" height="282"/><br />
Image by <a href="http://www.flickr.com/photos/pinksherbet/258911735/">D Sharon Pruitt</a>
</div>

October is here in all of its glory! Whether you are seeing the leaves turn or the sun coming back, it is time to celebrate Amarok. We are nearing the year’s end and are ready to sum up our efforts, while we continue to develop and Rok the World! 

In the last twelve months, we have made more than 4000 commits, closed over 4000 bugs, released 6 new versions of Amarok, written a Quick Start Guide to Amarok, attended over 10 conferences and had a big developer sprint in Switzerland. All of this, thanks to your donations! We have a big fundraising goal this year to keep us on the fast development track.
<!--break-->
<h2>A Year of Success</h2>

Some of the highlights this year:
<ul>
<li>New Automatic Playlist Generator</li>
<li>Improved cover fetching</li>
<li>New applets for the Context View (Upcoming Events, Similar Artists, Videos...)</li>
<li>Moodbar support</li>
<li>Better desktop integration (including KNotify support)</li>
<li>Better podcast handling</li>
<li>Queue support</li>
<li>Custom label support</li>
<li>A huge number of improvements to existing features!</li>
</ul>

<h2>Your Donations Help us Build the Future</h2>

For Amarok 2.4, due in early 2011, and upcoming releases we have planned:
<ul>
<li>Transcoding, so you can transfer files to your media device in the right format</li>
<li>UPnP support</li>
<li>Playdar integration</li>
<li>Spectrum analyzer</li>
<li>Completing the Amarok handbook</li>
<li>Many more exciting features and improvements!</li>
</ul>

You might have noticed that our development pace has quickened! In order to continue working on Amarok at such speed, we call on your help. Costs such as server maintenance and traveling expenses are too much for our tiny budgets to handle. Remember, everything we do, we do for free! And this is where you come in!

<h2>Roktober</h2>

This year’s Roktober, our annual fundraiser, has a goal of €5000. Reaching this goal will help us handle this next exciting year smoothly. The funds will help us develop more powerful features in Amarok, pay for our servers and send team members to conferences. We may even run a developer sprint or two!

As a sign of our appreciation for your support, donors can opt in to being added to a special section in Amarok’s About Dialog. Your name will then appear in each version of Amarok that gets released in the next 12 months.

So join us, and throw in your share to Rok the World! 

Please go to <a href="http://amarok.kde.org">Amarok's website</a>. In the left sidebar, there are two donate buttons. The upper one is for donations via Paypal and the lower one is for donations via Google Checkout. Choose your preferred one and donate to support Amarok development. Thank you!

If you wish to be listed in Amarok’s About Dialog as a supporter, please send an email to roktober @ getamarok.com after you donated with your name as it should appear there (and if you have one, your nickname on <a href="http://www.opendesktop.org">OpenDesktop</a>). If you are sending this from a different email address than your donation, please mention that as well. Your name will not appear unless you give permission. Should you have any problems or questions please email roktober @ getamarok.com.