---
title: "KOffice 2010 Summer Sprint Report"
date:    2010-06-25
authors:
  - "Boudewijn Rempt"
slug:    koffice-2010-summer-sprint-report
comments:
  - subject: "Great weekend, great team!"
    date: 2010-06-25
    body: "Good work guys.\r\n\r\np.s. are the picture labels of the second and theird picturs swapped by accident? ;)\r\n"
    author: "sebsauer"
  - subject: "How can we reach out and gain a real userbase "
    date: 2010-06-26
    body: ">How can we reach out and gain a real userbase\r\n\r\nPlease, first make the fonts look good in Koffice 2.2.x. It is evident as a proof kspread 2.2 does show nice font while typing (nice font rendering = bug), but it becomes blurry on input!\r\n\r\n<a target='_blank' title='ImageShack - Image And Video Hosting' href='http://img33.imageshack.us/i/kspreadfontsugly.png/'><img src='http://img33.imageshack.us/img33/1972/kspreadfontsugly.png' border='0'/></a>\r\n\r\nA nice way to Reach to new users:\r\n\r\n1. fix font rendering\r\n2. fix the \"welcome dialog\" which is another ugly thing in koffice\r\n\r\n<a target='_blank' title='ImageShack - Image And Video Hosting' href='http://img638.imageshack.us/i/welcomedialog.png/'><img src='http://img638.imageshack.us/img638/5183/welcomedialog.png' border='0'/></a>\r\n\r\n\r\nI as a user concerned for koffice's development (bug reporting, enhancement wishlist) have reported bug for this blurriness, and even with many votes, it is a year old bug...\r\n\r\nFor what I see as a user, Koffice is lagging because of this font rendering and the welcome dialog...\r\n\r\nBug I reported in March 2009:\r\n<a href=\"http://bugs.kde.org/186513\">fonts look blurry in kword 2.0 beta 7at 100%</a> \r\n\r\nOther users bothered by bad font rendering:\r\n<a href=\"http://forum.kde.org/viewtopic.php?f=96&t=51044\">Is it me or is KWord's font rendering really awful?</a>\r\n\r\nand nokia bug report is lying there, QT guys please have a look:\r\n<a href=\"http://bugreports.qt.nokia.com/browse/QTBUG-7177\">Zoomed text uses hinting from unzoomed text and looks ugly</a>\r\n\r\n\r\nPlease try to fix the font bug in QT 4.x. thanks."
    author: "fast_rizwaan"
  - subject: "In Progress"
    date: 2010-06-26
    body: "1. The font problem was discussed at the meeting, for more information see the meeting minutes. The problem is under investigation, but no solution has been found yet. We are aware that this serious problem.\r\n\r\n2. Depends on what you want to get fixed. The dialog will basically stay the same, though some details have been improved like new icons in Krita. The dialog isn't really fancy, but it generally works well and there are bigger problems to solve than replacing the opening dialog. "
    author: "slangkamp"
  - subject: "The opening dialog"
    date: 2010-06-26
    body: "I think the reporter didn't want to replace the dialog, but fix the bugs that are really obvious when you open it.  And I think we will be able to do that before 2.3."
    author: "ingwa"
  - subject: "opening dialog"
    date: 2010-06-26
    body: "Opening dialog needs interface cleanup, nice icons.. that'll do\r\n\r\n1. use of proper names/labels (like \"Create New document\" headline, then those options of general, business, etc. should be there\r\n2. new artwork, instead of 'black & white' and 'blurry scaled' icons, proper icons representing ods etc. should be there. \r\n\r\n\r\ninstead of sidebar on left using *Tabs* would give uncluttered and better appearance to the dialog\r\n\r\n\r\n------------------------------Open Dialog----------------------------\r\n[*Create Spreadsheet*] |   [Open Spreadsheet] |  [Recent Spreadsheet]\r\n--------------------------------------|---------------------------------------------\r\n\r\n1. General Document -> Blank Workbook (icons/artwork here)\r\n\r\n2. Business -> [list of available options]\r\n\r\n3. Home & Family -> [list of available options]\r\n\r\n\r\n\r\n[Create] (\"use this Template\" is too technical) [Cancel]\r\n--------------------------------------------------------------------------------------\r\n\r\n\r\nthanks."
    author: "fast_rizwaan"
  - subject: "Opening dialog and templates?"
    date: 2010-06-27
    body: "I agree with the above that the opening dialog is badly layouted and not very welcoming.\r\nWhat I also would like to see in KOffice are shared templates. I use KOffice docs for specific tasks as I am not an office worker. I need to print a kid birthday invitation, to print labels, to write a formal letter to an administration, to prepare a party (spreadsheet with invited people, answers, planned costs,...) ... and I would like to get ready made templates by other users and to share my own templates as well. Did you discuss the use of KNewStuff with kde-files.org?"
    author: "annnma"
  - subject: "Tabs"
    date: 2010-06-27
    body: "What's funny is that MS Office and iWork now use the same layout for side bar instead of tabs.\r\n\r\nI'm not convinced that tabs would reduce \"clutter\" as the sidebar separates everything nicely into groups, which isn't the case for tabs."
    author: "slangkamp"
  - subject: "KWord"
    date: 2010-06-28
    body: "Well in KWord I have found no way of changing the style of a text to \"Head 1\" or whatever and that is the first thing that disqualifies KWord for me, did not bother to look at the rest of it since it would be pointless.\r\n\r\nBtw. I tried some minutes. So either the initial layout I was confronted with is pretty bad or you should really make things like that more discoverable.\r\n\r\nAlso there is no option I find usefull in the side-panell. I want to write texts first so I do not care about transparency or  colour gradients.\r\n\r\nIn that regard you should imo focus more on the single applications. Flake is nice but it should imo be considered as \"addon\" for most cases --> so move it into a menu, like an add menu.\r\n\r\nBasline is in case of KWord: Please focus on what a word processor is for."
    author: "mat69"
  - subject: "Usability of the styles docker"
    date: 2010-06-28
    body: "Right, that's it.  We have to do something about that styles docker.  We will get a lot more complaints about this if we don't redesign it.\r\n\r\nHere's how it works right now to do what you want:\r\n\r\n1. When the cursor is in a paragraph that you want to change to \"Head 1\", go to the docker labeled \"Tool Options\".  Select the \"Styles\" tab.\r\n\r\n2. Click on the style that you want (\"Head 1\" in your case).\r\n\r\n3. Click on the button in the lower left corner with a check mark on it.  That will apply the chosen style.\r\n\r\nAnd yes, I agree that it's cumbersome and should be changed."
    author: "ingwa"
  - subject: "Oh, I just realised the"
    date: 2010-06-28
    body: "Oh, I just realised the needed docker was not present in my case and adding them is also not that optimal --> right clicking on the name of a docker or the seperator == small click space.\r\nI searched for dockers in the View menu, not the settings one. Though that is in fact debatable.\r\n\r\nImo the apply button here is really uneeded, a double click should be enough."
    author: "mat69"
  - subject: "How can we reach out and gain a real userbase?"
    date: 2010-07-02
    body: "> How can we reach out and gain a real userbase?\r\nMake a Windows installer."
    author: "axeloi"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/first.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/first_small.jpg" width="400" height="225"/></a><br />Hacking spot</div>Hot on the heels of the <a href="http://www.koffice.org/news/koffice-2-2-released/">2.2 release of KOffice</a> -- the first release we feel that users can give a try and <a href="http://www.asinen.org/2010/06/koffice-2-2-is-it-ready-yet/">use for real work</a> -- the KOffice developers met in Essen-Horst in Germany, in the wonderful <a href="http://www.linuxhotel.de/">Linux Hotel</a>. Thanks to sponsorship by the KDE e.V. and the hard work by Alexandra Leisse and Inge Wallin, we could spend three days discussing and hacking.

With the weather being perfect, the hotel hospitable and the surroundings leafy and all a-twitter we not only hacked and discussed. There was time to relax over a beer, get to know each other a little better and stroke the hotel cat.

Friday was mostly spent hacking, reviewing each others' code and sitting outside and discussing the state of KOffice until very late in the night. The next morning, Saturday, saw the start of a very long and rather gruesome meeting. No hacking, only a few coffee breaks and one pizza break. This day was devoted to three main topics:

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/second.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/second_small.jpg" width="400" height="225"/></a><br />Dangerous animals!</div>

<ul>
<li>How can we reach out and gain a real userbase
<li>How can we gain more developers (keep in mind that many applications of KOffice have only one, or even no, regular developer working on them!)
<li>How can we keep our work on KOffice relevant in what promises to become interesting times
</ul>

The meeting notes are on the <a href="http://wiki.koffice.org/index.php?title=Meetings/Mid_2010_meeting/Minutes">developer wiki</a>.

Nokia sponsored not just the Saturday dinner but also the Sunday lunch, which was pizza again, for which many thanks from everyone present!

Saturday night again was most spent hacking, comparing notes, benchmarking ODF loading and similar things and drinking Slovakian peach liquor.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/third.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/third_small.png" width="400" height="298"/></a><br />Group photo</div>Sunday was a lot more technical than Saturday: we discussed text rendering, which is still a sore point, a new architecture for file filters, text layout problems, charting issues and separating the user interface from the core even more. This is something that Nokia has quite some experience with, having developed a completely new user interface on top of the KOffice applications, and we're keen to pursue this path even further.

All in all, the attendants agreed this was a really positive sprint in a relaxing atmosphere. Many decisions have been made and a lot of work got done!