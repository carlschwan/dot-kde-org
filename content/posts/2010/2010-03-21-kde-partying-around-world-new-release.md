---
title: "KDE Partying Around the World for New Release"
date:    2010-03-21
authors:
  - "jospoortvliet"
slug:    kde-partying-around-world-new-release
---
On February 9th 2010 the KDE community released the a new major version of the KDE Software Compilation to the world. As this provided an excellent excuse for throwing a good party, the last 7 weeks have seen hundreds of KDE enthusiasts gather at over thirty release parties around the world. Most parties featured demos and talks about the new release and the majority included beer, other drinks, food (including KDE cake!), some had karaoke and all of them were about meeting cool people and having some fun.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/Kake.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Kake_small.jpg" width="400" height="300"/></a><br />Cool KDE party Kake in Iran!
Pic by <a href="http://community.ir/~emilsedgh/blog/?q=node/6">Emil Sedgh</a></div>

<b>How it all began...</b>

The release party tradition was really kicked off by the 4.0 release event in San Francisco, and was continued last year with 18 parties around the world. But surely never have we had so many well attended release parties as for this release - something we're proud of! The parties varied wildly in form and location. The chilly North of the US had <a href="http://neomantra.org/?p=198">a party in Washington DC</a>, where visitors had to fight a snowstorm to reach the party location. Canada apparently didn't have to suffer much snow (we noticed during the winter games...) but you can be pretty confident that the weather at the <a href="http://liveblue.wordpress.com/2010/02/11/kde-sc-4-4-release-party-salvador-br/">Salvadorian release party in Brazil</a> was superior to all other parties. The 30 attendants there enjoyed beaches and sun, talks, beer and snacks. It wasn't the only party in Brazil - there were four more in Alegrete, Belo Horizonte, Florianopólis and Teresina.

<b>And now we party...</b>

The release party at Prishtina in Kosovo, organized by Milot, had visitors bringing home made pie and pancakes! Clearly the KDE community shares an interest besides code - good food and cooking. The <a href="http://community.ir/~emilsedgh/blog/?q=node/6">release party in Tehran</a>, Iran, also had KDE releated food - a KDE Cake or, shorter, <em>Kake</em>. The visitors also tried out a KDE Domino but apparently it didn't work out.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/sharing_with_nature.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/sharing_with_nature_small.jpg" width="400" height="267"/></a><br />The Argentinians shared beer with this relative of Konqi.
Pic by <a href="http://atpic.com/nl/4364">Ariel Kanterewicz</a></div>

<b>Everywhere!</b>

The Barcelona party started with dinner at 22:30 - late even by Spanish standards - and after dinner a bar was found. More release parties were held in public places, like the FOSS-friendly Dr Mason bar in Buenos Aires and a “Frango Frito” (Fried Chicken) place in Minas Gerais. Stuttgart, Germany, saw a joint party with the FSFE in a bar - and in good German tradition, many beermugs were emptied. Other spots included the KDE friendly offices of <a href="http://www.kovoks.nl/">Kovoks</a> in the Netherlands or universities like Universidade Federal do Pampa and the university of Akron in Brazil. In Indonesia the release party organized by Andi Sugandi was combined with the <a href="http://unikomlinuxweek.org/">Unikom Linux Week</a> - their release parties are becoming a tradition already (<a href="http://picasaweb.google.com/andisugandi/ReleaseParty#5298055120031318562">pics from last year</a>). In Argentina the party lasted over eight hours. The team first met in a local place for KDE and KMyMoney talks. Then, maybe prompted by the topic of the talk, they decided the beer was too expensive and one of the visitors offered his house to continue eating and drinking. Off they went by bus, and the party continued with pizza and beer until 4 in the morning.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/tokamak4.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/tokamak4_small.jpg" width="400" height="300"/></a><br />Mastering Karaoke at Tokamak</a></div>

<b>Tokamak</b>

KDE's Plasma Team <a href="http://blog.uninstall.it/2010/03/01/more-tokamak-photos/tokamak4_27/">took the opportunity</a> of their Tokamak 4 get-together to also celebrate the release of KDE SC 4.4. The team went out for dinner and then headed straight into a Karaoke bar, where various performers brought real culture to this part of Frankonia. One highlight was Chani performing the German 80s anthem "99 Luftballons" and making everybody in the audience be in awe of her mad language skills. Previously, Sebas had taken the stage to present a KDE version of The Man in Black (Johnny Cash) performing Ring of Fire. After a good couple of hours of Karaoke, the team decided to move on and conquer a 70s-style bar in the heart of Nuremberg to lift the glasses on a great KDE SC 4.4 and a successful Tokamak 4.

<b>Conclusion</b>

All in all, lots of fun for the visitors of our release parties, and as Milot Shala told us: "I am looking forward to organize more KDE release parties". Well, so are we, and luckily, every 6 months there's a new excuse for a party... According to Ryan Rix, who organized the <a href="http://hackersramblings.wordpress.com/2010/02/22/kde-sc-4-4-release-party-the-aftermath/">Phoenix (USA) release party</a>, this is just about right: "I think I need at least 6 months to recover ;)".