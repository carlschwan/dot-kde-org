---
title: "Spreading KDE at the Southern California Linux Expo"
date:    2010-04-02
authors:
  - "ajohnson"
slug:    spreading-kde-southern-california-linux-expo
---


<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/DSC00063-full.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/DSC00063.JPG" /></a><br />KDE volunteers at SCALE8X</div>From February 20th to the 21st, Linux enthusiasts from the greater Los Angeles area converged at the Westin Hotel near Los Angeles Airport to celebrate Linux and Free Software at the annual <a href="http://www.socallinuxexpo.org/scale8x/">Southern California Linux Expo</a> (SCALE8X). KDE was there once again showing attendees the work of the KDE community.

The team was showing off Plasma Desktop and a prototype of the Plasma Netbook workspace. There were four new volunteers who did a great job representing KDE for the first time - Jonathan Prien, Wayne Speir, Aaron Reichman, and Barrington Daltrey. A special mention goes also to Gary Greene, who shipped out the KDE booth box for use at SCALE. Aaron Johnson gave a talk "A Basic Introduction to KDE", attended by about thirty people, that was well recieved.

All in all, the team had a great time and made a great start for a KDE community in Southern California.