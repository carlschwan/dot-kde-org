---
title: "Camp KDE About to Start!"
date:    2010-01-16
authors:
  - "jospoortvliet"
slug:    camp-kde-about-start
---
Tomorrow morning, the third major North American KDE event will start with an introduction by Jeff Mitchell and Aaron Johnson. At 11:00 a keynote by University of California San Diego professor Philip Bourne will introduce us to the university's Open Data initiative and after a lunch we'll get going with the various presentations.

The first day of Camp KDE will feature more generic presentations about KDE deployments, design and online collaboration. The second day will consist of a few more technical talks but also features a variety of more strategic talks like the keynote by Frank Karlitschek about KDE and the Cloud (TM). Starting on Monday we'll dive into details of Plasma development, Akonadi and a variety of other KDE technologies.

The campus is a very nice venue and the open door policy might bring in a few more interested students. We've got a large room with place for about 100 people, two large screens, good wireless and power and a bunch of tables for the snacks.

We're looking forward to it, hopefully so do you!