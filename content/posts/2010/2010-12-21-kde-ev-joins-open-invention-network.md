---
title: "KDE e.V. Joins Open Invention Network"
date:    2010-12-21
authors:
  - "aseigo"
slug:    kde-ev-joins-open-invention-network
comments:
  - subject: "In practice:"
    date: 2010-12-21
    body: "(This is of course just a personal view)\r\n\r\nEssentially, this means a couple of things:\r\n- KDE e.V. promises not to wage patent war against Linux\r\n- KDE gets royalty free access to the OIN patents\r\n\r\nSo not a lot has changed really: OIN were never going to sue KDE even if any of our software reads on to their patents and KDE e.V. was obviously not going to attack Linux with patents (apart from anything else, we do not have any).\r\n\r\nThis just formalises that a bit and, hypothetically, if a company outside OIN was to try and attack Linux relevant KDE software with patents they would have to think about whether OIN might have some patents that could hurt them too - the whole point really being to neutralise the effect of patents around Linux."
    author: "Stuart Jarvis"
  - subject: "OIN"
    date: 2010-12-21
    body: "Meanwhile, its fellow licensees keep promoting and using software patents.\r\n\r\nHi Oracle!\r\n\r\n:("
    author: "apol"
  - subject: "Actually, it gives us more."
    date: 2010-12-21
    body: "Actually, it gives us more. It gives us a license (and protection) to the 800 patents that Novell is selling to CPTN. And or to any other pattents that the OIN partners might sell in the future. Without being part of OIN we would not get that kind of protection."
    author: "cyrille"
  - subject: "not perfect, indeed"
    date: 2010-12-21
    body: "yes, it's not a perfect situation but as cyrille's comment above notes, it gives KDE additional protection from the madness.\r\n\r\nOIN is not the answer, it's just part of a strategy of safe haven for us while the real answers as to how to improve/change the patent system so it is not detrimental to innovation are being worked out by those with the necessary expertise. that will take years and the outcome is yet uncertain, so we are wise to take precautions that do not compromise our ethics but increase our safety in the meantime."
    author: "aseigo"
---
KDE e.V. is <a href="http://www.openinventionnetwork.com/press_release12_21_10.php">pleased to announce</a> that we have joined the Open Invention Network community as a <a href="http://www.openinventionnetwork.com/licensees.php">licensee</a>.

Open Invention Network was founded as a way to help defend the Linux ecosystem, and by extension much of the Free and Open Source software world, from the risks associated with software patents. Patents owned by Open Invention Network are available royalty-free to any company, institution or individual that agrees not to assert its patents against the Linux System, creating an umbrella of protection for its members.
<!--break-->
“We view an OIN license as one of the key methods through which open source innovators can deter patent aggression,” said Adriaan de Groot, vice president of KDE.  “We are committed to freedom of action in Linux, and in taking a license we help to address the threat from companies that support proprietary platforms to the exclusion of open source initiatives, and whose behaviors reflect a disdain for inventiveness and collaboration.”

“Given its leadership in creating a user-friendly computing experience, including its advanced graphical desktop for the Linux community, we are pleased to have KDE become a licensee,” said Keith Bergelt, CEO of Open Invention Network. “By doing so, KDE affirms its continued support for open source. We applaud their foresight in taking this step to support both itself and the open source community broadly.” 

While KDE maintains our position against software patents, this development provides an additional safety net for KDE when it comes to the risks Free Software is facing from such patents today.