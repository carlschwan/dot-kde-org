---
title: "Using KDE software labels, An interview with the developer of  Brewtarget"
date:    2010-04-10
authors:
  - "Justin Kirby"
slug:    using-kde-software-labels-interview-developer-brewtarget
comments:
  - subject: "two things"
    date: 2010-04-09
    body: "I'd like to point out that our dear Justin \"Neomantra\" Kirby brews beer, and considering the fact he drinks it himself and still lives I'm guessing he does a decent job at it ;-)"
    author: "jospoortvliet"
  - subject: "phonon on windows"
    date: 2010-04-10
    body: "also i'd like to note that phonon does work on windows---there is a phonon-directshow backend.\r\n\r\ngreat article!"
    author: "lfranchi"
  - subject: "QT typo"
    date: 2010-04-10
    body: "Just wanted to point out that places in the article where Qt is referenced by Justin Kirby, it is referred to as \"QT,\" which AFAIK is incorrect unless one is referring to Apple's QuickTime.\r\n\r\nNice article, though. Sad that it's getting harder to find people in the States that like beer with actual taste to it. I actually had to argue with someone to talk them out of buying a 30-pack of PBR for the evening."
    author: "maniacmusician"
  - subject: "QT vs Qt"
    date: 2010-04-10
    body: "Fixed ;-)"
    author: "jospoortvliet"
  - subject: "Hey, that's me"
    date: 2010-04-10
    body: "Hey, I'm the \"Philip\" up there! Just wanted to say thanks to Justin for writing the article. Brew on."
    author: "rocketman768"
  - subject: "Haha, it was lacking a whole"
    date: 2010-06-10
    body: "Haha, it was lacking a whole lot features and it hasn't been updated in years. I started formulating my recipes by hand doing some linear algebra and stuff for <a href=\"http://eveningjobs4u.com/\" style=\"color:#333333; text-decoration:none\">evening jobs</a> a couple of years. While I was at home over Christmas break in 2008, to keep me busy I decided to write the backend for what would become Brewtarget. I found a spec document online for BeerXML, which is a standard for all the ingredients and whatnot."
    author: "davidzh"
---
In early March <a href="http://dot.kde.org/2010/03/11/come-out-part-kde">Stuart Jarvis</a> wrote an article published here on the Dot which announced the winners of the <a href="http://kde-apps.org/poll/index.php?poll=251">poll results</a> for suitable KDE software labels. Since then work has begun on <a href="http://community.kde.org/Promo/Branding/Software_Label">coming up with suitable logos</a> for these labels. This work is still underway and in need of volunteers if you have time and artistic skills.

More recently I stumbled on to some real world examples of developers whom this might affect and thought it would be interesting to interview them and get their thoughts on these new labels and find out whether they might incorporate them into their project's identity. What follows is a short Q&A with Philip Lee, the developer of an open source beer recipe tool called Brewtarget. Philip is currently studying at Northwestern University for a PHD in Electrical Engineering and does software development in his spare time.  His project, Brewtarget, allows you to specify all of the ingredients you wish to use as well as the processes you are going to follow while brewing your batch of beer. It can then provide a host of useful calculations and information based on the recipe you enter and allows you to save them for future reference. It can even import recipes from closed source beer recipe software such as BeerSmith thanks to its use of the BeerXML standard. Brewtarget utilizes KDE technologies such as Qt and Phonon and is available for Linux, Mac, and Windows. The application can be downloaded at <a href="http://sourceforge.net/projects/brewtarget/">this page</a>.


<b>Justin</b>: First, can you tell me a little about how you got started on the idea of writing this application?  And what caused you to choose Qt and Phonon as critical components over other options?

<b>Philip</b>: Well, I was using QBrew, which used Qt when I first got into brewing a few years ago. However, it was lacking a whole lot features and it hasn't been updated in years. I started formulating my recipes by hand doing some linear algebra and stuff for a couple of years. While I was at home over Christmas break in 2008, to keep me busy I decided to write the backend for what would become Brewtarget. I found a spec document online for BeerXML, which is a standard for all the ingredients and whatnot. Since the well-known non-free windows software Beersmith uses BeerXML, I figured this would be a good idea since people could easily transfer all their recipes to my software.

I finished the back end in a week or so. Then, I started looking for GUI libraries. I tried a lot of stuff: FLK, GTK, Qt, etc. I frankly found Qt to be the best-documented and easiest to program with. I got a prototype up within a day or two, and just kept learning Qt as I developed. It also helped immensely that Qt contained XML parsing stuff. As for Phonon, it's a fairly recent addition. I wanted some timers in the program to be able to play sounds, so I started searching around the documentation and found Phonon could do it easily. However, I think that it's currently impossible to use Phonon in Windows. The windows version currently doesn't use Phonon since it can't be built in Windows. I'm not really sure how to fix this problem. I guess I might have to figure out DirectSound or something... don't really know.

<b>Justin</b>: Do you have any immediate plans for the future of Brewtarget's development as far as new features go?

<b>Philip</b>: I'm developing some better mashing tools that should give more complete control over the process. Also, I am going to separate the concept of "recipe" from "batch" so that multiple batches can be attached to a recipe. Being able to put the actual numbers you achieved in the batch will then enable the software to make suggestions to you based on your past performance of what you might be doing wrong and how to tweak the numbers in your particular equipment set up to better match the calculated values to the actual ones. Also, I plan to incorporate water chemistry calculations at some point.

<b>Justin</b>: I often find that all the recipe sharing that goes on in the home brewing community brings it very close to the same environment as open source software. Do you think commercial breweries stand to gain (or lose) anything from choosing to keep a recipe secret versus sharing it with the home brewing community?

<b>Philip</b>: I think most breweries wouldn't lose anything from open sourcing a recipe. First of all, there are plenty of good clones for many of the popular beers. Really, any person with a good palate and knowledge of techniques can "reverse engineer" most any recipe, just like restaurant food. Second, somewhere in "Brew Like a Monk," one Trappist said something to the effect that you can easily harvest their yeast from the bottle and there is nothing hidden about their recipe, but they are still thriving from beer sales because of the quality associated with them.

<b>Justin<b>: Considering the new labels we're working on for KDE applications which would you say fits your project best and why?

<b>Philip</b>: I suppose "Built on the KDE Platform" is most appropriate since I am using its Qt and Phonon technology. However, I've made a couple of friends working for KDE, and if the project attracts enough attention, perhaps it might be more closely coupled to KDE in the future.

<b>Justin</b>: How likely are you to make use of these logos when they're finalized?

<b>Philip</b>: Pretty likely. I'd like to give credit where it is due. I'd probably put it in the startup splash screen and the "About" dialog.

<b>Justin</b>: Would you say the explanation of the labels provided in our Dot article announcement were helpful in determining which one fits your project best?

<b>Philip</b>: The description seems to indicate that the uses overlap. One emphasizes KDE technology, one emphasizes the KDE community, and one emphasizes both. The descriptions are fairly clear, but it is a bit confusing at first since the scope of the labels overlap.

<b>Justin<b/>: Does having the ability to use such a label cause you to feel any more or less a part of KDE community versus being a stand alone project?

<b>Philip</b>: For me, using or not using a label wouldn't make me personally feel more or less a part of KDE. However, it would make me feel more like my app belongs with other KDE apps and motivate me to interact with the community.

Thanks to for the interview, Philip, and keep on brewin'!
