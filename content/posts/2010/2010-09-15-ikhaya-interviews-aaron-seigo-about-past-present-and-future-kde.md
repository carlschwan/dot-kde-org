---
title: "Ikhaya interviews Aaron Seigo about the Past, Present and Future of KDE"
date:    2010-09-15
authors:
  - "serenity"
slug:    ikhaya-interviews-aaron-seigo-about-past-present-and-future-kde
comments:
  - subject: "Nice"
    date: 2010-09-15
    body: "Those were great interviews, giving a very useful perspective on the whole KDE enterprise. Keep up the wonderful work!"
    author: "tangent"
---
<a href="http://ikhaya.ubuntuusers.de/">Ikhaya</a>, the news portal of <a href="http://ubuntuusers.de/">ubuntuusers.de</a>, has a two part interview with Aaron Seigo, lead Plasma developer and designer (<a href="http://ikhaya.ubuntuusers.de/2010/09/09/interview-with-aaron-seigo-part-1/">part one</a> and <a href="http://ikhaya.ubuntuusers.de/2010/09/10/interview-with-aaron-seigo-part-2/">part two</a>).

Aaron tells how he first got involved with KDE after seeing KDE 2.0's compelling design and getting his first patch accepted. He also reports the progress of KDE software spreading to mobile devices.  In addition, the interview includes details about the challenges and successes of moving from KDE 3 to the new KDE Platform, Applications and Plasma Workspaces.
<!--break-->
