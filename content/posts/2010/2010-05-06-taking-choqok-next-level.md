---
title: "Taking Choqok to the Next Level"
date:    2010-05-06
authors:
  - "danborne"
slug:    taking-choqok-next-level
comments:
  - subject: "How much is a better name?"
    date: 2010-05-07
    body: "More like Twitteriffic, Tweetie, Tweetdeck and less like Show Cock would be nice. I know there is a tradition with crappy K+(My)+(a..z)+K-like names. But I really think that has to stop. Plasma for example is a really great name. More of that please.\r\n\r\nFor actual feature I would like to see standards supported, like pubsubhubuphubpupsup or whatever it is called. So that Buzz etc are supported."
    author: "kragil"
  - subject: "No Tweetdecks please"
    date: 2010-05-08
    body: "Most KDE developers and users are not Americans, and some of the application names reflect that -- and I think it that's great. Enforcing English names would be a form of cultural imperialism. I think Choqok is a fine name and doesn't sound like 'show cock'. It doesn't even have a capital 'K' in it (which is good)."
    author: "Tuukka"
  - subject: "It used to have a capital K"
    date: 2010-05-10
    body: "It used to have a capital K until people complained :-)"
    author: "Haakon"
  - subject: "Hmm"
    date: 2010-05-10
    body: "The author of \"show cock\" also wrote \"mmm dick\"?\r\nThese should be chat roulette clients."
    author: "mpx"
  - subject: "Twitter Lists"
    date: 2010-05-17
    body: "Is there support for twitter lists planned / in development already?\r\n\r\nThanks for choqok,\r\nmuesli"
    author: "muesli"
  - subject: "There's a plan"
    date: 2010-06-08
    body: "http://choqok.gnufolks.org/forum/topic/14"
    author: "Mehrdad"
---

Anyone who has taken a class in economics is familiar with the difference between the direct cost of something and its 'opportunity cost'. Developing free software is a great example of this difference. A KDE developer does not have to spend hundreds of dollars on an SDK, he only needs to give up some of his time to develop.

The time given up could have been used working to earn money or doing another leisure activity; however, many choose to develop anyway. Other KDE developers have asked for sponsorship (<a href="http://www.kdenews.org/2009/12/02/krita-team-seeking-sponsorship-take-krita-next-level">the Krita team, for example</a>) to allow them to put in more serious time. Now Mehrdad Momeny (mtux on IRC) has gone a step further by linking the prioritization of particular features in <a href="http://choqok.gnufolks.org/">microblogging client Choqok</a> with donations, perhaps a way of feeding developers. We were interested in his idea, so we had a nice IRC discussion with him a while ago to learn more.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/choqok.png" width="299" height="461" /><br />Choqok: KDE microblogging</div><b>Who are you?</b>
I am Mehrdad Momeny, a Persian Free Software enthusiast and the developer of Choqok, currently serving conscription. I am 24 years old and live in Mashhad, Iran, east of Tehran. I'm also one of the developers of <a href="http://blogilo.gnufolks.org/">Blogilo</a> (KDE Blog client) and <a href="http://mdic.gnufolks.org/">MDIC</a> (a simply dictionary application).

<b>Can you tell us a bit about the history of Choqok?</b>
About a year ago - two weeks before the first release of Choqok - I felt that no Twitter client filled my needs. So I started to write a tiny one for myself. After I finished the first version, I put it up on KDE-Apps and there was some interest in it; there was no KDE client other than the KDE Microblogging Plasma Widget. The next version was added to the Kubuntu repositories by Harald Sitter. In short, Choqok filled a gap for not only me, but it seems to have filled a gap for other KDE users too!

<b>What do you have in mind for the future?</b>
What the users want! Choqok has been the answer to the needs of many users, but there are many features that can be added if users want those features to be developed. I started developing Choqok because I wanted a micro-blogging client with notifications and a 'Quick Post' feature. In the future I would like to add anything the users would like. Like many developers, I can only write code in my free time. As I am in the military that can be particularly problematic; I don't always get leisure time to develop Choqok, but I don't want to leave it unmaintained or stop its development.

<b>So how would you like us to help you develop the features we would like?</b>
I understand some users might like to have new features and ideas integrated sooner than would otherwise happen and I could make that possible if they were willing to support me in developing. I've launched <a href="http://choqok.gnufolks.org/forum/">a forum</a> where users can post ideas of features they would like implemented. They can discuss the ideas there and we can figure out how much of a donation it would take for them to be developed. Once we have an idea and a donation I can begin and quickly finish development of the feature.

<b>Thanks and good luck with this initiative, Chokoq development and your other projects!</b>

Free Software has great ambitions and bringing them into reality takes considerable resources. It is good to see developers looking for new ways of allowing them to contribute more of their time to FOSS!