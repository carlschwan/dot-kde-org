---
title: "Website for Akademy 2010 is Online, Time to Register!"
date:    2010-04-08
authors:
  - "jospoortvliet"
slug:    website-akademy-2010-online-time-register
---
Starting July 3rd 2010, hundreds of KDE community members, employees of companies working with us and many other Free Software enthusiasts will gather at Tampere, Finland. There, at the University of Tampere, the annual Akademy summit 2010 will take place. For a full week, Tampere will be the place where stunning new technology is demonstrated, hundreds of prominent Free Software contributors walk the corridors and new plans for the future of the Free Desktop emerge.

Today, the first step of this journey can be made. After months of work, the <a href="http://akademy.kde.org">Akademy 2010</a> website is live, and you can begin to register yourself, book your flight and hotel, and start preparing talks!

Again we would like to urge you to have a look at the <a href="http://akademy.kde.org/papers">Call for Papers</a>. If you have developed an interesting piece of technology, have ideas on where the community should venture next or want to present the state of your project, send in a proposal!

Room has also been made for lightning talks, 7-minute short bursts of creativity useful to quickly present a great idea, put forward a proposal or show off a new piece of technology. If you want a spot for the lightning talks, send a quick summary to the talks team, details at the <a href="http://akademy.kde.org/papers">Call for Papers</a> page.

We would like to explicitly extend this invitation to attend the Akademy summit to everyone involved in the KDE community. Translators, documentation writers and coders. Web developers, bug triagers and artists. We also invite companies building upon our great technology, or looking to take advantage of it. Yes, anyone interested in getting involved in this great community can and should join us on July the third in Tampere.

See you all in Finland!