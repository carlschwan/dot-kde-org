---
title: "Krusader Team Celebrates 10th Birthday and Seeks New Contributors"
date:    2010-05-05
authors:
  - "mateju"
slug:    krusader-team-celebrates-10th-birthday-and-seeks-new-contributors
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://www.krusader.org/screenshotsfs.php?limit=3&next=2"><img src="http://dot.kde.org/sites/dot.kde.org/files/kr_browsearchive.png" width="400" height="300"/></a><br />Ye Olde Krusader.</div>Ten years ago a simple twin panel file manager was released. It had a few small glitches like showing rrr instead of rwx for permissions, had some compatibility issues with Debian and Solaris, did not save keyboard settings, but it was, in spite of many bugs, sort of usable for everyday work. Ten years ago Krusader started on the path to becoming a top file manager for a large range of operating systems and users.

It started with young developers Shie and Rafi, over some falafel. In spite of academic duties they kick started an application which became a very popular <a href="http://en.wikipedia.org/wiki/Total_Commander">Total Commander</a> like file manager for the Free Desktop. On January 1st 2002, version 1.0 was released by the Krusader Krew.

Krusader pioneered many innovations for file management, like tabbed file browsing, advanced bookmarking functionality and a highly customizable interface. The <a href="http://www.krusader.org/">Krusader website</a> has the full story written by Matej Urbančič.

Today version the first beta for 2.2.0 is released, codenamed 'DeKade'. It features better trash integration, many new tabs actions, queued packing and unpacking and much more. You can find it on the <a href="http://www.krusader.org/downloads.php">Krusader Downloads page</a>.

Frank Schoolmeesters, long time Krusader supporter and Documentation Master, uses this memorable day to call out to Free Software enthousiasts:

<i>"We are still <a href="http://www.krusader.org/phpBB/viewtopic.php?t=2337">calling for new developers</a>.</i>

<i>"Currently we have only one (new) active developer, all the other developers are basically 'retired',  and one developer is not enough to do stable releases, so currently we only do beta releases."</i>

You can contact him and the Krusader team at <a href="http://www.krusader.org/developers.php">the Krusader website</a>, and they'll help you getting involved! The Krusader code can be found in the <a href="http://websvn.kde.org/trunk/extragear/utils/krusader/">Extragear/utils repository</a>.