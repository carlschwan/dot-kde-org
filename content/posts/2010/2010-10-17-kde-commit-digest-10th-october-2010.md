---
title: "KDE Commit-Digest for 10th October 2010"
date:    2010-10-17
authors:
  - "dannya"
slug:    kde-commit-digest-10th-october-2010
comments:
  - subject: "Yay!"
    date: 2010-10-18
    body: "Danny's back! Now folks, he has a system in place to allow others to help make the digest - so volunteer! :)"
    author: "troy"
  - subject: "Congratulations! It's"
    date: 2010-10-18
    body: "Congratulations! It's necessary show the world the good work that KDE Team do."
    author: "baltolkien"
  - subject: "Oh! "
    date: 2010-10-18
    body: "I didn't understand the return was so near!"
    author: "Caig"
  - subject: "Hurray for digestion"
    date: 2010-10-18
    body: "Thanks to danny and his awesome tool <a href=\"http://enzyme.commit-digest.org/\">\"Enzyme\"</a> even developers with very little time at hands are able to help a bit - although I guess he still does the main work: turning many commit-messages (even if pre-filtered) into a readable text must be a bit like porting assembler code to c++. Manually!\r\nSo: great work, Danny!"
    author: "TunaTom"
  - subject: "Awesome :)"
    date: 2010-10-18
    body: "A warm \"welcome back\" for Danny and the Digest - along with the Digesteers, too :)"
    author: "SSJ_GZ"
  - subject: "Made me more interested in KDE"
    date: 2010-10-18
    body: "The KDE Commit-Digest was one of the things that initially drew me to KDE. Every week, I would look forward to read about all cool new features and look at pretty screenshots.\r\n\r\nThanks Danny Allen and all contributors for reviving the Commit-Digest, you rock!"
    author: "Hans"
  - subject: "Digest is back!"
    date: 2010-10-18
    body: "Wow the digest is back, this is great news, you have really made my day"
    author: "Krul"
  - subject: "Yay!!!!"
    date: 2010-10-18
    body: "Yay, fantastic work Danny, it's great to see the digest back again.  OK peeps, if you enjoy reading it, you know enough to help compile it.  The more people who step up to help the less work it will be!\r\n\r\nWas there any conclusion reached about any tags the devs can add to the commits to help you out?"
    author: "odysseus"
  - subject: "What about hosting enzyme in"
    date: 2010-10-19
    body: "What about hosting enzyme in the new KDE git repo?"
    author: "zx2c4"
  - subject: "Project website"
    date: 2010-10-19
    body: "That's http://enzyme-project.org/ in the general case ;)\r\n\r\nDanny"
    author: "dannya"
  - subject: "Soon"
    date: 2010-10-19
    body: "Something i'm thinking of adding very shortly.\r\n\r\nWhat would the format of the tag be though?\r\nOthers are like BUG:888 or SVN_SILENT...\r\n\r\nI guess DIGEST could work, just need to make sure tags are specific enough to be extracted, and not caught in sentences like \"We digested the information...\"\r\n\r\nDanny"
    author: "dannya"
  - subject: "Maybe"
    date: 2010-10-19
    body: "It's something i'll think about (it would certainly make many things much easier, like mailing lists and using the KDE bug tracker), although I see Enzyme as being project-independent where many projects could use the platform as the basis of their own publications, so I thought it makes more sense to do all the project management myself and outside the KDE development sphere.\r\n\r\nDanny"
    author: "dannya"
  - subject: "keylogger"
    date: 2010-11-12
    body: "KDE software has traditionally been strongest in Europe and South America. With the growth of events such as Camp KDE and many key contributors calling North America home, KDE is increasing its presence in this region.<a href=\"http://www.keyloggersoftwarereviews.com>keylogger</a>\r\n\r\n"
    author: "sani45an"
---
In <a href="http://commit-digest.org/issues/2010-10-10/">this week's KDE Commit-Digest</a>: Return of the <a href="http://www.commit-digest.org/">KDE Commit-Digest</a>! Route Guidance mode (with automatic route recalculation based on position) in <a href="http://edu.kde.org/marble/">Marble</a>. A basic UDev backend added in <a href="http://solid.kde.org/">Solid</a>. <a href="http://www.koffice.org/kformula/">KFormula</a> "Formula" shape becomes compatible with OpenOffice.org. Support for multiple leading zeros when using sequential renaming in <a href="http://dolphin.kde.org/">Dolphin</a>. New HolidayRegionSelector widget which allows users to configure which holiday regions to use in KHolidays (kdepimlibs). Various work in kdepim/mobile. Begin of SMIL "send" support in <a href="http://kmplayer.kde.org/">KMPlayer</a>. Feature improvements in <a href="http://skrooge.org/">Skrooge</a>. New "IMStatus" plugin for <a href="http://choqok.gnufolks.org/">Choqok</a> to send status messages to Instant Messenger applications (currently supports Psi and <a href="http://kopete.kde.org/">Kopete</a>). KTTSD is renamed "Jovie". PowerDevil version 2 imported into KDE SVN.

<a href="http://commit-digest.org/issues/2010-10-10/">Read the rest of the Digest here</a>.
<!--break-->
