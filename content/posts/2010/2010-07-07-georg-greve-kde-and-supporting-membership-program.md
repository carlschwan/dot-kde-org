---
title: "Georg Greve on KDE and the Supporting Membership Program"
date:    2010-07-07
authors:
  - "jospoortvliet"
slug:    georg-greve-kde-and-supporting-membership-program
comments:
  - subject: "Great interview"
    date: 2010-07-07
    body: "I've really enjoyed reading this.\r\n\r\nWhen I originally read about Georg's supporting membership I mistook it for a purely political gesture without any actual passion in it - like when Mark Shuttleworth showed up in a KDE shirt a few years ago.\r\n\r\nIn this interview, though, you really can \"feel the love\". That makes me respect the FSFE yet a little more than I already did."
    author: "onety-three"
  - subject: "I agree."
    date: 2010-07-07
    body: "I agree."
    author: "Gallaecio"
  - subject: "Office"
    date: 2010-07-07
    body: "Although it hasn't been widely publicised, KDE e.V. now shares an office with FSFE"
    author: ""
---
At LinuxTag 2010, the KDE community announced the <a href="http://jointhegame.kde.org">"Join the Game"</a> campaign to support KDE e.V.'s Supporting Membership Program. The first new member was Georg Greve, founder of Free Software Foundation Europe (FSFE). We caught up with him for a 'short' interview that turned into a two hour conversation about life, the universe and everything. The pertinent bits are excerpted below.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org//sites/dot.kde.org/files/greve.png" width="202" height="303"/></div><b>Jos</b>: Georg, you've been interviewed before so you know what to do first...

<b>Georg</b>: Ah, yes, 42 short answers as to the why and how of me...

Well, I started out as a software developer at age 13. I studied Physics, did my diploma thesis in the area of nanotechnology and became part of the GNU project during my studies. The GNU Xlogmaster which I wrote was the first graphical log overview application that I know of. It's crappy, but there was no alternative back then. Today there are other applications based on that idea that do a much better job. So I was an alright coder, but it turned out that I had a knack at communicating technology to non-technical people, or "Muggles" if you are into Harry Potter. I had been writing the Brave GNU World column for years. So after chats with Richard Stallman and others we founded the FSFE in 2001. At that time I had several options, including an academic career with a PhD in nanotechnology, or a job in the industry. While both would certainly have been a tad easier, I thought that FSFE would be more fun and was more necessary. So after taking out a personal loan I started working full time. (Luckily the bank never knew what the money was for and it has been paid back in full some years later.) The organization grew quickly from there. While at first I had to do everything myself, more and more people joined and took over tasks and responsibilities. Much of these first years was spent travelling, speaking, meeting people and occasionally sneaking off to do a little coding or administrate FSFE's systems. At one point I remember that I fixed the mail system while sitting in the plenary of a UN conference, another time I ended up sneaking out into a street cafe in Buenos Aires, where I believe I wrote the Python layer that does the data transport over SMTP for FSFE's Fellowship registration process. Hard times.

In the last few years at FSFE, I've worked to prepare our organization for a change in leadership. I didn't want the FSFE to grow stale - fresh blood is good every now and then. We had some amazing interns over time. Now some of them form the next generation running the FSFE. A sign of a healthy organization (if you ask me) is where an ambitious and talented intern can become president! 

<b>Jos</b>: And then you decided to start up a new company.

<b>Georg</b>: Yes, indeed. The story is probably too long for the dot, but the short of it is that KDAB and Intevation brought Paul Adam and myself together. <a href="http://kolabsys.com">Kolab Systems</a> has been growing quickly since getting started in February 2010. We aim to support companies who deploy and service Kolab PIM and groupware installations.

<b>Jos</b>: Which is why you're here, at this conference?!?

<b>Georg</b>: Yes, and of course for the Supporting Program membership (grinning).

<b>Jos</b>: Ok, so tell us what made you say yes to this request of becoming the First?

<b>Georg</b>: Because I love the KDE community, of course! Seriously, I'm a huge fan. I used to use Gnome, but switched when more and more non-free technology was added. I think they are making a huge mistake giving up control of so much of their technological strategy to Microsoft.

So one day a friend came in and showed me how to slim down KDE 3.5 to a level I liked. And then I became an incredibly early adopter of the KDE 4 series. I installed the shiny Plasma desktop in the summer of 2008. It was interesting technology, and I wanted to support the KDE community in finding a new path from the bottom up instead of re-inventing something else as is done by others. Having the courage to do something really new is something I greatly respect. You guys and girls did it, and with success. So I started sending in bug reports and telling people about what you were doing, publicly supporting the move KDE has made. I was at the launch event of 4.2 in Zurich with Aaron Seigo. While I couldn't help much technically, these things I could do. 

Besides the guts and technology the KDE community has, the people behind it and the whole culture within the community are major reasons for me to support it. I always have great fun when meeting KDE people, be it for work or drinking beer. The way you have fun, the way the work flows, it is truly remarkable. The KDE community was one of the first in the Free Software world I knew to have a female president (Eva Brucherseifer). To me, an environment where women feel at home is a very positive and important thing. It has something to do with respect, an open and friendly atmosphere. I have no idea how to say what exactly it is that makes the KDE community so special. I know many  communities. Many are great, but KDE is clearly awesome and for myself second only to the FSFE, which has a different job in our community. KDE even helped to migrate my wife to Free Software. She comes from a very artistic family, both her grandfathers were famous painters, so she is quite visually oriented. When she saw my Plasma desktop she said "I want this". Now you must know I didn't try to force her. If you knew her, you'd understand that would never work. So getting her interested is quite a compliment to KDE.

And a final thing...I greatly appreciate the strong focus within the KDE community on freedom, Freedom with a capital F. You spread this message of Be Free, which is what I have spent a full 10 years of my own life on, and continue to do so. At the same time, you're a pragmatic community, like myself and many people in the FSFE. 

<b>Jos</b>: Wow, I feel the love! Cool to see you enjoy our company so much. So how did you hear about our Supporting Membership Program?

<b>Georg</b>: Well, Frank Karlitschek contacted me on jabber and asked if I wanted to join. I said I'd be honored (which made it in the initial announcement as a germanglish quote as we spoke German).

<b>Jos</b>: Haha, yes, I noticed that... And what do you think about the program?

<b>Georg</b>: Well, everyone should join. Ultimately it is the same for the Fellowship of FSFE. We as a community need to support the associations and groups protecting our ability to live in freedom and have the technology we want. So I'm talking about both the political (FSFE) and technical (KDE) levels. We even have the freedom to pick what we want to support, isn't that great? I pick the FSFE and the KDE communities because I agree with their values and choices and I'm happy to support them. It boils down to what a famous person said: "With great power comes great responsibility" (editors' note: hehe - Spiderman's Uncle Ben) and KDE is truly powerful.

<b>Jos</b>: So KDE and the FSFE fit together, it seems?

<b>Georg</b>: Yes, I like the strong connection between the FSFE and KDE. It is a match made in heaven. Both communities share a lot of similar values. We approach the same issues from different sides and thus are complementary. I have been working with KDE since Eva was President of KDE. We've always gotten along great as we stood for the same things, even from different angles. So I have a long-standing positive relationship with the KDE community and am happy to see it grow and be supported.

<b>Jos</b>: We're being interrupted by Paul so this is the end of our conversation. Any last words?

<b>Georg</b>: Well, let's hope they won't be my final words, but yeah. I would give a big thumbs up to the KDE community and would urge users to support it! Oh and on a more personal level, I can recommend the policy we have at Kolab Systems: If you have not had a good laugh today, something is wrong and you have to fix it!