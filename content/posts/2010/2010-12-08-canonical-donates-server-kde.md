---
title: "Canonical Donates Server to KDE"
date:    2010-12-08
authors:
  - "jriddell"
slug:    canonical-donates-server-kde
comments:
  - subject: "Thanks Canonical"
    date: 2010-12-08
    body: "Canonical is usually not nice with KDE, nice to see some good will.\r\n\r\nThanks Canonical for helping KDE :)"
    author: "Ikipou"
  - subject: "Nice gesture"
    date: 2010-12-08
    body: "A very nice gesture of Canonical to KDE. I'm sure it will be put to excellent use by de KDE admins and community!"
    author: "andresomers"
  - subject: "They are supporters"
    date: 2010-12-08
    body: "Canonical have sponsored KDE events in the past (e.g. Akademy 2010) and employ some KDE people.\r\n\r\nBut as you say, this is very welcome, so thanks!"
    author: "Stuart Jarvis"
  - subject: "Great news"
    date: 2010-12-08
    body: "Maybe http://bugs.kde.org can get some juice too. It's really needed."
    author: "tanghus"
  - subject: "Great news!"
    date: 2010-12-09
    body: "Thanks, Canonical! KDE will put this server to very good use."
    author: "valoriez"
  - subject: "Mark/Canonical is also a Supporting Member."
    date: 2010-12-09
    body: "Not just supporting, but the highest level.\r\nhttp://ev.kde.org/supporting-members.php"
    author: "Bios Element"
---
KDE is a growing community with growing needs. The KDE Sysadmin team works hard to keep up, but lately the servers have been coming under some strain. 

To help ease the situation, <a href="http://www.canonical.com/">Canonical</a> has donated a new server for the KDE Sysadmins to use. The server, named kundong, features an impressive 8 CPU cores, 6 GB of memory, and a 130 GB disk with space for several more disks as needed. It is hosted in Canonical's data center in central London.
<!--break-->
Welcoming the new server, Tom Albers from the KDE Sysadmin team said, "This machine will make it possible to expand the services we provide to KDE contributors even more, combined with the speed and power to improve some of our current services". The first services to move to the new server are expected to be KDE's <a href="http://techbase.kde.org/">TechBase</a>, <a href="http://userbase.kde.org/">UserBase</a> and <a href="http://community.kde.org/">Community</a> wikis.

