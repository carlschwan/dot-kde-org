---
title: "KDE Commit-Digest for 31st October 2010"
date:    2010-12-03
authors:
  - "dannya"
slug:    kde-commit-digest-31st-october-2010-0
comments:
  - subject: "Yay!"
    date: 2010-12-04
    body: "Very happy to see the digest continuing!"
    author: "tangent"
  - subject: "Thanks"
    date: 2010-12-06
    body: "Me too."
    author: "delta_squared"
---
In <a href="http://commit-digest.org/issues/2010-10-31/">this week's KDE Commit-Digest</a>: Improved map management and support for exporting routes in the KML format in <a href="http://edu.kde.org/marble/">Marble</a>. Support for showing/hiding the bottom page bar in <a href="http://okular.org/">Okular</a>. An option to create QML <a href="http://plasma.kde.org/">Plasmoids</a> added to Plasmate. Support for GetHotNewStuff in the Pastebin Plasmoid. Work to increase the number of supported rows in <a href="http://www.koffice.org/kspread/">KSpread</a>. Pidgin support added to the "IMStatus" function of <a href="http://choqok.gnufolks.org/">Choqok</a>. Animated tiles in <a href="http://kde.org/applications/games/kajongg/">Kajongg</a>. Various work in <a href="http://pim.kde.org/">KDE-PIM</a>. Start of filtering (no GUI yet) integrated into <a href="http://kontact.kde.org/kmail/">KMail</a> Mobile. Long-awaited ability to set custom button labels for yes, no, continue and cancel buttons in KDialog. "Find links only" option added to the "find bar" in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. More work in kdebindings/perl. New activity manager daemon (with the KDED and <a href="http://nepomuk.kde.org/">Nepomuk</a> service merged into a separate application). Facet API into imported into libnepomuk. Timestamp functionality added to KSharedDataCache to support KGameRenderer and similar libraries. Support for shutdown/restart when there is no active display manager (using ConsoleKit) in KDisplayManager. PolicyKit-kde removed from kdebase. <a href="http://koffice.org/">KOffice</a> 2.3 Beta 3 is tagged for release.

<a href="http://commit-digest.org/issues/2010-10-31/">Read the rest of the Digest here</a>.
<!--break-->
