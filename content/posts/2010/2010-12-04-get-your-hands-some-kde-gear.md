---
title: "Get Your Hands on Some KDE Gear"
date:    2010-12-04
authors:
  - "Justin Kirby"
slug:    get-your-hands-some-kde-gear
comments:
  - subject: "love these shirts"
    date: 2010-12-04
    body: "Totally fun. :)"
    author: "eean"
  - subject: "kde swag, finally! :) i've"
    date: 2010-12-04
    body: "kde swag, finally! :) i've always wanted one of those blue shirts with a gear on.. :)\r\n\r\nbut what happened to it? some iconic guy/heart got added to it?  I prefer the simple K+gear. \r\n\r\nGreat initiative, good job!"
    author: "mxttie"
  - subject: "nice"
    date: 2010-12-05
    body: "I like it so much... The red one is my favorite :-"
    author: "vector"
  - subject: "There are options"
    date: 2010-12-05
    body: "There are also shirts with just the K/gear logo - ideas are welcome if they those ones don't suit you.\r\n\r\nWe liked the heart-KDE logo, but there's a selection with and without because nothing is ever to everyone's taste."
    author: "Stuart Jarvis"
  - subject: "Its awesome I like this"
    date: 2010-12-13
    body: "Its awesome I like this post.I also love red color too much.\r\n<a href=\"http://www.realtests.com/exam/1z0-042.htm\">1z0-042</a> certified professional\r\n"
    author: "animaandy"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdeswag.png" /><br />A selection of the available items</div>Just in time for the holiday gift rush, the <a href="http://community.kde.org/Promo">Promo team</a> has set up stores where you can find a variety of shirts, hats, and accessories sporting cool KDE designs. Special thanks to <b>Eugene Trounev</b>, <b>Bugsbane</b> and <b>Sebastian Kügler</b> for some of the first designs.  
<!--break-->
To make shipping cheaper, there are currently two localized sites:

<ul>
<li>Europe - <a href="http://kde-gear.spreadshirt.net">http://kde-gear.spreadshirt.net</a></li>
<li>North America - <a href="http://gearwear.spreadshirt.com">http://gearwear.spreadshirt.com</a></li>
</ul>

We would be interested to hear from volunteers willing to help set up localized sites for other regions. Please <a href="mailto:kde-promo@kde.org">email the KDE-Promo team</a> if you are interested in helping with this.

We are also still accepting new designs. If you have an idea for new design, please post it to the <a href="http://community.kde.org/Promo/Material/Swag">"Swag" wiki page</a> for consideration. 

