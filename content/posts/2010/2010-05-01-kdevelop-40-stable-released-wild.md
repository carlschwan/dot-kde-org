---
title: "KDevelop 4.0 Stable Released into the Wild"
date:    2010-05-01
authors:
  - "apaku"
slug:    kdevelop-40-stable-released-wild
comments:
  - subject: "PHP for Qt development?"
    date: 2010-05-01
    body: "Can PHP with Qt be compiled (via Hiphop) ?\r\nAre the PHP Qt bindings also LGPL?\r\n\r\nAnyways: Awesome realse, congrats."
    author: "kragil"
  - subject: "Congrats!"
    date: 2010-05-01
    body: "Thanks for all the hard work :-)."
    author: "Eike Hein"
  - subject: "Great IDE!"
    date: 2010-05-01
    body: "I've been using the last RC for a week and it is very very nice.\r\n\r\nThe documentation integrated in the IDE works pretty well and I find everything to be just in place.\r\n\r\n*Really* good work and congratulations!"
    author: "Damnshock"
  - subject: "HipHop is not done yet"
    date: 2010-05-01
    body: "It's not that simple. Getting a PHP codebase to run on HipHop still requires some porting at the moment; some PHP features are not supported. This is perfectly fine for large companies with huge PHP code (it's an investment which gives a return value). But for small projects it's not \"just compile and go\" yet."
    author: "vdboor"
  - subject: "Thank you very much!"
    date: 2010-05-01
    body: "Great to see that decent PHP development will be possible with KDE4 native tools again. Let's hope that MW will be successful bringing back Quanta, too. :)"
    author: "lxg"
  - subject: "Many thanks for your work!"
    date: 2010-05-02
    body: "I tried the beta, and it is really a joy to code in it! (though emacs wins currently because I often need to worke remotely via command line)\r\n\r\nCode looks cleaner, and with the stable release it\u2019s time for me to use it for my project again. \r\n\r\nMany many thanks! "
    author: "ArneBab"
  - subject: "Great work! I'm looking"
    date: 2010-05-02
    body: "Great work! I'm looking forward to giving KDev4 a spin! :)"
    author: "teatime"
  - subject: "Re: PHP for Qt development?"
    date: 2010-05-02
    body: "\"Are the PHP Qt bindings also LGPL?\"\r\n\r\nYes, they are. They need a little work to get them in step with the other Smoke based language bindings, but that should be possible for KDE 4.5."
    author: "Richard Dale"
  - subject: "Code parser"
    date: 2010-05-02
    body: "I'm playing with it and it looks really nice!\r\n\r\nQuestion: is it possible to exclude specific project subdirs from the code parser?\r\n\r\nI'm asking because working on a project which imports the whole Boost headers leads to the code parser taking up a lot of resources (both CPU and memory). I would like to be able to have all the nifty features - code completion, online docs - just for my own classes but not for the Boost ones. Is this possible?\r\n\r\nThanks!"
    author: "bluescarni"
  - subject: "Thanks!"
    date: 2010-05-02
    body: "I have been using it since the early betas. It makes coding a bliss. I cannot count the number of man-hours it saved me, but if I were to send it back to the developers in cash, they would be rich, I am sure :D"
    author: "msoos"
  - subject: "Using it and loving it"
    date: 2010-05-03
    body: "Thanks for the excellent app! I'm using it for some time now and like it very much. Semantic highlighting is the killer-feature for me."
    author: "michalm"
  - subject: "Awesome!"
    date: 2010-05-03
    body: "I've been using it for a while now, and it's really great! I miss Git support though."
    author: "SP"
  - subject: "great!"
    date: 2010-05-03
    body: "I've been using older KDevelop 3.5 and since beta8 also KDevelop 4 and I really enjoy all the features it provides. Specially code completition and correction are great features throught making coder really lazy to remember some details about code :)"
    author: "ventYl"
  - subject: "Thank you"
    date: 2010-05-03
    body: "It seems as if you have really done a lot of work on this. Congratulations on all your hard work and thank you so much for taking the time to explain it all in detail and for sharing the code.  <a href=\"http://www.mobileactive08.org\"><font color=\"#f8f8f8\">online &#67;asino</font></a>"
    author: "Ethan"
  - subject: "Great effort "
    date: 2010-05-03
    body: "I really like Kdevelop now, you guys have done a great job.  \r\nCongratulations!"
    author: "Ricky"
  - subject: "qmake support?"
    date: 2010-05-04
    body: "First of all: congratulations on this milestone! It looks really nice. You can be very proud of this achievement.\r\n\r\nI tried to use KDevelop on a project that I am currently working on in QtCreator, and found out to my disapointment that KDevelop can not use qmake's pro files. It can not even read them for import. There seems to be an unmaintained, unsupported plugin, but that has not seen any work for more than a year, if I interpret the repository correctly. That is a real pitty. \r\n\r\n"
    author: "andresomers"
---
The KDevelop Hackers are proud and happy to announce that KDevelop 4.0 is finally available as a stable release. Released together is the first version of KDevelop PHP plugins, which make KDevelop a very interesting option for PHP developers.
<!--break-->
<h2>Features</h2>
KDevelop 4.0 comes with lots of features already, even though some things had to be dropped compared to the 3.5 series due to time constraints. In particular we have focused on building an excellent C++ IDE instead of trying to integrate lots of languages halfheartedly. Of course it is still possible to add support for more languages to KDevelop and we are confident that it is actually easier than before: the best proof for that is the PHP plugin that is released alongside KDevelop 4.0.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kdevelop_0.png"><image src="http://dot.kde.org/sites/dot.kde.org/files/kdevelop_wee.png" width="200" height="144"></a></div>

<h3>Code navigation, syntax checking and more</h3>
The major features for C++ support include code navigation, which allows you to jump to declarations, and semantic highlighting. Combined they make it much easier to grasp new code bases and navigate in big code projects. Support for syntax checking and semi-automatic code-correction with assistants is included as well as code completion that supports classes, functions, templates, variables and include statements. For the Qt developers among us there is extensive support for the signal/slot mechanisms in Qt. You get signal-completion in connect statements as well as completion for matching slots. Very handy is the weighting of completion items based on their matching in regard to the type of function arguments or the signal-slot signatures.

<h3>Project management and code navigation</h3>
For the project management we currently have CMake support as the main buildsystem plugin, building projects with custom or generated makefiles is possible too. The CMake support also has some of the above features available so you will get some completion and semantic highlighting as well as code-navigation even in CMake files. Integration between C++ and CMake means new classes can be added to targets in CMake files semi-automatically. To ease the fixing of build errors KDevelop allows jumping to the errors in the code. Furthermore you can use QuickOpen to navigate very fast to any file, class or function in your opened projects or inside the currently opened file.

<h3>GDB integration</h3>
The integrated GDB support makes it possible to find runtime errors you may have in your codebase. It allows for the usual features like breakpoints, stepping, stopping and inspecting the current call stack. Additionally through the use of special pretty-printing support in GDB we can show many Qt, KDE and STL types in a human-readable form. So your QString's will show up with their actual string literal in the variable list, a QList will have an expandable set of child items and so on.

<h3>Documentation</h3>
The last great feature of KDevelop we are highlighting is integration of documentation. KDevelop shows inline API documentation for both, CMake and Qt, in tooltips over classes and functions and it allows you to navigate in complete documentation via a toolview.

<h2>PHP plugins</h2>
This first release of the PHP support plugins already comes with a wealth of features that take some of the burden off a web developer:

Syntax errors get reported as you type and the sources of whole projects get semantically analyzed, including PHP Doc comments for type hints of parameters and return values. Furthermore it will give you extensive code completion, with solid support for OOP. We also support the above mentioned context browsing and semantic highlighting, integrate the online PHP.net documentation and hook into the QuickOpen and class browser plugins.

<h2>More to explore</h2>
There are many more things to discover in KDevelop and the PHP plugins, like SVN integration with inline editor annotations, grepping over files, session support and source code formatting. Far too much for a short article like this, so we have created a <a href="http://apaku.wordpress.com/2010/04/25/kdevelop-4-0-screenshots/">dedicated blog entry</a> to show off some of the features.

<h3>Get Started!</h3>
We invite everybody to get a copy of the source code from <a href="http://download.kde.org/stable/kdevelop/4.0.0/src/">KDE mirrors</a> or install packages from distributions. We wish you all many happy hacking hours. If you run into problems or have questions do not hesitate to contact us either in #kdevelop on freenode, via bugs.kde.org or on <a href="http://www.kdevelop.org/index.html?filename=mailinglist.html">our mailinglists</a>.