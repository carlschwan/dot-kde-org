---
title: "Amarok 2.3.0 \"Clear Light\" Released"
date:    2010-03-15
authors:
  - "nightrose"
slug:    amarok-230-clear-light-released
comments:
  - subject: "Hopefully the feature to hide"
    date: 2010-03-23
    body: "Hopefully the feature to hide menubar from GIT will get to next coming version."
    author: "Fri13"
  - subject: "Amarok"
    date: 2010-05-30
    body: "The Amarorok team is doing a great job, especially in fixing the Amarok 2.3.0 & making in bugs free.\r\nIt's a hard job but they did it the right way."
    author: "Basha besh"
  - subject: "I'm so sad :-("
    date: 2010-07-02
    body: "Hi amarok team,\r\n\r\nI loved Amarok in its 1.4 series. this is now over a year that I use KDE4 with Amarok 2, and I realy do not understand where you are going.\r\n\r\nAmarok is becomming an application for power users, there are lots of new things that are not intuitive. I think the most important one is how to navigate in media sources. The tabs on the left side where just perfect, now I have to click on a tiny button named '>', isn't that anoying ?! I still remember the first time I used Amarok2: it took me hours to find out where to click to browse my folders !\r\n\r\nSo I joined the team for a while and contributed a fiew things, but you had your roadmaps and ideas of how things should be and my ideas where just not on the right path so I lost the motivation.\r\n\r\nAll I would like to tell you now is: please remember, \"simple is beautiful\".\r\n\r\nI am now looking for another musik browser, this makes me really sad and I did not want to quit without a last user feedback.\r\n\r\nI used Amarok so many years and I thank you sincerlly for your hard work and the joy I had using it.\r\n\r\nbest regards.\r\n\r\n"
    author: "lbayle"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/amarok230.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok230t.png" /></a></div>The Amarok team just released Amarok 2.3.0. It comes with many bugfixes and improvements such as a new funky toolbar and a rewritten file browser featuring much better integration with the rest of Amarok. Read the <a href="http://amarok.kde.org/en/releases/2.3.0">release notes</a> and enjoy rediscovering your music!

Oh and a new <a href="http://amarok.kde.org/en/Insider/Issue_14">Amarok Insider</a> is out with some tips and tricks including how to search your music collection easily.
<!--break-->
.