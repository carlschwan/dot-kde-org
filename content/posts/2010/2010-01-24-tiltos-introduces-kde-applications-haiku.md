---
title: "TiltOS Introduces KDE Applications For Haiku"
date:    2010-01-24
authors:
  - "dequire"
slug:    tiltos-introduces-kde-applications-haiku
comments:
  - subject: "Looks great"
    date: 2010-01-24
    body: "I don't know about the rest of you, but the screenshots look great! I mean, I first saw these while some presentation was going on at Camp KDE, and couldn't resist passing my laptop around to ensure that everyone got a look. \r\n\r\nCongrats to the Haiku team for making it this far already, and I hope that they find KDE useful :)"
    author: "troy"
  - subject: "Looks great indeed, but..."
    date: 2010-01-25
    body: "It is great to see that KDE now also runs on Haiku, giving this operating system much needed software applications.\r\n\r\nbut:\r\n\r\n\"Why not Linux?\r\nLinux-based distributions stack up software -- the Linux kernel, the X Window System, and various DEs with disparate toolkits such as GTK+ and Qt -- that do not necessarily share the same guidelines and/or goals. This lack of consistency and overall vision manifests itself in increased complexity, insufficient integration, and inefficient solutions, making the use of your computer more complicated than it should actually be. [top]\r\nInstead, Haiku has a single focus on personal computing and is driven by a unified vision for the whole OS. That, we believe, enables Haiku to provide a leaner, cleaner and more efficient system capable of providing a better user experience that is simple and uniform throughout.\"\r\n\r\nThe above comes from their website:\r\nhttp://www.haiku-os.org/about/faq#3\r\n\r\nPorting Qt and Gtk/X11 to Haiku to get sufficient software kinda violates their own manifest..\r\n"
    author: "whatever noticed"
  - subject: "Please do keep in mind that"
    date: 2010-01-25
    body: "Please do keep in mind that both the Qt and KDE ports to Haiku are 3rd party projects, not coming officially from Haiku or Nokia (or KDE for that matter). So while core Haiku development has adopted that philosophy, I don't think they are prohibiting anyone from doing something like this (there aren't any violent or negative reactions on http://www.haiku-os.org/community/forum/qt_haiku for example).\r\n\r\nAnother good read (as well as the comment, which talk about the same thing you pointed out): http://arstechnica.com/open-source/news/2010/01/haiku-gains-kde-applications-as-qt-port-matures.ars\r\n\r\nAnyway, from a selfishly KDE POV, I do see this as an advantage (wider KDE reach) and an exciting development. It's always nice to see KDE running on another FOSS platform that isn't totally *nix-based. :)"
    author: "Jucato"
  - subject: "Looks really great"
    date: 2010-01-25
    body: "I thought the same. I read an article about 10 alternative operating systems that the most people never heard of and Haiku was one of them. It was the only one that I thought that I would try. I downloaded the iso and I will be installing it on an old machine. I am just curious and yes it looks great with the KDE apps."
    author: "Bobby"
  - subject: "The Article of 10 alternative OS's"
    date: 2010-01-28
    body: "I think it is important that we try to preserve the alternative OS's in use. Even that Linux is great OS, we need to try to make use cases for GNU's own operating system Hurd or all four of the BSD OS's and all these other OS's. This way we can keep choices of future open. "
    author: "Fri13"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/haiku-kde.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/haiku-kde_thumb.png" width="300" height="225"/></a><br />KDE applications on Haiku</div>Continuing the theme of "KDE Everywhere", recently it was announced by the folks over at <a href="http://tiltos.com/drupal/node/17">TiltOS</a> that KDE applications are now available for download and use. TiltOS provides a repository and an easy to use application manager for the <a href="http://www.haiku-os.org/">Haiku operating system</a>, which is in turn aims to be an open source implementation of the <a href="http://en.wikipedia.org/wiki/BeOS">BeOS operating system</a>.
<!--break-->
The available applications include:

<i>kchmviewer, kdeaccessibility, kdeadmin, kdebase, kdeedu, kdegames, kdegraphics, kdelibs, kdelibs-experimental, kdemultimedia, kdenetwork, kdepim, kdepimlibs, kdepim-runtime, kdesdk, kdetoys, kdeutils, kdevplatform, kdewebdev, kdiff3, koffice and many others packages. In all there are about 150 applications ready to run.</i>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/haiku-koffice-kpresenter.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/haiku-koffice-kpresenter_thumb.png" width="300" height="225"/></a><br />KOffice's KPresenter on Haiku</div>
As reported in the <a href="http://tiltos.com/drupal/node/17">announcement</a>, this is all possible thanks to the Qt port for <a href="http://www.haiku-os.org/">Haiku</a>. KDE continues to grow into more devices and onto more operating systems all the time. And now, users of this operating system can enjoy the KDE as well. And hopefully also become an important part of the growing KDE Community!