---
title: "Interview with Stephen Kelly"
date:    2010-06-04
authors:
  - "giovanni"
slug:    interview-stephen-kelly
comments:
  - subject: "No mention of any ProxyModel?"
    date: 2010-06-04
    body: "An interview with Stephen and not a single mention of the word ProxyModel, I'm amazed ;-)\r\n\r\nCya"
    author: "Milian Wolff"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org//sites/dot.kde.org/files/stephenkelly.jpg"><img src="http://dot.kde.org//sites/dot.kde.org/files/stephenkelly_small.jpg" width="300" height="224"/></a><br />Stephen Kelly</div>
Yesterday, Stephen Kelly wrote on the dot <a href="http://dot.kde.org/2010/06/03/kde-pim-stabilization-sprint">about the successful KDE PIM sprint</a>. Today, you can read more about him and his role as KJots maintainer in this interview by Giovanni from our Italian KDE community. This continues a trend of recent interviews talking to members of the KDE PIM team - last time we heard from <a href="http://dot.kde.org/2010/05/24/interview-kmail-developer-thomas-mcguire">Thomas McGuire</a> of KMail. For our Italian readers, there is also <a href="http://kde-it.org/2010/05/21/intervista-con-stephen-kelly/">the original interview</a>.
<!--break-->
<b>Hello Stephen, can you introduce yourself to our readers?</b>
My name is Stephen Kelly, I'm 25 years old, Irish and currently living in Berlin. I graduated from Mechanical Engineering in Dublin some years ago and now I work for <a href="http://www.kdab.com/">KDAB</a>, most visibly on <a href="http://pim.kde.org/akonadi/">Akonadi</a> and <a href="http://pim.kde.org/">KDE PIM</a>.

<b>When did you first hear about KDE?</b>
I first heard about KDE when I installed Fedora Core 5 in March 2006. It was my first encounter with linux and during the setup I installed both KDE and GNOME environments. In the end I used the KDE desktop more so I stuck with that.

<b>In which part of the KDE community and in what development area(s) are you involved? Can you talk us about KJots? What is it?</b>
My contributions within KDE are mostly in the area of model/view in the KDE Development platform (kdelibs and kdepimlibs) and developing infrastructure and prototypes for Akonadi applications.
Akonadi is a framework for Personal Information Management (PIM), which means email, contacts, events, tasks, notes etc. It provides a platform for creating PIM applications such as email readers, contact storage applications, calendaring and more. Currently we are basing popular KDE applications such as KMail, KaddressBook and KOrganizer on Akonadi, but it can be reused throughout the entire platform and by all applications.
I also maintain the KJots application, which is a simple note taker which makes it easy for users to organize notes in a heirarchy. KJots is already based on Akonadi, so the same notes can be viewed and edited directly on the Plasma workspace too.

<b>Is the porting to Akonadi ready or there are still some parts of your application that have to be ported? What benefits will Akonadi give to Kjots users?</b>
For KJots the porting to Akonadi is almost complete, and we expect to release it with version 4.5 of the KDE Software Compilation.
Aside from making the notes available through Plasma, Akonadi enables KJots to store the notes in a remote location instead of locally on your computer, such as an FTP server, a groupware server, and possibly even any IMAP server. Another benefit that Akonadi brings is code sharing with the rest of the KDE PIM applications, particularly KMail, as notes and emails are very similar.

<b>What are the difficulties you encounter in developing a PIM application? How much of your time do you dedicate to programming for KDE?</b>
For new developers it is getting easier to develop PIM applications every month. Akonadi provides a strong base for anyone to create their own application. If KMail doesn't suit your needs, get in touch and we can help you write an email client that better suits you and people like you!
As part of my employment involves Akonadi and KDE development I work at least 40 hours a week on KDE, and sometimes more.

<b>What application do you use to develop KJots? What is the knowledge needed to develop an application like Kjots?</b>
I use KDevelop and CMake for all of my Qt related development. The KDevelop team have made an excellent application and the stable release of KDevelop is very exciting.
There are parts of KJots which are very easy to get started developing on, such as the rich text sections and some configuration. Sending small patches to KJots was my introduction to Qt and KDE programming at a time before I knew anything about programming (some would say I still don't :) ).

<b>I personally prefer a desktop application to do my things. What do you think of all these integrated web application? Facebook, Google Apps?</b>
I also prefer rich client desktop applications, and KDE is an attractive platform to create and use them on. While a cloud/web service provides an integration point, many clouds provide many integration points which you need to keep track of, and then it's not an integration point anymore! Moving from one service to another is not usually possible either which means permanence of your data is a big issue. With KDE and OwnCloud though, there is only one integration point and it is controlled by the user.

<b>What are the missing features within KDE applications you think we should have to build a stronger product series?</b>
Full integration of Nepomuk and Akonadi throughout the platform and shared between multiple devices. There's a lot of potential there for allowing new ways to use computers but it is very new technology and there are many areas it could be used in. It will take us a while before we reach the true, integrated future. There is very exciting progress with each release though.

<b>What are in your opinion the KDE applications which are doing a very good job? And which ones need lots of love and help?</b>
I'm always very impressed with the KDE Games and Educational applications, and I think everyone is. And I like Konsole, KDevelop and Amarok which I use a lot.
I think there's a gap though for an application to handle jabber with many group chats with many participants. The existing clients don't seem to suit my specific needs there.

<b>What’s your favourite KDE application?</b>
Akregator, KNode and Konqueror perhaps. They are my window to the web and to mailing lists. :)

<b>What are your plans for KJots in the next KDE PIM release?</b>
Apart from Akonadi integration, KJots in KDE PIM 4.5 will use Grantlee internally for user defined theming and export of notes. There won't be a UI for configuring themes though until at least 4.6. Part of the Google Summer of Code project of Ronny Yabar this year will involve making KJots themes user-configurable and sharable.

<b>Are you payed to work on KDE software?</b>
Yep. My job currently involves improving the features and quality of KDE PIM and Akonadi on the desktop and on mobile platforms.

<b>What motivates/keeps you motivated to work on KDE software?</b>
Community spirit, a desire to make better software than the competition and scratching personal itches.

<b>If you could choose where you can live what place you will choose and why?</b>
I really like Dublin and Berlin, but I also really like travelling, so maybe I'd choose to be a Nomad for a while. :)

<b>Have you ever been in Italy? If so, where?</b>
Yes, I've been to Milan, Verona, Rome and Rimini. Plenty more left to see. :)

<b>Do you like reading? What was the last book you read?</b>
Yes I like reading, but don't do it as often as I would like to. The last book I read was Brave New World. I sometimes drop small (or big) references to books I'm reading in my <a href="http://steveire.wordpress.com/">blog</a>. If you recognize any, leave a comment!

Stephen, thank you for your time and good luck with what you're doing.