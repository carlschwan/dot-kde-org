---
title: "4.6 RC 1 Available, KDE PIM Delayed"
date:    2010-12-23
authors:
  - "sebas"
slug:    46-rc-1-available-kde-pim-delayed
comments:
  - subject: "Nepomuk error"
    date: 2010-12-23
    body: "I'm getting a Nepomuk-error every time I log in, without starting any application and having disabled nepomuk/akonadi in the system settings. A mysqld from akonadi is also always running on the background. Is this a general problem, or just me?"
    author: "trixl."
  - subject: "Quite a few problems"
    date: 2010-12-23
    body: "I know RC1 works quite well for many people, in particular other openSUSE users. Personally I had plenty of problems though. \r\n\r\n- Krunner crashes on login, and it keeps re-spawning and crashing until manually killed (Bug 261065)\r\n\r\n- Search and Launch activity crashes as soon as any text is input in the search field (Bug 261066)\r\n\r\n- Kwin freezes and shows artefacts consistently after some time running (Bug 261069)\r\n\r\nThere is a chance the two first problems might be related with the problematic NVIDIA 260.xx.xx driver, which has been bothering many openSUSE users, even though these do not occur in 4.5.4. As for the Kwin problems, these also occurred in Beta 2 when testing with the 25x.xx.xx driver. \r\n\r\nNote that starting KDE with a brand new user didn't fix anything. On the contrary, not only krunner but the whole plasma desktop crashed. I really wanted to move to KDE 4.6 as it seems to solve some annoying problems related to emacs and kwin. Seems like that will have to wait. I had to downgrade as I need a stable system to work at the moment, but if you experience any of these do provide more info. \r\n\r\nGood luck"
    author: "jadrian"
  - subject: "I cannot reproduce your"
    date: 2010-12-23
    body: "I cannot reproduce your problems. (opensuse rpm's / intel graphics)"
    author: "trixl."
  - subject: "note"
    date: 2010-12-23
    body: "<cite>Note that starting KDE with a brand new user didn't fix anything. On the contrary, not only krunner but the whole plasma desktop crashed.<cite>\r\n\r\nJust a quite note to add that now that I think of this, it is unsurprising, since I already new that if the digital clock (or any other clock) plasmoid is present, then the plasma desktop will crash when using NVIDIA 260.xx. "
    author: "jadrian"
  - subject: "backtraces"
    date: 2010-12-23
    body: "first, thanks for reporting the issues on bugs.kde.org. however, when something is crashing, especially without any specific trigger as in this case, a backtrace is absolutely critical to being able to resolve it.\r\n\r\nthat level of instability (combined with your issues with the digital clock) does seem to be indicative of something like a problematic graphics driver. hopefully you can upgrade/downgrade that driver to something that works :/"
    author: "aseigo"
  - subject: "backtraces"
    date: 2010-12-23
    body: "Hi Aaron. I know the backtraces are important and installed the necessary debug packages to provide them. But the bug wizard tool said no useful information was obtained. I did ask around the irc channels how to provide more info before downgrading but got no answer. Dario didn't take long to respond in bugs.kde.org with instructions on how to get a backtrace using gdb, but by then I was back in 4.5.4. I'll try to provide them as soon as I can take the time to do so, if no one does before me. \r\n\r\nRegarding the problems with NVIDIA 260.xx affecting the digital clock (and amarok, and ktorrent, and possibly others) in 4.5.4, these are very well documented in Bug 251719, with plenty of backtraces included. "
    author: "jadrian"
  - subject: "Nepomuk error"
    date: 2010-12-23
    body: "It's not just you. I really hate that dialog..."
    author: "mssola"
  - subject: "disabled akonadi?"
    date: 2010-12-23
    body: "where did you disable akonadi, exactly? nepomuk can be disabled in system settings, but akonadi is run-on-demand.\r\n\r\nby default the clock uses akonadi to get calendar appointment info. apparently this was not made configurable via the clock, and may be the source of akonadi being started for you.\r\n\r\nin 4.6.0 you can add a showEvents=false to the clock's configuration in plasma-desktop-appletsrc; i'll add a gui config for it in 4.7"
    author: "aseigo"
  - subject: "done."
    date: 2010-12-23
    body: "ok, that's done: i'v added the option to the config UI of the clocks for 4.7."
    author: "aseigo"
  - subject: "Unfortunately that didn't"
    date: 2010-12-23
    body: "Unfortunately that didn't solve the problem. \r\n\r\nEdit: Btw. why should akonadi ever be started when it isn't running? If it isn't running is for a reason. I don't want to be mean this time, but the fine _clock_ applet is starting a database server... dude!"
    author: "trixl."
  - subject: "I have been running KDE 4.6"
    date: 2010-12-23
    body: "I have been running KDE 4.6 Beta2 on Kubuntu for 2 weeks now. It is mostly running quite stable. My main problem is that at the moment everything is a little slower than before especially with Kmail2. Kmail looks mainly fine from a stability standpoint, but it is slow like hell. Disabelling Nepomuk helped very much to speed it up and also switching from Oxygen widget-them to plastique. After that one can work with Kmail although there are still many bugs, but none of them is criticall in the sense of data loss. I would really encourage people to test it, so that bugs can be discovered and fixed."
    author: "mark"
  - subject: "Only by curiosity for this"
    date: 2010-12-23
    body: "Only by curiosity for this 4.6 release, \r\n\"Share\" widgets option works? Since KDE SC 4.4 I never got it working, and the only demostration of it working is a developer video (before the release of that KDE version that added this feature), no tutorials, impossible to search tips/tutorials in Google due to how is called this \"Share\" menu name... and this \"Share\" menu is visible for everyone :-|\r\n\r\nAlways I wanted to share some desktop widgets between my desktop and my netbook like notes, folder views, etc... but is impossible.\r\n\r\nWhen I say that this not works I mean that it not \"share\" nothing except open a \"default\" widget."
    author: "xapi"
  - subject: "Bad planning..."
    date: 2010-12-23
    body: "That's because Nepomuk was bad planned from the beggining and has been patched and patched and patched and things don't go right.\r\n\r\nAnd because that stupid clock plasmoind is not reading the global variable that tells him that Nepomuk is dissabled, and who knows if the other applications that use Nepomuk does.\r\n\r\nSo spect another raly of patches to fix this issue.\r\n\r\nDie Nepomuk, die hard and painful."
    author: "ramsees.79@gmail.com"
  - subject: "akonadi"
    date: 2010-12-23
    body: "Ok, It's cool to hear that. However, my only problem is the noisy dialog telling me that Nepomuk is disabled: what's the point ? I know that I've disabled Nepomuk, it's not necessary to remind me this every time I log in."
    author: "mssola"
  - subject: "done"
    date: 2010-12-23
    body: "Backtraces added. Not too educated on the subject, but seems to me like the \"Search and Launch\" crash is Nepomuk related, and  the krunner crash related to the nvidia drivers."
    author: "jadrian"
  - subject: "Klipper"
    date: 2010-12-23
    body: "Is Klipper ever going to be fixed? I set a keyboard shortcut to show the list of copied items, but it doesn't respond to it. You don't want to click on the Klipper icon all the time to select from what you copied previously.\r\n\r\nOn windows there is a little utility called Ditto. Could that be ported over? That is the functionality that I'd like to see in KDE."
    author: "aiown"
  - subject: "it would be really cool if"
    date: 2010-12-24
    body: "it would be really cool if when you press ctrl-c a dialog field would open in which you can input some metadata describing what you just copied to the clipboard. in that way we could link klipper to nepomuk and when you press ctrl-v to paste, a similar dialog opens, but then you are able to input a sql query to search in the database for things you copied in the past. we could also use this to open an embedded instance of amarok playing the song in your music collection that best matches the string you are selecting."
    author: "trixl."
  - subject: "Thanks for your informed comment"
    date: 2010-12-24
    body: "However,\r\n\r\nThe clock started Akonadi /not/ nepomuk.\r\nIt is an 'on demand service' so there is no option to read (it's loaded on demand, and the clock demanded it)\r\n\r\nGiven the flaws in the gist of your argument I struggle to believe you're that aware of the initial planning stages of Nepomuk.\r\n\r\ntl;dr. Stop being an ill informed troll."
    author: "David Edmundson"
  - subject: "Replace Nepomuk for Akanodi"
    date: 2010-12-24
    body: "Replace Nepomuk for Akanodi in my comment above and still stands.\r\n\r\nBut don't change the last line of my comment, I really ment to say that."
    author: "ramsees.79@gmail.com"
  - subject: "akonadi .."
    date: 2010-12-24
    body: "ask the akonadi people; they have a habit of telling the user about things most users don't care about with dialogs. i've talked with various akonadi devs about this in the past, and while things have improved, akonadi still commits these faux pas from time to time."
    author: "aseigo"
  - subject: "of course not.."
    date: 2010-12-24
    body: "of course that didn't solve the problem for you ... it wasn't in rc1! unless you are using today's svn trunk build of kdebase-workspace, that config entry won't do anything.\r\n\r\n\"why should akonadi ever be started when it isn't running?\"\r\n\r\nit is started when something asks for something from akonadi. start-on-demand. if nothing asks for it, it doesn't start.\r\n\r\n\"but the fine _clock_ applet is starting a database server\"\r\n\r\nthat may sound stupid, but the day you start trying to build a desktop system where people expect / want features like \"see my calendar events in more than one application at a time\" it starts to make immense amounts of sense.\r\n\r\ngo look at how these kinds of things were done in KDE3 code; the limits we ran into and how poorly what did manage to be implemented worked is precisely why we now have a central system for caching and accessing this data.\r\n\r\nthe result is that the clock can now serve as a very lightweight \"my calender at an overview\" that is always there. for those who keep a calendar on their system, that becomes very useful. it's already helped me a number of times.\r\n\r\nwhether or not we can save anything meaningful by having some sort of check for whether or not akonadi has a calendar to access might be something to explore."
    author: "aseigo"
  - subject: "*sigh*"
    date: 2010-12-24
    body: "i've had the pleasure of receiving your comments on my blog and reading them elsewhere for years now, and here's the problem you chronically suffer from: you don't actually look into the design or the code before formulating opinion, and so don't actually understand it in any depth, but then form and share opinion with dramatic phrasing. \r\n\r\nbut it is impossible to formulate informed opinion without being informed. that you got nepomuk swapped for akonadi as the cause here is just so typical of your comments.\r\n\r\ni wish it were possible to pin a track record to your comments so people would know this about you before possibly taking what you write as being well-founded. :/"
    author: "aseigo"
  - subject: "share"
    date: 2010-12-24
    body: "it only works completely (as in full data synchronization) for plasmoids that use DataEngines and Services for their functionality. folderview is local access, and notes stores its text internally as well.\r\n\r\nplasmoids like the notification widget, system tray, now polaying, etc. which do use a full DataEngine-driven approach work with full data sync.\r\n\r\nthere are two ways to improve this situation:\r\n\r\na) make the plasmoids that are doing local data access use DataEngines and Services; this may be harder than it is worth in some cases, but in others is almost certainly the right way to go.\r\n\r\nb) add on to the RemoteAccess feature in libplasma so that widgets can easily send other kinds of information between the original and cloned instances. for those plasmoids where a DataEngine/Service just isn't the way to go, this might make sense."
    author: "aseigo"
  - subject: "???"
    date: 2010-12-24
    body: "Witch is extrange, being this the first time in years I put a comment in the dot and havent visited or comment in your blog in months and cause when I comment my critics are accurated, maybe not for the taste of everyone, I must admit.\r\n\r\n"
    author: "ramsees.79@gmail.com"
  - subject: "Thanks by reply Aaron, I"
    date: 2010-12-24
    body: "Thanks by reply Aaron, I wished to know \"why\" not works :)\r\n\r\nIf it only works completely for plasmoids that use DataEngines and Services for their functionality why this \"share\" menu is showed for every widget? isn't better to hide that \"Share\" menu for those widgets that aren't compatible (no DataEngines or Services)?\r\n\r\nAnyone that test that \"Share\" feature will think that KDE SC is \"half-broken\"/\"very unpolished\" :-("
    author: "xapi"
  - subject: "Wut?"
    date: 2010-12-24
    body: "Is you trying to say? I sink it helps to learn the Enlglish for kommunicating on teh Interwebz. \r\n\r\nConsider this pls.\r\n"
    author: "Mark Kretschmann"
  - subject: "Nepomuk dialog"
    date: 2010-12-24
    body: "The reason of the dialog is that users are aware that without nepomuk some functionality (e.g search) will not be available on akonadi based applications.\r\n Neverthless, I don't like that it always comes up, it should be possible to not show again, once the user sees it."
    author: "andras"
  - subject: "But you got what he wanted to express?"
    date: 2010-12-24
    body: "and it makes perfect sense.\r\nI ran into exactly the same issue. Now I know, why the folder view is not going to work remotely. You need the dataengine/service approach to pass on the data to the remote machine.\r\nI suggest to disable the settings option for plasmoids not beeing capable of \"remote display\".\r\n"
    author: "thomas"
  - subject: "ESL"
    date: 2010-12-24
    body: "Not everyone has English as their first or primary language.\r\n\r\nIt is <em>not</em> the easiest of languages to master, even for native speakers."
    author: "Thibit"
  - subject: "ok. I thought the"
    date: 2010-12-24
    body: "ok. I thought the config-option was there and you just added the field to the config-dialog. \r\n\r\nBtw. this \"on-demand\"-thing doesn't work, and this is the proof of it. If even the smallest programs are hardcoded with akonadi, it will always be running from the moment you start the computer. \r\n\r\nIt'd be more consistent if the application checks whether akonadi is running or not, and only use akonadi if it is running. In this way we could have a global option to disable it and both parties are happy. \r\n"
    author: "trixl."
  - subject: ">Not everyone has English as"
    date: 2010-12-24
    body: ">Not everyone has English as their first or primary language.\r\n\r\nCorrect.\r\n\r\n>It is not the easiest of languages to master, even for native speakers.\r\n\r\nEnglish is the easiest natural language for people in the western world. "
    author: "trixl."
  - subject: "No,"
    date: 2010-12-24
    body: "I did not. Now I did, thanks to your explanation.\r\n\r\nThanks :)\r\n"
    author: "Mark Kretschmann"
  - subject: "English is the easiest"
    date: 2010-12-24
    body: "<cite>English is the easiest natural language for people in the western world.<cite>\r\n\r\nNot at all. If your native language is Italian, you'll have an easier time learning other Latin languages such as Portuguese and Spanish. If you're Polish you'll have an easier time with other Slavic languages such as Russian. And if you're Swedish, then Danish is pretty simple. And if you don't take into account past experience, why would English be the easiest language in any case? "
    author: "jadrian"
  - subject: "Akonadi"
    date: 2010-12-24
    body: "Tixl: It doesnt look that you know something from computer design, and why it's a good thing to do things someway.\r\nIts nice from Aaron to answer your post anyways, because it reads like a useless rant..\r\n\r\nA clock starting an akonadi server ? the point that you dont get the purpose of it, is funny. As your post wants to suggest: \"Why does a little thing like a clock starts biiiiig Database like akonadi\", is so childish.\r\n\r\nI wont get down to the arguments, as you also didnt. \r\n\r\nKdenews sometimes gets to a rant site, where uninformed or better misinformed people post rant's as facts, and because of the publicity the site has, the devs sometimes need to explain and set things right.\r\n\r\n:/ thx for the everyone from kde that still answers to get the picture right.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"
    author: "wintersommer"
  - subject: "Of course languages that are"
    date: 2010-12-24
    body: "Of course languages that are related are easier to learn. But I still think that English is the easiest natural language. It has its exceptions here and there but the conjugations are a as easy as they get. Try learning German, French or Finnish and you'll see what I mean. People spend _years_ learning them and still cannot write a paragraph without massive errors."
    author: "trixl."
  - subject: "you wrote a very long comment"
    date: 2010-12-24
    body: "you wrote a long comment that doesn't say anything at all. Is that also your coding style?"
    author: "trixl."
  - subject: "Now that is just"
    date: 2010-12-24
    body: "Now that is just unacceptable. Any more of such comments (from someone who only whines) towards a very hardworking developer, who's well known for fixing thing way outward the realm of what he's supposed to do and I'll delete any more comments you make on the dot including blocking your account.\r\n\r\nI get why you're frustrated, and frankly, I'm far from happy with the situation myself. And so are many. But will is one of those people who goes out of his way to fix things wherever he can - insulting him is just the wrong thing to do - especially by someone who doesn't do anything. Remember, FOSS is a meritocracy - if you do something useful, you have a right to talk. If you're an user - sure you can ask questions or give comments and we'll try to help but realize we don't owe you anything, we're volunteers here."
    author: "jospoortvliet"
  - subject: "Thank you for your"
    date: 2010-12-24
    body: "Thank you for your understanding."
    author: "trixl."
  - subject: "English is the most natural"
    date: 2010-12-25
    body: "English is the most natural language for people who are from English speaking countries and it's definitely not the easiest language to learn. Pronouncing \"th\" for example is not natural for people that are not from English speaking countries. Germans even pronounce v like f when speaking English. Kn like in knife or knowledge is also one of those things, just to name two examples.\r\n\r\nI have learned to be a lot more tolerant with people that speak bad English since I learned German. I also make a lot more mistakes in English since then and German isn't that difficult after all. The pronunciation of words is strait forward (after mastering z like in ziemlich and ch like in auch). The other thing that is nice about German is the vocabulary. You hear the meaning of the most words within the word itself unlike English. The grammar is a bit difficult but there are rules for almost everything in German.  \r\nYour view is interesting but  I have to agree with Jadrian's argument. "
    author: "Bobby"
  - subject: "Why 2 database-servers?"
    date: 2010-12-25
    body: "Using a database is fine and a necessity of a modern desktop.\r\n\r\nBut why does kde use two of them? I see mysql and virtuoso in my current session.\r\n\r\nI think KDE should settle on one \"system-database\" and all of KDE should use that one."
    author: "yglodt"
  - subject: "Very slow compositing (nvidia 9800M GTX, 260.x drivers)"
    date: 2010-12-26
    body: "Thanks to the KDE team for the great work !\r\n\r\nA couple of little issues though :\r\n\r\n1) In my case, display is very jerky. It used to be almost completely smooth with KDE 4.5.4 (nvidia 260.x proprietary drivers, kubuntu, blur disabled).\r\n\r\n2) some settings seem not to remain ; for instance, the GTK theme is reset everytime (none selected after each reboot). The fonts size was reset as well at my 2nd reboot."
    author: "mahen"
  - subject: "Aside from Akonadi not being"
    date: 2010-12-25
    body: "Aside from Akonadi not being a KDE program and thus not having easy access to KDE settings, there is also a technological reason for that.\r\n\r\nSome developers have actually tested using Virtuoso as an Akonadi database backend (by manual configuration) and found that its SQL interface and/or the Qt plugin for using that are not yet capable enough for this use case.\r\n\r\nHowever I am quite sure that this will be monitored and re-evaluated every now and then to make this an option once it becomes viable."
    author: "Anda Skoa"
  - subject: "Nepomuk needs sparcl"
    date: 2010-12-26
    body: "cause akonadi needs sql and nepomuk needs sparcl. Atm Virtuoso seems to be the only db with half-acceptable performance for nepomuk. In fact there's not much choice anyway... All these sparcl dbs are still quite new and (hopefully) have still some room for optimizations. Bit of a weight cause KDE is not developping the db but still so dependant on the performance of Virtuoso (at least for the interesting semantic stuff)"
    author: "thomas"
  - subject: "Live CDs"
    date: 2010-12-26
    body: "Are there Live CDs available of RC1?"
    author: "vinum"
  - subject: "Compositing broken on Kubuntu"
    date: 2010-12-27
    body: "Since 10.10, compositing is broken for me on Kubuntu/amd64 with the radeon r600 driver.\r\n\r\nI get this in the xsession-errors log:\r\n\r\nkwin() KWin::Workspace::setupCompositing: Initializing OpenGL compositing\r\nkwin() KWin::Workspace::setupCompositing: KWin has detected that your OpenGL library is unsafe to use, falling back to XRender.\r\nkwin(): Failed to initialize compositing, compositing disabled\r\n\r\nAnyone else suffering from this?\r\n\r\nRelated launchpad issues:\r\nhttps://bugs.launchpad.net/kubuntu-ppa/+bug/619578\r\nhttps://bugs.launchpad.net/ubuntu/+bug/647467\r\n"
    author: "yglodt"
  - subject: "And not to forget"
    date: 2010-12-28
    body: "And, not to forget, what Aaron describes is something I\u2019ve wished to have in KDE for ages, and now it comes. And not just as a local hack, allowing the clock to access the calendar, but as a desktop-wide way to share data among programs! \r\n\r\nThat\u2019s so damn cool, it\u2019s hard to understate what this means for the desktop. \r\n\r\nTeaching my clock to tell me of important birthdays, directly offering me a birthday-mail option and also telling me what I wrote next time and what kind of mail the other one wrote to me at my birthday. Add to that the information how I can best reach the other one. \r\n\r\nAnd now go one step further: If I received a file from the same one and I\u2019m just reading it, the file could tell me when I look at the metadata, that today is the birthday of the author. \r\n\r\nAnd if I listen to a song and it\u2019s the birthday of the artist and the artist has a wishlist of presents from fans put up as part of some contact-information (or just a donation or flatter link), this could show, too. \r\n\r\nYes, this isn\u2019t available right now, but it can now be done, and be done in a desktop agnostic way, so that even if I should completely switch to emacs as desktop some day, someone could write an akonadi-backend for emacs and still access the same information. And keep it in sync even when I frequently switch between emacs desktop and KDE. And also sync all that with my phone. \r\n\r\nIt\u2019s a backend which can unite all free desktops and make personal information management enjoyable, putting away with losing data or painful migration and synchronizing when you switch programs. And if that isn\u2019t exciting, I don\u2019t know what else is!"
    author: "ArneBab"
  - subject: "I\u2019d say spanish is definitely"
    date: 2010-12-28
    body: "I\u2019d say spanish is definitely easier for germans to master, even though german and english are closely related. \r\n\r\nI say to master, because \u201cI want have pizza\u201d is barely broken english. \r\n\r\nI might speak and write english and not really speak good spanish, but that is because I\u2019ve been reading english fiction since 7th grade. \r\n\r\nBut anyway: If you want an easy to learn language for international communication, take Esperanto. That works, sounds nice (subjectively), has been used in international conferences for more than 100 years, is phonetic (every letter has exactly one phonem) and is very easy to learn \u2014 having only 18 grammar articles. And it was designed to make international communication easy and has been honed for exactly that task by Esperantists all around the globe for a century, now. And it is neutral ground for everyone. \r\n\r\nAnd don\u2019t get me started on the phonetics in english. Think about what you read today and read yesterday, then free yourself of the false assumption that english is easy and tread a bit softer. \r\n\r\nPS: And I still prefer reading a book in the original language, if I speak it fluently, as is the case for english. "
    author: "ArneBab"
  - subject: "Kubuntu compositing"
    date: 2010-12-28
    body: "some snazzy Kwin features are not supported by some video drivers (in my case, Intel 965GM).\r\n\r\nHere are some posts about the issue. Not sure if this applies to your video setup...\r\nhttp://blog.martin-graesslin.com/blog/2010/07/blacklisting-drivers-for-some-kwin-effects/\r\nhttp://kubuntuforums.net/forums/index.php?topic=3114107.0\r\n\r\nglxinfo may be needed to determine the proper driver name. It's part of the mesa-utils package."
    author: "kallecarl"
  - subject: "Whoops!"
    date: 2010-12-28
    body: "Sorry folks, that was supposed to be on my TODO list for 4.6, but looks like I forgot to write it down :-(  Thanks Aaron for fixing it."
    author: "odysseus"
  - subject: "That's amazing indeed. \nWe"
    date: 2010-12-28
    body: "That's amazing indeed. \r\n\r\nWe could also drop the cron and at daemons completely and let the clock applet read the cron-table and atjobs-file. \r\n\r\nKmail has to check for new emails by itself. That's some pretty horrible design. Let's make the clock applet call kmail on regular intervals!\r\n\r\nEven better, we could drop the whole scheduler from the Linux kernel and let the clock applet take care of that. Imagine that!!!\r\n"
    author: "trixl.."
---
Right before Christmas, KDE has <a href="http://kde.org/announcements/announce-4.6-rc1.php">published</a> the first candidate for the upcoming release of KDE Platform, Plasma and Applications 4.6. The focus at this stage is on fixing bugs and completing translations and artwork. As such, the rework of the Oxygen icon set is nearing completion, many bugs reported by testers in recent weeks have been fixed and stabilization is still in full swing.

KDE's release team has decided to not include the new release of KDE's PIM suite, containing the Kontact groupware client and the new KMail 2 which is based on the Akonadi groupware cache in this release due to open issues with migration of large sets of data from the traditional client. These bugs are being worked on as we speak, but the risk at this point is too high to include the new Kontact in the 4.6.0 release. Therefore, it has been decided to release KMail 2 and its companions together with one of the subsequent 4.6 releases, likely 4.6.1 one month later. A <a href="http://www.kdedevelopers.org/node/4365">3rd beta version of the KDE-PIM module is available</a> already.

As the other components of the frameworks, workspaces and applications are nearing release date, KDE encourages testers to give their favorite software some final thorough testing. The last chance for feedback will be the second release candidate, planned for 5th January 2011. The final release of 4.6.0 will be available on January, 26th 2011.