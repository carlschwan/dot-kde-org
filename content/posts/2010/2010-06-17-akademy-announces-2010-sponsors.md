---
title: "Akademy Announces 2010 Sponsors"
date:    2010-06-17
authors:
  - "jospoortvliet"
slug:    akademy-announces-2010-sponsors
---

From July 3rd through July 10th the KDE community will gather in Tampere, Finland for our yearly Akademy conference. We are pleased to announce that, once again, a number of sponsors will be helping to make this event possible.

<h2>Platinum Sponsor</h2>
<p style="color:#2e3436;text-align:left"><a href="http://nokia.com/" style="color:#396273;text-align:left;text-decoration:none"><img alt="Nokia Logo" src="http://dot.kde.org/sites/dot.kde.org/files/Nokia_Logo.png" style="border-color:initial;border-style:initial;color:#2e3436;float:right;margin-left:5px;margin-right:5px;text-align:left"></a></p>
<a href="http://nokia.com">Nokia's</a> <a href="http://qt.nokia.com">Qt</a> and <a href="http://meego.com">MeeGo Devices</a> divisions have teamed up to be the platinum sponsor of Akademy 2010. Qt has a long history of supporting KDE conferences, stretching back to the very first Akademy event. Qt is an advanced toolkit for building rich, touch enabled user interfaces and applications for cross platform deployment on desktop, mobile and embedded devices. Qt allows developers to code less, create more and deploy everywhere. Formerly known as Trolltech, Qt has always been close to the KDE community and with the opening up of their development our cooperation has risen to a new level. 

MeeGo Devices, formerly working on the mobile interface Maemo, builds upon Qt technology to create stunning applications for the next generation of mobile devices. The KDE community already works with the MeeGo community and we're looking forward to more cooperation.

<h2>Gold Sponsor</h2>

<p style="color:#2e3436;text-align:left"><a href="http://oss.intel.com/" style="color:#396273;text-align:left;text-decoration:none"><img alt="Intel Logo" height="132" width="200" src="http://dot.kde.org/sites/dot.kde.org/files/intel.png" style="border-color:initial;border-style:initial;color:#2e3436;float:right;margin-left:5px;margin-right:5px;float:right;text-align:left" width="267"></a></p>As gold sponsor this year we have <a href="http://oss.intel.com/">Intel</a>, a leading microprocessor company working on innovative technology around the globe. Intel has a strong history of working closely with Free Software communities. It became the first major graphics chip vendor to provide free drivers for modern graphics hardware in 2006. Intel is also well known for founding the Moblin project before handing over control to the wider community via the Linux Foundation. The merging of Moblin and Maemo to create MeeGo seems set to bring closer collaboration between Intel and KDE around this project.

<h2>Silver Sponsors</h2>

<p style="color:#2e3436;text-align:left"><img alt="Silver sponsor logos" height="178" width="340" src="http://dot.kde.org/sites/dot.kde.org/files/silvers_1.png" style="border-color:initial;border-style:initial;color:#2e3436;float:right;margin-left:5px;margin-right:5px;float:right;text-align:left" width="267"></p>Our silver sponsors include:

<a href="http://www.basyskom.de/index.pl/enhome">BasysKom</a>, a certified Qt consultancy company working with KDE technology to fulfill customer requirements.

<a href="http://www.canonical.com">Canonical</a>, commited to supporting the open source ecosystem, develops Kubuntu, a KDE based Linux distribution.

<a href="http://www.collabora.co.uk/">Collabora</a>, an international open source consultancy providing architectural, management, infrastructure and community support to companies around the world and employing several KDE developers.

<a href="http://www.google.com/">Google</a>, whose innovative search technology is known to everyone. Google supports many Free Software communities including KDE.

<a href="http://www.ixonos.com/en">Ixonos</a>, an ICT services company focusing on mobile, social and digital services with a responsible attitude.

<a href="http://en.opensuse.org/">openSUSE</a>, the Novell sponsored community Linux distribution, well known for its commitment to KDE software as the basis for its user friendly free Linux desktop and server experience.

<h2>Media Partners</h2>

<p style="color:#2e3436;text-align:left;float:right;"><img alt="Media sponsor logos" height="68" width="309" src="http://dot.kde.org/sites/dot.kde.org/files/media.png" style="border-color:initial;border-style:initial;color:#2e3436;margin-left:5px;margin-right:5px;text-align:left; float:right;" width="240"></p>Our Media Partners this year are <a href="http://www.linux-magazine.com/">Linux Magazine</a> and <a href="http://www.ubuntu-user.com/">Ubuntu User</a>. Linux Magazine is a monthly magazine available in over 50 countries in English, German, Spanish and Polish. Ubuntu User is the first print magazine specifically for Ubuntu's rapidly growing audience. Both magazines will have reporters at Akademy, providing in-depth and timely updates on what is going on.