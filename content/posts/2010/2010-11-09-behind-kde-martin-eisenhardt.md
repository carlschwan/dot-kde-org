---
title: "Behind KDE: Martin Eisenhardt"
date:    2010-11-09
authors:
  - "tomalbers"
slug:    behind-kde-martin-eisenhardt
---
Ever wondered who is behind anonsvn.kde.org? Want to know what drives a person to provide that service to KDE for years and years? Martin Eisenhardt will tell you in a Behind KDE interview this week. Even now he is setting up one of the first anongit.kde.org mirrors, which will be live later this week! Read the <a href="http://www.behindkde.org/node/889">interview at Behind KDE</a>.
<!--break-->
