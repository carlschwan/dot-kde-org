---
title: "Dirk Hohndel  at Akademy"
date:    2010-07-18
authors:
  - "jospoortvliet"
slug:    dirk-hohndel-akademy
comments:
  - subject: "small issues getting sorted out"
    date: 2010-07-20
    body: "> There is the lock & shutdown widget but it doesn't have a suspend \r\n> button\r\n\r\nit's been there since 4.4. :)\r\n\r\n> Or take KNetworkManager, its integration is weird.\r\n\r\nagreed; which is why there has been work on the network manager plasmoid which hopefully will be accompanying 4.5 on many distributions."
    author: "aseigo"
  - subject: "core team"
    date: 2010-07-21
    body: "A (b0rken) network manager client in 4.4 ???\r\n\r\nWhat KDE needs is a core team that should work on items that are of core requirements to users. Things like network management, Instant Messaging, Browser, CalDav et cetera should be handled with higher priority by this core team.\r\n\r\nWhere as special teams with special focus areas can work and deliver in their own flexible sweet time.\r\n* Nepomuk\r\n* Akonadi\r\n\r\n\r\nThis way, you don't lose users because you are working for the next gen technology but the yesterday technology is broken.\r\nSomething like the Linux kernel itself: Scales on 4096 node cluster but just crawls miserably on doing a \"cp -a foo/ bar/\" where foo/ is 200GiB in size."
    author: "rrs"
  - subject: "Suspend button"
    date: 2010-07-21
    body: "Darn, I didn't know that had been added. Must have a stern word with myself about not communicating new features properly ;-)\r\n\r\nThe NM plasmoid and even the KNetworkManager app are not bad now - and that's on 4.4, unless there's some backporting been going on."
    author: "Stuart Jarvis"
  - subject: "Re: core team"
    date: 2010-07-21
    body: "Unfortunately it doesn't work like that because all the things in your priority list depend on Nepomuk and Akonadi. Improving those items depends on improving their integration with Nepomuk and Akonadi. A separate 'core team' would have to be working on a temporary fork, implementing hacks if they could manage to do anything useful at all. I think it would most likely be a waste of time and money.\r\n\r\nYou comment about 'cp' is wrong. The speed of the Linux 'cp' command is limited by the devices on which the data is held, not Linux itself. A 'core team' to improve 'cp' wouldn't be much use there either.\r\n\r\nSorry to sound a bit depressing but advancing the state of the art in desktop software will inevitably involve some pain, delays and risk. As these things go, I would say the KDE community is doing pretty well. In comparison, very large companies like IBM, Apple and Microsoft have had similarly ambitious plans just fail completely in the past (remember Taligent or Microsoft's Cairo?)."
    author: "Richard Dale"
---
At <a href="http://akademy2010.kde.org">Akademy</a> in Tampere we interviewed <a href="http://www.hohndel.org/communitymatters/about/">Dirk Hohndel</a>, Chief Linux and Open Source Technologist (we would call him 'dude') at Intel. He was present representing Intel and checking out what the KDE community is up to. As he sacrificed spending the <a href="http://en.wikipedia.org/wiki/Independence_Day_(United_States)">4th of July</a> with his family for this, we were anxious to talk to him. Sunday, after the Elegant keynote by Aaron Seigo, we managed to catch him for a chat and first asked him what he thought about the keynote.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey; width: 160px;"><a href="http://www.flickr.com/photos/x180/2696726230/"><img src="http://dot.kde.org/sites/dot.kde.org/files/Dirk.jpg" width="160" height="240" ></a><br/>Dirk Hohndel (by <a href="http://www.flickr.com/photos/x180/">duncandavidson</a>)</div>
<b>Dirk H</b>: What I like about the keynote is that Aaron did a great job at capturing what is great about KDE and describing the challenges, then bringing these two together. To me that is a big part of being a successful community - to honestly look at where you are and turn it into a vision of where you want to go.

Moreover, I love the idea of taking a group of people who attack the issues they see in small increments, that is how you get things done. You can't do revolution, revolution, revolution. If you keep breaking things you start to lose people at some point! So the idea to find a focus on one or two clear areas for each release is a very good one. It brings clarity and thus helps decision making.

<h2>Who is Dirk?</h2>

<b>Dot</b>: Agreed, Aaron presented us with a great analysis of where we are and where we should be going. So, now it's time for you to introduce yourself. You've been around in Free Software, haven't you?

<b>Dirk H</b>: Sure, for quite a while. I joined Linux kernel development back in the early days, I was actually one of the first kernel developers, and later worked for SUSE Linux, where I was the CTO before I left. For the last 9 years I have been at Intel, a very interesting company. It is big, which always provides a challenge in any role. For a person who really embraces the spirit and ideas of open source it is phenomenal because the advantage of having this mass and these resources behind you is enormous. You can really make a lot happen, projects like MeeGo show that clearly. There is a balance in the pain working for a huge company versus the opportunities which it gives to me to work on the things I care about deeply. All in all, I'm a very happy person in this job.

<h2>Happiness in work</h2>

<b>Dot</b>: Well, that's how it should be, right?

<b>Dirk H</b>: Absolutely. Fundamentally, if your job doesn't provide you with at least an acceptable degree of happiness you need to look into changing your job or finding another one. You spend so much time working, you MUST draw positive energy from it or you're wasting your life...

And I think this is relevant for the KDE community. You can look at what you do, as an individual contributor, in a variety of ways. It often is a labor of love, but for many it is also a path towards employment. I mean, what is better than being able to do what you love to do full-time? Especially in a community like KDE with so many young people and newcomers. Aaron asked how many were at their first Akademy, that must have been at least a third of the visitors! They should realize there are a lot of opportunities in this space, not only for fun but also to have a career and do this for a living.

The Linux kernel is a great example - 80% of the work is done by developers who are paid to do what they do. And as large companies embrace Linux, not just on servers but also on clients, we will see companies create a lot of jobs for developers working on KDE software - yes, Intel is hiring and I know we're not the only company doing that. I think it's a great development. Many FOSS projects suffer from disappearing developers who go into unrelated jobs, they grow up, have families and they disappear. The communities are losing knowledge and manpower. So having more companies hiring KDE developers to work on KDE (related) things is good for the product and the community.

Because in our industry, and I'm talking about companies working in and with open source, you work on what you're supposed to on paper. But in reality you often have much more influence than in other software development companies, you can bring work and your community project together. We don't do code monkeys - we want people who take a pro-active stance, have deep knowledge and opinions and can do more than just execute what architects came up with. We look for developers who can help us shape what we do - which makes developer roles in FOSS companies much more interesting.

That's for me what fascinated me about open source. My first job was working on proprietary software. I moved from there to SUSE because it allowed me to do what I liked, as my day job and make a difference within SUSE. I worked on x86 support and the Linux kernel, and had great fun. And while the open source community has become much larger, as have the companies involved, the fundamental opportunity is still the same.

<b>Dot</b>: Yep, we hear this from pretty much all developers we talk with. So how would you say does this compare to the early computer era which lead to all these billionaires?

<b>Dirk H</b>: Ha. Well, there is a big difference between being a billionaire and having a satisfactory life. A lot of research has been done in the area of happiness. Almost everybody thinks that if they only had 30% more income, they would be much happier. And it doesn't matter how much they make in absolute terms... Except for people in very poor living conditions, money doesn't make you happy. Money simply has a break even point. You need a certain amount of money to allow you the lifestyle you need to be happy, to let you focus on having a fulfilling life. So if you're smart you stay at a job which gives you that amount and don't move for the money; likewise, always choose a more fulfilling job over one which pays more money. I am sure the ratio of people finding satisfaction as a FOSS developer is bigger than for people doing proprietary software - despite FOSS leading to less millionaires. Only 1 out of a 10.000 gets rich in proprietary software. In our world, pretty much anyone succeeds and can create something which gives him or her a positive experience in life. I'm having fun because the people working on software in Intel not only embrace but also truly understand open source and what it brings!

<h2>KDE involvement</h2>

<b>Dot</b>: So what has been your involvement in the KDE community?

<b>Dirk H</b>: I know a lot of the early KDE community members like Kalle and Matthias. I used KDE software from the start to the 2.0 release, and lost contact for a while. I'm interested in the Linux client so since  a couple of years I'm trying to figure out where it is going. I follow both Gnome and KDE development, see what is going on in the open source world. I've been repeatedly trying the latest KDE Plasma Desktop (which still fails for me for a number of reasons). The KDE community interestingly has undergone a lot of changes - very much unlike the Linux kernel where the 'oldtimers' are still very much involved. Seriously, of the 20 initial Linux developers, 15 are still very active. I'm unsure what this difference means for KDE, I don't want to judge either. I know it is a very welcoming community, which is growing fast. But it is also important to retain knowledge - you have to find a balance between embracing innovation and keeping the old guys and girls at hand...

<b>Dot</b>: So how would you say the current Free Desktop is doing?

<b>Dirk H</b>: Well, all projects have their ups and downs and KDE is in a very good place. I loved the keynote, where Aaron said we should focus on user experience, elegance, consistency. What is stopping me to use it is as much the little details as it is habits I have - it is not the technology per se. And to be fair and honest, whenever I switch to a KDE based set of apps I do it on Fedora. I have customized my Gnome desktop for the last 6 years and in the Plasma Desktop in Fedora I get the default configuration. So the hard part is that many things I expect to work just don't. I haven't figured out how to use a hotkey to suspend my machine, for example. You have to go through menus. There is the lock & shutdown widget but it doesn't have a suspend button - what were you guys smoking while you wrote it? Or take KNetworkManager, its integration is weird. And if I have a panel scaled to have two rows of buttons for my apps, the other icons and the clock get HUGE. And there was some more stuff driving me nuts, I don't remember. After a week I still hadn't gotten things to work as I liked it so I gave up. I'll try it again, but for now - not my cup of tea.

<b>Dot</b>: You don't use KDE software but you sponsor our main conference?

<b>Dirk H</b>: Aah, but that's different. That is about the developers and the project in general - we want KDE to make progress, and conferences are a great way of making that happen so we sponsor them. So it's nice to see what you guys have been doing - especially on an infrastructural level - gives me something nice and tangible to show for now. But to me this conference is about getting people together to talk, discuss and innovate.

<b>Dot</b>: Thank you for the interview, it was a fascinating chat! Enjoy the rest of the day...