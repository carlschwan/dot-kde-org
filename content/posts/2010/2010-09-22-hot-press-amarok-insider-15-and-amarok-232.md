---
title: "Hot Off The Press: Amarok Insider 15 and Amarok 2.3.2"
date:    2010-09-22
authors:
  - "valoriez"
slug:    hot-press-amarok-insider-15-and-amarok-232
comments:
  - subject: "Link to Amarok 2.3.2"
    date: 2010-09-22
    body: "Correct link is http://amarok.kde.org/en/releases/2.3.2\r\n\r\nThanks!"
    author: "valoriez"
---
<a href="http://amarok.kde.org/en/Insider/Issue_15">Amarok Insider 15</a> available now, includes the articles:
<img style="float:right; border: 1px solid grey; padding 5px;" src="http://dot.kde.org/sites/dot.kde.org/files/splash_2_3_2.jpeg" /><ul>
<li>What's New in Amarok</li>
<li>Interview with a Developer: Leo Franchi</li>
<li>Podcasts on your Mobile Device</li>
<li>Automated Playlist Generator</li>
<li>Weekly Windows Build Now Available</li>
<li>Organizing a Music Collection</li>
</ul>

The <em>Insider</em> explores Amarok for the user, looking at features, development, and how to  
get involved with the project.

<a href="http://amarok.kde.org/en/releases/2.3.2">Amarok 2.3.2</a> has also been released, with many bugfixes and improvements, led by our new Automated Playlist Generator, two new applets: Similar Artists and Upcoming Events, an upgrade to Dynamic Collections, and improvements in the collection browser, Cover fetching, file browser, podcasts and importing  
labels.

Finally, we have a <a href="http://amarok.kde.org/videos">new video tutorial page</a>. See our new features and how to use them. Discover your music!
<!--break-->
