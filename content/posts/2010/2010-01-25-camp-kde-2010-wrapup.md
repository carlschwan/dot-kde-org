---
title: "Camp KDE 2010 Wrapup"
date:    2010-01-25
authors:
  - "troy"
slug:    camp-kde-2010-wrapup
---
Last week, the KDE Community had their yearly Americas event, this year in sunny San Diego. Despite California not living up to its sunny reputation, the attendees certainly had a good time. The first three days featured talks about a variety of topics (<a href="http://dot.kde.org/2010/01/17/day-one-camp-kde-2010">day 1</a>, <a href="http://dot.kde.org/2010/01/21/camp-kde-2010-continues-more-talks">day 2</a> and <a href="http://dot.kde.org/2010/01/21/camp-kde-day-three-technical-talks-summaries">day 3</a>), there were CMake and Qt development courses and of course several small meetings and work to be done. However there was more than sitting in the conference room at UCSD. We had a great time at Banana Bungalows on the beach, went out for a variety of food, had a few dragons and babies visit us and risked our lives getting to and from the university. Read on for some general impressions on the event, and for some motivation to attend Camp KDE 2011 next January, at a location still to be determined.
<!--break-->
<h2>Accommodations</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/TheView.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/TheView_small.jpeg" width="300" height="225"/></a><br />Enjoying the view from the Banana Bungalow deck</div>

KDE events usually choose a hostel for primary accommodations as it provides close social interaction at relatively small expense. The hostel chosen this year was Banana Bungalow, on the beach of the Pacific Coast, with a few people staying at the nearby Best Western. The location was good for watching the waves, and reasonably cheap. The hostel was a decent place until it started to rain.

The place was run by longer term residents of the hostel, and always had people walking around and having fun, mingling with the KDE crowd. We filled the big picnic table downstairs with laptops and saturated the hostel's wireless, something that typically happens anywhere that KDE people congregate. Of course, the stay was made more interesting by some local beverages.

All was fine at the hostel until the heavy rain started on Monday. It rained so much power went down, the streets were flooded to two metres, and there was a real river flowing inside the hostel. Despite the dire circumstances, spirits were still high, and once again showed how the morale within the KDE community is something that cannot be destroyed! Fortunately, unlike the other visitors at the hostel, we were able to sneak away to the university during the day to enjoy warmth, wifi and food.

<h2>Conference Location</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/qttraining.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/qttraining_small.jpg" width="300" height="225"/></a><br />Till Adam trains without shoes</div>

The main event was held at the Computer Science and Engineering building at University of California at San Diego (UCSD), courtesy of their Computer Science department. The talks were relatively well attended, and an increase over the previous year to around 70 people. During the first three days of talks, there were a number of students from UCSD that managed to make it to the presentations, despite it being a long weekend in the U.S. Many of the talks ran slightly behind schedule, with an average of one hour delay each day. Next time Camp KDE runs, we will arrange for someone to remind the speakers of remaining time using flashcards or similar.

UCSD was a good location for the event, providing ample electricity and wireless access for the attendees. The event, however, did highlight the not-quite-there-yet nature of wireless support for many chipsets in Linux, as a few people were relegated to wired access. When the rain got really bad, many of the attendees stayed at UCSD late into the night to take advantage of the additional shelter provided over the hostel. The main disadvantage of the location was the distance between it and the accommodations - having the accommodations at the event location were considered one of the main advantages to the Camp KDE 2009 event in Jamaica.

<h2>Food</h2>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/feedinghackers.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/feedinghackers_small.jpg" width="300" height="225"/></a><br />This pizza did not have long to live</div>

San Diego certainly had plenty of food available for general consumption, much of which was very good. For the most part, the attendees arranged for their own food, although snacks were provided by the organizing team for consumption at UCSD. Generally speaking, people gravitated towards Mexican food, being only a few miles from Mexico. Bean burritos and fish tacos were consumed in mass quantities, but the food court at UCSD provided opportunity for some additional variety like Chinese, Greek, Italian and Thai food.

In the evenings, the attendees would often migrate to a restaurant or bar near the hostel to find some local fare. Once bored of Mexican food, people began to find pizza, Thai, and more to satisfy their need for energy. Along with the food, a few local beers and other liquor were sampled, occasionally resulting in bouts of karaoke upon return to the hostel. There are a number of recordings of KDE contributors crooning that may or may not be used as future blackmail.

On the final day of the event, those still remaining went for a more formal dinner at the Mexican restaurant El Torito, near the UCSD campus. This was the last significant gathering of the event, and resulted in a number of very lively discussions. Among the topics discussed were what to do for Camp KDE 2011, and how things can be improved for following years. In general, the event was considered a productive, successful event by those who attended.

<h2>Social Event</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/ofcourse.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ofcourse_small.jpg" width="300" height="225"/></a><br />What is powering their brewery?</div>

On Tuesday we had the scheduled social event. The weather forced some small changes in plans. We ended up going on a tour of a local brewery, the Stone Brewery, a medium size craft beer maker. We learned a lot about making beer, the origins of Ale and Lager and of course sampled a few different beers. The brewery was fairly technical in their details, showing us their touch screen user interface they used to monitor the brewing process. The consensus was that their interface would benefit from a few Plasma animation classes.

After the tour, we stayed at the attached restaurant for some delicious food, and watched the rain. The original plan for the afternoon had been to go to an outdoor location, such as the renown San Diego Zoo, but this was canceled due to the rain. We ended up splitting into several smaller groups, with one group heading to Tijuana, another going back to UCSD, and a third making a trip to Fry's. For the people from outside the U.S., the visit to Fry's was rather entertaining.

At the end of the day, KDE hacking had taken over, with most people congregating at the conference room at UCSD. Tables were thrown together, and the urge to hack on things could not be denied. Many remained at UCSD until midnight, to avoid the rain.

<h2>Dragons Galore!</h2>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/two_dragons.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/two_dragons_small.jpg" width="300" height="225"/></a><br />Konqi and Katie return!</div>

During the KDE 4.0 Release Event in Mountainview two years ago, local KDE enthusiasts decided that they would put together professionally designed costumes for KDE's mascots, Konqi and Katie. This appearance was quite well received, and since KDE was returning to California for Camp KDE, it afforded Aaron Reichmann, one of the two original people beneath the costumes, an opportunity to dust off the costumes once again.

This year, the original Katie was not available, so Plasma hacker Chani Armitage donned the Katie costume. Between her and Aaron, the dragons made two appearances to lighten the mood of the technical talks. Who knows where they will be seen again?

<h2>A Family Affair</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/babies.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/babies_small.jpeg" width="300" height="225"/></a><br />Our youngest attendees</div>

Perhaps as a sign of the maturing KDE community, more contributors are spawning new child processes. At this event, we had two infants in attendance, and several more attendees that left their children at home. Wade Olson (Camp KDE 2009 organizer) made a surprise appearance, with baby in tow, to supplant Kitware hacker Marcus Hanwell's child as the youngest conference attendee. According to Wade, spawning children has become a contagious event within the KDE community.

This is not the first time that children have attended a KDE event, and this is beginning to become a regular occurrence, however having two very young babies was interesting, especially when they would randomly cheer during a presentation. Overall, they were well behaved, and part of the positive atmosphere at the event.

<h2>Productivity</h2>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/MarcusPlotting.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/MarcusPlotting_small.jpg" width="300" height="225"/></a><br />Marcus distracts us from the rum</div>

Many of the benefits of a KDE event are of the social nature. Bringing KDE people together helps brainstorming and interaction in ways that email or IRC simply cannot match, so while much of the time spent during such a meeting is of the social variety, the practical benefits to the KDE community extend well beyond the conclusion of the event. But it isn't just meetings within the KDE community that occur. These events present the opportunity to meet many local Open Source personalities, such as the keynote speaker Philip Bourne or many of the UCSD students. A number of KDE people managed to arrange a meeting with a major company in the area to talk about KDE and embedded platforms. These meetings are far more useful in person than over email.

Additionally, having so many experienced hackers in the same room is good for code-related productivity. Most evenings, even if involving food or drink, also involved a significant amount of hacking. The Qt training provided by Till Adam of KDAB was invaluable to all attendees, rookies and experts alike. The Qt/Embedded training opened up a lot of eyes about the nature of embedded platforms, and how to potentially unlock the power of Qt/Embedded for KDE programmers. And Marcus's CMake training was useful to anyone that has ever tried to use the KDE build system.

While any of these training sessions were going on, a number of people were furiously coding on a number of projects. Amarok and Plasma, in particular, had a strong showing, and the immediate benefit of having these people in the same room was obvious to any outside observer.

James Cain won the Camp KDE Be Free contest, and was flown out to the event to participate. Coming from a mostly outside perspective, his experiences with the KDE community at the event have already begun to turn him into a significant contributor (his first dot story was posted already!). If Camp KDE has this affect on people, then perhaps it should be marketed as a way to get involved with the community, for future events.

<h2>Concluding Remarks</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/rainystreets.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/rainystreets_small.jpg" width="300" height="225"/></a><br />The streets were flooded</div>

Camp KDE 2010, the second installment of this annual event, was a huge success. It showed growth over the previous year, and solidified the event as KDE's second major annual event, next to Akademy. While attendance will certainly grow in the future, the nature of the conference allows for significant productivity, and personal networking within the KDE community, as well as externally. The rain and other difficulties faced by the team were met with cheerful optimism, and really showed the true strength of the morale within the community.

A special thank you to Jeff Mitchell and the organizing team, and the many sponsors that made this event possible.