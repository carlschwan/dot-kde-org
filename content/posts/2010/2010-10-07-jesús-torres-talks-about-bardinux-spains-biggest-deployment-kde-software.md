---
title: "Jes\u00fas Torres Talks About Bardinux, Spain's Biggest Deployment of KDE Software"
date:    2010-10-07
authors:
  - "a(toscalix)"
slug:    jesús-torres-talks-about-bardinux-spains-biggest-deployment-kde-software
comments:
  - subject: "SIMO"
    date: 2010-10-08
    body: "It's great to get KDE represented at big events like SIMO where a lot of people might not have heard of us.\r\n\r\nHow was that experience?"
    author: "Stuart Jarvis"
---

Many of you will know that KDE is very active in Spain. After all, KDE Spain recently <a href="http://dot.kde.org/2010/07/14/kde-ev-and-kde-españ-sign-agreement-further-kde-community">became the first regional KDE organization to officially affiliate with KDE e.V.</a>

<h2>Upcoming Events</h2>

Spain is an exciting place for KDE. There are often opportunities to meet people from KDE at events around the country. Sebastian Kügler was recently talking in Madrid and today Rafael Fernández, KDE Spain Vice President, <a href="http://madridonrails.com/modules/?r=news/view/Madrid-on-Rails-en-el-Stand-del-Ayuntamiento-del-SIMO">will be presenting</a> at <a href="http://www.ifema.es/web/ferias/simonetwork/default.html">SIMO</a>, the biggest IT event in Madrid. Next month, Antonio Larrosa will be giving a talk at <a href="http://www.opensourceworldconference.com/malaga10/">OSWC'10</a>

<h2>Bardinux, a KDE Success Story</h2>

Today though, reflecting a topic from Rafael's talk, we present an interview between <b>Michael Dixon</b> and <b>Jesús Torres</b>, the man in charge of Software Libre Office of the University of San Fernando de La Laguna and, through that, the largest deployment of KDE software in Spain. The University is situated in San Cristobal de La Laguna on the island of Tenerife in Spain. This University is the center of a revolution that empowers many students with knowledge of KDE and free and open software through its <a href="http://bardinux.ull.es/">Bardinux</a> Linux distribution. But don’t take our word for it, here’s what Jesús has to say.

<h2>Meet Jesús Torres</h2>

<b>Michael: </b>You are doing exciting things with KDE at the University of La Laguna, in Spain.  Excellent!  Before we talk about that, our readers need to know about you.  Please tell us about yourself.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/DSC_0495.JPG"><br/>Jesús Torres with a PC running Bardinux and KDE software</div><b>Jesús: </b>Well, I am an Assistant Professor at the University of La Laguna. I teach computer science courses, especially operating system topics.  I am also the Chairman of the Free Software Bureau at the university.

I started to get close to free/libre and open source software (FLOSS) around 1997, a few years before completing my bachelor's degree. I had tried some GNU/Linux distributions before but only as an entertainment.  I thought it was too difficult to use them as a real alternative to proprietary operating systems. But around 1997, I was developing software to easily fill in and send forms that came by fax to the family business from other companies. I spent some money to buy a good IDE integrated with the operating system SDK but when I started my project, I discovered that was not enough. I needed more information.  Information that I could only get spending more money or searching the Internet with the hope that someone had documented what I needed. Because of that, I lost a lot of time reverse engineering the libraries, without any documentation. In any case, toward the end of the project, I needed to manipulate the fax bitmaps in TIFF format. Thanks to the few hours spent in GNU/Linux, I remembered the LibTIFF library and found a port to Windows. I enjoyed developing with it compared to what I had been doing before. There was documentation and examples! If the documentation did not seem right or I did not understand it or I did not have a good enough example, I just looked at the LibTIFF source code or other developer’s programs.

After that project, I thought that learning more about FLOSS was the way to keep learning computing. Then I moved all my documents and files to GNU/Linux because I was convinced that the only way to learn was to force myself to use it each day. Later, I became a member of a local LUG. And that is how I arrived at the Free and Open Source Software world.

<b>Michael: </b>Tell us about your work with Free Software Bureau at La Laguna University in Spain.

<b>Jesús: </b>The objective of the Free Software Bureau (OSL is its Spanish acronym) is the promotion of Free and Open Source Software, mainly inside the University but also outside. To support that, we organize some courses about free software technologies each year and we make sure that the computers that the university offers to the students have as much free software as possible. In addition, we participate in FLOSS events (eg. GCDS'09), or in technology events where we can promote it, we do some development for the University and assist in decision-making processes about new technologies, with the aim of encouraging them to be free.

The work for the Free Software Bureau, at La Laguna University, is done by eight computer science undergraduate students.  My role is to organize them. I also act as an intermediary between the open source community inside the University and the University management.

<b>Michael: </b>La Laguna University created Bardinux. Can you tell us the story behind that?

<b>Jesús: </b>Well, Bardinux origins date back to mid-2003 when a group of skilled students of computer science customized the Knoppix Live CD. The Faculty of Computer Science wanted a distribution to facilitate the use of GNU/Linux for first year students at home. These students learn to use software for GNU/Linux in our Computer Center. However, at home they usually tried to use ports of these programs to Windows because they did not have GNU/Linux at home.  This usually caused problems.  So we wanted to give students a Live CD with all the software they needed. The Live CD was named Guachinche (which is the name given to a type of tavern typical of the Canary Island of Tenerife). It included changes in the artwork and packages that the professors indicated that first year students need.

At that time, there was no Free Software Bureau, only a Free Software Community supported the project. In early 2005, the University of La Laguna established the Free Software Bureau at the request of the community. One of the early projects was to extend Guachinche to the entire University. Bardinux 1.0 was released a year later. It was based on Kubuntu and included free software needed by all students of the university, not just the computer science students. Its name is the contraction of Linux and Bardino, a breed of dog originating in the Canary Island of Fuerteventura, and the Bardinux mascot.

<b>Michael: </b> Why did you decide to build on Ubuntu and a KDE-based desktop over other free software choices?

<b>Jesús: </b>When Bardinux was designed, there were some good technical reasons to choose KDE software for the desktop environment. In our opinion, what KDE offered was more complete and was more configurable than other choices. We thought it would allow us to prepare the environment for students with different technology skills. Furthermore, Bardinux was not just created to ensure students have GNU/Linux at home. It was also prepared to facilitate the deployment of GNU/Linux in the University computers and Computer Centers. In that sense, the maturity of the kiosk mode was another reason to choose KDE because we need to avoid giving users full control over the desktop configuration. I guess advanced users of other desktop environments could have done the same with their favourite one, but everyone on the team was a KDE user so we were more comfortable doing things in KDE.

The choice of Ubuntu was based on a handful of reasons, but I think none was decisive by itself.  We preferred to work with a Debian-based distribution because everyone in the Bardinux team was a Debian user.  Some were Debian system administrators. Moreover, the Debian distribution use has been widespread in our environment for many years; not only in the Free Software Bureau, but also among GNU/Linux users at the University and members of the local LUG. So we felt much more comfortable working with Debian-based distributions. Focusing on the end-user, the choice was Ubuntu. Knoppix was another, but we knew from experience how difficult is to customize it. 

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/DSC_0460.JPG"><br/>A whole room full of KDE goodness</div>Furthermore, the first version of Kubuntu had only been released recently at that time so we expected that the integration of KDE in the distribution would improve with time. For us, KDE software was nearly the first-class citizen of the distribution. Not a single add-on was as important. Finally, we thought the regularity and frequency of the releases were very interesting. The regularity helps us to schedule the Bardinux releases. The frequency is important because many teachers want to use the latest versions of many programs and we unfortunately have no resources to maintain backports of them. 

<b>Michael: </b> What has been the experience of the users?  How have they responded to Bardinux and KDE?  What do people like? What is missing or not working so well?

<b>Jesús: </b> Before answering your question I would like to tell you some data, so you may understand the context. We have about 1,400 computers for students in the university, but we do not know how many people use Bardinux at home. Many of these computers have dual boot, but over 300 of them have only Bardinux. With regard to users, we have users of all technological skill levels.

I really wonder how hard it is to get some feedback from users about Bardinux. Most complaints reported are about things like connectivity problems, loss of data or printing problems but there are practically no complaints about bugs or lack of features in Bardinux. Also, I guess we do not have many complaints about usability because there is a person in each computer classroom to assist users. We've only had a few complaints about laptops provided by University libraries. These notebooks have only Bardinux, so some users complain when they discover that there is no dual boot. In any case, eventually these users use the laptops without problems. 

I think in the Free Software Bureau the current main problems of Bardinux are the age, the latest version and the lack of a more usable application installer. The latest version of Bardinux is based on Kubuntu 8.04, so we have problems with new hardware and the installation of recent applications. Regarding the application installer, one of the biggest problems for users of a new operating system is to find applications that allow them to do what they did previously. So we think we need an installer that includes a list of free quality software. In the Free Software Bureau, we have a multi-platform catalog of free software applications (<a href="http://selibre.osl.ull.es">in Spanish</a>).  We would like to have an installer application that includes something similar. 

<b>Michael: </b> Bardinux currently makes use of KDE 3. Are there any plans for migrating to Plasma Desktop? Will there be another Bardinux version? 

<b>Jesús: </b>Yes, there are. We are working on a new version based on Kubuntu 10.4 that will be released in early September and, obviously, will come with Plasma Desktop and the new applications. Bardinux is not a one version project! We have released four Bardinux versions since 2006 and we want to keep it alive for a long time because it is a key element of our strategy. For example, we intend to release versions with more stuff that provide zero configuration support for the services of our university (such as VPN access, DNIe authentication, etc.). Unfortunately, this has been delayed for almost a year. Switching to software on KDE Platform 4 had something to do with it, but the main reason is that the Free Software Bureau staff is unstable because the students have to leave us when they graduate. In any case, we will finally have a new Bardinux in September.

<b>Michael: </b> Bardinux has found its way to Facebook and Bolivia. Do you have information about how widely Bardinux has spread? Are other universities or institutions using it?

<b>Jesús: </b>No, we do not have information about how widely Bardinux has spread. Other universities and institutions have told us they like Bardinux but will not use it because they prefer to build their own customized distribution. That's the beauty of free software; with a little effort anyone can make their Bardinux, with its own logos, artwork and content. 

So we are not very interested in knowing how many others use Bardinux. We are interested in what we can do together to make the effort to build it, as small as possible. For example, when we released the first versions of Bardinux the process to build a distribution was very complex. Therefore, we worked with other Spanish universities in a project to develop a tool to make the process easier. It was the Unidistro project. Now that we have the right tools, we are more interested in collaborating in the development of metapackages to group software by subject. For example, there are programs that could be important for students of chemistry, so we have created the metapackage bardinux-chemistry to install all that software easily.

We currently have a bunch of metapackages like bardinux-chemistry but for other subjects like mathematics, physics, geography, science, education, etc, that the Professors of our University assist us in creating and updating.  These metapackages are rather complicated. In addition, we rarely can respond when a teacher asks us what a particular program does. I think something similar may happen in other universities, so we are looking at collaborations with other universities to alleviate this task. 

<b>Michael: </b>The KDE community is very large and vibrant, would it help Bardinux to have an affiliation with KDE, such as a logo? We now have a <a href="http://dot.kde.org/2010/06/21/introducing-your-kde-software-labels">series of labels</a> you could use...

<b>Jesús: </b>Yes, I think it could help Bardinux. KDE is a very important project which has a large community, so affiliation would be very interesting. I also think it would be very useful for students of the Free Software Bureau to have more contact with the KDE community. In any case, I did not know about those labels but will include some in the version of Bardinux that will be released soon.

<b>Michael: </b> Is there anything else you want to say?

<b>Jesús: </b>I just want to thank you for this interview because it has given us the opportunity to tell the KDE community what we are doing. Thank you very much.