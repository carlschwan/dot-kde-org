---
title: "Get Involved: KWin Bug Day Tomorrow"
date:    2010-10-09
authors:
  - "javier"
slug:    get-involved-kwin-bug-day-tomorrow
comments:
  - subject: "I would like to wish you good"
    date: 2010-10-09
    body: "I would like to wish you good luck."
    author: "beer"
  - subject: "Me too. I love KDE 4.5, but"
    date: 2010-10-09
    body: "Me too. I love KDE 4.5, but KWin + 'Emacs with LaTeX-mode on' is driving me insane..."
    author: "jadrian"
---
Sunday is the first <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KWinDay1">KWin Bug Day</a> (10th day of the 10th month 2010 - easy to remember!). KWin is a vital part of the KDE Platform and used by all software running in a Plasma Workspace. However, with the latest influx of bug reports it is becoming increasingly difficult for regular contributors like Martin Grässlin and the KWin team to keep up. This is where <strong>you</strong> can help with squashing those bugs! 
<!--break-->
You don't need to have any special skills to get involved. The most important thing is that you are interested in helping us to clean up the bug report list so developers can focus on fixing the real problems.

The Bug Day has three goals:
<ul>
<li>Check whether old KWin bugs can still be reproduced (those from the KDE 3 days and early Platform 4 releases)</li>
<li>Check recent and UNCONFIRMED bugs, reproduce, check for duplicates and figure out step-by-step instructions (as well as special circumstances like graphic card drivers)</li>
<li>Flag bugs so they can easily be sorted into components</li>
</ul>

Wondering why you should help out? Then read our <a href="http://techbase.kde.org/Contribute/Bugsquad/Guide">primer on why triaging bugs is so important</a>.

Help make KWin better! Join us in the #kde-bugs IRC channel on FreeNode, Sunday, the 10th of October 2010 starting at 11:00 a.m. UTC.