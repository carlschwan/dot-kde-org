---
title: "KOffice 2.1.1 Gets the Bug Count Down"
date:    2010-01-14
authors:
  - "jriddell"
slug:    koffice-211-gets-bug-count-down
---
Your hard-working KOffice team have been coding away over the last couple of months and the resulting <a href="http://www.koffice.org/news/koffice-211-released/">KOffice 2.1.1</a> is now available.  This is a bugfix release and <a href="http://www.koffice.org/changelogs/koffice-211-changelog/">the changelog</a> shows that it improves every app in the suite.  KOffice is the most integrated and complete suite of office applications available, however the 2.1 series is still recommended to experimental adopters only.
