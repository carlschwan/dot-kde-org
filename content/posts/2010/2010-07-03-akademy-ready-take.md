---
title: "Akademy Ready for Take-off!"
date:    2010-07-03
authors:
  - "jospoortvliet"
slug:    akademy-ready-take
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/on_way_to_party.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/on_way_to_party_wee.jpg"></a><br/>Walking towards registration</div>In Tampere, Finland, the pre-Akademy party kicked off the conference last night. Hundreds of KDE contributors were there, meeting old and new friends and enjoying the party. Rowdy Brazilians and Dutch cheered on their teams as their countries battled each other in the World Cup on the big screen. Others spent time reliving their childhood on the classic arcade machine.  

This morning, the main conference will start with a keynote by Valtteri Halle, member of the MeeGo technical steering group and director of Nokia Meego Software. He will open the conference talking about one of the main tracks at Akademy this year, mobile computing interfaces and their influence on modern interfaces. Over the course of two days, over 40 other speakers will discuss topics as diverse as <a href="http://akademy.kde.org/node/469">Cloud computing</a>, <a href="http://akademy.kde.org/node/511">speech recognition</a> and <a href="http://akademy.kde.org/node/517">our efforts on porting to MS Windows</a>. Those talks will be recorded as much as possible so those who had to stay home will be able to see what is being discussed. The dot editors and the KDE promo team look forward to keeping you all informed on what is going on here and sharing a bit of this experience! Be sure to join the <a href="http://identi.ca/group/akademy">!Akademy group on identi.ca</a> to follow the buzz.
