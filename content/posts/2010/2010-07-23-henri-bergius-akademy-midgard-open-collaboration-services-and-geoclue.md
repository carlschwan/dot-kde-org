---
title: "Henri Bergius on Akademy, MidGard, Open Collaboration Services and GeoClue"
date:    2010-07-23
authors:
  - "jospoortvliet"
slug:    henri-bergius-akademy-midgard-open-collaboration-services-and-geoclue
comments:
  - subject: "It's cool to see OCS being"
    date: 2010-07-18
    body: "It's cool to see OCS being picked up. Just while working out the notes for this article, I read this blog by Luca Beltrame: http://www.dennogumi.org/2010/07/open-collaboration-services-and-kde-forums\r\n\r\nGo KDE Forums ;-)\r\n\r\nIt's actually big news that Nokia/Intel are using OSC for their app store - and hopefully we can get more providers to make use of it. I've heard the OCS meeting at Akademy was very successful, and work is ongoing to improve the spec. Now let's make sure we keep this up - improve this spec, don't duplicate work and get others on board!"
    author: "jospoortvliet"
  - subject: "Yeah it really is. Just"
    date: 2010-07-24
    body: "Yeah it really is. Just wanted to say the same. Nokia using OCS? Wow! Amazing :D"
    author: "vdboor"
---
At Akademy, we ran in to many interesting people, including "random motorcycle adventurer" Henri Bergius, who is also a board member for Akademy co-organizers <a href="http://www.coss.fi/en">COSS</a> among his many other free software related activities. Read on for the full conversation.
<!--break-->
<b>Dude! Who are you and what do you do?!?</b>

Hi dude! I'm a random motorcycle adventurer who happens to run a small company doing Free Software. We do most of our work on Midgard, a data storage library for desktop and web apps. Mainly my company builds web apps for customers.

I'm also active in the community politics side of Free Software, including being an active member of the FSFE in Finland. I've lived on FOSS for 12 years, and feel it's relevant ;-)

But seriously, I'm not that interested in creating demand for FOSS - I think it's there. I focus on removing obstacles, such as the tenders where a public organization wants a 'standard solution', which then means FOSS is out of the question.

In addition to volunteering for the FSFE, I'm also in the board of COSS, the Center for Open Source Software in Finland (hosting Akademy 2010).

<b>I've been trying to catch you for an interview for ages. You were working on being an <a href="http://opendesktop.org/">Open Desktop</a> provider?</b>

Yes. At <a href="http://maemo.org">maemo.org</a> we have an appstore for FOSS applications on the Maemo platform. This appstore is enabled by default on all Nokia N900s so we wanted to have some quality control. We had to create our own appstore approval process, compatible with the FOSS philosophy. Now any developer can submit an app, and anyone can test and vote. The whole process is completely transparent, auditable and visible. And it also provides a feedback channel from testers and users to the developers!

Next we wanted to improve the installation experience. Initially we had the UI for apt which doesn't provide much information - category, name, text description. So we were looking for a protocol that could provide the proper info. I happened to run into Frank Karlitschek at some conference, talking about <a href="http://freedesktop.org/wiki/Specifications/open-collaboration-services">Open Collaboration Services (OCS)</a>. I realized they had practically all that we needed. We developed an OCS server that talks to the appstore, and Daniel Wilms from Nokia built the appstore.

<div style="float: right; border: thin solid gray; padding 1ex; margin 1ex;"><embed src="http://blip.tv/play/hIsige%2BELgA" type="application/x-shockwave-flash" width="320" height="270" allowscriptaccess="always" allowfullscreen="true"></embed></div>
Henri demoing the appstore

<b>So you are actively developing on OCS?</b>

Yes, Nokia is sponsoring development. Today we are attending the OCS BoF to discuss changes we would like to see in the specification.

<b>But you're involved in much more than this - you've been giving out these Midgard stickers to everyone who did (and did not) want them... Tell us about it!</b>

Midgard is a data storage service. Whether you write desktop or web applications, instead of coming up with your own file format, you just use Midgard. You can work more easily and object-based. Users have many different devices these days, so Midgard has strong replication features to synchronize between different systems. Midgard is built on top of GObject; we provide bindings to a bunch of different languages so developers can choose the tools they like - PHP, Python, Javascript. Currently (as in <b>now</b>, while we're talking) Qt bindings are being developed here at Akademy.

<b>Tell us what's so good about Midgard?</b>

First of all, you have a pretty powerful storage system with query tools; a good API, featuring full or partial replication; it's a library so you don't need a storage daemon running around; and it can talk to relational databases so you can use MySQL and SQL lite. Replication will work between them too.

Midgard also provides RPC features. So when two apps work with same data, if one changes the data, the other will receive a D-Bus signal. On top of that, we have web development frameworks based on PHP and Python. This offers some convenience but it is not required.

One cool thing is that this community has been running for quite some time. Last May we had our 10 year anniversary. There are a bunch of companies and individuals working on it, so it's an open community, not controlled by just one company or individual. We try to have two developer meetings per year bringing everyone together. This Akademy is kind of a first for us as we decided to tag along and talk with people from other communities. Some interesting ideas have come out. We're currently building a Plasma DataEngine on Midgard so applets can store data in Midgard and synchronize it between devices.

<b>How is Akademy for you?</b>

Well, I'm biased as I'm part of the hosting organization, but this is also my third Akademy so I am familiar with the conference. More and more cool stuff is coming from KDE. I promised that once network management was fixed, I'd try the Plasma Desktop again. All this stuff like Silk and Plasma itself is incredibly cool. The KDE community is amazingly receptive to new ideas. We have seen a lot more positive reaction in this community compared to other places when we proposed and demonstrated things like Midgard and GeoClue.

<b>Aaaah, GeoClue, another thing you're involved with. Tell us a bit about it, then!</b>

As at previous Akademies, I gave a talk about it. I said I'd continue to push it until you guys and girls integrate it. It can make your apps much smarter - like automatically changing the timezone in the panel clock. We tried to get this in Gnome for three years but it still hasn't worked out. Now, unfortunately, Mac OS X has been the first to market with it. We could have had this first on the Free Desktop! So I hope the Plasma developers will pick this up. It shouldn't be too hard and they seem open to it.

<b>UPDATE</b> An hour later, Henri came back to report that there now is a Midgard Plasma DataEngine with about 50 lines of Python as a working proof of concept. When Aaron wakes up, Henri wants to work with him and the rest of the Plasma team to get it integrated. To cap things off, Henri would love to have this in most distributions by default. The Midgard people are looking at Suse Studio as well, as that's great technology to spread something new. And Henri has been seen talking to the ownCloud developers as well. As usual, cross-pollination at Akademy is going strong!