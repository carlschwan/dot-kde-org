---
title: "KOffice 2.2 is Here"
date:    2010-05-28
authors:
  - "ingwa"
slug:    koffice-22-here
comments:
  - subject: "Wow!"
    date: 2010-05-28
    body: "That sounds pretty good! Looks like Koffice will be soon able to replace OOo. I hope there soon will be Kubuntu-Packages :-)\r\n\r\nAbout:\r\n>There are still areas in the user interface that we want to work on before stating that KOffice 2 is ready \r\n>for the general public. \r\n\r\nCould you specify what a user would miss in functionality? Or in sense of document format compatibility?"
    author: "mark"
  - subject: "wrong download locations"
    date: 2010-05-28
    body: "The release announcement gives the download URL as\r\n\r\nhttp://download.kde.org/download.php?url=unstable/koffice-2.2.0/koffice-2.2.0.tar.bz2\r\n\r\ninstead of\r\n\r\nhttp://download.kde.org/download.php?url=stable/koffice-2.2.0/koffice-2.2.0.tar.bz2\r\n\r\nchepati"
    author: "chepati"
  - subject: "that list of changes..."
    date: 2010-05-29
    body: "that list of changes linked from the article is absurdly long.. and it does not even include krita's changes.\r\n\r\ni'm stunned by the amount of work that has gone into this release. understanding that this might be because nokia is sponsoring makes me really smile at opensource as way to collaborate.\r\n\r\nthank you nokia, thanks for thinking out of the box.\r\n\r\n"
    author: "cies"
  - subject: "thanks!"
    date: 2010-05-29
    body: "This is fixed now. Cheers!"
    author: "zander"
  - subject: "reviews?"
    date: 2010-05-29
    body: "I'm hoping to see some reviews of people finding out what they like and dislike. I'd be biased (as the maintainer of KWord) in saying that so much has improved its very usable in itself and one by one the major features are getting attention.\r\n\r\nKOffice (and KWord specifically) is based on plugins a lot and I am hoping to get more developers helping out fixing bugs in those plugins as those are much smaller to learn and re-compile.\r\nThink about things like spell-checking, variables (date and page number) as well as auto-correction. All those are plugins and I invite here anyone to come and add features or fixing issues if you just know a bit of C++.\r\n\r\nExciting times :)"
    author: "zander"
  - subject: "Broken link"
    date: 2010-05-29
    body: "On the \"KChart\" section of the changelog, the link to KDChart is wrong. It should be http://www.kdab.com/index.php?option=com_content&view=article&id=55:kd-chart&catid=37:kd-chart&Itemid=65"
    author: "majewsky"
  - subject: "Thanks for all the hard work!"
    date: 2010-05-29
    body: "Every time that a new KOffice comes around, I give it a try. It's always far faster than any other office suite, but it usually has a few niggling glitches that force me back to OpenOffice. Since I don't like OpenOffice even vaguely, I keep checking KOffice every chance I get.\r\n\r\nIt's building right now. Hopefully this one's the one and I can do my happy dance. Either way, I'll probably be sending you some bug-reports in the following week."
    author: "abresch"
  - subject: "I hope KWord 2.2 will replace KWord 1.6"
    date: 2010-05-30
    body: "I'm using KWord 1.6 every day. I hope this realease can let me replace KWord 1.6 with KWord 2.2 and that it let me continue to work on my ODT documents as well.\r\n\r\nFor the Italian readers I made a translation of the article here: http://kde-it.org/2010/05/30/rilasciato-koffice-2-2/"
    author: "giovanni"
  - subject: "niggling glitches"
    date: 2010-05-30
    body: "Those niggling glitches is something we are interested in.  Would you care writing a little report on what you are missing?  I hope you are already reporting the bugs that you see.\r\n\r\n"
    author: "ingwa"
  - subject: "Windows-installer"
    date: 2010-05-31
    body: "When this release has stabilized, I think a windows-installer file would increase the use of KOffice."
    author: "axeloi"
  - subject: "Extensively Testing a Single Use-Case"
    date: 2010-05-31
    body: "Well, I went through and did a use-case test for KWord, doing something I have done several times that uses many word-processor features.\r\n\r\nAs I went, I wrote down the details of every problem I encountered and also took a few screenshots. So, I've got some PNGs and a longish document on those niggling glitches, as well as a few rather serious errors.\r\n\r\nIs there somewhere in particular I could send that so that you could use it?\r\n\r\n(Also, I'll get to posting the bugs in another day or two. It was kinda a trade-off between writing out things as I did them and putting out bug-reports as I went. Doing both would have been... bleh.)\r\n\r\n((Furthermore, I want to say that although there were plenty of things for me to pick on, I was impressed overall. It still fell short of where I could use it full-time because there were problems when it was handling an especially large document, but it was vastly improved over the last time I tried it out.))"
    author: "abresch"
  - subject: "Extensively Testing a Single Use-Case"
    date: 2010-05-31
    body: "You can send the document to me.  I will try to do something productive with it.\r\n\r\n"
    author: "ingwa"
  - subject: "OSX Packaging"
    date: 2010-06-02
    body: "Is there a decent source for binary packages or even up to date compile instructions for OSX?  Fink and MacPorts both appear to be terribly out of date.\r\n\r\nI ask having just bought a brand new MacBook Pro, and reading in the changes that they are making sure it works on OSX.  Which is great, but I'm an utter novice in OSX (and the new hardware isn't supported by Linux yet; don't disagree, it will be, but isn't yet).  I have been using KDE since 1998, but I can't find any obvious links or support points for my new OS.\r\n\r\nmac.kde.org seems woefully out of date given the developer-side assertions of support and hard effort to make it work.  It seems that the developers are putting in the time to make sure it works, but a working solution is not getting through to end users.  I can't even find up to date manual build instructions.\r\n\r\nIs there some hidden world of KDE/KOffice on OSX community that I'm unaware of?  If so, linking more prominently to it would make sense.  If it doesn't exist, a plea for help and publicly stated honest assessment of the situation would be a good idea (and might garner support!).  \r\n\r\nKDE releases source, which is fine by me.  But right now there are vague rumors and dead-end links of compiled OSX packages or out-of-date solutions like Fink and MacPorts.  It would be much easier on end-users if it were clearly acknowledged that a manual compile is your only option.  Or, if there are up to date packages, the various official KDE-on-OSX sites pointed to those options.\r\n\r\nNote that I am not insulting anybody or demanding anything.  I'm just pointing out that there's an abandoned segment of the KDE project (end-user web documentation for current KDE/KOffice on OSX) that probably should be corrected.  Or at least have a \"Help Wanted, Apply Here\" link stuck to it for people who might be able to contribute.  Most KDE sites went through a wonderful overhaul recently, but this particular little audience went ignored.  The ONLY reason I'm posting it here is due to an honest ignorance of where else to draw attention to the problem (it's not exactly a bug, eh?)\r\n\r\nIn short, the developers are putting in the work, but their effort isn't percolating down to users out of the active developer loop, and all documentation on official sites and linked projects (like Fink and MacPorts) are out of date."
    author: "JabberWokky"
  - subject: "Stuff what bugs me :-("
    date: 2010-06-02
    body: "I need to share lots of very basic text documents between multiple users. I would like to use KOffice but I am forced not to use it at all, but OpenOffice.org as others. Why so?\r\n\r\nBecause the basic text functions like the document properties of the page size and margins does not stay correctly.\r\n\r\nWith <2.2 versions there was the ugly font rendering. Now it seems to be fixed. So it is easier to write text when the font really looks correct (at least on 75% or 100% zooms).\r\n\r\nBut if I set document size as A4 (210x297mm) with 2cm margins (20mm) and then I use same font (size, type etc) in both, OOo and KOffice 2.2. I get different results.\r\n\r\nThen if I type longer text, the OOo gives about 2-9 letters more space than KOffice.\r\nThat is something what brakes very basic text document structure totally and makes KOffice un-usable when needed to store and share documents in ODF's.\r\n\r\nIs there any dirty hacks or tricks how to avoid such things? We would like to use liberation sans font as 10pt. But same time we use lots of normal fonts as well, same size, document properties etc. But we end up to different format. Even when saving document and opening with another application and vice versa.\r\n\r\nShouldn't the ODF be answer even for this kind basic stuff that there would be no differences and sharing documents would be possible without structure being changed depending application and it version?\r\n\r\nWhat I (we) am (are) doing wrong? I have fighted so long time now to get good basic functions for sharing text documents. And as far I know, the only answer for that has be txt format!"
    author: "Fri13"
  - subject: "I'm afraid the kde-on-mac"
    date: 2010-06-04
    body: "I'm afraid the kde-on-mac team is simply suffering from a lack of resources..."
    author: "jospoortvliet"
  - subject: "what about Kivio ?"
    date: 2010-06-04
    body: "I am very excited by this new KOffice release.\r\nI always preferred KOffice to any other opensource office suite.\r\nbut why Kivio hasn't still be ported to KDE4 ?\r\n\r\nKivio was so excellent. please someone bring it back to life.\r\n\r\nI heard SVG artist was required for this tasks.\r\n\r\n"
    author: "somekool"
  - subject: "yes, among other things, they"
    date: 2010-06-05
    body: "yes, among other things, they need default stencil sets."
    author: "jospoortvliet"
---

The KOffice team is very happy to <a href="http://www.koffice.org/news/koffice-2-2-released/">announce the arrival of KOffice 2.2.0</a>, half a year after version 2.1 was released. This release brings an unprecedented number of new features and bugfixes as can be seen in  the <a href="http://www.koffice.org/changelogs/koffice-22-changelog/">full list of changes</a>.

There are two reasons for this very high development rate. Firstly, people have started to notice KOffice again and the developer community is growing. Secondly, Nokia is sponsoring development of KOffice for their mobile office viewer. Here follows a list of the most important changes in this release.

<h2>Highlights of KOffice 2.2</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org//sites/dot.kde.org/files/kexi2-windows-392.png" width="392" height="308"/><br/>Kexi returns in KOffice 2.2</div><h3>Kexi is Back</h3>

The big news for this release is that Kexi, the data management application, is back.  Kexi was last released in KOffice 1.6 so the new version has seen over 3 years of developement. Kexi has received a complete overhaul of the user interface and should be ready for production work.

<h3>Major Enhancements</h3>

There are many larger and smaller improvements in KOffice 2.2. The biggest enhancement that is available in all applications is a new framework for effects on shapes. Right now there are eleven effects and more will follow in future releases.

<h3>New Import Filters for MS OOXML Formats</h3>

KOffice has received new import filters for the Microsoft XML based office formats that are used in MS Office 2007 and later. The filters are still pretty basic, but definitely usable for real documents. At this time there are no plans to implement export filters for these file formats.

<h2>Improvements in the applications</h2>

All the applications of KOffice gained major new features and lots of bugfixes.  In total there have been over 4500 commits during the last 6 months. The list of changes is too big to include here, but curious readers can find the full details in the <a href="http://www.koffice.org/changelogs/koffice-22-changelog/">changelog</a>.

<h3>Suitability for Use</h3>
This release should be much more stable and full-featured than 2.1. KOffice 2.2 has received many enhancements to the layout engine, the libraries and the filters. There are still areas in the user interface that we want to work on before stating that KOffice 2 is ready for the general public. We are, however, at a stage where we think that KOffice can be used for real work.