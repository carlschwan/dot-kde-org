---
title: "KDE to Appear at SCALE 8x"
date:    2010-01-28
authors:
  - "ajohnson"
slug:    kde-appear-scale-8x
comments:
  - subject: "Hope to be there"
    date: 2010-01-31
    body: "I will be around add me up on identi.ca / twitter @jza\r\n\r\nHope to meet with other projects and also this will be my first time on SCALE and would love to meet new local contacts."
    author: "JZA"
---
Looking to help the KDE community and living in Southern California? Then this is a great opportunity for you! The Southern California Linux Expo will be in town February 20-21, 2010, at the Westin Hotel near LAX. All you need to help out is to be willing and able to be there and be a user of KDE's applications, such as the Plasma Workspaces, Amarok and Koffice.

The booth will need to be staffed from 9am-6pm. The great news is that you will get into SCALE for free, and because you will not need to be at the booth the whole show, you can take time to check out the talks or visit other booths.

If you are interested please contact <a href="mailto:aaronforjesus@gmail.com">Aaron Johnson</a> or the <a href="http://mail.kde.org/mailman/listinfo/kde-promo">KDE promo mailinglist</a>.