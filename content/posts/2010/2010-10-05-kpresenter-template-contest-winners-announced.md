---
title: "KPresenter Template Contest Winners announced"
date:    2010-10-05
authors:
  - "zagge"
slug:    kpresenter-template-contest-winners-announced
comments:
  - subject: "Beautful!"
    date: 2010-10-05
    body: "Very nice results!"
    author: "Boudewijn Rempt"
  - subject: "The \"K\" Logo"
    date: 2010-10-05
    body: "Thanks for the good job, these templates are great!\r\n\r\nBut there's one thing I can't understand: why do most of these templates include the KDE logo?\r\n\r\nIf KPresenter offers templates with the KDE logo, I guess they will mainly be used by KDE developers and hackers.\r\n\r\nA common user not belonging to the KDE community (i.e. a student, a teacher, a conference speaker, a Nokia smartphone owner, etc.) would probably prefer a general purpose template.\r\n\r\nThese are great templates, and they can really help the diffusion of KOffice, that's why I think they should be suitable to a general use.\r\n\r\nLorenzo"
    author: "Lor3nzo"
  - subject: "There are three templates"
    date: 2010-10-05
    body: "There are three templates with a KDE logo on them. It's just a fraction of all templates."
    author: "slangkamp"
---
<strong>Wow your audience with KPresenter!</strong>

The <a href="http://www.koffice.org/kpresenter/template-contest/">KPresenter Template Contest</a> was over on September 15. We are really happy with the number and the quality of the entries, and the fact that they came from all over the world. KPresenter 2.3 will be the first version of KPresenter 2 to ship with some cool templates to base your presentations on. Thanks to these templates it's easier than ever to wow your audience, your customers and your colleagues.

Some days ago the panel of judges, consisting of Thorsten Zachmann, Aaron Seigo and Eugene Trounev, sat down together -- virtually! -- to decide on the winners. As always, a difficult task. But these are the winners. Each will get a cool t-shirt:

<strong>1st place</strong>
<i>Burning Desire by Dion Moult</i>

<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/burning_desire/index.html"><img title="Burning Desire" src="http://dot.kde.org/sites/dot.kde.org/files/burning_desire.png" alt="Burning Desire" width="300" height="225" /></a>

A very elegant and well designed entry!

<strong>2nd place</strong>
<i>Skyline Template Set by Joshua L. Blocher</i>

<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/skyline_morning/index.html"><img title="Skyline Morning" src="http://dot.kde.org/sites/dot.kde.org/files/skyline_morning.png" alt="Skyline Morning" width="300" height="225" /></a>

A full set of templates with different themes for normal and widescreen displays.

<strong>3th place</strong>
<i>Flood Light by Dhananjay Garg</i>

<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/flood_light/index.html"><img title="Flood Light" src="http://dot.kde.org/sites/dot.kde.org/files/flood_light.png" alt="Flood Light" width="300" height="225" /></a>

An elegant and harmonized template.

<strong>Shipping more</strong>
But not only the entries of the three winners will be shipped with KPresenter 2.3, the following entries were so nice that they will also ship with KPresenter 2.3.

<table>
<tr valign="top">
<td>Blue Orange Vector by Dhananjay Garg<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/blue_orange_vector/index.html"><img title="Blue Orange Vector" src="http://dot.kde.org/sites/dot.kde.org/files/blue_orange_vector.png" alt="Blue Orange Vector" width="150" /></a>
</td><td>Business by Pramod S G<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/business/index.html"><img title="Business" src="http://dot.kde.org/sites/dot.kde.org/files/business.png" alt="Business" width="150" /></a></td>
<td>Curious penguin by Giacomo<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/curious_penguin/index.html"><img title="Curious penguin" src="http://dot.kde.org/sites/dot.kde.org/files/curious_penguin.png" alt="Curious penguin" width="150" /></a></td></tr>
<tr valign="top"><td>KDE events by Damien Tardy-Panis<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/kde_events/index.html"> <img title="KDE events" src="http://dot.kde.org/sites/dot.kde.org/files/kde_events.png" alt="KDE events" width="150" /></a></td>
<td>Rounded square by Giacomo<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/rounded_square/index.html"><img title="Rounded square" src="http://dot.kde.org/sites/dot.kde.org/files/rounded_square.png" alt="Rounded square" width="150" /></a></td>
<td>Simple waves by Giacomo<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/simple_waves/index.html"><img title="Simple waves" src="http://dot.kde.org/sites/dot.kde.org/files/simple_waves.png" alt="Simple waves" width="150" /></a></td></tr>
<tr valign="top"><td>Strange far hills by Giacomo<a href="http://www.koffice.org/static/kpresenter/template_contest_2010/html/strange_far_hills/index.html"><img title="Strange far hills" src="http://dot.kde.org/sites/dot.kde.org/files/strange_far_hills.png" alt="Strange far hills" width="150" /></a></td>
</tr>
</table>

All submitted entries have been exported to html for you to see. They can be downloaded from the <a href="http://www.koffice.org/kpresenter/template-contest/result/">result page</a>.

Thanks to all the entrants who have made this contest a success!