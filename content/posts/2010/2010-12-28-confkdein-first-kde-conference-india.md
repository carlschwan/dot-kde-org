---
title: "conf.KDE.in: First KDE Conference in India"
date:    2010-12-28
authors:
  - "shaan7"
slug:    confkdein-first-kde-conference-india
comments:
  - subject: "cool"
    date: 2010-12-28
    body: "Awesome news :D"
    author: "jospoortvliet"
  - subject: "Dates"
    date: 2010-12-28
    body: "Just a little clarification, main event is from 9th to 11th (3 days), followed by 2 days of code sprint on 12th and 13th.\r\nMore details at the FAQ."
    author: "shaan7"
  - subject: "I beg you"
    date: 2010-12-28
    body: "I beg you for a t-shirt with this gorgeous kde logo"
    author: "nickkefi"
  - subject: "same here"
    date: 2010-12-28
    body: "Great picture. When and where will it be available as clothware."
    author: "unormal"
  - subject: "Come to the conference ;-)"
    date: 2010-12-28
    body: "Come to the conference ;-)"
    author: "jospoortvliet"
  - subject: "Bit far"
    date: 2010-12-28
    body: "Morning Jos ;-)\r\n\r\nA bit far... Or is there a CfP and what should I take about..,"
    author: "unormal"
  - subject: "he, it's more of a"
    date: 2010-12-28
    body: "he, it's more of a hackfest/developer sprint - but if you want to help and are involved somewhere in KDE it's probably possible to go there. Otherwise you could of course bug Pradeepto, ask him where the image comes from and if it can go in the KDE shirt shop :D"
    author: "jospoortvliet"
---
<div style="float: left; padding: 1ex; margin: 1ex; border: none"><img src=" http://dot.kde.org/sites/dot.kde.org/files/kdeindialogo.jpg" /></div>The <a href="http://kde.in">Indian KDE community</a>  will organize its first conference at Bengaluru in March 2011. The 5 day event will bring together KDE contributors, Qt developers, users and FOSS enthusiasts.

We realise that there are not many KDE/Qt related events that are accessible to Indians. FOSS conferences or meetings are an excellent way to show people the technology first hand and ways to contribute to it. We not only dazzle them with the world of KDE, but show them first hand how simple it is to get involved and make a difference. This is our motivation for conducting this event.
<!--break-->
The main event will showcase Qt & KDE technologies and introduce participants to contributing to our projects. The first three days will have talks, tutorials and interactive sessions. Prominent members of the community will introduce participants to various aspects of development. If you are interested in other aspects of contributing to KDE, there will be talks on localization, documentation, usability, artwork, community, marketing and more. Talks and tutorials will range from beginner level to advanced level so that all kinds of audience members are catered to. You can get some hands on experience with contributing to KDE.

We welcome students, teachers and professionals to take advantage of this opportunity to learn and contribute to an international community.

The last two days of the event will be a more focused code sprint where contributors can brainstorm ideas on their favourite Qt or KDE projects and implement them.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kdeconfindiavenue.jpg"><img src=" http://dot.kde.org/sites/dot.kde.org/files/kdeconfindiavenuet.jpeg" alt="RVCE" /></a></div>The main event will be hosted by <a href="http://rvce.edu.in">R.V. College of Engineering, Bengaluru, India</a>. RVCE was established in 1963 and now offers 12 undergraduate engineering programmes, 16 master degree programmes and doctoral studies. It is located 13 km from the heart of Bengaluru city, the Silicon Valley of India, also known as Bangalore.  The college has a sprawling campus spread over an area of 52 acres set in sylvan surroundings. RVCE is rated amongst the top ten self-financing engineering institutions in India. Moreover it is very active in supporting free and open source community activities.

The main event at RVCE will happen from Wednesday, 9th March to Friday, 11th March. A code sprint will happen right after the main event with the venue to be declared shortly.

Registration dates and conference schedule will be put up very soon. Follow us on <a href="http://twitter.com/kdeindia">Twitter</a> or <a href="http://identi.ca/kdeindia">identi.ca</a> to know about the latest updates as soon as they come out.

For more information, please visit <a href="http://conf.kde.in">the conference website</a>. Should you have any queries the <a href="http://kde.in/conf/faq">FAQ</a> is a good place to start.