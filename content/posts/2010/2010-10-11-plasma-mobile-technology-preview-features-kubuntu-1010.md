---
title: "Plasma Mobile Technology Preview Features in Kubuntu 10.10"
date:    2010-10-11
authors:
  - "jriddell"
slug:    plasma-mobile-technology-preview-features-kubuntu-1010
comments:
  - subject: "Why are the numbers so tiny?"
    date: 2010-10-12
    body: "The numbers are not visible, and I do not see any reason why it should be that way..."
    author: "dgvirtual"
  - subject: "Bug or feature?"
    date: 2010-10-12
    body: "I suspect it may be a bug rather than a deliberate feature. Or perhaps the font setup assumes a particular phone dpi and the screenshot is from a virtual machine at a different dpi.\r\n\r\nThe workspace is in general still very much a work in progress of course"
    author: "Stuart Jarvis"
  - subject: "Simply put, I took the"
    date: 2010-10-13
    body: "Simply put, I took the screenshot from a regular-sized monitor. That dialer is just a mockup, anyways, and would probably use nice, scalable graphics in any practical application. It is a technology preview, after all."
    author: "JontheEchidna"
  - subject: "Kubuntu 10.10"
    date: 2010-10-13
    body: "I just wanted to say that I'm really impressed by Kubuntu 10.10. Very well done!\r\nThe prior incarnations of Kubuntu really weren't very usable for me (I didn't even bother to test 10.04, but 9.04 and 9.10 were quite bad).\r\n\r\nSo after using sidux (now dead) and Archlinux for a while I'm happy that Kubuntu is on the right track again. The default configuration is sane and everthing feels snappy and fast.\r\nThe only real problem I have so far is sound: I randomly hear some nasty skips/glitches/clicks during playback in Amarok. I guess this is due to the switch to pulseaudio, a step I don't really understand...\r\n\r\nBut in whole: Great release!"
    author: "Zapp"
  - subject: "pulseaudio"
    date: 2010-10-13
    body: "Yupp, it was pulseaudio, a simple <code>sudo apt-get autoremove pulseaudio</code> fixed the problem.\r\nIs pulse really worth the hassle? Why does Kununtu need it? Everthing works fine without it..."
    author: "Zapp"
  - subject: "this is really great thanks a"
    date: 2010-10-24
    body: "this is really great thanks a lot\r\n\r\n<a href=\"http://rensoftglobal.com\">social networking web development</a>"
    author: "larsen"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/mobile-wee.png"><br/>Plasma Mobile</div>Yesterday's <a href="http://www.kubuntu.org/news/10.10-release">Kubuntu 10.10</a> release features new KDE software for your phone. Working with KDE's Plasma Mobile team, Kubuntu have created Kubuntu Mobile, suitable for smart phones and available for i386 and ARM platforms. This is a technology preview of the upcoming Plasma Mobile workspace and is not ready for day to day use.

Kubuntu 10.10 also features an application-focused KDE interface to the cross-distribution PackageKit package management layer, providing a modern and easy to use native interface for installing and updating your software.
<!--break-->
