---
title: "Second 4.6 Series Betas Preview New Kontact"
date:    2010-12-08
authors:
  - "Stuart Jarvis"
slug:    second-46-series-betas-preview-new-kontact
comments:
  - subject: "Akregator"
    date: 2010-12-09
    body: "Does it mean Akregator will be released akonadified too?"
    author: "mark"
  - subject: "Unlikely, IIRC"
    date: 2010-12-09
    body: "Akgregator is, to my knowledge, only half-ported and basically completely unmaintained."
    author: "einar"
  - subject: "No mention of Kmail2"
    date: 2010-12-09
    body: "In the announcement you have linked to, there is no mention of v.2 of the PIM.\r\n\r\nAlso, I have just installed Beta2 from the Kubuntu beta repository, and the PIM is still one the same level as the 4.5 series. To get Kmail2, you need to add yet another repository and this could mean that Kmail2 is not considered ready for a beta2 yet.\r\n\r\nSo for anyone running Kubuntu 10.10 wanting to test KDE 4.6 Beta2 with the new Kmail2, you will need to add two repositories.\r\n\r\nhttp://www.kubuntu.org/news/kde-sc-4.6-beta-2"
    author: "Oceanwatcher"
  - subject: "Congratulation"
    date: 2010-12-10
    body: "in my laptop HP HDX 16, first with beta1 and then with beta2, overall efficiency has increased dramatically: Dolphin is reactive (from beta2:)),  kwin gives me the feeling of being \"fluid\", handling audio with phonon now allows me to enable bitexact playback with just one click, kate/kdevelop has become a must, k3b is solid like a rock... and last but not least, no crashes recorded during my daily use.\r\nNow the painful notes: the notification system is ugly, do not talk about features or types of information, I say that information is presented bad! too much confusion, downsizing dancers, invasive.\r\nLast: the new \"metallic\" clock effect is amazing but if you enable show date, then the result clock+date returns an amateurish feeling.\r\n\r\nThank you for all."
    author: "joethefox"
  - subject: "Kmail2 reliability"
    date: 2010-12-10
    body: "So is Kmail2 enough reliable and doesn't bring mail data corruption? I would be ok for now if could crash but I want not to loose mails (which are in fact on IMAP server, so they should not be deleted there).\r\nExperimental repository does sound scary.\r\n"
    author: "xeros"
  - subject: "KMail2 reliability"
    date: 2010-12-10
    body: "I use KMail2 for weeks, and there was no data loss. There a bugs, migration is not painless, it is slower than kmail1, but if you can live with those, it is usable. Again, I experienced no data loss using imap and pop3."
    author: "andras"
---
KDE today <a href="http://kde.org/announcements/announce-4.6-beta2.php">announced the immediate availability</a> of a second set of beta releases for the upcoming 4.6 series. These releases, codenamed <i>Caramel</i>, bring  previews of new versions of the Plasma Desktop and Plasma Netbook workspaces as well as introducing major changes to applications and the KDE Platform.

<i>Since the first set of beta releases two weeks ago 1318 bugs have been reported and 1176 bugs have been closed. Thank you to our testers and developers working on making the final releases shine!</i>

Well, that looks promising, but more testing (and helping us fix issues!) is needed. So here is another snapshot for you to grind your teeth on. In particular, we invite further testing of Kontact and its individual applications as they complete the transition to the <a href="http://pim.kde.org/akonadi/">Akonadi storage framework</a>.

The usual boilerplate applies as well: these beta releases do not meet production quality requirements and should be regarded as suitable for testing purposes only.