---
title: "KDE at FOSDEM this Weekend"
date:    2010-02-02
authors:
  - "jriddell"
slug:    kde-fosdem-weekend
comments:
  - subject: "VoiP Solution?"
    date: 2010-02-03
    body: "Hi,\r\n\r\nmaybe you could talk about a working VoIP solution for KDE4. AFAIK there is nothing working right now... :|"
    author: "floeschie"
  - subject: "Rygel and Coherence"
    date: 2010-02-03
    body: "Too bad I'm going to miss this. I like how the organizers put these competing projects back-to-back, should create some good drama. \r\n\r\nI suggest that Rygel gets a 5 minute rebuttal and then a 1 minute response from Coherence at the end. ;)"
    author: "eean"
  - subject: "KDE VoIp client solution"
    date: 2010-02-04
    body: "I think Sflphone ( http://www.sflphone.org/content/features ) could be considered the KDE SIP & IAX2 client."
    author: "lorenzofh"
  - subject: "VoIP ? Twinkle"
    date: 2010-02-05
    body: "I am using Twinkle daily, works like charm, does conf call with 3 people. Great software."
    author: "erichelle"
---
<img src="/sites/dot.kde.org/files/going-to.png" width="150" height="89" align="right" />

KDE will be at <a href="http://www.fosdem.org/">FOSDEM</a> this weekend, the largest gathering of free software contributors there is.  We will have <a href="http://www.fosdem.org/2010/schedule/devrooms/kde">a KDE devroom</a> on the Saturday with a packed programme of talks covering KDevelop, PIM, Designer and more.  On the Sunday we have been working with our friends at Gnome on a <a href="http://www.fosdem.org/2010/schedule/devrooms/crossdesktop">cross desktop schedule of talks</a>.  This will include semantic desktop, zeroconf, package installing and accessibility.  There will also be a KDE stall showing off the almost-final release of KDE Software Compilation 4.4.  See you there!
<!--break-->
