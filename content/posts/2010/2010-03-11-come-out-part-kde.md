---
title: "Come Out as Part of KDE"
date:    2010-03-11
authors:
  - "Stuart Jarvis"
slug:    come-out-part-kde
comments:
  - subject: "Slightly confused ..."
    date: 2010-03-12
    body: "It's a great idea to use labels to raise awareness of KDE and its technologies. However, reading the text labels and the longer explanations below, I am slightly confused: Initially, I thought ok, this is in ascending order: \"Powered by KDE\" means less involvement than \"Part of the KDE family\" which sounds, well, quite familiar. \r\nThe longer description below seeem to indicate that \"Powered by KDE\" expresses the strongest involvement with KDE. \r\nSo what's right?"
    author: "michbona"
  - subject: "Depends on what do you, and"
    date: 2010-03-12
    body: "Depends on what do you, and like to present yourself as:\r\n\r\nAs far as I see it:\r\n<ul>\r\n<li>Powered by KDE           = Made possible by KDE technology and people (e.g. getting help)</li>\r\n<li>Built on the KDE Plaform = Made with KDE tech</li>\r\n<li>Part of the KDE family   = We're part of the community.</li>\r\n</ul>\r\n\r\nThe first two are nice for developers, the last one is suitable for a broader audience."
    author: "vdboor"
  - subject: "In terms of strength..."
    date: 2010-03-12
    body: "In terms of strength of involvement with KDE, I'd say:\r\n\r\nPart of the KDE Family (strong)\r\nPowered by KDE (strong)\r\n\r\nBuilt on the KDE Platform (weak)\r\n\r\nI don't think there's a lot of difference between the first two (possibly family is a slightly stronger association)\r\n\r\nAs vdboor said, there's also the nature of the association:\r\n\r\nPowered... -> people or technology (and particularly if you want to emphasise both)\r\nBuilt... -> emphasises use of technology\r\nPart... -> emphasises part of the community"
    author: "Stuart Jarvis"
---
Part of the repositioning of the KDE brands was choosing an appropriate 'KDE Software Label' for developers working on applications outside the main KDE Software Compilation. Technology developed by KDE is used far and wide, as can be witnessed on <a href="http://kde-apps.org">kde-apps.org</a> and other sites. Some of these applications are developed by people closely entrenched in the KDE community, others by developers who just happened to like KDE technology and don't feel part of the KDE community in any big way. To allow authors to express their connection to the KDE community, the KDE community has chosen three appropriate labels.

We are also presenting the first draft of a guide for distributors of KDE software on how to integrate their communications with our brands.
<!--break-->
<h2>Acknowledging your relationship with the KDE community and its technology</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/map1_0.png" width="350" height="191"/><br />Finding the right words (via <a href="http://www.wordle.net/">Wordle</a>)</div>Back in December, <a href="http://dot.kde.org/2009/12/15/being-part-kde">the promo team asked for your opinions</a> on suitable KDE software labels. Following the <a href="http://kde-apps.org/poll/index.php?poll=251">poll results</a> these are the recommended labels:

<ul>
<li>Powered by KDE</li>
<li>Built on the KDE Platform</li>
<li>Part of the KDE family</li>
</ul>

<h3>'Powered by KDE'</h3>
This label encompasses the dual meanings of software that derives its strength from our fantastic community and from the KDE development platform. This label is recommended for all free software applications whose developers feel part of the KDE community or which make use of KDE technology.

<h3>'Built on the KDE Platform'</h3> 
This label has been chosen for the simplicity of the message it provides: this application uses the KDE platform. It is recommended for all application authors who want to express their use of KDE technology.

<h3>'Part of the KDE family'</h3> 
This label is recommended for authors of free software who in particular see themselves as part of the KDE community - and we hope anyone producing free software with the KDE Platform sees themselves this way.

<h3>Other terms</h3>

These are, of course, just recommendations. KDE is not going to dictate to anyone what labels they should use to describe their applications, but the community hopes that by providing these recommendations and (in due course) handy badges carrying these phrases, it will be easier for application authors to identify with KDE in a consistent and easily recognizable way. This makes the broad universe of KDE applications more readily apparent and allows third party developers to be part of the widely recognized KDE brand.

Developers may also like to call their free software applications "KDE software" or refer to their application with the prefix "KDE" - for example "KDE Kontact".

<h3>Call for artwork</h3>

To make these labels more easily usable, the promo team is calling for eye-catching artwork to carry, accompany or represent these phrases, for use on websites and perhaps in application splash and about screens where appropriate. Such images should be available in a small size (perhaps as small as 50x50 pixels) but larger variants are also needed. Please submit designs to the <a href="http://community.kde.org/Promo/Branding/Software_Label">dedicated page on the wiki</a>. Depending on the number of submissions the team may hold a poll to determine the best.

<h2>Guidance For Distros</h2>

The KDE promo team is also pleased to announce the first complete draft of the <a href="http://community.kde.org/Promo/Distribution_Communication">guide for distributors</a> of KDE software, helping them integrate KDE branding into their own.

The <a href="http://community.kde.org/Promo/Distribution_Communication">guide</a> provides examples for installation screens, website text and oral presentations. The promo team welcomes feedback from distributors to <a href="mailto:kde-promo@kde.org">the KDE-Promo mailing list</a> so that they can understand communication problems experienced by downstream communities and adjust their recommendations accordingly.