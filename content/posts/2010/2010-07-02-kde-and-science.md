---
title: "KDE and Science"
date:    2010-07-02
authors:
  - "Stuart Jarvis"
slug:    kde-and-science
comments:
  - subject: "General scientific applications are the way to go"
    date: 2010-07-02
    body: "I think, establishing a well integrated scientific workplace for the common workflow is the way to go. Every scientist has to deal with papers, thesises, ... \r\n\r\nKPapers seems like a *very good* idea to me, sth. like Amarok for papers, with integration into other applications (extracting/automatic downloading the BiBTeX entry and offering in in KBiBTex, extraction of graphs from pdfs, ...). Even little things like an easy setup for a common dictionary of typical terms of that subject for a whole workgroup for spell checking would be welcome.\r\n\r\nConcentrating on the process of doing literature research, and the typical workflow of coauthoring papers with other authors (version control integration, 1-click-sending of a preliminary version incl. sources via kmail to external coauthors and easy intergation of of the returned changes...) would be something I would expect from a scientific desktop environment.\r\n\r\n*Doing* the actual research is a too specialized task for a desktop environment, and there are already too many specialized and well established tools for each subject. Even if there's a KDE tool for that purpose you'll be often forced to use another one in a colaboration."
    author: "furanku"
  - subject: "LaTeX"
    date: 2010-07-02
    body: "I just ran the latest Kile 2.1 beta and was very happy to see the toolbar and menus are way cleaner. What prevents me from moving to Kile ate these point are mainly the lack of auctex-like capabilities. This means first keyboard shortcuts for most actions, and second a non-intrusive way of introducing options for these actions (like when doing a find). \r\n\r\nFor instance, we have two ways of introducing a tabular now. One is via menu, LaTeX->Tabular Env->Tabular. This just introduces the evnvironment with no options. No shortcut. The second way is via  Wizard->Tabular that introduces a big menu with lots of options, covering edit area while you choose them. \r\n\r\nThe auctex way is having shortcut for Env->Tabular, and getting asked for tabular options on a non intrusive bottom bar (just like when doing a find). When I have this, I will probably move to Kile. \r\n\r\nJ.A.\r\n"
    author: "jadrian"
  - subject: "Amarok for Papers"
    date: 2010-07-02
    body: "We need a good database of papers that makes it easy to retrieve paper by author, year, university or any other information. Should be possible to tag papers with keywords and create bibliographic information manually, automatically from info in the paper itself, or from online sources. And there should be plugins to take specialized advantage of external websites, such as citeseer, download new papers, find related ones, get statistics etc. \r\n\r\nSounds like a job for the amarok + okular guys. Considering how badly this is needed, universities should be giving you grants to develop something like that. \r\n"
    author: "jadrian"
  - subject: "Yes, we need kpapers"
    date: 2010-07-02
    body: "I completely agree that a program for conveniently working with literature would be both very helpful for many scientists using kde and also manageable without writing specialized code relevant for a particular field of science only. So this \"kpapers\" seems to be what we want ...\r\nI also agree that a strong bibtex support would be very welcome, something like kbibtex's way of supporting you in managing citations. In many ways, kbibtex could be used as a starting point for a kpapers.\r\n\r\nIt would probably be a good idea to create some kind of plugin system for interaction with various websites. Depending on your field of research, different sites will be relevant, and one can hardly expect the main developers to know all these sites. So if there is a modular system which can be easily expanded to access more websites for downloading preprints / bibliographic data / citations or other metadata, that could help in making this project useful for many.\r\n\r\nBut the biggest question obviously is: Are there any brave volunteers who want to make this vision reality?\r\n"
    author: "gg-lechner"
  - subject: "Matlab, Mathematica substitute"
    date: 2010-07-02
    body: "I think that something that might be very welcome by the scientific community is a a good FLOSS substitute for Matlab, Mathematica and related software. I have played a bit with Octave and looks like is up to the task. I have also played with qtOctave, and since it provides very good GUI experience, I feel is not there yet.\r\nI think providing a good integration of Octave in KDE Koctave maybe? might be a very good thing.\r\nAdditionally I personally use kjots a lot for papers/experiments note-taking. I think that the combination of kjots with Nepomuk-powered SemNotes might be a very attractive app. as well.\r\n\r\ngood to see KDE is approaching the scientific community\r\n\r\njaom7"
    author: "jaom7"
  - subject: "That would be certainly not"
    date: 2010-07-02
    body: "That would be certainly not me: If I start to code in C it always ends with me stabbig needles into vooodo dolls of K&R ... but if I try C++ I'm almost sure that Stroustrup has a voodoo doll of me. ;)"
    author: "furanku"
  - subject: "Try Cantor with the Maxima or"
    date: 2010-07-02
    body: "Try Cantor with the Maxima or sage (which again depends on Maxima for the CAS part) backend for a Mathematica replacement."
    author: "furanku"
  - subject: "Cross-platform"
    date: 2010-07-02
    body: "I'd rather not see too many KDE-specific tools created. Science isn't about one particular desktop, and I'd rather have good tools that work on different desktops and even operating systems. Having a polished text editor and PDF reader for the platform is good, though.\r\n\r\nFor reference management, see <a href=\"http://www.mendeley.com/\">Mendeley</a> (aimed at cataloguing PDFs) and <a href=\"http://www.zotero.org/\">Zotero</a> (works a bit like bookmarking, but with metadata for citations). Mendeley is proprietary, but given away for free and runs on Linux (it's written in Qt). Zotero is GPLed."
    author: "takowl"
  - subject: "not only scientists!"
    date: 2010-07-02
    body: "Please note that it is not only \"real\" scientists who have strong interests in scientific software for KDE.\r\n\r\nMost of the apps mentioned in this article are also useful to:\r\n\r\n - engineers\r\n - college/university students of science or engineering\r\n - high school students and teachers\r\n\r\nJust something to keep in mind while deciding what is most badly needed, or in what direction to go with specific apps, etc.\r\nAlso, the planned \"KDE Science Community\" should be open enough to include all of these people."
    author: "uetsah"
  - subject: "As a substitute to"
    date: 2010-07-02
    body: "As a substitute to Mathematica you really should use SAGE, it is   really good, plus scripting in python is a joy.\r\nYou can use it through the netbook interface in you browser, or in Cantor, whatever your choice it is still awesome."
    author: "Giulio Guzzinati"
  - subject: "Note that KDE applications"
    date: 2010-07-02
    body: "Note that KDE applications run fine on other Linux/Unix desktops (like, say, Gnome) and also on Windows and Mac OS X. There aren't really any \"KDE-specific\" applications, just applications built on the KDE Platform - they run great in a KDE Workspace, but they also run great on other desktops. In fact, that sort of wide cross-platform compatibility makes the KDE Platform an excellent choice for science apps for precisely the reasons you state IMHO."
    author: "Eike Hein"
  - subject: "I hadn't heard of Cantor"
    date: 2010-07-03
    body: "I hadn't heard of Cantor until reading this article, but use Maxima extensively. After trying out Cantor with Maxima I can highly recommend them."
    author: "abensonca"
  - subject: "Absolutely"
    date: 2010-07-03
    body: "I agree entirely. 'Scientist' is just an easy word to use, but of course this should be aimed at anyone who would find it useful to know about KDE science apps.\r\n\r\nEngineers? Absolutely. The college and high school sectors are also really important, I think"
    author: "Stuart Jarvis"
  - subject: "Kbibtex4"
    date: 2010-07-03
    body: "I too am waiting for a port of KBibtex4.  In the meantime, I have found that Zotero (http://www.zotero.org/) has a good mechanism for doing bibliographies.\r\n\r\nI am a physics student and I am very interested while playing around with Step.  The user interface could use a little more polishing, but overall it is a great little app."
    author: "iform"
  - subject: "Technical users?"
    date: 2010-07-03
    body: "What about using \"technical users\"?  I think that would cover both science and engineering."
    author: "todde2.718"
  - subject: "Existing infrastructure"
    date: 2010-07-03
    body: "Nepomuk/strigi already has the capability for full-text indexing.  Zotero, which is open-source, uses pattern-matching of some sort to extract relevant bibliographic fields from documents, so I think having nepomuk also have copies of this and use those to automatically scan documents for bibliographic information and store them as tags would work very well.  Similarly, zotero has ways of detecting whether a web page has bibliographic information and retrieving it, features which could be intregrated into konqueror.  Tellico, which is a third-party KDE app, already has ways to consult online databases for bibliographic information.  And a reference isn't fundamentally that different from an contact, so I think akonadi would provide a good mechanism for managing those.  Kio and dolphin part would make it fairly easy to browse the bibliographic database, and strigi would make it easy to search it and to find and record random papers scattered across your hard drive.  The okular part would make reading the papers easy.  So much of the pieces needed for a good bibliographic manager for KDE are already available, what it would take is for someone to take those pieces and put them together into an integrated unit."
    author: "todde2.718"
  - subject: "Focus on general tasks"
    date: 2010-07-03
    body: "I am cross-posting this from a previous comment, since I think it is relevant to the question of how to make KDE better for scientists.\r\n\r\nI think it would be most productive to ask what sort of tasks scientists currently do, and figure out how to make KDE better able to help in those tasks. There are, of course, many very subject-specific tasks, but there are also many general ones. Things like manipulating, organizing, and plotting data, doing statistics, handling citations, writing papers, doing simulations. \r\n\r\nThen there are basic tools used by wide ranges of scientists but not all. Mechanical system and circuit simulators aren\u2019t used just by mechanical and electrical engineers, for instance neuroscientists represent ion channels in neurons as electrical circuits, and many tissues are treated as simple mechanical networks. Nested categorization tools are used by numerous fields as I already mentioned.\r\n\r\nSo I know it is hard for scientists who are trained to specialize in their own field and tend to think about what they do in reference to that field, but I think the key to success in science for KDE is to figure out what general tasks are carried out by a wide variety of scientists and develop tools (or enhance existing tools) to be able to do those tasks. Basically, to take what individual scientists do, something they may see as very specific to their field, and to figure out the more abstract underlying task that is really shared by most scientists, then make an application that can do that in a flexible manner so scientists can tune it to their specific field.\r\n\r\nA general-purpose tool of this sort would be a nested categorization tool.  This tool would be able to display objects in nested categories, as well as taking various attributes of different arbitrary objects, calculating how closely related each one is, and grouping them. Kalzium already shows relationships between elements, someone else said they wanted to do something similar with subatomic particles. But this can also be used for star and planet types in astronomy, species in biology, proteins in molecular biology, drugs in pharmacology, rocks in geology, and more abstract data like plots and time series data. So instead of having a bunch of specialized tools categorizing specific objects in similar ways, having a tool to both lay out categories in an arbitrary manner for display as well as automatically categorizing data based on arbitrary rules would be an extremely useful tool for just about any area of science.\r\n\r\nAnother important task that all scientists do is track the individual subjects of their study. It might be individual humans, animals, stars, volcanoes, and or anything else people might want to study, but have a general tool that allows people to keep track of individual things that they are studying, tag their data with what subject it applied to and when, keep track of paperwork, schedule tasks that have to be carried out, encrypt sensitive data, and so. It would have to be flexible enough to handle arbitrary data fields and schedules and connect to files on the hard drive, but akonadi should provide the tools to carry this out. Pretty much any scientist could use a tool like this.\r\n\r\nA third general task is doing simulations. For instance the mathematics underlying mechanical systems like in step and circuits in electrical engineering are actually very similar, and can be applied to many other things as well (for instance predator/prey interactions). Further, circuits and mechanical systems are used as abstractions for things that are not really electrical circuits or simple machines, such as simulating neuron ion channels as electrical circuits or lung tissue as a simple machine. Making an underlying library and KDE tool that can be used for doing general simulations, and then building tools like step or a circuit simulator on top of such a library, would be really helpful. It would need to provide the ability to link differential equations, and to build re-usable objects that can be given their own symbol and used elsewhere, would be extremely useful to a huge variety of subjects. For instance a neuron ion channel is usually represented as a battery and a variable resistor in series. A neuron is often represented as a group of these ion channels and a capacitor in parallel. So someone could use a circuit simulator to build a simple ion channel object, whose resistance and battery voltage can be changed, and these can be grouped to form a neuron object, whose ion channels can each by changed, and then those can be grouped to form a collection of neurons. How gluon makes a graphical object-oriented programming system might be a good model for how a general simulation system could be made. It wouldn\u2019t have to be as complex as, say, finite element modeling, but having a general simulation tool which has mechanical and circuit simulator built on top of it as examples but which people could use for their own simulations would be extremely useful. GHNS could be used for exchanging objects, like simulations of popular integrated circuits or mechanical devices.\r\n\r\nThese are the sorts of general tasks that lots of scientists do.  I am not saying KDE should have these specific tools, although I think it would be good if it did, but rather this is the sort of approach to generalizing tasks done by scientists so that flexible tools with as wide a range of use as possible could be developed.  I think that is the key to success amongst scientists."
    author: "todde2.718"
  - subject: "DOI"
    date: 2010-07-04
    body: "one could also use the DOI that most articles come with nowadays to get a URL to scrape for bibliographic data as a check/alternative to Zotero looking at only just the document."
    author: "benajamin"
  - subject: "Python!"
    date: 2010-07-04
    body: "I used Matlab a lot during my PhD (physics), but am now switching to using Python instead and am finding Python to be much better in nearly all respects. For GUI stuff, I use pywxwidgets. I also considered Qt, but the bindings were problematic at the time I was starting - this is changing now as PyQt4 and PySide mature."
    author: "benajamin"
  - subject: "Has there been any more"
    date: 2010-07-10
    body: "Has there been any more discussion of this kpapers project / kde science website etc. at akademy? This would probably be the perfect place to look for some eager developers ... please keep us kde scientists informed about any news here."
    author: "gg-lechner"
  - subject: "Thanks"
    date: 2010-07-12
    body: "Greetings from GERMANY!!"
    author: "kaiserdeizisau"
  - subject: "One thing only tethers me to Windows: reference management"
    date: 2010-07-12
    body: "For almost everything there is an equivalent or superior Linux (and usually KDE) alternative. Except seamless integration of references into a word-processing document. Which is why I'm forced to fire up VirtualBox whenever I'm working on a paper. I see two solutions to this:\r\n\r\n1. A convenient and reliable method for converting manuscripts between LaTEX and DOC formats. This would allow me to use the various TEX tools yet still collaborate with colleagues who use Microsoft Office.\r\n\r\n...or...\r\n\r\n2. A way to duplicate the functionality of EndNote, ProCite, or Reference Manager in OpenOffice (or some other FOSS office suite). The functionality I depend on that no FOSS tool currently provides is the ability to automatically insert, format, and re-number in-line references and bibliographies those references link to. Organizing PDF files is much less of an issue for me than this is.\r\n\r\n\r\nI've had colleagues ask me whether they should migrate to Linux, and I have to qualify my recommendation of it with the above problem. For them, like for me, this is a bigger hurdle than \"user friendliness\" or availability of useful scientific software."
    author: "bokov"
  - subject: "For management of PDFs of"
    date: 2010-07-13
    body: "For management of PDFs of papers I would recommend taking a look at Mendeley (Linux/Windows), which is free and Qt-based, though not open source.\r\n\r\nI agree with you that formatting/ numbering of references is an issue. You may be able to write a Python script for renumbering - I have done this in Word (as I do not possess Endnote etc.).\r\n\r\nThe other area of interest for me is Electronic Laboratory Notebook software. My continuing experiments with writing a 'Knowledge' database were written with this in mind, and not just bibliography. Field types for graphs, data spreadsheets and equations could in principle be written."
    author: "agkdb"
  - subject: "Actually Zotero pretty much fits the bill."
    date: 2010-07-13
    body: "After reading about Zotero on this site, I tried it, and found a very EndNote/RefMan-like tool that integrates into OpenOffice exactly as described. The current version seems to crash on Ubuntu when the bookmark format is used for references (known bug, apparently to be fixed in the next release), and it's not clear what they mean by the footnote/endnote distinction, but heck, it's head and shoulders over everything else I've ever tried since migrating from Windows. Good job, Zotero team, and thanks to you folks for tipping me off to this excellent tool."
    author: "bokov"
  - subject: "Mendeley"
    date: 2010-07-15
    body: "Thank you for the tip! I searched a lot for something like this! Yes a kde app would be better but it sure does the trick for now."
    author: "jadrian"
  - subject: "Using KML"
    date: 2010-07-16
    body: "I would like to bring the attention of kde developpers and especially marble developers to the following paper:\r\nRepresenting Scienti\ufb01c Data Sets in KML: Methods\r\nand Challenges\r\nLisa M. Ballagh, Bruce H. Raup, Ruth E. Duerr,\r\nSiri Jodha S. Khalsa, Christopher Helm, Doug Fowler,\r\nAmruta Gupte\r\ndoi:10.1016/j.cageo.2010.05.004\r\n"
    author: "lacsilva-2"
---

Free thinkers. Curious people collaborating across borders. Pioneers pushing back the boundaries of what is possible. Teams building upon the work of others. People trying things just to see what happens.

Those are all phrases that could be applied to KDE - or to scientists. The scientific mindset shares a lot with that of free software and so it is no surprise that there are plenty of scientists within our community, nor that KDE has some strong applications in the world of science.

A few days ago, your writer did a quick, albeit decidedly unscientific, <a href="http://www.asinen.org/2010/06/calling-kde-scientists/">survey</a> to gather some thoughts of the KDE scientist community on our current software offerings and the missing links. Here are a few of the conclusions.

<h3>Popular Science Applications</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/rkward_dot.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/rkward_dot_sml.png"></a><br/>RKWard, a KDE R environment</div><a href="http://kile.sourceforge.net/">Kile</a> and <a href="http://okular.kde.org/">Okular</a> seem to be real favorites. No surprise really, as the activities common to every scientist are writing and reading scientific papers. <a href="http://www.unix-ag.uni-kl.de/~fischer/kbibtex/">KbibTeX</a> is also popular for organizing references, despite lacking a Platform 4 version.

<a href="http://kate-editor.org/">Kate</a> is popular among scientists, apparently finding favor for everything from note taking to coding and drew praise for its syntax highlighting.

Relative newcomer <a href="http://edu.kde.org/cantor/">Cantor</a> generated a lot of enthusiasm with several current users and many more interested in giving it a go in the future. The already existing R front end and IDE <a href="http://rkward.sourceforge.net">RKWard</a> is also used by several KDE enthusiasts working in the sciences.

<h3>Less Well Known Applications</h3>

Many commenters were not aware of all the applications that had been listed, while others suggested applications that had not made the initial list. <a href="http://labplot.sourceforge.net/">LabPlot</a> in particular was not that well known, although many respondents said it might be useful to them. This may be because the project has been, outwardly, quite quiet for a while as a lot of work is done behind the scenes on the Platform 4 version and sharing of code with the Qt <a href="http://scidavis.sourceforge.net/">SciDAVis</a> project.

<a href="http://edu.kde.org/rocs/">Rocs</a> and <a href="http://edu.kde.org/step/">Step</a> were also not well known, though considered to be potentially useful, by a number of respondents.

<h3>Plasma Workspaces and the KDE Platform</h3>

Proving that not everyone is scared of <a href="http://pim.kde.org/akonadi/">Akonadi</a>, biomedical engineer and informatics PhD student Daniele from Italy has a “<i>plan to migrate to KMail as soon as it gets fully integrated with Akonadi</i>”.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/desktop_0.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/desktop_sml.png"></a><br/>A typical KDE scientist's Plasma Desktop?</div>Some respondents are already using <a href="http://nepomuk.kde.org/">Nepomuk</a> to track projects, tag papers (or to extract meta-data from them) and even to rate student submissions. There was general enthusiasm for seeing more use of Nepomuk.

The Plasma workspace activities also appear popular with scientists, with several reporting that they have an activity or folderview set up for each project on which they are working. KRunner was mentioned as a useful tool for quick calculations and unit conversions. Similarly, many people reported using Plasma widgets, from using the Remember the Milk and Qalculate widgets to keeping up to date with <a href="http://www.phdcomics.com/">PhD Comics</a> and <a href="http://xkcd.com/">xkcd</a>.

While many people seem to recognize the benefits of using the KDE Platform to get access to technology such as Nepomuk and the Get Hot New Stuff framework, there is also a strong desire for cross-platform software so that teams using different operating systems can interact easily. For this reason some people have, in the past, tended to favor writing applications in pure Qt rather than using the KDE Platform.

<h3>Improving KDE Software for Scientists</h3>

Physicist  and mathematician Jack Summers leant his support to the interesting <a href="http://kde-apps.org/content/show.php/Kpapers:+idea+looks+for+developers!?content=58990">KPapers</a> concept to develop a new kind of reference manager. There was  widespread enthusiasm for KbibTeX 4 or its successor to take full advantage of Platform 4 technologies such as Nepomuk and integrate better with KOffice, Kile and Okular.

Many scientists seem to want a simple IDE for their chosen scripting language, with interest in Python in particular. Some, such  as Luca Beltrame (Italian bioinformatician, better known in KDE land as one of the forces behind KDE forums) would like to see better support  for such languages in a full IDE. However, Chris Warburton (a Computer  Scientist in the UK) prefers a simple approach: "<i>my favourite IDE is  Geany. A fullscreen text editor window with syntax highlighting, code  folding, auto-indent and a big 'Execute' button is all that’s needed for  scripting languages, and a KDE app like that would be great (something  like Kwrite with an Execute button)</i>". There were also calls for syntax  highlighting of FORTRAN (Luis, working in Leeds, UK)

There is interest in a KDE graphical application to compete with the likes of MatLab, Mathematica and similar proprietary software, and many felt that Cantor has potential to meet some of their needs in this respect.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/labplot-1.6-3d_1.png"><br/>LabPlot: a less well known KDE science application</div>There are also a  wide range of formats and third party services for which many scientists would like to see import and export capabilities. Given the range of requests, this may be best implemented through a plug-in system. <a href="http://kommander.kdewebdev.org/">Kommander</a> is missed as a means for quickly making graphical KDE applications.

<h3>Building a KDE Science Community</h3>

There seems to be a lot of enthusiasm for drawing together scientists (and other users of scientific software). Many people expressed an interest in either web pages dedicated to giving an overview of applications relevant to scientists or a mailing list, although the success of the latter would depend on a sufficient volume of relevant traffic.

Many of the respondents were physicists, computer scientists, engineers and people working in biological sciences. Maths and social sciences were also well represented with a few chemists and environmental scientists.

Several applications that scientists reported  using have uses outside the scientific community - for example Okular, Marble, Krita, coding tools and Akregator (for subscribing to RSS feeds of journals). Therefore, although some people called for a dedicated KDE module providing scientific applications (along the lines of KDE Edu) it could be difficult to define a list of applications that would belong there. A traditional module following the release pattern of the Software Compilation would also be unlikely to suit many of the application teams who currently enjoy the freedom of following their own schedule.

<h3>Next Steps</h3>

What do you think is the best way for KDE to better meet the needs of scientists and attract new users from among the scientific community?

If you will be at Akademy, come along to the <a href="http://akademy.kde.org/node/559">lightning talk</a> (Sunday at 1030), the <a href="http://community.kde.org/Events/Akademy/2010/Tuesday">bioinformatics BoF</a> (Tuesday, 1500) and the <a href="http://community.kde.org/Events/Akademy/2010/Wednesday">KDE Software for Scientists BoF</a> (Wednesday, 1030) to explore the issues and play your part. If you can't make it to Akademy, make a comment below or watch out for further news on the Dot and Planet KDE.