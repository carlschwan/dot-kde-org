---
title: "KDE Marble at INTERGEO"
date:    2010-10-12
authors:
  - "bholst"
slug:    kde-marble-intergeo
comments:
  - subject: "KDE-science"
    date: 2010-10-12
    body: "There were a number of requests from the KDE-science project to have these sorts of capabilities in KDE, either through marble itself or through a marble-based program.  So it is good to see the marble developers actively discussing this with people in the field.  \r\n\r\nIt would be helpful if the marble developers keep the KDE-science project appraised of ongoing developments in this area.  This is exactly the sort of thing the KDE-science project was set up for in the first place.  \r\n\r\nIf developers are going to be showing KDE off at a conference or trade show related to science, engineering, mathematics, or some other technical subject (besides pure software engineering), I think it would be beneficial to everyone if the notice is forwarded to the KDE-science mailing list."
    author: "toddrme2178"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEMarbleOSGeo.JPG" /><br/> Toni, Bastian, Astrid and Harald from the OSGeo<br />Booth</div>In the Free Software ecosystem, nearly everybody has heard about KDE. People associate us with a great desktop environment and some interesting applications. On other desktops there may be installations of KDE software, but those people may not know a single KDE application. This is why Torsten Rahn and Bastian Holst went to <a href="http://www.intergeo.de/en/englisch/index.php">INTERGEO</a> this year to present <a href="http://edu.kde.org/marble/">Marble</a>.
<!--break-->

INTERGEO is the world's largest event for geodesy, geoinformation and land management with a large number of exhibitors and a lot of visitors. This year it took place from October 5th to October 7th in Cologne, Germany. Luckily, the Marble team was invited to present its software at the booth of <a href="http://www.osgeo.org/">OSGeo</a> and its German representative FOSSGIS e.V.

We presented the KDE-Edu Marble application and its library on October 6th and 7th with two talks and live demonstrations at the booth for individuals. We also provided colorful Marble handouts to interested visitors. 

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/BastianMarbleDemo.jpeg" /><br/> Bastian showing Marble to a visitor</div>A lot of people came to our booth and were interested in what is possible with our software. Many of these people saw KDE software for the first time; others already knew about KDE and were interested in what has changed during the past few years.

All in all it was an interesting experience to present KDE software to a professional audience with a completely different focus. The Marble team will certainly visit this and similar events in the next year. You are invited to join us there.

We also encourage other KDE application teams to try a similar approach to promote our software.