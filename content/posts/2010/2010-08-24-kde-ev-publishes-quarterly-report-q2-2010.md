---
title: "KDE e.V. Publishes Quarterly Report for Q2 2010"
date:    2010-08-24
authors:
  - "sebas"
slug:    kde-ev-publishes-quarterly-report-q2-2010
---
KDE e.V., the organisation supporting KDE has published the <a href="http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf">quarterly report for the second quarter of 2010</a>. The report covers many KDE events held all over the world, such as the Brazilian Akademy-BR, which was held this year for the first time. Akademy-ES, the Spanish KDE conference is another topic of the report. The report also holds information such as new additions to the KDE e.V. membership and its financial status.
If you like the work you see in the report, consider <a href="http://jointhegame.kde.org">Joining the Game</a> by becoming a supporting member of the KDE e.V.. KDE contributors are of course welcome to <a href="http://ev.kde.org/getinvolved/members.php">join</a> the KDE e.V. as active members.