---
title: "Credativ Develops Two Groupware Resources for Akonadi"
date:    2010-01-23
authors:
  - "liquidat"
slug:    credativ-develops-two-groupware-resources-akonadi
---
<a href="http://credativ.com">Credativ</a> has supported the development of two more Akonadi resources - bringing more Groupware resources to upcoming releases of the KDE PIM suite. <a href="http://blog.credativ.com/2010/01/bringing-groupware-to-kde-akonadi-resources-for-open-xchange-and-groupdav/">Read here for a quick Q&A about this</a>.
