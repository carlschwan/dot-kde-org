---
title: "Opinion: Test Article"
date:    2010-10-26
authors:
  - "Stuart Jarvis"
slug:    opinion-test-article
---
Test article

With some content of course

etc
etc
etc

<p style="width: 100%; border-top: 1px dotted grey; color: grey; font-size: 0.6ex;">KDE.News <a href="http://dot.kde.org/category/dot-catgories/opinion">Opinion</a> articles represent only the view of the author on a particular issue and should not be interpreted as reflecting KDE's official position</p>