---
title: "KDE Releases Software Compilation 4.4.2"
date:    2010-03-30
authors:
  - "sebas"
slug:    kde-releases-software-compilation-442
comments:
  - subject: "kde distro without bug"
    date: 2010-03-31
    body: "I am using Debian with kde 4.4 but it still have this problem https://bugs.kde.org/show_bug.cgi?id=156475 with duel screen settup. I can see that the problem has been fix and therefore I would like to know if you know a distro where the bug is gone "
    author: "beer"
  - subject: "DUAL HEAD"
    date: 2010-03-31
    body: "Hi Beer, I don't use dual head (can't afford two monitors ;)) but you could try openSuse since it always provide the most up-to-date KDE packages. \r\nDebian is a bit slow to adopt new releases since it's a very stable distro. Not meaning that openSuse isn't stable, it is but the devs put out a lot of effort to give the users the latest and greatest stable packages. "
    author: "Bobby"
  - subject: "The red ball"
    date: 2010-03-31
    body: "Hi KDE guys and girls. First of all thanks for taking time out to make another update possible. \r\n\r\nI am very happy and contented with KDE 4 but I have one little request: Could you please make it possible for the red ball to bounce even when the plasmoids are locked? I mean, a locked ball somewhere on the monitor looks really stupid. Apart from that the purpose of the ball is to bounce and there are people who would like to lock the plasmoids but have the ball bouncing without the setting or configuration arm (don't know what's the name). This was possible in earlier KDE 4 version but somewhere along the line the ball got locked and isn't fixed up to now."
    author: "Bobby"
  - subject: "would you recommend opensuse"
    date: 2010-03-31
    body: "would you recommend opensuse 11.2 or opensuse 11.3 (factory)?"
    author: "beer"
  - subject: "New KDE release, new \"duel\""
    date: 2010-03-31
    body: "New KDE release, new \"duel\" post. :-)"
    author: "Haakon"
  - subject: "I think you mean DUEL head"
    date: 2010-03-31
    body: "..."
    author: "dalgaro"
  - subject: "Oh yes, right, that's what I"
    date: 2010-03-31
    body: "Oh yes, right, that's what I meant. Sorry my German is not so good ;)"
    author: "Bobby"
  - subject: "With Gnome 3.0"
    date: 2010-03-31
    body: "With Gnome 3.0"
    author: "Bobby"
  - subject: "OpenSuse 11.2 is the stable"
    date: 2010-03-31
    body: "OpenSuse 11.2 is the stable version. 11.3 is still in Alpha stages and is not suitable for daily use, it's only for testers. You will even have problems with installing graphic drivers if you use 11.3."
    author: "Bobby"
  - subject: "Arch Linux has the 4.4.2"
    date: 2010-03-31
    body: "Arch Linux has the 4.4.2 already in the repos. I use two screens and it seems to work fine even on KDE 4.4.1. I have an NVIDIA card though and set it up so it uses TwinView not two xscreens, so your setup may be different..."
    author: "Mark Watson"
  - subject: "I love the red ball!!!!!"
    date: 2010-03-31
    body: "I love the red ball!!!!!"
    author: "Mark Watson"
  - subject: "KDE 4.4.2 on openSUSE 11.3 Milestone 4"
    date: 2010-04-01
    body: "Hi,\r\n\r\ni use the KDE 4.4.2 on openSUSE 11.3 Milestone 4 64 Bit and\r\nit runs stable."
    author: "bernasek"
  - subject: "On to something"
    date: 2010-04-01
    body: "As someone that does not really care one way or another about the red ball, the request kind of makes sense. Could be because I prefer locked plasmoids as my default setting.\r\n\r\nThat said, I think this complaint shows that the KDE developers are on to something. If this is the things user complain about, it kind of underline the quality of KDE SC and how good it has become.\r\n\r\nSo I will just add my thanks to all of KDE. \r\n"
    author: "Morty"
  - subject: "yes my setup is different. I"
    date: 2010-04-01
    body: "yes my setup is different. I have 3 screens where the left and right screen is powered by 1 card and the middle screen is powered is powered by one card as well.\r\nThe nvidia-setings (a front end to xorg.conf) will not allow me to set all 3 screens to use twinview and I dont have the skills to dig into xorg.conf.\r\nI have grown tp like to have 3 separated xscreens "
    author: "beer"
  - subject: "I have tried to install"
    date: 2010-04-01
    body: "I have tried to install Opensuse 11.2 (the kde cd version) but early on the screen vent black and stayed so. Maybe I should tru 11.3 when it is ready"
    author: "beer"
  - subject: "OpenSuse 11.3 M4 with KDE 4.4.2"
    date: 2010-04-01
    body: "It works well and I can hardly see a difference between M4 and the present stable OpenSuse 11.2 BUT the fact is that OpenSuse 11.3 M4 is still Alpha/Beta software and is NOT recommended for daily use. It is for testers. \r\nI am almost sure that Beer needs a stable operating system since he is presently using Debian."
    author: "Bobby"
  - subject: "Yes they did"
    date: 2010-04-01
    body: "The KDE team did a pretty good job up to now. I just can't find anything else to pick on ;) \r\nYes it's really a very good quality software but I would still like them to fix the red ball. Just this one little wish please. I would even invite you all for dinner if you could do that for me :)\r\nWhere is Aaron? I know that that would only take 10ms of your time ;)"
    author: "Bobby"
  - subject: "What video card do you have?"
    date: 2010-04-01
    body: "What video card do you have? Maybe you should have tried the DVD version instead of CD. If you have a test box then you could install and try openSuse 11.3 M4 but i wouldn't advise you to install it on your productive system.\r\nI mean, crazy people like me do it but it's not recommended :)"
    author: "Bobby"
  - subject: "It is an old gf 8600 and it"
    date: 2010-04-01
    body: "It is an old gf 8600 and it works nice in both debian stable and debian oldstable.\r\n\r\nWhat are extra on the dvd version that one could need?"
    author: "beer"
  - subject: "I know that there are more"
    date: 2010-04-01
    body: "I know that there are more packages on the DVD. Not sure if that has anything to do with the graphics but from my experience I have always had a better installation  using the DVD.\r\nYour video driver should work on 11.2 but you will have to install the nvidia driver to get good performance. "
    author: "Bobby"
  - subject: "Normally I was using debian"
    date: 2010-04-02
    body: "Normally I was using debian testing eka the developing version of debian. But because if the bug in kde that I have already mention I am stuck with stable unto I have found something that works for me.\r\n\r\nhow stable/usefull are opensuse developing branch compared to Debian testing?"
    author: "beer"
  - subject: "I tried again with the DVD. I"
    date: 2010-04-02
    body: "I tried again with the DVD. I tried to install it in \"text mode\" with every single kernel the installer had to offer and I run into this problem:\r\nhttp://uni-one.adsl.dk/problem.jpg"
    author: "beer"
  - subject: "Stable enough"
    date: 2010-04-02
    body: "OpenSuse 11.3 M4 is not far behind the present 11.2 stable version. Since you are using a development version of Debian then it shouldn't be a problem to test OS11.3 M4. You will have to manually install the nVidia driver after the installation but that shouldn't be a problem. "
    author: "Bobby"
  - subject: "Why text mode?"
    date: 2010-04-02
    body: "I never install a Linux system in text mode. Install it in graphic mode and let the system automatically detect and configure your hardware. OpenSuse is VERY easy to install"
    author: "Bobby"
  - subject: "I tried to install opensuse"
    date: 2010-04-02
    body: "I tried to install opensuse 11.2 in \"graphic mode\" but I ended up with a black screen every time. Therefore I did choice to try to install it via text mode so I could see what when wrong. Unfortunate it did not give my any clue \r\n\r\nIf Opensuse is so easy to install why does it stop in the beginning just after loading the kernel? \r\n\r\nOn my test laptop Debian is easyer to install since it dos not stop/halt at any steps and detect my hardware."
    author: "beer"
  - subject: "It sounds like something is"
    date: 2010-04-02
    body: "It sounds like something is wrong with the DVD. It's recommended to burn iso images at the slowest possible speed.\r\nI can't relate to the problems that you mentioned and I have installed openSuse on more than one machines. Believe me, openSuse is easy to install. Even my brother who installed an OS for the first time in his life could do it.\r\nThere must be a problem with the iso image or the DVD."
    author: "Bobby"
  - subject: "maybe it is the DVD there is"
    date: 2010-04-02
    body: "maybe it is the DVD there is something wrong with but then I would expect an error that toled me that it could read from it. I have also tried to cd version of opensuse with the same behavior. And that CD did not have the same problem on my primary mashine. Maybe it is open suse that do not like my test mashine"
    author: "beer"
  - subject: "Hi,\nA broken cd is hard to"
    date: 2010-04-03
    body: "Hi,\r\n\r\nA broken cd is hard to detect - I would recommend to use K3B's \"check cd after burning\" function, and check the md5 of the downloaded file (again something k3b can do)..."
    author: "jospoortvliet"
  - subject: "Packages for Mandriva Linux 2010"
    date: 2010-04-03
    body: "Packages for Mandriva 2010 have just become available (both 64 and 32-bit). <a href=\"http://jlp.holodeck1.com/blog/2010/04/03/kde-sc-442-for-mandriva-linux-2010/\">More info here</a>."
    author: "JLP"
---
Today, the KDE team brings you <a href="http://kde.org/announcements/announce-4.4.2.php">another update</a> to the 4.4.0 development platform, applications and workspaces. Several bugs, some of them crashers have been fixed, so 4.4.2 will feel nicely stable.
As usual, the monthly updates do not contain new features, but concentrate on updated translations and bugfixes. KDE SC 4.4.2 is a recommended update for everyone using KDE SC 4.4.1 or earlier versions. Some distributions are providing packages already, others will follow shortly.
The <a href="http://www.kde.org/announcements/changelogs/changelog4_4_1to4_4_2.php">changelog</a> has some, but not all fixes that have gone into KDE SC 4.4.2.  Information how the source packages can be found is available on the <a href="http://kde.org/info/4.4.2.php">info page</a> corresponding to this release.