---
title: "Important Change to KDE's Security Policy"
date:    2010-02-15
authors:
  - "jefferai"
slug:    important-change-kdes-security-policy
---
<a href="http://www.kde.org/info/security/policy.php">KDE's Security Policy</a> has had a small update. The change is as follows:

<ul>
<li>Information about security vulnerabilities will no longer be published via the Dot at http://dot.kde.org.
</ul>

This change was made to reflect actual practices. Although the security policy has stated that security information would be published via the Dot, a review has shown that this has rarely occurred due to confusion as to whether the Dot is the appropriate venue for this information. This can mislead users into believing that following the Dot provides them with pertinent security information.
<!--break-->
Most users obtain KDE via distributions, which receive advance notification of security information via the private KDE Packagers mailing list. For those that compile from source, or who are simply interested in knowing about security vulnerabilities in KDE software, this information is already disseminated via two public venues: the <a href="https://mail.kde.org/mailman/listinfo/kde-announce">kde-announce mailing list</a> and <a href="http://www.securityfocus.com/archive/1">the BugTraq full disclosure list</a>.

As always, we ask users with information about possible security vulnerabilities to responsibly disclose this information to the security team at securi<span>ty@kd</span>e.org, so that patches can be readied before public disclosure.