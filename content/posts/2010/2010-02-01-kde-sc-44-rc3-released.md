---
title: "KDE SC 4.4 RC3 Released"
date:    2010-02-01
authors:
  - "sebas"
slug:    kde-sc-44-rc3-released
comments:
  - subject: "fetched - build starting"
    date: 2010-02-01
    body: "I already fetched the sources via the Gentoo overlay (kde-testing), and now it just started building kde-env. \r\n\r\nMany thanks for all the enhancements - and thank you for making sure that I can contribute to saving friends of mine from the small nuisances which commonly accompany huge changes. \r\n\r\nYou're great! "
    author: "ArneBab"
  - subject: "thanks for one more rc"
    date: 2010-02-01
    body: "I hope kde 4.4 will be free from show stopper bugs like this one https://bugs.kde.org/show_bug.cgi?id=224600 should be fixed; this bug affect all fullscreen applications (vlc, smplayer, dragon, okular, firefox, konqueror), bug is \"panel overlaps the fullscreen application\"; earlier, I thought it was VLC fullscreen bug; but later found out that it's plasma bug caught with firefox, smplayer.\r\n\r\nthanks for polishing kde 4.4 to such great extent! "
    author: "fast_rizwaan"
  - subject: "I can understand that you'd"
    date: 2010-02-02
    body: "I can understand that you'd like to see this bug fixed, but personally, I wouldn't call it a 'show stopper'"
    author: "borker"
  - subject: "awesome"
    date: 2010-02-02
    body: "With Chakra KDEmod-testing (KDE Repo for Archlinux) i was able to update at 00:00 CEST and it just works awesome\r\n\r\nA lot of bugs (really annoying bugs) was fixed =) thx"
    author: "Vamp898"
  - subject: "For me there is still a show"
    date: 2010-02-02
    body: "For me there is still a show stopper to use kde 4 (not only 4.4 but the whole series)\r\nEvery time I try I get 2 out of my 3 screen there is black. \r\nhttps://bugs.kde.org/show_bug.cgi?id=156475 <-   Aaron J. Seigo says it is something to do with dbus and xorg. It still works with kde 3.5 (I might be the last person to switch to kde 4 because of the screen problem)\r\n"
    author: "beer"
  - subject: "show stopper"
    date: 2010-02-02
    body: ">I can understand that you'd like to see this bug fixed, but personally, I wouldn't call it a 'show stopper'\r\n\r\nleterally, the users who play vlc would feel annoyed with the \"panel over the video\"; hence that will stop the video show :-)\r\n\r\napart from that, reviewers will use these kind of bugs to \"belittle\" kde's efforts.\r\n\r\n"
    author: "fast_rizwaan"
  - subject: "Older showstopper"
    date: 2010-02-02
    body: "Proxies don't work correctly.  That means that anything that uses kio_http (or kio_*) doesn't work.  And it's older than yours :)\r\n\r\nKonqueror, akregator, etc.\r\n\r\nhttp://bugs.kde.org/show_bug.cgi?id=155707"
    author: "Berend De Schouwer"
  - subject: "yeah, but....."
    date: 2010-02-02
    body: "> leterally, the users who play vlc would feel annoyed with the \"panel over the video\"; hence that will stop the video show :-)\r\n\r\ntouche ;)\r\n\r\n\r\nSeriously though, yeah its an issue and it's certainly a visible one (in multiple senses) but it's not like it crashes the desktop or anything. Definitely in the 'annoyance' category rather than the 'show stopper' for mine. "
    author: "borker"
  - subject: "Konqueror"
    date: 2010-02-03
    body: "Konqueror seems to have regressed in the 4.4 series.  It has trouble rendering common sites that it easily rendered in the past.  For instance, it has trouble displaying linuxtoday.com correctly.  It will display only a little of the first page most times.  And on the chance that it displays the first page correctly, if I click on any of the news stories, the page will not display and the progress bar gets stuck at around 25%.  I also frequent pclinuxos.com, and it will randomly crash when I'm in their forums section.  Anyone else having issues?"
    author: "cirehawk"
  - subject: "yes, it can't render gmail,"
    date: 2010-02-03
    body: "yes, it can't render gmail, google docs, facebook, hyves and many other modern sites properly. And crashes a lot.\r\n\r\nI'm looking forward to having WebKit in Konqi... Will probably have to wait until 4.5 :("
    author: "jospoortvliet"
  - subject: "Rekonq"
    date: 2010-02-03
    body: "If you want a KDE Webkit based browser you can use Rekonq for now. I hope nokia finds some value in it and decides to support it because I'd love to get rid of Firefox eventually."
    author: "jadrian"
  - subject: "Konqueror"
    date: 2010-02-03
    body: "That's too bad.  Although it has never rendered all sites correctly (especially the bill pay section for my online banking), I love Konqueror because it feels light and is well integrated with KDE.  I would use Firefox only in the few times Konqueror had a problem with a site.  Unfortunately, I find I have to use FF a majority of the time now.  :(  I have tried Reqonq as was suggested.  It renders things correctly, but the UI is pretty bad in my opinion.  I guess I'll be using FF now."
    author: "cirehawk"
  - subject: "rekonq does not use kwallet"
    date: 2010-02-03
    body: "well... that's what's hindering me from using it... I have a lot of account details, passwords and such stored in kwallet. "
    author: "thomas"
  - subject: "rekonq"
    date: 2010-02-03
    body: "Yeah, I was playing around with it a bit and noticed that.  I could probably get used to how rekonq works, but I agree it really needs to utilize kwallet."
    author: "cirehawk"
  - subject: "CPU usage has grown quite a bit too."
    date: 2010-02-03
    body: "Almost every time I've browsed the \"internets\" more than 10 minutes with couple of tabs open (khtml). I've had to kill the Konqueror process in the end except when just reading the lwn.net. It eats 50% of the CPU (there're many bug reports) and becomes unusable. QtWebkit (kdewebkit) is almost worse. It just freezes the whole browser if there're more that 4 tabs or couple of sites opening at once.\r\n\r\nKonqueror 4.3 worked much better IMO.\r\n\r\nOther than slightly broken web experience KDE 4.4 is pure awesomnesss! :)\r\n\r\nI use Konqueror myself as much as possible, but objectively just to piss someone off should it be considered moving Konqueror to extragear? I mean 3000 bugs, broken web and a bit uninspiring app that hasn't done anything innovative in ages is not that good PR. Seems like it's losing a lot on a feature front compared to other browsers these days and frankly it just kind of feels like it has been on this mainentace mode for ages now. \r\n\r\nI wonder if some kind of bounty for Konqueror, KHTML and KJS like krita had would be successful..."
    author: "bliblibli"
  - subject: "plasma-desktop or kwin?"
    date: 2010-02-04
    body: "it's a window management issue, so it may or may not be a plasma-desktop bug. you should test with another window manager to eliminate kwin as the culprit (i've added a comment to the bug report saying the same thing, btw)."
    author: "aseigo"
---
With the number of fixes still going into the 4.4 branch after RC2 last week, the release managers have decided to pop another release candidate in between. Last night, Dirk tagged <a href ="http://kde.org/announcements/announce-4.4-rc3.php">KDE Software Compilation 4.4 RC3</a>, and we're releasing it today already. Overall the release is shaping up nicely, with many bugs still being squashed even in this late phase. Thanks everybody for participating in making KDE SC 4.4 a solid release. 
Meanwhile, RC3 is trickling through to our mirrors. Packagers didn't have time to build packages yet, as we're basically attempting to get the release candidates out as quickly as possible to shorten bug-hunting cycles. Please give this RC another whirl and report bugs to us using <a href="http://bugs.kde.org">bugs.kde.org</a>.