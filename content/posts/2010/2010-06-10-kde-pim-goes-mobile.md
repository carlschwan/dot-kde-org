---
title: "KDE PIM Goes Mobile"
date:    2010-06-10
authors:
  - "steveire"
slug:    kde-pim-goes-mobile
comments:
  - subject: "sounds fantastic"
    date: 2010-06-10
    body: "I just got an N900 yesterday.  I looked on the maemo forums for syncing methods with KOrganizer.  It didn't look all that hopeful.  Now, I come across this post, which is great!  I am looking forward to the KDE design beauty and local caching and control to my data.  Keep up the good work!"
    author: "thewtex"
  - subject: "Where did you get the idea..."
    date: 2010-06-10
    body: "...for this linear treeview? Is that your original design or has it been inspired from somewhere else?"
    author: "majewsky"
  - subject: "Is is using Akonadi/Virtuoso"
    date: 2010-06-11
    body: "Is is using Akonadi/Virtuoso server as a database backend? I can't imagine running it on ARM phone."
    author: "dagger"
  - subject: "As far as I know Nuno and"
    date: 2010-06-11
    body: "As far as I know Nuno and Bjorn came up with the idea. We've been through a few different versions of it now. I think the current one works quite well, but there is still room for improvement."
    author: "steveire"
  - subject: "The mobile phone in the video"
    date: 2010-06-11
    body: "The mobile phone in the video is running a MySql server, and Akonadi server, a Nepomuk server, and a Virtuoso server, so yes, all this stuff does work.\r\n\r\nWe haven't started optimizing the configuration for the phone yet, but even now the services don't consume unrealistic resources."
    author: "steveire"
  - subject: "High resource usage"
    date: 2010-06-11
    body: "Hi,\r\nthx for porting kdepim to the N900.\r\nI tried the snapshot yesterday, but as it is now, it is to hungry for resources.\r\nEven when kmail is not running, the N900 becomes unresponsive. Unlocking screen takes me some tries untill it reacts.\r\nI seem this is because of nepumuk, virtuoso keep running in background.\r\nFor now i uninstalled it for that reason, but i will try again and wish good luck."
    author: "fgunni"
  - subject: "That could be a configuration"
    date: 2010-06-11
    body: "That could be a configuration issue. We disabled strigi indexing in packages which might not have come through yet.\r\n\r\nThat appears as nepomuk and virtuoso in resource usage.\r\n\r\nThanks for trying out the packages. Hopefully the experience will improve."
    author: "steveire"
  - subject: "Thank God"
    date: 2010-06-12
    body: "You are doing this!\r\n\r\nNot only for the future in any mobile device but for my N900! The actual email program is...  *HORRIBLE*\r\n\r\nThanks for the effort and the hard work guys\r\n\r\nRegards"
    author: "Damnshock"
  - subject: "I can't believe it!"
    date: 2010-06-12
    body: "It's 2010 and you still develop a system which needs a pen for proper usage! That is a no-go! Why you cannot make an UI which is usable by my fingers?\r\n\r\nOtherwise its cool that this gets ported."
    author: "BurkeOne"
  - subject: "It is usable with a finger."
    date: 2010-06-13
    body: "It is usable with a finger. \r\n\r\nI just used a stylus in the video so that my whole hand did not get in the way of the shot.\r\n\r\nSee the other video where I used my finger:\r\n\r\nhttp://steveire.wordpress.com/2010/04/30/kdepim-on-mobile-whats-going-on/"
    author: "steveire"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kmail_mobile_maemo1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kmail_mobile_maemo1_wee.png" width="300" height="180"/></a><br />KMail Mobile</div>

<h2>News from LinuxTag Berlin</h2>

Today a prototype version of mobile variants of the KDE PIM suite was demoed at LinuxTag (expect more information some time next week). These applications along with GNU/Linux, Mac OS and Windows versions of Kontact accessing a <a href="http://www.kolab.org/">Kolab Server</a> will be on show for the duration of LinuxTag. The application packages themselves can be downloaded by anyone willing to test them out. Since the <a href="http://steveire.wordpress.com/2010/04/30/kdepim-on-mobile-whats-going-on/">last update about KDE PIM Mobile</a>, there have been many visual and functional improvements to the applications.

<h2>Video</h2>

The video demonstrates some prototype versions of applications for email and calendering on the Nokia N900. Most of the underlying KDE Personal Information Management (PIM or Groupware) platform is UI independent and is shared between mobile and desktop variants of the applications. This allows developers to take advantage of the platform while improving it by sharing features across applications on desktop and mobile devices. These applications being demoed at LinuxTag show the flexibility of the KDE platform for creating PIM applications. 

<embed src="http://blip.tv/play/AYHlnmUC" type="application/x-shockwave-flash" width="480" height="414" allowscriptaccess="always" allowfullscreen="true"></embed>
<em><a href="http://blip.tv/file/get/Steveire-KDEPIMMobileApplicationsOnN900476.avi">Download Video as avi</a> | Music from <a href="http://www.jamendo.com/en/album/6833">Robot Wars by Binärpilot</a></em>

It is important to remember that these applications are prototypes and work is ongoing at a fast pace to polish and improve them over the coming months. In the best tradition of the Free Software "Release early, release often" mantra, the applications are <a href="http://files.kolab.org/local/maemo/README.html">available to try out</a> for anyone with a Nokia N900 phone.

The unstable packages are recommended to interested users and testers, with the usual caveats. The unstable packages will be updated automatically as the applications improve and will incorporate feedback from users about usability and functionality. Get in touch if you have any <a href="mailto:kde-mobile-users@kde.org">feedback</a> or if you want to <a href="mailto:kde-mobile@kde.org">help out in development</a>. KDE PIM uses many new platform technologies such as QML, so it provides an exciting playground with plenty of opportunities for innovation and experimentation if you want to get involved.

<h2>UI customized per device</h2>

The design of the KDE PIM platform is largely UI independent, making it easier portable to different devices. The current target platform of the KDE PIM Mobile prototype applications is Maemo on the Nokia N900. Having a stable platform to work on allows the development of some mobile specific parts of the platform which can be reused in future devices.

The current work on KDE PIM Mobile applications will be largely portable to MeeGo based devices in the future. The current choice of targetting Maemo, however, means that the applications can be used on devices which are available in shops today. The top UI layer of the applications will likely need to be customised on a per-device basis because of differences in screen size, orientation, DPI and other factors, but the portability built into the platform itself means that future ports of the applications to new devices will not take as long as the initial development time.

<h2>Cryptographic features</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/kmail_mobile_maemo2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kmail_mobile_maemo2_wee.png" width="300" height="180"/></a><br />Encrypted Email</div>

One of the major strengths of KDE PIM has always been support for cryptographic operations such as signing and encryption. Those operations are also a primary use-case of mobile variants of the KDE PIM applications.  

KMail-mobile makes on-the-move reading and writing of signed and encrypted emails as easy as possible, supporting multiple cryptographic technologies such as OpenPGP and S/MIME.

<h2>On-the-move access to multiple remote stores</h2>

At the center of the KDE PIM platform is <a href="http://pim.kde.org/akonadi/">Akonadi</a>, the generic data cache. Akonadi is a server for UI independent, fast read-write access to remote or local data, such as emails, events and addressees. The KDE PIM platform includes a maturing client library to make it easy for developers to use the server. Because Akonadi includes caching features, it is suitable for use on mobile devices where internet connections may not be available, and where they are available, may not be reliable.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/korganizer_mobile_maemo2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/korganizer_mobile_maemo2_wee.png" width="300" height="180"/></a><br />Access your data from multiple sources</div>

Akonadi solves that problem by operating as a transparent cache to make it easy to access the data you use most often, and by recording writes you make to remote locations, playing them back when the connection is available again. Because all remote data access is outside of application processes, this makes the applications themselves faster and insulated from network related issues.

<h2>Free Software and Your Data</h2>

The KDE PIM platform is entirely Free Software, developed collaboratively in open KDE repositories and communication channels. Well understood benefits of Free Software include easy control and migration of data and adherence to open and accessible protocols and standards. The KDE PIM platform already has support for accessing data from remote providers such as Kolab servers, IMAP servers, Google accounts and more. Any application can easily make use of these resources, and the KDE PIM Mobile applications already do.

Because most available services use open standards for storing data, such as vCard and iCal, it is easy to move away from a service if the data can be retrieved, and easy to migrate to a new service that supports available standards. In a highly cloud connected world, having multiple different storage locations for personal data allows an "eggs in different baskets" approach to managing information about yourself. While it is convenient to have all of your emails, contacts and events hosted by a single service, the aggregation of it leads to privacy concerns because the external service often acts like it owns your data. In some cases they actually do, due to the fine print in their agreement with you (the one you never read).

One option is to fragment the data, storing related information in different locations and different clouds. KDE PIM makes such an approach more convenient, because it provides a point to aggregate the data. Disparate pieces of information can be collected and indexed on devices and desktop computers controlled by the user, instead of on servers controlled by someone else.

<h2>Conclusion</h2>

All in all there are plenty of reasons why the developments shown at LinuxTag are great for the KDE community and Free Software in general. First, because excellent Free Software mail, calendar, notes and addressbook interfaces are coming to devices near us. Secondly because it makes use (and contributes to) an excellent platform we also use on our desktops. Third, this is also introducing a uniquely secure solution which (fourth) can help protect your data like no other client can. So - at LinuxTag the demo devices drew many interested visitors and there is plenty of reason to expect the online demo to do similar. Have a look at this and consider what you can do to make this happen!