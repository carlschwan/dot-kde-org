---
title: "Nokia Releases Qt 4.7 with Qt Quick"
date:    2010-09-22
authors:
  - "jriddell"
slug:    nokia-releases-qt-47-qt-quick
comments:
  - subject: "ML"
    date: 2010-09-22
    body: "Given the name I expected it to be a Qt variant of the ML programming language, possibly as a response to Microsoft's F#. So it was kind of a let down. In any case, seems quite interesting nonetheless."
    author: "jadrian"
  - subject: "No, its a reference to XML."
    date: 2010-09-27
    body: "No, its a reference to XML. :)"
    author: "eean"
---
Nokia has <a href="http://qt.nokia.com/about/news/nokia-releases-qt-4.7">released Qt 4.7</a>.  It features some impressive performance improvements through hardware accelerated compositing in QtWebkit, a new text renderer and painting improvements on OpenGL.  Notably Qt Quick is included, featuring the new QML (Qt Meta-Object-Language) language for writing programs based on outcomes. According to <a href="http://qt.nokia.com/about/news/nokia-releases-qt-4.7">the release</a>, QML is "an easy to learn, declarative language that ‘describes’ the interface of a program and how it behaves."  Sources are available from the <a href="ftp://ftp.trolltech.com/qt/source">FTP server</a>.
