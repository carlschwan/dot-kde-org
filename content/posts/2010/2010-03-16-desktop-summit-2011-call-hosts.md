---
title: "Desktop Summit 2011 - Call for Hosts"
date:    2010-03-16
authors:
  - "nightrose"
slug:    desktop-summit-2011-call-hosts
comments:
  - subject: "Desktop summit"
    date: 2010-05-30
    body: "I think you will easily find hosts, since the Desktop summit is one of the most favorable applications That people like.\r\n"
    author: "Basha besh"
---
The KDE and GNOME communities are looking for a host for the Desktop Summit 2011, the combined annual conference featuring KDE's Akademy and GNOME's GUADEC events. Following up on the successful Gran Canaria Desktop Summit, the second edition of the combined event in 2011 will be the premier place to learn about, discuss, and work on free software on the desktop.

The goal of the desktop summit will be to present and discuss the state of the art of free software for end users, do community building, enable cross-community collaboration, and enable partners from industry and other communities as well as individuals to get informed and involved.
<!--break-->
The desktop summit will consist of a joint conference, combining the Akademy and GUADEC programs, dedicated sub-events about special topics like free desktop standards or mobile platforms, free-form sessions, and extended opportunities for common discussions and getting work done.

The preferred format for the event is to start with a three-day conference over the weekend followed by a week of informal sessions and a coding marathon. Preferred time for the event is summer 2011.

The location should be in Europe and be easily reachable from all over the world. A major international airport is a requirement.

The event needs a strong local organization team, preferably with involvement of local members of both communities. Both communities are prepared to help do global organization and find sponsors.

Hosts should be prepared for 1000 enthusiatic participants. They will need to provide conference facilities, space for collaboration, and enough room for working. Internet should be broadly available and able to host all 1000 attendees.

A range of accommodation from inexpensive to business class hotels should be easily available. Food should be easily available and should include vegetarian and vegan options. There should also be options for social activities outside of the actual event.

For more details about the requirements look at the <a href="http://ev.kde.org/akademy/requirements.php">Akademy requirements</a> and the <a href="http://live.gnome.org/GuadecPlanningHowTo/CheckList">GUADEC requirements</a>. Please keep in mind that the combined event will be twice as big in size as the individual events, and the collaborative nature of the desktop summit might change the details.

If you are interested in hosting the desktop summit, one of the prime free software events in 2011, please send an application to the boards of KDE e.V. (kde-ev-board@kde.org) and the GNOME Foundation (board-list@gnome.org). Please also feel free to contact us in case of any questions. Send in your proposals no later than May 15th.

We are looking forward to a most excellent event. You can be a part as host. Send your proposal now.