---
title: "KDE-CVS-Digest for December 19, 2003"
date:    2003-12-21
authors:
  - "dkite"
slug:    kde-cvs-digest-december-19-2003
comments:
  - subject: "ahhh, my preciousss!"
    date: 2003-12-21
    body: "We wants it!  Thank you Derek.  :-)"
    author: "anon"
  - subject: "Kapture and KDebConf.."
    date: 2003-12-21
    body: "These things are just awesome. Couldn't have asked for anything else to make my day!\n\nIs this KDE's answer to Bruce Peren's exclusion of KDE in his latest project, UserLinux? It looks like now that SuSe has dumped us, its time to pick another Distro.. Lets go KDE/Debian!!!!\n\n:)"
    author: "anon"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Errr... I may have missed something, but AFAIK, SuSE didn't dump KDE at all."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Why should they? Isnt KDe their default desktop? But Suse's poor Gnome support will probably improve. "
    author: "Jupp"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Just because Novell bought SUSE doesn't mean they are going to fire the KDE developers they have on staff and go full blown with XD. So never ass*u*me anything."
    author: "aergern"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "*check* Nope, still working there ;-)"
    author: "Waldo Bastian"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-25
    body: "Waldo,\n\nIs there anything that you could say to clarify the SuSE-desktop issue? Confusion of SuSEs track seems to be huge at the moment.\n\nIMHO SuSE is a great distro - but you should remember that SuSEs greatness on the desktop (mass market) is mostly thanks to the KDE project. Please don't waste one of your biggest strengths..\n"
    author: "jmk"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Well, this news was spread by Ximian bought earlier for less by Novell. I think the market decides and Suse has to listen to the market."
    author: "Jack"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "There are many trolls embedded in your message but for those who truly don't know Kapture and KDebConf are part of the results of this project:\n\nhttp://desktop.kdenews.org/strategy.html"
    author: "anonymous"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Indeed they are welcome tools, especially given that Synaptic, the only GUI for APT at present that's any good, has a lot of usability issues that make it quite a pain to work with.\n\nKPackage, KPortage, Kapture, Konstruct... that's most distributions worked out :)"
    author: "Tom"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "But why is it in kdedebian.\nAs we all know (I assume), it is available for rpm based systems as well.\n\nDoes kapture take rpm in account.\n\n"
    author: "Richard"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Hi,\n\nlet me just briefly express how I feel that the goals of Debian and KDE are the best match out there.\n\nBack in the old days, there were quarrels, but both KDE and Debian matured and have come to share a very large part of their goals. I am totally setup by people attempting to hijack Debian efforts and tell people what to work on or not. Basing their Linux variant (they made it their, not ours) on Debian, but at the same time promoting limitation of the user seems pointless.\n\nThere is no way that Debian Developers (as we know them) will ever accept anything into Debian that limits the choice of KDE, MySQL and other significant Free Software. It's a shame that one of the writers of the social contract believes he can influence Debian in any such way.\n\nThe good thing is that whatever they make, anybody can come and take Debian and KDE and release something the users actually want.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Well said. "
    author: "Eike Hein"
  - subject: "Re: Kapture and KDebConf.."
    date: 2003-12-21
    body: "Well, in fact one of the reasons why it's easier to sell Linux to corporate desktops in Europe than in the States may be due to the fact that KDE is the default desktop environment. KDE already has a desktop market that grows rapidly, esp. since the Munich decision. But a Gnome desktop has backing from some large us corporations and RedHat, it still suffers under the bankrupty of Eazel.\n\nKDe still needs some usability improvements and better OO integration but I believe it is a mature uptodate desktop. 3.1 was a breaktrough on the desktop market"
    author: "Jack"
  - subject: "what's it gonna be called?"
    date: 2003-12-21
    body: "that kde/debian distro sounds cool but what is it gonna be called?\nKebian? "
    author: "Pat"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "kdebian of course\n\n"
    author: "Richard"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "I thought this platform would support the (still important) GTK toolkit given the plan that was offered for User Linux. Errr... uhmmm... kdebian sounds quite exclusive? =)"
    author: "uga"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "Not to mention stupid.  K this and K that, please :)"
    author: "Haakon Nilsen"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "He is kidding.  The name hasn't been decided."
    author: "manyoso"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "We're working under the banner of KDE Enterprise and will be open to targetting other platforms in addition to Debian.\n\nAnd although we're integrating and supporting GTK+, we're still KDE.  GTK+ is basically becoming another toolkit you can develop with under the KDE desktop."
    author: "Navindra Umanee"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "KDevelop for example has excellent support for GTK+ I am told, despite the fact that its name starts with K."
    author: "Waldo Bastian"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "What's the real difference between a KDE application and a qt application? Do all 3rd party KDE programs require QT? Why do shlashdotties alsways say that gpled qt was less free than lgpl of gtk? I don't understand this.\n\nOne example:\nHbasic  (http://hbasic.sf.net), is it a KDE program?"
    author: "Matze"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-22
    body: "KDE applications use the added KDE technologies which include things such as KIO (network/device-transparent file access), XMLUI (which gives the ability to easily customize menus, shortcuts and toolbars), the KDE file dialog, the KDE printing system, the various standard KDE dialogs (e.g. edit toolbars, config windows), KConfig, DCOP (for IPC and scripting), icon (and other) themes... etc, etc.. basically a whole bunch of stuff that improves on or adds to what Qt provides.\n\nHBasic isn't a KDE app, it's a Qt app... but it does work well and blend in quite nicely with a KDE desktop. KBasic, on the other hand, is a KDE app (http://kbasic.org)"
    author: "Aaron J. Seigo"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-22
    body: "<i>\"Why do shlashdotties alsways say that gpled qt was less free than lgpl of gtk? I don't understand this.\"</i>\n\nThey do this because some fairly prominent developers have chosen to pass around this FUD to slur KDE/Qt... despite knowing better.  It is an embarrasment and deeply ironic."
    author: "manyoso"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-21
    body: "Why don't we call it the KNU-System?\n\n"
    author: "Jupp"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-22
    body: "ooh! ooh! the DONALD KNU-System! ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-23
    body: "Knuth?"
    author: "Donny boy"
  - subject: "Re: what's it gonna be called?"
    date: 2003-12-23
    body: "KNU=Knuth's not user"
    author: "Donny boy"
  - subject: "Problem with Plastik checkboxes ?"
    date: 2003-12-21
    body: "Hi,\n\nit seems that the plastik widget style lacks \"half-checked\" checkboxes...\n\nThey appear e.g. when you visualize the permissions of multiple files whose\npermissions differ...\nThat's on kde3.1.4, but as plastik is in cvs, this is maybe cured in 3.2\n"
    author: "ac"
  - subject: "Re: Problem with Plastik checkboxes ?"
    date: 2003-12-21
    body: "Please report problems to bugs.kde.org. Reporting them on the dot is not very usefull. These reports tend to get lost."
    author: "Andr\u00e9 Somers"
  - subject: "Check out Aaron's take on this!"
    date: 2003-12-21
    body: "<a href=\"http://www.kdedevelopers.org/node/view/270\">\nhttp://www.kdedevelopers.org/node/view/270</a>\n<p>\nI totally agree with him.\n"
    author: "Mike"
---
In <a href="http://members.shaw.ca/dkite/dec192003.html">this week's KDE-CVS-Digest</a>:

<A href="http://www.nongnu.org/kimagemap/">KImageMapEditor</A>, an HTML image map editor is now part of <A href="http://quanta.sourceforge.net/">Quanta</A>. 
KConfEdit now supports editing remote KDE configurations over a network. 
<A href="http://www.koffice.org/kchart/">KChart</A> gets update from <A href="http://www.klaralvdalens-datakonsult.se/kdchart/morekdchart.html">KD Chart</A>. 
Kapture, an APT frontend for KDE and KDebConf, a Debian configuration front-end, were imported into the repository.
<!--break-->
